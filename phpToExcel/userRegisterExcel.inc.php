<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */
error_reporting(E_ALL);

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';


// Create new PHPExcel object
echo date('H:i:s') , " Create new PHPExcel object" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
echo date('H:i:s') , " Set document properties" , EOL;
$objPHPExcel->getProperties()->setCreator("Sollogic")
							 ->setLastModifiedBy("Sollogic")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Report");


// Create a first sheet, representing sales data
echo date('H:i:s') , " Add some data" , EOL;
$objPHPExcel->setActiveSheetIndex(0);
$message = "";
	$urObj = new GetUserRegistrationCdrReport;				
  $message = $urObj->getColumnNames();				
	$userArray = $urObj->getUserData($users);
	
$headingRowForRegistration = 14;
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValue('A'.$headingRowForRegistration, 'Current Registration Status < '.date('d M Y h:i:s') .' >');
$objPHPExcel->getActiveSheet()->mergeCells('A'.$headingRowForRegistration.':F'.$headingRowForRegistration);

$objPHPExcel->getActiveSheet()->setCellValue('F'.$headingRowForRegistration, '#12566');

echo date('H:i:s') , " Set fonts" , EOL;
//$objPHPExcel->getActiveSheet()->getStyle('A14')->getFont()->setName('Candara');
$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration)->getFont()->setSize(15);
$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration)->getFont()->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
$objPHPExcel->getActiveSheet()->getStyle('F'.$headingRowForRegistration)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);

$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration.':F'.$headingRowForRegistration)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration.':F'.$headingRowForRegistration)->getFill()->getStartColor()->setARGB('183D5D');
$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration)->getAlignment()->setShrinkToFit(true);

$columnNameArray = $urObj->getColumnNames();			
//echo "<pre>"; print_r($columnNameArray);
$rowForColumnRegistration = $headingRowForRegistration + 1;
$columnRowForRegistrationIndex = array("A", "B", "C", "D", "E", "F");
$countColumnForRegistrationIndex = count($columnRowForRegistrationIndex);
$lastColumnRowForRegistration = $columnRowForRegistrationIndex[$countColumnForRegistrationIndex-1];

//print column heading for the registration
foreach($columnNameArray as $key=>$val){
	//set value in column heading
	$objPHPExcel->getActiveSheet()->setCellValue($columnRowForRegistrationIndex[$key].$rowForColumnRegistration, $val);
	//auto size cell
	$objPHPExcel->getActiveSheet()->getColumnDimension($columnRowForRegistrationIndex[$key])->setAutoSize(true);
	//fill color in cells make it solid first and next fill the color 
	$objPHPExcel->getActiveSheet()->getStyle($columnRowForRegistrationIndex[$key].$rowForColumnRegistration)->getFont()->getColor()->setARGB(PHPExcel_Style_Color::COLOR_WHITE);
	
}

// Set style for header row using alternative method
echo date('H:i:s') , " Set style for header row using alternative method" , EOL;
$objPHPExcel->getActiveSheet()->getStyle($columnRowForRegistrationIndex[0].$rowForColumnRegistration.':'.$lastColumnRowForRegistration.$rowForColumnRegistration)->applyFromArray(
		array(
			'font'    => array(
				'bold'      => true
			),
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			),
			'borders' => array(
				'top'     => array(
 					'style' => PHPExcel_Style_Border::BORDER_THIN
 				)
			),
			'fill' => array(
	 			'type'       => PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
	  			'rotation'   => 90,
	 			'startcolor' => array(
	 				'argb' => '245B8D'
	 			),
	 			'endcolor'   => array(
	 				'argb' => '245B8D'
	 			)
	 		)
		)
);

// Unprotect a cell
echo date('H:i:s') , " Unprotect a cell" , EOL;
$objPHPExcel->getActiveSheet()->getStyle('B'.$rowForColumnRegistration)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);

//dynamic registration row for the user
echo $userRowStartted = $dynamicRowForColumnRegistration = $rowForColumnRegistration + 1;

foreach($userArray as $key1=>$val1){
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$dynamicRowForColumnRegistration, $val1['deviceName']);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$dynamicRowForColumnRegistration, $val1['deviceType']);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$dynamicRowForColumnRegistration, $val1['registerStatus']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$dynamicRowForColumnRegistration, $val1['publicIp']);
			$objPHPExcel->getActiveSheet()->setCellValue('E'.$dynamicRowForColumnRegistration, $val1['expiration']);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$dynamicRowForColumnRegistration, $val1['agentType']);
			$dynamicRowForColumnRegistration++;
}

$objPHPExcel->getActiveSheet()->getStyle('A'.$userRowStartted.':F'.$dynamicRowForColumnRegistration)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
$objPHPExcel->getActiveSheet()->getStyle('A'.$userRowStartted.':F'.$dynamicRowForColumnRegistration)->getFill()->getStartColor()->setARGB('C6C6C6');


// Set page orientation and size
echo date('H:i:s') , " Set page orientation and size" , EOL;
$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

// Rename first worksheet
echo date('H:i:s') , " Rename first worksheet" , EOL;
$objPHPExcel->getActiveSheet()->setTitle('Invoice');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
