<?php
/**
 * Created by Dipin Krishna.
 * Date: 12/12/17
 * Time: 4:51 PM
 */
?>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/broadsoft/adminPortal/config.php");
checkLogin();

require($_SERVER['DOCUMENT_ROOT'] . "/broadsoft/adminPortal/expressSheets/expressSheetFunctions.php");

if(isset($_POST['delete_filter'])) {
	if($_POST['delete_filter'] > 0 && deleteUserFilter($_SESSION["adminId"], $_POST['delete_filter'])) {
		echo '{"success": true}';
		return;
	}

	echo '{"success": false}';
	return;

} else if(isset($_POST['toggle_filter'])) {
	if($_POST['toggle_filter'] > 0 && toggleUserFilter($_SESSION["adminId"], $_POST['toggle_filter'], $_POST['inactive'])) {
		echo '{"success": true}';
		return;
	}

	echo '{"success": false}';
	return;
}

require_once("FilterRulesSql.php");

?>
<?php
// Get params
$all_filters = $_POST['all_filters'];
$all_errors = array();
$filter_sql = "";
$filter_params = array();
$result = array();
$output = array();

$jfr = new FilterRulesSql();
$jfr->set_allowed_functions(array('date_encode'));

foreach ($all_filters as $filter) {

	$filter_id = $filter['filter_id'];
	$a_rules = $filter['rules'];

	if (count($a_rules) == 0) {
		exit;
	}
	$result = $jfr->parse_rules($a_rules);

	$last_error = $jfr->get_last_error();
	if (!is_null($last_error['error_message'])) {
		$all_errors[] = array('filter_id' => $filter_id, 'error' => $last_error);
	}

	$jfr->append_rule_set = true;

}

if (count($all_errors)) {
	$output['errors'] = $all_errors;
} else {

	//Get Group Info
	$sp = $_SESSION['sp'];
	$groupId = $_SESSION['groupId'];
	unset($groupInfoData);
	require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/expressSheets/group_info.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/broadsoft/adminPortal/users/getAllUsers.php");
	require_once("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");

	if (isset($_SESSION["userDeviceList"])) {
		unset($_SESSION["userDeviceList"]);
	}

	if (isset($users)) {

		$stmt = $db->prepare("DROP TABLE IF EXISTS temp_filtered_users");
		$stmt->execute();

		$stmt = $db->prepare("CREATE TEMPORARY TABLE temp_filtered_users (
      				  userId varchar(50),
				      first_name varchar(50),
				      last_name varchar(50),
				      registred varchar(10),
				      activated varchar(10),
				      phoneNumber varchar(10),
				      extension varchar(10),
				      device_name varchar(50),
				      device_type varchar(50),
				      mac_address varchar(50),
				      service_packs text,
				      ep_type varchar(50),
				      fwd varchar(50),
				      fwd_to varchar(50),
				      dnd varchar(10)
    			)");
		$stmt->execute();

		//error_log(print_r($users, true));

		for ($a = 0; $a < count($users); $a++) {
			$userId = $users[$a]["userId"];
			$firstName = $users[$a]["firstName"];
			$lastName = $users[$a]["LastName"];
			$userName = $users[$a]["firstName"] . " " . $users[$a]["LastName"];
			$extension = $users[$a]["extension"];
			$DnD = $users[$a]["DnD"];
			$Fwd = $users[$a]["Fwd"]["active"];
			$remote = $users[$a]["remote"]["active"];
			$department = $users[$a]["department"];
			$servicePack = isset($users[$a]["servicePack"]) ? implode(", ", $users[$a]["servicePack"]) : "";

			if ($DnD == "false") {
				$DnD = "Off";
			} else {
				$DnD = "On";
			}
			if ($Fwd == "true") {
				$Fwd = "Active";
				$FwdTo = $users[$a]["Fwd"]["FwdTo"];
			} else {
				if (isset($users[$a]["Fwd"]["FwdTo"]) && $users[$a]["Fwd"]["FwdTo"] != "") {
					$Fwd = "Not Active";
					$FwdTo = $users[$a]["Fwd"]["FwdTo"];
				} else {
					$Fwd = "&nbsp;";
					$FwdTo = "&nbsp;";
				}
			}
			if ($remote == "true") {
				$remote = "*";
				$remote_number = $users[$a]["remote"]["number"];
			} else {
				$remote = "&nbsp;";
				$remote_number = "&nbsp;";
			}

			$fileName = "/var/www/SASTestingUser/SASTestUsers.csv";
			if (file_exists($fileName)) {
				$objcsv = new UserOperations();
				$searchInCsv = $objcsv->find_user_in_csv($fileName, $userId);
			} else {
				$searchInCsv = "";
			}

			// get user dn number activation response
			$userDn = new Dns ();
			$numberActivateResponse = $userDn->getUserDNActivateListRequest($userId);
			// echo "<pre>"; print_r($numberActivateResponse); die;
			$phoneNumberStatus = "";
			$userPhoneNumber = "";
			if (empty ($numberActivateResponse ['Error'])) {
				if (!empty ($numberActivateResponse ['Success'] [0] ['phoneNumber'])) {
					$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
				}
				if (!empty ($numberActivateResponse ['Success'] [0] ['status'])) {
					$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
				}
			}

			if (isset($users[$a]["registration"]) and count($users[$a]["registration"]) > 0) {
				for ($b = 0; $b < count($users[$a]["registration"]); $b++) {
					$deviceName = $users[$a]["registration"][$b]["deviceName"];
					$expiration = $users[$a]["registration"][$b]["expiration"];
					$epType = $users[$a]["registration"][$b]["epType"];

					$deviceType = $users[$a]["registration"][$b]["deviceType"];
					$macAddress = $users[$a]["registration"][$b]["macAddress"];

					$integration = $users[$a]["registration"][$b]["integration"];
					$ccd = $users[$a]["registration"][$b]["ccd"];

					if ($searchInCsv != "") {
						$isRegUserSasTester = "SAS";
					} else {
						$isRegUserSasTester = "Yes";
					}

					/*
					if (isset($_SESSION["permissions"]["modifyUsers"]) and $_SESSION["permissions"]["modifyUsers"] == "1") {
						$userIdValue = "<a href='#' class='userIdVal numberList' id='" . $userId . "' data-lastname='" . $users[$a]["LastName"] . "' data-firstname='" . $users[$a]["firstName"] . "' >" . $userId . "</a>";
					} else {
						$userIdValue = $userId;
					}

					echo "<tr>"
						.   "<td style='width:5%;' class=\"user_column col_Selected\">"
						.   "<input type='checkbox' class='users_selected' name='usersSelected[]' CHECKED value='" . $userId . "'></td>"
						.   "<td style='width:5%;' class=\"user_column col_Registered\">" . $phoneNumberStatus . "</td>"
						.   "<td style='width:5%;' class=\"user_column col_Registered\">" . $isRegUserSasTester . "</td>"
						.   "<td style='width:6%;' class=\"user_column col_Fist_Name\">" . $firstName . "</td>"
						.   "<td style='width:6%;' class=\"user_column col_Last_Name\">" . $lastName . "</td>"
						.   "<td style='width:6%;' class=\"user_column col_User_Id\">" . $userIdValue . "</td>"
						.   "<td style='width:5%;' class=\"user_column col_Extension\">" . $extension . "</td>"
						.   "<td style='width:6%;' class=\"user_column col_Device_Name\">" . $deviceName . "</td>"
						.   "<td style='width:6%;' class=\"user_column col_Device_Type\">" . $deviceType . "</td>"
						.   "<td style='width:6%;' class=\"user_column col_MAC_Address\">" . $macAddress . "</td>"
						.   "<!--<td style='width:6%;' class=\"user_column col_Department\">" . $department . "</td>-->"
						.   "<td style='width:6%;' class=\"user_column col_Service_Pack\">" . $servicePack . "</td>"
						.   "<!--<td style='width:5%;' class=\"user_column col_Expiration\">" . $expiration . "</td>-->"
						.   "<td style='width:5%;' class=\"user_column col_Type\">" . $epType . "</td>"
						.   "<td style='width:5%;' class=\"user_column col_DnD\">" . $DnD . "</td>"
						.   "<td style='width:5%;' class=\"user_column col_Fwd\">" . $Fwd . "</td>"
						.   "<td style='width:6%;' class=\"user_column col_Fwd_To\">" . $FwdTo . "</td>"
						.   "<!--<td style='width:5%;' class=\"user_column col_Remote_Office\">" . $remote . "</td>-->"
						.   "<!--<td style='width:6%;' class=\"user_column col_Remote_Number\">" . $remote_number . "</td>-->"
						.   "<td style='width:5%;' class=\"user_column col_Polycom_Phone_Services\">" . $integration . "</td>"
						.   "<td style='width:6%;' class=\"user_column col_Custom_Contact_Directory\">" . $ccd . "</td>"
						."</tr>";
					*/


					$stmt = $db->prepare("INSERT INTO temp_filtered_users (
      				  userId,
				      registred,
				      activated,
				      phoneNumber,
				      extension,
				      device_name,
				      dnd
    				) VALUES (?, ?, ?, ?, ?, ?, ?)");
					$stmt->execute(array($userId, $isRegUserSasTester, $phoneNumberStatus, $userPhoneNumber, $extension, $deviceName, $DnD));

				}

			} else {
				$deviceName = $users[$a]["deviceName"];
				$deviceType = $users[$a]["deviceType"];
				$macAddress = $users[$a]["macAddress"];
				$integration = $users[$a]["integration"];
				$ccd = $users[$a]["ccd"];
				if ($searchInCsv != "") {
					$isUnRegUserSasTester = "SAS";
				} else {
					$isUnRegUserSasTester = "No";
				}

				/*
				if (isset($_SESSION["permissions"]["modifyUsers"]) and $_SESSION["permissions"]["modifyUsers"] == "1") {
					$userIdVal = "<a href='#' class='userIdVal numberList' id='" . $userId . "' data-lastname='" . $users[$a]["LastName"] . "' data-firstname='" . $users[$a]["firstName"] . "' >" . $userId . "</a>";
				} else {
					$userIdVal = $userId;
				}
				echo "<tr style=\"background-color:#FFBC3D;\">"
					.   "<td style='width:5%;' class=\"user_column col_Selected\">"
					.   "<input type='checkbox' class='users_selected' name='usersSelected[]' CHECKED value='" . $userId . "'></td>"
					.   "<td style='width:5%;' class=\"user_column col_Registered\">" . $phoneNumberStatus . "</td>"
					.   "<td style='width:5%;' class=\"user_column col_Registered\">" . $isUnRegUserSasTester . "</td>
																			<td style='width:6%;' class=\"user_column col_Fist_Name\">" . $firstName . "</td>
																			<td style='width:6%;' class=\"user_column col_Last_Name\">" . $lastName . "</td>
																		<td style='width:6%;' class=\"user_column col_User_Id\">" . $userIdVal . "</td>
																		<td style='width:5%;' class=\"user_column col_Extension\">" . $extension . "</td>
																		<td style='width:6%;' class=\"user_column col_Device_Name\">" . $deviceName . "</td>
																		<td style='width:6%;' class=\"user_column col_Device_Type\">" . $deviceType . "</td>
																		<td style='width:6%;' class=\"user_column col_MAC_Address\">" . $macAddress . "</td>
																		<!--<td style='width:6%;' class=\"user_column col_Department\">" . $department . "</td>-->
																		<td style='width:6%;' class=\"user_column col_Service_Pack\">" . $servicePack . "</td>
																		<!--<td style='width:5%;' class=\"user_column col_Expiration\">&nbsp;</td>-->
																		<td style='width:5%;' class=\"user_column col_Type\">&nbsp;</td>
																		<td style='width:5%;' class=\"user_column col_DnD\">" . $DnD . "</td>
																		<td style='width:5%;' class=\"user_column col_Fwd\">" . $Fwd . "</td>
																		<td style='width:6%;' class=\"user_column col_Fwd_To\">" . $FwdTo . "</td>
																		<!--<td style='width:5%;' class=\"user_column col_Remote_Office\">" . $remote . "</td>-->
																		<!--<td style='width:6%;' class=\"user_column col_Remote_Number\">" . $remote_number . "</td>-->
																		<td style='width:5%;' class=\"user_column col_Polycom_Phone_Services\">" . $integration . "</td>
																		<td style='width:6%;' class=\"user_column col_Custom_Contact_Directory\">" . $ccd . "</td></tr>";


				*/

				$stmt = $db->prepare("INSERT INTO temp_filtered_users (
      				  userId,
				      registred,
				      activated,
				      phoneNumber,
				      extension,
				      device_name,
				      dnd
    				) VALUES (?, ?, ?, ?, ?, ?, ?)");
				$stmt->execute(array($userId, $isUnRegUserSasTester, $phoneNumberStatus, $userPhoneNumber, $extension, $deviceName, $DnD));

			}
		}
	}
	//error_log("SELECT * FROM temp_filtered_users " . $result['sql']);
	//error_log(print_r($result['bind_params'], true));

	//$stmt = $db->prepare("SELECT * FROM temp_filtered_users $filter_sql");
	//$stmt->execute();
	//error_log( print_r($stmt->fetchAll(PDO::FETCH_ASSOC), true));

	//Save filters to database
	$filters_saved = array();
	foreach ($all_filters as $filter) {

		$filter_id = $filter['filter_id'];
		$a_rules = $filter['rules'];
		$id = $filter['id'];
		$name = $filter['name'];
		$id = addUpdateUserFilter($_SESSION["adminId"], json_encode($a_rules), $name, $id);
		$filters_saved[] = array('filter_id' => $filter_id, 'id' => $id);
	}
	$output['filters_saved'] = $filters_saved;

	//Preset Filters
	parse_str($_POST['preset_filters'], $preset_filters);

	//print_r($preset_filters);

	$preset_filters_sql_array = array();

	if($preset_filters) {

		addUpdatePresetFilter($_SESSION["adminId"], json_encode($preset_filters));

		if ($preset_filters['Filters']) {
			foreach ($preset_filters['Filters'] as $preset_filter) {

				switch ($preset_filter) {
					case 'RegisteredUser' :

						if($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( registred = "YES" ) ';
						} else {
							$preset_filters_sql_array[] = ' ( registred != "YES" ) ';
						}
						break;
					case 'ActivatedUser' :

						if($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( activated = "YES" ) ';
						} else {
							$preset_filters_sql_array[] = ' ( activated != "YES" ) ';
						}
						break;
					case 'ExtensionOnly' :

						if($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( phoneNumber = "" and extension != "" ) ';
						} else {
							$preset_filters_sql_array[] = ' ( !(phoneNumber = "" and extension != "") ) ';
						}
						break;
					case 'DeviceLessUser' :

						if($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( device_name = "" ) ';
						} else {
							$preset_filters_sql_array[] = ' ( device_name != "" ) ';
						}
						break;
					case 'DnD' :

						if($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( dnd = "On" ) ';
						} else {
							$preset_filters_sql_array[] = ' ( dnd != "On" ) ';
						}
						break;
				}

			}
		}
	}

	//print_r($preset_filters_sql_array);

	$sql_query = "";

	if($result['sql']) {
		$sql_query = $result['sql'] . ' AND '. implode(" AND ", $preset_filters_sql_array);
	} else if(count($preset_filters_sql_array)) {
		$sql_query = ' WHERE ' . implode(" AND ", $preset_filters_sql_array);
	}

	$output['query'] = $sql_query;

	$stmt = $db->prepare("SELECT userId FROM temp_filtered_users " . $sql_query);
	$stmt->execute($result['bind_params']);
	$filtered_users = array();
	while ($filtered_user = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$filtered_users[] = $filtered_user['userId'];
	}
	$output['results'] = $filtered_users;
}
echo json_encode($output);
?>
