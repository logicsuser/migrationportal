<?php
/**
 * Created by Dipin Krishna.
 * Date: 12/15/17
 * Time: 11:00 AM
 */
?>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/broadsoft/adminPortal/config.php");
checkLogin();

include_once($_SERVER['DOCUMENT_ROOT'] . "/broadsoft/adminPortal/expressSheets/expressSheetFunctions.php");

$preset_filters = getUserPresetFilters($_SESSION["adminId"]);
$preset_filters_checked = isset($preset_filters->Filters) ? $preset_filters->Filters : array();
$preset_filters_options = isset($preset_filters->Filter) ? $preset_filters->Filter : array();
?>
<form name="userFiltersForm" id="userFiltersForm" method="POST" class="form-horizontal col-sm-20 col-sm-offset-2">

	<div class="panel panel-default panel-forms" id="filterUsersValuesRow" style="display: none;">

		<div class="panel-heading" role="tab" id="filtersTitle">
			<h4 class="panel-title">
				<a class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse"
				   href="#filtersDiv" aria-expanded="true"
				   aria-controls="filtersDiv">Filters</a>
			</h4>
		</div>

		<div id="filtersDiv" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="filtersTitle">

			<div class="panel-body">

				<div class="row formspace well">

					<h1 style="padding-left: 10px;">Preset Filters</h1>

					<div class="user_filter_set" id="defaultUserFiltersDiv">

						<table class="table table-condensed">
							<tr>
								<td style="width: 30px;">
									<input <?php echo in_array('RegisteredUser', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="RegisteredUser"
											class="presetToggleCheckbox"
											data-filter="RegisteredUser">
								</td>
								<td>
									Registred/Unregistered Users
									<div id="RegisteredUser_FilterOptions"  <?php echo in_array('RegisteredUser', $preset_filters_checked) ? "checked" : 'style="display: none;"' ?>>
										<div class="col-xs-20">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->RegisteredUser) && $preset_filters_options->RegisteredUser == 'YES') ? "checked" : ""; ?>
													       type="radio"
													       name="Filter[RegisteredUser]"
													       id="RegisteredUserFilterYES"
													       value="YES">Registered Users
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->RegisteredUser) && $preset_filters_options->RegisteredUser == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[RegisteredUser]"
															id="RegisteredUserFilterNO"
															value="NO">Unregistered Users
												</label>
											</div>

										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input <?php echo in_array('ActivatedUser', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="ActivatedUser"
											class="presetToggleCheckbox"
											data-filter="ActivatedUser">
								</td>
								<td>
									Activated/Not activated Users
									<div id="ActivatedUser_FilterOptions" <?php echo in_array('ActivatedUser', $preset_filters_checked) ? "checked" : 'style="display: none;"' ?>>
										<div class="col-sm-20">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->ActivatedUser) && $preset_filters_options->ActivatedUser == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[ActivatedUser]"
															id="ActivatedUserFilterYES"
															value="YES">Activated Users
												</label>
											</div>

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->ActivatedUser) && $preset_filters_options->ActivatedUser == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[ActivatedUser]"
															id="ActivatedUserFilterNO"
															value="NO">Not Activated Users
												</label>
											</div>

										</div>
									</div>
								</td>
							</tr>
							<!--
							<tr>
								<td>
									<input type="checkbox" name="Filters[]" value="VoiceMessaging" class="presetToggleCheckbox" data-filter="VoiceMessaging">
								</td>
								<td>
									Voice Messaging service
									<div id="VoiceMessaging_FilterOptions" style="display: none;">
										<div class="col-sm-20">

											<div class="radio-inline">
												<label>
													<input type="radio" name="Filter[VoiceMessaging]" id="VoiceMessagingServiceUserFilterYES" value="YES">Enabled
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input type="radio" name="Filter[VoiceMessaging]" id="VoiceMessagingServiceUserFilterNO" value="NO">Disabled
												</label>
											</div>

										</div>
									</div>
								</td>
							</tr>
							-->
							<tr>
								<td>
									<input  <?php echo in_array('ExtensionOnly', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="ExtensionOnly"
											class="presetToggleCheckbox"
											data-filter="ExtensionOnly">
								</td>
								<td>
									Extension only Users
									<div id="ExtensionOnly_FilterOptions" <?php echo in_array('ExtensionOnly', $preset_filters_checked) ? "checked" : 'style="display: none;"' ?>>
										<div class="col-sm-20">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->ExtensionOnly) && $preset_filters_options->ExtensionOnly == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[ExtensionOnly]"
															id="ExtensionOnlyUserFilterYES"
															value="YES">Show
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->ExtensionOnly) && $preset_filters_options->ExtensionOnly == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[ExtensionOnly]"
															id="ExtensionOnlyUserFilterNO"
															value="NO">Hide
												</label>
											</div>

										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input  <?php echo in_array('DeviceLessUser', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="DeviceLessUser"
											class="presetToggleCheckbox"
											data-filter="DeviceLessUser">
								</td>
								<td>
									Device-less Users
									<div id="DeviceLessUser_FilterOptions"  <?php echo in_array('DeviceLessUser', $preset_filters_checked) ? "checked" : 'style="display: none;"' ?>>
										<div class="col-sm-20">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->DeviceLessUser) && $preset_filters_options->DeviceLessUser == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[DeviceLessUser]"
															id="DeviceLessUserUserFilterYES"
															value="YES">Show
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->DeviceLessUser) && $preset_filters_options->DeviceLessUser == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[DeviceLessUser]"
															id="DeviceLessUserUserFilterNO"
															value="NO">Hide
												</label>
											</div>

										</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input <?php echo in_array('DnD', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="DnD"
											class="presetToggleCheckbox"
											data-filter="DnD">
								</td>
								<td>
									Users with DND active/not active
									<div id="DnD_FilterOptions" <?php echo in_array('DnD', $preset_filters_checked) ? "checked" : 'style="display: none;"' ?>>
										<div class="col-sm-20">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->DnD) && $preset_filters_options->DnD == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[DnD]"
															id="DnDUserFilterYES"
															value="YES">On
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->DnD) && $preset_filters_options->DnD == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[DnD]"
															id="DnDUserFilterNO"
															value="NO">Off
												</label>
											</div>

										</div>
									</div>
								</td>
							</tr>
							<!--
							<tr>
								<td>
									<input type="checkbox">
								</td>
								<td>
									SIP(digital)/Analog Users
									<div class="col-sm-20">

										<div class="radio">
											<label>
												<input type="radio" id="" value="YES">SIP
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" id="" value="NO">Analog
											</label>
										</div>

									</div>
									<div class="col-xs-4 pull-right">
										<button type="button" class="btn-averistar-sm">Clear</button>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="checkbox">
								</td>
								<td>
									Device-less Users
									<div class="col-xs-4 pull-right">
										<button type="button" class="btn-averistar-sm">Clear</button>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="checkbox">
								</td>
								<td>
									BLF users (users that are monitoring other users)
									<div class="col-xs-4 pull-right">
										<button type="button" class="btn-averistar-sm">Clear</button>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="checkbox">
								</td>
								<td>
									SCA Primary Users (users having shared call appearances on their devices)
									<div class="col-xs-4 pull-right">
										<button type="button" class="btn-averistar-sm">Clear</button>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="checkbox">
								</td>
								<td>
									SCA Users (SCA Appearances) (users having appearances on other devices)
									<div class="col-xs-4 pull-right">
										<button type="button" class="btn-averistar-sm">Clear</button>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="checkbox">
								</td>
								<td>
									Users with/without specific device type
									<div class="col-xs-4 pull-right">
										<button type="button" class="btn-averistar-sm">Clear</button>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="checkbox">
								</td>
								<td>
									Users with/without specific Service Pack
									<div class="col-xs-4 pull-right">
										<button type="button" class="btn-averistar-sm">Clear</button>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="checkbox">
								</td>
								<td>
									Users with/without specific User Service
									<div class="col-xs-4 pull-right">
										<button type="button" class="btn-averistar-sm">Clear</button>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="checkbox">
								</td>
								<td>
									Users with CFA/CFB/CFNA/CFNR active/not active
									<div class="col-xs-4 pull-right">
										<button type="button" class="btn-averistar-sm">Clear</button>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="checkbox">
								</td>
								<td>
									Users with DND active/not active
									<div class="col-xs-4 pull-right">
										<button type="button" class="btn-averistar-sm">Clear</button>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="checkbox" class="btn-averistar-sm">
								</td>
								<td>
									Users with Custom Profiles
									<div class="col-xs-4 pull-right">
										<button type="button" class="btn-averistar-sm">Clear</button>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<input type="checkbox">
								</td>
								<td>
									Users with specific Custom tag
									<div class="col-xs-4 pull-right">
										<button type="button" class="btn-averistar-sm">Clear</button>
									</div>
								</td>
							</tr>
							-->
						</table>

					</div>
				</div>


				<div class="row formspace well">

					<h1 style="padding-left: 10px;">Advanced Filters</h1>
					<div class="col-xs-24" id="addNewFilterSetButtonDiv">
						<button type="button" id="addNewFilterSet" class="btn pull-right">Add New Advanced Filter</button>
					</div>
				</div>

				<div class="row">
					<button type="button" class="btn-averistar applyFilterBtn">Apply Filters</button>
					<button type="button" id="clearAllFiltersBtn" class="btn btn-averistar pull-right">Disable all Filters</button>
				</div>

				<!-- User List-->
				<div id="filterUsersLoading" class="loading"><img src="/broadsoft/adminPortal/images/ajax-loader.gif"></div>
				<div class="row formspace" id="usersTableDiv" style="display: none;">

					<table id="allUsers" class="scroll tablesorter" style="width:100%;margin:0;">
						<thead>
						<tr>
							<th style="width:5%;" class="user_column col_Selected">Select</th>
							<th style="width:5%;" class="user_column col_Activated">Activated</th>
							<th style="width:5%;" class="user_column col_Registered">Registered</th>
							<th style="width:6%;" class="user_column col_First_Name">First Name</th>
							<th style="width:6%;" class="user_column col_Last_Name">Last Name</th>
							<th style="width:6%;" class="user_column col_User_Id">User Id</th>
							<th style="width:5%;" class="user_column col_Extension">Extension</th>
							<th style="width:6%;" class="user_column col_Device_Name">Device Name</th>
							<th style="width:6%;" class="user_column col_Device_Type">Device Type</th>
							<th style="width:6%;" class="user_column col_MAC_Address">MAC Address</th>
							<!--<th style="width:6%;" class="user_column col_Department">Department</th>-->
							<th style="width:6%;" class="user_column col_Service_Pack">Service Pack</th>
							<!--<th style="width:5%;" class="user_column col_Expiration">Expiration</th>-->
							<th style="width:5%;" class="user_column col_Type">Type</th>
							<th style="width:5%;" class="user_column col_DnD">DnD</th>
							<th style="width:5%;" class="user_column col_Fwd">Fwd</th>
							<th style="width:6%;" class="user_column col_Fwd_To">Fwd To</th>
							<!--<th style="width:5%;" class="user_column col_Remote_Office">Remote Office</th>-->
							<!--<th style="width:6%;" class="user_column col_Remote_Number">Remote Number</th>-->
							<th style="width:5%;" class="user_column col_Polycom_Phone_Services">Polycom Phone Services</th>
							<th style="width:6%;" class="user_column col_Custom_Contact_Directory">Custom Contact Directory</th>
						</tr>
						</thead>
						<tbody style="max-height: 450px;height: auto;">
						<?php

						require_once($_SERVER['DOCUMENT_ROOT'] . "/broadsoft/adminPortal/users/getAllUsers.php");
						require_once("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");

						if (isset($_SESSION["userDeviceList"])) {
							unset($_SESSION["userDeviceList"]);
						}

						$rowIndex = 0;
						if (isset($users)) {

							for ($a = 0; $a < count($users); $a++) {
								$userId = $users[$a]["userId"];
								$firstName = $users[$a]["firstName"];
								$lastName = $users[$a]["LastName"];
								$userName = $users[$a]["firstName"] . " " . $users[$a]["LastName"];
								$extension = $users[$a]["extension"];
								$DnD = $users[$a]["DnD"];
								$Fwd = $users[$a]["Fwd"]["active"];
								$remote = $users[$a]["remote"]["active"];
								$department = $users[$a]["department"];
								$servicePack = isset($users[$a]["servicePack"]) ? implode(", ", $users[$a]["servicePack"]) : "";

								if ($DnD == "false") {
									$DnD = "Off";
								} else {
									$DnD = "On";
								}
								if ($Fwd == "true") {
									$Fwd = "Active";
									$FwdTo = $users[$a]["Fwd"]["FwdTo"];
								} else {
									if (isset($users[$a]["Fwd"]["FwdTo"]) && $users[$a]["Fwd"]["FwdTo"] != "") {
										$Fwd = "Not Active";
										$FwdTo = $users[$a]["Fwd"]["FwdTo"];
									} else {
										$Fwd = "&nbsp;";
										$FwdTo = "&nbsp;";
									}
								}
								if ($remote == "true") {
									$remote = "*";
									$remote_number = $users[$a]["remote"]["number"];
								} else {
									$remote = "&nbsp;";
									$remote_number = "&nbsp;";
								}

								$fileName = "/var/www/SASTestingUser/SASTestUsers.csv";
								if (file_exists($fileName)) {
									$objcsv = new UserOperations();
									$searchInCsv = $objcsv->find_user_in_csv($fileName, $userId);
								} else {
									$searchInCsv = "";
								}

								// get user dn number activation response
								$userDn = new Dns ();
								$numberActivateResponse = $userDn->getUserDNActivateListRequest($userId);
								// echo "<pre>"; print_r($numberActivateResponse); die;
								$phoneNumberStatus = "";
								$userPhoneNumber = "";
								if (empty ($numberActivateResponse ['Error'])) {
									if (!empty ($numberActivateResponse ['Success'] [0] ['phoneNumber'])) {
										$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
									}
									if (!empty ($numberActivateResponse ['Success'] [0] ['status'])) {
										$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
									}
								}

								if (isset($users[$a]["registration"]) and count($users[$a]["registration"]) > 0) {
									for ($b = 0; $b < count($users[$a]["registration"]); $b++) {
										$deviceName = $users[$a]["registration"][$b]["deviceName"];
										$expiration = $users[$a]["registration"][$b]["expiration"];
										$epType = $users[$a]["registration"][$b]["epType"];

										$deviceType = $users[$a]["registration"][$b]["deviceType"];
										$macAddress = $users[$a]["registration"][$b]["macAddress"];

										$integration = $users[$a]["registration"][$b]["integration"];
										$ccd = $users[$a]["registration"][$b]["ccd"];

										if ($searchInCsv != "") {
											$isRegUserSasTester = "SAS";
										} else {
											$isRegUserSasTester = "Yes";
										}

										if (isset($_SESSION["permissions"]["modifyUsers"]) and $_SESSION["permissions"]["modifyUsers"] == "1") {
											$userIdValue = "<a href='#' class='userIdVal numberList' id='" . $userId . "' data-lastname='" . $users[$a]["LastName"] . "' data-firstname='" . $users[$a]["firstName"] . "' >" . $userId . "</a>";
										} else {
											$userIdValue = $userId;
										}
										echo "<tr id='broadsoft_user_$userId' class='broadsoft_users'>"
											. "<td style='width:5%;' class=\"user_column col_Selected\">"
											. "<input type='checkbox' id='users_selected_$userId' class='users_selected checkUserListBox' name='usersSelected[]' CHECKED value='" . $userId . "'></td>"
											. "<td style='width:5%;' class=\"user_column col_Registered\">" . $phoneNumberStatus . "</td>"
											. "<td style='width:5%;' class=\"user_column col_Registered\">" . $isRegUserSasTester . "</td>"
											. "<td style='width:6%;' class=\"user_column col_Fist_Name\">" . $firstName . "</td>"
											. "<td style='width:6%;' class=\"user_column col_Last_Name\">" . $lastName . "</td>"
											. "<td style='width:6%;' class=\"user_column col_User_Id\">" . $userIdValue . "</td>"
											. "<td style='width:5%;' class=\"user_column col_Extension\">" . $extension . "</td>"
											. "<td style='width:6%;' class=\"user_column col_Device_Name\">" . $deviceName . "</td>"
											. "<td style='width:6%;' class=\"user_column col_Device_Type\">" . $deviceType . "</td>"
											. "<td style='width:6%;' class=\"user_column col_MAC_Address\">" . $macAddress . "</td>"
											. "<!--<td style='width:6%;' class=\"user_column col_Department\">" . $department . "</td>-->"
											. "<td style='width:6%;' class=\"user_column col_Service_Pack\">" . $servicePack . "</td>"
											. "<!--<td style='width:5%;' class=\"user_column col_Expiration\">" . $expiration . "</td>-->"
											. "<td style='width:5%;' class=\"user_column col_Type\">" . $epType . "</td>"
											. "<td style='width:5%;' class=\"user_column col_DnD\">" . $DnD . "</td>"
											. "<td style='width:5%;' class=\"user_column col_Fwd\">" . $Fwd . "</td>"
											. "<td style='width:6%;' class=\"user_column col_Fwd_To\">" . $FwdTo . "</td>"
											. "<!--<td style='width:5%;' class=\"user_column col_Remote_Office\">" . $remote . "</td>-->"
											. "<!--<td style='width:6%;' class=\"user_column col_Remote_Number\">" . $remote_number . "</td>-->"
											. "<td style='width:5%;' class=\"user_column col_Polycom_Phone_Services\">" . $integration . "</td>"
											. "<td style='width:6%;' class=\"user_column col_Custom_Contact_Directory\">" . $ccd . "</td>"
											. "</tr>";

										++$rowIndex;
									}
								} else {
									$deviceName = $users[$a]["deviceName"];
									$deviceType = $users[$a]["deviceType"];
									$macAddress = $users[$a]["macAddress"];
									$integration = $users[$a]["integration"];
									$ccd = $users[$a]["ccd"];
									if ($searchInCsv != "") {
										$isUnRegUserSasTester = "SAS";
									} else {
										$isUnRegUserSasTester = "No";
									}

									if (isset($_SESSION["permissions"]["modifyUsers"]) and $_SESSION["permissions"]["modifyUsers"] == "1") {
										$userIdVal = "<a href='#' class='userIdVal numberList' id='" . $userId . "' data-lastname='" . $users[$a]["LastName"] . "' data-firstname='" . $users[$a]["firstName"] . "' >" . $userId . "</a>";
									} else {
										$userIdVal = $userId;
									}
									echo "<tr style=\"background-color:#FFBC3D;\" id='broadsoft_user_$userId' class='broadsoft_users'>"
										. "<td style='width:5%;' class=\"user_column col_Selected\">"
										. "<input type='checkbox' id='users_selected_$userId' class='users_selected checkUserListBox' name='usersSelected[]' CHECKED value='" . $userId . "'></td>"
										. "<td style='width:5%;' class=\"user_column col_Registered\">" . $phoneNumberStatus . "</td>"
										. "<td style='width:5%;' class=\"user_column col_Registered\">" . $isUnRegUserSasTester . "</td>
																					<td style='width:6%;' class=\"user_column col_Fist_Name\">" . $firstName . "</td>
																					<td style='width:6%;' class=\"user_column col_Last_Name\">" . $lastName . "</td>
																				<td style='width:6%;' class=\"user_column col_User_Id\">" . $userIdVal . "</td>
																				<td style='width:5%;' class=\"user_column col_Extension\">" . $extension . "</td>
																				<td style='width:6%;' class=\"user_column col_Device_Name\">" . $deviceName . "</td>
																				<td style='width:6%;' class=\"user_column col_Device_Type\">" . $deviceType . "</td>
																				<td style='width:6%;' class=\"user_column col_MAC_Address\">" . $macAddress . "</td>
																				<!--<td style='width:6%;' class=\"user_column col_Department\">" . $department . "</td>-->
																				<td style='width:6%;' class=\"user_column col_Service_Pack\">" . $servicePack . "</td>
																				<!--<td style='width:5%;' class=\"user_column col_Expiration\">&nbsp;</td>-->
																				<td style='width:5%;' class=\"user_column col_Type\">&nbsp;</td>
																				<td style='width:5%;' class=\"user_column col_DnD\">" . $DnD . "</td>
																				<td style='width:5%;' class=\"user_column col_Fwd\">" . $Fwd . "</td>
																				<td style='width:6%;' class=\"user_column col_Fwd_To\">" . $FwdTo . "</td>
																				<!--<td style='width:5%;' class=\"user_column col_Remote_Office\">" . $remote . "</td>-->
																				<!--<td style='width:6%;' class=\"user_column col_Remote_Number\">" . $remote_number . "</td>-->
																				<td style='width:5%;' class=\"user_column col_Polycom_Phone_Services\">" . $integration . "</td>
																				<td style='width:6%;' class=\"user_column col_Custom_Contact_Directory\">" . $ccd . "</td></tr>";

									++$rowIndex;
								}
							}
						}
						?>
						</tbody>
					</table>

					<div id="users_selected_count">
					</div>
				</div>

			</div>

		</div>

	</div>

</form>
<!-- User Filters Start -->
<script>
    /* Preset Filters */
    $(function () {

        $(".presetToggleCheckbox").click(function () {
            var filter = $(this).data("filter");
            if ($(this).is(":checked")) {
                $("#" + filter + "_FilterOptions").show();
            } else {
                $("#" + filter + "_FilterOptions").hide();
            }
        });

    });
</script>


<script>
    $(function () {

        var filter_set_id = 0;

        //Add New Filter
        $("#addNewFilterSet").click(function () {

            filter_set_id++;
            $('<div id="filter_set_' + filter_set_id + '" class="user_filter_set"></div>').insertBefore("#addNewFilterSetButtonDiv");
            $("#filter_set_" + filter_set_id).loadTemplate($("#filterSetTemplate"),
                {
                    filter_set_id_id: 'filter_set_id_' + filter_set_id,
                    filter_set_id_value: 0,
                    filter_set_name_id: 'filter_set_name_' + filter_set_id,
                    filter_set_name_value: 'Filter ' + filter_set_id,
                    filterUsersValuesDiv_id: 'filterUsersValuesDiv_' + filter_set_id,
                    applyFilterBtn_id: 'applyFilterBtn_' + filter_set_id,
                    css_class: "user_filters active",
                    hide_enable: "display: none;",
                    hide_disable: "",
                    filterUsersSetContainer_id: 'filterUsersSetContainer_' + filter_set_id,
                    filter_delete_id: "deleteUserFilterBtn_" + filter_set_id,
                    filter_enable_id: "enableUserFilterBtn_" + filter_set_id,
                    filter_disable_id: "disableUserFilterBtn_" + filter_set_id,
                    data_id: filter_set_id
                }
            );

            apply_jui_filter(filter_set_id);

        });

        //Apply Filters
        $(".applyFilterBtn").click(function () {
            apply_active_filters();
        });


		<?php
		$saved_user_filters = getAllUserFilters($_SESSION["adminId"]);
		foreach ($saved_user_filters as $saved_user_filter) {
		?>
        filter_set_id++;
        $('<div id="filter_set_' + filter_set_id + '" class="user_filter_set <?php echo $saved_user_filter['inactive'] ? 'inactive' : 'active'; ?>"></div>').insertBefore("#addNewFilterSetButtonDiv");
        $("#filter_set_" + filter_set_id).loadTemplate($("#filterSetTemplate"),
            {
                filter_set_id_id: 'filter_set_id_' + filter_set_id,
                filter_set_id_value: '<?php echo $saved_user_filter['id'] ?>',
                filter_set_name_id: 'filter_set_name_' + filter_set_id,
                filter_set_name_value: '<?php echo $saved_user_filter['filter_name'] ?>',
                filterUsersValuesDiv_id: 'filterUsersValuesDiv_' + filter_set_id,
                applyFilterBtn_id: 'applyFilterBtn_' + filter_set_id,
                css_class: "user_filters <?php echo $saved_user_filter['inactive'] ? 'inactive' : 'active'; ?>",
                hide_enable: "<?php echo $saved_user_filter['inactive'] ? '' : 'display: none;'; ?>",
                hide_disable: "<?php echo $saved_user_filter['inactive'] ? 'display: none;' : ''; ?>",
                filterUsersSetContainer_id: 'filterUsersSetContainer_' + filter_set_id,
                filter_delete_id: "deleteUserFilterBtn_" + filter_set_id,
                filter_enable_id: "enableUserFilterBtn_" + filter_set_id,
                filter_disable_id: "disableUserFilterBtn_" + filter_set_id,
                data_id: filter_set_id
            }
        );

        apply_jui_filter(filter_set_id);

        $("#filterUsersValuesDiv_" + filter_set_id).jui_filter_rules("setRules", jQuery.parseJSON('<?php echo $saved_user_filter['filter']; ?>'));
		<?php
		}
		?>

        $("#allUsers").dataTable({
            "paging": false,
            "ordering": false,
            "info": false,
            "filter": false,
        });


    });

    function apply_jui_filter(filter_id) {

        $("#filterUsersValuesDiv_" + filter_id).jui_filter_rules({

            bootstrap_version: "3",

            filters: [
                {
                    filterName: "Registred", "filterType": "text", field: "registred", filterLabel: "Registred",
                    excluded_operators: ["in", "not_in",
                        "less", "less_or_equal",
                        "greater", "greater_or_equal",
                        "is_null", "is_not_null",
                        "begins_with", "not_begins_with", "contains", "not_contains", "ends_with", "not_ends_with"
                    ],
                    filter_interface: [
                        {
                            filter_element: "select"
                        }
                    ],
                    lookup_values: [
                        {lk_option: "No", lk_value: "No"},
                        {lk_option: "Yes", lk_value: "Yes", lk_selected: "yes"}
                    ]
                },
                {
                    filterName: "Activated", "filterType": "text", field: "activated", filterLabel: "Activated",
                    excluded_operators: ["in", "not_in",
                        "less", "less_or_equal",
                        "greater", "greater_or_equal",
                        "is_null", "is_not_null",
                        "begins_with", "not_begins_with", "contains", "not_contains", "ends_with", "not_ends_with"
                    ],
                    filter_interface: [
                        {
                            filter_element: "select"
                        }
                    ],
                    lookup_values: [
                        {lk_option: "No", lk_value: "No"},
                        {lk_option: "Yes", lk_value: "Yes", lk_selected: "yes"}
                    ]
                },
                {
                    filterName: "DND", "filterType": "text", field: "dnd", filterLabel: "DND",
                    excluded_operators: ["in", "not_in",
                        "less", "less_or_equal",
                        "greater", "greater_or_equal",
                        "is_null", "is_not_null",
                        "begins_with", "not_begins_with", "contains", "not_contains", "ends_with", "not_ends_with"
                    ],
                    filter_interface: [
                        {
                            filter_element: "select"
                        }
                    ],
                    lookup_values: [
                        {lk_option: "Off", lk_value: "Off"},
                        {lk_option: "On", lk_value: "On", lk_selected: "yes"}
                    ]
                }
            ],

            onValidationError: function (event, data) {
                alert(data["err_description"] + ' (' + data["err_code"] + ')');
                if (data.hasOwnProperty("elem_filter")) {
                    data.elem_filter.focus();
                }
            }

        });

        //Delete Filter
        $("#deleteUserFilterBtn_" + filter_id).click(function () {
            deleteFilter(this);
        });

        //Enable/Disable Filter
        $("#enableUserFilterBtn_" + filter_id).click(function () {
            toggleFilter(this, 0);
        });
        $("#disableUserFilterBtn_" + filter_id).click(function () {
            toggleFilter(this, 1);
        });

    }

    //Toggle apply filter button
    function toggle_apply_filter_btn() {

        if ($(".user_filters.active").length > 0) {
            $("#applyFilterBtn").show();
        } else {
            if ($(".user_filters").length > 0) {
                $("#applyFilterBtn").prop('disabled', true);
            } else {
                $("#applyFilterBtn").hide();
            }
        }

    }

    //Apply Active Filters
    function apply_active_filters() {

        var all_filters = [];
        var filter_ids = [];
        $(".user_filters.active").each(function () {
            var filter_id = $(this).data('id');
            var a_rules = $("#filterUsersValuesDiv_" + filter_id).jui_filter_rules("getRules", 0, []);

            if (a_rules.length === 0) {
                alert("No rules defined...");
                $("#filter_set_" + filter_id).addClass("error");
                all_filters = null;
                return false;
            }
            $("#filter_set_" + filter_id).removeClass("error");
            filter_ids.push(filter_id);

            var filter = {};
            filter['rules'] = a_rules;
            filter['id'] = $("#filter_set_id_" + filter_id).val();
            filter['name'] = $("#filter_set_name_" + filter_id).val();
            filter['filter_id'] = filter_id;

            all_filters.push(filter);
        });

        //if (all_filters) {
        $("#filterUsersLoading").show();
        $("#usersTableDiv").hide();

      //  $(".users_selected").prop("disabled", true);

        var preset_filters = $("#userFiltersForm").serialize();

        $(".users_selected").prop("disabled", false);

        $(".users_selected").prop('checked', false);

        $.ajax({
            type: 'POST',
            url: "expressSheets/userFilters/filter_users.php",
            data: {
                all_filters: all_filters,
                preset_filters: preset_filters
            },
            dataType: "JSON",
            success: function (data) {

                if (data.hasOwnProperty("errors")) {
                    $.each(data["errors"], function (i, filterError) {
                        $("#filterUsersValuesDiv_" + filterError.filter_id)
                            .jui_filter_rules("markRuleAsError", data["error"]["element_rule_id"], true)
                            .triggerHandler("onValidationError",
                                {
                                    err_code: "filter_error_server_side",
                                    err_description: "Server error during filter converion..." + '\n\n' + data["error"]["error_message"]
                                }
                            );
                    });
                } else {

                    $(".broadsoft_users").hide();
                    if (data.results) {
                        $.each(data["results"], function (i, userId) {
                            $("#broadsoft_user_" + userId).show();
                            $("#users_selected_" + userId).prop("checked", true);
                        });
                    }
                    if (data.results) {
                        $.each(data["filters_saved"], function (i, filter_saved) {
                            $("#filter_set_id_" + filter_saved.filter_id).val(filter_saved.id);
                        });
                    }
                    $(filter_ids).each(function (i, filter_id) {
                        $("#filter_set_" + filter_id).addClass("success");
                    });

                }

                $("#filterUsersLoading").hide();
                $("#usersTableDiv").show();

                updateSelectedUsersCount();

            }
        });

        //}
    }

    function deleteFilter(deleteBtnObj) {

        var filter_id = $(deleteBtnObj).data('id');
        if (confirm("Are you sure?")) {
            var save_id = $(deleteBtnObj).data('save-id');
            if (save_id > 0) {

                $.ajax({
                    type: 'POST',
                    url: "expressSheets/userFilters/filter_users.php",
                    data: {
                        delete_filter: save_id
                    },
                    dataType: "JSON",
                    success: function (data) {

                        if (data.success) {
                            $("#filterUsersValuesDiv_" + filter_id).removeClass('active');
                            $("#filter_set_" + filter_id).slideUp(500, function () {
                                $(this).remove();
                                toggle_apply_filter_btn();
                                apply_active_filters();
                            });
                        } else {
                            alert("Filter was not deleted.");
                        }

                    }
                });

            } else {
                $("#filter_set_" + filter_id).slideUp(500, function () {
                    $(this).remove();
                });
                toggle_apply_filter_btn();
                apply_active_filters();
            }
        }

    }

    function toggleFilter(toggleBtnObj, inactive) {

        var filter_id = $(toggleBtnObj).data('id');
        //if(confirm("Are you sure?")) {
        var save_id = $(toggleBtnObj).data('save-id');
        if (save_id > 0) {

            $.ajax({
                type: 'POST',
                url: "expressSheets/userFilters/filter_users.php",
                data: {
                    toggle_filter: save_id,
                    inactive: inactive
                },
                dataType: "JSON",
                success: function (data) {

                    if (data.success) {

                        $(toggleBtnObj).hide();

                        if (inactive) {
                            $("#filter_set_" + filter_id).removeClass('success');
                            $("#filter_set_" + filter_id).addClass('inactive');
                            $("#filterUsersValuesDiv_" + filter_id).removeClass('active');
                            $("#filterUsersValuesDiv_" + filter_id).addClass('inactive');
                            $("#filterUsersValuesDiv_" + filter_id).slideUp(500, function () {
                                $(this).hide();
                                $("#enableUserFilterBtn_" + filter_id).show();
                            });
                        } else {
                            $("#filter_set_" + filter_id).removeClass('inactive');
                            $("#filterUsersValuesDiv_" + filter_id).addClass('active');
                            $("#filterUsersValuesDiv_" + filter_id).removeClass('inactive');
                            $("#filterUsersValuesDiv_" + filter_id).slideDown(500, function () {
                                $(this).show();
                                $("#disableUserFilterBtn_" + filter_id).show();
                            });
                        }

                        toggle_apply_filter_btn();
                        apply_active_filters();

                    } else {
                        alert("Filter was not disbaled.");
                    }

                }
            });

        } else {

            $(toggleBtnObj).hide();

            if (inactive) {
                $("#filter_set_" + filter_id).removeClass('success');
                $("#filter_set_" + filter_id).addClass('inactive');
                $("#filterUsersValuesDiv_" + filter_id).removeClass('active');
                $("#filterUsersValuesDiv_" + filter_id).addClass('inactive');
                $("#filterUsersValuesDiv_" + filter_id).slideUp(500, function () {
                    $(this).hide();
                    $("#enableUserFilterBtn_" + filter_id).show();
                });
            } else {
                $("#filter_set_" + filter_id).removeClass('inactive');
                $("#filterUsersValuesDiv_" + filter_id).addClass('active');
                $("#filterUsersValuesDiv_" + filter_id).removeClass('inactive');
                $("#filterUsersValuesDiv_" + filter_id).slideDown(500, function () {
                    $(this).show();
                    $("#disableUserFilterBtn_" + filter_id).show();
                });
            }

            toggle_apply_filter_btn();
            apply_active_filters();

        }
        //}

    }

</script>

<script type="text/html" id="filterSetTemplate">
	<input type="hidden"
	       class="form-control"
	       data-template-bind='[{"attribute": "data-id", "value": "data_id"}]'
	       data-id="filter_set_id_id"
	       data-value="filter_set_id_value"/>
	<div class="row">
		<div class="col-xs-8">
			<input type="text"
			       class="form-control"
			       data-template-bind='[{"attribute": "data-id", "value": "data_id"}]'
			       data-id="filter_set_name_id"
			       data-value="filter_set_name_value"/>
		</div>
		<div class="col-xs-16">
			<button type="button"
			        data-template-bind='[{"attribute": "data-id", "value": "data_id"}, {"attribute": "data-save-id", "value": "filter_set_id_value"}, {"attribute": "style", "value": "hide_disable"}]'
			        data-id="filter_disable_id"
			        class="disableUserFilterBtn">Disable
			</button>
			<button type="button"
			        data-template-bind='[{"attribute": "data-id", "value": "data_id"}, {"attribute": "data-save-id", "value": "filter_set_id_value"}, {"attribute": "style", "value": "hide_enable"}]'
			        data-id="filter_enable_id"
			        class="enableUserFilterBtn">Enable
			</button>
			<button type="button"
			        data-template-bind='[{"attribute": "data-id", "value": "data_id"}, {"attribute": "data-save-id", "value": "filter_set_id_value"}]'
			        data-id="filter_delete_id"
			        class="btn-danger deleteUserFilterBtn">Delete
			</button>
		</div>
	</div>
	<div data-id="filterUsersValuesDiv_id"
	     data-template-bind='[{"attribute": "data-id", "value": "data_id"}]'
	     data-class="css_class"
	     style="width: -moz-fit-content;width: fit-content;"></div>
</script>
<style>
	.user_filter_set {
		/* border: 1px dashed black; */
		padding: 10px;
		margin: 20px 10px
	}

	.user_filter_set.error {
		background: #ce0d0d;
	}

	.user_filter_set.success {
		background: #9cc99c;
	}

	.user_filter_set.inactive {
		background: #fff4b0;
	}

	.user_filters.inactive {
		display: none;
	}

	.user_filters.active {
		display: block;
	}

</style>
<!-- User Filters End -->
