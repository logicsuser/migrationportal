<?php
/**
 * Created by Sollogics .
 * Date: 22/1/18
 * Time: 8:07 PM
 */

if(isset($_GET['download_id']) && file_exists("/tmp/" . $_GET['download_id'])) {
	
	$download_status = json_decode(file_get_contents("/tmp/" . $_GET['download_id']), true);
	
	if(isset($_GET['download'])) {
		$file_name = $download_status['fileName'];
		$file_path = $download_status['filePath'];
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Transfer-Encoding: Binary");
		header("Content-disposition: attachment; filename=\"$file_name\"");
		readfile($file_path);
		exit;
	} else {
		$download_status['filePath'] = "";
		echo file_get_contents("/tmp/" . $_GET['download_id']);
	}
} else {
	echo json_encode(array("success" => false));
}
exit;
?>