<?php
session_start();

require_once "functions.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	//free all session variables
	session_unset();

	$uName = isset($_POST["userName"]) ? $_POST["userName"] : "";
	$pWord = isset($_POST["password"]) ? $_POST["password"] : "";
	$is_json = isset($_POST["json"]) ? $_POST["json"] : "0";
	$continue_session = isset($_POST["continue_session"]) ? $_POST["continue_session"] : "0";

	$db_validation = false;

	if(strlen($uName) <= 0 || strlen($pWord) <= 0) {
		echo '{"success" : false, "message":"Username/Password missing."}';
		exit;
	}
	//check input for malicious values
	//if (!preg_match("/^[A-Za-z0-9\.\+\-@]+$/", $uName)) {
	if (!preg_match("/^[A-Za-z0-9\-_.@]+$/", $uName)) {
		if ($is_json == "1") {
			echo '{"success" : false, "message":"Invalid Character in Username."}';
			exit;
		}
		$_SESSION["loginFailure"] = "true";
		unset($_SESSION["permissions"]);
		$_SESSION["invalidCharacter"] = "true";
	} else {
		//get settings from .ini file
		$settings = parse_ini_file("/var/www/adminPortal.ini", TRUE);

		//MySQL database connections
		require_once("db.php");

		require_once("/var/www/html/Express/config.php");

		//Get ldap info
		$systemConfig = new DBSimpleReader($db, "systemConfig");
		$ldapAuthentication = $systemConfig->get("ldapAuthentication");
		$smsAuthentication = $systemConfig->get("smsAuthentication");
		$ldapHost = $systemConfig->get("ldapHost");
		$ldapDomain = $systemConfig->get("ldapDomain");
		$ldapCapitalization = $systemConfig->get("ldapCapitalization");
        
		//check if user will use BroadSoft or MySQL database to validate login
		$bsQuery = "select bsAuthentication, superUser, use_ldap_authentication from users where userName = ? and disabled != '1' limit 1";

		$bsCount = 0;
		$bsQueryStmt = $db->prepare($bsQuery);
		if($bsQueryStmt->execute(array($uName))) {
			$bsResults = $bsQueryStmt->fetchAll(PDO::FETCH_ASSOC);
			$bsCount = count($bsResults);
		}

		$uNameForDomain = "";
		if ( $bsCount == 0 && strpos($uName, '@') !== false ) {
		    $tempUName = explode("@", $uName);
		        $uNameForDomain = $uName;
		        $bsQuery = "select bsAuthentication, superUser, use_ldap_authentication from users where userName = ? and use_ldap_authentication = ? and disabled != '1' limit 1";
		        $bsCount = 0;
		        $bsQueryStmt = $db->prepare($bsQuery);
		        if($bsQueryStmt->execute(array($tempUName[0], 1))) {
		            $bsResults = $bsQueryStmt->fetchAll(PDO::FETCH_ASSOC);
		            $bsCount = count($bsResults);
		        }
		        if($bsCount > 0) {
		            $uName = $tempUName[0];
		        }
		}
		
		if ($bsCount > 0) {

			$superUser = 0;
			$use_ldap_authentication = 0;
			$bsAuthentication = 0;

			foreach ($bsResults as $bsRow) {
				$bsAuthentication = $bsRow["bsAuthentication"];
				$superUser = $bsRow['superUser'];
				$use_ldap_authentication = $bsRow['use_ldap_authentication'];
			}

			if ($bsAuthentication == "1") //validate against BroadSoft
			{
				require_once("/var/www/lib/broadsoft/login.php");
				$xmlinput = xmlHeader(session_id(), "AuthenticationVerifyRequest14sp8");
				$xmlinput .= "<userId>" . $uName . "</userId>";
				$xmlinput .= "<password>" . $pWord . "</password>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
				if (!isset($xml->command->userId)) {
					$_SESSION["loginFailure"] = "true";
				}
			} else //validate against MySQL database
			{

				//IF LDAP authentication is enabled, use ldap for superUser != 2
				if ($ldapAuthentication == "true" && $use_ldap_authentication == 1) {
				    /* Check LDAP Domain. */
				    $userNameForLdapServer = $uNameForDomain != "" ? $uNameForDomain : $uName;
				    $uNameTemp = explode("@", $userNameForLdapServer);
				    if( !isset($uNameTemp[1]) && $ldapDomain == "") {
				        echo '{"success" : false, "message":"Invalid user name: required username@&lt;LDAP domain&gt;."}';
				        exit;
				    } else if ( !isset($uNameTemp[1]) && $ldapDomain != "" ) {
				        $userNameForLdapServer = $uNameTemp[0] . "@" . $ldapDomain;				        
				    }
				    $userNameForLdapServer = ($ldapCapitalization == "true") ? strtoupper($userNameForLdapServer) : $userNameForLdapServer;
					$ldap_connection = ldap_connect($ldapHost);
					
					$_SESSION["loginFailure"] = "true";

					if ($ldap_connection) {
					    if ($bind = ldap_bind($ldap_connection, $userNameForLdapServer, $pWord)) {
							$_SESSION["loginFailure"] = "false";
							ldap_close($bind);
						}
					} else {
						error_log("Could not connect to LDAP server.");
					}

					if ($_SESSION["loginFailure"] == "true") {

						if ($is_json == "1") {

							unset($_SESSION["loginFailure"]);
							echo '{"success" : false, "message":"Username/Password not correct."}';
						} else {
							header("Location: " . $_SERVER['HTTP_REFERER']);
						}
						exit;
					}
				} else {

					$db_validation = true;

					$loginQuery = "select * from users where userName='" . $uName . "' and password='" . md5($pWord) . "' and disabled!='1'";
					$loginCount = $db->query($loginQuery)->rowCount();

					if ($loginCount <= 0) {
						$_SESSION["loginFailure"] = "true";
					}

				}
			}
		} else {
			$_SESSION["loginFailure"] = "true";
		}

		if (isset($_SESSION["loginFailure"]) and $_SESSION["loginFailure"] == "true") {
			unset($_SESSION["permissions"]);

			//increment number of failed login attempts
			$failureQuery = "update users set failedLogins = failedLogins + 1 where userName = ?";
			$failureQueryStmt = $db->prepare($failureQuery);
			$failureQueryStmt->execute(array($uName));
			
			//update lastLogin column in case of faillure login
			$date = new DateTime("NOW");
			$lastLoginQuery = "update users set lastLogin = '{$date->format('Y-m-d H:i:s')}' where userName = ?";
			$lastLoginQueryStmt = $db->prepare($lastLoginQuery);
			$lastLoginQueryStmt->execute(array($uName));

			//retrieve number of failed login attempts; if 3 or more, disable user
			$checkQuery = "select failedLogins, id, userName, firstName, lastName from users where userName='" . $uName . "'";
			$checkResults = $db->query($checkQuery);
			while ($checkRow = $checkResults->fetch()) {

				//Add Admin Log Entry - Start
				require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
				$adminLogs = new adminLogs();
				$log = array(
					'adminUserID' => $checkRow["id"],
					'eventType' => 'FAILED_ATTEMPT',
					'adminUserName' => $checkRow["userName"],
					'adminName' => $checkRow["firstName"] . ' ' . $checkRow["lastName"],
					'updatedBy' => $checkRow["id"],
					'details' => array(
						'Failed Logins' => $checkRow["failedLogins"]
					)
				);
				//Add Admin Log Entry - End

				if ($checkRow["failedLogins"] >= $maxFailedLoginAttempts) {
					$_SESSION["failedLoginLimit"] = "true";

					$disableQuery = "update users set disabled = '1' where userName = ?";
					$disableQueryStmt = $db->prepare($disableQuery);
					$disableQueryStmt->execute(array($uName));

					//Add Admin Log Entry - Suspension
					$log['eventType'] = 'SUSPENSION';
					$adminLogs->addLogEntry($log);

					if ($is_json == "1") {
						//free all session variables
						session_unset();
						echo json_encode(array("success" => false, "message" => "You've reached the limit for failed login attempts and your account has been disabled. Contact Support to reset your password."));
						exit;
					}
				}
				//Add Admin Log Entry - Failed Attempt
				$adminLogs->addLogEntry($log);
			}
		} else {

			if (isset($_SESSION["loginMsg"])) {
				unset($_SESSION["loginMsg"]);
			}

			$user = get_user_from_username($uName);
			if ($user) {

				if($db_validation && ! checkPasswordStrength($adminPasswordRules, $pWord, $password_errors)) {
					$_SESSION['RESET_PASSWORD_MESSAGE'] = "Your password doesn't meet our latest password requirements. Please set a new password.";
				}

				//Continue Previous Session
				$_SESSION["continue_session"] = $continue_session;

				if ($smsAuthentication == 'true' && $user->use_sms_every_login == 1) {
					//free all session variables
					session_unset();
					$_SESSION["needTwoFactorAuthentication"] = 1;
					$_SESSION["twoFactorAuthUserName"] = $uName;
				} else {

					$_SESSION["authenticatedUserName"] = $uName;
				}

				//error_log(print_r($_SESSION, true));
			}
		}

		if ($is_json == "1") {

			if (isset($_SESSION["loginFailure"]) and $_SESSION["loginFailure"] == "true") {
				//free all session variables
				session_unset();
				echo json_encode(array("success" => false, "message" => "Username/Password not correct."));
			} else {
				ob_clean(); //Make sure nothing has been outputed
				$dateSuccess = new DateTime("NOW");
				$lastLoginQuerySuc = "update users set lastLogin = '{$dateSuccess->format('Y-m-d H:i:s')}' where userName = ?";
				$lastLoginQueryStmtSuc = $db->prepare($lastLoginQuerySuc);
				$lastLoginQueryStmtSuc->execute(array($uName));
				
				echo json_encode(array("success" => true));
			}
			exit;
		} else {
			require_once("/var/www/lib/broadsoft/adminPortal/getLoginHeader.php");
			require_once("/var/www/lib/broadsoft/login.php");
			$headerLoginDetails = new GetLoginHeader();
			$_SESSION['headerLoginDetailsOutput'] = $headerLoginDetails->loginHeader;
		}

	}
}
?>