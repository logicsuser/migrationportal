<?php
if(php_sapi_name() !== 'cli') {
	require_once("db.php");
	if (session_id() == '') {
		session_start();
	}
}

require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
require_once("/var/www/lib/broadsoft/adminPortal/util/ConfigUtil.php");
$expProvDB = new ExpressProvLogsDB();
$GLOBALS['expressProvLogsDb'] = $expProvDB->expressProvLogsDb;

function setLoginSession($user)
{
	global $db;

	if(isset($_SESSION['continue_session'])) {
		$continue_session = $_SESSION["continue_session"];                
	}
        
        //Code added @ 17 June 2019
        if(isset($_SESSION['continue_session']) && isset($_SESSION['selectedCluster'])){
            $clusterNameSess      = $_SESSION['selectedCluster'];
        }
        //End code

	if(isset($_SESSION['RESET_PASSWORD_MESSAGE'])) {
		$reset_password_message = $_SESSION["RESET_PASSWORD_MESSAGE"];
	}

	//free all session variables
	session_unset();

	//reset failed login counter & Set User session id - used to prevent login from multiple locations/browsers.
	$session_id = session_id();
	$resetQuery = $db->prepare("update users set failedLogins = 0, session_id = ? where id = ?");
	$resetQuery->execute(array($session_id, $user->id));

	$_SESSION["loggedInUserName"] = $user->userName;
	$_SESSION["loggedInUser"] = $user->firstName . " " . $user->lastName;
	$_SESSION["adminId"] = $user->id;
	$_SESSION["emailAddress"] = $user->emailAddress;
	$_SESSION["superUser"] = $user->superUser;
	$_SESSION["adminType"] = isset($user->adminType) ? $user->adminType : "";
	$_SESSION["use_ldap_authentication"] = $user->use_ldap_authentication;
        
        if(($_SESSION["superUser"] == "3" || $_SESSION["superUser"] == "0") && strlen($user->clusterName)) {
                $_SESSION["selectedCluster"] = $user->clusterName;
	}
        
        //echo "<br /> - selectedCluster - ".$_SESSION["selectedCluster"];
        
	if ($user->bsAuthentication !== "1") //only retrieve dayDifference value for users without BroadSoft validation
	{
		$_SESSION["dayDifference"] = $user->dayDifference;
	}
	if(isset($reset_password_message)) {
		$_SESSION["RESET_PASSWORD_MESSAGE"] = $reset_password_message;
	}
	$_SESSION['LAST_ACTIVITY'] = time();

	ob_start();
	require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getLoginHeader.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/login.php");
	global $bwAuthLevel;

	if (array_key_exists('client', $GLOBALS)) {
		$client = $GLOBALS['client'];
	}

	if (array_key_exists('sessionid', $GLOBALS)) {
		$sessionid = $GLOBALS['sessionid'];
	}

	if ($user->superUser == "2") {
	} else if ($user->superUser == "1") {
	} else if ($user->superUser == "3") {
                $a = 0;
		unset($_SESSION["sp"]);
		$grpListOfAdmin = array();
		$spListOfAdmin = array();
		$_SESSION['showAdminReportMsg'] = false;
		
		$permissionsQuery = "SELECT sp, securityDomain from permissions where userId = ?";
		$permissionsQueryStmt = $db->prepare($permissionsQuery);
		$permissionsQueryStmt->execute(array($user->id));

                $groupList = array();
		while ($permissionsRow = $permissionsQueryStmt->fetch(PDO::FETCH_ASSOC)) {
                    
                        $groupList                   = getGroupListBasedOnServiceProvider($permissionsRow["sp"], $sessionid, $client, $groupList);
                        $_SESSION["groups"]          = $groupList;    
			
			$_SESSION["spListNameId"][$a] = $permissionsRow["sp"];
                        $_SESSION["spLists"]          = $permissionsRow["sp"];
                        
                        $spName                       = $permissionsRow["sp"];
                        if(strpos($permissionsRow["sp"], "<span>") !== false){
                            $spNameArr                = explode("<span>", $permissionsRow["sp"]);
                            $spName                   = $spNameArr[0];
                        }
                        $_SESSION["spList"][$a]       = $spName;
                        
			$_SESSION['adminReportMessage'] = $securityDomainPattern != "" ? true : false;
                    
			$securityDomain = $permissionsRow["securityDomain"];
			if ($securityDomain != "") {
				$_SESSION['showAdminReportMsg'] = true;
				$_SESSION["securityDomain"] = $securityDomain;
				$spList = getSecurityDomainSp($securityDomain, $client, $sessionid);
				$spListNameId = getSecurityDomainSpNameId($securityDomain, $client, $sessionid);
				$_SESSION["groups"] = getAllSecurityDomainGroups($spList, $securityDomain, $client, $sessionid);
				                                
                                if (count($spList) == 1) {
					$_SESSION["sp"] = $spList[0];
					$_SESSION['spListNameId'] = $spList ;
				} else {
				    $_SESSION["spList"] = $spList;
				    $_SESSION['spListNameId'] = $spListNameId ;
				}
                                
                                $_SESSION["spLists"]  = $spList;
				
				$_SESSION['adminReportMessage'] = $securityDomainPattern == "" ? true : false;
				break;
			}                        
			//$_SESSION["groups"][$a] = $permissionsRow["groupId"];   //Code will be commented        
                        /*$groupList          = getGroupListBasedOnServiceProvider($permissionsRow["sp"], $sessionid, $client, $groupList);
                        $_SESSION["groups"] = $groupList;    
			
			$_SESSION["spListNameId"][$a] = $permissionsRow["sp"];
			$_SESSION['adminReportMessage'] = $securityDomainPattern != "" ? true : false;*/
			$a++;
		}
		/* If Admin does not have security domain */
		$_SESSION["spListNameId"] = array_unique($_SESSION["spListNameId"]);            
        } else //retrieve list of groups for regular admins
	{
		$a = 0;
		unset($_SESSION["sp"]);
		$grpListOfAdmin = array();
		$spListOfAdmin = array();
		$_SESSION['showAdminReportMsg'] = false;
		
		$permissionsQuery = "SELECT groupId, sp, securityDomain from permissions where userId = ?";
		$permissionsQueryStmt = $db->prepare($permissionsQuery);
		$permissionsQueryStmt->execute(array($user->id));

		while ($permissionsRow = $permissionsQueryStmt->fetch(PDO::FETCH_ASSOC)) {
			$securityDomain = $permissionsRow["securityDomain"];
			if ($securityDomain != "") {
				$_SESSION['showAdminReportMsg'] = true;
				$_SESSION["securityDomain"] = $securityDomain;
				$spList = getSecurityDomainSp($securityDomain, $client, $sessionid);
				$spListNameId = getSecurityDomainSpNameId($securityDomain, $client, $sessionid);
				$_SESSION["groups"] = getAllSecurityDomainGroups($spList, $securityDomain, $client, $sessionid);
				if (count($spList) == 1) {
					$_SESSION["sp"] = $spList[0];
					$_SESSION['spListNameId'] = $spList ;
				} else {
				    $_SESSION["spList"] = $spList;
				    $_SESSION['spListNameId'] = $spListNameId ;
				}
				
				$_SESSION['adminReportMessage'] = $securityDomainPattern == "" ? true : false;
				break;
			}
			$_SESSION["groups"][$a] = $permissionsRow["groupId"];
			//$_SESSION["sp"] = $permissionsRow["sp"];
			$_SESSION["spListNameId"][$a] = $permissionsRow["sp"];
			$_SESSION['adminReportMessage'] = $securityDomainPattern != "" ? true : false;
			$a++;
		}
		/* If Admin does not have security domain */
		$_SESSION["spListNameId"] = array_unique($_SESSION["spListNameId"]);		
	}
	//Continue Previous Session
	if(isset($continue_session) && $continue_session && strlen($user->sp_selected)) {
		$_SESSION["continue_session"] = $continue_session;
		$_SESSION["sp"] = $user->sp_selected;
		if(strlen($user->groupid_selected)) {
			$_SESSION["groupId"] = $user->groupid_selected;
		}
                if($_SESSION["superUser"] == "1") {
			$_SESSION["selectedCluster"] = $user->cluster_name_selected;
                        // Code added @ 03 July 2019 Set default cluster if selected cluster is empty
                        if(trim($_SESSION["selectedCluster"]) == ""){
                            $configUtilObj = new ConfigUtil($db, "bwAppServer");
                            $_SESSION["selectedCluster"] = $configUtilObj->setDefaultClusterIfSeletedClusterNoExists($db, "bwAppServer");
                        }
                        //End code
		}
	}

	$headerLoginDetails = new GetLoginHeader($bwAuthLevel);
	$_SESSION['headerLoginDetailsOutput'] = $headerLoginDetails->loginHeader;
	ob_get_clean();

	//Add Admin Log Entry - Start
	require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
	$adminLogs = new adminLogs();
	$log = array(
		'adminUserID' => $_SESSION["adminId"],
		'eventType' => 'LOGIN',
		'adminUserName' => $_SESSION["loggedInUserName"],
		'adminName' => $_SESSION["loggedInUser"],
		'updatedBy' => $_SESSION["adminId"],
	);
	$adminLogs->addLogEntry($log);
	//Add Admin Log Entry - End
}

function getSecurityDomainSp($securityDomain, $client, $sessionid)
{
	$serviceProviders = array();

	try {
		// get list of Enterprises that have security domain assigned
		$xmlinput = xmlHeader($sessionid, "SystemDomainGetAssignedServiceProviderListRequest");
		$xmlinput .= "<domain>" . $securityDomain . "</domain>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$a = 0;
		foreach ($xml->command->serviceProviderTable->row as $key => $value) {
			$serviceProviders[$a] = strval($value->col[0]);
			$a++;
		}
	} catch (Exception $e) {
		error_log($e->getMessage());
	}

	return $serviceProviders;

}

/*get enterprise name and sp name with id *
 * Choose group select Sp and enterprises name and id
 *  created date 25-06-2018
 */

function getSecurityDomainSpNameId($securityDomain, $client, $sessionid)
{
    $spNameIdList = array();
    
    try {
        // get list of Enterprises that have security domain assigned
        $xmlinput = xmlHeader($sessionid, "SystemDomainGetAssignedServiceProviderListRequest");
        $xmlinput .= "<domain>" . $securityDomain . "</domain>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);       
        
        $a = 0;
        foreach ($xml->command->serviceProviderTable->row as $key => $value) { // ex-774 
            if(isset($value->col[1]) && !empty($value->col[1])){
                //$spNameIdList[$a] = strval($value->col[0].' - '.$value->col[1]);
               $spNameIdList[$a] = strval($value->col[0].'<span>-</span>'.$value->col[1]);
                
            }else{
                $spNameIdList[$a] = strval($value->col[0]);
            }
            $a++;
        }
    } catch (Exception $e) {
        error_log($e->getMessage());
    }
    
    return $spNameIdList;
    
}

/*end development */

function getAllSecurityDomainGroups($spList, $securityDomain, $client, $sessionid)
{

	$groups = array();        
	try {
		// get list of Groups that have security domain assigned
		for ($i = 0; $i < count($spList); $i++) {
			$xmlinput = xmlHeader($sessionid, "ServiceProviderDomainGetAssignedGroupListRequest");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($spList[$i]) . "</serviceProviderId>";
			$xmlinput .= "<domain>" . $securityDomain . "</domain>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);                         
			foreach ($xml->command->groupTable->row as $key => $value) {
				$groups[] = strval($value->col[0]);
			}
		}
	} catch (Exception $e) {
		error_log($e->getMessage());
	}
	return $groups;

}

function get_access_code($user_id)
{
	global $db;
	$return = 0;
	$stmt = $db->prepare('select access_code from users where userName = ?');
	$stmt->execute(array($user_id));
	$row = $stmt->fetch(PDO::FETCH_OBJ);
	if ($row) {
		$return = $row->access_code;
	}
	return $return;

}

function get_systemConfig()
{
	global $db;
	$stmt = $db->prepare('select * from systemConfig limit 1');
	$stmt->execute(array());
	$rows = $stmt->fetch(PDO::FETCH_OBJ);
	return $rows;
}

function get_user_info($user_id, $as_array = false)
{
	global $db;
	$return = 0;
	$stmt = $db->prepare('select *, Timestampdiff(DAY, lastPasswordChange, Now()) as dayDifference from users where id = ? limit 1');
	$stmt->execute(array($user_id));
	$rows = $stmt->fetch($as_array ? PDO::FETCH_ASSOC : PDO::FETCH_OBJ);
	return $rows;
}

function get_user_from_username($userName)
{
	global $db;
	$return = 0;
	$stmt = $db->prepare('select *, Timestampdiff(DAY, lastPasswordChange, Now()) as dayDifference from users where userName = ? limit 1');
	$stmt->execute(array($userName));
	$rows = $stmt->fetch(PDO::FETCH_OBJ);
	return $rows;
}

function get_user_from_token($temporary_token)
{
	global $db;
	$stmt = $db->prepare('select *, Timestampdiff(DAY, lastPasswordChange, Now()) as dayDifference from users where temporary_token = ? limit 1');
	if ($stmt->execute(array($temporary_token))) {
		if ($row = $stmt->fetch(PDO::FETCH_OBJ)) {
			return $row;
		}
	}
	return null;
}

function send_two_factor_sms($user, $smsSID, $smsToken, $smsFrom)
{

	global $db;

	require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/SMSEntity.php");

	$six_digit_random_number = mt_rand(100000, 999999);

	$update_access_code = $db->prepare('UPDATE users SET access_code = ? WHERE userName = ? LIMIT 1');
	$update_access_code->execute(array($six_digit_random_number, $user->userName));

	$phone_number = $user->user_cell_phone_number;

	// Send SMS
	$smsObj = new SMSEntity($smsSID, $smsToken, $smsFrom);

	return $smsObj->sendMessage($phone_number, "$six_digit_random_number is your Averistar Access code.");

}

function update_user_password($new_password, $userName)
{

	global $db;

	$password = md5($new_password);
	$update_password = $db->prepare('UPDATE users SET password = ?, temporary_token = "" where userName = ?');
	$update_password->execute(array($password, $userName));
        
        //Code added @ 13 July 2018
        $sqlUser = "select id, userName, firstName, lastName from users where userName='" . $userName . "' and password='" . $password . "' ";
        $resultUser = $db->query($sqlUser);
        if ($rs = $resultUser->fetch()) 
        {
            $userId = $rs["id"];
        }
        if(!empty($userId))
        {
            $sqlUpdatePassHistory = $db->prepare('INSERT into adminPasswordHistory (userId, password, modifiedOn) VALUES (?,?,?)');
            $currDate = date("Y-m-d h:i:s");
            $sqlUpdatePassHistory->execute(
                                              array(
                                                        $userId,
                                                        $password,
                                                        $currDate
                                                    )
                                          );
        }        
        //End code


	//Add Admin Log Entry - Start
	if($userId) {
		require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
		$adminLogs = new adminLogs();
		$log = array(
			'adminUserID' => $userId,
			'eventType' => 'PASSWORD_RESET',
			'adminUserName' => $rs["userName"],
			'adminName' => $rs["firstName"] . ' ' . $rs["lastName"],
			'updatedBy' => $userId,
		);
		$adminLogs->addLogEntry($log);
	}
	//Add Admin Log Entry - End

}

function set_group_selected_user()
{
	global $db;
        
        $clusterName = (isset($_SESSION["selectedCluster"]) && $_SESSION["selectedCluster"]) ? $_SESSION["selectedCluster"] : "";
	$sp = (isset($_SESSION["sp"]) && $_SESSION["sp"]) ? $_SESSION["sp"] : "";
	$groupId = (isset($_SESSION["groupId"]) && $_SESSION["groupId"]) ? $_SESSION["groupId"] : "";

	$spQuery = "UPDATE users set cluster_name_selected = ?, sp_selected = ?, groupid_selected = ? where id = ?";
	$spQueryStmt = $db->prepare($spQuery);
	$spQueryStmt->execute(array($clusterName, $sp, $groupId, $_SESSION["adminId"]));

}

function get_change_log_modules($groupId)
{
	global $db;
	$expressProvLogsDb = $GLOBALS['expressProvLogsDb'];
	$stmt = $expressProvLogsDb->prepare('SELECT distinct module FROM changeLog WHERE groupId = ? ORDER BY module');
	if ($stmt->execute(array($groupId))) {
		$rows = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $rows;
	}
	return array();
}

function get_change_log_modules_enterprise_changelog($enterpriseId)
{
	global $db;
        if($enterpriseId!=""){
            $expressProvLogsDb = $GLOBALS['expressProvLogsDb'];
            $stmt = $expressProvLogsDb->prepare('SELECT distinct module FROM changeLog WHERE enterpriseId = ? ORDER BY module');
            if ($stmt->execute(array($enterpriseId))) {
                    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
                    return $rows;
            }
            return array();
        }else{
            $expressProvLogsDb = $GLOBALS['expressProvLogsDb'];
            $stmt = $expressProvLogsDb->prepare('SELECT distinct module FROM changeLog ORDER BY module');
            if ($stmt->execute()) {
                    $rows = $stmt->fetchAll(PDO::FETCH_OBJ);
                    return $rows;
            }
            return array();
        }
}

function get_change_log_entityNames($groupId)
{
	global $db;
	$expressProvLogsDb = $GLOBALS['expressProvLogsDb'];
	$stmt = $expressProvLogsDb->prepare('SELECT distinct entityName FROM changeLog WHERE groupId = ? ORDER BY entityName');
	
	if ($stmt->execute(array($groupId))) {
		$rows = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $rows;
	}
	return array();
}

function get_all_admin_users()
{
	global $db;
	$stmt = $db->prepare("SELECT *, CONCAT(firstName, \" \", lastName) as full_name FROM users ORDER BY firstName, lastName");
	if ($stmt->execute()) {
		$rows = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $rows;
	}
	return array();
}

function get_change_logs($filters = null, $orderBy='')
{
	global $db;
	$query = 'SELECT changeLog.*, CONCAT(firstName, " ", lastName) as modified_by FROM expressProvLogs.changeLog LEFT JOIN adminPortal.users on users.userName = changeLog.userName WHERE 1=1 ';
	$query_values = array();

	if ($filters) {
		foreach ($filters as $filter) {
			$query .= " and $filter[0] $filter[1] ? ";
			$query_values[] = $filter[2];
		}
	}

        if($orderBy != ""){
            $query .= " ORDER BY date desc";
        }else{
             $query .= " ORDER BY date ";
        }
        
        
	$stmt = $db->prepare($query);
	if ($stmt->execute($query_values)) {
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $rows;
	}
	return array();
}

function send_email($to, $subject, $message, $from = NULL) {

	global $supportEmailSupport;

	if(!$from) {
		$from = "Support Web<" . $supportEmailSupport . ">";
	}

	$headers = "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
	$headers .=  "From: $from\r\n";

	mail($to, $subject, $message, $headers);

}

function checkPasswordStrength($passwordRules, $pwd, &$errors) {

	$passwordRulesMinimumLength = $passwordRules->getMinimumLength();
	$passwordRulesNumberOfDigits = $passwordRules->getNumberOfDigits();
	$passwordRulesSpecialCharacters = $passwordRules->getSpecialCharacters();
	$passwordRulesUpperCaseLetter = $passwordRules->getUpperCaseLetter();
	$passwordRulesLowerCaseLetter = $passwordRules->getLowerCaseLetter();

	$errors_init = $errors;

	if (strlen($pwd) < $passwordRulesMinimumLength) {
		$errors[] = "Password must be at least $passwordRulesMinimumLength characters long!";
	}

	if (preg_match_all( "/[0-9]/", $pwd) < $passwordRulesNumberOfDigits) {
		$errors[] = "Password must include at least $passwordRulesNumberOfDigits digits!";
	}

	if($passwordRulesUpperCaseLetter == "true") {
		if (!preg_match("#[A-Z]+#", $pwd)) {
			$errors[] = "Password must include at least one Upper case letter!";
		}
	}

	if($passwordRulesLowerCaseLetter == "true") {
		if (!preg_match("#[a-z]+#", $pwd)) {
			$errors[] = "Password must include at least one Lower case letter!";
		}
	}

	if($passwordRulesSpecialCharacters == "true") {
		if (!preg_match('/[^a-zA-Z0-9]+/', $pwd)) {
			$errors[] = "Password must include at least one special character!";
		}
	}

	//error_log(print_r($errors, true));

	return ($errors == $errors_init);
}

// encrypts a string
function encryptString($string, $key, $method = 'AES-256-CBC')
{
	$ivSize = openssl_cipher_iv_length($method);
	$iv = openssl_random_pseudo_bytes($ivSize);

	$encrypted = openssl_encrypt($string, $method, $key, OPENSSL_RAW_DATA, $iv);

	// For storage/transmission, we simply concatenate the IV and cipher text
	$encrypted = base64_encode($iv . $encrypted);

	return $encrypted;
}

// decrypt the string
function decryptString($string, $key, $method = 'AES-256-CBC')
{
	$string = base64_decode($string);
	$ivSize = openssl_cipher_iv_length($method);
	$iv = substr($string, 0, $ivSize);
	$string = openssl_decrypt(substr($string, $ivSize), $method, $key, OPENSSL_RAW_DATA, $iv);

	return $string;
}

function get_all_super_admin_users()
{
	global $db;
	$stmt = $db->prepare("SELECT *, CONCAT(firstName, \" \", lastName) as full_name FROM users WHERE superUser = 2 ORDER BY firstName, lastName");
	if ($stmt->execute()) {
		$rows = $stmt->fetchAll(PDO::FETCH_OBJ);
		return $rows;
	}
	return array();
}

function decryptPasswordTool($encryptedPassword, $passwordSalt, $type, $username = "") {

	$key = "";

	switch ($type) {
		case "bwAppServer" :
		case "surgeMail" :
			$key = $username;
			break;
		case "staticBwPortalPassword" :
			$key = "StaticBwPortalPassword";
			break;
		case "surgeMailUserPassword" :
			$key = "SurgeMailPassword";
			break;
		case "billingPassword" :
			$key = "BillingPassword";
			break;
		/*case "sshPassword" :
			$key = "SSHPassword";
			break;*/

		default:
			break;

	}

	$string = decryptString($encryptedPassword, $key . "_" . $passwordSalt);

	return $string;
}

//code added @ 19 Feb 2019
function getGroupListBasedOnServiceProvider($sp, $sessionid, $client, $groupList)
{        
         try {
		// get list of Enterprises that have security domain assigned
		$xmlinput = xmlHeader($sessionid, "GroupGetListInServiceProviderRequest");
                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($sp) . "</serviceProviderId>";
                $xmlinput .= xmlFooter();
                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);		                
		foreach ($xml->command->groupTable->row as $key => $value) {
			$groupList[] = strval($value->col[0]);
		}
            } catch (Exception $e) {
                    error_log($e->getMessage());
            }
            
	return $groupList;
}
//End code
?>