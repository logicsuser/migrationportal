<?php
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

/*get serviceProvider list  */
require_once("/var/www/lib/broadsoft/adminPortal/getServiceProviders.php");
require_once("/var/www/lib/broadsoft/adminPortal/util/CommanUtil.php");
$objUtil = new CommanUtil();
/*get all system level sp list */
$enterpriseId = $_SESSION['sp'];
$getEnterSPLISTExpressLicense = $objUtil->getsystemLevelEnterPriseList($enterpriseId, $getSPName);

?>

 <script src="serviceLicense/js/serviceLicense.js"></script>

<div class="adminUserText" id="sitesubBanner">
    <?php 
        $siteSbubBannerMsg = "";
        if((isset($license["BroadWorksLicenseReport"]) && $license["BroadWorksLicenseReport"] == "true")){
            $siteSbubBannerMsg = "BroadWorks License Report";
        }
        if((isset($license["customProfile"]) && $license["customProfile"] == "true") && checkCustomProfiles()){
            $siteSbubBannerMsg = "Phone Profiles";
        }
        if((isset($license["BroadWorksLicenseReport"]) && $license["BroadWorksLicenseReport"] == "true") && (isset($license["customProfile"]) && $license["customProfile"] == "true") && checkCustomProfiles()){
            $siteSbubBannerMsg = "BroadWorks License Report";
        }
            echo $siteSbubBannerMsg;
    ?>
</div>
 
 
 <style>     
#loadingModule
{
    text-align: center;
    margin: 20px;
    font-size: 15px;
    width: 100%;
}
#addUserProgressBarDiv {
    width: 90%;
    margin: 5%;
}
/* css for express license  */
element.style {
	margin-top: 25px;
}
.GroupSummaryLiceseTh{
width:11% !important ;
}
.buttonAlignment{
    height: 200px;
	margin-left: auto;
	margin-right: auto;
	width: 1065px;
}

#enterPriseUserServiceDetailedDiv table tbody tr td{background-color: #DDDFE1 !important;}

.entPurchageProduct thead tr::after,.productQuantityServiceTbl thead tr::after{
    content: none !important;
    overflow-y: initial !important;
    visibility: hidden !important;
    height: 0;
}
 
table.productQuantityServiceTbl.samurySystemUserTable.divUserServiceGroupSummaryLicese.tblEnterpriseUserServiceLicense tbody tr {
    background-color: #dddfe1;
}
table.samurySystemUserTable tbody tr td{
    /* border: 1px solid #999999 !important; */
}
.productQuantityServiceTbl thead tr{border-right:none;}
.entPurchageProduct thead tr th, .entPurchageProduct tbody tr td{
   height: auto !important; 
   text-align:left;
   padding:8px ;
   word-break: break-word;
}

.entPurchageProduct tbody{
    overflow-y: hidden !important;
    border-right: none;
    border-bottom: none;
    max-height: 100% !important;
    margin-bottom: -15px;
}

 table.productQuantityServiceTbl tbody tr td {
    text-align: left;
}


.productNameWidth{width: 22% !important; text-align: left !important ;}
.productTypesWidth{width: 11% !important;}
.productPurchageWidth{width: 10% !important;}
.productConsumedWidth{width: 10% !important;}
.productAvailableWidth{width: 8% !important;}
.productRemainingWidth{width: 11% !important;}
.productServicePackWidth{width: 16% !important;}
.productQualityWidth{width: 9% !important;}
.productQuantityServiceTbl tbody tr {
    border-right: none;
    border-left: none;
    
}
#productQualityWidthTableTd{    
    width: 25% !important;
    padding-right: 0px !important;
    padding-left: 0px !important;
    text-align: left !important;
	
	}
        
.entPurchageProduct tbody tr td:nth-last-child(1) , .entPurchageProduct thead tr th:nth-last-child(1), .samurySystemUserTable tbody tr td:nth-last-child(1){
    border-right: none;
}


table tbody tr td:first-child,table thead tr th:first-child{
    padding-left:15px !important;
    
}
/*
.entPurchageProduct tbody tr.puchagesWarkingRow td{
    background-color:#F3C6CA;
}

.entPurchageProduct tbody tr.puchagesSuccessRow td{
    background-color:#F6EBB1;
} */

 
 
.colorTagWarning{    
    background-color: #F3C6CA !important;
}

.servicePackAlign li.ui-state-default.ui-corner-top{
    
    background-color: #F3C6CA !important;
}

div#enterPriseUserServiceDiv table tbody tr td {
    background-color: #DDDFE1;
}

#divUserServiceGroupSummaryLicese table tbody tr td , #enterPriseUserServiceDetailedDiv table tbody tr td{
    background-color: #DDDFE1;
 }



.adminUserMhover #summaryImg1,.adminUserMhover #EntsummaryImg1
{
    width: 137px;
    height: 170px;
    padding-top: 150px;
    text-align: center;
    background-image: url('/Express/images/NewIcon/summary_usage_rest.png');  
    background-repeat: no-repeat; 
    font-size: 12px;
    font-family: Open Sans; 
    color: #6ea0dc; 
    font-weight: bold;
}

.adminUserMhover #summaryImg1:hover,.adminUserMhover #EntsummaryImg1:hover  
{
    width: 137px;
    height: 170px;
    padding-top: 150px;
    background-image: url('/Express/images/NewIcon/summary_usage_over.png') ! important;
    background-repeat: no-repeat;
    color: #ffb200 ! important; 
}                                
                                
                                
                                
 .adminUserMhover #detailedImg2:hover, .adminUserMhover #EntdetailedImg2:hover {
    width: 137px;
    height: 170px;
    padding-top: 150px;
    background-repeat: no-repeat;
    color: #ffb200 ! important;
}

.adminUserMhover #detailedImg2:hover {
     background-image: url(/Express/images/NewIcon/report_alert_over.png) ! important;
}

.adminUserMhover #EntdetailedImg2:hover {
     background-image: url(/Express/images/NewIcon/ent_detailed_usage_over.png) ! important;
}

.adminUserMhover #detailedImg2 , .adminUserMhover #EntdetailedImg2 {
    width: 137px;
    height: 170px;
    padding-top: 150px;
    text-align: center;
    background-repeat: no-repeat;
    font-size: 12px;
    font-family: Open Sans;
    color: #6ea0dc;
    font-weight: bold;
 
}

.adminUserMhover #detailedImg2{
    background-image: url(/Express/images/NewIcon/report_alert_rest.png);
} 

.adminUserMhover #EntdetailedImg2{
    background-image: url(/Express/images/NewIcon/ent_detailed_usage_rest.png);
} 

#main_ServiceLicense {
    width: auto;
}

#summaryImg1,#detailedImg2,#EntsummaryImg1,#EntdetailedImg2{cursor:pointer ;}
                             
  .entPurchageProduct tbody tr td:first-child , .entPurchageProduct thead tr th:first-child{
    padding-left:15px !important ;
} 

#phoneProfilesTabdddd{ background-color: #F6EBB1; color: #333333;}
#serviceLicensesddd{color: #333333;}

.enterpriseSubscribe thead tr th,.enterpriseSubscribe tbody tr td{text-align: left;padding-left:15px !important;}

.serviceDetailedAlign ul{
    width: 25%;
    margin: auto !important;
    margin-top:80px !important;
    margin-bottom:25px !important;
}
.servicePackAlign ul{ width: 52%;
    margin: auto !important;
    margin-top:80px !important;
    margin-bottom:25px !important;
	border: none !important;
}

table.tablesorter tbody td {
	background-color: transparent !important;
}

 
/*
table.tablesorter tbody td {
    border-right: 1px solid #333333;
}

table.tablesorter tbody tr {
    border-right: 1px solid #333333;
}

.scroll tr {
   border: 1px solid #333;
 } */
/*  end express license css */

.trunctingServiceTh{width:26% !important;}
 
 .productQuantityServiceTbl{width: 100% !important; margin-top: -10px !important;margin-left: -3px;}
 div#divPurchaseProduct table tbody tr td table.productQuantityServiceTbl {
    margin-left: 0px !important;
}

table.productQuantityServiceTbl thead tr{border-right: none;} 
 
 /* table.divUserServiceGroupSummaryLicese tbody tr td{border:1px solid #999 !important ;} */
 table.enterPriseUserServiceDetailedDiv tbody tr td{background-color: #cccccc;}
 
 .divUserServiceGroupSummaryLicese tbody tr td:first-child, .divUserSerGroupSumLicInternalTable tbody tr td:first-child{border-left: none !important;}
 .divUserSerGroupSumLicInternalTable tbody tr td:last-child(1){border-right:1px solid #999 !important;}
 
 
/*#divUserServiceGroupSummaryLicese table.samurySystemUserTable tr td {border:1px solid #999 !important ;} */

 </style>
 
 
<div id="tabs">
    <div class="divBeforeUl">
	<ul>
            <?php 
                $broadWorksLicenseReportLogsArr       = explode("_", $_SESSION["permissions"]["broadWorksLicenseReport"]);
                $broadWorksLicenseReportLogPermission = $broadWorksLicenseReportLogsArr[0];

                $phoneProfilesLogsArr                 = explode("_", $_SESSION["permissions"]["phoneProfiles"]);
                $phoneProfilesLogPermission           = $phoneProfilesLogsArr[0];

               if((isset($license["BroadWorksLicenseReport"]) && $license["BroadWorksLicenseReport"] == "true") && $broadWorksLicenseReportLogPermission == "1"){?>
                    <li><a href="#main_ServiceLicense" id="serviceLicenses" class="tabs" onclick="tabClicked('BroadWorks License Report')">BroadWorks License Report</a></li>
                <?php }?>

                <?php if((isset($license["customProfile"]) && $license["customProfile"] == "true" && $phoneProfilesLogPermission == "1") && checkCustomProfiles()){?>
                    <li><a href="#phone_ProfilesTab" id="phoneProfilesTab" class="tabs" onclick="tabClicked('Phone Profiles')">Phone Profiles</a></li>
                <?php }?>
	</ul>

    </div>

    <!--
    <div class="row" style="width:830px;margin: 0 auto;">
        <div class="from-group" style="margin-top:25px;">
            <label class="labelText" for="">Date Collected:2018.07.30 at 18:31:21</label>
        </div>  
    </div>
    -->

   <div id="main_ServiceLicense" class="ui-tabs-panel ui-widget-content ui-corner-bottom userDataClass" style="display:none;" aria-labelledby="device_search_button" role="tabpanel" aria-expanded="true" aria-hidden="false">
        <div class="" style="height: 200px;">
            <form name="" id="searchDeviceOption" method="POST" class="fcorn-register">
                <?php 
                    $cssStyle = "style='display: block;'";
                    if($_SESSION["superUser"] == "3"){
                            $cssStyle = "style='display: none;'";
                    } 
                ?>
                <div class="row" <?php echo $cssStyle; ?> >
                    <div class="col-md-12 selectInputRadio form-group" style="">
                            <input type="radio" class="searchlevel" name="serviceLiecenVal" id="serviceLiecenVal" value="system"  style=""><label for="serviceLiecenVal"><span></span></label> 
                            <label class="labelText" for="deviceClassVoIP">System</label>

                            <input type="radio" class="searchlevel" name="serviceLiecenVal" id="serviceLiecenValEnterprise" value="enterprise"  style=""><label style="margin-left: 25px;" for="serviceLiecenValEnterprise"><span></span></label> 
                            <label class="labelText" for="deviceClassAnalogGateway">Enterprise</label>	
                    </div>
                </div>
                
                <div class="row" id="enterpriseList">   
                        <div class="col-md-12">
                            <?php 
                                /* if want future implementation */
                                 /*  $cssStyle = "style='display: block;'";
                                    if($checkedRadio == 'system')   {
                                        $cssStyle = "style='display: none;'"; 
                                }  */
                            ?>
                            <div class="form-group">
                                <label class="labelText" for="limitSearchTo">Select Enterprise:</label><br>
                                <div class="dropdown-wrap"> 
                                   <select class="form-control blue-dropdown" name="entExpressLicenseSpSelect" id="entExpressLicenseSpSelect" style="width:100% !important">
                                       <?php echo $getEnterSPLISTExpressLicense; ?>
                                   </select>
                                   <input type="hidden" name="enterpriseId" id="hiddenEnterpriseId" value="<?php echo $_SESSION["sp"]?>" />
                                </div>
                            </div>
                        </div>
                </div>
                
                <div class="row"> 
                <div class="progress" id="addUserProgressBarDiv">
			<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0;" id="addUser_progress_bar">0%</div>
                </div>
                <div id="loadingModule"></div>
                <div id="buildDev"></div>
                </div>
                <!--enperprise ui-->
                
                <div id="BWLicenseReportDiv">
                </div>
                 
              
                <div id ="sytemLevelBroadworkDiv"></div>  
                 <!-- <div id="enterPriseLevelDiv">&nbsp;</div> 
                -->
                
            </form>
            <div class="loading" id="loading2" style="display: none;">
                    <img src="/Express/images/ajax-loader.gif">
            </div>
        </div>
    </div>
    <!-- phone_profiles -->
    <div id="phone_ProfilesTab" class="userDataClass" style="display: none;">
        <?php 
        require_once ("/var/www/html/Express/phoneProfiles/phoneProfiles.php");
        ?>
    </div>
</div>

<div id="sas_dialog" class="dialogClass">
	<div id="sas_loader"> <img src="/Express/images/ajax-loader.gif"> </div>
	<div id="errorDiv"></div>
	 
</div>
 

 