var status = "";
var steps = 0;
var  width = 0
var totalSteps ;
var entpriseName ;
var resultAjaxComplete="" ;
var resultAjaxCompleteSystem = "";
var checkLevel="" ;
var downLoadCsvSp="";
$(function() {
    $("#mainPurchageProductDetailedDiv").hide();    
    var sourceSwapImage = function() 
    {
            var thisId = this.id;
            var image = $("#"+thisId).children('img');
            var newSource = image.data('alt-src');
            image.data('alt-src', image.attr('src'));
            image.attr('src', newSource);
    }

    $("#csvUrlForSummaryUsageDiv").hover(sourceSwapImage, sourceSwapImage);
    
    var $radios = $('input:radio[name=serviceLiecenVal]');
    if($radios.is(':checked') === false) 
    {
        $radios.filter('[value=enterprise]').prop('checked', true);
        $("#enterpriseList").show();
    }
    
  /*  var extractFileProgress = 10;
    var licenseFileProgress = 5;
    var ctrlFileProgress = 5;
    var groupIdExtractFileProgress = 70;

    var steps = 0;
    var width = 0; */
    
    
    
    entpriseName   = $("#entExpressLicenseSpSelect").val();
    downLoadCsvSp  = entpriseName;
    
   //
   //  runProgressBarWhileCreatingBWLicenseReport(entpriseName,status);
   // getServicePackUsageTable();
   $("#enterpriseList").hide(); 
   // var entpriseName = "";
  //  var sysstatus = "";
    $("#BWLicenseReportDiv").hide();
    $("#addUserProgressBarDiv").show();
    $("#loadingModule").show();
   // alert("dsdss") ;
   checkLevel ="enterprise" ;
    $("#serviceLiecenVal + label span").css("opacity","0.4");
   checkNoOfOperationsProgressBar(entpriseName);
    
});

 $(function(){
	$("#tabs").tabs();
	$("#loading2").hide();        
        //During the loading of BW License Report Enterprise, system context and Enterprise List will be hide 
        $("#enterpriseList").show();
        $("#BWLicenseReportDiv").hide();
        //End code        
        
	$("#sas_dialog").dialog(
	{
		autoOpen: false,
		width: 800,
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		open: function(event) {
			setDialogDayNightMode($(this));
			$('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('cancelButton');
		},
		buttons: {
	 		"Close": function() {
	 			
	 			$(this).dialog("close");
			}
		}
	});

	$("#serviceLicenseSystem").click(function(){
	 	var formData = $('form#searchDeviceOption').serializeArray();
		$("#errorDiv").html("");
		$("#sas_loader").show();
		$("#tagFormAndMsg").html("");
		$("#tagFormAndMsg").show();
		if (formData !=""){
			var formData = $('form#searchDeviceOption').serializeArray(); 
			$("#sas_dialog").html('');
			 $.ajax({
				type: "POST",
				url: "serviceLicense/getServiceLicense.php",
				data:formData,
				dataType: "html",
				cache: false,
				ajaxOptions: { cache: false },
				success: function(result){
					$.removeData($("#sas_dialog").html(result));
					if(result.Error && result.Error != "")
					{
						//alert('error');
						$("#errorDiv").html(result.Error);
						$('.ui-dialog-buttonpane button:contains("Close")').button().show();
					}
					else
					{  
						$("#sas_dialog").dialog("open");
						$("#sas_dialog").html(result);
						$('.ui-dialog-buttonpane button:contains("Close")').button().show();
					}
				}
			});
		}
		else{
			$('.ui-dialog-buttonpane button:contains("Close")').button().show();
		}

	});
	$("#tabs").tabs();
});


function tabClicked(title){
	$("#sitesubBanner").html(title);
};

function tabClicked(title){
	$("#sitesubBanner").html(title);
};


$("input[name='serviceLiecenVal']").click(function() 
{          
   if(resultAjaxComplete =="complete"){
    
        if($(this).val() =="system"){
            
          $("#serviceLiecenValEnterprise + label span").css("opacity","0.4");
          $("#serviceLiecenVal + label span").css("opacity","1");
            selectBroadworkUsage('summaryUsage');
            entpriseName = "";
            if(resultAjaxCompleteSystem =="complete"){
                //$("#BWLicenseReportDiv").show();
                 $("#serviceLiecenVal + label span").css("opacity","1");
                 $("#serviceLiecenValEnterprise + label span").css("opacity","1");
                 $("#BWLicenseReportDiv").hide();
                $("#sytemLevelBroadworkDiv").show();
                $("#mainPurchageProductDiv").hide();   
                $("#addUserProgressBarDiv").hide();
                $("#loadingModule").hide();
                $("#enterpriseList").hide();
            }else{
                $("#enterpriseList").hide(); 
                $("#BWLicenseReportDiv").hide();
                $("#addUserProgressBarDiv").show();
                $("#loadingModule").show();
                $("#serviceLiecenVal + label span").css("opacity","1");
                 checkNoOfOperationsProgressBar(entpriseName);
            }
    }
     if($(this).val() =="enterprise"){
     // resultAjaxComplete ="";
        if(resultAjaxCompleteSystem =="complete"){ 
            $("#serviceLiecenValEnterprise + label span").css("opacity","1");
            selectBroadworkUsage('entLevelDetailedUsage');
            checkLevel ="enterprise" ;
            $("#loadingModule").hide();
            $("#enterpriseList").show();
            $("#BWLicenseReportDiv").show();
            $("#sytemLevelBroadworkDiv").hide();
            $("#mainPurchageProductDiv").show();
        }else{
            $("#serviceLiecenValEnterprise + label span").css("opacity","0.4");
                return false ;
        }
    }
     /*   var extractFileProgress = 10;
    var licenseFileProgress = 5;
    var ctrlFileProgress = 5;
    var groupIdExtractFileProgress = 70;

    var steps = 0;
    var width = 0;
    */
 //   $("#enterpriseList").hide(); 
   // var entpriseName = "";
  //  var sysstatus = "";
   // $("#BWLicenseReportDiv").hide();
   // $("#addUserProgressBarDiv").show();
   // $("#loadingModule").show();
   // alert("dsdss") ;
    
    //runProgressBarWhileCreatingBWLicenseReport(extractFileProgress, licenseFileProgress, ctrlFileProgress, groupIdExtractFileProgress, steps, width, entpriseName, sysstatus);
   // alert('Change System - 12345 '+entpriseName);   
 }else{
   //  $("#serviceLiecenVal + label span").css("opacity","0.4");
     return false ;
    
 }
});

$("#entExpressLicenseSpSelect").change(function() 
{ 
    $("#serviceLiecenVal + label span").css("opacity","1");
    entpriseName   = $("#entExpressLicenseSpSelect").val();  
    downLoadCsvSp  = entpriseName;
	//var downLoadCsvSp =  $("#hiddenEnterpriseId").val(entpriseName) ;
 
    //$("#csvUrlForSummaryUsageDiv img").attr("onclick","fnExcelReportDup('divDetailedUsageAlertDiv','"+entpriseName+"','System_Detailed');");
    
    if(resultAjaxComplete =="complete"){
         //   resultAjaxComplete ="";
         //   entpriseName = "";
         checkLevel ="newSpSelected" ;
            //progressBar(status="");
           // width = 0;
            //$("#addUser_progress_bar").css('width', width + "%");
            //$("#addUser_progress_bar").html(width + "%"); 
            $("#BWLicenseReportDiv").hide();
            $("#addUserProgressBarDiv").show();
            $("#loadingModule").show();
          //  alert("yes");
            checkNoOfOperationsProgressBar(entpriseName);
        }else{
            return false;
        }
    
});


/* experss licesnse for broadwork report
 * code add at 28 march 2018
 * used function for summary and detailed hover image usage
 * system and enterprise context
 */
 $("#mainDetailedDiv").hide();
  //var downLoadCsvSp =  $("#hiddenEnterpriseId").val() ; //get service provider id for dynamic download csv button 
  

 //get service provider id for dynamic download csv button 
  function selectBroadworkUsage(selecter) {
      
      
        if(selecter === 'summaryUsage')
                {
                        $("#divDetailedUsage").show();
                        $("#mainPurchageProductDiv").hide();
                        $("#divDetailedUsageAlertDiv").hide();
                        $("#summaryImg1").css('background-image','url(images/NewIcon/summary_usage_over.png)');
                        $('#summaryImg1').css('color','#ffb200');
                        
                        $('#detailedImg2').css('background-image','url(images/NewIcon/report_alert_rest.png)');
                        $('#detailedImg2').css('color','#6ea0dc');
                        
                        $("#SystemlevelPurchageProductDiv").show();
                        $("#mainDetailedDiv").hide();
                        //$("#csvUrlForSummaryUsageDiv img").attr("onclick","fnExcelReportDup('divSystemLevelSumaryDetailedCSV','System','System_Summary');");
		}
		else if(selecter === 'detailedUsage')
                {
 
                    $("#summaryImg1").css('background-image','url(images/NewIcon/summary_usage_rest.png)');
                    $('#summaryImg1').css('color','#6ea0dc');

                    $('#detailedImg2').css('background-image','url(images/NewIcon/report_alert_over.png)');
                    $('#detailedImg2').css('color','#ffb200');
                       
        
         
                        $("#divDetailedUsage").hide();
                        $("#mainPurchageProductDiv").hide();
                        $("#divDetailedUsageAlertDiv").show();
                        //$("#csvUrlForSummaryUsageDiv img").attr("onclick","fnExcelReportDup('divDetailedUsageAlertDiv','System','System_Detailed');");
                       
		 }
                else if(selecter === 'enterPriseLevelsummaryUsage')
                {         
                        $("#EntsummaryImg1").css('background-image','url(images/NewIcon/summary_usage_over.png)');
                        $('#EntsummaryImg1').css('color','#ffb200');
                        
                        $('#EntdetailedImg2').css('background-image','url(images/NewIcon/ent_detailed_usage_rest.png)');
                        $('#EntdetailedImg2').css('color','#6ea0dc');
                         
                        $("#enterprieseContextSummaryUsage").show();
                        $("#mainPurchageProductDetailedDiv").hide();
                        /*dynamic download csv code detailed usages*/
                        //$("#csvUrlForSummaryUsageDiv img").attr("onclick","fnExcelReportDup('enterprieseContextSummaryUsage','"+window.downLoadCsvSp+"','Summary_Enterprise');");
                        
                        $("#mainDetailedDiv").hide();
						
						  /* In Detailed Usage, keep all folders collapsed, except User Services Licenses */
                        $("#headEnterPriseUserServiceDetailed").removeClass("collapsed");
                        $("#enterPriseUserServiceDetailedDiv").addClass("in");

                        $("#headEnterPriseSubscribeDetailed,#headEnterPriseGroupDetailed,#headEntVirSerDetail").addClass("collapsed");
                        $("#enterPriseSubscribeDetailedDiv,#enterPriseGroupDetailedDiv,enterPriseVirtualServiceDetailedDiv,#enterPriseVirtualServiceDetailedDiv").removeClass("in");
		}
		else if(selecter === 'entLevelDetailedUsage')
                {                      
                        
                    $("#enterprieseContextSummaryUsage").hide();
                    $("#mainPurchageProductDetailedDiv").show();
 
                    $("#EntsummaryImg1").css('background-image','url(images/NewIcon/summary_usage_rest.png)');
                    $('#EntsummaryImg1').css('color','#6ea0dc');

                    $('#EntdetailedImg2').css('background-image','url(images/NewIcon/ent_detailed_usage_over.png)');
                    $('#EntdetailedImg2').css('color','#ffb200');
                    
                   /*dynamic download csv code detailed usages*/
                   // $("#csvUrlForSummaryUsageDiv img").attr("onclick","location.href='"+downLoadCsvSp+"_Detailed_report.php'; ");
                   //$("#csvUrlForSummaryUsageDiv img").attr("onclick","fnExcelReportDup('mainPurchageProductDetailedDiv','"+window.downLoadCsvSp+"','Detailed_Enterprise');");
     }

		 

    }
    
    
    
            function checkNoOfOperationsProgressBar(entpriseName) 
            {
               // $("#serviceLiecenVal + label span").css("opacity","0.4");
                
                $("#addUser_progress_bar").css('width', width + "%");
                $("#addUser_progress_bar").html(width + "%");
                var totalTasks = ["Extract File","Extract License File","Extract Ctrl File","Extract GroupId Extract File"];  
                totalSteps = totalTasks.length;
                   progressBar(status="");
                   //if(totalSteps != 0 && entpriseName !=""){
                    if(totalSteps != 0){
                        getServicePackUsageTable() ;
                    }
                   /*if(totalSteps != 0 && entpriseName ==""){
                        getCountGroupIdData(entpriseName);
                    }*/
            }
   
              
		function progressBar(status) {
			//alert("Successsss ") ;
                    
                    steps += 1;
            var id = setInterval(frame, 100);
            function frame() {
    			var status_percentage = steps * (100/totalSteps);
    			if(status == 100) {
    				status_percentage = 100;
    			} else if(status_percentage > 98 ) {
    				status_percentage = 98;
    			}
    			status_percentage = parseInt(status_percentage);
    			
                if (width >= 100) {
                    width = 0;
                    steps = 0;
                    status_percentage = 0;
                  clearInterval(id);
                } else {
                    if(width < status_percentage ) {
                        width++; 
                    	$("#addUser_progress_bar").css('width', width + "%");
                        $("#addUser_progress_bar").html(width + "%");
                     } else {
                    	 clearInterval(id);
                     }
                }
              }
		}

    function getServicePackUsageTable()
    {    
         $("#loadingModule").html("Extract License Directory ...");
            $.ajax({
                    type: "POST",
                    url:"serviceLicense/proccessXmlLicenseFileData.php",
                    data:{"funcAction":"fileExtractMethod"},
                    success: function(result) 
                    {  
                       if(result =="done"){
                            progressBar(status="");
                             readLicenseXMLFileInBWExpressLicense();
                       }
                         
                    }
                    });
    };
   
//getServicePackUsageTable() ;
/*xml lic reader method */
function readLicenseXMLFileInBWExpressLicense()
{   
    $("#loadingModule").html("Extract XML License File ...");
    $.ajax({
          type: "POST",
          url:"serviceLicense/proccessXmlLicenseFileData.php",
          data:{"funcAction":"LicenseXMLFile"},
          success: function(result) {
            if(result =="done"){
               progressBar(status="");
                readCtrlXMLFileInBWExpressLicense();
           }
        }
    });
};

function readCtrlXMLFileInBWExpressLicense()
{ 
    $("#loadingModule").html("Extract Ctrl XML License File ...");
    $.ajax({
          type: "POST",
          url:"serviceLicense/proccessXmlLicenseFileData.php",
          data:{"funcAction":"readCtrlXMLFile"},
          success: function(result) {
            console.log(result);
            if(result =="done"){
                progressBar(status="");
                $("#loadingModule").html("Generating BroadWorks License Report...");
                getCountGroupIdData(entpriseName);
            }
        }
    });
};

function getCountGroupIdData(entpriseName)
{
resultAjaxComplete ="inProccess";
	//entpriseName = ""; 
   // alert('entpriseName - '+entpriseName);
    $.ajax({

          type: "POST",
          url:"serviceLicense/proccessXmlLicenseFileData.php",
          data:{"funcAction":"groupIdXML", "entName": entpriseName},
          success: function(result) {
             progressBar(status = 100);
               var steps = 0;
                var width = 0;
               resultAjaxComplete = "complete" ;
                //console.log(result);      
                
               $("#serviceLiecenVal + label span").css("opacity","1");
                $("#serviceLiecenValEnterprise + label span").css("opacity","1");
              //alert(entpriseName+"entpriseName");
              if(entpriseName ==""){
                resultAjaxCompleteSystem ="complete"; 
                $("#BWLicenseReportDiv").hide();
                $("#sytemLevelBroadworkDiv").show();
                $("#sytemLevelBroadworkDiv").html(result);
                    
                    
               }

             //  checkLevel ="enterprise"
             if((entpriseName !="" &&( checkLevel =="enterprise" ||  checkLevel =="newSpSelected" ))){
                  $("#BWLicenseReportDiv").html(result);  
                  $("#BWLicenseReportDiv").show();
                    $("#sytemLevelBroadworkDiv").hide();
              }
                $("#addUserProgressBarDiv").hide();
                    //alert("groupIdXML");
                    $("#loadingModule").hide(); 
            
          }
    });
};
/*
function runProgressBarWhileCreatingBWLicenseReport(entpriseName,status)
{ 
     
    alert(status+'ddd') ;
        checkNoOfOperationsProgressBar();
      //  progressBar(status);
   // if (extractFileProgress.length != 0){
            getServicePackUsageTable();
  //  }
   // if (licenseFileProgress.length != 0){ 
          //  readLicenseXMLFileInBWExpressLicense();
   // }
   // if (ctrlFileProgress.length != 0){ 
            readCtrlXMLFileInBWExpressLicense();
   // }
   // if (groupIdExtractFileProgress.length != 0){ 
            $("#loadingModule").html("Generating BroadWorks License Report...");
            getCountGroupIdData(entpriseName);
   // }
} */