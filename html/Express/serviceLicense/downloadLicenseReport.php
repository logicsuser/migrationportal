<?php
	require_once("/var/www/lib/broadsoft/login.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
	checkLogin();
        
        $message = "";
        
        if(isset($_GET['context']) && $_GET['context'] == "system")
        {
                $level       = $_GET['context'];
                $type        = "SummaryUsage";
                $reportType  = $_GET['report'];
                $fileName    = $level."_".$type."_".$reportType;
                
                if($reportType == "SummaryUsage"){
                    $message     = $_SESSION['csvSystemSummaryUsageData'];
                }
                if($reportType == "AlertReport"){
                    $message     = $_SESSION['csvSystemAlertReportData'];
                }
        }
        
        if(isset($_GET['context']) && $_GET['context'] == "enterprise")
        {
                if(isset($_GET['enterprise']) && $_GET['enterprise'] != ""){
                    $enterPriseName = $_GET['enterprise'];
                }                
                $reportType  = $_GET['report'];                
                $fileName = $enterPriseName."_".$reportType."_Enterprise";
                
                if($reportType == "Summary"){
                    $message     = $_SESSION['csvSummaryUsageData'];
                }
                if($reportType == "Detailed"){
                    $message     = $_SESSION['csvDetailUsageData'];
                }
        }
        
        
        header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment;filename=\"" . $fileName . ".csv\"");
	header("Content-Transfer-Encoding: binary");
        
	
        $fp = fopen("php://output", "a");
	fwrite($fp, $message);
	fclose($fp);
?>
