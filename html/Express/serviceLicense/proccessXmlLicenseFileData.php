<?php
    require_once("/var/www/html/Express/config.php");
    require_once("/var/www/lib/broadsoft/login.php");
    //echo phpinfo();
    //die();
    checkLogin();
    
    require_once("/var/www/lib/broadsoft/adminPortal/util/CommanUtil.php");
    
    require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
    
    //$clusterNameForBWLicnese;
    //$ipAddressForBWLicnese;
    
    $serviceObj             = new Services();
    $subscriberLicenseArr   = $serviceObj->getSystemSubscriberServiceLicenseList();
    
    $context = "";
    //$_SESSION['licenseZipFileNameWithPath'] = "";
    
    //Code added @ 10 May 2019
    function setServicePackRowColorBasedOnConsumedQunantity($productQty, $consumedLicensed, $warningThreshold, $attentionThreshold)
    {
        //In Progress
        $defaultRowStyle = "style='background-color: #eef0f3;'";
        if($productQty == "0" || strtolower($productQty) == "unlimited"){
           $defaultRowStyle = "style='background-color: #eef0f3;'";
        }
        else{
            if($consumedLicensed == "0"){
                $defaultRowStyle = "style='background-color: #eef0f3;'";
            }
            else{
                $persUsed = ($consumedLicensed * 100) / $productQty;
                if($persUsed >= $warningThreshold){
                    $defaultRowStyle = "style='background-color: #F3C6CA;'";
                }else if($persUsed >= $attentionThreshold && $persUsed <= $warningThreshold){
                    $defaultRowStyle = "style='background-color: #F6EBB1;'";
                }else{
                    $defaultRowStyle = "style='background-color: #eef0f3;'";
                }
            }
        }
        return $defaultRowStyle;
    }
    //End code
    
    function deleteAllFilesFromDirBWLicenseZipFilesExceptRsaAndLicenseZipFile($dirPath, $licenseZipFileName)
    {             
            //echo "<br />Exists File Name - $licenseZipFileName - ";
            $leave_files = array('id_rsa', $licenseZipFileName);
            foreach( glob("$dirPath/*") as $file ) 
            {
                if( !in_array(basename($file), $leave_files))
                {  
                        if(is_dir($file))
                        { 
                            $output1 = shell_exec("chmod 777 $file");
                            $output2 = shell_exec("rm -r $file");
                        }
                }                   
            }
    }    
    
    //code for sytem level detailed alert 
    function setServicePackRowColorBasedOnConsumedQunantityDetailedAlert($productQty, $consumedLicensed, $warningThreshold, $attentionThreshold)
    {
        //In Progress
        $defaultRowStyle = "style='display:none;'";
        if($productQty == "0" || strtolower($productQty) == "unlimited"){
            $defaultRowStyle = "style='display:none;'";
        }
        else{
            if($consumedLicensed == "0"){
               $defaultRowStyle = "style='display:none;'";
            }
            else{
                $persUsed = ($consumedLicensed * 100) / $productQty;
                if($persUsed >= $warningThreshold){
                    $defaultRowStyle = "style='background-color: #F3C6CA;'";
                }else if($persUsed >= $attentionThreshold && $persUsed <= $warningThreshold){
                    $defaultRowStyle = "style='background-color: #F6EBB1;'";
                }else{
                    $defaultRowStyle = "style='display:none;'";
                }
            }
        }
        return $defaultRowStyle;
    }
    
    //Code added @ 06 May 2019
    function findKeyOfArrayWhichHasMoreElementFromOthers($productSPackArr)
    { 
        $productSPArr           = array();
        $productNameWithSpList  = array();
        $productCountArr        = array();
        foreach($productSPackArr as $productLicenseName => $productLicenseRow)
        {
                $productType = $productLicenseRow['productType'];
                if($productType == "User Package" || $productType == "Trunk Package" || $productType == "Add-On Package" )
                { 
                    foreach($productLicenseRow['productServicePackDetails'] as $spName => $spDetailsRow)
                    {
                        if($spName != "NONE"){
                            $productSPArr[$productLicenseName][] = $spName;
                        }
                        if($spName == "NONE"){
                            foreach($productLicenseRow['productServiceDetails']['NONE'] as $serviceDetailsRow){
                                $productSPArr[$productLicenseName][] = $serviceDetailsRow['productServiceName'];
                            }                            
                        }
                    }
                    $count = count($productSPArr[$productLicenseName]);
                    $productCountArr[$productLicenseName] = $count;
                    $productNameWithSpList[$productLicenseName] = $productSPArr[$productLicenseName];
                }
        }
        
        $maximimSPsInProductList = max($productCountArr);
        $productNameWhichHasMaxSpArr = array_keys($productCountArr, $maximimSPsInProductList);
        $productNameWhichHasMaxSps = $productNameWhichHasMaxSpArr[0];
        
        $tmpProductListWithExistNotExistSP = array();
        foreach($productNameWithSpList as $pName => $spList)
        {
            $tmpProductListWithExistNotExistSP[$pName]['Exists']    = $spList;
            $tmpProductListWithExistNotExistSP[$pName]['NotExists'] = array_diff($productNameWithSpList[$productNameWhichHasMaxSps], $spList);
        }
        
        return $tmpProductListWithExistNotExistSP;
        
    }
    //End code
    
    function addTwoArrayValueBasedOnKeys($servicesArr1, $servicesArr2)
    {
        $keyArr      = array();
        $finalArr    = array();
        foreach($servicesArr1 as $keyName1 => $keyVal1){
            $keyArr[] = $keyName1;
        }
        foreach($servicesArr2 as $keyName2 => $keyVal2){
            $keyArr[] = $keyName2;
        }

        foreach($keyArr as $k1){
            $finalArr[$k1] = $servicesArr1[$k1] + $servicesArr2[$k1];
        }    
        return $finalArr;
    }
    
     function getTotalGroupServiceDataWithServicePack($licenseFile, $entName, $licenseServiceArrSPBise, $tmpProductListWithExistNotExistSP)
     {  
             $serviceObj  = new Services();
             $licenseServicesArr = array();
             $tmpServiceListOnUser = array();
             //echo "<pre>";
             $licenseSPArr  = array_keys($licenseServiceArrSPBise);
             $br            = array("1BASE", "1RESBASIC");
             $tg            = array("Trunk Group");
             $bl            = array("2RESSTANDARD", "2BUSINESSLINE");
             $sesoho        = array("3STANDARD", "5RESSTANDARD");
             $se            = array("3STDENT");                                                                               
             $pe            = array("3CONSUMERMOBILITY", "4SOHO", "5PREMIUM", "5SOHO", "Automatic Hold/Retrieve", "BroadTouch MobileLink", "Call Notify", "Outlook Integration");
             
             //echo "<br />licenseFile - $licenseFile";
             //if( $licenseFile == "/var/www/html/BWLicenseZipFiles/2523/252348592.xml"){
             if(1){
             if(file_exists($licenseFile))
             { 
                 $xml = simplexml_load_file($licenseFile);    
                 //Code added @ 28 July 2019
                 if(!empty($xml->services))
                 {
                    //echo "<br />licenseFile - $licenseFile";
                    $tmpGroupServiceList = array();
                    foreach($xml->services as $groupServices)
                    {  
                        $tmpServiceTmp  =  $groupServices->service;
                        foreach($tmpServiceTmp as $tmpServices) 
                        {
                               $tmpMethod  = strval($tmpServices['method']);
                               $serviceType = strval($tmpServices['virtualCount']);                               
                               if($serviceType != "0")
                               {                                   
                                    $tmpGroupServiceName      = strval($tmpServices['name']);
                                    $tmpGroupServiceList[]  = strval($tmpServices['name']);
                               }
                        }
                    }
                    
                    if(!empty($tmpProductListWithExistNotExistSP))
                    {
                           $serviceExistsTmp2 = "";
                           $isGroupServiceUsedLicense = "no";
                           $tmpProductServiceArrDup  = array();
                           $tmpProductNameDup        = "";
                           foreach($tmpProductListWithExistNotExistSP as $productNameTmp => $licenseProductRow)
                           {      

                                  $existsLicenseSPArrDup    = $licenseProductRow['Exists'];
                                  foreach($existsLicenseSPArrDup as $licenseSPNameExistDup)
                                  {                                                        
                                         foreach($licenseServiceArrSPBise[$licenseSPNameExistDup] as $tmpSrvceName)
                                         {
                                             $tmpProductServiceArrDup[$productNameTmp]['exists'][] = $tmpSrvceName;
                                         }                                                          
                                  }                                                      
                                  //$serviceExistsTmp2 = in_array($tmpGroupServiceName, $tmpProductServiceArrDup[$productNameTmp]['exists']);
                                  $serviceExistsTmp2 = !array_diff($tmpGroupServiceList, $tmpProductServiceArrDup[$productNameTmp]['exists']);
                                  if(!empty($serviceExistsTmp2)){
                                       $isGroupServiceUsedLicense = "yes";  
                                       $tmpProductNameDup  = $productNameTmp;
                                  }                                                      
                           }

                           if($isGroupServiceUsedLicense == "yes")
                           {                                      
                                //$productLicenseServicesConsumedList[][$tmpProductNameDup] = $tmpProductNameDup;                               
                           }
                    }
                 }
                 //End Code                 
                 
                 if(!empty($xml->users))
                 {     
                        $count = 1;
                        foreach($xml->users as $userAttriben)
                        {                             
                             if(!empty($userAttriben->user))
                             {                     
                                    $serceUser1  =  $userAttriben->user;                                    
                                    foreach($serceUser1 as $servicesRes)
                                    {
                                        $userId    = strval($servicesRes['id']);
                                        $truckUser = strval($servicesRes['trunk']);
                                        $userType  = strval($servicesRes['type']);
                                        
                                        //if($userType == "User"){
                                        $serceNameRes    = $servicesRes->services;                                        
                                        $tmpServiceListOnUser = array();
                                        foreach($serceNameRes as $ameList)
                                        {                     
                                            foreach($ameList as $listSpItem)
                                            {
                                                $userServiceName = strval($listSpItem['name']);
                                                $tmpServiceListOnUser[] = $userServiceName;  
                                            }
                                        }
                                    
                                     
                                     
                                     $serviceExists = array();
                                     
                                     foreach($licenseSPArr as $licenseSPName)
                                     {
                                         $serviceExists = array_intersect($tmpServiceListOnUser, $licenseServiceArrSPBise[$licenseSPName]);
                                         if(!empty($serviceExists)){
                                                   $licenseServicesArr[$licenseSPName][] = $userId;
                                         }
                                     }
                                                                  
                                     
                                     if(!empty($tmpServiceListOnUser))
                                     {
                                       
                                        
                                        $tmpServiceListOnUserDup        = array();
                                        $tmpPremiumEntProductServiceArr = array();
                                        
                                        $tmpExistsLicenseSPArr = $tmpProductListWithExistNotExistSP['Premium Enterprise']['Exists'];                                        
                                        foreach($tmpExistsLicenseSPArr as $tempSPNameTmp)
                                        {
                                            foreach($licenseServiceArrSPBise[$tempSPNameTmp] as $tmpSrvceName)
                                            {
                                                $tmpPremiumEntProductServiceArr[] = $tmpSrvceName;
                                            } 
                                        }
                                        
                                        foreach($tmpServiceListOnUser as $tmpTempServiceName)
                                        {
                                            if(in_array($tmpTempServiceName, $tmpPremiumEntProductServiceArr))
                                            {
                                                $tmpServiceListOnUserDup[] = $tmpTempServiceName;
                                            }
                                        }
                                        //echo "<br /> tmpServiceListOnUser New  - ";
                                        //print_r($tmpServiceListOnUserDup);
                                               
                                        if(!empty($tmpProductListWithExistNotExistSP))
                                        {
                                               $serviceExistsTmp1 = "";
                                               $isServiceUsedLicense = "no";
                                               $tmpProductServiceArr  = array();
                                               $tmpProductName        = "";
                                               
                                               //$productAllService = $tmpProductListWithExistNotExistSP['Premium Enterprise']['exists'];                                               
                                              
                                               
                                               foreach($tmpProductListWithExistNotExistSP as $productName => $licenseProductRow)
                                               {      
                                                      $existsLicenseSPArr    = $licenseProductRow['Exists'];
                                                      foreach($existsLicenseSPArr as $licenseSPNameExist)
                                                      {                                                        
                                                             foreach($licenseServiceArrSPBise[$licenseSPNameExist] as $tmpSrvceName)
                                                             {
                                                                 $tmpProductServiceArr[$productName]['exists'][] = $tmpSrvceName;
                                                             }                                                          
                                                      }
                                                      
                                                      
                                                      
                                                      $serviceExistsTmp1 = !array_diff($tmpServiceListOnUserDup, $tmpProductServiceArr[$productName]['exists']);
                                                      if(!empty($serviceExistsTmp1)){
                                                           $isServiceUsedLicense = "yes";  
                                                           $tmpProductName       = $productName;
                                                      }                                                      
                                               }                                               
                                        }
                                        
                                        
                                        //echo "<br />serviceExistsTmp1 New -- ";
                                        //print_r($serviceExistsTmp1);
                                        
                                        
                                        
                                        
                                        if($isServiceUsedLicense == "yes")
                                        {        
                                            $tempSPArr  = array();
                                            $tmpArrTest = array();
                                            foreach($tmpServiceListOnUserDup as $tempServiceName)
                                            {                               
                                                   $tempSPArr = $tmpProductListWithExistNotExistSP[$tmpProductName]['Exists'];
                                                   foreach($tempSPArr as $licenseSPNameTemp)
                                                   {       
                                                        if(in_array($tempServiceName, $licenseServiceArrSPBise[$licenseSPNameTemp]))
                                                        {
                                                            $tmpArrTest[$userId][$licenseSPNameTemp] = $tempServiceName;
                                                        }                   
                                                   }
                                            }
                                            //echo "<br />tmpArrTest - ";
                                            //print_r($tmpArrTest);
                                            
                                            foreach($tmpArrTest[$userId] as $tmpArrDupSPName => $TmpRowVal )
                                            {
                                                if(in_array($tmpArrDupSPName, $br)){
                                                    $productLicenseServicesConsumedList[$userId]['Basic Residential'] = "Basic Residential";
                                                }
                                                if(in_array($tmpArrDupSPName, $tg)){
                                                    $productLicenseServicesConsumedList[$userId]['Business Trunk'] = "Business Trunk";
                                                }
                                                if(in_array($tmpArrDupSPName, $bl)){
                                                    $productLicenseServicesConsumedList[$userId]['Business Line'] = "Business Line";
                                                }
                                                if(in_array($tmpArrDupSPName, $sesoho)){
                                                    $productLicenseServicesConsumedList[$userId]['Standard Residential / SOHO'] = "Standard Residential / SOHO";
                                                }
                                                if(in_array($tmpArrDupSPName, $se)){
                                                    $productLicenseServicesConsumedList[$userId]['Standard Enterprise'] = "Standard Enterprise";
                                                }
                                                if(in_array($tmpArrDupSPName, $pe)){
                                                    $productLicenseServicesConsumedList[$userId]['Premium Enterprise'] = "Premium Enterprise";
                                                }
                                            }

                                        }
                                        
                                        if(!empty($productLicenseServicesConsumedList[$userId]))
                                        {                                             
                                                if(count($productLicenseServicesConsumedList[$userId]) == "1")
                                                {
                                                     foreach($productLicenseServicesConsumedList[$userId] as $tmpArrDupProductPName => $TmpDupProductCount )
                                                     {
                                                            if($tmpArrDupProductPName == "Basic Residential"){
                                                                $newProductLicenseServicesConsumedList[$userId]['Basic Residential'] = 1;
                                                            }
                                                            if($tmpArrDupProductPName == "Business Trunk"){
                                                                $newProductLicenseServicesConsumedList[$userId]['Business Trunk'] = 1;
                                                            }
                                                            if($tmpArrDupProductPName == "Business Line"){
                                                                $newProductLicenseServicesConsumedList[$userId]['Business Line'] = 1;
                                                            }
                                                            if($tmpArrDupProductPName == "Standard Residential / SOHO"){
                                                                $newProductLicenseServicesConsumedList[$userId]['Standard Residential / SOHO'] = 1;
                                                            }
                                                            if($tmpArrDupProductPName == "Standard Enterprise"){
                                                                $newProductLicenseServicesConsumedList[$userId]['Standard Enterprise'] = 1;
                                                            }
                                                            if($tmpArrDupProductPName == "Premium Enterprise"){
                                                                $newProductLicenseServicesConsumedList[$userId]['Premium Enterprise'] = 1;
                                                            }
                                                     }
                                                }
                                                else
                                                {
                                                    if(count($productLicenseServicesConsumedList[$userId]) > 1)
                                                    { 
                                                        if(in_array("Business Trunk", $productLicenseServicesConsumedList[$userId]) && $truckUser == "true")
                                                        {
                                                            $newProductLicenseServicesConsumedList[$userId]['Business Trunk'] = 1;
                                                        }
                                                        else
                                                        {
                                                            if(in_array("Premium Enterprise", $productLicenseServicesConsumedList[$userId]))
                                                            {
                                                                    $newProductLicenseServicesConsumedList[$userId]['Premium Enterprise'] = 1;
                                                            }
                                                            else
                                                            {
                                                                if(in_array("Standard Enterprise", $productLicenseServicesConsumedList[$userId])){
                                                                    $newProductLicenseServicesConsumedList[$userId]['Standard Enterprise'] = 1;
                                                                }
                                                                else
                                                                {
                                                                    if(in_array("Standard Residential / SOHO", $productLicenseServicesConsumedList[$userId])){
                                                                        $newProductLicenseServicesConsumedList[$userId]['Standard Residential / SOHO'] = 1;
                                                                    }
                                                                    else
                                                                    {
                                                                        if(in_array("Business Line", $productLicenseServicesConsumedList[$userId])){
                                                                            $newProductLicenseServicesConsumedList[$userId]['Business Line'] = 1;
                                                                        }
                                                                        else
                                                                        {
                                                                            if(in_array("Basic Residential", $productLicenseServicesConsumedList[$userId])){
                                                                                $newProductLicenseServicesConsumedList[$userId]['Basic Residential'] = 1;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }                                             
                                        }
                                        
                                     }
                                 }     
                                 
                              }                              
                         }
                  }
                  
                  //echo "<br /> productLicenseServicesConsumedList -- Test -- ";
                  //print_r($productLicenseServicesConsumedList);
                  
                 $returnServicesLicenseArr['licenseServicesArr'] = $licenseServicesArr;
                 $returnServicesLicenseArr['productLicenseServicesConsumedList'] = $newProductLicenseServicesConsumedList;
                 return $returnServicesLicenseArr;
             }
             }
             
        }

     //Code added @ 06 May 2019
     function getConsumedProductLicensesBasedOnServicePack($licenseFile, $licenseServiceArrSPBise, $tmpProductListWithExistNotExistSP)
     {  
             $productLicenseServicesConsumedList = array();             
             $licenseSPArr = array_keys($licenseServiceArrSPBise);
             
             if(file_exists($licenseFile))
             { 
                 $xml = simplexml_load_file($licenseFile);
                 if(!empty($xml->users))
                 {     
                        $count = 1;
                        foreach($xml->users as $userAttriben)
                        {
                             if(!empty($userAttriben->user))
                             {                     
                                 $serceUser1  =  $userAttriben->user;
                                 foreach($serceUser1 as $servicesRes)
                                 {
                                     $userId = strval($servicesRes['id']);
                                     $serceNameRes         = $servicesRes->services;
                                     $tmpServiceListOnUser = array();
                                     foreach($serceNameRes as $ameList)
                                     {                     
                                         foreach($ameList as $listSpItem)
                                         {
                                             $userServiceName = strval($listSpItem['name']);    
                                             $tmpServiceListOnUser[] = $userServiceName;                                                                            
                                         }
                                     }             
                                     
                                     //Code added @ 07 May 2019
                                     $userSrvcePacks        = $servicesRes->servicePacks;                
                                     foreach($userSrvcePacks as $servicePackList)
                                     {
                                        foreach($servicePackList as $servicePackRow)
                                        {
                                            $userServicePackName    = strval($servicePackRow['name']);
                                            $tmpServicePackArr[]    = $userServicePackName;
                                            $tmpServiceListOnUser[] = $serviceObj->getServicesListOfServicePackBasedOnEnterprise($userServicePackName, $entName);
                                            $count = $count + 1;
                                        } 
                                     }    
                                     //End code
                                     
                                     
                                     
                                     $serviceExistsTmp1 = array();
                                     $serviceExistsTmp2 = array();
                                     
                                     
                                     $isServiceUsedLicense = "no";
                                     foreach($tmpProductListWithExistNotExistSP as $productName => $licenseProductRow)
                                     {      
                                            $existsLicenseSPArr    = $licenseProductRow['Exists'];
                                            $notExistsLicenseSPArr = $licenseProductRow['NotExists'];                                            
                                            
                                            foreach($existsLicenseSPArr as $licenseSPNameExist)
                                            { 
                                                  $serviceExistsTmp1 = array_intersect($tmpServiceListOnUser, $licenseServiceArrSPBise[$licenseSPNameExist]);
                                                   if(!empty($serviceExistsTmp1)){
                                                       $isServiceUsedLicense = "yes";
                                                   }
                                            }
                                            foreach($notExistsLicenseSPArr as $licenseSPNameNotExist)
                                            {
                                                $serviceExistsTmp2 = array_intersect($tmpServiceListOnUser, $licenseServiceArrSPBise[$licenseSPNameNotExist]);
                                                //echo "<br />ServiceExistsTmp2 - ";
                                                //print_r($serviceExistsTmp2);
                                                if(!empty($serviceExistsTmp2))
                                                {
                                                    $isServiceUsedLicense = "no";
                                                }
                                            }                                               
                                            if($isServiceUsedLicense == "yes" && !in_array($userId, $productLicenseServicesConsumedList[$productName]))
                                            {
                                                $productLicenseServicesConsumedList[$productName][] = $userId;
                                            }
                                     }
                                 }                                    
                              }                              
                         }
                  } 
                  return $productLicenseServicesConsumedList;                                
             }
    }
      //End code  
      
     function getServiceUsedTrunckByServiceAndUser($licenseFile, $tmpServiceListOnUser)
     {               
             if(file_exists($licenseFile))
             { 
                 $xml = simplexml_load_file($licenseFile);
                 if(!empty($xml->users))
                 {     
                        $count = 1;
                        //$tmpServiceListOnUser = array();
                        foreach($xml->users as $userAttriben)
                        {
                             if(!empty($userAttriben->user))
                             {                     
                                 $serceUser1  =  $userAttriben->user;
                                 foreach($serceUser1 as $servicesRes)
                                 {
                                     $userId    = strval($servicesRes['id']);
                                     $userType  = strval($servicesRes['type']); 
                                     $truckUser = strval($servicesRes['trunk']);
                                     if($truckUser == "true")
                                     {
                                            $serceNameRes         = $servicesRes->services;                                            
                                            foreach($serceNameRes as $ameList)
                                            {                     
                                                foreach($ameList as $listSpItem)
                                                {
                                                    $userServiceName = strval($listSpItem['name']);  
                                                    if(array_key_exists($userServiceName, $tmpServiceListOnUser))
                                                    {
                                                        $oldServiceTrunk = $tmpServiceListOnUser[$userServiceName];
                                                        $newServiceTrunk = $oldServiceTrunk + 1;
                                                        $tmpServiceListOnUser[$userServiceName] = $newServiceTrunk;
                                                    }
                                                    else
                                                    {
                                                        $tmpServiceListOnUser[$userServiceName] = 1; 
                                                    }                                                                                                                               
                                                }
                                            }
                                    }
                                  }                                    
                              }                              
                         }
                  }                 
                 return $tmpServiceListOnUser;
             }
        }

    
    function getBWLicenseZippedFileFromBrowdworks($clusterNameForBWLicnese, $ipAddressForBWLicnese, $bwAppServerInstance){
        
        $currDate1 = date("Ymd");
        $currDate2 = date("Y-m-d"); 
        
        if($bwAppServerInstance == "primary"){
            $bwInstance = "as1";
        }if($bwAppServerInstance == "secondary"){
            $bwInstance = "as2";
        }
        
        $getHostNameCmd = "ssh -i /var/www/BWLicenseZipFiles/id_rsa bwadmin@$ipAddressForBWLicnese '/bin/hostname' ";
        $opGetHostName  = shell_exec($getHostNameCmd);
        $bwInstance     = trim($opGetHostName);
        
        if($bwInstance == "")
        {
            if($ipAddressForBWLicnese == "172.31.0.70" || $ipAddressForBWLicnese == "as1-lab.averistar.internal"){
                $bwInstance = "as1";
            }if($ipAddressForBWLicnese == "172.31.0.71" || $ipAddressForBWLicnese == "as2-lab.averistar.internal"){
                $bwInstance = "as2";
            }if($ipAddressForBWLicnese == "10.100.10.201" || $ipAddressForBWLicnese == "as1-bae.averistar.internal"){
                $bwInstance = "as1-lab";
            }if($ipAddressForBWLicnese == "10.100.10.211" || $ipAddressForBWLicnese == "as2-bae.averistar.internal"){
                $bwInstance = "as2-lab";
            }if($ipAddressForBWLicnese == "172.31.0.111" || $ipAddressForBWLicnese == "as1.avlab.averistar.internal"){
                $bwInstance = "as1";
            }if($ipAddressForBWLicnese == "172.31.0.121" || $ipAddressForBWLicnese == "as2.avlab.averistar.internal"){
                $bwInstance = "as2";
            }
        }
        
        //$bwInstance = "as1";        
        //echo "<br />BW Instanse - $bwInstance, Cluster Name - $clusterNameForBWLicnese, IP Address - $ipAddressForBWLicnese, Curr Date 1 - $currDate1, Curr Date 2 - $currDate2";
        //$cmd = "sh /home/Sollogics/getLicenseZipFile.sh '".$clusterNameForBWLicnese."' '".$ipAddressForBWLicnese."' '".$currDate1."' '".$currDate2."'";        
                
        /*$currDate1 = "20190731";
        $currDate2 = "2019-07-31";*/
        
        $licenseZipFileName  = "/var/www/html/BWLicenseZipFiles/bwLicenseFiles-".$clusterNameForBWLicnese."_".$currDate2.".zip";
        $cmd = "/usr/bin/scp -o StrictHostKeyChecking=no -Bvp -i /var/www/BWLicenseZipFiles/id_rsa bwadmin@$ipAddressForBWLicnese:/var/broadworks/serviceLicenseReporting/serviceLicenseCollect_".$bwInstance."_$currDate1.zip $licenseZipFileName >> /tmp/test/test-error.log 2>&1";
        $output = shell_exec($cmd);
        //$licenseZipFileName = "serviceLicenseData.zip";
        return $licenseZipFileName;
    }    
        
    
    $BWLicenseError = array();
    if($_POST['funcAction'] == "fileExtractMethod")
    {    
            
            $licenseZipFileName            = getBWLicenseZippedFileFromBrowdworks($clusterNameForBWLicnese, $ipAddressForBWLicnese, $bwAppServerInstance);
            //$licenseZipFileName          = "bwLicenseFiles-AveriStar-172.31.0.70_2019-05-09.zip";
            $dirPath                       = "/var/www/html/BWLicenseZipFiles";
            $licenseZipFileNameWithPath    = $dirPath."/".$licenseZipFileName;
            
            $tmpArr                         = explode("_", $licenseZipFileName);
            $fileDate                       = $tmpArr[1];
            $_SESSION['licenseFileGenDate'] = str_ireplace(".zip", "", $fileDate);
        
            $zip = new ZipArchive;    
            $BWLicenseError = array();
            
            //Delete All existing Directories except RSA file and License Zip files
            deleteAllFilesFromDirBWLicenseZipFilesExceptRsaAndLicenseZipFile($dirPath, $licenseZipFileName);
            if(file_exists($licenseZipFileNameWithPath))
            {  
                if ($zip->open($licenseZipFileNameWithPath) === TRUE) 
                {
                    $zip->extractTo($dirPath);
                    $zip->close();
                    $BWLicenseError['BWLicenseZipFileMsg']['success'] = "File Downloaded and extracted successfully";
                    echo "done";
                }
            }
            else
            {
                if (is_dir($dirPath))
                {
                   //Didn't got latest zipped license file and Searching in current directory and process it
                   $filesArr = array();
                   if ($dh = opendir($dirPath))
                   {
                        while (($file = readdir($dh)) !== false)
                        {
                            if(stripos($file,"zip") !== false)
                            {                                
                                $licenseZipFileName = $file;
                                $licenseZipFileNameWithPath = $dirPath."/".$file;
                                $filesArr[filemtime($licenseZipFileNameWithPath)] = $licenseZipFileNameWithPath;
                            }
                        }
                        closedir($dh);
                        usort($filesArr, function($a, $b) {
                            return filemtime($a) < filemtime($b);
                        });
                        
                        if(isset($filesArr[0]) && $filesArr[0] != "")
                        {
                            $licenseZipFileName = $filesArr[0];
                            $licenseZipFileNameWithPath = $licenseZipFileName;
                        }
                        else
                        {                            
                            $licenseZipFileNameWithPath = $licenseZipFileName;
                        }                        
                    }
                    if(file_exists($licenseZipFileNameWithPath))
                        {  
                            if ($zip->open($licenseZipFileNameWithPath) === TRUE) 
                            {
                                $zip->extractTo($dirPath);
                                $zip->close();
                                $BWLicenseError['BWLicenseZipFileMsg']['success'] = "File Downloaded and extracted successfully";
                                echo "done";
                            }
                        }
                  }
            } 
            $_SESSION['licenseZipFileNameWithPath'] = $licenseZipFileNameWithPath;            
    }
    
        
    if($_POST['funcAction'] == "LicenseXMLFile")
    {
       $dirPath  = "/var/www/html/BWLicenseZipFiles/";
       function readLicenseXMLFileInBWExpressLicense($dirPath)
       {
                $licenseFile = $dirPath."lic.xml";
                $returnArr   = array();
                $returnArr['BWLicenseError'] = "";
                $BWLicenseError = array();
                if(file_exists($licenseFile))
                {
                    $BWLicenseError['ReadBWLicenseXMLFile']['success'] = "BWExpressLicense XML File is Exists";
                    $xml = simplexml_load_file($licenseFile);
                    $licenseServiceArr             = array();
                    $licenseServiceArrLevelBise    = array();
                    $licenseServiceArrLevelBiseDup = array();
                    $licenseServiceArrSPBise       = array();
                    $licenseProductArr             = array();
                    $licenseSPQTYArr               = array();
                    //Getting License Service Pack Name
                    
                    //Code added @ 01 May 2019
                    //echo "<br />License Tag Name - ".$licenseTagName        = $xml->BWLicense;
                    $licenseTagName        = $xml->BWLicense;
                    $userLicense           = strval($licenseTagName['userLicense']);
                    $groupLicense          = "unlimited";
                    if(strval($licenseTagName['numGroupLicense']) != "0"){
                            $groupLicense  = strval($licenseTagName['numGroupLicense']);
                    }
                    $returnArr['userLicense']            = $userLicense;
                    $returnArr['groupLicense']           = $groupLicense;
                    $returnArr['virtualUserLicense']     = "unlimited";
                    $returnArr['trunkGroupUserLicense']  = "unlimited";
                    //End code
                    
                    if(count($xml->servicePack) > 0)
                    {
                        foreach($xml->servicePack as $licenseServicePackName){
                             $servicePackName  = strval($licenseServicePackName['name']);
                             $servicePackLevel = strval($licenseServicePackName['level']);
                             $servicePackQty   = strval($licenseServicePackName['quantity']);

                             foreach($licenseServicePackName->service as $serviceArr)
                             {    
                                 $serviceName = strval($serviceArr['name']);
                                 $licenseServiceArr[$serviceName] = array('servicePackName' => $servicePackName, 'servicePackLevel' => $servicePackLevel, 'servicePackQty' => $servicePackQty);
                                 $licenseServiceArrLevelBise[$servicePackLevel][$serviceName] = array('servicePackName' => $servicePackName, 'servicePackLevel' => $servicePackLevel, 'servicePackQty' => $servicePackQty);
                                 $licenseServiceArrSPBise[$servicePackName][] =  $serviceName;
                                 $licenseSPQTYArr[$servicePackName] = $servicePackQty;
                                 $licenseServiceArrLevelBiseDup[$servicePackLevel][$servicePackName][] = $serviceName;
                                 
                                 
                             }
                        }
                    }
                    $returnArr['licenseServiceArr']             = $licenseServiceArr;
                    $returnArr['licenseServiceArrLevelBise']    = $licenseServiceArrLevelBise;
                    $returnArr['licenseServiceArrSPBise']       = $licenseServiceArrSPBise;
                    $returnArr['licenseSPQTYArr']               = $licenseSPQTYArr;
                    $returnArr['licenseServiceArrLevelBiseDup'] = $licenseServiceArrLevelBiseDup;
                    

                    $tagName1 = "com.broadsoft.apm.managedservice.licensedProduct";
                    $licensedProduct = $xml->licensedProductArray->$tagName1;
                    foreach($licensedProduct as $licensedProductRow)
                    {
                        $productName         = strval($licensedProductRow['productName']);
                        $productCode         = strval($licensedProductRow['productCode']);
                        $productType         = strval($licensedProductRow['type']);
                        $productQty          = strval($licensedProductRow['quantity']);
                        $productProfileName  = strval($licensedProductRow['profileName']);

                        $tagName2 = "com.broadsoft.apm.managedservice.licensedProductServicePack";
                        $productServicePackNameArr = $licensedProductRow->licensedProductServicePackArray->$tagName2;

                        $servicePackTmpArr = array();
                        $serviceTmpCompArr = array();

                        foreach($productServicePackNameArr as $productServicePackNameRow)
                        {                
                            $servicePackArr = array();
                            $servicePackArr['productServicePackName'] = $servicePckName = strval($productServicePackNameRow['servicePackName']); 
                            $servicePackArr['quantity']               = strval($productServicePackNameRow['quantity']); 
                            $servicePackTmpArr[$servicePckName]       = $servicePackArr;           

                            $tagName3 = "com.broadsoft.apm.managedservice.licensedProductServicePackEntity";
                            $productServiceNameArr         = $productServicePackNameRow->licensableEntitiesArray->$tagName3;
                            foreach($productServiceNameArr as $productServiceNameRow)
                            {
                                if(!empty($productServiceNameRow))
                                {
                                        $serviceTmpArr = array();
                                        $serviceTmpArr['productServiceName']  = strval($productServiceNameRow['serviceName']); 
                                        $serviceTmpArr['quantity']            = strval($productServiceNameRow['quantity']);
                                        $serviceTmpCompArr[$servicePckName][] = $serviceTmpArr;
                                }
                            }  
                        }

                        $licenseProductArr[$productName] = array(
                                                                        'productServicePackDetails' => $servicePackTmpArr,
                                                                        'productServiceDetails'     => $serviceTmpCompArr,
                                                                        'productName'               =>  $productName,
                                                                        'productCode'               =>  $productCode,
                                                                        'productType'               =>  $productType,
                                                                        'productName'               =>  $productName,
                                                                        'productQty'                =>  $productQty,
                                                                        'productProfileName'        =>  $productProfileName                                                            
                                                                );
                        $returnArr['licenseProductArr'] = $licenseProductArr;
                    }
                }
                else
                {                    
                    $returnArr['BWLicenseError'] = "error";
                }    
                return $returnArr;
        }
        
        $licenseArr = readLicenseXMLFileInBWExpressLicense($dirPath);
        $_SESSION['licenseArr'] = array();        
        if($licenseArr['BWLicenseError'] == "")
        {
            $_SESSION['licenseArr'] = $licenseArr;
            echo "done";
        }
        if($licenseArr['BWLicenseError'] == "error"){
            echo "Unable to read BWExpressLicense License XML File";
        }
        
        
    }

    if($_POST['funcAction'] == "readCtrlXMLFile"){    
        $dirPath  = "/var/www/html/BWLicenseZipFiles/";
        function readCtrlXMLFileInBWExpressLicense($dirPath)
        {
            $licenseFile = $dirPath."ctrl.xml";
            $returnArr['BWLicenseError'] = "";
            if(file_exists($licenseFile))
            {                
                $xml = simplexml_load_file($licenseFile);
                if(count($xml) > 0)
                {            
                    $systemProperties = array();
                    foreach($xml->properties as $properties)
                    {  
                        $systemProperties['hostname']   = strval($properties->hostname);
                        $systemProperties['sysDate']    = strval($properties->date);
                        $systemProperties['sysTime']    = strval($properties->time);
                        $systemProperties['sysVrsn']    = strval($properties->version);
                    }

                    $enterpriseArr = array();
                    foreach($xml->serviceProvider as $serverProviderArr)
                    {                

                        $spId  = strval($serverProviderArr['id']);
                        $isEnt = strval($serverProviderArr['isEnterprise']);

                        $groupDetailsArr = $serverProviderArr->group;
                        $groupArr = array();
                        foreach($groupDetailsArr as $groupRow)
                        {
                            $groupTmpArr              = array();
                            $groupTmpArr['groupId']   = strval($groupRow['uid']);
                            $groupTmpArr['groupName'] = strval($groupRow['id']);                          
                            $groupArr[]               = $groupTmpArr;                    
                        }
                        $enterpriseArr[$spId] = $groupArr;
                    }
                }
                else
                {
                    $returnArr['BWLicenseError'] = "error";
                }        
            }
            else{
                $returnArr['BWLicenseError'] = "error";
            }
            $returnArr['EnterpriseArr']  = $enterpriseArr;            
            return $returnArr;
        }
        
        $_SESSION['EnterpriseArr'] = array(); 
        $ctrlXMLDataArr = readCtrlXMLFileInBWExpressLicense($dirPath);
        if($ctrlXMLDataArr['BWLicenseError'] == ""){
            $_SESSION['EnterpriseArr'] = $ctrlXMLDataArr['EnterpriseArr']; 
            echo "done";
        }
        if($ctrlXMLDataArr['BWLicenseError'] == "error")
        {
            echo "Unable to read BWExpressLicense CTRL XML File";
        }
    }
            
    if($_POST['funcAction'] == "groupIdXML"){
       
       $dirPath  = "/var/www/html/BWLicenseZipFiles/";
       function getGroupServiceDataWithServicePack($licenseFile, $entName, $licenseServiceArrSPBise)
        {             
             $serviceObj                  = new Services();
             $servicesArr                 = array();
             $servicePackServicesArr      = array();
             $tmpServicePackArr           = array();
             $servicesArrTmp              = array();
             $servicesArrTmpTotal         = array();
             $groupServicesArrTmpTotal    = array();
             $servicesArrTmpTotalFromSP   = array();
             $groupServicesArr            = array();
             $virtualServicesArr          = array();
             $userLicenseCount            = 0;
             $virtualLicenseCount         = 0;
             $trunkUserLicenseCount       = 0;
             
             if(file_exists($licenseFile))
             { 
                 $xml = simplexml_load_file($licenseFile);
                 $count = 1; 
                 //echo "<pre>XML";
                 if(!empty($xml->services))
                 { 
                        $groupSerceNameRes      = $xml->services;
                        foreach($groupSerceNameRes as $groupServiceList)
                        {  
                            foreach($groupServiceList as $groupListSpItem)
                            {                                
                                $groupServiceName   = strval($groupListSpItem['name']);
                                $groupServicesArr[] = $groupServiceName;  
                                
                                $virtualCountStatus = strval($groupListSpItem['virtualCount']);
                                if($virtualCountStatus != "0"){
                                    for($x=1; $x<=$virtualCountStatus; $x++){
                                        $virtualServicesArr[] = $groupServiceName;
                                    }
                                }
                            }
                        }
                        $groupServicesArrTmpTotal     = array_count_values($groupServicesArr);
                        $virtualServicesArrTmpTotal   = array_count_values($virtualServicesArr);
                 }
                 
                 if(!empty($xml->users))
                 {
                     foreach($xml->users as $userAttribe)
                     {
                        if(!empty($userAttribe->user))
                        {                     
                            $serceUser  =  $userAttribe->user;                    
                            foreach($serceUser as $servicesRes)
                            {
                                $userType  = strval($servicesRes['type']); 
                                $truckUser = strval($servicesRes['trunk']);
                                if($userType == "User" && $truckUser == "false"){
                                    $userLicenseCount       = $userLicenseCount + 1;
                                }
                                if($userType != "User"){
                                    $virtualLicenseCount    = $virtualLicenseCount + 1;
                                }
                                if($truckUser == "true"){
                                    $trunkUserLicenseCount  = $trunkUserLicenseCount + 1;
                                }


                                $serceNameRes      = $servicesRes->services;
                                foreach($serceNameRes as $ameList)
                                {                     
                                    foreach($ameList as $listSpItem)
                                    {
                                        $userServiceName       = strval($listSpItem['name']);
                                        $servicesArrTmp[]      = $userServiceName;
                                    }
                                }
                                
                                $servicesArrTmpTotal   = array_count_values($servicesArrTmp);
                                $userSrvcePacks        = $servicesRes->servicePacks;                
                                foreach($userSrvcePacks as $servicePackList)
                                {
                                    foreach($servicePackList as $servicePackRow)
                                    {
                                        $userServicePackName    = strval($servicePackRow['name']);
                                        $tmpServicePackArr[]    = $userServicePackName;
                                        //$servicePackServicesArr[$count] = $serviceObj->getServicesListOfServicePackBasedOnEnterprise($userServicePackName, $entName);
                                        $count = $count + 1;
                                    } 
                                }              
                            }           
                         }
                     }   
                 }
             }         

             if(!empty($tmpServicePackArr) && count($tmpServicePackArr) > 0)
             {
                 $tmpServicePackArr = array_count_values($tmpServicePackArr); 
                 $tmpServicesArr = array();
                 foreach($tmpServicePackArr as $userServicePackName => $countOfServicePacks)
                 {
                     $tmpServicesArr = $serviceObj->getServicesListOfServicePackBasedOnEnterprise($userServicePackName, $entName);
                     foreach($tmpServicesArr as $srvceName)
                     {
                         $servicesArrTmpTotalFromSP[$srvceName] = $countOfServicePacks;
                     }
                 }         
             }
             $servicesArrTmpTotalFromSP          = array();
             $returnArr['servicesArr']           = addTwoArrayValueBasedOnKeys($servicesArrTmpTotal, $servicesArrTmpTotalFromSP);
             $returnArr['groupServicesArr']      = addTwoArrayValueBasedOnKeys($groupServicesArrTmpTotal, $servicesArrTmpTotalFromSP);
             $returnArr['virtualServicesArr']    = addTwoArrayValueBasedOnKeys($virtualServicesArrTmpTotal, $servicesArrTmpTotalFromSP);
             $returnArr['userLicenseCount']      = $userLicenseCount;
             $returnArr['virtualLicenseCount']   = $virtualLicenseCount;
             $returnArr['trunkUserLicenseCount'] = $trunkUserLicenseCount;
             
             
             return $returnArr;
         }

        //echo "<pre>";
        $groupUsersServiceArr        = array();
        $finalServicesArr            = array(); 
        $finalGroupServicesArr       = array();
        $finalVirtualServicesArr     = array();
        //echo "<pre>Ent Name - ".$_POST['entName'];
        //die();
        //if(0)
        if(isset($_POST['entName']) && trim($_POST['entName']) != "")
        {
            $spID                      = $_POST['entName'];
            $enterprisesGroupArr       = $_SESSION['EnterpriseArr'];
            $licenseArr                = $_SESSION['licenseArr'];
            $licenseServiceArrSPBise   = $licenseArr['licenseServiceArrSPBise'];
            $serviceProviderGroupArr   = $enterprisesGroupArr[$spID];
            
            $_SESSION['EnterpriseArr'] = array();
            $_SESSION['licenseArr']    = array();  
            
            $_SESSION['completeServicesArr']   = array();
            $_SESSION['Base1']                 = 0;
            $tmpProductListWithExistNotExistSP = array();
            
            if(isset($serviceProviderGroupArr) && count($serviceProviderGroupArr) > 0)
            {         
                    $i = 1;
                    $groupLicenseCount   = 0;
                    $userLicenseCount    = 0;
                    $virtualLicenseCount = 0;
                    foreach($serviceProviderGroupArr as $groupArr)
                    {
                        $groupId                 = $groupArr['groupId'];
                        $groupName               = $groupArr['groupName'];
                        $groupDirName            = substr($groupId, 0, 4);
                        $groupFileName           = $dirPath.$groupDirName."/".$groupId.".xml";        
                        $groupUsersServiceArr[]  = getGroupServiceDataWithServicePack($groupFileName, $spID, $licenseServiceArrSPBise);
                        $groupLicenseCount       = $groupLicenseCount + 1;
                        
                        $totalGroupUserServiceArr[] = getTotalGroupServiceDataWithServicePack($groupFileName, $spID, $licenseServiceArrSPBise, $tmpProductListWithExistNotExistSP);
                    }
                    
                    
                    foreach($totalGroupUserServiceArr as $tpKey1 => $totalGroupUserServiceRow)
                    {
                        $totalGroupUserServiceArr[]       = $totalGroupUserServiceRow['licenseServicesArr'];
                        //$totalConsumedProductServiceArr[] = $totalConsumedProductUserServiceRow['productLicenseServicesConsumedList'];
                    }
                    
                    
                    foreach($totalGroupUserServiceArr as $totalUsedSPArr)
                    {
                         foreach($totalUsedSPArr as $tmpSPName => $usersArr)
                         {
                             if(array_key_exists($tmpSPName, $totalSPCountArr)){
                                   $oldCount    = $totalSPCountArr[$tmpSPName];
                                   $totalCount  = $oldCount + count($usersArr);
                                   $totalSPCountArr[$tmpSPName] = $totalCount;
                             }else{
                                   $totalSPCountArr[$tmpSPName] = count($usersArr);
                             }
                         }                         
                    }
                    //echo "<br />Base1 - ".$_SESSION['Base1'];
                    //print_r($_SESSION['completeServicesArr']);
                        
                        //Getting User Services                    
                        $groupUsersServiceCompleteArr = array();
                        foreach ($groupUsersServiceArr as $tmpK => $tmpSubArray) 
                        {      
                            $userLicenseCount       += $tmpSubArray['userLicenseCount'];
                            $virtualLicenseCount    += $tmpSubArray['virtualLicenseCount'];
                            $trunkUserLicenseCount  += $tmpSubArray['trunkUserLicenseCount'];
                            foreach ($tmpSubArray['servicesArr'] as $tmpId =>$tmpValue) 
                            {                                
                                $groupUsersServiceCompleteArr[$tmpId]+= $tmpValue;
                            }
                        }
                        if(count($groupUsersServiceCompleteArr) > 0)
                        {
                            $licenseServicePackArr       = $licenseArr['licenseServiceArrLevelBise']['USER'];
                            foreach($groupUsersServiceCompleteArr as $serviceName => $serviceCount)
                            {
                                $licenseServicePackName = $licenseServicePackArr[$serviceName]['servicePackName'];
                                $finalServicesArr[$spID][$licenseServicePackName][$serviceName] = $serviceCount;                    
                            }
                        }
                    
                        //Getting Group Services                  
                        $groupServiceCompleteArr = array();                        
                        foreach ($groupUsersServiceArr as $tmpK1 => $tmpSubArray1) 
                        {
                            foreach ($tmpSubArray1['groupServicesArr'] as $tmpId1 => $tmpValue1)
                            {
                                $groupServiceCompleteArr[$tmpId1]+= $tmpValue1;
                            }
                        }                        
                        if(count($groupServiceCompleteArr) > 0)
                        {
                            $licenseServicePackArr1  = $licenseArr['licenseServiceArrLevelBise']['GROUP'];                            
                            foreach($groupServiceCompleteArr as $serviceName1 => $serviceCount1)
                            {
                                $licenseServicePackName1 = $licenseServicePackArr1[$serviceName1]['servicePackName'];
                                $finalGroupServicesArr[$spID][$licenseServicePackName1][$serviceName1] = $serviceCount1;                  
                            }
                        }
                     
                    //}
                    
                    //Getting Virtual Services                    
                    $virtualServiceCompleteArr = array();
                    foreach ($groupUsersServiceArr as $tmpK2 => $tmpSubArray2) 
                    {
                        //foreach ($tmpSubArray2['groupServicesArr'] as $tmpId2 => $tmpValue2) 
                        foreach ($tmpSubArray2['virtualServicesArr'] as $tmpId2 => $tmpValue2)    
                        {
                            $virtualServiceCompleteArr[$tmpId2]+= $tmpValue2;
                        }
                    }
                    if(count($virtualServiceCompleteArr) > 0)
                    {
                        $licenseServicePackArr2  = $licenseArr['licenseServiceArrLevelBise']['VIRTUAL'];
                        foreach($virtualServiceCompleteArr as $serviceName2 => $serviceCount2)
                        {
                            $licenseServicePackName2 = $licenseServicePackArr2[$serviceName2]['servicePackName'];
                            $finalVirtualServicesArr[$spID][$licenseServicePackName2][$serviceName2] = $serviceCount2;                  
                        }
                    }
                    
                                        
            }
           
            $finalEnterpriseServicesArr = array();
            foreach($finalServicesArr as $tmpKeyForEntService){
                $finalEnterpriseServicesArr = $tmpKeyForEntService;
            }        
            
            $finalEnterpriseGroupServicesArr = array();
            foreach($finalGroupServicesArr as $tmpKeyForEntGroupService){
                $finalEnterpriseGroupServicesArr = $tmpKeyForEntGroupService;
            }  
            
            $finalEnterpriseVirtualServicesArr = array();
            foreach($finalVirtualServicesArr as $tmpKeyForEntVirtualService){
                $finalEnterpriseVirtualServicesArr = $tmpKeyForEntVirtualService;
            }
            
            $context = "enterprise";
        }
          
        //if(1)
        if(trim($_POST['entName']) == "")               
        {       
            $context = "system";
            $licenseArr                = $_SESSION['licenseArr'];
            $enterprisesGroupArr       = $_SESSION['EnterpriseArr'];            
            $licenseServiceArrSPBise   = $licenseArr['licenseServiceArrSPBise'];
            $groupUsersServiceArr      = array();
            $totalGroupUserServiceArr  = array();
            $tmpServiceListOnUser      = array();
            $totalConsumedProductServiceArr = array();
            
            $groupLicenseCount1   = 0;
            $userLicenseCount1    = 0;
            $virtualLicenseCount1 = 0;
            
            //echo "<pre>Test - ";
            //Code added @ 06 May 2019
            $tmpProductListWithExistNotExistSP = findKeyOfArrayWhichHasMoreElementFromOthers($licenseArr['licenseProductArr']);
           
            //End code
            foreach($enterprisesGroupArr as $entName => $enterprisesGroupArr)
            {
                $j = 1;                
                foreach($enterprisesGroupArr as $groupArr)
                {
                    $groupId                                = $groupArr['groupId'];
                    $groupName                              = $groupArr['groupName'];
                    $groupDirName                           = substr($groupId, 0, 4);
                    $groupFileName                          = $dirPath.$groupDirName."/".$groupId.".xml";        
                    //$groupUsersServiceArr[]                 = getGroupServiceDataWithServicePack($groupFileName, $groupUsersServiceArr, $entName);
                    $groupUsersServiceArr[]                 = getGroupServiceDataWithServicePack($groupFileName, $entName, $licenseServiceArrSPBise);
                
                    $groupLicenseCount1                     = $groupLicenseCount1 + 1;   
                    $totalConsumedProductUserServiceArr[]   = getTotalGroupServiceDataWithServicePack($groupFileName, $entName, $licenseServiceArrSPBise, $tmpProductListWithExistNotExistSP);
                    $tmpServiceListOnUser                   = getServiceUsedTrunckByServiceAndUser($groupFileName, $tmpServiceListOnUser);
                    
                    //Code added @ 06 May 2019
                    //$totalConsumedProductServiceArr[] = getConsumedProductLicensesBasedOnServicePack($groupFileName, $licenseServiceArrSPBise, $tmpProductListWithExistNotExistSP);
                    //End code                    
                }
            }            
            //$totalGroupUserServiceArr = $totalGroupUserServiceArr[''];
            
            foreach($totalConsumedProductUserServiceArr as $tpKey1 => $totalConsumedProductUserServiceRow)
            {
                $totalGroupUserServiceArr[]       = $totalConsumedProductUserServiceRow['licenseServicesArr'];
                $totalConsumedProductServiceArr[] = $totalConsumedProductUserServiceRow['productLicenseServicesConsumedList'];
            }
            
            //Code added @ 07 May 2019
            $totalproductCountArr = array();
            foreach($totalConsumedProductServiceArr as $totalConsumedProductArr)
            {
                foreach($totalConsumedProductArr as $tmpUserId => $usersDataArr)
                {
                    foreach($usersDataArr as $tmpProductName => $tmpProductNameVal)
                    {                        
                        if(array_key_exists($tmpProductName, $totalproductCountArr))
                        { 
                             $oldProductCount    = $totalproductCountArr[$tmpProductName];
                             $totalProductCount  = $oldProductCount + $tmpProductNameVal;
                             $totalproductCountArr[$tmpProductName] = $totalProductCount;
                        }
                        else{
                            $totalproductCountArr[$tmpProductName] = $tmpProductNameVal;
                        }                        
                    }
                }            
            }
            //End code
            //Code added @ 30 April 2019  
            foreach($totalGroupUserServiceArr as $totalUsedSPArr)
            {
                 foreach($totalUsedSPArr as $tmpSPName => $usersArr)
                 {
                     if(array_key_exists($tmpSPName, $totalSPCountArr)){
                           $oldCount    = $totalSPCountArr[$tmpSPName];
                           $totalCount  = $oldCount + count($usersArr);
                           $totalSPCountArr[$tmpSPName] = $totalCount;
                     }else{
                           $totalSPCountArr[$tmpSPName] = count($usersArr);
                     }
                 }                         
            }
            
            //Getting User Services                    
            $groupUsersServiceCompleteArr = array();
            foreach ($groupUsersServiceArr as $tmpK => $tmpSubArray) 
            {      
                $userLicenseCount1       += $tmpSubArray['userLicenseCount'];
                $virtualLicenseCount1    += $tmpSubArray['virtualLicenseCount'];
                $trunkUserLicenseCount1  += $tmpSubArray['trunkUserLicenseCount'];
                foreach ($tmpSubArray['servicesArr'] as $tmpId =>$tmpValue) 
                {                                
                    $groupUsersServiceCompleteArr[$tmpId]+= $tmpValue;
                }
            }
            if(count($groupUsersServiceCompleteArr) > 0)
            {
                $licenseServicePackArr       = $licenseArr['licenseServiceArrLevelBise']['USER'];
                foreach($groupUsersServiceCompleteArr as $serviceName => $serviceCount)
                {
                    $licenseServicePackName = $licenseServicePackArr[$serviceName]['servicePackName'];
                    $finalServicesArr[$spID][$licenseServicePackName][$serviceName] = $serviceCount;                    
                }
            }

            //Getting Group Services                  
            $groupServiceCompleteArr = array();                        
            foreach ($groupUsersServiceArr as $tmpK1 => $tmpSubArray1) 
            {
                foreach ($tmpSubArray1['groupServicesArr'] as $tmpId1 => $tmpValue1)
                {
                    $groupServiceCompleteArr[$tmpId1]+= $tmpValue1;
                }
            }                        
            if(count($groupServiceCompleteArr) > 0)
            {
                $licenseServicePackArr1  = $licenseArr['licenseServiceArrLevelBise']['GROUP'];                            
                foreach($groupServiceCompleteArr as $serviceName1 => $serviceCount1)
                {
                    $licenseServicePackName1 = $licenseServicePackArr1[$serviceName1]['servicePackName'];
                    $finalGroupServicesArr[$spID][$licenseServicePackName1][$serviceName1] = $serviceCount1;                  
                }
            }

       

            //Getting Virtual Services                    
            $virtualServiceCompleteArr = array();
            foreach ($groupUsersServiceArr as $tmpK2 => $tmpSubArray2) 
            {
                foreach ($tmpSubArray2['virtualServicesArr'] as $tmpId2 => $tmpValue2) 
                {
                    $virtualServiceCompleteArr[$tmpId2]+= $tmpValue2;
                }
            }
            if(count($virtualServiceCompleteArr) > 0)
            {
                $licenseServicePackArr2  = $licenseArr['licenseServiceArrLevelBise']['VIRTUAL'];
                foreach($virtualServiceCompleteArr as $serviceName2 => $serviceCount2)
                {
                    $licenseServicePackName2 = $licenseServicePackArr2[$serviceName2]['servicePackName'];
                    $finalVirtualServicesArr[$spID][$licenseServicePackName2][$serviceName2] = $serviceCount2;                  
                }
            }
            
            
            $finalEnterpriseServicesArr = array();
            foreach($finalServicesArr as $tmpKeyForEntService){
                $finalEnterpriseServicesArr = $tmpKeyForEntService;
            }        

            $finalEnterpriseGroupServicesArr = array();
            foreach($finalGroupServicesArr as $tmpKeyForEntGroupService){
                $finalEnterpriseGroupServicesArr = $tmpKeyForEntGroupService;
            }  

            $finalEnterpriseVirtualServicesArr = array();
            foreach($finalVirtualServicesArr as $tmpKeyForEntVirtualService){
                $finalEnterpriseVirtualServicesArr = $tmpKeyForEntVirtualService;
            }
            //End code            
             
            
            //Testing Only
            //echo "<pre>LicenseServiceArrSPBise - ";
            //print_r($licenseServiceArrSPBise);
            $tmpArrForUsedTrunck = array();
            foreach($licenseServiceArrSPBise as $licenseSPName => $servicesRow)
            {
                foreach($servicesRow as $serKey => $serviceTmpName)
                {
                    $trunckUsedCount = ($tmpServiceListOnUser[$serviceTmpName] != "") ? $tmpServiceListOnUser[$serviceTmpName] : "0";
                    $tmpArrForUsedTrunck[$licenseSPName][$serviceTmpName] = $tmpServiceListOnUser[$serviceTmpName];
                }                
            }
            
            //echo "<pre>TotalproductCountArr Smile - ";
            //print_r($totalproductCountArr);
           
            
            //echo "<br />TotalConsumedProductServiceArr - ";
            //print_r($totalConsumedProductServiceArr);
            //die();
            $context = "system";
        }
    }    
?>
<?php
    if($context == "enterprise")
    {
 ?>
 
 <style>
 .servicePackAlign ul{ width: 52%;
    margin: auto !important;
    margin-top:80px !important;
    margin-bottom:25px !important;
	border: none !important;
}
 
table.tablesorter tbody td {
	background-color: transparent !important;
}
#enterPriseSubscribeDetailedDiv table tbody tr,#enterPriseGroupDetailedDiv table tbody tr,#enterPriseVirtualServiceDetailedDiv table tbody tr{background-color: #eef0f3;}
#enterPriseUserServiceDetailedDiv table tbody tr td{background-color: #DDDFE1 !important;}
/*enterprise summary usage */
#enterPriseSubscribeDiv table tbody tr,#enterPriseGroupDiv table tbody tr, #enterPriseVirtualServiceDiv table tbody tr{background-color: #eef0f3;}
 </style>
   
<script>    
   // selectBroadworkUsage('entLevelDetailedUsage');   
    selectBroadworkUsage('enterPriseLevelsummaryUsage');
</script>
        
        <div id="enterPriseLevelDiv">
            
                    <div class="row" style="width:830px;margin: 0 auto;">
                        <div class="from-group" style="margin-top:25px;">
                            <label class="labelText" for="">Date Collected: <?php echo date("Y.m.d H:i:s", filemtime($_SESSION['licenseZipFileNameWithPath']));  ?></label>
                        </div>  
                    </div>
                
                    <div class="row">   
                        <div class="">
                            <div class="col-md-12" style="margin-top:25px;">
				<div class="icons-div userAdd-icons-div">
                                    <ul style="" class="feature-list">
                                        <li class="changeOnMouseHover">
                                            <div class="adminUserMhover" id="" onclick="selectBroadworkUsage('enterPriseLevelsummaryUsage')">
                                                <div id="EntsummaryImg1" style="background-image: url(images/NewIcon/summary_usage_over.png); color: rgb(255, 178, 0);">Summary Usage</div>                
                                            </div>
                                        </li>
                                        <li>
                                            <div class="adminUserMhover" id="" onclick="selectBroadworkUsage('entLevelDetailedUsage')">
                                                <div id="EntdetailedImg2" style="background-image: url(images/NewIcon/ent_detailed_usage_rest.png); color: rgb(110, 160, 220);">Detailed Usage</div>
                                            </div>
                                        </li>
                                    </ul>
                                    <input type="hidden" name="superUser" id="superUser" value="1">
                                </div>
                            </div>
                        </div>
                    </div>
                        
                    <div id="mainPurchageProductDiv">                             
                        <div class="row">   
                            <div class="col-md-12" style="margin-top:25px;">
				<div class="form-group">
                                    <div class="adminUserText" id="sitesubBanner"><?php echo $spID ;?></div>
    				</div>
                            </div>
                        </div> 
                        <!--purchase product ui start  -->   
                        <div id="enterprieseContextSummaryUsage">
                            <div class="row uiModalAccordian form-group" style="display: block;">
                                
                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <div id="csvUrlForSummaryUsageDiv" style="margin-right:15px !important;">                                    
                                            <img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" onclick="location.href='serviceLicense/downloadLicenseReport.php?context=<?php echo $context; ?>&enterprise=<?php echo $spID; ?>&report=Summary' "/>
                                                <br>
                                                <span>Download<br>CSV</span>                                       
                                                <div id='MessageHolder'></div>
                                                <a href="#" id="testAnchor"></a>
                                        </div>
                                    </div>   
                               </div>
                                
                                <div class="">
                                    <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                        <div class="panel-heading" role="tab" id="">
                                            <h4 class="panel-title">
                                                <a id="" class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" aria-expanded="true" href="#enterPriseSubscribeDiv" aria-controls="enterPriseSubscribeDiv">Subscriber Licenses
                                                </a>
                                            </h4>
                                        </div>

                                        <div id="enterPriseSubscribeDiv" class="collapsideDivAdd panel-collapse collapse in" role="tabpanel" aria-labelledby="enterPriseSubscribeDiv">
                                            <div class="">
                                                <!--start content type here --> 
                                                <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct enterpriseSubscribe" id="">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:50%">Type</th>
                                                            <th style="width:50%">Used</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        <tr>
                                                            <td>User License</td>
                                                            <td><?php echo $userLicenseCount;  ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Virtual Users</td>
                                                            <td><?php echo $virtualLicenseCount;  ?></td>
                                                        </tr>
                                                        <tr>
                                                           <td>Trunk Group Users</td>
                                                           <td><?php echo $trunkUserLicenseCount;  ?></td> 
                                                       </tr>
                                                        <tr>
                                                            <td>Group License</td>
                                                            <td><?php echo $groupLicenseCount; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            <!--end content type start here -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                         <?php                   
                            $_SESSION['csvSummaryUsageData']   = "";
                            $_SESSION['csvSummaryUsageData']  .= "\n Subscriber Licenses \n";
                            $_SESSION['csvSummaryUsageData'] .= "Type,  Purchased \n";
                            $_SESSION['csvSummaryUsageData'] .= "User License,  $userLicenseCount \n";
                            $_SESSION['csvSummaryUsageData'] .= "Virtual Users, $virtualLicenseCount \n";
                            $_SESSION['csvSummaryUsageData'] .= "Trunk Group Users, $trunkUserLicenseCount \n";
                            $_SESSION['csvSummaryUsageData'] .= "Group License, $groupLicenseCount \n";
                        ?>

                        <!--group services -->
                            <div class="row uiModalAccordian form-group" style="display: block;">
                                
                                
                                <div class="">
                                    <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                        <div class="panel-heading" role="tab" id="">
                                            <h4 class="panel-title">
                                                <a id="" class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" aria-expanded="true" href="#enterPriseGroupDiv" aria-controls="enterPriseGroupDiv">Group Service Licenses
                                                </a>
                                            </h4>
                                        </div>

                                        <div id="enterPriseGroupDiv" class="collapsideDivAdd panel-collapse collapse in" role="tabpanel" aria-labelledby="enterPriseGroupDiv">
                                            <div class="">
                                                <!--start content type here --> 
                                                <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct enterpriseSubscribe" id="">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:50%">Service License Pack</th>
                                                            <th style="width:50%">Used</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>                                                   
                                                    <?php                                               
                                                        ksort($finalEnterpriseGroupServicesArr);
                                                        $tmpGSPArrForEntLevelSmry = array();
                                                        $licenseGroupSumData      = "";
                                                        if(!empty($finalEnterpriseGroupServicesArr) && count($finalEnterpriseGroupServicesArr) > 0)
                                                        {                                                            
                                                            foreach($finalEnterpriseGroupServicesArr as $licenseGroupServicePackName => $licenseGroupServicePackRow)
                                                            {                                                                 
                                                                if($licenseGroupServicePackName !="")
                                                                {
                                                                    $tmpGSPArrForEntLevelSmry[] = $licenseGroupServicePackName;
                                                                    $totalLicenseGroupServiceUsed = max($licenseGroupServicePackRow);
                                                                    echo "<tr>";
                                                                    echo "<td>$licenseGroupServicePackName</td>";
                                                                    echo "<td>$totalLicenseGroupServiceUsed &nbsp;</td>";
                                                                    echo "</tr>";
                                                                    $licenseGroupSumData .= "$licenseGroupServicePackName, $totalLicenseGroupServiceUsed \n ";
                                                                }
                                                            }
                                                        }
                                                        foreach($licenseArr['licenseServiceArrLevelBiseDup']['GROUP'] as $tmpGroupSpName => $tmpValNew1){
                                                            if(!in_array($tmpGroupSpName, $tmpGSPArrForEntLevelSmry)){
                                                                echo "<tr>";
                                                                echo "<td>$tmpGroupSpName</td>";
                                                                echo "<td>0 &nbsp;</td>";
                                                                echo "</tr>";
                                                                $licenseGroupSumData .= "$tmpGroupSpName, 0 \n ";
                                                            }
                                                        }
                                                    ?> 
                                                </tbody>
                                                </table>
                                                <!--end content type start here -->
                                                 <?php                                    
                                                        $_SESSION['csvSummaryUsageData'] .= "\n Group Service Licenses \n";
                                                        $_SESSION['csvSummaryUsageData'] .= "Service License Pack, Used  \n";
                                                        $_SESSION['csvSummaryUsageData'] .= $licenseGroupSumData;
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--end group services --> 
                    <!--Vertual sevrvice license -->
                            <div class="row uiModalAccordian form-group" style="display: block;">
                                <div class="">
                                    <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                        <div class="panel-heading" role="tab" id="">
                                            <h4 class="panel-title">
                                                <a id="" class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" aria-expanded="true" href="#enterPriseVirtualServiceDiv" aria-controls="enterPriseVirtualServiceDiv">Virtual Service Licenses
                                                </a>
                                            </h4>
                                        </div>

                                        <div id="enterPriseVirtualServiceDiv" class="collapsideDivAdd panel-collapse collapse in" role="tabpanel" aria-labelledby="enterPriseVirtualServiceDiv">
                                            <div class="">
                                            <!--start content type here --> 
                                                <table  class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct enterpriseSubscribe" id="">
                                                    <thead>
                                                        <tr>
                                                            <th style="width:50%">Service License Pack</th>
                                                            <th style="width:50%">Used</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>                                                     
                                                    <?php                                               
                                                        ksort($finalEnterpriseVirtualServicesArr);
                                                        $tmpVSPArrForEntLevelSmry = array();
                                                        $licenseVirtualSumData    = "";
                                                        if(!empty($finalEnterpriseVirtualServicesArr) && count($finalEnterpriseVirtualServicesArr) > 0)
                                                        {
                                                            foreach($finalEnterpriseVirtualServicesArr as $licenseVirtualServicePackName => $licenseVirtualServicePackRow)
                                                            {       
                                                                if($licenseVirtualServicePackName !="")
                                                                {
                                                                    $tmpVSPArrForEntLevelSmry[] = $licenseVirtualServicePackName;
                                                                    $totalLicenseVirtualServiceUsed = max($licenseVirtualServicePackRow);
                                                                    echo "<tr>";
                                                                    echo "<td>$licenseVirtualServicePackName</td>";
                                                                    echo "<td>$totalLicenseVirtualServiceUsed &nbsp;</td>";
                                                                    echo "</tr>";
                                                                    $licenseVirtualSumData .= "$licenseVirtualServicePackName, $totalLicenseVirtualServiceUsed \n ";
                                                                }
                                                            }
                                                        }
                                                        foreach($licenseArr['licenseServiceArrLevelBiseDup']['VIRTUAL'] as $tmpVirtualSpName => $tmpValNew1){
                                                                if(!in_array($tmpVirtualSpName, $tmpVSPArrForEntLevelSmry)){
                                                                    echo "<tr>";
                                                                    echo "<td>$tmpVirtualSpName &nbsp;</td>";
                                                                    echo "<td>0 &nbsp;</td>";
                                                                    echo "</tr>";
                                                                    $licenseVirtualSumData .= "$tmpVirtualSpName, 0 \n ";
                                                                }
                                                        }
                                                   ?>
                                                </tbody>
                                                </table>
                                                <!--end content type start here -->
                                                <?php                                    
                                                        $_SESSION['csvSummaryUsageData'] .= "\n VirtualService Licenses \n";
                                                        $_SESSION['csvSummaryUsageData'] .= "Service License Pack, Used  \n";
                                                        $_SESSION['csvSummaryUsageData'] .= $licenseVirtualSumData;
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--end vertual service license-->
                            <!--user service license -->

                            <div class="row uiModalAccordian form-group" style="display: block;">
                                <div class="">
                                    <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                        <div class="panel-heading" role="tab" id="">
                                            <h4 class="panel-title">
                                                <a id="" class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" aria-expanded="true" href="#enterPriseUserServiceDiv" aria-controls="enterPriseUserServiceDiv">User Service Licenses
                                                </a>
                                            </h4>
                                        </div>

                                        <div id="enterPriseUserServiceDiv" class="collapsideDivAdd panel-collapse collapse in" role="tabpanel" aria-labelledby="enterPriseUserServiceDiv">
                                            <div class="">
                                            <!--start content type here --> 
                                                <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct" id="">
                                                    <thead>
                                                        <tr>
                                                            <th class="productNameWidth">Service License Pack</th>
                                                            <th class="productServicePackWidth">Used</th>
                                                            <th class="productServicePackWidth">Used (Hosted)</th>
                                                            <th class="productServicePackWidth">Used (Trunk)</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>                                                     
                                                        <?php                                               
                                                            ksort($totalSPCountArr); 
                                                            $tmpUSPArrForEntLevelSmry = array();
                                                            $licenseUserSumData       = "";
                                                            if(!empty($totalSPCountArr) && count($totalSPCountArr) > 0)
                                                            {
                                                                foreach($totalSPCountArr as $licenseUserServicePackName => $licenseUserServicePackCount)
                                                                {       
                                                                    if($licenseUserServicePackName !="")
                                                                    {                           
                                                                        $tmpUSPArrForEntLevelSmry[] = $licenseUserServicePackName;
                                                                        echo "<tr class='' style='background-color: #DDDFE1 !important;'>";
                                                                        echo "<td class='productNameWidth'>$licenseUserServicePackName</td>";
                                                                        echo "<td class='productServicePackWidth'>$licenseUserServicePackCount</td>";
                                                                        echo "<td class='productServicePackWidth'>$licenseUserServicePackCount</td>";
                                                                        echo "<td class='productServicePackWidth'>0 &nbsp;</td>";
                                                                        echo "</tr>";
                                                                        $licenseUserSumData .= "$licenseUserServicePackName, $licenseUserServicePackCount,  $licenseUserServicePackCount, 0 \n ";
                                                                    }
                                                                }
                                                            }
                                                            foreach($licenseArr['licenseServiceArrLevelBiseDup']['USER'] as $tmpUserSpName => $tmpValNew1){
                                                                if(!in_array($tmpUserSpName, $tmpUSPArrForEntLevelSmry)){
                                                                        echo "<tr style='background-color: #DDDFE1 !important;'>";
                                                                        echo "<td class='productNameWidth'>$tmpUserSpName &nbsp;</td>";
                                                                        echo "<td class='productServicePackWidth'>0 &nbsp;</td>";
                                                                        echo "<td class='productServicePackWidth'>0 &nbsp;</td>";
                                                                        echo "<td class='productServicePackWidth'>0 &nbsp;</td>";
                                                                        echo "</tr>";
                                                                        $licenseUserSumData .= "$tmpUserSpName, 0,  0, 0 \n ";
                                                                }
                                                            }
                                                            
                                                       ?>                                                       
                                                    </tbody>
                                                </table>
                                                <?php                                    
                                                        $_SESSION['csvSummaryUsageData'] .= "\n User Service Licenses \n";
                                                        $_SESSION['csvSummaryUsageData'] .= "Service License Pack, Used, Used (Hosted), Used (Trunk)  \n";
                                                        $_SESSION['csvSummaryUsageData'] .= $licenseUserSumData;
                                                ?>
                                                <!--end content type start here -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!--end user service license -->
                        </div>
                    </div>
                    
                    <div id="mainPurchageProductDetailedDiv">
                        
                        
                        <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <div id="csvUrlForSummaryUsageDiv">                                    
                                            <img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" onclick="location.href='serviceLicense/downloadLicenseReport.php?context=<?php echo $context; ?>&enterprise=<?php echo $spID; ?>&report=Detailed' "/>
                                                <br>
                                                <span>Download<br>CSV</span>                                       
                                                <div id='MessageHolder'></div>
                                                <a href="#" id="testAnchor"></a>
                                        </div>
                                    </div>   
                       </div>
                
                        <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="headEnterPriseSubscribeDetailed" class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse" aria-expanded="false" href="#enterPriseSubscribeDetailedDiv" aria-controls="enterPriseSubscribeDetailedDiv">Subscriber Licenses
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="enterPriseSubscribeDetailedDiv" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="enterPriseSubscribeDetailedDiv" aria-expanded="false" style="height: 0px;">
                                        <div class="">
                                            <!--start content type here --> 
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct enterpriseSubscribe" id="">
                                                <thead>
                                                    <tr>
                                                        <th style="width:50%">Type</th>
                                                        <th style="width:50%">Purchased</th>
                                                    </tr>
                                                </thead>
                                                <tbody>                                                    
                                                    <tr>
                                                        <td>User License</td>
                                                        <td><?php echo $userLicenseCount;  ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Virtual Users</td>
                                                        <td><?php echo $virtualLicenseCount;  ?></td>
                                                    </tr>
                                                    <tr>
                                                      <td>Trunk Group Users</td>
                                                       <td><?php echo $trunkUserLicenseCount;  ?></td> 
                                                   </tr>
                                                    <!--end error show -->
                                                    <tr>
                                                        <td>Group License</td>
                                                        <td><?php echo $groupLicenseCount; ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        <!--end content type start here -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <?php       
                            $_SESSION['csvDetailUsageData']  = "";
                            $_SESSION['csvDetailUsageData'] .= "\n Subscriber Licenses \n";
                            $_SESSION['csvDetailUsageData'] .= "Type,  Purchased \n";
                            $_SESSION['csvDetailUsageData'] .= "User License,  $userLicenseCount \n";
                            $_SESSION['csvDetailUsageData'] .= "Virtual Users, $virtualLicenseCount \n";
                            $_SESSION['csvDetailUsageData'] .= "Trunk Group Users, $trunkUserLicenseCount \n";
                            $_SESSION['csvDetailUsageData'] .= "Group License, $groupLicenseCount \n";
                        ?>

                        <!--group services -->
                        <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="headEnterPriseGroupDetailed" class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse" aria-expanded="false" href="#enterPriseGroupDetailedDiv" aria-controls="enterPriseGroupDetailedDiv">Group Service Licenses
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="enterPriseGroupDetailedDiv" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="enterPriseGroupDetailedDiv" aria-expanded="false" style="height: 0px;">
                                        <div class="">
                                            <!--start content type here --> 
 
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct enterpriseSubscribe" id="">
 
                                                <thead>
                                                    <tr style="border-right: none;">
                                                        <th style="width: 0%;border: none;">Service License Pack</th>
                                                       <!-- <th style="width:59%">Service</th> -->
                                                        <th style="width: 60%;padding-bottom: 0px;">
                                                            <table class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" style=" border: none;">
                                                                <thead style=" border: none;">
                                                                    <tr class="" style=" border-bottom: none; margin-bottom: 0px;">                                      
                                                                        <th class="trunctingServiceTh" style="">Services</th>
                                                                        <th class="GroupSummaryLiceseTh" style="width:9% !important;">Used</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>                                                   
                                                    <?php                                               
                                                        ksort($finalEnterpriseGroupServicesArr);
                                                        $tmpGSPArrForEntLevelDtl    = array();
                                                        $tmpGSrvceArrForEntLevelDtl = array();
                                                        $licenseGroupData = "";
                                                        if(!empty($finalEnterpriseGroupServicesArr) && count($finalEnterpriseGroupServicesArr) > 0)
                                                        {
                                                            foreach($finalEnterpriseGroupServicesArr as $licenseGroupServicePackName => $licenseGroupServicePackRow)
                                                            {
                                                                
                                                                if($licenseGroupServicePackName != ""){
                                                                    $tmpGSPArrForEntLevelDtl[] = $licenseGroupServicePackName;
                                                                    echo "<tr class=''><td class='trunctingServiceTh' style='font-size: 14px !important;font-weight: 900;width:29% !important;'>$licenseGroupServicePackName</td>";
                                                                    echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;";
                                                                    
                                                                    if(!empty($licenseGroupServicePackRow) && count($licenseGroupServicePackRow) > 0)
                                                                    {
                                                                        echo "<table class='productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese'><tbody>";
                                                                        $totalLicenseGroupServiceUsed = max($licenseGroupServicePackRow);
                                                                        echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>Total</td>
                                                                                    <td class='GroupSummaryLiceseTh'>$totalLicenseGroupServiceUsed</td>
                                                                              </tr>";
                                                                        
                                                                        $licenseGroupData .= "$licenseGroupServicePackName, Total,  $totalLicenseGroupServiceUsed \n ";
                                                                        
                                                                        foreach($licenseGroupServicePackRow as $licenseGroupServiceName => $licenseGroupServiceUsed)
                                                                        {
                                                                            $tmpGSrvceArrForEntLevelDtl[] = $licenseGroupServiceName;
                                                                            echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>$licenseGroupServiceName</td>
                                                                                    <td class='GroupSummaryLiceseTh'>$licenseGroupServiceUsed</td>
                                                                                  </tr>";
                                                                            
                                                                            $licenseGroupData .= "   , $licenseGroupServiceName,  $licenseGroupServiceUsed \n ";
                                                                        }                                                                    
                                                                        echo "</tbody></table>";                                                                    
                                                                    }                                                                
                                                                    echo "</td></tr>";
                                                                }
                                                            }
                                                        }
                                                        foreach($licenseArr['licenseServiceArrLevelBiseDup']['GROUP'] as $tmpGroupSpName => $tmpValNew1){
                                                            if(!in_array($tmpGroupSpName, $tmpGSPArrForEntLevelDtl)){
                                                                    echo "<tr>";
                                                                    echo "<td class='trunctingServiceTh' style='font-size: 14px !important;font-weight: 900;width:29% !important;'>$tmpGroupSpName &nbsp;</td>";
                                                                    echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;
                                                                          <table class='productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese'><tbody>";
                                                                          
                                                                           echo "<tr class=''>                                                                        
                                                                                                    <td class='trunctingServiceTh'>Total</td>
                                                                                                    <td class='GroupSummaryLiceseTh'>0</td>
                                                                                 </tr>";
                                                                           $licenseGroupData .= "$tmpGroupSpName, Total,  0 \n ";
                                                                           foreach($tmpValNew1 as $serviceNameTmp1)
                                                                            {
                                                                                if(!in_array($serviceNameTmp1, $tmpGSrvceArrForEntLevelDtl))
                                                                                {
                                                                                    echo "<tr class=''>                                                                        
                                                                                                    <td class='trunctingServiceTh'>$serviceNameTmp1</td>
                                                                                                    <td class='GroupSummaryLiceseTh'>0</td>
                                                                                         </tr>";
                                                                                    $licenseGroupData .= " , $serviceNameTmp1 , 0 \n";
                                                                                }
                                                                            }
                                                                    echo "</tbody></table>
                                                                            </td>";
                                                                    echo "</tr>";
                                                            }
                                                        }
                                                        ?> 
                                                </tbody>
                                            </table>
                                            
                                        <?php                                    
                                            $_SESSION['csvDetailUsageData'] .= "\n Group Service Licenses \n";
                                            $_SESSION['csvDetailUsageData'] .= "Service License Pack, Services, Used  \n";
                                            $_SESSION['csvDetailUsageData'] .= $licenseGroupData;
                                        ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end group services --> 
                        <!--Vertual sevrvice license -->
                        <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="headEntVirSerDetail" class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse" aria-expanded="false" href="#enterPriseVirtualServiceDetailedDiv" aria-controls="enterPriseVirtualServiceDetailedDiv">Virtual Service Licenses
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="enterPriseVirtualServiceDetailedDiv" class="Surendra collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="enterPriseVirtualServiceDetailedDiv" aria-expanded="false" style="height: 0px;">
                                        <div class="">
                                        <!--start content type here --> 
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct enterpriseSubscribe" id="">
                                                <thead>
                                                    <tr style="border-right: none;">
                                                        <th style="width: 0%;border: none;">Service License Pack</th>
                                                       <!-- <th style="width:59%">Service</th> -->
                                                        <th style="width: 60%;padding-bottom: 0px;">
                                                            <table class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" style=" border: none;">
                                                                <thead style=" border: none;">
                                                                    <tr class="" style=" border-bottom: none; margin-bottom: 0px;">                                      
                                                                        <th class="trunctingServiceTh" style="">Services</th>
                                                                        <th class="GroupSummaryLiceseTh" style="width: 9% !important;">Used</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php                                               
                                                        ksort($finalEnterpriseVirtualServicesArr);
                                                        $tmpVSPArrForEntLevelDtl = array();
                                                        $licenseVirtualData = "";
                                                        if(!empty($finalEnterpriseVirtualServicesArr) && count($finalEnterpriseVirtualServicesArr) > 0)
                                                        {
                                                            foreach($finalEnterpriseVirtualServicesArr as $licenseVirtualServicePackName => $licenseVirtualServicePackRow)
                                                            {         
                                                                if($licenseVirtualServicePackName != ""){
                                                                    $tmpVSPArrForEntLevelDtl[] = $licenseVirtualServicePackName;
                                                                    echo "<tr class=''><td class='trunctingServiceTh' style='font-size: 14px !important;font-weight: 900;width:29% !important'>$licenseVirtualServicePackName</td>";
                                                                    echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;";
                                                                    
                                                                    
                                                                    
                                                                    if(!empty($licenseVirtualServicePackRow) && count($licenseVirtualServicePackRow) > 0)
                                                                    {                                                                        
                                                                        echo "<table class='productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese' style='margin-top: 0px;'><tbody>";
                                                                        $totalLicenseVirtualServiceUsed = max($licenseVirtualServicePackRow);
                                                                        echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>Total</td>
                                                                                    <td class='GroupSummaryLiceseTh'>$totalLicenseVirtualServiceUsed</td>
                                                                              </tr>";
                                                                        
                                                                        $licenseVirtualData .= "$licenseVirtualServicePackName, Total,  $totalLicenseVirtualServiceUsed \n ";
                                                                        
                                                                        foreach($licenseVirtualServicePackRow as $licenseVirtualServiceName => $licenseVirtualServiceUsed)
                                                                        {
                                                                            $tmpVSrvceArrForEntLevelDtl[] = $licenseVirtualServiceName;
                                                                            echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>$licenseVirtualServiceName</td>
                                                                                    <td class='GroupSummaryLiceseTh'>$licenseVirtualServiceUsed</td>
                                                                                  </tr>";
                                                                            
                                                                            $licenseVirtualData .= "  , $licenseVirtualServiceName,  $licenseVirtualServiceUsed \n ";
                                                                        }                                                                    
                                                                        echo "</tbody></table>";                                                                    
                                                                    }                                                                
                                                                    echo "</td></tr>";
                                                                }
                                                            }
                                                        }
                                                        foreach($licenseArr['licenseServiceArrLevelBiseDup']['VIRTUAL'] as $tmpVirtualSpName => $tmpValNew2 ){
                                                            if(!in_array($tmpVirtualSpName, $tmpVSPArrForEntLevelDtl)){
                                                                    echo "<tr>";
                                                                    echo "<td class='trunctingServiceTh' style='font-size: 14px !important;font-weight: 900;width:29% !important;'>$tmpVirtualSpName &nbsp;</td>";
                                                                    echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;
                                                                          <table class='productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese'><tbody>";
                                                                          
                                                                           echo "<tr class=''>                                                                        
                                                                                                    <td class='trunctingServiceTh'>Total</td>
                                                                                                    <td class='GroupSummaryLiceseTh'>0</td>
                                                                                  </tr>";
                                                                           $licenseVirtualData .= "$tmpVirtualSpName, Total,  0 \n ";
                                                                           
                                                                           foreach($tmpValNew2 as $serviceNameTmp2)
                                                                            {
                                                                                if(!in_array($serviceNameTmp2, $tmpVSrvceArrForEntLevelDtl))
                                                                                {
                                                                                    echo "<tr class=''>                                                                        
                                                                                                    <td class='trunctingServiceTh'>$serviceNameTmp2</td>
                                                                                                    <td class='GroupSummaryLiceseTh'>0</td>
                                                                                         </tr>";
                                                                                    $licenseVirtualData .= "  , $serviceNameTmp2,  0 \n ";
                                                                                }
                                                                            }
                                                                    echo "</tbody></table>
                                                                            </td>";
                                                                    echo "</tr>";
                                                            }
                                                        }
                                                        ?>
                                                </tbody>
                                            </table>
                                            <?php                                    
                                                $_SESSION['csvDetailUsageData'] .= "\n Virtual Service Licenses \n";
                                                $_SESSION['csvDetailUsageData'] .= "Service License Pack, Services, Used  \n";
                                                $_SESSION['csvDetailUsageData'] .= $licenseVirtualData;
                                            ?>
                                            <!--end content type start here -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end vertual service license-->
                            <!--user service license -->

                        <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="" class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" aria-expanded="true" href="#enterPriseUserServiceDetailedDiv"  aria-controls="enterPriseUserServiceDetailedDiv">User Service Licenses
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="enterPriseUserServiceDetailedDiv" class="collapsideDivAdd panel-collapse collapse in" role="tabpanel" aria-labelledby="enterPriseUserServiceDetailedDiv">
                                        <div class="">
                                        <!--start content type here --> 
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct divUserServiceGroupSummaryLicese" id="">
                                                <thead>
                                                    <tr>
                                                        <th class="trunctingServiceTh" style="width: 0%;border: none;">Service License Pack</th>
                                                            <th class="productQualityWidthTableTd" style="width: 100%;margin: 0 auto;">
								<table class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" id="" style="border: none;margin-bottom: -9px;">
                                                                    <thead>
									<tr class="" style=" border-bottom: none; margin-bottom: 0px;">  
                                                                            <th class="trunctingServiceTh" style='width: 170px !important;'>Service</th>
                                                                            <th class="productServicePackWidth" style='width: 103px !important;'>Used</th>
                                                                            <th class="productServicePackWidth" style="width: 106px !important;">Used (Hosted)</th>
                                                                            <th class="productServicePackWidth" style="width: 95px !important;">Used (Trunk)</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </th>
                                                            
                                                    </tr>
                                              </thead>
                                                <tbody>                                                    
                                                        <?php                                                        
                                                        ksort($finalEnterpriseServicesArr);
                                                        $tmpUSPArrForEntLevelDtl = array();
                                                        $licenseUserData         = "";
                                                        if(!empty($finalEnterpriseServicesArr) && count($finalEnterpriseServicesArr) > 0)
                                                        {
                                                            foreach($finalEnterpriseServicesArr as $licenseServicePackName => $licenseServicePackRow)
                                                            {
                                                                $tmpUSPArrForEntLevelDtl[] = $licenseServicePackName;
                                                                echo "<tr class='' style='background-color: #DDDFE1 !important;'><td class='trunctingServiceTh' style='font-size: 14px !important;font-weight: 900;border-right: 1px solid #c0c4c7 !important;width: 27% !important;'>$licenseServicePackName</td>";
                                                                echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;";
                                                                if(!empty($licenseServicePackRow) && count($licenseServicePackRow) > 0)
                                                                {
                                                                    $totalServiceBasedOnSP = $totalSPCountArr[$licenseServicePackName];
                                                                    echo "<table class='productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese'><tbody>";
                                                                    
                                                                    echo "<tr class=''>                                                                        
                                                                                <td class='trunctingServiceTh'>Total</td>
                                                                                <td class='productServicePackWidth'>$totalServiceBasedOnSP</td>
                                                                                <td class='productServicePackWidth'>$totalServiceBasedOnSP</td>
                                                                                <td class='productServicePackWidth'>0</td>
                                                                              </tr>";  
                                                                    
                                                                    $licenseUserData .= "$licenseServicePackName, Total,  $totalServiceBasedOnSP, $totalServiceBasedOnSP, 0 \n ";
                                                                    
                                                                    foreach($licenseServicePackRow as $licenseServiceName => $licenseServiceUsed)
                                                                    {
                                                                        $tmpUSrvceArrForEntLevelDtl[] = $licenseServiceName;
                                                                        echo "<tr class=''>                                                                        
                                                                                <td class='trunctingServiceTh'>$licenseServiceName</td>
                                                                                <td class='productServicePackWidth'>$licenseServiceUsed</td>
                                                                                <td class='productServicePackWidth'>$licenseServiceUsed</td>
                                                                                <td class='productServicePackWidth'>0</td>
                                                                              </tr>";
                                                                        $licenseUserData .= " , $licenseServiceName,  $licenseServiceUsed, $licenseServiceUsed, 0 \n ";
                                                                    }                                                                    
                                                                    echo "</tbody></table>";                                                                    
                                                                }                                                                
                                                                echo "</td></tr>";
                                                            }
                                                        }
                                                        foreach($licenseArr['licenseServiceArrLevelBiseDup']['USER'] as $tmpUserSpName => $tmpValNew3){
                                                            if(!in_array($tmpUserSpName, $tmpUSPArrForEntLevelDtl)){
                                                                    echo "<tr style='background-color: #DDDFE1 !important;'>";
                                                                    echo "<td class='trunctingServiceTh' style='font-size: 14px !important;font-weight: 900;border-right: 1px solid #c0c4c7 !important;width:27% !important;'>$tmpUserSpName &nbsp;</td>";
                                                                    echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;
                                                                          <table class='productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese'><tbody>";
                                                                          
                                                                            foreach($tmpValNew3 as $serviceNameTmp3)
                                                                            {
                                                                                if(!in_array($serviceNameTmp3, $tmpUSrvceArrForEntLevelDtl))
                                                                                {
                                                                                    echo "<tr class=''>                                                                        
                                                                                                    <td class='trunctingServiceTh'>Total</td>
                                                                                                    <td class='productServicePackWidth'>0</td>
                                                                                                    <td class='productServicePackWidth'>0</td>
                                                                                                    <td class='productServicePackWidth'>0</td>
                                                                                         </tr>";
                                                                                    $licenseUserData .= " $tmpUserSpName , Total,  0, 0, 0 \n ";
                                                                                    echo "<tr class=''>                                                                        
                                                                                                    <td class='trunctingServiceTh'>$serviceNameTmp3</td>
                                                                                                    <td class='productServicePackWidth'>0</td>
                                                                                                    <td class='productServicePackWidth'>0</td>
                                                                                                    <td class='productServicePackWidth'>0</td>
                                                                                         </tr>";
                                                                                    $licenseUserData .= " $tmpUserSpName , $serviceNameTmp3,  0, 0, 0 \n ";
                                                                                }
                                                                            }
                                                                    echo "</tbody></table>
                                                                            </td>";
                                                                    echo "</tr>";
                                                            }
                                                        }
                                                        ?>                                                        
                                                </tbody>
                                            </table>
                                        
                                        <?php                                    
                                                $_SESSION['csvDetailUsageData'] .= "\n User Service Licenses \n";
                                                $_SESSION['csvDetailUsageData'] .= "Service License Pack, Services, Used, Used (Hosted), Used (Trunk)  \n";
                                                $_SESSION['csvDetailUsageData'] .= $licenseUserData;
                                        ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end user service license -->                        
                    </div><!-- enterprise level detailed list end  -->
                </div> 
 <?php 
    }
    
    if($context == "system")
    {        
 ?>
                               
                    <div class="row" style="width:830px;margin: 0 auto;">
                        <div class="from-group" style="margin-top:25px;">
                            <label class="labelText" for="">Date Collected:<?php echo date("Y.m.d H:i:s", filemtime($_SESSION['licenseZipFileNameWithPath'])); ?></label>
                        </div>  
                    </div>
                     
                    <div class="row">   
                        <div class="">
                            <div class="col-md-12">
				<div class="icons-div userAdd-icons-div">
                                    <ul style="" class="feature-list">
                                        <li class="changeOnMouseHover">
                                            <div class="adminUserMhover" id="superUserDiv" onclick="selectBroadworkUsage('summaryUsage')">
                                                <div id="summaryImg1" style="background-image: url(images/NewIcon/summary_usage_over.png); color: rgb(255, 178, 0);">Summary Report</div>                
                                            </div>
                                        </li>
                                        <li>
                                            <div class="adminUserMhover" id="enterpriseAdminDiv" onclick="selectBroadworkUsage('detailedUsage')">
                                                <div id="detailedImg2" style="background-image: url(images/NewIcon/report_alert_rest.png); color: rgb(110, 160, 220);">Alerts Report</div>
                                            </div>
                                        </li>
                                        
                                    </ul>
                                    
                                    <input type="hidden" name="superUser" id="superUser" value="1">
                                </div>
                            </div>
                        </div>
                    </div>


                    
                            
               
                    <div id="SystemlevelPurchageProductDiv"> 
                         
                        <div class="divBeforeUl servicePackAlign">
                            <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
                		<li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active colorTagWarning" role="tab" tabindex="0" aria-controls="main_ServiceLicense" aria-labelledby="serviceLicenses" aria-selected="true">
                                    <div id="serviceLicensesddd"   class="tabs ui-tabs-anchor">Warning Threshold (<?php echo $warningThreshold;  ?>%)</div>
                                </li>
		                <li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="phone_ProfilesTabddd" aria-labelledby="phoneProfilesTabfff" aria-selected="false">
                                    <div id="phoneProfilesTabdddd" class="tabs ui-tabs-anchor">Attention Threshold (<?php echo $attentionThreshold;  ?>%)</div>
                                </li>
                            </ul>
                        </div>                  
  
                        
                        
                    <div id="divDetailedUsage">
                        
                        <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div id="csvUrlForSummaryUsageDiv">                              
                                <img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" onclick="location.href='serviceLicense/downloadLicenseReport.php?context=<?php echo $context; ?>&report=SummaryUsage' "/>
                                    <br>
                                    <span>Download<br>CSV</span>                                       
                                    <div id='MessageHolder'></div>
                                    <a href="#" id="testAnchor"></a>
                            </div>
                        </div>   
                        </div>
                        
                        
                     <div id ="divSystemLevelSumaryDetailedCSV">     
                    <!--purchase product ui start  -->   
                        <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    
                                    <?php
                                            $alertProductImgFlag  = "hide";
                                            $alertProductImgHtml  = "";  
                                            if(!empty($licenseArr['licenseProductArr']))
                                            {                                   
                                                        
                                                        foreach($licenseArr['licenseProductArr'] as $productLicenseName => $productLicenseRow)
                                                        {
                                                                $productType = $productLicenseRow['productType'];
                                                                $productQty  = $productLicenseRow['productQty'];
                                                                
                                                                if($productType == "User Package" || $productType == "Trunk Package")
                                                                {
                                                                         if(isset($totalproductCountArr[$productLicenseName])){
                                                                             
                                                                             $consumedLicensed     = $totalproductCountArr[$productLicenseName];
                                                                             
                                                                             if($consumedLicensed >= $productQty){
                                                                                 $consumedLicensed          = $productQty;
                                                                                 $availableProductLicensed  = "0";
                                                                                 $remainPrctge              = "0.0";
                                                                             }else{
                                                                                 $availableProductLicensed  = $productQty - $consumedLicensed;
                                                                                 $remainPrctge              = ($availableProductLicensed * 100 ) / $productQty;    
                                                                                 $remainPrctge              = number_format($remainPrctge, 1);
                                                                             }
                                                                         }else{
                                                                             $consumedLicensed          = "0";
                                                                             $availableProductLicensed  = "0";
                                                                             $remainPrctge              = "0.0";
                                                                         }
                                                                         $rowColorStyle = setServicePackRowColorBasedOnConsumedQunantity($productQty, $consumedLicensed, $warningThreshold, $attentionThreshold);
                                                                         if(stripos($rowColorStyle, "#F3C6CA") !== false || stripos($rowColorStyle, "#F6EBB1") !== false){ 
                                                                                $alertProductImgFlag = "show";
									 }
                                                                         

                                                                }                                                               
                                                                if($productType == "Add-On Package")
                                                                {
                                                                    $consumedLicensed          = "0";
                                                                    $availableProductLicensed  = "0";
                                                                    $remainPrctge              = "0";                                                                    
                                                                                                                                        
                                                                    if(!empty($productLicenseRow['productServiceDetails']))
                                                                    {  
                                                                        $productName = $productLicenseRow['productServiceDetails']['NONE']['0']['productServiceName'];
                                                                    }
                                                                    else
                                                                    {
                                                                        foreach($productLicenseRow['productServicePackDetails'] as $tpKey => $serviceRow)
                                                                        {
                                                                            $productName = $tpKey;
                                                                        }
                                                                    }                                                                    
                                                                    //if(isset($finalEnterpriseServicesArr[$productName]))
                                                                    if(1)
                                                                    {
                                                                        //echo "<br />productServiceDetails New - productServiceDetails";
                                                                        //print_r($productLicenseRow);
                                                                        
                                                                        //echo "<br />finalEnterpriseServicesArr";
                                                                        //print_r($finalEnterpriseServicesArr);
                                                                        
                                                                        $tmpConsumedQty = array();
                                                                        if(count($productLicenseRow['productServiceDetails']['NONE']) > 1)
                                                                        {
                                                                            //echo "<br /> productServiceDetails - 1";
                                                                            $productServiceDetailsArr = $productLicenseRow['productServiceDetails']['NONE'];
                                                                            foreach($productServiceDetailsArr as $serviceDetailsRow1)
                                                                            {               
                                                                                    $productLicenseServiceName1 = $serviceDetailsRow1['productServiceName'];                                                                                        
                                                                                    $productLicenseServiceQty1  = $serviceDetailsRow1['quantity'];
                                                                                    if($productLicenseServiceQty1 == "0")
                                                                                    {

                                                                                    }
                                                                                    else{
                                                                                        $tmpConsumedQty[] = $finalEnterpriseServicesArr[$productLicenseServiceName1][$productLicenseServiceName1];
                                                                                        $consumedLicensed = max($tmpConsumedQty);
                                                                                    }
                                                                            }
                                                                        }  
                                                                        else
                                                                        {                                                                            
                                                                            if(count($productLicenseRow['productServicePackDetails']) > 1)
                                                                            {
                                                                                //echo "<br /> productServicePackDetails - 2";
                                                                                $productServicePackDetailsArr = $productLicenseRow['productServicePackDetails'];
                                                                                foreach($productServicePackDetailsArr as $servicePackDetailsRow1)
                                                                                {               
                                                                                        $productLicenseServicePackName1 = $servicePackDetailsRow1['productServicePackName'];                                                                                        
                                                                                        $productLicenseServicePackQty1  = $servicePackDetailsRow1['quantity'];
                                                                                        if($productLicenseServicePackQty1 == "0")
                                                                                        {

                                                                                        }
                                                                                        else{
                                                                                            $tmpConsumedQty[] = max($finalEnterpriseServicesArr[$productLicenseServicePackName1]);
                                                                                            $consumedLicensed = max($tmpConsumedQty);
                                                                                        }
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                foreach($finalEnterpriseServicesArr[$productName] as $valQty)
                                                                                {
                                                                                     $consumedLicensed = $valQty;
                                                                                }
                                                                            }
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        if($productQty != "0")
                                                                        {                                                                            
                                                                            if($consumedLicensed > $productQty)
                                                                            { 
                                                                                $consumedLicensed = $productQty;
                                                                            }
                                                                            
                                                                            $availableProductLicensed = $productQty - $consumedLicensed;
                                                                            if($availableProductLicensed != "0")
                                                                            {
                                                                                $remainPrctge             = ($availableProductLicensed * 100 ) / $productQty;    
                                                                                $remainPrctge             = number_format($remainPrctge, 1);
                                                                            }
                                                                            else
                                                                            {
                                                                                $availableProductLicensed = "0";
                                                                                $remainPrctge             = "0.0";
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            $availableProductLicensed  = "unlimited";
                                                                            $remainPrctge              = "unlimited";
                                                                        }
                                                                    }      
                                                                    
                                                                    
                                                                    
                                                                    $rowColorStyle = setServicePackRowColorBasedOnConsumedQunantity($productQty, $consumedLicensed, $warningThreshold, $attentionThreshold);
                                                                    if(stripos($rowColorStyle, "#F3C6CA") !== false  || stripos($rowColorStyle, "#F6EBB1") !== false){
                                                                            $alertProductImgFlag = "show";
                                                                    }
                                                           }                                                             
                                                        }
                                            }
                                            //echo "<br />alertProductImgFlag - ".$alertProductImgFlag;
                                            if($alertProductImgFlag == "show"){
                                                  $alertProductImgHtml = "<img src='images/NewIcon/alert_icon.png' style='max-height: 18px; margin-bottom:0px;'>";
                                            }
                                     ?>
                                    
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="" class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" aria-expanded="true" href="#divPurchaseProduct"  aria-controls="divPurchaseProduct">Purchased Products <?php echo $alertProductImgHtml; ?></a>
                                        </h4>
                                    </div>
                
                                    <div id="divPurchaseProduct" class="collapsideDivAdd panel-collapse collapse in" role="tabpanel" aria-labelledby="divPurchaseProduct">
                                        <div class="">
                                        <!--start content type here --> 
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct" id="">
                                                <thead>
                                                    <tr>
                                                        <th class="productNameWidth">Product Name</th>
                                                        <th class="productTypesWidth">Type</th>
                                                        <th class="productPurchageWidth">Purchased</th>
                                                        <th class="productConsumedWidth">Consumed</th>
                                                        <th class="productAvailableWidth">Available</th>
                                                        <th class="productRemainingWidth">% Remaining</th>
 
                                                      <th class="">
                                                        <table class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" style="margin-left: 3px;">  
 
                                                            <thead style="border: none;/* margin-bottom: -10px !important; */">
                                                                <tr class="" style="border: none;margin-bottom: -7px;margin-top: 2px;">                                      
                                                                    <th class="" style="padding-left: 0px !important;">Service License Pack</th>
                                                                    <th class="">Quantity</th>
                                                                </tr>
                                                            </thead>
                                                        </table>  
                                                      </th>
                                                    </tr>
                                                </thead>
                            
                                                <tbody>                                                    
                                                <?php
                                                    $licenseSystemProductSumData = "";
                                                    if(!empty($licenseArr['licenseProductArr']))
                                                    {                                   
                                                        
                                                        foreach($licenseArr['licenseProductArr'] as $productLicenseName => $productLicenseRow)
                                                        {
                                                                $productType = $productLicenseRow['productType'];
                                                                $productQty  = $productLicenseRow['productQty'];
                                                                
                                                                if($productType == "User Package" || $productType == "Trunk Package")
                                                                {
                                                                         if(isset($totalproductCountArr[$productLicenseName])){
                                                                             
                                                                             $consumedLicensed     = $totalproductCountArr[$productLicenseName];
                                                                             
                                                                             if($consumedLicensed >= $productQty){
                                                                                 $consumedLicensed          = $productQty;
                                                                                 $availableProductLicensed  = "0";
                                                                                 $remainPrctge              = "0.0";
                                                                             }else{
                                                                                 $availableProductLicensed  = $productQty - $consumedLicensed;
                                                                                 $remainPrctge              = ($availableProductLicensed * 100 ) / $productQty;    
                                                                                 $remainPrctge              = number_format($remainPrctge, 1);
                                                                             }
                                                                         }
                                                                         else
                                                                         {
                                                                             $consumedLicensed          = "0";
                                                                             $availableProductLicensed  = $productQty - $consumedLicensed;
                                                                             $remainPrctge              = ($availableProductLicensed * 100 ) / $productQty;    
                                                                             $remainPrctge              = number_format($remainPrctge, 1);
                                                                         }
                                                                         $rowColorStyle = setServicePackRowColorBasedOnConsumedQunantity($productQty, $consumedLicensed, $warningThreshold, $attentionThreshold);
                                                                         echo   "<tr $rowColorStyle >";                                                                
                                                                         echo   "<td class='productNameWidth' style='font-size: 14px !important;font-weight: 900;'>$productLicenseName &nbsp;</td>";
                                                                         echo   "<td class='productTypesWidth'>$productType</td>
                                                                                 <td class='productPurchageWidth'>$productQty</td>";
                                                                         echo   "<td class='productConsumedWidth'>$consumedLicensed</td>
                                                                                 <td class='productAvailableWidth'>$availableProductLicensed</td>
                                                                                 <td class='productRemainingWidth'>$remainPrctge</td>";
                                                                         
                                                                         $licenseSystemProductSumData .= "$productLicenseName, $productType, $productQty,  $consumedLicensed, $availableProductLicensed, $remainPrctge ";
                                                                         
                                                                         if(!empty($productLicenseRow['productServicePackDetails']))
                                                                         {
                                                                             echo    "<td id='productQualityWidthTableTd'>
                                                                                     <table class='productQuantityServiceTbl' style='width: 100% !important; margin-top: -10px;'>
                                                                                     <tbody style=''>";
                                                                             $ctr = 1;
                                                                             foreach($productLicenseRow['productServicePackDetails'] as $productSPNameDetailsArr)
                                                                             {                                                                        
                                                                                 $productLicenseServicePackQty  = $productSPNameDetailsArr['quantity'];                                                                        
                                                                                 $productLicenseServicePackName = $productSPNameDetailsArr['productServicePackName'];
                                                                                 if($productLicenseServicePackQty == "0")
                                                                                 {
                                                                                     $productLicenseServicePackQty = "unlimited";
                                                                                 }                                                                                 
                                                                                 
                                                                                 if($productLicenseServicePackName != "NONE")
                                                                                 {
                                                                                     $productLicenseServicePackNameTmp1 = $productLicenseServicePackName;
                                                                                     $productLicenseServicePackQtyTmp1  = $productLicenseServicePackQty;
                                                                                     echo    "<tr><td class='' style='width: 40%;'>$productLicenseServicePackName</td><td class=''>$productLicenseServicePackQty</td></tr>";
                                                                                     
                                                                                     if($ctr == "1"){
                                                                                        $licenseSystemProductSumData .= " , $productLicenseServicePackNameTmp1, $productLicenseServicePackQtyTmp1 \n ";
                                                                                     }else{
                                                                                        $licenseSystemProductSumData .= " , , , , , , $productLicenseServicePackNameTmp1, $productLicenseServicePackQtyTmp1 \n ";
                                                                                     }
                                                                                     $ctr = $ctr + 1;
                                                                                 }
                                                                                 else
                                                                                 { 
                                                                                     if(!empty($productLicenseRow['productServiceDetails']))
                                                                                     {
                                                                                         $productServiceDetailsArr = $productLicenseRow['productServiceDetails']['NONE'];
                                                                                         foreach($productServiceDetailsArr as $serviceDetailsRow)
                                                                                         {
                                                                                             $productLicenseServiceName = $serviceDetailsRow['productServiceName'];
                                                                                             $productLicenseServiceQty  = $serviceDetailsRow['quantity'];
                                                                                             if($productLicenseServiceQty == "0")
                                                                                             {
                                                                                                 $productLicenseServiceQty = "unlimited";
                                                                                             }
                                                                                             $productLicenseServicePackNameTmp1 = $productLicenseServiceName;
                                                                                             $productLicenseServicePackQtyTmp1  = $productLicenseServiceQty;
                                                                                             $licenseSystemProductSumData .= " , , , , , , $productLicenseServicePackNameTmp1, $productLicenseServicePackQtyTmp1 \n ";
                                                                                             echo    "<tr><td class='' style='width: 40%;'>$productLicenseServiceName</td><td class=''>$productLicenseServiceQty</td></tr>";
                                                                                         }                                                                                
                                                                                     }
                                                                                 }
                                                                                 
                                                                                 
                                                                                 
                                                                             }
                                                                             echo    "</tbody>
                                                                                     </table>                                                
                                                                                  </td>";
                                                                         }
                                                                         echo   "</tr>";
                                                                         
                                                                    
                                                                         

                                                                }
                                                                if($productType == "Trunk Package1")
                                                                {
                                                                         $consumedLicensed          = "N/A";
                                                                         $availableProductLicensed  = "N/A";
                                                                         $remainPrctge              = "N/A";

                                                                         echo   "<tr>";                                                                
                                                                         echo   "<td class='productNameWidth' style='font-size: 14px !important;font-weight: 900;'>$productLicenseName &nbsp;</td>";
                                                                         echo   "<td class='productTypesWidth'>$productType</td>
                                                                                 <td class='productPurchageWidth'>$productQty</td>";
                                                                         echo   "<td class='productConsumedWidth'>$consumedLicensed</td>
                                                                                 <td class='productAvailableWidth'>$availableProductLicensed</td>
                                                                                 <td class='productRemainingWidth'>$remainPrctge</td>";
                                                                         $licenseSystemProductSumData .= "$productLicenseName, $productType, $productQty,  $consumedLicensed, $availableProductLicensed, $remainPrctge ";
                                                                         if(!empty($productLicenseRow['productServicePackDetails']))
                                                                         {
                                                                             echo    "<td id='productQualityWidthTableTd'>
                                                                                     <table class='productQuantityServiceTbl' style='width: 100% !important; margin-top: -10px;'>
                                                                                     <tbody style=''>";
                                                                             
                                                                             $ctr1 = 1;
                                                                             foreach($productLicenseRow['productServicePackDetails'] as $productSPNameDetailsArr)
                                                                             {                                                                        
                                                                                 $productLicenseServicePackQty  = $productSPNameDetailsArr['quantity'];                                                                        
                                                                                 $productLicenseServicePackName = $productSPNameDetailsArr['productServicePackName'];
                                                                                 if($productLicenseServicePackQty == "0")
                                                                                 {
                                                                                     $productLicenseServicePackQty = "unlimited";
                                                                                 }
                                                                                 
                                                                                 

                                                                                 if($productLicenseServicePackName != "NONE")
                                                                                 {
                                                                                     $productLicenseServicePackNameTmp1 = $productLicenseServicePackName;
                                                                                     $productLicenseServicePackQtyTmp1  = $productLicenseServicePackQty;
                                                                                     echo    "<tr><td class='' style='width: 40%;'>$productLicenseServicePackName</td><td class=''>$productLicenseServicePackQty</td></tr>";
                                                                                 }
                                                                                 else
                                                                                 { 
                                                                                     if(!empty($productLicenseRow['productServiceDetails']))
                                                                                     {
                                                                                         $productServiceDetailsArr = $productLicenseRow['productServiceDetails']['NONE'];
                                                                                         foreach($productServiceDetailsArr as $serviceDetailsRow)
                                                                                         {
                                                                                             $productLicenseServiceName = $serviceDetailsRow['productServiceName'];
                                                                                             $productLicenseServiceQty  = $serviceDetailsRow['quantity'];
                                                                                             if($productLicenseServiceQty == "0")
                                                                                             {
                                                                                                 $productLicenseServiceQty = "unlimited";
                                                                                             }
                                                                                             $productLicenseServicePackNameTmp1 = $productLicenseServiceName;
                                                                                             $productLicenseServicePackQtyTmp1  = $productLicenseServiceQty;
                                                                                             echo    "<tr><td class='' style='width: 40%;'>$productLicenseServiceName</td><td class=''>$productLicenseServiceQty</td></tr>";
                                                                                         }                                                                                
                                                                                     }
                                                                                 }
                                                                                 
                                                                                 
                                                                                 if($ctr1 == "1"){
                                                                                    $licenseSystemProductSumData .= " , $productLicenseServicePackNameTmp1, $productLicenseServicePackQtyTmp1 \n ";
                                                                                 }else{
                                                                                    $licenseSystemProductSumData .= " , , , , , , $productLicenseServicePackNameTmp1, $productLicenseServicePackQtyTmp1 \n ";
                                                                                 }
                                                                                 
                                                                                 $ctr1 = $ctr1 + 1;
                                                                             }
                                                                             echo    "</tbody>
                                                                                     </table>                                                
                                                                                  </td>";
                                                                         }
                                                                         echo   "</tr>";
                                                                }
                                                                if($productType == "Add-On Package")
                                                                {
                                                                    $consumedLicensed          = "0";
                                                                    $availableProductLicensed  = "0";
                                                                    $remainPrctge              = "0";                                                                    
                                                                                                                                        
                                                                    if(!empty($productLicenseRow['productServiceDetails']))
                                                                    {  
                                                                        $productName = $productLicenseRow['productServiceDetails']['NONE']['0']['productServiceName'];
                                                                    }
                                                                    else
                                                                    {
                                                                        foreach($productLicenseRow['productServicePackDetails'] as $tpKey => $serviceRow)
                                                                        {
                                                                            $productName = $tpKey;
                                                                        }
                                                                    }                                                                    
                                                                    //if(isset($finalEnterpriseServicesArr[$productName]))
                                                                    if(1)
                                                                    {
                                                                        //echo "<br />productServiceDetails New - productServiceDetails";
                                                                        //print_r($productLicenseRow);
                                                                        
                                                                        //echo "<br />finalEnterpriseServicesArr";
                                                                        //print_r($finalEnterpriseServicesArr);
                                                                        
                                                                        $tmpConsumedQty = array();
                                                                        if(count($productLicenseRow['productServiceDetails']['NONE']) > 1)
                                                                        {
                                                                            //echo "<br /> productServiceDetails - 1";
                                                                            $productServiceDetailsArr = $productLicenseRow['productServiceDetails']['NONE'];
                                                                            foreach($productServiceDetailsArr as $serviceDetailsRow1)
                                                                            {               
                                                                                    $productLicenseServiceName1 = $serviceDetailsRow1['productServiceName'];                                                                                        
                                                                                    $productLicenseServiceQty1  = $serviceDetailsRow1['quantity'];
                                                                                    if($productLicenseServiceQty1 == "0")
                                                                                    {

                                                                                    }
                                                                                    else{
                                                                                        $tmpConsumedQty[] = $finalEnterpriseServicesArr[$productLicenseServiceName1][$productLicenseServiceName1];
                                                                                        $consumedLicensed = max($tmpConsumedQty);
                                                                                    }
                                                                            }
                                                                        }  
                                                                        else
                                                                        {                                                                            
                                                                            if(count($productLicenseRow['productServicePackDetails']) > 1)
                                                                            {
                                                                                //echo "<br /> productServicePackDetails - 2";
                                                                                $productServicePackDetailsArr = $productLicenseRow['productServicePackDetails'];
                                                                                foreach($productServicePackDetailsArr as $servicePackDetailsRow1)
                                                                                {               
                                                                                        $productLicenseServicePackName1 = $servicePackDetailsRow1['productServicePackName'];                                                                                        
                                                                                        $productLicenseServicePackQty1  = $servicePackDetailsRow1['quantity'];
                                                                                        if($productLicenseServicePackQty1 == "0")
                                                                                        {

                                                                                        }
                                                                                        else{
                                                                                            $tmpConsumedQty[] = max($finalEnterpriseServicesArr[$productLicenseServicePackName1]);
                                                                                            $consumedLicensed = max($tmpConsumedQty);
                                                                                        }
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                foreach($finalEnterpriseServicesArr[$productName] as $valQty)
                                                                                {
                                                                                     $consumedLicensed = $valQty;
                                                                                }
                                                                            }
                                                                        }
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        if($productQty != "0")
                                                                        {                                                                            
                                                                            if($consumedLicensed > $productQty)
                                                                            { 
                                                                                $consumedLicensed = $productQty;
                                                                            }
                                                                            
                                                                            $availableProductLicensed = $productQty - $consumedLicensed;
                                                                            if($availableProductLicensed != "0")
                                                                            {
                                                                                $remainPrctge             = ($availableProductLicensed * 100 ) / $productQty;    
                                                                                $remainPrctge             = number_format($remainPrctge, 1);
                                                                            }
                                                                            else
                                                                            {
                                                                                $availableProductLicensed = "0";
                                                                                $remainPrctge             = "0.0";
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            $availableProductLicensed  = "unlimited";
                                                                            $remainPrctge              = "unlimited";
                                                                        }
                                                                    }      
                                                                    
                                                                    
                                                                    $rowColorStyle = setServicePackRowColorBasedOnConsumedQunantity($productQty, $consumedLicensed, $warningThreshold, $attentionThreshold);
                                                                    echo   "<tr $rowColorStyle >";                                                                
                                                                    echo   "<td class='productNameWidth' style='font-size: 14px !important;font-weight: 900;'>$productLicenseName &nbsp;</td>";
                                                                    echo   "<td class='productTypesWidth'>$productType</td>
                                                                            <td class='productPurchageWidth'>$productQty</td>";
                                                                    
                                                                    
                                                                    echo   "<td class='productConsumedWidth'>$consumedLicensed</td>
                                                                            <td class='productAvailableWidth'>$availableProductLicensed</td>
                                                                            <td class='productRemainingWidth'>$remainPrctge</td>"; 
                                                                    
                                                                    $licenseSystemProductSumData .= "$productLicenseName, $productType, $productQty,  $consumedLicensed, $availableProductLicensed, $remainPrctge ";
                                                                    
                                                                    
                                                                    if(!empty($productLicenseRow['productServicePackDetails']))
                                                                    {
                                                                        echo    "<td id='productQualityWidthTableTd'>
                                                                                 <table class='productQuantityServiceTbl' style='width: 100% !important; margin-top: -10px;'>
                                                                                 <tbody style=''>";
                                                                        
                                                                        $ctr2 = 1;
                                                                        foreach($productLicenseRow['productServicePackDetails'] as $productSPNameDetailsArr)
                                                                        {                                                                        
                                                                            $productLicenseServicePackQty  = $productSPNameDetailsArr['quantity'];                                                                        
                                                                            $productLicenseServicePackName = $productSPNameDetailsArr['productServicePackName'];
                                                                            if($productLicenseServicePackQty == "0")
                                                                            {
                                                                                $productLicenseServicePackQty    = "unlimited";                                                                                
                                                                            }
                                                                            
                                                                            $productLicenseServicePackNameTmp = $productLicenseServicePackName;
                                                                            $productLicenseServicePackQtyTmp  = $productLicenseServicePackQty;
                                                                            
                                                                            if($productLicenseServicePackName != "NONE")
                                                                            {
                                                                                echo    "<tr><td class='' style='width: 40%;'>$productLicenseServicePackName</td><td class=''>$productLicenseServicePackQty</td></tr>";
                                                                                
                                                                            }
                                                                            else
                                                                            { 
                                                                                if(!empty($productLicenseRow['productServiceDetails']))
                                                                                {
                                                                                    $productServiceDetailsArr = $productLicenseRow['productServiceDetails']['NONE'];
                                                                                    foreach($productServiceDetailsArr as $serviceDetailsRow)
                                                                                    {
                                                                                        $productLicenseServiceName = $serviceDetailsRow['productServiceName'];                                                                                        
                                                                                        $productLicenseServiceQty  = $serviceDetailsRow['quantity'];
                                                                                        if($productLicenseServiceQty == "0")
                                                                                        {
                                                                                            $productLicenseServiceQty = "unlimited";
                                                                                        }
                                                                                        $productLicenseServicePackNameTmp = $productLicenseServiceName;
                                                                                        $productLicenseServicePackQtyTmp  = $productLicenseServiceQty;
                                                                                        //$licenseSystemProductSumData     .= " , , , , , , $productLicenseServicePackNameTmp, $productLicenseServicePackQtyTmp \n ";
                                                                                        //$ctr2 = $ctr2 + 1;
                                                                                        echo    "<tr><td class='' style='width: 40%;'>$productLicenseServiceName</td><td class=''>$productLicenseServiceQty</td></tr>";
                                                                                    }                                                                                
                                                                                }
                                                                            }
                                                                            
                                                                            if($ctr2 == "1"){
                                                                                $licenseSystemProductSumData .= " , $productLicenseServicePackNameTmp, $productLicenseServicePackQtyTmp \n ";
                                                                            }else{
                                                                                $licenseSystemProductSumData .= " , , , , , , $productLicenseServicePackNameTmp, $productLicenseServicePackQtyTmp \n ";
                                                                            }
                                                                            $ctr2 = $ctr2 + 1;
                                                                             
                                                                             
                                                                        }
                                                                        echo    "</tbody>
                                                                                </table>                                                
                                                                             </td>";
                                                                    }
                                                                    echo   "</tr>";                                                                    
                                                           }                                                             
                                                        }
                                                    }
                                                ?>                                          
                                                <!--end error show -->
                                                </tbody>
                                            </table>
                                        
                                        <?php          
                                                $_SESSION['csvSystemSummaryUsageData']  = "";
                                                $_SESSION['csvSystemSummaryUsageData'] .= "\n Purchased Products \n";
                                                $_SESSION['csvSystemSummaryUsageData'] .= "Product Name, Type, Purchased, Consumed, Available, % Remaining, Service License Pack, Quantity  \n";
                                                $_SESSION['csvSystemSummaryUsageData'] .= $licenseSystemProductSumData;
                                        ?>
                        
                                            <!--end content type start here -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            
                        <!--end purchase product2 ui start  -->
 
                        <!--purchase product ui start  -->   
                        <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    
                                    <?php
                                        //if(0)
                                        //if(!empty($subscriberLicenseArr['Success']))
                                        if(0)
                                        {
                                                $userLicenseArr         = $subscriberLicenseArr['Success']['User License'];
                                                $totalUserLicensed      = $userLicenseArr['Licensed'];
                                                $usedUserLicensed       = $userLicenseArr['Used'];
                                                $remainUserLicensed     = $userLicenseArr['Available'];
                                                if($totalUserLicensed != "Unlimited")
                                                {
                                                        $remainUserPercentage  = ($remainUserLicensed * 100) / $totalUserLicensed;
                                                        $remainUserPercentage  = number_format($remainUserPercentage, 2);
                                                }
                                                else
                                                {
                                                        $remainUserPercentage = $totalUserLicensed;
                                                }

                                                $groupLicenseArr         = $subscriberLicenseArr['Success']['Group License'];
                                                $totalGroupLicensed      = $groupLicenseArr['Licensed'];
                                                $usedGroupLicensed       = $groupLicenseArr['Used'];
                                                $remainGroupLicensed     = $groupLicenseArr['Available'];
                                                if($totalGroupLicensed != "Unlimited")
                                                {
                                                        $remainGroupPercentage  = ($remainGroupLicensed * 100) / $totalGroupLicensed;
                                                        $remainGroupPercentage  = number_format($remainPercentage, 2);
                                                }
                                                else
                                                {
                                                        $remainGroupPercentage  = $totalGroupLicensed;
                                                }	
                                        }
                                        else
                                        {	
                                                $totalUserLicensed     = $licenseArr['userLicense'];
                                                $usedUserLicensed      = $userLicenseCount1 - $trunkUserLicenseCount1;
                                                if($totalUserLicensed != "unlimited")
                                                {
                                                    $remainUserLicensed   = $totalUserLicensed - $usedUserLicensed;   
                                                }
                                                else
                                                {
                                                        $remainUserLicensed   = $totalUserLicensed;
                                                }
                                                if($totalUserLicensed != "unlimited")
                                                {
                                                        $remainUserPercentage  = ($remainUserLicensed * 100) / $totalUserLicensed;
                                                        $remainUserPercentage  = number_format($remainUserPercentage, 2);
                                                }
                                                else
                                                {
                                                        $remainUserPercentage = $totalUserLicensed;
                                                }


                                                $totalGroupLicensed = $licenseArr['groupLicense'];
                                                $usedGroupLicensed  = $groupLicenseCount1;
                                                if($totalGroupLicensed != "unlimited")
                                                {
                                                        $remainGroupLicensed = $totalGroupLicensed - $usedGroupLicensed; 
                                                } 
                                                else 
                                                { 
                                                        $remainGroupLicensed = $totalGroupLicensed; 
                                                }
                                                if($totalGroupLicensed != "unlimited")
                                                {
                                                        $remainGroupPercentage  = ($remainUserLicensed * 100) / $totalGroupLicensed;
                                                        $remainGroupPercentage  = number_format($remainGroupPercentage, 2);
                                                }
                                                else
                                                {
                                                        $remainGroupPercentage = $totalGroupLicensed;
                                                }
                                        }
                                        $rowColorStyleUser       = setServicePackRowColorBasedOnConsumedQunantity($totalUserLicensed, $usedUserLicensed, $warningThreshold, $attentionThreshold);
                                        $rowColorStyleVUser      = setServicePackRowColorBasedOnConsumedQunantity($licenseArr['virtualUserLicense'], $virtualLicenseCount1, $warningThreshold, $attentionThreshold);
                                        $rowColorStyleTgroupUser = setServicePackRowColorBasedOnConsumedQunantity($licenseArr['trunkGroupUserLicense'], $trunkUserLicenseCount1, $warningThreshold, $attentionThreshold);
                                        $rowColorStyleGroup      = setServicePackRowColorBasedOnConsumedQunantity($totalGroupLicensed, $usedGroupLicensed, $warningThreshold, $attentionThreshold);
                                        
                                        $alertSubscribeImgFlag  = "hide";
                                        $alertSubscribeImgHtml  = "";
                                        if(stripos($rowColorStyleUser, "#F3C6CA") !== false  || stripos($rowColorStyleUser, "#F6EBB1") !== false){
                                            $alertSubscribeImgFlag  = "show";
                                        }
                                        if(stripos($rowColorStyleVUser, "#F3C6CA") !== false  || stripos($rowColorStyleVUser, "#F6EBB1") !== false){
                                            $alertSubscribeImgFlag  = "show";
                                        }
                                        if(stripos($rowColorStyleTgroupUser, "#F3C6CA") !== false  || stripos($rowColorStyleTgroupUser, "#F6EBB1") !== false){
                                            $alertSubscribeImgFlag  = "show";
                                        }
                                        if(stripos($rowColorStyleGroup, "#F3C6CA") !== false  || stripos($rowColorStyleGroup, "#F6EBB1") !== false){
                                            $alertSubscribeImgFlag  = "show";
                                        }
                                        
                                        if($alertSubscribeImgFlag == "show"){
                                            $alertSubscribeImgHtml = "<img src='images/NewIcon/alert_icon.png' style='max-height: 18px; margin-bottom:0px;'>";
                                        }
                                
                                 ?>
                                    
                                    
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="" class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" aria-expanded="true" href="#divPurchaseProductTwo"  aria-controls="divPurchaseProductTwo">Subscriber Licenses <?php echo $alertSubscribeImgHtml; ?></a>
                                        </h4>
                                    </div>

                                    <div id="divPurchaseProductTwo" class="collapsideDivAdd panel-collapse collapse in" role="tabpanel" aria-labelledby="divPurchaseProductTwo">
                                        <div class="">
                                        <!--start content type here --> 
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct" id="">
                                                <thead>
                                                    <tr>
                                                        <th class="productNameWidth">Type</th>
                                                        <th class="productServicePackWidth">Licensed</th>
                                                        <th class="productServicePackWidth">Used</th>
                                                        <th class="productServicePackWidth">Available</th>
                                                        <th class="productServicePackWidth" style="width: 132px !important;">% Remaining</th>

                                                    </tr>
                                                </thead>

                                                <tbody>                                                    
                                                     <tr class="puchagesSuccessRow" <?php echo $rowColorStyleUser; ?> >
                                                            <td class="productNameWidth">User Licenses</td>
                                                            <td class="productServicePackWidth"><?php echo $totalUserLicensed; ?></td>
                                                            <td class="productServicePackWidth"><?php echo $usedUserLicensed; ?></td>
                                                            <td class="productServicePackWidth"><?php echo $remainUserLicensed; ?></td>
                                                            <td class="productServicePackWidth"><?php echo $remainUserPercentage; ?></td>
                                                    </tr>

                                                     <tr class="puchagesSuccessRow" <?php echo $rowColorStyleVUser; ?> >
                                                        <td class="productNameWidth">Virtual Users</td>
                                                        <td class="productServicePackWidth"><?php echo $totalVirtualUserLicensed = $licenseArr['virtualUserLicense']; ?></td>
                                                        <td class="productServicePackWidth"><?php echo $virtualLicenseCount1;  ?></td>
                                                        <td class="productServicePackWidth">
                                                            <?php 
                                                            if($totalVirtualUserLicensed != "unlimited")
                                                                { 
                                                                        echo $remainVirtualUserCount = $totalVirtualUserLicensed - $virtualLicenseCount1; 
                                                                        $remainVirtualLicensedTmp    = $remainVirtualUserCount;
                                                                } 
                                                                else 
                                                                { 
                                                                        echo $totalVirtualUserLicensed;
                                                                        $remainVirtualLicensedTmp    = $totalVirtualUserLicensed;

                                                                } 
                                                         ?></td>
                                                        <td class="productServicePackWidth">
                                                            <?php 
                                                                    if($totalVirtualUserLicensed != "unlimited"){                                                                        
                                                                        $remainPercentage       = ($remainVirtualUserCount * 100) / $totalVirtualUserLicensed;
                                                                        echo $remainPercentage  = number_format($remainPercentage, 2);
                                                                        $remainPercVirtual             = $remainPercentage;
                                                                    }else{
                                                                        echo $totalVirtualUserLicensed;
                                                                        $remainPercVirtual             = $totalVirtualUserLicensed;
                                                                    }
                                                            ?>
                                                        </td>
                                                    </tr>

                                                     <tr class="puchagesSuccessRow" <?php echo $rowColorStyleTgroupUser; ?> >
                                                        <td class="productNameWidth">Trunk Group Users</td>
                                                        <td class="productServicePackWidth"><?php echo $totalTrunkUserLicensed = $licenseArr['trunkGroupUserLicense']; ?></td>
                                                        <td class="productServicePackWidth"><?php echo $trunkUserLicenseCount1;  ?></td>
                                                        <td class="productServicePackWidth">
                                                            <?php 
                                                            if($totalTrunkUserLicensed != "unlimited")
                                                                { 
                                                                    echo $remainTrunkUserLicensed = $totalTrunkUserLicensed - $trunkUserLicenseCount1; 
                                                                    $remainTrunkUserLicensedTmp   = $remainTrunkUserLicensed;
                                                                    
                                                                } 
                                                                else 
                                                                { 
                                                                    echo $totalTrunkUserLicensed;     
                                                                    $remainTrunkUserLicensedTmp   = $totalTrunkUserLicensed;
                                                                }
                                                            ?></td>
                                                        <td class="productServicePackWidth">
                                                            <?php 
                                                                    if($totalTrunkUserLicensed != "unlimited"){
                                                                        $remainPercentage       = ($remainTrunkUserLicensed * 100) / $totalTrunkUserLicensed;
                                                                        echo $remainPercentage  = number_format($remainPercentage, 2);
                                                                        $remainPercGroup      = $remainPercentage;
                                                                    }else{
                                                                        echo $totalTrunkUserLicensed;
                                                                        $remainPercGroup      = $totalTrunkUserLicensed;
                                                                    }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr class="puchagesSuccessRow" <?php echo $rowColorStyleGroup; ?> >
                                                            <td class="productNameWidth">Group Licenses</td>
                                                            <td class="productServicePackWidth"><?php echo $totalGroupLicensed; ?></td>
                                                            <td class="productServicePackWidth"><?php echo $usedGroupLicensed; ?></td>
                                                            <td class="productServicePackWidth"><?php echo $remainGroupLicensed; ?></td>
                                                            <td class="productServicePackWidth"><?php echo $remainGroupPercentage; ?></td>
                                                    </tr>
                                                    <!--end error show -->
                                                </tbody>
                                            </table>
                                             <!--end content type start here -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <?php                                    
                                $_SESSION['csvSystemSummaryUsageData'] .= "\n Subscriber Licenses \n";
                                $_SESSION['csvSystemSummaryUsageData'] .= "Type, Licensed, Used, Available, % Remaining  \n";
                                $_SESSION['csvSystemSummaryUsageData'] .= "User Licenses, $totalUserLicensed, $usedUserLicensed, $remainUserLicensed, $remainUserPercentage \n ";
                                $_SESSION['csvSystemSummaryUsageData'] .= "Virtual Users, $totalVirtualUserLicensed, $virtualLicenseCount1, $remainVirtualLicensedTmp , $remainPercVirtual \n ";
                                $_SESSION['csvSystemSummaryUsageData'] .= "Trunk Group Users, $totalTrunkUserLicensed, $trunkUserLicenseCount1,$remainTrunkUserLicensedTmp , $remainPercGroup \n ";
                                $_SESSION['csvSystemSummaryUsageData'] .= "Group Licenses, $totalGroupLicensed, $usedGroupLicensed, $remainGroupLicensed, $remainGroupPercentage \n ";
                        ?>
                        
                        <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    <!-- Code added @ 11 July 2019 -->
                                    <?php
                                        $alertGroupImgFlag  = "hide";
                                        $alertGroupImgHtml  = "";
                                        foreach($finalEnterpriseGroupServicesArr as $licenseGroupServicePackName => $licenseGroupServicePackRow)
                                         {  
                                                if(!empty($licenseGroupServicePackRow) && count($licenseGroupServicePackRow) > 0)
                                                        {
                                                                $totalLicenseGroupServiceUsed = max($licenseGroupServicePackRow);

                                                                if($licenseArr['licenseSPQTYArr'][$licenseGroupServicePackName] == "0"){
                                                                        $totalGroupLicensed          = "unlimited";
                                                                        $totalGroupLicensedAvailable = "unlimited";
                                                                }else{
                                                                        $totalGroupLicensed          = $licenseArr['licenseSPQTYArr'][$licenseGroupServicePackName];
                                                                        $totalGroupLicensedAvailable = $totalGroupLicensed - $totalLicenseGroupServiceUsed;
                                                                }
                                                                $rowColorStyleGroupServices = setServicePackRowColorBasedOnConsumedQunantity($totalGroupLicensed, $totalLicenseGroupServiceUsed, $warningThreshold, $attentionThreshold);

                                                                if(stripos($rowColorStyleGroupServices, "#F3C6CA") !== false  || stripos($rowColorStyleGroupServices, "#F6EBB1") !== false){
                                                                        $alertGroupImgFlag = "show";
                                                                }

                                                        }	
                                         }
                                         if($alertGroupImgFlag == "show"){
                                                 $alertGroupImgHtml = "<img src='images/NewIcon/alert_icon.png' style='max-height: 18px; margin-bottom:0px;'>";
                                         }
                                     ?>
                                    <!-- End code -->
                                    
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="" class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" aria-expanded="true" href="#divGroupServiceLicese" aria-controls="divPurchaseProductTwo">Group Service Licenses <?php echo $alertGroupImgHtml; ?></a>
                                        </h4>
                                    </div>

                                    <div id="divGroupServiceLicese" class="collapsideDivAdd panel-collapse collapse in" role="tabpanel" aria-labelledby="divPurchaseProductTwo">
                                        <div class="">
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct" id="">
                                                <thead>
                                                    <tr>

                                                        <th class="trunctingServiceTh" style="width: 16% !important;/* border: none; */">Service License Pack</th>
                                                            <th class="productQualityWidthTableTd" style="width: 74% !important;/* margin: 0 auto; */">
								<table class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" id="" style="border: none;margin-bottom: -9px;">

                                                      <!--    <th class="trunctingServiceTh" style="/* border: none; */">Service License Pack</th>
                                                            <th class="productQualityWidthTableTd" style="width: 100% !important;/* margin: 0 auto; */">
								<table border="1" class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" id="" style="border: none;margin-bottom: -9px;margin-left:3px !important;">
 -->
                                                                    <thead>
									<tr class="" style="border: none;margin-bottom: 0px;margin-top: 1px;">  
                                                                            <th class="trunctingServiceTh" style="width: 163px !important;">Service</th>
                                                                            <th class="productServicePackWidth">Licensed</th>
                                                                            <th class="productServicePackWidth">Used</th>
                                                                            <th class="productServicePackWidth" style="width: 90px !important;">Available</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </th>
                                                    </tr>
                                                </thead>
                                                <tbody>                                                   
                                                    <?php                                               
                                                        ksort($finalEnterpriseGroupServicesArr);
                                                        $licenseSystemGroupData = "";
                                                        if(!empty($finalEnterpriseGroupServicesArr) && count($finalEnterpriseGroupServicesArr) > 0)
                                                        {
                                                            foreach($finalEnterpriseGroupServicesArr as $licenseGroupServicePackName => $licenseGroupServicePackRow)
                                                            {         
                                                                if($licenseGroupServicePackName != ""){                                                                    
                                                                    if(!empty($licenseGroupServicePackRow) && count($licenseGroupServicePackRow) > 0)
                                                                    {
                                                                        $totalLicenseGroupServiceUsed = max($licenseGroupServicePackRow);
                                                                        
                                                                        if($licenseArr['licenseSPQTYArr'][$licenseGroupServicePackName] == "0"){
                                                                            $totalGroupLicensed          = "unlimited";
                                                                            $totalGroupLicensedAvailable = "unlimited";
                                                                        }else{
                                                                            $totalGroupLicensed          = $licenseArr['licenseSPQTYArr'][$licenseGroupServicePackName];
                                                                            $totalGroupLicensedAvailable = $totalGroupLicensed - $totalLicenseGroupServiceUsed;
                                                                        }
                                                                        $rowColorStyleGroupServices = setServicePackRowColorBasedOnConsumedQunantity($totalGroupLicensed, $totalLicenseGroupServiceUsed, $warningThreshold, $attentionThreshold);
                                                                        
                                                                        
                                                                        echo "<tr $rowColorStyleGroupServices><td class='trunctingServiceTh' style=''>$licenseGroupServicePackName</td>";
                                                                        echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;";
                                                                        
                                                                        echo "<table class='productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese'><tbody>";
                                                                        
                                                                        
                                                                        echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>Total</td>
                                                                                    <td class='productServicePackWidth'>$totalGroupLicensed</td>
                                                                                    <td class='productServicePackWidth'>$totalLicenseGroupServiceUsed</td>
                                                                                    <td class='productServicePackWidth'>$totalGroupLicensedAvailable</td>
                                                                              </tr>";
                                                                        
                                                                        $licenseSystemGroupData .= "$licenseGroupServicePackName, Total, $totalGroupLicensed,  $totalLicenseGroupServiceUsed, $totalGroupLicensedAvailable \n ";
                                                                        
                                                                        foreach($licenseGroupServicePackRow as $licenseGroupServiceName => $licenseGroupServiceUsed)
                                                                        {
                                                                            
                                                                            if($licenseArr['licenseServiceArr'][$licenseGroupServiceName]['servicePackQty'] == "0"){
                                                                                $groupLicensed  = "unlimited";       
                                                                                $groupAvailable = "unlimited";
                                                                            }else{
                                                                                $groupLicensed  = $licenseArr['licenseServiceArr'][$licenseGroupServiceName]['servicePackQty'];
                                                                                $groupAvailable = $groupLicensed - $licenseGroupServiceUsed;
                                                                            }
                                                                            
                                                                            echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>$licenseGroupServiceName</td>
                                                                                    <td class='productServicePackWidth'>$groupLicensed</td>
                                                                                    <td class='productServicePackWidth'>$licenseGroupServiceUsed</td>
                                                                                    <td class='productServicePackWidth'>$groupAvailable</td>
                                                                                  </tr>";
                                                                            
                                                                            $licenseSystemGroupData .= "  , $licenseGroupServiceName,  $groupLicensed, $licenseGroupServiceUsed, $groupAvailable \n ";
                                                                        }                                                                    
                                                                        echo "</tbody></table>";                                                                    
                                                                    }                                                                
                                                                    echo "</td></tr>";
                                                                }
                                                            }
                                                        }
                                                        ?>                                                    
                                                </tbody>                                                 
                                            </table>
                                             <!--end content type start here -->
                                             <?php                                    
                                                    $_SESSION['csvSystemSummaryUsageData'] .= "\n Group Service Licenses \n";
                                                    $_SESSION['csvSystemSummaryUsageData'] .= "Service License Pack, Services, Licensed, Used, Available  \n";
                                                    $_SESSION['csvSystemSummaryUsageData'] .= $licenseSystemGroupData;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <!--data vertual services -->
                        <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    <?php
                                        $alertVirtualImgFlag  = "hide";
                                        $alertVirtualImgHtml  = "";                                        
                                        foreach($finalEnterpriseVirtualServicesArr as $licenseVirtualServicePackName => $licenseVirtualServicePackRow)
                                        {         
                                                if($licenseVirtualServicePackName != "")
                                                {                                                                    
                                                        if(!empty($licenseVirtualServicePackRow) && count($licenseVirtualServicePackRow) > 0)
                                                        {
                                                                $totalLicenseVirtualServiceUsed = max($licenseVirtualServicePackRow);

                                                                if($licenseArr['licenseSPQTYArr'][$licenseVirtualServicePackName] == "0"){
                                                                        $totalVirtualLicensed          = "unlimited";
                                                                        $totalVirtualLicensedAvailable = "unlimited";
                                                                }else{
                                                                        $totalVirtualLicensed          = $licenseArr['licenseSPQTYArr'][$licenseVirtualServicePackName];
                                                                        $totalVirtualLicensedAvailable = $totalVirtualLicensed - $totalLicenseVirtualServiceUsed;
                                                                }

                                                                $rowColorStyleVirtualServices = setServicePackRowColorBasedOnConsumedQunantity($totalVirtualLicensed, $totalLicenseVirtualServiceUsed, $warningThreshold, $attentionThreshold);

                                                                if(stripos($rowColorStyleVirtualServices, "#F3C6CA") !== false  || stripos($rowColorStyleVirtualServices, "#F6EBB1") !== false){
                                                                        $alertVirtualImgFlag = "show";
                                                                }
                                                        }
                                                }
                                        }                                        
                                        if($alertVirtualImgFlag == "show"){
                                                 $alertVirtualImgHtml = "<img src='images/NewIcon/alert_icon.png' style='max-height: 18px; margin-bottom:0px;'>";
                                        }
                                     ?>
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="" class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" aria-expanded="true" href="#divVirtualServiceLicse" aria-controls="divVirtualServiceLicse">Virtual Service Licenses <?php echo $alertVirtualImgHtml ?></a>
                                        </h4>
                                    </div>

                                    <div id="divVirtualServiceLicse" class="collapsideDivAdd panel-collapse collapse in" role="tabpanel" aria-labelledby="divVirtualServiceLicse">
                                        <div class="">
                                        <!--start content type here --> 
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct" id="">
                                                <thead>
                                                    <tr>
                                                        <th class="trunctingServiceTh" style="width: 0%;/* border: none; */">Service License Pack</th>
                                                            <th class="productQualityWidthTableTd" style="width: 100%;margin:1px;">

								<table class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" id="" style="border: none;margin-bottom: -9px;margin-left:3px !important;">

							<!--	<table border="1" class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" id="" style="border: none;margin-bottom: -9px;">-->

                                                                    <thead>
									<tr class="" style="border: none;margin-bottom: 0px;margin-top: 1px;">  
                                                                           <th class="trunctingServiceTh" style="width: 158px !important;">Service</th>
                                                                            <th class="productServicePackWidth">Licensed</th>
                                                                            <th class="productServicePackWidth">Used</th>
                                                                            <th class="productServicePackWidth" style="width: 98px !important;">Available</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </th>

                                                        <!-- <th class="trunctingServiceTh" style=" width: 24% !important;">Service</th>
                                                        <th class="GroupSummaryLiceseTh">Used</th>
                                                        <th class="productServicePackWidth">Used (Hosted)</th>
                                                        <th class="productServicePackWidth">Used (Trunk)</th>-->
                                                    </tr>
                                              </thead>                                               
                                                <tbody>
                                                    <?php                                               
                                                        ksort($finalEnterpriseVirtualServicesArr);
                                                        $licenseSystemVirtualData = "";
                                                        $tmpVSPArrForSysLevelSumry = array(); 
                                                        //echo "<br /> finalEnterpriseVirtualServicesArr - ";
                                                        //print_r($finalEnterpriseVirtualServicesArr); 
                                                        //echo "<br /> licenseServiceArrLevelBiseDup - VIRTUAL - ";
                                                        //print_r($licenseArr['licenseServiceArrLevelBiseDup']['VIRTUAL']);
                                                        if(!empty($finalEnterpriseVirtualServicesArr) && count($finalEnterpriseVirtualServicesArr) > 0)
                                                        {
                                                            foreach($finalEnterpriseVirtualServicesArr as $licenseVirtualServicePackName => $licenseVirtualServicePackRow)
                                                            {         
                                                                if($licenseVirtualServicePackName != "")
                                                                {
                                                                    $tmpVSPArrForSysLevelSumry[] = $licenseVirtualServicePackName;
                                                                    if(!empty($licenseVirtualServicePackRow) && count($licenseVirtualServicePackRow) > 0)
                                                                    {
                                                                        $totalLicenseVirtualServiceUsed = max($licenseVirtualServicePackRow);
                                                                        
                                                                        if($licenseArr['licenseSPQTYArr'][$licenseVirtualServicePackName] == "0"){
                                                                            $totalVirtualLicensed          = "unlimited";
                                                                            $totalVirtualLicensedAvailable = "unlimited";
                                                                        }else{
                                                                            $totalVirtualLicensed          = $licenseArr['licenseSPQTYArr'][$licenseVirtualServicePackName];
                                                                            $totalVirtualLicensedAvailable = $totalVirtualLicensed - $totalLicenseVirtualServiceUsed;
                                                                        }
                                                                        
                                                                        $rowColorStyleVirtualServices = setServicePackRowColorBasedOnConsumedQunantity($totalVirtualLicensed, $totalLicenseVirtualServiceUsed, $warningThreshold, $attentionThreshold);
                                                                        
                                                                        echo "<tr class='' $rowColorStyleVirtualServices ><td class='trunctingServiceTh' style='font-size: 14px !important;font-weight: 900;border: 1px solid #cccccc !important;'>$licenseVirtualServicePackName</td>";
                                                                        echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;";
                                                                        
                                                                        echo "<table class='productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese' style='margin-top: 0px;'><tbody>";
                                                                        
                                                                        echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>Total</td>
                                                                                    <td class='productServicePackWidth'>$totalVirtualLicensed</td>
                                                                                    <td class='productServicePackWidth'>$totalLicenseVirtualServiceUsed</td>
                                                                                    <td class='productServicePackWidth'>$totalVirtualLicensedAvailable</td>
                                                                              </tr>";
                                                                        if($rowColorStyleVirtualServices != "style='display:none;'"){
                                                                            $licenseSystemVirtualData .= "$licenseVirtualServicePackName, Total, $totalVirtualLicensed,  $totalLicenseVirtualServiceUsed, $totalVirtualLicensedAvailable \n ";
                                                                        }
                                                                        $tmpVirtualServiceList = array();
                                                                        foreach($licenseVirtualServicePackRow as $licenseVirtualServiceName => $licenseVirtualServiceUsed)
                                                                        {
                                                                            $tmpVirtualServiceList[] = $licenseVirtualServiceName;
                                                                            if($licenseArr['licenseServiceArr'][$licenseVirtualServiceName]['servicePackQty'] == "0"){
                                                                                $virtualLicensed  = "unlimited";       
                                                                                $virtualAvailable = "unlimited";
                                                                            }else{
                                                                                $virtualLicensed  = $licenseArr['licenseServiceArr'][$licenseVirtualServiceName]['servicePackQty'];
                                                                                $virtualAvailable = $virtualLicensed - $licenseVirtualServiceUsed;
                                                                            }
                                                                            echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>$licenseVirtualServiceName</td>
                                                                                    <td class='productServicePackWidth'>$totalVirtualLicensed</td>
                                                                                    <td class='productServicePackWidth'>$totalLicenseVirtualServiceUsed</td>
                                                                                    <td class='productServicePackWidth'>$totalVirtualLicensedAvailable</td>
                                                                                  </tr>";
                                                                            if($rowColorStyleVirtualServices != "style='display:none;'"){
                                                                                $licenseSystemVirtualData .= "  , $licenseVirtualServiceName,  $virtualLicensed, $licenseVirtualServiceUsed, $virtualAvailable \n ";
                                                                            }
                                                                        }  
                                                                        
                                                                        foreach($licenseArr['licenseServiceArrLevelBiseDup']['VIRTUAL'][$licenseVirtualServicePackName] as $tmpVirtualTmpServiceName){
                                                                            if(!in_array($tmpVirtualTmpServiceName, $tmpVirtualServiceList))
                                                                            {
                                                                                $virtualTmpUsed      = "0";
                                                                                $virtualTmpAvailable = $virtualLicensed - $virtualTmpUsed;
                                                                                echo "<tr class='' >                                                                        
                                                                                    <td class='trunctingServiceTh'>$tmpVirtualTmpServiceName</td>
                                                                                    <td class='productServicePackWidth'>$virtualLicensed</td>
                                                                                    <td class='productServicePackWidth'>$virtualTmpUsed</td>
                                                                                    <td class='productServicePackWidth'>$virtualTmpAvailable</td>
                                                                                  </tr>";
                                                                                $licenseSystemVirtualData .= "  , $tmpVirtualTmpServiceName,  $virtualLicensed, $virtualTmpUsed, $virtualTmpAvailable \n ";
                                                                            }
                                                                        }
                                                                        echo "</tbody></table>";                                                                    
                                                                    }                                                                
                                                                    echo "</td></tr>";
                                                                }
                                                            }
                                                        } 
                                                        
                                                        if(!empty($tmpVSPArrForSysLevelSumry)){
                                                            foreach($licenseArr['licenseServiceArrLevelBiseDup']['VIRTUAL'] as $licenseVirtualServicePackNameDup => $licenseVirtualServicePackRowDup){
                                                               if(!in_array($licenseVirtualServicePackNameDup, $tmpVSPArrForSysLevelSumry)){
                                                                if($licenseVirtualServicePackNameDup != "")
                                                                {                                                                    
                                                                    if(!empty($licenseVirtualServicePackRowDup) && count($licenseVirtualServicePackRowDup) > 0)
                                                                    {
                                                                        $totalLicenseVirtualServiceUsed = "0";
                                                                        
                                                                        if($licenseArr['licenseSPQTYArr'][$licenseVirtualServicePackNameDup] == "0"){
                                                                            $totalVirtualLicensed          = "unlimited";
                                                                            $totalVirtualLicensedAvailable = "unlimited";
                                                                        }else{
                                                                            $totalVirtualLicensed          = $licenseArr['licenseSPQTYArr'][$licenseVirtualServicePackNameDup];
                                                                            $totalVirtualLicensedAvailable = $totalVirtualLicensed - $totalLicenseVirtualServiceUsed;
                                                                        }
                                                                        
                                                                       $rowColorStyleVirtualServices = setServicePackRowColorBasedOnConsumedQunantity($totalVirtualLicensed, $totalLicenseVirtualServiceUsed, $warningThreshold, $attentionThreshold);
                                                                        
                                                                        echo "<tr class='' $rowColorStyleVirtualServices><td class='trunctingServiceTh' style='font-size: 14px !important;font-weight: 900;border: 1px solid #cccccc !important;'>$licenseVirtualServicePackNameDup</td>";
                                                                        echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;";
                                                                        
                                                                        echo "<table class='productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese' style='margin-top: 0px;'><tbody>";
                                                                        
                                                                        echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>Total</td>
                                                                                    <td class='productServicePackWidth'>$totalVirtualLicensed</td>
                                                                                    <td class='productServicePackWidth'>$totalLicenseVirtualServiceUsed</td>
                                                                                    <td class='productServicePackWidth'>$totalVirtualLicensedAvailable</td>
                                                                              </tr>";
                                                                        
                                                                        if($rowColorStyleVirtualServices != "style='display:none;'"){
                                                                            $licenseSystemVirtualData .= "$licenseVirtualServicePackNameDup, Total, $totalVirtualLicensed,  $totalLicenseVirtualServiceUsed, $totalVirtualLicensedAvailable \n ";
                                                                        }
                                                                        foreach($licenseVirtualServicePackRowDup as $licenseVirtualServiceNameDup => $licenseVirtualServiceNameDup1)
                                                                        {                                                                            
                                                                            echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>$licenseVirtualServiceNameDup1</td>
                                                                                    <td class='productServicePackWidth'>$totalVirtualLicensed</td>
                                                                                    <td class='productServicePackWidth'>$totalLicenseVirtualServiceUsed</td>
                                                                                    <td class='productServicePackWidth'>$totalVirtualLicensedAvailable</td>
                                                                                  </tr>";
                                                                            if($rowColorStyleVirtualServices != "style='display:none;'"){
                                                                                $licenseSystemVirtualData .= "  , $licenseVirtualServiceNameDup1,  $totalVirtualLicensed, $totalLicenseVirtualServiceUsed, $totalVirtualLicensedAvailable \n ";
                                                                            }                                                                                
                                                                        }  
                                                                        
                                                                        echo "</tbody></table>";                                                                    
                                                                    }                                                                
                                                                    echo "</td></tr>";
                                                                }
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                </tbody>
                                            </table>
                                            <?php                                    
                                                    $_SESSION['csvSystemSummaryUsageData'] .= "\n Virtual Service Licenses \n";
                                                    $_SESSION['csvSystemSummaryUsageData'] .= "Service License Pack, Services, Licensed, Used, Available  \n";
                                                    $_SESSION['csvSystemSummaryUsageData'] .= $licenseSystemVirtualData;
                                            ?>
                                             <!--end content type start here -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <!--end vertual group services -->
                        <!--group context userservices form sumarry view -->
                        <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    
                                    <?php
                                        $alertUserImgFlag  = "hide";
                                        $alertUserImgHtml  = "";                                        
                                        foreach($finalEnterpriseServicesArr as $licenseServicePackName => $licenseServicePackRow)
                                        {
                                                if(!empty($licenseServicePackRow) && count($licenseServicePackRow) > 0)
                                                {
                                                        $totalServiceBasedOnSP = $totalSPCountArr[$licenseServicePackName];

                                                        if($licenseArr['licenseSPQTYArr'][$licenseServicePackName] == "0"){
                                                                $totalLicensed  = "unlimited";
                                                                $totalAvailable = "unlimited";
                                                        }else{
                                                                $totalLicensed = $licenseArr['licenseSPQTYArr'][$licenseServicePackName];
                                                                $totalAvailable = $totalLicensed - $totalServiceBasedOnSP;
                                                        }

                                                        $totalUsedTrunckCount = max($tmpArrForUsedTrunck[$licenseServicePackName]);
                                                        $totalHostedTrunk     = $totalServiceBasedOnSP - $totalUsedTrunckCount;

                                                        $rowColorStyleUserService = setServicePackRowColorBasedOnConsumedQunantity($totalLicensed, $totalServiceBasedOnSP, $warningThreshold, $attentionThreshold);

                                                        if(stripos($rowColorStyleUserService, "#F3C6CA") !== false  || stripos($rowColorStyleUserService, "#F6EBB1") ){
                                                                        $alertUserImgFlag = "show";
                                                        }			
                                                }
                                        }		
                                        
                                        if($alertUserImgFlag == "show"){
                                                 $alertUserImgHtml = "<img src='images/NewIcon/alert_icon.png' style='max-height: 18px; margin-bottom:0px;'>";
                                        }
                                     ?>
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="" class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" aria-expanded="true" href="#divUserServiceGroupSummaryLicese" aria-controls="divPurchaseProductTwo">User Service Licenses <?php echo $alertUserImgHtml; ?></a>
                                        </h4>
                                    </div>

                                    <div id="divUserServiceGroupSummaryLicese" class="collapsideDivAdd panel-collapse collapse in" role="tabpanel" aria-labelledby="divPurchaseProductTwo">
                                        <div class="">
                                        <!--start content type here --> 
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct divUserServiceGroupSummaryLicese" id="">
                                               <thead>
                                                    <tr>
                                                        <th class="trunctingServiceTh" style="width: 0%;/* border: none; */">Service License Pack</th>
                                                            <th class="productQualityWidthTableTd" style="width: 100%;margin: 0 auto;">
								<table class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" id="" style="border: none;margin-bottom: -9px;margin-left:3px !important;">
                                                                    <thead>
									<tr class="" style="border: none;margin-bottom: 1px;margin-top: 2px;">  
                                                                         <th class="trunctingServiceTh" style="width: 159px !important;">Service</th>
                                                                            <th class="GroupSummaryLiceseTh">Licensed</th>
                                                                            <th class="GroupSummaryLiceseTh">Used</th>
                                                                            <th class="productServicePackWidth">Available</th>
                                                                            <th class="productServicePackWidth">Used (Hosted)</th>
                                                                            <th class="productServicePackWidth" style="width: 98px !important;">Used (Trunk)</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </th>
                                                    </tr>
                                              </thead>  
                                                
                                                
                                                <tbody>                                                    
                                                        <?php
                                                        ksort($finalEnterpriseServicesArr);
                                                        $licenseSystemUserData  = "";
                                                        if(!empty($finalEnterpriseServicesArr) && count($finalEnterpriseServicesArr) > 0)
                                                        {
                                                            foreach($finalEnterpriseServicesArr as $licenseServicePackName => $licenseServicePackRow)
                                                            {
                                                                if(!empty($licenseServicePackRow) && count($licenseServicePackRow) > 0)
                                                                {
                                                                    $totalServiceBasedOnSP = $totalSPCountArr[$licenseServicePackName];
                                                                    
                                                                    if($licenseArr['licenseSPQTYArr'][$licenseServicePackName] == "0"){
                                                                        $totalLicensed  = "unlimited";
                                                                        $totalAvailable = "unlimited";
                                                                    }else{
                                                                        $totalLicensed = $licenseArr['licenseSPQTYArr'][$licenseServicePackName];
                                                                        $totalAvailable = $totalLicensed - $totalServiceBasedOnSP;
                                                                    }
                                                                    
                                                                    $totalUsedTrunckCount = max($tmpArrForUsedTrunck[$licenseServicePackName]);
                                                                    $totalHostedTrunk     = $totalServiceBasedOnSP - $totalUsedTrunckCount;
                                                                    
                                                                    $rowColorStyleUserService = setServicePackRowColorBasedOnConsumedQunantity($totalLicensed, $totalServiceBasedOnSP, $warningThreshold, $attentionThreshold);
                                                                    
                                                                    echo "<tr class='' $rowColorStyleUserService ><td class='trunctingServiceTh' style='font-size: 14px !important;font-weight: 900;border: 1px solid #cccccc !important;'>$licenseServicePackName</td>";
                                                                    echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;";
                                                                    
                                                                    echo "<table class='productQuantityServiceTbl samurySystemUserTable divUserSerGroupSumLicInternalTable' style=''><tbody>";
                                                                    
                                                                    echo "<tr class='font-size: 14px !important;font-weight: 900;'>                                                                       
                                                                                <td class='trunctingServiceTh'>Total</td>
                                                                                <td class='GroupSummaryLiceseTh'>$totalLicensed</td>
                                                                                <td class='GroupSummaryLiceseTh'>$totalServiceBasedOnSP</td>
                                                                                <td class='productServicePackWidth'>$totalAvailable</td>
                                                                                <td class='productServicePackWidth'>$totalHostedTrunk</td>
                                                                                <td class='productServicePackWidth'>$totalUsedTrunckCount</td>
                                                                              </tr>"; 
                                                                    
                                                                    $licenseSystemUserData .= "$licenseServicePackName, Total, $totalLicensed,  $totalServiceBasedOnSP, $totalAvailable, $totalHostedTrunk, $totalUsedTrunckCount \n ";
                                                                    
                                                                    foreach($licenseServicePackRow as $licenseServiceName => $licenseServiceUsed)
                                                                    {                                                                        
                                                                        if($licenseArr['licenseServiceArr'][$licenseServiceName]['servicePackQty'] == "0"){
                                                                             $licensed  = "unlimited";       
                                                                             $available = "unlimited";
                                                                        }else{
                                                                             $licensed  = $licenseArr['licenseServiceArr'][$licenseServiceName]['servicePackQty'];
                                                                             $available = $licensed - $licenseServiceUsed;
                                                                        }
                                                                        $usedTrunk  = 0;
                                                                        if(isset($tmpServiceListOnUser[$licenseServiceName])){
                                                                            $usedTrunk  = $tmpServiceListOnUser[$licenseServiceName];
                                                                        }
                                                                        $usedHosted = $licenseServiceUsed - $usedTrunk;
                                                                        
                                                                        echo "<tr class='font-size: 14px !important;font-weight: 900;'>      
                                                                                <td class='trunctingServiceTh'>$licenseServiceName</td>
                                                                                <td class='GroupSummaryLiceseTh'>$licensed</td>
                                                                                <td class='GroupSummaryLiceseTh'>$licenseServiceUsed</td>
                                                                                <td class='productServicePackWidth'>$available</td>
                                                                                <td class='productServicePackWidth'>$usedHosted</td>
                                                                                <td class='productServicePackWidth'>$usedTrunk</td>
                                                                              </tr>";
                                                                        $licenseSystemUserData .= " , $licenseServiceName, $licensed,  $licenseServiceUsed, $available, $usedHosted, $usedTrunk \n ";
                                                                    }                                                                    
                                                                    echo "</tbody></table>";                                                                    
                                                                }                                                                
                                                                echo "</td></tr>";
                                                            }
                                                        }
                                                        ?>                                                        
                                                </tbody>
                                            </table>
                                            <?php                                    
                                                    $_SESSION['csvSystemSummaryUsageData'] .= "\n User Service Licenses \n";
                                                    $_SESSION['csvSystemSummaryUsageData'] .= "Service License Pack, Services, Licensed, Used, Available, Used (Hosted), Used (Trunk) \n";
                                                    $_SESSION['csvSystemSummaryUsageData'] .= $licenseSystemUserData;
                                            ?>
                                             <!--end content type start here -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>  
                        
                        <!--end user services -->
                        
                        <!--virtual service license -->
                            
                    <!--detailed div start -->                    
                  </div><!--end detailed div start --> 
                
                    
                    
                    <!--systemLevel Alert report -->
                     
                     <div id="divDetailedUsageAlertDiv" style="display:none;">
                     <div id ="divSystemLevelSumaryDetailedCSV">     
                         
                        <div class="row">
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <div id="csvUrlForSummaryUsageDiv">                              
                                <img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" onclick="location.href='serviceLicense/downloadLicenseReport.php?context=<?php echo $context; ?>&report=AlertReport' "/>
                                    <br>
                                    <span>Download<br>CSV</span>                                       
                                    <div id='MessageHolder'></div>
                                    <a href="#" id="testAnchor"></a>
                            </div>
                        </div>   
                        </div>
                         
                         
                    <!--purchase product ui start  -->  
                    
                    <?php 
                            if($alertProductImgFlag == "show"){ 
                    ?> 
                        <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="" class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse" aria-expanded="true" href="#divPurchaseProductDetailed"  aria-controls="divPurchaseProductDetailed">Purchased Products <?php echo $alertProductImgHtml; ?></a>
                                        </h4>
                                    </div>
                
                                    <div id="divPurchaseProductDetailed" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="divPurchaseProductDetailed">
                                        <div class="">
                                        <!--start content type here --> 
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct" id="">
                                                <thead>
                                                    <tr>
                                                        <th class="productNameWidth">Product Name</th>
                                                        <th class="productTypesWidth">Type</th>
                                                        <th class="productPurchageWidth">Purchased</th>
                                                        <th class="productConsumedWidth">Consumed</th>
                                                        <th class="productAvailableWidth">Available</th>
                                                        <th class="productRemainingWidth">% Remaining</th>

                                                        <th class="">
                                                            <table class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" style="margin-left: 3px;">  
                                                                <thead style="border: none;/* margin-bottom: -10px !important; */">
                                                                    <tr class="" style="border: none;margin-bottom: -7px;margin-top: 2px;">                                      
                                                                        <th class="" style="padding-left: 0px !important;">Service License Pack</th>
                                                                        <th class="">Quantity</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>  
                                                        </th>
                                                    </tr>
                                                </thead>
                            
                                                <tbody>                                                    
                                                <?php
                                                    
                                                    //ksort($licenseArr['licenseProductArr']);
                                                    $licenseSystemProductAlertData = "";
                                                    if(!empty($licenseArr['licenseProductArr']))
                                                    {                                   
                                                        
                                                        foreach($licenseArr['licenseProductArr'] as $productLicenseName => $productLicenseRow)
                                                        {
                                                                $productType = $productLicenseRow['productType'];
                                                                $productQty  = $productLicenseRow['productQty'];
                                                                
                                                                if($productType == "User Package" || $productType == "Trunk Package")
                                                                {
                                                                         if(isset($totalproductCountArr[$productLicenseName])){
                                                                             
                                                                             $consumedLicensed     = $totalproductCountArr[$productLicenseName];
                                                                             
                                                                             if($consumedLicensed >= $productQty){
                                                                                 $consumedLicensed          = $productQty;
                                                                                 $availableProductLicensed  = "0";
                                                                                 $remainPrctge              = "0.0";
                                                                             }else{
                                                                                 $availableProductLicensed  = $productQty - $consumedLicensed;
                                                                                 $remainPrctge              = ($availableProductLicensed * 100 ) / $productQty;    
                                                                                 $remainPrctge              = number_format($remainPrctge, 1);
                                                                             }
                                                                         }else{
                                                                             $consumedLicensed          = "0";
                                                                             $availableProductLicensed  = $productQty - $consumedLicensed;
                                                                             $remainPrctge              = ($availableProductLicensed * 100 ) / $productQty;    
                                                                             $remainPrctge              = number_format($remainPrctge, 1);
                                                                         }
                                                                         $rowColorStyleUsr = setServicePackRowColorBasedOnConsumedQunantityDetailedAlert($productQty, $consumedLicensed, $warningThreshold, $attentionThreshold);
                                                                         echo   "<tr $rowColorStyleUsr >";                                                                
                                                                         echo   "<td class='productNameWidth' style='font-size: 14px !important;font-weight: 900;'>$productLicenseName &nbsp;</td>";
                                                                         echo   "<td class='productTypesWidth'>$productType</td>
                                                                                 <td class='productPurchageWidth'>$productQty</td>";
                                                                         echo   "<td class='productConsumedWidth'>$consumedLicensed</td>
                                                                                 <td class='productAvailableWidth'>$availableProductLicensed</td>
                                                                                 <td class='productRemainingWidth'>$remainPrctge</td>";
                                                                         if($rowColorStyleUsr != "style='display:none;'"){
                                                                             $licenseSystemProductAlertData = "$productLicenseName, $productType, $productQty,  $consumedLicensed, $availableProductLicensed, $remainPrctge ";
                                                                         }
                                                                         
                                                                         
                                                                         if(!empty($productLicenseRow['productServicePackDetails']))
                                                                         {
                                                                             echo    "<td id='productQualityWidthTableTd'>
                                                                                     <table class='productQuantityServiceTbl' style='width: 100% !important; margin-top: -10px;'>
                                                                                     <tbody style=''>";
                                                                             $ctr0T = 1;
                                                                             foreach($productLicenseRow['productServicePackDetails'] as $productSPNameDetailsArr)
                                                                             {                                                                        
                                                                                 $productLicenseServicePackQty  = $productSPNameDetailsArr['quantity'];                                                                        
                                                                                 $productLicenseServicePackName = $productSPNameDetailsArr['productServicePackName'];
                                                                                 if($productLicenseServicePackQty == "0")
                                                                                 {
                                                                                     $productLicenseServicePackQty = "unlimited";
                                                                                 }
                                                                                 
                                                                                 
                                                                                 
                                                                                 

                                                                                 if($productLicenseServicePackName != "NONE")
                                                                                 {
                                                                                     $productLicenseServicePackNameTmp =  $productLicenseServicePackName;
                                                                                     $productLicenseServicePackQtyTmp  =  $productLicenseServicePackQty;
                                                                                     echo    "<tr><td class='' style='width: 40%;'>$productLicenseServicePackName</td><td class=''>$productLicenseServicePackQty</td></tr>";
                                                                                     if($rowColorStyle != "style='display:none;'")
                                                                                     {
                                                                                        if($ctr0T == "1" && $rowColorStyleUsr != "style='display:none;'")
                                                                                        {
                                                                                           $licenseSystemProductAlertData .= " , $productLicenseServicePackNameTmp, $productLicenseServicePackQtyTmp \n ";
                                                                                        }else{
                                                                                           if($rowColorStyleUsr != "style='display:none;'"){
                                                                                                $licenseSystemProductAlertData .= " , , , , , , $productLicenseServicePackNameTmp, $productLicenseServicePackQtyTmp \n ";
                                                                                           }
                                                                                        }    
                                                                                     }
                                                                                 }
                                                                                 else
                                                                                 { 
                                                                                     if(!empty($productLicenseRow['productServiceDetails']))
                                                                                     {
                                                                                         $productServiceDetailsArr = $productLicenseRow['productServiceDetails']['NONE'];
                                                                                         foreach($productServiceDetailsArr as $serviceDetailsRow)
                                                                                         {
                                                                                             $productLicenseServiceName = $serviceDetailsRow['productServiceName'];
                                                                                             $productLicenseServiceQty  = $serviceDetailsRow['quantity'];
                                                                                             if($productLicenseServiceQty == "0")
                                                                                             {
                                                                                                 $productLicenseServiceQty = "unlimited";
                                                                                             }
                                                                                             $productLicenseServicePackNameTmp =  $productLicenseServiceName;
                                                                                             $productLicenseServicePackQtyTmp  =  $productLicenseServiceQty;
                                                                                             if($rowColorStyleUsr != "style='display:none;'"){
                                                                                                $licenseSystemProductAlertData .= " , , , , , , $productLicenseServicePackNameTmp, $productLicenseServicePackQtyTmp \n ";
                                                                                             }
                                                                                             echo    "<tr><td class='' style='width: 40%;'>$productLicenseServiceName</td><td class=''>$productLicenseServiceQty</td></tr>";
                                                                                         }                                                                                
                                                                                     }
                                                                                 }
                                                                                 
                                                                                                                                                              
                                                                                 $ctrT = $ctrT + 1;
                                                                             }
                                                                             echo    "</tbody>
                                                                                     </table>                                                
                                                                                  </td>";
                                                                         }
                                                                         echo   "</tr>";

                                                                }
                                                                if($productType == "Trunk Package1")
                                                                {
                                                                         $consumedLicensed          = "N/A";
                                                                         $availableProductLicensed  = "N/A";
                                                                         $remainPrctge              = "N/A";

                                                                         echo   "<tr>";                                                                
                                                                         echo   "<td class='productNameWidth' style='font-size: 14px !important;font-weight: 900;'>$productLicenseName &nbsp;</td>";
                                                                         echo   "<td class='productTypesWidth'>$productType</td>
                                                                                 <td class='productPurchageWidth'>$productQty</td>";
                                                                         echo   "<td class='productConsumedWidth'>$consumedLicensed</td>
                                                                                 <td class='productAvailableWidth'>$availableProductLicensed</td>
                                                                                 <td class='productRemainingWidth'>$remainPrctge</td>";
                                                                         if($rowColorStyle != "style='display:none;'"){
                                                                            $licenseSystemProductAlertData .= "$productLicenseName, $productType, $productQty,  $consumedLicensed, $availableProductLicensed, $remainPrctge ";
                                                                         }
                                                                         if(!empty($productLicenseRow['productServicePackDetails']))
                                                                         {
                                                                             echo    "<td id='productQualityWidthTableTd'>
                                                                                     <table class='productQuantityServiceTbl' style='width: 100% !important; margin-top: -10px;'>
                                                                                     <tbody style=''>";
                                                                              $ctr01 = 1;
                                                                             foreach($productLicenseRow['productServicePackDetails'] as $productSPNameDetailsArr)
                                                                             {                                                                        
                                                                                 $productLicenseServicePackQty  = $productSPNameDetailsArr['quantity'];                                                                        
                                                                                 $productLicenseServicePackName = $productSPNameDetailsArr['productServicePackName'];
                                                                                 if($productLicenseServicePackQty == "0")
                                                                                 {
                                                                                     $productLicenseServicePackQty = "unlimited";
                                                                                 }
                                                                                 
                                                                                 

                                                                                 if($productLicenseServicePackName != "NONE")
                                                                                 {
                                                                                     $productLicenseServicePackNameTmp =  $productLicenseServicePackName;
                                                                                     $productLicenseServicePackQtyTmp  =  $productLicenseServicePackQty;
                                                                                     echo    "<tr><td class='' style='width: 40%;'>$productLicenseServicePackName</td><td class=''>$productLicenseServicePackQty</td></tr>";
                                                                                 }
                                                                                 else
                                                                                 { 
                                                                                     if(!empty($productLicenseRow['productServiceDetails']))
                                                                                     {
                                                                                         $productServiceDetailsArr = $productLicenseRow['productServiceDetails']['NONE'];
                                                                                         foreach($productServiceDetailsArr as $serviceDetailsRow)
                                                                                         {
                                                                                             $productLicenseServiceName = $serviceDetailsRow['productServiceName'];
                                                                                             $productLicenseServiceQty  = $serviceDetailsRow['quantity'];
                                                                                             if($productLicenseServiceQty == "0")
                                                                                             {
                                                                                                 $productLicenseServiceQty = "unlimited";
                                                                                             }
                                                                                             
                                                                                             $productLicenseServicePackNameTmp =  $productLicenseServiceName;
                                                                                             $productLicenseServicePackQtyTmp  =  $productLicenseServiceQty;
                                                                                             echo    "<tr><td class='' style='width: 40%;'>$productLicenseServiceName</td><td class=''>$productLicenseServiceQty</td></tr>";
                                                                                         }                                                                                
                                                                                     }
                                                                                 }
                                                                                 
                                                                                 if($rowColorStyle != "style='display:none;'"){
                                                                                    if($ctr01 == "1"){
                                                                                       $licenseSystemProductAlertData .= " , $productLicenseServicePackNameTmp, $productLicenseServicePackQtyTmp \n ";
                                                                                    }else{
                                                                                       $licenseSystemProductAlertData .= " , , , , , , $productLicenseServicePackNameTmp, $productLicenseServicePackQtyTmp \n ";
                                                                                    }
                                                                                 }
                                                                                 
                                                                                 $ctr01 = $ctr01 + 1;
                                                                             }
                                                                             echo    "</tbody>
                                                                                     </table>                                                
                                                                                  </td>";
                                                                         }
                                                                         echo   "</tr>";
                                                                }
                                                                
                                                                if($productType == "Add-On Package")
                                                                {
                                                                    $consumedLicensed          = "0";
                                                                    $availableProductLicensed  = "0";
                                                                    $remainPrctge              = "0";                                                                    
                                                                                                                                        
                                                                    if(!empty($productLicenseRow['productServiceDetails']))
                                                                    {  
                                                                        $productName = $productLicenseRow['productServiceDetails']['NONE']['0']['productServiceName'];
                                                                    }
                                                                    else
                                                                    {
                                                                        foreach($productLicenseRow['productServicePackDetails'] as $tpKey => $serviceRow)
                                                                        {
                                                                            $productName = $tpKey;
                                                                        }
                                                                    }
                                                                    if(1)
                                                                    {
                                                                        
                                                                        $tmpConsumedQty = array();
                                                                        if(count($productLicenseRow['productServiceDetails']['NONE']) > 1)
                                                                        {
                                                                            //echo "<br /> productServiceDetails - 1";
                                                                            $productServiceDetailsArr = $productLicenseRow['productServiceDetails']['NONE'];
                                                                            foreach($productServiceDetailsArr as $serviceDetailsRow1)
                                                                            {               
                                                                                    $productLicenseServiceName1 = $serviceDetailsRow1['productServiceName'];                                                                                        
                                                                                    $productLicenseServiceQty1  = $serviceDetailsRow1['quantity'];
                                                                                    if($productLicenseServiceQty1 == "0")
                                                                                    {

                                                                                    }
                                                                                    else{
                                                                                        $tmpConsumedQty[] = $finalEnterpriseServicesArr[$productLicenseServiceName1][$productLicenseServiceName1];
                                                                                        $consumedLicensed = max($tmpConsumedQty);
                                                                                    }
                                                                            }
                                                                        }  
                                                                        else
                                                                        {                                                                            
                                                                            if(count($productLicenseRow['productServicePackDetails']) > 1)
                                                                            {
                                                                                //echo "<br /> productServicePackDetails - 2";
                                                                                $productServicePackDetailsArr = $productLicenseRow['productServicePackDetails'];
                                                                                foreach($productServicePackDetailsArr as $servicePackDetailsRow1)
                                                                                {               
                                                                                        $productLicenseServicePackName1 = $servicePackDetailsRow1['productServicePackName'];                                                                                        
                                                                                        $productLicenseServicePackQty1  = $servicePackDetailsRow1['quantity'];
                                                                                        if($productLicenseServicePackQty1 == "0")
                                                                                        {

                                                                                        }
                                                                                        else{
                                                                                            $tmpConsumedQty[] = max($finalEnterpriseServicesArr[$productLicenseServicePackName1]);
                                                                                            $consumedLicensed = max($tmpConsumedQty);
                                                                                        }
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                foreach($finalEnterpriseServicesArr[$productName] as $valQty)
                                                                                {
                                                                                     $consumedLicensed = $valQty;
                                                                                }
                                                                            }
                                                                        }
                                                                        
                                                                        if($productQty != "0")
                                                                        {                                                                            
                                                                            if($consumedLicensed > $productQty)
                                                                            { 
                                                                                $consumedLicensed = $productQty;
                                                                            }
                                                                            
                                                                            $availableProductLicensed = $productQty - $consumedLicensed;
                                                                            if($availableProductLicensed != "0")
                                                                            {
                                                                                $remainPrctge             = ($availableProductLicensed * 100 ) / $productQty;    
                                                                                $remainPrctge             = number_format($remainPrctge, 1);
                                                                            }
                                                                            else
                                                                            {
                                                                                $availableProductLicensed = "0";
                                                                                $remainPrctge             = "0.0";
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            $availableProductLicensed  = "unlimited";
                                                                            $remainPrctge              = "unlimited";
                                                                        }
                                                                    }      
                                                                    
                                                                    
                                                                    $rowColorStyleAdOnUsr = setServicePackRowColorBasedOnConsumedQunantityDetailedAlert($productQty, $consumedLicensed, $warningThreshold, $attentionThreshold);
                                                                    
                                                                    echo   "<tr $rowColorStyleAdOnUsr >";                                                                
                                                                    echo   "<td class='productNameWidth' style='font-size: 14px !important;font-weight: 900;'>$productLicenseName &nbsp;</td>";
                                                                    echo   "<td class='productTypesWidth'>$productType</td>
                                                                            <td class='productPurchageWidth'>$productQty</td>";
                                                                    
                                                                    
                                                                    echo   "<td class='productConsumedWidth'>$consumedLicensed</td>
                                                                            <td class='productAvailableWidth'>$availableProductLicensed</td>
                                                                            <td class='productRemainingWidth'>$remainPrctge</td>"; 
                                                                    if($rowColorStyleAdOnUsr != "style='display:none;'"){
                                                                        $licenseSystemProductAlertData .= "$productLicenseName, $productType, $productQty,  $consumedLicensed, $availableProductLicensed, $remainPrctge ";
                                                                    }
                                                                    
                                                                    if(!empty($productLicenseRow['productServicePackDetails']))
                                                                    {
                                                                        echo    "<td id='productQualityWidthTableTd'>
                                                                                 <table class='productQuantityServiceTbl' style='width: 100% !important; margin-top: -10px;'>
                                                                                 <tbody style=''>";
                                                                        
                                                                        $ctr2 = 1;
                                                                        foreach($productLicenseRow['productServicePackDetails'] as $productSPNameDetailsArr)
                                                                        {                                                                        
                                                                            $productLicenseServicePackQty  = $productSPNameDetailsArr['quantity'];                                                                        
                                                                            $productLicenseServicePackName = $productSPNameDetailsArr['productServicePackName'];
                                                                            if($productLicenseServicePackQty == "0")
                                                                            {
                                                                                $productLicenseServicePackQty    = "unlimited";                                                                                
                                                                            }
                                                                            
                                                                            $productLicenseServicePackNameTmp = $productLicenseServicePackName;
                                                                            $productLicenseServicePackQtyTmp  = $productLicenseServicePackQty;
                                                                            
                                                                            if($productLicenseServicePackName != "NONE")
                                                                            {
                                                                                echo    "<tr><td class='' style='width: 40%;'>$productLicenseServicePackName</td><td class=''>$productLicenseServicePackQty</td></tr>";
                                                                                
                                                                            }
                                                                            else
                                                                            { 
                                                                                if(!empty($productLicenseRow['productServiceDetails']))
                                                                                {
                                                                                    $productServiceDetailsArr = $productLicenseRow['productServiceDetails']['NONE'];
                                                                                    foreach($productServiceDetailsArr as $serviceDetailsRow)
                                                                                    {
                                                                                        $productLicenseServiceName = $serviceDetailsRow['productServiceName'];                                                                                        
                                                                                        $productLicenseServiceQty  = $serviceDetailsRow['quantity'];
                                                                                        if($productLicenseServiceQty == "0")
                                                                                        {
                                                                                            $productLicenseServiceQty = "unlimited";
                                                                                        }
                                                                                        $productLicenseServicePackNameTmp = $productLicenseServiceName;
                                                                                        $productLicenseServicePackQtyTmp  = $productLicenseServiceQty;
                                                                                        //$licenseSystemProductSumData     .= " , , , , , , $productLicenseServicePackNameTmp, $productLicenseServicePackQtyTmp \n ";
                                                                                        //$ctr2 = $ctr2 + 1;
                                                                                        echo    "<tr><td class='' style='width: 40%;'>$productLicenseServiceName</td><td class=''>$productLicenseServiceQty</td></tr>";
                                                                                    }                                                                                
                                                                                }
                                                                            }
                                                                            
                                                                            if($ctr2 == "1" && $rowColorStyleAdOnUsr != "style='display:none;'"){
                                                                                $licenseSystemProductAlertData .= " , $productLicenseServicePackNameTmp, $productLicenseServicePackQtyTmp \n ";
                                                                            }else{
                                                                                if($rowColorStyleAdOnUsr != "style='display:none;'"){
                                                                                        $licenseSystemProductAlertData .= " , , , , , , $productLicenseServicePackNameTmp, $productLicenseServicePackQtyTmp \n ";
                                                                                }
                                                                            }
                                                                            $ctr2 = $ctr2 + 1;
                                                                             
                                                                             
                                                                        }
                                                                        echo    "</tbody>
                                                                                </table>                                                
                                                                             </td>";
                                                                    }
                                                                    echo   "</tr>";                                                                    
                                                           }    
                                                                
                                                        }
                                                    }
                                                ?>                                          
                                                <!--end error show -->
                                                </tbody>
                                            </table>
                                            <?php  
                                                $_SESSION['csvSystemAlertReportData']  = "";
                                                $_SESSION['csvSystemAlertReportData'] .= "\n Purchased Products \n";
                                                $_SESSION['csvSystemAlertReportData'] .= "Product Name, Type, Purchased, Consumed, Available, % Remaining, Service License Pack, Quantity  \n";
                                                $_SESSION['csvSystemAlertReportData'] .= $licenseSystemProductAlertData;
                                        ?>
                                            <!--end content type start here -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>       
                        <!--end purchase product2 ui start  -->
 
                        <!--Subscriber Licenses product ui start  -->   
                        <?php 
                        if($alertSubscribeImgFlag == "show"){ 
                        ?>
                            <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="" class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse" aria-expanded="true" href="#divSystemSubscriberLicDetailed"  aria-controls="divSystemSubscriberLicDetailed">Subscriber Licenses <?php echo $alertSubscribeImgHtml; ?></a>
                                        </h4>
                                    </div>

                                    <div id="divSystemSubscriberLicDetailed" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="divSystemSubscriberLicDetailed">
                                        <div class="">
                                        <!--start content type here --> 
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct" id="">
                                                <thead>
                                                    <tr>
                                                        <th class="productNameWidth">Type</th>
                                                        <th class="productServicePackWidth">Licensed</th>
                                                        <th class="productServicePackWidth">Used</th>
                                                        <th class="productServicePackWidth">Available</th>
                                                        <th class="productServicePackWidth">% Remaining</th>

                                                    </tr>
                                                </thead>

                                                <tbody>
                                                    <?php 
                                                            $subscriberLicenseArr['Success'] = array();
                                                            if(!empty($subscriberLicenseArr['Success']))
                                                            {
                                                                    $userLicenseArr         = $subscriberLicenseArr['Success']['User License'];
                                                                    $totalUserLicensed      = $userLicenseArr['Licensed'];
                                                                    $usedUserLicensed       = $userLicenseArr['Used'];
                                                                    $remainUserLicensed     = $userLicenseArr['Available'];
                                                                    if($totalUserLicensed != "Unlimited")
                                                                    {
                                                                            $remainUserPercentage  = ($remainUserLicensed * 100) / $totalUserLicensed;
                                                                            $remainUserPercentage  = number_format($remainUserPercentage, 2);
                                                                    }
                                                                    else
                                                                    {
                                                                            $remainUserPercentage = $totalUserLicensed;
                                                                    }

                                                                    $groupLicenseArr         = $subscriberLicenseArr['Success']['Group License'];
                                                                    $totalGroupLicensed      = $groupLicenseArr['Licensed'];
                                                                    $usedGroupLicensed       = $groupLicenseArr['Used'];
                                                                    $remainGroupLicensed     = $groupLicenseArr['Available'];
                                                                    if($totalGroupLicensed != "Unlimited")
                                                                    {
                                                                            $remainGroupPercentage  = ($remainGroupLicensed * 100) / $totalGroupLicensed;
                                                                            $remainGroupPercentage  = number_format($remainPercentage, 2);
                                                                    }
                                                                    else
                                                                    {
                                                                            $remainGroupPercentage  = $totalGroupLicensed;
                                                                    }	
                                                            }
                                                            else
                                                            {	
                                                                    $totalUserLicensed     = $licenseArr['userLicense'];
                                                                    $usedUserLicensed      = $userLicenseCount1 - $trunkUserLicenseCount1;
                                                                    if($totalUserLicensed != "unlimited")
                                                                    {
                                                                        $remainUserLicensed   = $totalUserLicensed - $usedUserLicensed;   
                                                                    }
                                                                    else
                                                                    {
                                                                            $remainUserLicensed   = $totalUserLicensed;
                                                                    }
                                                                    if($totalUserLicensed != "unlimited")
                                                                    {
                                                                            $remainUserPercentage  = ($remainUserLicensed * 100) / $totalUserLicensed;
                                                                            $remainUserPercentage  = number_format($remainUserPercentage, 2);
                                                                    }
                                                                    else
                                                                    {
                                                                            $remainUserPercentage = $totalUserLicensed;
                                                                    }


                                                                    $totalGroupLicensed = $licenseArr['groupLicense'];
                                                                    $usedGroupLicensed  = $groupLicenseCount1;
                                                                    if($totalGroupLicensed != "unlimited")
                                                                    {
                                                                            $remainGroupLicensed = $totalGroupLicensed - $usedGroupLicensed; 
                                                                    } 
                                                                    else 
                                                                    { 
                                                                            $remainGroupLicensed = $totalGroupLicensed; 
                                                                    }
                                                                    if($totalGroupLicensed != "unlimited")
                                                                    {
                                                                            $remainGroupPercentage  = ($remainUserLicensed * 100) / $totalGroupLicensed;
                                                                            $remainGroupPercentage  = number_format($remainGroupPercentage, 2);
                                                                    }
                                                                    else
                                                                    {
                                                                            $remainGroupPercentage = $totalGroupLicensed;
                                                                    }
                                                            }
                                                            $rowColorStyleUser       = setServicePackRowColorBasedOnConsumedQunantityDetailedAlert($totalUserLicensed, $usedUserLicensed, $warningThreshold, $attentionThreshold);
                                                            $rowColorStyleVUser      = setServicePackRowColorBasedOnConsumedQunantityDetailedAlert($licenseArr['virtualUserLicense'], $virtualLicenseCount1, $warningThreshold, $attentionThreshold);
                                                            $rowColorStyleTgroupUser = setServicePackRowColorBasedOnConsumedQunantityDetailedAlert($licenseArr['trunkGroupUserLicense'], $trunkUserLicenseCount1, $warningThreshold, $attentionThreshold);
                                                            $rowColorStyleGroup      = setServicePackRowColorBasedOnConsumedQunantityDetailedAlert($totalGroupLicensed, $usedGroupLicensed, $warningThreshold, $attentionThreshold);
                                                              
                                                    ?>
                                                     <tr class="puchagesSuccessRow" <?php echo $rowColorStyleUser; ?> >
                                                            <td class="productNameWidth">User Licenses</td>
                                                            <td class="productServicePackWidth"><?php echo $totalUserLicensed; ?></td>
                                                            <td class="productServicePackWidth"><?php echo $usedUserLicensed; ?></td>
                                                            <td class="productServicePackWidth"><?php echo $remainUserLicensed; ?></td>
                                                            <td class="productServicePackWidth"><?php echo $remainUserPercentage; ?></td>
                                                    </tr>

                                                     <tr class="puchagesSuccessRow" <?php echo $rowColorStyleVUser; ?> >
                                                        <td class="productNameWidth">Virtual Users</td>
                                                        <td class="productServicePackWidth"><?php echo $totalVirtualUserLicensed = $licenseArr['virtualUserLicense']; ?></td>
                                                        <td class="productServicePackWidth"><?php echo $virtualLicenseCount1;  ?></td>
                                                        <td class="productServicePackWidth">
                                                            <?php 
                                                            if($totalVirtualUserLicensed != "unlimited")
                                                                { 
                                                                        echo $remainVirtualUserCount = $totalVirtualUserLicensed - $virtualLicenseCount1;
                                                                        $remainVirtualLicensedTmp    = $remainVirtualUserCount;
                                                                } 
                                                                else 
                                                                { 
                                                                        echo $totalVirtualUserLicensed;
                                                                        $remainVirtualLicensedTmp    = $totalVirtualUserLicensed;
                                                                } 
                                                            ?></td>
                                                        <td class="productServicePackWidth">
                                                            <?php 
                                                                    if($totalVirtualUserLicensed != "unlimited"){                                                                        
                                                                        $remainPercentage       = ($remainVirtualUserCount * 100) / $totalVirtualUserLicensed;
                                                                        echo $remainPercentage  = number_format($remainPercentage, 2);
                                                                        $remainPercVirtual = $remainPercentage;
                                                                    }else{
                                                                        echo $totalVirtualUserLicensed;
                                                                        $remainPercVirtual = $totalVirtualUserLicensed;
                                                                    }
                                                            ?>
                                                        </td>
                                                    </tr>

                                                     <tr class="puchagesSuccessRow" <?php echo $rowColorStyleTgroupUser; ?> >
                                                        <td class="productNameWidth">Trunk Group Users</td>
                                                        <td class="productServicePackWidth"><?php echo $totalTrunkUserLicensed = $licenseArr['trunkGroupUserLicense']; ?></td>
                                                        <td class="productServicePackWidth"><?php echo $trunkUserLicenseCount1;  ?></td>
                                                        <td class="productServicePackWidth">
                                                            <?php 
                                                            if($totalTrunkUserLicensed != "unlimited")
                                                                { 
                                                                    echo $remainTrunkUserLicensed = $totalTrunkUserLicensed - $trunkUserLicenseCount1; 
                                                                    $remainTrunkUserLicensedTmp   = $remainTrunkUserLicensed;
                                                                } 
                                                                else 
                                                                { 
                                                                    echo $totalTrunkUserLicensed; 
                                                                    $remainTrunkUserLicensedTmp   = $totalTrunkUserLicensed;
                                                                } 
                                                             ?>
                                                        </td>
                                                        <td class="productServicePackWidth">
                                                            <?php 
                                                                    if($totalTrunkUserLicensed != "unlimited"){
                                                                        $remainPercentage       = ($remainTrunkUserLicensed * 100) / $totalTrunkUserLicensed;
                                                                        echo $remainPercentage  = number_format($remainPercentage, 2);
                                                                        $remainPercGroup = $remainPercentage;
                                                                        
                                                                    }else{
                                                                        echo $totalTrunkUserLicensed;
                                                                        $remainPercGroup = $totalTrunkUserLicensed;
                                                                    }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <tr class="puchagesSuccessRow" <?php echo $rowColorStyleGroup; ?> >
                                                            <td class="productNameWidth">Group Licenses</td>
                                                            <td class="productServicePackWidth"><?php echo $totalGroupLicensed; ?></td>
                                                            <td class="productServicePackWidth"><?php echo $usedGroupLicensed; ?></td>
                                                            <td class="productServicePackWidth"><?php echo $remainGroupLicensed; ?></td>
                                                            <td class="productServicePackWidth"><?php echo $remainGroupPercentage; ?></td>
                                                    </tr>
                                                    <!--end error show -->
                                                </tbody>
                                            </table>
                                             <!--end content type start here -->
                                             <?php                     
                                             if($rowColorStyleGroup != "style='display:none;'"){
                                                    $_SESSION['csvSystemAlertReportData'] .= "\n Subscriber Licenses \n";
                                                    $_SESSION['csvSystemAlertReportData'] .= "Type, Licensed, Used, Available, % Remaining  \n";
                                                    $_SESSION['csvSystemAlertReportData'] .= "User Licenses, $totalUserLicensed, $usedUserLicensed, $remainUserLicensed, $remainUserPercentage \n ";
                                                    $_SESSION['csvSystemAlertReportData'] .= "Virtual Users, $totalVirtualUserLicensed, $virtualLicenseCount1, $remainVirtualLicensedTmp , $remainPercVirtual \n ";
                                                    $_SESSION['csvSystemAlertReportData'] .= "Trunk Group Users, $totalTrunkUserLicensed, $trunkUserLicenseCount1,$remainTrunkUserLicensedTmp , $remainPercGroup \n ";
                                                    $_SESSION['csvSystemAlertReportData'] .= "Group Licenses, $totalGroupLicensed, $usedGroupLicensed, $remainGroupLicensed, $remainGroupPercentage \n ";
                                             }
                                             ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?> 
                        
                        
                        <?php 
                        if($alertGroupImgFlag == "show"){ 
                        ?>
                        <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="" class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse" aria-expanded="true" href="#divSystemGroupServiceLiceseDetailed" aria-controls="divSystemGroupServiceLiceseDetailed">Group Service Licenses <?php echo $alertGroupImgHtml; ?></a>
                                        </h4>
                                    </div>

                                    <div id="divSystemGroupServiceLiceseDetailed" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="divSystemGroupServiceLiceseDetailed">
                                        <div class="">
                                        <!--start content type here --> 
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct" id="">
                                               <thead>
                                                    <tr>

                                                        <th class="trunctingServiceTh" style="width: 16% !important;/* border: none; */">Service License Pack</th>
                                                            <th class="productQualityWidthTableTd" style="width: 74% !important;/* margin: 0 auto; */">
								<table class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" id="" style="border: none;margin-bottom: -9px;">

                                                      <!--    <th class="trunctingServiceTh" style="/* border: none; */">Service License Pack</th>
                                                            <th class="productQualityWidthTableTd" style="width: 100% !important;/* margin: 0 auto; */">
								<table border="1" class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" id="" style="border: none;margin-bottom: -9px;margin-left:3px !important;">
 -->
                                                                    <thead>
									<tr class="" style="border: none;margin-bottom: 0px;margin-top: 1px;">  
                                                                            <th class="trunctingServiceTh" style="width: 163px !important;">Service</th>
                                                                            <th class="productServicePackWidth">Licensed</th>
                                                                            <th class="productServicePackWidth">Used</th>
                                                                            <th class="productServicePackWidth" style="width: 90px !important;">Available</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </th>
                                                    </tr>
                                                </thead>
                                                <tbody>                                                   
                                                    <?php                                               
                                                        ksort($finalEnterpriseGroupServicesArr);
                                                        $licenseSystemGroupAlertData = "";
                                                        if(!empty($finalEnterpriseGroupServicesArr) && count($finalEnterpriseGroupServicesArr) > 0)
                                                        {
                                                            foreach($finalEnterpriseGroupServicesArr as $licenseGroupServicePackName => $licenseGroupServicePackRow)
                                                            {         
                                                                if($licenseGroupServicePackName != ""){
                                                                    /*echo "<tr><td class='trunctingServiceTh' style=''>$licenseGroupServicePackName</td>";
                                                                    echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;";*/
                                                                    if(!empty($licenseGroupServicePackRow) && count($licenseGroupServicePackRow) > 0)
                                                                    {
                                                                        $totalLicenseGroupServiceUsed = max($licenseGroupServicePackRow);
                                                                        
                                                                        if($licenseArr['licenseSPQTYArr'][$licenseGroupServicePackName] == "0"){
                                                                            $totalGroupLicensed          = "unlimited";
                                                                            $totalGroupLicensedAvailable = "unlimited";
                                                                        }else{
                                                                            $totalGroupLicensed          = $licenseArr['licenseSPQTYArr'][$licenseGroupServicePackName];
                                                                            $totalGroupLicensedAvailable = $totalGroupLicensed - $totalLicenseGroupServiceUsed;
                                                                        }
                                                                        $rowColorStyleGroupServices = setServicePackRowColorBasedOnConsumedQunantityDetailedAlert($totalGroupLicensed, $totalLicenseGroupServiceUsed, $warningThreshold, $attentionThreshold);
                                                                        
                                                                        
                                                                        echo "<tr $rowColorStyleGroupServices><td class='trunctingServiceTh' style=''>$licenseGroupServicePackName</td>";
                                                                        echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;";
                                                                        
                                                                        echo "<table class='productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese'><tbody>";
                                                                        
                                                                        
                                                                        echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>Total</td>
                                                                                    <td class='productServicePackWidth'>$totalGroupLicensed</td>
                                                                                    <td class='productServicePackWidth'>$totalLicenseGroupServiceUsed</td>
                                                                                    <td class='productServicePackWidth'>$totalGroupLicensedAvailable</td>
                                                                              </tr>";
                                                                        if($rowColorStyleGroupServices != "style='display:none;'"){
                                                                            $licenseSystemGroupAlertData .= "$licenseGroupServicePackName, Total, $totalGroupLicensed,  $totalLicenseGroupServiceUsed, $totalGroupLicensedAvailable \n ";
                                                                        }
                                                                        foreach($licenseGroupServicePackRow as $licenseGroupServiceName => $licenseGroupServiceUsed)
                                                                        {
                                                                            
                                                                            if($licenseArr['licenseServiceArr'][$licenseGroupServiceName]['servicePackQty'] == "0"){
                                                                                $groupLicensed  = "unlimited";       
                                                                                $groupAvailable = "unlimited";
                                                                            }else{
                                                                                $groupLicensed  = $licenseArr['licenseServiceArr'][$licenseGroupServiceName]['servicePackQty'];
                                                                                $groupAvailable = $groupLicensed - $licenseGroupServiceUsed;
                                                                            }
                                                                            
                                                                            echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>$licenseGroupServiceName</td>
                                                                                    <td class='productServicePackWidth'>$groupLicensed</td>
                                                                                    <td class='productServicePackWidth'>$licenseGroupServiceUsed</td>
                                                                                    <td class='productServicePackWidth'>$groupAvailable</td>
                                                                                  </tr>";
                                                                            if($rowColorStyleGroupServices != "style='display:none;'"){
                                                                                $licenseSystemGroupAlertData .= "  , $licenseGroupServiceName,  $groupLicensed, $licenseGroupServiceUsed, $groupAvailable \n ";
                                                                            }
                                                                        }                                                                    
                                                                        echo "</tbody></table>";                                                                    
                                                                    }                                                                
                                                                    echo "</td></tr>";
                                                                }
                                                            }
                                                        }
                                                        ?>                                                    
                                                </tbody>                                                 
                                            </table>
                                        <?php                                    
                                                    $_SESSION['csvSystemAlertReportData'] .= "\n Group Service Licenses \n";
                                                    $_SESSION['csvSystemAlertReportData'] .= "Service License Pack, Services, Licensed, Used, Available  \n";
                                                    $_SESSION['csvSystemAlertReportData'] .= $licenseSystemGroupAlertData;
                                            ?>
                                             <!--end content type start here -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <?php } ?>
                        
                        <?php 
                        if($alertVirtualImgFlag == "show"){ 
                        ?>
                        <!--data vertual services -->
                        <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="" class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse" aria-expanded="true" href="#divSystemVirtualServiceLicseDetailed" aria-controls="divSystemVirtualServiceLicseDetailed">Virtual Service Licenses <?php echo $alertVirtualImgHtml ?></a>
                                        </h4>
                                    </div>

                                    <div id="divSystemVirtualServiceLicseDetailed" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="divSystemVirtualServiceLicseDetailed">
                                        <div class="">
                                        <!--start content type here --> 
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct" id="">
                                                <thead>
                                                    <tr>
                                                        <th class="trunctingServiceTh" style="width: 0%;/* border: none; */">Service License Pack</th>
                                                            <th class="productQualityWidthTableTd" style="width: 100%;margin:1px;">

								<table class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" id="" style="border: none;margin-bottom: -9px;margin-left:3px !important;">

							<!--	<table border="1" class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" id="" style="border: none;margin-bottom: -9px;">-->

                                                                    <thead>
									<tr class="" style="border: none;margin-bottom: 0px;margin-top: 1px;">  
                                                                           <th class="trunctingServiceTh" style="width: 158px !important;">Service</th>
                                                                            <th class="productServicePackWidth">Used</th>
                                                                            <th class="productServicePackWidth">Used (Hosted)</th>
                                                                            <th class="productServicePackWidth" style="width: 98px !important;">Used (Trunk)</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </th>

                                                        <!-- <th class="trunctingServiceTh" style=" width: 24% !important;">Service</th>
                                                        <th class="GroupSummaryLiceseTh">Used</th>
                                                        <th class="productServicePackWidth">Used (Hosted)</th>
                                                        <th class="productServicePackWidth">Used (Trunk)</th>-->
                                                    </tr>
                                              </thead>                                               
                                                <tbody>
                                                    <?php                                               
                                                        ksort($finalEnterpriseVirtualServicesArr);
                                                        $licenseSystemVirtualAlertData = "";
                                                        if(!empty($finalEnterpriseVirtualServicesArr) && count($finalEnterpriseVirtualServicesArr) > 0)
                                                        {
                                                            foreach($finalEnterpriseVirtualServicesArr as $licenseVirtualServicePackName => $licenseVirtualServicePackRow)
                                                            {         
                                                                if($licenseVirtualServicePackName != ""){
                                                                    /*echo "<tr class=''><td class='trunctingServiceTh' style='font-size: 14px !important;font-weight: 900;border: 1px solid #cccccc !important;'>$licenseVirtualServicePackName</td>";
                                                                    echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;"; */
                                                                    if(!empty($licenseVirtualServicePackRow) && count($licenseVirtualServicePackRow) > 0)
                                                                    {
                                                                        $totalLicenseVirtualServiceUsed = max($licenseVirtualServicePackRow);
                                                                        
                                                                        if($licenseArr['licenseSPQTYArr'][$licenseVirtualServicePackName] == "0"){
                                                                            $totalVirtualLicensed          = "unlimited";
                                                                            $totalVirtualLicensedAvailable = "unlimited";
                                                                        }else{
                                                                            $totalVirtualLicensed          = $licenseArr['licenseSPQTYArr'][$licenseVirtualServicePackName];
                                                                            $totalVirtualLicensedAvailable = $totalVirtualLicensed - $licenseVirtualServicePackName;
                                                                        }
                                                                        
                                                                        $rowColorStyleVirtualServices = setServicePackRowColorBasedOnConsumedQunantityDetailedAlert($totalVirtualLicensed, $totalLicenseVirtualServiceUsed, $warningThreshold, $attentionThreshold);
                                                                        
                                                                        echo "<tr class='' $rowColorStyleVirtualServices ><td class='trunctingServiceTh' style='font-size: 14px !important;font-weight: 900;border: 1px solid #cccccc !important;'>$licenseVirtualServicePackName</td>";
                                                                        echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;";
                                                                        
                                                                        echo "<table class='productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese' style='margin-top: 0px;'><tbody>";
                                                                        
                                                                        echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>Total</td>
                                                                                    <td class='productServicePackWidth'>$totalVirtualLicensed</td>
                                                                                    <td class='productServicePackWidth'>$totalLicenseVirtualServiceUsed</td>
                                                                                    <td class='productServicePackWidth'>$totalVirtualLicensedAvailable</td>
                                                                              </tr>";
                                                                        
                                                                        if($rowColorStyleVirtualServices != "style='display:none;'"){
                                                                            $licenseSystemVirtualAlertData .= "$licenseVirtualServicePackName, Total, $totalVirtualLicensed,  $totalLicenseVirtualServiceUsed, $totalVirtualLicensedAvailable \n ";
                                                                        }
                                                                        foreach($licenseVirtualServicePackRow as $licenseVirtualServiceName => $licenseVirtualServiceUsed)
                                                                        {
                                                                            if($licenseArr['licenseServiceArr'][$licenseVirtualServiceName]['servicePackQty'] == "0"){
                                                                                $virtualLicensed  = "unlimited";       
                                                                                $virtualAvailable = "unlimited";
                                                                            }else{
                                                                                $virtualLicensed  = $licenseArr['licenseServiceArr'][$licenseVirtualServiceName]['servicePackQty'];
                                                                                $virtualAvailable = $virtualLicensed - $licenseVirtualServiceUsed;
                                                                            }
                                                                            echo "<tr class=''>                                                                        
                                                                                    <td class='trunctingServiceTh'>$licenseVirtualServiceName</td>
                                                                                    <td class='productServicePackWidth'>$virtualLicensed</td>
                                                                                    <td class='productServicePackWidth'>$licenseVirtualServiceUsed</td>
                                                                                    <td class='productServicePackWidth'>$virtualAvailable</td>
                                                                                  </tr>";
                                                                            if($rowColorStyleVirtualServices != "style='display:none;'"){
                                                                                $licenseSystemVirtualAlertData .= "  , $licenseVirtualServiceName,  $virtualLicensed, $licenseVirtualServiceUsed, $virtualAvailable \n ";
                                                                            }
                                                                        }                                                                    
                                                                        echo "</tbody></table>";                                                                    
                                                                    }                                                                
                                                                    echo "</td></tr>";
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                </tbody>
                                            </table>
                                             <!--end content type start here -->
                                             <?php                                    
                                                    $_SESSION['csvSystemAlertReportData'] .= "\n Virtual Service Licenses \n";
                                                    $_SESSION['csvSystemAlertReportData'] .= "Service License Pack, Services, Licensed, Used, Available  \n";
                                                    $_SESSION['csvSystemAlertReportData'] .= $licenseSystemVirtualAlertData;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <?php } ?>
                        
                        <!--end vertual group services -->
                        <!--group context user services form sumarry view -->
                        <?php 
                        if($alertUserImgFlag == "show"){ 
                        ?>
                        <div class="row uiModalAccordian form-group" style="display: block;">
                            <div class="">
                                <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                                    <div class="panel-heading" role="tab" id="">
                                        <h4 class="panel-title">
                                            <a id="" class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse" aria-expanded="true" href="#divUserServiceGroupSummaryLiceseDetailed" aria-controls="divUserServiceGroupSummaryLiceseDetailed">User Service Licenses <?php echo $alertUserImgHtml; ?></a>
                                        </h4>
                                    </div>

                                    <div id="divUserServiceGroupSummaryLiceseDetailed" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="divUserServiceGroupSummaryLiceseDetailed">
                                        <div class="">
                                        <!--start content type here --> 
                                            <table class="dataTable leftDesc tablesorter scroll tagTableHeight entPurchageProduct divUserServiceGroupSummaryLicese" id="">
                                               <thead>
                                                    <tr>
                                                        <th class="trunctingServiceTh" style="width: 0%;/* border: none; */">Service License Pack</th>
                                                            <th class="productQualityWidthTableTd" style="width: 100%;margin: 0 auto;">
								<table class="productQuantityServiceTbl samurySystemUserTable divUserServiceGroupSummaryLicese" id="" style="border: none;margin-bottom: -9px;margin-left:3px !important;">
                                                                    <thead>
									<tr class="" style="border: none;margin-bottom: 1px;margin-top: 2px;">  
                                                                         <th class="trunctingServiceTh" style="width: 159px !important;">Service</th>
                                                                            <th class="GroupSummaryLiceseTh">Licensed</th>
                                                                            <th class="GroupSummaryLiceseTh">Used</th>
                                                                            <th class="productServicePackWidth">Available</th>
                                                                            <th class="productServicePackWidth">Used (Hosted)</th>
                                                                            <th class="productServicePackWidth" style="width: 98px !important;">Used (Trunk)</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </th>
                                                    </tr>
                                              </thead>
                                                
                                                
                                                <tbody>                                                    
                                                        <?php
                                                        ksort($finalEnterpriseServicesArr);
                                                        $licenseSystemUserAlertData = "";
                                                        if(!empty($finalEnterpriseServicesArr) && count($finalEnterpriseServicesArr) > 0)
                                                        {
                                                            foreach($finalEnterpriseServicesArr as $licenseServicePackName => $licenseServicePackRow)
                                                            {
                                                                
                                                                /*echo "<tr class=''><td class='trunctingServiceTh' style='font-size: 14px !important;font-weight: 900;border: 1px solid #cccccc !important;'>$licenseServicePackName</td>";
                                                                echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;";*/
                                                                
                                                                if(!empty($licenseServicePackRow) && count($licenseServicePackRow) > 0)
                                                                {
                                                                    $totalServiceBasedOnSP = $totalSPCountArr[$licenseServicePackName];
                                                                    
                                                                    if($licenseArr['licenseSPQTYArr'][$licenseServicePackName] == "0"){
                                                                        $totalLicensed  = "unlimited";
                                                                        $totalAvailable = "unlimited";
                                                                    }else{
                                                                        $totalLicensed = $licenseArr['licenseSPQTYArr'][$licenseServicePackName];
                                                                        $totalAvailable = $totalLicensed - $totalServiceBasedOnSP;
                                                                    }
                                                                    
                                                                    $totalUsedTrunckCount = max($tmpArrForUsedTrunck[$licenseServicePackName]);
                                                                    $totalHostedTrunk     = $totalServiceBasedOnSP - $totalUsedTrunckCount;
                                                                    
                                                                    $rowColorStyleUserService = setServicePackRowColorBasedOnConsumedQunantityDetailedAlert($totalLicensed, $totalServiceBasedOnSP, $warningThreshold, $attentionThreshold);
                                                                    
                                                                    echo "<tr class='' $rowColorStyleUserService ><td class='trunctingServiceTh' style='font-size: 14px !important;font-weight: 900;border: 1px solid #cccccc !important;'>$licenseServicePackName</td>";
                                                                    echo "<td id='productQualityWidthTableTd' style='width:100% !important ;'>&nbsp;";
                                                                    
                                                                    echo "<table class='productQuantityServiceTbl samurySystemUserTable divUserSerGroupSumLicInternalTable' style=''><tbody>";
                                                                    
                                                                    echo "<tr class='font-size: 14px !important;font-weight: 900;'>                                                                       
                                                                                <td class='trunctingServiceTh'>Total</td>
                                                                                <td class='GroupSummaryLiceseTh'>$totalLicensed</td>
                                                                                <td class='GroupSummaryLiceseTh'>$totalServiceBasedOnSP</td>
                                                                                <td class='productServicePackWidth'>$totalAvailable</td>
                                                                                <td class='productServicePackWidth'>$totalHostedTrunk</td>
                                                                                <td class='productServicePackWidth'>$totalUsedTrunckCount</td>
                                                                              </tr>"; 
                                                                    
                                                                    if($rowColorStyleUserService != "style='display:none;'"){
                                                                        $licenseSystemUserAlertData .= "$licenseServicePackName, Total, $totalLicensed,  $totalServiceBasedOnSP, $totalAvailable, $totalHostedTrunk, $totalUsedTrunckCount \n ";
                                                                    }
                                                                    foreach($licenseServicePackRow as $licenseServiceName => $licenseServiceUsed)
                                                                    {                                                                        
                                                                        if($licenseArr['licenseServiceArr'][$licenseServiceName]['servicePackQty'] == "0"){
                                                                             $licensed  = "unlimited";       
                                                                             $available = "unlimited";
                                                                        }else{
                                                                             $licensed  = $licenseArr['licenseServiceArr'][$licenseServiceName]['servicePackQty'];
                                                                             $available = $licensed - $licenseServiceUsed;
                                                                        }
                                                                        $usedTrunk  = 0;
                                                                        if(isset($tmpServiceListOnUser[$licenseServiceName])){
                                                                            $usedTrunk  = $tmpServiceListOnUser[$licenseServiceName];
                                                                        }
                                                                        $usedHosted = $licenseServiceUsed - $usedTrunk;
                                                                        
                                                                        echo "<tr class='font-size: 14px !important;font-weight: 900;'>      
                                                                                <td class='trunctingServiceTh'>$licenseServiceName</td>
                                                                                <td class='GroupSummaryLiceseTh'>$licensed</td>
                                                                                <td class='GroupSummaryLiceseTh'>$licenseServiceUsed</td>
                                                                                <td class='productServicePackWidth'>$available</td>
                                                                                <td class='productServicePackWidth'>$usedHosted</td>
                                                                                <td class='productServicePackWidth'>$usedTrunk</td>
                                                                              </tr>";
                                                                        if($rowColorStyleUserService != "style='display:none;'"){
                                                                            $licenseSystemUserAlertData .= " , $licenseServiceName, $licensed,  $licenseServiceUsed, $available, $usedHosted, $usedTrunk \n ";
                                                                        }
                                                                    }                                                                    
                                                                    echo "</tbody></table>";                                                                    
                                                                }                                                                
                                                                echo "</td></tr>";
                                                            }
                                                        }
                                                        ?>                                                        
                                                </tbody>
                                            </table>
                                             <!--end content type start here -->
                                             <?php                                    
                                                    $_SESSION['csvSystemAlertReportData'] .= "\n User Service Licenses \n";
                                                    $_SESSION['csvSystemAlertReportData'] .= "Service License Pack, Services, Licensed, Used, Available, Used (Hosted), Used (Trunk) \n";
                                                    $_SESSION['csvSystemAlertReportData'] .= $licenseSystemUserAlertData;
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     
                        <?php } ?>
                     
                     </div>  
                        
                        <!--end user services -->
                        
                        <!--virtual service license -->
                        

                   
                            
                    <!--detailed div start -->                    
                  </div><!--end detailed div start --> 
                    
                    <!--end systemlevel detailed alert table record -->
                    
                    
                    </div>  <!--end purchase product ui start  -->     

                    <!--download csv hidden record for ie -->
<!-- </div> -->
  
<!--end -->
<?php
    }
 ?>
