$(function() {
	$("#addNewSubmenu").hide();
	$("#newSubMenu").hide();
	
	$("#module").html("> Modify Auto Attendant: ");
	$("#endUserId").html("");
	$('#addAutoAttendant').hide();
	$("#tabs").tabs();
	$("#businessHoursScheduleDisplay").hide();
	$("#holidayScheduleDisplay").hide();
	
	var getServiceAssignAA = function(){
		
		var aaBasicServiceAssign = $("#aaBasicServiceAssign").val();
		var aaStandardServiceAssign = $("#aaStandardServiceAssign").val();
		//var aaTypeVal = $("#aaType").val();
		
		if(aaBasicServiceAssign == "false"){
			$("#aaType option[value=Basic]").hide();
		}
		if(aaStandardServiceAssign == "false"){
			$("#aaType option[value=Standard]").hide();
		}

		if(aaBasicServiceAssign == "false" && aaStandardServiceAssign == "false"){
			$("#aaType option[value=Basic]").hide()
			$("#aaType option[value=Standard]").hide()
			$("#dialogAutoAttendant").dialog("option", "title",	"Request Complete");
			$("#dialogAutoAttendant").html('Auto Attendant Services ( Basic / Standard ) not assigned to the group');
			$("#dialogAutoAttendant").dialog("open");
			$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
			$(".ui-dialog-buttonpane button:contains('Complete')").button().hide().addClass('subButton');;
			$(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide().addClass('returnToMainButton');;
		}
		
	};
	
	$('#newAttendant').click(function() {
		$('#autoAttendantadd').show();
		$('#addAutoAttendant').show();
		$('#AAData').hide();
		$("#AAChoice").val("");
		$('#autoAttendantadd')[0].reset();
		$('#aaType').prop('disabled', false);
		$("#subButtonDel").hide();
		$("#tabs").tabs('enable', 0);
		$("#tabs").tabs({
			active : 0
		});
		$("#tabs").tabs('disable', 1);
		$("#tabs").tabs('disable', 2);
		$("#tabs").tabs('disable', 3);
		$("#tabs").tabs('disable', 4);
		$("#tabs").tabs('disable', 5);
		// $("#tabs").tabs('disable', 6);
		$("#holidayMenu").show();
		// $("#holiday_menu").show();
		getAllSchedule();
		getDepartmentList();
		getTimeZoneList();
		allMenuData();
		
		$("#autoAttendantNameDiv").show();
		$("#autoAttendantClidFDiv").show();
		$("#autoAttendantClidLDiv").show();
		getServiceAssignAA();
		
		/* hide submenu button */
		$("#addNewSubmenu").hide();
	});

	$('#aaCancel').click(function() {
		 //getAAList(0);
		$("#AAChoice").val("");	
		$('#modifyAttendant').show();
		$('#autoAttendantadd').hide();
		$('#AAData').hide();
		$('#aaInfo').hide();
	});

	$('#backTomainForm').click(function() {
		$('#modifyAttendant').show();
		$('#addAutoAttendant').hide();
	})

	var getAllSchedule = function() {
		var html = "<option value=''>None</option>";
		var data = "<option value=''>None</option>";
		$.ajax({
			type : "POST",
			url : "autoAttendant/getAllSchedule.php",
			success : function(result) {
				var obj = jQuery.parseJSON(result);
				for (var i = 0; i < obj.Success.holiday.length; i++) {
					html += '<option value="' + obj.Success.holiday[i][0]
							+ '">' + obj.Success.holiday[i][0] + '</option>';
				}
				for (var i = 0; i < obj.Success.timeSchedule.length; i++) {
					data += '<option value="' + obj.Success.timeSchedule[i][0]
							+ '">' + obj.Success.timeSchedule[i][0]
							+ '</option>';
				}
				$("#holidaySchedule").html(html);
				$("#businessHoursSchedule").html(data);
			}
		});
	}

	var getDepartmentList = function() {
		var html = "<option value=''>None</option>";
		$
				.ajax({
					type : "POST",
					url : "autoAttendant/getDepartmentList.php",
					success : function(result) {
						var obj = jQuery.parseJSON(result);
						for (var i = 0; i < obj.Success.department.length; i++) {
							html += '<option value="'
									+ obj.Success.department[i]["name"] + '">'
									+ obj.Success.department[i]["fpName"]
									+ '</option>';
						}
						$("#department").html(html);
					}
				});
	}

	var getTimeZoneList = function() {
		var html = "<option value=''>None</option>";
		var defaultTimeZone = "";
		$.ajax({
			type : "POST",
			url : "autoAttendant/timeZoneList.php",
			success : function(result) {
				var obj = jQuery.parseJSON(result);
				if(obj.defaultVal){
					defaultTimeZone = obj.defaultVal;
				}
				for (var i = 0; i < obj.Success.length; i++) {
					html += '<option value="' + obj.Success[i]["name"] + '">'
							+ obj.Success[i]["displayName"] + '</option>';
				}
				$("#timeZone").html(html);
				$("#timeZone option[value='"+defaultTimeZone+"']").attr("selected","selected") ;
			}
		});
	}

	var allMenuData = function() {
		var html = "<option value=''>None</option>";
		var data = "";
		var ahData = "";
		var hdData = "";
		$.ajax({
			type : "POST",
			url : "autoAttendant/allMenuData.php",
			success : function(result) {
				var obj = jQuery.parseJSON(result);
				for (var i = 0; i < obj.menuData.length; i++) {
					html += '<option value="' + obj.menuData[i] + '">'
							+ obj.menuData[i] + '</option>';
				}
				for (var j = 0; j < 12; j++) {
					data = "#bhData" + [ j ];
					ahData = "#ahData" + [ j ];
					hdData = "#hdData" + [ j ];
					$(data).html(html);
					$(ahData).html(html);
					$(hdData).html(html);
				}
			}
		});
	}

	var serviceList = function(AAid) {
		var html = "";
		var data = "";
		$.ajax({
			type : "POST",
			url : "autoAttendant/serviceData.php",
			data : {
				AAid : AAid
			},
			success : function(result) {
				var obj = jQuery.parseJSON(result);
				if (obj.assigned.Success) {
					for (var j = 0; j < obj.assigned.Success.length; j++) {
						data += '<option value="' + obj.assigned.Success[j]
								+ '">' + obj.assigned.Success[j] + '</option>';
					}
				}
				if (obj.avail) {
					for (var i = 0; i < obj.avail.length; i++) {
						html += '<option value="' + obj.avail[i] + '">'
								+ obj.avail[i] + '</option>';
					}
				}
				$("#assignedServices").html(data);
				$("#availableServices").html(html);
			}
		});
	}

	var aaChoiceFunc = function() {
		$("#AAData").html("");
		$("#AAData").hide("");
		var id = $("#AAChoice").val();

		if (id !== "") {
			$.ajax({
				type : "POST",
				url : "autoAttendant/aaInfo.php",
				data : {
					AAId : id
				},
				success : function(result) {
					if(foundServerConErrorOnProcess(result, "")) {
    					return false;
                  	}
					serviceList(id);
					setTimeout(function(){ 
						setAnnouncementAutoattendantFields(); 
						$("#AAData").show();
					}, 5000);
					
					getCallPolicies(id);

					$('#addAutoAttendant').hide();
					
					$("#AAData").html(result);
					if($(result).find('#aaType option:selected').val() == "Standard") {
						$("#addNewSubmenu").show();
					}
					// added by jeetesh to detect the change of auto attendant
					getAnnouncementList(1);//business hours announcement list
					getAnnouncementList(3);//holidays hours announcement list
					getAnnouncementList(2);//after hours announcement list
					getAnnouncementList(4);//voice portal announcement list
					//getAnnouncementList(5);//voice portal announcement list
					getAnnouncementList(6);
					$("#newSubMenu").hide();
				}
			});
		} else {
			$("#endUserId").html("");
		}
	}
	
	$('#addNewSubmenu').click(function() {
		$("#newSubMenu").show();
		$("#newSubMenuATag").trigger("click");
		$("#checkNewSubMenuStatus").val("true");
	});

	$("#AAChoice").change(function() {
		$("#addNewSubmenu").hide();
		$("#newSubMenu").hide();
		aaChoiceFunc();
	});

	/*
	 * $("#AAChoice").change(function() { $("#AAData").html("");
	 * getAllSchedule(); getDepartmentList(); getTimeZoneList(); allMenuData();
	 * var id = $("#AAChoice").val(); $("#loading2").show();
	 * $('#addAutoAttendant').hide(); $('#newAttendant').prop('disabled', true);
	 * $("#newAttendant").css("background","gray"); var key = ""; var val = "";
	 * if (id !== "") { $.ajax({ type: "POST", url:
	 * "autoAttendant/autoAttendantInfo.php", data: { AAId: id }, success:
	 * function(result) {debugger; $('#newAttendant').prop('disabled', false);
	 * $("#newAttendant").css("background","black"); $("#loading2").hide();
	 * $('#addAutoAttendant').show(); $("#subButtonDel").show(); var obj =
	 * jQuery.parseJSON(result); if(obj.Error){ alert("No Data Found"); return
	 * false; }else{ if(obj.Success){ if(obj.Success.basicInfo.aaName){
	 * $("#aaName").val(obj.Success.basicInfo.aaName); }
	 * if(obj.Success.basicInfo.callingLineIdLastName){
	 * $("#callingLineIdLastName").val(obj.Success.basicInfo.callingLineIdLastName); }
	 * if(obj.Success.basicInfo.callingLineIdFirstName){
	 * $("#callingLineIdFirstName").val(obj.Success.basicInfo.callingLineIdFirstName); }
	 * if(obj.Success.basicInfo.type){
	 * $("#aaType").val(obj.Success.basicInfo.type);
	 * $('#aaType').prop('disabled', 'disabled'); if(obj.Success.basicInfo.type ==
	 * "Basic"){ $("#businessHoursScheduleDisplay").show();
	 * $("#holidayScheduleDisplay").show(); $("#holidayMenu").show();
	 * //$("#holiday_menu").show(); }else{
	 * $("#businessHoursScheduleDisplay").hide();
	 * $("#holidayScheduleDisplay").hide(); $("#holidayMenu").hide();
	 * //$("#holiday_menu").hide(); } } if(obj.Success.basicInfo.department){
	 * $("#department").val(obj.Success.basicInfo.department); }
	 * if(obj.Success.basicInfo.timeZone){
	 * $("#timeZone").val(obj.Success.basicInfo.timeZone); }
	 * if(obj.Success.basicInfo.extensionDialingScope){
	 * $("#extensionDialing").val(obj.Success.basicInfo.extensionDialingScope); }
	 * if(obj.Success.basicInfo.extensionDialingScope){
	 * $("#extensionDialing").val(obj.Success.basicInfo.extensionDialingScope); }
	 * if(obj.Success.basicInfo.nameDialingScope){
	 * $("#nameDialing").val(obj.Success.basicInfo.nameDialingScope); }
	 * if(obj.Success.basicInfo.nameDialingEntries){
	 * $("#nameDialingEntries").val(obj.Success.basicInfo.nameDialingEntries); }
	 * if(obj.Success.basicInfo.businessHours){
	 * $("#businessHoursSchedule").val(obj.Success.basicInfo.businessHours); }
	 * if(obj.Success.basicInfo.holidaySchedule){
	 * $("#holidaySchedule").val(obj.Success.basicInfo.holidaySchedule); }
	 * if(obj.Success.basicInfo.enableVideo){
	 * if(obj.Success.basicInfo.enableVideo == "true"){
	 * $('#eVsupport').prop('checked', true); }else{
	 * $('#eVsupport').prop('checked', false); }
	 *  } if(obj.Success.allMenuData.bhData){ for(var i = 0; i <
	 * obj.Success.allMenuData.bhData.length; i++){ key =
	 * obj.Success.allMenuData.bhData[i].key; val =
	 * obj.Success.allMenuData.bhData[i].action; var keyData = "#bhData"+key;
	 * $(keyData).val(val); } } if(obj.Success.allMenuData.ahData){ for(var i =
	 * 0; i < obj.Success.allMenuData.ahData.length; i++){ key =
	 * obj.Success.allMenuData.ahData[i].key; val =
	 * obj.Success.allMenuData.ahData[i].action; var keyDataAh = "#ahData"+key;
	 * $(keyDataAh).val(val); } } if(obj.Success.allMenuData.hdData){ for(var i =
	 * 0; i < obj.Success.allMenuData.hdData.length; i++){ key =
	 * obj.Success.allMenuData.hdData[i].key; val =
	 * obj.Success.allMenuData.hdData[i].action; var keyDatahd = "#hdData"+key;
	 * $(keyDatahd).val(val); } } if(obj.Success.menuOtherDetails){
	 * if(obj.Success.menuOtherDetails.businessHoursMenu){ var bhExtDialogVal =
	 * obj.Success.menuOtherDetails.businessHoursMenu[1]; if(bhExtDialogVal ==
	 * "true"){ $('#enableFLExtDialogBhMenu').prop('checked', true); }else{
	 * $('#enableFLExtDialogBhMenu').prop('checked', false); } }
	 * if(obj.Success.menuOtherDetails.afterHoursMenu){ var ahExtDialogVal =
	 * obj.Success.menuOtherDetails.afterHoursMenu[1]; if(ahExtDialogVal ==
	 * "true"){ $('#enableFLExtDialogAhMenu').prop('checked', true); }else{
	 * $('#enableFLExtDialogAhMenu').prop('checked', false); } }
	 * if(obj.Success.menuOtherDetails.holidayMenu){ var hdExtDialogVal =
	 * obj.Success.menuOtherDetails.holidayMenu[1]; if(hdExtDialogVal ==
	 * "true"){ $('#enableFLExtDialogHdMenu').prop('checked', true); }else{
	 * $('#enableFLExtDialogHdMenu').prop('checked', false); } }
	 *  } } } //$('#AAData').show(); //$("#AAData").html(result); } }); } else {
	 * $("#endUserId").html(""); } });
	 */
	$(".pGreetingSelect").prop('disabled', 'disabled');
	$(".pGreetingSelect").css({
		'background-color' : '#e9e9e9'
	});
	$(".pGreetingSelectHoliday").prop('disabled', 'disabled');
	$(".pGreetingSelectHoliday").css({
		'background-color' : '#e9e9e9'
	});
	$(".pGreetingSelectAfter").prop('disabled', 'disabled');
	$(".pGreetingSelectAfter").css({
		'background-color' : '#e9e9e9'
	});
	$("#vpAnnouncementId").prop('disabled', false);
	$("#vpAnnouncementId").css({
		'background-color' : '#fff'
	});

	$('#element').click(function() {
		if ($('#bhGreetingP').is(':checked')) {
			$(".pGreetingSelect").prop('disabled', false);
			$(".pGreetingSelect").css({
				'background-color' : '#fff'
			});
		} else {
			$(".pGreetingSelect").prop('disabled', 'disabled');
			$(".pGreetingSelect").css({
				'background-color' : '#e9e9e9'
			});
		}
	});

	$('#element2').click(function() {
		if ($('#bhGreetingPHoliday').is(':checked')) {
			$(".pGreetingSelectHoliday").prop('disabled', false);
			$(".pGreetingSelectHoliday").css({
				'background-color' : '#fff'
			});
		} else {
			$(".pGreetingSelectHoliday").prop('disabled', 'disabled');
			$(".pGreetingSelectHoliday").css({
				'background-color' : '#e9e9e9'
			});
		}
	});

	$('#element1').click(function() {
		if ($('#bhGreetingPAfter').is(':checked')) {
			$(".pGreetingSelectAfter").prop('disabled', false);
			$(".pGreetingSelectAfter").css({
				'background-color' : '#fff'
			});
		} else {
			$(".pGreetingSelectAfter").prop('disabled', 'disabled');
			$(".pGreetingSelectAfter").css({
				'background-color' : '#e9e9e9'
			});
		}
	});

	$("#aaType").on('change', function() {
		selectedvalue = this.value;
		if (selectedvalue == "Basic") {
			$("#businessHoursScheduleDisplay").show();
			$("#holidayScheduleDisplay").show();
		} else if (selectedvalue == "Standard") {
			$("#businessHoursScheduleDisplay").hide();
			$("#holidayScheduleDisplay").hide();
		}
	});

	$("#dialogAutoAttendant").dialog(
			{
				autoOpen : false,
				width : 800,
				modal : true,
				position : {
					my : "top",
					at : "top"
				},
				resizable : false,
				closeOnEscape : false,
				buttons : {
					"Complete" : function() {
						createAutoAttendant();
					},
					"Cancel" : function() {
						$(this).dialog("close");
					},
					"Delete" : function() {

					},
					"More changes" : function() {
						$('html, body').animate({
							scrollTop : '0px'
						}, 300);
						$("#loading2").show();
						$("#addAutoAttendant").hide();

						var aaId = $("#aaModOperation").val();
						var autoAttendantId = aaId.split("@");
						getAutoAttendantList(autoAttendantId[0]);
						$(this).dialog("close");
					},
					"Done" : function() {
						$(this).dialog("close");
						getAutoAttendantList(0);
						$("#addAutoAttendant").hide();
						$('#AAData').hide();
						$("html, body").animate({
							scrollTop : 0
						}, "slow");
					},
					"Ok" : function() {

					},
                    "Return to Main": function() {
                        location.href="main.php";
					}

				},
				open : function() {
					setDialogDayNightMode($(this));
					$(".ui-dialog-buttonpane button:contains('Complete')")
							.button().show().addClass('subButton');
					$(".ui-dialog-buttonpane button:contains('Cancel')")
							.button().show().addClass('cancelButton');
					$(".ui-dialog-buttonpane button:contains('More changes')")
							.button().hide().addClass('subButton');
					$(".ui-dialog-buttonpane button:contains('Done')").button()
							.hide().addClass('subButton');
					$(".ui-dialog-buttonpane button:contains('Delete')")
							.button().hide().addClass('deleteButton');
					$(".ui-dialog-buttonpane button:contains('Ok')").button()
							.hide().addClass('subButton');
				}
			});

	$("#subButton").click(function() {
		$("#loading2").show();
		$("#addAutoAttendant").hide();
		$("#dialogAutoAttendant").html('');
		var aaChoiceVal = $("#AAChoice").val();
		var aaChoiceName = $.trim($("#aaName").val());
		if(aaChoiceVal){ 
			validateModAutAttendant();
		}
		else {
			validateNewAutoAttendant();
		}
	});

	var validateNewAutoAttendant = function() {
		var dataToSend = $("form#autoAttendantadd").serializeArray();
		$(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide().addClass('returnToMainButton');;
				$.ajax({
					type : "POST",
					url : "autoAttendant/add/validate.php",
					data : {
						compare : 1,
						formdata : dataToSend
					},
					success : function(result) {
						$("#loading2").hide();
						$("#addAutoAttendant").show();
						var errorExist = result.search("#ac5f5d");
						var noChangesExist = result.search("No Changes");

						if (errorExist > 0 || noChangesExist > 0) {
							$(":button:contains('Complete')").attr("disabled",
									"disabled").addClass("ui-state-disabled").addClass('subButton');
						} else {
							$(":button:contains('Complete')").removeAttr(
									"disabled").removeClass("ui-state-disabled").addClass('subButton');
							$(".ui-dialog-buttonpane button:contains('Cancel')")
							.button().show().addClass('cancelButton');
						}
						$("#dialogAutoAttendant").dialog("option", "title",
								"Request Complete");
						$("#dialogAutoAttendant")
								.html(
										'<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelist" class="confSettingTable"><tbody><tr><th colspan="2" align="center">Please confirm the settings below and click Complete to process your modifications.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');

						$("#changelist").append(result);
						$("#dialogAutoAttendant").append('</tbody></table>');
						$("#dialogAutoAttendant").dialog("open");
					}
				});
	}

	var validateModAutAttendant = function() {
		var dataToSend = $("form#autoAttendantadd").serializeArray();
		$
				.ajax({
					type : "POST",
					url : "autoAttendant/modify/validate.php",
					data : {
						compare : 1,
						formdata : dataToSend
					},
					success : function(result) {
						$("#loading2").hide();
						$("#addAutoAttendant").show();
						var errorExist = result.search("#ac5f5d");
						var noChangesExist = result.search("No Changes");

						if (errorExist > 0 || noChangesExist > 0) {
							$(":button:contains('Complete')").attr("disabled",
									"disabled").addClass("ui-state-disabled");
						} else {
							$(":button:contains('Complete')").removeAttr(
									"disabled")
									.removeClass("ui-state-disabled");
						}
						$("#dialogAutoAttendant").dialog("option", "title",
								"Request Complete");
						$("#dialogAutoAttendant")
								.html(
										'<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelist" class="confSettingTable"><tbody><tr><th colspan="2" align="center">Please confirm the settings below and click Complete to process your modifications.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');

						$("#changelist").append(result);
						$("#dialogAutoAttendant").append('</tbody></table>');
						$("#dialogAutoAttendant").dialog("open");
					}
				});
	}

	var createAutoAttendant = function() {
		$("#dialogAutoAttendant")
				.html(
						'Creating the auto attendant request....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
		$(":button:contains('Complete')").attr("disabled", "disabled")
				.addClass("ui-state-disabled subButton"); 
		var dataToSend = $("form#autoAttendantadd").serializeArray();
		$.ajax({
			type : "POST",
			url : "autoAttendant/add/addAutoAttendant.php",
			data : dataToSend,
			success : function(result) {
				$("#loading2").hide();
				$("#dialogAutoAttendant").html('');
				var obj = jQuery.parseJSON(result);
                                //debugger;
				var aaError = "";
				if (obj.aaAddResponse) {
					if (obj.aaAddResponse.Error) {
						if (obj.aaAddResponse.Error[0].length > 1) {
							$("#dialogAutoAttendant").append(
									'<br/>' + obj.aaAddResponse.Error[0]);
						}else{
							$("#dialogAutoAttendant").append(
									'<br/>' + obj.aaAddResponse.Error);
						}
						//$("#dialogAutoAttendant").append('<br/>' + obj.aaAddResponse.Error);
						aaError = "1";
					} else {
						$("#dialogAutoAttendant").append(
								'<br/><div class=\'labelTextGrey\' style=\'text-align\'>Auto Attendant Created Successfully</div>');
						$("#aaModOperation").val(obj.aaAddResponse.aaIdMod);
					}
				}
				if (aaError == "1") {
					$(".ui-dialog-buttonpane button:contains('Cancel')")
							.button().show();
					$(":button:contains('Complete')").attr("disabled",
							"disabled").addClass("ui-state-disabled subClass").removeClass("ui-state-hover");;
				} else {
					// $("#dialogAutoAttendant").append('<br/><b>Group has been created.
					// Click on continue to add information to group</b>');
					$(".ui-dialog-buttonpane button:contains('Cancel')")
							.button().hide();
					$(".ui-dialog-buttonpane button:contains('Complete')")
							.button().hide();
					$(":button:contains('Complete')").removeAttr("disabled")
							.removeClass("ui-state-disabled ui-state-hover").addClass("subButton");
					$(".ui-dialog-buttonpane button:contains('More changes')")
							.button().show().addClass("subButton");
					$(".ui-dialog-buttonpane button:contains('Done')").button()
							.hide();
					$(".ui-dialog-buttonpane button:contains('Ok')").button()
							.hide();
					$(".ui-dialog-buttonpane button:contains('Return to Main')").button()
					.show().addClass('returnToMainButton');;
				}
			}
		});
	}

	// code by Jeetesh for js

	// function to get the list of announcement
	var getAnnouncementList = function(announcement_type_id) {
		var html = '<option value="">-Select Greetings-</option>';

		$.ajax({
			url : 'autoAttendant/getAnnouncement.php',
			type : 'POST',
			data : {
				announcement_type_id : announcement_type_id
			},
			success : function(data) {
				var obj = jQuery.parseJSON(data);
				if (obj.length > 0) {
					for (var i = 0; i < obj.length; i++) {
						html += '<option value="' + obj[i].announcement_id
								+ '" data-file="' + obj[i].announcement_file
								+ '">' + obj[i].announcement_name
								+ '</option>';
					}
				} else {
					html = '<option value="">Greetings not available</option>';
				}

				if (announcement_type_id == 1) {
					$("#bhAnnouncementId").html(html);
				}

				if (announcement_type_id == 2) {
					$("#ahAnnouncementId").html(html);
				}

				if (announcement_type_id == 3) {
					$("#hAnnouncementId").html(html);
				}

				if (announcement_type_id == 4) {
					$("#vpAnnouncementId").html(html);
				}
				
				if (announcement_type_id == 6) {
					for(var i=0; i<=10; i++){
						$("#submenu"+i+"AnnouncementId").html(html);
					}
					$("#newSubmenuAnnouncementId").html(html);
					
				}
				
				/*if (announcement_type_id == 5) {
					$(".aGreetingSelect").html(html);
				}*/
			}

		});
	};

	// function to get the list of announcement
	var saveAnnouncementList = function() {

		var bhmGreeting = $("#bhmGreetingSelect").val();
		var ahmGreeting = $("#ahmGreetingSelect").val();
		var bhgGreeting = $("#bhgGreetingSelect").val();
		var auto_attendant_id = $("#AAChoice").val();

		$.ajax({
			url : 'autoAttendant/saveAnnouncement.php',
			type : 'POST',
			data : {
				auto_attendant_id : auto_attendant_id,
				bhmGreeting : bhmGreeting,
				ahmGreeting : ahmGreeting,
				bhgGreeting : bhgGreeting
			},
			success : function(data) {
				var obj = jQuery.parseJSON(data);

			}

		});
	};

	$("#saveAnnouncement").click(function() {
		saveAnnouncementList();
	});

	var setPlayAnnouncement = function(fileName, audioId, downloadId, oggId, mpegId ) {
		
		if(fileName != ""){
			var filePath = window.location.origin
					+ '/broadsoft/announcementUpload/' + fileName;
			
			$.get(filePath, function(data, textStatus) {
		        if (textStatus == "success") {
		        	var downloadPath = window.location.origin+'/broadsoft/download.php?file=' + fileName;
		            // execute a success code
					$("#"+audioId).attr("src", filePath);
					//$("#"+oggId).attr("src", filePath);
					//$("#"+mpegId).attr("src", filePath);
					$("#"+audioId).show();
					
					$("#"+downloadId).attr("href", downloadPath);
					$("#"+downloadId).show();

		        }
		    });
		}
	}
	
	var setAnnouncementAutoattendantFields = function() {
		var id = $("#AAChoice").val();
		$.ajax({
			url : 'autoAttendant/setAnnouncementAutoattendantFields.php',
			type : 'POST',
			data : {aaId : id},
			success : function(result) {//debugger;
				var obj = jQuery.parseJSON(result);
				if(obj.bh){
					if (obj.bh.announcementSelection == "Personal") {
						$('input[id=bhGreetingP]').trigger('click');
						$('#bhAnnouncementId').val(obj.bh.announcementId);
						setPlayAnnouncement(obj.bh.announcement_file, 'bhaudio', 'bhdownload', 'bhaudioOgg', 'bhaudioMpeg');            		
					} else {
						$('input[id=bhGreeting]').trigger('click');
					}
				}
				if(obj.ah){
					if (obj.ah.announcementSelection == "Personal") {
						$('input[id=ahGreetingP]').trigger('click');
						$('#ahAnnouncementId').val(obj.ah.announcementId);
						setPlayAnnouncement(obj.ah.announcement_file, 'ahaudio', 'ahdownload', 'ahaudioOgg', 'ahaudioMpeg');            		
					} else {
						$('input[id=ahGreeting]').trigger('click');
					}
				}
				if (obj.h) {
					if (obj.h.announcementSelection == "Personal") {
						$('input[id=hGreetingP]').trigger('click');
						$('#hAnnouncementId').val(obj.h.announcementId);
						setPlayAnnouncement(obj.h.announcement_file, 'haudio', 'hdownload', 'haudioOgg', 'haudioMpeg');            		
					} else {
						$('input[id=hGreeting]').trigger('click');
					}
				}

				if (obj.vp) {
					//if (obj.vp.announcementSelection == "Personal") {
						//$('input[id=vpGreetingP]').trigger('click');
						$('#vpAnnouncementId').val(obj.vp.announcementId);
						setPlayAnnouncement(obj.vp.announcement_file, 'vpaudio', 'vpdownload', 'vpaudioOgg', 'vpaudioMpeg');  
						$("#vpAnnouncementId").prop('disabled', false);
						$("#vpAnnouncementId").css({
							'background-color' : '#fff'
						});

					//} else {
					//	$('input[id=vpGreeting]').trigger('click');
					//}
				}
				if(obj.submenu){
					var string = obj.submenu;
					var array = string.split(',');
					for (var i = 0; i < array.length; i++) {
					    var datawithid = array[i];
					    var splittedData = datawithid.split("***");
					    $("."+splittedData[0]).val(splittedData[1]);
					    $("."+splittedData[0]).prop('disabled', false);
					    $("."+splittedData[0]+"1").prop('checked', 'checked');
					    $("."+splittedData[0]).css({
							'background-color' : '#fff'
						});
					    
					}
				}
			}

		});
	};

	// function to get the list of announcement
	var getCallPolicies = function(announcementId) {
		var html = '<option value="">-Select Greetings-</option>';

		$.ajax({
					url : 'autoAttendant/getCallPolicies.php',
					type : 'POST',
					data : { announcementId : announcementId},
					success : function(data) {
						var obj = jQuery.parseJSON(data);
						// obj.Success.redirectedCallsCOLPPrivacy
						$("input[type=radio][name=redirectedCallsCOLPPrivacy][value='"+ obj.Success.redirectedCallsCOLPPrivacy+ "']").prop("checked", true);
						$("input[type=radio][name=callBeingForwardedResponseCallType][value='"+ obj.Success.callBeingForwardedResponseCallType+ "']").prop("checked", true);
						$("#AAData").show();
					}

				});
	};

	// function to get the list of autoattendant
	var getAutoAttendantList = function(autoAttendantId) {
		var html = '<option value=""></option>';

		$.ajax({
			url : 'autoAttendant/getAutoAttendantList.php',
			type : 'POST',
			success : function(data) {
				$("#loading2").hide();
				var obj = jQuery.parseJSON(data);
				if (obj.length > 0) {
					for (var i = 0; i < obj.length; i++) {
						html += '<option value="' + obj[i].id + '">'
								+ obj[i].name + '</option>';
					}
				}
				$("#AAChoice").html(html);
				if (autoAttendantId != "0") {
					$("#AAChoice").val(autoAttendantId);
					$("#AAChoice").trigger('change');
				}
				// $("#addAutoAttendant").show();
				// $("#loading2").hide();
			}
		});
	};
	getAutoAttendantList(0);
	
	$("#phoneNumber").change(function(){
		var el = $(this);
		var phoneVal = el.val();
		
		if(phoneVal == ""){
			$("#aaPhoneActivateNumber").attr("disabled", true);
			$("#aaPhoneActivateNumber").val("");
			$("#divActivateNumber").hide("");
		}else{
			$("#aaPhoneActivateNumber").attr("disabled", false);
			$("#aaPhoneActivateNumber").val(phoneVal);
			$("#divActivateNumber").show("");
		}
		
	});
	

	var showVideoEnabled = function(){

		var aaBasicServiceAssign = $("#aaBasicServiceAssign").val();
		var aaStandardServiceAssign = $("#aaStandardServiceAssign").val();
		var aaVideoServiceAssign = $("#aaVideoServiceAssign").val();
		var aaTypeVal = $("#aaType").val();

		$("#eVsupport").parent("div").parent("div").hide();
		
		if(aaVideoServiceAssign == "false" ){
			$("#eVsupport").parent("div").parent("div").hide();
		}else{
			if(aaBasicServiceAssign == "false"){
				$("#eVsupport").parent("div").parent("div").hide();
			}else{
				$("#eVsupport").parent("div").parent("div").show();
			}
		}
		
		if(aaTypeVal == "Standard"){
			$("#eVsupport").parent("div").parent("div").show();
		}
			
	}
	$("#holidayMenu").parent("li").hide();
	var showHoliday = function(){
		var aaTypeVal = $("#aaType").val();
		if(aaTypeVal == "Standard"){
			$("#holidayMenu").parent("li").show();
		}else{
			$("#holidayMenu").parent("li").hide();
		}
	}
	
	$("#aaType").change(function(){
		showVideoEnabled();
		showHoliday();
	});
	$("#noServicesAssign").hide();
	showVideoEnabled();

});

function extensionBuilderOnAddAA(num, extLen) { 
	var numLen = num.length;
	var ind = numLen - extLen;
	var extension = num.substring(ind);
	//$("#extension").val(extension);
	//var extension = num.substring("5");
	$("#extensionAdd").val(extension);
}
if ($.fn.button.noConflict) {
	var bootstrapButton = $.fn.button.noConflict();
	$.fn.bootstrapBtn = bootstrapButton;
	}	