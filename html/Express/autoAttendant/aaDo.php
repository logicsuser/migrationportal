<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/autoAttendant/aaController.php");
	$aaController = new AaController();
	
	require_once("/var/www/lib/broadsoft/adminPortal/autoAttendant/aaOperation.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/Announcement/Announcement.php");
	$announcement = new Announcement();
	
	require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
	
	$sp = $_SESSION["sp"]; 
	$groupId = $_SESSION["groupId"]; 
	
	$aaId = $_POST["aaId"];
	$updateCallPolicyArray = array();
	$xmlData = "";
	$menuData = "";
	$sip = "";
	$a = 0;
	$dnPostArray = array();
	
	if(!array_key_exists("eVSupport", $_POST)){
		$_POST["eVSupport"] = "false";
	}
	if(!array_key_exists("assignedServices", $_POST)){
		$_POST["assignedServices"] = array();
	}
	if(!array_key_exists("aaActivateNumber", $_POST)){
		$_POST["aaActivateNumber"] = "No";
	}
	if(!array_key_exists("aaPhoneActivateNumber", $_POST)){
		$_POST["aaPhoneActivateNumber"] = "";
	}
	
	$announcementAutoAttendantArray = array();
	$announcementAutoAttendantArray['bhAnnouncementId'] = "";
	$announcementAutoAttendantArray['ahAnnouncementId'] = "";
	$announcementAutoAttendantArray['hAnnouncementId']  = "";
	$announcementAutoAttendantArray['vpAnnouncementId'] = "";
	$checkifNewSubCreated= "";
	$submenuInsertionIntoDB = "";
	foreach ($_POST as $key => $value)
	{
		if ($key != "aaId" && $key != "aaPhoneActivateNumber") //hidden form fields that don't change don't need to be checked or displayed
		{
			if (!is_array($value))
			{
				if ($value !== $_SESSION["aa"][$key])
				{
					if ($key == "aaName" or $key == "callingLineIdLastName" or $key == "callingLineIdFirstName" or $key == "timeZone" or $key == "department" or $key == "phoneNumber" or $key == "extension")
					{
						$sipDo = 1;
						$sipHeader = "<serviceInstanceProfile>";
						if ($key == "aaName")
						{
							$sip .= "<name>" . $value . "</name>";
						}
						if ($key == "callingLineIdLastName")
						{
							$sip .= "<callingLineIdLastName>" . $value . "</callingLineIdLastName>";
						}
						if ($key == "callingLineIdFirstName")
						{
							$sip .= "<callingLineIdFirstName>" . $value . "</callingLineIdFirstName>";
						}
						if($key == "phoneNumber"){
							if(!empty($value)){
								$sip .= "<phoneNumber>".$value."</phoneNumber>";
							}else{
								$sip .= "<phoneNumber xsi:nil='true'>".$value."</phoneNumber>";
							}
						}
						if($key == "extension"){
							if(!empty($value)){
								$sip .= "<extension>".$value."</extension>";
							}else{
								$sip .= "<extension xsi:nil='true'>".$value."</extension>";
							}
						}
						if ($key == "timeZone")
						{
							$sip .= "<timeZone>" . $value . "</timeZone>";
						}
						if ($key == "department"){
							if(strlen($value) > 0){
								$sip.= "<department xsi:type='GroupDepartmentKey'>";
								$sip.= "<serviceProviderId>". htmlspecialchars($_SESSION['sp'])."</serviceProviderId>";
								$sip.= "<groupId>".htmlspecialchars($_SESSION['groupId'])."</groupId>";
									$sip.= "<name>".$value."</name>";
								$sip.= "</department>";
							}else{
								$sip .= "<department xsi:nil=\"true\" xsi:type='GroupDepartmentKey'/>";
							}
							
						}
						$sipFooter = "</serviceInstanceProfile>";
					}
					if($key == "eVSupport"){
						$xmlData.= "<enableVideo>".$value."</enableVideo>";
					}
					if($key == "nameDialingScope"){
						$xmlData.= "<nameDialingScope>".$value."</nameDialingScope>";
					}
					if($key == "extensionDialingScope"){
						$xmlData.= "<extensionDialingScope>".$value."</extensionDialingScope>";
					}
					if ($ociVersion == "21"){
						if($key == "firstDigitTimeoutSeconds"){
							$xmlData.= "<firstDigitTimeoutSeconds>".$value."</firstDigitTimeoutSeconds>";
						}
					}
					if ($key == "businessHoursSchedule")
					{
						$businessValueExplode = explode("**", $value);
						$value = $businessValueExplode[0];
						if (strlen($value) > 0)
						{
							$xmlData .= "<businessHours>
										<type>".htmlspecialchars($businessValueExplode[1])."</type>
										<name>" . htmlspecialchars($value) . "</name>
									</businessHours>";
						}
						else
						{
							$xmlData .= "<businessHours xsi:nil=\"true\" />";
						}
					}
					if ($key == "holidaySchedule")
					{ 
						$holidayValueExplode = explode("**", $value);
						$value = $holidayValueExplode[0];
						if (strlen($value) > 0)
						{
							$xmlData .= "<holidaySchedule>
										<type>".htmlspecialchars($holidayValueExplode[1])."</type>
										<name>" . htmlspecialchars($value) . "</name>
									</holidaySchedule>";
						}
						else
						{
							$xmlData .= "<holidaySchedule xsi:nil=\"true\" />";
						}
					}
					if ($key == "nameDialingEntries")
					{
						$xmlData .= "<nameDialingEntries>" . $value . "</nameDialingEntries>";
					}
					$changes[$a][$_SESSION["aaNames"][$key]]["oldValue"] = $_SESSION["aa"][$key];
					$changes[$a][$_SESSION["aaNames"][$key]]["newValue"] = $value;
					
					if ($key == "aaActivateNumber" && !empty($_POST['aaPhoneActivateNumber'])){
						
						if($_SESSION["aa"][$key] == "No"){
							$oldCodes = "Deactivated";
						}else{
							$oldCodes = "Activated";
						}
						
						if($_POST[$key] == "No"){
							$newCodes = "Deactivated";
						}else{
							$newCodes = "Activated";
						}
						
						$changes[$a][$_SESSION["aaNames"][$key]]["oldValue"] = $oldCodes;
						$changes[$a][$_SESSION["aaNames"][$key]]["newValue"] = $newCodes;
					}
					$a++;
				}
				
				if($key == "redirectedCallsCOLPPrivacy" || $key == "callBeingForwardedResponseCallType"){
					$updateCallPolicyArray['autoAttendantId'] = $aaId;

					if($key == "redirectedCallsCOLPPrivacy"){
						$changes[$a][$_SESSION["aaNames"][$key]]["oldValue"] = $_SESSION["aa"][$key];
						$changes[$a][$_SESSION["aaNames"][$key]]["newValue"] = $value;
						$updateCallPolicyArray['redirectedCallsCOLPPrivacy'] = $value;
					}
					if($key == "callBeingForwardedResponseCallType"){ 
						$changes[$a][$_SESSION["aaNames"][$key]]["oldValue"] = $_SESSION["aa"][$key];
						$changes[$a][$_SESSION["aaNames"][$key]]["newValue"] = $value;
						$updateCallPolicyArray['callBeingForwardedResponseCallType'] = $value;
					} 
				}
			}
			else
			{	
				$announcementAutoAttendantArray['auto_attendant_id'] = $aaId;
				
				if ($key == "vp" && isset($_POST['vp']['announcementId'])){
					if(isset($_POST['vp']['announcementId']) && !empty($_POST['vp']['announcementId'])){
						
						$announcementAutoAttendantArray['vpAnnouncementId'] = $_POST['vp']['announcementId'];						
						$where['announcement_id'] = $_POST['vp']['announcementId'];
						$getAnnouncementFileData = $announcement->getAnnouncement($db, $where);
						
						$fileName = $getAnnouncementFileData[0]['announcement_file'];
						$fpath = "../../announcementUpload/".$sp."/".$groupId."/";
						$announcement->voicePortalCustomAnnouncementModifyRequest($aaId, $fileName, $fpath);
					}
				}
				
				if ($key == "bh" or $key == "ah" or $key == "h" or strpos($key, "submenu") === 0)
				{	
					if ($key == "bh")
					{
						$header = "<businessHoursMenu>";
						$header .= "<announcementSelection>".$_POST['bh']['announcementSelection']."</announcementSelection>";						
						
						if(isset($_POST['bh']['announcementId']) && !empty($_POST['bh']['announcementId'])){
							$announcementAutoAttendantArray['bhAnnouncementId'] = $_POST['bh']['announcementId'];
							$where['announcement_id'] = $_POST['bh']['announcementId'];
							$getAnnouncementFileData = $announcement->getAnnouncement($db, $where);	
							if(!empty($getAnnouncementFileData[0]['announcement_file'])){
								$fileName = $getAnnouncementFileData[0]['announcement_file'];
								$fpath = "../../announcementUpload/".$sp."/".$groupId."/";
								$header .= $announcement->audioVideoXml($fileName, $fpath);
							}
						}
					}
					if ($key == "ah")
					{
						$header = "<afterHoursMenu>";
						$header .= "<announcementSelection>".$_POST['ah']['announcementSelection']."</announcementSelection>";						
						
						if(isset($_POST['ah']['announcementId']) && !empty($_POST['ah']['announcementId'])){
							$announcementAutoAttendantArray['ahAnnouncementId'] = $_POST['ah']['announcementId'];
							$where['announcement_id'] = $_POST['ah']['announcementId'];
							$getAnnouncementFileData = $announcement->getAnnouncement($db, $where);
							$fileName = $getAnnouncementFileData[0]['announcement_file'];
							$fpath = "../../announcementUpload/".$sp."/".$groupId."/";
							$header .= $announcement->audioVideoXml($fileName, $fpath);
						}
					}
					if ($key == "h")
					{
						$header = "<holidayMenu>";
						$header .= "<announcementSelection>".$_POST['h']['announcementSelection']."</announcementSelection>";
						
						
						if(isset($_POST['h']['announcementId']) && !empty($_POST['h']['announcementId'])){
							$announcementAutoAttendantArray['hAnnouncementId'] = $_POST['h']['announcementId'];
							$where['announcement_id'] = $_POST['h']['announcementId'];
							$getAnnouncementFileData = $announcement->getAnnouncement($db, $where);
							$fileName = $getAnnouncementFileData[0]['announcement_file'];
							$fpath = "../../announcementUpload/".$sp."/".$groupId."/";
							$header .= $announcement->audioVideoXml($fileName, $fpath);
						}
					}					
					if (strpos($key, "submenu") === 0)
					{
						if ($ociVersion == "20")
						{
							$header = xmlHeader($sessionid, "GroupAutoAttendantSubmenuModifyRequest20");
						}
						else
						{
							$header = xmlHeader($sessionid, "GroupAutoAttendantSubmenuModifyRequest");
						}
						$header .= "<serviceUserId>" . $aaId . "</serviceUserId>";
						$header .= "<submenuId>" . htmlspecialchars($_SESSION["aa"][$key]["id"]) . "</submenuId>";
						$header .= "<announcementSelection>".$value['announcementSelection']."</announcementSelection>";
						if(isset($value["announcementId"]) && !empty($value["announcementId"])){
						  //  $announcementAutoAttendantArray['hAnnouncementId'] = $_POST['h']['announcementId'];
						    $where['announcement_id'] = $value["announcementId"];
						    $getAnnouncementFileData = $announcement->getAnnouncement($db, $where);
						    $fileName = $getAnnouncementFileData[0]['announcement_file'];
						    $fpath = "../../announcementUpload/".$sp."/".$groupId."/";
						    $header .= $announcement->audioVideoXml($fileName, $fpath);
						    
						    $where['auto_attendant_id'] = $aaId;
						    $announcementSub1 = $announcement->getAnnouncementSubMenu($db, $where);
						    
						    if(count($announcementSub1) > 0 && !empty($announcementSub1)){
						        $thePostIdArray = explode(',', $announcementSub1[0]);
						       
						        foreach ($thePostIdArray as $keyrem => $valrem){
						            if( strpos( $valrem, $value["id"] ) !== false ){ 
						                unset($thePostIdArray[$keyrem]);
						            }
						            
						        }
						    }
						  
						    array_push($thePostIdArray, $value["id"]."***".$value["announcementId"]);
						   
						    $var11 = $announcement->insertAndUpdateSubmenu($db, $thePostIdArray,$aaId);
						    
						}
						if ($value["id"] !== $_SESSION["aa"][$key]["id"])
						{
							$header .= "<newSubmenuId>" . htmlspecialchars($value["id"]) . "</newSubmenuId>";
							$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]][$_SESSION["aaNames"]["id"]]["oldValue"] = $_SESSION["aa"][$key]["id"];
							$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]][$_SESSION["aaNames"]["id"]]["newValue"] = $value["id"];
							$a++;
						}
					}
					if ($value["enableLevelExtensionDialing"] !== $_SESSION["aa"][$key]["enableLevelExtensionDialing"])
					{
						if (strpos($key, "submenu") === 0)
						{
							$header .= "<enableLevelExtensionDialing>" . $value["enableLevelExtensionDialing"] . "</enableLevelExtensionDialing>";
						}
						else
						{
							$header .= "<enableFirstMenuLevelExtensionDialing>" . $value["enableLevelExtensionDialing"] . "</enableFirstMenuLevelExtensionDialing>";
						}
						if ($key == "bh")
						{
							$changes[$a]["BusinessHours"][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["oldValue"] = $_SESSION["aa"][$key]["enableLevelExtensionDialing"];
							$changes[$a]["BusinessHours"][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["newValue"] = $value["enableLevelExtensionDialing"];
						}
						if ($key == "ah")
						{
							$changes[$a]["AfterHours"][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["oldValue"] = $_SESSION["aa"][$key]["enableLevelExtensionDialing"];
							$changes[$a]["AfterHours"][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["newValue"] = $value["enableLevelExtensionDialing"];
						}
						if ($key == "h")
						{
							$changes[$a]["Holiday"][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["oldValue"] = $_SESSION["aa"][$key]["enableLevelExtensionDialing"];
							$changes[$a]["Holiday"][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["newValue"] = $value["enableLevelExtensionDialing"];
						}
						if (strpos($key, "submenu") === 0)
						{
							$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]][$_SESSION["aaNames"]["enableLevelExtensionDialing"]]["oldValue"] = $_SESSION["aa"][$key]["enableLevelExtensionDialing"];
							$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]][$_SESSION["aaNames"]["enableLevelExtensionDialing"]]["newValue"] = $value["enableLevelExtensionDialing"];
						}
						$a++;
					}
					$keys = "";
					foreach ($value["keys"] as $k => $v)
					{
						if (
							((isset($_SESSION["aa"][$key]["keys"][$k]["desc"]) and trim($_SESSION["aa"][$key]["keys"][$k]["desc"]) !== trim($v["desc"])) or !isset($_SESSION["aa"][$key]["keys"][$k]["desc"]))
							or
							((isset($_SESSION["aa"][$key]["keys"][$k]["action"]) and trim($_SESSION["aa"][$key]["keys"][$k]["action"]) !== trim($v["action"])) or !isset($_SESSION["aa"][$key]["keys"][$k]["action"]))
							or
							((isset($_SESSION["aa"][$key]["keys"][$k]["phoneNumber"]) and trim($_SESSION["aa"][$key]["keys"][$k]["phoneNumber"]) !== trim($v["phoneNumber"])) or !isset($_SESSION["aa"][$key]["keys"][$k]["phoneNumber"]))
							or
							((isset($_SESSION["aa"][$key]["keys"][$k]["submenuId"]) and trim($_SESSION["aa"][$key]["keys"][$k]["submenuId"]) !== trim($v["submenu"])) or !isset($_SESSION["aa"][$key]["keys"][$k]["submenuId"]))
							or
							((isset($_SESSION["aa"][$key]["keys"][$k]["announcements"]) and trim($_SESSION["aa"][$key]["keys"][$k]["announcements"]) !== trim($v["announcements"])) or !isset($_SESSION["aa"][$key]["keys"][$k]["announcements"]))
						)
						{
							$keys .= "<keyConfiguration>";
							$keys .= "<key>" . $k . "</key>";

							if ($v["action"] == "Transfer To Submenu")
							{
								$actionOld = isset($_SESSION["aa"][$key]["keys"][$k]["submenuId"]) ? $_SESSION["aa"][$key]["keys"][$k]["submenuId"] : "";
								$actionNew = $v["submenu"];
							}
							else
							{
								$actionOld = isset($_SESSION["aa"][$key]["keys"][$k]["phoneNumber"]) ? $_SESSION["aa"][$key]["keys"][$k]["phoneNumber"] : "";
								$actionNew = $v["phoneNumber"];
							}
							if ($key == "bh")
							{
								$changes[$a]["BusinessHours"]["keys"][$k]["oldValue"] = (isset($_SESSION["aa"][$key]["keys"][$k]["desc"]) ? $_SESSION["aa"][$key]["keys"][$k]["desc"] : "") . ":" . (isset($_SESSION["aa"][$key]["keys"][$k]["action"]) ? $_SESSION["aa"][$key]["keys"][$k]["action"] : "") . ":" . $actionOld;
								$changes[$a]["BusinessHours"]["keys"][$k]["newValue"] = $v["desc"] . ":" . $v["action"] . ":" . $actionNew;
							}
							if ($key == "ah")
							{
								$changes[$a]["AfterHours"]["keys"][$k]["oldValue"] = (isset($_SESSION["aa"][$key]["keys"][$k]["desc"]) ? $_SESSION["aa"][$key]["keys"][$k]["desc"] : "") . ":" . (isset($_SESSION["aa"][$key]["keys"][$k]["action"]) ? $_SESSION["aa"][$key]["keys"][$k]["action"] : "") . ":" . $actionOld;
								$changes[$a]["AfterHours"]["keys"][$k]["newValue"] = $v["desc"] . ":" . $v["action"] . ":" . $actionNew;
							}
							if ($key == "h")
							{
								$changes[$a]["Holiday"]["keys"][$k]["oldValue"] = (isset($_SESSION["aa"][$key]["keys"][$k]["desc"]) ? $_SESSION["aa"][$key]["keys"][$k]["desc"] : "") . ":" . (isset($_SESSION["aa"][$key]["keys"][$k]["action"]) ? $_SESSION["aa"][$key]["keys"][$k]["action"] : "") . ":" . $actionOld;
								$changes[$a]["Holiday"]["keys"][$k]["newValue"] = $v["desc"] . ":" . $v["action"] . ":" . $actionNew;
							}
							if (strpos($key, "submenu") === 0)
							{
								$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]]["keys"][$k]["oldValue"] = (isset($_SESSION["aa"][$key]["keys"][$k]["desc"]) ? $_SESSION["aa"][$key]["keys"][$k]["desc"] : "") . ":" . (isset($_SESSION["aa"][$key]["keys"][$k]["action"]) ? $_SESSION["aa"][$key]["keys"][$k]["action"] : "") . ":" . $actionOld;
								$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]]["keys"][$k]["newValue"] = $v["desc"] . ":" . $v["action"] . ":" . $actionNew;
							}
							$a++;
							
							if (trim($v["desc"]) == "" and trim($v["action"]) == "" and trim($v["phoneNumber"]) == "" and trim($v["submenu"]) == "")
							{
								$keys .= "<entry xsi:nil=\"true\" />";
							}
							else
							{
								$keys .= "<entry>";
								if (trim($v["desc"]) == "")
								{
									$keys .= "<description xsi:nil=\"true\" />";
								}
								else
								{
									$keys .= "<description>" . $v["desc"] . "</description>";
								}
								$keys .= "<action>" . $v["action"] . "</action>";
								if (trim($v["phoneNumber"]) == "")
								{
									$keys .= "<phoneNumber xsi:nil=\"true\" />";
								}
								else
								{
									$keys .= "<phoneNumber>" . $v["phoneNumber"] . "</phoneNumber>";
								}
								if (trim($v["announcements"]) == ""){
									if ($ociVersion !== "17")
									{
										if (trim($v["submenu"]) == "")
										{
											$keys .= "<submenuId xsi:nil=\"true\" />";
										}
										else
										{
											$keys .= "<submenuId>" . htmlspecialchars($v["submenu"]) . "</submenuId>";
										}
									} 
								}else {
									if(trim($v["announcements"]) != ""){
										$where['announcement_id'] = $v["announcements"];
										$getAnnouncementFileData = $announcement->getAnnouncement($db, $where);
										
										if(!empty($getAnnouncementFileData[0]['announcement_file'])){
											$fileName = $getAnnouncementFileData[0]['announcement_file'];
											$fpath = $_SERVER['DOCUMENT_ROOT']."/announcementUpload/".$sp."/".$groupId."/";
											$keys .= $announcement->audioVideoXml($fileName, $fpath);
											//$filePath = $fpath.$fileName;
											
										}
									}
								}
								$keys .= "</entry>";
							}
							$keys .= "</keyConfiguration>";
						}
					}
					if ($key == "bh")
					{
						$footer = "</businessHoursMenu>";
					}
					if ($key == "ah")
					{
						$footer = "</afterHoursMenu>";
					}
					if ($key == "h")
					{
						$footer = "</holidayMenu>";
					}
					if (strpos($key, "submenu") === 0)
					{
						$footer = xmlFooter();
					}

					if (strpos($key, "submenu") === 0)
					{
					    
						$response = $client->processOCIMessage(array("in0" => $header . $keys . $footer));
						$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
						readError($xml);
					}
					else
					{
						$menuData .= $header . $keys . $footer;
					}
					//echo $menuData; die;
				}
			}
			
			if ($key == "assignedServices"){
				$controllerObj = new AaController();
				$compareObj = new AaOperation();
				$compareResult = $compareObj->checkAssignedServiceChange($_POST["assignedServices"], $_SESSION["serviceData"]["assigned"]);
				if(count($compareResult["newAssign"]) > 0){
					$serviceAddResp = $controllerObj->addServicesToAA($aaId, $compareResult["newAssign"]);
				}
				if(count($compareResult["unAssign"]) > 0){
					$serviceRemoveResp = $controllerObj->removeServicesFromAA($aaId, $compareResult["unAssign"]);
				}
			}
			
			if($key == "newSubmenu" && $_POST["checkNewSubMenuStatus"] == "true"){
			    
			    $headerForXML = "";
			    $playAnn = array();
			    $controllerObj = new AaController();
			    foreach($_POST["newSubmenu"]["keys"] as $key => $value){
			        if($value["action"] == "Play Announcement"){
			            $playAnn[$key];
			            $where['announcement_id'] = $value["announcements"];
			            $getAnnouncementFileAllData = $announcement->getAnnouncement($db, $where);
			            
			            if(!empty($getAnnouncementFileAllData[0]['announcement_file'])){
			                $fileName = $getAnnouncementFileAllData[0]['announcement_file'];
			                $fpath = "../../announcementUpload/".$sp."/".$groupId."/";
			                $playAnn[$key] = $announcement->audioVideoXml($fileName, $fpath);
			            }
			            
			        }
			    }
			    
			    
			    if($_POST['newSubmenu']['bhGreeting'] == "Personal Greeting"){
			        $where['announcement_id'] = $_POST['newSubmenu']['announcementId'];
			        $getAnnouncementFileData = $announcement->getAnnouncement($db, $where);
			       
			        if(!empty($getAnnouncementFileData[0]['announcement_file'])){
			            $fileName = $getAnnouncementFileData[0]['announcement_file'];
			            $fpath = "../../announcementUpload/".$sp."/".$groupId."/";
			            $headerForXML .= $announcement->audioVideoXml($fileName, $fpath);
			        }
			    }
			    
			    $subMenuResponse = $controllerObj->createSubmenu($_POST["newSubmenu"], $_POST["aaId"], $headerForXML, $playAnn);
			   
			    if(readError($subMenuResponse) == "" && $_POST['newSubmenu']['bhGreeting'] == "Personal Greeting"){
			       
			        $where['auto_attendant_id'] = $_POST['aaId'];
			        $announcementSub = $announcement->getAnnouncementSubMenu($db, $where);
			       
			        array_push($announcementSub,$_POST['newSubmenu']['id']."***".$_POST['newSubmenu']['announcementId']);
			     
			        $checkifNewSubCreated= "1";
			        $submenuInsertionIntoDB = $_POST['newSubmenu']['id']."***".$_POST['newSubmenu']['announcementId'];
			   
			    }
			  
			}
		}
	}
	$getAnnouncementInfo = $announcement->saveAutoattendantAnnouncement($db, $announcementAutoAttendantArray);
	$callPolicy = $aaController->modifyCallPolicies($updateCallPolicyArray);
	
	
	if ($ociVersion == "21")
	{
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantModifyInstanceRequest20");
	}
	else
	{ 
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantModifyInstanceRequest17sp1");
	}
	$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
	if (isset($sipDo) and $sipDo == 1)
	{
		$xmlinput .= $sipHeader . $sip . $sipFooter;
	}
	$xmlinput .= $xmlData . $menuData . xmlFooter();

	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	
	readError($xml);
	
	if (isset($_POST["aaActivateNumber"]) && $_POST["aaActivateNumber"] == "Yes"){
		if(isset($_POST["aaPhoneActivateNumber"])  && !empty($_POST["aaPhoneActivateNumber"])){
			$aaDn = new Dns ();
			$dnPostArray = array(
					"sp" => $_SESSION['sp'],
					"groupId" => $_SESSION['groupId'],
					"phoneNumber" => array($_POST['aaPhoneActivateNumber'])
			);
			$dnsActiveResponse = $aaDn->activateDNRequest($dnPostArray);
		}
	}else if(isset($_POST["aaActivateNumber"]) && $_POST["aaActivateNumber"] == "No"){
		if(isset($_POST["aaPhoneActivateNumber"])  && !empty($_POST["aaPhoneActivateNumber"])){
			$aaDn = new Dns ();
			$dnPostArray = array(
					"sp" => $_SESSION['sp'],
					"groupId" => $_SESSION['groupId'],
					"phoneNumber" => array($_POST['aaPhoneActivateNumber'])
			);
			$dnsActiveResponse = $aaDn->deActivateDNRequest($dnPostArray);
		}
	}
	//echo "checkifNewSubCreated";echo $checkifNewSubCreated;
	if($checkifNewSubCreated == "1"){
	   $where['auto_attendant_id'] = $_POST['aaId'];
	   $announcementSub = $announcement->getAnnouncementSubMenu($db, $where);
	   
	   array_push($announcementSub,$submenuInsertionIntoDB);
	   $var12 =  $announcement->insertAndUpdateSubmenu($db, $announcementSub,$_POST['aaId']);
	}
	require_once("/var/www/lib/broadsoft/adminPortal/getFinishedAAInfo.php");

	$data = "<table cellspacing=\"0\" cellpadding=\"5\" align=\"center\" class=\"dialogClass\" width=\"70%\">";
	$data .= "<tr><th colspan=\"2\" align=\"center\">Changes complete. Here are your new auto attendant settings.</th></tr>";
	$data .= "<tr><td>&nbsp;</td></tr>";
	foreach ($aa as $key => $value)
	{
		if (!is_array($value))
		{
			if($key != "aaActivateNumber"){
				$data .= "<tr><td style=\"font-weight:bold;\">" . $_SESSION["aaNames"][$key] . "</td><td>" . $value . "</td></tr>";
			}else{
				if($key == "aaActivateNumber" && !empty($_POST['aaPhoneActivateNumber'])){
					if($value == "Yes"){
					    $value = $_POST['aaPhoneActivateNumber']." have been Activated";
					}else{
					    $value = $_POST['aaPhoneActivateNumber']." have been Deactivated";
					}
				}
				$data .= "<tr><td style=\"font-weight:bold;\">" . $_SESSION["aaNames"][$key] . "</td><td>" . $value . "</td></tr>";
			}
		}
		if ($key == "vp" or $key == "bh" or $key == "ah" or $key == "h" or strpos($key, "submenu") === 0)
		{
			$data .= "<tr><td colspan=\"2\"><table align=\"center\" width=\"100%\">";
			$data .= "<tr><td align=\"center\" colspan=\"4\" class=\"miniBanner\">";
			if ($key == "vp")
			{
				$data .= "Voice Portal Menu";
				$keyLabel = $_SESSION["aaNames"]["vpAnnouncementId"];
			}
			if ($key == "bh")
			{
				$data .= "Business Hours Menu";
				$keyLabel = $_SESSION["aaNames"]["bhAnnouncementId"];
			}
			if ($key == "ah")
			{
				$data .= "After Hours Menu";
				$keyLabel = $_SESSION["aaNames"]["ahAnnouncementId"];
			}
			if ($key == "h")
			{
				$data .= "Holiday Menu";
				$keyLabel = $_SESSION["aaNames"]["hAnnouncementId"];
			}
			if (strpos($key, "submenu") === 0)
			{
				$data .= "Submenu - " . $aa[$key]["id"];
			}
			$data .= "</td></tr>";
			if(!empty($aa[$key]['announcementId'])){
				$data .= "<tr><td style=\"font-weight:bold;\" colspan=\"2\">".$keyLabel."</td><td colspan=\"2\">".$aa[$key]['announcementId']."</td></tr>";
			}
			$data .= "<tr><td colspan=\"4\"><span style=\"font-weight:bold;\">";
			if (strpos($key, "submenu") === 0)
			{
				$data .= $_SESSION["aaNames"]["enableLevelExtensionDialing"];
			}
			else
			{
				$data .= $_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"];
			}
			$data .= "</span> " . $value["enableLevelExtensionDialing"] . "</td></tr>";
			$data .= "<tr><th style=\"width:10%\">Key</th><th width=\"30%\">Action</th><th width=\"30%\">Description</th><th width=\"30%\">Action Data</th></tr>";
			foreach ($value["keys"] as $k => $v)
			{
				$data .= "<tr><td style=\"width:10%\">" . $k . "</td><td style=\"width:30%\">" . $v["action"] . "</td><td style=\"width:30%\">" . $v["desc"] . "</td><td>";
				if ($v["action"] == "Transfer To Submenu")
				{
					$data .= $v["submenuId"];
				}else if($v["action"] == "Play Announcement"){
					$where['announcement_id'] = $v["announcements"]; 
					$where['sp'] = $sp;
					$where['group'] = $groupId;
					$announcement = new Announcement();
					$getAnnouncementInfo = $announcement->getAnnouncement($db, $where);
					
					$data .= $getAnnouncementInfo[0]['announcement_name'];
									
				}else
				{
					$data .= $v["phoneNumber"];
				}
				$data .= "</td></tr>";
			}
			$data .= "</table></td></tr>";
		}
	}
	$data .= "</table>";
	echo $data;

	$date = date("Y-m-d H:i:s");

	$query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
	$query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', 'Auto Attendant Modify', '" . $_SESSION["groupId"] . "', '" . $_SESSION["aa"]["aaName"] . "')";
	$sth = $db->query($query);

	$lastId = $db->lastInsertId();

	if (isset($changes))
	{
		for ($a = 0; $a < count($changes); $a++)
		{
			$key = key($changes[$a]);
//echo $key . "<br>";
			if ($key == "BusinessHours" or $key == "AfterHours" or $key == "Holiday" or strpos($key, "Submenu-") === 0)
			{
				if (isset($changes[$a][$key][$_SESSION["aaNames"]["id"]]) or isset($changes[$a][$key][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]) or isset($changes[$a][$key][$_SESSION["aaNames"]["enableLevelExtensionDialing"]]))
				{
					$insert = "INSERT into aaModChanges (id, serviceId, entityName, field, oldValue, newValue)";
					$values = " VALUES ('" . $lastId . "', '" . $aaId . "', '" . $_SESSION["aa"]["aaName"] . "', '" . addslashes($key) . " - ";
					if (isset($changes[$a][$key][$_SESSION["aaNames"]["id"]]))
					{
						$values .= $_SESSION["aaNames"]["id"] . "', '" . addslashes($changes[$a][$key][$_SESSION["aaNames"]["id"]]["oldValue"]) . "', '" . addslashes($changes[$a][$key][$_SESSION["aaNames"]["id"]]["newValue"]) . "')";
					}
					if (isset($changes[$a][$key][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]))
					{
						$values .= $_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"] . "', '" . $changes[$a][$key][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["oldValue"] . "', '" . $changes[$a][$key][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["newValue"] . "')";
					}
					if (isset($changes[$a][$key][$_SESSION["aaNames"]["enableLevelExtensionDialing"]]))
					{
						$values .= $_SESSION["aaNames"]["enableLevelExtensionDialing"] . "', '" . $changes[$a][$key][$_SESSION["aaNames"]["enableLevelExtensionDialing"]]["oldValue"] . "', '" . $changes[$a][$key][$_SESSION["aaNames"]["enableLevelExtensionDialing"]]["newValue"] . "')";
					}
					$query = $insert . $values;
					$rth = $db->query($query);
				}
				if (isset($changes[$a][$key]["keys"]))
				{
					foreach ($changes[$a][$key]["keys"] as $k => $v)
					{
						$digit = $k;
						$exp = explode(":", $v["oldValue"]);
						$oldDesc = $exp[0] ? $exp[0] : "None";
						$oldAct = $exp[1] ? $exp[1] : "None";
						$oldActData = $exp[2] ? $exp[2] : "None";

						$expN = explode(":", $v["newValue"]);
						$newDesc = $expN[0] ? $expN[0] : "None";
						$newAct = $expN[1] ? $expN[1] : "None";
						$newActData = $expN[2] ? $expN[2] : "None";

						if ($oldDesc !== $newDesc)
						{
							$insert = "INSERT into aaModChanges (id, serviceId, entityName, field, oldValue, newValue)";
							$values = " VALUES ('" . $lastId . "', '" . $aaId . "', '" . $_SESSION["aa"]["aaName"] . "', '" . addslashes($key) . " - Description for " . $digit . "', '" . $oldDesc . "', '" . $newDesc . "')";
							$query = $insert . $values;
							$rth = $db->query($query);
						}
						if ($oldAct !== $newAct)
						{
							$insert = "INSERT into aaModChanges (id, serviceId, entityName, field, oldValue, newValue)";
							$values = " VALUES ('" . $lastId . "', '" . $aaId . "', '" . $_SESSION["aa"]["aaName"] . "', '" . addslashes($key) . " - Action for " . $digit . "', '" . $oldAct . "', '" . $newAct . "')";
							$query = $insert . $values;
							$qth = $db->query($query);
						}
						if ($oldActData !== $newActData)
						{
							$insert = "INSERT into aaModChanges (id, serviceId, entityName, field, oldValue, newValue)";
							$values = " VALUES ('" . $lastId . "', '" . $aaId . "', '" . $_SESSION["aa"]["aaName"] . "', '" . addslashes($key) . " - Action Data for " . $digit . "', '" . $oldActData . "', '" . $newActData . "')";
							$query = $insert . $values;
							$yth = $db->query($query);
						}
					}
				}
			}
			else
			{
				$key = key($changes[$a]);
				$insert = "INSERT into aaModChanges (id, serviceId, entityName, field, oldValue, newValue)";
				$values = " VALUES ('" . $lastId . "', '" . $aaId . "', '" . $_SESSION["aa"]["aaName"] . "', '" . $key . "', '" . $changes[$a][$key]["oldValue"] . "', '" . $changes[$a][$key]["newValue"] . "')";
				$query = $insert . $values;
				$uth = $db->query($query);
			}
		}
	}
?>
