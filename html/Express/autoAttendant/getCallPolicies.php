<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/autoAttendant/aaOperation.php");

$announcement = new AaOperation();
$callPoliciesResult = "";

if(isset($_POST['announcementId']) && !empty($_POST['announcementId'])){
	$callPoliciesResult = $announcement->getCallPolicies($_POST['announcementId']);
	if(empty($callPoliciesResult['Error'])){
		$_SESSION["aa"]["redirectedCallsCOLPPrivacy"] = $callPoliciesResult['Success']['redirectedCallsCOLPPrivacy'];
		$_SESSION["aa"]["callBeingForwardedResponseCallType"] = $callPoliciesResult['Success']['callBeingForwardedResponseCallType'];
	}
}
echo json_encode($callPoliciesResult);
?>