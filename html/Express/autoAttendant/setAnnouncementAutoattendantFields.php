<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/Announcement/Announcement.php");
$announcement = new Announcement();

//echo "<pre>"; print_r($_SESSION['aa']);

$where = array();

$annouceArray = array();
if(isset($_SESSION['aa']['bh']['announcementSelection']) && !empty($_SESSION['aa']['bh']['announcementSelection'])){
	
	if(!empty($_SESSION['aa']['bh']['announcementId'])){
		
		//check the database if announcement exist. If not set autoattendant announcement to default
		$where['announcement_id'] = $_SESSION['aa']['bh']['announcementId'];
		$getAnnouncement = $announcement->getAnnouncement($db, $where);
		
		if(!empty($getAnnouncement[0]['announcement_id'])){
			$annouceArray['bh']['announcementSelection'] = $_SESSION['aa']['bh']['announcementSelection'];
			$annouceArray['bh']['announcementId'] = $getAnnouncement[0]['announcement_id'];	
			$annouceArray['bh']['announcement_file'] = $getAnnouncement[0]['announcement_file'];
		}else{
			$annouceArray['bh']['announcementSelection'] = "Default";
			$annouceArray['bh']['announcementId'] = "";
			$annouceArray['bh']['announcement_file'] = "";
		}
	}
}

if(isset($_SESSION['aa']['ah']['announcementSelection']) && !empty($_SESSION['aa']['ah']['announcementSelection'])){
	
	if(!empty($_SESSION['aa']['ah']['announcementId'])){
		
		//check the database if announcement exist. If not set autoattendant announcement to default
		$where['announcement_id'] = $_SESSION['aa']['ah']['announcementId'];
		$getAnnouncement = $announcement->getAnnouncement($db, $where);
		
		if(!empty($getAnnouncement[0]['announcement_id'])){
			$annouceArray['ah']['announcementSelection'] = $_SESSION['aa']['ah']['announcementSelection'];
			$annouceArray['ah']['announcementId'] = $getAnnouncement[0]['announcement_id'];
			$annouceArray['ah']['announcement_file'] = $getAnnouncement[0]['announcement_file'];
		}else{
			$annouceArray['ah']['announcementSelection'] = "Default";
			$annouceArray['ah']['announcementId'] = "";
			$annouceArray['ah']['announcement_file'] = "";
		}
	}
}

if(isset($_SESSION['aa']['h']['announcementSelection']) && !empty($_SESSION['aa']['h']['announcementSelection'])){
	
	if(!empty($_SESSION['aa']['h']['announcementId'])){
		
		//check the database if announcement exist. If not set autoattendant announcement to default
		$where['announcement_id'] = $_SESSION['aa']['h']['announcementId'];
		$getAnnouncement = $announcement->getAnnouncement($db, $where);
		
		if(!empty($getAnnouncement[0]['announcement_id'])){
			$annouceArray['h']['announcementSelection'] = $_SESSION['aa']['h']['announcementSelection'];
			$annouceArray['h']['announcementId'] = $getAnnouncement[0]['announcement_id'];
			$annouceArray['h']['announcement_file'] = $getAnnouncement[0]['announcement_file'];
		}else{
			$annouceArray['h']['announcementSelection'] = "Default";
			$annouceArray['h']['announcementId'] = "";
			$annouceArray['h']['announcement_file'] = "";
		}
	}
}


if(!empty($_SESSION['aa']['vp']['announcementId'])){
	$annouceArray['vp']['announcementId'] = $_SESSION['aa']['vp']['announcementId'];
	$where['announcement_id'] = $_SESSION['aa']['vp']['announcementId'];
	$getAnnouncement = $announcement->getAnnouncement($db, $where);
	$annouceArray['vp']['announcement_file'] = $getAnnouncement[0]['announcement_file'];
	if(!empty($getAnnouncement[0]['announcement_file'])){
		$annouceArray['vp']['announcement_file'] = $getAnnouncement[0]['announcement_file'];
	}else{
		$annouceArray['vp']['announcement_file'] = "";
	}
	
}else{
	$annouceArray['vp']['announcementId'] = "";
}

if(isset($_POST["aaId"]) && !empty($_POST["aaId"])){
    $where['auto_attendant_id'] = $_POST['aaId'];
    $announcementSub = $announcement->getAnnouncementSubMenu($db, $where);
    //echo "12345"; print_r($announcementSub);
    $annouceArray['submenu'] = $announcementSub[0];
}

echo json_encode($annouceArray);
?>