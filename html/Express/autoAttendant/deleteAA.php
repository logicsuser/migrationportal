<?php 
	require_once ("/var/www/html/Express/config.php");
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin ();
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/autoAttendant/aaController.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php"); // Added code @ 28 May 2018
                
	if(isset($_POST["modAAId"])){
            $obj = new AaController();            
            $delResp = $obj->deleteAutoAttendant($_POST["modAAId"]);            
            if(isset($delResp['Success']) && $delResp['Success']=="Success")
            {
                $changeLogObj = new ChangeLogUtility($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]); // Added code @ 28 May 2018
                $changeLogObj->changeLogDeleteUtility("Delete AutoAttendant", $_POST["modAAId"], "aaModChanges", "");
            }
            echo json_encode($delResp);
	}
?>