<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
//echo "<pre>"; print_r($_SESSION['aa']); print_r($_POST); die;
//$required = array("aaType", "aaName", "callingLineIdLastName", "callingLineIdFirstName", "timeZone", "extensionDialing", "nameDialing", "nameDialingEntries");
//$required = array("aaType", "timeZone", "extensionDialing", "nameDialing", "nameDialingEntries", "extensionAdd", "aaName");

$required = array("aaType", "timeZone", "extensionDialing", "nameDialing", "nameDialingEntries", "extensionAdd", "aaName");

$basicInfoArr = array("aaType", "aaName", "callingLineIdLastName", "callingLineIdFirstName", "department", "eVsupport", "timeZone", "businessHoursSchedule", "holidaySchedule", "extensionDialing", "nameDialing", "nameDialingEntries", "firstDigitTimeoutSeconds");
$formDataArr = array(
    "aaId" => "Auto Attendant Id",
    "aaType" => "Auto Attendant Type",
    "aaName" => "Auto Attendant name",
    "callingLineIdLastName" => "Calling Line Id Last Name",
    "callingLineIdFirstName" => "Calling Line Id First Name",
    "department" => "department",
    "eVsupport" => "Enable Video Support",
    "timeZone" => "timeZone",
    "businessHoursSchedule" => "Business Hours Schedule",
    "holidaySchedule" => "Holiday Schedule",
    "extensionDialing" => "Scope of Extension Dialing",
    "nameDialing" => "Scope of Name Dialing",
    "nameDialingEntries" => "Name Dialing Entries",
    "phoneNumberAdd" => "Phone Number",
    "extensionAdd" => "Extension",
	"firstDigitTimeoutSeconds" => "Transfer to operator after",		
);

foreach($_POST['formdata'] as $key=>$val){
    if(!empty($val['value'])){
        $postData[$val['name']] = $val['value'];
    }
    if(($val['name'] == "firstDigitTimeoutSeconds") && $val['value'] == 0){
    	$postData[$val['name']] = 0;
    }
}

foreach ($required as $key => $val){
    if(!array_key_exists($val, $postData)){
        $postData[$val] = "";
    }
}

$changeString = "";
$error = 0;
//print_r($postData);print_r($formDataArr); die;
foreach($postData as $key=>$val){

  $backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
    $bgColor = 'background: #ac5f5d;';
    if(in_array($key, $required) && $val == ""){
    		$error = 1;
    		$bg = INVALID;
    		$value = $formDataArr[$key] . " is a required field.";
    		$changeString .= '<tr><td class="errorTableRows" style="'.$bgColor.'">'.$formDataArr[$key].'</td><td class="errorTableRows">'.$value.' '. $errorMessage.' </td></tr>';
    }
    else if(in_array($val, $_SESSION["usedAANames"]) && $key <> "firstDigitTimeoutSeconds"){
    	if (isset($val) && strlen($val) > 0){
    		if($key == "aaName"){
    			$error = 1;
    			$bg = INVALID;
    			$value = " Auto Attendant with this name already exists.";
    			$changeString .= '<tr><td class="errorTableRows" style="'.$bgColor.'">'.$formDataArr[$key].'</td><td class="errorTableRows">'.$value.'</td></tr>';
    		}
    	}
    }
    else if(array_key_exists($key, $formDataArr)) {
    	if($key == "firstDigitTimeoutSeconds"){
    		if($val < 1 || $val > 60){
    			$error = 1;
    			$bg = INVALID;
    			$bgColor = 'background: #ac5f5d;';
    			$errorMessage = "<br/>Transfer to operator is a required field. It's minimum value should be 1 and maximum value should be 60.";
    			$changeString .= '<tr><td class="errorTableRows" style="'.$bgColor.'">'.$formDataArr[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
    		}else{
    			$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArr[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
    		}
    	}else{
    		$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArr[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
    	}
    }
}
echo $changeString;
