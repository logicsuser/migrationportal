<?php
	require_once ("/var/www/html/Express/config.php");
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin ();
	
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/autoAttendant/aaController.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/autoAttendant/aaController_rel_21.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/getDefaultDomain.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/arrays.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
	
	$formDataArr = array(
			"aaId" => "Auto Attendant Id",
			"aaType" => "Auto Attendant Type",
			"aaName" => "Auto Attendant name",
			"callingLineIdLastName" => "Calling Line Id Last Name",
			"callingLineIdFirstName" => "Calling Line Id First Name",
			"department" => "department",
			"eVsupport" => "Enable Video Support",
			"timeZone" => "timeZone",
			"businessHoursSchedule" => "Business Hours Schedule",
			"holidaySchedule" => "Holiday Schedule",
			"extensionDialing" => "Scope of Extension Dialing",
			"nameDialing" => "Scope of Name Dialing",
			"nameDialingEntries" => "Name Dialing Entries",
			"phoneNumberAdd" => "Phone Number",
			"extensionAdd" => "Extension",
			"firstDigitTimeoutSeconds" => "Transfer to operator after",
	);
	
	$changeLogObj = new ChangeLogUtility($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
	
	$sp = $_SESSION['sp'];
	$groupId = $_SESSION['groupId'];
	$groupName = isset($_SESSION["groupInfoData"]["groupName"]) ? $_SESSION["groupInfoData"]["groupName"] : "";
	$info = new AaController();
	$info21 = new AaController21();
	$addResp = array();
	$changesArray = array();
	
	require_once ("/var/www/lib/broadsoft/adminPortal/NameAndIDBuilder.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
	
	if($_POST["aaName"] == "" && empty($_POST["aaName"])){
	    $_POST["aaName"] = $info->constructAAName($groupId, $groupName, $_POST['phoneNumberAdd'], $_POST['extensionAdd'], $aaNameCriteriaValue);
	}
	
		
	$idValueMap = array();
	if(isset($_POST['phoneNumberAdd']) && !empty($_POST['phoneNumberAdd'])){
		$idValueMap["DN"] = $_POST['phoneNumberAdd']; // replace with AA DN
	}
	if(isset($_POST['extensionAdd']) && !empty($_POST['extensionAdd'])){
		$idValueMap["EXT"] = $_POST['extensionAdd']; // replace with AA EXT
	}
	if(isset($defaultDomain) && !empty($defaultDomain)){
		$idValueMap["DEFAULTDOMAIN"] = strval($defaultDomain);
	}
	
	$regexSet = array(
			ResourceNameBuilder::DN,
			ResourceNameBuilder::EXT,
			ResourceNameBuilder::DEFAULT_DOMAIN
	);
	
	$idRegex1 = $aaCriteria1Value;
	$idRegex2 = $aaCriteria2Value;
	
	$nameAndIdBuilder = new NameAndIDBuilder($idRegex1, $idValueMap, $regexSet);
	$aaId = $nameAndIdBuilder->getParsedId($idRegex1, $idValueMap);
	if(isset($aaId) && empty($aaId)) {
		$aaId= $nameAndIdBuilder->getParsedId($idRegex2, $idValueMap);
	}
	
	if(isset($aaId) && empty($aaId)) { //use default id
		$idDefaulRegex = $_POST['aaName']."-AA@<defaultDomain>";
		$aaId= $nameAndIdBuilder->getParsedId($idDefaulRegex, $idValueMap);
	}
	//regex code ends
	
	$_POST["aaId"] = $aaId;
	$changesArray["serviceId"] = $aaId;
	$changesArray["aaId"] = $aaId;
	
	if($_POST["callingLineIdLastName"] == "" && empty($_POST["callingLineIdLastName"])){
	   $_POST["callingLineIdLastName"] = $info->constructAAClidLName($groupId, $groupName, $_POST['phoneNumberAdd'], $_POST['extensionAdd'], $aaClidLnameValue);
	}
	
	if($_POST["callingLineIdFirstName"] == "" && empty($_POST["callingLineIdFirstName"])){
	    $_POST["callingLineIdFirstName"] = $_POST["callingLineIdFirstName"] = $info->constructAAClidFName($groupId, $groupName, $_POST['phoneNumberAdd'], $_POST['extensionAdd'], $aaClidFnameValue);
	}
	
	if($ociVersion == "19"){
		$addResp['aaAddResponse'] = $info->addAutoAttendant($sp, $groupId, $_POST);
	}else{
		$addResp['aaAddResponse'] = $info21->addAutoAttendant($sp, $groupId, $_POST);
	}
	$addResp['aaAddResponse']['aaIdMod'] = $aaId;
	if(empty($addResp['aaAddResponse']['Error'])){
		
			//code added to auto attendant
				$changeLog = array();
				foreach($_POST as $key=>$val){
					if(array_key_exists($key, $formDataArr)){
						$changeLog[$key] = $val;
					}
				}
				
				$serviceId = "";
				$changeLogObj = new ChangeLogUtility($serviceId, $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
				$module = "Add AutoAttendant";
				$tableName = "autoAttendantAddChanges";
				$entity = $_POST["aaName"];
				$changeResponse = $changeLogObj->changeLogAddUtility($module, $entity, $tableName, $changeLog);
				//print_r($changeResponse);
			//code ends
		
			$dnPostArray['phoneNumber'][0] = $_POST["phoneNumberAdd"];
			$dnPostArray['groupId'] = $_SESSION["groupId"];
			$dnsObj = new Dns();
			$dnsActiveResponse = $dnsObj->activateDNRequest($dnPostArray);
			foreach ($_POST as $changeLogKey => $changeLogValue){
			    if(array_key_exists($changeLogKey, $formDataArr)){
			        $changesArray[$changeLogKey] = $changeLogValue;
			    }
			    
			}
	}
	echo json_encode($addResp);
?>