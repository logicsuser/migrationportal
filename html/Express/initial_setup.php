<?php
/**
 * Created by Dipin Krishna.
 * Date: 6/07/18
 * Time: 10:23 AM
 */
?>
<?php
require_once("config.php");
require_once("functions.php");
$error_message = "";
$users = get_all_super_admin_users();
if (count(get_all_super_admin_users()) <= 0) {
	$_SESSION['INITIAL_SETUP'] = 1;
} else {
	header("Location: ./index.php");
	exit;
}

$passwordRulesMinimumLength = $adminPasswordRules->getMinimumLength();
$passwordRulesNumberOfDigits = $adminPasswordRules->getNumberOfDigits();
$passwordRulesSpecialCharacters = $adminPasswordRules->getSpecialCharacters();
$passwordRulesUpperCaseLetter = $adminPasswordRules->getUpperCaseLetter();
$passwordRulesLowerCaseLetter = $adminPasswordRules->getLowerCaseLetter();

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Express Admin Portal</title>
	<link rel="apple-touch-icon" sizes="57x57" href="images/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="images/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="images/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="images/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="images/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="images/favicon-32x32.png" sizes="32x32">
	<link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v15">
	<script type="application/javascript" src="/Express/js/jquery-ui/js/jquery-1.10.2.js"></script>
<!-- <script type="application/javascript" src="/Express/js/bootstrap.min.js"></script>-->
<script type="application/javascript" src="/Express/js/bootstrap/bootstrap.min.js"></script>
	<script type="application/javascript" src="/Express/js/jquery-1.17.0.validate.js"></script>
	<script type="application/javascript" src="/Express/js/jquery.loadTemplate.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/Express/css/custom.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/Express/css/bootstrap/bootstrap.min.css" media="screen" />
</head>
<body class="initialSetupBody">
<div class="averistar-bs3">
	<div class="container">
		<div class="panel panel-default" style="background-color: #eef0f3;">
			<div class="WelcomeForm">
				<img src="<?php echo $brandingLogoWebPath; ?>" style="max-height: 120px;width: auto;max-width: 330px;"/>
			<hr/>
			</div>
			
			<div class="panel-body">
			<div class="WelcomeForm">
				<h2>Welcome</h2>
				<hr/>
				<p>Welcome to Express. Please fill in the information below to start using the system.</p>

				<h2>Initial Setup Information</h2>
				<hr/>
				<p>Please provide the following information to create the Express Administrator Account.</p>
				<br/>
				<div class="alert alert-danger" style="<?php echo(isset($errorMessage) ? "" : "display:none;") ?>" id="error_msg">
					<?php echo isset($errorMessage) ? $errorMessage : ""; ?>
				</div>
			</div>
				<div>
					<form class="" id="initialSetupForm">
						<input type="hidden" name="adminType" value="2"/>
						<div class="row">
							<div class="form-group col-md-6">
								<label for="Username" class="labelText">User Name:</label>
									<input placeholder="Username" type="text" name="userName" id="userName" size="30" class="form-control" required/>
							</div>
							<div class="form-group col-md-6">
								<label for="emailAddress" class="labelText">Email:</label>
									<input placeholder="Email" type="email" name="emailAddress" id="emailAddress" class="form-control" required/>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								<label for="firstName" class="labelText">First Name:</label>
									<input placeholder="First Name" type="text" name="firstName" id="firstName" size="30" class="form-control" required/>
							</div>
							<div class="form-group col-md-6">
								<label for="lastName" class="labelText">Last Name:</label>
									<input placeholder="Last Name" type="text" name="lastName" id="lastName" size="30" class="form-control" required/>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								<label for="new_password" class="labelText">Enter New Password:</label>
									<input placeholder="Enter New Password"
									       type="password"
									       name="password"
									       id="new_password"
									       autocomplete='off'
									       class="form-control"
									       required/>
									<span id="password_strength" class="labelTextGrey">
										<?php
										echo "Your password must be at least 8 characters long. ";
										echo "Contain at least 1 digit";
										echo $passwordRulesLowerCaseLetter == 'true' ? ", 1 lower case letter" : "";
										echo $passwordRulesUpperCaseLetter == 'true' ? ", 1 upper case letter" : "";
										echo $passwordRulesSpecialCharacters == 'true' ? ", 1 special character" : ""; ?>.
									</span>
							</div>
							<div class="form-group col-md-6">
								<label for="new_password" class="labelText">Verify New Password:</label>
									<input required placeholder="Verify Password"
									       type="password"
									       name="confirm_password"
									       id="confirm_password"
									       autocomplete='off'
									       class="form-control"
									       required/>
							</div>
						</div>
						<div class="alignCenterBtn">
								<button type="button" class="subButton" name="setupSubmit" id="setup_submit">Submit</button>
						</div>
					</form>
					
				</div>
			</div>
		</div>
		<div class="copyrightIntSetup">
			Copyright © AveriStar All rights reserved.
		</div>
	</div>
</div>

<script type="application/javascript">
    $().ready(function () {

        $.validator.addMethod("strength", function (value) {

            //TextBox left blank.
            if (value.length === 0) {
                $("#password_strength").addClass("error");
                return false;
            }

            var password_strength = 1;

            //Validate for length of Password.
            if (value.length < <?php echo $passwordRulesMinimumLength ? $passwordRulesMinimumLength : 8 ?>) {
                password_strength = 0;
            } else if (value.replace(/[^0-9]/g, "").length < <?php echo $passwordRulesNumberOfDigits ? $passwordRulesNumberOfDigits : 1 ?>) {
                password_strength = 0;
            } else {
                //Regular Expressions.
                var regex = new Array();

				<?php
				if( $passwordRulesLowerCaseLetter == 'true') {
				?>
                regex.push("[a-z]"); //Lowercase Alphabet.
				<?php
				}
				?>
				<?php
				if( $passwordRulesUpperCaseLetter == 'true') {
				?>
                regex.push("[A-Z]"); //Uppercase Alphabet.
				<?php
				}
				?>
				<?php
				if( $passwordRulesSpecialCharacters == 'true') {
				?>
                regex.push("[^a-zA-Z0-9]"); //Special Character.
				<?php
				}
				?>

                //Validate for each Regular Expression.
                for (var i = 0; i < regex.length; i++) {
                    if (!new RegExp(regex[i]).test(value)) {
                        password_strength = 0;
                    }
                }
            }

            if (password_strength === 0) {
                $("#password_strength").addClass("error");
                return false;
            }

            $("#password_strength").removeClass("error");
            return true;

        }, '');

        $("#initialSetupForm").validate({
            rules: {
                password: "strength",
                confirm_password: {
                    required: true,
                    equalTo: "#new_password"
                }
            },
            messages: {
                confirm_password: {
                    required: "<small>Please enter the same password as above</small>",
                    equalTo: "<small>Password doesn't match</small>"
                }
            }
        });

        $("#setup_submit").click(function () {

            if ($("#initialSetupForm").valid()) {

                $("#error_msg").hide();
                $("#setup_submit").prop('disabled', true);
                $("#setup_submit").html("Please Wait ...");
                var formData = $("#initialSetupForm").serialize();

                $.ajax({
                    type: "POST",
                    url: "sysAdmin/createAdmin.php",
                    data: formData,
                    dataType: 'json',
                    success: function (result) {
                        if (result.success) {
                            window.location.replace("/Express/sysAdmin/sysAdminMod.php");
                        } else {

                            $("#setup_submit").prop('disabled', false);
                            $("#setup_submit").html("Create Admin");
                            $("#error_msg").html(result.msg);
                            $("#error_msg").show();
                        }
                    }
                }).fail(function () {
                    $("#error_msg").html("Unknown Error.");
                    $("#error_msg").show();
                    $("#setup_submit").prop('disabled', false);
                    $("#setup_submit").html("Create Admin");
                });

            }
        });

    });
</script>
<style>
	.error, .error text {
		color: red;
	}
</style>
</body>
</html>