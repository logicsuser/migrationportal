/**
 * Created by Dipin Krishna.
 * Date: 10/09/18
 * Time: 1:56 PM
 *
 * This is a DataTable extension for custom pagination
 * with page number and a Jump To Page input box.
 *
 */
(function ($) {
    $.extend($.fn.dataTableExt.oStdClasses, {
        'sPageJumpEllipsis': 'paginate_ellipsis',
        'sPageJumpNumber': 'paginate_number',
        'sPageJumpNumbers': 'paginate_numbers',
        'sPageJump': 'paginate_jump_to',
        'sPageJumpText': 'paginate_jump_text',
        'sPageJumpInput': 'paginate_jump_input',
        'sPageJumpGoBtn': 'paginate_jump_go_btn'
    });

    $.extend($.fn.dataTableExt.oLanguage, {
        oPaginate: {
            sPageJumpText: 'Jump to page ',
            sPageJumpGoBtn: 'GO'
        }
    });

    $.fn.dataTableExt.oPagination.dk_express_pagination = {
        'oDefaults': {
            'iShowPages': 5
        },
        'fnClickHandler': function (e) {
            //console.log(e);
            var fnCallbackDraw = e.data.fnCallbackDraw,
                oSettings = e.data.oSettings,
                sPage = e.data.sPage;

            if ($(this).is('[disabled]')) {
                return false;
            }

            oSettings.oApi._fnPageChange(oSettings, sPage);
            fnCallbackDraw(oSettings);

            return true;
        },
        // fnInit is called once for each instance of pager
        'fnInit': function (oSettings, nPager, fnCallbackDraw) {
            var oClasses = oSettings.oClasses,
                oLang = oSettings.oLanguage.oPaginate,
                that = this;

            var iShowPages = oSettings.oInit.iShowPages || this.oDefaults.iShowPages,
                iShowPagesHalf = Math.floor(iShowPages / 2);

            $.extend(oSettings, {
                _iShowPages: iShowPages,
                _iShowPagesHalf: iShowPagesHalf
            });

            var oNumbers = $('<span class="' + oClasses.sPageJumpNumbers + '"></span>');
            var oPageJump = $('<span class="' + oClasses.sPageJump + '"></span>');

            var nPage = document.createElement('span');
            var nGoBtn = document.createElement('button');

            nGoBtn.type = 'button';
            nPage.innerHTML = oLang.sPageJumpText;
            nGoBtn.innerHTML = oLang.sPageJumpGoBtn;

            nPage.className = oClasses.sPageJumpText;
            nGoBtn.className = oClasses.sPageJumpGoBtn;
            var nInput = document.createElement('input');
            nInput.type = 'number';
            nInput.value = '1';
            nInput.min = '1';
            nInput.className = oClasses.sPageJumpInput;

            $(nInput).keyup(function (e) {
                //console.log(this.value);
                /*
                // 38 = up arrow, 39 = right arrow
                if (e.which === 38 || e.which === 39) {
                    this.value++;
                }
                // 37 = left arrow, 40 = down arrow
                else if ((e.which === 37 || e.which === 40) && this.value > 1) {
                    this.value--;
                }
                */

                // 38 = up arrow, 39 = right arrow
                if (e.which === 39) {
                    this.value++;
                }
                // 37 = left arrow, 40 = down arrow
                else if ((e.which === 40) && this.value > 1) {
                    this.value--;
                }

                if (this.value === '' || this.value.match(/[^0-9]/)) {
                    /* Nothing entered or non-numeric character */
                    this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                    return;
                }

                var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                if (iNewStart < 0) {
                    iNewStart = 0;
                }
                if (iNewStart >= oSettings.fnRecordsDisplay()) {
                    iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                }

                oSettings._iDisplayStart = iNewStart;
                fnCallbackDraw(oSettings);
            });

            $(nInput).change(function () {
                //console.log("Changed" + this.value + ' - ' + oSettings._iCurrentPage);
                if(this.value === oSettings._iCurrentPage) {
                    return;
                }

                if (this.value === '' || this.value.match(/[^0-9]/)) {
                    /* Nothing entered or non-numeric character */
                    this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                    return;
                }

                var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                if (iNewStart < 0) {
                    iNewStart = 0;
                }
                if (iNewStart >= oSettings.fnRecordsDisplay()) {
                    iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                }

                oSettings._iDisplayStart = iNewStart;
                fnCallbackDraw(oSettings);
            });

            $(nInput).click(function () {
                //console.log("Changed" + this.value + ' - ' + oSettings._iCurrentPage);
                if(this.value === oSettings._iCurrentPage) {
                    return;
                }

                if (this.value === '' || this.value.match(/[^0-9]/)) {
                    /* Nothing entered or non-numeric character */
                    this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
                    return;
                }

                var iNewStart = oSettings._iDisplayLength * (this.value - 1);
                if (iNewStart < 0) {
                    iNewStart = 0;
                }
                if (iNewStart >= oSettings.fnRecordsDisplay()) {
                    iNewStart = (Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
                }

                oSettings._iDisplayStart = iNewStart;
                fnCallbackDraw(oSettings);
            });

            oPageJump.append(nPage);
            oPageJump.append(nInput);
            //oPageJump.append(nGoBtn);

            // Draw
            $(nPager).append(oNumbers, oPageJump);

        },
        // fnUpdate is only called once while table is rendered
        'fnUpdate': function (oSettings, fnCallbackDraw) {
            var oClasses = oSettings.oClasses,
                oLang = oSettings.oLanguage.oPaginate,
                that = this;

            var tableWrapper = oSettings.nTableWrapper;

            // Update stateful properties
            this.fnUpdateState(oSettings);

            var i, oNumber, oNumbers = $('.' + oClasses.sPageJumpNumbers, tableWrapper);
            var oPageJump = $('.' + oClasses.sPageJump, tableWrapper);
            var oPageJumpInput = $('.' + oClasses.sPageJumpInput, tableWrapper);


            // Erase
            oNumbers.html('');

            for (i = oSettings._iFirstPage; i <= oSettings._iLastPage; i++) {
                oNumber = $('<a class="' + oClasses.sPageButton + ' ' + oClasses.sPageJumpNumber + '">' + oSettings.fnFormatNumber(i) + '</a>');

                if (oSettings._iCurrentPage === i) {
                    oNumber.attr('active', true).attr('disabled', true);
                    oNumber.addClass('active');
                } else {
                    oNumber.click({'fnCallbackDraw': fnCallbackDraw, 'oSettings': oSettings, 'sPage': i - 1}, that.fnClickHandler);
                }

                // Draw
                oNumbers.append(oNumber);
            }

            // Add ellipses
            if (1 < oSettings._iFirstPage) {
                oNumbers.prepend('<span class="' + oClasses.sPageJumpEllipsis + '">...</span>');

                //Insert Page '1'
                oNumber = $('<a class="' + oClasses.sPageButton + ' ' + oClasses.sPageJumpNumber + '">' + oSettings.fnFormatNumber(1) + '</a>');
                oNumber.click({'fnCallbackDraw': fnCallbackDraw, 'oSettings': oSettings, 'sPage': 0}, that.fnClickHandler);
                oNumbers.prepend(oNumber);
            }

            if (oSettings._iLastPage < oSettings._iTotalPages) {
                oNumbers.append('<span class="' + oClasses.sPageJumpEllipsis + '">...</span>');

                //Insert Last Page
                oNumber = $('<a class="' + oClasses.sPageButton + ' ' + oClasses.sPageJumpNumber + '">' + oSettings.fnFormatNumber(oSettings._iTotalPages) + '</a>');
                oNumber.click({'fnCallbackDraw': fnCallbackDraw, 'oSettings': oSettings, 'sPage': oSettings._iTotalPages - 1}, that.fnClickHandler);
                oNumbers.append(oNumber);
            }

            if (oSettings._iTotalPages > oSettings._iShowPages) {
                oPageJump.show();
                oPageJumpInput.prop('max', oSettings._iTotalPages);
                oPageJumpInput.val(oSettings._iCurrentPage);
            } else {
                oPageJump.hide();
            }

        },
        // fnUpdateState used to be part of fnUpdate
        // The reason for moving is so we can access current state info before fnUpdate is called
        'fnUpdateState': function (oSettings) {
            var iCurrentPage = Math.ceil((oSettings._iDisplayStart + 1) / oSettings._iDisplayLength),
                iTotalPages = Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength),
                iFirstPage = iCurrentPage - oSettings._iShowPagesHalf,
                iLastPage = iCurrentPage + oSettings._iShowPagesHalf;

            if (iTotalPages < oSettings._iShowPages) {
                iFirstPage = 1;
                iLastPage = iTotalPages;
            } else if (iFirstPage < 1) {
                iFirstPage = 1;
                iLastPage = oSettings._iShowPages;
            } else if (iLastPage > iTotalPages) {
                iFirstPage = (iTotalPages - oSettings._iShowPages) + 1;
                iLastPage = iTotalPages;
            }

            $.extend(oSettings, {
                _iCurrentPage: iCurrentPage,
                _iTotalPages: iTotalPages,
                _iFirstPage: iFirstPage,
                _iLastPage: iLastPage
            });
        }
    };
})(jQuery);