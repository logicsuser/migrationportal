/*
 * Created by Dipin Krishna.
 * Date: 10/04/18
 * Time: 5:45 PM
*/

var dkFB_Params = {};
var dkFB_Filters_Status = {};
var dkFB_Row_Count = 1;


function clickedDKFBAddFilterRowBtn(btnObj, divID) {

    //console.log(dkFB_Params[divID]);

    if(dkFB_Params[divID]) {

        var filters = dkFB_Params[divID]['filters'];

        var filters_used_count = Object.keys(dkFB_Filters_Status[divID]).length;

        //console.log(filters.length);
        //console.log(filters_used_count);

        if(filters && Object.keys(filters).length > filters_used_count) {

            var filter_row = '<div class="row dkFB_Filter_Item_Row fcorn-registerTwo adminAddBtnDiv">';

            filter_row += '<div class="col-md-2" style="width: 21% !important;">';
            filter_row += '<div class="form-group">';
            filter_row += '<label class="labelText">Select Filter</label>';
            filter_row += '<div class="dropdown-wrap">';
            filter_row += '<select data-row="' + dkFB_Row_Count + '" name="filterItem[' + dkFB_Row_Count + ']" class="dkFB_Filter_Item_Dropdown" onchange="changedDKFBFilterItem(this, \''+ divID +'\')" style="width:100% !important">';
            var default_filter_value = '';
            $.each( filters, function( filter_value, filter ) {
                var disable_option = dkFB_Filters_Status[divID][filter_value] ? ' disabled ' : '';
                if(!disable_option && !default_filter_value) {
                    default_filter_value = filter_value;
                }
                filter_row += '<option class="dkFB_Filter_Item_Dropdown_Option" ' + disable_option + ' value="' + filter_value + '">'+ filter['title'] + '</option>';
            });
            filter_row += '</select>';
            filter_row += '</div>';
            filter_row += '</div>';
            filter_row += '</div>';

            filter_row += '<div class="col-md-2" style="width: 21% !important;">';
            filter_row += '<div class="form-group">';
            filter_row += '<label class="labelText">Select Mode</label>';
            filter_row += '<div class="dropdown-wrap">';
            filter_row += '<select data-row="' + dkFB_Row_Count + '" name="filterItemMode[' + dkFB_Row_Count + ']" class="dkFB_Filter_Item_Mode_Dropdown" style="width:100% !important">';
            $.each( filters[default_filter_value]['modes'], function( key, mode ) {
                filter_row += '<option value="' + mode + '">'+ mode +'</option>';
            });
            filter_row += '</select>';
            filter_row += '</div>';
            filter_row += '</div>';
            filter_row += '</div>';
            filter_row += '<div class="col-md-6"style="width: 41%;">';
            filter_row += '<div class="form-group">';
            filter_row += '<label class="labelText">Search String</label>';
             
            filter_row += '<input type="text" data-row="' + dkFB_Row_Count + '" name="filterItemValue[' + dkFB_Row_Count + ']"  class="dkFB_Filter_Item_Value_Input" style="width:100% !important"/ >';
            filter_row += '</div>';
            filter_row += '</div>';
            
            filter_row += '<div class="col-md-2 filterMarginTopAlign">';
            //filter_row += '<label class="labelText">&nbsp;</label>';
            filter_row += '<button type="button" onclick="clickedDKFBRemoveFilterRowBtn(this, \''+ divID +'\' )" class="deleteBtn deleteSmallButton dkFBRemoveFilterRowBtn dkFBRemoveFilterRowBtn_' + divID + '">Remove</button>';
            filter_row += '</div>';

            $(btnObj).closest('.dkFB_Block').append(filter_row);

            updateFilterItemDropdowns(divID);

            dkFB_Row_Count++;

        }

    }

}

function clickedDKFBRemoveFilterRowBtn(btnObj, divID) {

    $(btnObj).closest('.dkFB_Filter_Item_Row').remove();
    updateFilterItemDropdowns(divID);

}

function clickedDKFBResetFiltersBtn(btnObj, divID) {

    $(btnObj).closest('.dkFB_Block').find('.dkFB_Filter_Item_Row').remove();
    updateFilterItemDropdowns(divID);

    clickedDKFBAddFilterRowBtn('#dkFBAddFilterRowBtn_' + divID, divID);

    if(dkFB_Params[divID]) {
        var resetCallback = dkFB_Params[divID]['resetCallback'];
        if(resetCallback) {
            resetCallback();
        }
    }
}

function changedDKFBFilterItem(dropdownObj, divID) {
    var filter_value = $(dropdownObj).val();
    var row = $(dropdownObj).data('row');
    var mode_options = '';
    $.each( dkFB_Params[divID]['filters'][filter_value]['modes'], function( key, mode ) {
        mode_options += '<option value="' + mode + '">'+ mode +'</option>';
    });
    $('select[name="filterItemMode[' + row + ']"]').html(mode_options);

    updateFilterItemDropdowns(divID);
}

function updateFilterItemDropdowns(divID) {

    var filters_used_count = $("select.dkFB_Filter_Item_Dropdown").length;

    if(dkFB_Params[divID] && !dkFB_Params[divID]['allowMultiple']) {

        dkFB_Filters_Status[divID] = {};
        $('.dkFB_Filter_Item_Dropdown_Option').prop('disabled', false);
        $('.dkFB_Filter_Item_Dropdown_Option').removeClass('disabled');
        $('select.dkFB_Filter_Item_Dropdown').each(function (i, dropdownObj) {
            var item = $(dropdownObj).val();
            dkFB_Filters_Status[divID][item] = 1;
            $("select.dkFB_Filter_Item_Dropdown").not(dropdownObj).find("option[value="+ $(dropdownObj).val() + "]").prop('disabled', true).addClass('disabled');
        });

        var filters_count = Object.keys(dkFB_Params[divID]['filters']).length;
        filters_used_count = Object.keys(dkFB_Filters_Status[divID]).length;

        var addBtnObj = $('#dkFBAddFilterRowBtn_' + divID);
        if(addBtnObj) {
            $(addBtnObj).prop('disabled', (filters_count <= filters_used_count));
        }

    }

    var removeRowBtns = $('.dkFBRemoveFilterRowBtn_' + divID);
    removeRowBtns.prop('disabled', filters_used_count === 1);
    removeRowBtns.toggleClass('disabled', filters_used_count === 1);
    //removeRowBtns.toggle(filters_used_count !== 1);
}

/*
params = {
    'filters' : [
        'filter_1' :{
            'title' : 'Filter 1',
            'modes' : ['Contains', 'Starts with', 'Equal To']
        },
        'filter_2' :{
            'title' : 'Filter 1',
            'modes' : ['Contains', 'Starts with', 'Equal To']
        }
        'filter_3' :{
            'title' : 'Filter 1',
            'modes' : ['Contains', 'Starts with', 'Equal To']
        }
    ],
	'resetCallback': callbackFUnction
}
*/
function buildDKFilters(blockID, params) {

    if(params && params['filters'] && Object.keys(params['filters']).length) {

        var divObj = $(blockID);
        var divID = Math.random().toString(36).substr(2, 15);

        //console.log(divID);

        dkFB_Params[divID] = params;
        dkFB_Filters_Status[divID] = {};

        //clear the div
        divObj.html('');

        var divID_input_field = '<input type="hidden" class="dkFB_divID" value="' + divID + '"/>';

        var dkFB_add_btn_html = '<div class="col-md-6"><input onclick="clickedDKFBAddFilterRowBtn(this, \'' + divID + '\')" '
            + ' class="dkFBAddFilterRowBtn subButton deleteSmallButton userFilterAddBtnAlign" value="Add"'
            + ' id="dkFBAddFilterRowBtn_' + divID + '" '
            + ' style=""'
            + ' type="button"/></div>';
        var dkFB_reset_btn_html = '<div class="col-md-6 form-group"><input onclick="clickedDKFBResetFiltersBtn(this, \'' + divID + '\' )" '
            + ' class="dkFBResetFiltersBtn deleteBtn deleteSmallButton userFilterResetButtonAlign" value="Reset"'
            + ' style=""'
            + ' type="button"/></div>';
        var dkFB_top_buttons_row_html = '<div class="row fcorn-registerTwo adminAddBtnDiv">' + dkFB_add_btn_html + dkFB_reset_btn_html + '</div>';

        divObj.append('<div class="dkFB_Block">' + divID_input_field + dkFB_top_buttons_row_html + '</div>');

        clickedDKFBAddFilterRowBtn('#dkFBAddFilterRowBtn_' + divID, divID);

    }
}