<?php
/**
 * Created by Dipin Krishna.
 * Date: 10/27/17
 * Time: 9:51 AM
 */
?>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY']) < $sessionTimeout) {
	if(isset($_GET['set'])) {
		$_SESSION['LAST_ACTIVITY'] = time();
	}
	echo '{ "success": 1, "TIMEOUT": "' . ($sessionTimeout - (time() - $_SESSION['LAST_ACTIVITY'])) . '"}';
} else {
	echo '{ "success": 0 }';
}
exit;
?>