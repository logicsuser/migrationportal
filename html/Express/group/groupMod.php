<?php
//TODO: Remove once new group functionality is developed

	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
?>
<script>
	var buttonClick = "";

	$(function() {
		$("#module").html("> Group Modify: ");
		$("#endUserId").html("<?php echo $_SESSION["groupId"]; ?>");

		$("#dialogG").dialog({
			autoOpen: false,
			width: 800,
			modal: true,
			position: { my: "top", at: "top" },
			resizable: false,
			closeOnEscape: false,
			open: function(event) {
				setDialogDayNightMode($(this));
				$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('completeButton');
				$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
			},
			buttons: {
				"Complete": function() {
					var dataToSend = $("form#groupEdit").serializeArray();
					$.ajax({
						type: "POST",
						url: (buttonClick == "subButton" ? "" : "group/deleteGroup.php"),
						data: dataToSend,
						success: function(result) {
							$("#dialogG").dialog("option", "title", "Request Complete");
							$(".ui-dialog-titlebar-close", this.parentNode).hide();
							$(".ui-dialog-buttonpane", this.parentNode).hide();
							$("#dialogG").html(result);
							$("#dialogG").append("<p><a href=\"chooseGroup.php\">Click here to return to the group selection</a></p>");
						}
					});
				},
				"Cancel": function() {
					$(this).dialog("close");
				}
			}
		});

		$("#subButtonDel").click(function()
		{
			buttonClick = this.id;
			$("#dialogG").dialog("open");
			$("#dialogG").html("This will delete all users, settings, and information associated with the group \"<?php echo $_SESSION["groupId"]; ?>\". Are you sure you want to delete this group?");
		});
	});
</script>
<div class="subBanner">Modify Group</div>
<div class="vertSpacer">&nbsp;</div>

<form name="groupEdit" id="groupEdit">
	<div class="centerDesc">
		<input type="button" id="subButtonDel" value="Delete Group">
	</div>
</form>
<div id="dialogG" class="dialogClass"></div>
<?php
/*	require_once("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getDeviceList.php");

	$cisco7960Counter = 0;
	$polycomCounter = 0;
	$yealinkCounter = 0;
	$otherCounter = 0;
	if (isset($devices))
	{
		foreach ($devices as $key => $value)
		{
			if (strpos($value["deviceType"], "Cisco") === 0 and strpos($value["deviceType"], "7960"))
			{
				$cisco7960Counter++;
			}
			else if (strpos($value["deviceType"], "Polycom") === 0)
			{
				$polycomCounter++;
			}
			else if (strpos($value["deviceType"], "Yealink") === 0)
			{
				$yealinkCounter++;
			}
			else
			{
				$otherCounter++;
			}
		}
	}

//	echo "@" . $value["deviceName"] . "@" . $value["deviceType"] . "@<BR>";

//	$countyealink = array_keys($myArray, "Yealink-T46G");
//	$length = count($countyealink);
//	echo "@" . $length . "@";

	echo "<div style=\"clear:both;\"><div style=\"float:left;width:15%;margin-left:30%;\">Total Users</div><div style=\"float:left;\">" . (isset($users) ? count($users) : "0") . "</div></div>";
	echo "<div style=\"clear:both;\"><div style=\"float:left;width:15%;margin-left:30%;\">Total Devices</div><div style=\"float:left;\">" . (isset($devices) ? count($devices) : "0") . "</div></div>";
	echo "<div style=\"clear:both;\"><div style=\"float:left;width:10%;margin-left:33%;\">Cisco 7960</div><div style=\"float:left;margin-left:3%;\">" . $cisco7960Counter . "</div></div>";
	echo "<div style=\"clear:both;\"><div style=\"float:left;width:10%;margin-left:33%;\">Polycom</div><div style=\"float:left;margin-left:3%;\">" . $polycomCounter . "</div></div>";
	echo "<div style=\"clear:both;\"><div style=\"float:left;width:10%;margin-left:33%;\">Yealink</div><div style=\"float:left;margin-left:3%;\">" . $yealinkCounter . "</div></div>";
	echo "<div style=\"clear:both;\"><div style=\"float:left;width:10%;margin-left:33%;\">Other</div><div style=\"float:left;margin-left:3%;\">" . $otherCounter . "</div></div>";
//	echo "<div style=\"clear:both;\"><div style=\"float:left;width:15%;margin-left:30%;\">Total User Types</div></div>";
//	echo "<div style=\"clear:both;\"><div style=\"float:left;width:10%;margin-left:33%;\">Normal</div><div style=\"float:left;margin-left:3%;\">" . "</div></div>";
//	echo "<div style=\"clear:both;\"><div style=\"float:left;width:10%;margin-left:33%;\">Call Center</div><div style=\"float:left;margin-left:3%;\">" . "</div></div>";
//	echo "<div style=\"clear:both;\"><div style=\"float:left;width:10%;margin-left:33%;\">Auto Attendant</div><div style=\"float:left;margin-left:3%;\">" . "</div></div>";
*/
?>
