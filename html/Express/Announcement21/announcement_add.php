<?php 
require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
$serP = $_SESSION["sp"];
require_once ("/var/www/lib/broadsoft/adminPortal/getAllGroups.php");

?>
<script type="text/javascript" src="Announcement21/js/announcement_adnl.js"></script>
<?php if($_SESSION["sp"] && !isset($_SESSION["groupId"])){ ?>
<script type="text/javascript">
    var selectedSP = "<?php echo $serP; ?>";
    getGroupList(selectedSP);
</script>
<?php  } ?>
<script type="text/javascript" src="Announcement21/js/upload.js"></script>

<style>
input[type='file'] {
  opacity:0    
}
</style>
<div class="adminUserText" style="padding-top:0;">Add Announcement</div>
<div class="vertSpacer">&nbsp;</div>
<form method="POST" id="announcementForm" name="announcementForm" class="fcorn-registerTwo">
	
	
	 <?php $displayGroups = ! isset($_SESSION["groupId"]) ? "block" : "none"; ?>
	 
		<div class="" style="text-align: center ;display: <?php echo $displayGroups; ?>;">
        	<div class="row">
<!--             	<div class="col-md-3"></div> -->
        		<div class="col-md-12">
         			<label for="announcementGroup" class="labelText">Select Group:</label><span class="required">*</span><br/>
         			<div class="form-group">
         				 <div class="">
         				 	<!--
                                                <select style="width: 100% !important;" name="announcementGroup" id="announcementGroup">
                				<option value="">Select</option>
                				<?php 
                				/*$groupHtml = "";
                				foreach ($allGroups as $groupName){
                				    $groupHtml .= "<option value='".$groupName."'";
                				    if($_SESSION["groupId"] == $groupName){
                				    	$groupHtml .= "selected";
                				    }
                				    $groupHtml .= " >".$groupName."</option>";
                				}
                				echo $groupHtml;
                                                 */
                				?>
    						</select>
                                                -->
                                             <!-- Code added @ 10 jan 2019 Regarding EX-1032 -->
                                            <input class="form-control blue-dropdown magnify" style="overflow:scroll;" type="text" class="autoFill" name="announcementGroups" id="announcementGroups">
                                            <?php 
                                                 if(isset($_SESSION["groupId"]) && $_SESSION["groupId"]!="") {
                                                     $groupId = $_SESSION["groupId"];
                                                     echo "<input class='form-control' type='hidden' name='announcementGroup' id='announcementGroup' value='$groupId'>";
                                                 }
                                                 else{
                                                      echo "<input class='form-control' type='hidden' name='announcementGroup' id='announcementGroup'>";
                                                 }
                                            ?>

                                            <!-- End code -->
         				 </div>
         			</div>
         	 	</div>
         	 </div>
         </div>
         
         		<div class="row">
             	 	<div class="col-md-6">
             			<label for="announcementType" class="labelText">Announcement Type:</label><span class="required">*</span><br/>
             			<div class="form-group">
             				 <div class="dropdown-wrap">
             				 	<select name="announcementMediaType" id="announcementMediaType">
                    				<option value="">Select</option>
                <!-- 				<option value="WAV">WAV</option> -->
                <!-- 				<option value="3GP">3GP</option> -->
                <!-- 				<option value="MOV">MOV</option> -->
                <!-- 				<option value="WMA">WMA</option> -->
                					<option value="Audio">Audio</option>	
                					<option value="Video">Video</option>
        						</select>
             				 </div>
             			</div>
             		</div>
             		
             		<div class="col-md-6">
             			<label for="announcementName" class="labelText">Announcement Name:</label><span class="required">*</span><br/>
             			<div class="form-group">
             				 <input type="text" name="announcementName" id="announcementName" size="35" maxlength="30" >
             			</div>
             		</div>
         		</div>
         		
         	<div class="row">
         		<div class="col-md-3">
         			<label for="announcementFile" class="labelText">Announcement File:</label><span class="required">*</span><br/>
         			<div class="form-group">
         				<input type="button" class="addAnnouncement" id="uploadFile" value="Upload File">
         				<input type="file" name="announcementUpload" id="announcementUpload" />
						<input type="hidden" name="announcementUploadVal" id="announcementUploadVal" value="" />
						<div class="col span_2" id="chkFileUpload"></div>
						<div class="col span_2" id="showFileUpload"></div>
         			</div>
         		</div>
         	</div>
<!--              	<div class="col-md-3"></div> -->
         	 
         	<!-- select Announcement type -->
         
    
 </form><!-- end annoucement add form -->