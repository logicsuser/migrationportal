$(function() {
	/* used for tooltip instraction icon select all */
	$('[data-toggle="tooltip"]').tooltip();
	/*end i icon message */
	
	var inUseGlobal = false;
	var modifiedAnnouncementData = ""; 
	
	var hideOnLoad = function(){
		$("#announcementListDiv").hide(); // hide announcement div on load
		$("#fileSizeDiv").hide(); // hide file size
		$("#selectCheckDiv").hide(); // hide file size
		$("#deleteBtnDiv").hide(); // hide file size
	}
	hideOnLoad();
	
	//check teh value of click event
	var searchMode = function(){
		$("#selectAllChk").prop("checked", false);
		var smode = $("#searchMode").val();
		if(smode == "search"){
			announcementList();
		}
	}
	//check teh value of click event
	var errorInAnnForm = false;
	var clickMode = "";
	/*var clickMode = function(){
		var cmode = $("#clickMode").val();
		if(cmode == "add"){
			//announcementList();
			
		}
	}
	*/
	
       var hideSelectCheckBox = function() {
		if( $(".checkAnnouncement:disabled").length == $(".checkAnnouncement").length ) {
                        //$("#selectCheckDiv").show();
                        $("#selectAllChk").prop("disabled",true);
                        $("#selectAllChk + label span").css("opacity","0.4");
		}else{
                    $("#selectAllChk").prop("disabled",false);
                    $("#selectAllChk + label span").css("opacity","1");
                } 
	}
	
	var showOnLoad = function(){
		$("#announcementListDiv").show(); // hide announcement div on load
		$("#fileSizeDiv").show(); // hide file size
		$("#selectCheckDiv").show(); // hide file size
		$("#deleteBtnDiv").show(); // hide file size
	}
	
	var dlg = $('#announceAddModal').dialog({
		resizable : true,
		autoOpen : false,
		modal : true,
		width : 800,
		position : {
			my : "top",
			at : "top"
		},
		height : 500,
		resizable: false,
		closeOnEscape: false,
		buttons: {
			 
			"Confirm Settings": function() {
				confirmSetting();
				$("#selectAllChk").prop("checked", false);
			},
			"Cancel" : function(){
				
				//searchMode();
				$(this).dialog("close");
                                $("#announceAddModal").html(''); //Code added @ 26 Aril 2019
				//$("#selectAllChk").prop("checked", false);
			},
			 
		},
        open: function() {
                $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019
        	errorInAnnForm = false;
        	$("#dialogPlayMedia").show();
        	buttonsShowHide('Cancel', 'show');
        	setDialogDayNightMode($(this));
        	/* Button styling.*/
        	$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
        	$(":button:contains('Confirm Settings')").addClass('subButton');
        }
	});
	
	
	//function to load add view for announcement
	var loadAddView = function(){
		clickMode = "add";
		console.log("add called");
		
		$.ajax({
			url : 'Announcement21/announcement_add.php',
			type : 'GET',
			//data : { searchCriteria : searchCriteriaVal, searchAnnouncement : searchAnnouncementVal, searchLevel : searchLevelVal},
			success : function(data) {
                            $("#announceAddModal").html(data);
                            $("#announceAddModal").dialog("open");
                            // dlg.dialog('open');
			}
		});
		
		/*dlg.load('Announcement21/announcement_add.php', function() {
			
			dlg.dialog('open');
		});*/
	}
	
	// announcement add click event
	/*$('#addAnnouncement').click(function(e) {
		//e.preventDefault();
		console.log("add announcement clicked...")
		loadAddView();
	});*/
	
	$("#addAnnouncement").unbind().click(function() {
		console.log("add announcement clicked...")
		loadAddView();
	});

//	var countRecords = function(e){
//		if(e > 4){
//			//warning error show and append html
//			$("#warningErrorMsg").show();
//			$("#warningErrorMsg").html("There are " + e + " matching announcements - please narrow your search;");
//			$("#announcementListDiv").hide();alert("hi");
//			return false;
//		}else{
//			//warning error hide and append html
//			$("#warningErrorMsg").html("");
//			$("#warningErrorMsg").hide();
//		}
//	}
	// announcement list table function
	var announcementListTable = function(data) {
		
		// break data on the basis of 
		// TotalFileSize string so that main
		// table can be insert into div
		data = data.split("TotalFileSize");
		tableHtml = data[0]; // get the data for table
		sizeStr = data[1]; // make the data for file size
		
		sizeData = sizeStr.split("MaxFileSize"); // break on the basis of data parsing the max filesize	
		totalFileSizaVal = sizeData[0];
		maxFileSizeStr = sizeData[1];
		
		sizeData = maxFileSizeStr.split("CountRecords"); // break on the basis of data parsing the max filesize	
		maxFileSizeVal = sizeData[0];
		countRecords = sizeData[1]; //get the total records for the output
		
//		countRecords(numRecords);
		if(countRecords > 500){
			//warning error show and append html
			$("#warningErrorMsg").show();
			$("#warningErrorMsg").html("There are " + countRecords + " matching announcements - please narrow your search;");
			$("#announcementListDiv").hide();
			hideOnLoad();
		}else{
		
			if(countRecords == 0){
				hideOnLoad();
				$("#announcementListDiv").show(); // hide announcement div on load
			}else{
				showOnLoad();
			}
			
			//warning error hide and append html
			$("#warningErrorMsg").html("");
			$("#warningErrorMsg").hide();
	
			$("#announcementListDiv").html(tableHtml); // insert data html into div
	
			$("#totalFileSize").html('<label class="labelText" style="margin-left:10px">'+totalFileSizaVal+'</label>'); // append data for total file size
			$("#maxFileSize").html('<label class="labelText" style="margin-left:10px">'+maxFileSizeVal+'</label>');// append data for max fiel size
			
			var columnIndex = $('#announcementTable th:contains("Announcement Name")').index();
			$(document).find('#announcementTable').tablesorter({
			    sortList : [[ columnIndex, 0 ]]
			});
			hideSelectCheckBox();
		}
		
		createFileDowloadFileLink();
		$("#deleteBtnDiv").hide(); // hide file size
	}

	var createFileDowloadFileLink = function() {
		$(document).find(".downloadAnn").each(function() {
			var downloadFileHref = window.location.origin+ '/broadsoft/' + $(this).attr('href');
			$(this).attr('href', downloadFileHref);
		});
	}
	// announcement list function ajax request
	var announcementList = function() {
		$("#selectAllChk").prop("checked", false);
		var searchCriteriaVal = $("#searchCriteria").val();
		var searchAnnouncementVal = $("#searchAnnouncement").val();
		var searchLevelVal = $("input[name=searchlevel]:checked").val();
		$("#loadingBar").show();
		hideOnLoad();
		
		$.ajax({
			url : 'Announcement21/ajax/announcementList.php',
			type : 'POST',
			data : { searchCriteria : searchCriteriaVal, searchAnnouncement : searchAnnouncementVal, searchLevel : searchLevelVal},
			success : function(data) {
				if(foundServerConErrorOnProcess(data, "")) {
					return false;
              	}
				$("#loadingBar").hide();
				// pass the data into given function
				announcementListTable(data);
				$("#searchMode").val("search");
			}
		});
	}
	
	$("#searchAnnouncementBtn").click(function(){
		announcementList();
	});
	
	// count the checkbox selected
	var countCheckBoxSelect = function() {
		var countCheckBox = $(".checkAnnouncement").not(":disabled").length;
		var countCheckBoxChecked = $(".checkAnnouncement:checked").length;
		
		//check if any checkbox is selected display the delete announcement
		if (countCheckBoxChecked > 0){
			$("#deleteBtnDiv").show(); // show file size
		}else{
			$("#deleteBtnDiv").hide(); // hide file size
		}
		
		//code for select all chk if all checkbox is checked
		if (countCheckBox == countCheckBoxChecked) {
			$("#selectAllChk").prop("checked", true);
		} else {
			$("#selectAllChk").prop("checked", false);
		}
		$("#totalUserSelected").html( '<label class="labelText"style="margin-left:10px">'+countCheckBoxChecked + 'Announcement(s) Selected</label>');
	}

	$(document).on("click", ".checkAnnouncement", function() {
		countCheckBoxSelect();
	});

	// function to check all checkbox when select all is clicked
	var selectAllCheck = function(e) {
		var elChked = e.is(":checked");
		
		if(elChked){
			$(".checkAnnouncement").each(function(index){
				var el = $(this);
				var elName = el.attr("name");
				var elInUse = el.attr("data-inuse");
				
				if(elInUse == "Not In Use"){
					el.prop("checked", true);
				}
			});
		}else{
			$(".checkAnnouncement").prop("checked", false);
		}
		countCheckBoxSelect();
	}

	// click event for the select all chechbox
	$("#selectAllChk").click(function() {
		var el = $(this);
		selectAllCheck(el);
	})
	
	var getAnnouncementInfo = function(e){
		
		var name = e.attr("data-name");
		var mediaType = e.attr("data-mediaType");
		var announcementSp = e.attr("data-announcementSp");
		var announcementGroup = e.attr("data-announcementGroup");
		
		$.ajax({
			url : 'Announcement21/ajax/announcementInfo.php',
			type : 'POST',
			data : {name : name, mediaType : mediaType, announcementGroup : announcementGroup, announcementSp : announcementSp},
			success : function(data) {
				hideOnLoad();
				// pass the data into given function
				//console.log(data);
				$( "#announcementListDiv" ).show();
				$( "#announcementListDiv" ).html(data);
			}
		});
		
	}
	
	var loadModifyDialog = function(announcementRow){
		var announcementName = announcementRow.attr("data-name");
		var announcementMediaType = announcementRow.attr("data-mediaType");
		var announcementMediaTypeVal = announcementRow.attr("data-mediatypeval");
		var announcementSp = announcementRow.attr("data-sp");
		var announcementGroup = announcementRow.attr("data-group");
		var announcementExist = announcementRow.attr("data-exist");
		
		$.ajax({
			url : 'Announcement21/ajax/announcementInfo.php',
			type : 'POST',
			data : {announcementName : announcementName, announcementMediaType : announcementMediaType, announcementMediaTypeVal : announcementMediaTypeVal, announcementSp : announcementSp, announcementGroup:announcementGroup, announcementExist: announcementExist},
			success : function(data) {
				clickMode = 'Modify';
				$("#announcementModifiedId").val(name);
				$("#announceAddModal").html(data);
                                $("#announceAddModal").dialog("open");
				// dlg.dialog('open');
			}
		});
	}
	
	var loadModifyView = function(e){
		var name = e.attr("data-name");
		var mediaType = e.attr("data-mediaType");
		var mediaTypeVal = e.attr("data-mediatypeval");
		var announcementSp = e.attr("data-sp");
		var announcementGroup = e.attr("data-group");
		var announcementExist = e.attr("data-exist");
		
//		$( "#announcementListDiv" ).load( "Announcement21/ajax/announcementInfo.php?name="+name+"&mediaType="+mediaType, function(e) {
//			hideOnLoad();
//			$( "#announcementListDiv" ).show();
//		});
		 
		dlg.load("Announcement21/ajax/announcementInfo.php?announcementName="+name+"&announcementMediaType="+mediaType+'&announcementMediaTypeVal='+mediaTypeVal+'&announcementSp='+announcementSp+'&announcementGroup='+announcementGroup+'&announcementExist='+announcementExist, function() {
			dlg.dialog('open');
			clickMode = 'Modify';
			$("#announcementModifiedId").val(name);
			//hideOnLoad();
		});
	}
	
	
	
	$(document).on("click", ".announcementLink", function(event){
		if( (! announcementCheckBoxHovered) && (event.target.className !== "playAnnouncementMedia" && event.target.className !== "downloadMediaFile") ){

		
		//if(event.target.type !== "checkbox" && (event.target.className !== "playAnnouncementMedia" && event.target.className !== "downloadMediaFile") ){

			//$("#loadingBar").show();
			//hideOnLoad();
			//dlg.empty();
			var el = $(this);
			loadModifyDialog(el);
			//loadModifyView(el);
			//getAnnouncementInfo(el);
		}
		
	});
	
	//make delete array
	var makeDeleteArray = function(){
		
		//define the array
		var deleteArray = new Array();
		$(".checkAnnouncement").each(function(index){
			var el = $(this);
			var elChked = el.is(":checked");
			var elName = el.attr("name");
			var elMediaType = el.attr("data-mediaType");
			var elMediaTypeVal = el.attr("data-mediaTypeVal");
			var elInUse = el.attr("data-inuse");
			var elSp = el.attr("data-sp");
			var elGroup = el.attr("data-group");
			var elExist = el.attr("data-exist");
			
			var nameMediaType = elName + "<==>" + elMediaTypeVal + "<==>" + elSp + "<==>" + elGroup + "<==>" + elExist;
			
			if(elChked){
				if(elInUse == "Not In Use"){
					deleteArray.push(nameMediaType);
				}
			} 
		});
		return deleteArray;
	};
	
	//delete announcement function
	var deleteAnnouncement = function(){
		pendingProcess.push("Delete Announcement");
		var deleteAnnouncementArray = makeDeleteArray();
		
		$.ajax({
			type : "POST",
			url : "Announcement21/ajax/deleteAnnouncement.php",
			data : {deleteArray : deleteAnnouncementArray},
			success : function(data) {
				if(foundServerConErrorOnProcess(JSON.stringify(data), "Delete Announcement")) {
					return;
	          	}
				var obj = jQuery.parseJSON(data);
				
				if(obj.Error && obj.Error != ""){
					$("#dialogAN").html('Error: '+obj.Error + '<br/>Please try again.');
					buttonsShowHide('Cancel', 'show');
					$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
					buttonsShowHide('Done', 'hide');
				}else{
					$("#dialogAN").html("Announcement Deleted Successfully");
		        	buttonsShowHide('Done', 'show');
		        	$('.ui-dialog-buttonpane').find('button:contains("Done")').addClass('subButton');
		        	buttonsShowHide('Cancel', 'hide');
				}
				
				buttonsShowHide('Ok', 'hide');
	        	buttonsShowHide('Delete', 'hide');
				buttonsShowHide('Complete', 'hide');
	        	buttonsShowHide('More Changes', 'hide');
	        	buttonsShowHide('Return to Main', 'hide');
	        	buttonsShowHide('Add New Announcement', 'hide');
			}
		});
		
	}
	
	//check announcement attached with autoattendant or not
	var checkAnnouncementInUse = function(){
		$(".checkAnnouncement").each(function(index){
			var el = $(this);
			var elChked = el.is(":checked");
			var elInUse = el.attr("data-inuse");
			
			if(elChked){
				
				if(elInUse == "In Use"){
					inUseGlobal = true;
				}
			} 
		});
	}
	
	
	var moreChangeAction = function(data){
		var name = data.announcementName;
		var mediaType = data.announcementMediaType;
		var mediaTypeVal = data.announcementMediaTypeVal;
		var announcementSp = data.announcementSp;
		var announcementGroup = data.announcementGroup;
		modifiedAnnouncementData = "";
		
		$.ajax({
			type: "POST",
			url: "Announcement21/ajax/announcementInfo.php",
			data:{name:name, mediaType : mediaType, mediaTypeVal:mediaTypeVal, announcementSp:announcementSp, announcementGroup: announcementGroup},
			success: function(result) {
				$("#loadingBar").hide();
				clickMode = 'Modify';
				$("#announcementModifiedId").val(name);
			}
		});
	}
	
	var moreChangesAnnouncement = function(data){
		var name = data.announcementName;
		var mediaType = data.announcementMediaType;
		var mediaTypeVal = data.announcementMediaTypeVal;
		var announcementSp = data.announcementSp;
		var announcementGroup = data.announcementGroup;
		modifiedAnnouncementData = "";
//		$( "#announcementListDiv" ).load( "Announcement21/ajax/announcementInfo.php?name="+name+"&mediaType="+mediaType, function(e) {
//			hideOnLoad();
//			$( "#announcementListDiv" ).show();
//		});
		 
		dlg.load("Announcement21/ajax/announcementInfo.php?announcementName="+name+"&announcementMediaType="+mediaType+'&announcementMediaTypeVal='+mediaTypeVal+'&announcementSp='+announcementSp+'&announcementGroup='+announcementGroup, function() {
			$("#loadingBar").hide();
			dlg.dialog('open');
			clickMode = 'Modify';
			$("#announcementModifiedId").val(name);
			//hideOnLoad();
		});
	}
	
	//click event for delete button
	$("#deleteAnnouncement").click(function(){
		
		checkAnnouncementInUse();
		
		$("#dialogAN").dialog("open");
		$("#dialogAN").dialog("option", "title", "Delete Announcement");
		if(inUseGlobal){
			$("#dialogAN").html("Given announcement is attached with one or more Auto attendant services. Are you sure you want to delete this announcement?");
		}else{
			$("#dialogAN").html("<b>Announcement will be deleted from BroadWorks Group repository and/or Express repository.</b>");
		}
		//$(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled");
		$(":button:contains('Delete')").removeAttr("disabled", "disabled").removeClass("ui-state-disabled ui-state-hover").addClass("delButton");
		
		buttonsShowHide('Complete', 'hide');
    	buttonsShowHide('Cancel', 'show');
    	buttonsShowHide('More Changes', 'hide');
    	buttonsShowHide('Done', 'hide');
    	buttonsShowHide('Ok', 'hide');
    	buttonsShowHide('Delete', 'show');
    	buttonsShowHide('Return to Main', 'hide');
    	buttonsShowHide('Add New Announcement', 'hide');
	});
	
	var buttonsShowHide = function(b,e){
		if(e == "show"){
			$(".ui-dialog-buttonpane button:contains("+b+")").button().show();
		}else{
		 	$(".ui-dialog-buttonpane button:contains("+b+")").button().hide();
		}
	};

	
	$("#dialogAN").dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		buttons: {
			"Complete": function() {
				$(":button:contains('Complete')").attr("disabled", "disabled").removeClass("ui-state-hover").addClass("ui-state-disabled subButton");
				$("#dialogAN").html("<div style=\"text-align:center;\" >Creating Announcement...<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
				$("#announcementForm").submit();
			},
			"Cancel": function() {
				
//				$(this).dialog("close");
//				$("#loading2").hide();
//				searchMode();
				
				if(errorInAnnForm == true) {
					$(this).dialog("close");
					dlg.dialog('open');
					$("#loading2").hide();
				} else {
					$(this).dialog("close");
					$("#loading2").hide();
					//dlg.dialog('open');
					//searchMode();
				}
			},
			"Delete": function() {		
				$(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled");
				$("#dialogAN").html("<div style=\"text-align:center;\"> Deleting the announcement....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"> </div>");
				deleteAnnouncement();
			},
            "More Changes": function() {
            	
            	$(this).dialog("close");
            	/*$("#loadingBar").show();
        		hideOnLoad();
            	moreChangesAnnouncement(modifiedAnnouncementData);*/
            	announcementList();
            },
            "Done": function() {
            	$(this).dialog("close");
            	announcementList();
			},
            "Ok": function() {
            	$(this).dialog("close");
			},
			"Return to Main": function() {
				$(location).attr('href','main.php');
			},
			"Add New Announcement": function() {
				//dlg.empty();
				$(this).dialog("close");
				loadAddView();
			}
		},
        open: function() {
            $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019			
            setDialogDayNightMode($(this));
        	buttonsShowHide('Complete', 'show');
        	buttonsShowHide('Cancel', 'show');
        	buttonsShowHide('More Changes', 'hide');
        	buttonsShowHide('Done', 'hide');
        	buttonsShowHide('Delete', 'hide');
        	buttonsShowHide('Return to Main', 'hide');
        	buttonsShowHide('Add New Announcement', 'hide');
        	
        	/* Button styling.*/
        	$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
        	$(":button:contains('Complete')").addClass('subButton').removeClass("ui-state-hover");
        	$(":button:contains('More Changes')").addClass('cancelButton');
        	$(":button:contains('Return to Main')").addClass('cancelButton');
        	$(".ui-dialog-buttonpane button:contains('Delete')").addClass('deleteButton');
        	
        }
	});	
	
	//var function to handle error dialogue after completion
	var errorDialogueHtml = function(data){
		// Handle errors here
        console.log('ERRORS: ' + data.error);
        
        $("#dialogAN").dialog("open");
		$("#dialogAN").dialog("option", "title", "Response Create Announcement");
		$("#dialogAN").html('ERRORS: ' + data.error + '<br/>Please try again');
		buttonsShowHide('Cancel', 'show');
		buttonsShowHide('Complete', 'hide');
    	buttonsShowHide('More Changes', 'hide');
    	buttonsShowHide('Done', 'hide');
    	buttonsShowHide('Ok', 'hide');
    	buttonsShowHide('Delete', 'hide');
    	buttonsShowHide('Return to Main', 'show');
    	buttonsShowHide('Add New Announcement', 'hide');
	}
	
	//var function to handle error dialogue after completion
	var successDialogueHtml = function(data){
		// Handle errors here
		console.log('Suuccess: ' + data.success);
		
		$("#dialogAN").dialog("open");
		$("#dialogAN").dialog("option", "title", "Response Create Announcement");
		$("#dialogAN").html('Announcement Saved Successfully.');
		buttonsShowHide('Cancel', 'hide');
		buttonsShowHide('Complete', 'hide');
		buttonsShowHide('More Changes', 'show');
		buttonsShowHide('Done', 'hide');
		buttonsShowHide('Ok', 'hide');
		buttonsShowHide('Delete', 'hide');
		buttonsShowHide('Return to Main', 'show');
		buttonsShowHide('Add New Announcement', 'hide');
	}
	
	//code start for upload the file
	
	// Variable to store your files
	var files;

	// Add events
	$('input[type=file]').on('change', prepareUpload);

	// Grab the files and set them to our variable
	function prepareUpload(event)
	{
	  files = event.target.files;
	}
	
	$('form').on('submit', uploadFiles);
	
	// Catch the form submit and upload the files
	function uploadFiles(event){
		
		event.stopPropagation(); // Stop stuff happening
	    event.preventDefault(); // Totally stop stuff happening
	    
	    var announcementName = $("#announcementName").val();
	    var announcementGroup = $("#announcementGroup").val();
	    var announcementUploadVal = $("#announcementUploadVal").val();
	    var announcementId = "";
	    if($("#announcementId")){
	    	var announcementId = $("#announcementId").val();
	    }
	    // START A LOADING SPINNER HERE
	    // Create a formdata object and add the files
	    
	    var fileVal = $("input[type=file]").val();
	    
	    var data = new FormData();
	    data.append("announcementName", announcementName);
	    data.append("announcementId", announcementId);
	    data.append("announcementUploadVal", announcementUploadVal);
	    data.append("announcementGroup", announcementGroup);
	    if(fileVal != ""){
		    $.each(files, function(key, value)
		    {
		        data.append(key, value);
		    });
	    }
	    pendingProcess.push("Save Announcement");
	    $.ajax({
	        url: 'Announcement21/ajax/saveAnnouncement.php?files',
	        type: 'POST',
	        data: data,
	        cache: false,
	        dataType: 'json',
	        processData: false, // Don't process the files
	        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
	        success: function(data, textStatus, jqXHR)
	        {
	        	if(foundServerConErrorOnProcess(data, "Save Announcement")) {
					return false;
              	}
	            if(typeof data.error === 'undefined')
	            {
	                // Success so call function to process the form
	                submitForm(event, data);
	            }
	            else
	            {
	                // Handle errors here
	                console.log('ERRORS: ' + data.error);
	            }
	        },
	        error: function(jqXHR, textStatus, errorThrown)
	        { 
	            // Handle errors here
	            console.log('ERRORS: ' + textStatus);
	            //if()
	            // STOP LOADING SPINNER
	        }
	    });
	}
	
	function submitForm(event, data)
	{
	  // Create a jQuery object from the form
		errorInAnnForm = false;
		$form = $(event.target);
	    	
	    var fileVal = $("input[type=file]").val();
	    
	    // Serialize the form data
	    var formData = $form.serialize();

	    // You should sterilise the file names
	    $.each(data.files, function(key, value)
	    {
	        formData = formData + '&filenames[]=' + value;
	    });

	    pendingProcess.push("Save Announcement");
	    $.ajax({
	        url: 'Announcement21/ajax/saveAnnouncement.php',
	        type: 'POST',
	        data: formData,
	        cache: false,
	        dataType: 'json',
	        success: function(data, textStatus, jqXHR)
	        {
	        	if(foundServerConErrorOnProcess(data, "Save Announcement")) {
					return false;
              	}
	            //if(typeof data.error === 'undefined')
	            if(data.success != '')
	            {
	                // Success so call function to process the form
	                console.log('SUCCESS: ' + data.success);
	                successDialogueHtml(data);
//	                var obj = $.parseJSON(data);
	                modifiedAnnouncementData = data.announcementArray;
	            }
	            else
	            {
	            	errorDialogueHtml(data);
	            }
	        },
	        error: function(jqXHR, textStatus, errorThrown)
	        {
	            // Handle errors here
	            console.log('ERRORS: ' + textStatus);
	            errorDialogueHtml(data);
	        },
	        complete: function(data)
	        {
	            // STOP LOADING SPINNER
	        }
	    });
	}
	
	var announcementCheckBoxHovered = false;
	$(document).on("mouseover", ".annCheckBox", function(){
		announcementCheckBoxHovered = ! announcementCheckBoxHovered;
	});
	$(document).on("mouseout", ".annCheckBox", function(){
		announcementCheckBoxHovered = ! announcementCheckBoxHovered;
	});
	
	//trigger event for uploadFile
	$("#uploadFile").click(function(){ 
		   $("input[type='file']").trigger('click');
		   
		   $('#announcementUpload').change(function() {
		        var filename = $('#announcementUpload').val();
		        $("#announcementUploadVal").val(filename);
		        filename = filename.split('\\');
		        console.log(filename);
		        
		        $("#chkFileUpload").html('');
		        $("#showFileUpload").show();
		        $("#showFileUpload").html('<label class="labelpadding"><span id="attachedFileName"></span></label>');
		        $("#attachedFileName").html(filename[2]);
		   });
	});
	//code ends
	
	var confirmSetting = function(){
		errorInAnnForm = false;
		var dataToSend = $("form#announcementForm").serializeArray();
                //Code added @ 26 April 2019
                var annmntSpTmp = $("#announcementSp").val();
                var actionTmp = "create";
                if(typeof annmntSpTmp !== "undefined"){
                    actionTmp = "modify";
                }
                //alert('actionTmp - '+actionTmp);
                //End code
		$.ajax({
			type : "POST",
			url : "Announcement21/ajax/checkData.php",
			data : dataToSend,
			success : function(result) {
				
				var errorExist = result.search("Error");
				var noDataExist = result.search("No Changes");
				
				dlg.dialog('close');
				$("#dialogAN").dialog("open");
				if(actionTmp == "create"){
                                    $("#dialogAN").dialog("option", "title", "Request Create Announcement");
                                }
                                if(actionTmp == "modify"){
                                    $("#dialogAN").dialog("option", "title", "Request Modify Announcement");
                                }
				$("#dialogAN").html('<table style="width:830px;margin-left:auto;margin-right:auto;" cellspacing="0" cellpadding="5" id="changelist"><tbody><tr><th colspan="2" align="center">Please confirm the settings below and click Complete to process your modifications.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td> = Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td> = Invalid Value</td></tr></tbody></table></td></tr><tr><td colspan="2">&nbsp;</td></tr>');
				$("#changelist").append(result);
				$("#dialogAN").append('</tbody></table>');
				if(errorExist > -1){
					errorInAnnForm = true;
					$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton").removeClass("ui-state-hover");
					buttonsShowHide('Cancel', 'show');
					//buttonsShowHide('Complete', 'hide');
				}else{
					if(noDataExist > -1){
						 
                                                errorInAnnForm = true;
						buttonsShowHide('Cancel', 'show');
						buttonsShowHide('Complete', 'hide');
					}else{
						 
                                                buttonsShowHide('Cancel', 'show');
						buttonsShowHide('Complete', 'show');
						$(":button:contains('Complete')").removeAttr("disabled", "disabled").removeClass("ui-state-disabled ui-state-hover").addClass("subButton");
					}
				}
				
	        	buttonsShowHide('More Changes', 'hide');
	        	buttonsShowHide('Done', 'hide');
	        	buttonsShowHide('Ok', 'hide');
	        	buttonsShowHide('Delete', 'hide');
	        	buttonsShowHide('Return to Main', 'hide');
	        	buttonsShowHide('Add New Announcement', 'hide');
				
			}
		});
	}
	//$(document).on("click", "#subButtonAnn", function() {
//	$("#subButtonAnn").click(function(){
//		var dataToSend = $("form#announcementForm").serializeArray();
//		$.ajax({
//			type : "POST",
//			url : "Announcement21/ajax/checkData.php",
//			data : dataToSend,
//			success : function(result) {
//				
//				var errorExist = result.search("Error");
//				
//				dlg.dialog('close');
//				$("#dialogAN").dialog("open");
//				
//				$("#dialogAN").dialog("option", "title", "Request Create Announcement");
//				$("#dialogAN").html('<table style="width:80%;margin-left:auto;margin-right:auto;" cellspacing="0" cellpadding="5" id="changelist"><tbody><tr><th colspan="2" align="center">Please confirm the settings below and click Complete to process your modifications.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5"><tbody><tr><td style="background:#00AC3E;" width="5%">&nbsp;</td><td> = Value Changed</td></tr><tr><td style="background:#D52B1E;" width="5%">&nbsp;</td><td> = Invalid Value</td></tr></tbody></table></td></tr><tr><td colspan="2">&nbsp;</td></tr>');
//				$("#changelist").append(result);
//				$("#dialogAN").append('</tbody></table>');
//				if(errorExist > -1){
//					$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
//					buttonsShowHide('Cancel', 'show');
//					//buttonsShowHide('Complete', 'hide');
//				}else{
//					buttonsShowHide('Cancel', 'show');
//					buttonsShowHide('Complete', 'show');
//					$(":button:contains('Complete')").removeAttr("disabled", "disabled").removeClass("ui-state-disabled");
//					
//				}
//				
//	        	buttonsShowHide('More Changes', 'hide');
//	        	buttonsShowHide('Done', 'hide');
//	        	buttonsShowHide('Ok', 'hide');
//	        	buttonsShowHide('Delete', 'hide');
//	        	buttonsShowHide('Return to Main', 'hide');
//	        	buttonsShowHide('Add New Announcement', 'hide');
//				
//			}
//		});
//	});
	
	$("#cancelAnnButton").click(function(){
		dlg.dialog('close');
		if(clickMode == "modify"){
			announcementList();
		} 
	});
	 
	
	// Play Announcement
	
	$("#dialogPlayMedia").dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		buttons: {
			 
			"Cancel": function() {
				$("#dialogPlayMedia").find("#audioType audio").each(function () { this.pause() });
				$("#dialogPlayMedia").find("#vediotype video").each(function () { this.pause() });
				$("#dialogPlayMedia").dialog("close");
			}
			 
		},
        open: function() {
        	$("#dialogPlayMedia").show();
        	buttonsShowHide('Cancel', 'show');
        	setDialogDayNightMode($(this));
        	/* Button styling.*/
        	$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
        },
        close: function() {
        	/* Stop media player */
        	$("#dialogPlayMedia").find("#audioType audio").each(function () { this.pause() });
			$("#dialogPlayMedia").find("#vediotype video").each(function () { this.pause() });
        }
	});	
	
	$(document).on("click", ".playAnnouncementMedia", function() {
		
		$("#dialogPlayMedia").find("#mediaNotSupported").hide();
		$("#vediotype").hide();
		$("#audioType").hide();
		$("#embedType").hide();
		var mediaType = $(this).attr('data-mediatypeval');
		var mediaUrl = window.location.origin + '/' + $(this).attr('data-src') + "." + mediaType.toLowerCase();
		var supportedArray = ["WMA", "MOV", "WAV"];
		$("#audioType embed").remove();
		
		if(jQuery.inArray(mediaType, supportedArray) === -1) {
			$("#dialogPlayMedia").find("#mediaNotSupported").show();
		}
		else if(mediaType == "WAV" || mediaType == "WMA")  {
//			Audio
			$("#dialogPlayMedia").find("#audioType audio source").attr('src', mediaUrl);
			$("#dialogPlayMedia").find("#audioType audio").load();
			//$("#dialogPlayMedia > #audioType audio").load();
			$("#vediotype").hide();
			$("#audioType").show();
			
			var userAgentVal = $("#userAgent").val();
			if(userAgentVal.search("Trident/7.0") > -1){
				var embedUrl = '<embed src="'+ mediaUrl+'" id="embedType" autoplay="false" autostart="false">';
				$("#audioType").append(embedUrl);
				$("#audioType audio").hide();
			}
			
		} else {
//			vedio
			$("#dialogPlayMedia").find("#vediotype video source").attr('src', mediaUrl);
			$("#dialogPlayMedia").find("#vediotype video")[0].load();
			$("#audioType").hide();
			$("#vediotype").show();
		}
		
		$("#dialogPlayMedia").dialog("open");
	});
});
//$(document).ready(function(){
//	$('[data-toggle="tooltip"]').tooltip();   
//});



/* event for checkbox click show delete button
 * development only for new ui validation
 * delete  checkbox event  
 *  if checkbox not checked all row event fire validation or redirection 
 *  
 * */


