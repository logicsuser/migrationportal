$(function() {

	var files;
        var actionRes = "";

	// Add events
	$('input[type=file]').on('change', prepareUpload);

	// Grab the files and set them to our variable
	function prepareUpload(event) {
		files = event.target.files;
	}

	$('form').on('submit', uploadFiles);

	// Catch the form submit and upload the files
	function uploadFiles(event) {
                
                actionRes = "create";
		event.stopPropagation(); // Stop stuff happening
		event.preventDefault(); // Totally stop stuff happening
		var annmntSp         = $("#announcementSp").val();  //Code added @ 24 April 2019
		var announcementName = $("#announcementName").val();
		var announcementGroup = $("#announcementGroup").val();
		var announcementUploadVal = $("#announcementUploadVal").val();
		var announcementId = "";
		if ($("#announcementId")) {
			var announcementId = $("#announcementId").val();
		}
		// START A LOADING SPINNER HERE
		// Create a formdata object and add the files

		var fileVal = $("input[type=file]").val();

		var data = new FormData();
		if(typeof annmntSp !== "undefined"){
                        actionRes = "modify";
			data.append("announcementSp", annmntSp);
		}
		data.append("announcementName", announcementName);
		data.append("announcementId", announcementId);
		data.append("announcementUploadVal", announcementUploadVal);
		data.append("announcementGroup", announcementGroup);
		if (fileVal != "") {
			$.each(files, function(key, value) {
				data.append(key, value);
			});
		}

		$.ajax({
			url : 'Announcement21/ajax/saveAnnouncement.php?files',
			type : 'POST',
			data : data,
			cache : false,
			dataType : 'json',
			processData : false, // Don't process the files
			contentType : false, // Set content type to false as jQuery will
									// tell the server its a query string
									// request
			success : function(data, textStatus, jqXHR) {
				if (typeof data.error === 'undefined') {
					
					// Success so call function to process the form
					submitForm(event, data);
				} else {
					
					// Handle errors here
					console.log('ERRORS: ' + data.error);
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				// Handle errors here
				console.log('ERRORS: ' + textStatus);
				// if()
				// STOP LOADING SPINNER
			}
		});
	}

	
	// trigger event for uploadFile
//	$("#uploadFile")
//			.click(
//					function() {
	$("#uploadFile").on('click change input live', function(){
		$("input[type='file']").trigger('click');

	});

	$('#announcementUpload').on('input click change live', function() {
	
		var filename = $('#announcementUpload').val();
		$("#announcementUploadVal").val(filename);
		fname = filename.split('\\');
		filename = fname[fname.length-1];
		console.log(fname[fname.length-1]);
		
	
		$("#chkFileUpload").html('');
		$("#showFileUpload").show();
		$("#showFileUpload").html('<label class="labelpadding"><span id="attachedFileName"></span></label>');
	//					$("#attachedFileName").html(filename[2]);
		$("#attachedFileName").html(filename);
	});
	
	function submitForm(event, data) {
		// Create a jQuery object from the form
		$form = $(event.target);

		var fileVal = $("input[type=file]").val();

		// Serialize the form data
		var formData = $form.serialize();

		// You should sterilise the file names
		$.each(data.files, function(key, value) {
			formData = formData + '&filenames[]=' + value;
		});

		$.ajax({
			url : 'Announcement21/ajax/saveAnnouncement.php',
			type : 'POST',
			data : formData,
			cache : false,
			dataType : 'json',
			success : function(data, textStatus, jqXHR) {
				// if(typeof data.error === 'undefined')
				if (data.success != '') {
					
					// Success so call function to process the form
					console.log('SUCCESS: ' + data.success);
					successDialogueHtml(data);
					// var obj = $.parseJSON(data);
					modifiedAnnouncementData = data.announcementArray;
				} else {
					
					errorDialogueHtml(data);
				}
			},
			error : function(jqXHR, textStatus, errorThrown) {
				
				// Handle errors here
				console.log('ERRORS: ' + textStatus);
				errorDialogueHtml(data);
			},
			complete : function(data) {
				// STOP LOADING SPINNER
			}
		});
	}

	var successDialogueHtml = function(data) {
		// Handle errors here
		console.log('Suuccess: ' + data.success);

		$("#dialogAN").dialog("open");
		//$("#dialogAN").dialog("option", "title", "Response Create Announcement");                        
                if(actionRes == "create"){
                    $("#dialogAN").dialog("option", "title", "Response Create Announcement");
                }
                if(actionRes == "modify"){
                    $("#dialogAN").dialog("option", "title", "Response Modify Announcement");
                }     
                        
		$("#dialogAN").html('Announcement Saved Successfully.');
		buttonsShowHide('Cancel', 'hide');
		buttonsShowHide('Complete', 'hide');
		buttonsShowHide('More Changes', 'show');
		buttonsShowHide('Done', 'hide');
		buttonsShowHide('Ok', 'hide');
		buttonsShowHide('Delete', 'hide');
		buttonsShowHide('Return to Main', 'show');
		buttonsShowHide('Add New Announcement', 'hide');
	}

	
	var buttonsShowHide = function(b,e){
		if(e == "show"){
			$(".ui-dialog-buttonpane button:contains("+b+")").button().show();
		}else{
		 	$(".ui-dialog-buttonpane button:contains("+b+")").button().hide();
		}
	};
	
	var errorDialogueHtml = function(data){
		// Handle errors here
        console.log('ERRORS: ' + data.error);
        
        $("#dialogAN").dialog("open");
		//$("#dialogAN").dialog("option", "title", "Response Create Announcement");
                if(actionRes == "create"){
		$("#dialogAN").dialog("option", "title", "Response Create Announcement");
                }
                if(actionRes == "modify"){
                        $("#dialogAN").dialog("option", "title", "Response Modify Announcement");
                }
                
                
		$("#dialogAN").html('ERRORS: ' + data.error + '<br/>Please try again');
		buttonsShowHide('Cancel', 'show');
		buttonsShowHide('Complete', 'hide');
    	buttonsShowHide('More Changes', 'hide');
    	buttonsShowHide('Done', 'hide');
    	buttonsShowHide('Ok', 'hide');
    	buttonsShowHide('Delete', 'hide');
    	buttonsShowHide('Return to Main', 'show');
    	buttonsShowHide('Add New Announcement', 'hide');
	}
	// code ends

});