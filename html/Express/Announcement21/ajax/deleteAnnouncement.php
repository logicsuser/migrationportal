<?php
require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/Announcement21/Announcement21.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
server_fail_over_debuggin_testing(); /* for fail Over testing. */

$announcement = new Announcement21 ();
$announcementArray = array ();
foreach ( $_POST ["deleteArray"] as $key => $val ) {
	$explodeName = explode ( "<==>", $val );
	$announcementArray [$key] ["announcementFileName"] = $explodeName [0];
	$announcementArray [$key] ["announcementFileType"] = $explodeName [1];
	$announcementArray [$key] ["announcementSp"] = $explodeName [2];
	$announcementArray [$key] ["announcementGroupId"] = $explodeName [3];
	$announcementArray [$key] ["announcementExist"] = $explodeName [4];
}
// delete announcement function

$deleteAnnouncement = $announcement->deleteAnnouncement ( $announcementArray);
if(empty($deleteAnnouncement['Error'])){
    foreach($announcementArray as $keyDeleted => $valDeleted) {
        addToChangeLog($valDeleted);
    }
}

// check if no error present in fetching data we will get into success array and print the data
echo json_encode($deleteAnnouncement);

function addToChangeLog($annData) {
    //if(empty($deleteAnnouncement['Error'])){
    $groupId = $_SESSION["groupId"];
    if($groupId==""){
        $groupId = $annData['announcementGroupId'];
    }
    $changeLogObj = new ChangeLogUtility($annData['announcementFileName'], $groupId, $_SESSION["loggedInUserName"]);
        $module = "Delete Announcement";
        $tableName = "announcementModChanges";
        $entity = $annData["announcementFileName"];
        $changeResponse = $changeLogObj->changeLogDeleteUtility($module, $entity, $tableName);
   // }
    /*
     * admin Log show delete announcement details
     * ex- 765
     */
    if(isset($_SESSION["adminId"])) {
        require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
        $adminLogs = new adminLogs();
       // $resultAnnTypeName = $announcement->getAnnouncementTypeInfo($db,$annData["announcementType"]);
        $log = array(
            'adminUserID' =>$_SESSION["adminId"],
            'eventType' => 'DELETE_ANNOUNCEMENT',
            'adminUserName' => $_SESSION["loggedInUserName"],
            'adminName' => $_SESSION["loggedInUser"],
            'updatedBy' => $_SESSION["adminId"],
            'details' => array(
                'Group Name' => $annData["announcementGroupId"],
                'Announcement Name' => $annData["announcementFileName"],
            )
        );
        $adminLogs->addLogEntry($log);
    }
}
?>