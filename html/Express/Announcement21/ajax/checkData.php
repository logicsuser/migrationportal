<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/Announcement21/Announcement21.php");
// print_r($_POST);

$announcement = new Announcement21 ();

//$announcement->sp = $_SESSION["sp"];
//Code added @ 16 April 2019
if(isset($_POST['announcementSp']) && $_POST['announcementSp'] != ""){
    $announcement->sp = $_POST['announcementSp'];
}else{
    $announcement->sp = $_SESSION["sp"];
}
//End code
if(!empty($_SESSION["groupId"])){
	$groupId = $_SESSION["groupId"];
}else{
	$groupId = $_POST["announcementGroup"];
}

$ext = pathinfo($_POST["announcementUploadVal"], PATHINFO_EXTENSION);
$announcement->groupId = $groupId;
$announcement->announcementFileType = "Audio";
$announcement->announcementFileName = $_POST["announcementName"];
$announcement->includeAnnouncementTable = "true";
$announcement->announcementMode = "Equal To";

//$announcement->checkCriteria = false;
$announcementList = $announcement->getAnnouncementList();

$changeString = "";
if($_POST['announcementUploadVal'] == "temp/"){
	$_POST['announcementUploadVal'] = "";
}
$find = "C:\\fakepath\\";
$replace = "";
$_POST['announcementUploadVal'] = str_replace($find, $replace, $_POST['announcementUploadVal']);

$postArray = array();
$postArray['announcementId'] = isset($_POST['announcementId']) ? $_POST['announcementId'] : "";
$postArray['announcementGroup'] = isset($_POST['announcementGroup']) ? $_POST['announcementGroup'] : "";
$postArray['announcementMediaType'] = isset($_POST['announcementMediaType']) ? $_POST['announcementMediaType'] : "";
$postArray['announcementName'] = isset($_POST['announcementName']) ? $_POST['announcementName'] : "";
$postArray['announcementUploadVal'] = isset($_POST['announcementUploadVal']) ? $_POST['announcementUploadVal'] : "";

$announcement->fileExt = strtoupper($ext);
if(isset($_POST['announcementId']) && !empty($_POST['announcementId'])){ 
	$changeString .= $announcement->checkModifySaveData($postArray, $announcementList);
}else{ 
	$changeString .= $announcement->checkAddSaveData($postArray, $announcementList);
}

echo $changeString;
?>