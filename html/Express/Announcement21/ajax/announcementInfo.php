<?php
require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();

//print_r($_POST); print_r($_GET);die;

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/Announcement21/Announcement21.php");
//$serP = $_SESSION["sp"];
if(isset($_POST['announcementSp']) && $_POST['announcementSp'] != ""){
    $serP = $_POST['announcementSp'];
}else{
    $serP = $_SESSION["sp"];
}

require_once ("/var/www/lib/broadsoft/adminPortal/getAllGroups.php");

unset($_SESSION["addAnnouncement"]);
$announcement = new Announcement21 ();

// $announcement->includeAnnouncementTable = "true";
// $announcement->announcementMode = "Equal To";

// $announcement->announcementFileType = $_GET["mediaType"];
// $announcement->announcementFileName = $_GET["name"];
// $sp = $announcement->announcementSp = $_GET["announcementSp"];
// $groupId = $announcement->announcementGroup = $_GET["announcementGroup"];
// $announcement->sp = $sp;
// $announcement->groupId = $groupId;

// $announcementArray["name"] = $_GET["name"];
// $announcementArray["mediaType"] = $_GET["mediaType"];
// //check if no error present in fetching data we will get into success array and print the data
// $announcementInfo = $announcement->getAnnouncementInfo($announcementArray);
// //print_r($announcementInfo);
// //echo $announcementHtml;

// if(empty($announcementInfo["Error"])){
// 	$announcementName = $announcementInfo["Success"]["row"][0]["announcementName"];
// 	$announcementMediaType = $announcementInfo["Success"]["row"][0]["announcementMediaType"];
// 	$announcementDescription = $announcementInfo["Success"]["row"][0]["announcementDescription"];
// 	$announcementSp = $announcementInfo["Success"]["row"][0]["announcementSp"];
// 	$announcementGroup = $announcementInfo["Success"]["row"][0]["announcementGroup"];
// }
$_SESSION["addAnnouncement"]["announcementName"] = $announcementName = $_POST["announcementName"];
$announcementMediaType = $announcement->getMediaTypeVal($_POST["announcementMediaType"]);
$_SESSION["addAnnouncement"]["announcementMediaType"] = $announcementMediaType = $_POST["announcementMediaType"];
$_SESSION["addAnnouncement"]["announcementMediaTypeVal"] = $announcementMediaTypeVal = strtolower($_POST["announcementMediaTypeVal"]);
$_SESSION["addAnnouncement"]["announcementSp"] = $announcementSp = $_POST["announcementSp"];
$_SESSION["addAnnouncement"]["announcementGroup"] = $announcementGroup  = $_POST["announcementGroup"];
$_SESSION["addAnnouncement"]["announcementDescription"] = $announcementDescription = $_POST["announcementName"].".".$announcementMediaTypeVal;
$_SESSION["addAnnouncement"]["announcementUploadVal"] = $_POST["announcementName"].".".$announcementMediaTypeVal;
$announcementExist = $_POST["announcementExist"];
?>
<script type="text/javascript" src="Announcement21/js/upload.js"></script>
<style>
input[type='file'] {
  opacity:0    
}
</style>
<div class="adminUserText" style="padding-top:0;">Modify Announcement</div>
<div class="vertSpacer">&nbsp;</div>
<form method="POST" class="fcorn-registerTwo" id="announcementForm" name="announcementForm">
		<input type="hidden" name="announcementSp" id="announcementSp" value="<?php echo $serP; ?>" />
		<?php $displayGroups = ! isset($_SESSION["groupId"]) ? "block" : "none"; ?>
		<?php $disabledGroups = ! isset($_SESSION["groupId"]) ? "style='pointer-events:none;width: 100% !important; background-color:#ddd !important'": "style='pointer-events:auto;width: 100% !important;'"; ?>
        <div class="" style="text-align: center ;display: <?php echo $displayGroups; ?>;">
        	<div class="row">
        <!--             	<div class="col-md-3"></div> -->
            		<div class="col-md-12">
             			<label for="announcementGroup" class="labelText">Select Group:</label><span class="required">*</span><br/>
             			<div class="form-group">
             				 <div class="">
             				 	<select name="announcementGroup" id="announcementGroup" <?php echo $disabledGroups;?>>
                    				<option value="">Select</option>
                    				<?php 
                        				$groupHtml = "";
                        				foreach ($allGroups as $groupName){
                        				    $groupHtml .= "<option value='".$groupName."'";
                        				    if($announcementGroup == $groupName){
                        				    	$groupHtml .= "selected";
                        				    }
                        				    $groupHtml .= " >".$groupName."</option>";
                        				}
                        				echo $groupHtml;
                    				?>
        						</select>
             				 </div>
             			</div>
             	 	</div>
             	 </div>
          </div>

			<div class="row">
         	 	<div class="col-md-6">
         			<label for="announcementMediaType" class="labelText">Announcement Type:</label><span class="required">*</span><br/>
         			<div class="form-group">
         				 <div class="dropdown-wrap">
         				 	<select name="announcementMediaType" id="announcementMediaType">
                				<option value="">Select</option>
            <!-- 				<option value="WAV">WAV</option> -->
            <!-- 				<option value="3GP">3GP</option> -->
            <!-- 				<option value="MOV">MOV</option> -->
            <!-- 				<option value="WMA">WMA</option> -->
            					<option value="Audio" <?php if($announcementMediaType == "Audio"){echo "selected";}?>>Audio</option>	
								<option value="Video" <?php if($announcementMediaType == "Video"){echo "selected";}?>>Video</option>
    						</select>
         				 </div>
         			</div>
         		</div>
         		
         		<div class="col-md-6">
         			<label for="announcementName" class="labelText">Announcement Name:</label><span class="required">*</span><br/>
         			<div class="form-group">
         				 <input type="text" name="announcementName" id="announcementName"
            				size="35" maxlength="30" value="<?php echo $announcementName;?>">
            			<input type="hidden" name="announcementId" id="announcementId"
            				 value="<?php echo $announcementName;?>">
         			</div>
         		</div>
     		</div>

			<div class="row">
         		<div class="col-md-3">
         			<label for="announcementFile" class="labelText">Announcement File:</label><span class="required">*</span><br/>
         			<div class="form-group">
         				<input type="button" class="addAnnouncement" id="uploadFile" value="UploadFile">
            			<input type="file" name="announcementUpload" id="announcementUpload" />
            			<input type="hidden" name="announcementUploadVal" id="announcementUploadVal" value="<?php echo $announcementDescription;?>" />
			
						<div class="col span_2" id="chkFileUpload"></div>
						<div class="col span_2" id="showFileUpload"></div>
						<?php if(!empty($announcementDescription)){?>
                			<label class="labelpadding"><span id="attachedFileName"><?php echo $announcementDescription;?></span></label>
                		<?php }?>
         			</div>
         		</div>
         	</div>
    
		<input type="hidden" name="announcementMediaTypeVal" id="announcementMediaTypeVal" value="<?php echo $announcementMediaTypeVal;?>" />

<!-- 	<div class="row formspace"> -->
<!--        <div class="centerDesc" style="text-align: right;"> -->
<!-- 			<p class="register-submit"> -->
<!-- 				<input type="button" id="cancelAnnButton" value="Cancel"> <input -->
<!-- 					type="button" id="subButtonAnn" value="Confirm Settings"> -->
<!-- 			</p> -->
<!-- 		</div> -->
<!-- 	</div> -->
		
		<input type="hidden" name="announcementExist" value ="<?php echo $announcementExist; ?>">
</form>