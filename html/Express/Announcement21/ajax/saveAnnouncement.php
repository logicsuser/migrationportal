<?php
require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/Announcement21/Announcement21.php");
require_once ("/var/www/lib/broadsoft/adminPortal/Announcement21/AnnouncementChangeLog21.php");

$announcement = new Announcement21 ();
$sp = "";
$groupId = "";

//$sp = $_SESSION["sp"];
if(isset($_POST['announcementSp']) && $_POST['announcementSp'] != ""){
    $sp = $_POST['announcementSp'];
}else{
    $sp = $_SESSION["sp"];
}
if(!empty($_SESSION["groupId"])){
    $groupId = $_SESSION["groupId"];
}else{
    $groupId = $_POST["announcementGroup"];
}

    if (!file_exists('/var/www/html/announcementUpload/'.$sp."/")) {
        mkdir('/var/www/html/announcementUpload/'.$sp."/");
    }
    if (!file_exists('/var/www/html/announcementUpload/'.$sp."/".$groupId."/")) {
        mkdir('/var/www/html/announcementUpload/'.$sp."/".$groupId."/");
    }
    $uploaddir = "/var/www/html/announcementUpload/".$sp."/".$groupId."/"; 

$announcement->sp = $sp;
$announcement->groupId = $groupId;
// You need to add server side validation and better error handling here
// unset($_SESSION["fileUploaded"]);
server_fail_over_debuggin_testing(); /* for fail Over testing. */

$data = array ();
if (isset ( $_GET ['files'] )) {
	if (! empty ( $_FILES [0] ['name'] )) {
		
		$announcementName = $_POST["announcementName"];
		$uploadAnnouncement = $announcement->uploadAnnouncement ( $_FILES [0], $uploaddir, $announcementName);
		echo $uploadAnnouncement;
		$_SESSION["fileUploaded"] = "Yes";
	} else {
	    $_SESSION["fileUploaded"] = "No";
		if(!empty($_POST["announcementId"])){
			$ext = pathinfo($_POST["announcementUploadVal"], PATHINFO_EXTENSION);
			$filenames[] = $_POST["announcementName"].".".$ext;
			$error = false;
			$data = ($error) ? array (
					'error' => 'File is not available' 
			) : array (
					'files' => $filenames 
			);
			//print_r($data);
			
		}else {
			
			$error = true;
			$data = ($error) ? array (
					'error' => 'File is not available'
			) : array (
					'files' => $filenames
			);
		}//die;
		echo json_encode ( $data );
	}
}else{
	
// 	print_r($_POST);	print_r($_GET);	print_r($_FILES);die;
// 	$filename = $_POST["filenames"][0];
	$ext = pathinfo($_POST["filenames"][0], PATHINFO_EXTENSION);
	$filename = $_POST["announcementName"];
	$postArray["announcementId"] = isset($_POST["announcementId"]) ? $_POST["announcementId"] : "";
	$postArray["announcementName"] = $_POST["announcementName"];
	$postArray["announcementUploadVal"] = $_POST["announcementUploadVal"];
	$postArray["announcementMediaType"] = $ext;
	$postArray["announcementFileName"] = $filename; 
	$postArray["announcementDescription"] = $filename.".".$_POST["announcementMediaTypeVal"]; 
	//$postArray["announcementContent"] = $uploaddir.$filename.".".$ext;
	//$postArray["announcementContent"] = $uploaddir.$_POST["announcementUploadVal"];
	$postArray["announcementContent"] = $uploaddir.$_POST["filenames"][0];
	//$postArray["fileUploaded"] = isset($_POST["filenames"][0]) ? "Yes" :"No";
	
	$_POST["announcementType"] = $_POST["announcementMediaType"];
	if(!empty($postArray["announcementId"])){
		if($_POST["announcementName"] <> $_SESSION["addAnnouncement"]["announcementName"]){
			$postArray["newAnnouncementFileName"] = $_POST["announcementName"];
			$postArray["announcementName"] = $_SESSION["addAnnouncement"]["announcementName"];
			$postArray["fileChanged"] = "yes";
			$postArray["announcementContent"] = $uploaddir.$_POST["filenames"][0];
			
//			$oldPathFile = $uploaddir.$postArray["announcementUploadVal"];
//			$newPathFile = $uploaddir.$_POST["filenames"][0];
//			$rename = rename($oldPathFile, $newPathFile);
		}else{
			$postArray["newAnnouncementFileName"] = "";
		}
		
		if($_POST["announcementUploadVal"] <> $_SESSION["addAnnouncement"]["announcementUploadVal"]){
			$postArray["fileChanged"] = "yes";
		}
		
		
		if($_POST["announcementExist"] == "No") { //file exists in express repo only
		   // $uploaddir = $uploaddir. $postArray["announcementUploadVal"];
		    if($_SESSION["fileUploaded"] == "No"){
		        $postArray["announcementContent"] = $uploaddir.$_POST["announcementUploadVal"];
		    }
		   // print_r($uploaddir);exit;
		    $saveAnnouncement = $announcement->saveAnnouncement($postArray, $uploaddir);
		    if(empty($saveAnnouncement["Error"])) {
		        renameInLocalRepo($uploaddir, $postArray["announcementUploadVal"], $_POST["filenames"][0] );
		    }
		} else if($_POST["announcementExist"] == "Yes") { //file exists in broadsoft only
		    //if($postArray["fileUploaded"] == "No") {
    		    $postArray["fileUploaded"] = $_SESSION["fileUploaded"];
    		    unset($_SESSION["fileUploaded"]);
		        $saveAnnouncement = $announcement->saveModifyAnnouncement($postArray, $uploaddir);
		        if(empty($saveAnnouncement["Error"])) {
		            renameInLocalRepo($uploaddir, $postArray["announcementUploadVal"], $_POST["filenames"][0] );
		        }
		} else { //file exists in express repo and broadsoft both, modify announcement
		    $saveAnnouncement = $announcement->saveModifyAnnouncement($postArray, $uploaddir);
		    if(empty($saveAnnouncement["Error"])) {
		        renameInLocalRepo($uploaddir, $postArray["announcementUploadVal"], $_POST["filenames"][0] );
		    }
		}
		
	}else { // add announcement
		$saveAnnouncement = $announcement->saveAnnouncement($postArray, $uploaddir);
		if(!empty($saveAnnouncement["Error"])) { //if error then delete uploaded file
		    unlink($uploaddir.$postArray["announcementName"].".".$postArray["announcementMediaType"] );
		}
	}	
	
	if(empty($saveAnnouncement["Error"])){
	    addToChangeLog($_POST, $groupId, $sp, $announcement);
	    $modifiedArray = array(
	    		"announcementName" => $_POST["announcementName"], 	
	    		"announcementMediaType" => $_POST["announcementMediaType"], 	
	    		"announcementMediaTypeVal" => $postArray["announcementMediaType"], 	
	    		"announcementSp" => $sp, 	
	    		"announcementGroup" => $groupId, 	
	    );
	    $data = array('error' => '', 'success' => "Success", 'announcementArray' => $modifiedArray);
	}else{
		$data = array('error' => $saveAnnouncement["Error"], 'success' => "");
	}
	
	/* Start Change Log*/
	
	
	/* End Change Log*/
	
	echo json_encode ( $data );
}


function renameInLocalRepo($uploaddir, $oldName, $newName) {
    $oldPathFile = $uploaddir.$oldName;
    $newPathFile = $uploaddir.$newName;
    $rename = rename($oldPathFile, $newPathFile);
}

function addToChangeLog($post, $groupId, $sp, $announcement) {
    $changeLog = array();
    $logArray = array();
    
    if(! isset($post["announcementId"])){
    	$logArray = $announcement->makeAddArray($post);
        $logSessionArray = array(
            "sp" => $sp,
            "annGroupName" => $groupId
        );
        $changeLog = array_merge($logArray, $logSessionArray);
        $serviceId = "";
    }else{
    	$serviceId = $post["announcementId"];
    }
     
    $annChangeLog = new AnnouncementChangeLog21($serviceId, $post, $changeLog, $groupId);
    $annChangeLog->changeLogUtil();
}
?>