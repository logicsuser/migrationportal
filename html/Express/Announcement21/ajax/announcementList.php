<?php
require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/Announcement21/Announcement21.php");

$announcement = new Announcement21 ();

//print_R($_POST);die;

if(isset($_POST["searchCriteria"]) && !empty($_POST["searchCriteria"])){
	if($_POST["searchCriteria"] == "announcementName"){
		$announcement->announcementFileType = "";
	}else{
		$announcement->announcementFileType = $_POST["searchCriteria"];
	}
	
}else{
	$announcement->announcementFileType = "";
}

$announcement->announcementFileName = $_POST["searchAnnouncement"];
$announcement->includeAnnouncementTable = "true";
$announcement->announcementMode = "Contains";

$searchArray["searchCriteria"] = $_POST["searchCriteria"];
$searchArray["searchLevel"] = $_POST["searchLevel"];
$searchArray["searchAnnouncement"] = $_POST["searchAnnouncement"];

if(isset($_POST["searchLevel"]) && !empty($_POST["searchLevel"])){
	if($_POST["searchLevel"] == "enterprise"){
		$sp = $_SESSION["sp"];
		
		$allGroupsArray = $announcement->getAnnouncementSpGroupList($sp);
		
		$r = 0;
		$announcementListEnterPrise = array();
		$announcement->sp = $sp;
		
		foreach($allGroupsArray as $key1=>$val1){
			$announcement->groupId = $val1;
			
			$announcement->checkCriteria = false;
			$announcementListA = $announcement->getAnnouncementList();
			$announcementListEnterPrise[$r] = $announcementListA;
			$r++;
		}
		
		/* Get media file from local repository. */
		$listOfLocalFiles = $announcement->getListRepository($sp, $allGroupsArray);
		//compare the array and make it as compatible as the table format
		$finalTableArrayEnterprise = $announcement->compareAnnouncementRepository($announcementListEnterPrise, $listOfLocalFiles, $searchArray);
		//$announcementHtml = $announcement->showAnnouncementEnterpriseListTable($announcementListEnterPrise); old fucntion used before get the files from folder
		$announcementHtml = $announcement->showAnnouncementEnterpriseListTable($finalTableArrayEnterprise);
	}else{
	    $announcement->searchLevel = "System";
		$announcementListEnterPrise = array();
		$spList = $announcement->getAnnouncementServerProviderList();
		$r = 0;
		
		$allGroupsArray = array();
		$listOfLocalFiles = array();
		foreach($spList as $key2=>$val2){
			
			$s = 0;
			$announcement->sp = $serP = $sp = $val2;
			$allGroupsArray = $announcement->getAnnouncementSpGroupList($val2);
			/* Get media file from local repository. */
			$listOfLocalFilesTemp = $announcement->getListRepository($sp, $allGroupsArray);
			$listOfLocalFiles[] = $listOfLocalFilesTemp;
			foreach($allGroupsArray as $key3=>$val3){
				$announcement->groupId = $val3;
				$announcement->checkCriteria = false;
				$announcementListA = $announcement->getAnnouncementList();
				$announcementListEnterPrise[$r] = $announcementListA;
				$r++;
			}
			$s++;
		}
		
		$announcementListEnterPrise = $announcement->compareAnnouncementRepository($announcementListEnterPrise, $listOfLocalFiles, $searchArray);
		$announcementHtml = $announcement->showAnnouncementEnterpriseListTable($announcementListEnterPrise);
	}
	
}else{
//     $announcement->searchLevel = "System";
	$sp = $_SESSION["sp"];
	$announcement->sp = $sp;
	$announcement->groupId = $_SESSION["groupId"];
	$allGroupsArray = array($_SESSION["groupId"]);
	$announcement->checkCriteria = false;
	$announcementList = $announcement->getAnnouncementList();
	/* Get media file from local repository. */
	$listOfLocalFiles = $announcement->getListRepository($sp, $allGroupsArray);
	$announcementList = $announcement->compareAnnouncementRepository($announcementList, $listOfLocalFiles, $searchArray);
	
	//check if no error present in fetching data we will get into success array and print the data
	$announcementHtml = $announcement->showAnnouncementListTable($announcementList[0]);

}
echo $announcementHtml;
?>