<script type="text/javascript" src="AutoAttendantGroup/js/aaJsLib.js"></script>
<div class="subBanner">Auto Attendant</div>
<div class="vertSpacer">&nbsp;</div>

<div class="formPadding">
		<form method="POST" id="hgMod">
			<div class="fcorn-register container" style="width:30%;margin-left:auto;margin-right:auto;" id="modifyAttendant">
				<label for="AAChoice">Auto Attendant</label>
				<span class="spacer">&nbsp;</span>
				<div class="dropdown-wrap">
					<select name="AAChoice" id="AAChoice"></select>
				</div>
				<div style="width: 21%; float: center">
					<p class="register-submit" style="margin-top: 64; margin-left: 35px">
						<input name = "newAttendant" value = "Add New Auto Attendant" id = "newAttendant" type = "button" class = "email">
					</p>
				</div>
			</div>
		</form>
		<div id="addAutoAttendant">
		<form action="" method="POST" id="autoAttendantadd">
			
				<div id="tabs">
				<ul>
					<li><a href="#basic_info" class="tabs" id="basicInfo">Basic Info</a></li>
					<li><a href="#business_hours_menu" class="tabs" id="businessHoursMenu">Business Hours Menu</a></li>
					<li><a href="#after_hours_menu" class="tabs" id="afterHoursMenu">After Hours Menu</a></li>
					<li><a href="#holiday_menu" class="tabs" id="holidayMenu">Holiday Menu</a></li>
					<li><a href="#services_1" class="tabs" id="services">Services</a></li>
				</ul>
				<div id="basic_info" class="userDataClass" style="display: none;">
					<div class="subBanner" style="margin-bottom: 25px;">Basic Info</div>
				 	<div style="clear:right"></div>
					<div class="row formspace fcorn-register container">
					<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="aaType">Select Attendant Type:</label>
					</div>
					<div class="dropdown-wrap col span_13">
						<select name="aaType" id="aaType">
							<option value="">--Select Attendant Type--</option>
							<option value="1">Basic</option>
							<option value="2">Standard</option>
						</select>
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="aaId">Auto Attendant Id:</label>
					</div>
					<div class="col span_6">
						<input type="text" name="aaId" id="aaId" size="35" maxlength="30" />
					</div>
					<div class="col span_1" style="text-align: center;padding-top:8px;">
						@
					</div>
					<div class="col span_6 dropdown-wrap">
						<select id="domain" name="domain">
							<option>expresslab.averistar.com</option>
						</select>
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="name">Name:</label>
					</div>
					<div class="col span_13">
							<input type="text" name="name" id="name" size="35" maxlength="30" />
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="cLIDLName">Calling Line Id Last Name:</label>
					</div>
					<div class="col span_13">
							<input type="text" name="cLIDLName" id="cLIDLName" size="35" maxlength="30" />
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="cLIDFName">Calling Line Id First Name:</label>
					</div>
					<div class="col span_13">
						<input type="text" name="cLIDFName" id="cLIDFName" size="35" maxlength="30" />
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="aaType">Department:</label>
					</div>
					<div class="dropdown-wrap col span_13">
						<select name="aaType" id="aaType">
							<option value=""> --Select Department-- </option>
						</select>
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="eVsupport">Enable Video Support:</label>
					</div>
					<div class="inputText">
						<input type="checkbox" id="eVsupport" name="eVsupport" value="1">
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="extensionDialing">Scope Of Extension Dialing:</label>
					</div>
					<div class="dropdown-wrap col span_13">
						<select name="extensionDialing" id="extensionDialing">
							<option>Group</option>
							<option>Department</option>
						</select>
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="nameDialing">Scope Of Name Dialing:</label>
					</div>
					<div class="dropdown-wrap col span_13">
						<select name="nameDialing" id="nameDialing">
							<option>Group</option>
							<option>Department</option>
						</select>
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="nameDialingEntries">Name Dialing Entries:</label>
					</div>
					<div class="dropdown-wrap col span_13">
						<select name="nameDialingEntries" id="nameDialingEntries">
							<option>LastName + FirstName</option>
							<option>LastName + FirstName and FirstName + LastName</option>
						</select>
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="nameDialingEntries">Phone Number:</label>
					</div>
					<div class="dropdown-wrap col span_13">
						<select name="nameDialingEntries" id="nameDialingEntries">
							<option>None</option>
						</select>
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="extension">Extension:</label>
					</div>
					<div class="col span_13">
							<input type="text" name="extension" id="extension" size="35" maxlength="30" />
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="nameDialingEntries">Time zone:</label>
					</div>
					<div class="dropdown-wrap col span_13">
						<select name="timeZone" id="timeZone">
							<option>None</option>
						</select>
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="nameDialingEntries">Business Hours Schedule:</label>
					</div>
					<div class="dropdown-wrap col span_13">
						<select name="businessHoursSchedule" id="businessHoursSchedule">
							<option>None</option>
						</select>
					</div>
				</div>
				
				<div class="row formspace">
					<div class="col span_6">
						<label class="labelpadding" for="nameDialingEntries">Holiday Schedule:</label>
					</div>
					<div class="dropdown-wrap col span_13">
						<select name="holidaySchedule" id="holidaySchedule">
							<option>None</option>
						</select>
					</div>
				</div>
				
				</div>
				</div>
				<div id="business_hours_menu" class="userDataClass" style="display: none;">
				 	<div class="subBanner" style="margin-bottom: 25px;">Business Hours Menu</div>
				 	<div style="clear:right"></div>
					<div class="row formspace fcorn-register container">
						<div class="col span_9">
							<label class="labelpadding" for="enableFLED" style="padding: 7px 0;"><b> Enable first-level extension dialing:</b></label>
						</div>
						<div class="col span_3">
							<input type="checkbox" style="margin:11px;"/>
						</div>
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2"><label class="labelpadding" style="padding: 7px 0;"><b>Key</b></label></div>
						<div class="col span_7" style="margin-right:10px;"><label class="labelpadding"><b>Action</b></label></div>
						<div class="col span_7" style="margin-right:10px;"><label class="labelpadding"><b>Description</b></label></div>
						<div class="col span_7"><label class="labelpadding"><b>Action Data</b></label></div>
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">0</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div>
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">1</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">2</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">3</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">4</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">6</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">7</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">8</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">9</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">*</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">#</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
				</div>
				<div id="after_hours_menu" class="userDataClass" style="display: none;">
					<div class="subBanner" style="margin-bottom: 25px;">After Hours Menu</div>
				 	<div style="clear:right"></div>
					<div class="row formspace fcorn-register container">
						<div class="col span_9">
							<label class="labelpadding" for="enableFLED" style="padding: 7px 0;"><b> Enable first-level extension dialing:</b></label>
						</div>
						<div class="col span_3">
							<input type="checkbox" style="margin:11px;"/>
						</div>
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2"><label class="labelpadding" style="padding: 7px 0;"><b>Key</b></label></div>
						<div class="col span_7" style="margin-right:10px;"><label class="labelpadding"><b>Action</b></label></div>
						<div class="col span_7" style="margin-right:10px;"><label class="labelpadding"><b>Description</b></label></div>
						<div class="col span_7"><label class="labelpadding"><b>Action Data</b></label></div>
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">0</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div>
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">1</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">2</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">3</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">4</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">6</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">7</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">8</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">9</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">*</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">#</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
				</div>
				<div id="holiday_menu" class="userDataClass" style="display: none;">
					<div class="subBanner" style="margin-bottom: 25px;">Holiday Menu</div>
				 	<div style="clear:right"></div>
					<div class="row formspace fcorn-register container">
						<div class="col span_9">
							<label class="labelpadding" for="enableFLED" style="padding: 7px 0;"><b> Enable first-level extension dialing:</b></label>
						</div>
						<div class="col span_3">
							<input type="checkbox" style="margin:11px;"/>
						</div>
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2"><label class="labelpadding" style="padding: 7px 0;"><b>Key</b></label></div>
						<div class="col span_7" style="margin-right:10px;"><label class="labelpadding"><b>Action</b></label></div>
						<div class="col span_7" style="margin-right:10px;"><label class="labelpadding"><b>Description</b></label></div>
						<div class="col span_7"><label class="labelpadding"><b>Action Data</b></label></div>
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">0</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div>
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">1</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">2</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">3</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">4</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">6</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">7</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">8</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">9</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">*</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
					
					<div class="row formspace fcorn-register container">
						<div class="col span_2">#</div>
						<div class="col span_7" style="margin-right:10px;">
							<select>
								<option>None</option>
								<option>Transfer With Prompt</option>
								<option>Transfer Withput Prompt</option>
								<option>Transfer To Operator</option>
								<option>Name Dialing</option>
								<option>Extension Dialing</option>
								<option>Transfer To Mailbox</option>
								<option>Play Anouncement</option>
								<option>Repeat Menu</option>
								<option>Exit</option>
							</select>
						</div>
						<div class="col span_7" style="margin-right:10px;"><input type="text" name="" id="" size="35" maxlength="30" /></div>
<!-- 						<div class="col span_7"><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></div> -->
					</div>
				</div>
				<div id="services_1" class="userDataClass" style="display: none;">
					<div class="subBanner" style="margin-bottom: 25px;">Services</div>
				 	<div style="clear:right"></div>
				 	<div class="row formspace fcorn-register container">
				 	<div class="col span_6">
				 		<p style="text-align: center">Available Services</p>
				 		<br />
				 		<div style="width:100%;">
				 			<select style="height: 250px;" name="" id="" multiple="">
				 			
				 			</select>
				 		</div>
				 	</div>
				 	<div class="col span_4" style="text-align: center; padding: 70px 0;">
				 		<div style="max-width: 120px; margin: 0 auto; text-align: center;">
				 			<input type="button" style="width: 110px; font-size: 12px; margin: 5px 0;" value="Add >">
				 			<input type="button" style="width: 110px; font-size: 12px; margin: 5px 0;" value="< Remove">
				 			<input type="button" style="width: 110px; font-size: 12px; margin: 5px 0;" value="AddAll >>">
				 			<input type="button" style="width: 110px; font-size: 12px; margin: 5px 0;" value="<< Remove All">
				 		</div>
				 		
				 	</div>
				 	<div class="col span_6">
				 		<p style="text-align: center">User Services</p>
				 		<br />
				 		<div style="width:100%;">
				 			<select style="height: 250px;" name="" id="" multiple="">
				 			
				 			</select>
				 		</div>
				 	</div>
				 	</div>
				</div>
				</div>
				<div style="text-align: left; float:left;"><input id="subButtonDel" value="Delete Group" type="button"></div>
				<div style="text-align: right;">
				 <input type="button" id="aaCancel" value="Cancel"> <span class="spacer">&nbsp;</span>
				<input type="button" id="subButton" value="Confirm Settings">
				</div>
			
		</form>
		</div>
	<div id="AAData"></div>
</div>

<form name="aaInfo" id="aaInfo" method="POST">
	<input type="hidden" name="aaId" id="aaId" value="<?php echo $aaId; ?>">
	<div id="aaTabs">
		<ul>
			<li><a href="#aaSettings_1" class="tabs" id="aaSettings">Settings</a></li>
			<li><a href="#businessHours_1" class="tabs" id="businessHours">Business Hours Menu</a></li>
			<li><a href="#afterHours_1" class="tabs" id="afterHours">After Hours Menu</a></li>
			<?php
				if ($_SESSION["aa"]["type"] == "Standard")
				{
					echo "<li><a href=\"#holiday_1\" class=\"tabs\" id=\"holiday\">Holiday Menu</a></li>";

					for ($i = 0; $i < count(preg_grep("/^submenu(\d)+$/", array_keys($_SESSION["aa"]))); $i++)
					{
						echo "<li><a href=\"#submenu_" . $i . "\" class=\"tabs\" id=\"submenu" . $i . "\">Submenu - " . $_SESSION["aa"]["submenu" . $i]["id"] . "</a></li>";
					}
				}
			?>
		</ul>

		<div id="aaSettings_1" class="userDataClass" style="display:none;">
			<div class="subBanner">Basic Info</div>
			<div style="clear:right;">&nbsp;</div>

			<div class="row formspace fcorn-register container">
				<div style="padding:10px;" class="col span_12">
					<span class="required">*</span><label for="aaName">Auto Attendant Name</label>
					<input type="text" name="aaName" id="aaName" size="35" maxlength="30" value="">
				</div>
				<div style="padding:10px;" class="col span_12">
					<strong>Auto Attendant Type:</strong>
					<span id="autoAddentantTypeVal"><?php echo $_SESSION["aa"]["type"]; ?></span>
					<div class="clr"></div>
					<strong>Phone Number:</strong>
						<span id="aaPhoneNumber"></span>
					<div class="clr"></div>
					<strong>Extension:</strong>
					<span id="aaExtension"></span>
				</div>
				<span class="required">*</span> <label for="callingLineIdLastName">Calling Line ID Last Name</label>
				<input type="text" name="callingLineIdLastName" id="callingLineIdLastName" size="35" maxlength="30" value="">
				<div class="clr"></div>
				<span class="required">*</span> <label for="callingLineIdFirstName">Calling Line ID First Name</label>
				<input type="text" name="callingLineIdFirstName" id="callingLineIdFirstName" size="35" maxlength="30" value="">
				<div class="clr"></div>
				<span class="required">*</span> <label for="timeZone">Time Zone</label>
				<div class="dropdown-wrap">
					<select name="timeZone" id="timeZone"></select>
				</div>
				<div class="clr"></div>
				<label for="businessHoursSchedule">Business Hours Schedule</label>
				<div class="dropdown-wrap">
					<select name="businessHoursSchedule" id="businessHoursSchedule">
						<option value=""></option>
						<?php
							if (isset($schedules))
							{
								for ($a = 0; $a < count($schedules); $a++)
								{
									if ($schedules[$a]["type"] == "Time")
									{
										if ($schedules[$a]["name"] == $_SESSION["aa"]["businessHoursSchedule"])
										{
											$sel = "SELECTED";
										}
										else
										{
											$sel = "";
										}
										echo "<option value=\"" . $schedules[$a]["name"]. "\"" . $sel . ">" . $schedules[$a]["name"]. "</option>\n";
									}
								}
							}
						?>
					</select>
				</div>
				<div class="clr"></div>
				<label for="holidaySchedule">Holiday Schedule</label>
				<div class="dropdown-wrap">
					<select name="holidaySchedule" id="holidaySchedule">
						<option value=""></option>
						<?php
							if (isset($schedules))
							{
								for ($a = 0; $a < count($schedules); $a++)
								{
									if ($schedules[$a]["type"] == "Holiday")
									{
										if ($schedules[$a]["name"] == $_SESSION["aa"]["holidaySchedule"])
										{
											$sel = "SELECTED";
										}
										else
										{
											$sel = "";
										}
										echo "<option value=\"" . $schedules[$a]["name"]. "\"" . $sel . ">" . $schedules[$a]["name"]. "</option>\n";
									}
								}
							}
						?>
					</select>
				</div>
				<div class="clr"></div>
				<span class="required">*</span> <label for="nameDialingEntries">Dial By Name Entries</label>
				<div class="dropdown-wrap">
					<select name="nameDialingEntries" id="nameDialingEntries">
						<option value=""></option>
						<?php
							$dbns = array("LastName + FirstName", "LastName + FirstName or FirstName + LastName");
							foreach ($dbns as $value)
							{
								if ($value == $_SESSION["aa"]["nameDialingEntries"])
								{
									$sel = "SELECTED";
								}
								else
								{
									$sel = "";
								}
								echo "<option value=\"" . $value . "\"" . $sel . ">" . $value . "</option>";
							}
						?>
					</select>
				</div>
			</div>
		</div><!--end aaSettings_1 div-->
		<?php
			foreach ($menuTypes as $key => $value)
			{
					if ($key == "bh")
					{
						$divId = "businessHours_1";
					}
					if ($key == "ah")
					{
						$divId = "afterHours_1";
					}
					if ($key == "h")
					{
						$divId = "holiday_1";
					}
					if (strpos($key, "submenu") === 0)
					{
						$divId = "submenu_" . substr($key, 7);
					}
				?>
				<div id="<?php echo $divId; ?>" class="userDataClass" style="display:none;">
					<div class="subBanner"><?php echo $value; ?></div>
					<div style="clear:right;">&nbsp;</div>

					<div class="row formspace fcorn-register container">
						<?php
							if (strpos($key, "submenu") === 0)
							{
								?>
								<span class="required">*</span> <label for="<?php echo $key; ?>[id]">Submenu ID</label>
								<input type="text" name="<?php echo $key; ?>[id]" id="<?php echo $key; ?>[id]" size="40" maxlength="40" value="<?php echo $_SESSION["aa"][$key]["id"]; ?>">
								<?php
									echo "<label for=\"" . $key . "[enableLevelExtensionDialing]\">Enable extension dialing at anytime</label>";
							}
							else
							{
								echo "<label for=\"" . $key . "[enableLevelExtensionDialing]\">Enable first-level extension dialing</label>";
							}
						?>
						<input type="hidden" name="<?php echo $key; ?>[enableLevelExtensionDialing]" value="false" />
						<input style="margin-left:330px;margin-top:-16px;" type="checkbox" name="<?php echo $key; ?>[enableLevelExtensionDialing]" id="<?php echo $key; ?>[enableLevelExtensionDialing]" value="true"<?php echo $_SESSION["aa"][$key]["enableLevelExtensionDialing"] == "true" ? " checked" : ""; ?>>
						<div class="clr"></div>
						<div class="leftDesc2" style="width:11%;">Key</div>
						<div class="inputText"><span class="required">*</span> Action</div>
						<div class="inputText">Description</div>
						<div class="inputText">Action Data</div>
						<div style="clear:right;">&nbsp;</div>
						<?php
							foreach ($keys as $v)
							{
								echo "<div class=\"leftDesc2\" style=\"width:11%;\">" . $v . "</div>";
								$desc = (isset($_SESSION["aa"][$key]["keys"][$v]["desc"]) ? $_SESSION["aa"][$key]["keys"][$v]["desc"] : "");
								$act = (isset($_SESSION["aa"][$key]["keys"][$v]["action"]) ? $_SESSION["aa"][$key]["keys"][$v]["action"] : "");
								$ph = (isset($_SESSION["aa"][$key]["keys"][$v]["phoneNumber"]) ? $_SESSION["aa"][$key]["keys"][$v]["phoneNumber"] : "");
								$sub = (isset($_SESSION["aa"][$key]["keys"][$v]["submenuId"]) ? $_SESSION["aa"][$key]["keys"][$v]["submenuId"] : "");

								$description = "<input type=\"text\" name=\"" . $key . "[keys][" . $v . "][desc]\" id=\"" . $key . "[keys][" . $v . "][desc]\" size=\"25\" maxlength=\"20\" value=\"" . $desc . "\">";
								$action = "<select name=\"" . $key . "[keys][" . $v . "][action]\" id=\"" . $key . "[keys][" . $v . "][action]\" onchange=\"displayPh(this.value, '" . $key . "', '" . $v . "')\">";
								$action .= "<option value=\"\"></option>";
								foreach ($possibleActions as $vActions)
								{
									if (($_SESSION["aa"]["type"] == "Standard" and strpos($key, "submenu") === 0)
										or ($_SESSION["aa"]["type"] == "Standard" and strpos($key, "submenu") !== 0 and $vActions !== "Return to Previous Menu")
										or ($_SESSION["aa"]["type"] == "Basic" and $vActions !== "Transfer To Submenu" and $vActions !== "Return to Previous Menu"))
									{
										if ($act == $vActions)
										{
											$sel = "SELECTED";
										}
										else
										{
											$sel = "";
										}
										$action .= "<option value=\"" . $vActions . "\"" . $sel . ">" . $vActions . "</option>\n";
									}
								}
								$action .= "</select><div class=\"clr\"></div>";
								$phoneNumber = "<label for=\"" . $key . "[keys][" . $v . "][phoneNumber]\">Phone Number</label> ";
								$phoneNumber .= "<input place=\"Phone Number\" type=\"text\" name=\"" . $key . "[keys][" . $v . "][phoneNumber]\" id=\"" . $key . "[keys][" . $v . "][phoneNumber]\" size=\"30\" maxlength=\"30\" value=\"" . $ph . "\"><div class=\"clr\"></div>";
								$submenu = "<span class=\"required\">*</span> Submenu ";
								$submenu .= "<select name=\"" . $key . "[keys][" . $v . "][submenu]\" id=\"" . $key . "[keys][" . $v . "][submenu]\">";
								$submenu .= "<option value=\"\"></option>";
								for ($i = 0; $i < count(preg_grep("/^submenu(\d)+$/", array_keys($_SESSION["aa"]))); $i++)
								{
									if ((strpos($key, "submenu") === 0 and $i != substr($key, 7)) or strpos($key, "submenu") !== 0)
									{
										if ($sub == $_SESSION["aa"]["submenu" . $i]["id"])
										{
											$sel = "SELECTED";
										}
										else
										{
											$sel = "";
										}
										$submenu .= "<option value=\"" . $_SESSION["aa"]["submenu" . $i]["id"] . "\"" . $sel . ">" . $_SESSION["aa"]["submenu" . $i]["id"] . "</option>\n";
									}
								}
								$submenu .= "</select>";

								echo "<div class=\"inputText\">" . $action . "</div>";
								echo "<div class=\"inputText\">" . $description . "</div>";
								echo "<div class=\"inputText\" id=\"phone" . $key . $v . "\"";
								if ($act == "Transfer With Prompt" or $act == "Transfer Without Prompt" or $act == "Transfer To Operator")
								{
									echo " style=\"display:inline-block;\"";
								}
								else
								{
									echo " style=\"display:none;\"";
								}
								echo ">";
								echo $phoneNumber;
								echo "</div>";
								echo "<div class=\"inputText\" id=\"sub" . $key . $v . "\"";
								if ($act == "Transfer To Submenu")
								{
									echo " style=\"display:inline-block;\"";
								}
								else
								{
									echo " style=\"display:none;\"";
								}
								echo ">";
								echo $submenu;
								echo "</div>";
								echo "<div style=\"clear:right;\">&nbsp;</div>";
//								echo "<script>displayPh($(\"#ah\\\\[keys\\\\]\\\\[" . $v . "\\\\]\\\\[action\\\\] option:selected\").value, \"ah\", \"" . $v . "\");</script>";
							}
						?>
					</div>
				</div><!--end <?php echo $divId; ?> div-->
				<?php
			}
		?>
	</div>
	<div style="text-align:right;"><input type="button" id="subButtonAA" value="Confirm Settings" onClick="window.location='#top'"></div>
</form>
<div id="dialogAA" class="dialogClass"></div>