$(function()
{
	$("#aaTabs").tabs();
	$("#module").html("> Modify Auto Attendant: ");
	$("#endUserId").html("");
	$('#addAutoAttendant').hide();
	$("#tabs").tabs();
	
	$('#newAttendant').click(function() {
		$('#addAutoAttendant').show();
		$('#modifyAttendant').hide();
		$('#AAData').hide();
	});
	
	$('#aaCancel').click(function(){
		$('#addAutoAttendant').hide();
		$('#modifyAttendant').show();
		//$('#AAData').show();
    });
	
	$('#backTomainForm').click(function() {
		$('#modifyAttendant').show();
		$('#addAutoAttendant').hide();
	})
	
	var getBasicInfo = function(obj){
		$("#aaName").val(obj.aaName);
		$("#autoAddentantTypeVal").html(obj.type);
		$("#aaPhoneNumber").html(obj.phoneNumber);
		$("#aaPhoneNumber").html(obj.phoneNumber);
		$("#aaExtension").html(obj.extension);
		$("#callingLineIdLastName").val(obj.callingLineIdLastName);
		$("#callingLineIdFirstName").val(obj.callingLineIdFirstName);
		$("#timeZone").val(obj.timeZone);
		$("#timeZone").val(obj.timeZone);
		$("#businessHoursSchedule").val(obj.timeZone);
	}
	
	$("#AAChoice").change(function()
	{
		$("#AAData").html("");
		var id = $("#AAChoice").val();

		if (id !== "")
		{
			$.ajax({
				type: "POST",
				url: "AutoAttendantGroup/getAutoAttendantInfo.php",
				data: { AAId: id },
				success: function(result) {
					
					var obj = jQuery.parseJSON(result);
					getBasicInfo(obj.aa);
					$('#AAData').show();
					$("#AAData").html(result);
				}
			});
		}
		else
		{
			$("#endUserId").html("");
		}
	});

	$("#endUserId").html("<?php echo $aaId; ?>");

	$("#dialogAA").dialog($.extend({}, defaults, {
		width: 900,
		buttons: {
			"Complete": function() {
				var dataToSend = $("#aaInfo").serialize();
				$.ajax({
					type: "POST",
					url: "autoAttendant/aaDo.php",
					data: dataToSend,
					success: function(result) {
						$("#dialogAA").dialog("option", "title", "Request Complete");
						$(".ui-dialog-titlebar-close", this.parentNode).hide();
						$(".ui-dialog-buttonpane", this.parentNode).hide();
						$("#dialogAA").html(result);
						$("#dialogAA").append(returnLink);
					}
				});
			},
			"Cancel": function() {
				$(this).dialog("close");
			}
		}
	}));

	$("#subButtonAA").click(function() {
		var dataToSend = $("#aaInfo").serialize();
		$.ajax({
			type: "POST",
			url: "autoAttendant/checkData.php",
			data: dataToSend,
			success: function(result) {
				$("#dialogAA").dialog("open");
				if (result.slice(0, 1) == "1")
				{
					$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
				}
				else
				{
					$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
				}
				result = result.slice(1);
				$("#dialogAA").html(result);
			}
		});
	});
	function displayPh(dropdownValue, hours, value)
	{
		if (value == "*" || value == "#")
		{
			value = "\\" + value;
		}
		if (dropdownValue == "Transfer With Prompt" || dropdownValue == "Transfer Without Prompt" || dropdownValue == "Transfer To Operator")
		{
			$("#phone" + hours + value).css("display", "inline-block");
		}
		else
		{
			$("#phone" + hours + value).css("display", "none");
			$("#" + hours + "\\[keys\\]\\[" + value + "\\]\\[phoneNumber\\]").val("");
		}
		if (dropdownValue == "Transfer To Submenu")
		{
			$("#sub" + hours + value).css("display", "inline-block");
		}
		else
		{
			$("#sub" + hours + value).css("display", "none");
			$("#" + hours + "\\[keys\\]\\[" + value + "\\]\\[submenu\\]").val("");
		}
	}

	var autoAttendantList = function(){
		var html = "<option value=''>None</option>";
		$.ajax({
			type: "POST",
			url: "AutoAttendantGroup/getAllAttendants.php",
			success: function(result) {
				
				var obj = jQuery.parseJSON(result);
				for (var i = 0; i < obj.length; i++) {
					html += '<option value="'+ obj[i].id +'">' + obj[i].name + '</option>';
				}
				
				$("#AAChoice").html(html);
			}
		});
	};
	autoAttendantList();
	
	
	var schedulesList = function(){
		var html = "<option value=''>None</option>";
		$.ajax({
			type: "POST",
			url: "AutoAttendantGroup/getAllSchedules.php",
			success: function(result) {				
				var obj = jQuery.parseJSON(result);
				for (var i = 0; i < obj.length; i++) {
					html += '<option value="'+ obj[i].id +'">' + obj[i].name + '</option>';
				}
				
				$("#businessHoursSchedule").html(html);
			}
		});
	};
	schedulesList();
	
});