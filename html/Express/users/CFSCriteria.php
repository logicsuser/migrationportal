<?php
/**
 * Created by Karl.
 * Date: 6/18/2016
 */

    require_once("/var/www/html/Express/config.php");
    checkLogin();


    function getGroupScheduleLevel($scheduleName) {
        global $schedules;

        if ($scheduleName == "") {return "";}

        foreach ($_SESSION["schedules"] as $key=>$value) {
//        foreach ($schedules as $key=>$value) {
            if ($value["name"] == $scheduleName) {
                return $value["level"];
            }
        }

        return "";
    }


/*
    // DB Version
    function hasCriteria($criteriaName = "") {
        global $db;

        $query = "select count(*) from cfsCriteria";
        $query .= " where sp='" . $_SESSION["sp"] . "' and groupID='" . $_SESSION["groupId"] . "'";
        if ($criteriaName != "") {
            $query .= " and cfsCriteriaName='" . $criteriaName . "'";
        }
        $result = $db->query($query);
        return $result->fetchColumn() > 0;
    }
 */

    /*
     * check whether there are any criteria stored in $_SESSION data
     * input: criteria name - check whether criteria of specified name exists
     *        if parameter has no name, return status whether any CFS criteria exist
     */
    function hasCriteria($criteriaName = "") {
        if ($criteriaName != "") {
            return isset($_SESSION["cfsCriteria"][$criteriaName]);
        }

        return isset($_SESSION["cfsCriteria"]);
    }


    /*
     * Format for display the scenario of forwarding of incoming calls based on:
     * - whether it applies to any incoming calls
     * - whether it applies to incoming private calls
     * - whether it applies to incoming unavailable calls
     * - whether it applies to calls from user specified phone numbers
     */
    function determineForwardedCallsFrom($value) {
        if ($value["cfsCriteriaFwdFrom"] == "Any") {
            return "All calls";
        }

        $forwardedCalls = "";
        if ($value["cfsCriteriaFwdFromPrivate"] == "true") {
            $forwardedCalls = "All private";
        }
        if ($value["cfsCriteriaFwdFromUnavailable"] == "true") {
            $forwardedCalls .= $forwardedCalls == "" ? "All unavailable" : " and unavailable";
        }
        $forwardedCalls .= $forwardedCalls != "" ? " calls" : "";

        for ($i = 1; $i<= 12; $i++) {
            $attr = "cfsCriteriaSpecific";
            if ($i < 10) {
                $attr .= "0";
            }
            $attr .= $i;
            if ($value[$attr] != "") {
                $forwardedCalls .= $forwardedCalls == "" ? $value[$attr] : "; " . $value[$attr];
            }
        }
        return $forwardedCalls;
    }


    /*
     * Create HTML Criteria Table with the following columns:
     * - active checkboxes - checked on default
     * - Criteria name
     * - Forward to a number status
     * - 'Forward to' phone number
     * - Time Schedule
     * - Holiday Schedule
     * - Forwarding of incoming calls scenario
     *
     * Note: All criteria rows are selectable and have handlers implementation
     */
    function createCriteriaTable() {
        // No criteria message if no criteria has been created
        if (! hasCriteria()) {
            return "<div style=\"margin-left: 1%\" class=\"leftDesc\">-- No Criteria --</div>";
        }

        $htmlTable = "";
        $htmlTable .= "<table class=\"leftDesc1\" id=\"criteriaTable\" style=\"width: 100%;\">";
        $htmlTable .= "<thead><tr>";
        $htmlTable .= "<th style=\"width: 8%\"><strong>Active</strong></th>";
        $htmlTable .= "<th style=\"width: 15%\"><strong>Criteria Name</strong></th>";
        $htmlTable .= "<th style=\"width: 10%\"><strong>Forward</strong></th>";
        $htmlTable .= "<th style=\"width: 17%\"><strong>Forward To</strong></th>";
        $htmlTable .= "<th style=\"width: 17%\"><strong>Time Schedule</strong></th>";
        $htmlTable .= "<th style=\"width: 17%\"><strong>Holiday Schedule</strong></th>";
        $htmlTable .= "<th style=\"width: 12%\"><strong>Calls From</strong></th>";
        $htmlTable .= "<th style=\"width: 4%\"></th>";
        $htmlTable .= "</tr></thead>";

        $htmlTable .= "<tbody>";

        $a = 0;
        foreach ($_SESSION["cfsCriteria"] as $key => $value) {
            $name = $value["cfsCriteriaName"];
            $checkBoxName = "cfsCritChk" . $a;
            $hiddenCheckBoxName = "cfsCritHidden" . $a;
            $htmlTable .= "<tr id=\"" . $name . "\" style=\"margin-bottom: 20px\" class=\"criteriaTableTr\" onmouseover=\"highlightCriteriaRow(this, true)\" onmouseout=\"highlightCriteriaRow(this, false)\" onclick=\"evalCriteriaRow(this)\">";
            $htmlTable .= "<td style=\"width: 6%\"><input type=\"checkbox\" name=\"" . $checkBoxName . "\" id=\"" . $checkBoxName . "\" value=\"true\" checked><label onmouseover=\"checkBoxEventAct(true)\" onmouseout=\"checkBoxEventAct(false)\" for =\"" . $checkBoxName . "\"><span></span></label>";
            $htmlTable .= "<input type=\"hidden\" name=\"" . $hiddenCheckBoxName . "\" value=\"false\">";
            $htmlTable .= "</td>";
            $htmlTable .= "<td style=\"width: 13%\">" . $name . "</td>";

            $htmlTable .= "<td style=\"width: 14%\">";
            $htmlTable .= $value["cfsCriteriaFwd"] == "Do not forward" ? "No" : "Yes";
            $htmlTable .= "</td>";

            $htmlTable .= "<td style=\"width: 17%\">";
            $htmlTable .= $value["cfsCriteriaFwd"] == "Forward To Specified Number" ? $value["cfsAnotherFwdNum"] : "Default number";
            $htmlTable .= "</td>";

            $htmlTable .= "<td style=\"width: 17%\">" . ($value["cfsCriteriaTime"] != "" ? $value["cfsCriteriaTime"] : "Every Day All Day") . "</td>";
            $htmlTable .= "<td style=\"width: 17%\">" . ($value["cfsCriteriaHoliday"] != "" ? $value["cfsCriteriaHoliday"] : "None") . "</td>";
            $htmlTable .= "<td style=\"width: 12%\">" . determineForwardedCallsFrom($value) . "</td>";

            $htmlTable .= "<td style=\"width: 4%\"><img src=\"images/icons/eyes_rest.png\" >";
            $htmlTable .= "</tr>";
            $htmlTable .= "<tr class=\"blankTableRow\" style=\"height: 12px\"></tr>";
            $a++;
        }

        $htmlTable .= "</tbody>";
        $htmlTable .= "</table>";

        return $htmlTable;
    }

/*
    // DB VERSION
    function createCriteriaRecord() {
        global $db;

        $insert = "INSERT INTO cfsCriteria (sp, groupID, cfsCriteriaName, ";
        $values = "VALUES ('" . $_SESSION["sp"] . "', '" . $_SESSION["groupId"] . "', '" . $_POST["cfsCriteriaName"] . "', ";

        if ($_POST["cfsAnotherFwdNum"] != "") {
            $insert .= "cfsAnotherFwdNum, ";
            $values .= "'" . $_POST["cfsAnotherFwdNum"] . "', ";
        }
        $insert .= "cfsCriteriaFwd, cfsCriteriaTime, cfsCriteriaHoliday, cfsCriteriaFwdFrom, ";
        $values .= "'" . $_POST["cfsCriteriaFwd"] . "', '" . $_POST["cfsCriteriaTime"] . "', '" . $_POST["cfsCriteriaHoliday"] . "', '" . $_POST["cfsCriteriaFwdFrom"] . "', ";

        $insert .= "cfsCriteriaFwdFromPrivate, cfsCriteriaFwdFromUnavailable";
        $values .= isset($_POST["cfsCriteriaFwdFromPrivate"]) && $_POST["cfsCriteriaFwdFromPrivate"] == "true" ? "'true', " : "'false', ";
        $values .= isset($_POST["cfsCriteriaFwdFromUnavailable"]) && $_POST["cfsCriteriaFwdFromUnavailable"] == "true" ? "'true'" : "'false'";

        for ($i = 1; $i<= 12; $i++) {
            $attr = "cfsCriteriaSpecific";
            if ($i < 10) { $attr .= "0"; }
            $attr .= $i;
            if ($_POST[$attr] != "") {
                $insert .= ", " . $attr;
                $values .= ", '" . $_POST[$attr] . "'";
            }
        }

        $insert .= ")";
        $values .= ")";
        $query = $insert . " " . $values;
        $db->query($query);

        return $query; // for test purpose only
    }
 */

    /*
     * create criteria record from dialog form
     * input parameters: $name - name of the criteria (optional)
     * Note: when a brand new criteria is created, name is provided on the dialog form,
     * but when an existing criteria is being updated, name on the dialog form is disabled, therefore the name
     * is provided in the input parameter
     *
     * Criteria information is saved into $_SESSION
     */
    function createCriteriaRecord($name = "")
    {
        if ($name == "") {
            $name = $_POST["cfsCriteriaName"];
        }

        $_SESSION["cfsCriteria"][$name]["cfsCriteriaName"] = $name;
        $_SESSION["cfsCriteria"][$name]["cfsCriteriaActive"] = "true"; // always keep newly created criteria in Active state
        $_SESSION["cfsCriteria"][$name]["cfsAnotherFwdNum"] = $_POST["cfsAnotherFwdNum"];
        $_SESSION["cfsCriteria"][$name]["cfsCriteriaFwd"] = $_POST["cfsCriteriaFwd"];
        $_SESSION["cfsCriteria"][$name]["cfsCriteriaTime"] = $_POST["cfsCriteriaTime"];
        $_SESSION["cfsCriteria"][$name]["cfsCriteriaTimeLevel"] = getGroupScheduleLevel($_POST["cfsCriteriaTime"]);
        $_SESSION["cfsCriteria"][$name]["cfsCriteriaHoliday"] = $_POST["cfsCriteriaHoliday"];
        $_SESSION["cfsCriteria"][$name]["cfsCriteriaHolidayLevel"] = getGroupScheduleLevel($_POST["cfsCriteriaHoliday"]);
        $_SESSION["cfsCriteria"][$name]["cfsCriteriaFwdFrom"] = $_POST["cfsCriteriaFwdFrom"];
        $_SESSION["cfsCriteria"][$name]["cfsCriteriaFwdFromPrivate"] = isset($_POST["cfsCriteriaFwdFromPrivate"]) && $_POST["cfsCriteriaFwdFromPrivate"] == "true" ? "true" : "false";
        $_SESSION["cfsCriteria"][$name]["cfsCriteriaFwdFromUnavailable"] = isset($_POST["cfsCriteriaFwdFromUnavailable"]) && $_POST["cfsCriteriaFwdFromUnavailable"] == "true" ? "true" : "false";

        for ($i = 1; $i <= 12; $i++) {
            $attr = "cfsCriteriaSpecific";
            if ($i < 10) {
                $attr .= "0";
            }
            $attr .= $i;
            $_SESSION["cfsCriteria"][$name][$attr] = $_POST[$attr];
        }
    }


    function updateCriteriaActivationStatus($activationList) {
        $activations = explode(",", $activationList);
        $index = 0;

        foreach ($_SESSION["cfsCriteria"] as $key => $value) {
            $name = $value["cfsCriteriaName"];
            $_SESSION["cfsCriteria"][$name]["cfsCriteriaActive"] = $activations[$index++];
        }
    }


    // --------------------------------
    // Ajax POST processing starts here
    // --------------------------------

    $error = false;
    $resp = "";

    // Create new Criteria
    if (isset($_POST["cfsCriteriaName"])) {
        if ($resp = hasCriteria($_POST["cfsCriteriaName"])) {
            $error = true;
            $resp = "Criteria '" . $_POST["cfsCriteriaName"] . "' already exists";
        } else {
            createCriteriaRecord();
            $resp = createCriteriaTable();
        }
    }


    // Update Criteria
    elseif (isset($_POST["cfsCriteriaNameUpdate"])) {
        createCriteriaRecord($_POST["cfsCriteriaNameUpdate"]);
        $resp = createCriteriaTable();
    }


    // Update Criteria Activation Status
    elseif (isset($_POST["activeStatus"])) {
        updateCriteriaActivationStatus($_POST["activeStatus"]);
    }


    // Delete Criteria
    elseif (isset($_POST["deleteCriteria"])) {
        if ($_POST["deleteCriteria"] != "") {
            unset($_SESSION["cfsCriteria"][$_POST["deleteCriteria"]]);
        }

        $count = 0;
        foreach ($_SESSION["cfsCriteria"] as $key => $value) {
            $count++;
            break;
        }
        if ($count == 0) {
            unset($_SESSION["cfsCriteria"]);
        }

        $resp = createCriteriaTable();
    }


    // Delete all criteria
    elseif (isset($_POST["deleteAllCriteria"])) {
        if (isset($_SESSION["cfsCriteria"])) {
            unset($_SESSION["cfsCriteria"]);
        }
    }

    $errFlag = $error ? "1" : "0";
    echo $errFlag . $resp;

?>
