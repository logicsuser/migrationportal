<?php
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

// 	require_once ("/var/www/html/Express/users/usersBasicList/usersBasicList-js.php");

$colWidth = getTableWidth("userTable");

$col1 = $colWidth['col1'] . "px";
$col2 = $colWidth['col2'] . "px";
$col3 = $colWidth['col3'] . "px";
$col4 = $colWidth['col4'] . "px";
$col5 = $colWidth['col5'] . "px";
$col6 = $colWidth['col6'] . "px";
$col7 = $colWidth['col7'] . "px";
$col8 = $colWidth['col8'] . "px";
$col9 = $colWidth['col9'] . "px";
$col10 = $colWidth['col10'] . "px";
$col11 = $colWidth['col11'] . "px";
$col12 = $colWidth['col12'] . "px";
$col13 = $colWidth['col13'] . "px";
$col14 = $colWidth['col14'] . "px";
$col15 = $colWidth['col15'] . "px";
$col16 = $colWidth['col16'] . "px";
$col17 = $colWidth['col17'] . "px";
$col18 = $colWidth['col18'] . "px";
$col19 = $colWidth['col19'] . "px";
$col20 = $colWidth['col20'] . "px";
$col21 = $colWidth['col21'] . "px";

//$_SESSION['usersView_selected'] = $usersView_selected = getPrevousSelectedView();
$_SESSION['usersView_selected'] = $usersView_selected = "Basic";

$showDetailedTable = $usersView_selected == "Detail" || $usersView_selected == "Registration" ? "block" : "None";
$showBasicTable = $usersView_selected == "Basic" || $usersView_selected == "" ? "block" : "None";
?>
<link rel="stylesheet" type="text/css" href="/Express/js/jquery.dataTables.min.css" media="screen">
<link rel="stylesheet" type="text/css" href="/Express/js/fixedColumns.dataTables.min.css" media="screen">

<style type="text/css">	
	td, td > .userIdVal {
		white-space: normal;
		word-break: break-all;
	}

	div.dataTables_wrapper {
		width: 90%;
		margin: 0 auto;
	}

	/*   common css */
	table.dataTable tbody th, table.dataTable tbody td {
		text-align: left;
	}

	table.dataTable thead th, table.dataTable thead td {
		text-align: left !important;
                color: #FFFFFF;
	}

	table.dataTable tbody td {
		padding: 0px 10px !important;
	}

    /* Commented @ 30 Oct 2018 */
    /* @sollogics Developer 28 sep hide shorting icon on basic info module  */
    /*#basicUserListTable_wrapper table.dataTable.no-footer thead tr .sorting_asc:first-child{background-image: none !important;}
    #basicUserListTable_wrapper table.dataTable.no-footer thead tr .sorting_desc:first-child{background-image: none !important;}

    table.dataTable.no-footer thead tr .sorting_asc{background-image: url(/Express/images/blue/desc.gif) !important;background-position: 98% 48%;}
    table.dataTable.no-footer thead tr .sorting_desc 
    {
        background-image: url(/Express/images/blue/asc.gif);
        background-position: 98% 48%;
    }
    table.dataTable thead .sorting {
        background-image: url(/Express/images/blue/bg.gif);
        background-position: 98% 48%;
    }*/


    /*#usersTable1_wrapper table thead tr th {border-top:1px solid #cccccc;border-bottom: 1px solid #cccccc;border-right: 1px solid #cccccc;vertical-align: middle;}
    #usersTable1_wrapper table thead tr th:first-child {border-left: 1px solid #cccccc; background-image: none !important;}
    .DTFC_LeftBodyWrapper,.dataTables_scrollBody{margin-top: -10px;}
    .dataTables_scrollBody{border-right: 2px solid #ccc;border-bottom: 1px solid #ccc !important;}  
    
    table#usersTable1{margin: 0; white-space: normal !important; word-break: break-all;}
   .DTFC_LeftHeadWrapper table thead tr th:first-child,  .DTFC_LeftHeadWrapper table thead tr .sorting_asc{background-image: none !important;cursor: default;}
    #usersTable1 tbody tr td {border-right: 1px solid #cccccc;}
	tr.unRegisterUsr td {border-right: none !important;}
    .DTFC_LeftBodyWrapper table tbody tr td{border-right: 1px solid #cccccc;vertical-align: middle;}
    .DTFC_LeftBodyLiner{overflow: hidden !important;width:auto !important; } */
     /*  @media only screen and (max-width: 1600px) and (min-width: 600px){

	/* @sollogics Developer 28 sep
	*  hide shorting icon on basic info module
	*/
	#basicUserListTable_wrapper table.dataTable.no-footer thead tr .sorting_asc:first-child {
		background-image: none !important;
                border-bottom: 0px solid #d8dada;
                border-top: 0px solid #d8dada;
                border-right: 2px solid #d8dada;
	}

	#basicUserListTable_wrapper table.dataTable.no-footer thead tr .sorting_desc:first-child {
		background-image: none !important;
                border-bottom: 2px solid #d8dada;
                border-top: 2px solid #d8dada;
                border-right: 2px solid #d8dada;
	}

	/*end code */
	/*table.dataTable.no-footer thead tr .sorting_asc {
		background-image: url(/Express/images/blue/desc.gif) !important;
		background-position: 98% 48%;
	} */

	/* table.dataTable.no-footer thead tr .sorting_desc {

		background-image: url(/Express/images/blue/asc.gif);
		background-position: 98% 48%;
	} */
        
        table.dataTable thead th, table.dataTable thead td {
    padding: 10px 18px;
    border-bottom: 0px solid #111 !important;
}

	table.dataTable thead .sorting {
		background-image: url(/Express/images/blue/bg.gif);
		background-position: 98% 48%;
                border-bottom: 0px solid #d8dada;
                border-top: 0px solid #d8dada;
                border-right: 2px solid #d8dada;
	}

	#basicUserListTable_wrapper table thead tr th,
	#allUsersRegistration_wrapper table thead tr th
	{
		border-top: 0px solid #d8dada;
		border-bottom: 0px solid #d8dada;
		border-right: 2px solid #d8dada;
	}

	#basicUserListTable_wrapper table thead tr th:first-child,
	#allUsersRegistration_wrapper table thead tr th:first-child {
		border-left: 2px solid #d8dada;
		background-image: none !important;
	}
        
        #basicUserListTable_wrapper table thead tr th:last-child{
            border-right: 0px;
        }

	#basicUserListTable_wrapper .dataTables_wrapper.no-footer .dataTables_scrollBody,
	#allUsersRegistration_wrapper .dataTables_wrapper.no-footer .dataTables_scrollBody {
		border-bottom: none;
	}

	 /* #basicUserListTable_wrapper .DTFC_LeftBodyWrapper,
	#basicUserListTable_wrapper .dataTables_scrollBody,
	#allUsersRegistration_wrapper .DTFC_LeftBodyWrapper,
	#allUsersRegistration_wrapper .dataTables_scrollBody {
		margin-top: -48px;
		padding-top: 25px; 
		border-top: 1px solid #fff;
		min-height: 100px;
		height: auto !important;
		max-height: 300px; 
                
	} */
        
        /*sollogics developer add code */
       #basicUserListTable_wrapper .DTFC_LeftBodyWrapper,
	#basicUserListTable_wrapper .dataTables_scrollBody,
	#allUsersRegistration_wrapper .DTFC_LeftBodyWrapper/*,
	#allUsersRegistration_wrapper .dataTables_scrollBody */{
		margin-top: -48px;
		padding-top: 25px; 
		border-top: 1px solid #fff;
		min-height: 100px;
		height: auto !important;
		/* max-height: 300px; */
               max-height: 450px; 
	}  
        
        
         
		#allUsersRegistration_wrapper .dataTables_scrollBody {
                    margin-top: -15px !important;
                    padding-top: -16px !important;
                    max-height: 355px;;
                }
	 
        /*end code */

	.col1 {
		min-width: <?php echo $col1; ?> !important;
	}

	.col2 {
		min-width: <?php echo $col2; ?> !important;
	}

	.col3 {
		min-width: <?php echo $col3; ?> !important;
	}

	.col4 {
		min-width: <?php echo $col4; ?> !important;
	}

	.col5 {
		min-width: <?php echo $col5; ?> !important;
	}

	.col6 {
		min-width: <?php echo $col6; ?> !important;
	}

	.col7 {
		min-width: <?php echo $col7; ?> !important;
	}

	.col8 {
		min-width: <?php echo $col8; ?> !important;
	}

	.col9 {
		min-width: <?php echo $col9; ?> !important;
	}

	.col10 {
		min-width: <?php echo $col10; ?> !important;
	}

	.col11 {
		min-width: <?php echo $col11; ?> !important;
	}

	.col12 {
		min-width: <?php echo $col12; ?> !important;
	}

	.col13 {
		min-width: <?php echo $col13; ?> !important;
	}

	.col14 {
		min-width: <?php echo $col14; ?> !important;
	}

	.col15 {
		min-width: <?php echo $col15; ?> !important;
	}

	.col16 {
		min-width: <?php echo $col16; ?> !important;
	}

	.col17 {
		min-width: <?php echo $col17; ?> !important;
	}

	.col18 {
		min-width: <?php echo $col18; ?> !important;
	}

	.col19 {
		min-width: <?php echo $col19; ?> !important;
	}

	.col20 {
		min-width: <?php echo $col20; ?> !important;
	}

	.col21 {
		min-width: <?php echo $col21; ?> !important;
	}

	table#usersTable1 {
		margin: 0;
		white-space: normal !important;
		word-break: break-all;
	}

	.DTFC_LeftHeadWrapper table thead tr th:first-child, .DTFC_LeftHeadWrapper table thead tr .sorting_asc {
		background-image: none !important;
                border-top: 0px solid #d8dada;
		border-bottom: 0px solid #d8dada;
		border-right: 2px solid #d8dada;
		cursor: default;
	}

	#usersTable1 tbody tr td {
		border-right: 2px solid #d8dada;
	}

	.DTFC_LeftBodyWrapper table tbody tr td {
		border-right: 2px solid #d8dada !important;
		vertical-align: middle;
	}

	.DTFC_LeftBodyLiner {
		overflow: hidden !important;
		width: auto !important;
	}

	.registerUsr td {
		border-top: 2px solid #d8dada !important;
		border-bottom: 2px solid #d8dada !important;
		background-color: #fff !important;
	}

	.registerUsr td:first-child {
		border-left: 2px solid #d8dada !important;
	}
        
	/*Registration Table*/
	.usersTableColor td {
		background-color: #fff !important;
	}

	table.customDataTable {
		font-size: 10px !important;
	}

	table.customDataTable thead {
		/* background-color: #e6eeee; */
                background-color: #5d81ac;
	}

	.allUsersRegistration thead tr th {
		border-top: 2px solid #d8dada;
		border-bottom: 2px solid #d8dada;
		border-right: 2px solid #d8dada;
	}

	.allUsersRegistration thead tr th:first-child {
		border-left: 2px solid #d8dada;
                border-bottom: 2px solid #d8dada;
		border-right: 2px solid #d8dada;
	}

	table#allUsersRegistration tbody tr td:first-child {
		border-left: 2px solid #d8dada;
		height: 22px;
	}

        .dataTables_scrollHead { width: 100% !important;  } 
	.dataTables_scrollHead, .DTFC_LeftHeadWrapper {
		z-index: 1;
	}

	.userBasicListDiv .userIdVal_Basic:hover td {
		background-color: #dcfac9 !important;
		cursor: pointer;
	}

	.viewDetail1 .userIdVal {
		cursor: pointer;
	}

	/*.dataTables_scrollBody{
                width: 97.5% !important;
		float: left;
	}*/
        
        /* Code added @ 02 Nov 2018 */
        .dataTables_wrapper .dataTables_paginate .paginate_button{
            padding: 8px !important; 
           }

     /*      
        a.paginate_button{
            background-color: transparent !important;
        } 
           .dataTables_wrapper .dataTables_paginate .paginate_button:hover{
             color: #6ea0dc !important;
             font-weight:700;
             background: transparent !important;
             border: none !important;
             border-bottom: 1px solid #6ea0dc !important;
           }
           
           .dataTables_wrapper .dataTables_paginate .paginate_button:active{
             color: #6ea0dc !important;
             font-weight:700;
             background: transparent !important;
             border: none !important;
             border-bottom: 1px solid #6ea0dc !important;
           }
*/
               a.paginate_button {
            background-color: #5d81ac !important;
            color: #fff !important;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: white !important;
            border: 1px solid #FFB200 !important;
            background-color: #FFB200 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #FFB200), color-stop(100%, none)) !important;
            background: -webkit-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: -moz-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: -ms-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: -o-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: linear-gradient(to bottom, #FFB200 0%, #FFB200 100%) !important;
        }
         
 
        .dataTables_wrapper .dataTables_paginate .paginate_button:active {
            background: #ffb200 ! important;
            border-bottom: 1px solid #ffb200 !important;  
        }
        

        a.paginate_button.paginate_number.active {
            background: #ffb200 ! important;
            border-bottom: 1px solid #ffb200 !important;  
        }
        
        .dataTables_wrapper .dataTables_paginate .paginate_button{
            color: #fff !important;
        }


           .dataTables_info{
            color: #6ea0dc !important;
            font-weight:700;
           }
           
           .dataTables_bottom_info_bar {
		 text-align: center;
	 }
	
	.dataTables_bottom_info_bar .retrieving_info {
		display: inline-block;
		float: left;
		padding-top: 0.755em;
	}
	.dataTables_bottom_info_bar .currently_showing_info {
		display: inline-block;
        }
        
        
        .dataTables_wrapper .dataTables_paginate {
                float: right;
                text-align: right;
                padding-top: 0.25em;
        }
        input .paginate_jump_input{
            width: 60px !important;
            text-align: center;
        }
        .retrieving_info{
            color: #6ea0dc !important;
            font-weight:700;
        }
        
        .filterMainHeadDiv{
            width: 60%;
            margin-left: 20%;
            float: left;
            text-align: center;
        }
        .filterApplyHead{            
            float: left;
            color: #BD8383;
            font-weight: bold;
            font-size: 12px;
            vertical-align: middle;
            width: 15%;
            padding-top: 6px;
            text-align: right;
        }
        .filterApplyHead img{            
            width: 25px;
            padding-right: 5px;
        }
        .filterTitleBtn:hover{
            color: #FFF;
        }
        

	/* 100% Width*/
	#allUsersRegistration_wrapper .dataTables_scrollHeadInner {
		width: 100% !important;
		padding-left: 0 !important;
		/*padding-right: 0 !important;*/
	}
	#allUsersRegistration_wrapper .dataTables_scrollHeadInner table {
		width: 100% !important;
                padding-right: 20px;  
		/*padding-right: 1.5%;*/
	}
	#allUsersRegistration_wrapper .dataTables_scrollBody {
		width: 100% !important;
	}
	#allUsersRegistration_wrapper .dataTables_scrollBody table {
		width: 100% !important;
	}
        
        
        /*sollogics developer add code */
     
       /* #allUsersRegistration_wrapper .dataTables_scrollHead {
            background: #5d81ac;
        } */

#allUsersRegistration_wrapper .dataTables_scrollHeadInner table {
    border-collapse: collapse !important;
    width: 100% !important;
}

#basicUserListTable_wrapper .dataTables_scrollBody {
    margin-top: -15px;
    padding-top: 10px;
	
	}
        /*end code */
    .paginate_jump_text{
        color: #6ea0dc !important;
        font-weight: 700;
    }
    
    .dataTables_wrapper .dataTables_paginate {
			float: right;
			text-align: right;
			padding-top: 0.25em;
                            color: #6ea0dc !important;
		}
</style>

<script type="text/javascript">

    // This will execute whenever the window is resized
    $(window).resize(function () {
        $(".triggerColumn").trigger("click");
        $("table.dataTable tbody td").css("padding", "2px 10px");
    });

    function eventAfterLoad() {
        $("table.dataTable tbody td").css("padding", "3px 10px");
    };

</script>
<script type="text/javascript">
    var allUsersRegistration_DT = null;
    var allUsersRegistration_Count = 1;
    
    function setRegisterTableSorter() {

        allUsersRegistration_DT = $('#allUsersRegistration').DataTable({
            scrollY: 450,
            destroy: true,
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            searching: false,
            autoWidth: false,

            info: true,
            language: {
                emptyTable: " ",
                infoEmpty: (allUsersRegistration_Count ? "Loading ..." : "No Users Found"),
                lengthMenu: "Show<br/> _MENU_ <br/>users",
                info: "Showing _START_ to _END_ of _TOTAL_ users",
            },
            dom: '<"wrapper"<"allUsersRegistration_top_bar dataTables_top_bar"<"column1"><"column2"><"column3"><"column4">>'
            + 't'
            + '<"allUsersRegistration_bottom_bar dataTables_bottom_info_bar"<"retrieving_info"><"currently_showing_info"i>>>'
        });
        /* function setRegisterTableSorter() {
            var table = $('#allUsersRegistration').DataTable( {
                scrollY:        500,
                destroy: true,
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                searching : false,
                info:     false
            } );

        } */
   }
</script>
<script>
    var quickFiltersIsApplied = false;
    var detailedUsersTableLoaded = false;
    var loadDetailedUsersTable = true;

    var selectedView = "";
    var userDetailDataTable;

    if ('<?php echo $usersView_selected ?>' == "Basic" || '<?php echo $usersView_selected; ?>' == "") {
        loadDetailedUsersTable = false;
        //loadBasicUsersTable = true;
    } else {
        loadDetailedUsersTable = true;
        //loadBasicUsersTable = false;
    }

    function limit() {


        showLoader();
        detailedUsersTableLoaded = true;
        loadDetailedUsersTable = false;

        //console.log('quickFiltersIsApplied: ' + quickFiltersIsApplied);
        //console.log($('#useBasicFilteredListInDetailedList').is(':checked'));

        $("#selectAllDiv").hide();
        $("#usersTable").html("");
        $("#loading2").show();

        $.ajax({
            type: "POST",
            url: "users/userDetailedList/index.php",
            data: '',
            success: function (result) {
                
                showDetailLoader = false;
                $("#usersTable").html(result);
                detailedUsersTableLoaded = true;
                $("#usersTable").show();
                setTimeout(function (){
                    $("#selectAllDiv").show();
                }, 3000);
            }
        });
    }

    function recursiveDataTableReg() {
        if (!$(".allUsersRegistration").is(":visible")) {
            setTimeout(function () {
                recursiveDataTableReg();
            }, 300);
        } else {
            applyDataTable();
        }
    }


    function recursiveDataTableDetail() {
        if (!$("#usersTable1").is(":visible")) {
            setTimeout(function () {
                recursiveDataTableDetail();
            }, 300);
        } else {
            $(".triggerColumn").trigger("click");
        }
    }


    function applyDataTable() {
        setRegisterTableSorter();
        $(".triggerColumn").trigger("click");
        $("#allUsersRegistration_wrapper .hiddenHeadOnRegTable").trigger("click");
    }

    function showSelectedView() {
        console.log('selectedView: ' + selectedView);
        if (selectedView === "Registration") {
            showRegistrationView();
        } else if (selectedView === "Detail") {
            showDetailUserView();
        }
    }

    $(function () {

        $(".hideInitialDetailInfo").hide();

        if ("<?php echo $usersView_selected; ?>" == "Basic" || "<?php echo $usersView_selected; ?>" == "") {
            $("#viewBasicListTable").trigger("click");
        } else if ("<?php echo $usersView_selected; ?>" == "Detail") {
            //$("#viewUserTable").trigger("click");
        } else if ("<?php echo $usersView_selected; ?>" == "Registration") {
            //$("#viewUserRegistrationTable").trigger("click");
        }

        $("#module").html("> View Users");
        $("#endUserId").html("");

        //limit();
        //checkUserLength();

    });

    /*
		function disableGWModifyForm() {
			$("#gwModsLabel").hide();
			$("#departmentDropdown").css("visibility", "hidden");
			$("#selectAllChk").parent('label').parent('div').hide();
			$(".checkUserListBox").attr("checked", false);
			$("#selectAllChk").attr("checked", false);
			$("#gwMods").val('gwModsNone');
			$("#divCFA").hide();
			$("#divCFNR").hide();
			$("#divCFS").hide();
			$("#divAuth").hide();
			$("#divSwPck").hide();
			$("#divActivationNumber").hide();
			$("#groupWideModifyUser").hide();
		}
	*/

    $('#viewBasicListTable').click(function () {
	$('#viewBasicListTable').css('background-image','url(images/NewIcon/basic_list_over.png)');
		$('#viewBasicListTable').css('color','#ffb200');
		
		$("#viewUserTable").css('background-image','url(images/NewIcon/user_list_rest.png)');
		$('#viewUserTable').css('color','#6ea0dc');

		$('#viewUserRegistrationTable').css('background-image','url(images/NewIcon/registration_details_rest.png)');
		$('#viewUserRegistrationTable').css('color','#6ea0dc');
        loadBasicUsersTable = true;
        selectedView = "Basic";
        $("#loading2").hide();
        /* Loading Bars. */
        if (showBasicLoader) {
            $("#loading_Basic").show();
        } else {
            $("#loading_Basic").hide();
        }
        /* End Loading Bars. */
        $("#usersBasicListTable").show();
        $("#usersTable").hide();
        $(".viewDetail1").hide();

        $("#userFiltersForm").hide();
        $(".viewDetailRegistration").hide();
        
        //Download CSV
        //$("#departmentDropdown").attr('action', 'users/printCSV.php');
        $("#departmentDropdown_Basic").css("visibility", "visible");
        $("#selectAllChk_Basic").parent('label').parent('div').show();
        if ($('input.selectAl_Chk_Basic').is(':checked') || $('input.checkUserListBox_Basic').is(':checked')) {

            if (deleteUserPermission == "1") {
                $(".deleteUsers_Basic").show();
            }
            if (expressSheetUserPermission == "1") {
                $(".expressSheetUsersBasic").show();
            }
        }
        
        if (!basicUsersTableLoaded) {
            basicViewUsesList();
            quickFiltersIsApplied = false;
            $("#usersBanner").html("Users List / Group-wide Users Modify");
        } else {
            if (quickFiltersIsApplied) {
                //$("#usersBanner").html("Users List / Group-wide Users Modify<sup class='filtersApplied'>(Quick Filters Applied)</sup>");
                $("#usersBanner").html("<div class='filterMainHeadDiv'>Users List / Group-wide Users Modify </div><div class='filterApplyHead'><img src='/Express/images/icons/filter_icon_lrg.png' /><span>Quick Filters Applied<span></div>");
             }
            if(basicUserListDataTable) {
                basicUserListTableSorter(basicUserListCount);
            }
        }

        disableGWModifyFormOnDetail();
    });

    $('#viewUserTable').click(function () { 
	$("#viewUserTable").css('background-image','url(images/NewIcon/user_list_rollover.png)');
		$('#viewUserTable').css('color','#ffb200');

		$('#viewUserRegistrationTable').css('background-image','url(images/NewIcon/registration_details_rest.png)');
		$('#viewUserRegistrationTable').css('color','#6ea0dc');
		
		$('#viewBasicListTable').css('background-image','url(images/NewIcon/basic_list_rest.png)');
		$('#viewBasicListTable').css('color','#6ea0dc');
		
        $("#usersBasicListTable").hide();
        selectedView = "Detail";
        //console.log(detailedUsersTableLoaded);
        if (!detailedUsersTableLoaded) {
            limit();
        } else {
            showDetailUserView();
        }

        disableGWModifyFormOnBasic();
        recursiveDataTableDetail();
    });

    $('#viewUserRegistrationTable').click(function () {
$('#viewUserRegistrationTable').css('background-image','url(images/NewIcon/registration_details_rollover.png)');
		$('#viewUserRegistrationTable').css('color','#ffb200');

		$("#viewUserTable").css('background-image','url(images/NewIcon/user_list_rest.png)');                        
		$('#viewUserTable').css('color','#6ea0dc');
		
		$('#viewBasicListTable').css('background-image','url(images/NewIcon/basic_list_rest.png)');
		$('#viewBasicListTable').css('color','#6ea0dc');
        //loadDetailedUsersTable = true;
        $("#usersBasicListTable").hide();
        selectedView = "Registration";
        if (!detailedUsersTableLoaded) {
            limit();
        } else {
            showRegistrationView();
        }
        //setRegisterTableSorter();
        //setSelectedView(selectedView);

    });

    function showRegistrationView() {
        //$("#usersBasicListTable").hide();
        $(".hideInitialDetailInfo").hide();
        $(".viewDetail1").hide();
        $("#usersTableDiv").hide();
        $("#userFiltersForm").hide();
        $(".deleteUsers").hide();
        $(".expressSheetUsers").hide();
        $("#filterUsersValuesRow").hide();

        //$("#divActivationNumber").hide();
        $(".viewDetailRegistration").show();
        $("#gwModsLabel").hide();
        $("#departmentDropdown").attr('action', 'users/printCSVRegistration.php');

        $("#departmentDropdown").css("visibility", "hidden");
        $("#selectAllChk").parent('label').parent('div').hide();
        $(".checkUserListBox").attr("checked", false);
        $("#selectAllChk").attr("checked", false);
        $("#gwMods").val('gwModsNone');
        $("#divCFA").hide();
        $("#divCFNR").hide();
        $("#divCFS").hide();
        $("#divAuth").hide();
        $("#divSwPck").hide();
        $("#divActivationNumber").hide();
        $("#groupWideModifyUser").hide();

        $("#loading_Basic").hide();
        if (showDetailLoader) {
            $("#loading2").show();
        } else {
            $("#loading2").hide();
        }
        $("#usersTable").show();

        setRegisterTableSorter();

        disableGWModifyFormOnBasic();
    }

    function showDetailUserView() {       
        
        $(".hideInitialDetailInfo").show();
        //loadDetailedUsersTable = true;
        $(".hideInitialDetailInfo").show();
        $(".viewDetail1").show();
        $("#usersTableDiv").show();
        $("#userFiltersForm").show();
        $(".viewDetailRegistration").hide();
        $("#filterUsersValuesRow").show();

        $("#departmentDropdown").attr('action', 'users/printCSV.php');
        $("#departmentDropdown").css("visibility", "visible");
        $("#selectAllChk").parent('label').parent('div').show();
        if ($('input.selectAl_Chk').is(':checked') || $('input.checkUserListBox').is(':checked')) {

            if (deleteUserPermission == "1") {
                $(".deleteUsers").show();
            }
            if (expressSheetUserPermission == "1") {
                $(".expressSheetUsers").show();
            }
        }

        //checkUserLength();


        $("#loading_Basic").hide();
        if (showDetailLoader) {
            $("#loading2").show();
        } else {
            $("#loading2").hide();
        }
        $("#usersBasicListTable").hide();
        $("#usersTable").show();
        $("#usersTable1").show();
        if (usersDetailedListTable_DT) {
            usersDetailedListTable_DT.draw(false);
        }
    }

    function disableGWModifyFormOnDetail() {
        $("#gwModsLabel").hide();
        $("#departmentDropdown").css("visibility", "hidden");
        $("#selectAllChk").parent('label').parent('div').hide();
        $(".checkUserListBox").attr("checked", false);
        $("#selectAllChk").attr("checked", false).change();
        $("#gwMods").val('gwModsNone');
        $("#divCFA").hide();
        $("#divCFNR").hide();
        $("#divCFS").hide();
        $("#divAuth").hide();
        $("#divSwPck").hide();
        $("#divActivationNumber").hide();
        $("#groupWideModifyUser").hide();
    }

    function disableGWModifyFormOnBasic() {
        $("#gwModsLabelBasic").hide();
        $("#departmentDropdown_Basic").css("visibility", "hidden");
        $("#selectAllChk_Basic").parent('label').parent('div').hide();
        $(".checkUserListBox_Basic").attr("checked", false);
        if($("#gwMods_Basic").length) {
            $("#gwMods_Basic").val('gwModsNone');
        }
        $("#divCFA_Basic").hide();
        $("#divCFNR_Basic").hide();
        $("#divCFS_Basic").hide();
        $("#divAuth_Basic").hide();
        $("#divSwPck_Basic").hide();
        $("#divActivationNumber_Basic").hide();
        $("#groupWideModifyUser_Basic").hide();

        $('#selectAllChk_Basic').prop('checked', false);
        selectedUsers_Basic = {};
        toggleAllBasicUsers();
    }

    function showLoader() {
        if (showDetailLoader) {
            $("#loading2").show();
        } else {
            $("#loading2").hide();
        }

        if (showBasicLoader) {
            $("#loading_Basic").show();
        } else {
            $("#loading_Basic").hide();
        }
    }

    $(window).resize(function() {
        if(basicUserListDataTable) {
            basicUserListTableSorter(basicUserListCount);
        }
        if(allUsersRegistration_DT) {
            setRegisterTableSorter();
        }
    });


    $(window).resize(function() {
        if(basicUserListDataTable) {
            basicUserListTableSorter(basicUserListCount);
        }
        if(allUsersRegistration_DT) {
            setRegisterTableSorter();
        }
    });
</script>

<h2 class='userListTxt' id='usersBanner'>
    Users List / Group-wide Users Modify 
</h2>
<div style="height: 30px; clear: both;"></div>
<div class="icons-div">
<ul style="" class="feature-list">
		<li class="changeOnMouseHover">
            <div class="mhover1">
            	<input type="hidden" name="viewUser" value="" checked data-tabletype="viewBasicList" />
               <div id="viewBasicListTable">Basic List</div>                 
            </div>
        </li>
        <li class="changeOnMouseHover">
            <div class="mhover">
            	<input type="hidden" name="viewUser" value="" checked data-tabletype="viewDetail" />
               <div id="viewUserTable">Detailed List</div>                 
            </div>
        </li>
        <li>
            <div class="mhover2">
             	<input type="hidden" name="viewUser"  value="" data-tabletype="viewRegistrationDetail" />
              	<div id="viewUserRegistrationTable">Registration Details</div>
            </div>
        </li>	
</ul>
</div>


<div style="display: <?php echo $showDetailedTable; ?>" class="loading loadingRemove" id="loading2"><img src="/Express/images/ajax-loader.gif"></div>

<div style="display: <?php echo $showBasicTable; ?>" id="usersBasicListTable">
	<?php require_once("/var/www/html/Express/users/usersBasicList/index.php"); ?>
	<?php require_once("/var/www/html/Express/users/usersBasicList/usersBasicList-js.php"); ?>
</div>
<div style="display: <?php echo $showDetailedTable; ?>" id="usersTable"></div> 
<div id="userData"></div>
