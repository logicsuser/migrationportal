<?php
/**
 * Created by Sollogics.
 * Date: 9/10/2017
 */

    require_once("/var/www/html/Express/config.php");
    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();
    
    require_once("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
    require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
    
    $dn = new Dns();
    $activateIncrement = 0;
    
    ////echo "<pre>"; print_r($_POST);
    $selectedUsers = explode("+-+-+", $_POST["userList"]);
    //echo "<pre>"; print_r($selectedUsers); die;
    
    foreach($selectedUsers as $key=>$val){
    	
    	$explodeUserData = explode("*", $val);		
    	//get user id
    	$userId = $explodeUserData[0];
    	$userPhoneNumber = "";
    	$phoneNumberStatus = "";
    	
    	if($val == true){
    		$numberActivateResponse = $dn->getUserDNActivateListRequest($userId);
    		
    		if(empty($numberActivateResponse['Error'])){
    			
    			if(!empty($numberActivateResponse['Success'][0]['phoneNumber'])){
    				$userPhoneNumber = $numberActivateResponse['Success'][0]['phoneNumber'];
    			}
    			if(!empty($numberActivateResponse['Success'][0]['status'])){
    				$phoneNumberStatus = $numberActivateResponse['Success'][0]['status'];
    			}
    			
    			$dnPostArray = array(
    					"groupId" => $_SESSION['groupId'],
    					"phoneNumber" => array($userPhoneNumber)
    			);

    			if(!empty($_POST['action']) && $_POST['action'] == "numberActivate"){
    				$dnsActiveResponse = $dn->activateDNRequest($dnPostArray);
    				$process = "activated";
    			}elseif(!empty($_POST['action']) && $_POST['action'] == "numberDeactivate"){
    				$dnsActiveResponse = $dn->deActivateDNRequest($dnPostArray);
    				$process = "deactivated";
    			}
    			if(empty($dnsActiveResponse['Error'])){
    				$activateIncrement++;
    			}
    			
    		}
    		
    	}
    }
    echo $activateIncrement. " numbers of users has been ".$process;
    //die;
?>
