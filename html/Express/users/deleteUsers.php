    <?php
    //error_reporting(E_ALL);
    //ini_set('display_errors', '1');

    function getNumberOfAssignedPorts($device) {
        global $ociVersion, $sessionid, $client;

        $ociCmd = "GroupAccessDeviceGetRequest18sp1";
        $xmlinput = xmlHeader($sessionid, $ociCmd);
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $device . "</deviceName>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);        
        return strval($xml->command->numberOfAssignedPorts);
    }


    /**
     * @param $deviceType
     * @return bool
     */
    function deleteDeviceOnUserRemoval($deviceType) {
       global $db, $deleteAnalogUserDevice;

	$sipGatewayLookup = new DBLookup($db, "systemDevices", "deviceType", "phoneType");
	$deviceIsAnalog = $sipGatewayLookup->get($deviceType) == "Analog";

	// short-circuit for Audio-Codes
	$deviceIsAudioCodes = substr($deviceType, 0, strlen("AudioCodes-")) == "AudioCodes-";

	if ($deviceIsAnalog || $deviceIsAudioCodes) {
		return $deleteAnalogUserDevice == "true";
	}

	// device is not for analog user
	return true;
    }

    function getDeviceDetailsBasedOnUserId($userId){
        global $ociVersion, $sessionid, $client;
        
        if ($ociVersion == "17"){
		$xmlinput = xmlHeader($sessionid, "UserGetRequest17sp4");
	}else if ($ociVersion == "20"){
		$xmlinput = xmlHeader($sessionid, "UserGetRequest20");
	}else if ($ociVersion == "21"){
		$xmlinput = xmlHeader($sessionid, "UserGetRequest21sp1");
	}else if ($ociVersion == "22"){
		$xmlinput = xmlHeader($sessionid, "UserGetRequest21sp1");
	}else{
		$xmlinput = xmlHeader($sessionid, "UserGetRequest19");
	}
        
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);	
        
	$deviceName = isset($xml->command->accessDeviceEndpoint->accessDevice->deviceName) ? strval($xml->command->accessDeviceEndpoint->accessDevice->deviceName) : "";
        return $deviceName;
    }
    
    function getDeviceTypeByDeviceName($deviceName){
        global $ociVersion, $sessionid, $client;
        
        $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetRequest18sp1");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$deviceType = strval($xml->command->deviceType);        
        return $deviceType;
    }

    
    require_once("/var/www/lib/broadsoft/login.php");
    require_once("getAllUsers.php");

    checkLogin();

    include("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");
    include("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
    require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailOperations.php"); //Added 12 May 2018
    require_once("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php"); //Added 22 may 2018
    
    require_once ("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
    require_once("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
    $objCustomTagMgMt = new VdmOperations();
    require_once ("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
    $sipSoftPhoneLookup = new DBLookup ( $db, "systemDevices", "deviceType", "softPhoneType" );
    require_once("/var/www/lib/broadsoft/adminPortal/sca/SCAOperations.php");
    $scaOpsObj = new SCAOperations();
    require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
    $devObj = new DeviceOperations();

    // Delete users and devices from BroadWorks based on User Delete List
    //-------------------------------------------------------------------
    $deleteArray = array();
    server_fail_over_debuggin_testing(); /* for fail Over testing. */
    
    if ($_POST["action"] == "deleteUsers")
    {
        $usersToDelete = explode("+-+-+", $_POST["userList"]);
        for ($i = 0; $i < count($usersToDelete); $i++)
        {
            $userId = "";
            $deviceName = "";

            // Separator "*-*-*" is used to separate between posted user ID and device name
            // Note that not all users IDs may have device name associated with them
            $userAndDevice = explode("*-*-*", $usersToDelete[$i]);
            if (isset($userAndDevice[0])) {
                $userId = $userAndDevice[0];
            }
            if (strlen($userId) < 1) {continue;}

            $userObject  =   new UserOperations();
            $userDeviceList = $userObject->getDeviceListByUserId($userId);
            $softDeviceArray = array();
            if(isset($userDeviceList["Success"]["deviceList"])){
                foreach ($userDeviceList["Success"]["deviceList"] as $dKey => $dValue){
                    $tagResp = $objCustomTagMgMt->getDeviceCustomTags($_SESSION["sp"], $_SESSION["groupId"], $dValue["deviceName"]);
                    $softDeviceArray[$dValue["deviceName"]]["deviceName"] = $dValue["deviceName"];
                    $softDeviceArray[$dValue["deviceName"]]["deviceType"] = $dValue["deviceType"];
                    $softDeviceArray[$dValue["deviceName"]]["linePort"] = $dValue["linePort"];
                    $softDeviceArray[$dValue["deviceName"]]["cpAccount"] = $tagResp["Success"]["%CPUsername%"][0];
                }
            }
            //print_r($softDeviceArray);
            if(count($softDeviceArray) > 0){
                foreach ($softDeviceArray as $genKey => $genVal){

                    if ($sipSoftPhoneLookup->get (trim($genVal["deviceType"])) == "counterPath" || $sipSoftPhoneLookup->get (trim($genVal["deviceType"])) == "generic") {

                    }else{
                        unset($softDeviceArray[$genKey]);
                    }
                }
            }
            //print_r($softDeviceArray);
            if(count($softDeviceArray) > 0){
                foreach ($softDeviceArray as $softKey => $softValue){
                    if($softValue["cpAccount"] != ""){
                        require_once("/var/www/lib/broadsoft/adminPortal/counterPathStretto/CPSOperations.php");
                        $cpsobj = new CPSOperations();
                        $arrayToProcess["cpsArray"]["userName"] = trim($softValue["cpAccount"]);
                        $cpsDelResp = $cpsobj->counterPathAccountDelete($arrayToProcess);
                        //echo "rrrrrrrr";print_r($cpsDelResp);
                    }
                    $scaUnAssignResp = $scaOpsObj->deleteDevicesFromSCA($userId, $softKey, $softValue["linePort"]);
                    //print_r($scaUnAssignResp);
                    $devDelResp = $devObj->getDeleteDevice($softKey, $_SESSION["sp"], $_SESSION["groupId"]);
                    //print_r($devDelResp);
                }
            }
            
            // Deleting Voice Mailbox account from SurgeMail Server
            $deleteArray[$i] = $userId;
            // Check for Voice Messaging User service assignment
            $userServices = getUserAssignedUserServices($userId);
            $sugeObj      = new SurgeMailOperations();   //Added @ 12 May 2018

            if ($policiesSupportDeleteVMOnUserDel == "true" && in_array("Voice Messaging User", $userServices)) {
//                 $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetAdvancedVoiceManagementRequest14sp3");
//                 $xmlinput .= "<userId>" . $userId . "</userId>";
//                 $xmlinput .= xmlFooter();
//                 $response = $client->processOCIMessage(array("in0" => $xmlinput));
//                 $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

                if (readError($xml) != "") {break;}

                $groupMailServerUserId = isset($xml->command->groupMailServerUserId) ? strval($xml->command->groupMailServerUserId) : "";
                if ($groupMailServerUserId != "") {
                    $mailboxPassword = setEmailPassword();

                    $surgemailPost = array ("cmd" => "cmd_user_login",
                        "lcmd" => "user_delete",
                        "show" => "simple_msg.xml",
                        "username" => $surgemailUsername,
                        "password" => $surgemailPassword,
                        "lusername" => $groupMailServerUserId,
                        "lpassword" => $mailboxPassword,
                        "uid" => isset($userid) ? $userid : "");
                    //$result = http_post_fields($surgemailURL, $surgemailPost);
                    $srgeRsltDelUsr = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPost); //Added @ 12 May 2018
                }
            }

            //Retrieve device type and name before deletion
            $deviceName =  getDeviceDetailsBasedOnUserId($userId);
            $deviceType =  getDeviceTypeByDeviceName($deviceName);
            
            
            // Deleting User in back-end
            $xmlinput = xmlHeader($sessionid, "UserDeleteRequest");
            $xmlinput .= "<userId>" . $userId . "</userId>";
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING); 
            
            //Update change log on users delete
            //Code added @ 30 Jan 2019 regarding delete user change log was not working
            $objChngLogUtil  = new ChangeLogUtility($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
            $objChngLogUtil->changeLogDeleteUtility("Delete User", $userId, "userModChanges", "");
            //End code

            if (readError($xml) != "") {break;}

            // Some users may not have devices
            if (! isset($userAndDevice[1])) 
            {
                //continue;
            }

            // Deleting Device in back-end
            //$deviceName = $userAndDevice[1];
            //$deviceType = $userAndDevice[2];
            
            
            //echo "<br />User Id - $userId -- Device Name - $deviceName";
            //echo "<br />Device Type - ".$deviceType;
                        
            if (($deviceName != "") && (getNumberOfAssignedPorts($deviceName) == 0) && (deleteDeviceOnUserRemoval($deviceType)))
            {                
                $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceDeleteRequest");
                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
                $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
                $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
                $xmlinput .= xmlFooter();
                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                readError($xml);
            }           
 
                //Update change log on users delete
                /*$objChngLogUtil  = new ChangeLogUtility($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
                $objChngLogUtil->changeLogDeleteUtility("Delete User", $userId, "userModChanges", "");*/
        }
        
    }
    
    if(count($deleteArray) > 0){
    	require_once("/var/www/lib/broadsoft/adminPortal/sasOperation/sasOperation.php"); //Added 26 Oct 2018
    	
    	$fileName = "../../../SASTestingUser/SASTestUsers.csv";
    	$sasObject = new sasOperation();
    	$sasObject->deleteFromCSVFile($deleteArray, $fileName);
    }
?>
