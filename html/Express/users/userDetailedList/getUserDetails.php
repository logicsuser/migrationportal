<?php
/**
 * Created by Dipin Krishna.
 * Date: 10/10/18
 * Time: 2:53 PM
 */

require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/GroupLevelOperations.php");
require_once("/var/www/lib/broadsoft/adminPortal/dns/dns.php");

require_once '/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserDndOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserServiceOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserCallForwardingOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserBusyLampFieldServiceOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserDetailsOperation.php';
require_once("/var/www/html/Express/userMod/UserCustomTagManager.php");

require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserThirdPartyVoiceMailOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserVoiceMsgAdvanceVoiceMgmtServiceOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserVoiceMsgVoiceMgmtServiceOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserRemoteOfficeOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserPolycomPhoneServicesOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserBusyLampFieldServiceOperation.php';

require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/GroupDeviceCustomTagOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/GroupDeviceGetUserOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/GroupAccessDeviceGetOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserRegistrationListRequestOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserPolycomPhoneServicesOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/GroupAccessDeviceGetOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserEndpointDeviceNameOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserServiceGetAssignmentListRequestOperation.php';


if (isset($_POST['userIds'])) {

	$sp = $_SESSION['sp'];
	$groupId = $_SESSION['groupId'];

	$usersDetailsArray = array();
	foreach ($_POST['userIds'] as $userId) {

		$userDetailsOperation = new UserDetailsOperation($sp, $groupId);
		$userDetails = $userDetailsOperation->getSingleUserDetails($userId);
		if (isset($userDetails) && isset($userDetails[0])) {
			$userDetails = $userDetails[0];
			$userDetails['userID'] = $userId;
		} else {
			$userDetails = array('userID' => $userId);
		}


		$usersDetailsArray[] = $userDetails;

	}

	//print_r($response_array);

	require_once("/var/www/lib/broadsoft/adminPortal/getUserRegisterationCdrReport.php");
	$urObj = new GetUserRegistrationCdrReport ();
	$userArray = $urObj->getUserData($usersDetailsArray);
	$sno = 1;

	//print_r($userArray);

	$fileName = "/var/www/SASTestingUser/SASTestUsers.csv";
	$registrationArray = array();

	foreach ($userArray as $key => $val) {

		if (file_exists($fileName)) {
			$objcsv = new UserOperations ();
			$isExistInCSV = $objcsv->find_user_in_csv($fileName, $val ['userId']);
		} else {
			$isExistInCSV = "";
		}
		if ($isExistInCSV != "") {
			$registrationStatus = "SAS";
		} else {
			$registrationStatus = $val ['registerStatus'];
		}

		/*
		if(is_array($val ['deviceName']) && isset($val ['deviceName'][0])) {
			$val ['deviceName'] = $val ['deviceName'][0];
		}
		*/

		if ($registrationStatus) {
			$registrationArray[] = array_map('utf8_encode', array(
				$sno,
				$val ['deviceName'],
				$val ['deviceType'],
				$registrationStatus,
				$val ['publicIp'],
				$val ['expiration'],
				$val ['agentType']
			));
		}

		$sno++;
	}

	$response_array = array(
		'userDetails' => encode_items($usersDetailsArray),
		'registrationDetails' => $registrationArray
	);

	//print_r($registrationArray);

	echo json_encode($response_array);
}

function encode_items($array)
{
	foreach($array as $key => $value)
	{
		if(is_array($value))
		{
			$array[$key] = encode_items($value);
		}
		else
		{
			$array[$key] = utf8_encode($value);
		}
	}

	return $array;
}

?>

