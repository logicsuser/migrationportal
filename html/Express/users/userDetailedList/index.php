<?php
/**
 * Created by Dipin Krishna.
 * Date: 10/09/18
 * Time: 1:56 PM
 */

require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/GroupLevelOperations.php");
require_once("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
require_once("/var/www/html/Express/users/usersBasicList/GetUsersInGroup.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
$serviceInfo = new Services();

if (isset($_SESSION['detailedUsersList'])) {
	$users = $_SESSION['detailedUsersList'];
} else {
	$gUIG = new GetUsersInGroup();
	$gUIG->department = $_POST['department'];

	$userListInGroup = $gUIG->getAllUsersInGroup($searchCriteria);

	if (!empty($gUIG->error)) {
		$errorMsg = str_replace("%0D%0A", "", trim($gUIG->error));
		preg_match('/Please narrow your search, there are ([0-9]+) matching records/', $errorMsg, $output_array);
		if ($output_array && isset($output_array[1])) {
			$errorMsg = "There are " . $output_array[1] . " matching users - please narrow your search.";
		}
		echo "$errorMsg";
	} else {
		$users = $userListInGroup;
		//$_SESSION['detailedUsersList'] = $users;
	}
}

if (isset ($users)) {

	require_once("/var/www/lib/broadsoft/adminPortal/getAuthenticationPasswordRules.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getUserRegisterationCdrReport.php");
	require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");

	$_SESSION['usersView_selected'] = isset($_POST['selectedView']) ? $_POST['selectedView'] : "";

	$_SESSION["gwUsersMod"] = array(
		"activateCFA" => "Call Forwarding Always Active",
		"phSipFwdCFA" => "Number/SIP-URI to forward calls to",
		"ringReminderChk" => "Enable Ring Reminder play",
		"activateCFNR" => "Call Forwarding Not Reachable Active",
		"phSipFwdCFNR" => "Number/SIP-URI to forward calls to",
		"activateCFS" => "Call Forwarding Selective Active",
		"phSipFwdCFS" => "Number/SIP-URI to forward calls to",
		"ringReminderChkCFS" => "Enable Ring Reminder play",
		"authUserName" => "Authentication User Name",
		"authPassword1" => "Authentication Password",
		"authGeneratedPassword" => "Authentication Password",
		"srvPack" => "Service Pack",
		"assignSvcPack" => "Assign Service Pack",
		"usrServices" => "User Service",
		"assignUsrService" => "Assign User Service",
		"activateNumber" => "Activate/Deactivate Number",
		//gMods VoiceMessage Service
		"voiceMessaging" => "Assign Voice Messaging Service",
		"isActive" => "Voice Messaging/Support",
		"alwaysRedirectToVoiceMail" => "Send All Calls to Voice Mail",
		"busyRedirectToVoiceMail" => "Send Busy Calls to Voice Mail",
		"noAnswerRedirectToVoiceMail" => "Send Unanswered Calls to Voice Mail",
		"serverSelection" => "Third-Party Voice Mail Server",
		"userServer" => "User Specific Mail Server",
		"mailboxIdType" => "Mailbox ID on Third-Party Voice Mail Platform",
		"mailboxURL" => "SIP-URI",
		"noAnswerNumberOfRings" => "Number of rings before greeting",
		"usePhoneMessageWaitingIndicator" => "Use Phone Message Waiting Indicator",
		"sendCarbonCopyVoiceMessage" => "Email Carbon Copy of Message",
		"voiceMessageCarbonCopyEmailAddress" => "Carbon Copy Email Address",
		"transferOnZeroToPhoneNumber" => "Transfer on Zero to Phone Number",
		"transferPhoneNumber" => "Transfer Phone Number",
		"mailBoxLimit" => "MailBox Limit",
	    "callingLineIDBlockingInput" => "Block Calling Line ID"

	);

	require_once("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");
	?>

	<!-- User Filters -->
	<link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v11">
	<script src="/Express/js/bootstrap.min.js"></script>
	<script src="/Express/js/jquery-1.17.0.validate.js"></script>
	<script src="/Express/js/jquery.loadTemplate.min.js"></script>
        
        <style>
            input .paginate_jump_input{
                width: 60px !important;
                text-align: center;
            }
            
            .checkBoxLikeDisable{
                opacity: 0.2;
                cursor: not-allowed !important;
            }
            
            .checkBoxLikeEnable{
                opacity: 1;
                cursor: pointer !important;
            }
	</style>
        
        <style>
		.unRegisterUsr td {
			background-color: #F5D0C3 !important;
		}

		input#selectAllChk[disabled],
		input[type="button"][disabled], button[disabled] {
			pointer-events: visible;
			cursor: not-allowed;
		}

		#usersDetailedListTable thead tr th:first-child {
			border-left: 2px solid #DA9873;
                        background-image: none !important;cursor: default;
		}
                
                .DTFC_LeftBodyWrapper table tbody tr td {
                    border-right: 0px solid #d8dada !important;
                     
                }

		table#usersDetailedListTable tbody tr td {
			background-color: #FFF;
			/*border-right: 2px solid #d8dada;
			border-top: 2px solid #aa1111 !important;
			border-bottom: 2px solid #aa1111 !important;*/
		}

		table#usersDetailedListTable tbody tr td:first-child {
			border-left: 2px solid #d8dada;
		}
                
                table.dataTable.no-footer thead tr .sorting_asc, table.tablesorter thead tr .headerSortDown{border-right: 2px solid #d8dada !important;}
        table.dataTable.no-footer thead tr .sorting_desc, table.tablesorter thead tr .headerSortUp{border-right: 2px solid #d8dada !important;}

		#usersDetailedListTable_wrapper .DTFC_LeftBodyWrapper tr td {
			/*border-right: 2px solid #d8dada;	  		
			border-bottom: 0px solid #aa1111 !important;
                        border-top: 0 px solid #aa1111 !important;  */ 
		}

		#usersDetailedListTable_wrapper .dataTables_scrollHead, .DTFC_LeftHeadWrapper {
			 z-index: 1; 
		}

		/* #usersDetailedListTable_wrapper .DTFC_LeftBodyWrapper,
		#usersDetailedListTable_wrapper .dataTables_scrollBody {
			margin-top: -20px;
			overflow-y: hidden !important;
		} */
                
                /*code added by sollogics developer */
               #usersDetailedListTable_wrapper .DTFC_LeftBodyWrapper,
		#usersDetailedListTable_wrapper .dataTables_scrollBody {
			/* margin-top: -20px; */
                        margin-top: -10px;
			  overflow-y: auto !important;  
		}   
             
                #usersDetailedListTable_wrapper .dataTables_scrollBody{max-height: 426px !important ;overflow:scroll !important ;}
                /*end code sollogics developer */
		table#usersDetailedListTable tbody tr:hover {
			background-color: #dcfac9 !important;
                        border-right:     2px solid #DA9873;
			cursor: pointer;
		}

		#usersDetailedListTable tbody tr {
			background-color: #fff !important;
		}
                
                
                
                .usersDetailedListTable_top_bar .column1{
                    float: left;
                    width: 25%;
                    height: 100px;
                }

                .usersDetailedListTable_top_bar .column4 .dataTables_length{
                    float: right;
                    /* width: 100px; */
                    padding-left:20px; 
                    padding-top:5px;
                }
                .usersDetailedListTable_top_bar .column4 .dataTables_length select{
                    width: 65px !important;
                }
                .usersTableButton{
                    float: left;
                }
                
                .paginate_jump_input{
                    width: 60px !important;
                }
                
                table.dataTable tbody td {
                    padding: 0px 10px !important;
                }

                #allUsersRegistration_wrapper .allUsersRegistration thead tr th:first-child {
                    display: none;
                } 
                
                table#allUsersRegistration tbody tr td:first-child {
                    display: none !important;
                    
                }
                
                 table#allUsersRegistration tbody tr td:nth-child(2) {
                    border-left: 2px solid #d8dada;
                    border-bottom: 2px solid #d8dada;
                    height: 22px;
                }
                
                table#allUsersRegistration tbody tr td{ padding: 2px 10px 0px 8px !important;}
                
                a.paginate_button {
            background-color: #5d81ac !important;
            color: #fff !important;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: #fff !important;
            border: 1px solid #FFB200 !important;
            background-color: #FFB200 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #FFB200), color-stop(100%, none)) !important;
            background: -webkit-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: -moz-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: -ms-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: -o-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: linear-gradient(to bottom, #FFB200 0%, #FFB200 100%) !important;
        }
         
 
        .dataTables_wrapper .dataTables_paginate .paginate_button:active {
            background: #ffb200 ! important;
            border-bottom: 1px solid #ffb200 !important;  
            color: #fff !important;
        }
        

        a.paginate_button.paginate_number.active {
            background: #ffb200 ! important;
            border-bottom: 1px solid #ffb200 !important; 
            color: #fff !important;
        }
        
        .dataTables_wrapper .dataTables_paginate .paginate_button{
            color: #fff !important;
        }
        
        .paginate_jump_text{
            color: #6ea0dc !important;
            font-weight: 700;
        }
        
        .dataTables_wrapper .dataTables_paginate {
			float: right;
			text-align: right;
			padding-top: 0.25em;
                            color: #6ea0dc !important;
		}
        </style>

	<div class="">
		<?php
		$showFiltersByDefault = true;
		$collapseFiltersByDefault = true;
		$need_user_table = false;
		//include_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/userFilters/index.php");
		include_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/userFilters/sheetForUserFilter.php");
		?>
	</div>

	<!-- Drop-down menu of group-wide user actions -->
        <!-- 
	<div class="hideInitialDetailInfo hideInitialDetailInfo_selectAllDiv averistar-bs3">
		<div id="selectAllDiv">
			<label>
				<input type="checkbox"
				       id="selectAllChk"
				       class="selectAl_Chk disabled"
				       disabled
				       style="zoom: 130%; font-size:12px; margin-left: 1px; display: <?php if (isset($_SESSION["permissions"]["groupWideUserModify"])) {
					       echo($_SESSION["permissions"]["groupWideUserModify"] == "0" ? "none" : "inline-block");
				       } ?>"/>
			</label>
			<label><b style="margin-left:5px;">Select All</b></label>
			<a data-toggle="tooltip"
			   title="You may also select Users by clicking checkboxes in the table, then selecting Group-Wide Modify actions from pull-down menu."
			   id="ui-id-51"
			   class="red-tooltip">
        	<span class="averistar-bs3">
        		<i class="glyphicon glyphicon-info-sign"></i>
        	</span>
			</a>
		</div>
	</div>
	<div class="hideInitialDetailInfo hideInitialDetailInfo_groupModifyDiv">

		<?php if ($_SESSION["permissions"]["groupWideUserModify"] == "1") { ?>
			<label style="margin-left: 20px;" name="gwModsLabel"
			       id="gwModsLabel">
				<div>Group-wide Modifications</div>
				<select style="font-size: 12px" name="gwMods" id="gwMods"
				        onchange="onMenuSelectionChange(this)">
					<option value="gwModsNone">Select one:</option>
					<option value="gwModsCFA">Modify 'Call Forwarding Always' services</option>
					<option value="gwModsCFNR">Modify 'Call Forwarding Not Reachable'
						services
					</option>
					<option value="gwModsCFS">Modify 'Call Forwarding Selective' services</option>
					<option value="gwModsAuth">Reset SIP Authentication</option>
					<option value="gwModsSwPck">Services/Service Packs Management</option>
					<option value="gwModsDnActivate">Activate/Deactivate</option>
					<?php if ($_SESSION["permissions"]["voiceManagement"] == "1") { ?>
						<option value="gwModsVM">Modify 'Voice Management' Service</option>
					<?php } ?>
				</select>
			</label>
		<?php } ?>
	</div>
        -->
        
	<!-- Call Forwarding Always Form -->
	<!-- Note: Visibility of this form is initially switched off -->
	<div class="row twoColWidth" style="display: none;" id="divCFA">
            <div class="activeLabelDiv"><label class="activeLabel">Call Forwarding Always</label></div>
        <div class="col-md-12 divCFAforStyle">
        <form action="" name="CFA" id="CFA">
                        <!-- Form Title -->
                        <!-- Service Activation -->
            <div style="" class="col-md-12">
                    <div class="form-group">
                        <label for="activateCFATrue" class="labelText">Activate for all selected users:</label>
                        <input type="radio" name="activateCFA" id="activateCFATrue" value="true" onchange="evalActivation(this)"><label class="labelText" for="activateCFATrue"><span></span>On</label>
                        <input type="radio" name="activateCFA" id="activateCFAFalse" value="false" checked onchange="evalActivation(this)"><label class="labelText" for="activateCFAFalse"><span></span>Off</label> 
                        </div>
                        </div>

                        <!-- Forwarding Number  -->
            <div style="" class="col-md-12 CFSDivForInputs">
                <div class="form-group">
                <label for="phSipFwdCFA" class="labelText">All calls Forward to phone number / SIP-URI:</label><br>
                <input type="text" name="phSipFwdCFA" id="phSipFwdCFA" size="35" onchange="evalFwdNum(this)" oninput="evalFwdNum(this)">
                </div>


                        </div>

                        <!-- Play Ring Reminder -->

            <div style="" class="col-md-12">
                <div class="form-group">
                <input type="checkbox" name="ringReminderChk" id="ringReminderChk">
                <label style="" for="ringReminderChk" class="labelText"><span class="selectAllSpan"></span>Play Ring Reminder for forwarded calls</label>
                        </div>
                        </div>

                        <!-- Submit button -->
            <div class="col-md-12">
            <div class="form-group alignCenter groupWideModifyMarginBottom">
                <input style="" type="button" class="submitCFA subButton" name="submitCFA" id="submitCFA" value="Submit">
            </div>
            </div>
            </form>
            </div>
        </div>

	<!-- Call Forwarding Not Reachable Form -->
	<!-- Note: Visibility of this form is initially switched off -->
	<div class="row twoColWidth" style="display: none" id="divCFNR">
		<!-- Form Title -->
        <div class="activeLabelDiv"><label class="activeLabel">Call Forwarding Not Reachable</label></div>
        <div class="col-md-12 divCFAforStyle">
                <form action="" style="" name="CFNR" id="CFNR">
                                <!-- Service Activation -->
                        <div style="" class="col-md-12">
                        <div class="form-group">
                            <label for="activateCFNRTrue" class="labelText">Activate for all selected users:</label>
                            <input type="radio" name="activateCFNR" id="activateCFNRTrue" value="true" onchange="evalActivation(this)"><label class="labelText" for="activateCFNRTrue"><span></span>On</label>
                            <input type="radio" name="activateCFNR" id="activateCFNRFalse" value="false" checked onchange="evalActivation(this)"><label class="labelText" for="activateCFNRFalse"><span></span>Off</label>
                        </div>
                        </div>

                                <!-- Forwarding Number  -->
                        <div style="" class="col-md-12 CFSDivForInputs">
                        <div class="form-group">
                            <label for="phSipFwdCFNR" class="labelText">All calls Forward to phone number / SIP-URI:</label><br>
                            <input type="text" name="phSipFwdCFNR" id="phSipFwdCFNR" size="35" onchange="evalFwdNum(this)" oninput="evalFwdNum(this)">
                        </div>
                        </div>

                                <!-- Submit button -->
                        <div class="col-md-12">
                        <div class="form-group alignCenter groupWideModifyMarginBottom">
                            <input style="" type="button" class="submitCFNR subButton" name="submitCFNR" id="submitCFNR" value="Submit">
                        </div>
                        </div>
                    </form>
            </div>
         </div>


	<!-- Call Forwarding Selective Form -->
	<!-- Note: Visibility of this form is initially switched off -->
	<div class="row twoColWidth" style="display: none" id="divCFS">
		<!-- Form Title -->
    <div class="activeLabelDiv"><label class="activeLabel">Call Forwarding Selective</label></div>
    <div class="col-md-12 divCFAforStyle twoColWidth" style="margin-bottom: 10px;">
    <form action="" style="" name="CFS" id="CFS">

		<!-- Service Activation -->
        <div style="" class="col-md-12">
            <div class="form-group">
            <label for="activateCFSTrue" class="labelText">Activate for all selected users:</label>
            <input style="" type="radio" name="activateCFS" id="activateCFSTrue" value="true" onchange="evalActivationCFS(this)"> <label class="labelText" for="activateCFSTrue"><span></span>On</label>
            <input type="radio" name="activateCFS" id="activateCFSFalse" value="false" checked onchange="evalActivationCFS(this)"> <label class="labelText" for="activateCFSFalse"><span></span>Off</label>
            </div>
        </div>
		<!-- Forwarding Number  -->
        <div style="" class="col-md-12 CFSDivForInputs" id="CFSDivForInputs">
            <div class="form-group">
                <label for="phSipFwdCFS" class="labelText">Default Call Forward to phone number / SIP-URI:</label><span class="required">*</span> <br>
                <input type="text" name="phSipFwdCFS" class="input-control" id="phSipFwdCFS" size="35" onchange="evalCFSFwdNum(this)" oninput="evalCFSFwdNum(this)">
            </div>
        </div>

		<!-- Play Ring Reminder -->
        <div style="" class="col-md-12">
        <div class="form-group">
			<input type="checkbox" name="ringReminderChkCFS" id="ringReminderChkCFS">
            <label style="" class="labelText" for="ringReminderChkCFS"><span class="selectAllSpan"></span>Play Ring Reminder for forwarded calls</label>
		</div>
	
		</div>

		<!-- Forwarding Criteria Buttons -->
        <div class="col-md-12 alignBtn">
	
        <div class="form-group alignCenter">
	<input style="" type="button" class="newCriteriaCFS CFSNewBtn" name="newCriteriaCFS" id="newCriteriaCFS" value="Add selective forwarding criteria">
		<!--input style="font-size: 11px" type="button" class="deleteCriteriaCFS" name="deleteCriteriaCFS" id="deleteCriteriaCFS" value="Delete"-->
        	</div>
		</div>

		<!-- Criteria -->
        <div id="cfsCriteriaTable" class="col-md-12">
         <div class="form-group">
			<div style="" class="">--No Criteria--</div>
		</div>
        </div>

		<!-- Submit button -->
        <div class="col-md-12">
        <div class="form-group alignCenter groupWideModifyMarginBottom"><input style="" type="button" class="submitCFS subButton" name="submitCFS" id="submitCFS" value="Submit">
        </div>
        </div>
	</form>
</div>
</div>



	<!-- Authentication Password Form -->
	<!-- Visibility of this form is initially switched off -->
	<div class="row twoColWidth" style="display: none" id="divAuth">
        <!-- Form Title -->
        <div class="activeLabelDiv"><label style="" class="activeLabel">Authentication</label></div>   
        <div class="col-md-12 divCFAforStyle">     
        <form action="" name="authPasswords" id="authPasswords">


                    <!-- First typed authentication password -->
            <div class="col-md-12 CFSDivForInputs" id="typedPassword">
            <div class="form-group">

                    <label for="authPassword1" class="labelText" style="">Type new authentication password:</label><br>
                                    <input type="password" name="authPassword1" id="authPassword1" size="25" oninput="evalAuthPassword(this)">
                                    <label for="authPassword1" style="display: none" id="authPassword1CheckMark">&#10003</label>
                            </div>
                    </div>

                    <!-- Automatically generated password - initially invisible -->
            <div class="col-md-12 CFSDivForInputs" style="display: none" id="generatedPassword">
                <div class="form-group">
                    <label for="authGeneratedPassword" style="" class="labelText">New authentication password:</label><br>
                                            <input type="text" name="authGeneratedPassword" id="authGeneratedPassword" size="25" onchange="evalAuthGeneratedPassword(this)" oninput="evalAuthGeneratedPassword(this)">
                            </div>
                    </div>

                    <!-- Re-typed authentication password -->
            <div class="col-md-12 CFSDivForInputs" id="passwordRetype">
            <div class="form-group">
                    <label for="authPassword2" style="" class="labelText">Re-type new authentication password:</label><br>
                                    <input type="password" name="authPassword2" id="authPassword2" size="25" oninput="evalAuthPassword(this)">
                    <label for="authPassword2" style="display: none" class="" id="authPassword2CheckMark">&#10003</label>
                            </div>
                    </div>

                    <!-- Checkbox for automatic password generation -->
            <div style="" class="col-md-12">

                            <input type="checkbox" name="genAuthPassword" id="genAuthPassword" value="true" onclick="onGeneratePasswordSelect(this)">
                <label for="genAuthPassword" class="labelText"><span class="selectAllSpan"></span>Generate Password</label>
                            <!--input style="float: right; margin-right: 3px" type="button" class="submitAuthentication" name="submitAuthentication" id="submitAuthentication" value="Submit"-->
                    </div>
                            <!-- Submit button -->
                <div class="col-md-12 ">
                    <div class="form-group alignCenter groupWideModifyMarginBottom">
                                    <!--label for="submitAuthentication" style="margin-top: 3px">A&nbsp;</label-->
                        <input style="display: none;" type="button" class="submitAuthentication subButton noMarzin" name="submitAuthentication" id="submitAuthentication" value="Submit">
                    </div>
                </div>  
        </form>
        </div>
</div>


	<!-- Services / Service Packs form -->
	<!-- Note: Visibility of this form is initially switched off -->
	<div class="row twoColWidth" style="display: none" id="divSwPck">
    <div class="activeLabelDiv"><label class="activeLabel">Services / Service Packs Management</label></div>
    <div class="col-md-12 divCFAforStyle CFSDivForInputs">    
	<form action="" name="SwPck" id="SwPck" style="padding:25px 0;">
		<!-- Form Title -->
		<!-- Subtitle: Service Packs -->
        <div class="col-md-12">
        <div class="col-md-10 paddingZero">
        <div class="col-md-7 paddingZero">
        <div class="form-group">
        <label class="labelText">Service Packs</label>

		<!-- Service Pack selection -->
        <div class="dropdown-wrap">
            <select style="" name="srvPack" id="srvPack" onchange="onServicePackSelectionChange(this)">
				<option value="srvPackNone">Select one:</option>
                <?php
					if (isset ( $servicePacks )) {
						foreach ( $servicePacks as $key => $value ) {
							echo "<option value=\"" . $value . "\">" . $value . "</option>";
						}
					}
					?>
           </select>
 		</div>
        </div>
        </div>
          <div class="col-md-5 ">
        <div class="form-group" style="">
        <div>&nbsp;</div>
		<!-- Assignment radio buttons -->
            <label for="srvPack"><input type="radio" name="assignSvcPack" id="assignSvcPack1True" value="true"><label for="assignSvcPack1True" class="labelText"><span></span>Assign</label>
            </label>
            <input type="radio" name="assignSvcPack" id="assignSvcPackFalse" value="false" checked><label for="assignSvcPackFalse" class="labelText"><span></span>Unassign</label>
        </div>
        </div>
	
    </div>
     <div class="col-md-2"></div>
		</div>
       <div class="col-md-12">
		<!-- Subtitle: User Services -->
   <div class="col-md-10 paddingZero CFSDivForInputs">
        <div class="col-md-7 paddingZero">
        <div class="form-group">
        <label class="labelText">User Services</label>      

		<!-- User service selection -->
        <div class="dropdown-wrap">
            <select style="" name="usrServices" id="usrServices" onchange="onUserServiceSelectionChange(this)">
				<option value="usrServiceNone">Select one:</option>
                <?php
				if (isset ( $userServices )) {
					sort ( $userServices );
					foreach ( $userServices as $key => $value ) {
						echo "<option value=\"" . $value . "\">" . $value . "</option>";
					}
				}
				?>
            </select>
      </div>
		</div>
        </div>
		<!-- Assignment radio buttons -->
        <div class="col-md-5">
        <div class="form-group">
        <div>&nbsp;</div>
			<label for="srvPack"> 
	    <input type="radio" name="assignUsrService" id="assignUsrServiceTrue" value="true"><label for="assignUsrServiceTrue" class="labelText"><span></span>Assign</label>
            <input type="radio" name="assignUsrService" id="assignUsrServiceFalse" value="false" checked><label for="assignUsrServiceFalse" class="labelText"><span></span>Unassign</label>
			</label>
		</div>
        </div>
</div>
<div class="col-md-2"></div>
		
		</div>

		<!-- Submit button -->
        <div class="col-md-12">
        <div class="form-group alignCenter groupWideModifyMarginBottom">
	<input style="" type="button" class="submitSvcAssignment subButton" name="submitSvcAssignment" id="submitSvcAssignment" value="Submit">
        </div>
        </div>
    </form>
    </div>
</div>

        <!-- Number Activation / Deactivation form -->
	<!-- Note: Visibility of this form is initially switched off -->
	<div  class="row twoColWidth"  style="display: none;" id="divActivationNumber">
            <div class="activeLabelDiv"><label class="activeLabel">Activation / Deactivation Number</label></div>    
               <div class="col-md-12 divCFAforStyle">
               <form action="numberActivationAction" name="numberActivationFrm" id="numberActivationFrm">
                       <!-- Form Title -->

                       <!-- Service Activation -->
                       <div style="width: 100%" class="leftDesc userNumberAction">
                               <label for="activateCFATrue" class="labelText marginRightButton">Activate/Deactivate all selected
                                       users:</label> 
                           <input style="margin-left: 5px" type="radio" name="activateNumber" id="activateNumber" value="activate" checked> 
                           <label class="labelText" for="activateNumber" style="margin-left: 0;"><span></span>Activate</label> 
                           <input type="radio" name="activateNumber" id="deActivateNumber" value="deactivate"> 
                           <label class="labelText" for="deActivateNumber" style="margin-left: 0;"><span></span>Deactivate</label> 

                       </div>
                       <!-- <div style="margin-left: 5px; width: 30%" class="rightDesc">&nbsp;</div> -->
                       <div class="centerDesc"></div>
                       <!-- Submit button -->
                       <div class="col-md-12 alignCenter">
                       <div class="alignCenter form-group groupWideModifyMarginBottom">
                       <input type="button" class="submitCFA subButton " name="submitActiveNumber" id="submitActiveNumber" value="Submit">
                       </div>
                       </div>
               </form>
               </div>
       </div>


	<?php 
        if($_SESSION["permissions"]["voiceManagement"] == "1")
        { ?>

        <div style="display: none;" id="groupWideModifyUser" class="row twoColWidth">
                <div class="activeLabelDiv">
                <label class="activeLabel"> Modify Voice Management Service</label>
             </div>
             <div class="col-md-12 divCFAforStyle"> 
                <form action="" name="modsVoiceMessaging" id="modsVoiceMessaging">
                <!-- assign vm service -->
                        <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="labelText">Assign Voice Messaging Service</label>
                                        </div>
                        </div>

                                <div class="col-md-6" style="padding-left:0">
                                        <div class="form-group">
                                        <input type="radio" name="voiceMessaging" id="voiceMessagingYes" value="true" checked=""/>
                                        <label class="labelText" for="voiceMessagingYes" style="margin-left: 0;"><span></span>Yes</label> 
                                        <input type="radio" name="voiceMessaging" id="voiceMessagingNo" value="false" />
                                        <label class="labelText" for="voiceMessagingNo"><span></span>No</label> 
                                        </div>
                                </div>
                     </div>
         <!-- end assign vm service -->

         <!-- enable voice Message -->
                        <div id="dvPassport" style="display: none; text-align:center;">		
                        <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="labelText">Enable Voice Messaging</label>
                                        </div>
                                </div>

                                <div class="col-md-6" style="padding-left:0">
                                        <div class="form-group">
                                                <input type="radio" name="isActive" id="isActiveTrue" value="true" checked />
                                                <label class="labelText" for="isActiveTrue" style="margin-left: 0;"><span></span>On</label> 

                                                        <input  type="radio" name="isActive" id="isActiveFalse" value="false" />
                                                <label class="labelText" for="isActiveFalse"><span></span>Off</label> 
                                                <br/>

                                                <input type="checkbox" name="alwaysRedirectToVoiceMail" id="alwaysRedirectToVoiceMail" value="true">
                                                <label  for="alwaysRedirectToVoiceMail" class="labelText"><span class="selectAllSpan"></span>Send All Calls to Voice Mail</label>

                                                        <input style="width: 20px;" type="hidden" name="" value="false" /> <br/>

                                                        <input type="checkbox" name="busyRedirectToVoiceMail" id="busyRedirectToVoiceMail" value="true" checked="checked">

                                                        <label style="" for="busyRedirectToVoiceMail" class="labelText"><span class="selectAllSpan"></span>Send Busy Calls to Voice Mail</label><br/>

                                                        <input type="hidden" name="" value="false" />

                                                        <input type="checkbox" name="noAnswerRedirectToVoiceMail" id="noAnswerRedirectToVoiceMail" value="true" checked="checked">

                                                        <label style="" for="noAnswerRedirectToVoiceMail" class="labelText"><span class="selectAllSpan"></span>Send Unanswered Calls to Voice Mail</label><br/>
                                                        </div>
                                                </div>
         <!-- end voice Messaging -->     

         <!-- Use Unified Messaging -->  

                                <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="labelText">Use Unified Messaging</label>
                                        </div>
                                </div>

                                <div class="col-md-6" style="padding-left:0">
                                        <div class="form-group">
                                        <input type="hidden" name="processing" id="processingUnified" value="Unified Voice and Email Messaging"/>
                                        <label class="labelText" for=""><span></span>ON </label> 
                                </div>
                            </div>

        <!-- end use unified messaging -->  

         <!-- Additionally --> 
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="labelText">Additionally...</label>
                                        </div>	  
                                        </div>

                                        <div class="col-md-6" style="padding-left: 0;">
                                        <br/>
                                        <div class="form-group">
                                        <input type="checkbox" name="sendCarbonCopyVoiceMessage" id="sendCarbonCopyVoiceMessage" value="true">

                                        <label for="sendCarbonCopyVoiceMessage" class="labelText"><span class="selectAllSpan"></span>Email Message Carbon Copy</label><br/>

                                                        <label class="labelText">Carbon Copy Address</label><br/>
                                                        <input type="email" name="voiceMessageCarbonCopyEmailAddress" id="voiceMessageCarbonCopyEmailAddressVEA" size="32" maxlength="80" value="" onchange="evalFwdNum(this)" oninput="evalFwdNum(this)">
                                                                <div class="clr"></div>

                                                                <input  type="checkbox" name="transferOnZeroToPhoneNumber" id="transferOnZeroToPhoneNumber" value="true">

                                                                <label for="transferOnZeroToPhoneNumber" class="labelText"><span class="selectAllSpan"></span>Transfer on '0' to Phone Number</label><br/>


                                                                <label class="labelText">Transfer on '0' to DN</label>
                                                                <input type="text"  name="transferPhoneNumber" id="transferPhoneNumberDN" size="32" maxlength="80" value="" onchange="evalFwdNum(this)" oninput="evalFwdNum(this)" maxlength="12">

                                                        </div>
                                </div>
                 <div class="clr"></div> 
                                        <div class="col-md-6">
                                        <div class="form-group">
                                                <label class="labelText">Mailbox Limit </label>
                                        </div>
                                </div>
                                <div class="col-md-6" style="padding-left: 0;">
                                        <div class="form-group">
                                         <div class="dropdown-wrap oneColWidth">
                                                        <select name="mailBoxLimit" id="mailBoxLimit">
                                                                <option value="Use Group Default">Use Group Default</option>
                                                                <option value="10">10</option>
                                                                <option value="20">20</option>
                                                                <option value="30">30</option>
                                                                <option value="40">40</option>
                                                                <option value="50">50</option>
                                                                <option value="60">60</option>
                                                                <option value="70">70</option>
                                                                <option value="80">80</option>
                                                                <option value="90">90</option>
                                                                <option value="100">100</option>
                                                                <option value="200">200</option>
                                                                <option value="300">300</option>
                                                                <option value="400">400</option>
                                                                <option value="500">500</option>
                                                                <option value="600">600</option>
                                                                <option value="700">700</option>
                                                                <option value="800">800</option>
                                                                <option value="900">900</option>
                                        </select>
                                                                </div>
                                                        </div>
                            </div>

                        </div>  
                         </div><!-- end main row dvPasport -->
                          <div class="clr"></div>

                            <div class="row">
                                <div class="col-md-12">
                                                <div class="form-group groupWideModifyMarginBottom" style="text-align:center">
                                                        <input type="button" class="submitVM subButton" name="submitVoiceManagement" id="submitVoiceManagement" value="Submit">
                                                </div>
                                        </div>
                                </div>
                        </div>


                </form>	
                </div>
        <!-- </div> commented  --> 
        <?php }?>

        <div  class="row twoColWidth"  style="display: none;" id="callingLineIDBlocking">
         	<div class="activeLabelDiv"><label class="activeLabel">Block Calling Line ID</label></div>    
            <div class="col-md-12 divCFAforStyle">
            	<form action="callingLineIDBlockingAction" name="callingLineIDBlockingFrm" id="callingLineIDBlockingFrm">
           	     	<div style="width: 100%" class="leftDesc callingLineIDBlockingFrm">
            	    	<label for="" class="labelText marginRightButton">Block Calling Line ID,:</label> 
                        <input style="margin-left: 5px" type="radio" name="callingLineIDBlockingInput" id="blockCalling" value="block" checked> 
                    	<label class="labelText" for="blockCalling" style="margin-left: 0;"><span></span>Block</label> 
                    	<input type="radio" name="callingLineIDBlockingInput" id="unblockCalling" value="unblock"> 
                    	<label class="labelText" for="unblockCalling" style="margin-left: 0;"><span></span>Unblock</label> 
                	</div>
                	<div class="centerDesc"></div>
             		<div class="col-md-12 alignCenter">
             			<div class="alignCenter form-group groupWideModifyMarginBottom">
                            <input type="button" class="subButton" name="submitCallingLineIDBlocking" id="submitCallingLineIDBlocking" value="Submit">
                        </div>
             		</div>
            	</form>
            </div>
        </div>
        
        <!-- <div class="row" id="selectAllDiv">
                <div class="newUiSelectAllChk_hide"> -->
        <div id="selectAllDiv" style="margin-top: 22px; margin-left: 1px;" class="hideInitialDetailInfo hideInitialDetailInfo_selectAllDiv">
                <input type="checkbox" class="selAllChk selectAl_Chk" id="selectAllChk" />
                <label class="labelText" for="selectAllChk"><span class="selectAllChkSpan selectAllSpan"></span>Select All </label><a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="You may also select Users by clicking checkboxes in the table, then selecting Group-Wide Modify actions from pull-down menu"><img class="infoIcon" src="images/NewIcon/info_icon.png"></a>
        </div>
        <!-- Drop-down menu of group-wide user actions -->
        
         <div class="hideInitialDetailInfo hideInitialDetailInfo_groupModifyDiv">
                <div class="col-md-4">
                    <?php if($_SESSION["permissions"]["groupWideUserModify"] == "1"){?>
                    <div class="hideInitialDetailInfo"> 
                            <div class="" name="gwModsLabel" id="gwModsLabel" style="padding-left:0;margin-left: -5px;">
                                     <div class="">
                                            <label class="labelText" name="gwModsLabel" id="gwModsLabel"> Group-wide Modifications</label>
                                            <div class="dropdown-wrap form-group" class="dropdownGwMods" style="">
                                            <select style="" name="gwMods" class="form-control selectBlue" id="gwMods" onchange="onMenuSelectionChange(this)">
                                                            <option value="gwModsNone">Select one:</option>
                                                            <option value="gwModsCFA">Modify 'Call Forwarding Always' services</option>
                                                            <option value="gwModsCFNR">Modify 'Call Forwarding Not Reachable' services</option>
                                                            <option value="gwModsCFS">Modify 'Call Forwarding Selective' services</option>
                                                            <option value="gwModsAuth">Reset SIP Authentication</option>
                                                            <option value="gwModsSwPck">Services/Service Packs Management</option>
                                                            <option value="gwModsDnActivate">Activate/Deactivate</option>
                                                            <?php if($_SESSION["permissions"]["voiceManagement"] == "1")
                                                            { ?>
                                                                    <option value="gwModsVM">Modify 'Voice Management' Service</option>
                                                            <?php }?>
                                                            <?php if($serviceInfo->hasCLIDBlocKingService($_SESSION['groupId'])) { ?>
                                                    				<option value="callingLineIDBlocking">Calling Line ID Blocking</option>                                                                
                                                    		<?php } ?>
                                            </select>
                                            </div>
                                    </div>
                            </div>
                    </div>
                    <?php }?>
                    </div>
                    <div class="col-md-4 usersTableButton">		
                        <div class="col-md-2 disabled" disabled name="downloadCSV" id="downloadCSV" value="" style="cursor: pointer;"><img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png"/><br><span>Download<br>CSV</span></div>
                        <div class="col-md-2 deleteUsers" style="" name="deleteUsers" id="deleteUsers" ><img src="images/icons/delete_selected_users_icon.png" data-alt-src="images/icons/delete_selected_users_icon_over.png" /><br><span>Delete<br>Selected<br>Users</span></div>
                        <div class="col-md-2 expressSheetUsers" style="cursor:pointer; float:right" name="expressSheetUsers" id="expressSheetUsers" ><img src="images/icons/delete_selected_users_icon.png"  data-alt-src="images/icons/delete_selected_users_icon_over.png"/><br><span>Switch to<br/>Express<br/> Sheets</span></div>

                    </div>
                </div>
        
        
	<!-- Users Detailed Table -->        
	<div id="usersTableDiv">
		<table id="usersDetailedListTable" class="stripe row-border order-column customDataTable" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th class="col1 triggerColumn"  style="background-image: none !important;">&nbsp;</th>
				<th class="col2">User Id</th>
				<th class="col3">Activated</th>
				<th class="col4">Registered</th>
				<th class="col5">User Name</th>
                                <th class="col5">Phone Number</th>
				<th class="col6">Extension</th>
				<th class="col7">Device Name</th>
				<th class="col8">Device Type</th>
				<th class="col9">Analog Port Assignment</th>
				<th class="col10">MAC Address</th>
				<th class="col11">Department</th>
				<th class="col12">Service Pack</th>
				<th class="col13">Expiration</th>
				<th class="col14">Type</th>
				<th class="col15">DnD</th>
				<th class="col16">Fwd</th>
				<th class="col17">Fwd To</th>
				<?php if($serviceInfo->hasCLIDBlocKingService($_SESSION['groupId'])) { ?>
					<th class="col17">Calling Line ID Blocking</th>
				<?php } ?>
                <th class="col17">Sim Ring</th>
				<th class="col18">Remote Office</th>
				<th class="col19">Remote Number</th>
				<th class="col20">Polycom Phone Services</th>
				<th class="col21">Custom Contact Directory</th>
			</tr>
			</thead>
		</table>
		<div class="userFilterHtml" style="display: none;"></div>		
		<div class="usersDetailedListTable_action_buttons" style="clear: both; height: 40px; width: 90%; margin: 0 auto; padding-top: 10px">
			
		</div>  
	</div>
        
	<table id="usersDetailedListTableTemp" style="display: none;">
		<thead>
		<tr>
			<th class="col1 triggerColumn">&nbsp;</th>
			<th class="col2">User Id</th>
			<th class="col3">Activated</th>
			<th class="col4">Registered</th>
			<th class="col5">User Name</th>
                        <th class="col5">Phone Number</th>
			<th class="col6">Extension</th>
			<th class="col7">Device Name</th>
			<th class="col8">Device Type</th>
			<th class="col9">Analog Port Assignment</th>
			<th class="col10">MAC Address</th>
			<th class="col11">Department</th>
			<th class="col12">Service Pack</th>
			<th class="col13">Expiration</th>
			<th class="col14">Type</th>
			<th class="col15">DnD</th>
			<th class="col16">Fwd</th>
			<th class="col17">Fwd To</th>
            <?php if($serviceInfo->hasCLIDBlocKingService($_SESSION['groupId'])) { ?>
				<th class="col17">Calling Line ID Blocking</th>
			<?php } ?>
			<th class="col17">Sim Ring</th>
			<th class="col18">Remote Office</th>
			<th class="col19">Remote Number</th>
			<th class="col20">Polycom Phone Services</th>
			<th class="col21">Custom Contact Directory</th>
		</tr>
		</thead>
		<tbody>
		<?php
		$rowIndex = 0;
		$userIds = array();
		for ($a = 0; $a < count($users); $a++) {
			//print_r($users [$a]);

			$userId = $users [$a] ["userId"];
			$userIds[] = $userId;

			$usersHtml = '';

			$userPhoneNumber = $users [$a] ["phoneNumber"];
			$phoneNumberStatus = $users [$a] ["activated"];
			$userName = $users [$a] ["firstName"] . " " . $users [$a] ["lastName"];
			$extension = $users [$a] ["ext"];

			$cssAddClass = "";
			//$cssAddClass = "usersTableColor";

			$checkBoxStr = "<td class='col1'><input type=\"checkbox\" class=\"checkUserListBox usersDetailedListCheckBox\" id=\"userCheckBox_" . $rowIndex . "\" data-id=\"userCheckBox" . $rowIndex . "\" onclick=\"onUserSelect_Detailed(this)\" name=\"" . $userId . "*-*-*" .
				$deviceName . "*-*-*" . $deviceType . "\" id='users_selected_$userId' ";
			if ($_SESSION ["permissions"] ["groupWideUserModify"] == "0") {
				$checkBoxStr .= " disabled";
			}
			$checkBoxStr .= "><label for=\"userCheckBox_" . $rowIndex . "\"><span></span></label></td>";

			if (isset ($_SESSION ["permissions"] ["modifyUsers"]) && $_SESSION ["permissions"] ["modifyUsers"] == "1") {
                        //if (1) {
                        
				$usersHtml .= "<tr name='broadsoft_usersRow_$userId'
	                   class='userIdVal broadsoft_users $cssAddClass broadsoft_usersRow_$userId rowHoverEffect$rowIndex' data-class='rowHoverEffect$rowIndex'
	                   data-loaded='0'
	                   data-lastname='" . $users [$a] ["lastName"] . "'
	                   data-firstname='" . $users [$a] ["firstName"] . "'
	                   data-phone='" . $userPhoneNumber . "'
	                   data-id='" . $userId . "'
	                   data-extension='" . $extension . "'>" . $checkBoxStr;
			} else {
				//$userIdVal = "<tr>";
				//$usersHtml .= "<tr id='broadsoft_user_$userId' name='broadsoft_usersRow_$userId' class='broadsoft_users $cssAddClass broadsoft_usersRow_$userId rowHoverEffect$rowIndex' data-class='rowHoverEffect$rowIndex'>" . $checkBoxStr . "";
                               $usersHtml .= "<tr name='broadsoft_usersRow_$userId'
	                   class='userIdValDisable broadsoft_users $cssAddClass rowHoverEffect$rowIndex' data-class='rowHoverEffect$rowIndex'
	                   data-loaded='0'
	                   data-lastname='" . $users [$a] ["lastName"] . "'
	                   data-firstname='" . $users [$a] ["firstName"] . "'
	                   data-phone='" . $userPhoneNumber . "'
                           data-id='" . $userId . "'   
	                   data-extension='" . $extension . "'>" . $checkBoxStr;
			}

			$usersHtml .= "<td class='col2 userId'>" . $userId . "</td>";
			$usersHtml .= "<td class='col3 phoneNumberStatus'>" . $phoneNumberStatus . "</td>";
			$usersHtml .= "<td class='col4 registerStatus'></td>";
			$usersHtml .= "<td class='col5 userName'>" . $userName . "</td>";
                        $usersHtml .= "<td class='col5 userName'>" . $userPhoneNumber . "</td>";
                        
			$usersHtml .= "<td class='col6 extension'>" . $extension . "</td>";
			$usersHtml .= "<td class='col7 deviceName'></td>";
			$usersHtml .= "<td class='col8 deviceType'></td>";
			$usersHtml .= "<td class='col9 portNumber'></td>";
			$usersHtml .= "<td class='col10 macAddress'></td>";
			$usersHtml .= "<td class='col11 department'></td>";
			$usersHtml .= "<td class='col12 servicePack'></td>";
			$usersHtml .= "<td class='col13 expiration'></td>";
			$usersHtml .= "<td class='col14 epType'></td>";
			$usersHtml .= "<td class='col15 DnD'></td>";
			$usersHtml .= "<td class='col16 Fwd'></td>";
			$usersHtml .= "<td class='col17 FwdTo'></td>";
			if($serviceInfo->hasCLIDBlocKingService($_SESSION['groupId'])) {
			    $usersHtml .= "<td class='col17 clidBlock'></td>";
			}
            $usersHtml .= "<td class='col17 simRing'></td>";
			$usersHtml .= "<td class='col18 remote'></td>";
			$usersHtml .= "<td class='col19 remote_number'></td>";
			//$usersHtml .= "<td>" . $integration . "</td>";
			$usersHtml .= "<td class='col20 polycomPhoneService'></td>";
			$usersHtml .= "<td class='col21 customContactDirectory'></td></tr>";

			//echo $rowIndex . ',';
			++$rowIndex;

			echo $usersHtml;
		}
		?>
		</tbody>
	</table>


	<div class="viewDetailRegistration" style="width:100%">
		<table id="allUsersRegistration" class="stripe row-border order-column customDataTable allUsersRegistration" cellspacing="0" style="width: 100%;">
			<thead>
			<tr>
				<th class="triggerColumn1" style="width: 10%; text-align: center">&nbsp;</th>
				<th class="triggerColumn1" style="width: 15%; text-align: center">Device Name</th>
				<th style="width: 15%; text-align: center">Device Type</th>
				<th style="width: 15%; text-align: center">Registration Status</th>
				<th style="width: 15%; text-align: center">IP</th>
				<th style="width: 15%; text-align: center">Expiry</th>
				<th style="width: 15%; text-align: center">Agent Type</th>
			</tr>
			</thead>
		</table>
	</div>

	<div id="dialogUsers" class="dialogClass"></div>
	<div id="dialogGroupWideModify" class="dialogClass"></div>
	<div id="newCriteriaCFSDialog"></div>

	<script type="text/javascript">

		<?php
		$user_pages = array_chunk($userIds, 10);
		?>

        var user_pages_str = '<?= json_encode($user_pages) ?>';
        var user_pages = jQuery.parseJSON(user_pages_str);
        var allUsersRegistration_Count = <?php echo count($userIds) ?>;
        var usersDetailedListTable_Count = <?php echo count($userIds) ?>;

        $('#usersDetailedListTable').show();

        $('#downloadCSV').hide();
        $('#filtersStatusTitle').hide();

        var usersDetailedListTable_DT = $('#usersDetailedListTable').DataTable({
            //scrollY: false,
            destroy: true,
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            pagingType: "dk_express_pagination",
            lengthMenu: [10, 25, 50],
            searching: false,

            info: true,
            language: {
                emptyTable: " ",
                infoEmpty: (usersDetailedListTable_Count ? "Loading ..." : "No Users Found"),
                lengthMenu: "Show<br/> _MENU_ <br/>users",
                info: "Showing _START_ to _END_ of _TOTAL_ users",
                oPaginate: {
                    sPageJumpText: "Jump to pg ",
                    sPageJumpGoBtn: 'GO'
                }
            },

            dom: '<"wrapper"<"usersDetailedListTable_top_bar dataTables_top_bar"<"column1"><"column2"><"column3"><"column4"l>>'
            + 't'
            + '<"usersDetailedListTable_bottom_bar dataTables_bottom_info_bar"<"retrieving_info"><"currently_showing_info"i>p>>',
            // fixedColumns: {
            //     leftColumns: 2,
            //     rightColumns: 0
            // },

            "initComplete": initComplete
        });
 
        //Set allUsersRegistration_DT
        setRegisterTableSorter();

        showLoader();

        function initComplete() {
        	pendingProcess.push("User Detailed List");
            showSelectedView();

            loadDetails();

            $('#downloadCSV').addClass('disabled').prop('disabled', true);
            $('#filtersStatusTitle').addClass('disabled').prop('disabled', true);

            $('#selectAllChk').addClass('disabled').prop('disabled', true);
            $('.selectAllChkSpan').addClass('checkBoxLikeDisable');
            
            //$('#expressSheetUsers').hide();
            //$('#deleteUsers').hide();
            //$(".hideInitialDetailInfo").hide();
            $('.hideInitialDetailInfo_groupModifyDiv').hide();

            var DT_top_bar_column_1 = $('.usersDetailedListTable_top_bar .column1');
            var DT_top_bar_column_2 = $('.usersDetailedListTable_top_bar .column2');
            var DT_top_bar_column_3 = $('.usersDetailedListTable_top_bar .column3');
            var DT_top_bar_column_4 = $('.usersDetailedListTable_top_bar .column4');

            var hideInitialDetailInfo_selectAllDiv = $('.hideInitialDetailInfo_selectAllDiv');
            var hideInitialDetailInfo_groupModifyDiv = $('.hideInitialDetailInfo_groupModifyDiv');

            var actionBtns = $('.usersDetailedListTable_action_buttons');

            DT_top_bar_column_1.prepend(hideInitialDetailInfo_selectAllDiv);
            DT_top_bar_column_3.prepend(hideInitialDetailInfo_groupModifyDiv);


            //New UI
            //DT_top_bar_column_4.prepend(actionBtns);

        }

        function loadDetails(page) {

            page = page || 0;

            if (page < user_pages.length && $('#usersDetailedListTable').length) {

                var current_count = page * 10;
                var total_count = <?= count($userIds) ?>;

                users = user_pages[page];

                dataToSend = {userIds: users};

                //console.log(dataToSend);

                $.ajax({

                    type: "POST",
                    url: "users/userDetailedList/getUserDetails.php",
                    data: dataToSend,
                    success: function (result) {
                      //  console.log(result);
                      if(foundServerConErrorOnProcess(result, "User Detailed List")) {
        					return;
                      }
                         current_count += users.length;

                        if (current_count >= total_count) {
                            $('.usersDetailedListTable_bottom_bar .retrieving_info').html('Retrieved all ' + total_count + ' users');
                        } else {
                            $('.usersDetailedListTable_bottom_bar .retrieving_info').html('Retrieving ' + current_count + ' of ' + total_count + ' users');
                        }

                        var responseObj = JSON.parse(result);

                        //User Details
                        $.each(responseObj.userDetails, function (user_count, userDetails) {

                            var userID = userDetails.userID;

                            var user_row = $(".broadsoft_users[data-id='" + userID + "']");

                            if (user_row.length) {

                                //console.log(user_row.data('id'));

                                var DnD = userDetails.DnD;
                                var Fwd = userDetails.Fwd;
                                var FwdTo = "";
                                var remote = userDetails.remote;
                                var remote_number = "";
                                var simRing = userDetails.simRing;
                                var clidBlock = userDetails.clidBlock;
                                if (DnD === "false") {
                                    DnD = "Off";
                                } else {
                                    DnD = "On";
                                }
                                if (Fwd === "true") {
                                    Fwd = "Active";
                                    FwdTo = userDetails.Fwd.FwdTo;
                                } else {
                                    if (userDetails.Fwd.FwdTo && userDetails.Fwd.FwdTo !== "") {
                                        Fwd = "Not Active";
                                        FwdTo = userDetails.Fwd.FwdTo;
                                    } else {
                                        Fwd = "";
                                        FwdTo = "";
                                    }
                                }
                                if (remote === "true") {
                                    remote = "*";
                                    remote_number = userDetails.remote.number;
                                } else {
                                    remote = "";
                                    remote_number = "";
                                }

                                $('td.DnD', user_row).html(DnD);
                                $('td.Fwd', user_row).html(Fwd);
                                $('td.FwdTo', user_row).html(FwdTo);
                                if($('td.clidBlock').length > 0) {
                                	$('td.clidBlock', user_row).html(clidBlock);
                                }
                                $('td.simRing', user_row).html(simRing);
                                $('td.remote', user_row).html(remote);
                                $('td.remote_number', user_row).html(remote_number);


                                if (userDetails.registerStatus && userDetails.registerStatus === 'Registered') {
                                    $('td.registerStatus', user_row).html('Yes');
                                    user_row.addClass('registerUsr');
                                } else {
                                    $('td.registerStatus', user_row).html('No');
                                    user_row.addClass('unRegisterUsr');
                                }

                                $('td.deviceName', user_row).html(userDetails.deviceName);
                                $('td.deviceType', user_row).html(userDetails.deviceType);
                                $('td.portNumber', user_row).html(userDetails.portNumber);
                                $('td.macAddress', user_row).html(userDetails.macAddress);
                                $('td.department', user_row).html(userDetails.department);

                                if (userDetails.servicePack) {
                                    $('td.servicePack', user_row).html(userDetails.servicePack.join(','));
                                } else {
                                    $('td.servicePack', user_row).html('');
                                }
                                $('td.epType', user_row).html(userDetails.epType);
                              //  $('td.polycomPhoneService', user_row).html(userDetails.polycomPhoneService);
                                $('td.polycomPhoneService', user_row).html(userDetails.integration);
                                $('td.customContactDirectory', user_row).html(userDetails.customContactDirectory);

                                $('img.detailLoader', user_row).remove();

                                if ($('#usersDetailedListTable').length) {
                                    usersDetailedListTable_DT.row.add(user_row);
                                }

                                user_row.remove();

                            }

                        });

                        if ($('#usersDetailedListTable').length) {

                            usersDetailedListTable_DT.draw(false);

                            //Registration Details
                            $.each(responseObj.registrationDetails, function (user_count, registrationDetails) {

                                if (allUsersRegistration_DT) {
                                    //console.log(allUsersRegistration_DT.page.info().recordsTotal);
                                    var count = allUsersRegistration_DT.page.info().recordsTotal;
                                    registrationDetails[0] = count++;
                                    allUsersRegistration_DT.row.add(registrationDetails).draw();

                                    if (current_count >= total_count) {
                                        $('.allUsersRegistration_bottom_bar .retrieving_info').html('Processed all users');
                                    } else {
                                        $('.allUsersRegistration_bottom_bar .retrieving_info').html('Processing ' + current_count + ' of ' + total_count + ' users');
                                    }
                                }

                            });

                            if (page === 0) {
                                new $.fn.dataTable.FixedColumns(usersDetailedListTable_DT, {
                                    leftColumns: 2,
                                    rightColumns: 0
                                });

                                $('#downloadCSV').show();
                                $('#filtersStatusTitle').show();
                            }

                            page++;

                            if (page === user_pages.length) {
                                $('#downloadCSV').removeClass('disabled').prop('disabled', false);
                                $('#selectAllChk').removeClass('disabled').prop('disabled', false);
                                $('.selectAllChkSpan').addClass('checkBoxLikeEnable');
                                $('#filtersStatusTitle').removeClass('disabled').prop('disabled', false);

                                //$(".hideInitialDetailInfo").show();
                                //$('.hideInitialDetailInfo_selectAllDiv').show();
                                $('.hideInitialDetailInfo_groupModifyDiv').show();

                                if (allUsersRegistration_DT) {
                                    allUsersRegistration_DT.draw();
                                }

                            }

                            loadDetails(page);

                        }
                    }
                });

            }

        }

        function onUserSelect_Detailed(checkbox) {
           //if(event.target.type == "checkbox"){
            var selectAllCheckBox = document.getElementById("selectAllChk");
            selectAllCheckBox.checked = false;

            // if User is clicked, create array element with userID as the key
            var userId = checkbox.name;
            if (!selectedUsers.hasOwnProperty(userId)) {
                selectedUsers[userId] = false;
            }
            selectedUsers[userId] = checkbox.checked;

            // determine if there are any row check boxes checked
            var userChecked = false;
            for (var key in selectedUsers) {
                if (selectedUsers.hasOwnProperty(key) && selectedUsers[key]) {
                    userChecked = true;
                    break;
                }
            }

            // Make sure that Modification Forms are hidden if no checkbox is selected
            // And dropdown Modification Forms selection is not selecting any form
            if (!userChecked) {
                if($("#gwMods").length) {
                    $("#gwMods option")[0].selected = true;
                }
                disableAllModificationForms();
            }

            // Show 'Delete Selected Users' button as long as there is one row checkbox checked
            if (deleteUsersAllowed) {
                if (deleteButton) {
                    deleteButton.style.display = userChecked ? deleteButtonStyle : 'none';
                }
            }
            if (expressSheetUsersAllowed) {
                if (expressSheetButton) {
                    expressSheetButton.style.display = userChecked ? expressSheetButtonStyle : 'none';
                }
            }
            //downloadCSVButton.style.display = userChecked ? downloadCSVButtonStyle : 'none';
            if (groupWideOperationsAllowed == "1") {
                // Show/hide group-wide drop-down operations menu
                modificationMenu.style.display = userChecked ? modificationMenuStyle : 'none';
            }
            //activeButton.style.display = userChecked ? activeButtonStyle : 'none';
            //deactiveButton.style.display = userChecked ? deactiveButtonStyle : 'none';
        }
	</script>
	<script>

        /*$("#processingUnified").click(function(){

			if ($('#processingUnified').is(':checked')) {
				$("#usePhoneMessageWaitingIndicator").prop("disabled", false);
			}else{
				$("#usePhoneMessageWaitingIndicator").prop("disabled", true);
				$("#usePhoneMessageWaitingIndicator").prop("checked", false);
			}
		});*/

        var chkYes = document.getElementById("voiceMessagingYes");
        var dvPassport = document.getElementById("dvPassport");
        if (dvPassport && chkYes) {
            dvPassport.style.display = chkYes.checked ? "block" : "none";
        }
        var deleteButton = document.getElementById("deleteUsers");
        if (deleteButton) {
            var deleteButtonStyle = deleteButton.style.display;
            deleteButton.style.display = 'none';
        }
        var expressSheetButton = document.getElementById("expressSheetUsers");
        if (expressSheetButton) {
            var expressSheetButtonStyle = expressSheetButton.style.display;
            expressSheetButton.style.display = 'none';
        }
        var modificationMenu = document.getElementById("gwModsLabel");
        if (modificationMenu) {
            var modificationMenuStyle = modificationMenu.style.display;
            modificationMenu.style.display = 'none';
        }
        var submitCFAButton = document.getElementById("submitCFA");
        if (submitCFAButton) {
            var submitCFAButtonDisplayStyle = submitCFAButton.style.display;
        }

        var submitCFNRButton = document.getElementById("submitCFNR");
        if (submitCFNRButton) {
            var submitCFNRButtonDisplayStyle = submitCFNRButton.style.display;
        }
        var submitActiveNumberButton = document.getElementById("submitActiveNumber");
        if (submitActiveNumberButton) {
            var submitActiveNumberButtonDisplayStyle = submitActiveNumberButton.style.display;
        }
        var submitCFSButton = document.getElementById("submitCFS");
        if (submitCFSButton) {
            var submitCFSButtonDisplayStyle = submitCFSButton.style.display;
            var submitCFSButtonColorStyle = submitCFSButton.style.color;
        }
        var submitServicesButton = document.getElementById("submitSvcAssignment");
        if (submitServicesButton) {
            var submitServicesButtonDisplayStyle = submitServicesButton.style.display;
            submitServicesButton.style.display = "none";
        }
        var selectedUsers = {};
        var usersList = "";
        var selectAllPermission = "";

        var superUser = "<?php echo $_SESSION["superUser"]; ?>";

        var deleteUserPermission = "";
        var deleteUsersAllowed = false;

		<?php
		if (isset($_SESSION["permissions"]["deleteUsers"]) && $_SESSION["permissions"]["deleteUsers"] == "1") {
			echo $delPer = "1";
		} else {
			echo $delPer = "0";
		}
		?>

        deleteUserPermission = "<?php echo $delPer; ?>";

        if (deleteUserPermission == "1") {
            var deleteUsersAllowed = deleteUserPermission == "1";
        }

		<?php
		if (isset($_SESSION["permissions"]["groupWideUserModify"]) && $_SESSION["permissions"]["groupWideUserModify"] == "1") {
			echo $groupModifyPer = "1";
		} else {
			echo $groupModifyPer = "0";
		}
		?>

        var groupWideOperationsAllowed = <?php echo $groupModifyPer; ?>;
        var serializedArrayData = null;
        var action = "";

        var selectedServicePack = "";
        var selectedUserService = "";

        var hasAuthenticationPassword = false;
        var authMinLength = <?php echo $authenticationPasswordMinLength; ?>;

        // Criteria dialog states: create, modify
        var cfsCriteriaDialogState = "";

        var numCfsCriteria = 0;
        var numCfsCheckedCriteria = 0;

        //var expressSheetUserPermission = "<?php //echo $_SESSION["permissions"]["expressSheets"]; ?>";
        //var expressSheetUsersAllowed = expressSheetUserPermission == "1";


        var expressSheetUserPermission = "";
        var expressSheetUsersAllowed = false;

		<?php
		if (isset($_SESSION["permissions"]["expressSheets"]) && $_SESSION["permissions"]["expressSheets"] == "1") {
			echo $expressPer = "1";
		} else {
			echo $expressPer = "0";
		}
		?>

        expressSheetUserPermission = "<?php echo $expressPer; ?>";

        if (expressSheetUserPermission == "1") {
            var expressSheetUsersAllowed = expressSheetUserPermission == "1";
        }

        var globalFiltersAreEnabled = 0;

        $(function () {

            $("#modsVoiceMessaging input[name='voiceMessaging']").click(function () {
                if (document.getElementById("dvPassport")) {
                    $('#dvPassport').show();
                }
                if ($("#voiceMessagingYes").is(":checked")) {
                    $("#dvPassport").show();
                } else {
                    $("#voiceMessagingYes").val();
                    $("#dvPassport").hide();
                }
            });


            //-----------------------------------------
            $("#department").change(function () {
                loadDetailedUsersTable = true;
                limit();
            });


            //-----------------------------------------
            $("#downloadCSV").click(function () {
                $("#departmentDropdown").submit();
            });
            //-----------------------------------------
            $("#downloadCSVRegistration").click(function () {
                $("#departmentDropdownRegistration").submit();
            });


            //Star Enter button action perform
            $('#phSipFwdCFA').on('keyup keypress', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    $('#submitCFA').trigger('click');
                }
            });

            $('#phSipFwdCFNR').on('keyup keypress', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    $('#submitCFNR').trigger('click');
                }
            });
            $('#phSipFwdCFS').on('keyup keypress', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    $('#submitCFS').trigger('click');
                }
            });

            /*voice Management */

            $('#submitVoiceManagement').on('keyup keypress', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    $('#submitVoiceManagement').trigger('click');
                }
            });
            /*end Voice Management */

            //End Enter button action perform

            // Invoked by clicking on 'Delete Selected Users' button
            //------------------------------------------------------
            $("#deleteUsers").click(function () {
                usersList = getSelectedUsers();
                $("#dialogUsers").dialog("open");
                $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
                $(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
                $(":button:contains('Cancel')").removeAttr("disabled").removeClass("ui-state-disabled");
                $("#dialogUsers").html("Are you sure you want to delete selected users?");
                $(":button:contains('Delete')").show();
                $(":button:contains('Cancel')").show();
                $(":button:contains('More Changes')").hide();
            });

            $(".expressSheetUsers").click(function () {
                activeImageSwapExpress();
                $("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
                $("#loading").show();
                $(".navMenu").removeClass("active");
                $("#expressSheets").addClass("active");
                var module = "expressSheets";
                var userType = '';
                <?php if($_SESSION['superUser'] == 1){?>
                userType = 'superUser';
		<?php }else if($_SESSION['superUser'] == 3){ ?> 
                userType = 'superUser';                    
                <?php } else{?>
                userType = '<?php echo $_SESSION['adminType']?>';
				<?php }?>
                usersList = getSelectedUsers();
                $.ajax({
                    type: "POST",
                    url: "expressSheets/index.php",
                    data: {module: "userModule", applyFilters: globalFiltersAreEnabled, userList: usersList, switchToExpress: 1, selectAllchk: selectAllPermission},
                    success: function (result) {

                        $("#loading").hide();
                        $("#mainBody").html(result);
                        $('#helpUrl').attr('data-module', module);
                        $('#helpUrl').attr('data-adminType', userType);

                        // $('.expressSheetUsers').click(activeImageSwap, activeImageSwap);
                    }
                });

            });

            // Invoked by clicking on 'Active Number for Selected Users' button
            //------------------------------------------------------
            $("#numberActivation").click(function () {
                usersList = getSelectedUsers();
                $("#dialogUsers").dialog("open");
                $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
                $("#dialogUsers").html("Are you sure you want to activate number for selected users?");
                $(":button:contains('Delete')").hide();
                $(":button:contains('Activate')").show();
                $(":button:contains('Deactivate')").hide();
                $(":button:contains('Cancel')").show();
                $(":button:contains('More Changes')").hide();
            });

            $(".userNumberAction input").click(function () {
                $("#submitActiveNumber").prop("disabled", false);
            });

            // Invoked by clicking on 'Active Number for Selected Users' button
            //------------------------------------------------------
            $("#numberDeactivation").click(function () {
                usersList = getSelectedUsers();
                $("#dialogUsers").dialog("open");
                $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
                $("#dialogUsers").html("Are you sure you want to deactivate number for selected users?");
                $(":button:contains('Delete')").hide();
                $(":button:contains('Activate')").hide();
                $(":button:contains('Deactivate')").show();
                $(":button:contains('Cancel')").show();
                $(":button:contains('More Changes')").hide();
            });

//         var checkUserLength = function() {
//     		var userCount = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").length;
//     		var userCountChecked = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").filter(":checked").length;
//     		userCount ? $("#selectAllDiv").show() : $("#selectAllDiv").hide();
//     		userCountChecked ? $("#gwModsLabel").show() : $("#gwModsLabel").hide();debugger;
//     		if(userCount > 0){ alert('1');
//     			var checkVal = userCount == userCountChecked ? true: false;
//     			$("#selectAllDiv").show();
//     		}else{ alert('2');
//     			$("#selectAllDiv").hide();
//     		}
//     		$("#selectAllChk").prop("checked", checkVal);
//     		selectAllPermission = checkVal;
//     	}

            var checkUserLength = function () {
                var userCount = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").length;
                var userCountChecked = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").filter(":checked").length;
                userCount ? $("#selectAllDiv").show() : $("#selectAllDiv").hide();
                userCountChecked ? $("#gwModsLabel").show() : $("#gwModsLabel").hide();
                var checkVal = userCount == userCountChecked ? true : false;
                $("#selectAllChk").prop("checked", checkVal);
                selectAllPermission = checkVal;
            }


            // Dialog function after clicking on 'Delete Selected Users' button
            //-----------------------------------------------------------------
            $("#dialogUsers").dialog({

                autoOpen: false,
                width: 800,
                modal: true,
                position: {my: "top", at: "top"},
                resizable: false,
                closeOnEscape: false,
                buttons: {
                    "Delete": function () {
                    	pendingProcess.push("Delete User");
                        //$("#dialogUsers").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
                        $("#dialogUsers").html("<div id='loding' style='border: 0px solid red; width: auto;text-align:center'>&nbsp;Please wait user is deleting <br /><br /><img src='/Express/images/ajax-loader.gif'></div>");
                        $(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                        $(":button:contains('Cancel')").attr("disabled", "disabled").addClass("ui-state-disabled");
                        /*beforeSend: function() {
								   $("#dialogUsers").html("<div>Please wait user is deleting <br /><br /><img src='/Express/images/ajax-loader.gif'></div>");
						   }
						   async: false,*/
                        $.ajax({
                            type: "POST",
                            url: "users/deleteUsers.php",
                            data: {action: "deleteUsers", userList: usersList},
                            success: function (result) {
                            	if(foundServerConErrorOnProcess(result, "Delete User")) {
                					return false;
                              	}
                                $("#dialogUsers").dialog("option", "title", "Request Complete");
                                $(".ui-dialog-titlebar-close", this.parentNode).hide();
                                $(".ui-dialog-buttonpane", this.parentNode).show();
                                $("#loding").hide();
                                $(":button:contains('Delete')").hide();
                                $(":button:contains('Cancel')").hide();
                                $(":button:contains('More Changes')").show();
                                $("#dialogUsers").html(result);
                                $("#dialogUsers").append("User deleted successfully.");
                                $("#dialogUsers").append(returnLink);
                                checkUserLength();
                            }
                        });
                    },

                    "Cancel": function () {
                        $(this).dialog("close");
                    },
                    "More Changes": function () {
                        $(this).dialog("close");
                        $("html, body").animate({scrollTop: 0}, 600);
                        $("#mainBody").html("");
                        $("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
                        $("#loading").show();
                        $(".navMenu").removeClass("active");
                        $(this).addClass("active");
                        $.ajax({
                            type: "POST",
                            url: "navigate.php",
                            data: {module: 'users'},
                            success: function (result) {
                                $("#loading").hide();
                                $("#mainBody").html(result);
                                //apply_active_filters();
                                //checkUserLength();
                            }
                        });
                    }
                },
                open: function() {
    				setDialogDayNightMode($(this));
    				$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
    				$('.ui-dialog-buttonpane').find('button:contains("Delete")').addClass('deleteButton');			 
    				$('.ui-dialog-buttonpane').find('button:contains("More Changes")').addClass('moreChangesButton');			 
                }
            });


            // Invoked by clicking on 'Submit' button on the CFA form
            //-------------------------------------------------------
            $("#submitCFA").click(function () {
                action = "modifyCFA";
                usersList = getSelectedUsers();
                serializedArrayData = $("form#CFA").serializeArray();
                $.ajax({
                    type: "POST",
                    url: "users/checkData.php",
                    data: serializedArrayData,
                    success: function (result) {
                        processGroupWideModificationCheckDataResult(result);
                    }
                });
            });
//modify VoiceMessaging service

            $("#submitVoiceManagement").click(function () {
                action = "modifyVoiceMessaging";
                usersList = getSelectedUsers();
                serializedArrayData = $("form#modsVoiceMessaging").serializeArray();
                $.ajax({
                    type: "POST",
                    url: "users/checkData.php",
                    data: serializedArrayData,
                    success: function (result) {
                        processGroupWideModificationCheckDataResult(result);
                    }
                });
            });


            // Invoked by clicking on 'Submit' button on the CFNR form
            //--------------------------------------------------------
            $("#submitCFNR").click(function () {
                action = "modifyCFNR";
                usersList = getSelectedUsers();
                serializedArrayData = $("form#CFNR").serializeArray();
                $.ajax({
                    type: "POST",
                    url: "users/checkData.php",
                    data: serializedArrayData,
                    success: function (result) {
                        processGroupWideModificationCheckDataResult(result);
                    }
                });
            });


            // Invoked by clicking on 'Submit' button on the CFS form
            //-------------------------------------------------------
            $("#submitCFS").click(function () {
                action = "modifyCFS";
                usersList = getSelectedUsers();
                serializedArrayData = $("form#CFS").serializeArray();
                $.ajax({
                    type: "POST",
                    url: "users/checkData.php",
                    data: serializedArrayData,
                    success: function (result) {
                        processGroupWideModificationCheckDataResult(result);
                    }
                });
            });

            // Invoked by clicking on 'Submit' button on the CFS form
            //-------------------------------------------------------
            $("#submitActiveNumber").click(function () {
                var optVal = $("#numberActivationFrm").find("input[name='activateNumber']:checked").val();
                if (optVal == "activate") {
                    action = "numberActivate";
                } else if (optVal == "deactivate") {
                    action = "numberDeactivate";
                }
                usersList = getSelectedUsers();
                serializedArrayData = $("form#numberActivationFrm").serializeArray();
                $.ajax({
                    type: "POST",
                    url: "users/checkData.php",
                    data: serializedArrayData,
                    success: function (result) {
                        processGroupWideModificationCheckDataResult(result);
                    }
                });
            });


            // Invoked by clicking on 'Submit' button on the 'Reset Authentication' form
            // -------------------------------------------------------------------------
            $("#submitAuthentication").click(function () {
                action = "resetAuthentication";
                usersList = getSelectedUsers();
                serializedArrayData = $("form#authPasswords").serializeArray();
                $.ajax({
                    type: "POST",
                    url: "users/checkData.php",
                    data: serializedArrayData,
                    success: function (result) {
                        processGroupWideModificationCheckDataResult(result);
                    }
                });
            });


            // Invoked by clicking on 'Submit' button on the Services Management form
            //-----------------------------------------------------------------------
            $("#submitSvcAssignment").click(function () {
                action = "modifyServices";
                usersList = getSelectedUsers();
                serializedArrayData = $("form#SwPck").serializeArray();
                $.ajax({
                    type: "POST",
                    url: "users/checkData.php",
                    data: serializedArrayData,
                    success: function (result) {
                        processGroupWideModificationCheckDataResult(result);
                    }
                });
            });


            $(".callingLineIDBlockingFrm input").click(function(){
            	$("#callingLineIDBlocking").prop("disabled", false);
            });

             $("#submitCallingLineIDBlocking").click(function() {
            	action = "modifyCallingLineIDBlocking";
            	usersList = getSelectedUsers();
            	serializedArrayData = $("form#callingLineIDBlockingFrm").serializeArray();
            	$.ajax({
            		type: "POST",
            		url: "users/checkData.php",
            		data: serializedArrayData,
            		success: function(result) {
            			processGroupWideModificationCheckDataResult(result);
            		}
            	});
            });
            
            // Dialog function after clicking on hidden group-wide user actions
            //-----------------------------------------------------------------
            $("#dialogGroupWideModify").dialog({
                autoOpen: false,
                width: 800,
                modal: true,
                position: {my: "top", at: "top"},
                resizable: false,
                closeOnEscape: false,
                buttons: {
                    "Complete": function () {
                    	pendingProcess.push("Users group wide modify");
                        var isVMAction = action == "modifyVoiceMessaging";
                        //serializedArrayData = $("form#modsVoiceMessaging").serializeArray();
                        $.ajax({
                            type: "POST",
                            url: "users/modifyUsers.php",
                            data: {action: action, userList: usersList, formData: serializedArrayData},
                            async: false,
                            beforeSend: function(){
                                $("html, body").animate({ scrollTop: 0 }, 0); //Code added @07 Feb 2019
                            },
                            success: function (result) {
                            	if(foundServerConErrorOnProcess(result, "Users group wide modify")) {
                					return false;
                              	}
                                $("#dialogGroupWideModify").dialog("option", "title", "Request Complete");
                                $(".ui-dialog-titlebar-close", this.parentNode).hide();
                                $(".ui-dialog-buttonpane", this.parentNode).show();
                                $(":button:contains('Complete')").hide();
                                $(":button:contains('Cancel')").hide();
                                $(":button:contains('More Changes')").show();
                                $(":button:contains('Return To Main')").show();
                                $("#dialogGroupWideModify").html(result);
                                $("#dialogGroupWideModify").append(returnLink);
                                if (isVMAction) {
                                    vmProcessGroupWideModificationCheckDataResult(result);
                                }

                            }
                        });
                    },

                    "Cancel": function () {
                        $(this).dialog("close");
                    },
                    "More Changes": function () {

                        $(this).dialog("close");
                        $("html, body").animate({scrollTop: 0}, 600);
                        if (userDetailDataTable) {
                            $('#usersTable').off();
                            $('#usersTable').empty();
                        }
                        $("#mainBody").html("");
                        $("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
                        $("#loading").show();
                        $(".navMenu").removeClass("active");
                        $(this).addClass("active");
                        $.ajax({
                            type: "POST",
                            url: "navigate.php",
                            data: {module: 'users'},
                            success: function (result) {
                                $("#loading").hide();
                                $("#mainBody").html(result);
                                $("#dialogGroupWideModify").append(returnLink);
                            }
                        });
                    },
// 				"Return To Main": function(){
// 					window.location.href = "main.php";
// 				}
                },
                open: function() {
    				setDialogDayNightMode($(this));
    				$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
    				$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('deleteButton');			 
    				$('.ui-dialog-buttonpane').find('button:contains("More Changes")').addClass('moreChangesButton');			 
                }
            });


            // Invoked by clicking on 'Select All' checkbox sks
            //---------------------------------------------


            $(document).on("click", ".checkUserListBox", function () {

                var el = $(this);
                var elChked = el.is(":checked");
                var cName = el.attr("name");
                var cIds = el.attr("id");
                var cId = cIds.replace(/[\-\[\]\/\{\}\(\)\*\@\?\.\\\^\$\|]/g, "\\$&");
                /*$("#" + cId).prop("checked", elChked);
                $("#" + cId).prop("disabled", false);*/
                $(".DTFC_LeftBodyWrapper").find("#"+cId).prop("checked", elChked);
		$(".DTFC_LeftBodyWrapper").find("#"+cId).prop("disabled", false);
                checkUserLength();
                printSelectedUser();               
                
                /*
                var el = $(this);
		var elChked = el.is(":checked");
		var cName = el.attr("name");
		var cIds = el.attr("id");
		var cId = cIds.replace(/[\-\[\]\/\{\}\(\)\*\@\?\.\\\^\$\|]/g, "\\$&");
	  	$(".DTFC_LeftBodyWrapper").find("#"+cId).prop("checked", elChked);
		$(".DTFC_LeftBodyWrapper").find("#"+cId).prop("disabled", false);
	 	checkUserLength();
	 	printSelectedUser();
                */
            });


            $("#selectAllChk").click(function () {
                var selectAllCheck = $(this).is(":checked");
                var allCheckboxs = $(document).find(".DTFC_LeftBodyWrapper table td input[type=checkbox]");

                var rows = usersDetailedListTable_DT.rows().nodes();
                // Check/uncheck checkboxes for all rows in the table
                $('input[type="checkbox"]', rows).prop('checked', this.checked);
                usersDetailedListTable_DT.rows().draw();

                selectAllPermission = selectAllCheck;
                allCheckboxs.each(function () {

                    var el = $(this);
                    var elChked = el.is(":checked");
                    var cName = el.attr("name");
                    var cIds = el.attr("id");
                    var cId = cIds.replace(/[\-\[\]\/\{\}\(\)\*\@\?\.\\\^\$\|]/g, "\\$&");

                    if (elChked && !selectAllCheck) {
                        if (!el.hasOwnProperty(cName)) {
                            selectedUsers[cName] = false;
                        }
                        selectedUsers[cName] = selectAllCheck;
                    } else {
                        selectedUsers[cName] = selectAllCheck;
                    }

                });

                // Make sure that Modification Forms are hidden if no checkbox is selected
                // And dropdown Modification Forms selection is not selecting any form
                if (!selectAllCheck) {
                    if($("#gwMods").length) {
                        $("#gwMods option")[0].selected = true;
                    }
                    disableAllModificationForms();
                }

                if (deleteUsersAllowed) {
                    // Show/hide 'Delete Selected Users' button
                    if (deleteButton) {
                        deleteButton.style.display = selectAllCheck ? deleteButtonStyle : 'none';
                    }
                    //activeButton.style.display = selectAllCheck ? activeButtonStyle : 'none';
                    //deactiveButton.style.display = selectAllCheck ? deactiveButtonStyle : 'none';
                }

                if (expressSheetUsersAllowed) {
                    // Show/hide 'Generate Express Sheet Selected Users' button
                    if (expressSheetButton) {
                        expressSheetButton.style.display = selectAllCheck ? expressSheetButtonStyle : 'none';
                    }
                }

                if (groupWideOperationsAllowed == "1") {
                    // Show/hide group-wide drop-down operations menu
                    //modificationMenu.style.display = selectAllCheck ? modificationMenuStyle : 'none';
                    modificationMenu.style.display = selectAllCheck ? deleteButtonStyle : 'none';

                }
                printSelectedUser();
            });


            // New CFS Criteria Dialog
            // This dialog with CFS Criteria form is opened to create new CFS Criteria
            // or to update or delete the existing CFS criteria.
            // -----------------------------------------------------------------------
            $("#newCriteriaCFSDialog").dialog({
                autoOpen: false,
                width: 1000,
                modal: true,
                title: "Selective Call Forwarding Criteria",
                position: {my: "top", at: "top"},
                resizable: false,
                closeOnEscape: false,
                buttons: {
                    Create: function () {
                        var formData = $("form#newCFSCriteriaForm").serialize();
                        var settings = {
                            type: "POST",
                            url: "users/CFSCriteria.php",
                            data: formData,
                            async: false
                        };
                        $.ajax(settings).done(function (result) {
                            result = result.trim();
                            if (result.slice(0, 1) == "1") {
                                alert("Error: " + result.slice(1));
                            } else {
                                $("#newCriteriaCFSDialog").dialog("close");
                                $("#cfsCriteriaTable").html(result.slice(1));
                                numCfsCriteria = getCfsCriteriaCount(result.slice(1));
                                numCfsCheckedCriteria = getActiveCfsCriteriaCount();
                                updateCFSSubmitButtonStatus();
                            }
                        });
                    },
                    Update: function () {
                        var formData = $("form#newCFSCriteriaForm").serialize();
                        var settings = {
                            type: "POST",
                            url: "users/CFSCriteria.php",
                            data: formData,
                            async: false
                        };
                        $.ajax(settings).done(function (result) {
                            result = result.trim();
                            if (result.slice(0, 1) == "1") {
                                alert("Error: " + result.slice(1));
                            } else {
                                $("#newCriteriaCFSDialog").dialog("close");
                                $("#cfsCriteriaTable").html(result.slice(1));
                            }
                        });
                    },
                    Delete: function () {
                        var settings = {
                            type: "POST",
                            url: "users/CFSCriteria.php",
                            data: {deleteCriteria: $("#cfsCriteriaName").val()},
                            async: false
                        };
                        $.ajax(settings).done(function (result) {
                            result = result.trim();
                            if (result.slice(0, 1) == "1") {
                                alert("Error: " + result.slice(1));
                            } else {
                                $("#newCriteriaCFSDialog").dialog("close");
                                $("#cfsCriteriaTable").html(result.slice(1));
                                numCfsCriteria = getCfsCriteriaCount(result.slice(1));
                                numCfsCheckedCriteria = getActiveCfsCriteriaCount();
                                updateCFSSubmitButtonStatus();
                            }
                        });
                    },
                    Close: function () {
                        $(this).dialog("close");
                    }
                },
                open: function () {
					setDialogDayNightMode($(this));
					$('.ui-dialog-buttonpane').find('button:contains("Create")').addClass('createButton');
					$('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('cancelButton');
					$('.ui-dialog-buttonpane').find('button:contains("Delete")').addClass('deleteButton');
					$('.ui-dialog-buttonpane').find('button:contains("Update")').addClass('subButton');
					
                    $(".ui-dialog-buttonpane button:contains('Create')").button('disable');
                    if (cfsCriteriaDialogState == "create") {
                        $(".ui-dialog-buttonpane button:contains('Create')").button().show();
                        $(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
                        $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
                    }
                    else if (cfsCriteriaDialogState == "modify") {
                        $(".ui-dialog-buttonpane button:contains('Create')").button().hide();
                        $(".ui-dialog-buttonpane button:contains('Delete')").button().show();
                        $(".ui-dialog-buttonpane button:contains('Update')").button().show();
                    }
                }
            });

            $(".ui-button-text").css({"font-size": +12 + "px"});


            // Invoked on clicking 'Add' button on Call Forwarding Selective page
            // to create new Call Forwarding Selective criteria
            // ------------------------------------------------------------------
            $("#newCriteriaCFS").click(function () {
                cfsCriteriaDialogState = "create";
                $.ajax({
                    type: "POST",
                    url: "users/newCFSCriteria.php",
                    success: function (result) {
                        $("#newCriteriaCFSDialog").dialog("open");
                        $(".ui-dialog").find("ui-widget-header").css("background", "darkgreen");
                        $("#newCriteriaCFSDialog").html(result);
                    }
                });
            });


            //-----------------------------------------
//         $("#allUsers").tablesorter();
            //$("#allUsersRegistration").tablesorter();
            //$(".viewDetailRegistration").hide();

            $('#viewUserTable').click(function () {

            });

            $('#viewUserRegistrationTable').click(function () {

                //setRegisterTableSorter();
            });

            //function to make the selected user download in csv
            var printSelectedUser = function () {
                $("#selectedUserForDownload").val('');
                var checkedUsers = "";
                $(".DTFC_LeftBodyLiner .checkUserListBox").each(function () {
                    var el = $(this);
                    var userId = el.attr("id");
                    if (el.is(":checked")) {
                        checkedUsers += userId + ";";
                    }
                });
                $("#selectedUserForDownload").val(checkedUsers);
            };
        });


        // get number of CFS criteria based on displayed criteria rows
        // -----------------------------------------------------------
        function getCfsCriteriaCount(str) {
            var arr = str.split("<tr id");
            var c = arr.length > 0 ? arr.length - 1 : 0;
            return arr.length > 0 ? arr.length - 1 : 0;
        }


        // count number of active CFS criteria
        // -----------------------------------
        function getActiveCfsCriteriaCount() {
            if (numCfsCriteria == 0) {
                return 0;
            }

            var numCheckedCriteria = 0;
            var currentCriteriaCount = 0;

            while (currentCriteriaCount < numCfsCriteria) {
                var id = "cfsCritChk" + currentCriteriaCount;
                var element = document.getElementById(id);

                if (element.type && element.type === "checkbox") {
                    if (element.checked) {
                        numCheckedCriteria++;
                    }
                }

                currentCriteriaCount++;
            }

            return numCheckedCriteria;
        }


        // collect active status of all CFS criteria
        function getCFSCriteriaActiveStatus() {
            var result = [];
            var index = 0;

            while (index < numCfsCriteria) {
                var id = "cfsCritChk" + index;
                var element = document.getElementById(id);

                if (element.type && element.type === "checkbox") {
                    result[index] = element.checked ? "true" : "false";
                }

                index++;
            }
            return result;
        }


        // Process results processed by checkData.php module that is generating
        // verification data for group-wide modification processes
        // ---------------------------------------------------------------------

        function vmProcessGroupWideModificationCheckDataResult(result) {
            var result = JSON.parse(result);
            var unAssignErrorTable = "";
            var assingnErrorTable = "";
            var confErrorTable = "";

            var module = "";

// 		if(value.assignedServicePack) { module = "Service Assignment and Configuration Successfull.";}
// 		else if(value.unAssignedServicePack) { module = "Service UnAssigned Successfull";}

            //if(result.length > 0){
            var backgroundColor = 'background:#D52B1E; width:50%';

            for (var i = 0; i < result.length; i++) {
                var value = result[i];
                var userId = result[i].userId;
                if (value.assignedServicePack && value.assignedServicePack.status != "Error") {
                    module = "Service Assignment and Configuration Successfull.";
                }
                else if (value.unAssignedServicePack && value.unAssignedServicePack.status != "Error") {
                    module = "Service UnAssigned Successfull.";
                }

                if (value.assignedServicePack && value.assignedServicePack.status == "Error") {
                    var errorMessage = value.assignedServicePack.errorMsg;
                    assingnErrorTable += '<tr><td class="errorTableRows" style="' + backgroundColor + '">' + userId + '</td><td class="errorTableRows">' + errorMessage + ' </td></tr>';
                }
                if (value.configuration && value.configuration.status == "Error") {
                    var errorMessage = value.configuration.errorMsg;
                    confErrorTable += '<tr><td class="errorTableRows" style="' + backgroundColor + '">' + userId + '</td><td class="errorTableRows">' + errorMessage + ' </td></tr>';
                }
                if (value.unAssignedServicePack && value.unAssignedServicePack.status == "Error") {
                    var errorMessage = value.unAssignedServicePack.errorMsg;
                    unAssignErrorTable += '<tr><td class="errorTableRows" style="' + backgroundColor + '">' + userId + '</td><td class="errorTableRows">' + errorMessage + ' </td></tr>';
                }

            }

            $("#dialogGroupWideModify").html("");

            if (assingnErrorTable != "") {
                $("#dialogGroupWideModify").append('<table><tr><td colspan="2" style="width:100%"> Error in VM Assign Service</td></tr>');
                $("#dialogGroupWideModify").append(assingnErrorTable);
                $("#dialogGroupWideModify").append('</table> </br>');
            }
            else if (unAssignErrorTable != "") {
                $("#dialogGroupWideModify").append('<table><tr><td colspan="2"> Error in VM UnAssign Service For Following Users </td></tr>');
                $("#dialogGroupWideModify").append(unAssignErrorTable);
                $("#dialogGroupWideModify").append('</table> </br>');
            }
            else if (confErrorTable != "") {
                $("#dialogGroupWideModify").append('<table><tr><td colspan="2"> Error in Service Configuration  </td></tr>');
                $("#dialogGroupWideModify").append(confErrorTable);
                $("#dialogGroupWideModify").append('</table> </br>');
            }

            $("#dialogGroupWideModify").append(module);

            $("#dialogGroupWideModify").dialog("open");
            $(":button:contains('Complete')").hide();
            $(":button:contains('Cancel')").hide();
            $(":button:contains('More Changes')").show();
        }


        function processGroupWideModificationCheckDataResult(result) {
            
            $("html, body").animate({ scrollTop: 0 }, 0); //Code added @07 Feb 2019
            $("#dialogGroupWideModify").dialog("open");
            result = result.trim();
            if (result.slice(0, 1) == 1) {
                $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
            }
            else {
                $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
            }
            $("#dialogGroupWideModify").html(result.slice(1));
            $(":button:contains('Complete')").show();
            $(":button:contains('Cancel')").show();
            $(":button:contains('More Changes')").hide();
            $(":button:contains('Return To Main')").hide();
        }


        // Invoked by onClick event from any checkbox in users rows
        //---------------------------------------------------------
        function onUserSelect(checkbox) {
            var selectAllCheckBox = document.getElementById("selectAllChk");
            selectAllCheckBox.checked = false;

            // if User is clicked, create array element with userID as the key
            var userId = checkbox.name;
            if (!selectedUsers.hasOwnProperty(userId)) {
                selectedUsers[userId] = false;
            }
            selectedUsers[userId] = checkbox.checked;

            // determine if there are any row check boxes checked
            var userChecked = false;
            for (var key in selectedUsers) {
                if (selectedUsers.hasOwnProperty(key) && selectedUsers[key]) {
                    userChecked = true;
                    break;
                }
            }

            // Make sure that Modification Forms are hidden if no checkbox is selected
            // And dropdown Modification Forms selection is not selecting any form
            if (!userChecked) {
                if($("#gwMods").length) {
                    $("#gwMods option")[0].selected = true;
                }
                disableAllModificationForms();
            }

            // Show 'Delete Selected Users' button as long as there is one row checkbox checked
            if (deleteUsersAllowed) {
                if (deleteButton) {
                    deleteButton.style.display = userChecked ? deleteButtonStyle : 'none';
                }
            }
            if (expressSheetUsersAllowed) {
                if (expressSheetButton) {
                    expressSheetButton.style.display = userChecked ? expressSheetButtonStyle : 'none';
                }
            }
            //downloadCSVButton.style.display = userChecked ? downloadCSVButtonStyle : 'none';
            if (groupWideOperationsAllowed == "1") {
                // Show/hide group-wide drop-down operations menu
                modificationMenu.style.display = userChecked ? modificationMenuStyle : 'none';
            }
            //activeButton.style.display = userChecked ? activeButtonStyle : 'none';
            //deactiveButton.style.display = userChecked ? deactiveButtonStyle : 'none';
        }


        // Invoked by onChange event from a group-wide user modification drop-down menu
        //-----------------------------------------------------------------------------
        function onMenuSelectionChange(select) {
            $("#groupWideModifyUser").hide();
            var optVal = select.options[select.selectedIndex].value;

            if (optVal == "gwModsNone") {
                disableAllModificationForms();
            }
            else if (optVal == "gwModsCFA") {
                $("div[id^=div]").hide();
                $("#divCFA").show();
                $("#phSipFwdCFA").val("");
                $("#ringReminderChk").prop("checked", false);
            }
            else if (optVal == "gwModsCFNR") {
                $("div[id^=div]").hide();
                $("#divCFNR").show();
                $("#phSipFwdCFNR").val("");
            }
            else if (optVal == "gwModsCFS") {
                // Remove any stored CFS Criteria data
                $.ajax({
                    type: "POST",
                    url: "users/CFSCriteria.php",
                    data: {deleteAllCriteria: "yes"},
                    async: false
                });
                $("#cfsCriteriaTable").html("--No Criteria--");

                $("div[id^=div]").hide();
                $("#divCFS").show();
                $("#phSipFwdCFS").val("");
                $("#ringReminderChkCFS").prop("checked", false);
            }
            else if (optVal == "gwModsAuth") {
                $("div[id^=div]").hide();
                $("#divAuth").show();
                $("#authPassword1").val("");
                $("#authPassword2").val("");
                $("#genAuthPassword").prop("checked", false);
                $("#authPassword1CheckMark").hide();
                $("#authPassword2CheckMark").hide();
                $("#authGeneratedPassword").val("");
                $("#genAuthPassword").prop("checked", false);
            }
            else if (optVal == "gwModsSwPck") {
                $("div[id^=div]").hide();
                $("#divSwPck").show();
                $("#srvPack").val("srvPackNone");
                $("#assignSvcPackFalse").prop("checked", true);
                $("#usrServices").val("usrServiceNone");
                $("#assignUsrServiceFalse").prop("checked", true);

            } else if (optVal == "gwModsDnActivate") {
                $("div[id^=div]").hide();
                $("#divActivationNumber").show();
                $("#activateNumber").prop("checked", true);
            } else if (optVal == "gwModsVM") {
                $("div[id^=div]").hide();
                $("#groupWideModifyUser").show();
            } else if(optVal =="callingLineIDBlocking") {
            	$("div[id^=div]").hide();
            	$("#callingLineIDBlocking").show();
            	$("#blockCalling").prop("checked", true);
            	
            }
        }


        // Invoked by onChange event from Service Pack selection dropdown menu
        // that allows selecting service pack authenticated to a user
        // -------------------------------------------------------------------
        function onServicePackSelectionChange(select) {
            var optVal = select.options[select.selectedIndex].value;

            selectedServicePack = optVal == "srvPackNone" ? "" : optVal;
            submitServicesButton.style.display = (selectedServicePack != "" || selectedUserService != "") ?
                submitServicesButtonDisplayStyle : "none";
        }


        // Invoked by onChange event from User Services selection dropdown menu
        // that allows selecting user service authenticated to a user
        // --------------------------------------------------------------------
        function onUserServiceSelectionChange(select) {
            var optVal = select.options[select.selectedIndex].value;

            selectedUserService = optVal == "usrServiceNone" ? "" : optVal;
            submitServicesButton.style.display = (selectedServicePack != "" || selectedUserService != "") ?
                submitServicesButtonDisplayStyle : "none";
        }


        // Evaluate automatically generated password
        // Automatically generated password can be manually modified,
        // so it has to pass minimum password length test
        // ----------------------------------------------------------
        function evalAuthGeneratedPassword(textBox) {
            var length = textBox.value.length;
            if (textBox.value.length >= authMinLength) {
                $("#submitAuthentication").show();
            } else {
                $("#submitAuthentication").hide();
            }
        }

        // Invoked by onChange event from 'Generate Password' checkbox
        // to generateAuthentication Password on 'Reset Authentication form
        // ----------------------------------------------------------------
        function onGeneratePasswordSelect(checkbox) {
            var authPassword = "<?php echo setAuthPassword(); ?>";
            document.getElementById("authGeneratedPassword").value = authPassword;

            if (checkbox.checked) {
                $("#typedPassword").hide();
                $("#generatedPassword").show();
                $("#passwordRetype").hide();

                document.getElementById("authPassword1").value = "";
                document.getElementById("authPassword2").value = "";
                $("#authPassword1CheckMark").hide();
                $("#authPassword2CheckMark").hide();

                hasAuthenticationPassword = true;
                $("#submitAuthentication").show();
            } else {
                $("#generatedPassword").hide();
                $("#typedPassword").show();
                $("#passwordRetype").show();

                document.getElementById("authGeneratedPassword").value = "";

                hasAuthenticationPassword = false;
                $("#submitAuthentication").hide();
            }
        }


        // Evaluate the content of phone number entry box in Call Forwarding forms
        // and hide/show submit button on the form based on the content of phone number box
        // and Enable/Disable radio status of a Call Forwarding service
        //---------------------------------------------------------------------------------
        function evalFwdNum(txtBox) {

            // Determine which submit button needs to be controlled
            var service = "";
            if (txtBox.id.indexOf("CFA") != -1) {
                service = "CFA";
            } else if (txtBox.id.indexOf("CFNR") != -1) {
                service = "CFNA";
            } else {
                return;
            }


            var submitButton = submitCFNRButton;
            var submitDisplayStyle = submitCFNRButtonDisplayStyle;
            var activateRadioId = (service == "CFA") ? "activateCFATrue" : "activateCFNRTrue";
            // var activateCheckBoxIdVEA = (service == "VEA") ? "sendCarbonCopyVoiceMessage" : "";
            var activateService = document.getElementById(activateRadioId).checked;
            // var activateService = document.getElementById(activateCheckBoxIdVEA).checked;
            //var activateService = document.getElementById(activateCheckBoxIdDN).checked;

            if (service == "CFA") {
                submitButton = submitCFAButton;
                submitDisplayStyle = submitCFAButtonDisplayStyle;
            }

            var hasText = txtBox.value.length > 0;
            submitButton.style.display = (activateService && !hasText) ? "none" : submitDisplayStyle;
        }


        // Evaluate Authentication Passwords: Password1 and Password2 fields
        // Must comply with authentication password minimum length
        // -----------------------------------------------------------------
        function evalAuthPassword(txtBox) {
            var pass1 = $("#authPassword1").val();
            var pass2 = $("#authPassword2").val();
            if (pass1.length >= authMinLength) {
                $("#authPassword1CheckMark").show();

                if (pass2 === pass1) {
                    $("#authPassword2CheckMark").show();
                    hasAuthenticationPassword = true;
                    $("#submitAuthentication").show();
                }
                else {
                    $("#authPassword2CheckMark").hide();
                    hasAuthenticationPassword = false;

                    $("#submitAuthentication").hide();
                }
            }
            else { // password 1 length less than minimum length of 3
                $("#authPassword1CheckMark").hide();
            }
        }


        // Evaluate Enable/Disable status of a Call Forwarding service on Call Forwarding forms
        // and hide/show submit button on the form based on Enable/Disable radio status
        // and the content of forwarding phone number on the Call Forwarding forms
        //-------------------------------------------------------------------------------------
        function evalActivation(radio) {
            var service = "";
            if (radio.id.indexOf("CFA") != -1) {
                service = "CFA";
            } else if (radio.id.indexOf("CFNR") != -1) {
                service = "CFNR";
            } else {
                return;
            }
            var chkYes = document.getElementById("voiceMessagingYes");
            var dvPassport = document.getElementById("dvPassport");
            if (dvPassport && chkYes) {
                dvPassport.style.display = chkYes.checked ? "block" : "none";
            }
            var currActivateServiceId = radio.name + "True";

            var submitButton = submitCFNRButton;
            var submitDisplayStyle = submitCFNRButtonDisplayStyle;
            var fwdNumTextBoxId = (service == "CFA") ? "phSipFwdCFA" : "phSipFwdCFNR";
            var activateService = document.getElementById(currActivateServiceId).checked;

            if (service == "CFA") {
                submitButton = submitCFAButton;
                submitDisplayStyle = submitCFAButtonDisplayStyle;
            }

            var hasText = document.getElementById(fwdNumTextBoxId).value.length > 0;
            submitButton.style.display = (activateService && !hasText) ? "none" : submitDisplayStyle;
        }


        // This function controls enable/disable status of 'Submit' button for group-wide CFS modification
        // IF service activation is OFF THEN 'Submit' button is enabled
        // IF service activation is ON THEN enable status of 'Submit' button depends on status of
        // default call forwarding number and CFS criteria existence and criteria activation status
        // ----------------------------------------------------------------------------------------------
        function updateCFSSubmitButtonStatus() {
            var buttonEnabled = document.getElementById("activateCFSFalse").checked;
            if (!buttonEnabled) {
                buttonEnabled = document.getElementById("phSipFwdCFS").value.length > 0;
                if (buttonEnabled) {
                    buttonEnabled = numCfsCheckedCriteria > 0;
                }
            }

            document.getElementById("submitCFS").disabled = !buttonEnabled;
            // some browsers, eg Chrome do not gray out disabled controls
            submitCFSButton.style.color = buttonEnabled ? submitCFSButtonColorStyle : "gray";
        }


        // ------------
        // CFS Handlers
        // ------------

        // Invoked by onChange event from service activation radio buttons of Call Forwarding Selective
        // to update disable status of CFS service submit button
        // --------------------------------------------------------------------------------------------
        function evalActivationCFS(radio) {
            //var buttonEnabled = radio.id == "activateCFSFalse";
            updateCFSSubmitButtonStatus();
        }


        // Invoked by onChange and onInput events from forwarding number text box of Call Forwarding Selective
        // to update disable status of CFS service submit button
        // ---------------------------------------------------------------------------------------------------
        function evalCFSFwdNum(text) {
            updateCFSSubmitButtonStatus();
        }


        // -------------------------
        // CFS Criteria Row Handlers
        // -------------------------

        // CFS Criteria selector handler
        // This handler opens dialog form with information of a criteria row that
        // has been clicked on the Call Forwarding Selective page
        // ----------------------------------------------------------------------
        function evalCriteriaRow(row) {
            var numChecked = getActiveCfsCriteriaCount();

            // Check whether row selection event triggered because of checking/unchecking checkbox
            if (numChecked != numCfsCheckedCriteria) {
                numCfsCheckedCriteria = numChecked;

                // update disable status of CFS Submit button
                updateCFSSubmitButtonStatus();

                // update criteria active status
                var activeStatus = getCFSCriteriaActiveStatus();
                $.ajax({
                    type: "POST",
                    url: "users/CFSCriteria.php",
                    data: {activeStatus: activeStatus.join(",")},
                    async: false
                });

                return;
            }

            // row selection event has triggered because of criteria row selection
            cfsCriteriaDialogState = "modify";
            $.ajax({
                type: "POST",
                url: "users/newCFSCriteria.php",
                data: {criteriaName: row.id},
                success: function (result) {
                    $("#newCriteriaCFSDialog").dialog("open");
                    $(".ui-dialog").find("ui-widget-header").css("background", "darkgreen");
                    $("#newCriteriaCFSDialog").html(result);
                }
            });
        }


        // CFS Criteria highlighter
        // This handler highlights a CFS criteria row on mouse-over action
        // ---------------------------------------------------------------
        function highlightCriteriaRow(row, highlight) {
            row.style.backgroundColor = highlight ? "#dcfac9" : "white";
        }


        // Add all new group-wide action form
        //-----------------------------------
        function disableAllModificationForms() {
            $("#divCFA").hide();
            $("#divCFNR").hide();
            $("#divCFS").hide();
            $("#divAuth").hide();
            $("#divSwPck").hide();
            $("#divActivationNumber").hide();
            $("#groupWideModifyUser").hide();
            $("#callingLineIDBlocking").hide();
        }

        // Build list of all selected users
        // --------------------------------
        function getSelectedUsers() {
            var list = "";

            for (var id in selectedUsers) {
                if (selectedUsers.hasOwnProperty(id) && selectedUsers[id]) {
                    list += id + "+-+-+";
                }
            }

            return list;
        }

        $(function () {

           $(document).on("mouseover", "#usersTableDiv table .userIdVal, .userIdValDisable", function()
    		   	{
					var className = $(this).data("class");
    				if($(this).hasClass("registerUsr")) {
    					$(this).removeClass("registerUsr");
    					$(this).addClass("tempRegisterUsr");

    					$("." + className).removeClass("registerUsr");
    					$("." + className).addClass("tempRegisterUsr");
    					
    					$(".DTFC_LeftBodyWrapper ." + className).removeClass("registerUsr");
    					$(".DTFC_LeftBodyWrapper ." + className).addClass("tempRegisterUsr");
    				}
					if($(this).hasClass("unRegisterUsr")) {
    					$(this).removeClass("unRegisterUsr");
    					$(this).addClass("tempunRegisterUsr");

    					$("." + className).removeClass("unRegisterUsr");
    					$("." + className).addClass("tempunRegisterUsr");
    					
    					$(".DTFC_LeftBodyWrapper ." + className).removeClass("unRegisterUsr");
    					$(".DTFC_LeftBodyWrapper ." + className).addClass("tempunRegisterUsr");
    				}
					 
					
    				$(this).find("td").css("background-color", "#dcfac9 ", "important");
    		    	$("." + className).find("td").css("background-color", "#dcfac9", "important");
    		    	$(".DTFC_LeftBodyWrapper ." + className).find("td").removeClass("sorting_1");
    		    	$(".DTFC_LeftBodyWrapper ." + className).find("td").css("background-color", "#dcfac9", "important");
    								
    			});

            $(document).on("mouseleave", "#usersTableDiv table .userIdVal, .userIdValDisable", function()
    			{
					var className = $(this).data("class");
    		        if($(this).hasClass("tempRegisterUsr")) {
    		        	$(this).removeClass("tempRegisterUsr");
    		        	$(this).addClass("registerUsr");
    		        	
    		        	$(".DTFC_LeftBodyWrapper ." + className).removeClass("tempRegisterUsr");
    		        	$(".DTFC_LeftBodyWrapper ." + className).addClass("registerUsr");
    		        
    		        	$("." + className).removeClass("tempRegisterUsr");
    		        	$("." + className).addClass("registerUsr");
    		        }
					if($(this).hasClass("tempunRegisterUsr")) {
    		        	$(this).removeClass("tempunRegisterUsr");
    		        	$(this).addClass("unRegisterUsr");
    		        	
    		        	$(".DTFC_LeftBodyWrapper ." + className).removeClass("tempunRegisterUsr");
    		        	$(".DTFC_LeftBodyWrapper ." + className).addClass("unRegisterUsr");
    		        
    		        	$("." + className).removeClass("tempunRegisterUsr");
    		        	$("." + className).addClass("unRegisterUsr");
    		        }
					

    		       // $(this).find("td").css("background-color", "#F5D0C3 ", "important");
    		       // $("." + className).find("td").css("background-color", "#F5D0C3 ", "important");
    		       // $(".DTFC_LeftBodyWrapper ." + className).find("td").addClass("sorting_1");
    				//$(".DTFC_LeftBodyWrapper ." + className).find("td").css("background-color", "#F5D0C3 ", "important");

    		   	});

            /*
		$(document).on("mouseover", "#usersTableDiv table .userIdVal", function()
		{
			debugger;
			var rowName = $(this).attr("name");
			var rowElements = document.getElementsByClassName(rowName);
//         	rowElements.style.background-color = "#dcfac9 !important";
		});
		*/

            
            $(document).on("click", "#usersTableDiv .userIdVal", function (event) {
                //return false; // Remove after testing
                if (!(event.target.nodeName == "SPAN" || event.target.nodeName == "INPUT")) {
                    var userId = $(this).attr("data-id");
                    /*
                    var userIdValue1 = "";
                    var fname = $(this).attr("data-firstname");
                    var lname = $(this).attr("data-lastname");
                    var extension = $(this).attr("data-extension");
                    var phone = $(this).attr("data-phone");
                    if ($.trim(phone) !== "") {
                        userIdValue1 = phone + "x" + extension;
                    } else {
                        userIdValue1 = userId;
                    }
                    var userIdValue = userIdValue1 + " - " + lname + ", " + fname;
                    */
                    var userIdValue = userId;

                    $.ajax({
                        type: "POST",
                        url: "userMod/userMod.php",
                        //data: { searchVal: userId, userFname : fname, userLname : lname },
                        success: function (result) {
                            /*$(".spacerHide").hide();
							$("#usersTable").hide();
							$(".loadingRemove").remove();
							$("#usersBanner").hide();

							$("#userData").html(result);*/
                            $("#mainBody").html(result);
                            $("#searchVal").val(userIdValue);

                            setTimeout(function () {
                                $("#go").trigger("click");
                            }, 2000);

                            $('#helpUrl').attr('data-module', "userMod");
                            activeImageSwapModify();
                        }

                    });
                }
            });


            if ("<?php echo $_SESSION['usersView_selected']; ?>" == "Detail") {
                //$("#viewUserTable").trigger("click");
            } else if ("<?php echo $_SESSION['usersView_selected']; ?>" == "Registration") {
                //$("#viewUserRegistrationTable").trigger("click");
            }
        });

	</script>
	<script type="text/javascript">
        var activeImageSwapExpress = function () {

            previousActiveMenu = "users";
            currentClickedDiv = "expressSheets";

            //active
            if (previousActiveMenu != "") {
                $(".navMenu").removeClass("activeNav");
                var $thisPrev = $("#users").find('.ImgHoverIcon');
                var newSource = $thisPrev.data('alt-src');
                $thisPrev.data('alt-src', $thisPrev.attr('src'));
                $thisPrev.attr('src', newSource);

            }
            // inactive tab
            if (currentClickedDiv != "") {
                $("#expressSheets").addClass("activeNav");
                var $thisPrev = $("#expressSheets").find('.ImgHoverIcon');
                var newSource = $thisPrev.data('alt-src');
                $thisPrev.data('alt-src', $thisPrev.attr('src'));
                $thisPrev.attr('src', newSource);
                previousActiveMenu = currentClickedDiv;
            }

        }
	</script>
	<!-- sks code -->
	<script>

        var activeImageSwapModify = function () {
            previousActiveMenu = "users";
            currentClickedDiv = "userMod";

            //active
            if (previousActiveMenu != "") {
                $("#userMod").removeClass("activeNav");
                $(".navMenu").removeClass("activeNav");
                var $thisPrev = $("#users").find('.ImgHoverIcon');
                var newSource = $thisPrev.data('alt-src');
                $thisPrev.data('alt-src', $thisPrev.attr('src'));
                $thisPrev.attr('src', newSource);

            }
            // inactive tab
            if (currentClickedDiv != "") {
                $("#userMod").addClass("activeNav");
                var $thisPrev = $("#userMod").find('.ImgHoverIcon');
                var newSource = $thisPrev.data('alt-src');
                $thisPrev.data('alt-src', $thisPrev.attr('src'));
                $thisPrev.attr('src', newSource);
                previousActiveMenu = currentClickedDiv;

            }

        }

	</script>
	
	<script>
        var phoneNumber;
        var macAddress;
        var remoteOffice;
        
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();

            $('#downloadCSV').click(function () {

                var btn_text = $(this).val();
                $(this).val('Downloading ...');

                var titles = [];
                var TableData = [];
                var i = 0;

                $('#usersDetailedListTable th').each(function () {    //table id here
                    if (i > 0) {
                        titles.push($(this).text());
                    }
                    i++;
                });


                if (usersDetailedListTable_DT) {
                    var data = usersDetailedListTable_DT.rows().data().toArray();
                    $.each(data, function (index, row) {
                        //console.log(row);
                        $.each(row, function (c_index, value) {
                            if (c_index !== 0) {
                                //TableData.push(value);
                                if(c_index == 5 && value != ""){
                                    phoneNumber = " "+value;
                                    TableData.push(phoneNumber);
                                }else if(c_index == 10 && value != ""){
                                    macAddress = '="'+value.toString()+'"';
                                    TableData.push(macAddress);
                                }else if(c_index == 17 && value != ""){
                                    remoteOffice = '="'+value.toString()+'"';
                                    TableData.push(remoteOffice);
                                }else{                                    
                                    if(value.indexOf(",")>0){
                                        TableData.push('"'+value+'"');
                                    }else{
                                        TableData.push(value);
                                    }
                                }
                            }
                        });
                    });
                }

                var CSVString = prepCSVRow(titles, titles.length, '');
                CSVString = prepCSVRow(TableData, titles.length, CSVString);
                
                var downloadLink = document.createElement("a"); //new line added 
                //var blob = new Blob(["\ufeff", CSVString]);
                var blob = new Blob([CSVString], { type: 'text/csv;charset=utf-8;' });
                var url = URL.createObjectURL(blob);
                downloadLink.href = url;
                downloadLink.download = "<?php echo $_SESSION['groupId']?>_USERS_DEATILED" + ".csv";
                var fileName = "<?php echo $_SESSION['groupId']?>_USERS_DEATILED" + ".csv";
               // var blob = new Blob(["\ufeff", CSVString]);
                if (navigator.msSaveBlob) { // IE 10+
                	navigator.msSaveBlob(blob, fileName);
                }else{
                	var downloadLink = document.createElement("a");
                    downloadLink.href = url;
                    downloadLink.download = fileName;

                    document.body.appendChild(downloadLink);
                    downloadLink.click();
                    document.body.removeChild(downloadLink);
                    $(this).val(btn_text);
                }

                

            });

        });
	</script>
	<?php
} else {
	?>
	<script type="text/javascript">
        showLoader();
	</script>
	<?php
}
?>
