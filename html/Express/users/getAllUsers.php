<?php
include("/var/www/lib/broadsoft/adminPortal/getUserEndpointDeviceName.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
const ExpressCustomProfileName = "%Express_Custom_Profile_Name%";

/*
 * Get group's Access Device from BroadWorks
 */
function getGroupAccessDevice($ociVersion, $sessionId, $deviceName, $client)
{
	$xmlinput =  xmlHeader($sessionId, "GroupAccessDeviceGetRequest18sp1");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));

    return new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
}

/*
 * Get BroadWorks Polycom User Service attributes
 */
function getUserPolycomPhoneServicesAttributes($sessionId, $userId, $deviceName, $client)
{
    $xmlinput = xmlHeader($sessionId, "UserPolycomPhoneServicesGetRequest");
    $xmlinput .= "<userId>" . $userId . "</userId>";
    $xmlinput .= "<accessDevice>
						<deviceLevel>Group</deviceLevel>
						<deviceName>" . $deviceName . "</deviceName>
					</accessDevice>";
    $xmlinput .= xmlFooter();
    $response = $client->processOCIMessage(array("in0" => $xmlinput));

    return new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
}

function getAllUserOfDevice($serviceProviderId, $groupId, $deviceName)
{
	global $sessionid, $client;
	$allUserOfDevice = array();
	$returnResponse["Error"] = "";
	$returnResponse["Success"] = "";
	
	$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetUserListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProviderId). "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
	$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	return strval($xml->command->deviceUserTable->row->col[6]);
	
}

	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

    $numberOfUsersToDelete = 0;

	$xmlinput = xmlHeader($sessionid, "UserGetListInGroupRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<GroupId>" . htmlspecialchars($_SESSION["groupId"]) . "</GroupId>";
	if (isset($_POST["department"]) && strlen($_POST["department"]) > 0)
	{
		$xmlinput .= "<searchCriteriaExactUserDepartment><departmentKey xsi:type=\"GroupDepartmentKey\">";
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<name>" . $_POST["department"] . "</name>";
		$xmlinput .= "</departmentKey></searchCriteriaExactUserDepartment>";
	}
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	$c = 0;
	$users_SCA_Appearances = array();
	$users_BLF_Monitored = array();
	$users = array();
	foreach ($xml->command->userTable->row as $key => $value)
	{
		$userId = strval($value->col[0]);
		$firstName = strval($value->col[2]);
		$lastName = strval($value->col[1]);
		$phoneNumber = strval($value->col[4]);
		if ($ociVersion == "17")
		{
			$extension = "N/A";
		}
		else
		{
			$extension = strval($value->col[10]);
		}
		$department = strval($value->col[3]);

		$users[$c]["userId"] = $userId;
		$users[$c]["firstName"] = $firstName;
		$users[$c]["LastName"] = $lastName;
		$users[$c]["extension"] = $extension;
		$users[$c]["department"] = $department;
		$users[$c]["phoneNumber"] = $phoneNumber;

		$xmlinput = xmlHeader($sessionid, "UserGetRegistrationListRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$b = 0;
		$deviceName = "";
		$userid = '';
		foreach ($xml->command->registrationTable->row as $key => $value)
		{
			$deviceName = strval($value->col[1]);
			$expiration = strval($value->col[4]);
			$epType = strval($value->col[6]);
			$deviceLevel = strval($value->col[0]);
			$publicIp = strval($value->col[7]);
			$agentType = strval($value->col[11]);

			$users[$c]["registration"][$b]["deviceName"] = $deviceName;
			$users[$c]["registration"][$b]["deviceLevel"] = $deviceLevel;
			$users[$c]["registration"][$b]["expiration"] = $expiration;
			$users[$c]["registration"][$b]["epType"] = $epType;
			$users[$c]["registration"][$b]["publicIp"] = $publicIp;
			$users[$c]["registration"][$b]["agentType"] = $agentType;

            $xml = getGroupAccessDevice($ociVersion, $sessionid, $deviceName, $client);
			$users[$c]["registration"][$b]["deviceType"] = strval($xml->command->deviceType);
			$users[$c]["registration"][$b]["macAddress"] = strval($xml->command->macAddress);

            $xml = getUserPolycomPhoneServicesAttributes($sessionId, $userId, $deviceName, $client);
			$users[$c]["registration"][$b]["integration"] = strval($xml->command->integratePhoneDirectoryWithBroadWorks);
			$users[$c]["registration"][$b]["ccd"] = strval($xml->command->groupCustomContactDirectory);

			$b++;
		}

        // Retrieve device info and Polycom phone service info for unregistered users
        if ($deviceName == "")
        {
            $users[$c]["deviceName"] = "";
            $users[$c]["deviceType"] = "";
            $users[$c]["portNumber"] = "";
            $users[$c]["macAddress"] = "";
            $users[$c]["integration"] = "";
            $users[$c]["ccd"] = "";
            $users[$c]["epType"] = "";

            $tempStr = getUserEndpointDeviceName($ociVersion, $sessionid, $userId, $client);
            $tmpArr  = explode('---',$tempStr);
            $deviceName = $tmpArr[0];
            $portNumber = $tmpArr[1];
            
            if ($deviceName != "")
            {
                $users[$c]["deviceName"] = $deviceName;
                $users[$c]["portNumber"] = $portNumber;

                $xml = getGroupAccessDevice($ociVersion, $sessionid, $deviceName, $client);
                $users[$c]["deviceType"] = strval($xml->command->deviceType);
                $users[$c]["macAddress"] = strval($xml->command->macAddress);

                $xml = getUserPolycomPhoneServicesAttributes($sessionId, $userId, $deviceName, $client);
                $users[$c]["integration"] = strval($xml->command->integratePhoneDirectoryWithBroadWorks);
                $users[$c]["ccd"] = strval($xml->command->groupCustomContactDirectory);
                $devDetails = getAllUserOfDevice($_SESSION['sp'], $_SESSION['groupId'], $deviceName);
                $users[$c]["epType"] = $devDetails;
            }
        }

        if(isset($extraUserInfo) && $extraUserInfo == 1) {

	        //get Voice Management info
	        $xmlinput = xmlHeader($sessionid, "UserAssignedServicesGetListRequest");
	        $xmlinput .= "<userId>" . $userId . "</userId>";
	        $xmlinput .= xmlFooter();
	        $response = $client->processOCIMessage(array("in0" => $xmlinput));
	        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	        $users[$c]["voiceMessagingUser"] = "false";
	        $users[$c]["busyLampField"] = "false";
	        $users[$c]["cfa"] = "false";
	        $users[$c]["cfb"] = "false";
	        $users[$c]["cfn"] = "false";
	        $users[$c]["cfr"] = "false";

	        $users[$c]["userServices"] = array();

	        for ($i = 0; $i < count($xml->command->userServiceEntry); $i++) {
		        $users[$c]["userServices"][] = strval($xml->command->userServiceEntry[$i]->serviceName);

		        if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Voice Messaging User") {
			        $users[$c]["voiceMessagingUser"] = "true";
		        }
		        if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Busy Lamp Field") {
			        $users[$c]["busyLampField"] = "true";
		        }
		        if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Call Forwarding Always") {
			        $users[$c]["cfa"] = "true";
			        $users[$c]["callControlTab"] = "true";
		        }
		        if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Call Forwarding Busy") {
			        $users[$c]["cfb"] = "true";
			        $users[$c]["callControlTab"] = "true";
		        }
		        if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Call Forwarding No Answer") {
			        $users[$c]["cfn"] = "true";
			        $users[$c]["callControlTab"] = "true";
		        }
		        if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Call Forwarding Not Reachable") {
			        $users[$c]["cfr"] = "true";
			        $users[$c]["callControlTab"] = "true";
		        }
	        }

	        $xmlinput = xmlHeader($sessionid, "UserThirdPartyVoiceMailSupportGetRequest17");
	        $xmlinput .= "<userId>" . $userId . "</userId>";
	        $xmlinput .= xmlFooter();
	        $response = $client->processOCIMessage(array("in0" => $xmlinput));
	        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	        $thirdParty = strval($xml->command->isActive); //must be true for Third Party users

	        $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetVoiceManagementRequest17");
	        $xmlinput .= "<userId>" . $userId . "</userId>";
	        $xmlinput .= xmlFooter();
	        $response = $client->processOCIMessage(array("in0" => $xmlinput));
	        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	        $voiceManagement = strval($xml->command->isActive); //must be false for Third Party users

	        if ($thirdParty == "true" and $voiceManagement == "false")
	        {
		        $users[$c]["thirdPartyVoiceMail"] = "true";
	        }

	        //get Third Party or Voice Management info
	        if (isset($users[$c]["thirdPartyVoiceMail"]) and $users[$c]["thirdPartyVoiceMail"] == "true")
	        {
		        $xmlinput = xmlHeader($sessionid, "UserThirdPartyVoiceMailSupportGetRequest17");
	        }
	        else
	        {
		        $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetVoiceManagementRequest17");
	        }
	        $xmlinput .= "<userId>" . $userId . "</userId>";
	        $xmlinput .= xmlFooter();
	        $response = $client->processOCIMessage(array("in0" => $xmlinput));
	        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	        $users[$c]['vm']["isActive"] = strval($xml->command->isActive);
	        $users[$c]['vm']["alwaysRedirectToVoiceMail"] = strval($xml->command->alwaysRedirectToVoiceMail);
	        $users[$c]['vm']["busyRedirectToVoiceMail"] = strval($xml->command->busyRedirectToVoiceMail);
	        $users[$c]['vm']["noAnswerRedirectToVoiceMail"] = strval($xml->command->noAnswerRedirectToVoiceMail);
	        if (isset($users[$c]["thirdPartyVoiceMail"]) and $users[$c]["thirdPartyVoiceMail"] == "true")
	        {
		        $users[$c]['vm']["serverSelection"] = strval($xml->command->serverSelection);
		        $users[$c]['vm']["userServer"] = strval($xml->command->userServer);
		        $users[$c]['vm']["mailboxIdType"] = strval($xml->command->mailboxIdType);
		        $users[$c]['vm']["mailboxURL"] = strval($xml->command->mailboxURL);
		        $users[$c]['vm']["noAnswerNumberOfRings"] = strval($xml->command->noAnswerNumberOfRings);
	        }
	        else
	        {
		        $users[$c]['vm']["processing"] = strval($xml->command->processing);
		        $users[$c]['vm']["voiceMessageDeliveryEmailAddress"] = strval($xml->command->voiceMessageDeliveryEmailAddress);
		        $users[$c]['vm']["usePhoneMessageWaitingIndicator"] = strval($xml->command->usePhoneMessageWaitingIndicator);
		        $users[$c]['vm']["sendVoiceMessageNotifyEmail"] = strval($xml->command->sendVoiceMessageNotifyEmail);
		        $users[$c]['vm']["voiceMessageNotifyEmailAddress"] = strval($xml->command->voiceMessageNotifyEmailAddress);
		        $users[$c]['vm']["sendCarbonCopyVoiceMessage"] = strval($xml->command->sendCarbonCopyVoiceMessage);
		        $users[$c]['vm']["voiceMessageCarbonCopyEmailAddress"] = strval($xml->command->voiceMessageCarbonCopyEmailAddress);
		        $users[$c]['vm']["transferOnZeroToPhoneNumber"] = strval($xml->command->transferOnZeroToPhoneNumber);
		        $users[$c]['vm']["transferPhoneNumber"] = strval($xml->command->transferPhoneNumber);
	        }

	        require_once ("/var/www/lib/broadsoft/adminPortal/users/VMUsersServices.php");
	        $vmAdvancedObj = new VMUsersServices();
	        $vmAdvancedGetResp = $vmAdvancedObj->modifyUsersVoiceMessagingAdvancedGetRequest($userId);
	        if(isset($vmAdvancedGetResp["Error"]) && $vmAdvancedGetResp["Error"] == ""){
		        $users[$c]['vm']["groupMailServerFullMailboxLimit"] = strval($vmAdvancedGetResp["Success"][0]);
	        }

	        //CFA CFB CFNA CFNB
	        $xmlinput = xmlHeader($sessionid, "UserCallForwardingAlwaysGetRequest");
	        $xmlinput .= "<userId>" . $userId . "</userId>";
	        $xmlinput .= "</command>";
	        $xmlinput .= "<command xsi:type=\"UserCallForwardingBusyGetRequest\" xmlns=\"\">";
	        $xmlinput .= "<userId>" . $userId . "</userId>";
	        $xmlinput .= "</command>";
	        $xmlinput .= "<command xsi:type=\"UserCallForwardingNoAnswerGetRequest13mp16\" xmlns=\"\">";
	        $xmlinput .= "<userId>" . $userId . "</userId>";
	        $xmlinput .= "</command>";
	        $xmlinput .= "<command xsi:type=\"UserCallForwardingNotReachableGetRequest\" xmlns=\"\">";
	        $xmlinput .= "<userId>" . $userId . "</userId>";
	        $xmlinput .= xmlFooter();
	        $response = $client->processOCIMessage(array("in0" => $xmlinput));
	        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	        $users[$c]["cfaActive"] = strval($xml->command[0]->isActive);
	        $users[$c]["cfbActive"] = strval($xml->command[1]->isActive);
	        $users[$c]["cfnActive"] = strval($xml->command[2]->isActive);
	        $users[$c]["cfrActive"] = strval($xml->command[3]->isActive);

	        //BLF
	        $xmlinput = xmlHeader($sessionid, "UserBusyLampFieldGetRequest16sp2");
	        $xmlinput .= "<userId>" . $userId . "</userId>";
	        $xmlinput .= xmlFooter();
	        $response = $client->processOCIMessage(array("in0" => $xmlinput));
	        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	        $users[$c]["hasBLFUsers"] = "false";

	        if (isset($xml->command->monitoredUserTable->row) && count($xml->command->monitoredUserTable->row) > 0) {
		        $users[$c]["hasBLFUsers"] = "true";

		        $a = 0;
		        foreach ($xml->command->monitoredUserTable->row as $key => $value) {
			        $users_BLF_Monitored[] = strval($value->col[0]);
			        $users[$c]["monitoredUsers"][$a]["id"] = strval($value->col[0]);
			        $users[$c]["monitoredUsers"][$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
			        $a++;
		        }
	        }

	        //SCA
	        $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetUserListRequest");
	        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
	        $xmlinput .= xmlFooter();
	        $response = $client->processOCIMessage(array("in0" => $xmlinput));
	        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	        $users[$c]["sharedCallAppearances"] = "false";

	        if (isset($xml->command->deviceUserTable->row) && count($xml->command->deviceUserTable->row) > 1) {
		        $users[$c]["sharedCallAppearances"] = "true";

		        $a = 0;
		        foreach ($xml->command->deviceUserTable->row as $key => $value) {
			        if (strval($value->col[6]) == "Shared Call Appearance") {
				        $users_SCA_Appearances[] = strval($value->col[4]);
			        }
			        $users[$c]["sharedCallAppearanceUsers"][$a]["id"] = strval($value->col[4]);
			        $users[$c]["sharedCallAppearanceUsers"][$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
			        $users[$c]["sharedCallAppearanceUsers"][$a]["phoneNumber"] = strval($value->col[3]);
			        $users[$c]["sharedCallAppearanceUsers"][$a]["extension"] = strval($value->col[9]);
			        $users[$c]["sharedCallAppearanceUsers"][$a]["department"] = strval($value->col[10]);
			        $users[$c]["sharedCallAppearanceUsers"][$a]["linePort"] = strval($value->col[0]);
			        $users[$c]["sharedCallAppearanceUsers"][$a]["port"] = strval($value->col[7]);
			        $users[$c]["sharedCallAppearanceUsers"][$a]["endpointType"] = strval($value->col[6]);
			        $users[$c]["sharedCallAppearanceUsers"][$a]["primary"] = strval($value->col[8]);

			        $a++;
		        }
	        }

	        // get Custom Profile & custom tags associated with this user
	        $users[$c]["customProfile"] = "None";
	        $users[$c]["customTags"] = array();
	        $users[$c]["customTagsShortDescriptions"] = array();
	        if ($users[$c]["deviceName"] != "") {
		        require_once("/var/www/html/Express/userMod/UserCustomTagManager.php");
		        $customTags = new UserCustomTagManager($users[$c]["deviceName"], $users[$c]["deviceType"]);
		        $users[$c]["customProfile"] = $customTags->getCustomTagValue(ExpressCustomProfileName);
		        if ($users[$c]["customProfile"] == "") {
			        $users[$c]["customProfile"] = "None";
		        }

		        // get custom tags from back-end
		        if (isset($customTags)) {
			        $users[$c]["customTags"] = $customTags->getUserCustomTagList();

			        if(isset($users[$c]["customTags"]) && isset($users[$c]["customTags"]) > 0) {
				        foreach ($users[$c]["customTags"] as $name => $value) {
					        $users[$c]["customTagsShortDescriptions"][] = $customTags->getShortDescription($name);
				        }
				        natsort($users[$c]["customTagsShortDescriptions"]);
			        }
		        }
	        }

	        $users[$c]["numberOfTags"] = 0;

        }

		$xmlinput = xmlHeader($sessionid, "UserDoNotDisturbGetRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		$users[$c]["DnD"] = strval($xml->command->isActive);

		$xmlinput = xmlHeader($sessionid, "UserCallForwardingAlwaysGetRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		$users[$c]["Fwd"]["active"] = strval($xml->command->isActive);
		$users[$c]["Fwd"]["FwdTo"] = strval($xml->command->forwardToPhoneNumber);

		$xmlinput = xmlHeader($sessionid, "UserRemoteOfficeGetRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$isSuccess = readErrorXmlGenuine($xml);
		if ($isSuccess != "Success" && !empty($isSuccess)) {
		   
		    $users[$c]["remote"]["active"] = "";
		    $users[$c]["remote"]["number"] = "";
		}else{
		   
		    $users[$c]["remote"]["active"] = strval($xml->command->isActive);
		    $users[$c]["remote"]["number"] = strval($xml->command->remoteOfficePhoneNumber);
		}

		$xmlinput = xmlHeader($sessionid, "UserServiceGetAssignmentListRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		$spInd = 0;
		foreach ($xml->command->servicePacksAssignmentTable->row as $key => $value)
		{
			if ($value->col[1] == "true")
			{
				$users[$c]["servicePack"][$spInd++] = strval($value->col[0]);
			}
		}

		$c++;
	}

	//Get unique user ids
	$users_BLF_Monitored = array_unique($users_BLF_Monitored);
	$users_SCA_Appearances = array_unique($users_SCA_Appearances);

?>
