<?php
require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
require_once ("/var/www/lib/broadsoft/adminPortal/SurgeMailNameBuilder.php");
require_once ("/var/www/lib/broadsoft/adminPortal/VoicePasswordNameBuilder.php");
require ("/var/www/lib/broadsoft/adminPortal/UserGetRequest.php");

include ("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig.php");

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/html/Express/util/formDataUtil.php");
require_once ("/var/www/lib/broadsoft/adminPortal/users/VMUsersServices.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
require_once("/var/www/lib/broadsoft/adminPortal/UserServiceGetAssignmentList.php");

require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailOperations.php"); //Added 12 May 2018
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php"); // Added on 25 may 2018
require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
$expProvDB = new ExpressProvLogsDB();
$GLOBALS['expressProvLogsDb'] = $expProvDB->expressProvLogsDb;

$vmServiceObj = new VMUsersServices();
const cfaService = "Call Forwarding Always";
const cfnrService = "Call Forwarding Not Reachable";
const cfsService = "Call Forwarding Selective";

$result = "";
$isLocalError = false;
$userAssignedUserServices = array();

server_fail_over_debuggin_testing(); /* for fail Over testing. */

function addError($errMsg)
{
    global $result, $isLocalError;
    
    $result .= $result == "" ? "" : "<br/>";
    $result .= $errMsg;
    $isLocalError = true;
}
if ($_POST["action"] == "modifyVoiceMessaging") {
    
    unset($_SESSION["userMOdifydVoiceMgt"]);
    $formDataObj = new FormDataUtil();
    $postArray = $formDataObj->getFormDataNameValue($_POST["formData"]);
    /*if (! array_key_exists("usePhoneMessageWaitingIndicator", $postArray)) {
        $postArray["usePhoneMessageWaitingIndicator"] = "false";
    }*/
    $userList = getUsers();
    $userAssign = array();
    $userUnAssign = array();
    $_SESSION["userMOdifydVoiceMgt"] = array();
    
    
    
    if (isset($postArray["voiceMessaging"]) && $postArray["voiceMessaging"] == "true") {
        if (count($userList) > 0) {
            if(!isset($postArray['busyRedirectToVoiceMail'])) {
                $postArray['busyRedirectToVoiceMail'] = "false";
            }
            if(!isset($postArray['noAnswerRedirectToVoiceMail'])) {
                $postArray['noAnswerRedirectToVoiceMail'] = "false";
            }
            /*if(!isset($postArray['usePhoneMessageWaitingIndicator'])) {
                $postArray['usePhoneMessageWaitingIndicator'] = "false";
            }*/
            if(!isset($postArray['sendCarbonCopyVoiceMessage'])) {
                $postArray['sendCarbonCopyVoiceMessage'] = "false";
            }
            if(!isset($postArray['transferOnZeroToPhoneNumber'])) {
                $postArray['transferOnZeroToPhoneNumber'] = "false";
            }
            if(!isset($postArray['alwaysRedirectToVoiceMail'])) {
                $postArray['alwaysRedirectToVoiceMail'] = "false";
            }
            $userAssignServiceResponse = array();
            foreach ($userList as $key => $value) {
                $userAssign = getTextUserServicesAll($value);
                if (! in_array('Voice Messaging User', $userAssign)) {
                    $isGetuserServicePackAssign = getuserServicePackAssign($value);
                    if (empty($isGetuserServicePackAssign["Error"])) {
                        $isVoiceProcesConf = $vmServiceObj->modifyGroupWideUsersVoiceMessaging($value, $postArray);
                        if (empty($isVoiceProcesConf["Error"])) {
                            $isConfigurationDone = $vmServiceObj->modifyUsersVoiceMessagingAdvancedServices($value, $postArray);
                            if (empty($isConfigurationDone["Error"])) {
                                $_SESSION["userMOdifydVoiceMgt"][$value]["configuration"]["status"] = "Success";
                            } else {
                                $_SESSION["userMOdifydVoiceMgt"][$value]["configuration"]["status"] = "Error";
                                $_SESSION["userMOdifydVoiceMgt"][$value]["configuration"]["errorMsg"] = $isConfigurationDone["Error"];
                            }
                        } else {
                            $_SESSION["userMOdifydVoiceMgt"][$value]["configuration"]["status"] = "Error";
                            $_SESSION["userMOdifydVoiceMgt"][$value]["configuration"]["errorMsg"] = $isVoiceProcesConf["Error"];
                        }
                    }
                }
                
            }
        }
    }
    else {
        foreach ($userList as $key => $userId) {
            $userAssign = getTextUserServicesAll($userId);
            if (in_array('Voice Messaging User', $userAssign["Success"])) {
                $userUnassignResponse =  getuserServicePackUnAssign($userId);
                if(!empty($userUnassignResponse["Error"])) {
                    $_SESSION["userMOdifydVoiceMgt"][$userId]["unAssignedServicePack"]["status"] = "Error";
                    $_SESSION["userMOdifydVoiceMgt"][$userId]["unAssignedServicePack"]["errorMsg"] = $userUnassignResponse["Error"];
                } else {
                    $_SESSION["userMOdifydVoiceMgt"][$userId]["unAssignedServicePack"]["status"] = "Success";
                    $_SESSION["userMOdifydVoiceMgt"][$userId]["unAssignedServicePack"]["errorMsg"] = "";
                }
            } else {
                $_SESSION["userMOdifydVoiceMgt"][$userId]["unAssignedServicePack"]["status"] = "Success";
                $_SESSION["userMOdifydVoiceMgt"][$userId]["unAssignedServicePack"]["errorMsg"] = "No VM Service avilable for unasign.";
            }
        }
    }
    
    $resultNew = array();
    $z = 0;
    foreach ($_SESSION["userMOdifydVoiceMgt"] as $key3 => $val3) {
        
        $resultNew[$z]["userId"] = $key3;
        if (isset($val3["assignedServicePack"])) {
            $resultNew[$z]["assignedServicePack"] = $val3["assignedServicePack"];
        }
        if (isset($val3["configuration"])) {
            $resultNew[$z]["configuration"] = $val3["configuration"];
        }
        
        if (isset($val3["unAssignedServicePack"])) {
            $resultNew[$z]["unAssignedServicePack"] = $val3["unAssignedServicePack"];
        }
        $z ++;
    }
    echo json_encode($resultNew);
}

/* list of user */
function getUserServicesAll($userId)
{
    global $sessionid, $client;
    $xmlinput = xmlHeader($sessionid, "UserServiceGetAssignmentListRequest");
    $xmlinput .= "<userId>" . $userId . "</userId>";
    $xmlinput .= xmlFooter();
    
    $response = $client->processOCIMessage(array(
        "in0" => $xmlinput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    $userServices = array();
    foreach ($xml->command->userServicesAssignmentTable->row as $key => $value) {
        if ($value->col[1] == "true") {
            $userServices[] = strval($value->col[0]);
        }
    }
    if ($value->col[1] == "true") {
        $servicepackServices = getServicePackServices($value->col[0]);
        foreach ($servicepackServices as $srv) {
            if (! in_array($srv, $userServices)) {
                $userServices[] = $srv;
            }
        }
    }
    return $userServices;
}

/* end list of user */
function getUsers()
{
    $users = array();
    
    $usersList = explode("+-+-+", $_POST["userList"]);
    for ($i = 0; $i < count($usersList); $i ++) {
        $userId = "";
        // Separator "*-*-*" is used to separate between posted user ID and device name
        $userAndDevice = explode("*-*-*", $usersList[$i]);
        if (isset($userAndDevice[0])) {
            $userId = $userAndDevice[0];
        }
        
        if (strlen($userId) > 0) {
            $users[] = $userId;
        }
    }
    return $users;
}

function getUsersAndDeviceName()
{
    $users = array();
    
    $usersList = explode("+-+-+", $_POST["userList"]);
    for ($i = 0; $i < count($usersList); $i ++) {
        $userId = "";
        $userDevice = "";
        
        $userAndDevice = explode("*-*-*", $usersList[$i]);
        if (isset($userAndDevice[0])) {
            $userId = $userAndDevice[0];
        }
        if (isset($userAndDevice[1])) {
            $userDevice = $userAndDevice[1];
        }
        
        if (strlen($userId > 0)) {
            $users[$i]['userId'] = $userId;
            $users[$i]['userDevice'] = $userDevice;
        }
    }
    return $users;
}

function getMacAddress($deviceName)
{
    global $ociVersion, $sessionid, $client;
    
    $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetRequest18sp1");
    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
    $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
    $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
    $xmlinput .= xmlFooter();
    $response = $client->processOCIMessage(array(
        "in0" => $xmlinput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    
    return strval($xml->command->macAddress);
}

function getResourceForVoicePasswordCreation($attr, $phoneNumber, $extension, $usrClid)
{
    switch ($attr) {
        case ResourceNameBuilder::DN:
            return $phoneNumber;
        case ResourceNameBuilder::EXT:
            return $extension;
        case ResourceNameBuilder::USR_CLID:
            return $usrClid;
        case ResourceNameBuilder::GRP_DN:
            return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
        case ResourceNameBuilder::GRP_ID:
            return $_SESSION["groupId"];
    }
}

function getResourceForSurgeMailNameCreation($attr, $phoneNumber, $extension, $usrClid, $firstName, $lastName, $deviceName, $userId)
{
    global $surgemailDomain;
    
    switch ($attr) {
        case ResourceNameBuilder::DN:
            return $phoneNumber;
        case ResourceNameBuilder::EXT:
            return $extension;
        case ResourceNameBuilder::USR_CLID:
            return $usrClid;
        case ResourceNameBuilder::GRP_DN:
            return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
        case ResourceNameBuilder::GRP_ID:
            return str_replace(" ", "_", trim($_SESSION["groupId"]));
        case ResourceNameBuilder::SYS_DFLT_DOMAIN:
            return $_SESSION["systemDefaultDomain"];
        case ResourceNameBuilder::DEFAULT_DOMAIN:
            return $_SESSION["defaultDomain"];
        case ResourceNameBuilder::SRG_DOMAIN:
            return $surgemailDomain;
        case ResourceNameBuilder::FIRST_NAME:
            return $firstName;
        case ResourceNameBuilder::LAST_NAME:
            return $lastName;
        case ResourceNameBuilder::DEV_NAME:
            return $deviceName;
        case ResourceNameBuilder::USR_ID:
            $nameSplit = explode("@", $userId);
            return $nameSplit[0];
            break;
        default:
            return "";
    }
}

// assign VM only Service Pack or VM Serivce
function assignService($userId, $serviceName, $ServicePackName)
{
    
    global $sessionid, $client;
    $returnData['Error'] = "";
    $assignResponse['Success'] = "";
    $xmlinput = xmlHeader($sessionid, "UserServiceAssignListRequest");
    $xmlinput .= "<userId>" . $userId . "</userId>";
    if ($serviceName != '') {
        $xmlinput .= "<serviceName>" . $serviceName . "</serviceName>";
    }
    if ($ServicePackName != '') {
        $xmlinput .= "<servicePackName>" . $ServicePackName . "</servicePackName>";
    }
    $xmlinput .= xmlFooter();
    $response = $client->processOCIMessage(array(
        "in0" => $xmlinput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
   
    if (readErrorXmlGenuine($xml) != "") {
        $returnData["Error"] = strval($xml->command->summaryEnglish);
    } else {
        $returnData["Success"] = $xml->command;
    }
    return $returnData;
}

// Unassign service or service pack
function unAssignService($userId, $serviceName, $ServicePackName)
{
    global $sessionid, $client;
    $unAssignService["Error"] = "";
    $unAssignService["Success"] = "";
    
    $xmlinput = xmlHeader($sessionid, "UserServiceUnassignListRequest");
    $xmlinput .= "<userId>" . $userId . "</userId>";
    if ($serviceName != '') {
        $xmlinput .= "<serviceName>" . $serviceName . "</serviceName>";
    }
    if ($ServicePackName != '') {
        $xmlinput .= "<servicePackName>" . $ServicePackName . "</servicePackName>";
    }
    $xmlinput .= xmlFooter();
    $response = $client->processOCIMessage(array(
        "in0" => $xmlinput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    if (readErrorXmlGenuine($xml) != "") {
        $unAssignService["Error"] = strval($xml->command->detail);
    } else {
        $unAssignService["Success"] = "Success";
    }
    
    return $unAssignService;
}

function checkHasPackWithVM($assignedPacks) {
    $packDetail = array();
    
    foreach($assignedPacks as $key=>$packName ) {
        $servicepackServices = getServicePackServices($packName);
        if(in_array("Voice Messaging User", $servicepackServices)) {
            $packDetail[] = $packName;
            return $packDetail;
        }
    }
    return $packDetail;
}

function checkHasVmOnlyPack($assignedPacks) {
    $packDetail = array();
    
    foreach($assignedPacks as $key=>$packName ) {
        $servicepackServices = getServicePackServices($packName);
        if(in_array("Voice Messaging User", $servicepackServices) && count($servicepackServices) == 1) {
            $packDetail[] = $packName;
            return $packDetail;
        }
    }
    return $packDetail;
}

// assgin VM services by service pack or user service
function assignVMService($ServicePackName, $userServicePackAssigned, $userId)
{
    $assignService["Error"] = "";
    $assignService["Success"] = "";
    
    global $sessionid, $client;
    $assignServicePackFlag = 0;
    $isServicePackAssigned = "";
    $isVMUserAssignServices = "";
    $hasVmOnlyPack = false;
    $hasVmService =false;
    foreach($userServicePackAssigned as $key=>$packName ) {
        $servicepackServices = getServicePackServices($packName);
        if(in_array("Voice Messaging User", $servicepackServices)) {
            $hasVmService = true;
            break;
        }
    }
    if($hasVmService) {
        $assignService["Success"] = "Service Assinged.";
        $assignService["Error"] = "";
        return $assignService;
    }
    // if (count($ServicePackName) > 0) {
    $vmOnlyPackDetail = array();
    if(count($userServicePackAssigned) > 0) {
        // check if assigned packs has vm service.
        $vmOnlyPackDetail = checkHasPackWithVM($userServicePackAssigned);
    }
    if(count($vmOnlyPackDetail) == 0) { // there is no vm only pack assigned
        // get the pack from assignable list with vm only pack.
        $vmOnlyPackDetail =  checkHasVmOnlyPack($ServicePackName);
        if(count($vmOnlyPackDetail) > 0) {
            $packAssignResponse = assignService($userId, '', $vmOnlyPackDetail[0]);
            if(empty($packAssignResponse["Error"])) {
                $assignServicePackFlag = true;
            } else {
                $assignService["Error"] = $packAssignResponse["Error"];
                $assignService["Success"] = "";
                $assignServicePackFlag = false;
            }
        } 
    } else {
        $assignServicePackFlag = true;
    }
    if(!empty($assignService["Error"])) {
        return $assignService;
    }
    
    if ($assignServicePackFlag == false) {
        $vmServiceAssigned = assignService($userId, 'Voice Messaging User', '');
        if (empty($vmServiceAssigned["Error"])) {
            $isVMUserAssignServices = "Succes";
        } else {
            $assignService["Error"] = $vmServiceAssigned["Error"];
            $assignService["Succes"] = "";
        }
    }
    
    if ($isVMUserAssignServices == "Succes" || $assignServicePackFlag == "Succes") {
        $assignService["Success"] = "Service Assinged.";
    }
    
    return $assignService;
}

// Un assgin VM services by service pack or user service
function unAssignVMService($ServicePackName, $userId)
{
    global $sessionid, $client;
    
    $returnData["Error"] = "";
    $returnData["Success"] = "";
    $userModUnassingedService = "";
    $userModUnassingedVNService = "";
    
    $isServicePackUnAssigne = array();
    $hasVmWithMoreService =false;
    $vmOnlyServicePack = array();
    if (count($ServicePackName) > 0) {
        foreach($ServicePackName as $key=>$packName ) {
            $servicepackServices = getServicePackServices($packName);
            if(in_array("Voice Messaging User", $servicepackServices)) {
                if(count($servicepackServices) == 1) {
                    $vmOnlyServicePack[] = $packName;
                } else {
                    $hasVmWithMoreService = true;
                }
            }
        }
    }
    
    
    if($hasVmWithMoreService) {
        $errorMsg = "VM User Service cannot be un-assigned because it is assigned through ".$userId." Service Pack, containing multiple services.";
        $returnData["Error"] = $errorMsg;
        $returnData["Success"] = "";
        $_SESSION["userMOdifydVoiceMgt"][$userId]["unAssignedServicePack"]["status"] = "Error";
        $_SESSION["userMOdifydVoiceMgt"][$userId]["unAssignedServicePack"]["errorMsg"] = $errorMsg;
        return $returnData;
    }
    $packUnassignFail =false;
    if(empty($returnData["Error"]) && count($vmOnlyServicePack) > 0) {
        foreach($vmOnlyServicePack as $index=>$packName) {
            $packUnassingResponse = unAssignService($userId, '', $packName);
            if (empty($packUnassingResponse["Error"]) && !$packUnassignFail) {
                $isServicePackUnAssigne[] = "Success";
                $returnData["Success"] ="true";
            } else {
                $packUnassignFail = true;
                $returnData["Error"] = $packUnassingResponse["Error"];
                $returnData["Success"] ="";
                
            }
        }
    }
    
    
    $isVMUserExist = checkVMUserService($userId);
    if (empty($isVMUserExist["Error"])) {
        if ($isVMUserExist["Success"] == "true") {
            $assignResult = unAssignService($userId, 'Voice Messaging User', '');
            if (empty($assignResult["Error"])) {
                $isServicePackUnAssigne[] = "Success";
                $returnData["Success"] ="true";
            }else {
                $returnData["Error"] = $assignResult["Error"];
            }
        }
    } else {
        $returnData["Error"] = $isVMUserExist["Error"];
    }
    
//     if (in_array("Success", $isServicePackUnAssigne) ) {
    if (empty($returnData["Error"])) {
        $_SESSION["userMOdifydVoiceMgt"][$userId]["unAssignedServicePack"]["status"] = "Success";
    } else {
        $_SESSION["userMOdifydVoiceMgt"][$userId]["unAssignedServicePack"]["status"] = "Error";
        $errorMsg = isset($returnData["Error"]) ? $returnData["Error"] : 
        " Unknown error occured while unassiging voicemail service";
        $_SESSION["userMOdifydVoiceMgt"][$userId]["unAssignedServicePack"]["errorMsg"] = $errorMsg;
    }
    
    return  $returnData;
}

function getServicePackServices($ServicePackName)
{
    global $sessionid, $client;
    $userServices["Error"] = "";
    $userServices["Success"] = "";
    $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
    $xmlinput .= "<servicePackName>" . $ServicePackName . "</servicePackName>";
    $xmlinput .= xmlFooter();
    
    $response = $client->processOCIMessage(array(
        "in0" => $xmlinput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    
    if (readErrorXmlGenuine($xml) != "") {
        $userServices["Error"] = $xml->command->summaryEnglish;
    }
    
    $userServices = array();
    foreach ($xml->command->userServiceTable->row as $key => $value) {
        $userServices[] = strval($value->col[0]);
    }
    return $userServices;
}

// Get the list of all available unassigned service pack
function getuserServicePackUnassign($userId)
{
    $returnData['Error'] = array();
    $returnData['Success'] = array();
    
    global $sessionid, $client;
    $xmlinput = xmlHeader($sessionid, "UserServiceGetAssignmentListRequest");
    $xmlinput .= "<userId>" . $userId . "</userId>";
    $xmlinput .= xmlFooter();
    
    $response = $client->processOCIMessage(array(
        "in0" => $xmlinput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    
    if (readErrorXmlGenuine($xml) != "") {
        $returnData["Error"] = $xml->command->summaryEnglish;
    } else {
        
        $userServicesPack = array();
        foreach ($xml->command->servicePacksAssignmentTable->row as $key => $value) {
            if ($value->col[1] == "true") {
                $userServicesPack[] = strval($value->col[0]);
            }
        }
        
        $isUnAssignVMService = unAssignVMService($userServicesPack, $userId);
        if(!empty($isUnAssignVMService["Error"])) {
            $returnData["Error"] = $isUnAssignVMService["Error"];
        } else {
            $returnData["Success"] = "Success";
        }
    }
    
    return $returnData;
}

function getuserServicePackAssign($userId)
{
    $returnData["Error"] = "";
    $returnData["Success"] = "";
    global $sessionid, $client;
    $xmlinput = xmlHeader($sessionid, "UserServiceGetAssignmentListRequest");
    $xmlinput .= "<userId>" . $userId . "</userId>";
    $xmlinput .= xmlFooter();
    
    $response = $client->processOCIMessage(array(
        "in0" => $xmlinput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    if (readErrorXmlGenuine($xml) != "") {
        $returnData["Error"] = $xml->command->summaryEnglish;
    } else {
        $userServicesPack = array();
        $userServicePackAssigned = array();
        //$serServicesPack = array();
        if (count($xml->command->servicePacksAssignmentTable->row) > 0) {
            foreach ($xml->command->servicePacksAssignmentTable->row as $key => $value) {
                if ($value->col[1] == "false") {
                    $userServicesPack[] = strval($value->col[0]);
                } else {
                    $userServicePackAssigned[] = strval($value->col[0]);
                }
            }
        }
        $isVmuserServiceAssiged = "false";
        if (count($xml->command->userServicesAssignmentTable->row) > 0) {
            foreach ($xml->command->userServicesAssignmentTable->row as $key => $value) {
                if ($value->col[0] == "Voice Messaging User") {
                    $isVmuserServiceAssiged= strval($value->col[1]);
                    break;
                } 
            }
        }
        
        if($isVmuserServiceAssiged == "false") {
            $vmOnlyServicePack = assignVMService($userServicesPack,$userServicePackAssigned, $userId);
            if (!empty($vmOnlyServicePack["Error"])) {
                $returnData["Error"] = $vmOnlyServicePack["Error"];
            } else {
                $returnData["Success"] = "Success";
            }
        }
        if (empty($returnData["Error"])) {
                $returnData["Success"] = "Success";
                $_SESSION["userMOdifydVoiceMgt"][$userId]["assignedServicePack"]["status"] = "Success";
            } else {
                $returnData["Error"] = $returnData["Error"];
                $_SESSION["userMOdifydVoiceMgt"][$userId]["assignedServicePack"]["status"] = "Error";
                $_SESSION["userMOdifydVoiceMgt"][$userId]["assignedServicePack"]["errorMsg"] = $returnData["Error"];
            }
    }
    return $returnData;
}

function checkVMUserService($userId)
{
    $returnData["Error"] = "";
    $returnData["Success"] = "";
    global $sessionid, $client;
    $xmlinput = xmlHeader($sessionid, "UserServiceIsAssignedRequest");
    $xmlinput .= "<userId>" . $userId . "</userId>";
    $xmlinput .= "<serviceName>Voice Messaging User</serviceName>";
    $xmlinput .= xmlFooter();
    
    $response = $client->processOCIMessage(array(
        "in0" => $xmlinput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    if (readErrorXmlGenuine($xml) != "") {
        $returnData["Error"] = $xml->command->summaryEnglish;
    } else {
        $returnData["Success"] = strval($xml->command->isAssigned);
    }
    return $returnData;
}

// isActive Services

// end foreach Services
function getTextUserServicesAll($userId)
{
    $retrunData["Success"] = "";
    $retrunData["Error"] = "";
    
    global $sessionid, $client;
    $xmlinput = xmlHeader($sessionid, "UserServiceGetAssignmentListRequest");
    $xmlinput .= "<userId>" . $userId . "</userId>";
    $xmlinput .= xmlFooter();
    
    $response = $client->processOCIMessage(array(
        "in0" => $xmlinput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    if (readErrorXmlGenuine($xml) != "") {
        $retrunData["Error"] = $xml->command->summaryEnglish;
    } else {
        
        $userServices = array();
        foreach ($xml->command->userServicesAssignmentTable->row as $key => $value) {
            if ($value->col[1] == "true") {
                $userServices[] = strval($value->col[0]);
            }
        }
        $servicepackServices = array();
        foreach ($xml->command->servicePacksAssignmentTable->row as $key => $value) {
            if ($value->col[1] == "true") {
                
                $servicepackServices = getServicePackServices($value->col[0]);
            }
            foreach ($servicepackServices as $srv) {
                if (! in_array($srv, $userServices)) {
                    $userServices[] = $srv;
                }
            }
        }
        
        $retrunData["Success"] = $userServices;
    }
    
    return $retrunData;
}

function activateUserService($userId, $ociRequest, $activateFlag)
{
    global $sessionid, $client;
    
    $retrunData["Success"] = "";
    $retrunData["Error"] = "";
    
    $xmlInput = xmlHeader($sessionid, $ociRequest);
    $xmlInput .= "<userId>" . $userId . "</userId>";
    $xmlInput .= "<isActive>" . $activateFlag . "</isActive>";
    $xmlInput .= xmlFooter();
    $response = $client->processOCIMessage(array(
        "in0" => $xmlInput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    return readError($xml);
}

function modifyCFSDefaultForwardingNumber($userId)
{
    global $sessionid, $client;
    
    $userLastFirstName = getUserName($userId);
    createUserModificationLogEntry($userId, $userLastFirstName, "Default Number/SIP-URI to forward calls to", $_SESSION["CFS"]["phSipFwdCFS"]);
    
    $xmlInput = xmlHeader($sessionid, "UserCallForwardingSelectiveModifyRequest");
    $xmlInput .= "<userId>" . $userId . "</userId>";
    
    $xmlInput .= $_SESSION["CFS"]["phSipFwdCFS"] == "" ? "<defaultForwardToPhoneNumber xsi:nil=\"true\" />" : "<defaultForwardToPhoneNumber>" . $_SESSION["CFS"]["phSipFwdCFS"] . "</defaultForwardToPhoneNumber>";
    
    $xmlInput .= xmlFooter();
    
    $response = $client->processOCIMessage(array(
        "in0" => $xmlInput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    if (readError($xml) == "Error") {
        return "Error";
    }
    
    return "";
}

function modifyCFSService($userId)
{
    global $sessionid, $client;
    $userLastFirstName = getUserName($userId);
    $xmlInput = xmlHeader($sessionid, "UserCallForwardingSelectiveModifyRequest");
    $xmlInput .= "<userId>" . $userId . "</userId>";
    
    $ringReminder = $_SESSION["CFS"]["ringReminderChk"] == "on" ? "true" : "false";
    $xmlInput .= "<playRingReminder>" . $ringReminder . "</playRingReminder>";
    
    foreach ($_SESSION["cfsCriteria"] as $key => $value) {
        createUserModificationLogEntry($userId, $userLastFirstName, "CFS Criteria", $value["cfsCriteriaName"]);
        $xmlInput .= "<criteriaActivation>";
        $xmlInput .= "<criteriaName>" . $value["cfsCriteriaName"] . "</criteriaName>";
        $xmlInput .= "<isActive>" . $value["cfsCriteriaActive"] . "</isActive>";
        $xmlInput .= "</criteriaActivation>";
    }
    $xmlInput .= xmlFooter();
    
    $response = $client->processOCIMessage(array(
        "in0" => $xmlInput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    if (readError($xml) == "Error") {
        return "Error";
    }
    
    return "";
}

function deleteCFSCriteria($userId, $criteriaName)
{
    global $sessionid, $client;
    
    $xmlInput = xmlHeader($sessionid, "UserCallForwardingSelectiveDeleteCriteriaRequest");
    $xmlInput .= "<userId>" . $userId . "</userId>";
    $xmlInput .= "<criteriaName>" . $criteriaName . "</criteriaName>";
    $xmlInput .= xmlFooter();
    $response = $client->processOCIMessage(array(
        "in0" => $xmlInput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    return readError($xml);
}

function addCFSCriteria($userId)
{
    global $sessionid, $client;
    
    foreach ($_SESSION["cfsCriteria"] as $key => $value) {
        $xmlInput = xmlHeader($sessionid, "UserCallForwardingSelectiveAddCriteriaRequest16");
        $xmlInput .= "<userId>" . $userId . "</userId>";
        $xmlInput .= "<criteriaName>" . $value["cfsCriteriaName"] . "</criteriaName>";
        
        if ($value["cfsCriteriaTime"] != "") {
            $xmlInput .= "<timeSchedule>";
            $xmlInput .= "<type>" . $value["cfsCriteriaTimeLevel"] . "</type>";
            $xmlInput .= "<name>" . $value["cfsCriteriaTime"] . "</name>";
            $xmlInput .= "</timeSchedule>";
        }
        
        if ($value["cfsCriteriaHoliday"] != "") {
            $xmlInput .= "<holidaySchedule>";
            $xmlInput .= "<type>" . $value["cfsCriteriaHolidayLevel"] . "</type>";
            $xmlInput .= "<name>" . $value["cfsCriteriaHoliday"] . "</name>";
            $xmlInput .= "</holidaySchedule>";
        }
        
        $xmlInput .= "<forwardToNumberSelection>" . $value["cfsCriteriaFwd"] . "</forwardToNumberSelection>";
        
        if ($value["cfsAnotherFwdNum"] != "") {
            $xmlInput .= "<forwardToPhoneNumber>" . $value["cfsAnotherFwdNum"] . "</forwardToPhoneNumber>";
        }
        
        $xmlInput .= "<fromDnCriteria>";
        $xmlInput .= "<fromDnCriteriaSelection>" . $value["cfsCriteriaFwdFrom"] . "</fromDnCriteriaSelection>";
        $xmlInput .= "<includeAnonymousCallers>" . $value["cfsCriteriaFwdFromPrivate"] . "</includeAnonymousCallers>";
        $xmlInput .= "<includeUnavailableCallers>" . $value["cfsCriteriaFwdFromUnavailable"] . "</includeUnavailableCallers>";
        for ($i = 1; $i <= 12; $i ++) {
            $attr = "cfsCriteriaSpecific";
            if ($i < 10) {
                $attr .= "0";
            }
            $attr .= $i;
            if ($value[$attr] != "") {
                $xmlInput .= "<phoneNumber>" . $value[$attr] . "</phoneNumber>";
            }
        }
        $xmlInput .= "</fromDnCriteria>";
        $xmlInput .= xmlFooter();
        
        $response = $client->processOCIMessage(array(
            "in0" => $xmlInput
        ));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        if (readError($xml) == "Error") {
            return "Error";
        }
    }
    
    return "";
}

function getCFSCriteriaList($userId)
{
    global $sessionid, $client;
    
    $xmlInput = xmlHeader($sessionid, "UserCallForwardingSelectiveGetRequest16");
    $xmlInput .= "<userId>" . $userId . "</userId>";
    $xmlInput .= xmlFooter();
    $response = $client->processOCIMessage(array(
        "in0" => $xmlInput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    readError($xml);
    
    $criteriaList = array();
    foreach ($xml->command->criteriaTable->row as $key => $value) {
        $criteriaList[] = strval($value->col[1]);
    }
    
    return $criteriaList;
}

/*
 * Get the list of all user services assigned to a user.
 * Function returns list of BroadSoft User Services or NULL if back-end error
 */
function getUserAssignedUserServices($userId)
{
    global $sessionid, $client;
    $userAssignedUserServices = array();
    
    $xmlInput = xmlHeader($sessionid, "UserAssignedServicesGetListRequest");
    $xmlInput .= "<userId>" . $userId . "</userId>";
    $xmlInput .= xmlFooter();
    $response = $client->processOCIMessage(array(
        "in0" => $xmlInput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    
    if (readError($xml) != "") {
        return null;
    }
    
    foreach ($xml->command->userServiceEntry as $key => $value) {
        $userAssignedUserServices[] = strval($value->serviceName);
    }
    return $userAssignedUserServices;
}

/*
 * Get User's Last Name and First Name
 * Returns: "Last Name, First Name"
 */
function getUserName($userId)
{
    global $ociVersion, $sessionid, $client;
    
    if ($ociVersion == "17") {
        $xmlinput = xmlHeader($sessionid, "UserGetRequest17sp4");
    } else if ($ociVersion == "20") {
        $xmlinput = xmlHeader($sessionid, "UserGetRequest20");
    } else {
        $xmlinput = xmlHeader($sessionid, "UserGetRequest19");
    }
    $xmlinput .= "<userId>" . $userId . "</userId>";
    $xmlinput .= xmlFooter();
    $response = $client->processOCIMessage(array(
        "in0" => $xmlinput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    
    $lastName = strval($xml->command->lastName);
    $firstName = strVal($xml->command->firstName);
    
    return $lastName . ", " . $firstName;
}

/*
 * Add entry to AveriStar Change Log
 */
function createChangeLogEntry($userLastFirstName)
{
    global $sth, $db;
    $expressProvLogsDb = $GLOBALS['expressProvLogsDb'];
    $query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
    $query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . date("Y-m-d H:i:s") . "', 'Users:Group-Wide Modify', '" . $_SESSION["groupId"] . "', '" . $userLastFirstName . "')";
    $sth = $expressProvLogsDb->query($query);
}

/*
 * Add entry to AveriStar Users Modifications Log
 */
function createUserModificationLogEntry($userId, $userLastFirstName, $field, $newValue)
{
    global $pth, $db;
    $expressProvLogsDb = $GLOBALS['expressProvLogsDb'];
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, TRUE);
    $lastId = $expressProvLogsDb->lastInsertId();
    $query = "INSERT into userModChanges (id, serviceId, entityName, field, oldValue, newValue) ";
    $query .= "VALUES ('" . $lastId . "', '" . $userId . "', '" . $userLastFirstName . "', '" . $field . "', '" . "N/A" . "', '" . $newValue . "')";
    $pth = $expressProvLogsDb->query($query);
}

/*
 * Get BroadWorks Request for service assignment/unassignment based on the input from the form
 * Returns 'UserServiceAssignListRequest' or 'UserServiceUnassignListRequest'
 * or empty string if the name of service or service pack is not set
 */
function getServiceAssignmentRequest($name, $key)
{
    if ($name == "None") {
        return "";
    }
    
    return $_SESSION["SwPck"][$key] == "true" ? "UserServiceAssignListRequest" : "UserServiceUnassignListRequest";
}

/*
 * Modify one of BroadSoft user Call Forwarding Service:
 * - Call Forwarding Always
 * - Call Forwarding Not Reachable
 */
function modifyCallForwardingService($service)
{
    global $client, $sessionid;
    global $result, $isLocalError;
    $ringSplashVal = "";
    
    $ociRequest = "";
    $serviceKey = "";
    $activateKey = "";
    $phoneNumberKey = "";
    if ($service == cfaService) {
        $ociRequest = "UserCallForwardingAlwaysModifyRequest";
        $serviceKey = "CFA";
        $activateKey = "activateCFA";
        $phoneNumberKey = "phSipFwdCFA";
    } else if ($service == cfnrService) {
        $ociRequest = "UserCallForwardingNotReachableModifyRequest";
        $serviceKey = "CFNR";
        $activateKey = "activateCFNR";
        $phoneNumberKey = "phSipFwdCFNR";
    }
    
    $users = getUsers();
    $userAssignedUserServices = null;
    
    for ($i = 0; $i < count($users); $i ++) {
        $userId = $users[$i];
        $userAssignedUserServices = getUserAssignedUserServices($userId);
        if ($userAssignedUserServices == null) {
            break;
        }
        
        if (in_array($service, $userAssignedUserServices)) {
            $xmlInput = xmlHeader($sessionid, $ociRequest);
            $xmlInput .= "<userId>" . $userId . "</userId>";
            $xmlInput .= "<isActive>" . $_SESSION[$serviceKey][$activateKey] . "</isActive>";
            $xmlInput .= $_SESSION[$serviceKey][$phoneNumberKey] == "" ? "<forwardToPhoneNumber xsi:nil=\"true\" />" : "<forwardToPhoneNumber>" . $_SESSION[$serviceKey][$phoneNumberKey] . "</forwardToPhoneNumber>";
            
            // Ring Splash only for CFA
            if ($service == cfaService) {
                $ringSplashVal = $_SESSION[$serviceKey]["ringReminderChk"] == "on" ? "true" : "false";
                $xmlInput .= "<isRingSplashActive>" . $ringSplashVal . "</isRingSplashActive>";
            }
            
            $xmlInput .= xmlFooter();
            
            $response = $client->processOCIMessage(array(
                "in0" => $xmlInput
            ));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            readError($xml);
            
            // Log changes into Change Log Database
            $phoneNumberLog = $_SESSION[$serviceKey][$phoneNumberKey] == "" ? "None" : $_SESSION[$serviceKey][$phoneNumberKey];
            $userLastFirstName = getUserName($userId);
            
            createChangeLogEntry($userLastFirstName);
            createUserModificationLogEntry($userId, $userLastFirstName, $_SESSION["gwUsersMod"][$activateKey], $_SESSION[$serviceKey][$activateKey]);
            createUserModificationLogEntry($userId, $userLastFirstName, $serviceKey . ":" . $_SESSION["gwUsersMod"]["phSipFwdCFA"], $phoneNumberLog);
            if ($service == cfaService) {
                createUserModificationLogEntry($userId, $userLastFirstName, $serviceKey . ":" . $_SESSION["gwUsersMod"]["ringReminderChk"], $ringSplashVal);
            }
        } else {
            addError("Error: Service '" . $service . "' is not assigned to User '" . $userId . "'");
        }
    }
}

function modifyCallForwardingSelectiveService()
{
    global $result, $isLocalError;
    
    $users = getUsers();
    $userAssignedUserServices = null;
    
    for ($i = 0; $i < count($users); $i ++) {
        $userId = $users[$i];
        $userAssignedUserServices = getUserAssignedUserServices($userId);
        if ($userAssignedUserServices == null) {
            break;
        }
        
        if (in_array(cfsService, $userAssignedUserServices)) {
            // Log changes into Change Log database
            $userLastFirstName = getUserName($userId);
            createChangeLogEntry($userLastFirstName);
            
            // STEP 1: De-activate CFS Service
            if (activateUserService($userId, "UserCallForwardingSelectiveModifyRequest", "false") == "Error") {
                return;
            }
            
            // STEP 2: Get user CFS parameters to read criteria table
            $criteriaList = getCFSCriteriaList($userId);
            
            // STEP 3: Delete all old criteria
            for ($j = 0; $j < count($criteriaList); $j ++) {
                if (deleteCFSCriteria($userId, $criteriaList[$j]) == "Error") {
                    return;
                }
            }
            
            // STEP 4: Add Default Call Forwarding Number before adding criteria
            if (modifyCFSDefaultForwardingNumber($userId) == "Error") {
                return;
            }
            
            // STEP 5: Add all new criteria
            if (addCFSCriteria($userId) == "Error") {
                return;
            }
            
            // STEP 6: Modify CFS service including criteria activation
            if (modifyCFSService($userId) == "Error") {
                return;
            }
            
            // STEP 7: Activate CFS service if requested (Modify CFS service again)
            if ($_SESSION["CFS"]["activateCFS"] == "true") {
                if (activateUserService($userId, "UserCallForwardingSelectiveModifyRequest", "true") == "Error") {
                    return;
                }
            }
        } else {
            addError("Error: Service '" . cfsService . "' is not assigned to User '" . $userId . "'");
        }
    }
}

function getGroupMailServerUserID($userId)
{
    global $sessionid, $client;
    
    $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetAdvancedVoiceManagementRequest14sp3");
    $xmlinput .= "<userId>" . $userId . "</userId>";
    $xmlinput .= xmlFooter();
    $response = $client->processOCIMessage(array(
        "in0" => $xmlinput
    ));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    
    if (readError($xml) != "") {
        return "";
    }
    
    return isset($xml->command->groupMailServerUserId) ? strval($xml->command->groupMailServerUserId) : "";
}

function userServicesAssignmentPostProcess($userId, $userDevice, $groupMailServerUserId, $servicesBeforeAssignment, $servicesAfterAssignment)
{
    global $sessionid, $client, $bwVoicePortalPasswordType, $userVoicePortalPasscodeFormula;
    global $surgeMailIdCriteria1, $surgeMailIdCriteria2, $surgemailUsername, $surgemailPassword, $surgemailURL;
    global $policiesSupportDeleteVMOnUserDel;
    global $userid, $ociVersion;
    
    $sugeObj = new SurgeMailOperations();   //Added @ 12 May 2018
    
    // Process assignment of 'Busy Lamp Field' service
    if (! in_array("Busy Lamp Field", $servicesBeforeAssignment) && in_array("Busy Lamp Field", $servicesAfterAssignment)) {
        $expl = explode("@", $userId);
        $listURI = $expl[0] . "-blf@" . $_SESSION["defaultDomain"];
        
        $xmlinput = xmlHeader($sessionid, "UserBusyLampFieldModifyRequest");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= "<listURI>" . $listURI . "</listURI>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array(
            "in0" => $xmlinput
        ));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        readError($xml);
        
        if (readError($xml) == "" && ! empty($userDevice)) {
            rebuildAndResetDeviceConfig($userDevice, "rebuildAndReset");
        }
    }    // Process assignment of 'Voice Messaging User' service
    elseif (! in_array("Voice Messaging User", $servicesBeforeAssignment) && in_array("Voice Messaging User", $servicesAfterAssignment)) {
        $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= "<isActive>true</isActive>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array(
            "in0" => $xmlinput
        ));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        readError($xml);
        
        $userGetRequest = new UserGetRequest($userId);
        
        // Configure VM service
        // Build Voice Mail Password. Enforce fallback
        // ----------------------------------------------------------------------------------------------------
        if ($bwVoicePortalPasswordType == "Formula") {
            require_once ("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
            require_once ("/var/www/lib/broadsoft/adminPortal/VoicePasswordNameBuilder.php");
            
            $voicePasswordBuilder = new VoicePasswordNameBuilder($userVoicePortalPasscodeFormula, $userGetRequest->getPhoneNumber(), $userGetRequest->getExtension(), $userGetRequest->getCallingLineIdPhoneNumber(), isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "", true);
            
            if (! $voicePasswordBuilder->validate()) {
                // TODO: Implement validation resolution
            }
            
            do {
                $attr = $voicePasswordBuilder->getRequiredAttributeKey();
                if ($attr == ResourceNameBuilder::MAC_ADDR) {
                    $voicePasswordBuilder->setRequiredAttribute($attr, getMacAddress($userGetRequest->getDeviceName()));
                } else {
                    $voicePasswordBuilder->setRequiredAttribute($attr, getResourceForVoicePasswordCreation($attr, $userGetRequest->getPhoneNumber(), $userGetRequest->getExtension(), $userGetRequest->getCallingLineIdPhoneNumber()));
                }
            } while ($attr != "");
            
            $portalPassWord = $voicePasswordBuilder->getName();
            //Code added @ 08 May 2019 regarding Ex-1318
                if(is_numeric($portalPassWord) && strlen($portalPassWord) < 6){
                    $portalPassWord = "476983";
                }
                if(!is_numeric($portalPassWord)){
                    $portalPassWord = "476983";
                }
                //End code
        } else {
            $portalPassWord = setVoicePassword();
        }
        $xmlinput = xmlHeader($sessionid, "UserPortalPasscodeModifyRequest");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= "<newPasscode>" . $portalPassWord . "</newPasscode>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array(
            "in0" => $xmlinput
        ));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        readError($xml);
        // if (readError($xml)) { return; }
        
        // Build Surgemail Username.
        // ------------------------------------------------------------------------------------------------------
        
        // Create surgemail username from the first formula.
        // Enforce fallback if there is no second criteria/formula.
        // -------------------------------------------------------------
        $surgeMailNameBuilder1 = new SurgeMailNameBuilder($surgeMailIdCriteria1, $userGetRequest->getPhoneNumber(), $userGetRequest->getExtension(), $userGetRequest->getCallingLineIdPhoneNumber(), isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "", $_SESSION["systemDefaultDomain"], $userGetRequest->getFirstName(), $userGetRequest->getLastName(), $surgeMailIdCriteria2 == ""); // force fallback only if second criteria does not exist
        
        if (! $surgeMailNameBuilder1->validate()) {
            // TODO: Implement resolution when User ID input formula has errors
        }
        
        do {
            $attr = $surgeMailNameBuilder1->getRequiredAttributeKey();
            if ($attr == ResourceNameBuilder::MAC_ADDR) {
                $surgeMailNameBuilder1->setRequiredAttribute($attr, getMacAddress($userGetRequest->getDeviceName()));
            } else {
                $surgeMailNameBuilder1->setRequiredAttribute($attr, getResourceForSurgeMailNameCreation($attr, $userGetRequest->getPhoneNumber(), $userGetRequest->getExtension(), $userGetRequest->getCallingLineIdPhoneNumber(), $userGetRequest->getFirstName(), $userGetRequest->getLastName(), $userDevice, $userId));
            }
        } while ($attr != "");
        
        $surgemailName = $surgeMailNameBuilder1->getName();
        
        if ($surgemailName == "") {
            // Create surgemail name from the second formula. At this point we know that there is second formula,
            // because without second formula name would be forcefully resolved in the first formula.
            
            $surgeMailNameBuilder2 = new SurgeMailNameBuilder($surgeMailIdCriteria2, $userGetRequest->getPhoneNumber(), $userGetRequest->getExtension(), $userGetRequest->getCallingLineIdPhoneNumber(), isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "", $_SESSION["systemDefaultDomain"], $userGetRequest->getFirstName(), $userGetRequest->getLastName(), true);
            
            if (! $surgeMailNameBuilder2->validate()) {
                // TODO: Implement resolution when User ID input formula has errors
            }
            
            do {
                $attr = $surgeMailNameBuilder2->getRequiredAttributeKey();
                if ($attr == ResourceNameBuilder::MAC_ADDR) {
                    $surgeMailNameBuilder2->setRequiredAttribute($attr, getMacAddress($userGetRequest->getDeviceName()));
                } else {
                    $surgeMailNameBuilder2->setRequiredAttribute($attr, getResourceForSurgeMailNameCreation($attr, $userGetRequest->getPhoneNumber(), $userGetRequest->getExtension(), $userGetRequest->getCallingLineIdPhoneNumber(), $userGetRequest->getFirstName(), $userGetRequest->getLastName(), $userDevice, $userId));
                }
            } while ($attr != "");
            
            $surgemailName = $surgeMailNameBuilder2->getName();
        }
        
        $emailAddress = $surgemailName;
        $emailPassword = setEmailPassword();
        
        $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyAdvancedVoiceManagementRequest");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= "<groupMailServerEmailAddress>" . $emailAddress . "</groupMailServerEmailAddress>";
        $xmlinput .= "<groupMailServerUserId>" . $emailAddress . "</groupMailServerUserId>";
        $xmlinput .= "<groupMailServerPassword>" . $emailPassword . "</groupMailServerPassword>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array(
            "in0" => $xmlinput
        ));
        
        $surgemailPost = array(
            "cmd" => "cmd_user_login",
            "lcmd" => "user_delete",
            "show" => "simple_msg.xml",
            "username" => $surgemailUsername,
            "password" => $surgemailPassword,
            "lusername" => $emailAddress,
            "lpassword" => $emailPassword,
            "uid" => isset($userid) ? $userid : ""
        );
        //$res = http_post_fields($surgemailURL, $surgemailPost);
        $srgeRsltDelUsr = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPost); //Added @ 12 May 2018
        
        $surgemailPost = array(
            "cmd" => "cmd_user_login",
            "lcmd" => "user_create",
            "show" => "simple_msg.xml",
            "username" => $surgemailUsername,
            "password" => $surgemailPassword,
            "lusername" => $emailAddress,
            "lpassword" => $emailPassword,
            "uid" => isset($userid) ? $userid : ""
        );
        //$res = http_post_fields($surgemailURL, $surgemailPost);
        $srgeRsltCrteUsr = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPost); //Added @ 12 May 2018
        
    }    // Process un-assignment of 'Voice Messaging User' service
    elseif (in_array("Voice Messaging User", $servicesBeforeAssignment) && ! in_array("Voice Messaging User", $servicesAfterAssignment)) {
        if ($policiesSupportDeleteVMOnUserDel == "true" && $groupMailServerUserId != "") {
            $mailboxPassword = setEmailPassword();
            
            $surgemailPost = array(
                "cmd" => "cmd_user_login",
                "lcmd" => "user_delete",
                "show" => "simple_msg.xml",
                "username" => $surgemailUsername,
                "password" => $surgemailPassword,
                "lusername" => $groupMailServerUserId,
                "lpassword" => $mailboxPassword,
                "uid" => isset($userid) ? $userid : ""
            );
            //$res = http_post_fields($surgemailURL, $surgemailPost);
            $srgeRsltDelUser = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPost); //Added @ 12 May 2018
        }
    }
}

/*
 * Service Management for selected users
 * - Assigning/Unassigning Service Pack
 * - Assigning/Unassigning User Service
 */
function processUserServiceAssignment()
{
    global $sessionid, $client;
    
    $servicePack = $_SESSION["SwPck"]["srvPack"];
    $servicePackAssignmentRequest = getServiceAssignmentRequest($servicePack, "assignSvcPack");
    
    $userService = $_SESSION["SwPck"]["usrServices"];
    $userServiceAssignmentRequest = getServiceAssignmentRequest($userService, "assignUsrService");
    
    $servicePackProcessed = false;
    
    // $users = getUsers();
    $users = getUsersAndDeviceName();
    
    for ($i = 0; $i < count($users); $i ++) {
        $userId = $users[$i]['userId'];
        $userDevice = $users[$i]['userDevice'];
        $userLastFirstName = getUserName($userId);
        $servicesBeforeAssignment = getUserServicesAll($userId);
        
        // get Group Mail Server User ID if user has VM service
        // it may be needed for deleting Surge Mail account
        $groupMailServerUserId = in_array("Voice Messaging User", $servicesBeforeAssignment) ? getGroupMailServerUserID($userId) : "";
        
        // Log changes into Change Log Database
        createChangeLogEntry($userLastFirstName);
        
        if ($userService != "None") {
            // Either user service assignment or unassignment will be processed
            $xmlInput = xmlHeader($sessionid, $userServiceAssignmentRequest);
            $xmlInput .= "<userId>" . $userId . "</userId>";
            $xmlInput .= "<serviceName>" . $userService . "</serviceName>";
            
            // log entry
            $assignmentKey = $userServiceAssignmentRequest == "UserServiceAssignListRequest" ? "true" : "false";
            createUserModificationLogEntry($userId, $userLastFirstName, $_SESSION["gwUsersMod"]["usrServices"], $userService);
            createUserModificationLogEntry($userId, $userLastFirstName, $_SESSION["gwUsersMod"]["assignUsrService"], $assignmentKey);
            
            // Service Pack assignment/unassignment will be processed together with user service
            // assignment/unassignment only if both require the same operation - assignment or unassignment
            if ($servicePack != "None" && $servicePackAssignmentRequest == $userServiceAssignmentRequest) {
                $xmlInput .= "<servicePackName>" . $servicePack . "</servicePackName>";
                $servicePackProcessed = true;
                
                // log entry
                createUserModificationLogEntry($userId, $userLastFirstName, $_SESSION["gwUsersMod"]["srvPack"], $servicePack);
                createUserModificationLogEntry($userId, $userLastFirstName, $_SESSION["gwUsersMod"]["assignSvcPack"], $assignmentKey);
            }
            $xmlInput .= xmlFooter();
            
            $response = $client->processOCIMessage(array(
                "in0" => $xmlInput
            ));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            if ($userService != "None" && $servicePack != "None") {                
                readErrorDup($xml, $servicePack, "None");
            }elseif ($userService != "None" && $servicePack == "None") {                
                readErrorDup($xml, "None", $userService);
            }
            else
            {
                readError($xml);
            }
            //readError($xml);
        }
        
        if ($servicePack != "None" && ! $servicePackProcessed) {
            // log entry
            $assignmentKey = $servicePackAssignmentRequest == "UserServiceAssignListRequest" ? "true" : "false";
            createUserModificationLogEntry($userId, $userLastFirstName, $_SESSION["gwUsersMod"]["srvPack"], $servicePack);
            createUserModificationLogEntry($userId, $userLastFirstName, $_SESSION["gwUsersMod"]["assignSvcPack"], $assignmentKey);
            
            $xmlInput = xmlHeader($sessionid, $servicePackAssignmentRequest);
            $xmlInput .= "<userId>" . $userId . "</userId>";
            $xmlInput .= "<servicePackName>" . $servicePack . "</servicePackName>";
            
            $xmlInput .= xmlFooter();
            
            $response = $client->processOCIMessage(array(
                "in0" => $xmlInput
            ));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            if ($servicePack != "None") {                
                readErrorDup($xml, $servicePack, "None");
            }else
            {
                readError($xml);
            }
            //readError($xml);
        }
        
        $servicesAfterAssignment = getUserServicesAll($userId);
        userServicesAssignmentPostProcess($userId, $userDevice, $groupMailServerUserId, $servicesBeforeAssignment, $servicesAfterAssignment);
    }
}

function resetAuthenticationPassword()
{
    global $sessionid, $client;
    $password = $_SESSION["authPasswords"]["authPassword1"] != "" ? $_SESSION["authPasswords"]["authPassword1"] : $_SESSION["authPasswords"]["authGeneratedPassword"];
    // First we are getting password, then we are hiding it
    // $password = str_repeat("*", strlen($password));
    
    $users = getUsersAndDeviceName();
    
    for ($i = 0; $i < count($users); $i ++) {
        $userId = $users[$i]['userId'];
        $userDevice = $users[$i]['userDevice'];
        $userLastFirstName = getUserName($userId);
        
        // Log changes into Change Log database
        createChangeLogEntry($userLastFirstName);
        createUserModificationLogEntry($userId, $userLastFirstName, $_SESSION["gwUsersMod"]["authUserName"], $userId);
        createUserModificationLogEntry($userId, $userLastFirstName, $_SESSION["gwUsersMod"]["authPassword1"], $password);
        
        $xmlInput = xmlHeader($sessionid, "UserAuthenticationModifyRequest");
        $xmlInput .= "<userId>" . $userId . "</userId>";
        //$xmlInput .= "<userName>" . $userId . "</userName>";
        $xmlInput .= "<newPassword>" . $password . "</newPassword>";
        $xmlInput .= xmlFooter();
        
        $response = $client->processOCIMessage(array(
            "in0" => $xmlInput
        ));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if(isset($xml->command->summary) && $xml->command->summary != ""){
            $xmlInput = xmlHeader($sessionid, "UserServiceAssignListRequest");
            $xmlInput .= "<userId>" . $userId . "</userId>";
            $xmlInput .= "<serviceName>Authentication</serviceName>";
            $xmlInput .= xmlFooter();
            $response = $client->processOCIMessage(array(
                "in0" => $xmlInput
            ));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            
            $xmlInput = xmlHeader($sessionid, "UserAuthenticationModifyRequest");
            $xmlInput .= "<userId>" . $userId . "</userId>";
            //$xmlInput .= "<userName>" . $userId . "</userName>";
            $xmlInput .= "<newPassword>" . $password . "</newPassword>";
            $xmlInput .= xmlFooter();
            
            $response = $client->processOCIMessage(array(
                "in0" => $xmlInput
            ));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            
            readError($xml);
            if (readError($xml) == "" && ! empty($userDevice)) {
                rebuildAndResetDeviceConfig($userDevice, "rebuildAndReset");
            }
        }else if(readError($xml) == ""){
            if(! empty($userDevice)){
                rebuildAndResetDeviceConfig($userDevice, "rebuildAndReset");
            }
            
        }else{
            readError($xml);
        }
    }
}

function modifyCallingLineIDBlockingAssignment() {
    $inptuFormData = array();
    foreach ($_POST["formData"] as $key => $value) {
        $inptuFormData[$value["name"]] = $value["value"];
    }
    
    $isActive = $inptuFormData["callingLineIDBlockingInput"] == "block" ? "true" : "false";
    $serviceObj = new Services();
    $users = getUsers();
    $response = array();
    for ($i = 0; $i < count($users); $i ++) {
        $userId = $users[$i];
        $services = array("Calling Line ID Delivery Blocking");
        $oldOverRideVal = "";
        $overrideRres = $serviceObj->userCallingLineIDDeliveryBlockingGetRequest($userId);
        if( empty($overrideRres["Error"]) ) {
            $oldOverRideVal = $overrideRres["Success"]["isActive"] == "true" ? "block" : "unblock";
        }
        
        $assignedServices = new UserServiceGetAssignmentList($userId);
        if ($assignedServices->hasService("Calling Line ID Delivery Blocking")) {
            $asingedCLIB = $serviceObj->userCallingLineIDDeliveryBlockingModifyRequest($userId, $isActive);
            if( empty($asingedCLIB["Error"]) ) {
                $response[$userId]['status'] = "success";
                $response[$userId]['statusMsg'] = "Calling Line ID Blocking is successfully " . $inptuFormData["callingLineIDBlockingInput"] . " for user : " . $userId;
            } else {
                $response[$userId]['status'] = "error";
                $response[$userId]['statusMsg'] = $asingedCLIB["Error"];
            }
        } else {
            if($isActive == "true") {
                $assingedSerRes = $serviceObj->assignServicesToUser($userId, $services);
                if( empty($assingedSerRes["Error"]) ) {
                    $asingedCLIB = $serviceObj->userCallingLineIDDeliveryBlockingModifyRequest($userId, $isActive);
                    if( empty($asingedCLIB["Error"]) ) {
                        $response[$userId]['status'] = "success";
                        $response[$userId]['statusMsg'] = "Calling Line ID Blocking is successfully " . $inptuFormData["callingLineIDBlockingInput"] . " for user : " . $userId;
                    } else {
                        $response[$userId]['status'] = "error";
                        $response[$userId]['statusMsg'] = $asingedCLIB["Error"];
                    }
                } else {
                    $response[$userId]['status'] = "error";
                    $response[$userId]['statusMsg'] = $assingedSerRes["Error"];
                }
            } else {
                $response[$userId]['status'] = "success";
                $response[$userId]['statusMsg'] = "Calling Line ID Blocking is successfully " . $inptuFormData["callingLineIDBlockingInput"] . " for user : " . $userId;
            }
            
        }
        
        if($response[$userId]['status'] == "success" ) {
            $userLastFirstName = getUserName($userId);
            $objChngLogUtil  = new ChangeLogUtility($userId, $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
            $objChngLogUtil->createChangesArray("Calling Line ID Blocking", $oldOverRideVal, $inptuFormData["callingLineIDBlockingInput"]);
            $objChngLogUtil->changeLogModifyUtility($module = "Users:Group-Wide Modify", $userId, $tableName = "userModChanges");
            
        }
    }
    
    return $response;
}

function processUserNumberActivationDeactivation()
{
    require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
    $dn = new Dns();
    $activateIncrement = 0; 
    
    $selectedUsers = explode("+-+-+", $_POST["userList"]);
    $activatedDN = 0;
    $deactivatedDN = 0;
    foreach ($selectedUsers as $key => $val) {
        
        $explodeUserData = explode("*", $val);
        // get user id
        $userId = $explodeUserData[0];
        $userPhoneNumber = "";
        $phoneNumberStatus = "";
        
        if ($val == true) {
            
            $numberActivateResponse = $dn->getUserDNActivateListRequest($userId);            
            if (empty($numberActivateResponse['Error'])) {
                
                if (! empty($numberActivateResponse['Success'][0]['phoneNumber'])) {
                    $userPhoneNumber = $numberActivateResponse['Success'][0]['phoneNumber'];
                }
                if (! empty($numberActivateResponse['Success'][0]['status'])) {
                    $phoneNumberStatus = $numberActivateResponse['Success'][0]['status'];
                }
                
                $dnPostArray = array(
                    "groupId" => $_SESSION['groupId'],
                    "phoneNumber" => array(
                        $userPhoneNumber
                    )
                );
                
                if (! empty($_POST['action']) && $_POST['action'] == "numberActivate") {
                    if (! empty($numberActivateResponse['Success'][0]['phoneNumber']) && isset($numberActivateResponse['Success'][0]['phoneNumber'])) {
                        $dnsActiveResponse = $dn->activateDNRequest($dnPostArray);
                        $process  = "activated";
                        $newValue = "Yes";
                        $activatedDN ++;
                    }
                } elseif (! empty($_POST['action']) && $_POST['action'] == "numberDeactivate") {
                    if (! empty($numberActivateResponse['Success'][0]['phoneNumber']) && isset($numberActivateResponse['Success'][0]['phoneNumber'])) {
                        $dnsActiveResponse = $dn->deActivateDNRequest($dnPostArray);
                        $process = "deactivated";
                        $newValue = "No";
                        $deactivatedDN ++;
                    }
                }
                if (empty($dnsActiveResponse['Error'])) {
                    $activateIncrement ++;
                }
                
                //Code added @ 25 May 2018     
                $oldValue  = $phoneNumberStatus;
                
                $objChngLogUtil  = new ChangeLogUtility($userId, $_SESSION["groupId"], $_SESSION["loggedInUserName"]); // Added on 25 may 2018
                $objChngLogUtil->createChangesArray($module = "status", $oldValue, $newValue);
                $module    = "Change User Status";                
                $objChngLogUtil->changeLogModifyUtility($module, $userId, $tableName = "userModChanges");
                //End Code           
            }
        }
    }
    if ($activatedDN > 0 || $deactivatedDN > 0) {
        if ($activatedDN > 0) {
            echo $activatedDN . " numbers of User/Users has been " . $process;
        } else if ($deactivatedDN > 0) {
            echo $deactivatedDN . " numbers of User/Users has been " . $process;
        }
        // echo $activateIncrement. " numbers of User/Users has been ".$process;
    } else {
        echo "Selected User/Users have no domain ";
    }
    // echo $activateIncrement. " numbers of users has been ".$process;
}

/*
 * *************************************************
 * Modify services for all users on the provided list
 * **************************************************
 */

if ($_POST["action"] == "modifyCFA") // Modify 'Call Forwarding Always' services for all users on the list
{
    modifyCallForwardingService(cfaService);
}

if ($_POST["action"] == "modifyCFNR") // Modify 'Call Forwarding Not Reachable' services for all users on the list
{
    modifyCallForwardingService(cfnrService);
}

if ($_POST["action"] == "modifyCFS") // Modify 'Call Forwarding Selective' services for all users on the list
{
    modifyCallForwardingSelectiveService();
}

if ($_POST["action"] == "resetAuthentication") // Reset Authentication password for all users on the list
{
    resetAuthenticationPassword();
}

if ($_POST["action"] == "modifyServices") // Assign/Unassign Service Pack or User Service to a User
{
    processUserServiceAssignment();
}

if ($_POST["action"] == "numberActivate" || $_POST["action"] == "numberDeactivate") // Assign/Unassign Service Pack or User Service to a User
{
    processUserNumberActivationDeactivation();
}

if ($_POST["action"] == "modifyCallingLineIDBlocking") // Assign/Unassign Service Pack or User Service to a User
{
    $callingLineIDBlocking = modifyCallingLineIDBlockingAssignment();
    foreach ($callingLineIDBlocking as $key => $value) {
        if( $value["status"] == "success" ) {
            echo "<p style='text-align:center'>" . $value["statusMsg"] . "</p>";
        } else if($value["status"] == "error") {
            addError("<p style='color:red; text-align:center'>Error: '" . $value["statusMsg"] . "' : '" . $key . "'</p>");
        }
    }
}

echo $isLocalError ? $result : "";

?>

