<?php
	require_once("/var/www/lib/broadsoft/login.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
	checkLogin();
        
        
        //Code @ 20 Sep 2018 EX-819
        function checkPolyComPhoneServices($servicePackArr, $deviceType, $ociVersion)
        {
            require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
            require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
            require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");            
            
            $serviceObj  = new Services();
            $syslevelObj = new sysLevelDeviceOperations();
            
            $userAssignedServicePackArr = $servicePackArr;
            
            $plycmServicePacksArr = $serviceObj->servicePacksHavePolycomPhoneServices($servicePackArr);
            $userHasPolycomSPFlag = "No";
            foreach($userAssignedServicePackArr as $k2 => $v2)
            {
                if(in_array($v2, $plycmServicePacksArr))
                {
                    $userHasPolycomSPFlag = "Yes";
                }    
            }        
            $returnResp = $syslevelObj->getSysytemDeviceTypeServiceRequest($deviceType, $ociVersion);
                        
            if($returnResp["Error"] == "" && empty($returnResp["Error"])){
            $respTmp = strval($returnResp["Success"]->supportsPolycomPhoneServices);
            }else{
                    $respTmp = "false";
            }
            //End code
            if($userHasPolycomSPFlag=="Yes" && $respTmp!="false"){
                $polycomPhoneService = "Yes";
            }else{
                $polycomPhoneService = "No";
            }
            
            return $polycomPhoneService;
        }
        //End code
        

	$fileName = $_SESSION["groupId"] . rand(1092918, 9281716);
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment;filename=\"" . $fileName . ".csv\"");
	header("Content-Transfer-Encoding: binary");
        
	require_once("getAllUsers.php");
	$message = "User Id, Activated, Registered, User Name, Extension, Device Name, Device Type, Analog Port Assignment, MAC Address, Department, Service Pack, Expiration, Type, DnD, Fwd, Fwd To, Remote Office, Remote Number, Polycom Phone Services, Custom Contact Directory\n";

	if(isset($_POST["selectedUserForDownload"]) && !empty($_POST["selectedUserForDownload"])){
		//get the selected user from users table make array from it
		$selectedUsersCsv = explode(";", $_POST["selectedUserForDownload"]);
		
		if(count($selectedUsersCsv) > 0){
			//make the user id table remove the static string and make the new array
			foreach($selectedUsersCsv as $key=>$val){
				if(!empty($val)){
					$selectedUsersCsv[$key] = str_replace("users_selected_", "", $val);
				}
			}
			//check the user array and check the user id exist in selected user array
			foreach($users as $key=>$val){
				if(!in_array($val["userId"], $selectedUsersCsv)){
					unset($users[$key]);
				}
			}
		}
	}
	$users = array_values($users);	        
	if (isset($users))
	{
		for ($a = 0; $a < count($users); $a++)
		{                        
                        
			$userId = $users[$a]["userId"];
                        
                        //Code added @ 10 April 2018 to show Register Status in csv like Yes, No, SAS
                        $fileNameTmp = "/var/www/SASTestingUser/SASTestUsers.csv";
                        if (file_exists ( $fileNameTmp )) {
                                $objcsv = new UserOperations ();
                                $searchInCsv = $objcsv->find_user_in_csv ( $fileNameTmp, $userId );
                        } else {
                                $searchInCsv = "";
                        }
                        //End Code                       
                        
                        
                        // get user dn number activation response
                        $userDn = new Dns ();
                        $numberActivateResponse = $userDn->getUserDNActivateListRequest ( $userId );
                        $phoneNumberStatus = "";
                        $userPhoneNumber = "";
                        if (empty ( $numberActivateResponse ['Error'] )) {
                                if (! empty ( $numberActivateResponse ['Success'] [0] ['phoneNumber'] )) {
                                        $userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
                                }
                                if (! empty ( $numberActivateResponse ['Success'] [0] ['status'] )) {
                                        $phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
                                }
                        }
                        // code ends
                        
                        
                        
			$userName = $users[$a]["firstName"] . " " . $users[$a]["LastName"];
			$extension = $users[$a]["extension"];
			$DnD = $users[$a]["DnD"];
			$Fwd = $users[$a]["Fwd"]["active"];
			$remote = $users[$a]["remote"]["active"];
			$department = $users[$a]["department"];
                        $servicePack = isset($users[$a]["servicePack"]) ? implode("; ", $users[$a]["servicePack"]) : "";
                        //Code added @ 20 Sep 2018
                        $servicePackArr = $users [$a] ["servicePack"];
                        $customContactDirectory = $users[$a]["ccd"]; 
                        //End code
                        
                        $DnDVal   = "";
                        $FwdVal   = "";
                        $FwdToVal = "";
			if ($DnD == "false")
			{
				$DnDVal = "Off";
			}
			else
			{
				$DnDVal = "On";
			}
			if ($Fwd == "true")
			{
				$FwdVal = "Active";
				$FwdToVal = $users[$a]["Fwd"]["FwdTo"];
			}
			else
			{
                               if (isset ( $users [$a] ["Fwd"] ["FwdTo"] ) && $users [$a] ["Fwd"] ["FwdTo"] != "") {
                                        $FwdVal = "Not Active";
                                        $FwdToVal = $users [$a] ["Fwd"] ["FwdTo"];
                                } else {
                                        $FwdVal = " ";
                                        $FwdToVal = " ";
                                }                            
			}
			if ($remote == "true")
			{
				$remote = "*";
				$remote_number = $users[$a]["remote"]["number"];
			}
			else
			{
				$remote = "";
				$remote_number = "";
			}
			if (isset($users[$a]["registration"]) and count($users[$a]["registration"]) > 0)
			{
                                
				for ($b = 0; $b < count($users[$a]["registration"]); $b++)
				{
                                        //Code added @ 10 April 2018 to show Register Status in csv
                                        if ($searchInCsv != "") {
                                                $isRegUserSasTester = "SAS";
                                        } else {                                        
                                                $isRegUserSasTester = "Yes";
                                        }
                                        //End Code
					$deviceName = $users[$a]["registration"][$b]["deviceName"];
//					$uri = $users[$a]["registration"][$b]["uri"];
					$expiration = $users[$a]["registration"][$b]["expiration"];
//					$linePort = $users[$a]["registration"][$b]["linePort"];
					$epType = $users[$a]["registration"][$b]["epType"];

					$deviceType = $users[$a]["registration"][$b]["deviceType"];
					$macAddress = $users[$a]["registration"][$b]["macAddress"];
                                        $portNumber = $users[$a]["registration"][$b]["portNumber"];                              

					$integration = $users[$a]["registration"][$b]["integration"];
					$ccd = $users[$a]["registration"][$b]["ccd"];
                                        
                                        $polycomPhoneService =  checkPolyComPhoneServices($servicePackArr, $deviceType, $ociVersion);
					
					$message .=
					        "=\"" . $userId . "\"," .
                                                "=\"" . $phoneNumberStatus . "\"," .
						"=\"" . $isRegUserSasTester . "\"," .
						"=\"" . $userName . "\"," .
						"=\"" . $extension . "\"," .
						"=\"" . $deviceName . "\"," .
						"=\"" . $deviceType . "\"," .
                                                "=\"" . $portNumber . "\"," .
						"=\"" . $macAddress . "\"," .
						"=\"" . $department . "\"," .
						"=\"" . $servicePack . "\"," .
						"=\"" . $expiration . "\"," .
						"=\"" . $epType . "\"," .
						"=\"" . $DnDVal . "\"," .
						"=\"" . $FwdVal . "\"," .
						"=\"" . $FwdToVal . "\"," .
						"=\"" . $remote . "\"," .
						"=\"" . $remote_number . "\"," .
						"=\"" . $polycomPhoneService . "\"," .
						"=\"" . $customContactDirectory . "\"\n"; //Code added @ 20 Sep 2018
				}
			}
			else
			{
                            //Code added @ 10 April 2018 to show Register Status in csv
                            if ($searchInCsv != "") {
                                    $isUnRegUserSasTester = "SAS";
                            } else {                                    
                                    $isUnRegUserSasTester = "No";
                            }
                            //End Code
			    $macAddress = $users[$a]["macAddress"];
			    $portNumber = $users[$a]["portNumber"];
			    $deviceName = $users[$a]["deviceName"];
			    $deviceType = $users[$a]["deviceType"];
			    $epType     = $users[$a]["epType"];
                            
                            $polycomPhoneService =  checkPolyComPhoneServices($servicePackArr, $deviceType, $ociVersion);
                            
			    $message .=
				        "=\"" . $userId . "\"," .
                                        "=\"" . $phoneNumberStatus . "\"," .
					"=\"" . $isUnRegUserSasTester . "\",".
					"=\"" . $userName . "\"," .
					"=\"" . $extension . "\"," .
					"=\"" . $deviceName . "\"," .
					"=\"" . $deviceType . "\"," .
					"=\"" . $portNumber . "\"," .
					"=\"" . $macAddress . "\"," .
					"=\"" . $department . "\"," .
					"=\"" . $servicePack . "\"," .
					"=\"\"," .
					"=\"" . $epType . "\"," .
					"=\"" . $DnDVal . "\"," .
					"=\"" . $FwdVal . "\"," .
					"=\"" . $FwdToVal . "\"," .
					"=\"" . $remote . "\"," .
					"=\"" . $remote_number . "\"," .
					"=\"" . $polycomPhoneService . "\"," .    //Code added @ 20 Sep 2018
					"=\"" . $customContactDirectory . "\"\n"; //Code added @ 20 Sep 2018
			}
		}
	}        
	$fp = fopen("php://output", "a");
	fwrite($fp, $message);
	fclose($fp);
?>
