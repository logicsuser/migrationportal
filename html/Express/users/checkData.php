<?php
require_once ("/var/www/html/Express/config.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");
require_once ("/var/www/lib/broadsoft/adminPortal/arrays.php");

function formatBackgroundField($content, $backgroundStyle, $color)
{
    return "<td class=\"errorTableRows\" style=\"background:" . $backgroundStyle . ";color:" .$color. "\">" . $content . "</td>";
}

function validateForwardingNumber($number)
{
    global $restrictedNumbers;
    $errMsg = "";
    
    $numOnly = str_replace("+", "", $number);
    $numOnly = str_replace("-", "", $numOnly);
    $numOnly = str_replace(" ", "", $numOnly);
    $numOnly = str_replace("*", "", $numOnly);
    $numOnly = str_replace("?", "", $numOnly);
    
    if (! is_numeric($numOnly)) {
        $errMsg = " : must be  -, 0-9 or a E.164 formatted number.";
    } else {
        foreach ($restrictedNumbers as $k => $v) {
            if (substr($number, 0, strlen($k)) === $k and strlen($number) > strlen($k) + 5) {
                $errMsg = " is restricted.";
                break;
            }
        }
    }
    
    return $errMsg;
}

function validateCFSCriteria($cfsCriteriaActive)
{
    global $error, $bg;
    
    $resp = "";
    $index = 0;
    
    foreach ($_SESSION["cfsCriteria"] as $key => $value) {
        $bg = CHANGED;
        $name = $value["cfsCriteriaName"];
        $data1 = "CFS Criteria Active: " . $name;
        $data2 = $cfsCriteriaActive[$index];
        
        // Validate forwarding number
        $fwdNum = $value["cfsAnotherFwdNum"];
        if ($fwdNum != "") {
            $errMsg = validateForwardingNumber($fwdNum);
            if ($errMsg != "") {
                $error = 1;
                $bg = INVALID;
                $data1 = "CFS Criteria: " . $name;
                $data2 = $fwdNum . $errMsg;
            }
        }
        
        if ($error) {
            goto cont;
        }
        
        // Validate list of specific phone numbers
        for ($i = 1; $i <= 12; $i ++) {
            $attr = "cfsCriteriaSpecific";
            if ($i < 10) {
                $attr .= "0";
            }
            $attr .= $i;
            $fwdNum = $value[$attr];
            
            if ($fwdNum != "") {
                $errMsg = validateForwardingNumber($fwdNum);
                if ($errMsg != "") {
                    $error = 1;
                    $bg = INVALID;
                    $data1 = "CFS Criteria: " . $name;
                    $data2 = $fwdNum . $errMsg;
                }
            }
            if ($error) {
                goto cont;
            }
        }
        
        cont:
        $resp .= "<tr>";
        $resp .= formatBackgroundField($data1);
        $resp .= formatBackgroundField($data2);
        $resp .= "</tr>";
        
        $index ++;
    }
    
    return $resp;
}

// Key arrays for special processing
$callForwardingNumberKeys = array(
    "phSipFwdCFA",
    "phSipFwdCFNR",
    "phSipFwdCFS"
);
$screenedOutKeys = array(
    "authPassword2",
    "genAuthPassword"
);
// $modsVmserivecs = array("voiceMessageCarbonCopyEmailAddress","transferPhoneNumber");
$cfsCriteriaActive = array();

// $testStr = "";
$data = $errorTableHeader;
$hasPlayRingReminder = false;
$hasCFSCriteriaActiveStatus = false;
$modService = "";
$error = 0;

if(isset($_POST["voiceMessaging"]) && $_POST["voiceMessaging"] == "false"){
    foreach ($vmArray as $value){
        unset($_POST[$value]);
    }
}

if(!array_key_exists("processing", $_POST) && (isset($_POST["voiceMessaging"]) && $_POST["voiceMessaging"] == "true")){
    $_POST["processing"] = "";
}

foreach ($_POST as $key => $value) {
    
    if (in_array($key, $screenedOutKeys)) {
        continue;
    }
    $bg = CHANGED;
   
    if ($key == "processing") {
        if (isset($_POST["processing"]) && $_POST["processing"] == '') {
            $error = 1;
            $bg = INVALID;
            $value = $_SESSION["userModNames"][$key] . " is a required field.";
        }
    }
    
    
    
    
    if ($key == "voiceMessageCarbonCopyEmailAddress") {
        
        if ($_POST["sendCarbonCopyVoiceMessage"] == "true" and $value == "") {
            
            $error = 1;
            $bg = INVALID;
            $value = $_SESSION["userModNames"][$key] . " is required when you select to send a carbon copy of the voice message.";
        }
    }
    
    if ($key == "transferPhoneNumber") {
        if ($_POST["transferOnZeroToPhoneNumber"] == "true" and $value == "") {
            $error = 1;
            $bg = INVALID;
            $value = $_SESSION["userModNames"][$key] . " is required when you select to transfer on '0'.";
        } else if (strlen($value) > 0 and ! preg_match("/^[ 0-9-]+$/", $value)) {
            $error = 1;
            $bg = INVALID;
            $value = $_SESSION["userModNames"][$key] . " must be numbers, dashes, and spaces only.";
        }
    }
    if ($key == "useGroupDefault") {
        $error = 1;
        $bg = INVALID;
        $value = $_SESSION["userModNames"][$key] . " must be numbers, dashes, and spaces only.";
    }
    
    if ($key == "activateCFA") {
        $modService = "CFA";
    }
    
    if ($key == "activateCFNR") {
        $modService = "CFNR";
    }
    
    if ($key == "activateCFS") {
        $modService = "CFS";
    }
    
    if ($key == "srvPack" || $key == "usrServices") {
        $modService = "SwPck";
    }
    
    if ($key == "authPassword1" || $key == "authGeneratedPassword") {
        $modService = "authPasswords";
    }
    
    if ($key == "ringReminderChk" || $key == "ringReminderChkCFS") {
        $hasPlayRingReminder = true;
    }
    
    if (substr($key, 0, 10) == "cfsCritChk") {
        $hasCFSCriteriaActiveStatus = true;
        $cfsCriteriaActive[] = "true";
        continue;
    }
    
    if (substr($key, 0, 13) == "cfsCritHidden") {
        if (! $hasCFSCriteriaActiveStatus) {
            $cfsCriteriaActive[] = "false";
        }
        $hasCFSCriteriaActiveStatus = false;
        continue;
    }
    
    if ($key == "authPassword1") {
        $value = $value;
    }
    
    if ($value == "srvPackNone" || $value == "usrServiceNone") {
        $value = "None";
    }
    
    // Preserve modification attributes for each service
    $_SESSION[$modService][$key] = $value;
    
    if ($key == "authPassword1" && $value == "") {
        continue;
    }
    if ($key == "authGeneratedPassword" && $value == "") {
        continue;
    }
    
    // Validate Call Forwarding Number format
    if (in_array($key, $callForwardingNumberKeys)) {
        if ($value != "") {
            $errMsg = validateForwardingNumber($value);
            if ($errMsg != "") {
                $error = 1;
                $bg = INVALID;
                $value .= $errMsg;
            }
        } else {
            $value = "None";
        }
    }
    
    $data .= "<tr>";
    if ($error == 1) {
        $data .= formatBackgroundField($_SESSION["gwUsersMod"][$key], $bg, "#fff");
        $data .= formatBackgroundField($value);
    } else {
        $data .= formatBackgroundField($_SESSION["gwUsersMod"][$key], $bg, "#fff");
        $data .= formatBackgroundField($value);
    }
    $data .= "</tr>";
}

// Unchecked ring reminder will not be posted on the form, therefore set session variable here
if (($modService == "CFA" || $modService == "CFS") && ! $hasPlayRingReminder) {
    $data .= "<tr>";
    $data .= formatBackgroundField($_SESSION["gwUsersMod"]["ringReminderChk"], CHANGED, "#fff");
    $data .= formatBackgroundField("off");
    $data .= "</tr>";
    
    $_SESSION[$modService]["ringReminderChk"] = "off";
}

// Process CFS
if ($modService = "CFS") {
    $data .= validateCFSCriteria($cfsCriteriaActive);
}

$data .= "</table>";
echo $error . $data;

?>