<?php
/**
 * HTML content of Selective Call Forwarding criteria dialog form
 * Created by Karl.
 * Date: 6/15/2016
 */
    require_once("/var/www/html/Express/config.php");
    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();
    require_once("/var/www/lib/broadsoft/adminPortal/getAllSchedules.php");

    const defaultTimeSchedule = "Every Day All Day";
    const defaultHolidaySchedule = "None";

    $mode = "create";

    $cfsCriteriaName = "";
    $cfsCriteriaFwd = "Forward To Default Number";
    $cfsAnotherFwdNum = "";
    $cfsCriteriaTime = defaultTimeSchedule;
    $cfsCriteriaHoliday = defaultHolidaySchedule;
    $cfsCriteriaFwdFrom = "Any";
    $cfsCriteriaFwdFromPrivate = "false";
    $cfsCriteriaFwdFromUnavailable = "false";
    $cfsCriteriaSpecific01 = "";
    $cfsCriteriaSpecific02 = "";
    $cfsCriteriaSpecific03 = "";
    $cfsCriteriaSpecific04 = "";
    $cfsCriteriaSpecific05 = "";
    $cfsCriteriaSpecific06 = "";
    $cfsCriteriaSpecific07 = "";
    $cfsCriteriaSpecific08 = "";
    $cfsCriteriaSpecific09 = "";
    $cfsCriteriaSpecific10 = "";
    $cfsCriteriaSpecific11 = "";
    $cfsCriteriaSpecific12 = "";

    // Retrieve criteria information in update mode
    if (isset($_POST["criteriaName"])) {
        $name = $_POST["criteriaName"];
        $mode = "update";

        $cfsCriteriaName = $_SESSION["cfsCriteria"][$name]["cfsCriteriaName"];
        $cfsCriteriaFwd = $_SESSION["cfsCriteria"][$name]["cfsCriteriaFwd"];
        $cfsAnotherFwdNum = $_SESSION["cfsCriteria"][$name]["cfsAnotherFwdNum"];
        $cfsCriteriaTime = $_SESSION["cfsCriteria"][$name]["cfsCriteriaTime"];
        $cfsCriteriaHoliday = $_SESSION["cfsCriteria"][$name]["cfsCriteriaHoliday"];
        $cfsCriteriaFwdFrom = $_SESSION["cfsCriteria"][$name]["cfsCriteriaFwdFrom"];
        $cfsCriteriaFwdFromPrivate = $_SESSION["cfsCriteria"][$name]["cfsCriteriaFwdFromPrivate"];
        $cfsCriteriaFwdFromUnavailable = $_SESSION["cfsCriteria"][$name]["cfsCriteriaFwdFromUnavailable"];

        $cfsCriteriaSpecific01 = $_SESSION["cfsCriteria"][$name]["cfsCriteriaSpecific01"];
        $cfsCriteriaSpecific02 = $_SESSION["cfsCriteria"][$name]["cfsCriteriaSpecific02"];
        $cfsCriteriaSpecific03 = $_SESSION["cfsCriteria"][$name]["cfsCriteriaSpecific03"];
        $cfsCriteriaSpecific04 = $_SESSION["cfsCriteria"][$name]["cfsCriteriaSpecific04"];
        $cfsCriteriaSpecific05 = $_SESSION["cfsCriteria"][$name]["cfsCriteriaSpecific05"];
        $cfsCriteriaSpecific06 = $_SESSION["cfsCriteria"][$name]["cfsCriteriaSpecific06"];
        $cfsCriteriaSpecific07 = $_SESSION["cfsCriteria"][$name]["cfsCriteriaSpecific07"];
        $cfsCriteriaSpecific08 = $_SESSION["cfsCriteria"][$name]["cfsCriteriaSpecific08"];
        $cfsCriteriaSpecific09 = $_SESSION["cfsCriteria"][$name]["cfsCriteriaSpecific09"];
        $cfsCriteriaSpecific10 = $_SESSION["cfsCriteria"][$name]["cfsCriteriaSpecific10"];
        $cfsCriteriaSpecific11 = $_SESSION["cfsCriteria"][$name]["cfsCriteriaSpecific11"];
        $cfsCriteriaSpecific12 = $_SESSION["cfsCriteria"][$name]["cfsCriteriaSpecific12"];
    }


    // Build dropdown options menu of schedules based on schedule type
    // Schedule type can by 'Time' or 'Holiday'
    // $schedules is the list of all group schedules
    function getScheduleOptions($type) {
        global $schedules, $cfsCriteriaTime, $cfsCriteriaHoliday;

        $selection = $type == "Time" ? $cfsCriteriaTime : $cfsCriteriaHoliday;
        $defaultSelection = $type == "Time" ? defaultTimeSchedule : defaultHolidaySchedule;

        // Build the list of options and mark one of them 'selected' based on selection stored in $_SESSION
        // <option value="Every Day All Day" selected>Every Day All Day</option>
        $str = "<option value=\"\"";
        $str .= $selection == $defaultSelection ? " selected" : "";
        $str .= ">" . $defaultSelection . "</option>";
        echo $str;

        foreach ($schedules as $key=>$value) {
            if ($value["level"] == "System") {
                $level = " (System)";
            }
            elseif ($value["level"] == "Service Provider") {
                $level = " (Enterprise)";
            }
            elseif ($value["level"] == "Group") {
                $level = " (Group)";
            }
            else { $level = " (User)";}

            if ($value["type"] == $type) {
                echo "<option value=\"" . $value["name"] . "\"";
                echo $selection == $value["name"] ? " selected" : "";
                echo ">" . $value["name"] . $level . "</option>";
            }
        }
    }

?>
<script>

    function evalCriteriaName(txtBox) {
        var enableStatus = txtBox.value.length > 0 ? "enable" : "disable";
        $(".ui-dialog-buttonpane button:contains('Create')").button(enableStatus);
    }
</script>
<div style="font-size: 12px">
    <form action="#" method="POST" name="newCFSCriteriaForm" id="newCFSCriteriaForm" class="container">
        <!-- Criteria name  -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
            <label for="cfsCriteriaName" class="labelText"><?php echo $mode == "create" ? "<span class=\"required\" style=\"float: right;\">*</span> " : ""; ?>Criteria name:</label>
            <input class="diaBox12 form-control" type="text" name="cfsCriteriaName" id="cfsCriteriaName" value="<?php echo $cfsCriteriaName; ?>" size="35" onchange="evalCriteriaName(this)" oninput="evalCriteriaName(this)" <?php echo $mode == "update" ? "disabled" : ""; ?> >
            <?php if ($mode == "update") { ?>
                <input type="hidden" name="cfsCriteriaNameUpdate" value="<?php echo $cfsCriteriaName; ?>" >
            <?php } ?>
            </div>
        </div>
    </div>

                <!-- Forwarding Options  -->
    <div class="row">
        <div class="col-md-3 modalLeftDiv">
            <div class="form-group">
            <label for="cfsCriteriaFwdDefault" class="labelText">Forwarding</label>
            </div>
            <div class="form-group">
            <label class="labelText">Options:</label>
            </div>
        </div>
        <div class="col-md-9 modalRightDiv">
            <div class="col-md-12">
                <input style="vertical-align: middle" type="radio" name="cfsCriteriaFwd" id="cfsCriteriaFwdDefault" value="Forward To Default Number" <?php echo $cfsCriteriaFwd == "Forward To Default Number" ? "checked" : ""; ?> /><label for="cfsCriteriaFwdDefault"><span></span></label>
                <label class="labelText customLabelText">Use Default Forward phone number / SIP-URI</label>
            </div>
            <div class="col-md-12">
                <input style="vertical-align: middle" type="radio" name="cfsCriteriaFwd" id="cfsCriteriaFwdSpecified" value="Forward To Specified Number" <?php echo $cfsCriteriaFwd == "Forward To Specified Number" ? "checked" : ""; ?> /> <label for="cfsCriteriaFwdSpecified"><span></span></label>
                <label class="labelText customLabelText">Forward to another phone number / SIP-URI:</label>
            </div>
            <div class="col-md-12">
                <div class="col-md-6 modalRightDiv modalLeftDiv" style="padding-left:40px;"><input class="diaBox12 form-control" type="text" name="cfsAnotherFwdNum" id="cfsAnotherFwdNum" value="<?php echo $cfsAnotherFwdNum; ?>" size="35"></div>
	        </div>
            <div class="col-md-12">
            <input style="vertical-align: middle" type="radio" name="cfsCriteriaFwd" id="cfsCriteriaFwdNone" value="Do not forward" <?php echo $cfsCriteriaFwd == "Do not forward" ? "checked" : ""; ?> /><label for="cfsCriteriaFwdNone"><span></span></label>
            <label class="labelText customLabelText">Do not forward</label>
            </div>
        </div>
    </div>
        <!-- Schedules  -->
    <div class="row">
    <div class="col-md-3 modalLeftDiv">
            <div class="form-group">
            <label class="labelText">Time Schedule:</label>
            <div class="dropdown-wrap">
            <select class="diaBox12" name="cfsCriteriaTime" id="cfsCriteriaTime" style="width: 100% !important;">
                <?php getScheduleOptions("Time"); ?>
            </select>
            </div>
            </div>
            <div class="form-group">
            <label for="cfsCriteriaFwdDefault" class="labelText">Holiday Schedule:</label>
            <div class="dropdown-wrap">
            <select class="diaBox12" name="cfsCriteriaHoliday" id="cfsCriteriaHoliday"style="width: 100% !important;">
                <?php getScheduleOptions("Holiday"); ?>
            </select>
            </div>
            </div>
    </div>

    </div>
        <!-- Forwarding incoming calls  -->
<div class="row">
    <div class="col-md-3 modalLeftDiv">
            <div class="form-group">
            <label for="cfsCriteriaFwdFromAny" class="labelText">Forwarding<br>incoming calls:</label>
            </div>
    </div>
    <div class="col-md-9 modalRightDiv">
        <div class="">
          <input style="vertical-align: middle" type="radio" name="cfsCriteriaFwdFrom" id="cfsCriteriaFwdFromAny" value="Any" <?php echo $cfsCriteriaFwdFrom == "Any" ? "checked" : ""; ?> ><label for="cfsCriteriaFwdFromAny"><span></span></label>
          <label class="labelText customLabelText">From any phone number</label>
        </div>
	
        <div class="">
	    
            <input style="vertical-align: middle" type="radio" name="cfsCriteriaFwdFrom" id="cfsCriteriaFwdFromSpecified" value="Specified Only" <?php echo $cfsCriteriaFwdFrom == "Specified Only" ? "checked" : ""; ?> ><label for="cfsCriteriaFwdFromSpecified"><span></span></label>
            <label class="labelText customLabelText">From the following phone numbers:</label>

		<div class="chkFromAny">
                 <input style="vertical-align: middle" type="checkbox" name="cfsCriteriaFwdFromPrivate" id="cfsCriteriaFwdFromPrivate" value="true" <?php echo $cfsCriteriaFwdFromPrivate == "true" ? "checked" : ""; ?> ><label for="cfsCriteriaFwdFromPrivate"><span></span></label>
		 <label class="labelText">From any private number</label>
                </div>
		<div class="chkFromAny"> 
		<input style="vertical-align: middle" type="checkbox" name="cfsCriteriaFwdFromUnavailable" id="cfsCriteriaFwdFromUnavailable" value="true" <?php echo $cfsCriteriaFwdFromUnavailable == "true" ? "checked" : ""; ?> ><label for="cfsCriteriaFwdFromUnavailable"><span></span></label>
		<label class="labelText">From any unavailable number</label></div>
        </div>   
				
    </div>      
</div>

<div class="row">
<div class="col-md-12"><label class="labelText">From specific phone numbers:</label></div>
    <div class="col-md-4 inputTextDiv">
        <input class="diaBox12" type="text" name="cfsCriteriaSpecific01" id="cfsCriteriaSpecific01" value="<?php echo $cfsCriteriaSpecific01; ?>" size="25">
        
        <input class="diaBox12" type="text" name="cfsCriteriaSpecific02" id="cfsCriteriaSpecific02" value="<?php echo $cfsCriteriaSpecific02; ?>" size="25">
        
       <input class="diaBox12" type="text" name="cfsCriteriaSpecific03" id="cfsCriteriaSpecific03" value="<?php echo $cfsCriteriaSpecific03; ?>" size="25">
        
       <input class="diaBox12" type="text" name="cfsCriteriaSpecific04" id="cfsCriteriaSpecific04" value="<?php echo $cfsCriteriaSpecific04; ?>" size="25">
    </div>
    <div class="col-md-4 inputTextDiv">
       <input class="diaBox12" type="text" name="cfsCriteriaSpecific05" id="cfsCriteriaSpecific05" value="<?php echo $cfsCriteriaSpecific05; ?>" size="25">
       
       <input class="diaBox12" type="text" name="cfsCriteriaSpecific06" id="cfsCriteriaSpecific06" value="<?php echo $cfsCriteriaSpecific06; ?>" size="25">
       
       <input class="diaBox12" type="text" name="cfsCriteriaSpecific07" id="cfsCriteriaSpecific07" value="<?php echo $cfsCriteriaSpecific07; ?>" size="25">
       
       <input class="diaBox12" type="text" name="cfsCriteriaSpecific08" id="cfsCriteriaSpecific08" value="<?php echo $cfsCriteriaSpecific08; ?>" size="25">
    </div>
    <div class="col-md-4 inputTextDiv">
       <input class="diaBox12" type="text" name="cfsCriteriaSpecific09" id="cfsCriteriaSpecific09" value="<?php echo $cfsCriteriaSpecific09; ?>" size="25">
       
       <input class="diaBox12" type="text" name="cfsCriteriaSpecific10" id="cfsCriteriaSpecific10" value="<?php echo $cfsCriteriaSpecific10; ?>" size="25">
       
       <input class="diaBox12" type="text" name="cfsCriteriaSpecific11" id="cfsCriteriaSpecific11" value="<?php echo $cfsCriteriaSpecific11; ?>" size="25">
       
       <input class="diaBox12" type="text" name="cfsCriteriaSpecific12" id="cfsCriteriaSpecific12" value="<?php echo $cfsCriteriaSpecific12; ?>" size="25">
    </div>
</div>   
    </form>
</div>
