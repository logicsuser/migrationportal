<?php
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

$fileName = $_SESSION["groupId"] . rand(1092918, 9281716);
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment;filename=\"" . $fileName . ".csv\"");
header("Content-Transfer-Encoding: binary");

require_once("getAllUsers.php");
$message = " Device Name,DeviceType,Registration Status,IP,Expiry,Agent Type\n";
if (isset($users))
{
    for ($a = 0; $a < count($users); $a++)
    {

        if (isset($users[$a]["registration"]) and count($users[$a]["registration"]) > 0)
        {
            for ($b = 0; $b < count($users[$a]["registration"]); $b++)
            {
                $deviceLevel = $users[$a]["registration"][$b]["deviceLevel"];
                $agentType = $users[$a]["registration"][$b]["agentType"];
                $deviceName = $users[$a]["registration"][$b]["deviceName"];
                $expiration = $users[$a]["registration"][$b]["expiration"];
                $epType = $users[$a]["registration"][$b]["epType"];
                $publicIp = $users[$a]["registration"][$b]["publicIp"];

                $deviceType = $users[$a]["registration"][$b]["deviceType"];

                $message .=
                    "=\"" . $deviceName . "\"," .
                    "=\"" . $deviceType . "\"," .
                    "=\"Registered\"," .
                    "=\"" . $publicIp .  "\"," .
                    "=\"" . $expiration . "\"," .
                    "=\"" . $agentType . "\"\n";
            }
        }
        else
        {
            $agentType = "";
            $deviceName = $users[$a]["deviceName"];
            $expiration = "";
            $epType = "";
            $publicIp = "";
            $deviceType = $users[$a]["deviceType"];

            $message .=
                "=\"" . $deviceName . "\"," .
                "=\"" . $deviceType . "\"," .
                "=\"Non-Registered\"," .
                "=\"" . $publicIp .  "\"," .
                "=\"" . $expiration . "\"," .
                "=\"" . $agentType . "\"\n";
        }
    }
}
//echo $message; die;
$fp = fopen("php://output", "a");
fwrite($fp, $message);
fclose($fp);
?>
