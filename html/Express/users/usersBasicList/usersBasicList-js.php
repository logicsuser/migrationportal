<?php
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
?>

<script>
    var showDetailLoader = true;
    var showBasicLoader = true;

    var basicUsersTableLoaded = false;
    var loadBasicUsersTable = true;
    var tmpPhoneNumber;

    if('<?php echo $usersView_selected ?>' == "Basic" || '<?php echo $usersView_selected; ?>' == "") {
        loadBasicUsersTable = true;
    } else {
        loadBasicUsersTable = false;
    }

    $(function() {
        $("#selectAllDiv_Basic").hide();
        $(".userBasicListDiv").hide();
        $("#downloadCSV_Basic").hide();
        $(".hideInitial").hide();
        basicViewUsesList();
    });

    var basicUserListDataTable = null;
    var basicUserListCount = 0;
    var basicUserListQuerySession = 0;

    var hideInitialBasicInfo_selectAllDiv = $('.hideInitialBasicInfo_selectAllDiv');
    var hideInitialBasicInfo_groupModifyDiv = $('.hideInitialBasicInfo_groupModifyDiv');

    function basicUserListTableSorter(count) {
        if( typeof count === "undefined") {
        	count = 1;
        }

        hideInitialBasicInfo_selectAllDiv = $('.hideInitialBasicInfo_selectAllDiv');
        hideInitialBasicInfo_groupModifyDiv = $('.hideInitialBasicInfo_groupModifyDiv');

        basicUserListDataTable = $('#basicUserListTable').DataTable({
            scrollY: 300,
            destroy: true,
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            pagingType: "dk_express_pagination",
            lengthMenu: [50, 100, 200],
            searching: false,
            autoWidth: false,
            deferRender: false,

            info: true,
            language: {
                emptyTable: " ",
                infoEmpty: (count ? "Loading ..." : "No Users Found"),
                lengthMenu: "Show<br/> _MENU_ <br/>users",
                info: "Showing _START_ to _END_ of _TOTAL_ users",
                oPaginate: {
                    sPageJumpText: "Jump to pg ",
                    //sPageJumpGoBtn: 'GO'
                }
            },

            dom: '<"wrapper"<"usersBasicListTable_top_bar dataTables_top_bar"<"column1"><"column2"><"column3"><"column4"l>>'
                + 't'
                + '<"usersBasicListTable_bottom_bar dataTables_bottom_info_bar"<"retrieving_info"><"currently_showing_info"i>p>>',

            "initComplete": initCompleteBasic
        });

    }


    function initCompleteBasic() {

        //$('#downloadCSV_Basic').addClass('disabled').prop('disabled', true);
        //$('#selectAllChk_Basic').addClass('disabled').prop('disabled', true);

        var DT_top_bar_column_1 = $('.usersBasicListTable_top_bar .column1');
        var DT_top_bar_column_2 = $('.usersBasicListTable_top_bar .column2');
        var DT_top_bar_column_3 = $('.usersBasicListTable_top_bar .column3');
        var DT_top_bar_column_4 = $('.usersBasicListTable_top_bar .column4');

        //console.log(hideInitialBasicInfo_selectAllDiv);
        //console.log(hideInitialBasicInfo_groupModifyDiv);

        var tmp_selectAllDiv = hideInitialBasicInfo_selectAllDiv.clone();
        var tmp_groupModifyDiv = hideInitialBasicInfo_groupModifyDiv.clone();

        DT_top_bar_column_1.prepend(hideInitialBasicInfo_selectAllDiv);
        DT_top_bar_column_3.prepend(hideInitialBasicInfo_groupModifyDiv);

        hideInitialBasicInfo_selectAllDiv = tmp_selectAllDiv;
        hideInitialBasicInfo_groupModifyDiv = tmp_groupModifyDiv;

        //console.log(hideInitialBasicInfo_selectAllDiv);
        //console.log(hideInitialBasicInfo_groupModifyDiv);


        //New UI
        //var actionBtns = $('.usersDetailedListTable_action_buttons');
        //DT_top_bar_column_4.prepend(actionBtns);

    }

    function basicViewUsesList(filters) {
    	pendingProcess.push("User Basic List");
        filters = filters || null;

        if (!loadBasicUsersTable) {
            return false;
        }

        basicUsersTableLoaded = true;
        loadBasicUsersTable = false;

        var dataToSend = $("#departmentDropdown_Basic").serialize() + '&action=GetAllUserInGroup';
        if (filters) {
            dataToSend += '&' + filters;
            if ($('#useBasicFilteredListInDetailedList').is(':checked')) {
                dataToSend += '&useBasicFilteredListInDetailedList=1';
                detailedUsersTableLoaded = false;
            }
        }
        detailedUsersTableLoaded = false;
        //dataToSend + '&=selectedView' + selectedView;
        if (!dataToSend) {
            dataToSend = [];
            dataToSend.push({name: "department", value: ""});
        }

        if (!$("#loading_Basic").is(':visible')) {
            $('#basicUserListTableLoading').show();
        }
        $('#basicUserListNoUserAvailabe').hide();
        $('.selectedBasicUsersActionBtn').hide();

        //User Selections
        $('#selectAllChk_Basic').prop('checked', false);
        selectedUsers_Basic = {};
        toggleAllBasicUsers();

        //Reset Detailed List
        if ($('#usersDetailedListTable').length) {
            $('#usersDetailedListTable').remove();
        }

        basicUserListCount = 0;
        basicUserListQuerySession++;

        $.ajax({
            type: "POST",
            url: "users/usersBasicList/getUsersWithBasicInfo.php",
            //data: {"action": "GetAllUserInGroup"},
            data: dataToSend,
            success: function (result) {
            	if(foundServerConErrorOnProcess(result, "User Basic List")) {
					return false;
                }
                $('#basicUserListTableLoading').hide();
                $("#basicUsersQuickFiltersBox").show();

                $("#loading_Basic").hide();

                //$("#usersTable").html(result);
                showBasicLoader = false;
                var response = JSON.parse(result);

                if (!response.errorMsg && response.users && response.users.length > 0) {

                    var users = response.users;

                    basicUserListCount = response.count;

                    if (basicUserListDataTable) {
                        try {
                            basicUserListDataTable.clear().draw();
                        } catch (e) {
                            console.log(e);
                        }
                    }

                    $(".hideInitial").show();
                    $(".userBasicListDiv").show();

                    basicUserListTableSorter(basicUserListCount);

                    drawUsersWithBBasicInfo(users[0], basicUserListQuerySession);

                    if (users.length > 1) {
                        getBasicUsersActivationStatus(users, 1, basicUserListQuerySession);
                    }

                    if(basicUserListCount == 0) {
                    	$("#selectAllChk_Basic").prop('disabled', true);
                    	$("#selectAllDiv_Basic .selectAllSpan").addClass("checkBoxLikeDisable");
                    } else {
                    	$("#selectAllChk_Basic").prop('disabled', false);
                    	$("#selectAllDiv_Basic .selectAllSpan").removeClass("checkBoxLikeDisable");
                    }
                    
                } else {
                    if (response.errorMsg) {
                        $('#basicUserListNoUserAvailabe').html(response.errorMsg);
                    } else {
                        $('#basicUserListNoUserAvailabe').html('No User Availabe');
                    }
                    $("#selectAllDiv_Basic").hide();
                    $("#downloadCSV_Basic").hide();
                    $('#basicUserListNoUserAvailabe').show();
                }

                //setTableSorter();
                //checkUserLength();
                //eventAfterLoad();
            }
        });
    }

    function getBasicUsersActivationStatus(users, page, querySession) {

        if(basicUserListQuerySession === querySession && basicUserListDataTable) {

            if(users[page] && users[page].length) {

                try {
                    var userQueryParam = {'action': 'getActivationStatus', 'users': users[page]};
                    var current_count = basicUserListDataTable.page.info().recordsTotal;
                    var user_count = current_count + users[page].length;

                    $('.usersBasicListTable_bottom_bar .retrieving_info').html('Retrieving ' + user_count + ' of ' + basicUserListCount + ' users');

                    $.ajax({

                        type: "POST",
                        url: "users/usersBasicList/getUsersWithBasicInfo.php",
                        data: userQueryParam,
                        success: function (getActivationStatusResponse) {

                            var usersList = JSON.parse(getActivationStatusResponse);

                            drawUsersWithBBasicInfo(usersList, querySession);

                            page++;

                            if (page === users.length) {
                                //All Pages have been retrieved
                            } else {
                                getBasicUsersActivationStatus(users, page, querySession);
                            }
                        }
                    });
                } catch (e) {
                    console.log(e);
                }

            }

        }

    }

    function drawUsersWithBBasicInfo(users, querySession) {

        if (users && basicUserListQuerySession === querySession && basicUserListDataTable) {

            var rowIndex;
            try {
                rowIndex = basicUserListDataTable.page.info().recordsTotal;
            } catch (e) {
                console.log(e);
            }

            $.each(users, function (userKey, userVal) {
                var basicTableTr = "";
                if ('<?php echo $_SESSION ["permissions"] ["modifyUsers"]; ?>' == "1") {
                    basicTableTr += "<tr class='userIdVal_Basic' id='" + userVal.userId + "' data-lastname='" + userVal.lastName + "' data-firstname='" + userVal.firstName + "'data-phone='" + userVal.phoneNumber + "' data-extension='" + userVal.ext + "' >";
                } else {
                    basicTableTr += "<tr>";
                }

            basicTableTr += "<td><input type='checkbox' onclick='onUserSelect_Basic(this)' class='checkUserListBox_Basic' data-id='userCheckBox" + rowIndex + "' name = '" + userVal.userId + "' " +
	            "id='users_selected_Basic_" + userVal.userId + "' ";
            if ('<?php echo $_SESSION ["permissions"]["groupWideUserModify"]?>' == '0') {
                basicTableTr += " disabled";
            }
            basicTableTr += "><label for='users_selected_Basic_" +userVal.userId+ "'><span  onmouseover='checkBoxEventAct_Basic(true)' onmouseout='checkBoxEventAct_Basic(false)'></span></label></td>";
            // 					 basicTableTr += "<td></td>";
            basicTableTr += "<td class='thclassR'>" + userVal.userId + "</td>";
            basicTableTr += "<td class='thclassR'>" + userVal.activated + "</td>";
            basicTableTr += "<td class='thclassR'>" + userVal.userName + "</td>";
            basicTableTr += "<td class='thclassR'>" + userVal.phoneNumber + "</td>";
            basicTableTr += "<td class='thclassR'>" + userVal.ext + "</td>";
            if ("<?php echo $useDepartments ?>" == "true") {
                basicTableTr += "<td class='thclassR'>" + userVal.department + "</td>";
            }
                basicTableTr += "</tr>";

                if(basicUserListQuerySession === querySession) {
                    try {
                        basicUserListDataTable.row.add($(basicTableTr));
                    } catch (e) {
                        console.log(e);
                    }
                }

                rowIndex++;

            });

            try {
                basicUserListDataTable.draw(false);
            } catch (e) {
                console.log(e);
            }

            if (rowIndex >= basicUserListCount) {
                $('.usersBasicListTable_bottom_bar .retrieving_info').html('Retrieved all ' + basicUserListCount + ' users');
                $("#selectAllDiv_Basic").show();
                $("#downloadCSV_Basic").show();
            }
        }

    }


    $('#downloadCSV_Basic').click(function() {

        var btn_text = $(this).val();
        $(this).val('Downloading ...');

        var titles = [];
        var TableData = [];
        var i = 0;

        $('#basicUserListTable th').each(function() {    //table id here
            if( i > 0 ) {
                titles.push($(this).text());
            }
            i++;
        });


        if(basicUserListDataTable) {
            try {
                var data = basicUserListDataTable.rows().data().toArray();
                $.each(data, function (index, row) {
                    //console.log(row);
                    $.each(row, function (c_index, value) {
                        if (c_index !== 0) {
                            //TableData.push(value);
                            if(c_index == 4 && value != ""){
                                 tmpPhoneNumber = " "+value;
                                    TableData.push(tmpPhoneNumber);
                            }else{
                               TableData.push(value);
                            }
                        }
                    });
                });
            } catch (e) {
                console.log(e);
            }
        }

        var CSVString = prepCSVRow(titles, titles.length, '');
        CSVString = prepCSVRow(TableData, titles.length, CSVString);

        var fileName = "<?php echo $_SESSION['groupId']?>_USERS_BASIC" + ".csv";
        //var blob = new Blob(["\ufeff", CSVString]);
        var blob = new Blob([CSVString], { type: 'text/csv;charset=utf-8;' });
        
        if (navigator.msSaveBlob) { // IE 10+
        	navigator.msSaveBlob(blob, fileName);
        }else{
        	var downloadLink = document.createElement("a");
            
            var url = URL.createObjectURL(blob);
            downloadLink.href = url;
            downloadLink.download = fileName;

            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);

            $(this).val(btn_text);
        }

        

    });


    function prepCSVRow(arr, columnCount, initial) {
        var row = '';
        var delimeter = ',';
        var newLine = '\r\n';

        function splitArray(_arr, _count) {
            var splitted = [];
            var result = [];
            _arr.forEach(function(item, idx) {
                if ((idx + 1) % _count === 0) {
                    splitted.push(item);
                    result.push(splitted);
                    splitted = [];
                } else {
                    splitted.push(item);
                }
            });
            return result;
        }
        var plainArr = splitArray(arr, columnCount);

        plainArr.forEach(function(arrItem) {
            arrItem.forEach(function(item, idx) {
                row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
            });
            row += newLine;
        });
        return initial + row;
    }
</script>