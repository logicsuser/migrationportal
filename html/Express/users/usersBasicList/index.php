<?php
require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getAuthenticationPasswordRules.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once ("/var/www/lib/broadsoft/adminPortal/getUserRegisterationCdrReport.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
?>

<style>
table.dataTable tbody td{
    padding: 1px 10px !important;
}
</style>
<style type="text/css">
/* Ensure that the demo table scrolls */
td, td > .userIdVal {  white-space: normal; word-break: break-all; }
div.dataTables_wrapper {
    width: 90%;
    margin: 0 auto;
}
/*   common css */
table.dataTable tbody th, table.dataTable tbody td {text-align: left;}
table.dataTable thead th, table.dataTable thead td{text-align:left !important;}

table.dataTable tbody td{
    padding: 4px 10px;
}

/*table.dataTable.no-footer thead tr .sorting_asc{background-image: url(/Express/images/blue/desc.gif) !important;background-position: 98% 48%;}*/
/*table.dataTable.no-footer thead tr .sorting_desc {
    
    background-image: url(/Express/images/blue/asc.gif);
    background-position: 98% 48%;
} */
table.dataTable thead th, table.dataTable thead td {
    padding: 10px 18px;
    border-bottom: 0px solid #111 !important;
}
table.dataTable thead .sorting {
    background-image: url(/Express/images/blue/bg.gif);
    background-position: 98% 48%;
}
#usersTable1_wrapper table thead tr th {border-top:2px solid #d8dada;border-bottom: 2px solid #d8dada;border-right: 2px solid #d8dada;}
#usersTable1_wrapper table thead tr th:first-child {border-left: 2px solid #d8dada; background-image: none !important;}
.dataTables_wrapper.no-footer .dataTables_scrollBody{border-bottom: none;}
.DTFC_LeftBodyWrapper,.dataTables_scrollBody{margin-top: -20px;}



.col1{min-width:   <?php echo $col1; ?> !important;}
   .col2{min-width:   <?php echo $col2; ?> !important;}
   .col3{min-width:   <?php echo $col3; ?> !important;}
   .col4{min-width:   <?php echo $col4; ?> !important;}
   .col5{min-width:   <?php echo $col5; ?> !important;}
   .col6{min-width:   <?php echo $col6; ?> !important;}
   .col7{min-width:   <?php echo $col7; ?> !important;}
   .col8{min-width:   <?php echo $col8; ?> !important;}
   .col9{min-width:   <?php echo $col9; ?> !important;}
   .col10{min-width:  <?php echo $col10; ?> !important;}
   .col11{min-width:  <?php echo $col11; ?> !important;}
   .col12{min-width:  <?php echo $col12; ?> !important;}
   .col13{min-width:  <?php echo $col13; ?> !important;}
   .col14{min-width:  <?php echo $col14; ?> !important;}
   .col15{min-width:  <?php echo $col15; ?> !important;}
   .col16{min-width:  <?php echo $col16; ?> !important;}
   .col17{min-width:  <?php echo $col17; ?> !important;}
   .col18{min-width:  <?php echo $col18; ?> !important;}
   .col19{min-width:  <?php echo $col19; ?> !important;}
   .col20{min-width:  <?php echo $col20; ?> !important;}
   .col21{min-width:  <?php echo $col21; ?> !important;}
table#usersTable1{margin: 0; white-space: normal !important; word-break: break-all;}
   .DTFC_LeftHeadWrapper table thead tr th:first-child,  .DTFC_LeftHeadWrapper table thead tr .sorting_asc{background-image: none !important;cursor: default;}
    #usersTable1 tbody tr td {border-right: 2px solid #d8dada;}
    .DTFC_LeftBodyWrapper table tbody tr td{border-right: 2px solid #d8dada;vertical-align: middle;}
    .DTFC_LeftBodyLiner{overflow: hidden !important;width:auto !important; } 

    
        .registerUsr td {
            border-top: 2px solid #d8dada !important;
            border-bottom: 2px solid #d8dada !important;
            background-color: #fff !important;
        }     

        .registerUsr td:first-child {
            border-left: 2px solid #d8dada !important;
        }
        
    /*  @media only screen and (max-width: 1600px) and (min-width: 600px){
	table.dataTable tbody td{
		 padding: 2px 10px !important;
	}
     } 
     @media only screen and (max-width: 2500px) and (min-width: 1700px){
	table.dataTable tbody td{
	padding: 2px 10px !important;
	}
     }    */ 
	
  /*Registration Table*/
  .usersTableColor td{background-color:#fff !important;} 
  table.customDataTable{font-size:11px;}
 .basicUserListTable thead tr th {
    border-top: 2px solid #d8dada;
    border-bottom: 2px solid #d8dada;
    border-right: 2px solid #d8dada;
}

.basicUserListTable thead tr th:first-child {
	border-left: 2px solid #d8dada;
}

.basicUserListTable thead tr th:last-child {
	border-right: 0px;
}



table#basicUserListTable tbody tr td{
    /* background-color:#FFF; */ 
    /*border-right: 2px solid #d8dada;*/
    border-top: 2px solid #d8dada !important;
    border-bottom: 2px solid #d8dada !important;
}
table#basicUserListTable tbody tr td:first-child{border-left: 2px solid #d8dada;}


.dataTables_scrollHead, .DTFC_LeftHeadWrapper{z-index: 1;}

table#basicUserListTable tbody tr:hover {
        background-color: #dcfac9 !important;
        cursor: pointer;
}

#basicUserListTable tbody tr {
        background-color: #fff !important;
}

#basicUserListTableBody input[type="checkbox"] {
	zoom: 130%;
}


.usersBasicListTable_top_bar .column1{
    float: left;
    width: 25%;
    height: 100px;
}

.usersBasicListTable_top_bar .column4 .dataTables_length{
    float: right;
    padding-top:5px;
}
.usersBasicListTable_top_bar .column4 .dataTables_length select{
    width: 65px !important;
}
.usersTableButton{
    float: left;
}

input .paginate_jump_input{
            width: 60px !important;
            text-align: center;
        }
.fIcon{
      color:#fff !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button{
            padding: 8px !important; 
           }




/* 100% Width*/
#basicUserListTable_wrapper .dataTables_scrollHeadInner {
	/* width: 100% !important; */
	padding-left: 0 !important;
}
#basicUserListTable_wrapper .dataTables_scrollHeadInner table {
	width: 100% !important;
        border-collapse: collapse;
        padding-right: 21px;
	/*padding-right: 1.5%;*/
}
#basicUserListTable_wrapper .dataTables_scrollBody {
	width: 100% !important;
}
#basicUserListTable_wrapper .dataTables_scrollBody table {
	width: 100% !important;
}

.dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody{
    /* margin-top: -24px; */
}
#basicUserListTable_wrapper .dataTables_scrollHead { 
    background: #5d81ac;
    
}

#basicUserListTable_wrapper .dataTables_scrollBody{
    margin-top: -15px;
    padding-top: 10px; 
}

</style>

<script type="text/javascript">

// This will execute whenever the window is resized
	$(window).resize(function() {
        $(".triggerColumn").trigger("click");
		$("table.dataTable tbody td").css("padding", "2px 10px");
    });

</script>
<script type="text/javascript">

</script>
<?php
$_SESSION["gwUsersMod"] = array (
		"activateCFA" => "Call Forwarding Always Active",
		"phSipFwdCFA" => "Number/SIP-URI to forward calls to",
		"ringReminderChk" => "Enable Ring Reminder play",
		"activateCFNR" => "Call Forwarding Not Reachable Active",
		"phSipFwdCFNR" => "Number/SIP-URI to forward calls to",
		"activateCFS" => "Call Forwarding Selective Active",
		"phSipFwdCFS" => "Number/SIP-URI to forward calls to",
		"ringReminderChkCFS" => "Enable Ring Reminder play",
		"authUserName" => "Authentication User Name",
		"authPassword1" => "Authentication Password",
		"authGeneratedPassword" => "Authentication Password",
		"srvPack" => "Service Pack",
		"assignSvcPack" => "Assign Service Pack",
		"usrServices" => "User Service",
		"assignUsrService" => "Assign User Service",
		"activateNumber" => "Activate/Deactivate Number",
        //gMods VoiceMessage Service
        "voiceMessaging" => "Assign Voice Messaging Service",
        "isActive" => "Voice Messaging/Support",
        "alwaysRedirectToVoiceMail" => "Send All Calls to Voice Mail",
        "busyRedirectToVoiceMail" => "Send Busy Calls to Voice Mail",
        "noAnswerRedirectToVoiceMail" => "Send Unanswered Calls to Voice Mail",
        "serverSelection" => "Third-Party Voice Mail Server",
        "userServer" => "User Specific Mail Server",
        "mailboxIdType" => "Mailbox ID on Third-Party Voice Mail Platform",
        "mailboxURL" => "SIP-URI",
        "noAnswerNumberOfRings" => "Number of rings before greeting",
        "usePhoneMessageWaitingIndicator" => "Use Phone Message Waiting Indicator",
        "sendCarbonCopyVoiceMessage" => "Email Carbon Copy of Message",
        "voiceMessageCarbonCopyEmailAddress" => "Carbon Copy Email Address",
        "transferOnZeroToPhoneNumber" => "Transfer on Zero to Phone Number",
        "transferPhoneNumber" => "Transfer Phone Number",
        "mailBoxLimit"=>"MailBox Limit",
        "callingLineIDBlockingInput" => "Block Calling Line ID"

);

?>

<script>

var chkYes = document.getElementById("voiceMessagingBasicYes_Basic");
var dvPassport = document.getElementById("dvPassportBasic");
if(dvPassport && chkYes){
	dvPassport.style.display = chkYes.checked ? "block" : "none";
}
    var deleteButton_Basic = document.getElementById("deleteUsers_Basic");
    if(deleteButton_Basic){
	    var deleteButton_BasicStyle = deleteButton_Basic.style.display == 'none' ? 'inline-block' : deleteButton_Basic.style.display;
	    deleteButton_Basic.style.display = 'none';
	}
    var expressSheetButton_Basic = document.getElementById("expressSheetUsersBasic");
    if(expressSheetButton_Basic){
	    var expressSheetButton_BasicStyle = expressSheetButton_Basic.style.display == 'none' ? 'inline-block' : deleteButton_Basic.style.display;
	    expressSheetButton_Basic.style.display = 'none';
    }
    var modificationMenu_Basic = document.getElementById("gwModsLabelBasic");
    if(modificationMenu_Basic){
	    var modificationMenu_BasicStyle = modificationMenu_Basic.style.display;
	    modificationMenu_Basic.style.display = 'none';
    }
    var submitCFAButton_Basic = document.getElementById("submitCFABasic");
    if(submitCFAButton_Basic){
    	var submitCFAButton_BasicDisplayStyle = submitCFAButton_Basic.style.display;
    }

    var submitCFNRButton_Basic = document.getElementById("submitCFNRBasic");
    if(submitCFNRButton_Basic){
    var submitCFNRButton_BasicDisplayStyle = submitCFNRButton_Basic.style.display;
    }
    var submitActiveNumberButton_Basic = document.getElementById("submitActiveNumberBasic");
    if(submitActiveNumberButton_Basic){
    var submitActiveNumberButton_BasicDisplayStyle = submitActiveNumberButton_Basic.style.display;
    }
    var submitCFSButton_Basic = document.getElementById("submitCFS_Basic");
    if(submitCFSButton_Basic){
	    var submitCFSButton_BasicDisplayStyle = submitCFSButton_Basic.style.display;
	    var submitCFSButton_BasicColorStyle = submitCFSButton_Basic.style.color;
    }
    var submitServicesButton_Basic = document.getElementById("submitSvcAssignmentBasic");
    if(submitServicesButton_Basic){
    var submitServicesButton_BasicDisplayStyle = submitServicesButton_Basic.style.display;
    submitServicesButton_Basic.style.display = "none";
    }
    var selectedUsers_Basic = {};
    var usersList_Basic = "";
    var selectAllPermission_Basic ="";

    var superUser = "<?php echo $_SESSION["superUser"]; ?>";

    var deleteUserPermission_Basic = "";
    var deleteUsersAllowed_Basic   = false;

	<?php
	       if(isset($_SESSION["permissions"]["deleteUsers"]) && $_SESSION["permissions"]["deleteUsers"]=="1")
	       {
	       	    $delPer = "1";
	       }
	       else
	       {
	       	    $delPer = "0";
	       }
	?>

    deleteUserPermission_Basic = "<?php echo $delPer; ?>";

    if(deleteUserPermission_Basic=="1")
    {
    	var deleteUsersAllowed_Basic = deleteUserPermission_Basic == "1";
    }

    <?php
    if(isset($_SESSION["permissions"]["groupWideUserModify"]) && $_SESSION["permissions"]["groupWideUserModify"]=="1") { $groupModifyPer = "1"; }   else {  $groupModifyPer = "0"; }
 	?>

    var groupWideOperationsAllowed_Basic = <?php echo $groupModifyPer; ?>;
    var serializedArrayData_Basic = null;
    var action_Basic = "";

    var selectedServicePack_Basic = "";
    var selectedUserService_Basic = "";

    var hasAuthenticationPassword_Basic = false;
    var authMinLength_Basic = <?php echo $authenticationPasswordMinLength; ?>;

    // Criteria dialog states: create, modify
    var cfsCriteriaDialogState_Basic = "";

    var numCfsCriteria_Basic = 0;
    var numCfsCheckedCriteria_Basic = 0;






    //var expressSheetUserPermission_Basic = "<?php //echo $_SESSION["permissions"]["expressSheets"]; ?>";
    //var expressSheetUsersAllowed_Basic = expressSheetUserPermission_Basic == "1";


    var expressSheetUserPermission_Basic = "";
    var expressSheetUsersAllowed_Basic   = false;

	<?php
	if(isset($_SESSION["permissions"]["expressSheets"]) && $_SESSION["permissions"]["expressSheets"]=="1") { $expressPer = "1"; }   else {  $expressPer = "0"; }
	?>

	expressSheetUserPermission_Basic = "<?php echo $expressPer; ?>";

    if(expressSheetUserPermission_Basic=="1")
    {
    	var expressSheetUsersAllowed_Basic = expressSheetUserPermission_Basic == "1";
    }


    var globalFiltersAreEnabled = 0;


 // Invoked by onClick event from any checkbox in users rows
    //---------------------------------------------------------
    function onUserSelect_Basic(checkbox) {
        var selectAllCheckBox = document.getElementById("selectAllChk_Basic");
        selectAllCheckBox.checked = false;

        // if User is clicked, create array element with userID as the key
        var userId = checkbox.name;
        if (! selectedUsers_Basic.hasOwnProperty(userId)) {
            selectedUsers_Basic[userId] = false;
        }
        selectedUsers_Basic[userId] = checkbox.checked;

        // determine if there are any row check boxes checked
        var userChecked = false;
        for (var key in selectedUsers_Basic) {
            if (selectedUsers_Basic.hasOwnProperty(key) && selectedUsers_Basic[key]) {
                userChecked = true;
                break;
            }
        }

        // Make sure that Modification Forms are hidden if no checkbox is selected
        // And dropdown Modification Forms selection is not selecting any form
        if (! userChecked) {
            if($("#gwMods_Basic").length) {
                $("#gwMods_Basic option")[0].selected = true;
            }
            disableAllModificationForms_Basic();
        }

        // Show 'Delete Selected Users' button as long as there is one row checkbox checked
        if (deleteUsersAllowed_Basic) {
			if(deleteButton_Basic){
       			 deleteButton_Basic.style.display = userChecked ? deleteButton_BasicStyle : 'none';
			}
        }
        if (expressSheetUsersAllowed_Basic) {
            if(expressSheetButton_Basic){
            	expressSheetButton_Basic.style.display = userChecked ? expressSheetButton_BasicStyle : 'none';
            }
        }
        //downloadCSVButton.style.display = userChecked ? downloadCSVButtonStyle : 'none';
        if (groupWideOperationsAllowed_Basic == "1") {
            // Show/hide group-wide drop-down operations menu
            modificationMenu_Basic.style.display = userChecked ? modificationMenu_BasicStyle : 'none';
        }
        //activeButton.style.display = userChecked ? activeButtonStyle : 'none';
        //deactiveButton.style.display = userChecked ? deactiveButtonStyle : 'none';        
    }

	$(function()
	{

// 			basicUserListTableSorter();
			$("#modsVoiceMessaging_Basic input[name='voiceMessaging']").click(function () {
			if(document.getElementById("dvPassportBasic")){
			 	$('#dvPassportBasic').show();
			}
	        if ($("#voiceMessagingBasicYes_Basic").is(":checked")) {
	            $("#dvPassportBasic").show();
	        } else {
		        $("#voiceMessagingBasicYes_Basic").val();
	            $("#dvPassportBasic").hide();
	        }
	    });


        //-----------------------------------------
		$("#department_Basic").change(function()
		{
			//alert("department basic");
			loadBasicUsersTable = true;
			basicViewUsesList()
		});


        //-----------------------------------------
// 		$("#downloadCSV_Basic").click(function()
// 		{
// 			$("#departmentDropdown_Basic").submit();
// 		});
		//-----------------------------------------
// 		$("#downloadCSVRegistration").click(function()
// 		{
// 			$("#departmentDropdown_BasicRegistration").submit();
// 		});


		//Star Enter button action perform
		$('#phSipFwdCFA_Basic').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) {
				e.preventDefault();
					$('#submitCFABasic').trigger('click');
			  }
			});

		$('#phSipFwdCFNR_Basic').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) {
				e.preventDefault();
					$('#submitCFNRBasic').trigger('click');
			  }
			});
		$('#phSipFwdCFS_Basic').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) {
				e.preventDefault();
					$('#submitCFS_Basic').trigger('click');
			  }
			});

/*voice Management */

$('#submitVoiceManagement_Basic').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) {
				e.preventDefault();
					$('#submitVoiceManagement_Basic').trigger('click');
			  }
			});
/*end Voice Management */

		//End Enter button action perform

        // Invoked by clicking on 'Delete Selected Users' button
        //------------------------------------------------------
        $("#deleteUsers_Basic").click(function()
        {
            usersList_Basic = getSelectedUsers_Basic();
			$("#dialogUsers_Basic").dialog("open");
            $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
            $(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
            $(":button:contains('Cancel')").removeAttr("disabled").removeClass("ui-state-disabled");
            $("#dialogUsers_Basic").html("Are you sure you want to delete selected users?");
			$(":button:contains('Delete')").show();
			$(":button:contains('Cancel')").show();
			$(":button:contains('More Changes')").hide();
        });

        $(".expressSheetUsersBasic").click(function () {
        	activeImageSwapExpress();
        	$("#mainBody").html("<div id=\"loading_Basic\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
            $("#loading_Basic").show();
            $(".navMenu").removeClass("active");
            $("#expressSheets").addClass("active");
            var module = "expressSheets";
            var userType = '';
	        <?php if($_SESSION['superUser'] == 1){?>
            userType = 'superUser';
	        <?php }else{?>
            userType = '<?php echo $_SESSION['adminType']?>';
	        <?php }?>
            usersList_Basic = getSelectedUsers_Basic();
            $.ajax({
                type: "POST",
                url: "expressSheets/index.php",
                data: {module: "userModule", applyFilters: globalFiltersAreEnabled, userList: usersList_Basic, switchToExpress : 1,selectAllchk: selectAllPermission_Basic},
                success: function(result)
                {

                	 $("#loading_Basic").hide();
                    $("#mainBody").html(result);
                    $('#helpUrl').attr('data-module', module);
                    $('#helpUrl').attr('data-adminType', userType);

                   // $('.expressSheetUsersBasic').click(activeImageSwap, activeImageSwap);
                }
            });

        });

        // Invoked by clicking on 'Active Number for Selected Users' button
        //------------------------------------------------------
        $("#numberActivation").click(function()
        {
            usersList_Basic = getSelectedUsers_Basic();
            $("#dialogUsers_Basic").dialog("open");
            $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
            $("#dialogUsers_Basic").html("Are you sure you want to activate number for selected users?");
			$(":button:contains('Delete')").hide();
			$(":button:contains('Activate')").show();
			$(":button:contains('Deactivate')").hide();
			$(":button:contains('Cancel')").show();
			$(":button:contains('More Changes')").hide();
        });

        $(".userNumberAction_Basic input").click(function(){
        	$("#submitActiveNumberBasic").prop("disabled", false);
        });

        // Invoked by clicking on 'Active Number for Selected Users' button
        //------------------------------------------------------
        $("#numberDeactivation").click(function()
        {
            usersList_Basic = getSelectedUsers_Basic();
            $("#dialogUsers_Basic").dialog("open");
            $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
            $("#dialogUsers_Basic").html("Are you sure you want to deactivate number for selected users?");
			$(":button:contains('Delete')").hide();
			$(":button:contains('Activate')").hide();
			$(":button:contains('Deactivate')").show();
			$(":button:contains('Cancel')").show();
			$(":button:contains('More Changes')").hide();
        });

        var checkUserLength = function() {
    		var userCount = $(document).find(".basicUserListTable tbody tr td input[type=checkbox]").length;
    		var userCountChecked = $(document).find(".basicUserListTable tbody tr td input[type=checkbox]").filter(":checked").length;
    		//userCount ? $("#selectAllDiv").show() : $("#selectAllDiv").hide();
    		userCountChecked ? $("#gwModsLabelBasic").show() : $("#gwModsLabelBasic").hide();
    		var checkVal = userCount == userCountChecked ? true: false;
    		$("#selectAllChk_Basic").prop("checked", checkVal);
    		selectAllPermission_Basic = checkVal;
    	}



        // Dialog function after clicking on 'Delete Selected Users' button
        //-----------------------------------------------------------------
        $("#dialogUsers_Basic").dialog({

            autoOpen: false,
            width: 800,
            modal: true,
            position: { my: "top", at: "top" },
            resizable: false,
            closeOnEscape: false,
            buttons: {
                "Delete": function() {
                     //$("#dialogUsers_Basic").html("<div id=\"loading_Basic\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
                     $("#dialogUsers_Basic").html("<div id='loding' style='border: 0px solid red; width: auto;text-align:center'>&nbsp;Please wait user is deleting <br /><br /><img src='/Express/images/ajax-loader.gif'></div>");
                     $(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled");
		     $(":button:contains('Cancel')").attr("disabled", "disabled").addClass("ui-state-disabled");
                     /*beforeSend: function() {
                                $("#dialogUsers_Basic").html("<div>Please wait user is deleting <br /><br /><img src='/Express/images/ajax-loader.gif'></div>");
                        }
                        async: false,*/
                        
                     //console.log('Test');
                     //console.log(usersList_Basic);
                     $.ajax({
                        type: "POST",
                        url: "users/deleteUsers.php",
                        data: {action: "deleteUsers", userList: usersList_Basic},
                        success: function(result) {
                            $("#dialogUsers_Basic").dialog("option", "title", "Request Complete");
                            $(".ui-dialog-titlebar-close", this.parentNode).hide();
                            $(".ui-dialog-buttonpane", this.parentNode).show();
                            $("#loding").hide();
                            $(":button:contains('Delete')").hide();
                            $(":button:contains('Cancel')").hide();
                            $(":button:contains('More Changes')").show();
                            $("#dialogUsers_Basic").html(result);
                            $("#dialogUsers_Basic").append("User deleted successfully.");
                            $("#dialogUsers_Basic").append(returnLink);
                            checkUserLength();
                        }
                    });
                },

                "Cancel": function() {
                    $(this).dialog("close");
                },
				"More Changes": function()
				{
					$(this).dialog("close");
					$("html, body").animate({ scrollTop: 0 }, 600);
					$("#mainBody").html("<div id=\"loading_Basic\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
					$("#loading_Basic").show();
					$(".navMenu").removeClass("active");
					$(this).addClass("active");
					$.ajax({
						type: "POST",
						url: "navigate.php",
						data: { module: 'users' },
						success: function(result)
						{
							$("#loading_Basic").hide();
							$("#mainBody").html(result);
							//apply_active_filters();
							//checkUserLength();
						}
					});
				}
            },
			open: function() {
				setDialogDayNightMode($(this));
				$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
				$('.ui-dialog-buttonpane').find('button:contains("Delete")').addClass('deleteButton');			 
				$('.ui-dialog-buttonpane').find('button:contains("More Changes")').addClass('moreChangesButton');			 
            }
        });


        // Invoked by clicking on 'Submit' button on the CFA form
        //-------------------------------------------------------
        $("#submitCFABasic").click(function() {
            action_Basic = "modifyCFA";
            usersList_Basic = getSelectedUsers_Basic();
            serializedArrayData_Basic = $("form#CFA_Basic").serializeArray();
            $.ajax({
                type: "POST",
                url: "users/checkData.php",
                data: serializedArrayData_Basic,
                success: function(result) {
                    processGroupWideModificationCheckDataResult_Basic(result);
                }
            });
        });
//modify VoiceMessaging service

        $("#submitVoiceManagement_Basic").click(function() {
        	action_Basic ="modifyVoiceMessaging";
            usersList_Basic = getSelectedUsers_Basic();
            serializedArrayData_Basic = $("form#modsVoiceMessaging_Basic").serializeArray();
            $.ajax({
                type: "POST",
                url: "users/checkData.php",
                data: serializedArrayData_Basic,
                success: function(result) {
                   processGroupWideModificationCheckDataResult_Basic(result);
                }
            });
        });


        // Invoked by clicking on 'Submit' button on the CFNR form
        //--------------------------------------------------------
        $("#submitCFNRBasic").click(function() {
            action_Basic = "modifyCFNR";
            usersList_Basic = getSelectedUsers_Basic();
            serializedArrayData_Basic = $("form#CFNR_Basic").serializeArray();
            $.ajax({
                type: "POST",
                url: "users/checkData.php",
                data: serializedArrayData_Basic,
                success: function(result) {
                    processGroupWideModificationCheckDataResult_Basic(result);
                }
            });
        });


        // Invoked by clicking on 'Submit' button on the CFS form
        //-------------------------------------------------------
        $("#submitCFS_Basic").click(function() {
            action_Basic = "modifyCFS";
            usersList_Basic = getSelectedUsers_Basic();
            serializedArrayData_Basic = $("form#CFS_Basic").serializeArray();
            $.ajax({
                type: "POST",
                url: "users/checkData.php",
                data: serializedArrayData_Basic,
                success: function(result) {
                    processGroupWideModificationCheckDataResult_Basic(result);
                }
            });
        });

        // Invoked by clicking on 'Submit' button on the CFS form
        //-------------------------------------------------------
        $("#submitActiveNumberBasic").click(function() {
            var optVal = $("#numberActivationFrm_Basic").find("input[name='activateNumber']:checked").val();
            if(optVal == "activate"){
            	action_Basic = "numberActivate";
            }else if(optVal == "deactivate"){
            	action_Basic = "numberDeactivate";
            }
            usersList_Basic = getSelectedUsers_Basic();
            serializedArrayData_Basic = $("form#numberActivationFrm_Basic").serializeArray();
            $.ajax({
                type: "POST",
                url: "users/checkData.php",
                data: serializedArrayData_Basic,
                success: function(result) {
                    processGroupWideModificationCheckDataResult_Basic(result);
                }
            });
        });


        // Invoked by clicking on 'Submit' button on the 'Reset Authentication' form
        // -------------------------------------------------------------------------
        $("#submitAuthentication_Basic").click(function() {
           action_Basic = "resetAuthentication";
            usersList_Basic = getSelectedUsers_Basic();
            serializedArrayData_Basic = $("form#authPasswords_Basic").serializeArray();
            $.ajax({
                type: "POST",
                url: "users/checkData.php",
                data: serializedArrayData_Basic,
                success: function(result) {
                    processGroupWideModificationCheckDataResult_Basic(result);
                }
            });
        });


        // Invoked by clicking on 'Submit' button on the Services Management form
        //-----------------------------------------------------------------------
        $("#submitSvcAssignmentBasic").click(function() {
            action_Basic = "modifyServices";
            usersList_Basic = getSelectedUsers_Basic();
            serializedArrayData_Basic = $("form#SwPck_Basic").serializeArray();
            $.ajax({
                type: "POST",
                url: "users/checkData.php",
                data: serializedArrayData_Basic,
                success: function(result) {
                    processGroupWideModificationCheckDataResult_Basic(result);
                }
            });
        });


        $(".callingLineIDBlockingFrm_Basic input").click(function(){
        	$("#callingLineIDBlockingBasic").prop("disabled", false);
        });

 		$("#callingLineIDBlockingBasic").click(function() {
 			action_Basic = "modifyCallingLineIDBlocking";
            usersList_Basic = getSelectedUsers_Basic();
            serializedArrayData_Basic = $("form#callingLineIDBlockingFrm_Basic").serializeArray();
            $.ajax({
                type: "POST",
                url: "users/checkData.php",
                data: serializedArrayData_Basic,
                success: function(result) {
                    processGroupWideModificationCheckDataResult_Basic(result);
                }
            });
        });
        
        // Dialog function after clicking on hidden group-wide user action_Basics
        //-----------------------------------------------------------------
        $("#dialogGroupWideModify_Basic").dialog({
            autoOpen: false,
            width: 800,
            modal: true,
            position: { my: "top", at: "top" },
            resizable: false,
            closeOnEscape: false,
            buttons: {
                "Complete": function() {
                	pendingProcess.push("Users group wide modify");
                    var isVMAction = action_Basic == "modifyVoiceMessaging";
                	//serializedArrayData_Basic = $("form#modsVoiceMessaging_Basic").serializeArray();
                    $.ajax({
                        type: "POST",
                        url: "users/modifyUsers.php",
                        data: {action: action_Basic, userList: usersList_Basic,formData:serializedArrayData_Basic},
                        async: false,
                        beforeSend: function(){                                
                                $("html, body").animate({ scrollTop: 0 }, 0); //Code added @07 Feb 2019                                
                        },
                        success: function(result) {
                        	if(foundServerConErrorOnProcess(result, "Users group wide modify")) {
            					return false;
                          	}
                            $("html, body").animate({ scrollTop: 0 }, 0); //Code added @07 Feb 2019
                            $("#dialogGroupWideModify_Basic").dialog("option", "title", "Request Complete");
                            $(".ui-dialog-titlebar-close", this.parentNode).hide();
                            $(".ui-dialog-buttonpane", this.parentNode).show();
							$(":button:contains('Complete')").hide();
							$(":button:contains('Cancel')").hide();
							$(":button:contains('More Changes')").show();
							$(":button:contains('Return To Main')").show();
                            $("#dialogGroupWideModify_Basic").html(result);
                            $("#dialogGroupWideModify_Basic").append(returnLink);
                            if(isVMAction){
                            	vmProcessGroupWideModificationCheckDataResult_Basic(result);
                            }

                        }
                    });
                },

                "Cancel": function() {
                    $(this).dialog("close");
                },
				"More Changes": function()
				{
					$(this).dialog("close");
					$("html, body").animate({ scrollTop: 0 }, 600);
					$("#mainBody").html("<div id=\"loading_Basic\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
					$("#loading_Basic").show();
					$(".navMenu").removeClass("active");
					$(this).addClass("active");
					$.ajax({
						type: "POST",
						url: "navigate.php",
						data: { module: 'users' },
						success: function(result)
						{
							$("#loading_Basic").hide();
							$("#mainBody").html(result);
						}
					});
				}
                /* code coment for duplicate return to main   */
                /*,
				"Return To Main": function(){
					window.location.href = "main.php";
				} */
            },
			open: function() {
				setDialogDayNightMode($(this));				
                $('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('completeButton');
				$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');                
				$('.ui-dialog-buttonpane').find('button:contains("More Changes")').addClass('moreChangesButton');                
            }
        });


        // Invoked by clicking on 'Select All' checkbox sks
        //---------------------------------------------


    $(".checkUserListBox_Basic").click(function() {
    	var el = $(this);
		var elChked = el.is(":checked");
		var cName = el.attr("name");
		var cIds = el.attr("id");
		var cId = cIds.replace(/[\-\[\]\/\{\}\(\)\*\@\?\.\\\^\$\|]/g, "\\$&");

		$(".basicUserListTable ").find("#"+cId).prop("checked", elChked);
		$(".basicUserListTable ").find("#"+cId).prop("disabled", false);

		//$("#"+this.id).prop("checked", $(this).is(":checked"));
	 	//$("#"+this.id).prop("disabled", false);

	 	checkUserLength();
	 	printSelectedUser();
  	});


        // New CFS Criteria Dialog
        // This dialog with CFS Criteria form is opened to create new CFS Criteria
        // or to update or delete the existing CFS criteria.
        // -----------------------------------------------------------------------
        $("#newCriteriaCFSDialog_Basic").dialog({
            autoOpen: false,
            width: 1000,
            modal: true,
            title: "Selective Call Forwarding Criteria",
            position: { my: "top", at: "top" },
            resizable: false,
            closeOnEscape: false,
            buttons: {
                Create: function() {
                    var formData = $("form#newCFSCriteriaForm_Basic").serialize();
                    var settings = {
                        type: "POST",
                        url: "users/usersBasicList/CFSCriteria_Basic.php",
                        data: formData,
                        async: false
                    };
                    $.ajax(settings).done(function(result) {
                        result = result.trim();
                        if (result.slice(0, 1) == "1") {
                            alert( "Error: " + result.slice(1));
                        } else {
                            $("#newCriteriaCFSDialog_Basic").dialog("close");
                            $("#cfsCriteriaTable_Basic").html(result.slice(1));
                            numCfsCriteria_Basic = getCfsCriteriaCount_Basic(result.slice(1));
                            numCfsCheckedCriteria_Basic = getActiveCfsCriteriaCount_Basic();
                            updateCFSSubmitButtonStatus_Basic();
                        }
                    });
                },
                Update: function() {
                    var formData = $("form#newCFSCriteriaForm_Basic").serialize();
                    var settings = {
                        type: "POST",
                        url: "users/usersBasicList/CFSCriteria_Basic.php",
                        data: formData,
                        async: false
                    };
                    $.ajax(settings).done(function(result) {
                        result = result.trim();
                        if (result.slice(0, 1) == "1") {
                            alert( "Error: " + result.slice(1));
                        } else {
                            $("#newCriteriaCFSDialog_Basic").dialog("close");
                            $("#cfsCriteriaTable_Basic").html(result.slice(1));
                        }
                    });
                },
                Delete: function() {
                    var settings = {
                        type: "POST",
                        url: "users/usersBasicList/CFSCriteria_Basic.php",
                        data:  { deleteCriteria: $("#cfsCriteriaName_Basic").val() },
                        async: false
                    };
                    $.ajax(settings).done(function(result) {
                        result = result.trim();
                        if (result.slice(0, 1) == "1") {
                            alert( "Error: " + result.slice(1));
                        } else {
                            $("#newCriteriaCFSDialog_Basic").dialog("close");
                            $("#cfsCriteriaTable_Basic").html(result.slice(1));
                            numCfsCriteria_Basic = getCfsCriteriaCount_Basic(result.slice(1));
                            numCfsCheckedCriteria_Basic = getActiveCfsCriteriaCount_Basic();
                            updateCFSSubmitButtonStatus_Basic();
                        }
                    });
                },
                Close: function() {
                    $(this).dialog("close");
                }
            },
            open: function() {
				setDialogDayNightMode($(this));
				$('.ui-dialog-buttonpane').find('button:contains("Create")').addClass('createButton');
				$('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('cancelButton');
				$('.ui-dialog-buttonpane').find('button:contains("Delete")').addClass('deleteButton');
				$('.ui-dialog-buttonpane').find('button:contains("Update")').addClass('subButton');
				
                $(".ui-dialog-buttonpane button:contains('Create')").button('disable');
                if (cfsCriteriaDialogState_Basic == "create") {
                    $(".ui-dialog-buttonpane button:contains('Create')").button().show();
                    $(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
                    $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
                }
                else if (cfsCriteriaDialogState_Basic == "modify") {
                    $(".ui-dialog-buttonpane button:contains('Create')").button().hide();
                    $(".ui-dialog-buttonpane button:contains('Delete')").button().show();
                    $(".ui-dialog-buttonpane button:contains('Update')").button().show();
                }
            }
        });

        $(".ui-button-text").css({"font-size": +12+"px"});


        // Invoked on clicking 'Add' button on Call Forwarding Selective page
        // to create new Call Forwarding Selective criteria
        // ------------------------------------------------------------------
        $("#newCriteriaCFS_Basic").click(function(){
            cfsCriteriaDialogState_Basic = "create";
            $.ajax({
                type: "POST",
                url: "users/usersBasicList/newCFSCriteria_Basic.php",
                success: function(result) {
                    $("#newCriteriaCFSDialog_Basic").dialog("open");
                    $(".ui-dialog").find("ui-widget-header").css("background", "darkgreen");
                    $("#newCriteriaCFSDialog_Basic").html(result);
                }
            });
        });


        //-----------------------------------------
//         $("#allUsers").tablesorter();
				//$("#basicUserListTable").tablesorter();

				/* comment
				$(".viewDetailRegistration").hide();

				$('#viewUserTable').click(function() {
							$(".viewDetail1").show();
                            $("#userFiltersForm").show();
							$(".viewDetailRegistration").hide();
					  		$("#departmentDropdown_Basic").attr('action', 'users/printCSV.php');
							$("#departmentDropdown_Basic").css("visibility", "visible");
							$("#selectAllChk").parent('label').parent('div').show();
							if ($('input.selectAl_Chk').is(':checked') || $('input.checkUserListBox_Basic').is(':checked')) {

								if(deleteUserPermission_Basic == "1") {
									$(".deleteUsers_Basic").show();
								}
								if(expressSheetUserPermission_Basic == "1") {
									$(".expressSheetUsersBasic").show();
								}
							}

			 });


				$('#viewUserRegistrationTable').click(function() {
							$(".viewDetail1").hide();
                            $("#userFiltersForm").hide();
							$(".deleteUsers_Basic").hide();
							$(".expressSheetUsersBasic").hide();

							//$("#divActivationNumber_Basic").hide();
                            $(".viewDetailRegistration").show();
                            $("#gwModsLabelBasic").hide();
							$("#departmentDropdown_Basic").attr('action', 'users/printCSVRegistration.php');

							$("#departmentDropdown_Basic").css("visibility", "hidden");
							$("#selectAllChk").parent('label').parent('div').hide();
							$(".checkUserListBox_Basic").attr("checked", false);
							$("#selectAllChk").attr("checked", false);
							$("#gwMods").val('gwModsNone');
							$("#divCFA_Basic").hide();
							$("#divCFNR_Basic").hide();
							$("#divCFS_Basic").hide();
							$("#divAuth_Basic").hide();
							$("#divSwPck_Basic").hide();
							$("#divActivationNumber_Basic").hide();
							$("#groupWideModifyUser_Basic").hide();

                            basicUserListTableSorter();
				 });
				*/

				//function to make the selected user download in csv
				var printSelectedUser = function(){
					$("#selectedUserForDownload").val('');
					var checkedUsers = "";
					$(".DTFC_LeftBodyLiner .checkUserListBox_Basic").each(function(){
						var el = $(this);
						var userId = el.attr("id");
						if(el.is(":checked")){
							checkedUsers += userId + ";";
						}
					});
					$("#selectedUserForDownload").val(checkedUsers);
				};
	});

	//function to make the selected user download in csv
	var printSelectedUser = function(){
	    $("#selectedUserForDownload").val('');
	    var checkedUsers = "";
	    $(".DTFC_LeftBodyLiner .checkUserListBox_Basic").each(function(){
	        var el = $(this);
	        var userId = el.attr("id");
	        if(el.is(":checked")){
	            checkedUsers += userId + ";";
	        }
	    });
	    $("#selectedUserForDownload").val(checkedUsers);
	};

    function selectAllBasicUsers(checkBoxObj) {

        var selectAllCheck = $(checkBoxObj).is(":checked");
        toggleAllBasicUsers(selectAllCheck);

    }

	function toggleAllBasicUsers(selectAllCheck) {

        selectAllCheck = selectAllCheck || false;

        selectAllPermission_Basic = selectAllCheck;
        selectedUsers_Basic = {};

        if(typeof basicUserListDataTable !== 'undefined' && basicUserListDataTable) {
            var allCheckboxs = $('input', basicUserListDataTable.cells().nodes());
            allCheckboxs.each(function() {

                var el = $(this);
                var cName = el.attr("name");
                selectedUsers_Basic[cName] = selectAllCheck;

            });

            allCheckboxs.prop('checked',selectAllCheck);
        }

        // Make sure that Modification Forms are hidden if no checkbox is selected
        // And dropdown Modification Forms selection is not selecting any form
        if (! selectAllCheck) {
            if($("#gwMods_Basic").length) {
                $("#gwMods_Basic option")[0].selected = true;
            }
            disableAllModificationForms_Basic();
        }

        if (deleteUsersAllowed_Basic) {
            // Show/hide 'Delete Selected Users' button
            if(deleteButton_Basic){
                deleteButton_Basic.style.display = selectAllCheck ? deleteButton_BasicStyle : 'none';
            }
            //activeButton.style.display = selectAllCheck ? activeButtonStyle : 'none';
            //deactiveButton.style.display = selectAllCheck ? deactiveButtonStyle : 'none';
        }

        if (expressSheetUsersAllowed_Basic) {
            // Show/hide 'Generate Express Sheet Selected Users' button
            if(expressSheetButton_Basic){
                expressSheetButton_Basic.style.display = selectAllCheck ? expressSheetButton_BasicStyle : 'none';
            }
        }

        if (groupWideOperationsAllowed_Basic == "1") {
            // Show/hide group-wide drop-down operations menu
            modificationMenu_Basic.style.display = selectAllCheck ? modificationMenu_BasicStyle : 'none';

        }
        printSelectedUser();

	}

    // get number of CFS criteria based on displayed criteria rows
    // -----------------------------------------------------------
    function getCfsCriteriaCount_Basic(str) {
        var arr = str.split("<tr id");
        var c = arr.length > 0 ? arr.length -1 : 0;
        return arr.length > 0 ? arr.length -1 : 0;
    }


    // count number of active CFS criteria
    // -----------------------------------
    function getActiveCfsCriteriaCount_Basic() {
        if (numCfsCriteria_Basic == 0) {
            return 0;
        }

        var numCheckedCriteria = 0;
        var currentCriteriaCount = 0;

        while (currentCriteriaCount < numCfsCriteria_Basic) {
            var id = "cfsCritChk_Basic" + currentCriteriaCount;
            var element = document.getElementById(id);

            if (element.type && element.type === "checkbox") {
                if (element.checked) {
                    numCheckedCriteria++;
                }
            }

            currentCriteriaCount++;
        }

        return numCheckedCriteria;
    }


    // collect active status of all CFS criteria
    function getCFSCriteriaActiveStatus_Basic() {
        var result = [];
        var index = 0;

        while (index < numCfsCriteria_Basic) {
            var id = "cfsCritChk_Basic" + index;
            var element = document.getElementById(id);

            if (element.type && element.type === "checkbox") {
                result[index] = element.checked ? "true" : "false";
            }

            index++;
        }
        return result;
    }


    // Process results processed by checkData.php module that is generating
    // verification data for group-wide modification processes
    // ---------------------------------------------------------------------

    function vmProcessGroupWideModificationCheckDataResult_Basic(result)
    {
        var result = JSON.parse(result);
        var unAssignErrorTable = "";
		var assingnErrorTable = "";
		var confErrorTable = "";

		var module = "";

// 		if(value.assignedServicePack) { module = "Service Assignment and Configuration Successfull.";}
// 		else if(value.unAssignedServicePack) { module = "Service UnAssigned Successfull";}

    	//if(result.length > 0){
    	var backgroundColor = 'background:#D52B1E; width:50%';

     		for(var i=0; i< result.length; i++)
         	{
				var value = result[i];
				var userId = result[i].userId;
				if(value.assignedServicePack && value.assignedServicePack.status != "Error") { module = "Service Assignment and Configuration Successfull.";}
		 		else if(value.unAssignedServicePack && value.unAssignedServicePack.status != "Error") { module = "Service UnAssigned Successfull.";}

    			if(value.assignedServicePack && value.assignedServicePack.status == "Error") {
        			var errorMessage = value.assignedServicePack.errorMsg;
					assingnErrorTable += '<tr><td class="errorTableRows" style="'+ backgroundColor+'">' + userId + '</td><td class="errorTableRows">' + errorMessage +' </td></tr>';
    			}
    			if(value.configuration && value.configuration.status == "Error") {
    				var errorMessage = value.configuration.errorMsg;
					confErrorTable += '<tr><td class="errorTableRows" style="'+ backgroundColor+'">' + userId + '</td><td class="errorTableRows">' + errorMessage +' </td></tr>';
	    			}
    			if(value.unAssignedServicePack && value.unAssignedServicePack.status == "Error") {
    				var errorMessage = value.unAssignedServicePack.errorMsg;
					unAssignErrorTable += '<tr><td class="errorTableRows" style="'+ backgroundColor+'">' + userId + '</td><td class="errorTableRows">' + errorMessage +' </td></tr>';
	    		}

 	 	    }

    	$("#dialogGroupWideModify_Basic").html("");

    	if(assingnErrorTable != ""){
    		$("#dialogGroupWideModify_Basic").append('<table><tr><td colspan="2" style="width:100%"> Error in VM Assign Service</td></tr>');
    		$("#dialogGroupWideModify_Basic").append(assingnErrorTable);
    		$("#dialogGroupWideModify_Basic").append('</table> </br>');
        }
    	else if(unAssignErrorTable != "")
        {
    		$("#dialogGroupWideModify_Basic").append('<table><tr><td colspan="2"> Error in VM UnAssign Service For Following Users </td></tr>');
    		$("#dialogGroupWideModify_Basic").append(unAssignErrorTable);
    		$("#dialogGroupWideModify_Basic").append('</table> </br>');
        }
    	else if(confErrorTable != "") {
    		$("#dialogGroupWideModify_Basic").append('<table><tr><td colspan="2"> Error in Service Configuration  </td></tr>');
    		$("#dialogGroupWideModify_Basic").append(confErrorTable);
    		$("#dialogGroupWideModify_Basic").append('</table> </br>');
        }

		$("#dialogGroupWideModify_Basic").append(module);

		$("#dialogGroupWideModify_Basic").dialog("open");
		$(":button:contains('Complete')").hide();
		$(":button:contains('Cancel')").hide();
		$(":button:contains('More Changes')").show();
    }


    function processGroupWideModificationCheckDataResult_Basic(result)
    {
        
        $("html, body").animate({ scrollTop: 0 }, 0); //Code added @07 Feb 2019
    	$("#dialogGroupWideModify_Basic").dialog("open");
        result = result.trim();
        if (result.slice(0, 1) == 1)
        {
            $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
        }
        else
        {
            $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
        }
        $("#dialogGroupWideModify_Basic").html(result.slice(1));
		$(":button:contains('Complete')").show();
		$(":button:contains('Cancel')").show();
		$(":button:contains('More Changes')").hide();
		$(":button:contains('Return To Main')").hide();
    }


    // Invoked by onChange event from a group-wide user modification drop-down menu
    //-----------------------------------------------------------------------------
    function onMenuSelectionChange_Basic(select) {
    	$("#groupWideModifyUser_Basic").hide();
        var optVal = select.options[select.selectedIndex].value;

        if (optVal == "gwModsNone") {
            disableAllModificationForms_Basic();
        }
        else if (optVal == "gwModsCFA") {
            $("div[id^=div]").hide();
            $("#divCFA_Basic").show();
            $("#phSipFwdCFA_Basic").val("");
            $("#ringReminderChk_Basic").prop("checked", false);
        }
        else if (optVal == "gwModsCFNR") {
            $("div[id^=div]").hide();
            $("#divCFNR_Basic").show();
            $("#phSipFwdCFNR_Basic").val("");
        }
        else if (optVal == "gwModsCFS") {
            // Remove any stored CFS Criteria data
            $.ajax({
                type: "POST",
                url: "users/usersBasicList/CFSCriteria_Basic.php",
                data: {deleteAllCriteria: "yes"},
                async: false
            });
            $("#cfsCriteriaTable_Basic").html("--No Criteria--");

            $("div[id^=div]").hide();
            $("#divCFS_Basic").show();
            $("#phSipFwdCFS_Basic").val("");
            $("#ringReminderChkCFS_Basic").prop("checked", false);
        }
        else if (optVal == "gwModsAuth") {
            $("div[id^=div]").hide();
            $("#divAuth_Basic").show();
            $("#authPassword1_Basic").val("");
            $("#authPassword2_Basic").val("");
            $("#genAuthPassword_Basic").prop("checked", false);
            $("#authPassword1CheckMark_Basic").hide();
            $("#authPassword2CheckMark_Basic").hide();
            $("#authGeneratedPassword_Basic").val("");
            $("#genAuthPassword_Basic").prop("checked", false);
        }
        else if (optVal == "gwModsSwPck") {
            $("div[id^=div]").hide();
            $("#divSwPck_Basic").show();
            $("#srvPack_Basic").val("srvPackNone");
            $("#assignSvcPackFalse_Basic").prop("checked", true);
            $("#usrServices_Basic").val("usrServiceNone");
            $("#assignUsrServiceFalse_Basic").prop("checked", true);

        }else if(optVal == "gwModsDnActivate"){
        	$("div[id^=div]").hide();
        	$("#divActivationNumber_Basic").show();
        	$("#activateNumber_Basic").prop("checked", true);
        }else if(optVal =="gwModsVM"){
        	$("div[id^=div]").hide();
        	$("#groupWideModifyUser_Basic").show();
        }  else if(optVal =="callingLineIDBlocking") {
        	$("div[id^=div]").hide();
        	$("#callingLineIDBlocking_Basic").show();
        	$("#blockCalling_Basic").prop("checked", true);        	
         }
    }


    // Invoked by onChange event from Service Pack selection dropdown menu
    // that allows selecting service pack authenticated to a user
    // -------------------------------------------------------------------
    function onServicePackSelectionChange_Basic(select) {
        var optVal = select.options[select.selectedIndex].value;

        selectedServicePack_Basic = optVal == "srvPackNone" ? "" : optVal;
        submitServicesButton_Basic.style.display = (selectedServicePack_Basic != "" || selectedUserService_Basic != "") ?
            submitServicesButton_BasicDisplayStyle : "none";
    }


    // Invoked by onChange event from User Services selection dropdown menu
    // that allows selecting user service authenticated to a user
    // --------------------------------------------------------------------
    function onUserServiceSelectionChange_Basic(select) {
        var optVal = select.options[select.selectedIndex].value;

        selectedUserService_Basic = optVal == "usrServiceNone" ? "" : optVal;
        submitServicesButton_Basic.style.display = (selectedServicePack_Basic != "" || selectedUserService_Basic != "") ?
            submitServicesButton_BasicDisplayStyle : "none";
    }


    // Evaluate automatically generated password
    // Automatically generated password can be manually modified,
    // so it has to pass minimum password length test
    // ----------------------------------------------------------
    function evalAuthGeneratedPassword_Basic(textBox) {
        var length = textBox.value.length;
        if (textBox.value.length >= authMinLength_Basic) {
            $("#submitAuthentication_Basic").show();
        } else  {
            $("#submitAuthentication_Basic").hide();
        }
    }

    // Invoked by onChange event from 'Generate Password' checkbox
    // to generateAuthentication Password on 'Reset Authentication form
    // ----------------------------------------------------------------
    function onGeneratePasswordSelect_Basic(checkbox) {
        var authPassword = "<?php echo setAuthPassword(); ?>";
        document.getElementById("authGeneratedPassword_Basic").value = authPassword;

        if (checkbox.checked) {
            $("#typedPassword_Basic").hide();
            $("#generatedPassword_Basic").show();
            $("#passwordRetype_Basic").hide();

            document.getElementById("authPassword1_Basic").value = "";
            document.getElementById("authPassword2_Basic").value = "";
            $("#authPassword1CheckMark_Basic").hide();
            $("#authPassword2CheckMark_Basic").hide();

            hasAuthenticationPassword_Basic = true;
            $("#submitAuthentication_Basic").show();
        } else {
            $("#generatedPassword_Basic").hide();
            $("#typedPassword_Basic").show();
            $("#passwordRetype_Basic").show();

            document.getElementById("authGeneratedPassword_Basic").value = "";

            hasAuthenticationPassword_Basic = false;
            $("#submitAuthentication_Basic").hide();
        }
    }


    // Evaluate the content of phone number entry box in Call Forwarding forms
    // and hide/show submit button on the form based on the content of phone number box
    // and Enable/Disable radio status of a Call Forwarding service
    //---------------------------------------------------------------------------------
    function evalFwdNum_Basic(txtBox) {

        // Determine which submit button needs to be controlled
        var service = "";
        if (txtBox.id.indexOf("CFA_Basic") != -1) {
            service = "CFA";
        } else if (txtBox.id.indexOf("CFNR_Basic") != -1) {
            service = "CFNA";
        }else {
            return;
        }



        var submitButton = submitCFNRButton_Basic;
        var submitDisplayStyle = submitCFNRButton_BasicDisplayStyle;
        var activateRadioId = (service == "CFA") ? "activateCFATrue_Basic" : "activateCFNRTrue_Basic";
       // var activateCheckBoxIdVEA = (service == "VEA") ? "sendCarbonCopyVoiceMessage" : "";
       var activateService = document.getElementById(activateRadioId).checked;
       // var activateService = document.getElementById(activateCheckBoxIdVEA).checked;
        //var activateService = document.getElementById(activateCheckBoxIdDN).checked;

        if (service == "CFA") {
            submitButton = submitCFAButton_Basic;
            submitDisplayStyle = submitCFAButton_BasicDisplayStyle;
        }

        var hasText = txtBox.value.length > 0;
        submitButton.style.display = (activateService && ! hasText) ?  "none" : submitDisplayStyle;
    }


    // Evaluate Authentication Passwords: Password1 and Password2 fields
    // Must comply with authentication password minimum length
    // -----------------------------------------------------------------
    function evalAuthPassword_Basic(txtBox) {
        var pass1 = $("#authPassword1_Basic").val();
        var pass2 = $("#authPassword2_Basic").val();
        if (pass1.length >= authMinLength_Basic) {
            $("#authPassword1CheckMark_Basic").show();

            if (pass2 === pass1) {
                $("#authPassword2CheckMark_Basic").show();
                hasAuthenticationPassword_Basic = true;
                $("#submitAuthentication_Basic").show();
            }
            else {
                $("#authPassword2CheckMark_Basic").hide();
                hasAuthenticationPassword_Basic = false;

                $("#submitAuthentication_Basic").hide();
            }
        }
        else { // password 1 length less than minimum length of 3
            $("#authPassword1CheckMark_Basic").hide();
        }
    }


    // Evaluate Enable/Disable status of a Call Forwarding service on Call Forwarding forms
    // and hide/show submit button on the form based on Enable/Disable radio status
    // and the content of forwarding phone number on the Call Forwarding forms
    //-------------------------------------------------------------------------------------
    function evalActivation_Basic(radio) {
        var service = "";
        if (radio.id.indexOf("CFA") != -1) {
            service = "CFA";
        } else if (radio.id.indexOf("CFNR_Basic") != -1) {
            service = "CFNR";
        } else {
            return;
        }
        var chkYes = document.getElementById("voiceMessagingBasicYes_Basic");
        var dvPassport = document.getElementById("dvPassportBasic");
        if(dvPassport && chkYes){
        	dvPassport.style.display = chkYes.checked ? "block" : "none";
        }
        var currActivateServiceId = radio.name + "True";

        var submitButton = submitCFNRButton_Basic;
        var submitDisplayStyle = submitCFNRButton_BasicDisplayStyle;
        var fwdNumTextBoxId = (service == "CFA") ? "phSipFwdCFA_Basic" : "phSipFwdCFNR_Basic";
        var activateService = document.getElementById(currActivateServiceId).checked;

        if (service == "CFA") {
            submitButton = submitCFAButton_Basic;
            submitDisplayStyle = submitCFAButton_BasicDisplayStyle;
        }

        var hasText = document.getElementById(fwdNumTextBoxId).value.length > 0;
        submitButton.style.display = (activateService && ! hasText) ?  "none" : submitDisplayStyle;
    }


    // This function controls enable/disable status of 'Submit' button for group-wide CFS modification
    // IF service activation is OFF THEN 'Submit' button is enabled
    // IF service activation is ON THEN enable status of 'Submit' button depends on status of
    // default call forwarding number and CFS criteria existence and criteria activation status
    // ----------------------------------------------------------------------------------------------
    function updateCFSSubmitButtonStatus_Basic() {
        var buttonEnabled = document.getElementById("activateCFSFalse_Basic").checked;
        if (! buttonEnabled) {
            buttonEnabled = document.getElementById("phSipFwdCFS_Basic").value.length > 0;
            if (buttonEnabled) {
                buttonEnabled = numCfsCheckedCriteria_Basic > 0;
            }
        }

        document.getElementById("submitCFS_Basic").disabled = ! buttonEnabled;
        // some browsers, eg Chrome do not gray out disabled controls
        submitCFSButton_Basic.style.color = buttonEnabled ? submitCFSButton_BasicColorStyle : "gray";
    }


    // ------------
    // CFS Handlers
    // ------------

    // Invoked by onChange event from service activation radio buttons of Call Forwarding Selective
    // to update disable status of CFS service submit button
    // --------------------------------------------------------------------------------------------
    function evalActivationCFS_Basic(radio) {
        //var buttonEnabled = radio.id == "activateCFSFalse_Basic";
        updateCFSSubmitButtonStatus_Basic();
    }


    // Invoked by onChange and onInput events from forwarding number text box of Call Forwarding Selective
    // to update disable status of CFS service submit button
    // ---------------------------------------------------------------------------------------------------
    function evalCFSFwdNum_Basic(text) {
        updateCFSSubmitButtonStatus_Basic();
    }


    // -------------------------
    // CFS Criteria Row Handlers
    // -------------------------

    // CFS Criteria selector handler
    // This handler opens dialog form with information of a criteria row that
    // has been clicked on the Call Forwarding Selective page
    // ----------------------------------------------------------------------
    function evalCriteriaRow_Basic(row) {
        var numChecked = getActiveCfsCriteriaCount_Basic();

        // Check whether row selection event triggered because of checking/unchecking checkbox
        if (numChecked != numCfsCheckedCriteria_Basic) {
            numCfsCheckedCriteria_Basic = numChecked;

            // update disable status of CFS Submit button
            updateCFSSubmitButtonStatus_Basic();

            // update criteria active status
            var activeStatus = getCFSCriteriaActiveStatus_Basic();
            $.ajax({
                type: "POST",
                url: "users/usersBasicList/CFSCriteria_Basic.php",
                data: { activeStatus: activeStatus.join(",") },
                async: false
                });

            return;
        }

        // row selection event has triggered because of criteria row selection
        cfsCriteriaDialogState_Basic = "modify";
        $.ajax({
            type: "POST",
            url: "users/usersBasicList/newCFSCriteria_Basic.php",
            data: { criteriaName: row.id },
            success: function(result) {
                $("#newCriteriaCFSDialog_Basic").dialog("open");
                $(".ui-dialog").find("ui-widget-header").css("background", "darkgreen");
                $("#newCriteriaCFSDialog_Basic").html(result);
            }
        });
    }


    // CFS Criteria highlighter
    // This handler highlights a CFS criteria row on mouse-over action
    // ---------------------------------------------------------------
    function highlightCriteriaRow_Basic(row, highlight) {
        row.style.backgroundColor = highlight ? "#dcfac9" : "white";
    }


    // Add all new group-wide action form
    //-----------------------------------
    function disableAllModificationForms_Basic() {
        $("#divCFA_Basic").hide();
        $("#divCFNR_Basic").hide();
        $("#divCFS_Basic").hide();
        $("#divAuth_Basic").hide();
        $("#divSwPck_Basic").hide();
        $("#divActivationNumber_Basic").hide();
        $("#groupWideModifyUser_Basic").hide();
        $("#callingLineIDBlocking_Basic").hide();
    }

    // Build list of all selected users
    // --------------------------------
    function getSelectedUsers_Basic()
    {
        var list = "";
        for (var id in selectedUsers_Basic) {
            if (selectedUsers_Basic.hasOwnProperty(id) && selectedUsers_Basic[id]) {
                list += id + "+-+-+";
            }
        }

        return list;
    }
/* code add by Sollogics Developer @ 02-11-2018 for basic list table checkbox event */
    var isCheckBoxIsClicked_Alert = false;
	function checkBoxEventAct_Basic(status) {
		isCheckBoxIsClicked_Alert = status;
	}
	
    $(function() {
		$(document).on("click", ".userIdVal_Basic", function(event)
		{
			
			if (!isCheckBoxIsClicked_Alert) {

                var userId = $(this).attr("id");

                /*
				var userIdValue1 = "";
				var fname = $(this).attr("data-firstname");
				var lname = $(this).attr("data-lastname");
				var extension = $(this).attr("data-extension");
				var phone = $(this).attr("data-phone");
				if($.trim(phone) != ""){
					userIdValue1 = phone+"x"+extension;
				}else{
					userIdValue1 = userId;
				}
				var userIdValue = userIdValue1+" - "+lname+", "+fname;
				*/
                var userIdValue = userId;
    			$.ajax({
    					type: "POST",
    					url: "userMod/userMod.php",
    					//data: { searchVal: userId, userFname : fname, userLname : lname },
    					success: function(result)
    					{
    						/*$(".spacerHide").hide();
    						$("#usersTable").hide();
    						$(".loadingRemove").remove();
    						$("#usersBanner").hide();

    						$("#userData").html(result);*/
    						$("#mainBody").html(result);
    						$("#searchVal").val(userIdValue);

    						setTimeout(function() {
    							$("#go").trigger("click");
    						}, 2000);

    						$('#helpUrl').attr('data-module', "userMod");
    						activeImageSwapModify();
    					}

    				});
			}

		});
	});

</script>
<script type="text/javascript">
var activeImageSwapExpress = function(){

	previousActiveMenu	= "users";
	 currentClickedDiv = "expressSheets";

		//active
  		if(previousActiveMenu != "")
		{
  		 	$(".navMenu").removeClass("activeNav");
			var $thisPrev = $("#users").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);

		}
		// inactive tab
		if(currentClickedDiv != ""){
			$("#expressSheets").addClass("activeNav");
			var $thisPrev = $("#expressSheets").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);
			 previousActiveMenu = currentClickedDiv;
    	}

}
</script>
<!-- sks code -->
<script>

var activeImageSwapModify = function()
{	previousActiveMenu	= "users";
	 currentClickedDiv = "userMod";

		//active
  		if(previousActiveMenu != "")
		{
  			$("#userMod").removeClass("activeNav");
  		 	$(".navMenu").removeClass("activeNav");
			var $thisPrev = $("#users").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);

		}
		// inactive tab
		if(currentClickedDiv != ""){
			$("#userMod").addClass("activeNav");
			var $thisPrev = $("#userMod").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);
			 previousActiveMenu = currentClickedDiv;

    	}

}

</script>


<?php
require_once ("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");
?>
<div class="row twoColWidth">
<form action="users/printCSV.php" method="POST"
	name="departmentDropdown" id="departmentDropdown_Basic">
	<div class="centerDesc" <?php echo "style= \"display: " . ($useDepartments == "true" ? "block" : "none") . "\""; ?>>
            <label class="labelText" for="department">Limit by Department</label> <span
			class="spacer">&nbsp;</span> <select name="department"
			id="department_Basic">
			<option value=""></option>
			<?php
			require_once ("/var/www/lib/broadsoft/adminPortal/getDepartments.php");
			if (isset ( $departments )) {
				foreach ( $departments as $key => $value ) {
					echo "<option value=\"" . $value . "\"";
					if ($_POST ["department"] == $value) {
						echo " SELECTED";
					}
					echo ">" . $value . "</option>";
				}
			}
			?>
		</select>
	</div>
	<div style="clear: right;">&nbsp;</div>
	<input type="hidden" name="selectedUserForDownload" id="selectedUserForDownload" value="" />
</form>
</div>

<div style="clear: right;">&nbsp;
	<div style="display: <?php echo $showBasicTable; ?>; text-align: center;" class="" id="loading_Basic"><img src="/Express/images/ajax-loader.gif"></div>
</div>
<!-- Some debug stuff -->
<div>
    <?php //if(isset($_SESSION["debug"])) { echo $_SESSION["debug"]; } ?>
</div>

<!-- <div class="dropdown-wrap"></div> -->

<!-- User Filters -->
<link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v11">
<script src="/Express/js/bootstrap.min.js"></script>
<script src="/Express/js/jquery-1.17.0.validate.js"></script>
<script src="/Express/js/jquery.loadTemplate.min.js"></script>

<!-- User - Basic Filters Starts -->
<div class="" id="basicUsersQuickFiltersBox" style="display: none;">
	<div class="row" style="margin: 0 auto;width: 90%;">
		<button type="button"
		        class="filterTitleBtn cancelButton"
		        id="basicfiltersStatusTitleBtn" style="margin-top:-180px;float:right">
			<span id="basicfiltersStatusTitleText" style="color: inherit;">Quick Filters</span>
			<span id="basicfiltersStatusTitleIcon" class="glyphicon glyphicon-chevron-right fIcon" style="color: inherit;"></span>
		</button>
	</div>

<!-- <div class="row" style="text-align: right;width: 90%;">
							<button type="button" class="filterTitleBtn cancelButton" id="basicfiltersStatusTitleBtn">Quick Filters</button>
						</div>
						 -->
						
	<div id="basicfiltersDiv" class="row" style="display: none;">
		 <div class="divCFAforStyle fcorn-registerTwo">
			<div class="row " >
				  <h2 class="subBannerCustom">Quick Filters</h2>
				<div class="col-md-12">
					<form id="basicFiltersForm">
						<div id="basicUserFilters">
						</div>
						<br/>
						<div class="row fcorn-registerTwo adminAddBtnDiv">
							<div class="col-md-2" style="width:21% !important">
								<div class="form-group">
									<label class="labelText">Response Size Limit</label>
    								<div class="dropdown-wrap">
        								<select class="" name="responseSizeLimit" style="width:100% !important">
        									<option value="">Unlimited</option>
        									<option value="10">10</option>
        									<option value="50">50</option>
        									<option value="100">100</option>
        									<option value="500">500</option>
        								</select>
        							</div>
								</div>
							</div>
							
							<div class="col-md-2" style="width:21% !important"></div>	
							<div class="col-md-6">
								<div class="form-group filterMarginTopAlign">
									<input CHECKED type="checkbox" id="useBasicFilteredListInDetailedList"/>
									<label class="labelText" for="useBasicFilteredListInDetailedList"><span></span>Use Selection in Detailed List</label>
								</div>
							</div>
						</div>
						<div class="row" style="text-align:center">
							<button type="button"
							        id="basicFiltersFormSubmitBtn"
							        class="subButton">Apply Filters</button>
						</div>
					</form>
				</div>
			</div>
		</div> 
	</div>
</div>
<script type="text/javascript">
    var fb_params = {
        'filters' : {
            'UserFirstName' : {
                'title' : 'User First Name',
                'modes' : ['Contains', 'Starts With', 'Equal To']
            },
            'UserLastName' : {
                'title' : 'User Last Name',
                'modes' : ['Contains', 'Starts With', 'Equal To']
            },
            'Extension' : {
                'title' : 'Extension',
                'modes' : ['Contains', 'Starts With', 'Equal To']
            },
            'Dn' : {
                'title' : 'DN',
                'modes' : ['Contains', 'Starts With', 'Equal To']
            },
            'UserId' : {
                'title' : 'User ID',
                'modes' : ['Contains', 'Starts With', 'Equal To']
            },
            'EmailAddress' : {
                'title' : 'User Email Address',
                'modes' : ['Contains', 'Starts With', 'Equal To']
            },
	        /*
            'ExactUserDepartment' : {
                'title' : 'Group Department',
                'modes' : ['Equal To']
            }
            */
        },
	    'resetCallback': resetQuickFilters
    };

    buildDKFilters('#basicUserFilters', fb_params);

    $('#basicFiltersFormSubmitBtn').click(function () {
        $("#selectAllDiv_Basic").hide();
        $(".userBasicListDiv").hide();
        $("#downloadCSV_Basic").hide();
        $(".hideInitial").hide();
        basicUsersTableLoaded = false;
        loadBasicUsersTable = true;
        basicViewUsesList($('#basicFiltersForm').serialize());

        toggleBasicfiltersDiv(false);


        if ($('#basicfiltersStatusTitleBtn').isInViewport()) {
            //The Button is vissible, no need to scroll
        } else {
            //$('#basicUsersQuickFiltersBox').scrollTop(0);

            $('html, body').animate({
                scrollTop: ($('#basicUsersQuickFiltersBox').offset().top)
            },500);
        }

        quickFiltersIsApplied = true;
        //$('#basicfiltersStatusTitleText').html('Quick Filters - Applied');
        //$("#usersBanner").html("Users List / Group-wide Users Modify<sup class='filtersApplied'>(Quick Filters Applied)</sup>");
        $("#usersBanner").html("<div class='filterMainHeadDiv'>Users List / Group-wide Users Modify </div><div class='filterApplyHead'><img src='/Express/images/icons/filter_icon_lrg.png' /><span>Quick Filters Applied<span></div>");

    });

    $('#basicfiltersStatusTitleBtn').click(function () {
        toggleBasicfiltersDiv();
    });

    function toggleBasicfiltersDiv(status) {

        status = status || null;

        var basicfiltersDivObj = $('#basicfiltersDiv');

        if(status != null) {
            basicfiltersDivObj.toggle(status);
        } else {
            basicfiltersDivObj.toggle();
        }

        if(basicfiltersDivObj.is(":visible")) {
            $('#basicfiltersStatusTitleIcon').addClass('glyphicon-chevron-down');
        } else {
            $('#basicfiltersStatusTitleIcon').removeClass('glyphicon-chevron-down');
        }

	}

    function resetQuickFilters() {
        $('#basicFiltersForm select[name="responseSizeLimit"]').val('');
        //Reload Users only if Filters are currently applied.
        if(quickFiltersIsApplied) {
            $("#selectAllDiv_Basic").hide();
            $(".userBasicListDiv").hide();
            $("#downloadCSV_Basic").hide();
            $(".hideInitial").hide();
            basicUsersTableLoaded = false;
            loadBasicUsersTable = true;
            basicViewUsesList();

            quickFiltersIsApplied = false;
            detailedUsersTableLoaded = false;

            //$('#basicfiltersStatusTitleText').html('Quick Filters - Not Applied');
            $("#usersBanner").html("Users List / Group-wide Users Modify");
        }

        toggleBasicfiltersDiv(false);
    }

</script>
<!-- User - Basic Filters Ends -->


<!-- Call Forwarding Always Form -->
<!-- Note: Visibility of this form is initially switched off -->
<div class="row twoColWidth" style="display: none;" id="divCFA_Basic">
    <div class="activeLabelDiv"><label class="activeLabel">Call Forwarding Always</label></div>
<div class="col-md-12 divCFAforStyle">
<form action="" name="CFA" id="CFA_Basic">
		<!-- Form Title -->
		<!-- Service Activation -->
    <div style="" class="col-md-12">
            <div class="form-group">
                <label for="activateCFATrue_Basic" class="labelText">Activate for all selected users:</label>
                <input type="radio" name="activateCFA" id="activateCFATrue_Basic" value="true" onchange="evalActivation_Basic(this)"><label class="labelText" for="activateCFATrue_Basic"><span></span>On</label>
                <input type="radio" name="activateCFA" id="activateCFAFalse" value="false" checked onchange="evalActivation_Basic(this)"><label class="labelText" for="activateCFAFalse"><span></span>Off</label> 
			</div>
	</div>

		<!-- Forwarding Number  -->
    <div style="" class="col-md-12 CFSDivForInputs">
        <div class="form-group">
        <label for="phSipFwdCFA_Basic" class="labelText">All calls Forward to phone number / SIP-URI:</label><br>
	<input type="text" name="phSipFwdCFA" id="phSipFwdCFA_Basic" size="35" onchange="evalFwdNum_Basic(this)" oninput="evalFwdNum_Basic(this)">
	</div>
		
		
		</div>

		<!-- Play Ring Reminder -->
	
    <div style="" class="col-md-12">
        <div class="form-group">
			<input type="checkbox" name="ringReminderChk" id="ringReminderChk_Basic">
			<label style="" for="ringReminderChk_Basic" class="labelText"><span class="selectAllSpan"></span>Play Ring Reminder for forwarded calls</label>
		</div>
	</div>

		<!-- Submit button -->
    <div class="col-md-12">
    <div class="form-group alignCenter groupWideModifyMarginBottom">
        <input style="" type="button" class="submitCFABasic subButton" name="submitCFA" id="submitCFABasic" value="Submit">
    </div>
    </div>
    </form>
    </div>
</div>


<!-- Call Forwarding Not Reachable Form -->
<!-- Note: Visibility of this form is initially switched off -->
<div class="row twoColWidth" style="display: none" id="divCFNR_Basic">
		<!-- Form Title -->
<div class="activeLabelDiv"><label class="activeLabel">Call Forwarding Not Reachable</label></div>
<div class="col-md-12 divCFAforStyle">
	<form action="" style="" name="CFNR" id="CFNR_Basic">
			<!-- Service Activation -->
	        <div style="" class="col-md-12">
	        <div class="form-group">
	            <label for="activateCFNRTrue_Basic" class="labelText">Activate for all selected users:</label>
	            <input type="radio" name="activateCFNR" id="activateCFNRTrue_Basic" value="true" onchange="evalActivation_Basic(this)"><label class="labelText" for="activateCFNRTrue_Basic"><span></span>On</label>
	            <input type="radio" name="activateCFNR" id="activateCFNRFalse" value="false" checked onchange="evalActivation_Basic(this)"><label class="labelText" for="activateCFNRFalse"><span></span>Off</label>
	        </div>
	        </div>
	
			<!-- Forwarding Number  -->
	        <div style="" class="col-md-12 CFSDivForInputs">
	        <div class="form-group">
	            <label for="phSipFwdCFNR" class="labelText">All calls Forward to phone number / SIP-URI:</label><br>
		    <input type="text" name="phSipFwdCFNR" id="phSipFwdCFNR_Basic" size="35" onchange="evalFwdNum_Basic(this)" oninput="evalFwdNum_Basic(this)">
	        </div>
	        </div>
	
			<!-- Submit button -->
	        <div class="col-md-12">
	        <div class="form-group alignCenter groupWideModifyMarginBottom">
	            <input style="" type="button" class="submitCFNRBasic subButton" name="submitCFNR" id="submitCFNRBasic" value="Submit">
	        </div>
	        </div>
	    </form>
    </div>
 </div>

<!-- Call Forwarding Selective Form -->
<!-- Note: Visibility of this form is initially switched off -->
<div class="row twoColWidth" style="display: none" id="divCFS_Basic">
		<!-- Form Title -->
    <div class="activeLabelDiv"><label class="activeLabel">Call Forwarding Selective</label></div>
    <div class="col-md-12 divCFAforStyle twoColWidth" style="margin-bottom: 10px;">
    <form action="" style="" name="CFS" id="CFS_Basic">

		<!-- Service Activation -->
        <div style="" class="col-md-12">
            <div class="form-group">
            <label for="activateCFSTrue" class="labelText">Activate for all selected users:</label>
            <input style="" type="radio" name="activateCFS" id="activateCFSTrue" value="true" onchange="evalActivationCFS_Basic(this)"> <label class="labelText" for="activateCFSTrue"><span></span>On</label>
            <input type="radio" name="activateCFS" id="activateCFSFalse_Basic" value="false" checked onchange="evalActivationCFS_Basic(this)"> <label class="labelText" for="activateCFSFalse_Basic"><span></span>Off</label>
            </div>
        </div>
		<!-- Forwarding Number  -->
        <div style="" class="col-md-12 CFSDivForInputs" id="CFSDivForInputs">
            <div class="form-group">
                <label for="phSipFwdCFS_Basic" class="labelText">Default Call Forward to phone number / SIP-URI:</label><span class="required">*</span> <br>
                <input type="text" name="phSipFwdCFS" class="input-control" id="phSipFwdCFS_Basic" size="35" onchange="evalCFSFwdNum_Basic(this)" oninput="evalCFSFwdNum_Basic(this)">
            </div>
        </div>

		<!-- Play Ring Reminder -->

        <div style="" class="col-md-12">
        <div class="form-group">
			<input type="checkbox" name="ringReminderChkCFS" id="ringReminderChkCFS_Basic">
            <label style="" class="labelText" for="ringReminderChkCFS_Basic"><span class="selectAllSpan"></span>Play Ring Reminder for forwarded calls</label>
		</div>
	
		</div>

		<!-- Forwarding Criteria Buttons -->
        <div class="col-md-12 alignBtn">
	
			<div class="form-group alignCenter">
				<input style="" type="button" class="newCriteriaCFS CFSNewBtn" name="newCriteriaCFS" id="newCriteriaCFS_Basic" value="Add selective forwarding criteria">
		<!--input style="font-size: 11px" type="button" class="deleteCriteriaCFS" name="deleteCriteriaCFS" id="deleteCriteriaCFS" value="Delete"-->
        	</div>
		</div>

		<!-- Criteria -->
        <div id="cfsCriteriaTable_Basic" class="col-md-12">
			 <div class="form-group">
				<div style="" class="">--No Criteria--</div>
			</div>
        </div>

		<!-- Submit button -->
        <div class="col-md-12">
        <div class="form-group alignCenter groupWideModifyMarginBottom"><input style="" type="button" class="submitCFS_Basic subButton" name="submitCFS" id="submitCFS_Basic" value="Submit">
        </div>
        </div>
	</form>
</div>
</div>


<!-- Authentication Password Form -->
<!-- Visibility of this form is initially switched off -->
<div class="row twoColWidth" style="display: none" id="divAuth_Basic">
    <!-- Form Title -->
    <div class="activeLabelDiv"><label style="" class="activeLabel">Authentication</label></div>   
    <div class="col-md-12 divCFAforStyle">     
    <form action="" name="authPasswords" id="authPasswords_Basic">
		

		<!-- First typed authentication password -->
        <div class="col-md-12 CFSDivForInputs" id="typedPassword_Basic">
        <div class="form-group">
			
                <label for="authPassword1_Basic" class="labelText" style="">Type new authentication password:</label><br>
				<input type="password" name="authPassword1" id="authPassword1_Basic" size="25" oninput="evalAuthPassword(this)">
				<label for="authPassword1_Basic" style="display: none" id="authPassword1CheckMark_Basic">&#10003</label>
			</div>
		</div>

		<!-- Automatically generated password - initially invisible -->
        <div class="col-md-12 CFSDivForInputs" style="display: none" id="generatedPassword_Basic">
            <div class="form-group">
                <label for="authGeneratedPassword_Basic" style="" class="labelText">New authentication password:</label><br>
					<input type="text" name="authGeneratedPassword" id="authGeneratedPassword_Basic" size="25" onchange="evalAuthGeneratedPassword_Basic(this)" oninput="evalAuthGeneratedPassword_Basic(this)">
			</div>
		</div>

		<!-- Re-typed authentication password -->
        <div class="col-md-12 CFSDivForInputs" id="passwordRetype_Basic">
        <div class="form-group">
                <label for="authPassword2_Basic" style="" class="labelText">Re-type new authentication password:</label><br>
				<input type="password" name="authPassword2" id="authPassword2_Basic" size="25" oninput="evalAuthPassword_Basic(this)">
                <label for="authPassword2_Basic" style="display: none" class="" id="authPassword2CheckMark_Basic">&#10003</label>
			</div>
		</div>

		<!-- Checkbox for automatic password generation -->
        <div style="" class="col-md-12">
		
			<input type="checkbox" name="genAuthPassword" id="genAuthPassword_Basic" value="true" onclick="onGeneratePasswordSelect_Basic(this)">
            <label for="genAuthPassword_Basic" class="labelText"><span class="selectAllSpan"></span>Generate Password</label>
			<!--input style="float: right; margin-right: 3px" type="button" class="submitAuthentication" name="submitAuthentication" id="submitAuthentication" value="Submit"-->
		</div>
			<!-- Submit button -->
	    <div class="col-md-12 ">
	        <div class="form-group alignCenter groupWideModifyMarginBottom">
				<!--label for="submitAuthentication" style="margin-top: 3px">A&nbsp;</label-->
		    <input style="display: none;" type="button" class="submitAuthentication subButton noMarzin" name="submitAuthentication" id="submitAuthentication_Basic" value="Submit">
	        </div>
	    </div>  
    </form>
    </div>
</div>


<!-- Services / Service Packs form -->
<!-- Note: Visibility of this form is initially switched off -->
<div class="row twoColWidth" style="display: none" id="divSwPck_Basic">
    <div class="activeLabelDiv"><label class="activeLabel">Services / Service Packs Management</label></div>
    <div class="col-md-12 divCFAforStyle CFSDivForInputs">    
	<form action="" name="SwPck" id="SwPck_Basic" style="padding:25px 0;">
		<!-- Form Title -->
		<!-- Subtitle: Service Packs -->
        <div class="col-md-12">
        <div class="col-md-10 paddingZero">
        <div class="col-md-7 paddingZero">
        <div class="form-group">
        <label class="labelText">Service Packs</label>

		<!-- Service Pack selection -->
        <div class="dropdown-wrap">
            <select style="" name="srvPack" id="srvPack_Basic" onchange="onServicePackSelectionChange_Basic(this)">
				<option value="srvPackNone">Select one:</option>
                <?php
					if (isset ( $servicePacks )) {
						foreach ( $servicePacks as $key => $value ) {
							echo "<option value=\"" . $value . "\">" . $value . "</option>";
						}
					}
					?>
           </select>
 		</div>
        </div>
        </div>
        <div class="col-md-5 ">
			<div class="form-group" style="">
				<div>&nbsp;</div>
			<!-- Assignment radio buttons -->
				<label for="srvPack"><input type="radio" name="assignSvcPack" id="assignSvcPack1True_Basic" value="true"><label for="assignSvcPack1True_Basic" class="labelText"><span></span>Assign</label>
				</label>
				<input type="radio" name="assignSvcPack" id="assignSvcPackFalse_Basic" value="false" checked><label for="assignSvcPackFalse_Basic" class="labelText"><span></span>Unassign</label>
			</div>
        </div>
	
    </div>
     <div class="col-md-2"></div>
		</div>
       <div class="col-md-12">
		<!-- Subtitle: User Services -->
   <div class="col-md-10 paddingZero CFSDivForInputs">
        <div class="col-md-7 paddingZero">
        <div class="form-group">
        <label class="labelText">User Services</label>      

		<!-- User service selection -->
        <div class="dropdown-wrap">
            <select style="" name="usrServices" id="usrServices_Basic" onchange="onUserServiceSelectionChange_Basic(this)">
				<option value="usrServiceNone">Select one:</option>
                <?php
				if (isset ( $userServices )) {
					sort ( $userServices );
					foreach ( $userServices as $key => $value ) {
						echo "<option value=\"" . $value . "\">" . $value . "</option>";
					}
				}
				?>
            </select>
      </div>
		</div>
        </div>
		<!-- Assignment radio buttons -->
        <div class="col-md-5">
        <div class="form-group">
        <div>&nbsp;</div>
			<label for="srvPack"> 
	    <input type="radio" name="assignUsrService" id="assignUsrServiceTrue_Basic" value="true"><label for="assignUsrServiceTrue_Basic" class="labelText"><span></span>Assign</label>
            <input type="radio" name="assignUsrService" id="assignUsrServiceFalse_Basic" value="false" checked><label for="assignUsrServiceFalse_Basic" class="labelText"><span></span>Unassign</label>
			</label>
		</div>
        </div>
</div>
<div class="col-md-2"></div>
		
		</div>

		<!-- Submit button -->
        <div class="col-md-12">
        <div class="form-group alignCenter groupWideModifyMarginBottom">
	<input style="" type="button" class="submitSvcAssignmentBasic subButton" name="submitSvcAssignment" id="submitSvcAssignmentBasic" value="Submit">
        </div>
        </div>
    </form>
    </div>
</div>



<!-- Note: Visibility of this form is initially switched off -->
<div  class="row twoColWidth"  style="display: none;" id="divActivationNumber_Basic">
     <div class="activeLabelDiv"><label class="activeLabel">Activation / Deactivation Number</label></div>    
        <div class="col-md-12 divCFAforStyle">
	<form action="numberActivationAction" name="numberActivationFrm" id="numberActivationFrm_Basic">
		<!-- Form Title -->
		
		<!-- Service Activation -->
		<div style="width: 100%" class="leftDesc userNumberAction_Basic">
			<label for="activateCFATrue_Basic" class="labelText marginRightButton">Activate/Deactivate all selected
				users:</label> 
                    <input style="margin-left: 5px" type="radio" name="activateNumber" id="activateNumber_Basic" value="activate" checked> 
                    <label class="labelText" for="activateNumber_Basic" style="margin-left: 0;"><span></span>Activate</label> 
                    <input type="radio" name="activateNumber" id="deActivateNumber_Basic" value="deactivate"> 
                    <label class="labelText" for="deActivateNumber_Basic" style="margin-left: 0;"><span></span>Deactivate</label> 
                    
		</div>
		<!-- <div style="margin-left: 5px; width: 30%" class="rightDesc">&nbsp;</div> -->
		<div class="centerDesc"></div>
		<!-- Submit button -->
 		<div class="col-md-12 alignCenter">
 		<div class="alignCenter form-group groupWideModifyMarginBottom">
                <input type="button" class="submitCFABasic subButton " name="submitActiveNumber" id="submitActiveNumberBasic" value="Submit">
                </div>
 		</div>
	</form>
        </div>
</div>




<?php 
if($_SESSION["permissions"]["voiceManagement"] == "1")
{ ?>

<div style="display: none;" id="groupWideModifyUser_Basic" class="row twoColWidth">
	<div class="activeLabelDiv">
     	<label class="activeLabel"> Modify Voice Management Service</label>
     </div>
     <form action="" name="modsVoiceMessaging" id="modsVoiceMessaging_Basic">
     <div class="col-md-12 divCFAforStyle"> 
    	
    	<!-- assign vm service -->
        	<div class="row">
         		<div class="col-md-6">
             			<div class="form-group">
             				<label class="labelText">Assign Voice Messaging Service</label>
             			</div>
             	</div>
             	
         		<div class="col-md-6" style="padding-left:0">
         			<div class="form-group">
             		 	<input type="radio" name="voiceMessaging" id="voiceMessagingBasicYes_Basic" value="true" checked=""/>
                 		<label class="labelText" for="voiceMessagingBasicYes_Basic" style="margin-left: 0;"><span></span>Yes</label> 
                		<input type="radio" name="voiceMessaging" id="voiceMessagingNo_Basic" value="false" />
                 		<label class="labelText" for="voiceMessagingNo_Basic"><span></span>No</label> 
         			</div>
         		</div>
             </div>
 <!-- end assign vm service -->
             
 <!-- enable voice Message -->
     		<div id="dvPassportBasic" style="display: none; text-align:center;">		
             	<div class="row">
             		<div class="col-md-6">
                 		<div class="form-group">
                 			<label class="labelText">Enable Voice Messaging</label>
                 		</div>
             		</div>
                 	
             		<div class="col-md-6" style="padding-left:0">
             			<div class="form-group">
                 			<input type="radio" name="isActive" id="isActiveTrue_Basic" value="true" checked />
                 			<label class="labelText" for="isActiveTrue_Basic" style="margin-left: 0;"><span></span>On</label> 
                 		 
    						<input  type="radio" name="isActive" id="isActiveFalse_Basic" value="false" />
                 		 	<label class="labelText" for="isActiveFalse_Basic"><span></span>Off</label> 
                 		 	<br/>
                 		 	
                 		 	<input type="checkbox" name="alwaysRedirectToVoiceMail" id="alwaysRedirectToVoiceMail_Basic" value="true">
                 		 	<label  for="alwaysRedirectToVoiceMail_Basic" class="labelText"><span class="selectAllSpan"></span>Send All Calls to Voice Mail</label>
    						 
    						<input style="width: 20px;" type="hidden" name="" value="false" /> <br/>
    						
    						<input type="checkbox" name="busyRedirectToVoiceMail" id="busyRedirectToVoiceMail_Basic" value="true" checked="checked">
    						
    						<label style="" for="busyRedirectToVoiceMail_Basic" class="labelText"><span class="selectAllSpan"></span>Send Busy Calls to Voice Mail</label><br/>
    						
    						<input type="hidden" name="" value="false" />
    						
    						<input type="checkbox" name="noAnswerRedirectToVoiceMail" id="noAnswerRedirectToVoiceMail_Basic" value="true" checked="checked">
    					
    						<label style="" for="noAnswerRedirectToVoiceMail_Basic" class="labelText"><span class="selectAllSpan"></span>Send Unanswered Calls to Voice Mail</label><br/>
						</div>
					</div>
 <!-- end voice Messaging -->     
         
 <!-- Use Unified Messaging -->  
           
             		<div class="col-md-6">
             			<div class="form-group">
                 			<label class="labelText">Use Unified Messaging</label>
                 		</div>
                 	</div>
                 	
                 	<div class="col-md-6" style="padding-left:0">
                 		<div class="form-group">
                 		<input type="hidden" name="processing" id="processingUnified" value="Unified Voice and Email Messaging"/>
                     		<label class="labelText" for=""><span></span>ON </label> 
                    	</div>
                    </div>
                     	 
<!-- end use unified messaging -->  

 <!-- Additionally --> 
        			<div class="col-md-6">
                 		<div class="form-group">
                 			<label class="labelText">Additionally...</label>
                 		</div>	  
     				</div>
      
                 		<div class="col-md-6" style="padding-left: 0;">
                 		<br/>
             			<div class="form-group">
                     		<input type="checkbox" name="sendCarbonCopyVoiceMessage" id="sendCarbonCopyVoiceMessage_Basic" value="true">
                     		
                     		<label for="sendCarbonCopyVoiceMessage_Basic" class="labelText"><span class="selectAllSpan"></span>Email Message Carbon Copy</label><br/>
                     		 
    						<label class="labelText">Carbon Copy Address</label><br/>
    						<input type="email" name="voiceMessageCarbonCopyEmailAddress" id="voiceMessageCarbonCopyEmailAddressVEA" size="32" maxlength="80" value="" onchange="evalFwdNum_Basic(this)" oninput="evalFwdNum_Basic(this)">
						 	<div class="clr"></div>
						
							<input  type="checkbox" name="transferOnZeroToPhoneNumber" id="transferOnZeroToPhoneNumber_Basic" value="true">
					
							<label for="transferOnZeroToPhoneNumber_Basic" class="labelText"><span class="selectAllSpan"></span>Transfer on '0' to Phone Number</label><br/>
							 
					
							<label class="labelText">Transfer on '0' to DN</label>
							<input type="text"  name="transferPhoneNumber" id="transferPhoneNumberDN" size="32" maxlength="80" value="" onchange="evalFwdNum_Basic(this)" oninput="evalFwdNum_Basic(this)" maxlength="12">
					 
						</div>
             		</div>
					<div class="clr"></div> 
           			<div class="col-md-6">
                 		<div class="form-group">
                 			<label class="labelText">Mailbox Limit </label>
                 		</div>
             		</div>
             		<div class="col-md-6" style="padding-left: 0;">
             			<div class="form-group">
                     		 <div class="dropdown-wrap oneColWidth">
            					<select name="mailBoxLimit" id="mailBoxLimit">
            						<option value="Use Group Default">Use Group Default</option>
            						<option value="10">10</option>
            						<option value="20">20</option>
            						<option value="30">30</option>
            						<option value="40">40</option>
            						<option value="50">50</option>
            						<option value="60">60</option>
            						<option value="70">70</option>
            						<option value="80">80</option>
            						<option value="90">90</option>
            						<option value="100">100</option>
            						<option value="200">200</option>
            						<option value="300">300</option>
            						<option value="400">400</option>
            						<option value="500">500</option>
            						<option value="600">600</option>
            						<option value="700">700</option>
            						<option value="800">800</option>
            						<option value="900">900</option>
                                </select>
							</div>
						</div>
                    </div>
                     	 
             	</div>  
                 </div><!-- end main row dvPasport -->
             	  <div class="clr"></div>
             	   
             	    <div class="row">
                     	<div class="col-md-12">
                			<div class="form-group groupWideModifyMarginBottom" style="text-align:center">
                				<input type="button" class="submitVM subButton" name="submitVoiceManagement" id="submitVoiceManagement_Basic" value="Submit">
                			</div>
            			</div>
            		</div>
     		</div>
     					
     		 
     	</form>	
 	</div>
	
<?php }?>

        <div class="row twoColWidth"  style="display: none;" id="callingLineIDBlocking_Basic">
             <div class="activeLabelDiv"><label class="activeLabel">Block Calling Line ID</label></div>    
                <div class="col-md-12 divCFAforStyle">
        	<form action="callingLineIDBlockingAction" name="callingLineIDBlockingFrm" id="callingLineIDBlockingFrm_Basic">
        		<!-- Form Title -->
        		
        		<!-- Service Activation -->
        		<div style="width: 100%" class="leftDesc callingLineIDBlockingFrm_Basic">
        			<label for="" class="labelText marginRightButton">Block Calling Line ID : </label> 
                            <input style="margin-left: 5px" type="radio" name="callingLineIDBlockingInput" id="blockCalling_Basic" value="block" checked> 
                            <label class="labelText" for="blockCalling_Basic" style="margin-left: 0;"><span></span>Block</label> 
                            <input type="radio" name="callingLineIDBlockingInput" id="unblockCalling_Basic" value="unblock"> 
                            <label class="labelText" for="unblockCalling_Basic" style="margin-left: 0;"><span></span>Unblock</label> 
                            
        		</div>
        		<!-- <div style="margin-left: 5px; width: 30%" class="rightDesc">&nbsp;</div> -->
        		<div class="centerDesc"></div>
        		<!-- Submit button -->
         		<div class="col-md-12 alignCenter">
         		<div class="alignCenter form-group groupWideModifyMarginBottom">
                        <input type="button" class="subButton" name="submitCallingLineIDBlocking" id="callingLineIDBlockingBasic" value="Submit">
                        </div>
         		</div>
        	</form>
                </div>
        </div>

    	<?php 
        	$serviceInfo = new Services();
        ?>
		<div id="selectAllDiv_Basic" style="margin-top: 22px; margin-left: 7.2%;" class="hideInitialBasicInfo_selectAllDiv">

			<input type="checkbox"
			       onclick="selectAllBasicUsers(this);"
			       class="selAllChk selectAl_Chk_Basic" id="selectAllChk_Basic" />
			<label class="labelText" for="selectAllChk_Basic"><span class="selectAllSpan"></span>Select All </label><a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="You may also select Users by clicking checkboxes in the table, then selecting Group-Wide Modify actions from pull-down menu"><img class="infoIcon" src="images/NewIcon/info_icon.png"></a>

		</div>
                <div class="hideInitialBasicInfo_groupModifyDiv">
                    <div class="col-md-4">
                    <?php if($_SESSION["permissions"]["groupWideUserModify"] == "1"){?>
                    
			<div name="gwModsLabel" id="gwModsLabelBasic" style="padding-left:0;margin-left: -5px;">
			 <div class="">
				<label class="labelText" name="gwModsLabel" id="gwModsLabel_Basic"> Group-wide Modifications</label>
				<div class="dropdown-wrap form-group" class="dropdownGwMods" style="">
				<select style="" name="gwMods" class="form-control selectBlue" id="gwMods_Basic" onchange="onMenuSelectionChange_Basic(this)">
						<option value="gwModsNone">Select one:</option>
						<option value="gwModsCFA">Modify 'Call Forwarding Always' services</option>
						<option value="gwModsCFNR">Modify 'Call Forwarding Not Reachable' services</option>
						<option value="gwModsCFS">Modify 'Call Forwarding Selective' services</option>
						<option value="gwModsAuth">Reset SIP Authentication</option>
						<option value="gwModsSwPck">Services/Service Packs Management</option>
						<option value="gwModsDnActivate">Activate/Deactivate</option>
						<?php if($_SESSION["permissions"]["voiceManagement"] == "1")
						{ ?>
							<option value="gwModsVM">Modify 'Voice Management' Service</option>
						<?php }?>
						<?php  if($serviceInfo->hasCLIDBlocKingService($_SESSION['groupId'])) { ?>
						<option value="callingLineIDBlocking">Calling Line ID Blocking</option>
						<?php } ?>
				</select>
				</div>
			</div>
			</div>
                    
		<?php }?>
		</div>
			<div class="col-md-4 usersTableButton">
                            <div class="col-md-2" name="downloadCSV" id="downloadCSV_Basic" value="" ><img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png"/><br><span>Download<br>CSV</span></div>
				
				<div class="col-md-2 deleteUsers_Basic" style="" name="deleteUsers" id="deleteUsers_Basic" ><img src="images/icons/delete_selected_users_icon.png" data-alt-src="images/icons/delete_selected_users_icon_over.png" /><br><span>Delete<br>Selected<br>Users</span></div>
				
				<div class="col-md-2 expressSheetUsersBasic selectedBasicUsersActionBtn" style="cursor:pointer; float:right;" name="expressSheetUsers" id="expressSheetUsersBasic" ><img src="images/icons/delete_selected_users_icon.png"  data-alt-src="images/icons/delete_selected_users_icon_over.png"/><br><span>Switch to<br/>Express<br/> Sheets</span></div>
				
			</div>
		</div>
	
        

<!-- User Filters -->

<div id="basicUserListTableLoading" style="text-align: center; display: none;"><img src="/Express/images/ajax-loader.gif"/></div>
<div id="basicUserListNoUserAvailabe" style="text-align: center;color: red;">No User Availabe</div>

<div class="userBasicListDiv" style="width:100%">
	<div style="zoom: 1;">
           <!--  <table id="usersTable1" class="stripe row-border order-column customDataTable" cellspacing="0" width="100%"> -->
		<table id="basicUserListTable" class="stripe row-border order-column customDataTable basicUserListTable" cellspacing="0" style="width: 100%;">
			<thead>
				<tr>
					<th style="width: 5%; text-align: center">&nbsp;</th>
					<th style="width: 15%; text-align: center">User Id</th>
					<th style="width: 15%; text-align: center">Activated</th>
					<th style="width: 15%; text-align: center">User Name</th>
					<th style="width: 15%; text-align: center">Phone Number</th>
					<th style="width: 15%; text-align: center">Extension</th>
					<?php if($useDepartments == "true") { ?>
					<th style="width: 15%; text-align: center">Department</th>
                    <?php } ?>
				</tr>
			</thead>
			<tbody id="basicUserListTableBody">

			</tbody>
		</table>
	</div>

</div>
<!-- <div style="clear: both; height: 40px; width: 90%; margin: 0 auto; padding-top: 10px">
	<div class="col span_8 hideInitial">
		<input type="button" class="deleteUsers_Basic deleteButtonWithoutCS selectedBasicUsersActionBtn" name="deleteUsers" id="deleteUsers_Basic" value="Delete Selected Users"/>
	</div>
	<div class="col span_8 hideInitial" style="text-align: center;">
		<input type="button" class="expressSheetUsersBasic btn-averistar selectedBasicUsersActionBtn" name="expressSheetUsers" id="expressSheetUsersBasic" value="Switch to Express Sheets"/>
	</div>
	<div class="col span_8" style="float:right;">
		<input type="button" name="downloadCSV" id="downloadCSV_Basic" value="Download as CSV" style="float: right;"/>
	</div>
</div>-->

<div id="dialogUsers_Basic" class="dialogClass"></div>
<div class="row">
	<div class="col-md-12">
        <div id="dialogGroupWideModify_Basic" class="dialogClass"></div>
        </div>
</div>
<div class="row">
	<div class="col-md-12">
<div id="newCriteriaCFSDialog_Basic"></div>
</div>
</div>

<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
