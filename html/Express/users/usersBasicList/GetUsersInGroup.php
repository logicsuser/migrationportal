<?php

require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/GroupLevelOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");

Class GetUsersInGroup {
    
    public $userListInGroup = array();
    public $department = "";
	public $error = null;
    
    public function getAllUsersInGroup($searchCriteria = null, $withActivationStatus = false) {
        $gLO = new GroupLevelOperations();
        $userDn = new Dns ();

        if($this->department) {
        	if($searchCriteria) {
		        $searchCriteria['ExactUserDepartment'] = array('value' => $this->department);
	        } else {
		        $searchCriteria = array();
		        $searchCriteria['ExactUserDepartment'] = array('value' => $this->department);
	        }
        }

        $this->error = null;
        $userListInGroup = $gLO->getUsersInGroup($_SESSION['sp'], $_SESSION['groupId'], $searchCriteria);
        //echo"RRRR"; print_r($userListInGroup); //exit;
        if(empty($userListInGroup['Error'])) {

        	if(is_array($userListInGroup['Success'])) {
		        foreach ($userListInGroup['Success'] as $key => $value) {
			        //if ($this->filterUsers($value)) {
				    //    continue;
			        //}
			        if($withActivationStatus) {
				        $numberActivateResponse = $userDn->getUserDNActivateListRequest($value['userId']);
				        $phoneNumberStatus = "";
				        $userPhoneNumber = "";
				        //echo "<br/>data => ";print_R($numberActivateResponse);
				        if (empty ($numberActivateResponse ['Error'])) {
					        if (count($numberActivateResponse ['Success']) > 0) {
						        if (!empty ($numberActivateResponse ['Success'] [0] ['phoneNumber'])) {
							        $userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
						        }
						        if (!empty ($numberActivateResponse ['Success'] [0] ['status'])) {
							        $phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
						        }
					        }
				        }

				        $value['phoneNumber'] = $userPhoneNumber;
				        $value['activated'] = $phoneNumberStatus;
			        }

			        $value['userName'] = $value["firstName"] . " " . $value["lastName"];

			        if($value["activated"] == 'true') {
				        $value["activated"] = 'Yes';
			        } else if($value["activated"] == 'false') {
				        $value["activated"] = 'No';
			        } else {
				        $value["activated"] = '';
			        }
			        $this->userListInGroup[] = $value;
		        }
	        }
            
        } else {
	        $this->error = trim($userListInGroup['Error']);
        }
        
        return $this->userListInGroup;
    }
    
    public function setSelectedView($selectedView, $userId) {
            global $db;
            $updatePreView = 'UPDATE users SET '
                . 'usersView_selected = ?';
                $updatePreView .= " where id = ?";
                
                $params = array($selectedView, $userId);
                $delete_stmt = $db->prepare($updatePreView);
                $response = $delete_stmt->execute($params);
                
                return $response;
    }
    
    public function filterUsers($userData) {
        $res = false;
        if($this->department != "") {
            $res = $userData['department'] == $this->department ? false : true;
        }
        return $res;
    }
}
?>