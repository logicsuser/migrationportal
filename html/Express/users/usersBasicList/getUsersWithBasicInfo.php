<?php

require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/GroupLevelOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
require_once ("/var/www/html/Express/users/usersBasicList/GetUsersInGroup.php");

// print_r($userListInGroup['Success']); exit;
$gUIG   = new GetUsersInGroup();

if(isset($_POST['action']) && $_POST['action'] == "GetAllUserInGroup") {
    $gUIG->department = $_POST['department'];

    //User Filters - Start
	$searchCriteria = null;
    if(isset($_POST['filterItem'])) {

    	//error_log(print_r($_POST['filterItem'], true));
    	foreach ($_POST['filterItem'] as $key => $filterItem) {
    		if(isset($_POST['filterItemValue'][$key]) && ($_POST['filterItemValue'][$key] || $_POST['filterItemMode'][$key] == 'Equals to')) {
			    $searchCriteria[$filterItem] = array('mode' => $_POST['filterItemMode'][$key], 'value' => $_POST['filterItemValue'][$key]);
		    }
	    }

    }

    if(isset($_POST['responseSizeLimit']) && $_POST['responseSizeLimit'] > 0) {
	    $searchCriteria['responseSizeLimit'] = $_POST['responseSizeLimit'];
    }
	//error_log(print_r($searchCriteria, true));
	//User Filters - End

	$userListInGroup = $gUIG->getAllUsersInGroup($searchCriteria, false);

    $response = "";
    unset($_SESSION['detailedUsersList']);
    if(!empty($gUIG->error)) {
	    $errorMsg = str_replace( "%0D%0A", "", trim($gUIG->error));
	    preg_match('/Please narrow your search, there are ([0-9]+) matching records/', $errorMsg, $output_array);
		if($output_array && isset($output_array[1])) {
			$errorMsg = "There are " . $output_array[1] . " matching users - please narrow your search.";
		}
	    $response = json_encode(array('errorMsg' => $errorMsg));
    } else {

	    //Get Activation Status for first 50 users

	    /*
	    $first_50 = array_slice($userListInGroup, 0, 50);
	    $first_50 = getActivationStatusOfUsers($first_50);
	    $users = array($first_50);

	    if(count($userListInGroup) > 50) {
		    $rest_of_users = array_slice($userListInGroup, 50);
		    $rest_of_users = array_chunk($rest_of_users, 100);
		    $users = array_merge($users, $rest_of_users);
	    }
		*/

	    //$users = array_chunk($userListInGroup, 50);
	    //$users[0] = getActivationStatusOfUsers($users[0]);

	    //$response = json_encode(array('users' => $users, 'count' => count($userListInGroup)));

	    $response = json_encode(array('users' => array($userListInGroup), 'count' => count($userListInGroup)));

	    if(isset($_POST['useBasicFilteredListInDetailedList'])) {
		    $_SESSION['detailedUsersList'] = $userListInGroup;
	    }
    }
    echo $response;
}

if(isset($_POST['action']) && $_POST['action'] == "setSelectedView") {
    $response = $gUIG->setSelectedView($_POST['selectedView'], $_POST['userId']);
    echo $response;
}

if(isset($_POST['action']) && $_POST['action'] == "getActivationStatus") {
	$users = getActivationStatusOfUsers($_POST['users']);
	echo json_encode($users);;
}

function getActivationStatusOfUsers($users) {
	$userDn = new Dns();
	$returnArray = array();
	foreach($users as $value) {
		$numberActivateResponse = $userDn->getUserDNActivateListRequest($value['userId']);
		$phoneNumberStatus = "";
		$userPhoneNumber = "";
		//echo "<br/>data => ";print_R($numberActivateResponse);
		if (empty ($numberActivateResponse ['Error'])) {
			if (count($numberActivateResponse ['Success']) > 0) {
				if (!empty ($numberActivateResponse ['Success'] [0] ['phoneNumber'])) {
					$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
				}
				if (!empty ($numberActivateResponse ['Success'] [0] ['status'])) {
					$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
				}
			}
		}

		$value['phoneNumber'] = $userPhoneNumber;
		$value['activated'] = $phoneNumberStatus;

		$returnArray[] = $value;
	}
	return $returnArray;
}

?>