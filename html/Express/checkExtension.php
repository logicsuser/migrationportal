<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$xmlinput = xmlHeader($sessionid, "GroupExtensionLengthGetRequest17");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

//	$extLen = intval($xml->command->defaultExtensionLength);
	$minLen = intval($xml->command->minExtensionLength);
	$maxLen = intval($xml->command->maxExtensionLength);

	$hasExtensionError = false;
	if (strlen($_POST["extension"]) < $minLen or strlen($_POST["extension"]) > $maxLen)
	{
		$error = 1;
        $hasExtensionError = true;
		$bg = INVALID;
		if ($minLen == $maxLen)
		{
			$value = "Extension must be " . $minLen . " digits.";
		}
		else
		{
			$value = "Extension must be between " . $minLen . " and " . $maxLen . " digits.";
		}
	}
	if (!is_numeric($_POST["extension"]))
	{
		$error = 1;
        $hasExtensionError = true;
		$bg = INVALID;
		$value = "Extension must be numeric.";
	}

	if (! $hasExtensionError) {
        $xmlinput = xmlHeader($sessionid, "UserGetListInGroupRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<GroupId>" . htmlspecialchars($_SESSION["groupId"]) . "</GroupId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (isset($xml->command->userTable->row)) {
            foreach ($xml->command->userTable->row as $index => $extValue) {
                if (strval($extValue->col[10]) == $_POST["extension"]) {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Extension already in use.";
                    break;
                }
            }
        }
    }
?>
