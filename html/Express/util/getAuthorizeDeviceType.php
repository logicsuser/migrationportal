<?php 
/*
 * $nonObsoleteDevices - Group Devices feched from BW 
 * $sipGatewayLookup - DBLookUP object
 * $customDevices - A Device Type list of customDeviceList table
 * $sipSoftPhoneLookup - DBLookUP object
 * $useCustomDeviceList - Configured from Express Configuration
*/
class GetAuthorizeDeviceType{
    
    //function to get all authorized Analog Device Type
    function getAnalogDeviceType($nonObsoleteDevices, $sipGatewayLookup, $customDevices, $sipSoftPhoneLookup, $useCustomDeviceList){
        $deviceTypesAnalogGateways = array ();
        foreach ( $nonObsoleteDevices as $key => $value ) {
            // shortcut custom device list for Audio Codes until they are resolved
            $deviceIsAudioCodes = substr ( $value, 0, strlen ( "AudioCodes-" ) ) == "AudioCodes-";
            
            if ($useCustomDeviceList == "true" && ! in_array ( $value, $customDevices )) {
                if (! $deviceIsAudioCodes) {
                    continue;
                }
            }
            
            if (($sipGatewayLookup->get ( $value ) == "true" && $sipSoftPhoneLookup->get ( $value ) != "counterPath") || $deviceIsAudioCodes) {
                $deviceTypesAnalogGateways [] = $value;
            }
        }
        return $deviceTypesAnalogGateways;
    }
    
    //function to get all authorized SIP Device Type
    function getDigitalDeviceType($nonObsoleteDevices, $sipGatewayLookup, $customDevices, $sipSoftPhoneLookup, $useCustomDeviceList){
        $deviceTypesDigitalVoIP = array ();
        foreach ( $nonObsoleteDevices as $key => $value ) {
            // shortcut custom device list for Audio Codes until they are resolved
            $deviceIsAudioCodes = substr ( $value, 0, strlen ( "AudioCodes-" ) ) == "AudioCodes-";
            
            if ($useCustomDeviceList == "true" && ! in_array ( $value, $customDevices )) {
                if (! $deviceIsAudioCodes) {
                    continue;
                }
            }
            
            if (($sipGatewayLookup->get ( $value ) == "false" && $sipSoftPhoneLookup->get ( $value ) != "counterPath" && $sipSoftPhoneLookup->get ( $value ) != "generic") && !$deviceIsAudioCodes) {
                $deviceTypesDigitalVoIP [] = $value;
            }
        }
        return $deviceTypesDigitalVoIP;
    }
    
    //function to get all authorized SIP Device Type
    function getSoftPhoneDeviceType($nonObsoleteDevices, $sipGatewayLookup, $customDevices, $sipSoftPhoneLookup, $useCustomDeviceList){
        $deviceTypesSoftPhones = array ();
        $k = 0;
        foreach ( $nonObsoleteDevices as $key => $value ) {
            // shortcut custom device list for Audio Codes until they are resolved
            $deviceIsAudioCodes = substr ( $value, 0, strlen ( "AudioCodes-" ) ) == "AudioCodes-";
            
            if ($useCustomDeviceList == "true" && ! in_array ( $value, $customDevices )) {
                if (! $deviceIsAudioCodes) {
                    continue;
                }
            }
            
            if (($sipGatewayLookup->get ( $value ) == "false" && ($sipSoftPhoneLookup->get ( $value ) == "counterPath" || $sipSoftPhoneLookup->get ( $value ) == "generic")) && !$deviceIsAudioCodes) {
                $deviceTypesSoftPhones [$k]["softDeviceType"] = $value;
                $deviceTypesSoftPhones [$k]["softPhoneType"] = $sipSoftPhoneLookup->get ( $value );
                $k++;
            }
        }
        return $deviceTypesSoftPhones;
    }
    
}
?>
