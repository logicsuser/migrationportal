<?php 
class FormDataArrayDiff{
    
    function diffInTwoArray($array1, $array2){
        $diffArray = array();
            // determine Added Value
            $i = 0;
            foreach ($array1 as $val1) {
                if (! in_array($val1, $array2)) {
                    $diffArray["assigned"][$i++] = $val1;
                }
            }
            // determine Removed Value
            $i = 0;
            foreach ($array2 as $val2) {
                if (! in_array($val2, $array1)) {
                    $diffArray["removed"][$i++] = $val2;
                }
            }
        return $diffArray;
    }
}
?>