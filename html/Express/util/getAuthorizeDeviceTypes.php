<?php 
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/adminPortal/devices/deviceTypeManagement.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
$devobj = new DeviceTypeManagement();
$systemTableAllData = $devobj->getSystemTableDevices();
if(count($systemTableAllData) > 0){
    foreach ($systemTableAllData as $dkey => $dvalue){
        if($dvalue["enabled"] == "true"){
            $enabledDevicetypeList[$dkey] = $dvalue;
        }
    }
    
}
//print_r($enabledDevicetypeList);
$i=0;
foreach ($enabledDevicetypeList as $key => $value){
    if($value["phoneType"] == "Analog"){
        $deviceTypesAnalogGateways[] = $key;
    }else if($value["phoneType"] == "SIP Phone"){
        $deviceTypesDigitalVoIP[] = $key;
    }else if($value["phoneType"] == "Soft Phone"){
        $deviceTypesSoftPhones[$i]["softDeviceType"] = $key;
        $deviceTypesSoftPhones[$i]["softPhoneType"] = $value["softPhoneType"];
        $i++;
    }
}
?>