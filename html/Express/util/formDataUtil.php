<?php
/*
 * This method searched for the name in the form data array and
 * returns the value for the name.
 */
class FormDataUtil{
    function getFormDataValue($formDataArray, $formDataName)
    {
        if (isset($formDataArray) && ! empty($formDataArray)) {
            foreach ($formDataArray as $key => $value) {
                if ($value['name'] != '' && $value['name'] == $formDataName) {
                    return $value['value'];
                }
            }
        }
        return - 1;
    }
    
    /*
     * This method returns the form data in the key value format
     * as associative array.
     */
    function getFormDataNameValue($formDataArray)
    {
        $keys = array();
        if (isset($formDataArray) && !empty($formDataArray)) {
            foreach ($formDataArray as $key => $value) {
                if (isset($value['name']) && !empty($value['name'])) {
                    $keys[$value['name']] = $value['value'] ;
                }
            }
        }
        return $keys;
    }
}
?>