<?php

function getDeviceNamefromCriteria($deviceNameDID1, $deviceNameDID2) {
	
	require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
	require_once("/var/www/lib/broadsoft/adminPortal/DeviceNameBuilder.php");
	
	require_once ("/var/www/html/Express/util/deviceNameUtil.php");
	$deviceNameBuilder = new DeviceNameBuilder($deviceNameDID1,
			getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
			getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
			getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
			$forceResolve);
	
	if (! $deviceNameBuilder->validate()) {
		// TODO: Implement resolution when Device name input formula has errors
	}
	
	do {
		$attr = $deviceNameBuilder->getRequiredAttributeKey();
		$deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
	} while ($attr != "");
	
	$deviceName = $deviceNameBuilder->getName();
	
	if ($deviceName == "") {
		// Create Device Name builder from the second formula (DID type only).
		// At this point we know that there is second formula, because without second formula
		// name would be forcefully resolved in the first formula.
		
		$deviceNameBuilder = new DeviceNameBuilder($deviceNameDID2,
				getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
				getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
				getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
				true);
		
		if (! $deviceNameBuilder->validate()) {
			// TODO: Implement resolution when Device name input formula has errors
		}
		
		do {
			$attr = $deviceNameBuilder->getRequiredAttributeKey();
			$deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
		} while ($attr != "");
		
		$deviceName = $deviceNameBuilder->getName();
	}
	return $deviceName;
}




function getResourceForDeviceNameCreation($attr) {
	switch ($attr) {
		case ResourceNameBuilder::DN:               return $_SESSION["userInfo"]["phoneNumber"];
		case ResourceNameBuilder::EXT:              return $_SESSION["userInfo"]["extension"];
		case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
		case ResourceNameBuilder::GRP_ID:           return $_SESSION["groupId"];
		case ResourceNameBuilder::FIRST_NAME:       return $_SESSION["userInfo"]["firstName"];
		case ResourceNameBuilder::LAST_NAME:        return $_SESSION["userInfo"]["lastName"];
		case ResourceNameBuilder::DEV_TYPE:
			$deviceType = $_POST["deviceType"];
			return $deviceType;
			break;
		case ResourceNameBuilder::MAC_ADDR:
			$macAddress = $_POST["macAddress"];
			return $macAddress;
			break;
		case ResourceNameBuilder::INT_1:
		case ResourceNameBuilder::INT_2:            return isset($_SESSION["userInfo"]["deviceIndex"]) ? $_SESSION["userInfo"]["deviceIndex"] : "";
		//TODO: Device Index must be implemented in getUserInfo
		default: return "";
	}
}
?>