$(function()
{
	 var sourceSwapImage = function() {
            var thisId = this.id;
            var image = $("#"+thisId).children('img');
            var newSource = image.data('alt-src');
            image.data('alt-src', image.attr('src'));
            image.attr('src', newSource);
        }
        
		$('#deleteGroupCallPickupInstance').hover(sourceSwapImage, sourceSwapImage);

});

