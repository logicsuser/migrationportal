
<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

server_fail_over_debuggin_testing(); /* for fail Over testing. */

include("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
include("/var/www/lib/broadsoft/adminPortal/groupCallPickup.php");
include("/var/www/lib/broadsoft/adminPortal/GroupCallPickupUsers.php");

   include("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
   $changeLogObj = new ChangeLogUtility($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
   
$data = "Done";
//echo "<pre>"; print_r($_POST); die;


$mod = $_POST['module'];
$grp = new GroupCallPickup();
$resultMsg = "";

if($mod == "add"){	
			$name = $_POST['name'];
			$resultMsg = $grp->addGroupCallPickupAddInstance($name);

			if($resultMsg <> "Success"){
				$data = $resultMsg;
			}else{
			    $changeLogObj->module = "Call Pickup Groups Add";			    
			    $changeLogObj->entityName = $_POST['name'];
			    //$changeLogObj->changeLog(); //Code commented @ 06 Aug 2018
                            
                            //Code added @ 06 Aug 2018
                            $changeLog = array();
                            $cPGPostArr = $_POST;
                            unset($cPGPostArr['module']);
				foreach($cPGPostArr as $key=>$val){			
						$changeLog[$key] = $val;					
				}
                            $changeLogObj->changeLogAddUtility($changeLogObj->module, $changeLogObj->entityName, "callPickupGroupAddChanges", $changeLog);
                            //End code
			}
		 echo $data;

}elseif($mod == "list"){
    
    $resultMsg = $grp->showGroupCallPickupInstance();
    //$resultMsg = array();
    $html = "";
    $html .= "<div>
	      <div class='col-md-10'>
		      <div class='form-group'>
                 <label class='labelText' for='name'>Group Call Pickup List</label>
                     <div class='dropdown-wrap'>   
                                <select name='listGroupCallPickup' id='listGroupCallPickup' class='form-control selectBlue'>
                                <option value=''>None</option>";
                                foreach($resultMsg[0] as $key=>$val){
                                    $html .= "<option value='".$val."'>".$val."</option>";
                                }
                                $html .="</select>
                      </div>
                  </div>
			</div>";
           
           if(count($resultMsg)>0)
              {
                  $html     .="<script src='groupCallPickup/addl.js'></script>";
                  $html     .="<div class='col-md-1 deleteUsers'  name='deleteGroupCallPickupInstance' id='deleteGroupCallPickupInstance'>
                              <img src='images/icons/delete_selected_users_icon.png' data-alt-src='images/icons/delete_selected_users_icon_over.png' />
                              <br><span>Delete<br>Call Pickup<br>Group</span></div>	 
                               </div>";
                
            }
          
     $html .="</div>";
    echo $html;
    
}elseif($mod == "delete"){

				$name = $_POST['name'];
				
				if(isset($_POST['status']) && !empty($_POST['status'])){
					$status = $_POST['status'];
				}else{
					$status = "";
				}
	
				$grp1 = new GroupCallPickupUsers($name);
				//;//$grp->groupCallParkName = $name;
				$usersList = $grp1->getUsersInCallPickupGroup();
				//echo "<pre>"; print_r(count($usersList)); die;
				
				if(count($usersList) == 0 && empty($status)){
							$data = "UsersNotExist";
							// $resultMsg = $grp->deleteGroupCallPickupInstance($name);
							// if($resultMsg <> "Success"){
										// $data = $resultMsg;
							// }
				}else if(count($usersList) > 0 && empty($status)){
								$data = "UsersExist";
				}else{
					 if(count($usersList) >= 0 && !empty($status)){
				
								$resultMsg = $grp->deleteGroupCallPickupInstance($name);
								if($resultMsg <> "Success"){
											$data = $resultMsg;
								}else{
								    $changeLogObj->module = "Call Pickup Groups Delete";
								    $changeLogObj->entityName = $_POST['name'];
								    //$changeLogObj->changeLog(); //Code commented @ 06 Aug 2018
                                                                    $changeLogObj->changeLogDeleteUtility("Call Pickup Groups Delete", $changeLogObj->entityName, "callPickGroupModchanges", ""); //Code added @ 06 Aug 2018
								}
					 }
				}
				echo $data;
}

?>


