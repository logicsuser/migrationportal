<?php
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
?>
<script>

    $(function()
	{
/*hover back button image */
 		var sourceSwapImage = function() {
            var thisId = this.id;
            var image = $("#"+thisId).children('img');
            var newSource = image.data('alt-src');
            image.data('alt-src', image.attr('src'));
            image.attr('src', newSource);
        }
        
		$('#backButton,#addSubButton,#newGroup').hover(sourceSwapImage, sourceSwapImage);


    	
		$("#addGroupCallPickup").hide();
		$(".register-submit").hide();
		$(".cpgAddBtn").hide();

		$('#name').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) {
				e.preventDefault();
				$('#addSubButton').trigger('click');
			  }
			});
		 	
		$("#newGroup").click(function(){
			$("#addGroupCallPickup").show();
			$(".register-submit").show();
			$("#showListGroupCallPickup").hide();
			$("#newGroup").hide();
			$(".cpgAddBtn").hide();
			$(".ui-dialog-titlebar-close", $("#dialogBox").parentNode).show();
			$(".ui-dialog-buttonpane", $("#dialogBox").parentNode).show();
		});
		 
		$("#backButton").click(function(){ 
		 	$("#addGroupCallPickup").hide();
			$(".register-submit").show();
			$("#showListGroupCallPickup").show();
			$("#newGroup").show();
			$(".cpgAddBtn").show();
			$(".ui-dialog-titlebar-close", $("#dialogBox").parentNode).hide();
			$(".ui-dialog-buttonpane", $("#dialogBox").parentNode).hide();			
		});
			
			
		$("#dialogBox").dialog(
			{
				autoOpen: false,
				width: 800,
				modal: true,
				position: { my: "top", at: "top" },
				resizable: false,
				closeOnEscape: false,
				buttons: {
					"Complete": function() {
                                                
						var dataToSend = $("form#groupCallPickupAdd").serializeArray();
						$.ajax({
							type: "POST",
							url: "groupCallPickup/groupCallPickupAddDo.php",
							data: dataToSend,
							success: function(result) {
									$("#dialogBox").dialog("option", "title", "Request Complete");
									$(".ui-dialog-titlebar-close", this.parentNode).show();
									$(".ui-dialog-buttonpane", this.parentNode).show();
									$(":button:contains('Complete')").hide();
                                                                        
                                                                        //$(":button:contains('Return To Main')").show().addClass('subButton');
                                                                        //$(":button:contains('Add/Delete Another CPG')").show().addClass('subButton');
                                                                        
									$(":button:contains('Cancel')").hide();
									$("#dialogBox").html(result);
									$("#name").val('');
							}
						});
					},
					"Continue": function() {

									$("#dialogBox").html("<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">");
									$(":button:contains('Continue')").hide();
									$(":button:contains('Cancel')").hide();

									instanceId = $("#listGroupCallPickup").val();
									$.ajax({
											type: "POST",
											url: "groupCallPickup/do.php",
											data: {module:"delete", name : instanceId, status : "confirm"},
											success: function(result) {

												$.when($.ajax(showListGroup())).then(function () {
													$("#dialogBox").dialog("option", "title", "Request Complete");
													$(".ui-dialog-titlebar-close", this.parentNode).hide();
													//$(".ui-dialog-buttonpane", this.parentNode).show();

													$(":button:contains('More Changes')").show().addClass('moreChangesButton');
													$(":button:contains('Return To Main')").show().addClass('returnToMainButton');
													$("#dialogBox").html("<b>" + instanceId + "</b> <div class='labelTextGreyHeadingRc ceterText'> Call Pickup Group has been deleted successfully</div>");
												});
											}
									});
					},
					"More Changes" : function() {

									$("#dialogBox").dialog("close");
					},
					"Return To Main": function() {
									window.location.href = "main.php";
									
					},
					"Add/Delete Another CPG": function() {
									$(this).dialog("close");
									$('.dialogClass ').find('button:contains("Add/Delete Another CPG")').addClass('subButton');
                                                                        $(".cpgAddBtn").show();
					},
					"Cancel": function() {
									$(this).dialog("close");
					}
				},
				open: function() {
					setDialogDayNightMode($(this));
					$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
					$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('completeButton');
					$('.ui-dialog-buttonpane').find('button:contains("Continue")').addClass('completeButton');
					$('.ui-dialog-buttonpane').find('button:contains("Return to Main")').addClass('returnToMainButton');			 
					$('.ui-dialog-buttonpane').find('button:contains("More Changes")').addClass('moreChangesButton');			 
					$('.ui-dialog-buttonpane').find('button:contains("Add/Delete Another CPG")').addClass('subButton');			 
				}
			});

			$("#addSubButton").click(function()
			{                                
				$(":button:contains('Continue')").hide();
				$(":button:contains('More Changes')").hide();
				$(":button:contains('Return To Main')").hide();
				$(":button:contains('Add/Delete Another CPG')").hide().addClass('subButton');
				$("#addGroupCallPickup").show();
				$(".cpgAddBtn").hide();
				$(".cpgAddBtnCpg").show();
				$(".ui-dialog-titlebar-close", $("#dialogBox").parentNode).show().addClass('subButton');
				$(".ui-dialog-buttonpane", $("#dialogBox").parentNode).show();
				var dataToSend = $("form#groupCallPickupAdd").serializeArray();
				
				$.ajax({
					type: "POST",
					url: "groupCallPickup/checkData.php",
					data: dataToSend,
					success: function(result) {
						
						$("#dialogBox").dialog("open");
						$("#dialogBox").dialog("option", "title", "Confirm Setting");
						if (result.slice(0, 1) == "1")
						{
							$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
						}
						else
						{                                                         
							$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
						}
						$(":button:contains('Complete')").show().addClass('subButton');
						$(":button:contains('Cancel')").show().addClass('cancelButton');
						$("#dialogBox").html(result.slice(1));							

					}
				});
			});
			
			$(document).on("click", "#deleteGroupCallPickupInstance", function()
			{
				var el = $(this);
				var instanceId = $("#listGroupCallPickup").val();

				$("#dialogBox").dialog("open");
				$(":button:contains('Cancel')").show().addClass('cancelButton');
				$(":button:contains('Continue')").show().addClass('subButton');
				$('.dialogClass ').find('button:contains("Continue")').addClass('subButton');

				$(":button:contains('Complete')").hide();
				$(":button:contains('More Changes')").hide();
				$(":button:contains('Return To Main')").hide();
				$(":button:contains('Add/Delete Another CPG')").hide();

				if(instanceId == "") {
					$(":button:contains('Continue')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
					$("#dialogBox").html('<label class="labelTextGrey ceterText">No any Call Pickup Group selected</label>');
					return false;

				}
				else if(instanceId || instanceId != "")
				{	$(":button:contains('Continue')").removeAttr("disabled", "disabled").removeClass("ui-state-disabled");
					$("#dialogBox").html("<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">");
					$("#dialogBox").dialog("option", "title", "Processing");
					$(".ui-dialog-titlebar-close", $("#dialogBox").parentNode).hide();
					$(".ui-dialog-buttonpane", $("#dialogBox").parentNode).hide();
				
					$.ajax({
						type: "POST",
						url: "groupCallPickup/do.php",
						data: {module:"delete", name : instanceId},
						success: function(result) {

							$("#dialogBox").html("<label class='ceterText'>Deleting Group Call Pick Instance <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></label>");
							if (result.trim() == "UsersExist" || result.trim() == "UsersNotExist")
							{
								if(result.trim() == "UsersExist"){
									$("#dialogBox").html("<label class='labelTextGrey ceterText'>Are you sure you want to un-assign all users from <b>" + instanceId + "</b> Call Pickup Group and delete <b>" + instanceId + "</b> Call Pickup Group ? </label>");
								}
								else if(result.trim() == "UsersNotExist"){
									$("#dialogBox").html("<label class='labelTextGrey ceterText'>Are you sure you want to delete <b>" + instanceId + "</b> Call Pickup Group ?</label>");

								}
								continueUserBuild = 0;
// 								$("#dialogBox").append(returnLink);
								$(".ui-dialog-buttonset", $("#dialogBox").parentNode).show();
								$(".ui-dialog-buttonpane", $("#dialogBox").parentNode).show();
							}
							else if (result.trim() == "Done")
							{
								showListGroup();
								setTimeout(function(){
									$("#dialogBox").dialog("close");

									/*$("#dialogBox").dialog("option", "title", "Request Complete");
									$("#dialogBox").html("Deleted Group Call Pick Instance....<img src=\"images/greenCheck.png\" width=\"15px\" height=\"15px\">");
									$(".ui-dialog-titlebar-close", $("#dialogBox").parentNode).show();
									$(":button:contains('Complete')").hide();
									$(":button:contains('Continue')").hide();
									$(".ui-dialog-buttonpane", $("#dialog").parentNode).show();		*/
								}, 4000);
							}
							else
							{	continueUserBuild = 0;
								$("#dialogBox").html(result);
								$("#dialogBox").append(returnLink);
								$(".ui-dialog-titlebar-close", $("#dialogBox").parentNode).show();
								$(".ui-dialog-buttonpane", $("#dialogBox").parentNode).hide();
							}
						}
					});
				}
			});
			
			var showListGroup = function(){

				$.ajax({
					type: "POST",
					url: "groupCallPickup/do.php",
					data: { module: "list"},
					success: function(result) {
						$("#showListGroupCallPickup").html(result);
						 $(".register-submit").show();
						 $(".cpgAddBtn").show();
					}
				});
			};
			showListGroup();
	});
</script>

<!-- <<<<<<< HEAD | Commented by Anshu @ 15 Feb 2018 --> 
<h2 class="adminUserText">Group Call Pickup</h2>
 
<div id = "cpgList">
	
	<form action="" class="groupCallPick">
	 <div id="showListGroupCallPickup"></div>
          
         
    			 <div class="col-md-1 addcpgButtonTextAlign cpgAddBtn" style="" name="newGroup" id="newGroup" >
               	<img src="images/icons/add_admin_rest.png" data-alt-src="images/icons/add_admin_over.png" />
               	<br><span>Add<br>Call Pickup<br>Group</span></div></div>
        
        
         
         
         
         
         
         <!--  Code commented by Anshu @ 15 Feb 2018
	 <div class='row formspace' style="float:right; width:50%;">
			<div style="width: 21%; float: right">
				<p class="register-submit" style="margin-top: -40px; float:right;">
					<input name="newGroup" id="newGroup" value="Add" type="button"
						class="small">
				</p>
			</div>
        </div>
        -->         
	</form>
	
	<form action="#" method="POST" id="groupCallPickupAdd" class="groupCallPick">
		<div id="addGroupCallPickup" class="col-md-12">
			<!--div class="subBanner">Add Group Call Pickup</div-->
			 <div class="row">
			
				<div class="col-md-11">
					<div class="form-group">
						<label class="labelText">Group Call Pickup Name:</label><span class="required">*</span><br/>
						<input type="text" name="name" id="name" class="from-control" size="35" maxlength="30" style="width:100% !important" />
					</div>
				</div>
    			 
    			 	
               <!-- backbutton cpgs -->		
               		
               	<div class="col-md-1 deleteUsers" style="" name="backButton" id="backButton" >
               	<img src="images/icons/delete_selected_users_icon.png" data-alt-src="images/icons/delete_selected_users_icon_over.png" />
               	<br><span>Go Back<br>Call Pickup<br>Group</span></div>
                 <div class="col-md-1 newAddcpgButtonTextAlign" style="" name="subButton" id="addSubButton">
                 Add</div>
               	 
    		</div>	
		</div>
	</form>
</div>
<div id="dialogBox" class="dialogClass"></div>
<div id="result"></div>
