<?php
require_once ("/var/www/html/Express/config.php");
checkLogin ();

?>
<script>
	$(function() {
		var supportEmail = "<?php echo $supportEmailSupport; ?>";
		var showList = function(){
				var settings = {
					type: "POST",
					url: "groupCallPickup/do.php",
					data: { module: "list"}
				}
					
				$.ajax(settings).done(function (result){														
					$("#showListGroupCallPickup").html(result);														
					$("#addGroupCallPickup").hide();
					$("#showListGroupCallPickup").show();
					$(".register-submit").show();
					$("#newGroup").show();
					// setTimeout(function(){
								// $("#dialogBox").dialog("close");
					// }, 4000);
				});
			};
			showList();
			<?php if(isset($_POST["name"]) && !empty($_POST["name"])){?>

				var name = "<?php echo $_POST["name"]; ?>";
				$("#dialogBox").html("");

				var continueUserBuild = 1;
				var hasDeviceName = "N";
					// Add the User
					//-------------
					if (continueUserBuild == 1)
					{
						$("#dialogBox").append("<div id=\"buildGroupCallPickup\" class='labelTextGrey ceterText'>Building Group Call Pick Instance <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\" class=\"center-block\"style='margin-top:25px'></div>");
							var settings = {
								type: "POST",
								url: "groupCallPickup/do.php",
								data: { module: "add", name: name }
							}

							$.ajax(settings).done(function(result)
							{                                                               
                                                               //var res = result.split("<br />"); //Code Added by Anshu @ 15 Feb 2018 to remove addiontional javascript that is exist on the do.php
							       //var result = res[1];
                                                               if (result.trim() !== "Done")
								{
									continueUserBuild = 0;
									if(supportEmail.length != 0){
                                                                                //$("#buildGroupCallPickup").html("There was an error building instance. Please <a href=\"mailto:<?php echo $supportEmailSupport; ?>?body=" + result + "\">Click here</a> to send email to support.");
                                                                                $("#buildGroupCallPickup").html(result+"<br/>");
                                                                                $("#buildGroupCallPickup").append(returnLink);
                                                                        }
                                                                        else
                                                                        {
                                                                                $("#buildGroupCallPickup").html(result);
                                                                        }

                                                                                                $("#showListGroupCallPickup").hide();
                                                                                                $("#newGroup").hide();
                                                                                                $("#subButton").show();
                                                                                                $("#addGroupCallPickup").show();
								 }
								else
								{
									$.when($.ajax(showList())).then(function(){
										$("#buildGroupCallPickup").html("<div class='ceterText'><b>" + name + " </b> Call Pickup Group has been added </div>");
										//$("#dialogBox").append(returnLink); //Commented By Anshu 
										$(".ui-dialog-buttonpane", this.parentNode).show();
										$(":button:contains('Return To Main')").show().addClass('cancelButton'); //Added By Anshu 
                                                                                $(":button:contains('Add/Delete Another CPG')").show().addClass('subButton'); //Added By Anshu 
                                                                                
										$(":button:contains('Complete')").hide();
									});



								}
							});

					}//end Add the User block
			<?php }?>
	});//end of function
</script>
<div id="result"></div>
