<?php
 $softwareRevisionLogin = file("/var/www/revision.txt", FILE_IGNORE_NEW_LINES);

if (isset($_SESSION["loginMsg"])) {
	$errorMessage = $_SESSION["loginMsg"];
	unset($_SESSION["loginMsg"]);
} else {
	$errorMessage = "&nbsp;";
}

function scallLoginLogo($width, $height, $filename) {
    list($width_orig, $height_orig) = getimagesize($filename);
    if($height_orig > 130) {
        $scallFactor = $height_orig/$height;
        $newWidth = $width_orig / $scallFactor;
        $newHeight = $height;
    } else {
        $newHeight = $height_orig;
        $newWidth = $width_orig;
    }
    return array('imgWidth' => $newWidth, 'imgHeight' => $newHeight);
}

?>
<style>
	.mainHeader{
		display:none;
	}
	.blueSeparator{
		z-index:0;
	}
	.spAddressDiv {
		display:none;
	}
	.loginTextMessageAlign{display: table;height: 43px;/*padding-bottom: 10px;*/}
</style>
<div class="login-bg">
	<div class="fadeInUp" id="bodyForm" style="width: 470px">
		<span style="margin-right: 55px"><img src="images/icons/express_nuovo_logo_login2.png" style="width:400px; max-height:250px;"/></span>
                <div style="text-align:center">
                    &nbsp;
                    <?php 
                    if($customerLogoOnLoginPage == "true" && $brandingLoginLogo != "") {
                		echo "<br/><br/>";
                		$logoFile = $brandingLoginLogoWebPath ? $brandingLoginLogoWebPath : "";
                		$imageDetail = scallLoginLogo($width = 300, $height = 130, "/var/www".$logoFile);
                		$imgWidth = $imageDetail['imgWidth'];
                		$imgHeight = $imageDetail['imgHeight'];
                		echo "<img src=\"$logoFile?". time(). "\" title=\"logo\" class=\"RedirectNavHeader\" style=\"width:".$imgWidth."px;height:".$imgHeight."px;padding-bottom: 0;\"/>";
                		//echo "<img src=\"$logoFile?". time(). "\" title=\"logo\" class=\"RedirectNavHeader\" style=\"width:148px;height:40px;padding-bottom: 0;\"/>";
                    }
	?>

                     <div class="loginTextMessageAlign">
                            <div style="display: table-cell; vertical-align: bottom;"><div style="clear:both;width: 473px;text-align:center;color:red;<?php echo ($errorMessage ? "" : "display:none;")?>" id="error_msg"><?php echo $errorMessage; ?></div></div>

                     </div>
                       </div>
		<div style="width:100%;text-align:center;">
			<form name="login" id="login" method="POST">
				<input placeholder="Username" type="text" name="userName" id="userName" class="inptText" />
				<input placeholder="Password" type="password" name="password" id="password" class="inptPass" />
				<div class="continue_session_Div">
					<input type="checkbox" class="checkbox-lg" id="continue_session" name="continue_session" value="1"  <?php echo $enableContinuePreviousSession == "true" ? "checked" : ""?>/>
					<label for="continue_session"><span></span></label>
					<span class="continue_session_Txt">Continue Previous Session</span>
				</div>
				<div class="loginSubmitDiv form-group">
					<input type="submit" name="loginSubmit" id="loginSubmit" value="Login">
				</div>
				<div class="forgotPassDiv">
					<a href="forgot.php" class="m-t-mini">
						<small>Forgot password?</small>
					</a>
				</div>
                                <br/>
                                <br/>
                              
                                 <!--show express rel version -->
                                <div class="forgotPassDiv">
					<small>Rel. <?php echo $softwareRevisionLogin[0]; ?></small>
                                </div>
			</form>
			<div class="clr"></div>
			
	</div>

	</div>
	<div class="copyright">
			Copyright &copy; AveriStar All rights reserved.
			</div>
</div>
