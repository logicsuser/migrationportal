<?php
	session_start();
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>	
	<title>Express Admin Portal</title>
    <script src="/Express/js/jquery-ui/js/jquery-1.10.2.js"></script>
        <link rel="stylesheet" type="text/css" href="/Express/css/css.php" media="screen" />	 
	<link rel="stylesheet" type="text/css" href="/Express/css/reset.css" media="screen" /> 
	<link rel="stylesheet" type="text/css" href="/Express/css/responsive.gs.24col.css" media="screen" />	 
        <script src="/Express/js/bootstrap/bootstrap.min.js"></script>
 	<link rel="stylesheet" type="text/css" href="/Express/css/bootstrap/bootstrap.min.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/Express/css/custom.css" media="screen" />  
<script>       
	$(function() {     
                
                //$(".newBody").css("background-image", "url('/Express/images/icons/login_bg.png')");
                $(".tmpImage").hide();
                $(".main_body").show();
                
		$("#helpUrl").hide();                
		function authorize(event)
		{
			var dataToSend = "json=1&" + $("#login").serialize();

			event.preventDefault();
			 $("#error_msg").hide();
			$("#loginSubmit").val("Please Wait ...");
			$("#loginSubmit").prop("disabled", true);

			$.ajax({
				type: "POST",
				url: "auth.php",
				data: dataToSend,
                dataType: 'json',
				success: function(result) {
                    if (result.success) {
                        //$("#mainBody").html(result);
                        window.location.replace("./twoFactorAuthentication.php");
                    } else {
                        if(result.message) {
                            $("#error_msg").html(result.message);
                        } else {
                            $("#error_msg").html("Username/Password not correct.");
                        }
                        $("#error_msg").show();


                        $("#loginSubmit").val("Login");
                        $("#loginSubmit").prop("disabled", false);
                    }
				}
			});
		}

		$("#loginSubmit").click(function(event)
		{
			authorize(event);
		});

		$("input").keypress(function(event)
		{
			if (event.which == 13)
			{
				authorize(event);
			}
		});

		$("#userName").focus();
	});
</script>
 <style> 
     .newBody{
         background-color: black !important;  
 }

    
 </style>
 <?php 
    
	if (isset($_SESSION["loginMsg"]))
	{
		$lMsg = $_SESSION["loginMsg"];
	}

	session_destroy();
	session_start();
	if (isset($lMsg))
	{
		$_SESSION["loginMsg"] = $lMsg;
	}

	//get settings from .ini file
	$settings = parse_ini_file("/var/www/adminPortal.ini", TRUE);

	require_once("config.php");

	$users = get_all_super_admin_users();
	if (count(get_all_super_admin_users()) <= 0) {
		if(file_exists("initial_setup.php")) {
			$_SESSION['INITIAL_SETUP'] = 1;
			header("Location: ./initial_setup.php");
			exit;
		}
	}
	//require_once("header.php");
 ?>
</head>
<!-- End code -->
<body class="newBody">
    <div class="tmpImage" style="display: block;">&nbsp;</div>
    <div class="main_body" style="display: none;">
    <div class="leftMenu"></div>
    <div id="mainBody">
            <?php
                    include("login.php");
            ?>
    </div>
    </div>
</body>
</html>