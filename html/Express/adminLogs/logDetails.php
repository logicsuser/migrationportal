<?php
/**
 * Created by Dipin Krishna.
 * Date: 7/22/18
 * Time: 9:05 PM
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
checkLogin();

require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");

try {

	$logID = isset($_GET['logID']) ? trim($_GET['logID']) : 0;

	$adminLogs = new adminLogs();

	if($logID && $details = $adminLogs->getLogDetails($logID)) {

		$eventTypes = $adminLogs->event_types;

		include($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/views/logDetails.php");

	} else {

		echo "Sorry, log not found.";
	}

} catch (Exception $exception) {
	error_log($exception->getMessage());
	echo "Sorry, not available at this moment.";
}
exit;