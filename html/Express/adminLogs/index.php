<?php
/**
 * Created by Dipin Krishna.
 * Date: 7/20/18
 * Time: 8:25 PM
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
checkLogin();

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");

require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");

try {

	$adminLogs = new adminLogs();

	$eventTypes = $adminLogs->event_types;
	$adminTypes = $adminLogs->admin_types;

	$allAdminUsers = array();
	$adminUsers = get_all_admin_users();

	foreach ($adminUsers as $adminUser) {
		if($adminUser->superUser != 2) {
			$allAdminUsers[] = array(
				'label' => " {$adminUser->userName} - {$adminUser->full_name}",
				'value' => " {$adminUser->userName} - {$adminUser->full_name}",
				'userID' => $adminUser->id
			);
		}
	}

	include($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/views/index.php");

} catch (Exception $exception) {
	error_log($exception->getMessage());
	echo "Sorry, not available at this moment.";
}
exit;
?>