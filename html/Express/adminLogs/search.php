<?php
/**
 * Created by Dipin Krishna.
 * Date: 7/20/18
 * Time: 9:30 PM
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
checkLogin();

require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");

try {

	$eventType = isset($_POST['event_type']) ? trim($_POST['event_type']) : "ALL";
	$adminType = isset($_POST['admin_type']) ? trim($_POST['admin_type']) : "ALL";
	$userID = isset($_POST['userID']) ? trim($_POST['userID']) : 0;


	$startDate = isset($_POST['start_date']) ?  strtotime(trim($_POST['start_date'])) : null;
	$endDate = isset($_POST['end_date']) ?  strtotime(trim($_POST['end_date'])) : null;

	$adminLogs = new adminLogs();

	$filters = array();
	if($startDate) {
		$filters[] = array('DATE(adminLogs.created_on)', '>=', date("Y-m-d", $startDate));
	}
	if($endDate) {
		$filters[] = array('DATE(adminLogs.created_on)', '<=', date("Y-m-d", $endDate));
	}
	if($eventType != 'ALL') {
		$filters[] = array('adminLogs.eventType', '=', $eventType);
	}
	if($adminType != 'ALL') {
		if($adminType == 'SUPER_USER') {
			$filters[] = array('adminUser.superUser', '>', 0);
		} else {
			$filters[] = array('adminUser.adminTypeIndex', '=', $adminType);
		}
	}
	if($userID > 0) {
		$filters[] = array('adminLogs.adminUserID', '=', $userID);
	}

	$searchResults = $adminLogs->searchAdminLogs($filters, "adminLogs.created_on desc");

	$eventTypes = $adminLogs->event_types;
	$adminTypes = $adminLogs->admin_types;

	$eventTypeName = $eventTypes[$eventType];
	$adminTypeName = $adminTypes[$adminType];
	$startDateFormatted = date('M j, Y', $startDate);
	$endDateFormatted = date('M j, Y', $endDate);

	$downloadCSVParams = json_encode($_POST);

	include($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/views/results.php");

} catch (Exception $exception) {
	error_log($exception->getMessage());
	echo "Sorry, not available at this moment.";
}
exit;
?>