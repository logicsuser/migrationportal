<?php
/**
 * Created by Dipin Krishna.
 * Date: 7/23/18
 * Time: 1:02 AM
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
checkLogin();

require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");

try {

	$params = json_decode(base64_decode($_GET['params']), TRUE);

	$eventType = isset($params['event_type']) ? trim($params['event_type']) : "ALL";
	$adminType = isset($params['admin_type']) ? trim($params['admin_type']) : "ALL";
	$userID = isset($params['userID']) ? trim($params['userID']) : 0;

	$startDate = isset($params['start_date']) ?  strtotime(trim($params['start_date'])) : null;
	$endDate = isset($params['end_date']) ?  strtotime(trim($params['end_date'])) : null;

	$adminLogs = new adminLogs();

	$filters = array();
	if($startDate) {
		$filters[] = array('DATE(adminLogs.created_on)', '>=', date("Y-m-d", $startDate));
	}
	if($endDate) {
		$filters[] = array('DATE(adminLogs.created_on)', '<=', date("Y-m-d", $endDate));
	}
	if($eventType != 'ALL') {
		$filters[] = array('adminLogs.eventType', '=', $eventType);
	}
	if($adminType != 'ALL') {
		if($adminType == 'SUPER_USER') {
			$filters[] = array('adminUser.superUser', '>', 0);
		} else {
			$filters[] = array('adminUser.adminTypeIndex', '=', $adminType);
		}
	}
	if($userID > 0) {
		$filters[] = array('adminLogs.adminUserID', '=', $userID);
	}

	$searchResults = $adminLogs->searchAdminLogs($filters, "adminLogs.created_on desc");

	$eventTypes = $adminLogs->event_types;

	$fileName = "adminLogs_" . rand(1092918, 9281716);
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment;filename=\"" . $fileName . ".csv\"");
	header("Content-Transfer-Encoding: binary");

	$fp = fopen("php://output", "a");
	fputcsv($fp,  array('Date', 'Event', 'Username', 'Name'));

	foreach ($searchResults as $row)
	{
		fputcsv($fp,  array($row["created_on"], $eventTypes[$row["eventType"]], $row["adminUserName"], $row["adminName"]));
	}
	fclose($fp);

} catch (Exception $exception) {
	error_log($exception->getMessage());
	echo "Sorry, not available at this moment.";
}
exit;

?>