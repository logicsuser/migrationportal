<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();
?>
<script>
	$(function() {
		$("#module").html("> Contact Support");
		$("#endUserId").html("");

		$("#dialogTicket").dialog({
			autoOpen: false,
			width: 800,
			modal: true,
			position: { my: "top", at: "top" },
			resizable: false,
			closeOnEscape: false
		});

		$("#subButton").click(function()
		{
			var dataToSend = $("form#webTicket").serializeArray();
			$.ajax({
				type: "POST",
				url: "webTicket/postTicket.php",
				data: dataToSend,
				success: function(result)
				{
					$("#dialogTicket").dialog("open");
					$(".ui-dialog-titlebar-close", this.parentNode).hide();
					$(".ui-dialog-buttonpane", this.parentNode).hide();
					$("#dialogTicket").html(result);
					$("#dialogTicket").append(returnLink);
				}
			});
		});
	});
</script>
<div class="subBanner">Web Ticket</div>
<div class="vertSpacer">&nbsp;</div>

<form method="POST" name="webTicket" id="webTicket">
	<?php
		$date = date("Y-m-d h:i:s");
		$ticketTypes = array("MAC", "Trouble", "Billing Inquiry", "Technical Information Request");
	?>
	<input type="hidden" name="today" id="today" value="<?php echo $date; ?>" />
	<input type="hidden" name="groupId" id="groupId" value="<?php echo $_SESSION["groupId"]; ?>" />
	<input type="hidden" name="userName" id="userName" value="<?php echo $_SESSION["loggedInUserName"]; ?>" />
	<div id="ticket" style="clear:both;width:850px; margin: 0 auto;">
		<div class="leftDesc2"><label for="today">Today's Date</label></div>
		<div class="inputText"><?php echo $date; ?></div>
		<div class="quarterDesc"><label for="groupId">Group Name</label></div>
		<div class="inputText"><?php echo $_SESSION["groupId"]; ?></div>
		<div class="leftDesc2"><label for="userName">Username</label></div>
		<div class="inputText"><?php echo $_SESSION["loggedInUserName"]; ?></div>
		<div class="quarterDesc"><label for="fromAddress">From Address</label></div>
		<div class="inputText"><?php echo $_SESSION["emailAddress"]; ?></div>
		<div class="leftDesc2"><label for="ticketType">Ticket Type</label></div>
		<div class="inputText">
			<select name="ticketType" id="ticketType">
				<option value=""></option>
				<?php
					foreach ($ticketTypes as $value)
					{
						echo "<option value=\"" . $value . "\">" . $value . "</option>";
					}
				?>
			</select>
		</div>
		<div class="quarterDesc"><label for="desc">Ticket Description</label></div>
		<div class="inputText"><input type="text" name="desc" id="desc" size="65" maxlength="255"></div>
		<div class="leftDesc2"><label for="details">Ticket Details</label></div>
		<div class="inputText" style="width:75%;"><textarea name="details" id="details" rows="10" cols="90"></textarea></div>
		<div class="vertSpacer">&nbsp;</div>

		<div class="centerDesc"><input type="button" id="subButton" value="Submit Request"></div>
	</div>
</form>
<div id="dialogTicket" class="dialogClass"></div>
