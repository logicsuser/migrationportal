<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();
	require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
	$expProvDB = new ExpressProvLogsDB();
	
	$to = $supportEmailSupport;
	$subject = $_POST["userName"] . " from " . $_POST["groupId"] . " has submitted a request";
	$message = "<br>
		Date: " . $_POST["today"] . "<br>
		Group: " . $_POST["groupId"] . "<br>
		User: " . $_POST["userName"] . " (" . $_SESSION["emailAddress"] . ")<br>
		<br>
		Type: " . $_POST["ticketType"] . "<br>
		Description: " . $_POST["desc"] . "<br>
		<br>
		Details: " . $_POST["details"] . "<br>";

	$headers = "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
	$headers .= "From: Support Web<" . $supportEmailSupport . ">\r\n";
	$headers .= "Reply-To: " . $supportEmailSupport . "\r\n";
	$headers .= "Cc: " . $_SESSION["emailAddress"] . "\r\n";
//	$headers .= "Bcc: jthorpe@fusionconnect.com\r\n";
	mail($to, $subject, $message, $headers);

	$date = date("Y-m-d H:i:s");
	$query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
	$query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', 'Support Email', '" . $_SESSION["groupId"] . "', '" . $_POST["ticketType"] . "')";
	
	$sth = $expProvDB->expressProvLogsDb->query($query);
	$lastId = $expProvDB->expressProvLogsDb->lastInsertId();
	
	$insert = "INSERT into supportEmailChanges (id, serviceId, description, details)";
	$insert .= " VALUES ('" . $lastId . "', '" . $_POST["ticketType"] . "', '" . addslashes($_POST["desc"]) . "', '" . addslashes($_POST["details"]) . "')";
	$sth = $expProvDB->expressProvLogsDb->query($insert);
?>
The following message has been sent to Support.<br><br>
<div style="text-align:left;width:850px; margin: 0 auto;">
	<?php
		echo $message;
	?>
</div>
