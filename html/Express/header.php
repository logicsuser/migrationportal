<?php 
//error_reporting(E_ALL ^ E_NOTICE); 
//Code added @ 19 Dec 2018
// print_r($_SESSION['server_connection_detail']); exit;
function getUserInfo($srviceProviderId, $groupId){
    global $client, $sessionid;
    if( ! isset($client) ) {
        return;
    }
    $tmpGroupInfoData = array();
    $xmlinput = xmlHeader($sessionid, "GroupGetRequest14sp7");
    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($srviceProviderId) . "</serviceProviderId>";
    $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
    $xmlinput .= xmlFooter();
    $response = $client->processOCIMessage(array("in0" => $xmlinput));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

    $tmpGroupInfoData["defaultDomain"] = strval($xml->command->defaultDomain);
    $tmpGroupInfoData["groupName"] = strval($xml->command->groupName);
    $tmpGroupInfoData["addressLine1"] = strval($xml->command->address->addressLine1);
    $tmpGroupInfoData["addressLine2"] = strval($xml->command->address->addressLine2);
    $tmpGroupInfoData["city"] = strval($xml->command->address->city);
    $tmpGroupInfoData["stateOrProvince"] = strval($xml->command->address->stateOrProvince);
    $tmpGroupInfoData["zipOrPostalCode"] = strval($xml->command->address->zipOrPostalCode);
    $tmpGroupInfoData["country"] = strval($xml->command->address->country);
    $tmpGroupInfoData["timeZone"] = strval($xml->command->timeZone);    
    return $tmpGroupInfoData;
}
//End code
 

/*enterprise permission for superuser and ent user */

$usersLogsArr                         = explode("_", $_SESSION["permissions"]["users"]);
$userPermission                       = $usersLogsArr[0];

$changeLogLogsArr                     = explode("_", $_SESSION["permissions"]["changeLog"]);
$changeLogLogPermission               = $changeLogLogsArr[0];

$phoneProfilesLogsArr                 = explode("_", $_SESSION["permissions"]["phoneProfiles"]);
$phoneProfilesLogPermission           = $phoneProfilesLogsArr[0];

$broadWorksLicenseReportLogsArr       = explode("_", $_SESSION["permissions"]["broadWorksLicenseReport"]);
$broadWorksLicenseReportLogPermission = $broadWorksLicenseReportLogsArr[0];

$cdrLogsArr                           = explode("_", $_SESSION["permissions"]["cdr"]);
$cdrLogPermission                     = $cdrLogsArr[0];

$entServicePacksLogsArr               = explode("_", $_SESSION["permissions"]["entServicePacks"]);
$entServicePacksLogPermission         = $entServicePacksLogsArr[0];

$entAnnouncementsLogsArr              = explode("_", $_SESSION["permissions"]["entAnnouncements"]);
$entAnnouncementsLogPermission        = $entAnnouncementsLogsArr[0];

$devicesLogsArr                       = explode("_", $_SESSION["permissions"]["devices"]);
$devicesLogPermission                 = $devicesLogsArr[0];


//start code for Group Related Permissions 
$basicInformationLogsArr              = explode("_", $_SESSION["permissions"]["basicInformation"]);
$basicInformationLogPermission        = $basicInformationLogsArr[0];
$basicInformationModPermission        = $basicInformationLogsArr[1];

$callProcessingPoliciesLogsArr        = explode("_", $_SESSION["permissions"]["callProcessingPolicies"]);
$callProcessingPoliciesLogPermission  = $callProcessingPoliciesLogsArr[0];
$callProcessingPoliciesModPermission  = $callProcessingPoliciesLogsArr[1];

$dnsLogsArr                           = explode("_", $_SESSION["permissions"]["DNs"]);
$dnsLogPermission                     = $dnsLogsArr[0];
$dnsModPermission                     = $dnsLogsArr[1];

$domainsLogsArr                       = explode("_", $_SESSION["permissions"]["domains"]);
$domainsLogPermission                 = $domainsLogsArr[0];   
$domainsModPermission                 = $domainsLogsArr[1];

$networkClassesofServiceLogsArr       = explode("_", $_SESSION["permissions"]["networkClassesofService"]);
$networkClassesofServiceLogPermission = $networkClassesofServiceLogsArr[0];
$networkClassesofServiceModPermission = $networkClassesofServiceLogsArr[0];

$outgoingCallingPlanLogsArr           = explode("_", $_SESSION["permissions"]["outgoingCallingPlan"]);
$outgoingCallingPlanLogPermission     = $outgoingCallingPlanLogsArr[0];
$outgoingCallingPlanModPermission     = $outgoingCallingPlanLogsArr[1];

$securityDomainsLogsArr               = explode("_", $_SESSION["permissions"]["securityDomains"]);
$securityDomainsLogPermission         = $securityDomainsLogsArr[0];
$securityDomainsModPermission         = $securityDomainsLogsArr[1];

$servicesLogsArr                      = explode("_", $_SESSION["permissions"]["services"]);
$servicesLogPermission                = $servicesLogsArr[0];
$servicesModPermission                = $servicesLogsArr[1];

$voicePortalServiceLogsArr            = explode("_", $_SESSION["permissions"]["voicePortalService"]);
$voicePortalServiceLogPermission      = $voicePortalServiceLogsArr[0];
$voicePortalServiceModPermission      = $voicePortalServiceLogsArr[1];
//End code for Group Related Permissions


$adminLogsArr                         = explode("_", $_SESSION["permissions"]["adminLogs"]);
$adminLogPermission                   = $adminPrmnsArr[0];

$administratorsServiceLogsArr         = explode("_", $_SESSION["permissions"]["administrators"]);
$administratorsLogPermission          = $administratorsServiceLogsArr[0];

$adminLogsServiceLogsArr              = explode("_", $_SESSION["permissions"]["adminLogs"]);
$adminLogsLogPermission               = $adminLogsServiceLogsArr[0];
//die();

//Manage Group Level Permissions Based on Enterprise Admin Permission
//Code added @ 07 March 2019
function manageGroupLevelPermissionBasedOnEnterpriseAdminPermission(){
    
            $tempUserPermissionsArr             = explode("_", $_SESSION["permissions"]["groupAdminUserPermission"]);
            $groupAdminUserPermission           = $tempUserPermissionsArr[0];
            $groupAdminUserMod                  = $tempUserPermissionsArr[1];
            $groupAdminUserAddDel               = $tempUserPermissionsArr[2];
            
            $tempDevicePermissionsArr           = explode("_", $_SESSION["permissions"]["groupAdminDevicesPermission"]);
            $groupAdminDevicesPrmson            = $tempDevicePermissionsArr[0];
            $groupAdminDevicesMod               = $tempDevicePermissionsArr[1];
            $groupAdminDevicesAddDel            = $tempDevicePermissionsArr[2];
            
            $tempServicesPermissionsArr         = explode("_", $_SESSION["permissions"]["groupAdminServicesPermission"]);
            $groupAdminServicesPrmson           = $tempServicesPermissionsArr[0];
            
            $tempSupervisoryPermissionsArr      = explode("_", $_SESSION["permissions"]["groupAdminSupervisoryPermission"]);
            $groupAdminSupervisoryPrmson        = $tempSupervisoryPermissionsArr[0];
            
            $entAnnouncementsLogsArr            = explode("_", $_SESSION["permissions"]["entAnnouncements"]);
            $entAnnouncementsLogPermission      = $entAnnouncementsLogsArr[0];
            $entAnnouncementsAddDelPermission   = $entAnnouncementsLogsArr[2];
            
            $cdrLogsArr                         = explode("_", $_SESSION["permissions"]["cdr"]);
            $cdrLogPermission                   = $cdrLogsArr[0];
            
            $changeLogLogsArr                   = explode("_", $_SESSION["permissions"]["changeLog"]);
            $changeLogLogPermission             = $changeLogLogsArr[0];
            
            
            //Setting Group Level Users Permissions         
            if($groupAdminUserPermission == "1" && $groupAdminUserAddDel == "yes"){
                $_SESSION["permissions"]["addusers"]                        = "1";           //Users  entAddUsers
                $_SESSION["permissions"]["deleteUsers"]                     = "1";           //Users  entDeleteUsers
                $_SESSION["permissions"]["expressSheets"]                   = "1";           //Users  userExpressSheets
                $_SESSION["permissions"]["groupWideUserModify"]             = "1";           //Users  groupWideUsersModify
            }
            
            if($groupAdminUserPermission == "1" && $groupAdminUserMod == "yes"){               
                $_SESSION["permissions"]["basicUser"]                       = "1";           //Users  basicInfoUser  		    
                $_SESSION["permissions"]["modifyUsers"]                     = "1";           //Users  entModifyUsers
                $_SESSION["permissions"]["userPolicyCallLimits"]            = "1";           //Users  callLimitsPolicy
                $_SESSION["permissions"]["userPolicyCLID"]                  = "1";           //Users  callingLineIDPolicy
                $_SESSION["permissions"]["userPolicyEmergencyCalls"]        = "1";           //Users  emergencyCallsPolicy
                $_SESSION["permissions"]["userPolicyIncomingCLID"]          =  "1";          //Users  incomingCallerIDPolicy
                $_SESSION["permissions"]["timeZone"]                        = "1";           //Users  entTimeZone
                $_SESSION["permissions"]["userNCOS"]                        = "1";           //Users  networkClassofService
                $_SESSION["permissions"]["userCallingLineIdNumber"]         = "1";           //Users  callingLineIDPhoneNumber
                $_SESSION ["permissions"]["servicePacks"]                   = "1";           //Users  userServicePacks
                $_SESSION["permissions"]["userAddress"]                     = "1";           //Users  entUserAddress
                $_SESSION["permissions"]["activateNumber"]                  = "1";           //Users  entActivateNumber
                $_SESSION["permissions"]["ocpUser"]                         = "1";           //Users  outgoingCallingPlanUser
            }
            
            if($groupAdminUserPermission == "1"){
                $_SESSION["permissions"]["viewusers"]                       = "1";           //Users  viewusers
            }
            
            
            //Setting Group Level Devices Permissions
            if($groupAdminDevicesPrmson == "1" && $groupAdminDevicesAddDel == "yes"){
                $_SESSION["permissions"]["scaOnlyDevice"]                   = "1";             //Devices   //SCA-Only Devices                 
                $_SESSION["permissions"]["devCustomTags"]                   =  "1";            //Devices   //Custom Tags
                $_SESSION["permissions"]["deviceConfigCustomTags"]          = "1";             //Devices   //Device Config. Custom Tags
                $_SESSION["permissions"]["deviceInventory"]                 = "1";             //Devices   //Device Inventory
             }
            
            if($groupAdminDevicesPrmson == "1" && $groupAdminDevicesMod == "yes"){
                $_SESSION["permissions"]["devVdmSoftKeys"]                  = "1";             //Devices   //Device Management - Soft Keys
                $_SESSION["permissions"]["devVdmCodecs"]                    = "1";             //Devices   //Device Management - Codecs
                $_SESSION["permissions"]["devVdmFeatures"]                  = "1";             //Devices   //Device Management – Features
                $_SESSION["permissions"]["vdmLight"]                        = "1";             //Devices   //Device Management
                $_SESSION["permissions"]["vdmAdvanced"]                     = "1";             //Devices   //Advanced Device Management
                $_SESSION["permissions"]["changeDevice"]                    = "1";             //Devices   //Phone Devices 
                $_SESSION["permissions"]["clearMACAddress"]                 = "1";             //Devices   //Clear MAC Address		      
            }
            
            if($groupAdminDevicesPrmson == "1"){ 
                $_SESSION["permissions"]["deviceInfoReadOnly"]              = "1";             //Devices   //Read-Only Device Information
            }
            
            
            //Setting Group Level Services Permissions
            if($groupAdminServicesPrmson == "1"){  
                $_SESSION["permissions"]["aamod"]                           = "1";            //Services	
                $_SESSION["permissions"]["changeblf"]                       = "1";            //Services
                $_SESSION["permissions"]["modcallCenter"]                   = "1";            //Services
                $_SESSION["permissions"]["modhuntgroup"]                    = "1";            //Services            
                $_SESSION["permissions"]["modCcd"]                          = "1";            //Services   
                $_SESSION["permissions"]["modschedule"]                     = "1";            //Services   		                          
                $_SESSION["permissions"]["callPickupGroup"]                 = "1";            //Services   
                $_SESSION["permissions"]["srvSpeedDial8"]                   = "1";            //Services  
                $_SESSION["permissions"]["srvSpeedDial100"]                 = "1";            //Services  
                $_SESSION["permissions"]["ocpGroup"]                        = "1";            //Services
                $_SESSION["permissions"]["simRing"]                         = "1";            //Services                    
                $_SESSION["permissions"]["changeForward"]                   = "1";            //Services
                $_SESSION["permissions"]["changeHotel"]                     = "1";            //Services                    
                $_SESSION["permissions"]["changeSCA"]                       = "1";	      //Services	
                $_SESSION["permissions"]["voiceManagement"]                 = "1";            //Services                
            }            
            
            //Setting Group Level Services Permissions
            if($groupAdminSupervisoryPrmson == "1"){
                $_SESSION["permissions"]["sasTest"]                         = "1";              //Supervisory
                $_SESSION["permissions"]["siteReports"]                     = "1";              //Supervisory
                $_SESSION["permissions"]["viewnums"]                        = "1";              //Supervisory
                $_SESSION["permissions"]["voiceMailPasscode"]               = "1";              //Supervisory  
                $_SESSION["permissions"]["resetPasswords"]                  = "1";              //Supervisory
            }
            
            if($entAnnouncementsLogPermission == "1" && $entAnnouncementsAddDelPermission == "yes"){
                $_SESSION["permissions"]["announcements"]    = "1";              //Supervisory
            }if($cdrLogPermission == "1"){
                $_SESSION["permissions"]["viewcdrs"]         = "1";              //Supervisory
            }if($changeLogLogPermission == "1"){
                $_SESSION["permissions"]["viewChangeLog"]    = "1";              //Supervisory
            }
}
if($_SESSION['superUser'] == "3") {
    
        //Code added @ 18 March 2019
        if($basicInformationModPermission == "yes" || $callProcessingPoliciesModPermission == "yes"
           || $dnsModPermission == "yes" || $domainsModPermission == "yes"
           || $networkClassesofServiceModPermission == "yes" || $outgoingCallingPlanModPermission == "yes"
           || $securityDomainsModPermission == "yes" || $servicesModPermission == "yes" 
           || $voicePortalServiceModPermission == "yes"){
            $_SESSION["permissions"]["modifyGroup"]   = "1";
        }
        //End code
        if($basicInformationLogPermission == "1"){ 
            $_SESSION["permissions"]["groupBasicInfo"]   = "1";
        }
        
        if($basicInformationLogModPermission == "yes" || $voicePortalServiceLogModPermission == "yes"
           || $outgoingCallingPlanLogModPermission == "yes" || $networkClasesofServcLogModPermission == "yes"
           || $callProcessingPoliciesLogModPermission == "yes" || $securityDomainsLogModPermission == "yes"
           || $servicesLogModPermission == "yes" || $domainsLogModPermission == "yes" || $dnsLogAddDelPermission == "yes"){
            $_SESSION["permissions"]["modifyGroup"]      = "1";
        }
        manageGroupLevelPermissionBasedOnEnterpriseAdminPermission();
}
//End code

/* end  */


/*ex-1124 Inventory permission */
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    $invernotryPermissionLevel = (($_SESSION['superUser'] == "1")  
        || ($_SESSION["permissions"]["viewnums"] == "1")
	|| ($_SESSION["permissions"]["viewChangeLog"] == "1") 
	|| ($_SESSION["permissions"]["viewcdrs"] == "1" and ! is_null($billDB))
	|| ($_SESSION["permissions"]["sasTest"] == "1")
        || ($_SESSION["permissions"]["siteReports"] == "1")
	|| ((isset($license["vdmLite"]) && $license["vdmLite"] =="true") and $_SESSION["permissions"]["vdmLight"] == "1")
	|| ($_SESSION["permissions"]["announcements"] == "1")
	|| ($_SESSION["permissions"]["deviceInventory"] == "1")
    );


/*end Inventory Permission */
    
    
    
    //Manage Enterprise Module Permission
    //Code added @ 29 May 2019
    
    /* Basic Info Permission */
    $basicInfoEntLogsArr                       = explode("_", $_SESSION["permissions"]["entPermBasicInformation"]);
    $basicInfoEntLogPermission                 = $basicInfoEntLogsArr[0];
    $basicInfoEntLogModPermission              = $basicInfoEntLogsArr[1];
    $basicInfoEntLogAddDelPermission           = $basicInfoEntLogsArr[2];  //Done

    // Domain Permisson 
    $entPermDomainsLogArr                      = explode("_", $_SESSION["permissions"]["entPermDomains"]);
    $entPermDomainsLogPermission               = $entPermDomainsLogArr[0];
    $entPermDomainsLogModPermission            = $entPermDomainsLogArr[1];
    $entPermDomainsLogAddDelPermission         = $entPermDomainsLogArr[2];  //Need to implement Another place 

    // DNs Permission 
    $entPermDNsLogArr                          = explode("_", $_SESSION["permissions"]["entPermDNs"]);
    $entPermDNsLogPermission                   = $entPermDNsLogArr[0];
    $entPermDNsLogModPermission                = $entPermDNsLogArr[1];
    $entPermDNsLogAddDelPermission             = $entPermDNsLogArr[2];   //Done

    // BW Administrator
    $entPermBWAdminsLogArr                     = explode("_", $_SESSION["permissions"]["entPermBWAdministrators"]);
    $entPermBWAdminsLogPermission              = $entPermBWAdminsLogArr[0];
    $entPermBWAdminsLogModPermission           = $entPermBWAdminsLogArr[1];
    $entPermBWAdminsLogAddDelPermission        = $entPermBWAdminsLogArr[2];

    // Security Domains
    $entPermSecurityDomainsLogArr              = explode("_", $_SESSION["permissions"]["entPermSecurityDomains"]);
    $entPermSecurityDomainsLogPermission       = $entPermSecurityDomainsLogArr[0];
    $entPermSecurityDomainsLogModPermission    = $entPermSecurityDomainsLogArr[1];

    // Services Permission
    $entPermServicesLogArr                     = explode("_", $_SESSION["permissions"]["entPermServices"]);
    $entPermServicesLogPermission              = $entPermServicesLogArr[0];
    $entPermServicesLogModPermission           = $entPermServicesLogArr[1];

    // Call Processing Policies 
    $entPermCallPrcsPoliciesLogArr             = explode("_", $_SESSION["permissions"]["entPermCallProcessingPolicies"]);
    $entPermCallPrcsPoliciesLogPermission      = $entPermCallPrcsPoliciesLogArr[0];
    $entPermCallPrcsPoliciesLogModPermission   = $entPermCallPrcsPoliciesLogArr[1];

    // Dial Plan Policy
    $entPermDialPlanPolicyLogArr               = explode("_", $_SESSION["permissions"]["entPermDialPlanPolicy"]);
    $entPermDialPlanPolicyLogPermission        = $entPermDialPlanPolicyLogArr[0];
    $entPermDialPlanPolicyLogModPermission     = $entPermDialPlanPolicyLogArr[1];

    // Voice Messaging
    $entPermVoiceMessagingLogArr               = explode("_", $_SESSION["permissions"]["entPermVoiceMessaging"]);
    $entPermVoiceMessagingLogPermission        = $entPermVoiceMessagingLogArr[0];
    $entPermVoiceMessagingLogModPermission     = $entPermVoiceMessagingLogArr[1];

    // Voice Portal
    $entPermVoicePortalLogArr                  = explode("_", $_SESSION["permissions"]["entPermVoicePortal"]);
    $entPermVoicePortalLogPermission           = $entPermVoicePortalLogArr[0];
    $entPermVoicePortalLogModPermission        = $entPermVoicePortalLogArr[1];

    // Network Class of Services Permission
    $entPermNCOSLogArr                         = explode("_", $_SESSION["permissions"]["entPermNetworkClassesofService"]);
    $entPermNCOSLogPermission                  = $entPermNCOSLogArr[0];
    $entPermNCOSLogModPermission               = $entPermNCOSLogArr[1];


    // Routing Profile
    $entPermRoutingProfileLogArr               = explode("_", $_SESSION["permissions"]["entPermRoutingProfile"]);
    $entPermRoutingProfileLogPermission        = $entPermRoutingProfileLogArr[0];
    $entPermRoutingProfileLogModPermission     = $entPermRoutingProfileLogArr[1];

    // Passwords Rules 
    $entPermPasswordRulesLogArr                = explode("_", $_SESSION["permissions"]["entPermPasswordRules"]);
    $entPermPasswordRulesLogPermission         = $entPermPasswordRulesLogArr[0];
    $entPermPasswordRulesLogModPermission      = $entPermPasswordRulesLogArr[1];
    //End code
    
    
    $enterpriseModulePermissionLevel = (isset($license["Enterprises"]) && $license["Enterprises"] == "true" && ($basicInfoEntLogPermission == "1" || $entPermDomainsLogPermission == "1"
                                         || $entPermDNsLogPermission == "1" || $entPermBWAdminsLogPermission == "1"
                                         || $entPermSecurityDomainsLogPermission == "1" || $entPermServicesLogPermission == "1"
                                         || $entPermCallPrcsPoliciesLogPermission == "1" || $entPermDialPlanPolicyLogPermission == "1"
                                         || $entPermVoiceMessagingLogPermission == "1" || $entPermVoicePortalLogPermission == "1"
                                         || $entPermNCOSLogPermission == "1" || $entPermRoutingProfileLogPermission == "1"
                                         || $entPermPasswordRulesLogPermission == "1")
                                      );
    
    
    
    
    
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Express Admin Portal</title>
	<link rel="apple-touch-icon" sizes="57x57" href="images/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/apple-touch-icon-152x152.png">
	<link rel="icon" type="image/png" href="images/favicon-196x196.png" sizes="196x196">
	<link rel="icon" type="image/png" href="images/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="images/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="images/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="images/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#2D89EF">
	<meta name="msapplication-TileImage" content="images/mstile-144x144.png">
	<link rel="stylesheet" type="text/css" href="/Express/js/jquery-ui/css/smoothness/jquery-ui-1.10.4.custom.css?v2" />
	
	
	<link rel="stylesheet" type="text/css" href="/Express/css/css.php" media="screen" />
	<link rel="stylesheet" type="text/css" href="/Express/css/timePicker.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/Express/css/reset.css" media="screen" /> 
	<link rel="stylesheet" type="text/css" href="/Express/css/responsive.gs.24col.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/Express/css/animate.css" media="screen" />

	
    <!--     <link rel="stylesheet" type="text/css" href="/Express/css/style.css" media="screen" /> -->
        <!-- <script src="/Express/js/bootstrap/bootstrap.min.js"></script> -->
 	<link rel="stylesheet" type="text/css" href="/Express/css/bootstrap/bootstrap.min.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/Express/css/custom.css?v1" media="screen" />
	<link rel="stylesheet" type="text/css" href="/Express/css/custom_temp.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/Express/css/custom_temp_2.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/Express/css/custom_temp_9.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/Express/css/custom_temp_4.css" media="screen" />
        
        
<!-- <link rel="stylesheet" type="text/css" href="/Express/css/style.css?v1" media="screen" />-->
	<link rel="stylesheet" href="/Express/icons/styles.css">
	<link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v16">
	<script src="//use.typekit.net/xsy0zmb.js"></script>
	<script>try { Typekit.load(); } catch(e) {}</script>
	<script src="/Express/js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script src="/Express/js/jquery-ui/js/jquery-ui-1.10.4.custom.js" type="text/javascript"></script>


	
        <!--<script src="/Express/js/dataTables/js/jquery.dataTables.js"></script>
	<script src="/js/jquery.tablesorter.js" type="text/javascript"></script>	 
	<script src="/Express/js/popper.js"></script>
        -->

	<script src="/Express/js/dataTables/js/jquery.dataTables.min.js"></script>
	<script src="/Express/js/dataTables/js/jquery.dataTable.dk_express_pagination.js?v1"></script>
	<script type="text/javascript" src="/Express/js/dataTables.fixedColumns.min.js"></script>
	<script src="/js/jquery.tablesorter.js" type="text/javascript"></script>

	<script src="/Express/js/DKFilterBuilder.js?v2"></script>
        
 <!-- admin permission tooltip open  js -->
 <script src="/Express/js/popper.js"></script>
  <!-- end admin -->

	
	<!-- <script src="/Express/js/jquery-1.10.2.min.js"></script>
	<script src="/Express/js/jquery-2.1.3.min.js"></script>
	<script src="/Express/js/bootstrap.min.js"></script> -->
<style>
 .passwordExpiration{
         opacity: 1.0;
         z-index:1000;
         background:#eee !important;
  }
</style>
<style>
.dropdown {
    position: relative;
    display: inline-block;
}
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #303a47;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
	margin: 0;
	border-right: 1px solid #fff;
	border-left: 1px solid #fff;
	border-bottom: 1px solid #fff;
	margin-top: 17px;
}

.dropdown-content a {
    color: #ffffff;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #ffb200;}
.dropdown-content a:hover {background-color: #ffb200;}
.inventoryMenuTitle a:hover, .groupMenuTitle a:hover, .userMenuTitle a:hover, .adminMenuTitle a:hover{background-color:transparent !important;}

.dropdown:hover .dropdown-content {display: block;}
.logInOffDiv:hover{background-color:#303a47;}
.changeGroupOffDiv:hover{background-color:#303a47;
   float: right;
    padding-top: 14px;
    margin-right: 0;
    cursor: pointer;
    width: 55px;
    height:66px;
    text-align: center;
    
      
}

.changeGroupOffDiv{
   float: right;
    padding-top: 14px;
    margin-right: 0;
    cursor: pointer;
    width: 55px;
    height: 66px;
    text-align: center;
    
      
}

.dropdown:hover .dropbtn {/*background-color: #3e8e41;*/}
</style>

	<script type="text/javascript">
        $.fn.isInViewport = function() {
            var elementTop = $(this).offset().top;
            var elementBottom = elementTop + $(this).outerHeight();

            var viewportTop = $(window).scrollTop();
            var viewportBottom = viewportTop + $(window).height();

            return elementBottom > viewportTop && elementTop < viewportBottom;
        };
	</script>
</head>
<body class="not_dirty">
	<script>
          
              var defaults = {
			autoOpen: false,
			modal: true,
			position: { my: "top", at: "top" },
			resizable: false,
			closeOnEscape: false
		};
		var returnLink = "<p class='mainPageButton alignBtn' id='mainPageButton'><a href=\"main.php\">Return to Main</a></p>";
		var returnLinkToEntMainPage = "<p class='mainPageButton alignBtn' id='mainPageButton'><a href=\"main_enterprise.php\">Return to Main</a></p>";
		var isGroup = "<?php if (isset($_SESSION["groupId"])){echo $_SESSION["groupId"];}else{echo "";} ?>";

		var pendingProcess = [];
		var serverConnectionStatusMsg = "";
		var restoredConnWithDefaultServer = false;
		var checkDefaultInterval = 120000;
		function fail_over_dialog_on_OciFailure() {
			fail_over_action();
		}

		/* start fail over */
		var showServerErrorMessage = false;
		function foundServerConErrorOnProcess(result, processName) {
			if( result.indexOf("server connection error") > -1) {
				showServerErrorMessage = true;
                serverConnectionStatusMsg = result;
            	fail_over_dialog_on_OciFailure();
            	return true;
            } else {
            	index = pendingProcess.indexOf(processName);
          	  	if ( index > -1) {
          	  		//timer = 0;
          	    	pendingProcess.splice(index, 1);
          	  	}          	  
          	  if( pendingProcess.length == 0) {
        				if( timer === 0 ) {
							showServerErrorMessage = true;
							checkDefaulServerConnection();
            			}
              	}
            }
		}
		
		var timer;
		function checkDefaulServerConnection() {
			$.ajax({
                type: "POST",
                url: "checkServerConnection.php",
     			data: {action: "checkDefaultServerConnection"},
     			success: function (result) {
						if( result.indexOf("Default Server is Connected") > -1 ) {
							if( pendingProcess.length == 0) {
								clearInterval(timer);
								$(".ui-dialog-content").dialog("close");
								$("#fail_over_msg_dialog").dialog("open");
			    				$(".ui-dialog-buttonpane button:contains('Ok')").button().show();
			    				restoredConnWithDefaultServer = true;
			    				$("#fail_over_msg_dialog").html("Restored connection with default Feature Server.");
							} else {
								clearInterval(timer);
								timer = 0;
							}
						}                              
                 }
             });
		}

		function server_connection_operation(action, pageReload) {
			$.ajax({
                type: "POST",
                url: "checkServerConnection.php",
     			data: {action: action, pendingProcess: pendingProcess},
     			success: function (result) {
         			if(pageReload) {
         				history.go(0);
             		}
         			console.log(result);
                 }
             });
		}
		
		var fail_over_connection_message = "";
		function fail_over_action() {
			if( serverConnectionStatusMsg.indexOf("switching to alternate server") > -1 || (serverConnectionStatusMsg == "" && '<?php echo $_SESSION['server_connection_detail']['isDefaultSerConnected']; ?>' == 'false' && '<?php echo $_SESSION['server_connection_detail']['isAlternateSerConnected']; ?>' == 'true') ){
				fail_over_connection_message = "Express cannot connect to default Feature Server; switching to alternate server.";
				server_connection_operation(action = "connect_to_alternate_server", pageReload = false);
				if(timer !== 0) {
					timer = setInterval(checkDefaulServerConnection, checkDefaultInterval);
				}
			} else if( serverConnectionStatusMsg.indexOf("Please contact Express Administrator.") > -1 || (serverConnectionStatusMsg == "" && '<?php echo $_SESSION['server_connection_detail']['isDefaultSerConnected']; ?>' == 'false' && '<?php echo $_SESSION['server_connection_detail']['isAlternateSerConnected']; ?>' == 'false') ){
				fail_over_connection_message = "Connection with Feature Server has failed. Please contact Express Administrator.";
				timer = setInterval(checkDefaulServerConnection, checkDefaultInterval);
			}
			if(fail_over_connection_message != "") {
				if('<?php echo $_SESSION["do_not_load_fail_over_action"]; ?>' == "false" || showServerErrorMessage) {
					$(".ui-dialog-content").dialog("close");
					$("#fail_over_msg_dialog").html(fail_over_connection_message);
					$("#fail_over_msg_dialog").dialog("open");
					$(".ui-dialog-buttonpane").show();
					$("#mainBody").html("");
    				if( fail_over_connection_message.indexOf("switching to alternate server") > -1) {
        				$('.ui-dialog-buttonpane').find('button:contains("Ok")').show();
        			} else {
        				$('.ui-dialog-buttonpane').find('button:contains("Exit")').show();
            		}
        		}
    		} else {
        		if( '<?php echo $_SESSION['server_connection_detail']['current_server_connection']; ?>' == "alternate" ) {
        			checkDefaulServerConnection();
                } 
                else {
                	server_connection_operation(action = "connect_to_default_server", pageReload = false);
                }
        	}
		}
		
		$(function()
		{

			 $("#dialogVideoContent").hide();

			 $("#changeGroup").click(function() {
				alert("clicked");
			});

			/* Previous Change Group */
					
			$("#fail_over_msg_dialog").dialog($.extend({}, defaults, {
				width: 800,
				minHeight: 200,
				buttons: {
					"Ok": function() {						
						$(this).dialog("close");
						if( restoredConnWithDefaultServer ) {
							server_connection_operation(action = "connect_to_default_server", pageReload = true);
						} else {
							history.go(0);
						}
					},
					"Exit": function() {
						var pageReload = ( window.location.pathname.indexOf("main_enterprise.php") === -1) ? true : false;
						server_connection_operation(action = "all_connection_failed", pageReload);
						window.location.href = '/Express/index.php';
						$(this).dialog("close");
					}
				},
				open: function(event) {
					 setDialogDayNightMode($(this));
				     $('.ui-dialog-buttonpane').find('button:contains("Ok")').addClass('cancelButton').hide();
				     $('.ui-dialog-buttonpane').find('button:contains("Exit")').addClass('cancelButton').hide();
				 }
			}));
			
    		
    		if( window.location.pathname.indexOf("chooseGroup.php") === -1) {				
					fail_over_action();
            }
            
       /*  var checkChangesModule = false;
         function checkChangesTrue(trueChanges){
             checkChangesModule = true ;
         }
            */
        var checkLogoutClick = ""  ; 
        var checkChangePass = ""  ;
        $(".systemConfiRedirectCls").click(function(){
        if($('.changesCollapsedConfigColor').length >0){
                checkLogoutClick = "yes";
                var messageDialog =  "System Configuration";
                $("#systemConfigChangesDialog").html('Are you sure you want to leave? <br/> You might lose any change you have made for this '+messageDialog+'.');
               $("#systemConfigChangesDialog").dialog('open');
            }else{
                window.location.href = '/Express/index.php';
            }
        }) ;
        
        
         $(".systemConfiRedirectResetPassCls").click(function(){
            if($('.changesCollapsedConfigColor').length >0){
                checkChangePass = "yes";
                var messageDialog =  "System Configuration";
                $("#systemConfigChangesDialog").html('Are you sure you want to leave? <br/> You might lose any change you have made for this '+messageDialog+'.');
               $("#systemConfigChangesDialog").dialog('open');
            }else{
               
                window.location.href = '/Express/reset_password.php';
            }
        }) ;
        
        
        
         $("#systemConfigChangesDialog").dialog({
		  	autoOpen: false,
		  	width: 600,
		  	modal: true,
		  	position: { my: "top", at: "top" },
		  	resizable: false,
		  	closeOnEscape: false,
		  	buttons: {
		  		"Yes": function() {
		  			if(checkLogoutClick =="yes") {
                                             window.location.href = '/Express/index.php';
                                        }
                                        if(checkChangePass =="yes") {
                                             window.location.href = '/Express/reset_password.php';
                                        }
                                   $(this).dialog("close");     
				},
		  		"No": function() {
                                        $(this).dialog("close");
		  			return false; 
		  		} 
		  	},  
		      open: function() {
                        $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019
			setDialogDayNightMode($(this));
		      	$(".ui-dialog-buttonpane button:contains('Yes')").button().show().addClass("subButton");
		          $(".ui-dialog-buttonpane button:contains('No')").button().show().addClass("cancelButton");
		      }
		  });
        
        
        
        
        
        
        
        
        
        
        
           
        $(document).on("click", ".groupReturnToMainPage", function(){
                var sp = "<?php echo $_SESSION["previousSP"]; ?>";
                var groupId = "<?php echo $_SESSION["previousGroupId"]; ?>";
                var dataToSend = {groupName:groupId, Selsp:sp};
                updateLastAccessed(dataToSend);
            });

            $(document).on("click", ".returnToEnterpriseMainPage", function(){
                var sp = "<?php echo $_SESSION["sp"]; ?>";
                updateLastAccessed({Selsp:sp});
            });
            
		$(document).on("click", ".prevChangeGroup", function(){
                    var text = $(this).html();
                    text = text.split("/");
                    //var sp = text[0];
                    //var groupId = typeof text[1] === "undefined" ? "" : text[1];
                    var clsName  = $(this).attr('data-clsName');
                    var sp       = $(this).attr('data-spName');
                    var groupId  = $(this).attr('data-groupName');
                    var redirect_url;
                     if(groupId != ""){
                            var dataToSend = {groupName:groupId, Selsp:sp, clusterName: clsName};     
                            redirect_url = "main.php";
                      } 
                      else {
                            var dataToSend = {Selsp: sp, clusterName: clsName};
                            redirect_url = "main_enterprise.php";
                       } 
                       console.log(dataToSend);
                       checkEnterprisExist(dataToSend, redirect_url);
                });
                
			$("#helpUrl").click(function()
				{
				var showBtnVideo  = '<?php echo $license["videoHelp"]; ?>';
				var dataModule = $("#helpUrl").attr("data-module");
				var dataAdminType = $("#helpUrl").attr("data-adminType");
				var userType = '';
				<?php if($_SESSION['superUser'] == 1){?>
					userType = 'superUser';
				<?php 
                                } else if($_SESSION['superUser'] == 3){ 
                                ?>
                                       userType = 'superUser';
                                <?php
                                }else{
                                 ?>
					userType = '<?php echo $_SESSION['adminType']?>';
				<?php 
                                }
                                ?>
				if (typeof dataModule === "undefined" && typeof dataAdminType === "undefined") {
					if(isGroup){
						var dataModule = "main";
					}else{
						var dataModule = "entMain";
					}					
					var dataAdminType = userType;
				}
				
				
				if(dataModule == "userMod" || dataModule == "siteManagement" || dataModule == "modDevice" || dataModule == "siteReport"){
					var dataTabId = $("#tabs .ui-state-active a").attr("id");
				}else if(dataModule == "modAA"){
					if($('#addAutoAttendant').is(':visible')) {
						var dataTabId = $("#tabs .ui-state-active a").attr("id"); 
					}else{
						var dataTabId = $("#aaTabs .ui-state-active a").attr("id");
					}
						
				}else if(dataModule == "modCallCenter"){
					var dataTabId = $("#ccTabs .ui-state-active a").attr("id");
				}else if(dataModule == "modHunt"){
					var dataTabId = $("#hgTabs .ui-state-active a").attr("id");
				}else if(dataModule == "groupModify"){
					if($("#groupDetails").is(':visible')){
						var dataTabId = $("#tabs .ui-state-active a").attr("id");
					}
				}else if(dataModule == "modVdm"){
					var dataTabId = $("#selectedDeviceType").val();
					
					/*if( !$('#deviceDetails').is(':empty') ) {debugger;
						$('#deviceName option').each(function() {debugger;
							if($(this).is(':selected')){
								var selectedData = $('#deviceName').val();
								var data = selectedData.split('||');
								var name = data[0];
								var splitData = name.split('-');
								var splittedData = splitData[0];
								dataTabId = splittedData;
							}
						});
					}*/
				}else{
					var dataTabId = "";
				}
				$.ajax({
					type: "POST",
					url: "help.php",
					data: {module : dataModule, adminType : dataAdminType, tabId : dataTabId},
					success: function(result) {
						//$('#helpDialog').html("");
						$('#dialogVideoContent').html("");
						$('#dialogHtmlContent').html("");
						var obj = jQuery.parseJSON(result);
						if(obj){

								$("#helpUrlDataID").val(obj.Video);
    							if (obj.Success == "false") 
        						{
    								if(showBtnVideo =="true"){
    									$(":button:contains('Video')").attr("disabled", "disabled").addClass("ui-state-disabled");
    								}
    								$("#helpDialog").dialog("open");
    								$("#dialogHtmlContent").html("Help file not found, filename="+obj.FileName+'.htm');
    							} 
    							else 
        					    {    	
        								if (/\s/.test(obj.FileName)) {
        									var name = obj.FileName.replace(" ", "%20");
        									//string.replace(/ /g, '%20')
        									var fileUrl = '../../../helpUrl/'+name+'.htm';
        								}else{
        									var fileUrl = '../../../helpUrl/'+obj.FileName+'.htm';
        								}
        								if(showBtnVideo =="true"){
        									$('.ui-dialog-buttonpane').find('.ui-dialog-buttonset').addClass('buttonSetClass');
        								}
        								$("#helpDialog").dialog("open");
        								$("#dialogHtmlContent").load(fileUrl);

							}   							
							if(showBtnVideo =="true")
							{
								
    							if(obj.Video == "false")
    							{    								
									$(":button:contains('Video')").attr("disabled", "disabled").addClass("ui-state-disabled");
								}    								
    							else
        						{
    								$(":button:contains('Video')").removeAttr("disabled").removeClass("ui-state-disabled");
    							}
							}
							 
							//$("#helpDialog").dialog("option", "title", "Request Complete");
						}
    						
    						if($("#dialogVideoContent").is(':visible'))
        					{
    							$(".ui-dialog-buttonpane button:contains('Video')").button().hide();
    							$(".ui-dialog-buttonpane button:contains('Close Video')").button().show();
    						}
    						else
        					{
    							if(showBtnVideo =="true")
    							{
    								$(".ui-dialog-buttonpane button:contains('Video')").button().show();
        							$(".ui-dialog-buttonpane button:contains('Close Video')").button().hide();
    							}	
    							if(showBtnVideo =="false")
    							{
    								$(".ui-dialog-buttonpane button:contains('Video')").button().hide();
        							$(".ui-dialog-buttonpane button:contains('Close Video')").button().hide();
    							}				
    							
    						}
					}
				});
				});

			$("#helpDialog").dialog($.extend({}, defaults, {
				width: 800,
				minHeight: 450,
				buttons: {
					"Video" : function(){
						playHelpVideo();
					},
					"Close": function() {
						$('#videoPlayBack').trigger('pause');
						$(this).dialog("close");
						$('#dialogVideoContent').html("");
						$('#dialogHtmlContent').html("");
					},
					"Close Video" : function(){
						$('#videoPlayBack').trigger('pause');
						$('#dialogVideoContent').hide('slide', {direction: 'right'}, 1400);
						$("#dialogHtmlContent").delay(1400).show(0);
						//$("#dialogHtmlContent").show();
						//$("#dialogVideoContent").hide();	
						$(".ui-dialog-buttonpane button:contains('Video')").button().show();
						$(".ui-dialog-buttonpane button:contains('Close Video')").button().hide();
					}
				},
				open: function(event) {
					 setDialogDayNightMode($(this));
					 $('.ui-dialog-buttonpane').find('.ui-dialog-buttonset').addClass('buttonSetClass');
				     $('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('cancelButton');
				     $('.ui-dialog-buttonpane').find('button:contains("Video")').addClass('subButton');
				 }
			}));

			
			var playHelpVideo = function(){
				var videoUrlVal = $("#helpUrlDataID").val();
				var videoUrl = '../../../help-video/'+videoUrlVal+'.mp4';
				var data = '<video width="470" height="270" controls="controls" controlsList="nodownload" id="videoPlayBack"><source src="'+videoUrl+'" type="video/mp4"></video>';
	
				//$("#dialogHtmlContent").hide();
				$('#dialogHtmlContent').hide('slide', {direction: 'right'}, 1400);
				$("#dialogVideoContent").delay(2000).show(0);
				$("#dialogVideoContent").html(data);
				
				$(".ui-dialog-buttonpane button:contains('Video')").button().hide();
				$(".ui-dialog-buttonpane button:contains('Close Video')").button().show();
			};

			$('#helpDialog').on('dialogclose', function(event) {
				$('#videoPlayBack').trigger('pause');
				//$('#helpDialog').html("");
			 });
			
		});

		// Add Remove Active class
		$(document).ready(function(){
		  getChangeGroupDetail(); 
		  $('.dropdown').click(function(event){
			  matches = event.target.matches ? event.target.matches('.dropbtn') : event.target.msMatchesSelector('.dropbtn');
			  if (matches) {
			  //commented due to code not working in IE and above code applied
			  //if (event.target.matches('.dropbtn')) {
				  	$('.menuicon.dropdown').toggleClass("activeMenu");					
				  	$(".menuIconActive img").attr('src',"/Express/images/NewIcon/menu.png");
				
				    var dropdowns = document.getElementsByClassName("dropdown-content");
				    var i;
				    for (i = 0; i < dropdowns.length; i++) {
				      var openDropdown = dropdowns[i];
				      if (openDropdown.classList.contains('show')) {
				    	  $(".menuIconActive img").attr('src',"/Express/images/NewIcon/menu_white.png");
						
				      }
				    }
				  }

			});
			 $('.dropdown1').click(function(event){
				 matches = event.target.matches ? event.target.matches('.dropbtn') : event.target.msMatchesSelector('.dropbtn');
				 if (matches) {
				 //commented due to code not working in IE and above code applied
				 //if (event.target.matches('.dropbtn')) {
				  	
					$('.addressmenuIcon.dropdown1').toggleClass("activeMenu");
					$(".menuIconActiveImg img").attr('src',"/Express/images/NewIcon/map_rest.png");
					
				    var dropdowns = document.getElementsByClassName("dropdown-content");
				    var i;
				    for (i = 0; i < dropdowns.length; i++) {
				      var openDropdown = dropdowns[i];
				      if (openDropdown.classList.contains('show')) {
				    	  $(".menuIconActiveImg img").attr('src',"/Express/images/NewIcon/map+over.png");
				      }
				    }
				  }

			});
			
		});
		
		// toggle day night vision

		$(document).ready(function () {
		$('img.rolloverImg').hover(sourceSwap, sourceSwap);
		$(".logInOffDiv").hover(sourceSwapLogout, sourceSwapLogout);
		//$(".changeGroupOffDiv").hover(sourceSwapLogout, sourceSwapLogout);
		
    	$(".imgToggle").click(function () {
        		if($(this).hasClass('nightToggle')){
                	/* Day Case */
                	$(this).removeClass('nightToggle');
                	$("body").removeClass("darkBack");
        			$('.ui-dialog').removeClass('darkBlackModal');
        			$('#chooseGroupDialogModal .modal-content').removeClass('darkBlackModal');
                 	$(this).attr("src","/Express/images/NewIcon/day.png");
                 	$(".labelTextGrey").removeClass("changeTextColor");
                 	$("#AlgoAdvancedOption").removeClass("nightModeAdvOpt");
                 	localStorage.setItem("modeEnabledLogoClass", "");
                 	$(".addServicePack").removeClass("changeSPTextColor");
              } else {
                    /* Night Case. */
                	$(this).addClass('nightToggle');
                  	$("body").addClass("darkBack");
          			$('.ui-dialog').addClass('darkBlackModal');
          			$('#chooseGroupDialogModal .modal-content').addClass('darkBlackModal');
          			$(this).attr("src","/Express/images/NewIcon/night.png");
          			$(".labelTextGrey").addClass("changeTextColor");
          			$("#AlgoAdvancedOption").addClass("nightModeAdvOpt");
          			localStorage.setItem("modeEnabledLogoClass", "darkBack");
          			$(".addServicePack").addClass("changeSPTextColor");
              }
        });
	});
 		
	var sourceSwap = function () {
        var $this = $(this);
        var newSource = $this.data('alt-src');
        $this.data('alt-src', $this.attr('src'));
        $this.attr('src', newSource);
    }
	
	/* Logout */
	var sourceSwapLogout = function() {
			var thisId = $(this).find("img");	
	        var newSource = thisId.data('alt-src');
	        thisId.data('alt-src', thisId.attr('src'));
	        thisId.attr('src', newSource);
	    }
	
		/* When the user clicks on the button, toggle between hiding and showing the dropdown content */
		function mainMenuDropdown() {
		    document.getElementById("mainDropdown").classList.toggle("show");
			document.getElementById("addressDropdown").classList.remove("show");
		}
		function addressDropdownMenu() {
		    document.getElementById("addressDropdown").classList.toggle("show");
		}

		// Close the dropdown if the user clicks outside of it
		window.onclick = function(event) {
		matches = event.target.matches ? event.target.matches('.dropbtn') : event.target.msMatchesSelector('.dropbtn');
		if (!matches) {
		  	//if (!event.target.matches('.dropbtn')) {
		  	$('.menuicon.dropdown').removeClass("activeMenu");
			$('.addressmenuIcon.dropdown1').removeClass("activeMenu");
		  	$(".menuIconActive img").attr('src',"/Express/images/NewIcon/menu.png");
		<!--	$(".menuIconActiveAdd img").attr('src',"images/icons/menu.png"); -->
			$(".menuIconActiveImg img").attr('src',"/Express/images/NewIcon/map_rest.png");

		    var dropdowns = document.getElementsByClassName("dropdown-content");
		    var i;
		    for (i = 0; i < dropdowns.length; i++) {
		      var openDropdown = dropdowns[i];
		      if (openDropdown.classList.contains('show')) {
		        openDropdown.classList.remove('show');
		      }
		    }
		  }
		}

	</script>
	<?php
	if (isset($_SESSION["loggedInUserName"]) and $_SESSION["loggedInUserName"] != "") {
		?>
		<!-- CHECK SESSION-->
		<script type="application/javascript">
            function pad(str, max) {
                str = str.toString();
                return str.length < max ? pad("0" + str, max) : str;
            }

            var timeout = <?php echo (isset($sessionTimeout) && $sessionTimeout > 0 ? $sessionTimeout : 0); ?>;
            var interval;
            var timeout_interval = 120;
            var dirty = 0;

            function set_user_active() {
                dirty = 1;
            }

            $().ready(function () {

                $("#session_timeout_dialog").dialog($.extend({}, defaults, {
                    width: 500,
                    title: "Session Timeout",
                    position: "center",
                    open: function (event, ui) {
						setDialogDayNightMode($(this));
                        $(".ui-widget-overlay").addClass("passwordExpiration");
                        $(".ui-dialog-buttonpane button:contains('Ok')").button().show().addClass('subButton');
                    },
                    close: function () {
                    	 $(".ui-widget-overlay").removeClass("passwordExpiration");
       			}
                }));

                $('body.not_dirty').one('mousemove', function (e) {
                    set_user_active();
                });

                $('body.not_dirty').one('keypress', function (e) {
                    set_user_active();
                });

                window.setInterval(function () {
                    if (dirty == 1) {
                        $.getJSON("/Express/check_session.php?set=1", function (data) {
                            console.log("success");
                            if (data.success) {
                                dirty = 0;
                            }
                        });
                        $('body.not_dirty').one('mousemove', function (e) {
                            set_user_active();
                        });
                        $('body.not_dirty').one('keypress', function (e) {
                            set_user_active();
                        });
                    }
                }, 10000);

                window.setInterval(function () {
                    /// call your function here
                    $.getJSON("/Express/check_session.php", function (data) {
                        console.log("success");
                        if (data.success) {
                            timeout = data.TIMEOUT;
                            handleTimeoutResponse();
                        } else {
                            show_timeout_dialog(true);
                        }
                    }).fail(function() {
                        timeout -= 30;
                        handleTimeoutResponse();
                    });
                }, 30000);
            });

            function handleTimeoutResponse() {

                console.log("timeout: " + timeout);
                if (timeout < timeout_interval) {

                    if(timeout > 0) {

	                    show_timeout_dialog(false);
	                    if (!interval) {
	                        interval = window.setInterval(function () {
	                            // call your function here
	                            console.log("countdown: " + timeout);
	                            if (timeout > 0) {
	                                timeout--;
	                                $("#timeout_countdown").html(pad(Math.floor(timeout / 60), 2) + ":" + pad((timeout % 60), 2));
	                                $("#timeout_message").hide();
	                                $("#countdown_message").show();
	                            } else {
                                    show_timeout_dialog(true);
	                            }
	                        }, 1000);
	                    }

                    } else {
                        show_timeout_dialog(true);
                    }

                } else {
                    $("#session_timeout_dialog").dialog('close');
                    clearInterval(interval);
                    interval = undefined;
                }

            }

            function show_timeout_dialog(timed_out) {

                if (timed_out) {

                    if(interval) {
                        clearInterval(interval);
                        interval = undefined;
                    }
                    $("#countdown_message").hide();
                    $("#timeout_message").show();

                    $("#session_timeout_dialog").dialog({
                		 buttons: {
                			"Ok": function() {
                				 window.location = "/Express/index.php";
                                 $(this).dialog("close");
                                 $(".ui-widget-overlay").removeClass("passwordExpiration");
                			} 
                	},
        			close: function () {
                     	 window.location = "/Express/index.php";
                        $( this ).dialog( "close" );
                        $(".ui-widget-overlay").removeClass("passwordExpiration");
        			}
                    });
                    
/*                   $("#session_timeout_dialog").dialog("option", "buttons",
                        [

                            {
                                text: "Ok",
                                icon: "ui-icon-heart","class": "sessionOk",
                                click: function () {
                                    window.location = "/Express/index.php";
                                    $(this).dialog("close");
                                }
                            }
                        ]
                    );
 */

                } else {

                    $("#timeout_message").hide();
                    $("#countdown_message").show();

                    $("#session_timeout_dialog").dialog("option", "buttons",
                        [
                            {
                                text: "Close","class": "cancelButton",
                                click: function () {
                                    $(this).dialog("close");
                                    $(".ui-widget-overlay").removeClass("passwordExpiration");
                                }
                            },
                            {
                                text: "Keep me Logged in","class": "sessionKeepMe",
                                click: function () {
                                    $(this).dialog("close");
                                    $.getJSON("/Express/check_session.php?set=1", function (data) {
                                        console.log("success");
                                        if (data.success) {
                                            timeout = data.TIMEOUT;
                                            if (timeout > timeout_interval) {
                                                clearInterval(interval);
                                                $("#session_timeout_dialog").dialog('close');
                                                $(".ui-widget-overlay").removeClass("passwordExpiration");
                                            }
                                        }
                                    });
                                }
                            }
                        ]
                    );
                }

                $("#session_timeout_dialog").dialog('open');
                $(".ui-dialog-buttonpane button:contains('Ok')").button().show().addClass('subButton');

            }

            function modeEnabledLogo(){
            	if($("body").hasClass('darkBack')){
            		localStorage.setItem("modeEnabledLogoClass", "darkBack");
                }else{
                	localStorage.setItem("modeEnabledLogoClass", "");
                }
            }

            function setDialogDayNightMode(dialog) {
            	$('html').scrollTop("0");
            	if(localStorage['modeEnabledLogoClass']) {
                	var varVal = localStorage.getItem('modeEnabledLogoClass');
                	if(varVal == "darkBack") {
                		$(".ui-dialog").addClass('darkBlackModal');
                	} else {
                		$(".ui-dialog").removeClass('darkBlackModal');
                	}
            	}
            }

            
            function callToSetGroup(dataToSend, redirect_url) {
                //alert();
                console.log('callToSetGroup - ');
                console.log(dataToSend);
            	$.ajax({
                     type: "POST",
                     url: "setGroup.php",
                     data: dataToSend,
                     success: function (result) {
                     	console.log('Success - ');
                        console.log(result);
                        result = result.trim();
                     	if( typeof dataToSend.groupName != "undefined") {
                     		if (result.slice(0, 1) == "1") {
                                alert("Error:  Group not found: " + dataToSend.groupName);
                                if(confirm){
                                	deleteGroupNotExist(dataToSend);
                                }
                            } else {
                            	$.ajax({
                                    type: "POST",
                                    url: "adminUsers/getAllGroups.php",
                                    data: {getSP: dataToSend.Selsp, chooseGroup: "true"},
                                    success: function (result) {
                	                        if ( result.indexOf(dataToSend.groupName) <= -1) {
                	                        	deleteGroupNotExist(dataToSend);
                	                        	alert("Error:  Group not found: " + dataToSend.groupName);
                	                        } else {
                	                        	window.location.replace(redirect_url);
                    	                    }
                                    }
                            	});
                            }
                         } else {
                        	 	window.location.replace(redirect_url);
                             }
                     }
                 });
            }

            function updateLastAccessed(dataToSend) {
            	$.ajax({
                    type: "POST",
                    url: "changeGroupDialog/setChangeGroupDetails.php",
         			data: {module: "clickedPrevChangeGroup", spId: dataToSend.Selsp, groupId: dataToSend.groupName, clusterName : dataToSend.clusterName},
         			success: function (result) {
				console.log(result);                              
                     }
                 });
            }
            
        	function checkEnterprisExist(dataToSend, redirect_url) {		
        		$.ajax({
        			type: "POST",
        			url: "changeGroupDialog/setChangeGroupDetails.php",
        			data: {'module': 'checkEnterPriseExist', selectedSP : dataToSend.Selsp, clusterName : dataToSend.clusterName},
        			success: function (result) {
                                console.log(dataToSend);
                                console.log(result);
            			if( result.indexOf("Enterprise is Exist") != -1) {
            				callToSetGroup(dataToSend, redirect_url);
            				updateLastAccessed(dataToSend);
                		} else {
                			deleteSpNotExist(dataToSend);
                			alert("Error: Enterprise not found.");
                		}
        			}
        		});
        	}

        	function deleteGroupNotExist(dataToSend) {		
        		$.ajax({
        			type: "POST",
        			url: "changeGroupDialog/setChangeGroupDetails.php",
        			data: {'module': 'deleteGroupNotExist', enterpriseId : dataToSend.Selsp, groupId : dataToSend.groupName, clusterName : dataToSend.clusterName},
        			success: function (result) {
        				getChangeGroupDetail();
        			}
        		});
        	}

        	function deleteSpNotExist(dataToSend) {
        		$.ajax({
        			type: "POST",
        			url: "changeGroupDialog/setChangeGroupDetails.php",
        			data: {'module': 'deleteSpNotExist', enterpriseId : dataToSend.Selsp, clusterName : dataToSend.clusterName},
        			success: function (result) {
        				getChangeGroupDetail();
        			}
        		});
        	}
        	
        	function getChangeGroupDetail() {
                $(".chooseGroupMainDiv").removeClass("changeGroupOffDiv dropdown");
            	$.ajax({
                    type: "POST",
                    url: "changeGroupDialog/setChangeGroupDetails.php",
         			data: { module: 'getChangeGroupDetail' },
         			success: function (result) {
             				if(foundServerConErrorOnProcess(result, "")) {
            					return false;
                            }
							result = JSON.parse(result);
							html = "";
							if(result.length > 0) {
                                                            var supUser        = "<?php echo $_SESSION["superUser"]; ?>";
                                                            var cluster_supprt = "<?php echo $_SESSION["cluster_support"]; ?>";
                                                            var clsName        = "<?php echo $_SESSION["selectedCluster"]; ?>";
                                                            
                                                            $.each(result, function(i, val) {
                                                            var groupId = val.groupId != "" ? " / " + val.groupId : "";
                                                            groupId += val.groupName != "" ? " - " + val.groupName : "";
                                                            var sp = val.sp;
                                                            sp += val.spName != "" ? " - " + val.spName : "";
                                                            var htmlText = sp + groupId;
                                                            if(supUser == "1" && cluster_supprt == true){
                                                                var clusterName = val.clusterName != "" ? val.clusterName + ":" : "";
                                                                //alert('val - clusterName - '+ val.clusterName);
                                                                    if(val.clusterName != null){   
                                                                        clsName = val.clusterName;
                                                                    }
                                                                    if(clusterName != "null:"){                                                                    
                                                                    var htmlText = clusterName + " " + sp + groupId;
                                                                }
                                                                else{
                                                                    var htmlText = sp + groupId;
                                                                }
                                                            }                                                            
                                                            html += "<a class='prevChangeGroup' data-clsName='"+clsName+ "' data-spName='"+val.sp+ "' data-groupName='" +val.groupId + "'title='Change Group'>" + htmlText + "</a>";
                                                        });
                                                        $(".changeGroupToolTip").html(html);
                                        
                                        $( ".changeGroupIcon" ).mouseover(function() {
                                        	$(".dropdown-wrap select").blur();
                                           	$(".chooseGroupMainDiv").addClass("changeGroupOffDiv dropdown");
                                            $('.chooseGroupImageIcon').attr('src', '/Express/images/NewIcon/change_groups_white.png');
                                        }) ; 
                                         
                                        $( ".changeGroupIcon" ).mouseout(function() {
                                        
                                        $('.chooseGroupImageIcon').attr('src', '/Express/images/NewIcon/change_groups.png');
                                            $(".chooseGroupImageIcon").removeClass("changeGroupOffDiv dropdown");
                                        }) ;  

                                }   
                        }
                 });
            }
            
            $(function()
            {
                if(localStorage['modeEnabledLogoClass']) {
                	var varVal = localStorage.getItem('modeEnabledLogoClass');
                	if(varVal != "darkBack") {
                		$(".imgToggle").addClass("nightToggle");	
                    }
                	$(".imgToggle").trigger("click");
                }
            });
            
        </script>
<style>
    div.spAddressDiv span.headerTextTooltip .tooltip{
        width: auto !important;
        padding: 10px 30px !important;
    }
    
    div.spAddressDiv span.headerTextTooltip div{
        white-space: nowrap !important;
        text-transform: none !important;
    }
    div.spAddressDiv span.headerTextTooltip a:hover{
        text-decoration: none;
        color: #6ea0dc !important;
        text-transform: none !important;
    }
    
    div.spAddressDiv span.headerTextTooltip a{
        color: #6ea0dc !important;
        text-transform: none !important;
    }
    
    
.dropdown-menu.columns-3 {
	width: 952px;
	right:0;
	left:auto;
	padding: 0;
	overflow: hidden;
	margin-top: 0 !important;
}

.multi-column-dropdown {
  list-style: none;
  margin-bottom: 45px;
  padding: 0px;
} 
@media (max-width: 767px) {
	.dropdown-menu.multi-column {
		min-width: 240px !important;
		overflow-x: hidden;
	}
}
</style>
        <!-- SESSION TIMEOUT MODAL -->
        <div id="session_timeout_dialog">
            <div class="modal-dialog" role="document">
                <h4 id="countdown_message" style="display: none;">You will be logged out in <b id="timeout_countdown"></b> minutes.</h4>
                <h4 id="timeout_message" style="display: none;">Your session has expired.</h4>
            </div>
        </div>

		<?php
	}
	?>
<div class="customContainer customRow">
<?php if(!isset($no_header) || !$no_header)  { ?>
	<div style="" class="mainHeader">
            <div class="col-md-4 logoCol" style="width:30% !important;">

		
                        <div class="headerLogoPos leftDesc3" style="">
			<!--<div class="leftDesc3" style=""> -->
			<!--  <img src="images/icons/express_logo.png" title="logo" style='max-height: 100px;max-width: 178px;'/> -->
			<!--<img class="rolloverImg" src="images/icons/icon_viewmodifyuser.png" data-alt-src="images/icons/icon_viewmodifyuser_rollover.png" />-->
					<?php
						$logoFile = $brandingLogoWebPath ? $brandingLogoWebPath : "/logo.jpg";
						if (isset($_SESSION["groupId"]) && !empty($_SESSION["groupId"])) {
						//	echo "<a id=\"logoImage\" href='main.php'><img src=\"$logoFile?". time(). "\" title=\"logo\" style='max-height: 100px;max-width: 178px;' onclick='modeEnabledLogo()'/></a>";
						 
							
							// echo "<a id=\"logoImage\" class=\"RedirectNavHeader\"><img src=\"$logoFile?". time(). "\" title=\"logo\" class=\"RedirectNavHeader\" style='max-height: 100px;max-width: 178px;' onclick='modeEnabledLogo()'/></a>";
                                                        echo "<a id=\"logoImage\" class=\"RedirectNavHeader\"><img src=\"$logoFile?". time(). "\" title=\"logo\" class=\"RedirectNavHeader\" onclick='modeEnabledLogo()'/></a>";
						} else {
							// echo "<a id=\"logoImage\" ><img src=\"$logoFile?". time(). "\" title=\"logo\" style='max-height: 100px;max-width: 178px;'/></a>";
		//echo "<a id=\"logoImage\" class=\"RedirectNavHeader\"><img src=\"$logoFile?". time(). "\" title=\"logo\" class=\"RedirectNavHeader\" style='max-height: 100px;max-width: 178px;'/></a>";
                                                    echo "<a id=\"logoImage\" class=\"RedirectNavHeader\"><img src=\"$logoFile?". time(). "\" title=\"logo\" class=\"RedirectNavHeader\"/></a>";				
//echo "<a id=\"logoImage\" class=\"RedirectNavHeader\"><img src=\"$logoFile?". time(). "\" title=\"logo\" class=\"RedirectNavHeader\" style='max-height: 100px;max-width: 178px;'/></a>";
						}
					?>
						 </div>
		</div>
<!-- 160 server start-->
		<!--<div class="col-md-4 welcomUser" style="width:40% !important;"> --> 
                <div class="col-md-4 welcomUser" style="width: 38.8% !important;"> 
			<span class="col-md-12 spAddressDiv loginUserAndSpan">
				<?php
					//only show when user is logged in
					if (isset($_SESSION["sp"]))
					{	
					$showChangeGroup = $_SESSION['headerLoginDetailsOutput']['changeGroup'];
						?>
							Welcome <?php echo isset($_SESSION["loggedInUser"]) ? $_SESSION["loggedInUser"] : ""; ?>
						
						<?php
					}
				?>
			</span>
			<div class="col-md-12 spAddressDiv">
			<!--	<div style="" class="float-left">
					<?php// if(strpos($_SERVER['REQUEST_URI'], 'chooseGroup') == false && isset($_SESSION["groupId"])){ ?>
						<a href = "main.php"><img src="/Express/images/NewIcon/group_home_icon.png" class="" border="0" alt="" title="Main Page" onclick="modeEnabledLogo()"/></a>
					<?php //} ?>
					<?php// if ($_SESSION["superUser"] == "1" && (!isset($_SESSION["groupId"]) || $_SESSION["groupId"] == "")) {
						//$show_enterprise_link = (isset($_SESSION["sp"]) && $_SESSION["sp"] != "" && $_SESSION["sp"] != "None") ? "inline-block" : "none";
						?>
						<a id="enterprise_main_page_link" style="display: <?php// echo $show_enterprise_link; ?>" href="main_enterprise.php">
						<img src="/Express/images/NewIcon/Enterprise_home_icon.png" class="" border="0" alt="" title="Enterprise Main Page" onclick="modeEnabledLogo()"/></a>
						<?php
					//}
					?>
				
				</div>-->
			<span class="groupNameSpan headerTextTooltip">
                        <?php 
                            $clusterSupport  =  (isset($bwClusters) && $bwClusters == "true") && $enableClusters == "true" ? true : false;
                            if($_SESSION["superUser"] == "1" && $clusterSupport){ 
                                  echo "Cluster: ".$_SESSION['selectedCluster']." | ";
                            }
                        ?>                        
                        Enterprise:                            
                        <?php 
                                if(isset($_SESSION["sp"])){
                                    if(strlen($_SESSION["sp"])>16){ 
                                        $spName     = trim(substr($_SESSION["sp"], 0,16))."...";
                                        $spNameFull = $_SESSION["sp"];
                                        echo "<a style='margin-left: 5px;'class='customTooltip' data-toggle='tooltip' data-placement='bottom' title='$spNameFull'>$spName</a>";
                                      }
                                    else{
                                        echo $_SESSION["sp"];
                                    }
                                }
                                //echo " ".(isset($_SESSION["sp"]) ? $_SESSION["sp"] : "")."";                         
				if(isset($_SESSION["groupId"])){ 
					 echo "&nbsp;|&nbsp;Group:"; 
                                         if(strlen($_SESSION["groupId"])>16){  
                                             $groupName      =  trim(substr($_SESSION["groupId"], 0,16))."...";
                                             $groupNameFull  =  $_SESSION["groupId"];
                                             echo "<a style='margin-left: 5px;'class='customTooltip' data-toggle='tooltip' data-placement='bottom' title='$groupNameFull'>$groupName</a>";
                                          }else{
                                             echo $_SESSION["groupId"];
                                         }
                                         //echo " " . (isset($_SESSION["groupId"]) ? $_SESSION["groupId"] : "") . "</span>"; 
				}                                
                                
                           ?>
                         </span>
			<div class="addressmenuIcon dropdown1 dropdownAdd">
				<?php
				if(isset($_SESSION["groupId"])){ ?>
					 <div class="menuIconActiveAdd menuIconActiveImg">
                                            <a style='margin-left: 5px;' title='Site Address'>
                                                <img src="images/NewIcon/map_rest.png" class="dropbtn" onclick="addressDropdownMenu()" title='Site Address' />
                                            </a>
					</div> 
                                        
                                  <?php } ?>
				<div class="dropdown-content" id="addressDropdown">
					<div class="addressBtn">
					   <?php                                                 
                                                echo (!empty($_SESSION["groupInfo"]["groupName"]) ? ("" . $_SESSION["groupInfo"]["groupName"]) : ""); ?>
						<?php
						$address = array();
						if (!empty($_SESSION["groupInfo"]["addressLine1"])) { $address[] = $_SESSION["groupInfo"]["addressLine1"]; }
						if (!empty($_SESSION["groupInfo"]["addressLine2"])) { $address[] = $_SESSION["groupInfo"]["addressLine2"]; }
						if (!empty($_SESSION["groupInfo"]["city"]))         { $address[] = $_SESSION["groupInfo"]["city"]; }
						if (count($address) > 0) { echo "<br/>" . join(", ", $address). " "; }

						$address = array();
						if (!empty($_SESSION["groupInfo"]["stateOrProvince"])) { $address[] = $_SESSION["groupInfo"]["stateOrProvince"]; }
						if (!empty($_SESSION["groupInfo"]["zipOrPostalCode"])) { $address[] = $_SESSION["groupInfo"]["zipOrPostalCode"]; }
						if (count($address) > 0) { echo "<br/>" . join(", ", $address); }
						
                                                //Code added @ 19 Dec 2018
                                                
                                                if(empty($_SESSION["groupInfo"]["groupName"]) && !empty($_SESSION["sp"]) && !empty($_SESSION["groupId"])){                                                    
                                                   $groupInfoTmpArr = getUserInfo($_SESSION["sp"], $_SESSION["groupId"]);
                                                   echo $groupInfoTmpArr["groupName"];
                                                   $address = array();
                                                    if (!empty($groupInfoTmpArr["addressLine1"])) { $address[] = $groupInfoTmpArr["addressLine1"]; }
                                                    if (!empty($groupInfoTmpArr["addressLine2"])) { $address[] = $groupInfoTmpArr["addressLine2"]; }
                                                    if (!empty($groupInfoTmpArr["city"]))         { $address[] = $groupInfoTmpArr["city"]; }
                                                    if (count($address) > 0) { echo "<br/>" . join(", ", $address). " "; }

                                                    $address = array();
                                                    if (!empty($groupInfoTmpArr["stateOrProvince"])) { $address[] = $groupInfoTmpArr["stateOrProvince"]; }
                                                    if (!empty($groupInfoTmpArr["zipOrPostalCode"])) { $address[] = $groupInfoTmpArr["zipOrPostalCode"]; }
                                                    if (count($address) > 0) { echo "<br/>" . join(", ", $address); }
                                                }
                                                //End code
                                                ?>
					</div>
				</div>
			</div>
			</div>
		</div>
		
		<div  class="col-md-4 loggedInUserCol" style="width:30% !important;right:1px;">
		
				<?php if($_SESSION["superUser"] != "2") { ?>
				<div style="" class="menuicon dropdown">
				  <!-- <button onclick="myFunction()" class="dropbtn">Dropdown</button> -->
				
				<div class="menuIconActive"><img src="/Express/images/NewIcon/menu.png" title="Menu" class="dropbtn" onclick="mainMenuDropdown()"/></div>

	
			</div>
			 <ul class="dropdown-menu multi-column columns-3 dropdown-content" id="mainDropdown">
		            <div class="">
		            	<?php if(isset($_SESSION["groupId"])) { 
		                if($userModuleExist = $_SESSION["permissions"]["viewusers"] == "1" ||
		                    $_SESSION["permissions"]["addusers"] == "1" || $_SESSION["permissions"]["modifyUsers"] == "1" ||
		                    $_SESSION["permissions"]["groupWideUserModify"] == "1" || $_SESSION["permissions"]["adminUsers"] == "1" ||
		                    $_SESSION["superUser"] == "1" || $_SESSION["permissions"]["expressSheets"] == "1") {
		                ?>
			            <div class="col-sm-4 usersMenu">
				            <ul class="multi-column-dropdown">
                            		<li class="userMenuTitle"><a href="#">Users</a></li>
								
					           <!-- For main -->
								<?php 
							      if (isset($_SESSION["permissions"]["addusers"]) and $_SESSION["permissions"]["addusers"] == "1") {
								  ?>
									 <li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="userAdd">Add User</a></li>
								<?php
							      }
								?>
								
								<!-- For main --> 
                    			<?php 
                    			if (isset($_SESSION["permissions"]["adminUsers"]) and $_SESSION["permissions"]["adminUsers"] == "1" and $_SESSION["superUser"] == "0")
                    			{
                            		         ?>
                        					<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="adminUser">Administrators</a></li>
                        				<?php
                        		  }
                    		      ?>
                    		      
								<!-- For main -->	            
                				<?php if ($_SESSION["superUser"] == "1" || $_SESSION["permissions"]["groupWideUserModify"] == "1")
                    		     {
                    		         ?>
                    			         <!--li><div><a href="#" class="navMenu navigate navButton mainMenuBtn" id="modGroup"><img src="images/groups-icon.png" /><br />Modify Group</a></div></li-->				
                    					<li style="display:none !important;"><a href="#" class="navMenu navigate navButton mainMenuBtn" id="bulk"> Bulk Modify </a></li>
                    				<?php
                    		      }?>
                    		      
                    		    <?php 
                        		if ($_SESSION["superUser"] == "1" || $_SESSION["permissions"]["expressSheets"] == "1")
                        		{
                        		?> 
					            <li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="expressSheets">Express Sheets</a></li>
								<?php } ?>
								
					           <!-- For main -->
					            <?php if (isset($_SESSION["permissions"]["modifyUsers"]) and $_SESSION["permissions"]["modifyUsers"] == "1")
                    		      {
                    		          ?>
                    					<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="userMod">Modify User</a></li>
                    					<!--<li><div><a href="#" class="navMenu navigate navButton mainMenuBtn" id="findMac"><img src="images/ip-icon.png" /><br />Find MAC Address</a></div></li> -->
                    				<?php
                    		      }?>
                				
                    		      <!-- For main -->
								<?php
									if (isset($_SESSION["permissions"]["viewusers"]) and $_SESSION["permissions"]["viewusers"] == "1")
								{
								?>
									 <li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="users">Users</a></li>
									<?php
								 } ?>
								 
							</ul>
			            </div>
			            <?php 
		                    } 
		                  }
			            ?> 
			            
			         <!-- Group/Services -->   
			         <?php 
                	if($groupSerModuleExist = ($_SESSION["superUser"] == "1") || ($enterpriseModulePermissionLevel) || ($basicInformationLogPermission == "1") || ($_SESSION["permissions"]["groupBasicInfo"] == "1") || 
                	    ($_SESSION["permissions"]["groupDomains"] == "1") || ($_SESSION["permissions"]["groupServices"] == "1") || 
                	    ($_SESSION["permissions"]["groupDN"] == "1") || ($_SESSION["permissions"]["groupPolicies"] == "1") || 
                	    ($_SESSION["permissions"]["groupNCOS"] == "1") || ($_SESSION["permissions"]["groupVoicePortal"] == "1") && isset($_SESSION["permissions"]["modifyGroup"]) && $_SESSION["permissions"]["modifyGroup"] == "1" || 
                	    $_SESSION["permissions"]["aamod"] == "1" || $_SESSION["permissions"]["modhuntgroup"] == "1" || $_SESSION["permissions"]["modcallCenter"] == "1" ||
                	    $_SESSION["permissions"]["modschedule"] == "1" || $cpgFieldVisible == "true" && (isset($_SESSION["permissions"]["callPickupGroup"]) && $_SESSION["permissions"]["callPickupGroup"]== "1" && $_SESSION["groupServicesAuthorizationTable"] == "1")){ 
                             
                            ?> 
			            <div class="col-sm-4 macdMenu">
				            <ul class="multi-column-dropdown">
					            <li class="groupMenuTitle"><a href="#">Group/Services</a></li>
					            
					            <?php
					            //Auto Attendants
					            if(isset($_SESSION["groupId"])){
    							  if (isset($_SESSION["permissions"]["aamod"]) and $_SESSION["permissions"]["aamod"] == "1")
    							  {
    								  ?>
    									<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="modAA">Auto Attendants</a></li>
    								<?php
    							  }
							    } ?>
							  
							  	<?php //Call Centers
								if(isset($_SESSION["groupId"])){
        							if (isset($_SESSION["permissions"]["modcallCenter"]) and $_SESSION["permissions"]["modcallCenter"] == "1")
        							{
        								?>
        								<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="modCallCenter">Call Centers</a></li>
        								<?php
							        } 
        						}?>
        							
        						 <!-- For main -->    
    					          <?php //Call Pickup Groups
    		                      if(isset($_SESSION["groupId"])) {
    	                          if (isset($_SESSION["permissions"]["callPickupGroup"]) and $_SESSION["permissions"]["callPickupGroup"] == "1" and $cpgFieldVisible == "true" and $_SESSION["groupServicesAuthorizationTable"] == "1")
        	                          {
                        		        ?>
                        					 <li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="modGrpCallPickup">Call Pickup Groups</a></li>
                        				<?php
                        		      }
    		                      }
    		                      ?>
		                      
					            <!-- For Enterprise -->
					            <?php if(($_SESSION["superUser"] == "1" || $_SESSION["superUser"] == "3") && (!isset($_SESSION["groupId"]) || $_SESSION["groupId"] == "")){?>
					            
					            <?php 
                                                        //if(isset($license["Enterprises"]) &&  $license["Enterprises"] == "true") 
                                                        if($enterpriseModulePermissionLevel)
                                                        { 
                                                    ?>
									<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="Enterprises">Enterprises</a></li>
						    <?php } ?>        
                                                                        
                                                  <?php 

	if($groupSerModuleExist = ($_SESSION["superUser"] == "1") ||
                ($_SESSION["superUser"] == "3") || 
                ($basicInformationLogPermission == "1") || 
                ($outgoingCallingPlanLogPermission == "1") || 
                ($domainsLogPermission == "1") || 
                ($securityDomainsLogPermission == "1") || 
                ($servicesLogPermission == "1") || 
                ($dnsLogPermission == "1") ||
                ($callProcessingPoliciesLogPermission == "1") ||
                ($networkClassesofServiceLogPermission == "1") || 
                ($voicePortalServiceLogPermission == "1")){ ?>
			<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="groupModify">Groups</a></li>
	<?php } ?>                       
                                                                        
                                                                        
                                                                        
                                                                        
					            
					            <?php } ?>
					            
					            <?php  //Hunt Groups
							  if(isset($_SESSION["groupId"])){
								    if (isset($_SESSION["permissions"]["modhuntgroup"]) && $_SESSION["permissions"]["modhuntgroup"] == "1")
    								{
    									?>
    									<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="modHunt">Hunt Groups</a></li>
    									<?php
    								}
								} ?>
								
					             <!-- For main -->
					            <?php 
					            if(isset($_SESSION["groupId"])) { 
					                if ( ($_SESSION["superUser"] == "1") || ($_SESSION["permissions"]["groupBasicInfo"] == "1") || 
					                    ($_SESSION["permissions"]["groupDomains"] == "1") || 
					                    ($_SESSION["permissions"]["groupServices"] == "1") || 
					                    ($_SESSION["permissions"]["groupDN"] == "1") || 
					                    ($_SESSION["permissions"]["groupPolicies"] == "1") || 
					                    ($_SESSION["permissions"]["groupNCOS"] == "1") || 
					                    ($_SESSION["permissions"]["groupVoicePortal"] == "1") && 
					                    isset($_SESSION["permissions"]["modifyGroup"]) && 
					                    $_SESSION["permissions"]["modifyGroup"] == "1")
					                {
					            ?>
					            <li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="groupModify">Modify Group</a></li>
					            <?php 
					                }
					            } ?>
					            
					           <?php  //Schedules
        						if(isset($_SESSION["groupId"])){
                    		     if (isset($_SESSION["permissions"]["modschedule"]) and $_SESSION["permissions"]["modschedule"] == "1")
                        		     {
                        		         ?>
                        					 <li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="modSched">Schedules</a></li>
                        				<?php
                        		     }
		                          } ?>
		               </ul>
			            </div>
			           <?php } ?> 
                                 
                               
                                 
	            <div class="col-sm-4 inventoryMenu">
		            <ul class="multi-column-dropdown">
					 <?php if($invernotryPermissionLevel) { ?>
                                <li class="inventoryMenuTitle"><a href="#">Inventory</a></li>
                                        
                                <?php } ?>
				
		         <!-- For main -->
                <?php
                 if(isset($_SESSION["groupId"])){
                  if (isset($_SESSION["permissions"]["announcements"]) and $_SESSION["permissions"]["announcements"] == "1")
                  {
                      ?>
                <li> <a href="#" class="navMenu navigate navButton mainMenuBtn" id="announcement">Announcements</a></li>
                <?php 
                    }
                }
                ?>
		    
                <!-- For Enterprise -->
                
                <?php if($_SESSION['groupId'] == "" && ($entAnnouncementsLogPermission =="1" || $_SESSION['superUser'] =="1")){?>
    			<li> <a href="#" class="navMenu navigate navButton mainMenuBtn" id="announcement">Announcements</a></li>
    			<?php } ?>     		      
    			
    			<?php
    		    if(isset($_SESSION["groupId"])){
        			if (isset($_SESSION["permissions"]["viewcdrs"]) and $_SESSION["permissions"]["viewcdrs"] == "1" and ! is_null($billDB))
            		{
            			?>
            			<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="cdrs">CDRs</a></li>
            			<?php
            		}
		        }
		        ?>
                                
                                <!-- For CDRs on Enterprise -->
    		    <?php if(($_SESSION['superUser'] == "1"  || $cdrLogPermission == "1") && ! is_null($billDB) && (!isset($_SESSION["groupId"]) || $_SESSION["groupId"] == "")){?>
    			<li> <a href="#" class="navMenu navigate navButton mainMenuBtn" id="cdrsEnterprise">CDRs</a></li>
    			<?php } ?> 
                                
                        <!-- Change Logs -->
		        <?php                        
		        if(isset($_SESSION["groupId"])){
            		if (isset($_SESSION["permissions"]["viewChangeLog"]) && $_SESSION["permissions"]["viewChangeLog"] == "1")
            		{
            			?>
            			<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="changeLog">Change Logs</a></li>
            			<?php 
            		}
    		    } ?>
                                
                                 <!-- For Enterprise changeLog-->
                        <?php if(($_SESSION["superUser"] == "1" || $changeLogLogPermission) && (!isset($_SESSION["groupId"]) || $_SESSION["groupId"] == "")){?>
                            <li> <a href="#" class="navMenu navigate navButton mainMenuBtn" id="entChangeLog">Change Logs</a></li>
    			<?php } ?> 
    		    
    		    <!-- For main -->
    			<?php 
    			if(isset($_SESSION["groupId"])){
        			if (isset($_SESSION["permissions"]["deviceInventory"]) and $_SESSION["permissions"]["deviceInventory"] == "1")
                     { ?> 
        			<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="modDevice">Device Inventory</a> </li>
        			<?php 
        			}
    			}?>
    			
    			<!-- For Enterprise -->
    			<?php if(($_SESSION["superUser"] == "1" || $devicesLogPermission =="1") && (!isset($_SESSION["groupId"]) || $_SESSION["groupId"] == "")){ ?>
                            
    			<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="modDevice">Device Inventory</a> </li>
    			<?php } ?>
				
				 
                
				<!-- For main -->
    			<?php 
    			if(isset($_SESSION["groupId"])){
    			     if(isset($license["vdmLite"]) && $license["vdmLite"] =="true"){
    			         if (isset($_SESSION["permissions"]["vdmLight"]) and $_SESSION["permissions"]["vdmLight"] == "1"){?>
        			<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="modVdm">Device Management</a> </li>
    			<?php 
    			         } 
			         } 
    			}?>
                                
                                 <!-- For Enterprise -->
    		    <?php if($_SESSION['groupId'] == "" && ($entServicePacksLogPermission == "1" || $_SESSION['superUser'] =="1")){?>
    			<li> <a href="#" class="navMenu navigate navButton mainMenuBtn" id="servicePackModule">Service Pack</a></li>
    			<?php } ?> 
			     
    		    
			<!-- For Group -->
			<?php 
			if(isset($_SESSION["groupId"])){
			if (($_SESSION["permissions"]["sasTest"] == "1") or ($_SESSION["permissions"]["siteReports"] == "1") or ($_SESSION["superUser"] == "1") ) { ?>	            
			<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="siteManagement">Site Management</a></li>
			<?php }
			}?>
			
			<!-- For Enterprise -->
			<?php 
			if(($_SESSION["superUser"] == "1" || $_SESSION["superUser"] == "3") && (!isset($_SESSION["groupId"]) || $_SESSION["groupId"] == "")){
    			if ($_SESSION["permissions"]["sasTest"] == "1" || $phoneProfilesLogPermission == "1" ||  $broadWorksLicenseReportLogPermission  == "1") { 
                            if((isset($license["BroadWorksLicenseReport"]) && $license["BroadWorksLicenseReport"] == "true") or (isset($license["customProfile"]) && $license["customProfile"] == "true") && checkCustomProfiles())
                            {
                                ?>
                                <li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="siteReport">Site Management</a></li>
                                <?php                
                            }
    			}        
			}
			?>   
                          
                            
                            
                            
                            
               <?php   if($userPermission == "1" && (!isset($_SESSION["groupId"]) || $_SESSION["groupId"] == "")){
                     ?>
                       <li> <a href="#" class="navMenu navigate navButton mainMenuBtn" id="usersEnterprise">Users</a></li>
                       
                      <?php
                    } ?>       
                       
                                
			
            <!-- View Numbers -->
            <?php
            if(isset($_SESSION["groupId"])){
            	if (isset($_SESSION["permissions"]["viewnums"]) and $_SESSION["permissions"]["viewnums"] == "1")
            	{
            			?>
            		<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="dids">View Numbers</a></li>
            		<?php
                }
            } ?>
		        
			<?php //} ?> <!-- Check Level -->
			
				            </ul>
							
							<?php //} ?>			
			            </div>
			         <?php if(($administratorsLogPermission == "1" || $adminLogsLogPermission == "1") && (!isset($_SESSION["groupId"]) || $_SESSION["groupId"] == "")){ ?>   
			          <div class="col-sm-4 adminMenu">
				            <ul class="multi-column-dropdown">
				            <?php if ($administratorsLogPermission == "1")  { ?>
				            <li class="adminMenuTitle"><a href="#">Administrators</a></li>
                                            <li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="adminUserEnt">Administrators</a></li>
                                            <?php } if ($adminLogsLogPermission == "1")  { ?>
                                             
							
							<li><a href="#" class="navMenu navigate navButton mainMenuBtn" id="adminLogs">Admin Logs</a></li>
							<?php } ?>
							</ul>
						</div>
					<?php } ?>	
		            </div>
	            </ul>
				
				<div class="logInOffDiv dropdown">
		<img class="logoutWhiteIcon" class="RedirectNavHeader"  src="/Express/images/NewIcon/log_out.png" data-alt-src="/Express/images/NewIcon/log_out_down.png" />
			<div class="dropdown-content" id ="logoutDropD">
			<?php if($_SESSION["use_ldap_authentication"] == 0) { ?>
			    <a id="redirectHeaderNavResetPass" class="RedirectNavHeader" title="Reset Password">Change Password</a>
			<?php } ?>			 
			<!--<a href="/Express/index.php" id="logout" title="Logout">Logout</a> -->
			
			<a id="logout" class="RedirectNavHeader" title="Logout">Logout</a>
			
			 
			</div>
		</div>
		<?php } else { ?>
						<div class="logInOffDiv dropdown">
						<img class="logoutWhiteIcon" src="/Express/images/NewIcon/log_out.png" data-alt-src="/Express/images/NewIcon/log_out_down.png" />
							<div class="dropdown-content" id ="logoutDropD">
							<?php if($_SESSION["use_ldap_authentication"] == 0) { ?>
                			   <!-- <a href="/Express/reset_password.php" id="logout" title="Reset Password">Change Password</a> -->
                                           <a  class="systemConfiRedirectResetPassCls" id="logout" title="Reset Password">Change Password</a>
                			<?php } ?>							
							<!--<a href="/Express/index.php" id="logout" title="Logout">Logout</a> -->
                                                        <a  id="logout" class="systemConfiRedirectCls" title="Logout">Logout</a>
							</div>
						</div>

		<?php } ?>	
	 
	  
	  
<!-- <div class="dropdown">
  <button class="dropbtn">Dropdown</button>
  <div class="dropdown-content">
    <a href="#">Link 1</a>
    <a href="#">Link 2</a>
    <a href="#">Link 3</a>
  </div>
</div>-->

		
		<div class="imgToggleDiv"><img src="/Express/images/NewIcon/day.png" class="imgToggle" title="Day/Night"/></div>
		<div class="imgToggleDivHelp"><img src="/Express/images/NewIcon/icons_help.png" id="helpUrl" title="Help" /></div>
		
		<!-- Go To Enterprise main page -->
		<?php if ($_SESSION["superUser"] == "1" || $_SESSION["superUser"] == "3") { ?>
		<div class="homeIconDiv">
				<?php
				$show_enterprise_link = (isset($_SESSION["sp"]) && $_SESSION["sp"] != "" && $_SESSION["sp"] != "None") ? "inline-block" : "none";
			    ?>
				<a id="enterprise_main_page_link" style="display: <?php echo $show_enterprise_link; ?>" class="RedirectNavHeader">
				
				<!--<a id="enterprise_main_page_link" style="display: <?php // echo $show_enterprise_link; ?>" href="main_enterprise.php"> -->
				
				<img src="/Express/images/NewIcon/enterprise.png" class="returnToEnterpriseMainPage" border="0" alt="" title="Enterprise Main Page" onclick="modeEnabledLogo()"/></a>
		</div>
		<?php } ?>
		
		<!-- Go To main page -->
		<?php if(strpos($_SERVER['REQUEST_URI'], 'chooseGroup') == false && isset($_SESSION["groupId"])){ ?>
		<div class="homeIconDiv">
				<a id="main_page_link" class="RedirectNavHeader">
				<!--<a href = "main.php"> -->
				
				<img src="/Express/images/NewIcon/group_main.png" class="" border="0" alt="" title="Group Main Page" onclick="modeEnabledLogo()"/></a>
		</div>
		<?php } ?>
		
		<!-- Go To main page -->
		<?php if(isset($_SESSION["previousGroupId"])){ ?>
		<div id="goToPreviousGroup" class="GhomeIconDiv">
				<a id="main_page_link" class="RedirectNavHeader">
				<!--<a href = "main.php"> -->
				<img src="/Express/images/NewIcon/group_return.png" class="groupReturnToMainPage" border="0" alt="" title="Group Main Page"/></a>
		</div>
		<?php } ?>
		
                 <div class="homeIconDiv changeGroupIcon chooseGroupMainDiv">
                   <!-- <a id="chooseGroupModal" class="RedirectNavHeader"><img class="logoutWhiteIcon" src="/Express/images/NewIcon/change_groups.png" onmouseover="hover(this);" onmouseout="unhover(this);"></a>  -->
                
                  <?php if((($_SESSION["superUser"] == "1" || $_SESSION["superUser"] == "3") && (isset($_SESSION["sp"]) && $_SESSION["sp"] != "" && $_SESSION["sp"] != "None")) || ($showChangeGroup == "yes" && isset($_SESSION["groupId"]) && $_SESSION["groupId"]))
                    {?>       
                        <a id="chooseGroupModal" class="RedirectNavHeader">
                            <img class="logoutWhiteIcon chooseGroupImageIcon" src="/Express/images/NewIcon/change_groups.png" data-alt-src="/Express/images/NewIcon/change_groups_white.png">
                        </a>       
                         <div class="dropdown-content changeGroupToolTip" id ="chooseGroup"></div>	
                    <?php }?>
                         <!--{?>&nbsp;<a style="text-decoration: underline;" href="chooseGroup.php?change_group=true" onclick='modeEnabledLogo()'>Change Group</a>-->
			 <!-- <a style="text-decoration: underline;" href="/Express/index.php" id="logout">Logout</a> -->
		</div>
 </div>
				

           
	</div>
<!-- 160 server end -->
		
		<div class="blueSeparator"></div>
			
		    
				<?php } ?>
		<div id="helpDialog" class="dialogClass">
		<div id="dialogHtmlContent"></div>
		<div id="dialogVideoContent" style="text-align: center;"></div>
		</div>
		<div id="fail_over_msg_dialog"></div>
		<input type="hidden" value="" id="helpUrlDataID" />
		</div>
	<style>
	   #chooseGroup{margin-top: 10px !important;}
	   #chooseGroupDialogModal .modal-dialog{width: 100%;top: 23%;background-color:#EBECEC;}
        .groupAutoClass, .ui-autocomplete{z-index: 1;}
        .changeGroupToolTip{z-index: 100;} 
        .modal-dialog { position: relative; width: 100% !important;margin: 0px auto !important;}
   </style>
<!-- chooseGroupModal For New UI
  <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script> -->
  <script src="changeGroupDialog/js/changeGroupDialogBootstrap_3.3.7.js"></script>      
<script>
    $('[data-toggle="tooltip"]').tooltip();
   /* $('#chooseGroupModal').click(function (){
    	 $.ajax({url: "changeGroupDialog/index.php", success: function(result){
               $("#contentBody").html(result);
               $("#helpUrl").show();
               $("#chooseGroupDialogModal").modal('show'); 
    	}});
    }); */
    
</script>
   
<div class="modal fade" id="chooseGroupDialogModal" tabindex="-1" role="dialog" aria-labelledby="chooseGroupDialogModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
         
        <button type="button" class="close" id="cGModalBtn" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    	  <div id="contentBody"></div>		 
      </div>
      
    </div>
  </div>
</div>
  
<div id='systemConfigChangesDialog'></div>