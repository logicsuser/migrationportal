
<link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v15">
<script src="userMod/multiDragAndDrop.js"></script>
<script src="userMod/multiDragAndDropList.js"></script>
<style>
/*
table#userCustomTagsTable thead tr .headerSortDown, table#userCustomTagsTable thead tr .headerSortUp {
    background-color:#E6EEEE; !important;
}
table#userCustomTagsTable thead th {
    padding: 12px !important;
}

#userCustomTagsTable tbody tr {
    margin-top: 2px  !important;
    
}
table#userCustomTagsTable tbody tr td {
   padding: 9px 13px;
    border-left: 0px solid #fff !important;
    cursor: pointer;
}
.diaP12 input{width: 100% !important;height:30px !important ;max-height:30px !important ;padding: 9px 13px;}
*/
/* #availableUserServiceToAssign option.selected, #assignedUserService option.selected{
    background:#ffb200;
} */
/*#availableUserServiceToAssign, #assignedUserService{
    background-color:white !important;
} */ 
#availableUserServiceToAssign li.selected, #assignedUserService li.selected,#sortable_3 li.selected, #sortable_4 li.selected{
    background:#ffb200;
}
#availableUserServiceToAssign li:hover, #assignedUserService li:hover,#sortable_3 li:hover, #sortable_4 li:hover{
    background:#ffb200;
    color:white;
}
#availableUserServiceToAssign, #assignedUserService,#sortable_3, #sortable_4{
   padding:25px 0 0 0;
}
#availableUserServiceToAssign, #assignedUserService{background: #fff;}

    

#availableUserServiceToAssign li, #assignedUserService li,#sortable_3 li, #sortable_4 li{
	width: 100%;
	cursor:pointer;
	font-size:12px;
	font-weight:bold;
	text-align:center;
	padding:4px 4px 4px 3px;
	margin:0 0 7px -1px;
}
</style>

<?php

const customProfileTable = "customProfiles";
const customProfileName = "customProfileName";
const customProfileType = "deviceType";
const ExpressCustomProfileName = "%Express_Custom_Profile_Name%";
const ExpressPhoneProfileName = "%phone-template%";

// "activateCFA" => "Call Forwarding Always Active",
$customTagsOptions = array (
		"polycomVVX" => "Polycom VVX",
		"algo8028" => "Algo 8028",
		"algo8128" => "Algo 8128",
		"algo8180" => "Algo 8180",
		"algo8301" => "Algo 8301" 
);

require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once ("/var/www/html/Express/config.php");
// require_once ("/var/www/html/Express/userMod/analogUserData.php");
require_once("/var/www/html/Express/userMod/UserModifyDBOperations.php");
$modifyDBObj = new UserModifyDBOperations();
unset($_SESSION['usersProvisionedCustomTags']);
unset($_SESSION['assignCustomTagToUserForTable']);

function checkUserRegStatus($userId) {
    $isRegisteredUser = false;
    require_once ("/var/www/lib/broadsoft/adminPortal/UserOperation/UserRegistrationListRequestOperation.php");
    $regOp = new UserRegistrationOperation();
    $registrationStatus = $regOp->getRegistrationDetails($userId);
    $registrationStatusRes = UserRegistrationOperation::getParsedRegistrationGetResponse($registrationStatus->command);
    
    if( empty($registrationStatusRes["Error"]) ) {
        foreach ( $registrationStatusRes["Success"]["row"] as $key1 => $registrationValue)
        {
            $isRegisteredUser = true;
        }
    }
    
    return $isRegisteredUser;
}

function buildDeviceTypesSelection($deviceList, $devClass) {
    $str = "<select name=\"deviceType\" class=\"$devClass\" id=\"deviceTypeAnalog\" onchange=\"processDeviceTypeChange(this)\" >";
    $str .= "<option value=\"\">No Device</option>";
    foreach ( $deviceList as $key => $value ) {
        $selected = ($value == $_SESSION["userInfo"]["deviceType"]) ? "selected":"";
        $str .= "<option value=\"" . $value . "\"" .$selected.">" . $value . "</option>";
    }
    
    $str .= "</select>";
    return $str;
}

function isVdmDevice($db, $deviceType) {
	$vdmDeviceInfo = array ();
        //Code added @ 16 July 2019
        $whereCndtn = "";
        if($_SESSION['cluster_support']){
            $selectedCluster = $_SESSION['selectedCluster'];
            $whereCndtn = " AND clusterName ='$selectedCluster' ";
        }
        //End code
	$query = "SELECT deviceType, vdmTemplate from systemDevices where deviceType = '" . $deviceType . "' $whereCndtn ";
	$qwr = $db->query ( $query );
	while ( $r = $qwr->fetch () ) {
		$vdmDeviceInfo ["sysDevice"] = $r["deviceType"];
		$vdmDeviceInfo ["vdmTemplate"] = $r["vdmTemplate"];
	}
	return $vdmDeviceInfo;
}

//Code added @ 18 July 2018
function getDevicePortFromDeviceTable($db, $deviceType) {
	$deviceInfo = array ();
        //Code added @ 16 July 2019
        $whereCndtn = "";
        if($_SESSION['cluster_support']){
            $selectedCluster = $_SESSION['selectedCluster'];
            $whereCndtn = " AND clusterName ='$selectedCluster' ";
        }
        //End code
	$query = "SELECT ports, deviceType  from systemDevices where deviceType = '" . $deviceType . "' $whereCndtn ";
	$qwr = $db->query ( $query );
	while ( $r = $qwr->fetch () ) {
		$deviceInfo ["sysDevice"] = $r ["deviceType"];
		$deviceInfo ["ports"] = $r ["ports"];
	}
	return $deviceInfo;
}
//End code
function getMonitoredUserNumberAndExtension($users, $id) {
	foreach ( $users as $index => $user ) {
		if ($user ["id"] != $id) {
			continue;
		}
		$numExt = $user ["extension"];
		if ($user ["phoneNumber"] != "") {
			$userNum = explode ( "+1-", $user ["phoneNumber"] );
			$userNum = $userNum [1];
			$numExt = $userNum . "x" . $numExt;
		}
		return $numExt;
	}
	return "";
}
function getCustomProfiles($deviceType) {
	global $db;
	
	/*$query = "select " . customProfileName . " from " . customProfileTable . " where " . customProfileType . "='" . $deviceType . "' order by " . customProfileName;
	$results = $db->query ( $query );
	$profiles = array ();
	$i = 0;
	while ( $row = $results->fetch () ) {
		if (! in_array ( $profile = $row [customProfileName], $profiles )) {
			$profiles [$i ++] = $profile;
		}
	}*/
	$profiles = array ();
	$modifyDBObj = new UserModifyDBOperations();
	$customProfiles = $modifyDBObj->getAllCustomProfiles();
	if(in_array($deviceType, $customProfiles)){
	    $profiles = $modifyDBObj->getAllPhoneProfiles();
	    //print_r($profiles);
	    return $profiles;
	}
	
	/*if(count($modProfileArr) > 0){
	    foreach ($modProfileArr as $keyP => $valueP){
	        $select = $customProfile == $valueP ? "selected" : "";
	        echo "<option ".$select." value = ".$valueP.">".$valueP."</option>";
	    }
	}*/
	
	return $profiles;
}

function selectCustomProfile() {
    global $profiles, $customProfile, $systemDefaultTagVal, $phoneProfileSetOnDevice;
    $customProfileSelectedSetOnDevice = $phoneProfileSetOnDevice;
    $str = "<select name=\"customProfile\" class=\"modCustomProfile\" id=\"customProfile\" onchange=\"processPhoneProfileChange(this)\" >";
    //$str .= "<option value=\"None\"";
    //$str .= $customProfile == "" ? " selected" : "";
    //$str .= "></option>";
    if($customProfileSelectedSetOnDevice ==""){
        $customProfileSelectedSetOnDevice = $systemDefaultTagVal;
    }
    
    foreach ( $profiles as $key => $profile ) {
        if( in_array($profile[0], PhoneProfilesOperations::$restrictedPhoneProfiles) ) {
              continue;
        }
        $str .= "<option value=\"" . $profile[1] . "\"";
        $str .= $customProfileSelectedSetOnDevice == $profile[1] ? " selected" : "";
        $str .= ">" . $profile[0] . "</option>";
    }
    
    $str .= "</select>";
    return $str;
}

function getTagBundles($deviceType){
    $tagBundles = array();
    $modifyDBObj = new UserModifyDBOperations();
    $customProfiles = $modifyDBObj->getAllCustomProfiles();
    if(in_array($deviceType, $customProfiles)){
        $tagBundles = $modifyDBObj->getAllTagBundles();
        
        return $tagBundles;
    }
    return $tagBundles;
}
function selectTagBundle() {
    global $tagBundles;
    $str = "";
  //  $str = "<select name=\"modTagBundle[]\" id=\"modTagBundle\" multiple = \"multipe\" onchange=\"processTagBundleChange(this)\" >";
     $i = 0;
    if (count($tagBundles) > 0) {
        foreach ( $tagBundles as $keyName => $bundleName ) {

            $bundleID = "modTagBundle".$i;
            $selectedValForBundle = in_array ( $bundleName, $_SESSION ["userInfo"] ["modTagBundle"] ) ? " checked" : "";
    //   $str .= "<option $selectedValForBundle value=".$bundleName .">" . $bundleName . "</option>";
             $str .= "<div class='form-group'><input type='checkbox' $selectedValForBundle name='modTagBundle[]' value=".$bundleName." id=".$bundleID."><label for=".$bundleID."><span></span></label><label class='labelText'> ".$bundleName."</label></div>";
            $i++;
        }
    } 
    
   // $str .= "</select>";
    return $str;
}  

function displayLinePortDomain($lineport) {
	$str = "Lineport Domain";
	
	if ($lineport != "") {
		$expl = explode ( "@", $lineport );
		$str .= ": " . $expl [1];
	}
	
	echo $str;
}

function selectGroupDomain() {
	global $groupDefaultDomain, $groupDomains;
	global $proxyDomainType, $staticProxyDomain;
	
	$selectedDomain = "";
	switch ($proxyDomainType) {
		case "Static" :
			$selectedDomain = $staticProxyDomain;
			break;
		case "Default" :
			$selectedDomain = $groupDefaultDomain;
			break;
	}
	
	if ($selectedDomain == "") {
		$selectedDomain = $groupDefaultDomain;
	}
	
	for($i = 0; $i < count ( $groupDomains ); $i ++) {
		echo "<option value=\"" . $groupDomains [$i] . "\"";
		if ($groupDomains [$i] == $selectedDomain) {
			echo " selected";
		}
		echo ">" . $groupDomains [$i];
		if ($groupDomains [$i] == $groupDefaultDomain) {
			echo " (default domain)";
		}
		echo "</option>";
	}
}

function selectGroupDomainWithDevice($selectedDomain) {
    global $groupDefaultDomain, $groupDomains;
    global $proxyDomainType, $staticProxyDomain;
    
    for($i = 0; $i < count ( $groupDomains ); $i ++) {
        echo "<option value=\"" . $groupDomains [$i] . "\"";
        if ($groupDomains [$i] == $selectedDomain) {
            echo " selected";
        }
        echo ">" . $groupDomains [$i];
        if ($groupDomains [$i] == $groupDefaultDomain) {
            echo " (default domain)";
        }
        echo "</option>";
    }
}

function selectNetworkClassOfService($userNcos) {
	global $networkClassesOfService, $defaultNetworkClassOfService;
	
	$str = "";
	if (count ( $networkClassesOfService ) > 0) {
		foreach ( $networkClassesOfService as $key => $value ) {
			$str .= "<option value=\"" . $value . "\"";
			$str .= $userNcos == $value ? " selected" : "";
			$str .= ">" . $value;
			$str .= $defaultNetworkClassOfService == $value ? "   (default NCOS)" : "";
			$str .= "</option>";
		}
	} else {
		$str .= "<option value=\"\" selected>None</option>";
	}
	
	return $str;
}
function selectCustomTagsLookupOptions() {
	global $customTagsOptions;
	
	$str = "<option value=\"\" selected></option>";
	foreach ( $customTagsOptions as $model => $description ) {
		$str .= "<option value=\"" . $model . "\">" . $description . "</option>";
	}
	return $str;
}
function selectDeviceType($deviceList, $digitalDisable) {
	
	
	
	$str = "<option value=\"\">No Device</option>";
	foreach ( $deviceList as $key => $value ) {
		
		
		
		$selected = $value == $_SESSION ["userInfo"] ["deviceType"] ? "selected" : "";
		$str .= "<option value=\"" . $value . "\"" . $selected . ">" . $value . "</option>";
	}
	
	return $str;
}
function buildServicePacksSelection($availableServicePacks) {
	$str = "";
	foreach ( $availableServicePacks as $key => $value ) {
		$str .= "<option value=\"" . $value . "\"";
		$str .= in_array ( $value, $_SESSION ["userInfo"] ["servicePacksAssigned"] ) ? " selected" : "";
		$str .= ">" . $value . "</option>";
	}
	return $str;
}
function buildServicePacksSelectionAvailable($availableServicePacks) {
	$str = '';
	foreach ( $availableServicePacks as $key => $value ) {
		if(!in_array ( $value, $_SESSION ["userInfo"] ["servicePacksAssigned"] )){
			$str .= '<li class="ui-state-default" id="'.$value.'">'.$value.'</li>';
		}
	}
	return $str;
}
function buildServicePacksSelectionAssign($availableServicePacks) {
	$str = "";
	foreach ( $availableServicePacks as $key => $value ) {
		if(in_array ( $value, $_SESSION ["userInfo"] ["servicePacksAssigned"] )){
			$str .= '<li class="ui-state-default" id="'.$value.'">'.$value.'</li>';
		}
	}
	return $str;
}
function buildServicePacksSelectionAssignHidden($availableServicePacks) {
	$str = "";
	foreach ( $availableServicePacks as $key => $value ) {
		if(in_array ( $value, $_SESSION ["userInfo"] ["servicePacksAssigned"] )){
			$str .= $value.';';
		}
	}
	return $str;
}

function buildServiceSelectionAvailable($availableService) {
    $str = '';
    foreach ( $availableService as $servKey => $servValue ) {
      //  $str .= '<option id="'.$servValue.'" value="'.$servValue.'">'.$servValue.'</option>';
        $str .= '<li class="ui-state-default" id="'.$servValue.'">'.$servValue.'</li>';
        
    }
    return $str;
}
                
                
function buildUserServiceSelectionAssign($assignedService) {
    $str = "";
    foreach ( $assignedService as $assKey => $assValue ) {
      //  $str .= '<option id="'.$assValue.'" value="'.$assValue.'">'.$assValue.'</option>';
        
           $str .= '<li class="ui-state-default" id="'.$assValue.'">'.$assValue.'</li>';
    }
    return $str;
}
                
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/getGroupDomains.php");
require_once("/var/www/lib/broadsoft/adminPortal/getGroupClid.php");
require_once ("/var/www/lib/broadsoft/adminPortal/phoneProfiles/PhoneProfilesOperations.php");

$_SESSION ["userModNames"] = array (
		"lastName" => "Last Name",
		"firstName" => "First Name",
		"callingLineIdLastName" => "Calling Line ID Last Name",
		"callingLineIdFirstName" => "Calling Line ID First Name",
                "nameDialingFirstName" => "Name Dialing First Name",
                "nameDialingLastName" => "Name Dialing Last Name",
		"callingLineIdPhoneNumber" => "Calling Line ID Phone Number",
		"timeZone" => "Time Zone",
		"networkClassOfService" => "Network Class of Service",
                "addressLocation" => "Location",
		"emailAddress" => "Email Address",
		"department" => "Department",
		"addressLine1" => "Address",
		"addressLine2" => "Suite",
		"city" => "City",
		"stateOrProvince" => "State/Province",
		"zipOrPostalCode" => "Zip/Postal Code",
		"speedDial8" => "Speed Dial 8",
		"speedCode" => "Speed Dial 100",
		"isActive" => "Voice Messaging/Support",
		"alwaysRedirectToVoiceMail" => "Send All Calls to Voice Mail",
		"busyRedirectToVoiceMail" => "Send Busy Calls to Voice Mail",
		"noAnswerRedirectToVoiceMail" => "Send Unanswered Calls to Voice Mail",
		"serverSelection" => "Third-Party Voice Mail Server",
		"userServer" => "User Specific Mail Server",
		"mailboxIdType" => "Mailbox ID on Third-Party Voice Mail Platform",
		"mailboxURL" => "SIP-URI",
		"noAnswerNumberOfRings" => "Number of rings before greeting",
		"processing" => "Message Processing",
		"usePhoneMessageWaitingIndicator" => "Use Phone Message Waiting Indicator",
		"voiceMessageDeliveryEmailAddress" => "Voice Message Delivery Email Address",
		"sendVoiceMessageNotifyEmail" => "Notify by Email of New Message",
		"voiceMessageNotifyEmailAddress" => "Notify Email Address",
		"sendCarbonCopyVoiceMessage" => "Email Carbon Copy of Message",
		"voiceMessageCarbonCopyEmailAddress" => "Carbon Copy Email Address",
		"transferOnZeroToPhoneNumber" => "Transfer on Zero to Phone Number",
		"transferPhoneNumber" => "Transfer Phone Number",
		"resetWeb" => "Reset Web Password",
		"web" => "Web Password",
		"resetPortal" => "Reset Voice Mail Password",
		"portal" => "Voice Mail Password",
		"hotelHostisActive" => "Hoteling Host Active",
		"hotelisActive" => "Hoteling Guest Active",
		"hotelHostId" => "Hotel Host",
		"phoneNumber" => "Phone Number",
		"extension" => "Extension",
		"deviceType" => "Device Type",
		"customProfile" => "Custom Profile",
		"linePortDomain" => "Lineport Domain",
		"linePortDomainResult" => "Lineport Domain",
		"thirdPartyVoiceMail" => "Third-Party Voice Mail",
		"macAddress" => "MAC Address",
		"rebuildPhoneFiles" => "Rebuild Phone Files",
		"resetPhone" => "Reset the Phone",
    
        "rebuildPhoneFiles_sca" => "Rebuild Phone Files",
        "resetPhone_sca" => "Reset the Phone",
    
        "rebuildPhoneFiles_CustomTag" => "Rebuild Phone Files",
        "resetPhone_CustomTag" => "Reset the Phone",
		"resetDevice" => "Reset Device",
		"isRecallActive" => "Call Transfer Recall",
		"recallNumberOfRings" => "Number of Rings before Recall",
		"enableBusyCampOn" => "Enable Busy Camp",
		"busyCampOnSeconds" => "Busy Camp on Seconds",
		"useDiversionInhibitorForBlindTransfer" => "Use Diversion Inhibitor for Blind Transfer",
		"useDiversionInhibitorForConsultativeCalls" => "Use Diversion Inhibitor for Consultative Calls",
		"cfaActive" => "Call Forwarding Always",
		"cfaForwardToPhoneNumber" => "Call Forwarding Always Calls Forward to phone number / SIP-URI",
		"cfbActive" => "Call Forwarding Busy",
		"cfbForwardToPhoneNumber" => "Call Forwarding Busy Calls Forward to phone number / SIP-URI",
		"cfnActive" => "Call Forwarding No Answer",
		"cfnForwardToPhoneNumber" => "Call Forwarding No Answer Calls Forward to phone number / SIP-URI",
		"cfrActive" => "Call Forwarding Not Reachable",
		"cfrForwardToPhoneNumber" => "Call Forwarding Not Reachable Calls Forward to phone number / SIP-URI",
		"dnd" => "Do Not Disturb",
		"simultaneousRingIsActive" => "Simultaneous Ring Personal",
		"doNotRingIfOnCall" => "Do not ring my Simultaneous Ring Numbers if I'm already on a call",
		"simultaneousRingPhoneNumber" => "Simultaneous Ring Numbers",
		"monitoredUsers" => "Busy Lamp Fields",
		"scaUsers" => "Shared Call Appearances",
		"primary" => "Primary Line/Port",
        "deviceLinePortPrimary" => "Device Primary Line/Port",
		"useUserCLIDSetting" => "Use User Policy",
        "useUserDCLIDSetting" => "Use User Dialable Caller ID Policy",
                "criteriaName" => "Criteria Names",
                "criteriaNameDup" => "Criteria Names",   
		"clidPolicy" => "Non-Emergency Calls",
		"cpgGroupName" => "Call Pickup Group",
        "linePortTableOrderedData" => "Device Port Assignment",
		"groupCallPickup" => "Call Pickup Group Change",
		"servicePack" => "Service Pack(s)",
        "modTagBundle" => "Tag Bundle(s)",
		"uActivateNumber" => "Activate Number",
        "emergencyClidPolicy" => "Emergency Calls",
        "useGroupName" => "Use Group Name fro CLID",
        "allowAlternateNumbersForRedirectingIdentity" => "Allow Alternate Numbers for Redirecting Identity",
        "allowConfigurableCLIDForRedirectingIdentity" => "Allow Configurable CLID for Redirecting Identity",
        "blockCallingNameForExternalCalls" => "Block Calling Name for External Calls",
        "useMaxSimultaneousCalls" => "Enable Maximum Number of Concurrent Calls",
        "maxSimultaneousCalls" => "Number of Concurrent Calls",
        "useMaxSimultaneousVideoCalls" => "Enable Maximum Number of Concurrent Video Calls",
        "maxSimultaneousVideoCalls" => "Number of Concurrent Video Calls",
        "useMaxCallTimeForAnsweredCalls" => "Enable Maximum Duration for Answered Calls",
        "maxCallTimeForAnsweredCallsMinutes" => "Duration for Answered Calls",
        "useMaxCallTimeForUnansweredCalls" => "Enable Maximum Duration for Unanswered Calls",
        "maxCallTimeForUnansweredCallsMinutes" => "Duration for Unanswered Calls",
        "useMaxConcurrentRedirectedCalls" => "Enable Maximum Number of Concurrent Redirected Calls",
        "maxConcurrentRedirectedCalls" => "Number of Concurrent Redirected Calls",
        "useMaxConcurrentFindMeFollowMeInvocations" => "Enable Maximum Number of Concurrent Find Me/Follow Me Invocations",
        "maxConcurrentFindMeFollowMeInvocations" => "Number of Concurrent Find Me/Follow Me Invocations",
        "useMaxFindMeFollowMeDepth" => "Enable Maximum Find Me/Follow Me Depth ",
        "maxFindMeFollowMeDepth" => "Find Me/Follow Me Depth",
        "maxRedirectionDepth" => "Maximum Redirection Depth",
        "enableDialableCallerID" => "Dialable Caller ID",
        "useGroupDCLIDSetting" => "Incoming Caller ID",
        "useUserCallLimitsSetting" => "User/Group Call Limit Policy",
	"cpgGroupName" => "Call Pickup Group",
        "groupCallPickup" => "Call Pickup Group Change",
        "servicePack" => "Service Pack(s)",
    	"uActivateNumber" => "Activate Number",
        "mailBoxLimit" => "Mail Box Limit",
        "voiceMessaging" => "Voice Messaging",
        "sharedCallAppearance" => "Shared Call Appearance",
        "polycomPhoneServices" => "Polycom Phone Services",
        "ccd" => "Custom Contact Directory",
        "allowDepartmentCLIDNameOverride" => "Allow Department Clid Name Override",
        "cfnNumberOfRings" => "Number of Rings Before Forwarding",
        "cfaIsRingSplashActive" => "Play Ring Reminder",
        "dndRingSplash" => "Play Ring Reminder",
        //"deviceTypeResult" => "Device Type",
        "deviceIndexText" => "Device Instance",
        "portNumberText" => "Port Number",
        "portNumberSIP" => "Port Number",
        "userType" => "User Type",
		"webAccessPassword" => "Web Portal Password",
		"voiceMailPasscode" => "Voice Portal Passcode",
		"userPassword" => "Web Portal Password",
		"userPasscode" => "Voice Portal Passcode",
		"input-password" => "Password",
		"input-confirm-password" => "Confirm Password",
		"input-passcode" => "Passcode",
		"input-confirm-passcode" => "Confirm Passcode",
		"resetWebDefault" => "Reset to System Default",
		"resetPassDefault" => "Reset to System Default",
        "modTagBundleAssign" => "Tags To Be Added",
        "userPermissionsGroup" => "Calls within the business group",
        "userPermissionsLocal" => "Calls within the local calling area",
        "userPermissionsTollfree" => "Calls made to toll free numbers",
        "userPermissionsToll" => "Local toll calls",
        "userPermissionsInternational" => "International calls",
        "userPermissionsOperatorAssisted" => "Calls made with the chargeable assistance of an operator",
        "userPermissionsOperatorChargDirAssisted" => "Directory assistance calls",
        "userPermissionsSpecialService1" => "Special Services I (700 Number) calls",
        "userPermissionsSpecialService2" => "Special Services II",
        "userPermissionsPremiumServices1" => "Premium Services I (900 Number) calls",
        "userPermissionsPremiumServices2" => "Premium Services II (976 Number) calls",
        "userPermissionsCasual" => "1010XXX chargeable calls. Example: 1010321",
        "userPermissionsUrlDialing" => "Calls from internet",
        "userPermissionsUnknown" => "Unknown call type",
        "useCustomSettings" => "Use Custom Settings",
        "softPhoneDeviceName" => "Soft Phone Device Name",
        "assignedUserServicesVal" => "Assignment User Services",
        "userCallingLineIdBlockingOverride" => "Calling Line ID Blocking"
);
unset($_SESSION["devicesListToAdd"]);
unset($_SESSION["softPhoneListToAdd"]);
unset($_SESSION["counterPathDevices"]);
unset($_SESSION["softPhoneModData"]);
unset($_SESSION["softPhoneMod"]);
function checkCustomTagSpec() {
	global $db;
	
	$query = "select count(*) as count from customTagSpec";
	$results = $db->query ( $query );
	$row = $results->fetch ();
	
	return $row["count"];
}

if (isset ( $_POST ["searchVal"] )) {
	$postForData = $_POST ["searchVal"];
        $val = trim($postForData);
       
	$userId = isset ( $_SESSION ["autoFill"] [$val] ) ? $_SESSION ["autoFill"] [$val] : "";
	if ($userId == "") {
             //  $userId = $val; //code comment for ex-1420
             /* NEW CODE ADDED FOR RESTRICTED DIFFRENCE GROUP USER SEARCH RECORD */
                if(in_array($val,$_SESSION["autoFill"])){
                     $userId = $val;
                 }else{
                     $userId = "";
                 } 
                /*END CODE */
                if (isset ( $_POST ['userLname'] ) && ! empty ( $_POST ['userLname'] ) && isset ( $_POST ['userFname'] ) && ! empty ( $_POST ['userFname'] )) {
			$userInfoVal = " - " . $_POST ['userLname'] . ", " . $_POST ['userFname'];
		} else if (isset ( $_POST ['userLname'] ) && empty ( $_POST ['userLname'] ) && isset ( $_POST ['userFname'] ) && ! empty ( $_POST ['userFname'] )) {
			$userInfoVal = " - " . $_POST ['userFname'];
		} else if (isset ( $_POST ['userLname'] ) && ! empty ( $_POST ['userLname'] ) && isset ( $_POST ['userFname'] ) && empty ( $_POST ['userFname'] )) {
			$userInfoVal = " - " . $_POST ['userLname'];
		}
	}
	$_SESSION ["modUserId"] = $userId;
} else {
	$userId = $_SESSION ["modUserId"];
}
require_once ("/var/www/lib/broadsoft/adminPortal/arrays.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/users/VMUsersServices.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getUserInfo.php");

require_once ("/var/www/html/Express/userMod/UserCustomTagManager.php");

require_once ("/var/www/lib/broadsoft/adminPortal/DBLookup.php");

require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ocpFeatures/OCPOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getAllCustomContactDirectories.php"); //Code added @ 12 Sep 2018
require_once ("/var/www/html/Express/util/getAuthorizeDeviceTypes.php");
require_once ("/var/www/lib/broadsoft/adminPortal/devices/deviceTypeManagement.php");
$devobj = new DeviceTypeManagement();
$vdmArray = $devobj->getVDMTemplate();


//Code added @ 16 Jan 2019 regarding EX-1005
require_once ("/var/www/lib/broadsoft/adminPortal/util/CommanUtil.php");
$cmnUtilObj         = new CommanUtil();
$countryCodeArr     = $cmnUtilObj->getDefaultCountryCode();
$defaultCountryCode = $_SESSION["userInfo"]["countryCode"];
if($defaultCountryCode==""){
    if(isset($countryCodeArr['Success']) && !empty($countryCodeArr['Success'])){
        $defaultCountryCode = $countryCodeArr['Success']['defaultCountryCode'];
    }
}
//End code


$syslevelObj = new sysLevelDeviceOperations();

/*$deviceIsAudioCodeName = "false";
if (strpos ( $_SESSION ["userInfo"] ["deviceType"], 'AUDIO-MP' ) !== false) {
	$deviceIsAudioCodeName = "true";
}
$_SESSION["deviceIsAudioCodeName"] = $deviceIsAudioCodeName;
*/
$deviceIsAudioCodeName = "false";
$sipGatewayLookupDeviceType = new DBLookup ( $db, "systemDevices", "deviceType", "phoneType" );
if ($sipGatewayLookupDeviceType->get ( $_SESSION ["userInfo"] ["deviceType"] ) != "") {
    $deviceIsSipPhoneLook = $sipGatewayLookupDeviceType->get ( $_SESSION ["userInfo"] ["deviceType"] );
    if($deviceIsSipPhoneLook == "Analog"){
        $deviceIsAudioCodeName = "true";
    }
}
$_SESSION["deviceIsAudioCodeName"] = $deviceIsAudioCodeName;

$hasDevice = $_SESSION ["userInfo"] ["deviceName"] != "";
$customTags = new UserCustomTagManager ( $_SESSION ["userInfo"] ["deviceName"], $_SESSION ["userInfo"] ["deviceType"] );
$profiles = isset ( $_SESSION ["userInfo"] ["deviceType"] ) ? getCustomProfiles ( $_SESSION ["userInfo"] ["deviceType"] ) : array ();
$tagBundles = isset ( $_SESSION ["userInfo"] ["deviceType"] ) ? getTagBundles ( $_SESSION ["userInfo"] ["deviceType"] ) : array ();
$customProfile = isset ( $_SESSION ["userInfo"] ["deviceType"] ) ? $customTags->getCustomTagValue ( ExpressCustomProfileName ) : "";
$phoneProfileSetOnDevice = isset ( $_SESSION ["userInfo"] ["deviceType"] ) ? $customTags->getCustomTagValue ( ExpressPhoneProfileName ) : "";
$deviceTypeCheckForTagsTab = $_SESSION ["userInfo"] ["deviceType"];
/*
$customprofileArray = $modifyDBObj->getAllCustomProfiles();
$customTagDisplay = false;
if(in_array($deviceTypeCheckForTagsTab, $customprofileArray)){
    $customTagDisplay = true;
}*/


//Code added @ 01 Aug 2018
$userHasSCAFlag = "No";
$userAssignedServicePackArr = $_SESSION["userInfo"]["servicePacksAssigned"];
$scaServicePack             = $_SESSION["scaServicePacksArr"];
foreach($userAssignedServicePackArr as $k1 => $v1)
{
    if(in_array($v1, $scaServicePack))
    {
        $userHasSCAFlag = "Yes";
    }    
}

//End code


//Remove sim ring critera from temp session
unset($_SESSION['addSimRingCriteria']);
unset($_SESSION['editSimRingCriteria']);
unset($_SESSION['deleteSimRingCriteria']);
unset($_SESSION["simRingCriteriaList"]);
unset($_SESSION["userInfo"]['simRingCriteriaDetail']);

require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
$dOP = new DeviceOperations;
$dOP->ociVersion = $ociVersion;
$detail = $dOP->getDeviceTypesProfileDetail($_SESSION ["userInfo"] ["deviceType"]);
if($detail['Success']['staticLineOrdering'] == "false") {
    $spportPortType = "dynamic";
} else if($detail['Success']['staticLineOrdering'] == "true") {
    $spportPortType = "static";
}

$customTagDisplay = $modifyDBObj->checkDeviceTypeForCustomTagDisplay($deviceTypeCheckForTagsTab);

$setName = $syslevelObj->getTagSetNameByDeviceType($_SESSION ["userInfo"] ["deviceType"], $ociVersion);
$systemDefaultTagVal = "";
if($setName["Success"] != "" && !empty($setName["Success"])){
    $defaultSettags = $syslevelObj->getDefaultTagListByTagSet($setName["Success"]);
    $phoneTemplateName = "%phone-template%";
    if(array_key_exists($phoneTemplateName, $defaultSettags["Success"])){
        $systemDefaultTagVal = $defaultSettags["Success"]["%phone-template%"][0];
    }
   // print_r($defaultSettags);
}

if($_SESSION["userInfo"]["customProfile"] == "None" && $license["customProfile"] == "true"){
    $_SESSION["userInfo"]["customProfile"] = $systemDefaultTagVal;
}

$isVdm = false;
$vdmDeviceName = "";
$vdmDeviceTemplateName = "";
$vdmDeviceType = "";
if ($hasDevice && isset ( $_SESSION ["userInfo"] ["deviceType"] )) {
    $vdmDeviceInfo = isVdmDevice ( $db, $_SESSION ["userInfo"] ["deviceType"]);
    //echo "rajesh";print_r($vdmDeviceInfo);
    if($vdmDeviceInfo["vdmTemplate"] > 0){
        $isVdm = true;
    }
	//$isVdm = count ( $vdmDeviceInfo ) > 0;
	if ($isVdm) {
		$vdmDeviceName = $_SESSION ["userInfo"] ["deviceName"];
		$vdmDeviceTemplateName = $vdmArray[$vdmDeviceInfo ["vdmTemplate"]];
		$vdmDeviceType = $_SESSION ["userInfo"] ["deviceType"];
	}
}

$linePortDomain = $_SESSION ["userInfo"] ["linePort"];
$linePortDomain = explode ( "@", $linePortDomain );
$linePortDomain = isset ( $linePortDomain [1] ) ? $linePortDomain [1] : "";

$deviceIsSipPhone = false;
$sipGatewayLookup = new DBLookup ( $db, "systemDevices", "deviceType", "phoneType" );
if ($sipGatewayLookup->get ( $_SESSION ["userInfo"] ["deviceType"] ) != "") {
	$deviceIsSipPhone = $sipGatewayLookup->get ( $_SESSION ["userInfo"] ["deviceType"] ) != "Analog";
}
$isVMAllowed = $_SESSION["permissions"]["voiceManagement"] == "1";
// show alert if user not found, otherwise display page normally
if (isset ( $_SESSION ["userInfo"] ["error"] ) and $_SESSION ["userInfo"] ["error"] == "true") {
	?>
<script>
			alert("User not found");
		</script>
<?php
} else {
	require_once ("/var/www/lib/broadsoft/adminPortal/getAvailableNumbers.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/allNumbers.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/getHotelAvailable.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/getBlfUsers.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/getCallCenterUnavailableCodes.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/getStates.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/getTimeZones.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/getNetworkClassesOfService.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/getDepartments.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/getDevices.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/getThirdPartyVoiceMailSupport.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/getGroupCallPickupRequest.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/GroupCallPickupUsers.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupOperation.php");
        
        
        //Code @ 07 Sep 2018 EX-789
        require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
        $serviceObj  = new Services();
        $plycmServicePacksArr = $serviceObj->servicePacksHavePolycomPhoneServices($servicePacks);        
        $_SESSION['plycmServicePacksArr']  = $plycmServicePacksArr;
        $_SESSION["plycmServicePacksStr"]  = implode(",", $plycmServicePacksArr);
        $userServicesArr = $_SESSION['groupInfoData']['userServiceAuth'];
        //Code added @ 19 Sep 2018        
        if(empty($_SESSION['groupInfoData']['userServiceAuth'])){        
            $respoArr =  $serviceObj->GroupUserServiceGetAuthorizationListRequest($_SESSION["groupId"]);
        }      
        //End Code
        
        
        
        $userHasPolycomSPFlag = "No";   
        
        foreach($userAssignedServicePackArr as $k2 => $v2)
        {
            if(in_array($v2, $plycmServicePacksArr))
            {
                $userHasPolycomSPFlag = "Yes";
            }    
        }
        
        //Code  added @ 14 Sep 2018
        $returnResp = $syslevelObj->getSysytemDeviceTypeServiceRequest($_SESSION ["userInfo"]["deviceType"], $ociVersion);
                
        if($returnResp["Error"] == "" && empty($returnResp["Error"])){
	$respTmp = strval($returnResp["Success"]->supportsPolycomPhoneServices);
        }else{
                $respTmp = "false";
        }
        //End code
	
// 	print_r($deviceTypesAnalogGateways); exit;
	
	//if ($_SESSION ["groupServicesAuthorizationTable"] == "1") {
	if ($_SESSION["groupServicesAuthorizationTable"] !="" && $_SESSION["groupServicesAuthorizationTable"] == "1") {
		$grp1 = new GroupCallPickupUsers ( $userId );
		$usersGroupName = $grp1->findCallPickupGroupByUsername ();
		$_SESSION ["userInfo"] ["usersGroupName"] = $usersGroupName;
	}
	//$gcpObject = new GroupCallPickupUsers($userId);
	//$_SESSION["userInfo"]["cpgGroupName"]= $gcpObject->findCallPickupGroupByUsername();
	
	$a = 0;
	$info = new GroupOperation($_SESSION["sp"], $_SESSION["groupId"]);
	$basicInfo = $info->getGroupBasicInfo();
	$callingLineIdName = "";
	$callingLineIdDisplayPhoneNumber = "";
	if(isset($basicInfo["Success"]->callingLineIdName)){
	    $callingLineIdName = strval($basicInfo["Success"]->callingLineIdName);
	}
	if(isset($basicInfo["Success"]->callingLineIdDisplayPhoneNumber)){
	    $callingLineIdDisplayPhoneNumber = strval($basicInfo["Success"]->callingLineIdDisplayPhoneNumber);
	}
	
	?>
	
<script>
function changeUserValueFirstName(firstName)
{
         var bwRelVersion  = "<?php echo $bwVersion; ?>"; 
         if(bwRelVersion != 19){
            $("#nameDialingFirstName").val(firstName);
         }
}
function changeUserValueLastName(LastName)
{
         var bwRelVersion  = "<?php echo $bwVersion; ?>";
         if(bwRelVersion != 19){
            $("#nameDialingLastName").val(LastName);
         }	
} 

$(".checkLinePortRebuild").change(function(){
		$("#rebuildPhoneFiles_sca").prop("checked",true);
		$("#resetPhone_sca").prop("checked",true);

	});

var linePortTableOrderedData = "";
$(document).ready(function(){
	/* Make table element draggable. */
	//$('#deviceLinePortTable tbody').sortable();
	
	var linePortTableOrderedDataOnInitial = getLinePortTableOrderedData();
	$( "#deviceLinePortTable tbody" ).sortable({
        update: function( ) {
            linePortTableOrderedData = getLinePortTableOrderedData();
            if( linePortTableOrderedData == linePortTableOrderedDataOnInitial ) {
            	linePortTableOrderedData = "";
            }
           console.log(linePortTableOrderedData);
        }
    });

	$("#deviceLinePortTable").tablesorter();
    $("#scaLinePortTable").tablesorter();
    $('[data-toggle="tooltip"]').tooltip();

    columnIndexCustom = $('#userCustomTagsTable th:contains("Tag Name")').index();
    sortCustomTagTable(columnIndexCustom);
    if ("<?php  echo isset($_SESSION ["permissions"] ["deviceInfoReadOnly"]); ?>" && "<?php echo $_SESSION ["permissions"] ["deviceInfoReadOnly"]; ?>" == "1") {
        $("#phoneDevice_1 :input:not(#modTagBundle, #uActivateNumber)").prop("disabled", true);
        $("#phoneDevice_1:not('#modTagBundleId')").css("pointer-events", "none");
        $("#modTagBundle option").prop("disabled", true);
        $("#modTagBundleId").css("pointer-events", "auto");
        $("#phoneDevice_1 :input").css("background", "#e3e3e3");
    }

});

function getLinePortTableOrderedData() {
	debugger;
	var linePortUpdatedRowData = "";
	$("#deviceLinePortTable tbody tr").filter(function(){
		linePortUpdatedRowData += $(this).find('td:eq(1)').text() + ";";
	});
   console.log(linePortUpdatedRowData);
   return linePortUpdatedRowData;
}

var columnIndexCustom;
function sortCustomTagTable(columnIndexCustom) {
	$(document).find('#userCustomTagsTable').tablesorter({
	    sortList : [[ columnIndexCustom, 0 ]]
	});
}

function checkDefaultRebuitResetCheckBox() {
	var settings = {
            type: "POST",
            url: "userMod/customTagDo.php",
            data: {isChangesDoneForUserCustomTag : ""},
            async: false
        };
        $.ajax(settings).done(function(result) {
            result = result.trim();
            if (result.slice(0, 1) == "1") {
                alert( "Error: " + result.slice(1));
            } else {
				if(result.slice(1) == "MadeChanges") {
					$("#rebuildPhoneFiles_CustomTag").prop("checked", true);
					$("#resetPhone_CustomTag").prop("checked", true);
				} else {
					$("#rebuildPhoneFiles_CustomTag").prop("checked", false);
					$("#resetPhone_CustomTag").prop("checked", false);
				}
            }
            
        });
        
}

</script>
<style>
    .scaUserstable thead tr th{
        text-align: left !important;
        padding-left: 10px;
    }
    .scaUserstable tr td{
        text-align: left !important;
        padding-left: 4px;
    }   
/*
.ui-tooltip-content {    
    background-color: #d9edf7 !important;
    border-color: #bce8f1 !important;
    color: #31708f !important;
   }
   
.ui-tooltip {
    padding: 0px !important;
     border: none !important;
         
}
 
.averistar-bs3 .glyphicon {
    top: 2px !important;
 }
 
 label.labelText {
    padding-left: 12px !important;
    padding-bottom: 5px !important;
}

.subBanner.callingPlanText {
    font-size: 24px;
}
*/
</style>
 
		<script type="text/javascript" src="js/filereader.js"></script>
		<script>
                    
                    
                 //Code added for Sim Ring @ 25 Nov 2018 New UI
                 var simRingPos = "";
                    
                    //Code added @ 29 Oct 2018                 
                   var successGetList = function (data){
                                    //alert('Function Called Again ');
                                    var criteriaTableData   = "";
                                    var criteriaList        = new Array();
                                    var phoneNumberList     = new Array();
                                    var activeStatus        = "";
                                    var doNotRingIfOnCall   = "";
                                    var result              = JSON.parse(data);
                                    //criteriaList            = result.criteriaTableArr; 
                                    criteriaList            = result.criteriaTableTmpArr;
                                    phoneNumberList         = result.simultaneousRingNumberArr;
                                    activeStatus            = result.activeStatus;
                                    doNotRingIfOnCall       = result.doNotRingIfOnCall;
                                    var i = 0;                                   
                                    
                                    if(phoneNumberList!=undefined){
                                        $.each(phoneNumberList, function(k, val){                                            
                                            var phoneNumber     = val.phoneNumber;     
                                            var ansConfmReq     = val.answerConfirmationRequired;
                                            var phnTxtBoxId     = "#phoneNumber_"+i;
                                            
                                            if(ansConfmReq=="true"){
                                                var ansCnfmReqChk   = "#ansCnfmReq_"+i;
                                                $(ansCnfmReqChk).prop('checked', true);
                                                $(ansCnfmReqChk).prop('value', "true");
                                            }else{
                                                $(ansCnfmReqChk).prop('value', "false");
                                            }
                                                                                        
                                            $(phnTxtBoxId).val(phoneNumber);                                         
                                            i = i+1;
                                        })
                                    }                                    
                                    //console.log(criteriaList);
                                    if(criteriaList!=undefined){
                                            $.each(criteriaList, function(key, value){  
                                                 var criteriaName            = value.criteriaName;     
                                                 var timeSchedule            = value.timeSchedule;     
                                                 var isActive                = value.isActive;     
                                                 var clsFrmIncmgPhneList     = value.callsFromIncomingPhoneNumberList;     
                                                 var holidaySchedule         = value.holidaySchedule;   
                                                 var criteriaSimRingPrsl     = value.userSimRingPersonal;   
                                                 if(criteriaSimRingPrsl==="false"){
                                                     criteriaSimRingPrsl = "Yes";
                                                 }else{
                                                     criteriaSimRingPrsl = "No";
                                                 }
                                                 
                                                 if(timeSchedule==undefined){
                                                     timeSchedule    = "Every Day All Day";
                                                 }
                                                 if(holidaySchedule==undefined){
                                                     holidaySchedule  = "None";
                                                 }
                                                 //var timeSchedule    = timeSchedule.replace("(", " (");
                                                 //var holidaySchedule = holidaySchedule.replace("(", " (");
                                                 
                                                 if(timeSchedule!=undefined){
                                                    timeSchedule    = timeSchedule.replace("(", " (");
                                                 }
                                                 if(holidaySchedule!=undefined){
                                                    holidaySchedule = holidaySchedule.replace("(", " (");
                                                 }
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 criteriaTableData += "<tr class='tableTrAlignment' style='' id=''>";

                                                 criteriaTableData += "<td class='deleteCheckboxtd adminTableCol thSno header'>";

                                                 if(isActive=="true"){
                                                     criteriaTableData += "<input type='checkbox' id='" +criteriaName+ "'   class='deleteCheckboxSimRing' data-criteria-name='" +criteriaName+ "' name='criteriaName[]' value='" +criteriaName+ "' checked><label for='" +criteriaName+ "'><span></span></label>";
                                                 }else{
                                                     criteriaTableData += "<input type='checkbox' id='" +criteriaName+ "'  class='deleteCheckboxSimRing' data-criteria-name='" +criteriaName+ "' name='criteriaName[]' value='" +criteriaName+ "'> <label for='" +criteriaName+ "'><span></span></label>";
                                                 }
                                                 criteriaTableData += "</td>";

                                                 criteriaTableData += "<td class='adminTableCol header thsmall' onclick='javascript: showEditCriteriaDialog(this);'>"+ criteriaName +"</td>";
                                                 criteriaTableData += "<td class='adminTableCol header thsmall' onclick='javascript: showEditCriteriaDialog(this);'>"+ criteriaSimRingPrsl +"</td></td>";
                                                 criteriaTableData += "<td class='adminTableCol header thsmall' onclick='javascript: showEditCriteriaDialog(this);'>"+ timeSchedule +"</td></td>";
                                                 criteriaTableData += "<td class='adminTableCol header thsmall' onclick='javascript: showEditCriteriaDialog(this);'>"+ holidaySchedule +"</td></td>";
                                                 criteriaTableData += "<td class='adminTableCol header thsmall' onclick='javascript: showEditCriteriaDialog(this);'>"+ clsFrmIncmgPhneList +"</td>";                                                 

                                                 criteriaTableData += "</tr>"; 
                                            });
                                            
                                            $(".criteroiaListMessage").hide(); 
                                            $(".simRingCriteriaTable").show();
                                            $("#criteriaListTable").html(criteriaTableData);
                                            $("#simRingCriteriaList").tablesorter();
                                            

                                    }else{
                                            $(".simRingCriteriaTable").hide();
                                            $(".criteroiaListMessage").show(); 
                                            $(".criteroiaListMessage").html("No Criteria Available");                                            
                                    }

                                    $("#customConfigTagDeviceNameDiv").show();
                                    //$("#deviceCustomTagCongLoader").hide() ;
                        };

                        var errorMsg = function (error){	
                                //alert("error - "+error);
                                alert('Please wait a while some data loading is in process');
                        };
                   
                   
                   var getSimRingDataList = function (userId){                       
                                var userId = "<?php echo $_SESSION["userInfo"]['userId']; ?>";
                                var listShowAction = "Yes";
                           
                                $.ajax({
                                        method:"POST",
                                          url:"userMod/simRingCriteriaList.php",
                                          data:{"userId":userId, "listShowAction":listShowAction}
                                 }).then(successGetList,errorMsg);
                    };
                    //End code
                    
                    getSimRingDataList();
                     
                     
                     function showEditCriteriaDialog(el){
                         
                                    var criteriaName =  $(el).closest('tr.tableTrAlignment').find('.deleteCheckboxSimRing').data("criteria-name");
                                    $.ajax({
                                         type: "POST",
                                         url: "userMod/simRing/editSimRingRecord.php",
                                         data: {funcType:"getModifyListSimRing", criteriaNameVal: criteriaName},
                                         success: function(result) {
                                             simRingPos = "Update"; 
                                             var res = result.split("divBreak");
                                             //alert(res[1]);
                                             $("#simRingDialog").html(res[1]);
                                             $("#sim_ring_criteria_add_dialog").html('');
                                             $("#sim_ring_criteria_edit_dialog").html('');
                                             $("#simRingDialog").dialog("open");
                                         }
                                     });
                     }
                   //End Code 
                    
                    //$(document).on("click", "tbody#criteriaListTable tr.tableTrAlignment td", function(event) {
                    /*$(document).on("click", "table#simRingCriteriaList tbody tr.tableTrAlignment td", function(event) {
                        alert('Hi ');
                        if(event.target.type !=='checkbox'){
                               var criteriaName =  $(this).closest('tr.tableTrAlignment').find('.deleteCheckboxSimRing').data("criteria-name");
                                $.ajax({
                                         type: "POST",
                                         url: "userMod/simRing/editSimRingRecord.php",
                                         data: {funcType:"getModifyListSimRing", criteriaNameVal: criteriaName},
                                         success: function(result) {	
                                             alert('Hello ');
                                             simRingPos = "Update";
                                             //$("#sim_ring_criteria_add_dialog").html('');
                                             $("#simRingDialog").html(result);
                                             $("#simRingDialog").dialog("open");		               
                                             	
                                         }
                                     });

                        }
                       });*/
                    
                    var simRingAddDialogData = $("#sim_ring_criteria_add_dialog").html();
                    $("#addSimRingCriteriaBtn").click(function(){
                        simRingPos = "Create";
                        //var simRingResult = $("#sim_ring_criteria_add_dialog").html();
                        $("#simRingDialog").html(simRingAddDialogData);
                        $("#sim_ring_criteria_add_dialog").html('');
                        $("#simRingDialog").dialog("open");                        
                    });

                    /* end */
                    
                    /*$('#deleteSimRingCriteria').click(function(e) {
                     simRingPos = "Delete" ;
                     $("#simRingDialog").dialog("open");
                     $("#simRingDialog").html("Are you sure want to delete SimRing Criteria ?");	
                    });*/
                    
                    
                    var simRingCriteriaAction = "";
                    
                    $("#simRingDialog").dialog({
                        autoOpen: false,
                        width: 1000,
                        modal: true,

                        title: "Sim Ring Criteria Add/Modify",

                        position: { my: "top", at: "top" },
                        resizable: false,
                        closeOnEscape: false,
                        buttons: {
                             Create: function() {                                 
                                 var dataToSend = $("form#sim_ring_criteria_add_form").serializeArray();
                                 var pageUrl = "simRing/criteriaCheckData.php"; 
                                 $.ajax({
                                   type: "POST",
                                   url: "userMod/"+pageUrl,
                                   data: dataToSend,
                                   success: function(result)
                                   {
                                       result = result.trim();
                                       if (result.slice(0, 1) == 1)
                                       {
                                           $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                                       }
                                       else
                                       {

                                           $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                                       }
                                       simRingCriteriaAction ="addSimRingCriteria";
                                       $("#simRingDialog").dialog("close");

                                       $("#simRingDialogSuccess").html(result.slice(1)); //Need to add this dialog
                                       $("#simRingDialogSuccess").dialog("open");        //Need to add this dialog

                                       $(":button:contains('Complete')").show().addClass('subButton');
                                       $(":button:contains('Cancel')").show().addClass('cancelButton');
                                       $(":button:contains('More Changes')").hide();
                                       $(":button:contains('Return To Main')").hide();
                                       $(":button:contains('Create')").hide();
                                       //  getCustomTagList(deviceTypeNameValue);
                                    }
                            });  
                            },
                           Update: function() {
                                 //alert('Update Called');
                                 var dataToSendNew = $("form#sim_ring_criteria_edit_form").serializeArray();                                 
                                 var pageUrl = "simRing/modifyCriteriaCheckData.php"; 
                                 $.ajax({
                                   type: "POST",
                                   url: "userMod/"+pageUrl,
                                   data: dataToSendNew,
                                   success: function(result)
                                   {
                                       //alert('Update Succes Called');
                                       result = result.trim();
                                       if (result.slice(0, 1) == 1)
                                       {
                                           $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                                       }
                                       else
                                       {

                                           $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                                       }
                                       simRingCriteriaAction ="updateSimRingCriteria";
                                       $("#simRingDialog").dialog("close");

                                       $("#simRingDialogSuccess").html(result.slice(1)); //Need to add this dialog
                                       $("#simRingDialogSuccess").dialog("open");        //Need to add this dialog

                                       $(":button:contains('Complete')").show().addClass('subButton');
                                       $(":button:contains('Cancel')").show().addClass('cancelButton');
                                       $(":button:contains('More Changes')").hide();
                                       $(":button:contains('Return To Main')").hide();
                                       $(":button:contains('Create')").hide();
                                       //  getCustomTagList(deviceTypeNameValue);
                                    }
                                });  
                           },
                           Delete: function() { 
                                simRingPos = "DeleteSimRingCriteria";
                                $("#simRingDialog").dialog("close");                                
                                $("#simRingDialogSuccess").html("Are you sure want to delete SimRing Criteria ?");
                                $("#simRingDialogSuccess").dialog("open");
                                $(":button:contains('More Changes')").hide();
                           },
                           
                           
                            Close: function() {
                                $(this).dialog("close");

                            },                             
                            Cancel: function() {
                                $(this).dialog("close");
                            }

                        },
                        open: function() {
							  setDialogDayNightMode($(this));
                              $(".ui-dialog-buttonpane button:contains('Create')").button().hide();
                              $(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
                              $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
                              $(".ui-dialog-buttonpane button:contains('Close')").button().hide();
                              $(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
                              $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
                              $(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
                              

                            if(simRingPos == "Delete"){	    
                                    $(".ui-dialog-buttonpane button:contains('Confirm')").button().show().addClass('subButton');    
                                    $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
                            }      	
                            if(simRingPos == "Create"){    	    
                                    $(".ui-dialog-buttonpane button:contains('Create')").button().show().addClass('subButton');    
                                    $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');   
                            }
                            if(simRingPos == "Update"){	  
                                     $(".ui-dialog-buttonpane button:contains('Delete')").addClass('criteriaDelBtn');
                                    $(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass('deleteButton'); 
                                    $(".ui-dialog-buttonpane button:contains('Update')").button().show().addClass('subButton');  
                                    $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton'); 
                            }
                        }  
                    });
                    
                    $("#simRingDialogSuccess").dialog({
                        autoOpen: false,
                        width: 1000,
                        modal: true,
                        title: "Sim Ring Criteria Add/Modify",
                        position: { my: "top", at: "top" },
                        resizable: false,
                        closeOnEscape: false,
                        buttons: {  	  	
                                    Create: function() {
                            },
                            Complete: function() {      
                                var dataToSend = "";
                                if(simRingCriteriaAction=="addSimRingCriteria"){
                                    dataToSend = $("form#sim_ring_criteria_add_form").serializeArray();
                                }
                                if(simRingCriteriaAction=="updateSimRingCriteria"){
                                    dataToSend = $("form#sim_ring_criteria_edit_form").serializeArray();
                                }
                                $.ajax({
                                        method:"post",
                                        url:"userMod/simRing/manageSimRingAction.php",
                                        data: dataToSend,
                                    }).success(function(res){           
                                          $("#simRingDialog").dialog("close");
                                          $("#simRingDialogSuccess").dialog("close");
                                          $(":button:contains('Complete')").hide();
                                          $(":button:contains('Cancel')").hide();
                                          $("#simRingDialogSuccess").html(res);        
                                          $("#simRingDialogSuccess").dialog("open");
                                          //$(":button:contains('More Changes')").show();
                                          $(":button:contains('More Changes')").show().addClass('cancelButton');
                                          //$("#simRingDialogSuccess").append(returnLink);
                                  });   
                           },
                           Update: function() {


                           },
                           Confirm: function() {                             
                              var dataToSendNew = $("form#sim_ring_criteria_delete_form").serializeArray();    
                              $.ajax({
                                  method:"post",
                                  url:"userMod/simRing/manageSimRingAction.php",
                                  data: dataToSendNew,
                              }).success(function(res){
                                          simRingPos = 'deleteSuccess';
                                          $("#simRingDialog").dialog("close");
                                          $("#simRingDialogSuccess").dialog("close");                                          
                                          $(":button:contains('Confirm')").hide();
                                          $(":button:contains('Cancel')").hide();
                                          $("#simRingDialogSuccess").html(res);     
                                          $("#simRingDialogSuccess").dialog("open");
                                          $(":button:contains('More Changes')").show();
                                           $(":button:contains('More Changes')").show().addClass('cancelButton');
                                      //$("#simRingDialogSuccess").append(returnLink);
                                    });    
                           },

                            Close: function() {
                                $(this).dialog("close");

                            },
                            Cancel: function() {
                                $(this).dialog("close");
                            },
                           'More Changes': function() {
                                $(this).dialog("close");                                
                                getSimRingDataList();
                            },

                        },
                        open: function() {
							  setDialogDayNightMode($(this));
                              $(".ui-dialog-buttonpane button:contains('Create')").button().hide();
                              $(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
                              $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
                              $(".ui-dialog-buttonpane button:contains('Close')").button().hide();
                              $(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
                              $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();

                            if(simRingPos == "deleteSuccess"){
                                   $(".ui-dialog-buttonpane button:contains('More Changes')").button().show().addClass('cancelButton');
                                    $(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
                            }                            
                                
                            if(simRingPos == "DeleteSimRingCriteria"){
                                    $(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
                                    $(".ui-dialog-buttonpane button:contains('Confirm')").button().show().addClass('subButton');
                                    $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
                            }

                            if(simRingPos == "addSimRing"){
                                    $(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');
                                    $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
                            }
                        }  
                    });
                 //End Code
                  
                    
               //Code added @ 11 July 2018 Select Shared Call Appearance Button Based User Service Packs
               var polycomPhoneServiceIsAssigned = function(){
                    
               var deviceTypeDigitalVal = $("#deviceTypeDigital").val(); 
               if( typeof deviceTypeDigitalVal == "undefined"){
                    var deviceTypeDigitalVal = $("#deviceTypeDigitalDup").val();
               }
               //alert('deviceTypeDigitalVal - '+deviceTypeDigitalVal);
                        
               var settings = {
                    type: "POST",
                    url: "userMod/polycomPhoneService.php",
                    data: { deviceTypeDigital : deviceTypeDigitalVal},
                    async: false
               };

               $.ajax(settings).done(function(result) {
            	   if (result == "false") {
                       document.getElementById("polycomPhoneServicesNo").checked = true;
                       
                   } 
               });
               
        }
                 function setSharedCallAppearanceBtnBasedOnUserServicePack()
                 {
                     var userSelectedServicePacks = document.getElementById("servicePack").value;
                     var userUSPArr = userSelectedServicePacks.split(";");                     
                     var tmpUSPArrLgth = parseInt(userUSPArr.length);
                     
                     var scaServicePacksStr = "<?php echo (isset($_SESSION["scaServicePacksStr"]) ? $_SESSION["scaServicePacksStr"] : ""); ?>";
                     var scaSPArr        = scaServicePacksStr.split(",");
                     var scaSPArrLgth    = parseInt(scaSPArr.length);
                     var scaFoundStatus = false;  
                     
                     if( scaServicePacksStr != "")
                     {
                        for (var i = 0; i < tmpUSPArrLgth-1; i++)
                        {                            
                              for (var j = 0; j < scaSPArrLgth; j++) 
                              {                                   
                                   if (userUSPArr[i] == scaSPArr[j]) 
                                   {
                                       scaFoundStatus = true;
                                       break;
                                   }
                               }
                               if (scaFoundStatus) {
                                   break;
                               }
                        }
                    }
                    
                    if (scaFoundStatus) {
                        document.getElementById("sharedCallAppearanceYes").checked = true;
                    } else {
                        document.getElementById("sharedCallAppearanceNo").checked = true;
                    }
                }                    
               //setSharedCallAppearanceBtnBasedOnUserServicePack(); 
               //End Code
               
               
               //Code added @ 07 Sep 2018 Select Polycom phone service Button Based User Service Packs
               function setPolycomPhoneServiceBtnBasedOnUserServicePack()
                 {
                     var userSelectedServicePacks = document.getElementById("servicePack").value;
                     var userUSPArr = userSelectedServicePacks.split(";");                     
                     var tmpUSPArrLgth = parseInt(userUSPArr.length);
                     
                     var plycmServicePacksStr = "<?php echo (isset($_SESSION["plycmServicePacksStr"]) ? $_SESSION["plycmServicePacksStr"] : ""); ?>";
                     var plycmSPArr        = plycmServicePacksStr.split(",");
                     var plycmSPArrLgth    = parseInt(plycmSPArr.length);
                     var plycmFoundStatus = false;  
                     
                     if( plycmServicePacksStr != "")
                     {
                        for (var i = 0; i < tmpUSPArrLgth-1; i++)
                        {                            
                              for (var j = 0; j < plycmSPArrLgth; j++) 
                              {                                   
                                   if (userUSPArr[i] == plycmSPArr[j]) 
                                   {
                                       plycmFoundStatus = true;
                                       break;
                                   }
                               }
                               if (plycmFoundStatus) {
                                   break;
                               }
                        }
                    }
                    
                    if (plycmFoundStatus) {
                        document.getElementById("polycomPhoneServicesYes").checked = true;
                    } else {
                        document.getElementById("polycomPhoneServicesNo").checked = true;
                    }
                }                    
               //setPolycomPhoneServiceBtnBasedOnUserServicePack();
               //polycomPhoneServiceIsAssigned();
               //End code    
                    
                    
		function verifySelectedServicePackOptions(sel) {
	        var opts = [];
	        var opt;
	        var len = sel.options.length;
	        for (var i = 0; i < len; i++) {
	            opt = sel.options[i];

	            if (opt.selected) {
	                opts.push(opt.value);
	            }
	        }

	        var vmServicePacks = "<?php echo (isset($_SESSION["vmServicePacksFlat"]) ? $_SESSION["vmServicePacksFlat"] : ""); ?>";
	        var vmFound = false;
	        if ( vmServicePacks != "") {
	            vmServicePacks = vmServicePacks.split(",");

	            for (i = 0; i < opts.length; i++) {
	                for (var j = 0; j < vmServicePacks.length; j++) {
	                    if (vmServicePacks[j] == opts[i]) {
	                        vmFound = true;
	                        break;
	                    }
	                }
	                if (vmFound) {
	                    break;
	                }
	            }
	        }

	        if (vmFound) {
	            document.getElementById("voiceMessagingYes").checked = true;
	        } else {
	            document.getElementById("voiceMessagingNo").checked = true;
	        }
	    }

	    function AllowOutOfSpecTag(checkBox) {
			if(checkBox.checked) {
					$("#AllowOutOfSpecValue").val("");
					$("#allTagValuesDiv").hide();
					$("#AllowOutOfSpecValDiv").show();					
			} else {
				$("#AllowOutOfSpecValue").val("");
				$("#allTagValuesDiv").show();
				$("#AllowOutOfSpecValDiv").hide();
			}
					

	}

	    
			var buttonClick = "";
            var deviceName = "<?php echo $_SESSION["userInfo"]["deviceName"]; ?>";
            var hasDevice = deviceName != "";
            var vdmDeviceName = "<?php echo $vdmDeviceName;?>";
            var vdmDeviceType = "<?php echo $vdmDeviceType; ?>";
            var vdmType = "<?php echo $vdmDeviceTemplateName; ?>";

            var useGroupName = "";
            var callingLineIdName = "<?php echo $callingLineIdName; ?>";
            if(callingLineIdName){
                $("#useGroupName").prop("disabled", false);
            }else{
            	$("#useGroupName").prop("disabled", true);
            }

			$("#useGroupName").click(function(){

				if ($('#useGroupName').is(':checked')) {
					$("#allowDepartmentCLIDNameOverride").prop("disabled", false);
					
					
				}else{
					$("#allowDepartmentCLIDNameOverride").prop("disabled", true);
					$("#allowDepartmentCLIDNameOverride").prop("checked", false);
				}
			});

			var callingLineIdNameDisabled = "<?php echo $callingLineIdName; ?>";
			if(callingLineIdNameDisabled == ""){
				$("#emDisabled").prop("disabled", true);
				$("#nonEmDisabled").prop("disabled", true);
			}else{
				$("#emDisabled").prop("disabled", false);
				$("#nonEmDisabled").prop("disabled", false);
			}
            
            // Custom Tag dialog states: create, modify
            var customTagDialogState = "";

			// tooltip
			$(document).ready(function(){
				$('[data-toggle="tooltip"]').tooltip();   
			});

			$(function()
			{
                             
                    //Code added @ 02 July 2018 to select Service Pack if that have VM service
                               var redrawListToAddServicePack = function(ulId, servicePackWithVMServivce){                                        
					var liArray = [];
					var liNewArrayUsers = [];
					var liStr = "";
                                        
					$(ulId+" li").each(function(){
						var el = $(this);
						liArray.push(el.text());
					});
                                        liArray.push(servicePackWithVMServivce);
					
					var liNewArray = liArray.sort(insensitive);
					$(ulId).html('');
					for (i = 0; i < liNewArray.length; i++)
					{
						liStr += '<li class="ui-state-default" id="'+liNewArray[i]+'">'+liNewArray[i]+'</li>';
					}
					$(ulId).html(liStr);                                        
				}
                                
                                
                                var redrawListToDelServicePack = function(ulId, servicePackWithVMServivce){                                        
                                        var liArray = [];
					var liNewArrayUsers = [];
					var liStr = "";
                                        
					$(ulId+" li").each(function(){
						var el = $(this);
						liArray.push(el.text());
					});                                        
                                        
                                        var arrLength = parseInt(liArray.length);
                                        if(arrLength > 1)
                                        {                                            
                                            for( var j = arrLength; j--;){                                                
                                                if ( liArray[j] === servicePackWithVMServivce) 
                                                {                                                     
                                                    liArray.splice(j, 1); 
                                                }
                                             }
                                        }
                                        else
                                        {
                                            liArray.pop(servicePackWithVMServivce);                                            
                                        }
					var liNewArray = liArray.sort(insensitive);                                       
					$(ulId).html('');                                        
					for (i = 0; i < liNewArray.length; i++)
					{
						liStr += '<li class="ui-state-default" id="'+liNewArray[i]+'">'+liNewArray[i]+'</li>';
					}                                        
					$(ulId).html(liStr);                                        
				}

				$("#deviceTypeDigital").change(function(){
					polycomPhoneServiceIsAssigned();
				})
                
                                               
                                $("#voiceMessagingYes").click(function()
                                {
                                    //alert('Hello');
                                    var userServicePack = new Array();
                                    var allServicePack  = new Array();
                                    $('#sortable_4 li').each(function() {
                                        var userListData = $(this).attr('id');
                                        userServicePack.push(userListData);
                                    });
                                    
                                    $('#sortable_3 li').each(function() {
                                        var allListData = $(this).attr('id');
                                        allServicePack.push(allListData);
                                    });                                 
                                    
                                    $.ajax({
                                                type: "POST",
                                                url: "userMod/checkServicePackWithVMServiceOnUpdate.php",
                                                data: { userServicePack: userServicePack, allServicePack: allServicePack, act: "retrieveServicePackToAdd" },
                                                success: function(result) 
                                                {    
                                                        if(result!="NONE")
                                                        {
                                                            var servicePackWithVMServivce = result;
                                                            redrawListToAddServicePack('#sortable_4', servicePackWithVMServivce);
                                                            redrawListToDelServicePack('#sortable_3', servicePackWithVMServivce);
                                                            
                                                            var scaUsers = "";
                                                            var order = $("#sortable_4").sortable("toArray");
                                                            for (i = 0; i < order.length; i++)
                                                            {
                                                                    scaUsers += order[i] + ";";
                                                            }                                                            
                                                            $("#servicePack").val(scaUsers);
                                                        }
                                                                                                              
                                                }
                                        });
                                });                                 
                                
                                $("#voiceMessagingNo").click(function()
                                {
                                    //alert('Hello');
                                    var userServicePack = new Array();
                                    $('#sortable_4 li').each(function() {
                                        var userListData = $(this).attr('id');
                                        userServicePack.push(userListData);
                                    });                        
                                    
                                    $.ajax({
                                                type: "POST",
                                                url: "userMod/checkServicePackWithVMServiceOnUpdate.php",
                                                data: { userServicePack: userServicePack, act: "retrieveServicePackToDel" },
                                                success: function(result) 
                                                {                                                            
                                                        if(result!="NONE")
                                                        {                                                            
                                                            var servicePackWithVMService = result;                                                            
                                                            redrawListToDelServicePack('#sortable_4', servicePackWithVMService);
                                                            redrawListToAddServicePack('#sortable_3', servicePackWithVMService);
                                                            //alert('servicePackWithVMServivce - '+ servicePackWithVMServivce);
                                                            var scaUsers = ""; 
                                                            var order = $("#sortable_4").sortable("toArray");
                                                            for (i = 0; i < order.length; i++)
                                                            {
                                                                    scaUsers += order[i] + ";";
                                                            }
                                                            //alert('ScaUsers Del Update -  - '+scaUsers);
                                                            $("#servicePack").val(scaUsers);
                                                        }
                                                                                                              
                                                }
                                        });
                                });
                                //End Code
                                
                                
                                
                                //Code added @ 10 July 2018 
                                $("#sharedCallAppearanceYes").click(function()
                                {
                                    //alert('Hello');
                                    var userServicePack = new Array();
                                    var allServicePack  = new Array();
                                    $('#sortable_4 li').each(function() {
                                        var userListData = $(this).attr('id');
                                        userServicePack.push(userListData);
                                    });
                                    
                                    $('#sortable_3 li').each(function() {
                                        var allListData = $(this).attr('id');
                                        allServicePack.push(allListData);
                                    });                                 
                                    
                                    $.ajax({
                                                type: "POST",
                                                url: "userMod/checkServicePackWithSCAServiceOnUpdate.php",
                                                data: { userServicePack: userServicePack, allServicePack: allServicePack, act: "retrieveServicePackToAdd" },
                                                success: function(result) 
                                                {    
                                                        if(result!="NONE")
                                                        {
                                                            var servicePackWithSCAService = result;
                                                            redrawListToAddServicePack('#sortable_4', servicePackWithSCAService);
                                                            redrawListToDelServicePack('#sortable_3', servicePackWithSCAService);
                                                            
                                                            var scaUsers = "";
                                                            var order = $("#sortable_4").sortable("toArray");
                                                            for (i = 0; i < order.length; i++)
                                                            {
                                                                    scaUsers += order[i] + ";";
                                                            }                                                            
                                                            $("#servicePack").val(scaUsers);
                                                        }                                                                                                              
                                                }
                                        });
                                });
                                
                                $("#sharedCallAppearanceNo").click(function()
                                {
                                    //alert('Hello');
                                    var userServicePack = new Array();
                                    $('#sortable_4 li').each(function() {
                                        var userListData = $(this).attr('id');
                                        userServicePack.push(userListData);
                                    });                        
                                    
                                    $.ajax({
                                                type: "POST",
                                                url: "userMod/checkServicePackWithSCAServiceOnUpdate.php",
                                                data: { userServicePack: userServicePack, act: "retrieveServicePackToDel" },
                                                success: function(result) 
                                                {          
                                                        //alert(result);
                                                        if(result!="NONE")
                                                        {                                                            
                                                            var servicePackWithSCAService = result; 
                                                            
                                                            redrawListToDelServicePack('#sortable_4', servicePackWithSCAService);
                                                            redrawListToAddServicePack('#sortable_3', servicePackWithSCAService);
                                                            var scaUsers = "";
                                                            var order = $("#sortable_4").sortable("toArray");
                                                            for (i = 0; i < order.length; i++)
                                                            {
                                                                    scaUsers += order[i] + ";";
                                                            }
                                                            //alert('ScaUsers Del Update -  - '+scaUsers);
                                                            $("#servicePack").val(scaUsers);
                                                        }
                                                                                                              
                                                }
                                        });
                                });
                                //End code
                            
				
				$("#deviceIndexText").val($("#deviceIndex option:selected").text());
				$("#portNumberText").val($("#portNumber option:selected").text());

				$("#tabs").tabs();
				$("#endUserId").html("<?php echo $userId; ?>");

				$("#sortable1, #sortable2").sortable({
					placeholder: "ui-state-highlight",
					connectWith: ".connectedSortable",
					cursor: "crosshair",
					update: function(event, ui)
					{
						var monUsers = "";
						var order = $("#sortable2").sortable("toArray");
						for (i = 0; i < order.length; i++)
						{
							monUsers += order[i] + ";";
						}
						$("#monitoredUsers").val(monUsers);
						//$("#rebuildPhoneFiles_sca").prop("checked",true);
						//$("#resetPhone_sca").prop("checked",true);
					}
				}).disableSelection();

				$("#sortable_1, #sortable_2").sortable({
					placeholder: "ui-state-highlight",
					connectWith: ".connectedSortable",
					cursor: "crosshair",
					update: function(event, ui)
					{
						var scaUsers = "";                                                
						var order = $("#sortable_2").sortable("toArray");
						for (i = 0; i < order.length; i++)
						{
							scaUsers += order[i] + ";";
						}
						$("#scaUsers").val(scaUsers);

						$("#scaUsers").val(scaUsers);
						$("#rebuildPhoneFiles_sca").prop("checked",true);
						$("#resetPhone_sca").prop("checked",true);
  					}
				}).disableSelection();
				
				$("#sortable_3, #sortable_4").multisortableList({
					placeholder: "ui-state-highlight",
					//connectWith: ".connectedSortable",
                                        connectWith: "#sortable_3, #sortable_4",
					cursor: "crosshair",
					update: function(event, ui)
					{
						var scaUsers = "";
						var order = $("#sortable_4").sortable("toArray");
						for (i = 0; i < order.length; i++)
						{
							scaUsers += order[i] + ";";
						}
                                                //alert('scaUsers - '+scaUsers);
						$("#servicePack").val(scaUsers);
                                                
                                                
						/* setTimeout(function() {
							sortUnorderedList(event.target.id, "li");
                                                        getArrayFromLi('#sortable_3');
                                                        getArrayFromLi('#sortable_4');
						}, 2000);*/
                                                
                                                
						//reactRadioButtonsOnEvents();
					},
					stop : function(){
                                            
						getArrayFromLi('#sortable_3');
						getArrayFromLi('#sortable_4');
                                                reactRadioButtonsOnEvents();
					}
				}).disableSelection();

				var insensitive = function(s1, s2) {
					 var s1lower = s1.toLowerCase();
					 var s2lower = s2.toLowerCase();
					 return s1lower > s2lower? 1 : (s1lower < s2lower? -1 : 0);
				}

				//make new html for the ul tag for ordering the li data
				var getArrayFromLi = function(ulId){
					var liArray = [];
					var liNewArrayUsers = [];
					var liStr = "";
					
					$(ulId+" li").each(function(){
						var el = $(this);
						liArray.push(el.text());
					});
					
					var liNewArray = liArray.sort(insensitive);
					$(ulId).html('');
					for (i = 0; i < liNewArray.length; i++)
					{
						liStr += '<li class="ui-state-default" id="'+liNewArray[i]+'">'+liNewArray[i]+'</li>';
					}
					$(ulId).html(liStr);
                                        /*alert('liStr - '+liStr);
                                        alert('liNewArray - '+liNewArray);
                                        alert('liNewArrayUsers - '+liNewArrayUsers);
					console.log(liNewArrayUsers);*/
				}
                            getArrayFromLi("#assignedUserService") ;
                            getArrayFromLi("#availableUserServiceToAssign") ;
			/*	var verifySelectedServicePackOptionsNew = function(){
					var opts = [];
			        var opt;

					var servicePack = $("#servicePack");
					var servicePackVal = servicePack.val();
					var servicePackArray = servicePackVal.split(";");
					var opts = servicePackArray;
			        
			        /*var len = sel.options.length;
			        for (var i = 0; i < len; i++) {
			            opt = sel.options[i];

			            if (opt.selected) {
			                opts.push(opt.value);
			            }
			        }*/

			       // var vmServicePacks      = "<?php //echo (isset($_SESSION["vmServicePacksFlat"]) ? $_SESSION["vmServicePacksFlat"] : ""); ?>";
			       //var vmFound = false;
                                
                             //   var scaServicePacks     = "<?php //echo (isset($_SESSION["scaServicePacksStr"]) ? $_SESSION["scaServicePacksStr"] : ""); ?>";
                              //  var scaFound = false;
                                
                              //  var polycomServicePacks = "<?php //echo (isset($_SESSION["plycmServicePacksStr"]) ? $_SESSION["plycmServicePacksStr"] : ""); ?>";
                             //   var polycomFound = false;
                                
			    /*    if ( vmServicePacks != "") {
			            vmServicePacks = vmServicePacks.split(",");

			            for (i = 0; i < opts.length; i++) {
			                for (var j = 0; j < vmServicePacks.length; j++) {
			                    if (vmServicePacks[j] == opts[i]) {
			                        vmFound = true;
			                        break;
			                    }
			                }
			                if (vmFound) {
			                    break;
			                }
			            }
			        }

			        if (vmFound) {
			            document.getElementById("voiceMessagingYes").checked = true;
			        } else {
			            document.getElementById("voiceMessagingNo").checked = true;
			        }
                                
                                
                                //Code added @ 09 July 2018       
                                if ( scaServicePacks != "") 
                                {
                                    scaServicePacks = scaServicePacks.split(",");
                                    for (k = 0; k < opts.length; k++) {
                                        for (var l = 0; l < scaServicePacks.length; l++) {
                                            if (scaServicePacks[l] == opts[k]) {
                                                scaFound = true;
                                                break;
                                            }
                                        }
                                        if (scaFound) {
                                            break;
                                        }
                                    }
                                }
                                if (scaFound) {
                                    document.getElementById("sharedCallAppearanceYes").checked = true;
                                } else {
                                    document.getElementById("sharedCallAppearanceNo").checked = true;
                                }
                                //End Code
                                
                                
                                //Code added @ 07 Sep 2018       
                                if ( polycomServicePacks != "") 
                                {
                                    polycomServicePacks = polycomServicePacks.split(",");
                                    for (p = 0; p < opts.length; p++) {
                                        for (var q = 0; q < polycomServicePacks.length; q++) {
                                            if (polycomServicePacks[q] == opts[p]) {
                                                polycomFound = true;
                                                break;
                                            }
                                        }
                                        if (polycomFound) {
                                            break;
                                        }
                                    }
                                }
                                if (polycomFound) {
                                    document.getElementById("polycomPhoneServicesYes").checked = true;
                                } else {
                                    document.getElementById("polycomPhoneServicesNo").checked = true;
                                }
                                //End Code
                                
				}; */
				
				$("#dialogMU").dialog({
					autoOpen: false,
					width: 800,
					modal: true,
					position: { my: "top", at: "top" },
					resizable: false,
					closeOnEscape: false,
					buttons: {
						"Complete": function() {
							var processAction = buttonClick == "subButton" ? "User Modify" : "Delete User";
							pendingProcess.push(processAction);
							var dataToSend = $("form#userMod").serializeArray();
							console.log(linePortTableOrderedData);
							dataToSend.push({name: 'linePortTableOrderedData', value: linePortTableOrderedData });
							
							$("#dialogMU").html('');
							$("#dialogMU").html('Modifying the request....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
							//$("#loading2").show();
                            $.ajax({
								type: "POST",
								url: (buttonClick == "subButton" ? "userMod/userModDo.php" : "userMod/deleteUser.php"),
								data: dataToSend,
								success: function(result) {
									if(foundServerConErrorOnProcess(result, processAction)) {
			        					return false;
			                      	}
									$("#loading2").hide();
									$("#dialogMU").dialog("option", "title", "Request Complete");
									$('.ui-dialog-buttonpane').find('button:contains("Create")').addClass('createButton');
									$('.ui-dialog-buttonpane').find('button:contains("Delete")').addClass('deleteButton');
									$('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('closeButton');
									$('.ui-dialog-buttonpane').find('button:contains("Update")').addClass('updateButton');
                                    $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
                                    $(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
                                    if(buttonClick == "subButton") {
										$(".ui-dialog-buttonpane button:contains('More changes')").button().show();
									} else {
										$(".ui-dialog-buttonpane button:contains('More changes')").button().hide();
									}
									buttonClick ="";
                                    $(".ui-dialog-buttonpane button:contains('Return to Main')").button().show();
									$("#dialogMU").html(result);
								}
							});
						},
						"Cancel": function() {
							$(this).dialog("close");
						},
                        "More changes": function() {
                            $(this).dialog("close");
                            var userId = "<?php echo $userId; ?>";
                            $('html, body').animate({scrollTop: '0px'}, 300);
                            $("#loading2").show();
                            $("#userData2").hide();
                            $.ajax({
                                type: "POST",
                                url: "userMod/userInfo.php",
                                data: { userId: userId },
                                dataType: "html",
                                cache: false,
                                ajaxOptions: { cache: false },
                                success: function(result)
                                {
                                    $("#loading2").hide();
                                    $("#userData2").show();
                                    $("#userData2").html(result);
                                    $(".ui-dialog-buttonpane button:contains('Complete')").button().show();
                    				$(".ui-dialog-buttonpane button:contains('Cancel')").button().show();
                                }
                            });

                        },
                        "Return to Main": function() {
                            location.href="main.php";
						}
					},
                    open: function() {
						setDialogDayNightMode($(this));
                        $('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('createButton');
                        $('.ui-dialog-buttonpane').find('button:contains("Return to Main")').addClass('returnToMainButton');
                        $('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
                        $('.ui-dialog-buttonpane').find('button:contains("More changes")').addClass('moreChangesButton');

                    	 $('.ui-dialog-titlebar-close').addClass('ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only');
                    	 $('.ui-dialog-titlebar-close').append('<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text">close</span>');
                        $(".ui-dialog-buttonpane button:contains('Complete')").button().show();
                        $(".ui-dialog-buttonpane button:contains('Cancel')").button().show();
                        $(".ui-dialog-buttonpane button:contains('More changes')").button().hide();
                        $(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide();
                    }
				});


                // New Device Custom Tag Dialog
                // This dialog inside Custom Tags tab is opened to create new device Custom Tag
                // or to update or delete the existing Custom Tag.
                // ----------------------------------------------------------------------------
$("#customTagDialog").dialog({
                    autoOpen: false,
                    width: 500,
                    modal: true,
                    title: "Device Custom Tag",
                    position: { my: "top", at: "top" },
                    resizable: false,
                    closeOnEscape: false,
                    buttons: {
                        Create: function() {
                            var tagName = $("#tagShortDescription option:selected").val();
                            var tagNameDescription = "";

// 							if($('#AllowOutOfSpecCheckBox').is(':checked')) {
// 								tagNameDescription = tagName;
// 							} else {
								tagNameDescription = $("#tagShortDescription option:selected").text();
// 							}
							
                            var tagValue = "";

                            if($('#tagInputFieldValue').is(':visible')) {
                            	tagValue = $('#tagValue').val();
                            } else if ($('#tagSelectFieldValue').is(':visible')) {
                            	tagValue =  $('#tagSelectFieldValue option:selected').val() ;
                            } else if ($('#tagRangeFieldValue').is(':visible')) {
                                tagValue = parseInt($('#tagRangeInputField').val());

                            } else if ($('#AllowOutOfSpecValue').is(':visible')) {
                                tagValue = $('#AllowOutOfSpecValue').val();

                            }

                            var settings = {
                                type: "POST",
                                url: "userMod/customTagDo.php",
                                data: {tagName : tagName , tagValue: tagValue, tagNameDescription: tagNameDescription},
                                async: false
                            };
                            $.ajax(settings).done(function(result) {
                                result = result.trim();
                                if (result.slice(0, 1) == "1") {
                                    alert( "Error: " + result.slice(1));
                                } else {
                                    $("#customTagDialog").dialog("close");
                                    $(document).find("#customTagsTable").html(result.slice(1));
                                    sortCustomTagTable(columnIndexCustom);
                                    checkDefaultRebuitResetCheckBox();
                                }
                            });
                        },
                        Update: function() {
                            var modifiedTag = "";
                            if($("#tagValue").val() && $('#tagValue').is(':visible')){
                            	modifiedTag = $("#tagValue").val();
                            }else if($('select[name=tagValue]').val() && $('select[name=tagValue]').is(':visible')) {
                                modifiedTag = $('select[name=tagValue]').val();
                            }else if($("#tagRangeInputField").val() && $('#tagRangeInputField').is(':visible') ){
                            	modifiedTag = $("#tagRangeInputField").val();
                            } else if ($('#AllowOutOfSpecValue').is(':visible')) {
                            	modifiedTag = $('#AllowOutOfSpecValue').val();
                            }
                            var settings = {
                                type: "POST",
                                url: "userMod/customTagDo.php",
                                data: { modifyCustomTag: $('select[name=tagName]').val(), modifiedTag: modifiedTag},
                                async: false
                            };
                            $.ajax(settings).done(function(result) {
                                result = result.trim();
                                if (result.slice(0, 1) == "1") {
                                    alert( "Error: " + result.slice(1));
                                } else {
                                    $("#customTagDialog").dialog("close");
                                    $(document).find("#customTagsTable").html(result.slice(1));
                                    sortCustomTagTable(columnIndexCustom);
                                    checkDefaultRebuitResetCheckBox();
                                }
                            });
                        },
                        Delete: function() {
                            var settings = {
                                type: "POST",
                                url: "userMod/customTagDo.php",
                                data:  { deleteCustomTag: $('select[name=tagName]').val() },
                                async: false
                            };
                            $.ajax(settings).done(function(result) {
                                result = result.trim();
                                if (result.slice(0, 1) == "1") {
                                    alert( "Error: " + result.slice(1));
                                } else {
                                    $("#customTagDialog").dialog("close");
										$(document).find("#customTagsTable").html(result.slice(1));
                                    	sortCustomTagTable(columnIndexCustom);
                                    	checkDefaultRebuitResetCheckBox();
                                }
                            });
                        },
                        Close: function() {
                            $(this).dialog("close");
                        }
                    },
                    open: function() {
						setDialogDayNightMode($(this));
                        $('.ui-dialog-buttonpane').find('button:contains("Create")').addClass('createButton');
                        $('.ui-dialog-buttonpane').find('button:contains("Delete")').addClass('deleteButton');
                        $('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('closeButton');
                        $('.ui-dialog-buttonpane').find('button:contains("Update")').addClass('updateButton');
                        $(".ui-dialog-buttonpane button:contains('Create')").button('disable');
						 /*	$('.ui-dialog-titlebar-close').addClass('ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only');
						$('.ui-dialog-titlebar-close').append('<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text">close</span>');*/

                        if (customTagDialogState == "create") {
                            $(".ui-dialog-buttonpane button:contains('Create')").button().show();
                            $(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
                            $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
                        }
                        else if (customTagDialogState == "modify") {
                            $(".ui-dialog-buttonpane button:contains('Create')").button().hide();
                            $(".ui-dialog-buttonpane button:contains('Delete')").button().show();
                            $(".ui-dialog-buttonpane button:contains('Delete')").button("disable");
                            $(".ui-dialog-buttonpane button:contains('Update')").button().show();
                            $(".ui-dialog-buttonpane button:contains('Update')").button("disable");
                            $(".ui-dialog-buttonpane button:contains('Close')").button("disable");
                        }
                    }
                });


                // Invoked on clicking 'Add' button on Custom Tags tab
                // to create new custom tag for user's primary device
                // ------------------------------------------------------------------
                $("#newCustomTag").click(function(){
                    customTagDialogState = "create";
                    $.ajax({
                        type: "POST",
                        url: "userMod/customTagForm.php",
                        success: function(result) {
                        	customTagList();
                            $("#customTagDialog").dialog("open");
                            $(".ui-dialog").find("ui-widget-header").css("background", "darkgreen");
                            $("#customTagDialog").html(result);
                        }
                    });
                });

								// Invoked on clicking 'Add' button on Custom Tags tab
                $("#subButton").click(function() {
					buttonClick = this.id;
					var dataToSend = $("form#userMod").serializeArray();
					dataToSend.push({name: 'linePortTableOrderedData', value: linePortTableOrderedData });
					$.ajax({
						type: "POST",
						url: "userMod/checkData.php",
						data: dataToSend,
						success: function(result) {
							$("#dialogMU").dialog("open");
							if (result.slice(2, 3) > 0 || result.search("ff0000") > 0)
							{
								$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
							}
							else
							{
								$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
							}
							$("#dialogMU").html(result.slice(3));
						}
					});
				});

				$("#subButtonDel").click(function()
				{
					buttonClick = this.id;
					$("#dialogMU").dialog("open");
					$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
					$("#dialogMU").html("Are you sure you want to delete this user?");
				});

				$("#addSpeedDial").click(function() {
					<?php
						$dropdown = "";
						for ($i = 0; $i <= 99; $i++)
						{
							$pad = str_pad($i, 2, "0", STR_PAD_LEFT);
							$dropdown .= "<option value=\\\"" . $pad . "\\\">" . $pad . "</option>";
						}
					?>
                    $(".speedDials").append("<div class=\"row\">");
					$(".speedDials").append("<div class=\"col-md-3\"><div class=\"form-group\"> <div class=\"dropdown-wrap\"><select name=\"speedCode[]\" class=\"\" id=\"speedCode[]\"><?php echo $dropdown; ?></select></div></div></div>");
					$(".speedDials").append("<div class=\"col-md-3\"><div class=\"form-group\"><input type=\"text\" class=\"\" name=\"phone[]\" size=\"35\" maxlength=\"161\" id=\"phone[]\"></div></div>");
					$(".speedDials").append("<div class=\"col-md-3\"><div class=\"form-group\"><input type=\"text\" class=\"\" name=\"description[]\" size=\"25\" maxlength=\"25\" id=\"description[]\"></div></div>");
					$(".speedDials").append("<div class=\"col-md-3\"><div class=\"form-group\">&nbsp;</div></div>");
                    $(".speedDials").append("</div>");
					
					$(".speedDials").append($("#addSpeedDialDiv"));
					// echo "<input type=\"button\" value=\"Add Speed Dial\" id=\"addSpeedDial\">";
				});

				$(".remSpeedDial").click(function()
				{
					$(this).parent().parent().remove();
				});

				var var1 = document.getElementsByClassName("callService3");
		        var var2 = document.getElementsByClassName("callService4");
				
				if (var1.length == 0 && var2.length == 0){
					$(".callService2").css("float", "left");
					$(".callService5").css("width", "");

					$(".callService5").addClass("col span_10 leftMargin30px marginZero");
					//$(".removeClass")removeClass("leftMargin30px");
					$(".removeClass1").css("margin-left", "0");
					$(".callService5 .leftMargin30px").css("width", "100%");
					$(".callService2").insertAfter(".callService5");
				}
				
                                
                                
				 /* $("#availableUserServiceToAssign, #assignedUserService").multisortable({
					placeholder: "ui-state-highlight",
					//connectWith: ".connectedSortable",
                                        connectWith: "#availableUserServiceToAssign, #assignedUserService",
					cursor: "crosshair",
					update: function(event, ui)
					{
						var userServiceList = "";                                                
						var order = $("#assignedUserService").sortable("toArray");
						for (i = 0; i < order.length; i++)
						{
							userServiceList += order[i] + ";";
						}
						$("#assignedUserServicesVal").val(userServiceList);
						setTimeout(function() {
							sortUnorderedList(event.target.id, "li");
							reactRadioButtonsOnEvents();
			            }, 2000)
					}
				}).disableSelection();       */         
                                
                                
                                
                               $("#availableUserServiceToAssign, #assignedUserService").multisortableList({
					placeholder: "ui-state-highlight",
					//connectWith: ".connectedSortable",
                                        connectWith: "#availableUserServiceToAssign, #assignedUserService",
					cursor: "crosshair",
					update: function(event, ui)
					{
						var scaUsers = "";
                                                var userServiceList = "";  
						var order = $("#assignedUserService").sortable("toArray");
						for (i = 0; i < order.length; i++)
						{
							userServiceList += order[i] + ";";
						}
                                                //alert('scaUsers - '+scaUsers);
						//$("#assignedUserServicesVal").val(scaUsers);
                                                $("#assignedUserServicesVal").val(userServiceList);
                                      
						setTimeout(function() {
							sortUnorderedList(event.target.id, "li");
							reactRadioButtonsOnEvents();
			            }, 2000)
					 
						/* setTimeout(function() {
							sortUnorderedList(event.target.id, "li");
                                                        getArrayFromLi('#sortable_3');
                                                        getArrayFromLi('#sortable_4');
						}, 2000);*/
                                                
                                                
						//reactRadioButtonsOnEvents();
					},
					stop : function(){
                                            
						getArrayFromLi('#availableUserServiceToAssign');
						getArrayFromLi('#assignedUserService');
                                                reactRadioButtonsOnEvents();
					}
				}).disableSelection(); 

				var insensitive = function(s1, s2) {
					 var s1lower = s1.toLowerCase();
					 var s2lower = s2.toLowerCase();
					 return s1lower > s2lower? 1 : (s1lower < s2lower? -1 : 0);
				}

				//make new html for the ul tag for ordering the li data
				var getArrayFromLi = function(ulId){
                                   
					var liArray = [];
					var liNewArrayUsers = [];
					var liStr = "";
					
					$(ulId+" li").each(function(){
						var el = $(this);
						liArray.push(el.text());
					});
					
					var liNewArray = liArray.sort(insensitive);
					$(ulId).html('');
					for (i = 0; i < liNewArray.length; i++)
					{
						liStr += '<li class="ui-state-default" id="'+liNewArray[i]+'">'+liNewArray[i]+'</li>';
					}
					$(ulId).html(liStr);
                                        /*alert('liStr - '+liStr);
                                        alert('liNewArray - '+liNewArray);
                                        alert('liNewArrayUsers - '+liNewArrayUsers);
					console.log(liNewArrayUsers);*/
				}
                                
                                
                                
                                
                                
			});

			$("#file").click(function()
			{
				$("#file").val("");
			});


			function updateHiddenPair(selector) {
				// extract "NotHidden" portion from the name
                var pairedName = selector.id;
                pairedName = pairedName.slice(0, pairedName.length - 9);

                document.getElementById(pairedName).value = selector.checked;

                if(pairedName == "resetPhone" && selector.checked) {
                	$("#rebuildPhoneFilesNotHidden").prop("checked", true);
                	$("#rebuildPhoneFiles").val(selector.checked);
                }
            }


            function processDeviceTypeChange(selector) {

            	$("#portForSipDeviceDiv").hide();
				$("#portForSipDeviceSelect").html("");
				$("#supportPortNumberType").val("");
            	$("#deviceIndexText").val("");
        		$("#portNumberText").val("");

        		document.getElementById("linePortDomainDropdownWithDevice").style.display = "none";
            	document.getElementById("deviceTypeResult").value = selector.value;
            	var deviceType = document.getElementById("deviceTypeResult").value;
            	
                var hideResetPhoneFlags = false;

                //if (selector.value != "") {
                    document.getElementById("linePortDomainDropdownWithoutDevice").style.display = (selector.value != "") ? "block" : "none";
					document.getElementById("linePortDomainText").style.display = (selector.value != "") ? "block" : "none";
                    var e =  document.getElementById("linePortDomainWithoutDevice");
                    document.getElementById("linePortDomainResult").value = e.options[e.selectedIndex].value;
                //} else {

                //}

                var analogFlag = "";

                //get Custom Profile of a new device type
                var settings = {
                    type: "POST",
                    url: "userMod/customTagDo.php",
                    data: { deviceType: selector.value },
                    async: false
                };
                $.ajax(settings).done(function(result) {
                    result = result.trim();
                    if (result.slice(0, 1) == "1") {
                        alert( "Error: " + result.slice(1));
                    } else {
                        // Extract device analog flag status ('0' or '1')
                        result = result.slice(1);
                        analogFlag = result.slice(0,1);
                        $("#customProfile").html(result.slice(1));

                        var checkValue = analogFlag != "1" ? "true" : "false";

                        $("#rebuildPhoneFiles").attr("value", checkValue);
                        $("#resetPhone").attr("value", checkValue);

                        document.getElementById("phoneResetControls").style.display = "block";
                        $("#rebuildPhoneFilesNotHidden").attr("checked", checkValue);
                        $("#resetPhoneNotHidden").attr("checked", checkValue);

                        // Hide/show 'Rebuild Phone Files' and 'Reset the Phone' flags based on device type
                        document.getElementById("phoneResetControls").style.display = analogFlag == "1" ? "none" : "block";
                        
                    /*    var elements = document.getElementById("modTagBundle").options;

                        for(var i = 0; i < elements.length; i++){
                          elements[i].selected = false;
                        } */

                        $(".modCustomProfile").val("");
                        getTagBundlesOnDeviceChange(selector.value);
                        
                    }
                });
                checkDeviceSupports(deviceType);
            }

            function checkDeviceSupports(deviceType) {
            	var userType = $("#userTypeResult").val();
        	    var settings = {
        	            type: "POST",
        	            url: "userAdd/checkDeviceSupports.php",                                            //TODO: Rename this file as it is also used to process devices other than analog
        	            data: { action: "checkDeviceSupport", deviceType: deviceType },
        	            async: false
        	        };

        	        $.ajax(settings).done(function(result)
        	        {
        	            if(userType == "analog") {
        	            	if(result.indexOf("dynamic") != "-1"){
        	            		$("#supportPortNumberType").val("dynamic");
        	                	$("#deviceIndexGroupMod").hide();
                    			$("#portNumberGroupMod").hide();
                				$("#deviceIndex").val("");
                				$("#portNumber").val("");
        	                } else {
        	                		createDeviceInstance(deviceType);
        							$("#portForSipDeviceDiv").hide();
        							$("#portForSipDeviceSelect").html("");
        	                }
        	             }

        	            else if(userType == "digital") {
        	            	if(result.indexOf("static") != "-1"){
        	            		$("#supportPortNumberType").val("static");
        	                	portDropdownForSIP(deviceType, userType);
        	                	$("#portForSipDeviceDiv").show();
        	                	//$("#macAddressShowHide").addClass("addWithPortNumber").removeClass("addMaxWithFull");
        	                } else {
        	                	$("#portForSipDeviceDiv").hide();
        						$("#portForSipDeviceSelect").html("");
        	                	//$("#macAddressShowHide").addClass("addMaxWithFull").removeClass("addWithPortNumber");
        	                }
        	             }
        	            
        	        });
            }

            function portDropdownForSIP(deviceType, userType) {
            	var phoneNumber = $("#phoneNumber").val();
                var extension = $("#extension").val();
                var settings = {
                    type: "POST",
                    url: "userAdd/analogUserDo.php",
                    data: { actionModule: "userModify", module: "buildPortListForDigital", deviceType: deviceType, userType : userType, phoneNumber: phoneNumber, extension:  extension},
                    async: false
                };

                $.ajax(settings).done(function(result)
                {
                    result = result.trim();
                    if (result.slice(0, 1) == "1") {
                        alert( "Error: " + result.slice(1));
                    } else {
                        $("#portForSipDeviceSelect").html(result.slice(1));
                    }
                });
            }
            
            function createDeviceInstance(deviceType) {
            	if ($("#userTypeResult").val() == "analog")
                {

                    		if (deviceType != "") {

                    			$("#deviceIndexGroupMod").show();
                        		$("#portNumberGroupMod").show();
                        		$("#portNumberDivMod").html("<select name=\"portNumber\" id=\"portNumber\"><option value=\"\"></option></select>");
                        		
                        		$("#deviceIndex").prop('disabled',false);
                				$("#portNumber").prop('disabled',false);
                				
                    		    var settings = {
                    		        type: "POST",
                    		        url: "userAdd/analogUserDo.php",
                    		        data: { module: "buildIndexList", deviceType: deviceType },
                    		        async: false
                    		    };
                    		    
                    		    $.ajax(settings).done(function(result)
                    		    {
                    		        result = result.trim();
                    		        if (result.slice(0, 1) == "1") {
                    		            alert( "Error: " + result.slice(1));
                    		        } else {
                    		            $("#deviceIndexDivMod").html(result.slice(1));
                    		        }
                    		    });
                    		} else {
                    			$("#deviceIndexGroupMod").hide();
                    			$("#portNumberGroupMod").hide();
                				$("#deviceIndex").val("");
                				$("#portNumber").val("");
                    		}
                }
            }
            
            function getTagBundlesOnDeviceChange(deviceType){
                debugger;
            	var settings = {
                        type: "POST",
                        url: "userMod/customTagDo.php",
                        data: { deviceTypeForTagBundle: deviceType },
                        async: false
                    };
                    $.ajax(settings).done(function(result) {
                        result = result.trim();
                        if (result.slice(0, 1) == "1") {
                            alert( "Error: " + result.slice(1));
                        } else {
                            // Extract device analog flag status ('0' or '1')
                            result = result.slice(1);
                            analogFlag = result.slice(0,1);
                           if(result=="1" || result.slice(1) == "" ) {
                                  document.getElementById("deviceMngmtTagBundleDiv").style.display = "none";
                            } else {
                            	$("#modTagBundleId").html(result.slice(1));
                               	document.getElementById("deviceMngmtTagBundleDiv").style.display = "block";  
                            }
                        	
                        }
                    });
            }


            function processPhoneProfileChange(selector) {
                if (document.getElementById('phoneResetControls').style.display == "block") {
                    $("#rebuildPhoneFiles").attr("value", "true");
                    $("#resetPhone").attr("value", "true");
                    $("#rebuildPhoneFilesNotHidden").attr("checked", true);
                    $("#resetPhoneNotHidden").attr("checked", true);
                }
                //var elements = document.getElementById("modTagBundle").options;

                /*for(var i = 0; i < elements.length; i++){
                  elements[i].selected = false;
                }*/
			}

            function processTagBundleChange(selector) {
                if (document.getElementById('phoneResetControls').style.display == "block") {
                    $("#rebuildPhoneFiles").attr("value", "true");
                    $("#resetPhone").attr("value", "true");
                    $("#rebuildPhoneFilesNotHidden").attr("checked", true);
                    $("#resetPhoneNotHidden").attr("checked", true);
                }
			}


			function processCustomTagLookupChange(selector) {
                var selectedType = selector.options[selector.selectedIndex].value;
                if( selectedType != "") {
                    $("#customTagsOperations").hide();
                    $("#customTagsLookupTableDiv").show();

                    //Create lookup table for a selected value
                    var settings = {
                        type: "POST",
                        url: "userMod/customTagDo.php",
                        data: { lookupType: selectedType },
                        async: false
                    };
                    $.ajax(settings).done(function(result) {
                        result = result.trim();
                        if (result.slice(0, 1) == "1") {
                            alert( "Error: " + result.slice(1));
                        } else {
                            // Extract device analog flag status ('0' or '1')
                            result = result.slice(1);
                            //analogFlag = result.slice(0,1);
                            $("#customTagsLookupTable").html(result);
							$("#customTagsLookupTable").tablesorter();
							//$("#customTagsLookupTable").append(result.slice(1));

                            /*
                             var checkValue = analogFlag != "1" ? "true" : "false";

                             $("#rebuildPhoneFiles").attr("value", checkValue);
                             $("#resetPhone").attr("value", checkValue);

                             document.getElementById("phoneResetControls").style.display = "block";
                             $("#rebuildPhoneFilesNotHidden").attr("checked", checkValue);
                             $("#resetPhoneNotHidden").attr("checked", checkValue);

                             // Hide/show 'Rebuild Phone Files' and 'Reset the Phone' flags based on device type
                             document.getElementById("phoneResetControls").style.display = analogFlag == "1" ? "none" : "block";
                             */
                        }
                    });
                }
                else {
                    $("#customTagsLookupTable").empty();
                    $("#customTagsLookupTableDiv").hide();
                    if (hasDevice) {
                        $("#customTagsOperations").show();
                    }
                }

            }


            function selectLinePortDomain(selector) {
                document.getElementById("linePortDomainResult").value = selector.options[selector.selectedIndex].value;
            }


            // ----------------
            // Tag Row Handlers
            // ----------------

            // Tag selector handler
            // This handler opens dialog form with information of a tag row that
            // has been clicked
            // -----------------------------------------------------------------
            
            // Row highlighter
            // Tag selector handler
            // This handler opens dialog form with information of a tag row that
            // has been clicked
            // -----------------------------------------------------------------
            function evalRow(row) {
                customTagDialogState = "modify";
                var tagType = typeof row.dataset.tagtype != "undefined" ? row.dataset.tagtype : "";
                var customTagName = row.id;
                var customTagValueId = "userCustomTagValue" + customTagName;
                var customTagValue = document.getElementById(customTagValueId).textContent;
                if(tagType == "In-Spec") {
                	$.ajax({
                        type: "POST",
                        url: "userMod/customTagForm.php",
                        data: { customTagName: customTagName, customTagValue: customTagValue },
                        success: function(result) {
                            $("#customTagDialog").dialog("open");
                            $(".ui-dialog").find("ui-widget-header").css("background", "darkgreen");
                            $("#customTagDialog").html(result);
                            //$(":button:contains('Update')").removeAttr("disabled").removeClass("ui-state-disabled");
                            //$(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
                            //$(":button:contains('Close')").removeAttr("disabled").removeClass("ui-state-disabled");
                            customTagListModify(customTagName, customTagValue);
                        }
                    });
                } else {
                	customTagName = customTagName.substr(1).slice(0, -1);
                	$("#createCustomTagForm").find("#user_tagName").addClass("disableInput");
                	$("#createCustomTagForm").find("#user_tagName").prop('disabled', true);
                	
                	$("#createCustomTagForm").find("#user_tagName").val(customTagName);
                   	$("#createCustomTagForm").find("#user_tagValue").val(customTagValue);
                   	$("#createCustomTagForm").dialog("open");
                    $(":button:contains('Update')").show();
                    $(":button:contains('Delete')").show();
                }
                
            }

            // Row highlighter
            // This handler highlights a row on mouse-over action
            // --------------------------------------------------
            function highlightRow(row, highlight) {
               // row.style.backgroundColor = highlight ? "#dcfac9" : "white";
            }


            function handle_files(files)
			{
				for (i = 0; i < files.length; i++)
				{
					file = files[i];
					console.log(file);
					var reader = new FileReader();
					ret = [];
					reader.onload = function(e)
					{
						data = e.target.result;
						$.ajax({
							type: "POST",
							url: "userMod/parseCSV.php",
							data: { data: data },
							success: function(result)
							{
								$("#csvData").html(result);
							}
						});
					}
					reader.onerror = function(stuff)
					{
						console.log("error", stuff);
						console.log(stuff.getMessage());
					}
					reader.readAsText(file); //readAsdataURL
				}
			}

            	function customTagList(){
            		var userDeviceName = "<?php echo $userInfo['deviceType']; ?>";
            		var html = "<option value=''>None</option>";
            		//var tagOptionValue = "<option value=''>None</option>";
            		$.ajax({
            			type: "POST",
            			url: "userMod/customTag.php",
            			data: {deviceType : userDeviceName},
            			success: function(result) {
            				if (result.slice(0, 1) == "1") {
                                alert( "Error: " + result.slice(1));
                            } else {
                            	result = result.slice(1);
                            }
            				var obj = jQuery.parseJSON(result);
            				for (var i = 0; i < obj.length; i++) {
            					html += '<option title="'+obj[i].description+'" value="'+obj[i].tagName+'" tagType="'+obj[i].tagType+'" id="'+obj[i].id+'">' + obj[i].shortDescription + '</option>';
            					$("#tagShortDescription").html(html);
            				}
            				$('#tagInputFieldValue').hide();
                			$('#tagSelectFieldValue').hide();
                			$('#tagRangeFieldValue').hide();
            				$("#tagShortDescription").change(function() {
            					$("#tagOptions").html("");
                    			var tagType = $(this).children(":selected").attr("tagType");
                    			var id = $(this).children(":selected").attr("id");
                    			if(tagType == 0){
                        			$('#tagInputFieldValue').show();
                        			$('#tagSelectFieldValue').hide();
                        			$('#tagRangeFieldValue').hide();
                        			inputData = '<input type = "text" id="tagValue" name="tagValue" onkeyup="evalTagDialog(this);" />';
                        			$("#customTagInputFieldValue").html(inputData);
                        		}else if(tagType == 1){
                        			$('#tagInputFieldValue').hide();
                        			$('#tagRangeFieldValue').hide();
                        			$('#tagSelectFieldValue').show();
                        			var tagOptionValue = "<option value=''>None</option>";
                        			for (var i = 0; i < obj.length; i++) {
                            			if(obj[i].id == id){
                                			if(obj[i].option1Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option1Value+'">' + obj[i].option1Description + " - "+obj[i].option1Value+ '</option>';
                                			}
                                			if(obj[i].option2Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option2Value+'">' + obj[i].option2Description + " - "+obj[i].option2Value+  '</option>';
                                			}
                                			if(obj[i].option3Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option3Value+'">' + obj[i].option3Description + " - "+obj[i].option3Value+  '</option>';
                                			}
                                			if(obj[i].option4Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option4Value+'">' + obj[i].option4Description + " - "+obj[i].option4Value+  '</option>';
                                			}
                                			if(obj[i].option5Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option5Value+'">' + obj[i].option5Description + " - "+obj[i].option5Value+  '</option>';
                                			}
                                			if(obj[i].option6Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option6Value+'">' + obj[i].option6Description + " - "+obj[i].option6Value+  '</option>';
                                			}
                                			if(obj[i].option7Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option7Value+'">' + obj[i].option7Description + " - "+obj[i].option7Value+  '</option>';
                                			}
                                			if(obj[i].option8Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option8Value+'">' + obj[i].option8Description + " - "+obj[i].option8Value+  '</option>';
                                			}
                                			if(obj[i].option9Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option9Value+'">' + obj[i].option9Description + " - "+obj[i].option9Value+  '</option>';
                                			}
                                			if(obj[i].option10Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option10Value+'">' + obj[i].option10Description + " - "+obj[i].option10Value+  '</option>';
                                			}
                                			if(obj[i].option11Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option11Value+'">' + obj[i].option11Description + " - "+obj[i].option11Value+  '</option>';
                                			}
                                			if(obj[i].option12Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option12Value+'">' + obj[i].option12Description + " - "+obj[i].option12Value+  '</option>';
                                			}
                                			if(obj[i].option13Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option13Value+'">' + obj[i].option13Description + " - "+obj[i].option13Value+  '</option>';
                                			}
                                			if(obj[i].option14Value ) {
	                        					tagOptionValue += '<option value="'+obj[i].option14Value+'">' + obj[i].option14Description + " - "+obj[i].option14Value+  '</option>';
                                			}
	                        				$("#tagOptions").html(tagOptionValue);
                            			}
                        		    }

                            	}else if(tagType == 2){
                            		$('#tagRangeFieldValue').show();
                            		$('#tagSelectFieldValue').hide();
                            		$('#tagInputFieldValue').hide();
                            		for (var i = 0; i < obj.length; i++) {
                            			if(obj[i].id == id){
                                			var range = obj[i].tagValueRange;
                                			var splitRange = range.split("-");
                                			var splitRange1 = splitRange[0];
                                			var splitRange2 = splitRange[1];
                                			var tagRangeField = '<input type="number" name="tagValue" id="tagRangeInputField" min="'+splitRange1+'" max="'+splitRange2+'" onkeyup="evalTagDialog(this);" onchange="evalTagDialog(this);"/>';
                                			$("#tagRangeFieldVisible").html(tagRangeField);
                                			$("#rangeName").html('Range Between '+range);

                            			}
                        		    }
                                }
                    		});
            			}
            		});

            	};

            	function customTagListModify(customTagName, customTagValue){
            		var userDeviceName = "<?php echo $userInfo['deviceType']; ?>";
            		var html = "<option value=''>None</option>";
            		var inputData = "";
            		//var tagOptionValue = "<option value=''>None</option>";
                        
            		$.ajax({
            			type: "POST",
            			url: "userMod/customTagModify.php",
            			data: {deviceType : userDeviceName},
            			success: function(result) {
            				var obj = jQuery.parseJSON(result);
            				var tagNameFound = false;
            				for (var i = 0; i < obj.length; i++) {
                				if(customTagName == obj[i].tagName){
                					tagNameFound = true;
	            					html += '<option value="'+obj[i].tagName+'" tagType="'+obj[i].tagType+'" id="'+obj[i].id+'" selected>' + obj[i].shortDescription + '</option>';
	            					$("#tagShortDescription").html(html);
	            					$("#tagShortDescription").css("background", "#ddd");
	            					$("#AllowOutOfSpecCheckBoxDiv").show();
                				}

            				}
            				if(!tagNameFound) {
            					alert( "Error: Tagname " + customTagName +" is not configured in xpress specifications");
            				} else {

	            				$('#tagInputFieldValue').hide();
	                			$('#tagSelectFieldValue').hide();
	                			$('#tagRangeFieldValue').hide();

	            					$("#tagOptions").html("");
	                    			var tagType = $('#tagShortDescription').find('option:selected').attr('tagType');
	                    			var id = $('#tagShortDescription').find('option:selected').attr('id');

	                    			if(tagType == 0){
	                        			$('#tagInputFieldValue').show();
	                        			$('#tagSelectFieldValue').hide();
	                        			$('#tagRangeFieldValue').hide();
	                        			inputData = '<input type = "text" id="tagValue" name="tagValue" onkeyup="evalTagDialog(this);" value="'+customTagValue+'" />';
	                        			$("#customTagInputFieldValue").html(inputData);
	                        		}else if(tagType == 1){
	                        			$('#tagInputFieldValue').hide();
	                        			$('#tagRangeFieldValue').hide();
	                        			$('#tagSelectFieldValue').show();
	                        			var tagOptionValue = "<option value=''>None</option>";
	                        			for (var i = 0; i < obj.length; i++) {
	                            			if(obj[i].id == id){
	                                			if(obj[i].option1Value) {
		                        					tagOptionValue += '<option '+( obj[i].option1Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option1Value+'">' + obj[i].option1Description + " - "+obj[i].option1Value+  '</option>';
	                                			}
	                                			if(obj[i].option2Value) {
		                        					tagOptionValue += '<option '+( obj[i].option2Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option2Value+'">' + obj[i].option2Description +  " - "+obj[i].option2Value+ '</option>';
	                                			}
	                                			if(obj[i].option3Value) {
		                        					tagOptionValue += '<option '+( obj[i].option3Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option3Value+'">' + obj[i].option3Description + " - "+obj[i].option3Value+  '</option>';
	                                			}
	                                			if(obj[i].option4Value) {
		                        					tagOptionValue += '<option '+( obj[i].option4Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option4Value+'">' + obj[i].option4Description + " - "+obj[i].option4Value+  '</option>';
	                                			}
	                                			if(obj[i].option5Value) {
		                        					tagOptionValue += '<option '+( obj[i].option5Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option5Value+'">' + obj[i].option5Description + " - "+obj[i].option5Value+  '</option>';
	                                			}
	                                			if(obj[i].option6Value) {
		                        					tagOptionValue += '<option '+( obj[i].option6Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option6Value+'">' + obj[i].option6Description + " - "+obj[i].option6Value+  '</option>';
	                                			}
	                                			if(obj[i].option7Value) {
		                        					tagOptionValue += '<option '+( obj[i].option7Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option7Value+'">' + obj[i].option7Description + " - "+obj[i].option7Value+  '</option>';
	                                			}
	                                			if(obj[i].option8Value) {
		                        					tagOptionValue += '<option '+( obj[i].option8Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option8Value+'">' + obj[i].option8Description + " - "+obj[i].option8Value+  '</option>';
	                                			}
	                                			if(obj[i].option9Value) {
		                        					tagOptionValue += '<option '+( obj[i].option9Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option9Value+'">' + obj[i].option9Description + " - "+obj[i].option9Value+  '</option>';
	                                			}
	                                			if(obj[i].option10Value) {
		                        					tagOptionValue += '<option '+( obj[i].option10Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option10Value+'">' + obj[i].option10Description + " - "+obj[i].option10Value+  '</option>';
	                                			}
	                                			if(obj[i].option11Value) {
		                        					tagOptionValue += '<option '+( obj[i].option11Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option11Value+'">' + obj[i].option11Description + " - "+obj[i].option11Value+  '</option>';
	                                			}
	                                			if(obj[i].option12Value) {
		                        					tagOptionValue += '<option '+( obj[i].option12Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option12Value+'">' + obj[i].option12Description + " - "+obj[i].option12Value+  '</option>';
	                                			}
	                                			if(obj[i].option13Value) {
		                        					tagOptionValue += '<option '+( obj[i].option13Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option13Value+'">' + obj[i].option13Description + " - "+obj[i].option13Value+  '</option>';
	                                			}
	                                			if(obj[i].option14Value) {
		                        					tagOptionValue += '<option '+( obj[i].option14Value == customTagValue ? 'selected="selected"' : '')+' value="'+obj[i].option14Value+'">' + obj[i].option14Description + " - "+obj[i].option14Value+  '</option>';
	                                			}
		                        				$("#tagOptions").html(tagOptionValue);
	                            			}
	                        		    }

	                            	}else if(tagType == 2){
	                            		$('#tagRangeFieldValue').show();
	                            		$('#tagSelectFieldValue').hide();
	                            		$('#tagInputFieldValue').hide();
	                            		for (var i = 0; i < obj.length; i++) {
	                            			if(obj[i].id == id){
	                                			var range = obj[i].tagValueRange;
	                                			var splitRange = range.split("-");
	                                			var splitRange1 = splitRange[0];
	                                			var splitRange2 = splitRange[1];
	                                			var tagRangeField = '<input type="number" value="'+customTagValue+'" name="tagValue" id="tagRangeInputField" min="'+splitRange1+'" max="'+splitRange2+'" onkeyup="evalTagDialog(this);" onchange="evalTagDialog(this);"/>';
	                                			$("#tagRangeFieldVisible").html(tagRangeField);
	                                			$("#rangeName").html('Range Between '+range);

	                            			}
	                        		    }
	                                } //end else if
	                    		$(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
	                    		$(":button:contains('Update')").removeAttr("disabled").removeClass("ui-state-disabled");
	                    		$(":button:contains('Close')").removeAttr("disabled").removeClass("ui-state-disabled");
            				}

            			}
            		});

                }

            	$("#resetDevicetag").click(function(){
            		if (confirm('Do you want to reset device custom tags?')) {
	            		var deviceType = "<?php echo $userInfo['deviceType']; ?>";
	            		var deviceCustomProfile = "<?php echo $customProfile; ?>";
	            		var deviceName = " <?php echo $_SESSION["userInfo"]["deviceName"]; ?>"
	            		resetDeviceTag(deviceType, deviceName, deviceCustomProfile);
            		}
                });

                function resetDeviceTag(deviceType, deviceName, deviceCustomProfile){
            		$.ajax({
            			type: "POST",
            			url: "userMod/resetCustomTag.php",
            			data: {deviceType : deviceType, deviceName : deviceName, deviceCustomProfile : deviceCustomProfile},
            			success: function(result) {
//                 			$("#customTagsTable").html(result);
                			$(document).find("#customTagsTable").html(result);
                            sortCustomTagTable(columnIndexCustom);
                            checkDefaultRebuitResetCheckBox();
            			}
            		});
                }
                $(function()
            			{

        			$("#phoneNumber").change(function(){
						var sessionPhoneNumber = "<?php echo $_SESSION["userInfo"]["phoneNumber"];?>";
						var sessionPhoneNumberStatus = "<?php echo $_SESSION["userInfo"]["uActivateNumber"];?>";
						var el = $(this);
						var phoneValue = el.val();
						
						if(phoneValue == "" && sessionPhoneNumber != phoneValue){
							$("#divActivateNumber").hide();
							$("#uActivateNumber").prop('checked', false);
						}else if(phoneValue != "" && sessionPhoneNumber != phoneValue){
							$("#divActivateNumber").show();
							$("#uActivateNumber").prop('checked', false);
						}else{
							$("#divActivateNumber").show();
							if(sessionPhoneNumberStatus == "Yes"){
								$("#uActivateNumber").prop('checked', true);
							}else{
								$("#uActivateNumber").prop('checked', false);
							}
						}
            		});
            
            	/*$(".navToVDMLightModule").click(function(){
            		 checkResult(true);
            	       			 //alert(vdmModuleData);
            	                	$("#modUserBodyVDM").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
            	        			$("#loading").show();
            	        			$(".navMenu").removeClass("active");
            	        			$("#modVdm").addClass("active");
            	        			var module = $(this).attr("id");
            	        			var userType = '';
            	        			<?php // if($_SESSION['superUser'] == 1){?>
            	        				//userType = 'superUser';
            	        			<?php// }else{?>
            	        				userType = '<?php// echo $_SESSION['adminType']?>';
            	        			<?php //}?>
            	        			var deviceForVdm = $("#deviceTypeDigital").val();
            	        			
            	        			$.ajax({
            	        				type: "POST",
            	        				url: "navigate.php",
            	        				data: { module: module,
            	        					deviceName: vdmDeviceName,
            	        					vdmDeviceType: vdmDeviceType,
            	        					vdmType: vdmType},
            	        				success: function(result)
            	        				{	
            	        					$("#searchOption").html("");
            	        					$("#userData").html("");
            	        					$(".searchBodyForm").hide();
            	        					$(".mainBannerModify").hide();
            	        					$("#usersBanner").hide();
            	        					$("#modUserBody .vertSpacer").hide();
            	        	 				$(".subBanner").html("Device Management");
            	        					$("#loading").hide();
            	        					$('#helpUrl').attr('data-module', module);
            	        					$('#helpUrl').attr('data-adminType', userType);
            	        					
            	        					//alert(deviceForVdm);
            	        					
            	        					//debugger;
            	        					$("#mainBody").html(result);
            	        					$("#selectedDeviceType").val(deviceForVdm);
            	        					
            	        					$(window).scrollTop(0);
            	        				}
            	        			});
            	                	
            	                    }); */
            	                  //  var deviceForVdm = $("#deviceTypeDigital").val();
            	                    getDeviceManageMentResultType(vdmDeviceName,vdmDeviceType,vdmType) ; // this function used for click device management button  and switch to from userModify module  to device management module.It is define in main.php and main_enterprise.php.
				var checkPassword = function(){
					$("#inputPassword, #inputConfirmPassword").on('input', function() {
						
						var passwordL = $("#inputPassword").val();
						var cPasswordL = $("#inputConfirmPassword").val();
						if(passwordL != "" || cPasswordL != "" ){
							$("#webAccessPassword").prop('checked', true);
						}else{ 
							$("#webAccessPassword").prop('checked', false);
						}
					});
				};
				checkPassword();
				
				var checkPasscode = function(){
					$("#inputPasscode, #inputConfirmPasscode").on('input', function() {
						
						var passwordL = $("#inputPasscode").val();
						var cPasswordL = $("#inputConfirmPasscode").val();
						if(passwordL != "" || cPasswordL != "" ){
							$("#voiceMailPasscode").prop('checked', true);
						}else{
							$("#voiceMailPasscode").prop('checked', false);
						}
					});
				};
				checkPasscode();
            	
            	$("#resetWebDefault").click(function(){
    				if ($('#resetWebDefault').is(':checked')) {
    					$("#passwordDiv").hide();
    					$("#webAccessPassword").prop('checked', true);
    				}else{
    					$("#passwordDiv").show();
    					checkPassword();
    				}
    			});
            	$("#resetPassDefault").click(function(){
    				if ($('#resetPassDefault').is(':checked')) {
    					$("#passcodeDiv").hide();
    					$("#voiceMailPasscode").prop('checked', true);
    				}else{
    					$("#passcodeDiv").show();
    					checkPasscode();
    				}
    			});

//             	$("#resetWebDefaultDiv").prop("checked", false);
            	
//     			/*$("#webAccessPassword").click(function(){
    				//var bwWebPortalPasswordType = '<?php echo $bwWebPortalPasswordType?>';
//      				$("#resetWebDefaultDiv").prop("checked", false);
    				
//     				if(bwWebPortalPasswordType == "Static"){
//     					$("#resetWebDefaultDiv").show();
//     					if($("#resetWebDefault")){
//     						$("#resetWebDefaultDiv").show();
//     					}
 
//     				}else{
//     					$("#resetWebDefaultDiv").hide();
//     				}
//     				$(".passWordCode").text("Password");
//    				});
    			
//     			$("#voiceMailPasscode").click(function(){
    				var bwVoicePortalPasswordType = '<?php echo $bwVoicePortalPasswordType;?>';
    				var userVoicePortalPasscodeFormula = '<?php echo $userVoicePortalPasscodeFormula;?>';
    				
//     				$("#resetPassDefaultDiv").hide();
//     				$("#resetPassDefault").prop("checked", false);
    				
//     				if(bwVoicePortalPasswordType == "Static"){
//     					$("#resetPassDefaultDiv").show();
//     					if($("#resetPassDefault")){
//     						$("#resetPassDefaultDiv").show();
//     					}
//     				}else if (bwVoicePortalPasswordType == "Formula" && userVoicePortalPasscodeFormula != ""){
//     					$("#resetPassDefaultDiv").show();
//     					if($("#resetPassDefault")){
//     						$("#resetPassDefaultDiv").show();
//     					}
//     				}else{
//     					$("#resetPassDefaultDiv").hide();
//     				}
//     				$(".passWordCode").text("Passcode");
//     			});
          
          });


            	
var mailBoxLimit = "<?php if(isset($_SESSION["userInfo"]["mailBoxLimit"])){echo $_SESSION["userInfo"]["mailBoxLimit"];}else{echo "";}?>";    
if(mailBoxLimit){
	if(mailBoxLimit == "true"){
		$("#mailBoxLimit").val("Use Group Default");
	}else{
		$("#mailBoxLimit").val(mailBoxLimit);
	}
}


function selectDeviceTypeList(selector) {
    $(".deviceTypeAnalog").val("");
    $(".deviceTypeDigital").val("");
	$("#deviceTypeResult").val("");
    
	$("#portForSipDeviceDiv").hide();
	$("#portForSipDeviceSelect").html("");
	$("#supportPortNumberType").val("");
	
  //  $(".deviceTypeAnalog").trigger("change");
   // $(".deviceTypeDigital").val("change");
    
    var rgValue = getRadioGroupValue("userType");
   	if(rgValue == "digital") {
		$("#deviceTypeDigital").prop('disabled',false);
		$("#deviceTypeAnalog").prop('disabled',true);
		$("#userTypeResult").val("digital");
		 
		$("#deviceIndexGroupMod").hide();
		$("#portNumberGroupMod").hide();
		$("#deviceIndex").val("");
		$("#portNumber").val("");
	}else if(rgValue == "analog") {
		$("#deviceTypeDigital").prop('disabled',true);
		$("#deviceTypeAnalog").prop('disabled',false);
		$("#userTypeResult").val("analog");
		$("#show").hide();
	 
	}
   	document.getElementById("deviceMngmtTagBundleDiv").style.display  = "none";
    document.getElementById("digitalDeviceTypeVoipDiv").style.display  = (rgValue == "digital") ? "block" : "none";
    document.getElementById("analogDeviceTypeVoipDiv").style.display = (rgValue == "analog")  ? "block" : "none";

    document.getElementById("tagBundleDiv").style.display  = (rgValue == "digital") ? "block" : "none";
    
//    document.getElementById("deviceIndexGroupMod").style.display = (rgValue == "analog")  ? "block" : "none";
//    document.getElementById("portNumberGroupMod").style.display = (rgValue == "analog")  ? "block" : "none";
}

function getRadioGroupValue(name) {
    var radioGroup = document.getElementsByName(name);
    for (var i = 0; i < radioGroup.length; i++) {
        if (radioGroup[i].checked) {
            return radioGroup[i].value;
        }
    }
    return "";
}


function processPortNumbers(selector) {
  	var deviceType = document.getElementById("deviceTypeResult").value;
	    
// 	    var deviceType = $("input[deviceType]").val();
		var deviceIndexText = selector.options[selector.selectedIndex].text;
		$("#deviceIndexText").val(deviceIndexText);
		$("#portNumberText").val("");
		
	    if(deviceType != "") {
	    	var $deviceIndex = selector.options[selector.selectedIndex].value;
		    var settings = {
		        type: "POST",
		        url: "userAdd/analogUserDo.php",
		        data: { module: "buildPortList", deviceType: deviceType, deviceIndex: $deviceIndex },
		        async: false
		    };

		    $.ajax(settings).done(function(result)
		    {
		        result = result.trim();
		        if (result.slice(0, 1) == "1") {
		            alert( "Error: " + result.slice(1));
		        } else {
		            $("#portNumberDivMod").html(result.slice(1));
		        }
		    }); 
		}	
}


$(document).on('change', '#portNumber', function() {
	var portNumberText = $("#portNumber option:selected").text();
	$("#portNumberText").val(portNumberText);
	});

/*cutom tag crate dialog*/
 
 $("#createCustomTagForm").dialog({
            
            autoOpen: false,
            width: 600,
            modal: true,
            position: { my: "top", at: "top" },
            resizable: false,
            closeOnEscape: false,
            open: function() {
				setDialogDayNightMode($(this));
            	$('.ui-dialog-buttonpane').find('button:contains("Create")').addClass('createButton');
				$('.ui-dialog-buttonpane').find('button:contains("Delete")').addClass('deleteButton');
				$('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('closeButton');
				$('.ui-dialog-buttonpane').find('button:contains("Update")').addClass('updateButton');
				
            	 $(":button:contains('Create')").hide();
            	 $(":button:contains('Update')").hide();
            	 $(":button:contains('Delete')").hide();
            	 $(":button:contains('Close')").show();
            	 $(":button:contains('Create')").attr("disabled", "disabled").addClass("ui-state-disabled");
            	 $(".customTagErrorMsg").hide();
            },
            buttons: {
               
				"Create": function()
				{
					var user_tagName =  "%"+ $("#user_tagName").val() + "%";
					var user_tagValue =  $("#user_tagValue").val();
					
					var settings = {
                            type: "POST",
                            url: "userMod/customTagDo.php",
                            data: {tagName : user_tagName , tagValue: user_tagValue, tagNameDescription: user_tagName, operationType: "CREATE"},
                            async: false
                        };
                        $.ajax(settings).done(function(result) {
                            result = result.trim();
                            if (result.slice(0, 1) == "1") {
                                alert( "Error: " + result.slice(1));
                            }
                            else if (result.slice(0, 1) == "2") {
                            	$("#createCustomTagForm .customTagErrorMsg").html(result.slice(1));
                            	$(".customTagErrorMsg").show();
                            	$(":button:contains('Create')").attr("disabled", "disabled").addClass("ui-state-disabled");
                            }
                             else {
                                $("#createCustomTagForm").dialog("close");
								$(document).find("#customTagsTable").html(result.slice(1));
                                sortCustomTagTable(columnIndexCustom);
                                checkDefaultRebuitResetCheckBox();
                            }
                        });
				 
				},
				"Update": function()
				{
					var user_tagName =  "%"+ $("#user_tagName").val() + "%";
					var user_tagValue =  $("#user_tagValue").val();
					
					updateCustomTag(user_tagName, user_tagValue);
					checkDefaultRebuitResetCheckBox();
				},
				"Delete": function()
				{
					var user_tagName =  "%"+ $("#user_tagName").val() + "%";
					var user_tagValue =  $("#user_tagValue").val();
					
					deleteCustomTag(user_tagName, user_tagValue);
					checkDefaultRebuitResetCheckBox();
				 
				},
				"Close": function() {
                    $(this).dialog("close");
                }
            }
        });
 
 $("#createCustomTagBtn").on("click", function() {
	 $("#createCustomTagForm").find("#user_tagName").removeClass("disableInput");
	 $("#createCustomTagForm").find("#user_tagName").prop('disabled', false);
	 
	 $("#createCustomTagForm").find("#user_tagName").val("");
	 $("#createCustomTagForm").find("#user_tagValue").val("");
	 $("#createCustomTagForm").dialog("open");
	 $(":button:contains('Create')").show();
 });

 $("#user_tagName, #user_tagValue").on("input", function() {
	var user_tagName =  $("#user_tagName").val();
	var user_tagValue =  $("#user_tagValue").val();
	if(user_tagName !="" && user_tagValue != "") {
    	$(":button:contains('Create')").removeAttr("disabled").removeClass("ui-state-disabled");
    	$(":button:contains('Update')").removeAttr("disabled").removeClass("ui-state-disabled");
	} else {
		$(":button:contains('Create')").attr("disabled", "disabled").addClass("ui-state-disabled");
		$(":button:contains('Update')").attr("disabled", "disabled").addClass("ui-state-disabled");
	}
	 
});

 var updateCustomTag = function(modifyCustomTag, modifiedTagValue) {
     var settings = {
         type: "POST",
         url: "userMod/customTagDo.php",
         data: { modifyCustomTag: modifyCustomTag, modifiedTag: modifiedTagValue},
         async: false
     };
     $.ajax(settings).done(function(result) {
         result = result.trim();
         if (result.slice(0, 1) == "1") {
             alert( "Error: " + result.slice(1));
         }
         else if(result.slice(0, 1) == "2") {
        	$("#createCustomTagForm .customTagErrorMsg").html(result.slice(1));
         	$(".customTagErrorMsg").show();
         	$(":button:contains('Create')").attr("disabled", "disabled").addClass("ui-state-disabled");
         }
          else {
             $("#createCustomTagForm").dialog("close");
             $(document).find("#customTagsTable").html(result.slice(1));
             sortCustomTagTable(columnIndexCustom);
         }
     });
}

 var deleteCustomTag = function(deleteCustomTag) {
	  var settings = {
              type: "POST",
              url: "userMod/customTagDo.php",
              data:  { deleteCustomTag: deleteCustomTag },
              async: false
          };
          $.ajax(settings).done(function(result) {
              result = result.trim();
              if (result.slice(0, 1) == "1") {
                  alert( "Error: " + result.slice(1));
              } else {
            	  $("#createCustomTagForm").dialog("close");
            	  $(document).find("#customTagsTable").html(result.slice(1));
            	  sortCustomTagTable(columnIndexCustom);
              }
          });
}
 
</script>
		<?php
		
		$userIsRegistered = checkUserRegStatus($userId);
		if($userIsRegistered) {
		    $textColor = "#91bd83";
		    $regStatusIcon = "images/registered_icon.png";
		    $regTitle = "Registered";
		} else {
		    $textColor = "#be8585";
		    $regStatusIcon = "images/not_registered_icon.png";
		    $regTitle = "Not registered";
		}
		?>
		<div class="labelTextGrey" style="padding-bottom: 15px;padding-top: 15px;text-align: center;"> 
		<h2 class="subBannerCustom"><b style="color:<?php echo $textColor; ?>"><?php echo $userId." - ".$_SESSION["userInfo"]["lastName"].", ".$_SESSION["userInfo"]["firstName"]; ?></b> 
		<img style="margin-left: 12px;" src="<?php echo $regStatusIcon; ?>" width="30" height="30" align="" title="<?php echo $regTitle; ?>">
		</h2> </div>

<script>
    
    

/* var activeImageSwap = function(){
	previousActiveMenu	= "userMod";
	 currentClickedDiv = "modVdm";       
  
		//active
  		if(previousActiveMenu != "")
		{ 
  		 	$(".navMenu").removeClass("activeNav");
			var $thisPrev = $("#userMod").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);
	       
		}
		// inactive tab
		if(currentClickedDiv != ""){
			$("#modVdm").addClass("activeNav");
			var $thisPrev = $("#modVdm").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);
			 previousActiveMenu = currentClickedDiv;
			
    	}
   
} */
//$('.navToVDMLightModule').click(activeImageSwap, activeImageSwap);

$(function(){
	$('#useCustomOcp').change(function () {
        if (!this.checked) {
        	$('#useGroupDefaultSetting .selectClass').prop('disabled', true);
        	$('.selectClass').css("background-color","#ddd");
        }else {
        	$('#useGroupDefaultSetting .selectClass').prop('disabled', false);
        	$('.selectClass').css("background-color","#fff");
        }
    });

	$("#useCustomOcp").trigger("change");

	$('#restrictToGroup').change(function() {
        if(this.checked) {
        	getBLFUsers("group");
        }else{
        	getBLFUsers("enterprise");
        }
    });

	var getBLFUsers = function(seachLabel){
		$("#allUserSearch").prop("disabled", true);
		$("#allUserSearch").css("background-color","#ddd");
		$("#allUserSearch").val("");
		//alert(seachLabel);
		var userId = "<?php echo $userId; ?>";
		//alert(userId);
		$("#allUserSearch").html("");
		var autoCompleteData = new Array();
		$.ajax({
	        type: "POST",
	        url: "userMod/getBLFUsersList.php",
	        data: {seachLabel : seachLabel, userId : userId},
	        success: function(result) {
	        	var explode = result.split(":");
				for (a = 0; a < explode.length; a++)
				{
					autoCompleteData[a] = explode[a];
				}
				$("#allUserSearch").autocomplete({
					source: autoCompleteData
				});
				$("#allUserSearch").prop("disabled", false);
				$("#allUserSearch").css("background-color","#eee");
	        	//$("#dataShow").html(""); 
	        	//$("#dataShow").html(result);              
	        }
	    });
	}

	$('#restrictToGroup').trigger("change");

	$("#allUserSearchGet").click(function(){
		$("#errorDisplay").html("<div id=\"loading4\" style=\"margin:0\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
		$("#loading4").show();
		var userId = "<?php echo $userId; ?>";
		if ($('#restrictToGroup').is(':checked')) {
			var restrictToGroup = "true";
		}else{
			var restrictToGroup = "false";
		}
		
		var allUserSearch = $("#allUserSearch").val();
		var monitoredUsers = $("#monitoredUsers").val();
        $.ajax({
            type: "POST",
            url: "userMod/searchBLFUsers.php",
            data: {restrictToGroup : restrictToGroup, allUserSearch : allUserSearch, userId : userId, monitoredUsers : monitoredUsers},
            success: function(result) {
            	var arr = result.split('***');
            	if(parseInt(arr[0]) > 500){
                	$("#errorDisplay").html("Please narrow your search, there are " +parseInt(arr[0])+ " matching records.");
                	$("#sortable1").html("");
                }else{
                	$("#errorDisplay").html("");
                	$("#sortable1").html(arr[1]);
                }
            	
            }
        });
    });
	
});

$(document).ready(function() {
	/* User Service Operations Start */
	reArrangeSelect("availableUserServiceToAssign");
	reArrangeSelect("assignedUserService");
	reactRadioButtonsOnEvents();
	$("#addUserService, #addUserServiceAll").click(function(){ 
		var BtnId = $(this).attr('id');
		if(BtnId == "addUserServiceAll"){
			$('#availableUserServiceToAssign li').prop('selected', true);
		}else{
			$('#availableUserServiceToAssign li.selected').prop('selected', true);
 		}
		$.when(addAvailableServiceToAssignList()).done(function() {
			reactRadioButtonsOnEvents();
		});
	});
		
	var addAvailableServiceToAssignList = function(){
		var html = "";
		var availableService = $("#availableUserServiceToAssign").val();	
                if(availableService){
			for (var i = 0; i < availableService.length; i++) {
				html += '<option id="'+availableService[i]+'" class="selected" value="'+availableService[i]+'">' +availableService[i]+ '</option>';
				
			//	$("#availableUserServiceToAssign option[value='"+availableService[i]+"']").remove();	
                                $("#availableUserServiceToAssign li").remove();		
			}
		}
		$("#assignedUserService").append(html);
		reArrangeSelect("assignedUserService");
		return true;
	}

	$("#removeFromUserAssigned, #removeAllServiceFromAssigned").click(function(){ 
		var BtnId = $(this).attr('id');
		if(BtnId == "removeAllServiceFromAssigned"){
			$('#assignedUserService option').prop('selected', true);
		}
		$.when(removeServiceFromAssigned()).done(function() {
			reactRadioButtonsOnEvents();
		});
	});

	var removeServiceFromAssigned = function(){
		var html = "";
		var assignedService = $("#assignedUserService").val();

		for (var i = 0; i < assignedService.length; i++) {
				html += '<option value="'+assignedService[i]+'">' +assignedService[i]+ '</option>';
				$("#assignedUserService option[value='"+assignedService[i]+"']").remove();
		}
		$("#availableUserServiceToAssign").append(html);
		reArrangeSelect("availableUserServiceToAssign");
	}

	/* User Service Operations End*/
});

 function verifySelectedServiceOptions() {
	var assignedServices = "";
	var vmFound = false;
	var scaFound = false;
	var polycomFound = false;
	var assignedService = $("#assignedUserService").val();

	 $('#assignedUserService li').each(function() {
          var optionValue = $(this).attr('id');
                                          
                                 
        
        
        
        
       //  $("#assignedUserService li").each(function() {
	// 	var optionValue = $(this).val();
             //   alert("optionValue"+optionValue);
		assignedServices += optionValue + ";";
                //debugger ;
		if(optionValue.indexOf("Voice Messaging User") >= 0 ) {
			vmFound = true;
		}
		else if(optionValue.indexOf("Shared Call Appearance") >= 0) {
			scaFound = true;
		}
		else if(optionValue.indexOf("Polycom Phone Services") >= 0) {
			polycomFound = true;
		}
	});

	var serviceObj = {vmFound: vmFound, scaFound: scaFound, polycomFound: polycomFound};
	$("#assignedUserServicesVal").val(assignedServices);

		return serviceObj;
 }
 
 	function reactRadioButtonsOnEvents() {
            var servicePackObj = verifySelectedServicePackOptionsNew();
            if (typeof servicePackObj !== "undefined") {
                var serviceObj = verifySelectedServiceOptions();
 		
 		var vmFound = (servicePackObj.vmFound || serviceObj.vmFound) ? true : false;
 		var scaFound = (servicePackObj.scaFound || serviceObj.scaFound) ? true : false;
 		var polycomFound = (servicePackObj.polycomFound || serviceObj.polycomFound) ? true : false;
 		
 		if (vmFound) {
                    document.getElementById("voiceMessagingYes").checked = true;
                } else {
                    document.getElementById("voiceMessagingNo").checked = true;
                }
 		if (scaFound) {
                    document.getElementById("sharedCallAppearanceYes").checked = true;
                    } else {
                    document.getElementById("sharedCallAppearanceNo").checked = true;
                }

 		if (polycomFound) {
                    document.getElementById("polycomPhoneServicesYes").checked = true;
                } else {
                    document.getElementById("polycomPhoneServicesNo").checked = true;
                }

 		polycomPhoneServiceIsAssigned();
            }
	}

 /*	 function unAssignUserServices(serviceName) {
     		var html = "";
     		var assignedServices = "";
     		$('#assignedUserService option').prop('selected', true);
    		var assignedService = $("#assignedUserService").val();	
    		if(assignedService){
    			for (var i = 0; i < assignedService.length; i++) {
    				if(assignedService[i].indexOf(serviceName) >= 0) {
    					html += '<option value="'+assignedService[i]+'">' +assignedService[i]+ '</option>';
    					$("#assignedUserService option[value='"+assignedService[i]+"']").remove();
    				}
    			}
    		}
    		$("#availableUserServiceToAssign").append(html);
    		$("#assignedUserService").find("option").each(function() {
    			var optionValue = $(this).val();
    			assignedServices += optionValue + ";";
    		});
    		$("#assignedUserServicesVal").val(assignedServices);
  	 } */
	 	 
    function verifySelectedServicePackOptionsNew() {
    var opts = [];
    var opt;

	var servicePack = $("#servicePack");
	var servicePackVal = servicePack.val();
        
        if (typeof servicePackVal !== "undefined") 
        {
            var servicePackArray = servicePackVal.split(";");
            var opts = servicePackArray;
    
            var vmServicePacks      = "<?php echo (isset($_SESSION["vmServicePacksFlat"]) ? $_SESSION["vmServicePacksFlat"] : ""); ?>";
            var vmFound = false;
                
            var scaServicePacks     = "<?php echo (isset($_SESSION["scaServicePacksStr"]) ? $_SESSION["scaServicePacksStr"] : ""); ?>";
            var scaFound = false;

            var polycomServicePacks = "<?php echo (isset($_SESSION["plycmServicePacksStr"]) ? $_SESSION["plycmServicePacksStr"] : ""); ?>";
            var polycomFound = false;
                
            if ( vmServicePacks != "") {
                vmServicePacks = vmServicePacks.split(",");

                for (i = 0; i < opts.length; i++) {
                    for (var j = 0; j < vmServicePacks.length; j++) {
                        if (vmServicePacks[j] == opts[i]) {
                            vmFound = true;
                            break;
                        }
                    }
                    if (vmFound) {
                        break;
                    }
                }
            }
                                
                //Code added @ 09 July 2018       
            if ( scaServicePacks != "") 
            {
                scaServicePacks = scaServicePacks.split(",");
                for (k = 0; k < opts.length; k++) {
                    for (var l = 0; l < scaServicePacks.length; l++) {
                        if (scaServicePacks[l] == opts[k]) {
                            scaFound = true;
                            break;
                        }
                    }
                    if (scaFound) {
                        break;
                    }
                }
            }
                //Code added @ 07 Sep 2018       
            if ( polycomServicePacks != "") 
            {
                polycomServicePacks = polycomServicePacks.split(",");
                for (p = 0; p < opts.length; p++) {
                    for (var q = 0; q < polycomServicePacks.length; q++) {
                        if (polycomServicePacks[q] == opts[p]) {
                            polycomFound = true;
                            break;
                        }
                    }
                    if (polycomFound) {
                        break;
                    }
                }
            }
            var servicePackObj = {vmFound: vmFound, scaFound: scaFound, polycomFound: polycomFound};
            return servicePackObj;
        }
    }

             function reArrangeSelect(selectId) {
                $("#"+selectId).html($("#" + selectId + " li").sort(function(a, b) {
                    return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
                }))
            }  

          function sortUnorderedList(ul, tag) {
                //debugger;
                //Code added @ 08 Feb 2019
                var ulId = "#"+ul+" option";
                $(ulId).each(function() {
                        $(this).removeClass("selected");
                });
                //End code
                if(typeof ul == "string")
                  ul = document.getElementById(ul);

        	  // Get the list items and setup an array for sorting
        	  var lis = ul.getElementsByTagName(tag);
                  //alert('List - '+lis);
                  //console.log(lis);
        	  var vals = [];
                  
        	  // Populate the array
        	  for(var i = 0, l = lis.length; i < l; i++)
                  {
                        
                        vals.push(lis[i].innerHTML);
                  }

        	  // Sort it
        	  //vals.sort();

        	vals.sort(function(a, b) {
        	    var textA = a.toUpperCase();
        	    var textB = b.toUpperCase();
        	    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        	});

        	  // Sometimes you gotta DESC
        	  //if(sortDescending)
        	  //  vals.reverse();

        	  // Change the list on the page
        	  for(var i = 0, l = lis.length; i < l; i++){
        		lis[i].innerHTML = vals[i];
        		//var valofId = vals[i];
        		//var classToadd = lis.valofId.className;
        		//var className = $('#'+valofId).attr('class');
        		//$("#"+vals[i]).addClass(className);
            }
        	    
        	}   
            
</script>

<style>
.disableInput {
    pointer-events: none;
    background: #ddd;
}
#marginZero div:nth-child(3){margin-left: 0 !important;}

.marginBottom {
    margin-bottom: 20px;
}

.leftMargin30px {
    margin-left: 68px;
    min-height:200px;
}
.leftMargin {
    margin-left: 10px;
}

.selectClass{width:100% !important;}

.nORBF{height: 36.5px;width:75%;}
.userModForm .newCheckBoxImg {
    display: none;
}

.userModForm .newCheckBoxImg:checked + label span {
   /* background: url("/Express/icons/polycom_radio.png") no-repeat;*/
    background: url("/Express/icons/green_button.png") no-repeat;
    width: 35px;
    height: 35px;
    margin: -2px 5px 0 0;
    vertical-align: middle;
    cursor: pointer;
 
}
.userModForm .newCheckBoxImg + label span {
    display: inline-block;
    background: url("/Express/icons/radial_inactive.png") no-repeat;
    width: 35px;
    height: 35px;
    margin: -2px 5px 0 0;
    vertical-align: middle;
     cursor: pointer;

}

.userModForm .newCheckBoxImg + label .checkRealBtn {
    display:inline-block;
    padding: 0 0 0 0px !important;
    background:url("/Express/icons/checkbox_unchecked.png") no-repeat;
    height: 22px !important;
    width: 22px !important;
    vertical-align: middle !important;
    cursor:pointer;
}

.userModForm .newCheckBoxImg:checked + label .checkRealBtn {
    background:url("/Express/icons/checkbox_checked.png") no-repeat !important;
    height: 22px !important;
    width: 22px !important;
    display:inline-block !important;
    vertical-align: middle !important;
    cursor:pointer;
}
.clr2{margin-bottom:20px;}
</style>

<?php 

require_once("/var/www/lib/broadsoft/adminPortal/services/userServices.php");

$showServiceSpeedDial8 = "false";
$showServiceSpeedDial100 = "false";
$scaServiceShow = "false";

$service = new userServices();
$serviceResponse = $service->getUserServicesAll($userId, $_SESSION["sp"]);

if(empty($serviceResponse["Error"])){
	$serviceArray = $serviceResponse["Success"]["TotalServices"];
	if(in_array("Speed Dial 8", $serviceArray)){ 
		$showServiceSpeedDial8 = "true";
	}
			
	if(in_array("Speed Dial 100", $serviceArray)){
		$showServiceSpeedDial100 = "true";
	}
	//sca services check flag
	$scaArray = array(
		"Shared Call Appearance", 
		"Shared Call Appearance 10", 
		"Shared Call Appearance 15", 
		"Shared Call Appearance 20", 
		"Shared Call Appearance 25",
		"Shared Call Appearance 30",
		"Shared Call Appearance 35",
		"Shared Call Appearance 5",
    );
	$scaServiceCountCheck = array_intersect($scaArray, $serviceArray);
	if(count($scaServiceCountCheck) > 0){
		$scaServiceShow = "true";
	}
}

$_SESSION ["userInfo"]["sharedCallAppearance"] = "No";
if($userHasSCAFlag=="Yes"){
    $_SESSION ["userInfo"]["sharedCallAppearance"] = "Yes";
}

foreach( $serviceResponse["Success"]["Services"]['assigned'] as $shareKey => $shareValue ) {
    if(strpos($shareValue , "Shared Call Appearance") !== false) {
        $_SESSION ["userInfo"]["sharedCallAppearance"] = "Yes";
    }
}

$_SESSION ["userInfo"]["polycomPhoneServices"] = "No";
if($userHasPolycomSPFlag=="Yes" && $respTmp!="false"){
    $_SESSION ["userInfo"]["polycomPhoneServices"] = "Yes";
}
foreach( $serviceResponse["Success"]["Services"]['assigned'] as $polyKey => $polyValue ) {
    if(strpos($polyValue , "Polycom Phone Services") !== false && $respTmp!="false") {
        $_SESSION ["userInfo"]["polycomPhoneServices"] = "Yes";
    }
}

$varData = "";
    if(isset($_SESSION['userInfo']['voiceMessagingUser'])){
        $varData = $_SESSION['userInfo']['voiceMessagingUser'];
        $_SESSION['userInfo']['voiceMessaging'] = "Yes";
    }else{
        $varData = 'false';
        $_SESSION['userInfo']['voiceMessaging'] = "No";
    }    
    
?>

<?php 
	$countSpec = checkCustomTagSpec();        
?>

<form name="userMod" id="userMod" class="userModForm" method="POST" autocomplete="off">

	<input type="hidden" name="userId" id="userId"
		value="<?php echo $userId; ?>">
	<div id="tabs">
	<div class="divBeforeUl">
		<ul>
		<?php
		if (isset($_SESSION ["permissions"] ["basicUser"]) && $_SESSION ["permissions"] ["basicUser"] == "1") {
		echo "<li><a href=\"#basicInfo_1\" id=\"basicInfo\" class=\"tabs\">Basic Information</a></li>";
		
		if($_SESSION["permissions"]["servicePacks"] == "1"){
			echo "<li><a href=\"#servicePacks_1\" class=\"tabs\" id=\"servicePacks\">Service Packs / Services</a></li>";
		}
		
	 if($showServiceSpeedDial8 == "true" && isset($_SESSION["permissions"]["srvSpeedDial8"]) && $_SESSION["permissions"]["srvSpeedDial8"] == "1"){
			echo "<li><a href=\"#speedDial8_1\" class=\"tabs\" id=\"speedDial8\">Speed Dial 8</a></li>";
		}
		if($showServiceSpeedDial100 == "true" && isset($_SESSION["permissions"]["srvSpeedDial100"]) && $_SESSION["permissions"]["srvSpeedDial100"] == "1"){
			echo "<li><a href=\"#speedDial100_1\" class=\"tabs\" id=\"speedDial100\">Speed Dial 100</a></li>";
		}
		
		if($isVMAllowed) {
    		if ((isset ( $_SESSION ["userInfo"] ["thirdPartyVoiceMail"] ) and $_SESSION ["userInfo"] ["thirdPartyVoiceMail"] == "true") or (isset ( $_SESSION ["userInfo"] ["voiceMessagingUser"] ) and $_SESSION ["userInfo"] ["voiceMessagingUser"] == "true")) {
    			echo "<li><a href=\"#voiceManagement_1\" class=\"tabs\" id=\"voiceManagement\">";
    			if (isset ( $_SESSION ["userInfo"] ["thirdPartyVoiceMail"] ) and $_SESSION ["userInfo"] ["thirdPartyVoiceMail"] == "true") {
    				echo "Third-Party Voice Mail";
    			} else if ($_SESSION ["userInfo"] ["voiceMessagingUser"] == "true" && $_SESSION["permissions"]["voiceManagement"] == "1") {
    				echo "Voice Management";
    			}
    			echo "</a></li>";
    		}

		}
	}
	// USPS change - permanently hiding Web password tab
	// if ($_SESSION["permissions"]["resetPasswords"] == "1")
	// {
	// echo "<li><a href=\"#passwords_1\" class=\"tabs\" id=\"passwords\">Web Password</a></li>";
	// }
	if (isset($_SESSION ["permissions"] ["changeHotel"]) && $_SESSION ["permissions"] ["changeHotel"] == "1" and ! ($_SESSION ["userInfo"] ["hotelHostisActive"] == "" and $_SESSION ["userInfo"] ["hotelisActive"] == "")) {
		echo "<li><a href=\"#hotel_1\" class=\"tabs\" id=\"hotel\">Hoteling</a></li>";
	}
	if ($phoneDeviceIsVisible = isset($_SESSION ["permissions"] ["changeDevice"]) && $_SESSION ["permissions"] ["changeDevice"] == "1" && ! (isset ( $_SESSION ["userInfo"] ["deviceIsAnalog"] ) && isset($_SESSION ["userInfo"] ["deviceIsAnalog"]) && $_SESSION ["userInfo"] ["deviceIsAnalog"] == "true") || (isset($_SESSION ["permissions"] ["deviceInfoReadOnly"]) && $_SESSION ["permissions"] ["deviceInfoReadOnly"] == "1")) {
		echo "<li><a href=\"#phoneDevice_1\" class=\"tabs\" id=\"phoneDevice\">Phone and Device Information</a></li>";
	}
	//conter path soft phone
	if(isset($license["softPhone"]) && $license["softPhone"] == "true" && $deviceIsAudioCodeName == "false"){
	    echo "<li><a href=\"#softPhones\" class=\"tabs\" id=\"counterPathSoftPhones\">Soft Phones</a></li>";
	}

	if (isset($_SESSION ["permissions"] ["resetPasswords"]) && $_SESSION ["permissions"] ["resetPasswords"] == "1" || isset($_SESSION ["permissions"] ["voiceMailPasscode"]) && $_SESSION ["permissions"] ["voiceMailPasscode"] == "1") {
		echo "<li><a href=\"#passwords_1\" class=\"tabs\" id=\"passwords1\">Passwords</a></li>";
	}

	if (isset($_SESSION ["permissions"] ["clearMACAddress"]) && $_SESSION ["permissions"] ["clearMACAddress"] == "1" && 
                (!$phoneDeviceIsVisible || isset($_SESSION ["permissions"] ["deviceInfoReadOnly"]) && $_SESSION ["permissions"] ["deviceInfoReadOnly"] == "1") && $_SESSION['adminType'] != "superUser") {
	    echo "<li><a href=\"#clearMACAddress_1\" class=\"tabs\" id=\"clearMACAddress\">Clear MAC Address</a></li>";
	}
	
	if(isset($license["customTags"]) && $license["customTags"] == "true" && isset($_SESSION ["permissions"] ["devCustomTags"]) && $_SESSION ["permissions"] ["devCustomTags"] == "1"){ 
		//if($_SESSION["userInfo"]["customProfile"] != "None"){ 
	       if($countSpec > 0 && $customTagDisplay !=""){ 
				echo "<li><a href=\"#customTags_1\" class=\"tabs\" id=\"customTags\">Custom Tags</a></li>";
			}
		//}
	}
	
	if (isset ( $_SESSION ["userInfo"] ["callControlTab"] ) and $_SESSION ["userInfo"] ["callControlTab"] == "true" and $_SESSION ["permissions"] ["changeForward"] == "1") {
		echo "<li><a href=\"#control_1\" class=\"tabs\" id=\"callControl\">Call Control</a></li>";
	}
        
        /*shim Ring tab */
 	if(in_array("Simultaneous Ring Personal", $_SESSION["groupInfoData"]["userServiceAuth"]) and $_SESSION["userInfo"]["simRingService"]=="true" and $_SESSION ["permissions"] ["simRing"] == "1") {
	    echo "<li><a href=\"#simRing\" class=\"tabs\" id=\"simRings\">Sim Ring</a></li>";
 	}
        
	if (isset($_SESSION ["permissions"] ["changeblf"]) && $_SESSION ["permissions"] ["changeblf"] == "1" && isset ( $_SESSION ["userInfo"] ["busyLampField"] ) && $_SESSION ["userInfo"] ["busyLampField"] == "true") {
		echo "<li><a href=\"#blf_1\" class=\"tabs\" id=\"blf\">Busy Lamp Fields</a></li>";
	}
        //Code commented @ 18 July 2018 regarding Issue EX-658
	/*if (isset($_SESSION ["permissions"] ["changeSCA"]) && $_SESSION ["permissions"] ["changeSCA"] == "1" && $scaServiceShow == "true") {
           echo "<li><a href=\"#sca_1\" class=\"tabs\" id=\"sca\">Shared Call Appearances</a></li>";
	}*/
        //Code added @ 18 July 2018
        $deviceInfo = getDevicePortFromDeviceTable($db, $_SESSION["userInfo"]["deviceType"]);       
        if(isset($_SESSION["userInfo"]["deviceType"]) && $_SESSION["userInfo"]["deviceType"] != "" && $deviceInfo['ports']>1){
                    echo "<li><a href=\"#sca_1\" class=\"tabs\" id=\"sca\">Shared Call Appearances</a></li>";
            }       
	//End code
	if ($cpgFieldVisible == "true" && $_SESSION ["groupServicesAuthorizationTable"] == "1" && isset($_SESSION ["permissions"] ["callPickupGroup"]) && 
	    $_SESSION ["permissions"] ["callPickupGroup"] == "1") {
		echo "<li><a href=\"#callPickupGroup_1\" class=\"tabs\" id=\"callPickupGroup\">Call Pickup Group</a></li>";
	}
	
	//permissions for policies
	$showPoliciesTab = (isset($_SESSION ["permissions"] ["userPolicyCallLimits"]) && $_SESSION ["permissions"] ["userPolicyCallLimits"] == "1" ||
                    	isset($_SESSION ["permissions"] ["userPolicyCLID"]) && $_SESSION ["permissions"] ["userPolicyCLID"] == "1" ||
                    	isset($_SESSION ["permissions"] ["userPolicyIncomingCLID"]) && $_SESSION ["permissions"] ["userPolicyIncomingCLID"] == "1") &&
                        isset($_SESSION ["permissions"] ["basicUser"]) && $_SESSION ["permissions"] ["basicUser"] == "1";
                        
    if ($showPoliciesTab) {
		echo "<li><a href=\"#policies_1\" class=\"tabs\" id=\"policies\">Policies</a></li>";
	}
	if(in_array("Outgoing Calling Plan", $_SESSION["groupInfoData"]["groupServiceAssign"]) && (isset($_SESSION ["permissions"] ["ocpUser"]) && $_SESSION ["permissions"] ["ocpUser"] == "1")){
	    echo "<li><a href=\"#ocp_1\" class=\"tabs\" id=\"ocpFeatures\">Outgoing Calling Plan</a></li>";
	}
	
	?>
				</ul>
                </div>
				<?php
					if ($_SESSION["permissions"]["basicUser"] == "1")
					{
						?>
					<div id="basicInfo_1" class="userDataClass" style="display:none;"><!--begin basicInfo_1 div-->
						<h2 class="subBannerCustom">Basic Info</h2>

					<?php 
						$serviceInfoObj = new Services();
						if($serviceInfoObj->hasCLIDBlocKingService($_SESSION['groupId'])) {
					?>
                        	<div class="form-group">                                                                                    
    								 <input name="userCallingLineIdBlockingOverride" id="" type="hidden" value="false"> 
                                    <input class="newCheckBoxImg" style="width:20px; float:right;margin: 10px 0;" type="checkbox" name="userCallingLineIdBlockingOverride" id="userCallingLineIdBlockingOverride" <?php if($_SESSION["userInfo"]["userCallingLineIdBlockingOverride"] == "true"){?>checked<?php }?> value="true">
                                    <label class="labelClsForVPSrvce" for="userCallingLineIdBlockingOverride" style="opacity: 1;"><span class="voicePortalSpanCls"></span></label> 
    								<label class="labelText fontHead"><b>Calling Line ID Blocking</b></label>
                        	</div>
					<?php } ?>
												
						<div class="row">
						<!--changes start first block -->
							<div class="col-md-6">
								<div class="form-group">	
								<label class="labelText">First Name</label><span class="required">*</span><br>
								<input type="text" name="firstName" id="firstName" size="30" maxlength="30" onChange="changeUserValueFirstName(this.value);" value="<?php echo $_SESSION["userInfo"]["firstName"]; ?>">
								</div>
							</div>
							
							
							<div class="col-md-6"><!--start div col-md-6-->
								<div class="form-group">
								<label class="labelText">Last Name</label><span class="required">*</span><br/>
								<input type="text" name="lastName" id="lastName" size="30" maxlength="30"   onChange="changeUserValueLastName(this.value);" value="<?php echo $_SESSION["userInfo"]["lastName"]; ?>">
								</div>
							</div>
						</div>

					    <div class="row">
							<div class="col-md-6">
								<div class="form-group">	
								<label class="labelText">Calling Line ID First Name</label><span class="required">*</span><br>
								<input type="text" name="callingLineIdFirstName" id="callingLineIdFirstName" size="30" maxlength="30" value="<?php echo $_SESSION["userInfo"]["callingLineIdFirstName"]; ?>" >
								</div>
							</div>
							
							
							<div class="col-md-6">
								<div class="form-group">	
								<label class="labelText">Calling Line ID Last Name</label><span class="required">*</span><br>
								<input type="text" name="callingLineIdLastName" id="callingLineIdLastName" size="30" maxlength="30" value="<?php echo $_SESSION["userInfo"]["callingLineIdLastName"]; ?>">
								</div>
							</div>
							
						</div>
						
						<div class="row">
                                                            
                                                                <!-- Name Dialing Names @ 23 Oct 2018 -->
                                                                <?php
                                                                    $cssStyle = "display: none;";                    
                                                                    if($ociVersion=="20" || $ociVersion=="21"  || $ociVersion=="22"){
                                                                        $cssStyle = "display: block;";                                                                        
                                                                    }
                                                                ?>
                                                                <div id="nameDialingNames" style="<?php echo $cssStyle; ?>">
																	<div class="col-md-6">
																		<div class="form-group">	
																		<label class="labelText">Name Dialing First Name:</label><br>
																		<input type="text" name="nameDialingFirstName" id="nameDialingFirstName" size="30" maxlength="30"   value="<?php echo $_SESSION["userInfo"]["nameDialingFirstName"]; ?>">
																		</div>                                                                    
																	</div>
																	
																	<div class="col-md-6">
																		<div class="form-group">	
																		<label class="labelText">Name Dialing Last Name:</label><br>
																		<input type="text" name="nameDialingLastName" id="nameDialingLastName" size="30" maxlength="30"       value="<?php echo $_SESSION["userInfo"]["nameDialingLastName"]; ?>">
																		</div>
																	</div>
																</div>
                                                                 <!-- End code -->                                                            
							</div><!--row-->
							
							
							
			<!--changes end first block -->	
						<div class="row">
 <?php 
                                        $displayCallingLineIdNumber = "style='display: none;'";
                                        $userCallingLineIdNumberPermission = $_SESSION["permissions"]["userCallingLineIdNumber"];
                                        if($userCallingLineIdNumberPermission == "1")
                                        {
                                            $displayCallingLineIdNumber = "style='display: block;float: left;'";
                                        }                        
                                ?>						
							<div class="col-md-6" <?php echo $displayCallingLineIdNumber; ?>>
							 

	                                <div class="form-group" >
	                                <label class="labelText">Calling Line ID Phone Number:</label><br>
                                                        <div class="dropdown-wrap">
                                                            <?php     
                                                                $callingLineIdNumber = $_SESSION["userInfo"]["callingLineIdPhoneNumber"];
                                                                if(stripos($_SESSION["userInfo"]["callingLineIdPhoneNumber"],"+") !== false) {
                                                                        $defaultCountryCode = "+$defaultCountryCode";
                                                                        $callingLineIdNumberArr = explode($defaultCountryCode, $callingLineIdNumber); 
                                                                        $callingLineIdNumberStrToCompare     = $callingLineIdNumberArr[1];
                                                                        $_SESSION["userInfo"]["callingLineIdPhoneNumber"] = $callingLineIdNumberStrToCompare;
                                                                }
                                                                else
                                                                {
                                                                        $callingLineIdNumberStrToCompare = $_SESSION["userInfo"]["callingLineIdPhoneNumber"];
                                                                }
                                                            ?>
                                                                <select name="callingLineIdPhoneNumber" id="callingLineIdPhoneNumber">
                                                                        <option value=""></option>
                                                                         <?php                                                                                                    
                                                                            //Code added @ 20 Dec 2018
                                                                            /*$callingLineIdNumber = $_SESSION["userInfo"]["callingLineIdPhoneNumber"];
                                                                            if(strpos($callingLineIdNumber,"-")){
                                                                                $callingLineIdNumberArr = explode("-", $callingLineIdNumber); 
                                                                                $callingLineIdNumberStrToCompare = $callingLineIdNumberArr[1];
                                                                                $_SESSION["userInfo"]["callingLineIdPhoneNumber"] = $callingLineIdNumberStrToCompare;
                                                                            }else{
                                                                                $callingLineIdNumberStrToCompare = $_SESSION["userInfo"]["callingLineIdPhoneNumber"];
                                                                            }*/
                                                                            //End code      
                                                                            foreach ( $allNumbers as $key => $value ) {
                                                                                            //if ($_SESSION ["userInfo"] ["callingLineIdPhoneNumber"] == $value) {
                                                                                            if ($callingLineIdNumberStrToCompare == $value) { //Code added @ 20 Dec 2018    
                                                                                                    //$sel = "SELECTED";
                                                                                                    echo "<option value='$value' SELECTED>$value</option>";

                                                                                            } else {                                                                                                                                    
                                                                                                    //$sel = "";
                                                                                                    echo "<option value='$value'>$value</option>";
                                                                                            }
                                                                                            //echo "<option value=\"" . $value . "\"" . $sel . ">" . $value . "</option>";
                                                                            }
                                                                            ?>
                                                                </select>
                                                        </div>
									</div>
								</div>
								<?php
									 $displayNCOS = "style='display: none;'";
									 $networkClsOfServicePermission = $_SESSION["permissions"]["userNCOS"];
									if($networkClsOfServicePermission == "1") {
										$displayNCOS = "style='display: block; float: left;'";
									} 
							    ?>
								<div class="col-md-6" <?php echo $displayNCOS; ?>>
									 <div class="form-group">
									<label class="labelText">Network Class of Service:</label>    
                                    <div class="dropdown-wrap">
                                       
                                        <select name="networkClassOfService" id="networkClassOfService">
                                            <?php echo selectNetworkClassOfService($_SESSION["userInfo"]["networkClassOfService"]); ?>
                                        </select>
                                    </div>
                                </div>
								
								</div>
								
								
							</div>
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText">Email Address:</label><br>
										<input type="text" name="emailAddress" id="emailAddress" size="35" maxlength="80" value="<?php echo $_SESSION["userInfo"]["emailAddress"]; ?>">
									</div>
								</div>
								
								<div class="col-md-6">
									<?php 
                                        $displayLocation = "style='display: none;'";
                                        if($locationFieldVisible == "true")
                                        {
                                            $displayLocation = "style='display: block;'";
                                        }                        
									?>
								
									<div class="form-group" <?php echo $displayLocation; ?>>
										<label class="labelText">Location:</label><br />
										<input type="text"  name="addressLocation" id="addressLocation" size="35" maxlength="80" value="<?php echo $_SESSION["userInfo"]["addressLocation"]; ?>">
									</div>
								</div>
							
							</div>
							
							<!--department fields-->
							<div class="row">
							<?php 
                                        $departmentDisplay = "style='display: none;'";
                                        if($useDepartments == "true")
                                        {
                                            $departmentDisplay = "style='display: block;'";
                                        }
		
                                ?>
								<div class="col-md-6" <?php echo $departmentDisplay; ?> >
									
								<div class="form-group">
									<label class="labelText">Department:</label>
									<div class="dropdown-wrap">
										<select name="department" id="department">
										<option value=""></option>
											<?php
												foreach ( $departments as $value ) {
														if ($value == $_SESSION ["userInfo"] ["department"]) {
																$sel = "SELECTED";
														} else {
																$sel = "";
														}
														echo "<option value=\"" . $value . "\"" . $sel . ">" . $value . "</option>";
												}
											?>
										</select>                                    
									</div>
								</div>
								
								
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText">Custom Contact Directory:</label>
										<div class="dropdown-wrap">
											<select name="ccd" id="ccd">
												<option value=""></option>
												<?php
																	$ccd = "";
																	if(isset($_SESSION['userInfo']['ccd']) && $_SESSION['userInfo']['ccd']!=""){
																		$ccd = $_SESSION['userInfo']['ccd'];
																	}
												if (isset ( $directories )) {
													foreach ( $directories as $key => $value ) {
												?>
													<option value="<?php echo $value; ?>" <?php if($value==$ccd) echo "selected"; ?> > <?php echo $value; ?></option>
																					<?php
													}
												}
												?>
											</select>                                   
										</div>
									</div>
								</div>
							</div>	
							
							<!--end department fields -->
							
							<!--Address information -->
									
						    <!--address information -->
								
								<?php  
                
									$displayAddresInfo = "style='display: none;'";
									$addressInfoPermission = $_SESSION["permissions"]["userAddress"];
									if($addressFieldsModifiable == "true" && $addressInfoPermission == "1")
									{                        
										$displayAddresInfo = "style='display: block;'";                                         
									}
								?>
								
								
								<?php
									if($addressFieldsModifiable == "true" && $addressInfoPermission == "1"){ ?>
										<div class="row">
										<h2 class="subBannerCustom">Address Information</h2>
											<div class="col-md-4">
												<div class="form-group">
													<label class="labelText"><?php if ($bwRequireAddress == "true") echo "<span class=\"required\">*</span> "; ?>Address:</label><br>
													<input type="text" name="addressLine1" id="addressLine1" size="35" maxlength="80" value="<?php echo $_SESSION["userInfo"]["addressLine1"]; ?>" style="width:100% !important">
												</div>
											</div>
											
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="labelText">Suite:</label><br>
													<input type="text" name="addressLine2" id="addressLine2" size="35" maxlength="80" value="<?php echo $_SESSION["userInfo"]["addressLine2"]; ?>" style="width:100% !important">
												</div>
											</div>
											
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="labelText"><?php if ($bwRequireAddress == "true") echo "<span class=\"required\">*</span> "; ?>City:</label><br>
													<input type="text" name="city" id="city" size="35" maxlength="50" value="<?php echo $_SESSION["userInfo"]["city"]; ?>" style="width:100% !important">
												</div>
											</div>
										
										</div>
									
									
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<label class="labelText"><?php if ($bwRequireAddress == "true") echo "<span class=\"required\">*</span> "; ?>State/Province:</label><br>
													<div class="dropdown-wrap">
														<select name="stateOrProvince" style="width:100% !important">
															<option value=""></option>
															<?php
																foreach ($states as $key => $value)
																{
																	if ($_SESSION["userInfo"]["stateOrProvince"] == $value)
																	{
																		$sel = "SELECTED";
																	}
																	else
																	{
																		$sel = "";
																	}
																	echo "<option value=\"" . $value . "\"" . $sel . ">" . $value . "</option>";
																}
															?>
														</select>
													</div>
												</div>
											</div>
											
											<div class="col-md-4">
												<div class="form-group">
													<label class="labelText"><?php if ($bwRequireAddress == "true") echo "<span class=\"required\">*</span> "; ?>Zip/Postal Code:</label><br>
													<input type="text" name="zipOrPostalCode" id="zipOrPostalCode" size="11" maxlength="10" value="<?php echo $_SESSION["userInfo"]["zipOrPostalCode"]; ?>" style="width:100% !important">
												</div>
											</div>
											
											<div class="col-md-4">
												 <?php              
												$displayTimeZone = "style='display: none;'";
												$timeZonePermission = $_SESSION["permissions"]["timeZone"];
												if($timeZonePermission == "1") {
                                                ?>
													<div class="form-group" style="display: block;">	
														<label class="labelText">Time Zone</label><span class="required">*</span>
														<div class="dropdown-wrap">
															<select name="timeZone" id="timeZone" style="width:100% !important">
																<option value=""></option>
																<?php
																	foreach ( $timeZones as $key => $value ) {
																			if ($value == $_SESSION ["userInfo"] ["timeZone"]) {
																					$sel = "SELECTED";
																			} else {
																					$sel = "";
																			}
																			echo "<option value=\"" . $value . "\"" . $sel . ">" . $value . "</option>";
																	}
																?>
															</select>
														</div>
													</div>
												<?php 
												}
											?>
										</div>
									</div>
								<?php } ?>
							</div> 

 
                        
		 
<!--New code found(May be arrangment)start-->
				<div id="servicePacks_1" class="userDataClass" style="display:none;"><!--begin servicePacks_1 div-->
                            <h2 class="subBannerCustom">Service Packs <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Drag and Drop Service Packs from Left to Right"><img src="images/NewIcon/info_icon.png" /></a></h2>
						<div>
							<div class="col-md-6 availNCurrentGroup">
								<div class="form-group">
									Available Service Pack
									<!-- <a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="Drag Service Pack(s) that you want to apply on your phone from Left to Right."><img src="images/NewIcon/info_icon.png"></a> -->
								</div>
							</div>
							
							<div class="col-md-6 availNCurrentGroup">
								<div class="form-group">
									Assigned Service Pack
									<!-- <a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="Drag Service Pack(s) that you want to remove from your phone from Right to Left."><img src="images/NewIcon/info_icon.png"></a> -->
								</div>
							</div>
						</div>
						
						<div class="">
    						<div class="col-md-6" style="padding-left:0;">
    							<div class="form-group scrollableListCustom1" style="overflow-x:auto;max-height:280px;">
    								<ul id="sortable_3" class="connectedSortable ui-sortable connectedSortableCustom">
    									<?php echo buildServicePacksSelectionAvailable($servicePacks) ?>
    								</ul>
    							</div>
    						</div>
    						
    						<div class="col-md-6" style="padding-right:0;">
    							<div class="form-group scrollableListCustom2" style="overflow-x:auto;max-height:280px;">
    								<ul id="sortable_4" class="connectedSortable ui-sortable connectedSortableCustom">
    								<?php echo buildServicePacksSelectionAssign($servicePacks) ?>
    								</ul>
    								<input type="hidden" name="servicePack" id="servicePack"
									value="<?php echo buildServicePacksSelectionAssignHidden($servicePacks);?>" />
    							</div>
    						</div>
						</div>
						
                        <!-- Services Start -->
						
							<div class="row">
								<div class="col-md-12">
								<h2 class="subBannerCustom">Services <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Drag and Drop Services from Left to Right"><img src="images/NewIcon/info_icon.png" /></a> </h2>
						<div class="col-md-6" style="float:left;padding-left:0px;">
                        				<div class="form-group alignCenter assignUserSerTitle">
                        					<label class="labelTextGrey">Available Services</label><br>
                        				</div>
				
                        				<div class="form-group alignCenter dnranges userServiceDivHeight" style="padding:0 !important;">
                        					<ul name="availableUserServiceToAssign[]" id="availableUserServiceToAssign" multiple="" class="connectedSortable" style="border-radius:0;">
                        						<?php echo buildServiceSelectionAvailable($serviceResponse["Success"]["Services"]['unassigned']); ?>
                        					</ul>
                        				</div>
									</div>
			                <!-- button arrow  -->
                                            <!--
							<div class="userServiceBts">
								<div class="form-group alignCenter servicesButton ">
                					<button id="addUserService" type="button" class="addBlueCircle"> 
                						<img src="/Express/images/icons/arrow_Add.png">
                					</button>
                    				
                                                    <br> 
                    				
                					<button id="removeFromUserAssigned" type="button" class="addRedCircle"> 
                						<img src="/Express/images/icons/arrow_remove.png">
                					</button>
                					
                                                    <br><br> 
                				   
                				   <button id="addUserServiceAll" type="button" class="addBlueCircle"> 
                						<img src="/Express/images/icons/arrow_add_all.png">
                					</button>
                					
                                                    <br>
                                  
                				  <button id="removeAllServiceFromAssigned" type="button" class="addRedCircle"> 
                						<img src="/Express/images/icons/arrow_remove_all.png">
                				  </button>
			  					</div>
		   					</div>
                                                        -->
		                  <!-- Assign N/W service -->
					<div class="col-md-6" style="float:left; ">
                				<div class="form-group alignCenter assignUserSerTitle">
                					<label class="labelTextGrey ">Assigned Services</label>
                				</div>
                				
                				<div class="form-group alignCenter dnranges userServiceDivHeight" style="padding:0 !important;">
                					<ul id="assignedUserService" name="assignedUserService[]" multiple="" class="connectedSortable" style="border-radius:0;">
                						<?php echo buildUserServiceSelectionAssign($serviceResponse["Success"]["Services"]['assigned']); ?>
                					</ul>
                					<input id="assignedUserServicesVal" name="assignedUserServicesVal" type="hidden" value = "<?php echo implode(";", $serviceResponse["Success"]["Services"]['assigned']); ?>">
                				 </div>
							</div>
									
							</div>
						</div>
						<!-- Services End -->
						
					   	<div class="row servDivAlign"  style="display: <?php echo ($voiceMessagingVisible == "true" ? " block" : " none"); ?> ">
							<div class="col-md-6" style="margin-top:15px;"></div>	
							<div class="col-md-6" style="margin-top:15px;">	
								<div class="col-md-6"><label class="labelText">Voice Messaging:</label></div>
								<div class="col-md-6">
									<input type="radio" name="voiceMessaging" id="voiceMessagingYes"
										value="Yes" style="width: 5%;" <?php if($varData == "true"){echo "checked";}?>><label class="labelText"
										for="voiceMessagingYes"><span></span>Yes</label>
									<input type="radio"
										name="voiceMessaging" id="voiceMessagingNo" value="No"
										style="width: 5%;" <?php if($varData == "false"){echo "checked";}?>> <label class="labelText labelButtonMargin"
										for="voiceMessagingNo"><span></span>No</label>
								</div>							
									
							</div>
						</div>
						
<!-- New code added @ 09 July 2018 -->
                        
                        <div class="row servDivAlign">
                        	<div class="col-md-6" style="margin-top:15px;"></div>	
                        	<div class="col-md-6" style="margin-top:15px;">
                        	<div class="col-md-6">
                        		<label class="labelText">Shared Call Appearance:</label></div>
                        	<div class="col-md-6">
                        		<input type="radio" name="sharedCallAppearance" id="sharedCallAppearanceYes"
        							value="Yes" style="width: 5%;" ><label class="labelText"
										for="sharedCallAppearanceYes"><span></span>Yes</label>
								<input type="radio" name="sharedCallAppearance" id="sharedCallAppearanceNo" value="No"
        							style="width: 5%;"> <label class="labelText labelButtonMargin"
        							for="sharedCallAppearanceNo"><span></span>No</label>
        					</div>
                        	</div>	
        					
						</div>

                            <div class="row servDivAlign" style="clear: both;">
                        	<div class="col-md-6" style="margin-top:15px;"></div>	
                        	<div class="col-md-6" style="margin-top:15px;">
                        	<div class="col-md-6">
                        		<label class="labelText">Polycom Phone Services:</label></div>
                        	<div class="col-md-6">
							<input type="radio" name="polycomPhoneServices" id="polycomPhoneServicesYes" value="Yes" style="width: 5%;" > 
                        		<label class="labelText" for="polycomPhoneServicesYes"><span></span>Yes</label>
								<input type="radio" name="polycomPhoneServices" id="polycomPhoneServicesNo" value="No" style="width: 5%;">  
								<label class="labelText labelButtonMargin" for="polycomPhoneServicesNo"><span></span>No</label>
        					</div>
                        	</div>	
        					<div style=\"clear:left;\">&nbsp;</div>
			    </div>	
				</div><!--end servicePacks_1 div-->
				
                        <div id="speedDial8_1" class="userDataClass" style="display:none;"><!--begin speedDial8_1 div-->
							<h2 class="subBannerCustom">Speed Dial 8</h2>
							<div class="row">
							<div class="">
								<div class="col-md-4 textAlignCenter">
									<label class="labelText">Speed Code</label>
								</div>
								<div style="" class="col-md-4 textAlignCenter">
									<label class="labelText">Phone Number / SIP-URI<span class="required">*</span></label>
								</div>
								<div style="" class="col-md-4 textAlignCenter">
									<label class="labelText">Name</label>
								</div>
								<div style=\"clear:left;\">&nbsp;</div>
								<?php
									for ($i = 2; $i <= 9; $i++)
									{
										echo "<div style=\"text-align:center;padding:9px 0px;float:left;\" class=\"col-md-4 labelTextGrey\">" . $i . "</div>";
										$ph = $_SESSION["userInfo"]["speedDial8"][$i]["phoneNumber"]; 
										$nm = $_SESSION["userInfo"]["speedDial8"][$i]["name"];
										$phoneNumber = "<input style=\"width:100% !important;\" type=\"text\" name=\"speedDial8[" . $i . "][phoneNumber]\" id=\"speedDial8[" . $i . "][phoneNumber]\" value=\"" . $ph . "\">";
										$name = "<input style=\"width:100% !important;\" type=\"text\" name=\"speedDial8[" . $i . "][name]\" id=\"speedDial8[" . $i . "][name]\" value=\"" . $nm . "\">";
										echo "<div class=\"col-md-4\" style=\"float:left\">" . $phoneNumber . "</div>"; 
										echo "<div class=\"col-md-4 \" style=\"float:left\">" . $name . "</div>";
										echo "<div style=\"clear:left;\">&nbsp;</div>";
									}
								?>
							</div>
							</div>
						</div><!--end speedDial8_1 div-->					

				<div id="speedDial100_1" class="userDataClass" style="display:none;"><!--begin speedDial100_1 div-->
							<h2 class="subBannerCustom">Speed Dial 100</h2>
					<div class="row">
							<div class="form-group">
							<div class="col-md-6">
								<a style="float:left;margin-right:5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="Upload Speed Dials (will not work in IE 9 or earlier)"><img src="images/NewIcon/info_icon.png" /></a>
								<input type="file" class="inputfile" value="Browse" name="file" id="file" onchange="handle_files(this.files)" style="width: 82px;">			
							</div>
							<div class="col-md-6 sampleCsvDiv">
								<a href="./userMod/sample.csv" class="labelText">Click here</a><span class="labelTextGrey"> for a sample template</span>
							</div>
							</div>
							</div>
					<div class="row">
					<div class="col-md-12">					
						<div class="form-group">
						<div style="" class="addSpeedDialBox">
							<div class="speedDials row" style="zoom:1;">
								<div class="col-md-3">
                                    		<div class="form-group">
                                       		<label class="labelText">Speed Code 100</label>
                                   		</div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label class="labelText">Phone Number / SIP-URI<span class="required">*</span></label>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label class="labelText">Description</label>
                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group">
                                        <label class="labelText">Remove Speed Dial</label>
                                    </div>
                                </div>
									<?php
										$a = 0;
										foreach ($_SESSION["userInfo"]["speedDial100"] as $key => $value)
										{
												$speedCode = $value["speedCode"];
												$phone = $value["phone"];
												$description = $value["description"];
											?>
											<div class ="alignMentClass" id="a_<?php echo $a; ?>">
												<div class="col-md-3">
												  <div class="form-group">
												  <div class="dropdown-wrap">
													<select name="speedCode[]" id="speedCode[]">
														<?php
															for ($i = 0; $i <= 99; $i++)
															{
																$pad = str_pad($i, 2, "0", STR_PAD_LEFT);
																echo "<option value=\"" . $pad . "\"";
																if ($pad == $speedCode)
																{
																	echo " SELECTED";
																}
																echo ">" . $pad . "</option>";
															}
														?>
													</select>
													</div>
													</div>
												</div>
												<div class="inputText col-md-3">
    												 <div class="form-group">
    												 	<input type="text" name="phone[]" value="<?php echo $phone; ?>" size="25" id="phone[]">
    												 </div>
												 </div>
												<div class="quarterDesc col-md-3">
        											<div class="form-group">
        												<input type="text" name="description[]" value="<?php echo $description; ?>" size="25" maxlength="25" id="description[]">
        											</div>
												</div>
												<div class="inputText col-md-3">
												 	<div class="form-group">
														<input style="height:28px; width: 135px;" type="button" class="remSpeedDial deleteBtn" value="Remove" id="removeSpeedDial_<?php echo $a; ?>">
													</div>
												 </div>	
											</div>
										
											<?php
												$a++; 
										}
									?>
									<div class="col-md-12" id="addSpeedDialDiv">
									<div class="form-group">
									<input type="button" value="Add Speed Dial" id="addSpeedDial" style="float: left;">
									</div>
									</div>
								</div>
							</div>
							</div>
							</div>
							</div>
						</div><!--end speedDial100_1 div-->
						<input type="hidden" id="speedCode[]" name="speedCode[]" value="NIL">
						<input type="hidden" id="phone[]" name="phone[]" value="NIL">
						<input type="hidden" id="description[]" name="description[]" value="NIL">
						<?php
							if ($isVMAllowed && $_SESSION["userInfo"]["thirdPartyVoiceMail"] == "true" or $_SESSION["userInfo"]["voiceMessagingUser"] == "true")
							{
								?>
                                <!--start voiceManagement_1 div-->
								<div id="voiceManagement_1" class="userDataClass" style="display:none;"><!-- begin voiceManagement div-->
									<h2 class="subBannerCustom"><?php echo $_SESSION["userInfo"]["thirdPartyVoiceMail"] == "true" ? "Third-Party Voice Mail" : "Voice Management"; ?></h2>
									
							
									<div class="row">
                                    <div class="col-md-12 marginBottom50">
                                        <div style="" class="col-md-4">
                                        <div class="form-group">
                                            <label class="labelText">Reset Voice Mail Passcode</label>
                                        </div>
                                        </div>
                                        <div style="" class="col-md-8">
                                        <div class="form-group">
                                            <input style="" type="hidden" name="resetPortal" value="false" />
                                            <input style="" type="checkbox" name="resetPortal" id="resetPortal" value="true"><label for="resetPortal"><span></span></label>
                                            <label class="labelText">Reset Mailbox Passcode</label>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 marginBottom50">
									    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="labelText">Voice Messaging/Support</label>
                                        </div>
                                        </div>
										<div style="" class="col-md-8">
                                        <div class="form-group">
                                            <input style="" type="radio" name="isActive" id="isActiveTrue" value="true" <?php echo $_SESSION["userInfo"]["isActive"] == "true" ? "checked " : ""; ?>/>
                                            <!-- <label class="labelText">On</label> -->
                                            <label for="isActiveTrue" class="labelText labelTextMarginNeg"><span></span></label><label class="labelText">On</label>
											<input style="" type="radio" name="isActive" id="isActiveFalse" value="false" <?php echo $_SESSION["userInfo"]["isActive"] == "false" ? "checked " : ""; ?>/>
                                          
                                            <label for="isActiveFalse" class="labelText labelTextMargin labelButtonMargin"><span></span></label><label class="labelText">Off</label>
                                            <br>
											<input style="" type="hidden" name="alwaysRedirectToVoiceMail" value="false" />
											<input style="" type="checkbox" name="alwaysRedirectToVoiceMail" id="alwaysRedirectToVoiceMail" value="true"<?php echo $_SESSION["userInfo"]["alwaysRedirectToVoiceMail"] == "true" ? " checked" : ""; ?>>
				                                            <label class="" for="alwaysRedirectToVoiceMail"><span></span></label>
				                                             <label class="labelText">Send All Calls to Voice Mail</label>
				                                            <br>
											<input style="" type="hidden" name="busyRedirectToVoiceMail" value="false" />
											<input style="" type="checkbox" name="busyRedirectToVoiceMail" id="busyRedirectToVoiceMail" value="true"<?php echo $_SESSION["userInfo"]["busyRedirectToVoiceMail"] == "true" ? " checked" : ""; ?>>
				                                            <label class="" for="busyRedirectToVoiceMail"><span></span></label>
				                                            <label class="labelText">Send Busy Calls to Voice Mail</label>
				                                            <br>
											<input style="" type="hidden" name="noAnswerRedirectToVoiceMail" value="false" />
											<input style="" type="checkbox" name="noAnswerRedirectToVoiceMail" id="noAnswerRedirectToVoiceMail" value="true"<?php echo $_SESSION["userInfo"]["noAnswerRedirectToVoiceMail"] == "true" ? " checked" : ""; ?>>
				                                            <label class="" for="noAnswerRedirectToVoiceMail"><span></span></label>
				                                            <label class="labelText">Send Unanswered Calls to Voice Mail</label>
										</div>
                                        </div>
                                    </div>

										<?php
											if ($_SESSION["userInfo"]["thirdPartyVoiceMail"] == "true")
											{
												?>
                                            <div class="col-md-12 solTest">
												<div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="labelText">Third-Party Voice Mail Server</label>
                                                </div>
                                                </div>
												<div class="col-md-8">
                                              					<div class="form-group">
													<input type="radio" name="serverSelection" id="serverSelectionGroup" value="Group Mail Server" <?php echo $_SESSION["userInfo"]["serverSelection"] == "Group Mail Server" ? "checked " : ""; ?>/>
                                                    <label for="serverSelectionGroup"><span></span></label>
                                                    <label class="labelText">Group Mail Server</label><br>
													<input type="radio" name="serverSelection" id="serverSelectionUser" value="User Specific Mail Server" <?php echo $_SESSION["userInfo"]["serverSelection"] == "User Specific Mail Server" ? "checked " : ""; ?>/>
                                                    <label for="serverSelectionUser"><span></span></label>
                                                    <label class="labelText">User Specific Mail Server</label>
                                                </div>
                                                <div class="form-group">
													<input type="text" name="userServer" id="userServer" size="60" maxlength="161" value="<?php echo $_SESSION["userInfo"]["userServer"]; ?>">
												</div>
                                                </div>
                                            </div>

										    <div class="col-md-12">
                                                <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="labelText">Mailbox ID on Third-Party Voice Mail Platform</label>
                                                </div>
                                                </div>
												<div class="col-md-8">
                                                <div class="form-group">
													<input type="radio" name="mailboxIdType" id="mailboxIdTypeNumber" value="User Or Group Phone Number" <?php echo $_SESSION["userInfo"]["mailboxIdType"] == "User Or Group Phone Number" ? "checked " : ""; ?>/>
                                                    <label for="mailboxIdTypeNumber"><span></span></label>
                                                    <label class="labelText">User's (or Group's) Phone Number</label>
													<input type="radio" name="mailboxIdType" id="mailboxIdTypeURL" value="URL" <?php echo $_SESSION["userInfo"]["mailboxIdType"] == "URL" ? "checked " : ""; ?>/><label for="mailboxIdTypeURL"><span></span></label>
                                                    <label class="labelText">SIP-URI</label>
                                                </div>
                                                <div class="form-group">    
													<input type="text" name="mailboxURL" id="mailboxURL" size="60" maxlength="161" value="<?php echo $_SESSION["userInfo"]["mailboxURL"]; ?>">
												</div>
                                                </div>
                                            </div>
												

										    <div class="col-md-12">
                                                <div class="col-md-4">
                                                <div class="form-group">
                                                <label class="labelText">Number of rings before greeting</label>
                                                </div>
                                                </div>
												<div class="col-md-8">
                                                <div class="form-group">
													<select name="noAnswerNumberOfRings" id="noAnswerNumberOfRings">
														<option value="0">None</option>
														<?php
															for ($i = 2; $i <= 20; $i++)
															{
																echo "<option value=\"" . $i . "\"";
																if ($i == $_SESSION["userInfo"]["noAnswerNumberOfRings"])
																{
																	echo " SELECTED";
																}
																echo ">" . $i . "</option>";
															}
														?>
													</select>
                                                </div>
												</div>
                                            </div>
												<?php
											}
											else
											{
												?>
                                            <div class="col-md-12 marginBottom50">    
												<div class="col-md-4">
                                                <div class="form-group">
                                                <label class="labelText">When a message arrives...</label>
                                                </div>
                                                </div>
												<div style="" class="col-md-8">
                                                <div class="form-group">
													<input type="radio" name="processing" id="processingUnified" value="Unified Voice and Email Messaging" <?php echo $_SESSION["userInfo"]["processing"] == "Unified Voice and Email Messaging" ? "checked " : ""; ?>/><label for="processingUnified" class="labelTextMarginNeg"><span></span></label>
                                                    <label class="labelText">Use unified messaging</label>
                                                    <br>
								<input type="hidden" name="usePhoneMessageWaitingIndicator" value="false" />
							<input type="checkbox" name="usePhoneMessageWaitingIndicator" id="usePhoneMessageWaitingIndicator" value="true"<?php echo $_SESSION["userInfo"]["usePhoneMessageWaitingIndicator"] == "true" ? " checked" : ""; ?>>
                                                    <label for="usePhoneMessageWaitingIndicator" style="margin-top:7px;"><span></span></label>
                                                    <label class="labelText" style="margin-left: 17px;">Use Phone Message Waiting Indicator</label>
                                                    <br>
													<input type="radio" name="processing" id="processingForward" value="Deliver To Email Address Only" <?php echo $_SESSION["userInfo"]["processing"] == "Deliver To Email Address Only" ? "checked " : ""; ?>/> 
                                                    <label for="processingForward" class="labelTextMarginNeg"><span></span></label>
                                                    <label class="labelText">Forward it to this email address</label>
                                                    <br>
													<input type="text" name="voiceMessageDeliveryEmailAddress" id="voiceMessageDeliveryEmailAddress" size="32" maxlength="80" value="<?php echo $_SESSION["userInfo"]["voiceMessageDeliveryEmailAddress"]; ?>">
												</div>
                                                </div>
                                            </div>    
                                            <div class="col-md-12">    
                                                <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="labelText">Additionally...</label>
                                                </div>
                                                </div>
                                                <div style="" class="col-md-8">
                                                <div class="form-group">
													<input type="hidden" name="sendVoiceMessageNotifyEmail" value="false" />
													<input type="checkbox" name="sendVoiceMessageNotifyEmail" id="sendVoiceMessageNotifyEmail" value="true"<?php echo $_SESSION["userInfo"]["sendVoiceMessageNotifyEmail"] == "true" ? " checked" : ""; ?>>
                                                    <label for="sendVoiceMessageNotifyEmail"><span></span></label>
                                                    <label class="labelText">Notify me by email of the new message at this address</label>
                                                    <br>
													<input type="text" name="voiceMessageNotifyEmailAddress" id="voiceMessageNotifyEmailAddress" size="32" maxlength="80" value="<?php echo $_SESSION["userInfo"]["voiceMessageNotifyEmailAddress"]; ?>">
													<input type="hidden" name="sendCarbonCopyVoiceMessage" value="false" />
                                                    
                                                </div>
                                                <div class="form-group">
													<input type="checkbox" name="sendCarbonCopyVoiceMessage" id="sendCarbonCopyVoiceMessage" value="true"<?php echo $_SESSION["userInfo"]["sendCarbonCopyVoiceMessage"] == "true" ? " checked" : ""; ?>>
                                                    <label for="sendCarbonCopyVoiceMessage"><span></span></label>
                                                    <label class="labelText">Email a carbon copy of the message to</label>
                                                    <br>
													<input type="text" name="voiceMessageCarbonCopyEmailAddress" id="voiceMessageCarbonCopyEmailAddress" size="32" maxlength="80" value="<?php echo $_SESSION["userInfo"]["voiceMessageCarbonCopyEmailAddress"]; ?>">
													<input type="hidden" name="transferOnZeroToPhoneNumber" value="false" />
                                                </div>
                                                <div class="form-group">
													<input type="checkbox" name="transferOnZeroToPhoneNumber" id="transferOnZeroToPhoneNumber" value="true"<?php echo $_SESSION["userInfo"]["transferOnZeroToPhoneNumber"] == "true" ? " checked" : ""; ?>>
                                                    <label for="transferOnZeroToPhoneNumber"><span></span></label>
                                                    <label class="labelText">Transfer on '0' to Phone Number</label>
                                                    <br>
													<input type="text" name="transferPhoneNumber" id="transferPhoneNumber" maxlength="30" value="<?php echo $_SESSION["userInfo"]["transferPhoneNumber"]; ?>">
												</div>
												
												<div class="form-group">
                                                    <label class="labelText">Mailbox Limit</label>
                                                    <br>
                                                    <div class="dropdown-wrap oneColWidth">
													<select name="mailBoxLimit" id="mailBoxLimit">
                                						<option value="Use Group Default">Use Group Default</option>
                                						<option value="10">10</option>
                                						<option value="20">20</option>
                                						<option value="30">30</option>
                                						<option value="40">40</option>
                                						<option value="50">50</option>
                                						<option value="60">60</option>
                                						<option value="70">70</option>
                                						<option value="80">80</option>
                                						<option value="90">90</option>
                                						<option value="100">100</option>
                                						<option value="200">200</option>
                                						<option value="300">300</option>
                                						<option value="400">400</option>
                                						<option value="500">500</option>
                                						<option value="600">600</option>
                                						<option value="700">700</option>
                                						<option value="800">800</option>
                                						<option value="900">900</option>
                                					</select>
                                					</div>
												</div>
					
												<?php
											}
										?>
									   </div>
                                    </div>
                                </div>
								</div><!--end voiceManagement_1 div-->
								<?php
					}
			}	
		if ($_SESSION ["permissions"] ["resetPasswords"] == "1" || $_SESSION ["permissions"] ["voiceMailPasscode"] == "1") {
			?>
<!--New code found Start (passwords_1)-->
				
		<div id="passwords_1" class="userDataClass" style="display: none;">
			<!-- begin passwords div-->
			<h2 class="subBannerCustom">Passwords</h2>
			
			<?php if ($_SESSION ["permissions"] ["resetPasswords"] == "1") {?>
				
					<div style="text-align: left;" class="col span_20">
						<h2 class="subBannerCustom">
								<input style="width: 20px; display:none;" type="checkbox" name="userPassword" id="webAccessPassword" value="webAccessPassword"/>
									Web Portal Password 
						</h2>
					</div>
				
				<?php if($bwWebPortalPasswordType == "Static"){?>
					<div class="row" id="resetWebDefaultDiv">
						<div class="col-md-6">
							<input type="checkbox" name="resetWebDefault" id="resetWebDefault" value="true" style="width:5%; margin-top:16px; padding-left:30px;"/>
							<label for = "resetWebDefault">
								<span></span>
							</label>
							<label class="labelText">Reset To System Default</label>
						</div>
					</div>
				<?php }?>
				<div class="row" id="passwordDiv">
    				<div class="col-md-6">
    					<div class="form-group">
    						<label class="labelText">New Password</label><br />
    						<input type="password" name="input-password" id="inputPassword" value="" autocomplete="new-password"/>
    					</div>
    				</div>
    				
    				<div class="col-md-6">
    					<div class="form-group">
    						<label class="labelText">Confirm Password</label><br />
    						<input type="password" name="input-confirm-password" id="inputConfirmPassword" value="" autocomplete="new-password"/>
    					</div>
    				</div>
				</div>
			<?php }?>
			<div class="clear">&nbsp;</div>
			<?php if ($_SESSION ["permissions"] ["voiceMailPasscode"] == "1") {?>
				<div class="row">
						<h2 class="subBannerCustom">
							<input style="width: 20px; display:none" type="checkbox" name="userPasscode" id="voiceMailPasscode" value="voiceMailPasscode"/>
								Voice Mail Passcode
						</h2>
				</div>
				<?php if($bwVoicePortalPasswordType == "Static" || ($bwVoicePortalPasswordType == "Formula" && $userVoicePortalPasscodeFormula != "")){?>
					<div class="row" id="resetPassDefaultDiv">
						<div class="col-md-6">
							<input type="checkbox" name="resetPassDefault" id="resetPassDefault" value="true" style="width:5%; margin-top:16px; padding-left:30px;"/>
							<label for = "resetPassDefault">
								<span></span>
							</label>
							<label class="labelText">Reset To System Default</label>
							
						</div>
						
					</div>
				<?php }?>
				<div class="row" id="passcodeDiv">
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText">New Passcode</label><br />
							<input type="password" name="input-passcode" id="inputPasscode" value="" autocomplete="new-password"/>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText">Confirm Passcode</label><br />
							<input type="password" name="input-confirm-passcode" id="inputConfirmPasscode" value="" autocomplete="new-password"/>
						</div>
					</div> 	
				</div>
			<?php }?>
		</div>
		<!--end passwords_1 div-->
<!--New code found End (passwords_1)-->		
                                        <?php
					}
                                        //Code added @ 15 March 2019 regarding EX-1110 
                                        if(isset($_SESSION ["userInfo"] ["deviceType"]) && $_SESSION ["userInfo"] ["deviceType"] != ""){
                                            $dupDeviceType = $_SESSION ["userInfo"] ["deviceType"];
                                            echo "<input type='hidden' id='deviceTypeDigitalDup' value='$dupDeviceType' />";
                                        }
                                        //End code
					if ($_SESSION ["permissions"]["changeHotel"] == "1" and ! ($_SESSION ["userInfo"] ["hotelHostisActive"] == "" and $_SESSION ["userInfo"] ["hotelisActive"] == ""))
					{
						?>
						<div id="hotel_1" class="userDataClass" style="display:none;"><!--begin hotel div-->
							<h2 class="subBannerCustom">Hoteling</h2>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label class="labelText">Hotel Host</label>
										<div class="dropdown-wrap oneColWidth">
										<select name="hotelHostId">
											<option value=""></option>
											<?php
												if ($_SESSION["userInfo"]["hotelHostId"] !== "")
												{
													echo "<option value=\"" . $_SESSION["userInfo"]["hotelHostId"] . "\" SELECTED>" . $_SESSION["userInfo"]["hotelHostName"] . "</option>";
												}
												foreach ($hotelUsers as $key => $value)
												{
													echo "<option value=\"" . $value["userId"] . "\">" . $value["userName"] . "</option>";
												}
											?>
										</select>
									</div>
								    </div>
                                </div>
								<div style="" class="col-md-6">
									<div class="form-group">
									<?php
										if ($_SESSION["userInfo"]["hotelHostisActive"] !== "")
										{
												echo "<img style=\"float:left;margin-left: 3px;\" src=\"images/";
												if ($_SESSION["userInfo"]["hotelHostisActive"] == "true")
												{
													echo "green_light.png";
												}
												else
												{
													echo "red_light.png";
												}
												echo "\" width=\"25\" height=\"25\" align=\"left\">";
											?>
											<label class="labelText">Hoteling Host Active</label>
											<input type="hidden" name="hotelHostisActive" value="false" />
											<input style="" type="checkbox" name="hotelHostisActive" id="hotelHostisActive" value="true"<?php echo $_SESSION["userInfo"]["hotelHostisActive"] == "true" ? " checked" : ""; ?>>
											<label for="hotelHostisActive" class="labelButtonMargin"><span></span></label>
									</div>
									<div class="form-group">
											<?php
										}
										if ($_SESSION["userInfo"]["hotelisActive"] !== "")
										{
												echo "<img style=\"float:left;margin-left: 3px;\" src=\"images/";
												if ($_SESSION["userInfo"]["hotelisActive"] == "true")
												{
													echo "green_light.png";
												}
												else
												{
													echo "red_light.png";
												}
												echo "\" width=\"25\" height=\"25\" align=\"middle\">";
											?>
											<label class="labelText">Hoteling Guest Active</label>
											<input type="hidden" name="hotelisActive" value="false" />
											<input style="" type="checkbox" name="hotelisActive" id="hotelisActive" value="true"<?php echo $_SESSION["userInfo"]["hotelisActive"] == "true" ? " checked" : ""; ?>>
											<label for="hotelisActive" class="labelButtonMargin"><span></span></label>
										<?php } ?>	
									</div>
								</div>
							</div>
						</div><!--end hotel_1 div-->
						<?php
					}
					if ($_SESSION["permissions"]["changeDevice"] == "1" || (isset($_SESSION ["permissions"] ["deviceInfoReadOnly"]) && $_SESSION ["permissions"] ["deviceInfoReadOnly"] == "1") )
					{
						if ( isset($_SESSION ["permissions"] ["deviceInfoReadOnly"]) && $_SESSION ["permissions"] ["deviceInfoReadOnly"] == "1") {
								$headerText = "(Read Only)";
							}
						?>
						<div id="phoneDevice_1" class="userDataClass" style="display:none;"><!--begin phoneDevice_1 div-->
							<h2 class="subBannerCustom">Phone Number Information <?php echo $headerText; ?></h2>
							<div class="row">
								<div class="col-md-6">
                                <div class="form-group">
									<label class="labelText">Phone Number</label><br>
									<div class="dropdown-wrap">
										<select name="phoneNumber" id="phoneNumber">
											<option value=""></option>
											<?php
												if ($_SESSION["userInfo"]["phoneNumber"])
												{
													echo "<option value=\"" . $_SESSION["userInfo"]["phoneNumber"] . "\" SELECTED" . ">" . $_SESSION["userInfo"]["phoneNumber"] . "</option>";
												}
												foreach ($availableNumbers as $value)
												{
													echo "<option value=\"" . $value . "\">" . $value . "</option>";
												}
											?>
										</select>
									</div>
								</div>
								</div>
								<div class="col-md-6">
                                <div class="form-group extDiv">
									<label class="labelText">Extension<span class="required">*</span></label><br>
									<input type="text" name="extension" id="extension" size="<?php echo $_SESSION["groupExtensionLength"]["max"]; ?>" maxlength="<?php echo $_SESSION["groupExtensionLength"]["max"]; ?>" value="<?php echo $_SESSION["userInfo"]["extension"]; ?>">
								</div>
							    </div>
						
							<?php if(!empty($_SESSION["userInfo"]["phoneNumber"])){?>
								<div class="col-md-6" id="divActivateNumber">
									<div class="form-group">
										<input type="checkbox" name="uActivateNumber" id="uActivateNumber" value="Yes" <?php if($_SESSION["userInfo"]["uActivateNumber"] == "Yes"){ echo "checked";} ?> /><label for="uActivateNumber"><span></span></label>
                                        <label class="labelText">Activate Number</label>
									</div>
									
								</div>
							
							<?php }?>
							</div>
							<h2 class="subBannerCustom">Device Information <?php echo $headerText; ?> </h2>            
								<?php 
								if(isset($license["vdmLite"]) && $license["vdmLite"] == "true"){
								if($isVdm and isset($_SESSION["permissions"]["vdmLight"]) and $_SESSION["permissions"]["vdmLight"] == "1"   ) {?>
									<div class="form-group" style="text-align: center;">
                                    	<input type="button" class="navToVDMLightModule" style="color:#fff;" id="modVdm" value="Device Management">
                                    </div>
                                    <?php } }?>
				    <!-- 	code start pankaj -->

				<div class="col-md-12">
				<div style="text-align: center;" class="form-group">

				<?php
				if(isset($_SESSION["userInfo"]["deviceType"]) && $_SESSION["userInfo"]["deviceType"] == "")
				{
				    $checked = isset($deviceIsAudioCodeName) && $deviceIsAudioCodeName == "true" ? "checked" : "";
				?>
				
				<span> <label class="labelText"> User Type : </label></span>
				<input name="userType" id="userTypeDigital" value="digital" checked="" onchange="selectDeviceTypeList(this)" type="radio">
				<label for="userTypeDigital"><span></span></label>
				<label class="labelText" for="userTypeDigital">SIP Phone</label>
				<input name="userType" id="userTypeAnalog" value="analog" style="width: 5%;" <?php echo $checked; ?> onchange="selectDeviceTypeList(this)" type="radio"> 
				<label for="userTypeAnalog"><span></span></label>
				<label class="labelText" for="userTypeAnalog">Analog</label>
				<?php } ?>
				</div>
				</div>
<!--  code end pankaj -->
				
				<?php if(isset($deviceIsAudioCodeName) && $deviceIsAudioCodeName == "true"){
				        $userType = "analog";
				}else{
				    $userType = "digital";
				}?>
				<input type="hidden" name="userTypeResult" id="userTypeResult" value="<?php echo $userType; ?>">
				<input type="hidden" name="userTypePrevious" id="$userType" value="<?php echo $userType; ?>">	
	
	
	<div class="row">	
    	<div class="col-md-6">
    			<div class="form-group">
            		<label class="labelText">Device Type: </label>
            		<?php 
					if(isset($deviceIsAudioCodeName) && $deviceIsAudioCodeName == "true") {
						require_once ("/var/www/html/Express/userMod/analogUserData.php");
						$showAnalogDiv = "block";
						$showDigitalDiv = "none";
						$deviceIndexGroupMod = "block";
						$portNumberGroupMod = "block";
						$tagBundleDiv ="none";
					}
					else if( !isset($_SESSION["userInfo"]["deviceType"]) || $_SESSION["userInfo"]["deviceType"] == "") {
						$showAnalogDiv = "none";
						$showDigitalDiv = "block";
						$deviceIndexGroupMod = "none";
						$portNumberGroupMod = "none";
						$tagBundleDiv ="none";
					} else {
						$showAnalogDiv = "none";
						$showDigitalDiv = "block";
						$deviceIndexGroupMod = "none";
						$portNumberGroupMod = "none";
						$tagBundleDiv ="block";
					}
					
					?>
            		<div class="dropdown-wrap oneColWidth" style="display: <?php echo $showAnalogDiv; ?>" id="analogDeviceTypeVoipDiv">
                        <?php echo buildDeviceTypesSelection($deviceTypesAnalogGateways, "deviceTypeAnalog"); ?>
            		</div>
            		
            		<div class="dropdown-wrap oneColWidth" style="display: <?php echo $showDigitalDiv; ?>" id="digitalDeviceTypeVoipDiv">
                    	<select name="deviceType" id="deviceTypeDigital" class="deviceTypeDigital"
            					onchange="processDeviceTypeChange(this)">
                                   <?php echo selectDeviceType($deviceTypesDigitalVoIP); ?>
            			</select>
            		</div>		
            </div>
    	</div>
	 
	
	 
    	<div class="col-md-6">
            <div class="form-group">
				<label class="labelText">MAC Address</label><br>
			<input type="text" name="macAddress" id="macAddress" size="15" maxlength="12" value="<?php echo $_SESSION["userInfo"]["macAddress"]; ?>">
				 
			<?php
				//check if group has Third Party Voice Mail Support; if so, display Third-Party Voice Mail info
				if ($thirdPartyVoiceMailSupport == "true")
				{
					?>
					Third-Party Voice Mail
					<?php echo $_SESSION["userInfo"]["thirdPartyVoiceMail"] == "true" ? "True" : "False"; ?>
<!--											<input type="hidden" name="thirdPartyVoiceMail" value="false" />
											<input type="checkbox" name="thirdPartyVoiceMail" id="thirdPartyVoiceMail" value="true"<?php echo $_SESSION["userInfo"]["thirdPartyVoiceMail"] == "true" ? " checked" : ""; ?>>-->
					<?php
				}
			?>
			</div>

			<input type="hidden" name="linePortDomainResult" id="linePortDomainResult" value="<?php echo $linePortDomain; ?>">

		</div>
            
                <!--port number dropdown for SIP -->
                 <div class="col-md-6" id="portForSipDeviceDiv" style="display:<?php echo $deviceIsAudioCodeName != 'true' && $spportPortType == 'static' ? 'block' : 'none'; ?> ">
                       <div class="form-group">
                           <label class="labelText" for="">Port Number:</label> <span class="required">*</span>
                            <div class="dropdown-wrap" id="portForSipDeviceSelect">
                            	<?php 
                            	require_once ("/var/www/html/Express/userMod/analogUserData.php");
                					//if(isset($deviceIsAudioCodeName) && $deviceIsAudioCodeName == "true") {
                    					$sp = $_SESSION["sp"];
                    					$grpId = $_SESSION["groupId"];
                    					$deviceName = $_SESSION["userInfo"]["deviceName"];
                    					$deviceType = $_SESSION["userInfo"]["deviceType"];
                    					$userAssignedPort = getUserAssignedPort($sp, $grpId, $deviceName, $userId);
//                     					print_r($userAssignedPort); exit;
                    					if($userAssignedPort != -1) {
                    					    echo buildPortSelectionSIP($userAssignedPort, $deviceType, $deviceName);
                    					}
                					//}
					             ?>
                            </div>
                        </div>   
                    </div>
                 <!--end port number dropdown -->
	
	</div>
	
	<div class="row">	
	 <div class="col-md-6">
	 
<!-- Device index -->
	 	 
        	 	 	<input type="hidden" name="deviceTypeResult" id="deviceTypeResult" value="<?php echo $_SESSION["userInfo"]["deviceType"]; ?>">

        		<input type="hidden" name="previousDeviceType" value="<?php echo $_SESSION["userInfo"]["deviceType"]; ?>">
        		<input type="hidden" name="previousDeviceName" value="<?php echo $_SESSION["userInfo"]["deviceName"]; ?>">
        		<input type="hidden" id="deviceIndexText" name="deviceIndexText" value="">
				<input type="hidden" id="portNumberText" name="portNumberText" value="">
				
				<div id="deviceIndexGroupMod" style="display: <?php echo ($deviceIndexGroupMod == "block" && $spportPortType == 'static') ? 'block' : 'none'; ?>">
        				<div class="form-group"> 	
                                    <label class="labelText">Device Instance:</label>
        					<span class="required">*</span>
        				 
        				<div class="dropdown-wrap oneColWidth" id="deviceIndexDivMod">
        					<?php 
        					if(isset($deviceIsAudioCodeName) && $deviceIsAudioCodeName == "true") {
        					    $deviceNameResources = array();
        					    $deviceNameResources["deviceName"] = $_SESSION["userInfo"]["deviceName"];
        					    $deviceNameResources["deviceType"] =  $_SESSION["userInfo"]["deviceType"];
        					    $deviceNameResources["groupClid"] = isset($_SESSION["groupClid"])? $_SESSION["groupClid"]: "";
        					    $deviceNameResources["macAddress"] = $_SESSION["userInfo"]["macAddress"];
        					    $deviceNameResources["groupId"] = $_SESSION["groupId"];
        					    echo buildDeviceIndexSelection($_SESSION["userInfo"]["deviceName"], $deviceNameResources);
        					}
        					 ?>
        				</div>
        		</div>	
	 	 </div>
	 
	 <!--  port number  -->	
	 	
	 		<div id="portNumberGroupMod" class="form-group" style="display: <?php echo ($portNumberGroupMod == "block" && $spportPortType == 'static') ? 'block' : 'none'; ?>">
				<div class="form-group">
                            <label class="labelText">Port Number:</label><span class="required">*</span>
				<div class="dropdown-wrap oneColWidth" id="portNumberDivMod">

					<?php 
					if(isset($deviceIsAudioCodeName) && $deviceIsAudioCodeName == "true") {
    					$sp = $_SESSION["sp"];
    					$grpId = $_SESSION["groupId"];
    					$deviceName = $_SESSION["userInfo"]["deviceName"];
    					$deviceType = $_SESSION["userInfo"]["deviceType"];
    					$userAssignedPort = getUserAssignedPort($sp, $grpId, $deviceName, $userId);
    					if($userAssignedPort != -1) {
    					    echo buildPortSelection($userAssignedPort, $deviceType, $deviceName);
    					}
					}
					?>
				</div>
			</div>
	 	</div>	 

		</div>	
		</div>
		
		
				<!--	<?php //}?> -->

 						 <div class="row">
							<div class="col-md-6">
                                
                                        <label class="labelText" id="linePortDomainText">Lineport Domain: </label>
                                
                                        <div class="" style="display: none" id="linePortDomainDropdownWithoutDevice">
                                            <div class="form-group Test">
                                                <div class="dropdown-wrap oneColWidth">
                                                    <select name="linePortDomain" id="linePortDomainWithoutDevice" onchange="selectLinePortDomain(this)" >
                                                        <?php selectGroupDomain(); ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
								
								<div class="" style="display: <?php echo $hasDevice ? 'block' : 'none'?>" id="linePortDomainDropdownWithDevice">
                                    <div class="form-group Test">
                                        <div class="dropdown-wrap oneColWidth">
                                            <select name="linePortDomain" id="linePortDomainWithDevice" onchange="selectLinePortDomain(this)" >
                                                <?php selectGroupDomainWithDevice($linePortDomain); ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
								
								
<!--New code found-->								
								<?php  $tagBundleDiv = $deviceIsAudioCodeName == "true" ? 'none' : 'block'; ?>
								  <div class="form-group" id="tagBundleDiv" style="display: <?php echo $tagBundleDiv; ?>">
								  <?php if(isset($license["customProfile"]) && $license["customProfile"] == "true") {?>
                                    <label class="labelText">Phone Profile:</label><br>
                                    <div class="dropdown-wrap" id="customProfile">
                                    <?php echo selectCustomProfile(); ?>
                                    </div>
                                            </div>
                                                        </div>
                        <div class="clr"></div>
                        <!-- add device management -->
                           <?php // if(isset($_SESSION ["userInfo"] ["deviceType"]) && $_SESSION ["userInfo"] ["deviceType"] !=""){$tagBundleDivShowHide = "block";}else{$tagBundleDivShowHide = "none";} ?>
                        <?php $showTagBundles = count($tagBundles) > 0 ? "block" : "none"; ?>
                        <div class="col-md-12" id="deviceMngmtTagBundleDiv" style="display: <?php echo $showTagBundles; ?>">
                            <div class="form-group">
    						<span class="selectInputRadio"> <label class="labelText labelAboveInput" for="">
        							Device Management Tag Bundles: </label><a href="#" style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Select device management tag bundles items with multiple checkboxes"> <img class="infoIcon" src="images/NewIcon/info_icon.png"></a>
        					</span> <br /> 
        					<div class="" id="modTagBundleId">
        					<?php echo selectTagBundle(); ?>
        						<!-- <select name="modTagBundle[]" id="modTagBundle" size="3" multiple="" onchange="processTagBundleChange(this)">
        						<?php 
            						/*$modBundleArr = $modifyDBObj->getAllTagBundles();
            						//print_r($modBundleArr);
            						if(count($modBundleArr) > 0){
            						    foreach ($modBundleArr as $keyTB => $valueTB){
            						        $selectedValForBundle = in_array ( $valueTB, $_SESSION ["userInfo"] ["modTagBundle"] ) ? " selected" : "";
            						        echo "<option ".$selectedValForBundle." value = ".$valueTB.">".$valueTB."</option>";
            						    }
            						}*/
        						
        						?>
                				</select> -->
        					</div>
                        <!-- end device Management --> 
                    <?php } ?>
                     </div>  
<!--New code found-->
                              
                              
                     </div>
                     </div>
                              
<!--        Start Device Port Assignment        -->
		<h2 class="subBannerCustom">Device Port Assignment</h2>
		<div class="row" style="padding:0;width: 1290px;margin-left: -226px;">
                        <div class="autoHeight viewDetailNew">
                        <div style="zoom:1;">
							<table id='deviceLinePortTable' border="1" class="scroll tablesorter scaUserstable" style="border-collapse:collapse;width:100%;">
								<thead>
								<tr>
									<th style="width:8%; background-image: none !important; text-align: center; pointer-events: none; cursor: default;">Primary Line/Port</th>
									<th style="width:14%;">Line/Port</th>
									<th style="width:8%;">Port</th>
									<th style="width:10%;">Endpoint Type</th>
									<th style="width:8%;">User ID</th>
									<th style="width:10%;">Name</th>
									<th style="width:10%;">Phone Number</th>
									<th style="width:8%;">Extension</th>
									<th style="width:8%;">Department</th>
									<th style="width:8%;">Primary Registration</th>
									<th style="width:8%;">SCA Registration</th>
								</tr>
                                </thead>
                                <tbody>
								<?php
                                    if (isset($_SESSION["userInfo"]["devicePortAssignmentDetail"]))
									{
										$devicePortAssignmentDetail = subval_sort($_SESSION["userInfo"]["devicePortAssignmentDetail"], "port");

										for ($a = 0; $a < count($devicePortAssignmentDetail); $a++)
										{
										    $isLinePortUserRegistered = checkUserRegStatus($devicePortAssignmentDetail[$a]["id"]);
										    if($isLinePortUserRegistered) {
										        $linePortUserPrimaryStatusIcon = "images/registered_icon.png";
										        $linePortUserPrimaryTitle = "Registered";
										    } else {
										        $linePortUserPrimaryStatusIcon = "images/not_registered_icon.png";
										        $linePortUserPrimaryTitle = "Not registered";
										    }
										    
										    if($devicePortAssignmentDetail[$a]["endpointType"] == "Primary") {
										        $linePortUserScaStatus = "";
										    }
										    else if($devicePortAssignmentDetail[$a]["endpointType"] == "Shared Call Appearance") {
										        $linePortUserScaStatus = "images/registered_icon.png";
										        $linePortUserScaTitle = "SCA Registered";
										    }
										    else {
										        $linePortUserScaStatus = "images/not_registered_icon.png";
										        $linePortUserScaTitle = "Not SCA Registered";
										    }
										    
											echo "<tr>";
											//echo "<td><input type=\"radio\" class='checkLinePortRebuild' name=\"primary\" id=\"primary" . $devicePortAssignmentDetail [$a] ["linePort"] . "\" value=\"" . $devicePortAssignmentDetail [$a] ["linePort"] . "\"" . ($devicePortAssignmentDetail [$a] ["primary"] == "true" ? " checked" : "") . "/></td>";
    	                                        echo "<td style=\"width:8%;text-align: center;\"><input type=\"radio\" name=\"deviceLinePortPrimary\"  class=\"\" id=\"deviceLinePortPrimary" . $devicePortAssignmentDetail[$a]["linePort"] . "\" value=\"" . $devicePortAssignmentDetail[$a]["linePort"] . "\"" . ($devicePortAssignmentDetail[$a]["primary"] == "true" ? " checked" : "") . "/><label for=\"deviceLinePortPrimary" . $devicePortAssignmentDetail[$a]["linePort"] . "\"><span style=\"margin: -4px 0 0 0;\"></span></label></td>";
    											echo "<td style=\"width:14%;\">" . $devicePortAssignmentDetail[$a]["linePort"] . "</td>";
    											echo "<td style=\"width:8%;\">" .  $devicePortAssignmentDetail[$a]["port"] . "</td>";
    											echo "<td style=\"width:10%;\">" . $devicePortAssignmentDetail[$a]["endpointType"] . "</td>";
    											echo "<td style=\"width:8%;\">" . $devicePortAssignmentDetail[$a]["id"] . "</td>";
    											echo "<td style=\"width:10%;\">" . $devicePortAssignmentDetail[$a]["name"] . "</td>";
    											echo "<td style=\"width:10%;\">" . $devicePortAssignmentDetail[$a]["phoneNumber"] . "</td>";
    											echo "<td style=\"width:8%;\">" . $devicePortAssignmentDetail[$a]["extension"] . "</td>";
    											echo "<td style=\"width:8%;\">" . $devicePortAssignmentDetail[$a]["department"] . "</td>";
    											echo '<td style="width:8%;"> <img style="margin-left: 12px;" src="' . $linePortUserPrimaryStatusIcon . '" width="30" height="30" align="" title="' . $linePortUserPrimaryTitle . '"> </td>';
    											echo '<td style="width:8%;">';
        											if($linePortUserScaStatus != "") {
        											    echo '<img style="margin-left: 12px;" src="' . $linePortUserScaStatus . '" width="30" height="30" align="" title="' . $linePortUserScaTitle . '">';
        											} else {
        											    echo "&nbsp;";
        											}
                                                echo '</td>';
											echo "</tr>";
										}
									}
									else
									{
										echo "<tr><td align=\"center\" colspan=\"100%\">No Data Available</td></tr>";
									}
								?>
							</tbody>	
							</table>
							</div>
						</div>
					</div>
<!--        End Device Port Assignment          -->
                   
                   
                   <?php if($rebuildResetDevice == "true"){?>
                  	<div class="row" id="phoneResetControls" <?php echo "style=\"display: " . ($deviceIsSipPhone ? "block" : "none") . "\""; ?>>
                  			<h2 class="subBannerCustom">Rebuild and Reset</h2>
							<div class="col-md-6">  
                                    <div>
                                        
                                        <div class="form-group">
                                                <input type="hidden" name="rebuildPhoneFiles" id="rebuildPhoneFiles" value="false" />
                                                <input type="checkbox" name="rebuildPhoneFilesNotHidden" id="rebuildPhoneFilesNotHidden" value="true" onclick="updateHiddenPair(this)"><label for="rebuildPhoneFilesNotHidden"><span></span></label>
                                                 <label class="labelText">Rebuild Phone Files</label>
                                        </div>
                                        <div class="form-group">
                                                <input type="hidden" name="resetPhone" id="resetPhone" value="false" />
                                            <input type="checkbox" name="resetPhoneNotHidden" id="resetPhoneNotHidden" value="true" onclick="updateHiddenPair(this)"><label for="resetPhoneNotHidden"><span></span></label>
                                            <label class="labelText">Reset the Phone</label>
                                        </div>
                                    </div>
									                                    

								</div>                                
						</div>
						<div class="row">&nbsp;</div>
						<?php }else{?>
						     <input type="hidden" name="rebuildPhoneFiles" id="rebuildPhoneFiles" value="true" />
                             <input type="hidden" name="resetPhone" id="resetPhone" value="true" />
						<?php }?>	
							
					</div><!--end phoneDevice_1 div-->                            
						
						<!--</div>
						end tab div-->
						
						<?php } ?>
<!-- soft phone work -->	
<?php 
if(isset($license["softPhone"]) && $license["softPhone"] == "true" && $deviceIsAudioCodeName == "false"){
    include('softPhone/softPhoneIndex.php');
}


?>	
<!-- end soft phone work -->			
			
			<!--begin clearMACAddress div-->
			<?php
			if ($_SESSION ["permissions"] ["clearMACAddress"] == "1" && !$phoneDeviceIsVisible && $_SESSION['adminType'] != "superUser") {
			?>
			<div id="clearMACAddress_1" class="userDataClass" style="display: none">
			<div>
			<h2 class="subBannerCustom">Clear MAC Address</h2>
			<?php 
			if(isset($license["vdmLite"]) && $license["vdmLite"] == "true"){
			if($isVdm and isset($_SESSION["permissions"]["vdmLight"]) and $_SESSION["permissions"]["vdmLight"] == "1"   ) {?>
				<div class="form-group" style="text-align: center;">
                                    	<input type="button" class="navToVDMLightModule" style="color:#fff;font-size:20px;" id="modVdm" value="Device Management">
                                    </div>
			<?php } }?>
			</div>
			
			<div style="clear: right;">&nbsp;</div>
					
			<div class="row formspace">
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="">Device Type: </label>
						<label class="labelText"><?php  echo $_SESSION["userInfo"]["deviceType"]; ?></label>
					</div>
				</div>
				
				<div class="col-md-6">
				</div>
			</div>
			
						
						<div class="row formspace">
							<div class="col-md-6">
								<div class="form-group">
									<input type="hidden" name="macAddressClearMac" id="macAddressClearMac" value="<?php echo $_SESSION["userInfo"]["macAddress"]; ?>">
									<label class="labelText" for="">MAC Address:</label>
									<label class="labelText"><?php  echo $_SESSION["userInfo"]["macAddress"]!="" ? $_SESSION["userInfo"]["macAddress"]:"None"; ?></label>
								</div>
							</div>
						</div>
						
						<div class="row formspace">
							<div class="col-md-6">
								<div class="form-group">
									<?php $disableCheckbox = $_SESSION["userInfo"]["macAddress"] == "" ? "disabled" : ""; 
								      $checkVal = $_SESSION["userInfo"]["macAddress"] != "" ? "true" : "false";
								      $style = $_SESSION["userInfo"]["macAddress"] == "" ? "style='cursor:no-drop; opacity:0.5'" : "";
								    ?>
								    
								    <input type="hidden" name="doClearMacAddress" id="" value="false">
									<input type="checkbox" name="doClearMacAddress" id="doClearMacAddress" <?php echo $disableCheckbox." ".$style; ?> value="true">
									<label for = "doClearMacAddress" style="margin-left:10px;"><span <?php echo $disableCheckbox." ".$style; ?>></span></label>
									<label class="labelText">Clear Mac Address</label>
								    
								</div>
							</div>
						</div>
						
						<?php 
						if($_SESSION["userInfo"]["macAddress"] != "") {
						?>
							<input type="hidden" name="macAddress" id="macAddress"
							value="" />
						<?php
						}
						?>
						
					<!--	<div style="text-align: center; margin-top: 50px;">
							<input class="clearMACAddressBtn" name="clearMACAddress" id="clearMACAddressBtn" value="Clear MAC Address" type="button">
						</div>		-->									
			</div>
			<?php } ?>
			<!--End clearMACAddress div-->
			
<!--New code found End(clearMACAddress)-->
			
					<div id="customTags_1" class="userDataClass" style="display: none"><!--begin customTags_1 div-->
                            <h2 class="subBannerCustom">Custom Tags</h2>
                        <div class="row">
                            <!-- Custom Tags Lookup Options-->
                            <div class="col-md-12" style="display:none">
                            <div class="form-group">
                                <div class="customTags_custom">
                                <label class="labelText" for="customTagsLookup">Custom Tags Lookup:</label><br>
                                <select name="customTagsLookup" id="customTagsLookup" onchange="processCustomTagLookupChange(this)"><?php echo selectCustomTagsLookupOptions(); ?></select>
                                </div>
                            </div>
                            </div>
                            <div class="col-md-12">
                            <div class="form-group">
                                <div style="display: none" id="customTagsLookupTableDiv" class="viewDetailRegistration viewDetailRegistrationCustom">
                                    <div class="autoHeight">
                                    <div style="zoom:1;">
                                        <table name="customnTagsLookupTable" id="customTagsLookupTable" class="scroll tablesorter" style="width:100%;margin:0;">
                                        </table>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            </div>

                            <!-- Show No Device message for device-less user -->
                            <div class="col-md-12" id="customTagsNoDeviceMsg" style="display: <?php echo ($hasDevice ? " none" : " block"); ?> ">
                            <div class="form-group textAlignCenter">
							<label class="labelTextGrey">This user has no device</label>
                            </div>
							</div>
                            <div class="" id="customTagsOperations" style="display: <?php echo ($hasDevice ? " block" : " none"); ?> ">
                                <!-- Add new custom tag button -->
				                <div class="col-md-4">
				                <div class="addTagBtnLabel">
    				                <label class="labelText"> Primary Device Custom Tags </label>
    				                <a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add In-Spec Custom Tag">
    				                	<img src="images/NewIcon/info_icon.png">
    				                </a>
				                </div>
                                <div class="customTags_custom form-group">
                                    <input type="button" class="newCustomTag addBtnCustom" name="newCustomTag" id="newCustomTag" value="Add">
                              	</div>
				                </div>
								
								<!-- Create custom tag BUTTON -->
								<div class="col-md-4">
									<div class="textAlignCenter">
        				                <label class="labelText"> Create Tags </label>
        				                <a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Create Out-of-Spec Custom Tag">
        				                	<img src="images/NewIcon/info_icon.png">
        				                </a>
				                	</div>
									<div class="customTags_custom form-group textAlignCenter">
										<input type="button" class="newCustomTag subButton" id="createCustomTagBtn" value="Create">
									</div>
				                </div>
								
				                <div class="col-md-4">
				                	<div class="resetTagBtnLabel">
        				                <label class="labelText"> Reset Tags </label>
        				                <a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Reset all Custom Tags to their default values">
        				                	<img src="images/NewIcon/info_icon.png">
        				                </a>
				                	</div>
									<div class="customTags_custom form-group floatRight">
										<input type="button" class="resetDevicetag deleteBtn" name="resetDevicetag" id="resetDevicetag" value="Reset">
									</div>
				                </div>
                              </div>

                                <!-- Custom Tags Table -->
                                <div class="col-md-12">
									<div id="customTagsTable" class="form-group">
										<!--div style="margin-left: 5%" class="leftDesc">--No Custom Tags--</div-->
										<?php echo $customTags->formatCustomTagsTable(); ?>
					 
									</div>
								
									<div class="">
											<input type="checkbox" name="rebuildPhoneFiles_CustomTag" id="rebuildPhoneFiles_CustomTag" value="true">
											<label for="rebuildPhoneFiles_CustomTag"><span></span></label>
											 <label class="labelText">Rebuild Device Files</label>
									</div>
									<div class="">
										 <input type="checkbox" name="resetPhone_CustomTag" id="resetPhone_CustomTag" value="true">
										 <label for="resetPhone_CustomTag"><span></span></label>
										<label class="labelText">Reset Devices</label>
									</div>
								
								</div>
								
								<!-- Create custom Tag dialog -->
							  <div id="createCustomTagForm" class="dialogClass" style="font-size: 12px">
<!-- 									<form id="createUserCustomTag" name="createUserCustomTag" class="">					 -->
										<div class="row">
											<div class="col-md-12">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-6">
                                                	<div class="customTagErrorMsg" style="display: none; color: red"></div>	
                                                </div>
                                        	</div>
										</div>
										
										<div class="row">
											<div class="col-md-12">
                                        		<div class="col-md-3"></div>
                                         	 	<div class="col-md-6">
                                         			<label for="user_tagName" class="labelText">Tag Name <span class="required">*</span> :</label>
                                         			<div class="form-group">
                                         				<input type="text" name="user_tagName" id="user_tagName" value="" style="float: left;">
                                         			</div>
                                         		</div>
                                        	</div>
										</div>
										<div class="row">&nbsp;</div>
										<div class="row">
        									<div class="col-md-12">
                                        		<div class="col-md-3"></div>
                                         	 	<div class="col-md-6">
                                         			<label for="user_tagValue" class="labelText">Tag Value <span class="required">*</span> :</label>
                                         			<div class="form-group">
                                         				<input  type="text" name="user_tagValue" id="user_tagValue" value="">
                                         			</div>
                                         		</div>
                                        	</div>
										</div>
<!-- 									</form> -->
							  </div>
						</div>
					</div>
					<!--End customTags_1 div-->

<!-- 	call control -->
<?php 
if (isset ( $_SESSION ["userInfo"] ["callControlTab"] ) and $_SESSION ["userInfo"] ["callControlTab"] == "true") {
?>

<div id="control_1" class="userDataClass" style="display: none;">
<h2 class="subBannerCustom">Call Control</h2>

<div class="row">
	 <div class="">
	<?php 
	
	$rowCount = 0;
	foreach ( $callForwardsFields as $key => $value ) {
	    if ($_SESSION ["userInfo"] [$key] == "true") {
	    
	        $ind = $key . "Active";
	        $ind2 = $key . "ForwardToPhoneNumber";
	?>
						
						<div class="col-md-6 minHeight marginZero <?php echo "callService".$rowCount ?>">
						<div class="form-group">
						<input style="float: left;" type="hidden"
						name="<?php echo $key; ?>Active" value="false" /> 
						<input class="newCheckBoxImg" style="width:20px; float:right;margin: 10px 0;"
						type="checkbox" name="<?php echo $key; ?>Active"
						id="<?php echo $key; ?>Active" value="true"
						<?php echo $_SESSION["userInfo"][$ind] == "true" ? " checked" : ""; ?>>
						<label for="<?php echo $key; ?>Active"><span></span></label> 
						
						<label class="labelText fontHead"><b><?php echo $value; ?></b></label>
						<div> &nbsp;</div>
						<label class="labelText"> Forward to phone number / SIP-URI </label>
						<input style="margin-bottom: 15px;width: 75%;" type="text" name="<?php echo $ind2; ?>" id="<?php echo $ind2; ?>"
						maxlength="161" value="<?php echo $_SESSION["userInfo"][$ind2]; ?>"><br>
					
						<?php 
						if($value == "Call Forwarding Always") {
                        ?>
                       <input type="hidden" name="cfaIsRingSplashActive" value="false">
						<input class="newCheckBoxImg" type="checkbox" id="cfaIsRingSplashActive" name="cfaIsRingSplashActive" value="true"
						<?php echo $_SESSION["userInfo"]["cfaIsRingSplashActive"] == "true" ? " checked" : ""; ?>>

						<label for="cfaIsRingSplashActive"><div class="checkRealBtn"></div></label>
						<label class="labelText"> Play Ring Reminder</label>
						<?php 
						}
						?>
						
						<?php 
						if($value == "Call Forwarding No Answer") {
                        ?>
                        <div class="">
                        <label class="labelText" style="padding-top:6px;">Number of Rings Before Forwarding</label>
                        <input style="width: 100px !important;float:right;" type="text" name="cfnNumberOfRings" id="cfnNumberOfRings" size="5"
                        value= <?php echo $_SESSION["userInfo"]["cfnNumberOfRings"]; ?>>
                        </div>
                        <?php 
						}
						?>
						</div>	 
 						</div>
						
	<?php 
			
	$rowCount++;
			}
		}
		
		?>

	<?php 
	if($rowCount %2 != 0 && $_SESSION["userInfo"]["dnd"] != ""){?>
	      <div class="col-md-6 minHeight marginZero callService4">
	    
						<div class="col-md-6">
						<div class="form-group">
							<input type="hidden" name="dnd" value="false" />
							<input class="newCheckBoxImg" type="checkbox" name="dnd" id="dnd" value="true"
							<?php echo $_SESSION["userInfo"]["dnd"] == "true" ? " checked" : ""; ?>>
							<label for="dnd"><span></span></label>
							<label class="labelText fontHead"><b>Do Not Disturb</b></label>
							</div>
						 </div>
						 
						<div class="col-md-6">
						<div class="form-group">
						 <div class="nORBF">
						  <div style="padding-top: 7px;">
							<input type="hidden" name="dndRingSplash"  value="false">
							<input class="newCheckBoxImg" type="checkbox" name="dndRingSplash" id="dndRingSplash" value="true"
							<?php echo $_SESSION["userInfo"]["dndRingSplash"] == "true" ? " checked" : ""; ?>>
							<label for="dndRingSplash" style="margin-left: 6px;"><div class="checkRealBtn"></div></label>
							<label class="labelText fontHead">Play Ring Reminder</label>
						</div>
						</div>
						</div>
						</div>
					
		</div> 
	<?php }?>	
	
	 <?php
		// Call Transfer Rajesh
		if ($_SESSION ["userInfo"] ["isRecallActive"] != "") {
		echo "<div class='minHeight callService5'>";
	?>	
						<div class="col-md-6 removeClass1">
						<div class="form-group">
						<input type="hidden" name="isRecallActive" value="false">
						<input class="newCheckBoxImg" style="width: 20px;" type="checkbox" name="isRecallActive"
						id="isRecallActiveTrue" value="true"
						<?php echo $_SESSION["userInfo"]["isRecallActive"] == "true" ? "checked " : ""; ?> />
						<label for="isRecallActiveTrue"><span></span> </label>
							<label class="labelText fontHead"><b>Call Transfer Recall</b><a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Below options will work only if Call Tranfer Recall is on"><img src="images/NewIcon/info_icon.png" /></a></label>
							 
 						
						 <div class="">
							<label class="labelText" for="" style="padding-top: 6px;">Number of Rings before Recall</label>							
							<input style="width: 100px !important;float:right;" type="text" name="recallNumberOfRings" id="recallNumberOfRings" 
							value="<?php echo $_SESSION ["userInfo"] ["recallNumberOfRings"]; ?>" />
						</div>
						
						
						<div class="" style="margin-top:40px;">
    						<input type="hidden" name="enableBusyCampOn" value="false" /> 
    						<input class="newCheckBoxImg" style="width: 20px;" type="checkbox" name="enableBusyCampOn" id="enableBusyCampOn" value="true"
    						<?php echo $_SESSION["userInfo"]["enableBusyCampOn"] == "true" ? " checked" : ""; ?>>
    						<label for="enableBusyCampOn" style="vertical-align: text-bottom;margin-left: 42px;"><span></span></label>
    						<label class="labelText"> Enable Busy<br>Camp-On (seconds) </label>
    						<input style="width:100px !important;float:right;" type="text" name="busyCampOnSeconds" id="busyCampOnSeconds" value="<?php echo $_SESSION ["userInfo"] ["busyCampOnSeconds"]; ?>"/>
						</div>
						
					
						<div class="nORBF" style="margin-top:40px;">
    						<input type="hidden" name="useDiversionInhibitorForBlindTransfer" value="false">
    						<input class="newCheckBoxImg" type="checkbox" name="useDiversionInhibitorForBlindTransfer"
    						id="useDiversionInhibitorForBlindTransferTrue" value="true"
    						<?php echo $_SESSION["userInfo"]["useDiversionInhibitorForBlindTransfer"] == "true" ? "checked " : ""; ?> />
    						<label for="useDiversionInhibitorForBlindTransferTrue" style="vertical-align: text-bottom;margin-left: 42px;"><span></span></label>
    						<label class="labelText"> Use Diversion Inhibitor<br>for Blind Transfer </label>
						</div>
							 
 						
				
						<div class="nORBF" style="margin-top:40px;">
						<input type="hidden"
						name="useDiversionInhibitorForConsultativeCalls" value="false">
						<input class="newCheckBoxImg" style="width: 20px;" type="checkbox"
						name="useDiversionInhibitorForConsultativeCalls"
						id="useDiversionInhibitorForConsultativeCallsTrue" value="true"
						<?php echo $_SESSION["userInfo"]["useDiversionInhibitorForConsultativeCalls"] == "true" ? "checked " : ""; ?> />
						<label for="useDiversionInhibitorForConsultativeCallsTrue" style="vertical-align: text-bottom;margin-left: 42px;"><span></span></label>
						<label class="labelText"> Use Diversion Inhibitor<br>for Consultative Calls </label>
							
					 	</div>
					 	</div>
 						</div>
	<?php 
	}
	?>
	<?php 
	if($_SESSION["userInfo"]["dnd"] != "" && $rowCount %2 == 0) {
	?>			
					
					<div class="col-md-6 marginZero callService4">
					<div class="form-group">
						<div class="">
							<input type="hidden" name="dnd" value="false" />
							<input class="newCheckBoxImg" type="checkbox" name="dnd" id="dnd" value="true"
							<?php echo $_SESSION["userInfo"]["dnd"] == "true" ? " checked" : ""; ?>>
							<label for="dnd"><span></span></label>
							<label class="labelText"><b>Do Not Disturb</b></label>
							
						 </div>
						 
						<div class="">
						 <div class="nORBF">
						  <div style="padding-top: 7px;">
							<input type="hidden" name="dndRingSplash"  value="false">
							<input class="newCheckBoxImg" type="checkbox" name="dndRingSplash" id="dndRingSplash" value="true"
							<?php echo $_SESSION["userInfo"]["dndRingSplash"] == "true" ? " checked" : ""; ?>>
							<label for="dndRingSplash" style="margin-left: 6px;"><div class="checkRealBtn"></div></label>
							<label class="labelText">Play Ring Reminder</label>
						</div>
						</div>
						</div>
					</div>
					</div>
	
	<?php } ?>
	
	<?php
		// Call Transfer Rajesh
		if ($_SESSION ["userInfo"] ["isRecallActive"] != "") {
		echo "</div>";
		}
	?>	
		
	
</div>	
</div>
</div>
<?php } ?>			
<!-- 	End call control -->


    <!-- Start Code for Sim Ring Div -->
    <?php 
        if(in_array("Simultaneous Ring Personal", $_SESSION["groupInfoData"]["userServiceAuth"]) and $_SESSION["userInfo"]["simRingService"]=="true" and $_SESSION ["permissions"] ["simRing"] == "1") 
        {
        

    ?>
            <script src="userMod/simRing/js/simRing.js"></script>
            <div id="simRing" class="userDataClass" style="display: none;">

<div class="col-md-12">
    <div class="from-group">
    	<div class=" "><h2 class="subBannerCustom">Simultaneous Ring Personal</h2></div>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group" style="text-align:center;">
    	<input type="button" name="addSimRingCriteriaBtn" class="subButton marginRightButton" id="addSimRingCriteriaBtn" value="Add Sim Ring Criteria">
    </div>
</div>

<!-- radio and checkbox simultaneousRing work star -->
 <div class="row">
 <div class="form-group">
   <input type="hidden" name="simultaneousRingIsActive" value="false" />
   <input type="checkbox" class="newCheckBoxImg" value='true' style="width:20px; float:right;margin: 10px 0;" name="simultaneousRingIsActive" id="simultaneousRingIsActive"  <?php if($_SESSION["userInfo"]["simultaneousRingIsActive"]=="true")  echo "checked"; ?> /> <label for="simultaneousRingIsActive"><span style="margin-left:-5px !important;"></span></label>
<label for="simultaneousRingIsActive"><span></span>Enable Service</label><br />
 <input type="hidden" name="doNotRingIfOnCall" value="false" />
 <input style="margin-left: 5px;" type="checkbox" class="newCheckBoxImg" name="doNotRingIfOnCall" id="doNotRingIfOnCall" value='true'  <?php if($_SESSION["userInfo"]["doNotRingIfOnCall"]=="true")  echo "checked"; ?> /> <label for="doNotRingIfOnCall"><div class="checkRealBtn"></div></label>
 <label for="doNotRingIfOnCall"><span></span>Do not ring if already on call</label><br />
 </div>
  </div>
<!-- end radio and checkbox simultaneousRing work star -->
 <!-- table first work start -->
 <div class="row">
 	<div style="width:49%;float:left">
 		<div class="form-group">
 			 
 				<table border="1" class="tableAlignMentDesign scroll tablesorter tagTableHeight newUItableAlign" id="">
 					<thead>
                	 	<tr> 
                	 		<th id="disableTh" style="width: 220px!important; padding:12px 17px">Phone Number/SIP-URL</th>
                	 		<th id="disableTh" style="text-align: center;padding-top: 12px;">Answer Confirmation</th> 
                	 	</tr> 
            		</thead>
            		
            		<!-- tbody -->
            		<tbody id="simRingFirstTableBody">
            			<tr class="tableTrAlignment" style="" id="">
                        	<td class="adminTableCol">
                                <input name="simultaneousRingPhoneNumber[0]" id="phoneNumber_0" type="text">
                            </td>
                    		
                    		<td class="deleteCheckboxtd">
                    			<input type="checkbox" class="deleteCheckbox" name="answerConfirmationRequired[0]" id="ansCnfmReq_0" value="true">
                    			<label for="ansCnfmReq_0"><span></span></label>
                        	</td>
                    	</tr>
                    	
                    	<!-- phone number2 -->
                    	<tr class="tableTrAlignment" style="" id="">
                        	<td class="adminTableCol">
                                <input name="simultaneousRingPhoneNumber[2]" id="phoneNumber_2" type="text">
                            </td>
                    		
                    		<td class="deleteCheckboxtd">
                    			<input type="checkbox" class="deleteCheckbox" name="answerConfirmationRequired[2]" id="ansCnfmReq_2">
                    			<label for="ansCnfmReq_2"><span></span></label>
                        	</td>
                    	</tr>
						
					<!-- end phone number2 -->
                    	
                    	
                    	
                    	<!-- phone number3 -->
						<tr class="tableTrAlignment" style="" id="">
                        	<td class="adminTableCol">
                                <input name="simultaneousRingPhoneNumber[4]" id="phoneNumber_4" type="text">
                            </td>
                    		
                    		<td class="deleteCheckboxtd">
                    			<input type="checkbox" class="deleteCheckbox" name="answerConfirmationRequired[4]" id="ansCnfmReq_4">
                    			<label for="ansCnfmReq_4"><span></span></label>
                        	</td>
                    	</tr>
					<!-- end phone number3 -->
                    	
                    	
                    	<!-- phone number4 -->
						<tr class="tableTrAlignment" style="" id="">
                        	<td class="adminTableCol">
                                <input name="simultaneousRingPhoneNumber[6]" id="phoneNumber_6" type="text">
                            </td>
                    		
                    		<td class="deleteCheckboxtd">
                    			<input type="checkbox" class="deleteCheckbox" name="answerConfirmationRequired[6]" id="ansCnfmReq_6">
                    			<label for="ansCnfmReq_6"><span></span></label>
                        	</td>
                    	</tr>
						<!-- end phone number4 -->
                    	
                    	<!-- phone number5 -->
                    	  <tr class="tableTrAlignment" style="" id="">
                        	<td class="adminTableCol">
                                <input name="simultaneousRingPhoneNumber[8]" id="phoneNumber_8" type="text">
                            </td>
                    		
                    		<td class="deleteCheckboxtd">
                    			<input type="checkbox" class="deleteCheckbox" name="answerConfirmationRequired[8]" id="ansCnfmReq_8">
                    			<label for="ansCnfmReq_8"><span></span></label>
                        	</td>
                    	</tr>
                    	<!-- end phone number5 -->
            		</tbody>
            		<!-- end tbody -->
 				</table>
 			 
 		</div>
 	</div>
 	
 	<div style="width:2%;float:left">&nbsp;</div>
 	
 	<div style="width:49%; float:left">
 		<div class="form-group">
 			 
 				<table border="1" class="tableAlignMentDesign scroll tablesorter tagTableHeight newUItableAlign" id="">
 					<thead>
                	 	<tr> 
                	 		<th id="disableTh" style="width: 220px!important;padding:12px 17px">Phone Number/SIP-URL</th>
                	 		<th id="disableTh" style="text-align: center;padding-top: 12px;">Answer Confirmation</th> 
                	 	</tr> 
            		</thead>
            		
            		<!-- tbody -->
            		<tbody id="simRingFirstTableBody">
            			<tr class="tableTrAlignment" style="" id="">
                        	<td class="adminTableCol">
                        	 <input name="simultaneousRingPhoneNumber[1]" id="phoneNumber_1" type="text">
                            </td>
                    		
                    		<td class="deleteCheckboxtd">
                    			<input type="checkbox" class="deleteCheckbox" name="answerConfirmationRequired[1]" id="ansCnfmReq_1" />
                    			<label for="ansCnfmReq_1"><span></span></label>
                        	</td>
                    	</tr>
                    	
                    	<!-- phone number2 -->
                    	<tr class="tableTrAlignment" style="" id="">
                        	<td class="adminTableCol">
                                <input name="simultaneousRingPhoneNumber[3]" id="phoneNumber_3" type="text">
                            </td>
                    		
                    		<td class="deleteCheckboxtd">
                    			<input type="checkbox" class="deleteCheckbox" name="answerConfirmationRequired[3]" id="ansCnfmReq_3" />
                    			<label for="ansCnfmReq_3"><span></span></label>
                        	</td>
                    	</tr>
						
					<!-- end phone number2 -->
                    	
                    	
                    	
                    	<!-- phone number3 -->
						<tr class="tableTrAlignment" style="" id="">
                        	<td class="adminTableCol">
                               <input name="simultaneousRingPhoneNumber[5]" id="phoneNumber_5" type="text">
                            </td>
                    		
                    		<td class="deleteCheckboxtd">
                    			<input type="checkbox" class="deleteCheckbox" name="answerConfirmationRequired[5]" id="ansCnfmReq_5" />
                    			<label for="ansCnfmReq_5"><span></span></label>
                        	</td>
                    	</tr>
					<!-- end phone number3 -->
                    	
                    	
                    	<!-- phone number4 -->
						<tr class="tableTrAlignment" style="" id="">
                        	<td class="adminTableCol">
                                <input name="simultaneousRingPhoneNumber[7]" id="phoneNumber_7" type="text">
                            </td>
                    		
                    		<td class="deleteCheckboxtd">
                    			<input type="checkbox" class="deleteCheckbox" name="answerConfirmationRequired[7]" id="ansCnfmReq_7" />
                    			<label for="ansCnfmReq_7"><span></span></label>
                        	</td>
                    	</tr>
						<!-- end phone number4 -->
                    	
                    	<!-- phone number5 -->
                    	  <tr class="tableTrAlignment" style="" id="">
                        	<td class="adminTableCol">
                                <input name="simultaneousRingPhoneNumber[9]" id="phoneNumber_9" type="text">
                            </td>
                    		
                    		<td class="deleteCheckboxtd">
                    			<input type="checkbox" class="deleteCheckbox" name="answerConfirmationRequired[9]" id="ansCnfmReq_9" />
                    			<label for="ansCnfmReq_9"><span></span></label>
                        	</td>
                    	</tr>
                    	<!-- end phone number5 -->
            		</tbody>
            		<!-- end tbody -->
 				</table>
 			 
 		</div>
 	</div>
 
 </div>
 <!-- end table first work -->

<div class="row simRingCriteriaTable">
<div class="col-md-12 usersTableButton" style="margin-bottom: 10px;margin-right: -20px !important;float: right;">
 <div id="simRingTableThhiredDiv">
 	<div name="downloadCSV" id="downloadCSV" value="" style="display: block;"><img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png"><br><span>Download<br>CSV</span></div>
  </div>
</div>
	<div style="width:100%;">
  		<table  border="1" id="simRingCriteriaList" class="tableAlignMentDesign scroll tablesorter tagTableHeight">
    		<thead>
            	 <tr>  
            	 	<th id="disableTh" class="">Active</th>
                        <th class="header thsmall" style="height: auto !important;">Criteria Name</th>
            	 	<th class="header thsmall" style="height: auto !important;">Ring Simultaneously</th>
                        <th class="header thsmall" style="height: auto !important;">Time Schedule</th>
                        <th class="header thsmall" style="height: auto !important;">Holiday Schedule</th>
            	 	<th class="header thsmall" style="height: auto !important;">Calls from</th>
            	 </tr> 

        	</thead>
                <input type='hidden' name='criteriaNameDup' value='' />
    		<tbody id="criteriaListTable">
                    	
            </tbody>
    	</table>
    	<div class="criteroiaListMessage" style="clear: both;">&nbsp;&nbsp;</div>
	</div>
</div>	              
         
    	
            </div>
    <?php 
        }
    ?>
    <!-- End Code for Sim Ring Div -->




						
						<?php
					//}
					if ($_SESSION["permissions"]["changeblf"] == "1")
					{
						?>
						<div id="blf_1" class="row userDataClass" style="display:none;"><!--begin blf_1 div-->
						<h2 class="subBannerCustom">Busy Lamp Fields</h2>
						
						<div style="font-size: 11px; width: 100%; text-align: center">&nbsp;</div>
						<div style="font-size: 11px; width: 100%; text-align: center">&nbsp;</div>
            			<div class="row" style="margin-bottom:15px;">
            				<div class="col-md-4">
            					<label class="labelpadding" for="restrictToGroup"><b>Restrict Search to Group</b></label>
            				</div>
            				<div class="col-md-8" style="text-align:left;">
            					<input type="checkbox" id="restrictToGroup" name="restrictToGroup" value="true" style="width:15px;height:15px;">
            					<label for="restrictToGroup" class="labelText labelTextMarginNeg"><span></span></label>
            				</div>
            				
            			</div>
            			<div class="row" style="margin-bottom:15px;">
            				<div class="col-md-4">
            					<label class="labelpadding" for="allUserSearch"><b>Search By Name or Phone Number</b></label>
            				</div>
            				<div class="col-md-6">
            					<input type="text" class="autoFill" id="allUserSearch" name="allUserSearch" style="width:100%;">
            				</div>
            				<div class="col-md-2" style="text-align:right;"><input type="button" name="allUserSearchGet" id="allUserSearchGet" value="Search" class="searchGet" style="width:100px;height:30px;"></div>
            			</div>
			
            			<!-- <div class="row">
                			<div class="col-md-12" style="height:auto;text-align:center;">
                				<input type="button" name="allUserSearchGet" id="allUserSearchGet" value="Get" class="searchGet">
                			</div>
            			</div> -->
			<div id="dataShow"></div>
			
			<div style="font-size: 11px; width: 100%; text-align: center; height:20px;">&nbsp;</div>
			<div id="errorDisplay" style="color:red;text-align:center;float:left;width:100%;height:30px;"></div>

						<div class="">
							<div class="col-md-6 availNCurrentGroup">
								<div class="form-group">Available Users <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Drag and drop Users from Left to Right; Reorder Monitored Users by holding Left mouse and Drag"><img src="images/NewIcon/info_icon.png" /></a></div>
							</div>

							<div class="col-md-6 availNCurrentGroup">
								<div class="form-group">Monitored Users <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Drag and drop Users from Left to Right; Reorder Monitored Users by holding Left mouse and Drag"><img src="images/NewIcon/info_icon.png" /></a></div>
							</div>
						</div>

						<div class="">
							<div class="col-md-6" style="padding-left:0;">
								<div class="form-group scrollableListCustom1">
									<ul id="sortable1" class="connectedSortable connectedSortableCustom">
											<?php
											/*for ($a = 0; $a < count($blfUsers); $a++)
											{
												//$id = $blfUsers[$a]["id"] . ":" . $blfUsers[$a]["name"];
												$userNumAndExt = getMonitoredUserNumberAndExtension($users, $blfUsers[$a]["id"]);

                                                $id = $blfUsers[$a]["id"] . ":" . $blfUsers[$a]["name"] . ":" .$userNumAndExt;
                                                echo "<li class=\"ui-state-default\" id=\"" . $id . "\">" . $blfUsers[$a]["name"] . "  (" . $userNumAndExt . ")" . "</li>";
												//echo "<li class=\"ui-state-default\" id=\"" . $id . "\">" . $blfUsers[$a]["name"] . "  (" . $userNumAndExt . ")" . "</li>";

											}*/
											?>
									</ul>
								</div>
							</div>
							<div class="col-md-6" style="padding-right:0;">
								<div class="form-group scrollableListCustom2">
										<ul id="sortable2" class="connectedSortable connectedSortableCustom">
											<?php
												if (isset($_SESSION["userInfo"]["monitoredUsers"]))
												{
													for ($a = 0; $a < count($_SESSION["userInfo"]["monitoredUsers"]); $a++)
													{
												if(isset($_SESSION["userInfo"]["monitoredUsers"][$a]["phoneNumber"])){
												        $blfPhone = $_SESSION["userInfo"]["monitoredUsers"][$a]["phoneNumber"] ;
												        $blfPhone .= ($blfPhone != "") ? "x" : "";
												    }else{
												        $blfPhone = "";
												    }
												    if(isset($_SESSION["userInfo"]["monitoredUsers"][$a]["extension"])){
												        $blfExt = $_SESSION["userInfo"]["monitoredUsers"][$a]["extension"];
												    }else{
												        $blfExt = "";
												    }
													$id = $_SESSION["userInfo"]["monitoredUsers"][$a]["id"] . ":" . $_SESSION["userInfo"]["monitoredUsers"][$a]["name"].":".$blfPhone.$blfExt;
													echo "<li class=\"ui-state-highlight\" id=\"" . $id . "\">" . $_SESSION["userInfo"]["monitoredUsers"][$a]["name"] .  "  (" . $blfPhone.$blfExt . ")" . "</li>";

													}
												}
											?>
											<input type="hidden" name="monitoredUsers" id="monitoredUsers" value="">
										</ul>
								</div>
							</div>
						</div>
						</div><!--end blf_1 div-->
						<?php
					}
				//if ($_SESSION ["permissions"] ["changeSCA"] == "1") {
				if(isset($_SESSION["userInfo"]["deviceType"]) && $_SESSION["userInfo"]["deviceType"] != "" && $deviceInfo['ports']>1){
				?>
					<div id="sca_1" class="row userDataClass" style="display:none;"><!--begin sca_1 div-->
							<h2 class="subBannerCustom">Shared Call Appearances</h2>
        

						<div class="">
                            <div class="col-md-6 availNCurrentGroup">
                                <div class="form-group">Available Users <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Drag Users that you want to appear on your phone from Left to Right; Reorder SCA Users by holding Left mouse and Drag"><img src="images/NewIcon/info_icon.png" /></a></div>
                            </div>
                            <div class="col-md-6 availNCurrentGroup">
                                <div class="form-group">Shared Call Appearance Users <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Drag Users that you want to appear on your phone from Left to Right; Reorder SCA Users by holding Left mouse and Drag"><img src="images/NewIcon/info_icon.png" /></a></div>
                            </div> 
                        </div>       

					    <div class="">
                            <div class="col-md-6" style="padding-left:0;">
                                <div class="form-group scrollableListCustom1">
    								<ul id="sortable_1" class="connectedSortable connectedSortableCustom">
										<?php

										for($a = 0; $a < count ( $users ); $a ++) {
										$match = false;
										for($b = 0; $b < count ( $_SESSION ["userInfo"] ["sharedCallAppearanceUsers"] ); $b ++) {
											if ($users [$a] ["id"] == $_SESSION ["userInfo"] ["sharedCallAppearanceUsers"] [$b] ["id"]) {
												$match = true;
											}
										}
										if ($match == false) {
											$userNumAndExt = $users [$a] ["extension"];
															$userExt       = $users [$a] ["extension"];
											if (isset ( $users [$a] ["phoneNumber"] ) && $users [$a] ["phoneNumber"] != "") {
												$userNum = explode ( "+1-", $users [$a] ["phoneNumber"] );
												$userNum = $userNum [1];
												$userNumAndExt = $userNum . "x" . $userNumAndExt;
											}
															if($users [$a] ["phoneNumber"]!="")
																$id = $users [$a] ["id"] . ":" . $users [$a] ["name"] . ":" . preg_replace ( "/^\+1-/", "", $users [$a] ["phoneNumber"] );
											else
																$id = $users [$a] ["id"] . ":" . $users [$a] ["name"] . ":" . $userExt;
															echo "<li class=\"ui-state-default\" id=\"" . $id . "\">" . $users [$a] ["name"] . "  (" . $userNumAndExt . ")" . "</li>";
										}
									}
									?>

									</ul>
								</div>
							</div>
							<div class="col-md-6" style="padding-right:0;">
                                <div class="form-group scrollableListCustom2">
									<ul id="sortable_2" class="connectedSortable connectedSortableCustom">
										<?php
											if (isset($_SESSION["userInfo"]["sharedCallAppearanceUsers"]))
											{
												for ($a = 0; $a < count($_SESSION["userInfo"]["sharedCallAppearanceUsers"]); $a++)
												{
													if ($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["endpointType"] == "Shared Call Appearance")
													{
													    if(isset($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["phoneNumber"])){
													        $sharedPhone = $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["phoneNumber"];
													    }else{
													        $sharedPhone = "";
													    }
													    //if(isset($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["extension"])){
                                                                                                            if(isset($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["extension"]) && $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["extension"]!="")
                                                                                                            {
                                                                                                                if($sharedPhone != ""){
                                                                                                                    $sharedExt = "x".$_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["extension"];
                                                                                                                }
                                                                                                                if($sharedPhone == ""){
                                                                                                                    $sharedExt = $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["extension"];
                                                                                                                }
                                                                                                                
													    }else{
                                                                                                                //$sharedExt = "";                                                                            
                                                                                                                for($x = 0; $x < count ( $users ); $x ++) 
                                                                                                                {
                                                                                                                    if ($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["id"]==$users[$x]["id"])
                                                                                                                    {
                                                                                                                         $sharedExt = $users[$x]["extension"];       
                                                                                                                    }
                                                                                                                }
													    }
                                                                                                            
                                                                                                                if(trim($sharedPhone)!="")
                                                                                                                {
                                                                                                                    
                                                                                                                    $id = $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["id"] . ":" . $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["name"] . ":" . $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["phoneNumber"];
                                                                                                                }
                                                                                                                else
                                                                                                                {
                                                                                                                    $id = $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["id"] . ":" . $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["name"] . ":" . $sharedExt;
                                                                                                                }
														
														echo "<li class=\"ui-state-highlight\" id=\"" . $id . "\">" . $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["name"] . "  (" . $sharedPhone.$sharedExt . ")" . "</li>";

													}
												}
											}
										?>
										<input type="hidden" name="scaUsers" id="scaUsers" value="">
									</ul>
								</div>
							</div>
                        </div>    
							
                        <div class="col-md-12" style="padding:0;">
                        <div class="autoHeight viewDetailNew">
                        <div style="zoom:1;">
							<table id='scaLinePortTable' border="1" class="scroll tablesorter scaUserstable" style="border-collapse:collapse;width:100%;">
								<thead>
								<tr>
									<th style="width:8%; background-image: none !important; text-align: center; pointer-events: none; cursor: default;">Primary Line/Port</th>
									<th style="width:14%;">Line/Port</th>
									<th style="width:8%;">Port</th>
									<th style="width:14%;">Endpoint Type</th>
									<th style="width:12%;">User ID</th>
									<th style="width:10%;">Name</th>
									<th style="width:11%;">Phone Number</th>
									<th style="width:12%;">Extension</th>
									<th style="width:12%;">Department</th>
								</tr>
                                </thead>
								<?php
                                    if (isset($_SESSION["userInfo"]["sharedCallAppearanceUsers"]))
									{
										$scaSorted = subval_sort($_SESSION["userInfo"]["sharedCallAppearanceUsers"], "port");

										for ($a = 0; $a < count($scaSorted); $a++)
										{
											echo "<tr>";
											//echo "<td><input type=\"radio\" class='checkLinePortRebuild' name=\"primary\" id=\"primary" . $scaSorted [$a] ["linePort"] . "\" value=\"" . $scaSorted [$a] ["linePort"] . "\"" . ($scaSorted [$a] ["primary"] == "true" ? " checked" : "") . "/></td>";
	                                           echo "<td style=\"width:8%;text-align: center;\"><input type=\"radio\" name=\"primary\"  class=\"checkLinePortRebuild\" id=\"primary" . $scaSorted[$a]["linePort"] . "\" value=\"" . $scaSorted[$a]["linePort"] . "\"" . ($scaSorted[$a]["primary"] == "true" ? " checked" : "") . "/><label for=\"primary" . $scaSorted[$a]["linePort"] . "\"><span style=\"margin: -4px 0 0 0;\"></span></label></td>";
											echo "<td style=\"width:14%;\">" . $scaSorted[$a]["linePort"] . "</td>";
											echo "<td style=\"width:8%;\">" . $scaSorted[$a]["port"] . "</td>";
											echo "<td style=\"width:14%;\">" . $scaSorted[$a]["endpointType"] . "</td>";
											echo "<td style=\"width:12%;\">" . $scaSorted[$a]["id"] . "</td>";
											echo "<td style=\"width:10%;\">" . $scaSorted[$a]["name"] . "</td>";
											echo "<td style=\"width:11%;\">" . $scaSorted[$a]["phoneNumber"] . "</td>";
											echo "<td style=\"width:12%;\">" . $scaSorted[$a]["extension"] . "</td>";
											echo "<td style=\"width:12%;\">" . $scaSorted[$a]["department"] . "</td>";
											echo "</tr>";
										}
									}
									else
									{
										echo "<tr><td align=\"center\" colspan=\"100%\">No shared call appearances</td></tr>";
									}
								?>
							</table>
							</div>
						</div>
					</div>
    <!-- dafdsjklsdf -->
				
	<div class="clr"></div>
             <?php if($rebuildResetDevice == "true"){?>
		<div style="text-align: left;" class="col span_12">
            <input type="checkbox"name="rebuildPhoneFiles_sca" id="rebuildPhoneFiles_sca" value="true"><label class="labelText" for ="rebuildPhoneFiles_sca"><span></span>&nbsp;&nbsp;&nbsp;Rebuild Phone Files</label>
        </div>
		<div class="clr"></div>
		
		<div style="text-align: left;" class="col span_12">
            <input type="checkbox" name="resetPhone_sca" id="resetPhone_sca" value="true"><label class="labelText" for ="resetPhone_sca"><span></span>&nbsp;&nbsp;&nbsp;Reset the Phone</label>
        </div>
						 
		<div class="clr"></div>
		<?php
            }else{
	       ?>
			<input type="hidden" name="rebuildPhoneFiles_sca" id="rebuildPhoneFiles_sca" value="true" /> 
			<input type="hidden"name="resetPhone_sca" id="resetPhone_sca" value="true" />
			<?php }?>	                    
 <div class="clr"></div>
<!--  -->			
		</div>
		<?php }?>
		<!--end sca_1 div-->

			<?php		
            if ($cpgFieldVisible == "true" && $_SESSION ["groupServicesAuthorizationTable"] == "1" && $_SESSION ["permissions"] ["callPickupGroup"] == "1") {
					?>
					<div id="callPickupGroup_1" class="userDataClass" style="display:none;"><!--begin callPickupGroup_1 div-->
						<h2 class="subBannerCustom">Call Pickup Group</h2>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
								<label class="labelText">Call Pickup Group</label>
								<div class="dropdown-wrap">
									<select name="cpgGroupName" id="groupName" style="">
									<option value="">None</option>
										<?php
											//echo "<pre>"; print_r($groupCallPickupList[0]); die;
											if (isset($groupCallPickupList[0]))
											{
												foreach ($groupCallPickupList[0] as $key => $value)
												{
													echo "<option value=\"" . $value . "\"";
													if($value == $_SESSION["userInfo"]["usersGroupName"]){
														echo " selected";
													}
													echo ">" . $value . "</option>";

												}
											}

										?>
								</select>
								</div>
								</div>
								</div>
							
							</div>
						</div> <!--end callPickupGroup div-->
					<?php } ?>

		<?php
		if ($_SESSION ["permissions"] ["basicUser"] == "1") {
		?>
<!--begin policies_1 div-->
		<!-- div id="policies_1" class="userDataClass" style="display: none">
							<div class="subBanner">Policies</div>
							<div style="clear:right;">&nbsp;</div>
							<div class="row formspace fcorn-register container">
								<div style="padding:10px;text-align:center;" class="col span_8">Calling Line ID Policy</div>
								<div style="padding:10px;text-align:center;text-align:left !important;" class="col span_16">
									<input style="width:20px;" type="radio" name="useUserCLIDSetting" id="useUserCLIDSettingUser" value="true" <?php //echo $_SESSION["userInfo"]["useUserCLIDSetting"] == "true" ? "checked " : ""; ?>/> Use User Policy
									<input style="width:20px;" type="radio" name="useUserCLIDSetting" id="useUserCLIDSettingGroup" value="false" <?php //echo $_SESSION["userInfo"]["useUserCLIDSetting"] == "false" ? "checked " : ""; ?>/> Use Group Policy<br>
									<div style="clear:right;">&nbsp;</div>
									<label style="margin-left: 20px"><u>User Policy</u></label><br>
									<input style="width:20px;" type="radio" name="clidPolicy" id="clidPolicyDN" value="Use DN" <?php //echo $_SESSION["userInfo"]["clidPolicy"] == "Use DN" ? "checked " : ""; ?>/>Use user phone number for Calling Line Identity<br>
									<input style="width:20px;" type="radio" name="clidPolicy" id="clidPolicyConfig" value="Use Configurable CLID" <?php //echo $_SESSION["userInfo"]["clidPolicy"] == "Use Configurable CLID" ? "checked " : ""; ?>/>Use configurable CLID for Calling Line Identity<br>
									<input style="width:20px;" type="radio" name="clidPolicy" id="clidPolicyGroup" value="Use Group CLID" <?php //echo $_SESSION["userInfo"]["clidPolicy"] == "Use Group CLID" ? "checked " : ""; ?>/>Use group phone number for Calling Line Identity<br>
								</div>
							</div>
						</div-->

<!--New code found Start-->	
		<div id="policies_1" class="userDataClass" style="display: none">

			<h2 class="subBannerCustom">Policies</h2>
			<!-- Calling Line ID Policy -->
			<?php
			    $userPolicyCLIDShow = isset($_SESSION["permissions"]["userPolicyCLID"]) && 
			    $_SESSION["permissions"]["userPolicyCLID"] == "1" ? "block" : "none";
			?>
			<div class="" style = "display: <?php echo $userPolicyCLIDShow; ?>">
				<h2 class="subBannerCustom">Calling Line ID</h2>
				<div class="row">
					<div style="" class="col-md-6">
						<div class="form-group">
							<input type="radio" id="userCLIDPolicy" name="useUserCLIDSetting" <?php if($_SESSION["userInfo"]["useUserCLIDSetting"] == "true"){?>checked<?php }?> value="true">
							<label for="userCLIDPolicy"> <span></span></label>
							<label class="labelText"> Use User Calling Line Id Policy </label>
						</div>
					</div>
					<div style="" class="col-md-6">
						<div class="form-group">
							<input type="radio" id="useGLIDPolicy" name="useUserCLIDSetting" value="false" 
							<?php if($_SESSION["userInfo"]["useUserCLIDSetting"] == "false"){?>checked<?php }?>> 
							<label for="useGLIDPolicy"> <span></span></label>
							<label class="labelText textDisable"> Use Group Calling Line Id Policy</label>
						</div>
					</div>
				</div>		
				<div class="clr"></div>
				<div class="row">
					<div style="" class="col-md-6">
					<div class="form-group">
					<label class="labelText fontHead">Non-Emergency Calls : </label>
					</div>
					</div>
					<div style="" class="col-md-6">
					<div class="form-group">
						<input name="clidPolicy" id="useUPNFCLID" type="radio" checked value="Use DN" <?php if($_SESSION["userInfo"]["clidPolicy"] == "Use DN"){?>checked<?php }?>>
						<label for="useUPNFCLID"> <span></span></label>
						<label class="labelText"> Use user phone number for Calling Line Identity </label>
						<div class="clr"></div>
						<input name="clidPolicy" id="useCCFC" type="radio" value="Use Configurable CLID" <?php if($_SESSION["userInfo"]["clidPolicy"] == "Use Configurable CLID"){?>checked<?php }?>>
						<label for="useCCFC"> <span></span></label>
						<label class="labelText"> Use configurable CLID for Calling Line Identity</label>
						<div class="clr"></div>
						<input id="nonEmDisabled" name="clidPolicy" type="radio" value="Use Group CLID" <?php if($_SESSION["userInfo"]["clidPolicy"] == "Use Group CLID"){?>checked<?php }?>>
						<label for="nonEmDisabled"> <span style="margin-bottom: 15px;"></span></label>
						<label class="labelText"> Use group/department phone number for Calling <br>Line Identity </label>
					</div>
					</div>
				</div>
				<div class="clr"></div>
<!-- Emergency Calls Policy permission -->
				<?php
				    $userPolicyEmergencyCallsShow = isset($_SESSION["permissions"]["userPolicyEmergencyCalls"]) && 
				    $_SESSION["permissions"]["userPolicyEmergencyCalls"] == "1" ? "block" : "none";
				?>
				<div class="row" style = "display: <?php echo $userPolicyEmergencyCallsShow; ?>">
					<div style="" class="col-md-6">
    					<div class="form-group">
    						<label class="labelText fontHead"> Emergency Calls :</label>
    					</div>
					</div>
					<div style="" class="col-md-6">
						<input name="emergencyClidPolicy" id="useUPNFCLIdentity" type="radio" checked
							value="Use DN" <?php if($_SESSION["userInfo"]["emergencyClidPolicy"] == "Use DN"){?>checked<?php }?>>
							<label for="useUPNFCLIdentity"> <span></span></label>
							<label class="labelText"> Use user phone number for Calling Line Identity </label>

						<div class="clr"></div>
						<input id="useCCFCLineId" name="emergencyClidPolicy" type="radio"
							value="Use Configurable CLID" <?php if($_SESSION["userInfo"]["emergencyClidPolicy"] == "Use Configurable CLID"){?>checked<?php }?>>
							<label for="useCCFCLineId"> <span></span></label>
							<label class="labelText"> Use configurable CLID for Calling Line Identity </label>
						<div class="clr"></div>
						<input id="emDisabled" name="emergencyClidPolicy" type="radio"
							value="Use Group CLID" <?php if($_SESSION["userInfo"]["emergencyClidPolicy"] == "Use Group CLID"){?>checked<?php }?>>
						<label for="emDisabled"> <span style="margin-bottom: 15px;"></span></label>
						<label class="labelText"> Use group/department phone number for Calling <br> Line Identity </label>
					</div>
				</div>
				<div class="row">
						<div style="" class="col-md-6"></div>
						<div style="padding-left: 20px;" class="col-md-6">
							<input type="checkbox" name="useGroupName" id="useGroupName"
								size="30" maxlength="30" value="true" <?php if($_SESSION["userInfo"]["useGroupName"] == "true"){?>checked<?php }?> >
							<label for="useGroupName" class="widthForLabelCheckbox"> <span></span></label>
							<label class="labelText"> Use group name for Calling Line Identity </label>
							<div class="clr"></div>
							
							<input type="checkbox" name="allowDepartmentCLIDNameOverride"
								id="allowDepartmentCLIDNameOverride" size="30" maxlength="30"
								value="true" style="margin-left:21px;"
								<?php if($_SESSION["userInfo"]["useGroupName"] == "false"){?>disabled<?php }?>
								<?php if($_SESSION["userInfo"]["allowDepartmentCLIDNameOverride"] == "true"){?>checked<?php }?> > 
								<label for="allowDepartmentCLIDNameOverride" class=""><span style="margin-left: 34px;"></span></label>
								<span class="textDisable"> <label class="labelText" style="padding-left:10px;"> Allow Department Name Override </label></span>
							<div class="clr"></div>
							
						</div>
					</div>
				<div class="row">
					<div style="" class="col-md-6"></div>
					<div style="padding-left: 20px;" class="col-md-6">
						<input name="allowAlternateNumbersForRedirectingIdentity"
							id="allowAlternateNumbersForRedirectingIdentity" type="checkbox"
							<?php if($_SESSION["userInfo"]["allowAlternateNumbersForRedirectingIdentity"] == "true"){?>checked<?php }?> value="true">
							<label for="allowAlternateNumbersForRedirectingIdentity" class="widthForLabelCheckbox"> <span></span></label>
							<label class="labelText"> Allow Alternate Numbers for Redirecting Identity </label>
					</div>
				</div>
				<div class="row">
					<div style="" class="col-md-6"></div>
					<div style="padding-left: 20px;" class="col-md-6">
						<input name="allowConfigurableCLIDForRedirectingIdentity"
							id="allowConfigurableCLIDForRedirectingIdentity" type="checkbox"
							<?php if($_SESSION["userInfo"]["allowConfigurableCLIDForRedirectingIdentity"] == "true"){?>checked<?php }?> value="true">
							<label for="allowConfigurableCLIDForRedirectingIdentity" class="widthForLabelCheckbox"> <span></span></label>
							<label class="labelText"> Allow Configurable CLID for Redirecting Identity </label>
					</div>
				</div>
				<div class="row">
					<div style="" class="col-md-6"></div>
					<div style="padding-left: 20px;" class="col-md-6">
						<input name="blockCallingNameForExternalCalls"
							id="blockCallingNameForExternalCalls" type="checkbox"
							value="true" <?php if($_SESSION["userInfo"]["blockCallingNameForExternalCalls"] == "true"){?>checked<?php }?> >
							<label for="blockCallingNameForExternalCalls" class="widthForLabelCheckbox"><span></span></label>
							<label class="labelText">Block Calling Name for External Calls</label>
					</div>

				</div>
				<div class="clr"></div>
				<div class="row">
					<div style="" class="col-md-6"></div>
					<div style="padding-left: 52px;" class="col-md-6">
						<label class="labelText">Calling Line ID Group Number<span id="clidgnu"><?php echo $callingLineIdDisplayPhoneNumber; ?></span> </label>
						<div class="clr"></div>
						<label class="labelText">Calling Line ID Group Name<span id="clidgna"><?php echo $callingLineIdName; ?></span> </label>
					</div>
				</div>
				</div>
				<div class="clr"></div>
<!-- Call Limits Policy -->
				<?php
				    $userPolicyCallLimitsShow = isset($_SESSION["permissions"]["userPolicyCallLimits"]) && 
				    $_SESSION["permissions"]["userPolicyCallLimits"] == "1" ? "block" : "none";
				?>
				<div style = "display: <?php echo $userPolicyCallLimitsShow; ?>">
				<h2 class="subBannerCustom">Call Limits</h2>				
				<div style="clear: right;">&nbsp;</div>
				
				<div class="row">
					<div class="col-md-6">
					<div class="form-group">
						<input id="useUCLPolicy" name="useUserCallLimitsSetting" type="radio" value="true" <?php if($_SESSION["userInfo"]["useUserCallLimitsSetting"] == "true"){?>checked<?php }?>>
						<label for="useUCLPolicy"> <span></span></label>
						<label class="labelText">Use User Call Limits Policy &nbsp;&nbsp;&nbsp;&nbsp;</label>
					</div>
					</div>
					<div class="col-md-6">
    					<div class="form-group">
    						<input id="useGCLPolicy" name="useUserCallLimitsSetting"
							 type="radio" value="false" <?php if($_SESSION["userInfo"]["useUserCallLimitsSetting"] == "false"){?>checked<?php }?>>
							<label for="useGCLPolicy"> <span></span></label>
							<label class="labelText">Use Group Call Limits Policy</label>	
    					</div>
    				</div>	
				</div>

				<div class="row">
					<div class="col-md-6">
    					<div class="form-group">
    						<input name="useMaxSimultaneousCalls" id="useMaxSimultaneousCalls"
    							type="checkbox" value="true" <?php if($_SESSION["userInfo"]["useMaxSimultaneousCalls"] == "true"){?>checked<?php }?>>
    							<label for="useMaxSimultaneousCalls" class="widthForLabelCheckbox"> <span></span></label>
    							<label class="labelText">Enable Maximum Number of Concurrent Calls&nbsp;&nbsp;</label>
    							<input name="maxSimultaneousCalls"
    							id="maxSimultaneousCalls" type="text" size="3" maxlength="3"
    							value="<?php echo $_SESSION["userInfo"]["maxSimultaneousCalls"];?>">
    					</div>
    				</div>
    				<div class="col-md-6">
    					<div class="form-group">
    						<input name="useMaxSimultaneousVideoCalls"
    							id="useMaxSimultaneousVideoCalls" type="checkbox" value="true" <?php if($_SESSION["userInfo"]["useMaxSimultaneousVideoCalls"] == "true"){?>checked<?php }?>>
    						<label for="useMaxSimultaneousVideoCalls" class="widthForLabelCheckbox"> <span></span></label>
    						<label class="labelText">Enable Maximum Number of Concurrent Video Calls &nbsp;&nbsp;</label>
    						<input name="maxSimultaneousVideoCalls" id="maxSimultaneousVideoCalls"
							type="text" size="3" maxlength="3" value="<?php echo $_SESSION["userInfo"]["maxSimultaneousVideoCalls"];?>" >
    					</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
    					<div class="form-group">
    						<input name="useMaxCallTimeForAnsweredCalls"
    							id="useMaxCallTimeForAnsweredCalls" type="checkbox" value="true" <?php if($_SESSION["userInfo"]["useMaxCallTimeForAnsweredCalls"] == "true"){?>checked<?php }?>>
    						<label for="useMaxCallTimeForAnsweredCalls" class="widthForLabelCheckbox"> <span></span></label>
    						<label class="labelText"> Enable Maximum Duration for Answered Calls &nbsp;&nbsp; </label>
    						<input
    							name="maxCallTimeForAnsweredCallsMinutes"
    							id="maxCallTimeForAnsweredCallsMinutes" type="text" size="4"
    							maxlength="4" value="<?php echo $_SESSION["userInfo"]["maxCallTimeForAnsweredCallsMinutes"];?>">
    					</div>
    				</div>
    				<div class="col-md-6">	
    					<div class="form-group">
    						<input name="useMaxCallTimeForUnansweredCalls"
    							id="useMaxCallTimeForUnansweredCalls" type="checkbox"
    							value="true" <?php if($_SESSION["userInfo"]["useMaxCallTimeForUnansweredCalls"] == "true"){?>checked<?php }?>> 
    							<label for="useMaxCallTimeForUnansweredCalls" class="widthForLabelCheckbox"> <span></span></label>
    							<label class="labelText"> Enable Maximum Duration for Unanswered Calls &nbsp;&nbsp; </label>
    							<input name="maxCallTimeForUnansweredCallsMinutes"
    							id="maxCallTimeForUnansweredCallsMinutes" type="text" size="2"
    							maxlength="2" value="<?php echo $_SESSION["userInfo"]["maxCallTimeForUnansweredCallsMinutes"];?>">
    					</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
    					<div class="form-group">
    							<input name="useMaxConcurrentRedirectedCalls"
    							id="useMaxConcurrentRedirectedCalls" type="checkbox" value="true" <?php if($_SESSION["userInfo"]["useMaxConcurrentRedirectedCalls"] == "true"){?>checked<?php }?>>
    						<label for="useMaxConcurrentRedirectedCalls" class="widthForLabelCheckbox"> <span></span></label>
    						<label class="labelText">Enable Maximum Number of Concurrent Redirected Calls &nbsp;&nbsp;</label>
    						<input
    							name="maxConcurrentRedirectedCalls"
    							id="maxConcurrentRedirectedCalls" type="text" size="3"
    							maxlength="3" value="<?php echo $_SESSION["userInfo"]["maxConcurrentRedirectedCalls"];?>">
    					</div>
    				</div>
    				
    				<div class="col-md-6">
    					<div class="form-group">
    						<label class="labelText">Maximum Redirection Depth:&nbsp;&nbsp;</label>
    						<input name="maxRedirectionDepth" id="maxRedirectionDepth" type="text"
    							size="3" maxlength="3" value="<?php echo $_SESSION["userInfo"]["maxRedirectionDepth"];?>">
    					</div>
					</div>
    				
    				
				</div>
				
				<div class="row">
					<div class="col-md-6">
    					<div class="form-group">
    							 <input name="useMaxFindMeFollowMeDepth"
    							id="useMaxFindMeFollowMeDepth" type="checkbox" value="true" <?php if($_SESSION["userInfo"]["useMaxFindMeFollowMeDepth"] == "true"){?>checked<?php }?>>
    						<label for="useMaxFindMeFollowMeDepth" class="widthForLabelCheckbox"> <span></span></label>
    						<label class="labelText">Enable Maximum Find Me/Follow Me Depth &nbsp;&nbsp;</label>
							
    						<input
    							name="maxFindMeFollowMeDepth" id="maxFindMeFollowMeDepth"
    							type="text" size="3" maxlength="3" value="<?php echo $_SESSION["userInfo"]["maxFindMeFollowMeDepth"];?>">
    					</div>
    				</div>
    				
    				<div class="col-md-6">	
    					<div class="form-group">
    						<input name="useMaxConcurrentFindMeFollowMeInvocations"
    							id="useMaxConcurrentFindMeFollowMeInvocations" type="checkbox"
    							value="true" <?php if($_SESSION["userInfo"]["useMaxConcurrentFindMeFollowMeInvocations"] == "true"){?>checked<?php }?>>
    							<label for="useMaxConcurrentFindMeFollowMeInvocations" style="vertical-align: text-bottom;" class="widthForLabelCheckbox"> <span></span></label>
    							<label class="labelText">Enable Maximum Number of Concurrent Find Me/Follow <br> Me Invocations &nbsp;&nbsp;</label><input
    							name="maxConcurrentFindMeFollowMeInvocations"
    							id="maxConcurrentFindMeFollowMeInvocations" type="text" size="3"
    							maxlength="3" value="<?php echo $_SESSION["userInfo"]["maxConcurrentFindMeFollowMeInvocations"];?>">
    					</div>
					</div>
    				
    				
				</div>
			</div>
				
				<?php
				    $userPolicyIncomingCLIDShow = isset($_SESSION["permissions"]["userPolicyIncomingCLID"]) && 
				    $_SESSION["permissions"]["userPolicyIncomingCLID"] == "1" ? "block" : "none";
				?>
				<div style = "display: <?php echo $userPolicyIncomingCLIDShow; ?>">
				<h2 class="subBannerCustom">Incoming Caller ID</h2>
				<div class="row">
					<div style="" class="col-md-3">
						<input id="useUDCIdPolicy" name="useUserDCLIDSetting" type="radio" value="true" <?php if($_SESSION["userInfo"]["useUserDCLIDSetting"] == "true"){?>checked<?php }?>>
						<label for="useUDCIdPolicy" style="vertical-align: top;"><span></span></label>
						<label class="labelText">Use User Dialable<br/> Caller ID Policy</label>
					</div>
					<div style="" class="col-md-3">
						<input name="useUserDCLIDSetting" id="useGroupDialableCallerIDPolicy" type="radio"
							value="false" <?php if($_SESSION["userInfo"]["useUserDCLIDSetting"] == "false"){?>checked<?php }?>><label for="useGroupDialableCallerIDPolicy" style="vertical-align: top;"><span></span></label>
							<label class="labelText">Use Group Dialable<br/> Caller ID Policy</label>
					</div>
					<div style="" class="col-md-6"></div>
					<div style="" class="col-md-6">
						<label class="labelText">Dialable Caller ID</label> 
						<input id="enableDialableCallerIDOn" class="labelButtonMargin" name="enableDialableCallerID"
							type="radio" value="true" <?php if($_SESSION["userInfo"]["enableDialableCallerID"] == "true"){?>checked<?php }?>> 
							<label for="enableDialableCallerIDOn" class="labelButtonMargin"> <span></span></label>
							<label class="labelText">On</label>
							<input id="enableDialableCallerIDOff" name="enableDialableCallerID" type="radio" <?php if($_SESSION["userInfo"]["enableDialableCallerID"] == "false"){?>checked<?php }?>
							value="false">
							<label for="enableDialableCallerIDOff" class="labelButtonMargin"><span></span></label>
							<label class="labelText">Off</label>							
							
					</div>
				</div>
				
			<div class="clr"></div>
			<div class="clr"></div>
			<div class="clr"></div>
				
				</div>				
		</div>
		<!--end policies_1 div-->
		
		
		<!-- ocp ocp_1 start div -->
		<?php 
		$objOCP = new OCPOperations();
		$ocpData = $objOCP->userCallingPlanGetOriginatingRequest($userId);
		//print_r($ocpData);
		?>
	 
			<div id="ocp_1" class="userDataClass" style="display: none;">
			<h2 class="subBannerCustom">Outgoing Calling Plan</h2>
            	<div class="row">
            	
            		<div class="col-md-12">
            			<input type="hidden" name="useCustomSettings" value="false">
            			<input type="checkbox" name="useCustomSettings" id="useCustomOcp" value="true" <?php if($ocpData["Success"]["useCustomSettings"]== "true"){echo "checked"; }else{ echo ""; }?> style="width:5%;"><label for="useCustomOcp" class="widthForLabelCheckbox"> <span></span></label>Use Custom Settings
            		</div>
            	
            	<div id="useGroupDefaultSetting">
            		 
                        <div class="col-md-12">
                             <div class="form-group">
                                    <label class="labelText">Call Type:</label><br/>
                                             
                              <div class="dropdown-wrap">
                                   <select>
                                		<option>Originating Calls</option>
                                   </select>
                              </div>
                           </div>
 						</div>
            			 
            	<!-- heading calling line id plan  -->		
            		 <div class="col-md-12">
            		 	<div class="form-group">
            		 		<h2 class="subBannerCustom">Calling Plan Name/Permissions</h2>
            		 	</div>
            		 </div>
            	<!-- end heading    -->	 
            		
            		<!-- group div start --> 
            		 <div class="col-md-6 smIconImg">
            		 	<div class="form-group">
            		 	<label class="labelText">Group</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Calls within the business group"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="userPermissionsGroup" id="">
                					<?php 
                					foreach ($ocpArray as $keyocp => $valueocp){
                					    if($valueocp == $ocpData["Success"]["userPermissions"]["group"]){
                					        $varSel = "selected";
                					    }else{
                					        $varSel = "";
                					    }
                					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                					}
                					?>
            					</select>
            				</div>
            		 	</div>
						<!-- local div start -->
						<div class="form-group">
            		 	<label class="labelText">Local</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Calls within the local calling area"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
                				<select class="selectClass" name="userPermissionsLocal" id="">
                					<?php 
                					foreach ($ocpArray as $keyocp => $valueocp){
                					    if($valueocp == $ocpData["Success"]["userPermissions"]["local"]){
                					        $varSel = "selected";
                					    }else{
                					        $varSel = "";
                					    }
                					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                					}
                					?>
                				</select>
            				</div>
            		 	</div>
						 <!-- toll free div start -->
						<div class="form-group">
            		 	<label class="labelText">Toll Free</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Calls made to toll free numbers"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="userPermissionsTollfree" id="">
                					<?php 
                					
                					foreach ($ocpArray as $keyocp => $valueocp){
                					    if($valueocp == $ocpData["Success"]["userPermissions"]["tollFree"]){
                					        $varSel = "selected";
                					    }else{
                					        $varSel = "";
                					    }
                					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                					}
                					?>
            					</select>
            				</div>
            		 	</div>
						<!-- toll div start -->
						<div class="form-group">
						<label class="labelText">Toll</label>
						<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Local toll calls"><img src="images/NewIcon/info_icon.png"></a>
								<div class="dropdown-wrap">
            					<select class="selectClass" name="userPermissionsToll" id="">
                					<?php 
                					foreach ($ocpArray as $keyocp => $valueocp){
                					    if($valueocp == $ocpData["Success"]["userPermissions"]["toll"]){
                					        $varSel = "selected";
                					    }else{
                					        $varSel = "";
                					    }
                					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                					}
                					?>
            					</select>
            				</div>
            		 	</div>
						<!-- international div start -->
						<div class="form-group">
            		 	<label class="labelText">International</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="International calls"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="form-group dropdown-wrap">
            					<select class="selectClass" name="userPermissionsInternational" id="">
                					<?php 
                					foreach ($ocpArray as $keyocp => $valueocp){
                					    if($valueocp == $ocpData["Success"]["userPermissions"]["international"]){
                					        $varSel = "selected";
                					    }else{
                					        $varSel = "";
                					    }
                					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                					}
                					?>
            					</select>
            				</div>
            		 	</div>
						<!-- Operator assist div start -->
						<div class="form-group">
            		 	<label class="labelText">Operator Assisted</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Calls made with the chargeable assistance of an operator"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="userPermissionsOperatorAssisted" id="">
                					<?php 
                					foreach ($ocpArray as $keyocp => $valueocp){
                					    if($valueocp == $ocpData["Success"]["userPermissions"]["operatorAssisted"]){
                					        $varSel = "selected";
                					    }else{
                					        $varSel = "";
                					    }
                					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                					}
                					?>
            					</select>
            				</div>
            		 	</div>
					<!-- Changable dir assistance assist div start -->
						<div class="form-group">
            		 	<label class="labelText">Chargeable Dir. Assistance</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Directory assistance calls"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="userPermissionsOperatorChargDirAssisted" id="">
                					<?php 
                					foreach ($ocpArray as $keyocp => $valueocp){
                					    if($valueocp == $ocpData["Success"]["userPermissions"]["chargeableDirectoryAssisted"]){
                					        $varSel = "selected";
                					    }else{
                					        $varSel = "";
                					    }
                					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                					}
                					?>
            					</select>
            				</div>
            		 	</div>
            		 </div>
					 
            		  <!-- Casual div start -->
            		 <div class="col-md-6 smIconImg">
            		 	<div class="form-group">
            		 	<label class="labelText">Casual</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="1010XXX chargeable calls. Example: 1010321"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="userPermissionsCasual" id="">
                					<?php
                					foreach ($ocpArray as $keyocp => $valueocp){
                					    if($valueocp == $ocpData["Success"]["userPermissions"]["casual"]){
                					        $varSel = "selected";
                					    }else{
                					        $varSel = "";
                					    }
                					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                					}
                					?>
            					</select>
            				</div>
            		 	</div>
						
            		
            		 
            		 <!-- Special Services I div start -->
            		 
            		 	<div class="form-group">
            		 	<label class="labelText">Special Services I</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Special Services I (700 Number) calls"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="userPermissionsSpecialService1" id="">
                					<?php 
                					foreach ($ocpArray as $keyocp => $valueocp){
                					    if($valueocp == $ocpData["Success"]["userPermissions"]["specialServicesI"]){
                					        $varSel = "selected";
                					    }else{
                					        $varSel = "";
                					    }
                					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                					}
                					?>
            					</select>
            				</div>
            		 	</div>
            		
            		 
            		 <!-- Special Services II div start -->
            		
            		 	<div class="form-group">
            		 		<label class="labelText">Special Services II</label>
            		 		<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Special Services II"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="userPermissionsSpecialService2" id="">
                					<?php 
                					foreach ($ocpArray as $keyocp => $valueocp){
                					    if($valueocp == $ocpData["Success"]["userPermissions"]["specialServicesII"]){
                					        $varSel = "selected";
                					    }else{
                					        $varSel = "";
                					    }
                					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                					}
                					?>
            					</select>
            				</div>
            		 	</div>
            		
            		 
            		 <!-- Premium  Services I div start -->
            		
            		 	<div class="form-group">
            		 		<label class="labelText">Premium Services I</label>
            		 		<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Premium Services I (900 Number) calls"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="form-group dropdown-wrap">
            					<select class="selectClass" name="userPermissionsPremiumServices1" id="">
                					<?php 
                					foreach ($ocpArray as $keyocp => $valueocp){
                					    if($valueocp == $ocpData["Success"]["userPermissions"]["premiumServicesI"]){
                					        $varSel = "selected";
                					    }else{
                					        $varSel = "";
                					    }
                					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                					}
                					?>
            					</select>
            				</div>
            		 	</div>
            		
            		 
            		 <!-- Premium  Services II div start -->
            		
            		 	<div class="form-group">
            		 		<label class="labelText">Premium Services II</label>
            		 		<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Premium Services II (976 Number) calls"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="form-group dropdown-wrap">
            					<select class="selectClass" name="userPermissionsPremiumServices2" id="">
                    				<?php
                    					foreach ($ocpArray as $keyocp => $valueocp){
                    					    if($valueocp == $ocpData["Success"]["userPermissions"]["premiumServicesII"]){
                    					        $varSel = "selected";
                    					    }else{
                    					        $varSel = "";
                    					    }
                    					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                    					}
                    					?>
            					</select>
            				</div>
            		 	</div>
            		
            		 
            		 <!-- URL Dailing div start -->
            		
            		 	<div class="form-group">
            		 		<label class="labelText">URL Dialing</label>
            		 		<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Calls from internet"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="userPermissionsUrlDialing" id="">
                					<?php
                					foreach ($ocpArray as $keyocp => $valueocp){
                					    if($valueocp == $ocpData["Success"]["userPermissions"]["urlDialing"]){
                					        $varSel = "selected";
                					    }else{
                					        $varSel = "";
                					    }
                					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                					}
                					?>
            					</select>
            				</div>
            		 	</div>
            		
            		 
            		  <!-- unknown div start -->
            		
            		 	<div class="form-group">
            		 	<label class="labelText">Unknown</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Unknown call type"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="userPermissionsUnknown" id="">
                					<?php
                					foreach ($ocpArray as $keyocp => $valueocp){
                					    if($valueocp == $ocpData["Success"]["userPermissions"]["unknown"]){
                					        $varSel = "selected";
                					    }else{
                					        $varSel = "";
                					    }
                					    echo "<option value='".$valueocp."' " .$varSel. ">".$valueocp."</option>";
                					}
                					?>
            					</select>
            				</div>
						</div>
				</div>
            		 
            	</div><!-- useGroupDefaultSetting -->
            </div><!-- end row -->
         </div>   <!-- end main div --> 
		 
<!-- ocp ocp_1 end div -->
						
						
						<?php
	}
	?>

<!--New code found End-->
		</div>
			<!-- <div class="row">-->
			<div class="row">
				<div class="form-group">
					<div class="col-md-4"></div>
    				<div class="col-md-4" style="text-align:center;">
    					<input type="button" id="subButton" class="marginRightButton" value="Confirm Settings" onClick="window.location='#top'">
    				</div>
    				<div class="col-md-4" style="text-align:right;">
    					<?php
    					   if ($_SESSION["permissions"]["deleteUsers"] == "1") {
    						?>
    						<input type="button" class="deleteBtn deleteSmallButton" id="subButtonDel" value="Delete User">
    						<?php
                            }
    				    ?>
    				</div>
				</div>
			</div>
			<div class="clr"></div>
			<div class="clr"></div>
			<div class="clr"></div>
         <input type="hidden" value="<?php echo $spportPortType; ?>" name="supportPortNumberType" id="supportPortNumberType">
		</form>
		
        <div id="updateSoftPhoneDialog" style="display:none"> 
 </div>
        <!-- Sim Ring Criteria Add Form Code Start --> 
        <?php include("simRing/AddSimRingRecord.php"); ?>
        <!-- Sim Ring Criteria Add Form Code End -->

        <form action="userMod/simRing/printCSVSimRingCriteria.php" method="POST" name="downloadCSVForm" id="downloadCSVForm" >	
                 <input type="hidden" name="userIdForSimRingCriteria" id="userIdForSimRingCriteria" value="<?php echo $userId;  ?>" />
        </form>



		<div id="dialogMU" class="dialogClass"></div>  
                <div style="background-color: " id="customTagDialog"></div>
		<div style="background-color: lightgoldenrodyellow" id="createCustomTagDialog"></div>
                <div id="simRingDialog" class="dialogClass"></div>
                <div id="csvData"></div>
		<div id="clearMACADialog" class="dialogClass"></div>
                <div id="deleteSimRingDialog" class="dialogClass"></div>
                <div id="deleteSimRingDialogSuccess" class="dialogClass"></div>
                <div id="simRingDialogSuccess" class="dialogClass"></div>
<?php
	}
?>
<div id="modUserBodyVDM"></div>