<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/customTagManagement.php");
require_once("/var/www/html/Express/userMod/UserCustomTagManager.php");
require_once("/var/www/lib/broadsoft/adminPortal/util/sasUtil.php");

unset($_SESSION['usersProvisionedCustomTags']);
unset($_SESSION['assignCustomTagToUserForTable']);

$deviceType = $_POST['deviceType'];
$deviceName = $_POST['deviceName'];
$customProfile = $_POST['deviceCustomProfile'];
$error = false;
$sasUtil = new SasUtil();

if(!empty($deviceName)){
	$customTagManager = new CustomTagManagement();
	$response = $customTagManager->getUserCustomTags($deviceName);
	$existingCustomTagList = $response["Success"];
	$customTag = array();
	
	if(count($existingCustomTagList) > 0){
		foreach ($existingCustomTagList as $key => $value){
			$isSasUser = $sasUtil->isSASUser($value);
			if($isSasUser){
				unset($existingCustomTagList[$key]);
			}
		}
		
	}
	
	if(count($existingCustomTagList)>0){
	    foreach($existingCustomTagList as $key => $tagName){
			$var = $customTagManager->groupAccessDeviceDeleteCustomTag($deviceName, $tagName);
			$customTag["delete"][] = array('status'=> "success", 'customTagName'=>$tagName, 'customTagValue'=>"");
		}
		/* changeLog Deleted Tags */
		$customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
		$changeLogRes = $customTags->userCustomTagChangeLogOperations($customTag, $_SESSION ["modUserId"]);
	}
	
	if ($customProfile == "None") { 
		return ""; 
	}
	$query = "select customTagName, customTagValue from customProfiles where  deviceType ='" . $deviceType . "' and customProfileName ='" . $customProfile . "'";
	$results = $db->query($query);
	
	while ($row = $results->fetch()) {
		$errMsg = $customTagManager->addDeviceCustomTag($deviceName, $row["customTagName"], $row["customTagValue"]);
		$customTag["add"][] = array('status'=> "success", 'customTagName'=>$row["customTagName"], 'customTagValue'=> $row["customTagValue"]);
		//return $errMsg;		
	}
	
	/* changeLog Added tags */
	if(isset($customTag['delete'])) {
	    unset($customTag['delete']);
	}
	$customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
	$changeLogRes = $customTags->userCustomTagChangeLogOperations($customTag, $_SESSION ["modUserId"]);
	
	/* changeLog Added tags */
	if(isset($customTag['delete'])) {
	    unset($customTag['delete']);
	}
	$customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
	$changeLogRes = $customTags->userCustomTagChangeLogOperations($customTag, $_SESSION ["modUserId"]);
	// finally add custom tag with the custom profile name for the reporting purpose
	$errMsg = $customTagManager->addDeviceCustomTag($deviceName, "%Express_Custom_Profile_Name%", $customProfile);
	//	return $errMsg;
	
	$userCustomTagManager = new UserCustomTagManager($deviceName, $_SESSION["userInfo"]["deviceType"]);
	
	//echo $userCustomTagManager->formatCustomTagsTableFlag();
        echo $userCustomTagManager->formatCustomTagsTable();
        
}
