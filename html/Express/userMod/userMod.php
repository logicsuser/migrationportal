<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();

	unset($_SESSION["autoFill"]);

?>
<script>
	$(function() {
		
		//dialog box code 
		  $("#userModifyPreventDialog").dialog({
		  	autoOpen: false,
		  	width: 600,
		  	modal: true,
		  	position: { my: "top", at: "top" },
		  	resizable: false,
		  	closeOnEscape: false,
		  	buttons: {
		  		"Yes": function() {
		  			if(buttonNameClickEvent =='groupModify'){
						checkResult(false) ;
						getModifyData();
					}
				$("#userModifyPreventDialog").dialog('close');
				},
		  		"No": function() {
		  			checkResult(true);
		  			$(this).dialog("close");
		  			return false; 
		  		} 
		  	},  
		      open: function() {
                        $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019	
                        $(".ui-dialog-buttonpane button:contains('Yes')").button().show().addClass('subButton');
		        $(".ui-dialog-buttonpane button:contains('No')").button().show().addClass('cancelButton');;
		      }
		  });
		/// 
		
		
		$("#module").html("> User Modify: ");
		$("#endUserId").html("");

		var sp = "<?php echo $_SESSION["sp"]; ?>";
		var group = "<?php echo $_SESSION["groupId"]; ?>";
		var autoComplete = new Array();

		pendingProcess.push("Modify User");
		$.ajax({
			type: "POST",
			url: "userMod/getAutoFill.php",
			success: function(result)
			{
				if(foundServerConErrorOnProcess(result, "Modify User")) {
					return false;
              	}
				var explode = result.split(":");
				for (a = 0; a < explode.length; a++)
				{
					autoComplete[a] = explode[a];
				}
				$("#searchVal").autocomplete({
					source: autoComplete,
					appendTo: "#hidden-stuff"
				});
			}
		});

		
		$('#searchVal').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) {
				e.preventDefault();
				$('#go').trigger('click');
			  }
			});

		
		$('#searchOption').on('keyup keypress', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) {
			e.preventDefault();
			return false;
		  }
		});
		/* $("#go").click(function()
		{
			$("#userData2").html("");
			if ($("#searchVal").val() !== "")
			{
				$("#loading2").show();
				var dataToSend = $("#searchOption").serialize();
				$.ajax({
					type: "POST",
					url: "userMod/userInfo.php",
					data: dataToSend,
					dataType: "html",
					cache: false,
					ajaxOptions: { cache: false },
					success: function(result)
					{
						$("#loading2").hide();
						$("#userData2").html(result);
						checkResult(true); // this function used for user modify or group if any changes and switch to different module.It is define in main.php 
						getPreviousModuleId("userMod"); // this function used for check previous id it is define in main.php and main_enterprise.php.
					}
				});
			}
			else
			{
				$("#endUserId").html("");
			}
		}); */
		
		function functionModifyUserChangesFind(currentClickEvent){
		buttonNameClickEvent = currentClickEvent;
		var dataToSend = $("form#userMod").serializeArray();
						$.ajax({
							type: "POST",
							url: "userMod/checkData.php",
							data: dataToSend,
							success: function(result) {
							  if (result.slice(2, 3) > 0 || result.search("ff0000") > 0)
								{
									 if(buttonNameClickEvent =='groupModify'){
									checkResult(false) ;
									 getModifyData();}
									 $("#userModifyPreventDialog").dialog('close');
								}
								else{									
										$("#userModifyPreventDialog").html('Are you sure you want to leave? <br/> You might lose any change you have made for this Modify User ');
										$("#userModifyPreventDialog").dialog('open');
							}
								 
							}
						});
			 }	
			 
	
		var getModifyData =  function(){
			
			$("#userData2").html("");
			if ($("#searchVal").val() !== "")
			{
				$("#loading2").show();
				var dataToSend = $("#searchOption").serialize();
				$.ajax({
					type: "POST",
					url: "userMod/userInfo.php",
					data: dataToSend,
					dataType: "html",
					cache: false,
					ajaxOptions: { cache: false },
					success: function(result)
					{
						$("#loading2").hide();
						$("#userData2").html(result);
						checkResult(true); // this function used for user modify or group if any changes and switch to different module.It is define in main.php and main_enterprise.php.
						getPreviousModuleId("userMod"); // this function used for check previous id it is define in main.php and main_enterprise.php.
					}
				});
			}
			else
			{
				$("#endUserId").html("");
			}
			
		}
		
		$("#go").click(function()
		{
			 
			if(isCheckDataModify == true){
				$("#userModifyPreventDialog").dialog('close');
				functionModifyUserChangesFind('groupModify'); } 
			
		 else{
			getModifyData(); 
		 }
		 });
		
		
		

	});
</script>
	<div id="modUserBody">

        <h2 class="h2Heading mainBannerModify">Modify User</h2>
<!-- <div class="vertSpacer">&nbsp;</div> -->


<div style="">
        <div class="searchBodyForm">
        	<form name="searchOption" id="searchOption" method="POST" class="fcorn-register container">
			<div class="row">
				<div class="">
						<div class="form-group">
						<label class="labelText" for="searchVal">Search By Name or Phone Number</label><br>
						<input style="" type="text" class="autoFill magnify" name="searchVal" id="searchVal" size="55" value="<?php echo $_GET["userId"]; ?>">
						<div id="hidden-stuff" style="height: 0px;"></div>
						</div>
				</div>
				<div class="col-md-12" style="text-align: center;">
					<div class="col-md-4"></div>
					<div class="col-md-4" style="text-align: center;"><input type="button" name="go" class="go" id="go" value="Get"></div>
					<div class="col-md-4"></div>
				</div>
    		</div>
        	</form>
        </div>
        <div class="loading" id="loading2"><img src="/Express/images/ajax-loader.gif"></div>
        <div id="userData2"></div>
     </div>
<div id="userModifyPreventDialog" class="dialogClass"></div>