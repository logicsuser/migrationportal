<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	if (!isset($_SESSION["autoFill"]))
	{
		$xmlinput = xmlHeader($sessionid, "UserGetListInGroupRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<GroupId>" . htmlspecialchars($_SESSION["groupId"]) . "</GroupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		foreach ($xml->command->userTable->row as $key => $value)
		{
			$name = strval($value->col[1]) . ", " . strval($value->col[2]);
			if (strval($value->col[4]))
			{
				$phoneNumber = substr(strval($value->col[4]), 3) . "x" . strval($value->col[10]) . " - " . $name;
			}
			else
			{
				$phoneNumber = strval($value->col[0]) . " - " . $name;
			}

			$_SESSION["autoFill"][$phoneNumber] = strval($value->col[0]);
		}

		if (isset($_SESSION["autoFill"]))
		{
			ksort($_SESSION["autoFill"]);
		}
//print_r($_SESSION["autoFill"]);
	}

	if (isset($_SESSION["autoFill"]))
	{
		foreach ($_SESSION["autoFill"] as $key => $value)
		{
			echo $key . ":";
		}
	}
?>
