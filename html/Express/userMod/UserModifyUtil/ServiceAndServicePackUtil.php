<?php
/**
 * Created by Sollogics [Pankaj].
 * Date: 10/Jan/2019
 */
require_once ("/var/www/lib/broadsoft/adminPortal/services/userServices.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
// require_once("/var/www/lib/broadsoft/adminPortal/ModifyBLFService.php");

class ServiceAndServicePackUtil {
    public $userServices = array();
    public $userId;
    private $deviceName;
    public $surgeMailUserId = "";
    
    public function __construct($userId, $deviceName="") {
        $this->userId = $userId;
        $this->deviceName = $deviceName;
        $this->userServices = $this->getUserServices();
    }
    
    private function getUserServices() {
        $serviceResponse = array();
        $service = new userServices();
        $serviceResponse = $service->getUserServicesAll($this->userId, $_SESSION["sp"]);
        if( empty($serviceResponse["Error"]) ) {
           $serviceResponse = $serviceResponse["Success"]["Services"];
        }
        
        return $serviceResponse;
    }
    
	public function userServiceOperation($POST) {
	    $responseData["Error"] = array();
	    $responseData["Success"] = array();
	    
	    $assignedUserServicesVal = ($POST['assignedUserServicesVal'] != "") ? explode(";", rtrim($POST['assignedUserServicesVal'], ";")) : array();
	    $servicesToBeOperated = $this->getAssingedAndUnAssingedServices($assignedUserServicesVal);
	    $serviceObj = new Services();
	    $initialUserServicesAssignmentList = null;
	        /* Assignment of services */
	        if( $servicesToBeOperated['NewServicesForAssignment'] == "Yes" ) {
	            $initialUserServicesAssignmentList = new UserServiceGetAssignmentList($this->userId);
	            $assignResponse = $serviceObj->assignServicesToUser($this->userId, $servicesToBeOperated['assign']);
	            if( empty($assignResponse["Error"]) ) {
	                $responseData["Success"][] = "Assigned Services to user : " . implode(", ", $servicesToBeOperated['assign']) . ".";
	            } else {
	                $serviceErrorList = $serviceObj->getServiceLicenseErrorOnAssignServicePackToUser($_SESSION["groupId"], $servicesToBeOperated['assign']);
	                foreach($serviceErrorList as $errorServiceName => $serviceErrorMessage) {
	                    $errorMsg = "";
	                    $errorMsg .= "<li style='color:red' > Service : ". $errorServiceName ." can not assign to user </li>";
	                    $errorMsg .= "<li style='color:red' >$errorServiceName : $serviceErrorMessage</li>";
	                    $responseData["Error"][] = $errorMsg;
	                }
	                //$responseData["Error"][] = ErrorHandlingUtil::getErrorMessage($assignResponse);
	            }
	        }
	        
	        /* Unassignment of services */
	        if( $servicesToBeOperated['NewServicesForUnAssignment'] == "Yes" ) {
	            if ($initialUserServicesAssignmentList == null) {
	                $initialUserServicesAssignmentList = new UserServiceGetAssignmentList($this->userId);
	            }
	            if ($initialUserServicesAssignmentList->hasService("Voice Messaging User")) {
	                $this->surgeMailUserId = $this->getsurgeMailUserId($this->userId);
	            }
	            $unAssignResponse = $serviceObj->unAssignServicesFromUser($this->userId, $servicesToBeOperated['unAssign']);
	            if( empty($unAssignResponse["Error"]) ){
	                $responseData["Success"][] = "Unassigned Services to user : " . implode(", ", $servicesToBeOperated['unAssign']) . ".";
	            } else {
	                $responseData["Error"][] = ErrorHandlingUtil::getErrorMessage($unAssignResponse);
	            }
	        }
	        
	        /* Configuration for BLF, VM, and surgemail */
            if(  $servicesToBeOperated['NewServicesForAssignment'] == "Yes" ||
                 $servicesToBeOperated['NewServicesForUnAssignment'] == "Yes"
	        ){
                
	            $this->configOperationsForServices($initialUserServicesAssignmentList);
	        }
	        
	    return $responseData;
	}

	public function getAssingedAndUnAssingedServices($newAssignedServices) {
		$servicesToBeOperated = array();
		foreach($this->userServices['assigned'] as $assignedKey => $assignedValue) {
			if( ! in_array($assignedValue, $newAssignedServices) ) {
				$servicesToBeOperated['unAssign'][] = $assignedValue;
				$servicesToBeOperated['NewServicesForUnAssignment'] = "Yes";
			}
		}

		foreach($newAssignedServices as $serKey => $serValue) {
		    if( ! in_array($serValue, $this->userServices['assigned'])) {
				$servicesToBeOperated['assign'][] = $serValue;
				$servicesToBeOperated['NewServicesForAssignment'] = "Yes";
			}
		}
		
		if( empty($servicesToBeOperated['assign']) && empty($newAssignedServices) ){
		    $servicesToBeOperated['NewServicesForAssignment'] = "No";
		}
		if( empty($servicesToBeOperated['unAssign']) ){
		    $servicesToBeOperated['NewServicesForUnAssignment'] = "No";
		}

		return $servicesToBeOperated;
	}
	
	public function builChangeLogDataForServices($newAssignedServices) {
// 	    $key = 0;
// 	    if($changeLog) {
// 	        $key = end(array_keys($changeLog));
// 	        $key++;
// 	    }
	    $changeLog = array();
	    //$changeLog["field"] = "assignedUserServicesVal";
	    $changeLog["old"] = implode(",", $this->userServices['assigned']);
	    $changeLog["new"] = implode(",", $newAssignedServices);
	   
	    return $changeLog;
	}
	
	function checkUserHasService($services, $serviceName) {
	    foreach($services as $sKey => $sVal) {
	        if(strpos($sVal , $serviceName) !== false) {
	            return true;
	        }
	    }
	    return false;
	}
	
	public function configOperationsForServices($initialUserServicesAssignmentList) {
	        
	       $finalUserServicesAssignmentList = new UserServiceGetAssignmentList($this->userId);
	        
	        // process BLF service initialization
	        if ( ! $initialUserServicesAssignmentList->hasService("Busy Lamp Field") && $finalUserServicesAssignmentList->hasService("Busy Lamp Field")) {
	            $blfModify = new ModifyBLFService($this->userId, $this->deviceName);
	        }
	        // Process VM service initialization
	        if ( ! $initialUserServicesAssignmentList->hasService("Voice Messaging User") && $finalUserServicesAssignmentList->hasService("Voice Messaging User")) {
	            $emailAddress = $this->getSurgeMailAddress();
	            $passCode = $this->getVoicePortalPassCode();
	            $vmService = new ModifyVMService($this->userId);
	            $vmService->add($passCode, $emailAddress);
	        }
	        
	        // Remove Surge Mail on VM Service removal
	        if ( $initialUserServicesAssignmentList->hasService("Voice Messaging User") && ! $finalUserServicesAssignmentList->hasService("Voice Messaging User")) {
	            $vmService = new ModifyVMService($this->userId);
	            $vmService->remove($this->surgeMailUserId);
	        }	        
	}
	
	// Create surgemail username from the first formula.
	public function getSurgeMailAddress() {
	    global $surgeMailIdCriteria1, $surgeMailIdCriteria2;
	    
	    $surgeMailNameBuilder1 = new SurgeMailNameBuilder($surgeMailIdCriteria1,
	        $_SESSION["userInfo"]["phoneNumber"],
	        $_SESSION["userInfo"]["extension"],
	        $_SESSION["userInfo"]["callingLineIdPhoneNumber"],
	        isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "",
	        $_SESSION["systemDefaultDomain"],
	        $_SESSION["userInfo"]["firstName"],
	        $_SESSION["userInfo"]["lastName"],
	        $surgeMailIdCriteria2 == "");    // force fallback only if second criteria does not exist
	        if (! $surgeMailNameBuilder1->validate()) {}
	        do {
	            $attr = $surgeMailNameBuilder1->getRequiredAttributeKey();
	            $surgeMailNameBuilder1->setRequiredAttribute($attr, getResourceForNameCreation($attr));
	        } while ($attr != "");
	        
	        $surgemailName = $surgeMailNameBuilder1->getName();
	        if ($surgemailName == "") {
	            // Create surgemail name from the second formula. At this point we know that there is second formula,
	            // because without second formula name would be forcefully resolved in the first formula.
	            
	            $surgeMailNameBuilder2 = new SurgeMailNameBuilder($surgeMailIdCriteria2,
	                $_SESSION["userInfo"]["phoneNumber"],
	                $_SESSION["userInfo"]["extension"],
	                $_SESSION["userInfo"]["callingLineIdPhoneNumber"],
	                isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "",
	                $_SESSION["systemDefaultDomain"],
	                $_SESSION["userInfo"]["firstName"],
	                $_SESSION["userInfo"]["lastName"],
	                true);
	            if (! $surgeMailNameBuilder2->validate()) {}
	            do {
	                $attr = $surgeMailNameBuilder2->getRequiredAttributeKey();
	                $surgeMailNameBuilder2->setRequiredAttribute($attr, getResourceForNameCreation($attr));
	            } while ($attr != "");
	            
	            $surgemailName = $surgeMailNameBuilder2->getName();
	        }
	        return $surgemailName;
	}
	
	// Build Voice Mail Password. Enforce fallback
	public function getVoicePortalPassCode() {
	    global $bwVoicePortalPasswordType, $userVoicePortalPasscodeFormula;
	    
	    if ($bwVoicePortalPasswordType == "Formula") {
	        $voicePasswordBuilder = new VoicePasswordNameBuilder($userVoicePortalPasscodeFormula,
	            $_SESSION["userInfo"]["phoneNumber"],
	            $_SESSION["userInfo"]["extension"],
	            $_SESSION["userInfo"]["callingLineIdPhoneNumber"],
	            isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "",
	            true);
	        if (! $voicePasswordBuilder->validate()) {}
	        do {
	            $attr = $voicePasswordBuilder->getRequiredAttributeKey();
	            $voicePasswordBuilder->setRequiredAttribute($attr, getResourceForNameCreation($attr));
	        } while ($attr != "");
	        
	        $portalPassWord = $voicePasswordBuilder->getName();                
	    } else {
	        $portalPassWord = setVoicePassword();
	    }
	    $portalPassWord = setVoicePassword();  //Added by Anshu due to the postcode error (Must be numeric) @05 March 2018
	    return $portalPassWord;
	}
	
	public function getsurgeMailUserId($userId) {
	        global $sessionid, $client;
	        $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetAdvancedVoiceManagementRequest14sp3");
	        $xmlinput .= "<userId>" . $userId . "</userId>";
	        $xmlinput .= xmlFooter();
	        $response = $client->processOCIMessage(array("in0" => $xmlinput));
	        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	        $surgeMailUserId = isset($xml->command->groupMailServerUserId) ? strval($xml->command->groupMailServerUserId) : "";
	    return $surgeMailUserId;
	}
	
}
?>