<?php 
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/devices/deviceTypeManagement.php");
$devobj = new DeviceTypeManagement();
$enterpriseDeviceTypeList = $devobj->getEnterpriseTableDevices($_SESSION["sp"]);
$deviceTypeList = $devobj->getSystemTableDevices();
$finalList = array();
$obj = new UserOperations();
$explodedValArr[] = $_POST["userId"];
$_POST["allUserSearch"] = trim($_POST["allUserSearch"]);
if($_POST["restrictToGroup"] == "true"){
    require_once ("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");
    $userLists1 = $obj->getGroupBlfUsersList($_POST["userId"]);
}else{
    require_once ("/var/www/lib/broadsoft/adminPortal/getServiceProviderAllUsers.php");
    $userLists1 = $obj->getEnterpriseBlfUsersList();
}

if($userLists1["Error"] == ""){
    if($_POST["monitoredUsers"] != ""){
        $explodedVal = explode(";", $_POST["monitoredUsers"]);
        foreach ($explodedVal as $expvalue){
            $arrVal = explode(":", $expvalue);
            $explodedValArr[] = $arrVal[0];
        }
        
    }
    foreach ($userLists1["Success"] as $arrKey => $arrValue){
        if(in_array($arrValue["id"], $explodedValArr)){
            unset($userLists1["Success"][$arrKey]);
        }
    }
    if(count($userLists1["Success"]) > 0){
        foreach ($userLists1["Success"] as $key => $value){
            $userAssigned = $obj->getUserDetail($value["id"]);
            if($userAssigned["Success"]["deviceType"] !=""){
                if(array_key_exists($userAssigned["Success"]["deviceType"], $enterpriseDeviceTypeList)){
                    if($enterpriseDeviceTypeList[$userAssigned["Success"]["deviceType"]]["enabled"] != "true" || $enterpriseDeviceTypeList[$userAssigned["Success"]["deviceType"]]["blf"] == "No"){
                        unset($userLists1["Success"][$key]);
                    }
                    
                }else if(array_key_exists($userAssigned["Success"]["deviceType"], $deviceTypeList)){
                    if($deviceTypeList[$userAssigned["Success"]["deviceType"]]["enabled"]!="true" || $deviceTypeList[$userAssigned["Success"]["deviceType"]]["blf"] == "No"){
                        unset($userLists1["Success"][$key]);
                    }
                }
            }
        }
        
        
    }
    $userLists1["Success"] = array_values($userLists1["Success"]);
}


if($userLists1["Error"] == ""){
    $listOfUsers = $userLists1["Success"];
    //print_r($listOfUsers);
    if($_POST["allUserSearch"] != ""){
        $arr = explode(" - ", $_POST["allUserSearch"]);
        $_POST["allUserSearch"] = trim($arr[0]);
        //print_r($_POST["allUserSearch"]);
        //print_r($listOfUsers);
        foreach ($listOfUsers as $key1 => $value1){
            /*if(trim($value1["id"]) == trim($_POST["userId"])){
                continue;
            }*/
            if (stripos($value1["tempId"], $_POST["allUserSearch"]) !== false) {
                $finalList[] = $listOfUsers[$key1];
            }else if(stripos($value1["name"], $_POST["allUserSearch"]) !== false){
                $finalList[] = $listOfUsers[$key1];
            }
        }
    }else{
        $finalList = $listOfUsers;
    }
}
echo count($finalList)."***";
for ($a = 0; $a < count($finalList); $a++)
{
    $userNumAndExt = getMonitoredUserNumberAndExtension($users, $finalList[$a]["id"]);
    
    $id = $finalList[$a]["id"] . ":" . $finalList[$a]["name"] . ":" .$userNumAndExt;
    echo "<li class=\"ui-state-default\" id=\"" . $id . "\">" . $finalList[$a]["name"] . "  (" . $userNumAndExt . ")" . "</li>";
    
}

function getMonitoredUserNumberAndExtension($users, $id) {
    foreach ( $users as $index => $user ) {
        if ($user ["id"] != $id) {
            continue;
        }
        $numExt = $user ["extension"];
        if ($user ["phoneNumber"] != "") {
            $userNum = explode ( "+1-", $user ["phoneNumber"] );
            $userNum = $userNum [1];
            $numExt = $userNum . "x" . $numExt;
        }
        return $numExt;
    }
    return "";
}

?>