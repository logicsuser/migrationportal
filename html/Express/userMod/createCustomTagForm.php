<?php
/**
 * HTML content of device's Custom Tag dialog form
 * Created by Karl.
 * Date: 11/14/2016
 */

    $mode = "create";

    $tagName = "";
    $tagValue = "";
    $tagDisable = "";

    // Retrieve tag information in update mode
    if (isset($_POST["customTagName"])) {
        $mode = "update";

        $tagName  = $_POST["customTagName"];
        $tagValue = $_POST["customTagValue"];
        $tagDisable = "disabled";
    }

?>

<script>

    function evalTagDialog(txtBox) {
        var enableCreate = "disable";
        if($('#tagInputFieldValue').is(':visible')) {
            enableCreate = $('#tagValue').val().length > 0 ? "enable" : "disable";
        } else if ($('#tagSelectFieldValue').is(':visible')) {
        	enableCreate = $('#tagSelectFieldValue option:selected').val() !== ''? "enable" : "disable";
        } else if ($('#tagRangeFieldValue').is(':visible')) {
            var min = parseInt($('#tagRangeInputField').attr('min'));
            var max = parseInt($('#tagRangeInputField').attr('max'));
            var tagVal = parseInt($('#tagRangeInputField').val());
            enableCreate = (tagVal >= min && tagVal <= max) ? "enable" : "disable";
        }
        $(".ui-dialog-buttonpane button:contains('Create')").button(enableCreate);
        $(".ui-dialog-buttonpane button:contains('Update')").button(enableCreate);
    }

</script>

<div style="background-color: lightgoldenrodyellow; font-size: 12px">
    <form action="#" method="POST" name="customTagForm" id="customTagForm" class="fcorn-register container">
        <!-- Tag name  -->
        <div class="diaPL diaP12" style="width: 30%"><label for="tagName"><?php echo $mode == "create" ? "<span class=\"required\">*</span> " : ""; ?>Tag Name</label></div>
        	<div class="diaPN diaP12 dropdown-wrap" style="width:66%;">
        		<!-- select Name="tagName" onmouseover="this.title=this.options[this.selectedIndex].title" id="tagShortDescription" onchange="evalTagDialog(this)" <?php echo ($mode == "update" ? " disabled" : ""); ?>>
        		</select-->
        		<input name="tagName" id="tagName" value="<?php echo $tagName;?>" <?php echo $tagDisable;?> />
        	</div>
        <div id = "tagSelectFieldValue">
        	<div class="diaPL diaP12" style="width: 30%"><label for="tagValue">Tag Value</label></div>
        	<div class="diaPN diaP12 dropdown-wrap" style="width: 66%">
        		<!-- select name="tagValue" onmouseover="this.title=this.options[this.selectedIndex].title" id ="tagOptions" onchange="evalTagDialog(this)"></select-->
        		<input name="ctagValue" id="ctagValue" value="<?php echo $tagValue;?>" />
        	</div>
        </div>
    </form>
</div>
