<?php
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");

$flag = 0;
$servicesArr = array();
$spId    = $_SESSION["sp"];
$groupId = $_SESSION["groupId"];


$userServicePack = array();
$allServicePack  = array();

if(isset($_POST['act']) && $_POST['act']=="retrieveServicePackToAdd")
{
    $userServicePack = $_POST['userServicePack'];
    $allServicePack  = $_POST['allServicePack'];

    $servicePackWithSCA = checkSCAServiceInUserSelectedServicePack($userServicePack, $client, $sessionid, $spId);
    if($servicePackWithSCA=="NONE")
    {
        echo $servicePackWithSCAToSelected = checkSCAServiceInUserSelectedServicePack($allServicePack, $client, $sessionid, $spId);
    }
    else 
    {
        echo "NONE";
    }
}


if(isset($_POST['act']) && $_POST['act']=="retrieveServicePackToDel")
{
    $userServicePack   = $_POST['userServicePack'];
    $servicePackWithSCA = checkSCAServiceInUserSelectedServicePack($userServicePack, $client, $sessionid, $spId);
    if($servicePackWithSCA!="NONE")
    {        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId). "</serviceProviderId>";
        $xmlinput .= "<servicePackName>" . $servicePackWithSCA . "</servicePackName>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
          
        if(count($xml->command->userServiceTable->row)=="1")
        {
            $pos = strpos(strval($xml->command->userServiceTable->row->col[0]), "Shared Call Appearance");
            if($pos === false)
            {
                echo "NONE";
            }
            else
            {            
                echo $servicePackWithSCA;
            }
        }
        else
        {
            echo "NONE";
        }
    }
    else
    {
        echo "NONE";
    }
}



function checkSCAServiceInUserSelectedServicePack($servicePacks, $client, $sessionid, $spId)
{
        $flag = 0;
        $anFlag = 0;
        $servicePacksWithSCAServiceArr = explode(",", $_SESSION["scaServicePacksStr"]); 
        //$servicePacksWithVMServiceArr = array("sol_express");       
        for($i=0;$i<count($servicePacks);$i++)
        {
            if(in_array($servicePacks[$i], $servicePacksWithSCAServiceArr))
            {
                $flag = $flag + 1;
                $servicePackWithSCAService = $servicePacks[$i];
            }
        }        
        if($flag==1)
        {    
            $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId). "</serviceProviderId>";
            $xmlinput .= "<servicePackName>" . $servicePackWithSCAService . "</servicePackName>";
            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            
            if(count($xml->command->userServiceTable->row)=="1")
            {
                $pos = strpos(strval($xml->command->userServiceTable->row->col[0]), "Shared Call Appearance");
                if($pos === false)
                {
                    //echo "NONE";
                }
                else
                {            
                    $anFlag = 1;
                }
            }
            else
            {
                $anFlag = 1;
            }
            
            if($anFlag==1)
            {
                return $servicePackWithSCAService;
            }
            else
            {
                return "NONE";
            }
        }
        else 
        {
            return "NONE";
        }
}

?>