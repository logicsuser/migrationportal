<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/simRingCriteriaManagement.php");
 
$objSimRingMgmt   = new SimRingCriteriaManagement();
$userId = $_SESSION["userInfo"]['userId'] ;
if($_POST['funcType'] == "deleteSimRingCriteria"){
               
        $arraySimRing['userId']       = $_POST['userId'];
        $arraySimRing['criteriaName'] = $_POST['criteriaName'];
        
        if(isset($_SESSION['deleteSimRingCriteria']) && !empty($_SESSION['deleteSimRingCriteria'])){
            $tempTotalRows                     =  $_SESSION['deleteSimRingCriteria'];
            $tempTotalRows[]                   =  $arraySimRing;
            $_SESSION['deleteSimRingCriteria'] =  $tempTotalRows;
        }else{
            $tempTotalRows                     =  array(); 
            $tempTotalRows[]                   =  $arraySimRing;
            $_SESSION['deleteSimRingCriteria'] =  $tempTotalRows;
        }
        
        echo "The following changes are complete:<br /><br />";
        echo "Criteria name will be deleted";
}


if($_POST['funcType'] == "updateSimRingCriteria"){ 
    
    $arraySimRing = array();    
    for($x=0; $x<12; $x++){
        if($_POST["criteriaIncomingPhone"][$x]==""){
            unset($_POST["criteriaIncomingPhone"][$x]);
        }
    }
    
    $arraySimRing["userId"]                             = $userId ;
    $arraySimRing["useSimRingPersonal"]                 = ($_POST['useSimRingPersonal'] == "true" ? "true" : "false");       
    $arraySimRing["userSimRingPersonal"]                = ($_POST['useSimRingPersonal'] == "true" ? "true" : "false");       
    $arraySimRing["oldCriteriaName"]                    = $_POST['simRingOldCriteriaName'];
    $arraySimRing["criteriaName"]                       = $_POST['simRingCriteriaName'];
    $arraySimRing["isActive"]                           = $_POST['simRingCriteriaActiveStatus'];
    
    if($_POST['simRingCriteriaTime']!=""){
        
        $tmpTimeArr = explode(" - ", $_POST['simRingCriteriaTime']);
        $arraySimRing["timeScheduleName"]                   = $tmpTimeArr[0];
        $arraySimRing["timeScheduleLevel"]                  = $tmpTimeArr[1];
        $arraySimRing["simRingCriteriaTime"]                = $_POST['simRingCriteriaTime'];
        $tmpLevelTime                                       = $tmpTimeArr[1];
        if($tmpTimeArr[1]=="Service Provider"){
            $tmpLevelTime = "Enterprise";
        }
        $arraySimRing["timeSchedule"]                       = $tmpTimeArr[0]."(".$tmpLevelTime.")";
    }
    if($_POST['simRingCriteriaHoliday']!=""){
        
        $tmpHolidayArr = explode(" - ", $_POST['simRingCriteriaHoliday']);
        $arraySimRing["holidayScheduleName"]                = $tmpHolidayArr[0];
        $arraySimRing["holidayScheduleLevel"]               = $tmpHolidayArr[1];
        $arraySimRing["simRingCriteriaHoliday"]             = $_POST['simRingCriteriaHoliday'];
        $tmpLevelHoliday                                    = $tmpHolidayArr[1];
        if($tmpHolidayArr[1]=="Service Provider"){
            $tmpLevelHoliday = "Enterprise";
        }
        $arraySimRing["holidaySchedule"]                    = $tmpHolidayArr[0]."(".$tmpLevelHoliday.")";
    }
    
    $arraySimRing["simRingIncomingCallsFrom"]               = $_POST['simRingIncomingCallsFrom']; 
    
    
    $arraySimRing["simRingIncomingPhoeFromPrivate"]         = ($_POST['simRingIncomingPhoeFromPrivate'] == "true" ? "true" : "false");   
    $arraySimRing["simRingIncomingPhoeFromUnavailable"]     = ($_POST['simRingIncomingPhoeFromUnavailable'] == "true" ? "true" : "false");   
    $arraySimRing["criteriaIncomingPhone"]                  = array_values($_POST['criteriaIncomingPhone']);
    
    
    if($arraySimRing["simRingIncomingCallsFrom"]=="Any"){
        $arraySimRing["callsFromIncomingPhoneNumberList"]   = "All calls"; 
    }
    if($arraySimRing["simRingIncomingCallsFrom"]=="Specified Only"){
        $tmpVal = "";
        if($arraySimRing["simRingIncomingPhoeFromPrivate"]=="true"){
            $tmpVal .= "Any private number, ";
        }
        if($arraySimRing["simRingIncomingPhoeFromUnavailable"]=="true"){
            $tmpVal .= "Any unavailable number, ";
        }
        if(!empty($arraySimRing["criteriaIncomingPhone"])){
            $tmpVal .= implode(", ", $arraySimRing["criteriaIncomingPhone"]);
        }
        $arraySimRing["callsFromIncomingPhoneNumberList"]   = $tmpVal; 
    }
    
    
    
    if(isset($_SESSION['editSimRingCriteria']) && !empty($_SESSION['editSimRingCriteria'])){
            $tempTotalRows                     =  $_SESSION['editSimRingCriteria'];
            $tempTotalRows[]                   =  $arraySimRing;
            $_SESSION['editSimRingCriteria']   =  $tempTotalRows;
    }else{
            $tempTotalRows                     =  array(); 
            $tempTotalRows[]                   =  $arraySimRing;
            $_SESSION['editSimRingCriteria']   =  $tempTotalRows;
    } 
    
    echo "The following changes are complete:<br /><br />";
    echo "Criteria updated in list";
}


if($_POST['funcType'] == "addSimRingCriteria"){
    
    $arraySimRing = array();    
    for($x=0; $x<12; $x++){
        if($_POST["criteriaIncomingPhone"][$x]==""){
            unset($_POST["criteriaIncomingPhone"][$x]);
        }
    }
    
    $arraySimRing["userId"]                             = $userId ;
    $arraySimRing["useSimRingPersonal"]                 = ($_POST['useSimRingPersonal'] == "true" ? "true" : "false");       
    $arraySimRing["userSimRingPersonal"]                = ($_POST['useSimRingPersonal'] == "true" ? "true" : "false");       
    $arraySimRing["criteriaName"]                       = $_POST['simRingCriteriaName'];
    $arraySimRing["isActive"]                           = "true";
    
    
    if($_POST['simRingCriteriaTime']!=""){
        
        $tmpTimeArr = explode(" - ", $_POST['simRingCriteriaTime']);
        $arraySimRing["timeScheduleName"]               = $tmpTimeArr[0];
        $arraySimRing["timeScheduleLevel"]              = $tmpTimeArr[1];
        $tmpLevelTime                                   = $tmpTimeArr[1];
        if($tmpTimeArr[1]=="Service Provider"){
            $tmpLevelTime = "Enterprise";
        }
        $arraySimRing["timeSchedule"]                   = $tmpTimeArr[0]."(".$tmpLevelTime.")";
    }
    if($_POST['simRingCriteriaHoliday']!=""){
        
        $tmpHolidayArr = explode(" - ", $_POST['simRingCriteriaHoliday']);
        $arraySimRing["holidayScheduleName"]            = $tmpHolidayArr[0];
        $arraySimRing["holidayScheduleLevel"]           = $tmpHolidayArr[1];
        $tmpLevelHoliday                                = $tmpHolidayArr[1];
        if($tmpHolidayArr[1]=="Service Provider"){
            $tmpLevelHoliday = "Enterprise";
        }
        $arraySimRing["holidaySchedule"]                = $tmpHolidayArr[0]."(".$tmpLevelHoliday.")";
    }
    
    $arraySimRing["simRingIncomingCallsFrom"]           = $_POST['simRingIncomingCallsFrom'];   
    $arraySimRing["simRingIncomingPhoeFromPrivate"]     = ($_POST['simRingIncomingPhoeFromPrivate'] == "true" ? "true" : "false");   
    $arraySimRing["simRingIncomingPhoeFromUnavailable"] = ($_POST['simRingIncomingPhoeFromUnavailable'] == "true" ? "true" : "false");   
    $arraySimRing["criteriaIncomingPhone"]              = array_values($_POST['criteriaIncomingPhone']);
    
    
    if($arraySimRing["simRingIncomingCallsFrom"]=="Any"){
        $arraySimRing["callsFromIncomingPhoneNumberList"]   = "All calls"; 
    }
    if($arraySimRing["simRingIncomingCallsFrom"]=="Specified Only"){
        $tmpVal = "";
        if($arraySimRing["simRingIncomingPhoeFromPrivate"]=="true"){
            $tmpVal .= "Any private number, ";
        }
        if($arraySimRing["simRingIncomingPhoeFromUnavailable"]=="true"){
            $tmpVal .= "Any unavailable number, ";
        }
        if(!empty($arraySimRing["criteriaIncomingPhone"])){
            $tmpVal .= implode(", ", $arraySimRing["criteriaIncomingPhone"]);
        }
        $arraySimRing["callsFromIncomingPhoneNumberList"]   = $tmpVal; 
    }
    
    
    if(isset($_SESSION['addSimRingCriteria']) && !empty($_SESSION['addSimRingCriteria'])){
            $tempTotalRows                    =  $_SESSION['addSimRingCriteria'];
            $tempTotalRows[]                  =  $arraySimRing;
            $_SESSION['addSimRingCriteria']   =  $tempTotalRows;
    }else{
            $tempTotalRows                    =  array(); 
            $tempTotalRows[]                  =  $arraySimRing;
            $_SESSION['addSimRingCriteria']   =  $tempTotalRows;
    } 
    echo "The following changes are complete:<br /><br />";
    echo "Criteria Added in list";
}

?>  
    
   