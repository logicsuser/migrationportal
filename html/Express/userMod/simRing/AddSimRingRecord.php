<?php
    require_once("/var/www/lib/broadsoft/adminPortal/getAllSchedules.php");
    $_SESSION ["criteriaAddNames"] = array (
		"simRingCriteriaName" => "Criteria Name",
                "useSimRingPersonal" => "Simultaneous Ring Personal",
                "simRingCriteriaTime" => "Time Schedule",
                "simRingCriteriaHoliday" => "Holiday Schedule",
                "simRingIncomingCallsFrom" => "Incoming Calls From",
                "simRingIncomingPhoeFromPrivate" => "Incoming Calls From any private number",
                "simRingIncomingPhoeFromUnavailable" => "Incoming Calls From any unavailable number",
                "criteriaIncomingPhone" => "Incoming phone number list"
        );
    
    const defaultTimeSchedule = "Every Day All Day";
    const defaultHolidaySchedule = "None";
    
    function getScheduleOptions($type) {
        global $schedules, $cfsCriteriaTime, $cfsCriteriaHoliday;

        $selection = $type == "Time" ? $cfsCriteriaTime : $cfsCriteriaHoliday;
        $defaultSelection = $type == "Time" ? defaultTimeSchedule : defaultHolidaySchedule;

        // Build the list of options and mark one of them 'selected' based on selection stored in $_SESSION
        // <option value="Every Day All Day" selected>Every Day All Day</option>
        $str = "<option value=\"\"";
        $str .= $selection == $defaultSelection ? " selected" : "";
        $str .= ">" . $defaultSelection . "</option>";
        echo $str;

        foreach ($schedules as $key=>$value) {
            if ($value["level"] == "System") {
                $tmpLevel = "System";
                $level = " (System)";
            }
            elseif ($value["level"] == "Service Provider") {
                $tmpLevel = "Service Provider";
                $level = " (Enterprise)";
            }
            elseif ($value["level"] == "Group") {
                $tmpLevel = "Group";
                $level = " (Group)";
            }
            else { 
                $tmpLevel = "User";
                $level = " (User)";
                
            }

            if ($value["type"] == $type) {
                echo "<option value=\"" . $value["name"]." - ".$tmpLevel. "\"";
                echo $selection == $value["name"] ? " selected" : "";
                echo ">" . $value["name"] . $level . "</option>";
            }
        }
    }
?>


<div id="sim_ring_criteria_add_dialog" style="display:none;">  
    <div>
          <form action="#" method="POST" name="sim_ring_criteria_add_form" id="sim_ring_criteria_add_form">
              <input type="hidden" name="funcType" value="addSimRingCriteria" />
              <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="simRingCriteriaName" class="labelText"><span class="required" style="float: right;">*</span> Criteria name:</label>
                        <input class="diaBox12 form-control" type="text" name="simRingCriteriaName" id="simRingCriteriaName" value="" size="35">
                    </div>
                </div>

            </div>

                <!-- Forwarding Options  -->
    <div class="row">        
        <div class="col-md-9 modalRightDiv">
            <div class="col-md-12">
                <input type="radio" style="vertical-align: middle"  name="useSimRingPersonal" id="useSimRingPersonalYes" value="false" checked><label for="useSimRingPersonalYes"><span></span></label>
                <label class="labelText customLabelText">Use Simultaneous Ring Personal</label>
            </div>
            
            <div class="col-md-12">            	
                <input style="vertical-align: middle" type="radio" name="useSimRingPersonal" id="useSimRingPersonalNo" value="true"><label for="useSimRingPersonalNo"><span></span></label>
                <label class="labelText customLabelText">Do not use Simultaneous Ring Personal</label>
            </div>
       
           
         </div>
    </div>
        <!-- Schedules  -->
    <div class="row">
        <div class="col-md-6 modalLeftDiv">
            <div class="form-group">
            	<label class="labelText">Time Schedule:</label>
            	<div class="dropdown-wrap">
            		<select  name="simRingCriteriaTime" id=simRingCriteriaTime class="diaBox12" style="width: 100% !important;">
                            <?php getScheduleOptions("Time"); ?>
                	</select>
            	</div>
            </div>
        </div>
         <div class="col-md-6 modalLeftDiv">
            <div class="form-group">
            	<label for="cfsCriteriaFwdDefault" class="labelText">Holiday Schedule:</label>
            	<div class="dropdown-wrap">
            		<select class="diaBox12" name="simRingCriteriaHoliday" id="simRingCriteriaHoliday" style="width: 100% !important;">
                            <?php getScheduleOptions("Holiday"); ?>
                	</select>
            	</div>
            </div>
    	</div>
	</div>
        <!-- Forwarding incoming calls  -->
<div class="row">
    <div class="col-md-3 modalLeftDiv">
            <div class="form-group">
            <label for="cfsCriteriaFwdFromAny" class="labelText">Incoming calls:</label>
            </div>
    </div>
    <div class="col-md-9 modalRightDiv">
        <div class="">
          <input style="vertical-align: middle" type="radio" name="simRingIncomingCallsFrom" id="simRingIncomingCallsFromAnyChk" value="Any" checked ><label for="simRingIncomingCallsFromAnyChk"><span></span></label>
          <label class="labelText customLabelText">From any phone number</label>
        </div>
	
        <div class="">
	    
            <input style="vertical-align: middle" type="radio" name="simRingIncomingCallsFrom" id="simRingIncomingCallsFromChk" value="Specified Only"><label for="simRingIncomingCallsFromChk"><span></span></label>
            <label class="labelText customLabelText">From the following phone numbers:</label>

		<div class="chkFromAny">
                 <input style="vertical-align: middle" type="checkbox" name="simRingIncomingPhoeFromPrivate" id="simRingIncomingPhoeFromPrivate1" value="true"  ><label for="simRingIncomingPhoeFromPrivate1"><span></span></label>
		 <label class="labelText">From any private number</label>
                </div>
		<div class="chkFromAny"> 
		<input style="vertical-align: middle" type="checkbox" name="simRingIncomingPhoeFromUnavailable" id="simRingIncomingPhoeFromUnavailableChk" value="true"><label for="simRingIncomingPhoeFromUnavailableChk"><span></span></label>
		<label class="labelText">From any unavailable number</label></div>
        </div>   
				
    </div>      
</div>
<div class="row">
<div class="col-md-12"><label class="labelText">From specific phone numbers:</label></div>
     <?php 
            //$phoneNumberList
            for($x=0; $x < 12; $x++)
            {                 
                echo "<div class='col-md-3 inputTextDiv' ><input class='diaBox12' style='width:100%;' type='text' name='criteriaIncomingPhone[$x]' id='criteriaIncomingPhone_$x' value=''></div>";
            }
        ?>
    </div>
   </form>  
</div>
</div>


