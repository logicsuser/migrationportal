<?php
    require_once("/var/www/html/Express/config.php");
    require_once ("/var/www/lib/broadsoft/login.php");
    checkLogin();    
    require_once ("/var/www/lib/broadsoft/adminPortal/simRingCriteriaManagement.php");
    $objSimRingMgmt   = new SimRingCriteriaManagement();
    
    $message          = "Criteria Name, Ring Simultaneously, Time Schedule,  Holiday Schedule, Any private number, Any unavailable number, Phone List\n";
     
    if(isset($_POST['userIdForSimRingCriteria']) && $_POST['userIdForSimRingCriteria']!="")
    { 
        $userId = $_POST['userIdForSimRingCriteria'];
        $fileName = $userId."-SimRingCriteria-".rand(1092918, 9281716);
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment;filename=\"" . $fileName . ".csv\"");
        header("Content-Transfer-Encoding: binary");
        
        foreach($_SESSION["userInfo"]["criteriaTableList"] as $value){
            
                $criteraName               = $value['criteriaName'];
                $criteriaDetailResponse    =  $objSimRingMgmt->getSimRingCreteriaDetails($userId, $criteraName);
                $criteriaDetailArr         =  $criteriaDetailResponse['Success'];    
               
                $userSimRingPersonal       =  ($criteriaDetailArr["blacklisted"]=="false" ? "Yes" : "No");
                $timeScheduleLevel         =  $criteriaDetailArr["timeScheduleLevel"];
                $timeScheduleName          =  $criteriaDetailArr["timeScheduleName"];
                $timeScheduleVal           =  $timeScheduleName." - ".$timeScheduleLevel;
                $holidayScheduleLevel      =  $criteriaDetailArr["holidayScheduleLevel"];
                $holidayScheduleName       =  $criteriaDetailArr["holidayScheduleName"];
                $holidayScheduleVal        =  $holidayScheduleName." - ".$holidayScheduleLevel;
                $fromDnCriteriaSelection   =  $criteriaDetailArr["fromDnCriteriaSelection"];
                $includeAnonymousCallers   =  $criteriaDetailArr["includeAnonymousCallers"];
                $includeUnavailableCallers =  $criteriaDetailArr["includeUnavailableCallers"];
                $phoneNumberList           =  implode(";", $criteriaDetailArr["phoneNumberList"]);
                
                $message .=                    
                            "=\"" . $criteraName . "\"," .
                            "=\"" . $userSimRingPersonal .  "\"," .
                            "=\"" . $timeScheduleVal .  "\"," .
                            "=\"" . $holidayScheduleVal .  "\"," .
                            "=\"" . $includeAnonymousCallers .  "\"," .
                            "=\"" . $includeUnavailableCallers .  "\"," .
                            "=\"" . $phoneNumberList . "\"\n";
                
        }        
        //echo $message; die;
        $fp = fopen("php://output", "a");
        fwrite($fp, $message);
        fclose($fp);
    }
?>
