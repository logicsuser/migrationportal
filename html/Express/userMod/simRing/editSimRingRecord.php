<div id='sim_ring_criteria_edit_dialog'> 
<?php 
echo "divBreak";
?>
    <div>

        <?php
    /**
         * HTML content of Sim Ring Criteria Show Data 
         * Created by Sollogics Developer.
         * Date: 15 Nov 2018
    */
    
    
    
    require_once("/var/www/html/Express/config.php");
    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();
    require_once("/var/www/lib/broadsoft/adminPortal/getAllSchedules.php");
    
    require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/simRingCriteriaManagement.php");
    $objSimRingMgmt   = new SimRingCriteriaManagement();
    $noOfEffectedDevice = array();
    
    $_SESSION ["criteriaEditNames"] = array (
            "simRingCriteriaName" => "Criteria Name",
            "useSimRingPersonal" => "Simultaneous Ring Personal",
            "simRingCriteriaTime" => "Time Schedule",
            "simRingCriteriaHoliday" => "Holiday Schedule",
            "simRingIncomingCallsFrom" => "Incoming Calls From",
            "simRingIncomingPhoeFromPrivate" => "Incoming Calls From any private number",
            "simRingIncomingPhoeFromUnavailable" => "Incoming Calls From any unavailable number",
            "criteriaIncomingPhone" => "Incoming phone number list"
    );    
    
    
    if($_POST['funcType']=="getModifyListSimRing"){
        
        $userId                    = $_SESSION["userInfo"]['userId'];
        $criteraName               = $_POST["criteriaNameVal"];
        $criteriaDetailResponse    = $objSimRingMgmt->getSimRingCreteriaDetails($userId, $criteraName);
        $criteriaDetailArr         = $criteriaDetailResponse['Success'];
                
        if(!empty($criteriaDetailArr)){
            //foreach($_SESSION["userInfo"]["criteriaTableList"] as $value){
                foreach($_SESSION["userInfo"]["crtiaNameListInSession"] as $value){                
                if($value['criteriaName'] == $criteraName){
                    
                    $tempOldCriteriaArr = array();
                    
                    /*$timeScheduleLevel         =  $criteriaDetailArr["timeScheduleLevel"];
                    $timeScheduleName          =  $criteriaDetailArr["timeScheduleName"];
                    $holidayScheduleLevel      =  $criteriaDetailArr["holidayScheduleLevel"];
                    $holidayScheduleName       =  $criteriaDetailArr["holidayScheduleName"];*/
                    
                    
                    $timeScheduleLevel         =  $value["timeScheduleLevel"];
                    $timeScheduleName          =  $value["timeScheduleName"];
                    $holidayScheduleLevel      =  $value["holidayScheduleLevel"];
                    $holidayScheduleName       =  $value["holidayScheduleName"];
                    $userSimRingPersonal       =  $value["useSimRingPersonal"];
                    $fromDnCriteriaSelection   =  $value["simRingIncomingCallsFrom"];
                    $includeAnonymousCallers   =  $value["simRingIncomingPhoeFromPrivate"];
                    $includeUnavailableCallers =  $value["simRingIncomingPhoeFromUnavailable"];
                    $phoneNumberList           =  $value["criteriaIncomingPhone"];
                    
                    if($timeScheduleLevel==""){
                        $timeScheduleLevel       =  $criteriaDetailArr["timeScheduleLevel"];
                    }
                    if($timeScheduleName==""){
                        $timeScheduleName        =  $criteriaDetailArr["timeScheduleName"];
                    }
                    if($holidayScheduleLevel==""){
                        $holidayScheduleLevel     =  $criteriaDetailArr["holidayScheduleLevel"];
                    }
                    if($holidayScheduleName==""){
                        $holidayScheduleName      =  $criteriaDetailArr["holidayScheduleName"];
                    }
                    
                    if($userSimRingPersonal==""){
                        $userSimRingPersonal      =  $criteriaDetailArr["blacklisted"];
                    }
                    
                    if($fromDnCriteriaSelection==""){
                        $fromDnCriteriaSelection   =  $criteriaDetailArr["fromDnCriteriaSelection"];
                    }
                    
                    if($includeAnonymousCallers==""){
                        $includeAnonymousCallers   =  $criteriaDetailArr["includeAnonymousCallers"];
                    }
                    
                    if($includeUnavailableCallers==""){
                        $includeUnavailableCallers =  $criteriaDetailArr["includeUnavailableCallers"];
                    }
                    
                    if(empty($phoneNumberList)){
                        $phoneNumberList           =  $criteriaDetailArr["phoneNumberList"];
                    }
                    
                    
                    
                    

                    $tempOldCriteriaArr['isActive']                              = $criteriaActiveStatus      =  $_SESSION["userInfo"]['simRingCriteriaDetail']["isActive"]                            = $value['isActive'];
                    $tempOldCriteriaArr['criteriaName']                          = $criteriaName              =  $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingCriteriaName"]                 = $value["criteriaName"];
                    $tempOldCriteriaArr['useSimRingPersonal']                    = $userSimRingPersonal       =  $_SESSION["userInfo"]['simRingCriteriaDetail']["useSimRingPersonal"]                  = $userSimRingPersonal;
                    $tempOldCriteriaArr['simRingCriteriaTime']                   = $timeScheduleVal           =  $timeScheduleName." - ".$timeScheduleLevel;
                    $tempOldCriteriaArr['simRingCriteriaHoliday']                = $holidayScheduleVal        =  $holidayScheduleName." - ".$holidayScheduleLevel;
                    $tempOldCriteriaArr['simRingIncomingCallsFrom']              = $fromDnCriteriaSelection   =  $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingIncomingCallsFrom"]            = $fromDnCriteriaSelection;
                    $tempOldCriteriaArr['simRingIncomingPhoeFromPrivate']        = $includeAnonymousCallers   =  $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingIncomingPhoeFromPrivate"]      = $includeAnonymousCallers;
                    $tempOldCriteriaArr['simRingIncomingPhoeFromUnavailable']    = $includeUnavailableCallers =  $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingIncomingPhoeFromUnavailable"]  = $includeUnavailableCallers;
                    $tempOldCriteriaArr['criteriaIncomingPhone']                 = $phoneNumberList           =  $_SESSION["userInfo"]['simRingCriteriaDetail']["criteriaIncomingPhone"]               = $phoneNumberList;
                    
                    //Code added @ 06 Dec 2018
                    if($timeScheduleName!=""){
                        $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingCriteriaTime"] = $timeScheduleName." - ".$timeScheduleLevel;
                    }else{
                        $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingCriteriaTime"] = "";
                    }
                    if($holidayScheduleName!=""){
                        $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingCriteriaHoliday"] = $holidayScheduleName." - ".$holidayScheduleLevel;
                    }else{
                        $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingCriteriaHoliday"] = "";
                    }
                    //End code
                    
                    
                    $_SESSION["simRingCriteriaList"][$criteriaName] = $tempOldCriteriaArr;
                }
            }
        }
        
        if(empty($criteriaDetailArr)){
            foreach($_SESSION["userInfo"]["crtiaNameListInSession"] as $rowSimRingCritera){
                
                if($rowSimRingCritera['criteriaName'] == $criteraName){
                    $tempOldCriteriaArr = array();
                    
                    $timeScheduleLevel         =  $rowSimRingCritera["timeScheduleLevel"];
                    $timeScheduleName          =  $rowSimRingCritera["timeScheduleName"];
                    $holidayScheduleLevel      =  $rowSimRingCritera["holidayScheduleLevel"];
                    $holidayScheduleName       =  $rowSimRingCritera["holidayScheduleName"];

                    $tempOldCriteriaArr['isActive']                              = $criteriaActiveStatus      =  $_SESSION["userInfo"]['simRingCriteriaDetail']["isActive"]                            = $rowSimRingCritera['isActive'];
                    $tempOldCriteriaArr['criteriaName']                          = $criteriaName              =  $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingCriteriaName"]                 = $rowSimRingCritera["criteriaName"]; 
                    $tempOldCriteriaArr['useSimRingPersonal']                    = $userSimRingPersonal       =  $_SESSION["userInfo"]['simRingCriteriaDetail']["useSimRingPersonal"]                  = $rowSimRingCritera["useSimRingPersonal"];
                    $tempOldCriteriaArr['simRingCriteriaTime']                   = $timeScheduleVal           =  $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingCriteriaTime"]                 = $timeScheduleName." - ".$timeScheduleLevel;
                    $tempOldCriteriaArr['simRingCriteriaHoliday']                = $holidayScheduleVal        =  $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingCriteriaHoliday"]              = $holidayScheduleName." - ".$holidayScheduleLevel;
                    $tempOldCriteriaArr['simRingIncomingCallsFrom']              = $fromDnCriteriaSelection   =  $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingIncomingCallsFrom"]            = $rowSimRingCritera["simRingIncomingCallsFrom"];
                    $tempOldCriteriaArr['simRingIncomingPhoeFromPrivate']        = $includeAnonymousCallers   =  $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingIncomingPhoeFromPrivate"]      = $rowSimRingCritera["simRingIncomingPhoeFromPrivate"];
                    $tempOldCriteriaArr['simRingIncomingPhoeFromUnavailable']    = $includeUnavailableCallers =  $_SESSION["userInfo"]['simRingCriteriaDetail']["simRingIncomingPhoeFromUnavailable"]  = $rowSimRingCritera["simRingIncomingPhoeFromUnavailable"];
                    $tempOldCriteriaArr['criteriaIncomingPhone']                 = $phoneNumberList           =  $_SESSION["userInfo"]['simRingCriteriaDetail']["criteriaIncomingPhone"]               = $rowSimRingCritera["criteriaIncomingPhone"];
                    
                    //echo "<pre>crtiaNameListInSession - ";
                    //print_r($rowSimRingCritera);                    
                    $_SESSION["simRingCriteriaList"][$criteriaName] = $tempOldCriteriaArr;
                }
            }
            
        }
   }
    
    const defaultTimeSchedule = "Every Day All Day";
    const defaultHolidaySchedule = "None";

    $mode = "update";
    
    function getScheduleOptions($type, $timeSchedule, $holidaySchedule) {
        global $schedules;

        $selection = $type == "Time" ? $timeSchedule : $holidaySchedule;
        $defaultSelection = $type == "Time" ? defaultTimeSchedule : defaultHolidaySchedule;

        // Build the list of options and mark one of them 'selected' based on selection stored in $_SESSION
        // <option value="Every Day All Day" selected>Every Day All Day</option>
        $str = "<option value=\"\"";
        $str .= $selection == $defaultSelection ? " selected" : "";
        $str .= ">" . $defaultSelection . "</option>";
        echo $str;

        foreach ($schedules as $key=>$value) {
            if ($value["level"] == "System") {
                $tmpLevel = "System";
                $level = " (System)";
            }
            elseif ($value["level"] == "Service Provider") {
                $tmpLevel = "Service Provider";
                $level = " (Enterprise)";
            }
            elseif ($value["level"] == "Group") {
                $tmpLevel = "Group";
                $level = " (Group)";
            }
            else { 
                $tmpLevel = "User";
                $level = " (User)";
                
            }

            if ($value["type"] == $type) {
                $criteriaVal = $value["name"]." - ".$tmpLevel;
                echo "<option value=\"" . $criteriaVal. "\"";
                echo $selection == $criteriaVal ? " selected" : "";
                echo ">" . $value["name"] . $level . "</option>";
            }
        }
    }
?>

 <form action="#" method="POST" name="sim_ring_criteria_delete_form" id="sim_ring_criteria_delete_form">
            <input type="hidden" name="funcType" value="deleteSimRingCriteria" />
            <input type="hidden" name="userId"       value="<?php echo $userId; ?>" />
            <input type="hidden" name="criteriaName" value="<?php echo $criteriaName; ?>" />
        </form>
    
          <form action="#" method="POST" name="sim_ring_criteria_edit_form" id="sim_ring_criteria_edit_form">
            <input type="hidden" name="funcType" value="updateSimRingCriteria" />
            <input type="hidden" name="simRingCriteriaActiveStatus" value="<?php echo $criteriaActiveStatus; ?>" />
            <input type="hidden" name="simRingOldCriteriaName" value="<?php echo $criteriaName; ?>" />

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="simRingCriteriaName" class="labelText"><span class="required" style="float: right;">*</span> Criteria name:</label>
                        <input class="diaBox12 form-control" type="text" name="simRingCriteriaName" id="simRingCriteriaName" value="<?php echo $criteriaName; ?>" size="35">
                    </div>
                </div>
            </div>

                <!-- Forwarding Options  -->
    <div class="row">        
        <div class="col-md-9 modalRightDiv">
            <div class="col-md-12">
                <input type="radio" style="vertical-align: middle"  name="useSimRingPersonal" id="useSimRingPersonalYes" value="false" <?php echo $userSimRingPersonal == "false" ? "checked" : ""; ?> ><label for="useSimRingPersonalYes"><span></span></label>
                <label class="labelText customLabelText">Use Simultaneous Ring Personal</label>
            </div>
            
            <div class="col-md-12">            	
                <input style="vertical-align: middle" type="radio" name="useSimRingPersonal" id="useSimRingPersonalNo" value="true" <?php echo $userSimRingPersonal == "true" ? "checked" : ""; ?> ><label for="useSimRingPersonalNo"><span></span></label>
                <label class="labelText customLabelText">Do not use Simultaneous Ring Personal</label>
            </div>
         </div>
    </div>
        <!-- Schedules  -->
        <div class="row">
            <div class="col-md-6 modalLeftDiv">
                <div class="form-group">
                    <label class="labelText">Time Schedule:</label>
                    <div class="dropdown-wrap">
                            <select  name="simRingCriteriaTime" id=simRingCriteriaTime class="diaBox12" style="width: 100% !important;">
                                <?php getScheduleOptions("Time", $timeScheduleVal, $holidayScheduleVal); ?>
                            </select>
                    </div>
                </div>
            </div>
             <div class="col-md-6 modalLeftDiv">
                <div class="form-group">
                    <label for="cfsCriteriaFwdDefault" class="labelText">Holiday Schedule:</label>
                    <div class="dropdown-wrap">
                            <select class="diaBox12" name="simRingCriteriaHoliday" id="simRingCriteriaHoliday" style="width: 100% !important;">
                                <?php getScheduleOptions("Holiday", $timeScheduleVal, $holidayScheduleVal); ?>
                            </select>
                    </div>
                </div>
            </div>
            </div>
            <!-- Forwarding incoming calls  -->
            <div class="row">
                <div class="col-md-3 modalLeftDiv">
                        <div class="form-group">
                        <label for="cfsCriteriaFwdFromAny" class="labelText">Incoming calls:</label>
                        </div>
                </div>
                <div class="col-md-9 modalRightDiv">
                    <div class="">
                      <input style="vertical-align: middle" type="radio" name="simRingIncomingCallsFrom" id="simRingIncomingCallsFromAnyChk" value="Any" <?php echo $fromDnCriteriaSelection == "Any" ? "checked" : ""; ?> ><label for="simRingIncomingCallsFromAnyChk"><span></span></label>
                      <label class="labelText customLabelText">From any phone number</label>
                    </div>

                    <div class="">

                        <input style="vertical-align: middle" type="radio" name="simRingIncomingCallsFrom" id="simRingIncomingCallsFromChk" value="Specified Only" <?php echo $fromDnCriteriaSelection == "Specified Only" ? "checked" : ""; ?>  ><label for="simRingIncomingCallsFromChk"><span></span></label>
                        <label class="labelText customLabelText">From the following phone numbers:</label>

                            <div class="chkFromAny">
                             <input style="vertical-align: middle" type="checkbox" name="simRingIncomingPhoeFromPrivate" id="simRingIncomingPhoeFromPrivate1" value="true" <?php echo $includeAnonymousCallers == "true" ? "checked" : ""; ?>  ><label for="simRingIncomingPhoeFromPrivate1"><span></span></label>
                             <label class="labelText">From any private number</label>
                            </div>
                            <div class="chkFromAny"> 
                            <input style="vertical-align: middle" type="checkbox" name="simRingIncomingPhoeFromUnavailable" id="simRingIncomingPhoeFromUnavailableChk" value="true"  <?php echo $includeUnavailableCallers == "true" ? "checked" : ""; ?> ><label for="simRingIncomingPhoeFromUnavailableChk"><span></span></label>
                            <label class="labelText">From any unavailable number</label></div>
                    </div>   

                </div>      
            </div>
            <div class="row">
            <div class="col-md-12"><label class="labelText">From specific phone numbers:</label></div>
            <?php 
                    //$phoneNumberList
                    for($x=0; $x < 12; $x++)
                    {
                         $phoneNumber = "";
                        $phoneNumber = $phoneNumberList[$x];
                        
                        echo "<div class='col-md-3 inputTextDiv' ><input class='diaBox12' style='width:100%;' type='text' name='criteriaIncomingPhone[$x]' id='criteriaIncomingPhone[$x]' value='$phoneNumber' ></div>";
                    }
            ?>      
            </div>
            
        </form>  
        </div> 
<?php 
echo "divBreak";
?>
</div> 