<?php
require_once ("/var/www/html/Express/config.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");
require_once ("/var/www/lib/broadsoft/adminPortal/arrays.php");

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");

    $data = $errorTableHeader;	
    $error = 0;
    $changes = 0; 
     
    for($x=0; $x<12; $x++)
    {
        if($_POST["criteriaIncomingPhone"][$x]==""){
            unset($_POST["criteriaIncomingPhone"][$x]);
        }
    }
    
    if($_POST['simRingIncomingCallsFrom']!="Specified Only"){
        if(empty($_POST['criteriaIncomingPhone'])){
            unset($_POST['criteriaIncomingPhone']);
        }
    }
        
    $nonRequired = array("useSimRingPersonal", "simRingCriteriaTime", "simRingCriteriaHoliday", 
        "simRingIncomingCallsFrom", "simRingIncomingPhoeFromPrivate", "simRingIncomingPhoeFromUnavailable"
       );
    
    unset($_POST["funcType"]);
    unset($_POST["simRingOldCriteriaName"]);
    unset($_POST["simRingCriteriaActiveStatus"]);
    
    foreach ($_POST as $key => $value)
    {               
               $bg = UNCHANGED;
               //Comparing Criteria old values with new values 
               foreach($_SESSION["userInfo"]['simRingCriteriaDetail'] as $sessKey => $sessValue){
                    if($key == $sessKey){
                        if($value <> $sessValue){ 
                                $changes++;
                                $bg = CHANGED;
                        }
                    }
                }
                //End code
                
                if (!in_array($key, $nonRequired) and $value == "")
		{
			$error = 1;
			$bg = INVALID;
			$value = $_SESSION["criteriaEditNames"][$key] . " is a required field.";
		}
        
                if($key=="criteriaIncomingPhone" && $_POST['simRingIncomingCallsFrom']=="Specified Only"){
                    if(empty($_POST['criteriaIncomingPhone'])){
                        $error = 1;
			$bg = INVALID;
			$value = $_SESSION["criteriaEditNames"][$key] . " is a required field.";
                    }
                }
                
                if($key=="criteriaIncomingPhone" && !empty($_POST['criteriaIncomingPhone'])){
                            foreach($_POST['criteriaIncomingPhone'] as $key1 => $val1){
                                if (!preg_match("/^[0-9-]+$/", $val1)){                                    
                                    $error = 1;
                                    $bg = INVALID;
                                }
                            }
                            
                            //start code for check array if has duplicate values                        
                            $simRingCriteriaPhoneArr = array();
                            function array_has_dupes($simRingCriteriaPhoneArr) {
                                return count($simRingCriteriaPhoneArr) !== count(array_unique($simRingCriteriaPhoneArr));
                            }
                            for ($m = 0; $m < count($_POST['criteriaIncomingPhone']); $m++)
                            {
                                if($_POST["criteriaIncomingPhone"][$m]!=""){
                                    $simRingCriteriaPhoneArr[] = $_POST["criteriaIncomingPhone"][$m];
                                }
                            }
                            if(array_has_dupes($simRingCriteriaPhoneArr)=="1"){
                                $error = 1;
                                $bg = INVALID;
                                $value[] = "Phone number already exists.";
                            }
                            //End code
                            
                            
                }
                
                if ($key == "simRingCriteriaName" and $value != "" and $value!=$_SESSION["userInfo"]['simRingCriteriaDetail']["simRingCriteriaName"])
		{
                        if(in_array($_POST["simRingCriteriaName"], $_SESSION["userInfo"]["crtiaNameList"]))
                        {
                            $error = 1;
                            $bg = INVALID;                   
                            $value = "Criteria Name <".$_POST["simRingCriteriaName"]."> already exists.";
                        }
		}
        
		if ($bg != UNCHANGED)
		{                        
                        //if(!($key=="simRingIncomingCallsFrom" && $_POST['simRingIncomingCallsFrom']=="Specified Only")){
                        $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["criteriaEditNames"][$key] . "</td><td class=\"errorTableRows\">";
                        //}
                        
                        if($key=="criteriaIncomingPhone" && !empty($_POST['criteriaIncomingPhone'])){
                            foreach($_POST['criteriaIncomingPhone'] as $key1 => $val1){
                                if (!preg_match("/^[0-9-]+$/", $val1)){                                    
                                    $value[$key1] =  "$val1 - Invalid phone number";
                                }else{
                                    $value[$key1] =  $val1;
                                }
                            }
                        }
                        
                        if (is_array($value))
			{   
                            foreach ($value as $k => $v)
                            {
                                    $data .= $v . "<br>";
                            }
			}else if($value !== "")
			{     
                                
                                $value = str_replace("Service Provider", "Enterprise", $value);
                                $data .= $value;

			}
			else
			{
				$data .= "None";
			}
			$data .= "</td></tr>";
		}
	}
	$data .= "</table>";
        if ($changes == 0)
        {
            $error = 1;
            $data = "You have made no changes.";
        }
	echo $error . $data;

?>