<?php
    /**
     * HTML content of Sim Ring Criteria Show Data 
     * Created by Sollogics.
     * Date: 15 Nov 2018
     */
    require_once("/var/www/html/Express/config.php");
    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();
    require_once("/var/www/lib/broadsoft/adminPortal/getAllSchedules.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/simRingCriteriaManagement.php");
    $objSimRingMgmt   = new SimRingCriteriaManagement();
    $noOfEffectedDevice = array() ;
    if($_POST['funcType'] =="getModifyListSimRing"){
        
        $userId = $_SESSION["userInfo"]['userId'];
        $criteraName = $_POST["criteriaName"];
        $criteriaDetailResponse   = $objSimRingMgmt->updateGetSimRingCreteriaList($userId, $criteraName);
        $criteriaDetailArr        = $criteriaDetailResponse['Success'];
        
        $fromDnCriteriaSelection   =  $criteriaDetailArr["fromDnCriteriaSelection"];
        $includeAnonymousCallers   =  $criteriaDetailArr["includeAnonymousCallers"];
        $includeUnavailableCallers =  $criteriaDetailArr["includeUnavailableCallers"];
        $phoneNumberList           =  $criteriaDetailArr["phoneNumberList"];
        
        foreach($_SESSION["userInfo"]["criteriaTableList"] as $value){
            if($value['criteriaName'] == $criteraName){                
                $criteriaName              =  $_SESSION["userInfo"]['simRingCriteriaDetail']["criteriaName"]              = $value["criteriaName"];
                $timeSchedule              =  $_SESSION["userInfo"]['simRingCriteriaDetail']["timeSchedule"]              = $value["timeSchedule"];
                $holidaySchedule           =  $_SESSION["userInfo"]['simRingCriteriaDetail']["holidaySchedule"]           = $value["holidaySchedule"];
                $fromDnCriteriaSelection   =  $_SESSION["userInfo"]['simRingCriteriaDetail']["fromDnCriteriaSelection"]   = $criteriaDetailArr["fromDnCriteriaSelection"];
                $includeAnonymousCallers   =  $_SESSION["userInfo"]['simRingCriteriaDetail']["includeAnonymousCallers"]   = $criteriaDetailArr["includeAnonymousCallers"];
                $includeUnavailableCallers =  $_SESSION["userInfo"]['simRingCriteriaDetail']["includeUnavailableCallers"] = $criteriaDetailArr["includeUnavailableCallers"];
                $phoneNumberList           =  $_SESSION["userInfo"]['simRingCriteriaDetail']["phoneNumberList"]           = $criteriaDetailArr["phoneNumberList"];
            }
        }
   }
    
    const defaultTimeSchedule = "Every Day All Day";
    const defaultHolidaySchedule = "None";

    $mode = "update";
    
    function getScheduleOptions($type, $timeSchedule, $holidaySchedule) {
        global $schedules;

        $selection = $type == "Time" ? $timeSchedule : $holidaySchedule;
        $defaultSelection = $type == "Time" ? defaultTimeSchedule : defaultHolidaySchedule;

        // Build the list of options and mark one of them 'selected' based on selection stored in $_SESSION
        // <option value="Every Day All Day" selected>Every Day All Day</option>
        $str = "<option value=\"\"";
        $str .= $selection == $defaultSelection ? " selected" : "";
        $str .= ">" . $defaultSelection . "</option>";
        echo $str;

        foreach ($schedules as $key=>$value) {
            if ($value["level"] == "System") {
                $tmpLevel = "System";
                $level = " (System)";
            }
            elseif ($value["level"] == "Service Provider") {
                $tmpLevel = "Enterprise";
                $level = " (Enterprise)";
            }
            elseif ($value["level"] == "Group") {
                $tmpLevel = "Group";
                $level = " (Group)";
            }
            else { 
                $tmpLevel = "User";
                $level = " (User)";
                
            }

            if ($value["type"] == $type) {
                echo "<option value=\"" . $value["name"]." - ".$tmpLevel. "\"";
                echo $selection == $value["name"] ? " selected" : "";
                echo ">" . $value["name"] . $level . "</option>";
            }
        }
    }
?>
<div style="background-color: lightgoldenrodyellow; font-size: 12px;">
    <form action="#" method="POST" name="newCFSCriteriaForm" id="newCFSCriteriaForm_Basic" class="fcorn-register container">
        <!-- Criteria name  -->
        <div class="diaPL diaP12" style="width: 12%"><label for="cfsCriteriaName_Basic"><?php echo $mode == "create" ? "<span class=\"required\">*</span> " : ""; ?>Criteria name:</label></div>
        <div class="diaPN diaP12"><input class="diaBox12" type="text" name="cfsCriteriaName" id="cfsCriteriaName_Basic" value="<?php echo $criteriaName; ?>" size="35" onchange="evalCriteriaName(this)" oninput="evalCriteriaName(this)" <?php echo $mode == "update" ? "disabled" : ""; ?> ></div>
        <?php if ($mode == "update") { ?>
            <input type="hidden" name="cfsCriteriaNameUpdate" value="<?php echo $criteriaName; ?>" >
        <?php } ?>

                <!-- Forwarding Options  -->
        <div class="diaPL diaP18" style="width: 12%"><label for="cfsCriteriaFwdDefault">Forwarding</label></div>
        <div class="diaPN diaP18" style="width: 2%"><input style="margin-top: 2px; vertical-align: middle" type="radio" name="cfsCriteriaFwd" id="cfsCriteriaFwdDefault" value="Forward To Default Number" <?php echo $cfsCriteriaFwd == "Forward To Default Number" ? "checked" : ""; ?> /></div>
        <div class="diaPN diaP18"><label>Use Default Forward phone number / SIP-URI</label></div>

        <div class="diaPL diaP3" style="width: 12%"><label for="cfsCriteriaFwdSpecified">Options:</label></div>
        <div class="diaPN diaP3" style="width: 2%"><input style="margin-top: 2px; vertical-align: middle" type="radio" name="cfsCriteriaFwd" id="cfsCriteriaFwdSpecified" value="Forward To Specified Number" <?php echo $cfsCriteriaFwd == "Forward To Specified Number" ? "checked" : ""; ?> /></div>
        <div class="diaPN diaP3" style="width: 30%"><label>Forward to another phone number / SIP-URI:</label></div>
        <div class="diaPN diaP3"><input class="diaBox12" type="text" name="cfsAnotherFwdNum" id="cfsAnotherFwdNum" value="<?php echo $cfsAnotherFwdNum; ?>" size="35"></div>

        <div class="diaPL diaP3" style="width: 12%"><label for="cfsCriteriaFwdNone">&nbsp;</label></div>
        <div class="diaPN diaP3" style="width: 2%"><input style="margin-top: 2px; vertical-align: middle" type="radio" name="cfsCriteriaFwd" id="cfsCriteriaFwdNone" value="Do not forward" <?php echo $cfsCriteriaFwd == "Do not forward" ? "checked" : ""; ?> /></div>
        <div class="diaPN diaP3"><label>Do not forward</label></div>

        <!-- Schedules  -->
        <div class="diaPL diaP18" style="width: 12%"><label>Schedules</label></div>
        <div class="diaPL diaP6" style="text-align: right; margin-right: 3px; width: 12%"><label for="cfsCriteriaFwdDefault">Time Schedule:</label></div>
        <div class="diaPN diaP6"><select class="diaBox12" name="cfsCriteriaTime" id="cfsCriteriaTime">
                <?php getScheduleOptions("Time"); ?>
        </select></div>
        <div class="diaPL diaP6" style="text-align: right; margin-right: 3px; width: 12%"><label for="cfsCriteriaFwdDefault">Holiday Schedule:</label></div>
        <div class="diaPN diaP6"><select class="diaBox12" name="cfsCriteriaHoliday" id="cfsCriteriaHoliday">
                <?php getScheduleOptions("Holiday"); ?>
        </select></div>

        <!-- Forwarding incoming calls  -->
        <div class="diaPL diaP18" style="width: 12%"><label for="cfsCriteriaFwdFromAny">Forwarding</label></div>
        <div class="diaPN diaP18" style="width: 2%"><input style="margin-top: 2px; vertical-align: middle" type="radio" name="cfsCriteriaFwdFrom" id="cfsCriteriaFwdFromAny" value="Any" <?php echo $cfsCriteriaFwdFrom == "Any" ? "checked" : ""; ?> ></div>
        <div class="diaPN diaP18"><label>From any phone number</label></div>

        <div class="diaPL diaP3" style="width: 12%"><label for="cfsCriteriaFwdFromSpecified">incoming calls:</label></div>
        <div class="diaPN diaP3" style="width: 2%"><input style="margin-top: 2px; vertical-align: middle" type="radio" name="cfsCriteriaFwdFrom" id="cfsCriteriaFwdFromSpecified" value="Specified Only" <?php echo $cfsCriteriaFwdFrom == "Specified Only" ? "checked" : ""; ?> ></div>
        <div class="diaPN diaP3" style="width: 30%"><label>From the following phone numbers:</label></div>

        <div class="diaPL diaP6" style="width: 14%"><label for="cfsCriteriaFwdFromPrivate">&nbsp;</label></div>
        <div class="diaPN diaP6" style="width: 2%"><input style="margin-top: 2px; vertical-align: middle" type="checkbox" name="cfsCriteriaFwdFromPrivate" id="cfsCriteriaFwdFromPrivate" value="true" <?php echo $cfsCriteriaFwdFromPrivate == "true" ? "checked" : ""; ?> ></div>
        <div class="diaPN diaP6" style="width: 30%"><label style="margin-top: 1px">From any private number</label></div>

        <div class="diaPL diaP6" style="width: 14%"><label for="cfsCriteriaFwdFromUnavailable">&nbsp;</label></div>
        <div class="diaPN diaP6" style="width: 2%"><input style="margin-top: 2px; vertical-align: middle" type="checkbox" name="cfsCriteriaFwdFromUnavailable" id="cfsCriteriaFwdFromUnavailable" value="true" <?php echo $cfsCriteriaFwdFromUnavailable == "true" ? "checked" : ""; ?> ></div>
        <div class="diaPN diaP6" style="width: 30%"><label style="margin-top: 1px">From any unavailable number</label></div>

        <div class="diaPL diaP6" style="width: 14%"><label>&nbsp;</label></div>
        <div class="diaPN diaP6" style="width: 30%"><label>From specific phone numbers:</label></div>

        <div class="diaPL diaP6" style="width: 14%"><label>&nbsp;</label></div>
        
        <?php 
            //$phoneNumberList
            for($x=0; $x < 12; $x++)
            {
                if($x%4==0){
                    echo "<div class='diaPL diaP6' style='width: 14%'><label>&nbsp;</label></div>";
                }
                $phoneNumber = "";
                $phoneNumber = $phoneNumberList[$x];
                echo "<div class='diaPN diaP6'>
                    <input class='diaBox12' type='text' name='criteriaIncPhone_$x' id='criteriaIncPhone_$x' value='$phoneNumber' size='25'></div>
                <div class='diaPN diaP6' style='width: 1%'><label>&nbsp;</label></div>";
            }
        ?>
    </form>
</div>
