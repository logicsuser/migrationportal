<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/customTagManagement.php");

if(!empty($_POST["deviceType"])){
	$availableTagsArray = array();
	$info = new CustomTagManagement();
	$tagList = $info->getTagList($_POST["deviceType"]);
	uasort($tagList, function($a, $b) {
		return strnatcmp($a['shortDescription'], $b['shortDescription']);
	});
	//$response = $info->getUserCustomTags($_SESSION["userInfo"]["deviceName"]);
	$responseJson = "";
		if(count($tagList)>0 ){
			foreach($tagList as $key=>$val){
			    if( ! isset($_SESSION['assignCustomTagToUserForTable'][$val['tagName']]) ){
					$availableTagsArray[] = $val;
				}
			}
		}
		
		$responseJson = "0".json_encode($availableTagsArray);
	
	echo $responseJson;
}

?>

