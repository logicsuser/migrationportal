<?php
/**
 * Created by Karl.
 * Date: 11/12/2016
 */

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();

    require_once("/var/www/lib/broadsoft/adminPortal/getDeviceList.php");
    require_once("/var/www/lib/broadsoft/adminPortal/getDevicesR19.php");
    require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/DeviceNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");

    const customProfileTable = "customProfiles";
    const customProfileName  = "customProfileName";
    const customProfileType  = "deviceType";

    function getUserAssignedPort($sp, $grpId, $deviceName, $userId) {
       
        $port = "-1";
        $vop =  new VdmOperations();
        $returnResponse = $vop->getAllUserOfDevice($sp, $grpId, $deviceName);
        if(empty($returnResponse["Error"])){
            $userDetails = $returnResponse["Success"];
            foreach($userDetails as $index=>$details) {
                if($userId == $details['userId']) {
                    $port = $details['order'];
                    break;
                }
            }
        }else{
            $port = "-1";
        }
        $_SESSION["userInfo"]["portNumber"] = $port;
        return $port;
    }
   
    function deviceExists($device) {
        global $devices;

        foreach ($devices as $key => $value) {
            if ($value["deviceName"] == $device) {
                return true;
            }
        }
        return false;
    }

    function getDeviceTypeNumberOfPorts($deviceType) {
        global $staticLineOrderingDeviceTypes;

        foreach ($staticLineOrderingDeviceTypes as $key => $value) {
            if ($value["deviceType"] == $deviceType) {
                return $value["numberOfPorts"];
            }
        }
    }


    function resetDevices($deviceName) {
        global $devices;
        foreach($devices as $deviceKey => $deviceVal) {
            if($deviceVal["deviceName"] == $deviceName) {
                unset($devices[$deviceKey]);
                return;
            }
        }
    }
    
    function getDevicePortUsage($deviceName) {
        global $ociVersion, $sessionid, $client;
        $portDetails = array();
        $ociCmd = "GroupAccessDeviceGetRequest18sp1";
        $xmlinput = xmlHeader($sessionid, $ociCmd);
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $portDetails["numberOfPorts"]         = isset($xml->command->numberOfPorts->quantity) ? strval($xml->command->numberOfPorts->quantity) : 1000;
        $portDetails["numberOfAssignedPorts"] = strval($xml->command->numberOfAssignedPorts);
        
        return $portDetails;
    }

    function getDeviceAvailablePorts($deviceName) {
        global $sessionid, $client;

        $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceAvailablePortGetListRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<accessDevice>";
        $xmlinput .=    "<deviceLevel>Group</deviceLevel>";
        $xmlinput .=    "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= "</accessDevice>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $a = 0;
        $availablePorts = array();
        foreach ($xml->command->portNumber as $key => $value) {
            $availablePorts[$a++] = strval($value);
        }

        return $availablePorts;
    }

    function getResourceForDeviceNameCreationData($attr, $index="0") {
        switch ($attr) {
            //TODO: Revise rules for Device Name Creation to remove commented out portions through the entire Express
            //case ResourceNameBuilder::DN:               return $_POST["phoneNumber"] != "" ? $_POST["phoneNumber"] : "";
            //case ResourceNameBuilder::EXT:              return $_POST["extension"];
            case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:           return $_SESSION["groupId"];
            //case ResourceNameBuilder::FIRST_NAME:       return $_POST["firstName"];
            //case ResourceNameBuilder::LAST_NAME:        return $_POST["lastName"];
            case ResourceNameBuilder::DEV_TYPE:         return $_SESSION["userInfo"]["deviceType"];;
            case ResourceNameBuilder::MAC_ADDR:         return $_SESSION["userInfo"]["macAddress"];
            case ResourceNameBuilder::INT_1:
            case ResourceNameBuilder::INT_2:            return $index;
            default: return "";
        }
    }

    function getDeviceName($index) {
        global $deviceNameAnalog;

        // create device builder... since this is for analog devices, force resolve is always set to true.
        $deviceNameBuilder = new DeviceNameBuilder($deviceNameAnalog,
            getResourceForDeviceNameCreationData(ResourceNameBuilder::DN),
            getResourceForDeviceNameCreationData(ResourceNameBuilder::EXT),
            getResourceForDeviceNameCreationData(ResourceNameBuilder::GRP_DN),
            true);

        if (! $deviceNameBuilder->validate()) {
            // TODO: Implement resolution when Device name input formula has errors
        }

        do {
            $attr = $deviceNameBuilder->getRequiredAttributeKey();
            $deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreationData($attr, $index));
        } while ($attr != "");

        return $deviceNameBuilder->getName();
    }

    function buildDeviceIndexSelection($userDeviceName, $deviceNameResources) {
        global $numSIPGatewayInstances;
        
        $str  = "<select name=\"deviceIndex\" id=\"deviceIndex\" onchange=\"processPortNumbers(this)\">";
        $str .= "<option value=\"\"></option>";     // empty row
        for ($i = 1; $i <= $numSIPGatewayInstances; $i++) {
            $_SESSION["devicePortUsage"]["numberOfPorts"] = 0;
            
            $deviceName = getDeviceName($i);
            $selected = "";
            if($deviceName == $userDeviceName) {
                $selected = "selected";
                $_SESSION["userInfo"]["deviceIndex"] = $i;
            }
            
            $portDetails = array();
            $portDetails["numberOfPorts"] = 0;
            if (isset($deviceName) && deviceExists($deviceName)) {
                $portDetails = getDevicePortUsage($deviceName);
            }
           
            $style = "";
            $disabled = "";
            $numberOfPorts = $portDetails["numberOfPorts"];
            $availablePorts = $numberOfPorts == 0 ? 0 : $numberOfPorts - $portDetails["numberOfAssignedPorts"];
            
            $displayInfo  = $i . ":  " . $deviceName . "   (";
            
            if ($numberOfPorts == 0) {
                $displayInfo .= "new device";
            }
            elseif ($availablePorts == 0) {
                $displayInfo .= "No ports available";
                $disabled = " disabled";
                $style = "style=\"color: gray\"";
            }
            else {
                $displayInfo .= "available " . $availablePorts;
                $displayInfo .= $availablePorts == 1 ? " port" : " ports";
            }
            
            $displayInfo .= ")";
            if($selected == "selected") {
                $_SESSION["userInfo"]["deviceIndexText"] = $displayInfo;
            }
            
            $str .= "<option " . $style . " value=\"" . $i . "\"" . $disabled . " " .$selected. ">" . $displayInfo . "</option>";
        }
        
        $str .= "</select>";
        return $str;
    }

    function buildPortSelection($userDevicePort, $deviceType, $deviceName) {
        $availablePorts = array();
        $numberOfPorts = getDeviceTypeNumberOfPorts($deviceType);
        $deviceDoesNotExist = ! deviceExists($deviceName);
        if (deviceExists($deviceName)) {
            $availablePorts = getDeviceAvailablePorts($deviceName);
        }
        $str  = "<select name=\"portNumber\" id=\"portNumber\">";
        $str .= "<option value=\"\"></option>";     // empty row

        for ($i = 1; $i <= $numberOfPorts; $i++) {
            $selected = "";
            if($userDevicePort !="" && $userDevicePort != "-1"){
                $selected = $userDevicePort == $i ? "selected" : "";
                if($userDevicePort == $i) {
                    $_SESSION["userInfo"]["portNumberText"] = $i . ":  Port " . $i;
                }
            }
            // port is always available for non-existent (not yet created devices),
            // otherwise check whether port is on the list of available ports
            $portInUse = $deviceDoesNotExist ? false : ! in_array($i, $availablePorts);
            $style = $portInUse ? "style=\"color: gray\"" : "";
            $disabled = $portInUse ? " disabled" : "";
            
            $str .= "<option " . $style . " value=\"" . $i . "\"" . $disabled . " ".$selected.">" . $i . ":  Port " . $i . "</option>";
        }

        $str .= "</select>";
        return $str;

    }
    
    function buildPortSelectionSIP($userDevicePort, $deviceType, $deviceName) {
        $availablePorts = array();
        $numberOfPorts = getDeviceTypeNumberOfPorts($deviceType);
        $deviceDoesNotExist = ! deviceExists($deviceName);
        if (deviceExists($deviceName)) {
            $availablePorts = getDeviceAvailablePorts($deviceName);
        }
        $str  = "<select name=\"portNumberSIP\" id=\"portNumberSIP\">";
        $str .= "<option value=\"\"></option>";     // empty row
        for ($i = 1; $i <= $numberOfPorts; $i++) {
            $selected = "";
            if($userDevicePort !="" && $userDevicePort != "-1"){
                $selected = $userDevicePort == $i ? "selected" : "";
                if($userDevicePort == $i) {
                    $_SESSION["userInfo"]["portNumberText"] = $i . ":  Port " . $i;
                }
            }
            $portInUse = $deviceDoesNotExist ? false : ! in_array($i, $availablePorts);
            $style = $portInUse ? "style=\"color: gray\"" : "";
            $disabled = $portInUse ? " disabled" : "";
            $str .= "<option " . $style . " value=\"" . $i . "\"" . $disabled . " ".$selected.">" . $i . ":  Port " . $i . "</option>";
        }
        $str .= "</select>";
        return $str;
    }
    
    function buildCustomProfilesList() {
        $str  = "<select name=\"customProfile\" id=\"customProfile\">";
        $str .= "<option value=\"None\" selected>None</option>";

        $profiles = getCustomProfiles($_POST["deviceType"]);
        foreach ($profiles as $key => $profile) {
            $str .= "<option value=\"" . $profile . "\"";
            $str .= ">" . $profile . "</option>";
        }

        $str .= "</select>";
        return $str;
    }


    // --------------------------------
    // Ajax POST processing starts here
    // --------------------------------

?>
