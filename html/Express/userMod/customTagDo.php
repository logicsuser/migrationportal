<?php
/**
 * Created by Karl.
 * Date: 11/14/2016
 */

    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();

    require_once("/var/www/html/Express/userMod/UserCustomTagManager.php");
    require_once("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
    require_once("/var/www/lib/broadsoft/adminPortal/HTMLTable.php");
    require_once("/var/www/html/Express/userMod/CustomTagsLookupTable.php");
    require_once("/var/www/html/Express/userMod/UserModifyDBOperations.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/customTagManagement.php");
    
    const customProfileTable = "customProfiles";
    const customProfileName  = "customProfileName";
    const customProfileType  = "deviceType";

    function getCustomProfiles($deviceType) {
        
        /*global $db;

        $query = "select " . customProfileName . " from " . customProfileTable . " where " . customProfileType . "='" . $deviceType . "' order by " . customProfileName;
        $results = $db->query($query);
        $profiles = array();
        $i = 0;
        while ($row = $results->fetch()) {
            if ( ! in_array($profile = $row[customProfileName], $profiles)) {
                $profiles[$i++] = $profile;
            }
        }*/
        $profiles = array();
        $obj = new UserModifyDBOperations();
        $customProfiles = $obj->getAllCustomProfiles();
        if(in_array($deviceType, $customProfiles)){
            $profiles = $obj->getAllPhoneProfiles();
            return $profiles;
        }
        return $profiles;
    }

    function buildCustomProfileSelection($deviceType) {
        $str  = "<select name=\"customProfile\" id=\"customProfile\">";
       // $str .= "<option value=\"None\" selected></option>";

        if ($deviceType != "") {
            $profiles = getCustomProfiles($deviceType);
            foreach ($profiles as $key => $profile) {
                $str .= "<option value=\"" . $profile[1] . "\"";
                $str .= ">" . $profile[0] . "</option>";
            }
        }
        $str .= "</select>";
        return $str;
    }
    
    function buildTagBundleSelection($deviceType) {
        $obj = new UserModifyDBOperations();
        $customProfiles = $obj->getAllCustomProfiles();
        $str = "";
        
      //  $str = "<select name='modTagBundle[]' id='modTagBundle' size='3' multiple='' onchange='processTagBundleChange(this)'>";
        if(in_array($deviceType, $customProfiles)){
            $modBundleArr = $obj->getAllTagBundles();
            if(count($modBundleArr) > 0){  
            $showHideTagBundle ="block";
              $i = 0;
                foreach ($modBundleArr as $keyTB => $valueTB){
                    $bundleID = "modTagBundle".$i;
                   // $str .= "<option value = ".$valueTB.">".$valueTB."</option>";
                     $str .= "<div class='form-group'><input type='checkbox' name='modTagBundle[]' value=".$valueTB." id=".$bundleID."><label for=".$bundleID."><span></span></label><label class='labelText'> ".$valueTB."</label></div>";
                    $i++;
                }
            }
        }
     //  $str .= "</select>";
       return $str;
    }

    function getDeviceAnalogStatusFlag($deviceType) {
        global $db;

        // kludgy, but return '1' if device type is empty string
        if ($deviceType == "" ) { return "1"; }

        $sipGatewayLookup = new DBLookup($db, "systemDevices", "deviceType", "phoneType");
        return $sipGatewayLookup->get($deviceType) == "Analog" ? "1" : "0";
    }

    function createLookupTable($lookupType) {
        global $db;

        $customTagsLookupTable = new CustomTagsLookupTable($db, $lookupType);

        $head  = "<thead><tr>";
        $head .= $customTagsLookupTable->createTableHeader();
        $head .= "</tr></thead>";

        $body  = "<tbody>";
        $body .= $customTagsLookupTable->getData();
        $body .= "</tbody>";

        return $head . $body;
    }

    function addCustomTagToUsersDevice() {
        $customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
        if (($resp = $customTags->addCustomTag($_POST["tagName"], $_POST["tagValue"])) == "") {
            $resp = $customTags->formatCustomTagsTable();
            $error = false;
        }
    }
    
    function deleteCustomTag() {
        $customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
        if (($resp = $customTags->deleteCustomTag($_POST["deleteCustomTag"])) == "") {
            $resp = $customTags->formatCustomTagsTable();
            $error = false;
        }
    }
    
    function modifyCustomTag() {
        $customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
        if (($resp = $customTags->modifyCustomTag($_POST["modifyCustomTag"], $_POST["modifiedTag"])) == "") {
            $resp = $customTags->formatCustomTagsTable();
            $error = false;
        }
    }
    
    
    function removeGroupCustomTagsInAvailableCustomTag($customTagArray) {
        $customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
        
        $tempArr = array();
        foreach($customTagArray as $sKey => $sValue) {
            if( ! isset($tempArr[$sKey]) ) {
                $tempArr[$sKey] = $sValue;
            } else if( isset($tempArr[$sKey]) && isset($tempArr[$sKey]["groupLevel"]) && $tempArr[$sKey]["groupLevel"] == "Yes" ) {
                unset($tempArr[$sKey]);
                $tempArr[$sKey] = $sValue;
            }
            
        }
        $tempArr = $customTags->pushGroupLevelCustomTag($tempArr);
        return $tempArr;
    }
    
    
    function formCustomTagsTableAfterChanges() {
        $customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
        if(isset($_SESSION['assignCustomTagToUserForTable'])) {
//             $htmlTable = "";
            $assingedUniqueArr = removeGroupCustomTagsInAvailableCustomTag($_SESSION['assignCustomTagToUserForTable']);
            $customTags->numberOfTags = count($_SESSION['assignCustomTagToUserForTable']);
            $htmlTable = $customTags->createUserCustomTagTable($assingedUniqueArr);
            
            /*
             foreach($assingedUniqueArr as $sKey => $sValue) {
                if(isset($sValue['groupLevel']) && $sValue['groupLevel'] == "Yes") {
                    $customTagType = "Group";
                } else {
                    $customTagType = $customTags->checkCustomTagType($sValue['customTagName'], $sValue['customTagValue']);
                }
                $pointerEvents = $customTagType == "Group" ? "pointer-events: none;" : "";
                $tagDesc = ltrim(rtrim($sValue['customTagShortDesc'], "%"), "%");
                
                $valueId = "userCustomTagValue".$sValue['customTagName'];
                $htmlTable .= "<tr data-tagType='". $customTagType ."' style='". $pointerEvents ."' title='".$sValue['customTagLongDesc']."' id=\"" . $sValue['customTagName']. "\" onmouseover=\"highlightRow(this, true)\" onmouseout=\"highlightRow(this, false)\" onclick=\"evalRow(this)\">";
                $htmlTable .= "<td style='' >" . $tagDesc . "</td>";
                $htmlTable .= "<td style='' >" . $customTagType. "</td>";
                $htmlTable .= "<td id=\"" . $valueId . "\" style='' value=".$sValue['customTagValue']." >" . $sValue['customTagValue']. "</td>";
                $htmlTable .= "</tr>";
            }
            */
        }
        return $htmlTable;
    }
    
    function checkCustomTagExistInSpec($tagName, $deviceType) {
        $info = new CustomTagManagement();
        $tagList = $info->getTagList($deviceType);
        
        if(count($tagList)>0 ){
            foreach($tagList as $key=>$val){
                if( $val['tagName'] == $tagName){
                    return true;
                }
            }
        }
        return false;
    }
    
    function addCustoTagToUser($post) {
//         echo"11111";
        $_SESSION['assignCustomTagToUserForTable'][$post["tagName"]] = Array
        (
            "customTagName" => $post["tagName"],
            "customTagValue" => $post["tagValue"],
            "customTagShortDesc" => $post["tagNameDescription"],
            "customTagLongDesc" => $post["tagNameDescription"]
            );
    }
    
    function processCustomTagArr($usersProvisionedCustomTags ,$usersAvailableCustomTags){
        $customTag = array();
        foreach ($usersAvailableCustomTags as $key => $Value){
            if( ! isset($Value['groupLevel']) ) {
                if(! isset($usersProvisionedCustomTags[$key])) {
                    $customTag["add"][] =    $usersAvailableCustomTags[$key];
                } 
                else if( isset($usersProvisionedCustomTags[$key]) && isset($usersProvisionedCustomTags[$key]['groupLevel']) && ! isset($usersAvailableCustomTags[$key]['groupLevel']) ) {
                    $customTag["add"][] =    $usersAvailableCustomTags[$key];
                }
                else if(isset($usersProvisionedCustomTags[$key]) && $usersAvailableCustomTags[$key]["customTagValue"] != $usersProvisionedCustomTags[$key]["customTagValue"]) {
                    $Value['oldValue'] = $usersProvisionedCustomTags[$key]['customTagValue'];
                    $customTag["modify"][] = $Value;
                }
            }
        }
        
        foreach ($usersProvisionedCustomTags as $key => $Value1){
            if( ! isset($Value1['groupLevel']) && ! isset($usersAvailableCustomTags[$key]))
            {
                $customTag["delete"][] = $Value1;
            }
            
        }
        return  $customTag;
    }
    // --------------------------------
    // Ajax POST processing starts here
    // --------------------------------
    $assignedValidationMsg = " Custom Tag is already assigned to this user. Please try with other Tag name";
    $inSpecValidationMsg = " Custom Tag name is Availabe in In-Spec. Please try with other Tag name";
    $error = true;
    $resp = "";

    // Create new Custom Tag
    if (isset($_POST["tagName"])) {
    	
    	/*
        $customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
        if (($resp = $customTags->addCustomTag($_POST["tagName"], $_POST["tagValue"])) == "") {
        	$resp = $customTags->formatCustomTagsTable();
            $error = false;
        }
        */
        
        if( isset($_POST["operationType"]) && $_POST["operationType"] == "CREATE") {                                /*Create Out-Of-Spec Custom tag*/
             
            if( isset($_SESSION['assignCustomTagToUserForTable'][$_POST["tagName"]]) && 
                ! isset($_SESSION['assignCustomTagToUserForTable'][$_POST["tagName"]]["groupLevel"]) 
                ) {
                    
                $error = "2";                                                                                       /* "2" is identifying the validation message*/
                $resp = $_POST["tagName"] . $assignedValidationMsg;
            }
            else if( checkCustomTagExistInSpec($_POST["tagName"], $_SESSION["userInfo"]["deviceType"] ) ) {
                $error = "2";                                                                                        /* "2" is identifying the validation message*/
                $resp = $_POST["tagName"] . $inSpecValidationMsg;
            }
            else if( isset($_SESSION['assignCustomTagToUserForTable'][$_POST["tagName"]]) &&
                    isset($_SESSION['assignCustomTagToUserForTable'][$_POST["tagName"]]["groupLevel"]) &&
                    $_SESSION['assignCustomTagToUserForTable'][$_POST["tagName"]]["groupLevel"] == "Yes"
                 ) {
                     addCustoTagToUser($_POST);
                     $resp = formCustomTagsTableAfterChanges();
                     $error = false;
                }
            else {
                addCustoTagToUser($_POST);
                $resp = formCustomTagsTableAfterChanges();
                $error = false;
            }
        } else {                                                                                                    /* Add In-Spec Custom Tag */
            if( isset($_SESSION['assignCustomTagToUserForTable'][$_POST["tagName"]]) &&
                isset($_SESSION['assignCustomTagToUserForTable'][$_POST["tagName"]]["groupLevel"]) &&
                $_SESSION['assignCustomTagToUserForTable'][$_POST["tagName"]]["groupLevel"] == "Yes"
                ) {
                   // unset($_SESSION['assignCustomTagToUserForTable'][$_POST["tagName"]]);
                    addCustoTagToUser($_POST);
                    $resp = formCustomTagsTableAfterChanges();
                    $error = false;
                }else {
                   addCustoTagToUser($_POST);
                   $resp = formCustomTagsTableAfterChanges();
                   $error = false;
               }          
            
        }
        
        //echo"2222222222";
        //$customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
//         $customTags->customTags = $_SESSION['assignCustomTagToUserForTable'];
//         $resp = formCustomTagsTableAfterChanges();
        //$error = false;
        
    }

    // Delete Custom Tag
    elseif (isset($_POST["deleteCustomTag"])) {
    	/*
        $customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
        if (($resp = $customTags->deleteCustomTag($_POST["deleteCustomTag"])) == "") {
        	$resp = $customTags->formatCustomTagsTable();
            $error = false;
        }
        */
        foreach($_SESSION['assignCustomTagToUserForTable'] as $key2 => $value2) {
            if( $_POST["deleteCustomTag"] == $key2 && !isset($value2['groupLevel']) ) {
                unset($_SESSION['assignCustomTagToUserForTable'][$key2]);
            }
        }
//         unset($_SESSION['assignCustomTagToUserForTable'][$_POST["deleteCustomTag"]]);
//         $customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
//         $customTags->customTags = $_SESSION['assignCustomTagToUserForTable'];
        $resp = formCustomTagsTableAfterChanges();
        $error = false;
    }

    // Modify Custom Tag
    elseif (isset($_POST["modifyCustomTag"])) { //echo "<pre>"; print_r($_POST); die;
        /*
    	$customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
        if (($resp = $customTags->modifyCustomTag($_POST["modifyCustomTag"], $_POST["modifiedTag"])) == "") {
        	$resp = $customTags->formatCustomTagsTable();
            $error = false;
        }
        */
        
        $_SESSION['assignCustomTagToUserForTable'][$_POST["modifyCustomTag"]]['customTagValue'] = $_POST["modifiedTag"];
//         $customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
//         $customTags->customTags = $_SESSION['assignCustomTagToUserForTable'];
        $resp = formCustomTagsTableAfterChanges();
        $error = false;
    }

    // Format Custom Profile drop-down list after the change of device type
    elseif (isset($_POST["deviceType"])) {
        $resp = buildCustomProfileSelection($_POST["deviceType"]);
        $analogDeviceStatusFlag = getDeviceAnalogStatusFlag($_POST["deviceType"]);

        // Concatenating together device analog status and device Custom Profile Selection
        $resp = $analogDeviceStatusFlag . $resp;
        $error = false;
    }
    // Return tag bundles if device type supported by Express
    elseif (isset($_POST["deviceTypeForTagBundle"])) {
        $resp = buildTagBundleSelection($_POST["deviceTypeForTagBundle"]);
        $analogDeviceStatusFlag = getDeviceAnalogStatusFlag($_POST["deviceTypeForTagBundle"]);
        
        // Concatenating together device analog status and device Custom Profile Selection
        $resp = $analogDeviceStatusFlag . $resp;
        $error = false;
    }

    // Create Custom Profile lookup table
    elseif (isset($_POST["lookupType"])) {
        $resp = createLookupTable($_POST["lookupType"]);
        $error = false;
    }
    else if (isset($_POST["isChangesDoneForUserCustomTag"])) {
        $resultCustomTagArr =  processCustomTagArr($_SESSION['usersProvisionedCustomTags'], $_SESSION['assignCustomTagToUserForTable']);
        
        if( !empty($resultCustomTagArr["add"]) || !empty($resultCustomTagArr["delete"]) || !empty($resultCustomTagArr["modify"]) ) {
            $resp = "MadeChanges";
            $error = false;
        }
        
    }

    if($error == "2") {
        /* 2 is identifying the validation message*/
        $errFlag = "2";
    } else {
        $errFlag = $error ? "1" : "0";
    }
    
    echo $errFlag . $resp;

?>
