<?php
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
//print_r($_POST);exit;
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/html/Express/userMod/arrayData.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");
//require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/userServices.php");
require_once ("/var/www/html/Express/userMod/UserModifyUtil/ServiceAndServicePackUtil.php");
require_once("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");

$userServices = new userServices();
$serviceObj = new ServiceAndServicePackUtil($_POST["userId"]);
$assignedUserServicesVal = ($_POST['assignedUserServicesVal'] != "") ? explode(";", rtrim($_POST['assignedUserServicesVal'], ";")) : array();
$userServices = $userServices->getUserServicesAll($_POST["userId"], $_SESSION["sp"]);
$servicesToBeOperated = $serviceObj->getAssingedAndUnAssingedServices($assignedUserServicesVal);
$objSysLevel = new sysLevelDeviceOperations();

if(array_key_exists("servicePack", $_POST)){
    $_POST['servicePack']= explode(";", $_POST['servicePack']);
    array_pop($_POST['servicePack']);
}

if(!array_key_exists("modTagBundle", $_POST)){
    $_POST['modTagBundle'][0] = "";
}

unset($_POST['availableUserServiceToAssign']);
unset($_POST['assignedUserService']);
unset($_POST["restrictToGroup"]);
unset($_POST["allUserSearch"]);
$bundleAssigned = false;
$bundleRemoved = false;
$modTagBundleChanged = false;
require_once ("/var/www/html/Express/util/formDataArrayDiff.php");
require_once ("/var/www/html/Express/userMod/UserModifyDBOperations.php");
$diffObj = new FormDataArrayDiff();
$userOpDBOperation = new UserModifyDBOperations();
//print_r($_POST["modTagBundle"]);
//print_r($_SESSION ["userInfo"]["modTagBundle"]);
$diffArr = $diffObj->diffInTwoArray($_POST["modTagBundle"], $_SESSION ["userInfo"]["modTagBundle"]);

require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");   //Code added @ 10 July 2018
$srvceObj = new Services();

if( $_POST['ccd'] == ""){
    if($_SESSION['userInfo']['ccd']==""){
        unset($_POST['ccd']);
    }    
}

if($ociVersion=="19"){
    
    unset($_POST['nameDialingFirstName']);
    unset($_POST['nameDialingLastName']);
}

    function checkServiceAssignedToUser($services, $serviceName) {
        foreach($services as $sKey => $sVal) {
            if(strpos($sVal , $serviceName) !== false) {
                return true;
            }
        }
        return false;
    }
    
    function servicePacksHaveVMService($servicePacks) {
        global $sessionid, $client;

        foreach ($servicePacks as $servicePack) {
            $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]). "</serviceProviderId>";
            $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

            foreach ($xml->command->userServiceTable->row as $key => $value){
                if (strval($value->col[0]) == "Voice Messaging User") {
                    return true;
                }
            }
        }
        return false;
    }

    //Code added @ 02 July 2018 to check Service packs have more then 1 VM Service
    function servicePacksHaveMoreOneVMService($servicePacks)
    {
        $counter = 0;
        $servicePackWithVMService = "";
        $servicePacksWithVMServiceArr = explode(",", $_SESSION["vmServicePacksFlat"]);        
        for($i=0;$i<count($servicePacks);$i++)
        {
            if(in_array($servicePacks[$i], $servicePacksWithVMServiceArr))
            {
                $counter = $counter + 1;
                $servicePackWithVMService = $servicePacks[$i];
            }
        }               
        if($counter>1)
            return true;
        else
            return false;
    }
    //End Code


function servicePackChanged($newServicePacks) {
    for ($i = 0; $i < count($newServicePacks); $i++) {
        if (! in_array($newServicePacks[$i],$_SESSION["userInfo"]["servicePacksAssigned"])) {
            return true;
        }
    }
    for ($i = 0; $i < count($_SESSION["userInfo"]["servicePacksAssigned"]); $i++) {
        if (! in_array($_SESSION["userInfo"]["servicePacksAssigned"][$i], $newServicePacks)) {
            return true;
        }
    }
    return false;
}

function tagBundleChanged($newBundle) {
    //print_r($newBundle);
    //print_r($_SESSION["userInfo"]["modTagBundle"]);
    for ($i = 0; $i < count($newBundle); $i++) {
        if (! in_array($newBundle[$i],$_SESSION["userInfo"]["modTagBundle"])) {
            return true;
        }
    }
    for ($i = 0; $i < count($_SESSION["userInfo"]["modTagBundle"]); $i++) {
        if (! in_array($_SESSION["userInfo"]["modTagBundle"][$i], $newBundle)) {
            return true;
        }
    }
    return false;
}

require_once("/var/www/lib/broadsoft/adminPortal/arrays.php");

if(isset($_POST["servicePack"]) && count($_POST["servicePack"])>0)
{
    if($_POST["servicePack"][0]=="object HTMLInputElement")
    {
        unset($_POST["servicePack"][0]);
    }
}


//express License premssion set value custom Profile/Phone Profile
if(!isset($_POST["customProfile"]) && $license["customProfile"] =="false"){
    $_POST["customProfile"] = "None";
}

$_SESSION["userInfo"]["resetWeb"] = "false";
$_SESSION["userInfo"]["resetPortal"] = "false";
$_SESSION["userInfo"]["resetDevice"] = "false";
if (strlen($_POST["monitoredUsers"]) < 2)
{
    unset($_POST["monitoredUsers"]);
}
if (strlen($_POST["scaUsers"]) < 2)
{
    unset($_POST["scaUsers"]);
}

$hidden = array("userId","rebuildPhoneFilesNotHidden","resetPhoneNotHidden", "input-confirm-password", "input-confirm-passcode");
$required = array("firstName", "lastName", "callingLineIdFirstName", "callingLineIdLastName", "timeZone", "extension");



if ($bwRequireAddress == "true")
{
    $required[] = "addressLine1";
    $required[] = "city";
    $required[] = "stateOrProvince";
    $required[] = "zipOrPostalCode";
}
$checkForCharacters = array("firstName", "lastName", "addressLine1", "addressLine2", "city", "zipOrPostalCode", "callingLineIdFirstName", "callingLineIdLastName");

$callForwards = array(
    "cfaActive" => "cfaForwardToPhoneNumber",
    "cfbActive" => "cfbForwardToPhoneNumber",
    "cfnActive" => "cfnForwardToPhoneNumber",
    "cfrActive" => "cfrForwardToPhoneNumber"
);

//Added code @ 17 May 2018 
if($_SESSION["permissions"]["changeDevice"]=="1" && !array_key_exists("uActivateNumber", $_POST))
{
    $_POST['uActivateNumber'] = "No";
}
//Code Commented @ 17 May 2018 
/*
if(!array_key_exists("uActivateNumber", $_POST)){
    $_POST['uActivateNumber'] = "No";
}*/

$data = $errorTableHeader;


if($_POST['rebuildPhoneFiles'] == "true" && $_POST['resetPhone'] == "true"){
    $data .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild and reset after successful modification</b></td></tr>';
}else if($_POST['rebuildPhoneFiles'] == "true" && $_POST['resetPhone'] == "false"){
    $data .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild after successful modification</b></td></tr>';
}else if($_POST['rebuildPhoneFiles'] == "false" && $_POST['resetPhone'] == "true"){
    $data .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be reset after successful modification</b></td></tr>';
}

/* Rebuilt Reset check for Custom tag Operation*/
if($_POST['rebuildPhoneFiles_CustomTag'] == "true" && $_POST['resetPhone_CustomTag'] == "true"){
    $data .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild and reset after successful Custom Tag Operations.</b></td></tr>';
}


if($_POST['rebuildPhoneFiles_sca'] == "true" && $_POST['resetPhone_sca'] == "true"){
   $data .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild and reset after successful SCA Operations</b></td></tr>';
}else if($_POST['rebuildPhoneFiles_sca'] == "true" && $_POST['resetPhone_sca'] == "false"){
    $data .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild after successful modification</b></td></tr>';
}else if($_POST['rebuildPhoneFiles_sca'] == "false" && $_POST['resetPhone_sca'] == "true"){
    $data .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be reset after successful modification</b></td></tr>';
}




$changes = 0;
$error = 0;

if(isset($_POST["deviceTypeResult"])) {
    $_POST["deviceType"] = $_POST["deviceTypeResult"];
}

if(isset($_POST["doClearMacAddress"]) && $_POST["doClearMacAddress"] == "true") {
    unset($_POST["doClearMacAddress"]);
    unset($_POST["macAddressClearMac"]);
    $_POST["rebuildPhoneFiles"] = "true";
    $_POST["resetPhone"] = "true";
    
} else if(isset($_POST["doClearMacAddress"]) && $_POST["doClearMacAddress"] == "false") {
    unset($_POST["doClearMacAddress"]);
    unset($_POST["macAddressClearMac"]);
    unset($_POST["macAddress"]);
}

$userType = $_POST["userTypeResult"];
$deviceType = $_POST["deviceTypeResult"];
$userTypePrevious = $_POST["userTypePrevious"];

unset($_POST["deviceTypeResult"]);
unset($_POST["userTypeResult"]);
unset($_POST["previousDeviceType"]);
unset($_POST["previousDeviceName"]);
unset($_POST["userTypePrevious"]);
unset($_POST["deviceIndex"]);
unset($_POST["portNumber"]);

if($userTypePrevious == $userType) {
    unset($_POST["userType"]);
}
if( isset($_POST["supportPortNumberType"]) ) {
    $supportPortNumberType = $_POST["supportPortNumberType"];
    unset($_POST["supportPortNumberType"]);
}
if(isset($_POST["userPassword"])){
    if(isset($_POST["resetWebDefault"])){
        unset($_POST["input-password"]);
        unset($_POST["input-confirm-password"]);
    }
    if(!isset($_POST["resetWebDefault"]) && (empty($_POST["input-password"]) && empty($_POST["input-confirm-password"]))){
        unset($_POST["userPassword"]);
        unset($_POST["input-password"]);
        unset($_POST["input-confirm-password"]);
    }
}else{
    unset($_POST["input-password"]);
    unset($_POST["input-confirm-password"]);
}

if(isset($_POST["userPasscode"])){
    if(isset($_POST["resetPassDefault"])){
        unset($_POST["input-passcode"]);
        unset($_POST["input-confirm-passcode"]);
    }
    if(!isset($_POST["resetPassDefault"]) && (empty($_POST["input-passcode"]) && empty($_POST["input-confirm-passcode"]))){
        unset($_POST["userPasscode"]);
        unset($_POST["input-passcode"]);
        unset($_POST["input-confirm-passcode"]);
    }
}else{
    unset($_POST["input-passcode"]);
    unset($_POST["input-confirm-passcode"]);
}

if($userType == "digital" || ($userType == "analog" && $_POST["deviceType"] == "") || ($userType == "analog" && $supportPortNumberType == "dynamic")) {
    unset($_POST["deviceIndexText"]);
    unset($_POST["portNumberText"]);
    
}
if(($_POST["deviceType"] == "") || ($userType == "analog" && $supportPortNumberType == "dynamic") || ($userType == "digital" && $supportPortNumberType == "dynamic")) {
    unset($_POST["portNumberSIP"]);
}

if($_POST["deviceType"] != $_SESSION["userInfo"]["deviceType"] || $_POST["deviceIndexText"] != $_SESSION["userInfo"]["deviceIndexText"]) {
    $_SESSION["userInfo"]["portNumberText"] = "";
}

/* Start Users custom tag validation*/
$resultCustomTagArr =  processCustomTagArr($_SESSION['usersProvisionedCustomTags'], $_SESSION['assignCustomTagToUserForTable']);

if(isset($resultCustomTagArr['add']) && !empty($resultCustomTagArr['add'])){
    $changes++;
    $bg = CHANGED;
    $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Custom Tags Add</td><td class=\"errorTableRows\">";
    foreach($resultCustomTagArr['add'] as $addCustomTag){
        if(!empty($addCustomTag['customTagName'])){
            $data .= $addCustomTag['customTagName']. " - ".$addCustomTag['customTagValue']." ,<br />";
        }
    }
    $data .= "</td></tr>";
}

if(isset($resultCustomTagArr['modify']) && !empty($resultCustomTagArr['modify'])){
    $changes++;
    $bg = CHANGED;
    $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Custom Tags Modified</td><td class=\"errorTableRows\">";
    foreach($resultCustomTagArr['modify'] as $modifyCustomTag){
        if(!empty($modifyCustomTag['customTagName'])){
            $data .= $modifyCustomTag['customTagName']. " - ".$modifyCustomTag['customTagValue']." ,<br />";
        }
    }
    $data .= "</td></tr>";
}

if(isset($resultCustomTagArr['delete']) && !empty($resultCustomTagArr['delete'])){
    $changes++;
    $bg = CHANGED;
    $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Custom Tags Delete</td><td class=\"errorTableRows\">";
    foreach($resultCustomTagArr['delete'] as $delCustomTag){
        if(!empty($delCustomTag['customTagName'])){
            $data .= $delCustomTag['customTagName']. " - ".$delCustomTag['customTagValue']." ,<br />";
        }
    }
    $data .= "</td></tr>";
}
/* End Users custom tag validation*/


if(isset($_SESSION["softPhoneListToAdd"]) && count($_SESSION["softPhoneListToAdd"]) > 0){
    $servicePackAssigned = "";
    $grpServiceList = new Services();
    $respServices = getUserAssignedUserServices($_POST["userId"]);
    //print_r($respServices);
    $scaServiceCountCheck = array_intersect($scaArray, $respServices);
      
    $userservicePackArray = array();
    $userServices = new userServices();
    $userSessionservicePackArray = $userServices->getUserServicesAll($_POST["userId"], $_SESSION["sp"]);
   // echo "1- ";print_r($userSessionservicePackArray);
    $servicesSessionArray = $grpServiceList->getServicePackServicesListUsers($userSessionservicePackArray["Success"]["ServicePack"]["assigned"]);
    //echo "2- ";print_r($servicesSessionArray);
    $scaSessionServiceArray = $grpServiceList->checkScaUserServiceInServicepack($servicesSessionArray);
    //echo "3- ";print_r($scaSessionServiceArray);
    foreach($scaSessionServiceArray as $ssa=>$ssv){
        $pos = strpos($ssv->col[0], "Shared Call Appearance");
        if($pos === false){
        }else{
            $servicePackAssigned = $userSessionservicePackArray["Success"]["ServicePack"]["assigned"][$ssa];
        }
    }
    
    //echo "1";print_r($servicePackAssigned);
    if($servicePackAssigned == ""){
        $grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
        $servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
        $servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray);
        $scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);
        //echo "sdsdsd";print_r($scaServiceArray);
        $oneServicePack = "false";
        $multipleServicePack = "false";
        $noServicePack = "false";
        $r = 0;
        foreach($scaServiceArray as $ssa=>$ssv){
            $pos = strpos($ssv->col[0], "Shared Call Appearance");
            if($pos === false){
                
            }else{
                if($ssv->count == 1){
                    $oneServicePack = "true";
                    $r++;
                    //$countMore = "0";
                }else if($ssv->count > 1){
                    $multipleServicePack = "true";
                    //$countMore = "true";
                }else{
                    $noServicePack = "true";
                    //$countMore = "0";
                }
            }
            
        }
        //print_r($oneServicePack);
        //print_r($multipleServicePack);
        //print_r($r);
        if($oneServicePack == "false" || $r != "1"){
            $error = 1;
            $bg = INVALID;
            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Service Pack</td><td class=\"errorTableRows\">Express cannot unambiguously assign Service Pack with SCA service to user</td></tr>";
        }
    }
    
    
}

if(isset($_SESSION["softPhoneListToAdd"]) && count($_SESSION["softPhoneListToAdd"]) > 0){
    $softPArr = $_SESSION["softPhoneListToAdd"];
    //print_r($softPArr);
    $changes++;
    $bg = CHANGED;
    
    foreach ($softPArr as $skey => $svalue){
        unset($svalue["action"]);
        unset($svalue["label"]);
        unset($svalue["port"]);
        if($svalue["softPhoneType"] != "counterPath"){
            unset($svalue["accountUserName"]);
            unset($svalue["accountProfile"]);
            unset($svalue["softPhoneAccountEmailAddress"]);
        }
        unset($svalue["softPhoneType"]);
        foreach ($svalue as $siKey => $siVal){
            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">".$softArray[$siKey]."</td><td class=\"errorTableRows\">".$siVal."</td></tr>";
        }
        
    }
    
}

if($_SESSION["softPhoneMod"] == "softPhoneMod"){
    if(isset($_SESSION["softPhoneModData"]["changes"]) && count($_SESSION["softPhoneModData"]["changes"]) > 0){
        $changes++;
        $bg = CHANGED;
        foreach ($_SESSION["softPhoneModData"]["changes"] as $modKey => $modVal){
            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">".$modKey."</td><td class=\"errorTableRows\">".$modVal."</td></tr>";
        }
    }
}

/* Line Port Validation */
foreach( $_SESSION["userInfo"]["devicePortAssignmentDetail"] as $portKey => $portValue) {
    if( $portValue["primary"] == "true") {
        $oldLinePortPrimary = $portValue["linePort"];
        break;
    }
}
/* Line Port Validation */


foreach ($_POST as $key => $value)
{
    // Do not show flags for rebuilding and resetting device if they are not set to 'true'
    
    if($key == "deviceLinePortPrimary") {
        if( $value != $oldLinePortPrimary) {
            $changes++;
            $bg = CHANGED;
        } else {
            $bg = UNCHANGED;
            continue;
        }
    }
    
    if($key == "linePortTableOrderedData") {
        if($value != "") {
            $changes++;
            $bg = CHANGED;
            $table = "<table>";
            
            $linPorts = explode(";", rtrim($value, ";"));
            foreach( $linPorts as $portKey => $portValue) {
                $table .= "<tr><td class=\"errorTableRows\" style=\"width:50%; background:" . $bg . ";\">" . $portValue . "</td><td class=\"errorTableRows\">" . ($portKey + 1) . "</td></tr>";
            }
            $table .= "</table>";
            $value = $table;
        } else {
                continue;
                $bg = UNCHANGED;
        }
                
    }
    
    if($key == "assignedUserServicesVal") {
        if( $servicesToBeOperated['NewServicesForAssignment'] == "Yes" ) {
            $changes++;
            $bg = CHANGED;
            $value = ( empty($servicesToBeOperated['assign']) ? "None" : implode(", ", $servicesToBeOperated['assign']));
            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\"> Assigning User Services </td><td class=\"errorTableRows\">" . ( empty($value) ? "None" : $value) . "</td></tr>";
        }
        if( $servicesToBeOperated['NewServicesForUnAssignment'] == "Yes" ) {
            $changes++;
            $bg = CHANGED;
            $value = implode(", ", $servicesToBeOperated['unAssign']);
            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\"> Unassigning User Services </td><td class=\"errorTableRows\">" . ( empty($value) ? "None" : $value) . "</td></tr>";
        }
        continue;
    }
    
    if($key == "mailBoxLimit") {
        if ($value == "Use Group Default" && $_SESSION["userInfo"]["mailBoxLimit"] == "true") {
            continue;
        }
    }
    
    if ( ($key == "rebuildPhoneFiles" || $key == "resetPhone") && $value != "true" ) {
        continue;
    }
    
    // Do not show lineport domain from posted dropdown, as it is replicated in a hidden field.
    if ($key == "linePortDomain") {
        continue;
    }
    
    if ($key == "customTagsLookup") {
        continue;
    }
    
    if ($key == "cpgGroupName") {
        if ($_POST['cpgGroupName'] == $_SESSION["userInfo"]["usersGroupName"] || (empty($_POST['cpgGroupName']) && empty($_SESSION["userInfo"]["usersGroupName"]))){
            continue;
        }
    }
    
    /*Start code for Name Dialing Names show changed data */    
    if ($key == "nameDialingFirstName")
    {        
        if(($_POST["nameDialingFirstName"] != $_SESSION["userInfo"]["nameDialingFirstName"]) || ($_POST["nameDialingLastName"] != $_SESSION["userInfo"]["nameDialingLastName"]))
        {            
            if(!($_POST['nameDialingFirstName']=="" && $_POST['nameDialingLastName']=="")){
                if($_POST["nameDialingFirstName"]=="" && $_POST["nameDialingLastName"]!=""){
                    $required[] = "nameDialingFirstName";
                }
                if($_POST["nameDialingLastName"]=="" && $_POST["nameDialingFirstName"]!=""){
                    $required[] = "nameDialingLastName";
                }
            }
        }        
        if(!in_array($key, $required)){
            if(($_POST["nameDialingFirstName"] != $_SESSION["userInfo"]["nameDialingFirstName"])){            
                $changes++;
                $bg = CHANGED;                
                //$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">".$_SESSION["userModNames"][$key] ."</td><td class=\"errorTableRows\">" .($value == "" ? "None" : $value). "</td></tr>"; 
             }
        }           
    }
    
    if ($key == "nameDialingLastName"){
        if(!in_array($key, $required)){
            if(($_POST["nameDialingLastName"] != $_SESSION["userInfo"]["nameDialingLastName"])){
                $changes++;
                $bg = CHANGED;
                //$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">".$_SESSION["userModNames"][$key] ."</td><td class=\"errorTableRows\">" .($value == "" ? "None" : $value). "</td></tr>";            
            }
        }
    }
    /* End code for Name Dialing Names */
    //echo "<br />Data - ".$data."<br />";
    
    
    if ($key == "servicePack") {
        if($_SESSION['userInfo']['servicePacksAssigned'][0] != "") {
            if (! servicePackChanged($_POST["servicePack"])) {
                //$changes > 1 ? $changes-- : "";
                continue;
            }
            $changes++;
            $bg = CHANGED;
            $value = "";
            foreach ($_POST["servicePack"] as $pack) {
                $value .= $value != "" ? "<br/>" : "";
                $value .= $pack;
            }
        } else if($_SESSION['userInfo']['servicePacksAssigned'][0] == "" && count($_POST["servicePack"]) > 0) {
            $changes++;
            $bg = CHANGED;
            $value = "";
            foreach ($_POST["servicePack"] as $pack) {
                $value .= $value != "" ? "<br/>" : "";
                $value .= $pack;
            }
        }
        else if($_SESSION['userInfo']['servicePacksAssigned'][0] == "" && count($_POST["servicePack"]) == 0) {
            $value = "";
        }
        
      }
    
    if ($key == "modTagBundle" && in_array($_POST["deviceType"], $deviceTypeArrForCheck)) {
        // if($_SESSION['userInfo']['modTagBundle'][0] != "") {
        if (! tagBundleChanged($_POST["modTagBundle"])) {
            continue;
        }
        //}
        
        $changes++;
        $bg = CHANGED;
        $value = "";
        $modTagBundleChanged = true;
        //print_r($diffArr);
        //echo "123";print_r(count($diffArr["removed"]));
        if((isset($diffArr["assigned"]) && $diffArr["assigned"][0] != "") || (isset($diffArr["removed"]) && $diffArr["removed"][0] != "")){
            if(isset($diffArr["removed"]) && $diffArr["removed"][0] != ""){
                $tagToBeRemovedIfBundleChanged = $userOpDBOperation->getAllCustomTagsFromTagBundles($diffArr["removed"]);
                $bundleAssigned = true;
                $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Custom tags to be Removed </td><td class=\"errorTableRows\">" . implode(", ", array_keys($tagToBeRemovedIfBundleChanged)). "</td></tr>";
                
            }
            if(isset($diffArr["assigned"]) && $diffArr["assigned"][0] != "" ){
                $tagToBeAddIfBundleChanged = $userOpDBOperation->getAllCustomTagsFromTagBundles($diffArr["assigned"]);
                $bundleAssigned = true;
                $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Custom tags to be added </td><td class=\"errorTableRows\">" . implode(", ", array_keys($tagToBeAddIfBundleChanged)). "</td></tr>";
                
            }
            
        }
        
        
        foreach ($_POST["modTagBundle"] as $bundle) {
            $value .= $value != "" ? "<br/>" : "";
            $value .= $bundle;
        }
    }
    
    if ($key == "modTagBundle" && !in_array($_POST["deviceType"], $deviceTypeArrForCheck)) {
        continue;
    }
    
    
    
    if($key == "voiceMessaging" && $value !=""){
        if(!isset( $_SESSION['userInfo']['voiceMessagingUser']) && $value == "No"){
            continue;
        }
        if(isset( $_SESSION['userInfo']['voiceMessagingUser']) && $value == "Yes"){
            //continue;
        }
    }  
    
    /*if($key == "useCustomSettings"){
        if($value == $_SESSION["userInfo"]["useCustomSettings"]){
            continue;
        }
        
    }*/
     
    
    if (!in_array($key, $hidden)) //hidden form fields that don't change don't need to be checked or displayed
    {
        if ($key !== "speedCode" and $key !== "phone" and $key !== "description" and $key !== "answerConfirmationRequired" and $key !== "simultaneousRingPhoneNumber"  and $key !== "criteriaName" and $key !== "criteriaNameDup" and ((isset($_SESSION["userInfo"][$key]) and $value !== $_SESSION["userInfo"][$key]) or !isset($_SESSION["userInfo"][$key])))
        {
            /* code for service pack*/
            if ($key !== "servicePack") {
                $changes++;
                $bg = CHANGED;
            }
            /* end code for service pack*/
            
            //             $changes++;
            //             $bg = CHANGED;
        }
        else
        {
            $bg = UNCHANGED;
        }
        
        if (in_array($key, $checkForCharacters))
        {
            if (strlen($value) > 0)
            {
                if (!preg_match("/^[0-9a-zA-Z\- \/_?;:.,\s]+$/", $value))
                //if (!preg_match("/^[ A-Za-z0-9-_]+$/", $value))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = $_SESSION["userModNames"][$key] . " must be letters, numbers, and dashes only.";
                }
            }
        }
        if ($key == "macAddress" and $value !== "")
        {
            if ($value !== $_SESSION["userInfo"]["macAddress"])
            {
                include("../checkMac.php");
                if (isset($errorMsg))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = $errorMsg;
                }
            }
        }
        if ($key == "extension" && $value != "") {
            if ($value != $_SESSION["userInfo"]["extension"]) {
                include("../checkExtension.php");
            }
        }
        
        
        if($userType == "analog")
        {
            
            if ($key == "deviceIndexText")
            {
                if (isset($deviceType) && $deviceType != "" && $value == "")
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Device index is required .";
                }
            }
            
            if ($key == "portNumberText")
            {
                if (isset($deviceType) && $deviceType != "" && $value == "")
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Port number is required .";
                }
            }
        }
        
        if($userType == "digital") {
            if ($key == "portNumberSIP")
            {
                if (isset($deviceType) && $deviceType != "" && $value == "")
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Port number is required .";
                } else {
                    $value = $value . " : Port " . $value;
                }
            }
        }
        
        if ($key == "phoneNumber")
        {
            //if third-party voice mail is checked and phone number wasn't set, generate an error
            if (isset($_POST["thirdPartyVoiceMail"]) and $_POST["thirdPartyVoiceMail"] == "true" and $value == "")
            {
                $error = 1;
                $bg = INVALID;
                $value = $_SESSION["userModNames"][$key] . " is required for third-party voice mail.";
            }
        }
        
        if($key == "uActivateNumber"){
            if(!empty($_POST["phoneNumber"])){
                if($_POST["phoneNumber"] !== $_SESSION["userInfo"]["phoneNumber"]){
                    if($value == "Yes"){
                        $changes++;
                        $bg = CHANGED;
                        if($value == "Yes"){
                            $value = "Activated";
                        }else{
                            $value = "Deactivated";
                        }
                    }else{
                        $bg = UNCHANGED;
                    }
                }else{
                    if($value !== $_SESSION["userInfo"][$key]){
                        $changes++;
                        $bg = CHANGED;
                        if($value == "Yes"){
                            $value = "Activated";
                        }else{
                            $value = "Deactivated";
                        }
                    }
                }
            }
        }
        
        if ($key == "cfnNumberOfRings")
        {
            if($value !="")
            {
                if($value == 0)
                {
                    if($_SESSION["userInfo"]["cfnNumberOfRings"] == $value) {
                        continue;
                    }
                }
                else if ($value < 2 || $value > 20)
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Number of Rings Before Forwarding value should be between 2 to 20.";
                }
            }
            
        }
        
        if ($key == "recallNumberOfRings")
        {
            if ($value < 2 || $value > 20)
            {
                $error = 1;
                $bg = INVALID;
                $value = "Number of Rings before Recall value should be between 2 to 20.";
            }
        }
        
        if ($key == "busyCampOnSeconds")
        {
            if ($value < 30 || $value > 600)
            {
                $error = 1;
                $bg = INVALID;
                $value = "Enable Busy Camp value should be between 30 to 600.";
            }
        }
        
        if ($key == "hotelHostId")
        {
            if ($value !== "" and $_POST["hotelisActive"] == "false")
            {
                $error = 1;
                $bg = INVALID;
                $value = "Cannot associate with host if hoteling guest is disabled.";
            }
        }
        
        if ($key == "maxFindMeFollowMeDepth")
        {
            if($value != ""){
                if (( $value < 1) || ($value > 10))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Maximum Find Me/Follow Me Depth- should be between 1 and 10";
                }
            }
            
        }
        
        if ($key == "maxCallTimeForUnansweredCallsMinutes")
        {
            if($value != ""){
                if (( $value < 1) || ($value > 60))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Maximum Duration for Unanswered Calls- should be between 1 and 60";
                }
            }
            
        }
        if ($key == "maxConcurrentRedirectedCalls")
        {
            if($value != ""){
                if (( $value < 1) || ($value > 32))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Maximum Number of Concurrent Redirected Calls- should be between 1 and 32";
                }
            }
            
        }
        if ($key == "maxConcurrentFindMeFollowMeInvocations")
        {
            if($value != ""){
                if (( $value < 1) || ($value > 32))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Maximum Number of Concurrent Find Me/Follow Me Invocations- should be between 1 and 32";
                }
            }
            
        }
        if ($key == "maxRedirectionDepth")
        {
            if($value != ""){
                if (( $value < 1) || ($value > 15))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Maximum Redirection Depth- should be between 1 and 15";
                }
            }
            
        }
        if ($key == "maxSimultaneousCalls")
        {
            if($value != ""){
                if (( $value < 1) || ($value > 500))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Maximum Number of Concurrent Calls- should be between 1 and 500";
                }
            }
            
        }
        
        if ($key == "maxSimultaneousVideoCalls")
        {
            if($value != ""){
                if (( $value < 1) || ($value > 500))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Maximum Number of Concurrent Video Calls- should be between 1 and 500";
                }
            }
            
        }
        
        if ($key == "maxCallTimeForAnsweredCallsMinutes")
        {
            if($value != ""){
                if (( $value < 3) || ($value > 2880))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Maximum Duration for Answered Calls- should be between 3 and 2880";
                }
            }
            
        }
       
        /* Remove this validation as per discussion with Karl
        //Code added @ 02 July 2018 to check Service packs have more then 1 VM Service
        if ($key == "voiceMessaging" && isset($_POST["servicePack"])) {
            if (servicePacksHaveMoreOneVMService($_POST["servicePack"])) {
                $error = 1;
                $bg = INVALID;
                $value = "Express cannot unambiguously assign Service Pack with Voice Messaging User service.";
            }            
        }
        //End Code
         */
        
        if ($key == "voiceMessaging" && $value == "Yes" && isset($_POST["servicePack"])) {            
            //if (!servicePacksHaveVMService($_POST["servicePack"])) {
            if (!servicePacksHaveVMService($_POST["servicePack"]) &&
                !checkServiceAssignedToUser($servicesToBeOperated['assign'], "Voice Messaging User") &&
                !checkServiceAssignedToUser($assignedUserServicesVal, "Voice Messaging User")
             ) {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Selected Service Packs to be assigned to User do not include 'Voice Messaging User' service.";
             }
            
        }
        
        //Code added @ 11 July 2018
        if($key == "sharedCallAppearance" && $value == "Yes")
        {
//                 if(empty($_POST["servicePack"]))
//                 {
                if(empty($_POST["servicePack"]) &&
                    !checkServiceAssignedToUser($servicesToBeOperated['assign'], "Shared Call Appearance") &&
                    !checkServiceAssignedToUser($assignedUserServicesVal, "Shared Call Appearance")
                )
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Express cannot unambiguously assign Service Pack with Shared Call Appearance service.";
                }
        }
        if ($key == "sharedCallAppearance" && $value == "Yes" && isset($_POST["servicePack"]) && !empty($_POST["servicePack"])) 
        {            
            if (! $srvceObj->servicePacksHaveSCAService($_POST["servicePack"])) {            
                $error = 1;
                $bg = INVALID;
                $value = "Selected Service Packs to be assigned to User do not include 'Shared Call Appearance' service.";
            }
        }
        //End code
        
        
        //if ($key == "polycomPhoneServices" and $value !== "")
        if ($key == "polycomPhoneServices" and $value !== "" && $_SESSION["permissions"]["changeDevice"] == "1")  //Code added @ 15 March 2019 if polycom phone service is enabled then device type must be set and support polycom phone service
        {
            if($value == "Yes"){
                if($_SESSION["deviceIsAudioCodeName"] != "true" && isset($_POST["deviceType"]) && $_POST["deviceType"] != ""){
                
                    //if(isset($_POST["deviceType"]) && !empty($_POST["deviceType"])){
                        
                        
                        $returnResp = $objSysLevel->getSysytemDeviceTypeServiceRequest($_POST["deviceType"], $ociVersion);
                        if($returnResp["Error"] == "" && empty($returnResp["Error"])){
                            $resp = strval($returnResp["Success"]->supportsPolycomPhoneServices);
                            if($resp == "false"){
                                $error = 1;
                                $bg = INVALID;
                                $value = "Polycom Phone Services' is not enabled for Device Type: ".$_POST['deviceType']."";
                            }
                        }
                    //}
                }
               /* else{
                    $error = 1;
                    $bg = INVALID;
                    $value = "Polycom Phone Services requires supported Polycom device type to be assigned to User.";
                } */
                
                else{
                    if( !checkServiceAssignedToUser($servicesToBeOperated['assign'], "Polycom Phone Services") &&
                        !checkServiceAssignedToUser($assignedUserServicesVal, "Polycom Phone Services")
                     ) {
                            $error = 1;
                            $bg = INVALID;
                            $value = "Polycom Phone Services requires supported Polycom device type to be assigned to User.";
                     }
                }
                
            }
            
        }
        
        //Code added @ 10 Sep 2018
        //Code commented @ 25 Sep 2018 Regarding EX-817
        /*if($key == "polycomPhoneServices" && $value == "Yes")
        {
                $userServicesArr = $_SESSION['groupInfoData']['userServiceAuth'];                
                if(!in_array("Polycom Phone Services", $userServicesArr))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Polycom Phone Services is not assigned.";
                }
        }*/
        if ($key == "polycomPhoneServices" && $value == "Yes" && isset($_POST["servicePack"]) && !empty($_POST["servicePack"])) 
        {            
//          if (!$srvceObj->checkservicePacksHasPolycomPhoneService($_POST["servicePack"])) {
            if (!$srvceObj->checkservicePacksHasPolycomPhoneService($_POST["servicePack"]) &&
                !checkServiceAssignedToUser($assignedUserServicesVal, "Polycom Phone Services") &&
                !checkServiceAssignedToUser($servicesToBeOperated['assign'], "Polycom Phone Services")) {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Selected Service Packs to be assigned to User do not include 'Polycom Phone Services.";              
            }            
        }
//         if($key == "polycomPhoneServices" && $value == "Yes" && empty($_POST["servicePack"]))
        if($key == "polycomPhoneServices" && $value == "Yes" && empty($_POST["servicePack"]) &&
            !checkServiceAssignedToUser($servicesToBeOperated['assign'], "Polycom Phone Services") &&
            !checkServiceAssignedToUser($assignedUserServicesVal, "Polycom Phone Services")
         ) {
                $error = 1;
                $bg = INVALID;
                $value = "Please select service pack that contains 'Polycom Phone Services'. ";
         }
        //End code
        
        
        
        
        if ($key == "speedDial8")
        {
            for ($i = 2; $i <= 9; $i++)
            {
                if ($value[$i]["name"] and $value[$i]["phoneNumber"] == "")
                {
                    $error = 1;
                    $bg = INVALID;
                    $value[$i]["error"] = "It is invalid to define a " . $_SESSION["userModNames"][$key] . " Name without defining Phone Number.";
                }
                else if (strlen($value[$i]["phoneNumber"]) > 0 and !preg_match("/^[ 0-9-]+$/", $value[$i]["phoneNumber"]))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value[$i]["error"] = $_SESSION["userModNames"][$key] . " Phone Number must be numbers, dashes, and spaces only.";
                }
            }
        }
        
        if ($key == "speedCode")
        {
            $oldCodes = "";
            $newCodes = "";
            //check if speed codes/phone numbers/descriptions changed; if so, make note of changes for logging purposes
            if (isset($_SESSION["userInfo"]["speedDial100"]))
            {
                foreach ($_SESSION["userInfo"]["speedDial100"] as $kk => $vv)
                {
                    $oldCodes .= str_pad($vv["speedCode"], 2, "0", STR_PAD_LEFT) . ":" . $vv["phone"] . ":" . $vv["description"] . "; ";
                }
            }
            foreach ($value as $k => $v)
            {
                if ($_POST["speedCode"][$k] != "NIL")
                {
                    $newCodes .= $_POST["speedCode"][$k] . ":" . $_POST["phone"][$k] . ":" . $_POST["description"][$k] . "; ";
                }
            }
            if ($oldCodes !== $newCodes)
            {
                $changes++;
                $bg = CHANGED;
            }
            
            $codeData = "";
            foreach ($value as $k => $v)
            {
                if ($v != "NIL")
                {
                    $phone = $_POST["phone"][$k];
                    $description = $_POST["description"][$k];
                    if ($_POST["phone"][$k] == "")
                    {
                        $error = 1;
                        $bg = INVALID;
                        $bg2 = $bg;
                        $phone = "Phone Number / SIP-URI is a required field.";
                    }
                    else if (strlen($_POST["phone"][$k]) > 0 and !preg_match("/^[ 0-9-]+$/", $_POST["phone"][$k]))
                    {
                        $error = 1;
                        $bg = INVALID;
                        $bg2 = $bg;
                        $phone = "Phone Number / SIP-URI must be numbers, dashes, and spaces only.";
                    }
                    else
                    {
                        //make sure each speed code is not used more than once
                        $speedCodeOccurrences = array_count_values($_POST["speedCode"]);
                        if ($speedCodeOccurrences[$v] > 1)
                        {
                            $error = 1;
                            $bg = INVALID;
                            $bg2 = $bg;
                            $description = "Speed Code must not be used more than once.";
                        }
                        else
                        {
                            $bg2 = UNCHANGED;
                        }
                    }
                    $codeData .= "<tr bgcolor=\"" . $bg2 . "\"><td class=\"dialogClass\">" . $v . "</td><td class=\"dialogClass\">" . $phone . "</td><td class=\"dialogClass\">" . $description . "</td></tr>";
                }
            }
            if ($codeData == "")
            {
                $codeData = "<tr><td class=\"dialogClass\">None</td></tr>";
            }
            unset($value);
            $value = "<table>" . $codeData . "</table>";
        }
        
        if ($key == "simultaneousRingPhoneNumber")
        {    
            
            $simRingPhoneArr = array();
            
            for ($m = 0; $m < 10; $m++)
            {         
                if($_POST["simultaneousRingPhoneNumber"][$m]!=""){
                    $simRingPhoneArr[] = $_POST["simultaneousRingPhoneNumber"][$m];
                }
            }
            
            //check if list of simultaneous ring numbers has changed; if so, make note of changes for logging purposes
            if(!empty($simRingPhoneArr)){
                $codeData        = "";
                $oldNumbers      = "";
                $newNumbers      = "";
            for ($i = 0; $i < 10; $i++)
            {
                if (isset($_SESSION["userInfo"]["simultaneousRingNumber"]))
                {
                    $oldNumbers .= $_SESSION["userInfo"]["simultaneousRingNumber"][$i]["phoneNumber"] . ":" . ($_SESSION["userInfo"]["simultaneousRingNumber"][$i]["answerConfirmationRequired"] == "false" ? "" : $_SESSION["userInfo"]["simultaneousRingNumber"][$i]["answerConfirmationRequired"]) . "; ";
                }
                $newNumbers .= $_POST["simultaneousRingPhoneNumber"][$i] . ":" . $_POST["answerConfirmationRequired"][$i]. "; ";
            }
            if ($oldNumbers !== $newNumbers)
            {
                $changes++;
                $bg = CHANGED;
            }
            
            
            //start code for check array if has duplicate values
            
            function array_has_dupes($simRingPhoneArr) {
                return count($simRingPhoneArr) !== count(array_unique($simRingPhoneArr));
            }
            
            if(array_has_dupes($simRingPhoneArr)=="1"){
                $error = 1;
                $bg = INVALID;
                $bg2 = $bg;
                $codeData .= "<tr bgcolor=\"" . $bg2 . "\"><td class=\"dialogClass\">Phone number already exists.</td></tr>";
            }
            //End code
            
            for ($i = 0; $i < 10; $i++)
            {
                if ($_POST["simultaneousRingPhoneNumber"][$i] !== "")
                {
                    $phone = $_POST["simultaneousRingPhoneNumber"][$i];
                    if (!preg_match("/^[ 0-9-]+$/", $phone))
                    {
                        $error = 1;
                        $bg = INVALID;
                        $bg2 = $bg;
                        $phone = "Phone Number / SIP-URI must be numbers, dashes, and spaces only.";
                    }
                    $codeData .= "<tr bgcolor=\"" . $bg2 . "\"><td class=\"dialogClass\">" . $phone . " - " . $_POST["answerConfirmationRequired"][$i] . "</td></tr>";
                }
            }
            if ($codeData == "")
            {
                $codeData = "<tr><td class=\"dialogClass\">None</td></tr>";
            }
            unset($value);
            $value = "<table>" . $codeData . "</table>";
            }
        }
        
        
              
        
        
        if ($key == "criteriaNameDup")
        {        
            
            $change = "no";
            $keyCriteria = "yes";
            $criteriaNameAdded = "";
            foreach($_SESSION['addSimRingCriteria'] as $criteriaAddRow){
                $change = "yes";
                $criteriaNameAdded .= $criteriaAddRow['criteriaName']." - Criteria Name Added <br />";
            }
            
            $criteriaNameModified = "";
            foreach($_SESSION['editSimRingCriteria'] as $criteriaEditRow){
                $change = "yes";
                $criteriaNameModified .= $criteriaEditRow['oldCriteriaName']." Modified To  ".$criteriaEditRow['criteriaName']."<br />";
            }
            
            $criteriaNameDel = "";            
            foreach($_SESSION['deleteSimRingCriteria'] as $criteriaDelRow){
                $change = "yes";
                $criteriaNameDel .= $criteriaDelRow['criteriaName']." - Criteria Name Deleted <br />";
            }
            
                
            $aCrtaCount  = count($_SESSION["userInfo"]["activeCriteriaNameList"]);
            $pCrtaCount  = count($_POST['criteriaName']);
            if($aCrtaCount>$pCrtaCount){
                $criteriaNameDiff = array_diff($_SESSION["userInfo"]["activeCriteriaNameList"], $_POST['criteriaName']);
            }
            else{
                $criteriaNameDiff = array_diff($_POST['criteriaName'], $_SESSION["userInfo"]["activeCriteriaNameList"]);
            }
            
            if(empty($_POST['criteriaName']) && !empty($_SESSION["userInfo"]["activeCriteriaNameList"])){
                $change = "yes";                
                $criteriaNameArr  = "";
                $criteriaNameArr  = implode(", ", $_SESSION["userInfo"]["activeCriteriaNameList"]);
                $criteriaNameArr .= " Unapplying on Sim Ring Service <br />";
            }
            
            if(!empty($criteriaNameDiff)){   
                $change = "yes";                
                $criteriaNameArr  = "";
                $criteriaNameArr  = implode(", ", $_POST['criteriaName']);
                $criteriaNameArr .= " Applying on Sim Ring Service <br />";
                
            }
            
            if($change=="yes"){
                $codeData = "";
                $changes++;
                $bg = CHANGED;
                $bg2 = $bg;
                $codeData .= "<tr bgcolor=\"" . $bg2 . "\"><td class=\"dialogClass\"> $criteriaNameArr $criteriaNameAdded $criteriaNameModified $criteriaNameDel</td></tr>";
                unset($value);
                $value = "<table>" . $codeData . "</table>";
            }
            /*if ($codeData == "")
            {
                $codeData = "<tr><td class=\"dialogClass\">None</td></tr>";
            }*/
            
        }
        
        
       if ($key == "simultaneousRingIsActive")
       {
           if(($_POST["simultaneousRingIsActive"] != $_SESSION["userInfo"]["simultaneousRingIsActive"])){
                
                $changes++;
                $bg = CHANGED;                
                if ($_POST['simultaneousRingIsActive'] == "true")
                {           
                    $simRingPhoneListDup = array();   
                    for ($p = 0; $p < 10; $p++)
                    {         
                        if($_POST["simultaneousRingPhoneNumber"][$p]!=""){
                            $simRingPhoneListDup[] = $_POST["simultaneousRingPhoneNumber"][$p];
                        }
                    }            
                    if(empty($simRingPhoneListDup)){
                        $error = 1;
                        $bg = INVALID;
                        $bg2 = $bg;
                        $value = "Please specify at least one Phone Number / SIP-URI to turn the service on.";
                      }
               }
           }
        }
        
         if ($key == "doNotRingIfOnCall")
         {
            
           if(($_POST["doNotRingIfOnCall"] != $_SESSION["userInfo"]["doNotRingIfOnCall"]))
           {
                $changes++;
                $bg = CHANGED;  
           }
        }
        
        
        
        
        
        //do error checking for voice management
        if ($key == "userServer")
        {
            if ($_POST["serverSelection"] == "User Specific Mail Server" and $value == "")
            {
                $error = 1;
                $bg = INVALID;
                $value = $_SESSION["userModNames"][$key] . " is required when you select User Specific Mail Server.";
            }
            else if (strlen($value) > 0 and !preg_match("/^[ 0-9-]+$/", $value))
            {
                $error = 1;
                $bg = INVALID;
                $value = $_SESSION["userModNames"][$key] . " must be numbers, dashes, and spaces only.";
            }
        }
        if ($key == "mailboxURL" and $_POST["mailboxIdType"] == "URL" and $value == "")
        {
            $error = 1;
            $bg = INVALID;
            $value = $_SESSION["userModNames"][$key] . " is required when you select SIP-URI.";
        }
        if ($key == "voiceMessageDeliveryEmailAddress" and isset($_POST["processing"]) and $_POST["processing"] == "Deliver To Email Address Only" and $value == "")
        {
            $error = 1;
            $bg = INVALID;
            $value = $_SESSION["userModNames"][$key] . " is required when you select to have your voice messages forwarded.";
        }
        if ($key == "voiceMessageNotifyEmailAddress")
        {
            if ($_POST["sendVoiceMessageNotifyEmail"] == "true" and $value == "")
            {
                $error = 1;
                $bg = INVALID;
                $value = $_SESSION["userModNames"][$key] . " is required when you select to receive a voice message notification.";
            }
        }
        if ($key == "voiceMessageCarbonCopyEmailAddress")
        {
            if ($_POST["sendCarbonCopyVoiceMessage"] == "true" and $value == "")
            {
                $error = 1;
                $bg = INVALID;
                $value = $_SESSION["userModNames"][$key] . " is required when you select to send a carbon copy of the voice message.";
            }
        }
        if ($key == "transferPhoneNumber")
        {
            if ($_POST["transferOnZeroToPhoneNumber"] == "true" and $value == "")
            {
                $error = 1;
                $bg = INVALID;
                $value = $_SESSION["userModNames"][$key] . " is required when you select to transfer on '0'.";
            }
            else if (strlen($value) > 0 and !preg_match("/^[ 0-9-]+$/", $value))
            {
                $error = 1;
                $bg = INVALID;
                $value = $_SESSION["userModNames"][$key] . " must be numbers, dashes, and spaces only.";
            }
        }
        /*if ($key == "primary" and $value !== "" and strpos($_POST["scaUsers"], $value) == false)
         {
         $error = 1;
         $bg = INVALID;
         $value = $_SESSION["userModNames"][$key] . " cannot be deleted from shared call appearances list.";
         //$value = $_POST["scaUsers"];
         }*/
        
        //check for required fields
        if (in_array($key, $required) and $value == "")
        {
            $bg = INVALID;
            $error = 1;
            $value = $_SESSION["userModNames"][$key] . " is a required field.";
        }
        //if user chose forwarding, make sure there is a number, and it's not in the restricted numbers array
        if (array_key_exists($key, $callForwards))
        {
            if ($value == "true" and $_POST[$callForwards[$key]] == "")
            {
                $bg = INVALID;
                $error = 1;
                $value = "If " . $_SESSION["userModNames"][$key] . " is true, " . $_SESSION["userModNames"][$callForwards[$key]] . " must have a value.";
            }
            foreach ($restrictedNumbers as $k => $v)
            {
                if (substr($_POST[$callForwards[$key]], 0, strlen($k)) === $k and strlen($_POST[$callForwards[$key]]) > strlen($k) + 5)
                {
                    $bg = INVALID;
                    $error = 1;
                    $value = $v . " is restricted.";
                }
            }
        }
        
        if($key == "input-password" && !isset($_POST["resetWebDefault"])){
            
            if(empty($_POST["input-password"]) && empty($_POST["input-confirm-passcode"])){
                /*$error = 1;
                 $bg = INVALID;
                 $value = "Password and Confirm password cannot be None.";*/
                continue;
            }
            
            if(!empty($_POST["input-password"])){
                if ($_POST["input-password"] <> $_POST["input-confirm-password"])
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Password and Confirm password for " . $_SESSION["userModNames"]["webAccessPassword"] . " do not match.";
                }
            }
            if(!empty($_POST["input-confirm-password"])){
                if ($_POST["input-password"] <> $_POST["input-confirm-password"])
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Password and Confirm password for " . $_SESSION["userModNames"]["webAccessPassword"] . " do not match.";
                }
            }
        }
        
        if($key == "input-passcode" && !isset($_POST["resetPassDefault"])){
            
            if(empty($_POST["input-passcode"]) && empty($_POST["input-confirm-passcode"])){
                /*$error = 1;
                 $bg = INVALID;
                 $value = "Passcode and Confirm passcode cannot be None.";*/
                continue;
            }
            
            if(!empty($_POST["input-passcode"])){
                if ($_POST["input-passcode"] <> $_POST["input-confirm-passcode"])
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Passcode and Confirm passcode for " . $_SESSION["userModNames"]["voiceMailPasscode"] . " do not match.";
                }
            }
            
            if(!empty($_POST["input-confirm-passcode"])){
                if ($_POST["input-passcode"] <> $_POST["input-confirm-passcode"])
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Passcode and Confirm passcode for " . $_SESSION["userModNames"]["voiceMailPasscode"] . " do not match.";
                }
            }
        }
        
        if($key == "deviceType" && $value != $_SESSION["userInfo"]["deviceType"]){
            require_once ("/var/www/html/Express/util/deviceNameUtil.php");
            require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
            $getDeviceName = getDeviceNamefromCriteria($deviceNameDID1, $deviceNameDID2);
            
            $deviceOperationObj = new DeviceOperations;
            $checkDeviceNameExist = $deviceOperationObj->GroupAccessDeviceExistanceCheck($_SESSION["sp"], $_SESSION["groupId"], $getDeviceName);
            if(isset($checkDeviceNameExist["Success"][0]["deviceName"]) && !empty($checkDeviceNameExist["Success"][0]["deviceName"])){
                $error = 1;
                $bg = INVALID;
                //$value = "Device already exists. Please delete the device ". $checkDeviceNameExist["Success"][0]["deviceName"];
                $value = "Device already exists. Please delete the device";
            }
            
        }
        
       if($key == "softPhoneDeviceName"){
           $sdArray = array();
           foreach ($value as $sdKey => $sdVal){
               $newval = explode(",", $sdVal);
               $sdArray[] = $newval[0];
           }
           $softVal = implode(', ', $sdArray);
           $value = $softVal;
       }
        
        if ($key == "monitoredUsers" or ($key == "speedDial8" and $value !== $_SESSION["userInfo"][$key]))
        {
            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["userModNames"][$key] . "</td><td class=\"errorTableRows\"><Table class=\"dialogClass\">";
            if ($key == "speedDial8")
            {
                $sD8data = "";
                for ($i = 2; $i <= 9; $i++)
                {
                    if (isset($value[$i]["error"]))
                    {
                        $sD8data .= "<tr><td>Speed Code " . $i . ": " . $value[$i]["error"] . "</td></tr>";
                    }
                    else if ($value[$i]["name"] or $value[$i]["phoneNumber"])
                    {
                        $sD8data .= "<tr><td>Speed Code " . $i . ": " . $value[$i]["phoneNumber"] . " / " . $value[$i]["name"] . "</td></tr>";
                    }
                }
                if ($sD8data != "")
                {
                    $data .= $sD8data;
                }
                else
                {
                    $data .= "<tr><td>None</td></tr>";
                }
            }
            else if ($key == "monitoredUsers")
            {
                if ($_POST["monitoredUsers"] !== "monitoredUsers;")
                {
                    $exp = explode(";", $_POST["monitoredUsers"]);
                    foreach ($exp as $k => $v)
                    {
                        if ($v !== "monitoredUsers" and $v !== "")
                        {
                            
                            $exp2 = explode(":", $v);
                            $name = $exp2[1];
                            $numberExtension = $exp2[2];
                            // $data .= "<tr><td>" . $name . "</td></tr>";
                            $data .= "<tr><td>" . $name . "  (" . $numberExtension . ")". "</td></tr>";
                        }
                    }
                }
                else
                {
                    $data .= "<tr><td>None</td></tr>";
                }
            }
            
            $data .= "</table></td></tr>";
        }else if($key == "scaUsers"){
        	
        	
                $grpServiceList = new Services();
        	
        	
        	//Jeetesh code for check group doesnot have more sca services pack ex-615
        	
        	$grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
        	$servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
        	$servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray);
        	$scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);
        	
        	$oneServicePack = "false";
        	$multipleServicePack = "false";
        	$noServicePack = "false";
        	$r = 0;
        	foreach($scaServiceArray as $ssa=>$ssv){
        		$pos = strpos($ssv->col[0], "Shared Call Appearance");
        		if($pos === false){
        			
        		}else{
        			if($ssv->count == 1){
        				$oneServicePack = "true";
        				$r++;
        				//$countMore = "0";
        			}else if($ssv->count > 1){
        				$multipleServicePack = "true";
        				//$countMore = "true";
        			}else{
        				$noServicePack = "true";
        				//$countMore = "0";
        			}
        		}
        		
        	}
        	
        	//code adds to check service pack cases
        	
        	//code to compare the session and post array to check the assign and unassign value of sca user
        	$sharedCallAppearance = $grpServiceList->compareSessionScaUserForModUser();
        	$postScaUsers = explode(";", $_POST["scaUsers"]);
        	
        	array_pop($postScaUsers);
        	array_pop($postScaUsers);
        	
        	$aa = 0;
        	//make the assign and unassign array for service pack assign to user in EX-615
        	$scaUserOperateArray = $grpServiceList->assignUnassignScaUserForModUser($sharedCallAppearance, $postScaUsers);
        	
        	//code ends
        	
            if ($_POST["scaUsers"] !== "scaUsers;")
            {
            	            	
                //sca services check flag
                $scaArray = array(
                    "Shared Call Appearance",
                    "Shared Call Appearance 10",
                    "Shared Call Appearance 15",
                    "Shared Call Appearance 20",
                    "Shared Call Appearance 25",
                    "Shared Call Appearance 30",
                    "Shared Call Appearance 35",
                    "Shared Call Appearance 5",
                );
                
                $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["userModNames"][$key] . "</td><td class=\"errorTableRows\"><Table class=\"dialogClass\">";
                $exp = explode(";", $_POST["scaUsers"]);
                
                foreach ($exp as $k => $v)
                { 
                    $servicePackToBeAssigned = "";
                    if ($v !== "scaUsers" and $v !== "")
                    { 
                        $exp2 = explode(":", $v);
                        $uid = $exp2[0];
                        $name = $exp2[1];
                        $numberExtension = $exp2[2];
                        $respServices = getUserAssignedUserServices($uid);
                        
                        $scaServiceCountCheck = array_intersect($scaArray, $respServices);
                        
                        if((empty($scaServiceCountCheck) && count($scaServiceCountCheck) == 0) && $oneServicePack == "false"){
                            $bg = INVALID;
                            $error = 1;
                            $data .= "<tr><td style='color:#ff0000'>" . $name . "  (" . $numberExtension . ")". " does not have any SCA User Service assigned.</td></tr>";
                        }else{ 
                        	if($oneServicePack == "true" && $r == "1"){  
                            	$data .= "<tr><td>" . $name . "  (" . $numberExtension . ")". "</td></tr>";
                            	
                        	}else{ 
                        		$nameStr = $name."(".$numberExtension.")";
                        		$nameStr = str_replace(" ", "", $nameStr);
                        		$userservicePackArray = array();
                        		$userServices = new userServices();
                        		$userSessionservicePackArray = $userServices->getUserServicesAll($uid, $_SESSION["sp"]);
                        		$servicesSessionArray = $grpServiceList->getServicePackServicesListUsers($userSessionservicePackArray["Success"]["ServicePack"]["assigned"]);
                        		$scaSessionServiceArray = $grpServiceList->checkScaUserServiceInServicepack($servicesSessionArray);
                        		foreach($scaSessionServiceArray as $ssa=>$ssv){
                        			$pos = strpos($ssv->col[0], "Shared Call Appearance");
                        			if($pos === false){
                        			}else{
                        				$servicePackToBeAssigned = $userSessionservicePackArray["Success"]["ServicePack"]["assigned"][$ssa];
                        			}
                        		}
                        		if(!in_array($nameStr, $scaUserOperateArray["assignNameArray"])){                                                
                        			if(!empty($servicePackToBeAssigned)){
                        				$data .= "<tr><td>" . $name . "  (" . $numberExtension . ")". "</td></tr>";
                        			}else{
                        				$data .= "<tr><td style='color:#ff0000'>Express cannot unambiguously assign Service Pack with SCA service to users(s) : ".$name." (" . $numberExtension . ")</td></tr>";
                        			}
                        		}
                        	}
                        }
                        // $data .= "<tr><td>" . $name . "</td></tr>";
                    }
                }

                if($oneServicePack == "true" && $r == "1"){  
                	
                }else{
	                /*if(count($scaUserOperateArray["unAssign"]) > 0){
	                	foreach($scaUserOperateArray["unAssign"] as $unAssignKey=>$unAssignVal){
	                		if(count($scaServiceArray) > 1){
	                			$data .= "<tr><td style='color:#ff0000'>Express cannot unambiguously unassign Service Pack with SCA service to users(s) : ".$unAssignVal."</td></tr>";
	                		}
	                	}
	                }*/
                }
                $data .= "</Table>";
            }else if($_POST["scaUsers"] == "scaUsers;" && count($scaUserOperateArray["unAssign"]) > 0){
            	if(count($scaUserOperateArray["unAssign"]) > 0)
            	{
            		//Code added @ 30 Aug 2018
            		$scaArray = array(
            				"Shared Call Appearance",
            				"Shared Call Appearance 10",
            				"Shared Call Appearance 15",
            				"Shared Call Appearance 20",
            				"Shared Call Appearance 25",
            				"Shared Call Appearance 30",
            				"Shared Call Appearance 35",
            				"Shared Call Appearance 5",
            		);
            		foreach ($scaUserOperateArray["unAssign"] as $k => $tmpVal)
            		{
            			if ($tmpVal !== "scaUsers" and $tmpVal !== "")
            			{
            				$exp2 = explode(":", $tmpVal);
            				$uid = $exp2[0];
            				$name = $exp2[1];
            				$numberExtension = $exp2[2];
            				$respServices = getUserAssignedUserServices($uid);
            				
            				$scaServiceCountCheck = array_intersect($scaArray, $respServices);
            				if((empty($scaServiceCountCheck) && count($scaServiceCountCheck) == 0)){
            					$bg = INVALID;
            					$error = 1;
            					$data .= "<tr><td class=\"errorTableRows\" style=\"background:#D52B1E;\">".$_SESSION["userModNames"][$key]."</td><td style='color:#ff0000'>" . $name . "  (" . $numberExtension . ")". " does not have any SCA User Service assigned.</td></tr>";
            					
            				}else{
            					
            					$nameStr = $name."(".$numberExtension.")";
            					$nameStr = str_replace(" ", "", $nameStr);
            					$userservicePackArray = array();
            					$userServices = new userServices();
            					$userSessionservicePackArray = $userServices->getUserServicesAll($uid, $_SESSION["sp"]);
            					
            					
            					//$userServicePackArr      = $userSessionservicePackArray["Success"]["ServicePack"]["assigned"];
            					//$scaServicePackArr       = $userServices->getServicePacksListHaveScaServices($userServicePackArr);
            					//$scaServicePackCustomArr = $userServices->getServicePacksHasOneScaServiceOnly($userServicePackArr);
                                                
                                                
                                                $userServicePackArr      = $userSessionservicePackArray["Success"]["ServicePack"]["assigned"];
            					$scaServicePackArr       = $userServices->getServicePacksListHaveScaServices($userServicePackArr);
            					$scaServicePackCustomArr = $userServices->getServicePacksHasOneScaServiceOnly($scaServicePackArr);
                                                
                                                
            					
            					$serverPacksWithOneSCAService = "";
            					$servicePackSCACounter = 0;
            					foreach($scaServicePackCustomArr as $keyTmpN => $valTmpN)
            					{
            						if($valTmpN['hasOneSCAServiceOnly']=="true")
            						{
            							$servicePackSCACounter = $servicePackSCACounter + 1;
            						}
            					}
            					if($servicePackSCACounter>1)
            					{
            						$bg = INVALID;
            						$error = 1;
            						$data .= "<tr><td class=\"errorTableRows\" style=\"background:#D52B1E;\">".$_SESSION["userModNames"][$key]."</td><td style='color:#ff0000'>Express cannot unambiguously unassign Service Pack with SCA service to users(s) : ".$tmpVal."</td></tr>";
            					}
            					else
            					{
            						$data .= "<tr><td class=\"errorTableRows\" style=\"background:#72ac5d;\">Shared Call Appearances</td><td>None</td></tr>";
            					}
            					
            				}
            			}
            		}
            		//End code
            		
            	}
            }
            else
            {
                $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Shared Call Appearances<td>None</td></tr>";
            }
            
            
        }
        else
        {
            if ($key !== "userChoice")
            {
                //only show changes or errors, don't show every field
                if($license["customProfile"] == "true"){
                    // grt user custom tag
                    if($key == "customProfile" && $value !=""){
                        if($value != $_SESSION["userInfo"]["customProfile"]){
                            require_once ("/var/www/lib/broadsoft/adminPortal/customTagManagement.php");
                            $customTagManager = new CustomTagManagement();
                            $response = $customTagManager->getUserCustomTags($_SESSION["userInfo"]["deviceName"]);
                            $existingCustomTagList= $response["Success"];
                            $value1=implode(", ", $existingCustomTagList);
                            //$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\"> Custom Tag will be deleted</td><td class=\"errorTableRows\">" . $value1. "</td></tr>";
                            //$changes++;
                            //$bg = CHANGED;
                        }
                    }
                }
                
                
                if ($bg != UNCHANGED) //unchanged background color constant
                {
                    $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" .$_SESSION["userModNames"][$key] . "</td><td class=\"errorTableRows\">" . ($value == "" ? "None" : $value) . "</td></tr>";
                }
            }
        }
    }
}

$data .= "</table>";


if ($changes == 0)
{
    $error = 1;
    $data = "You have made no changes.";
}

echo $error . $data;


/*=======================================================================================================================*/

function processCustomTagArr($usersProvisionedCustomTags ,$usersAvailableCustomTags){
    
    $customTag = array();
    foreach ($usersAvailableCustomTags as $key => $Value){
        if( ! isset($Value['groupLevel']) ) {
            if(! isset($usersProvisionedCustomTags[$key]) ) {
                $customTag["add"][] =    $usersAvailableCustomTags[$key];
            } else if( isset($usersProvisionedCustomTags[$key]) && isset($usersProvisionedCustomTags[$key]['groupLevel']) && ! isset($usersAvailableCustomTags[$key]['groupLevel']) ) {
                $customTag["add"][] =    $usersAvailableCustomTags[$key];
            }
            else if(isset($usersProvisionedCustomTags[$key]) && $usersAvailableCustomTags[$key]["customTagValue"] != $usersProvisionedCustomTags[$key]["customTagValue"]) {
                $customTag["modify"][] = $Value;
            }
        }
    }
    
    foreach ($usersProvisionedCustomTags as $key => $Value1){
        if( ! isset($Value1['groupLevel']) && ! isset($usersAvailableCustomTags[$key]))
        {
            $customTag["delete"][] = $Value1;
        }
        
    }
    return  $customTag;
}
?>
