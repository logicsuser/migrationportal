<?php 
if(!array_key_exists("useGroupName", $_POST)){
    $_POST['useGroupName'] = "false";
}
if(!array_key_exists("allowAlternateNumbersForRedirectingIdentity", $_POST)){
    $_POST['allowAlternateNumbersForRedirectingIdentity'] = "false";
}
if(!array_key_exists("allowConfigurableCLIDForRedirectingIdentity", $_POST)){
    $_POST['allowConfigurableCLIDForRedirectingIdentity'] = "false";
}
if(!array_key_exists("blockCallingNameForExternalCalls", $_POST)){
    $_POST['blockCallingNameForExternalCalls'] = "false";
}
if(!array_key_exists("blockCallingNameForExternalCalls", $_POST)){
    $_POST['blockCallingNameForExternalCalls'] = "false";
}
if(!array_key_exists("useMaxSimultaneousCalls", $_POST)){
    $_POST['useMaxSimultaneousCalls'] = "false";
}
if(!array_key_exists("useMaxSimultaneousVideoCalls", $_POST)){
    $_POST['useMaxSimultaneousVideoCalls'] = "false";
}
if(!array_key_exists("useMaxCallTimeForAnsweredCalls", $_POST)){
    $_POST['useMaxCallTimeForAnsweredCalls'] = "false";
}
if(!array_key_exists("useMaxCallTimeForUnansweredCalls", $_POST)){
    $_POST['useMaxCallTimeForUnansweredCalls'] = "false";
}
if(!array_key_exists("useMaxConcurrentRedirectedCalls", $_POST)){
    $_POST['useMaxConcurrentRedirectedCalls'] = "false";
}
if(!array_key_exists("useMaxConcurrentFindMeFollowMeInvocations", $_POST)){
    $_POST['useMaxConcurrentFindMeFollowMeInvocations'] = "false";
}
if(!array_key_exists("useMaxFindMeFollowMeDepth", $_POST)){
    $_POST['useMaxFindMeFollowMeDepth'] = "false";
}
if(!array_key_exists("allowDepartmentCLIDNameOverride", $_POST)){
    $_POST['allowDepartmentCLIDNameOverride'] = "false";
}

$deviceTypeArrForCheck = array(0 => "Polycom_VVX310", 1 => "Polycom_VVX410", 2 => "Polycom_VVX500", 3 => "Polycom_VVX600"); 

$softArray = array(
    'deviceName' => 'Soft Phone Device Name',
    'deviceType' => 'Soft Phone Device Type',
    'softPhoneDeviceTypeLinePort' => 'Soft Phone Line Port Domain',
    'softPhoneDeviceAccUserName' => 'Soft Phone Device Access User Name',
    'softPhoneDeviceAccUserPass' => 'Soft Phone Accees Password',
    'accountUserName' => 'Account User Name',
    'accountProfile' => 'Account Profile',
    'softPhoneAccountEmailAddress' => 'Soft Phone Email Address',
    "rebuildSoftPhoneFiles" => "Rebuild Soft Phone Files",
    "resetSoftPhone" => "Reset Soft Phone",
    "linePort" => "Line/Port"
);

$scaArray = array(
    "Shared Call Appearance",
    "Shared Call Appearance 10",
    "Shared Call Appearance 15",
    "Shared Call Appearance 20",
    "Shared Call Appearance 25",
    "Shared Call Appearance 30",
    "Shared Call Appearance 35",
    "Shared Call Appearance 5",
);

?>