<?php
	require_once("/var/www/html/Express/config.php");
	server_fail_over_debuggin_testing();      /* Only for testing purpose. */
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	//print_r($_POST);exit;
	require_once("/var/www/lib/broadsoft/adminPortal/GroupCallPickupUsers.php");
	require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once("/var/www/lib/broadsoft/adminPortal/ErrorHandlingUtil.php");
    require_once("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");
    require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/LineportNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/VoicePasswordNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/UserServiceGetAssignmentList.php");
    require_once("/var/www/lib/broadsoft/adminPortal/ModifyBLFService.php");
    require_once("/var/www/lib/broadsoft/adminPortal/ModifyVMService.php");
    require_once("/var/www/lib/broadsoft/adminPortal/GetUserCallProcessingInfo.php");
    require_once ("/var/www/html/Express/userMod/arrayData.php");
    require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
    require_once("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
    require_once("/var/www/lib/broadsoft/adminPortal/getGroupClid.php");
    require_once("/var/www/lib/broadsoft/adminPortal/userPassword.php");
    $userPassword = new userPassword();
    
    //Code added @ 12 July 2018
    require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");   
    $srvceObj = new Services();
    //End code
    require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailOperations.php"); //Added 12 May 2018
    require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/services/userServices.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/util/scaUtility.php");
    require_once("/var/www/html/Express/userMod/UserCustomTagManager.php");
    require_once ("/var/www/html/Express/userMod/UserModifyUtil/ServiceAndServicePackUtil.php");
    require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
    $expProvDB = new ExpressProvLogsDB();
    //Code added @ 21 Nov 2018
    require_once ("/var/www/lib/broadsoft/adminPortal/simRingCriteriaManagement.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
    $objSimRingMgmt = new SimRingCriteriaManagement();
    
    $scaUtilityObj = new SCAUtility();
    $vdmOperationObj = new VdmOperations();
    
    $syslevelObj = new sysLevelDeviceOperations();
    $syslevelArr = $syslevelObj->getSysytemConfigDeviceTypeRequest($_SESSION["userInfo"]["deviceType"], $ociVersion);
    $systemLinePortFlag = false;
    $scaUtilityData = "";
    if(isset($syslevelArr["Success"]->staticLineOrdering)){
        $systemLinePortFlag = strval($syslevelArr["Success"]->staticLineOrdering);
    }

    $deviceDetails = $vdmOperationObj->getUserDeviceDetail($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["userInfo"]["deviceName"]);

    
    $grpServiceList = new Services();

    if(array_key_exists("servicePack", $_POST)){
    	$_POST['servicePack']= explode(";", $_POST['servicePack']);
    	array_pop($_POST['servicePack']);
    }
    
	$gcpChangeLog = array();
	$initialUserServicesAssignmentList = null;
    $surgeMailUserId = "";
	function determineVMServicePacks() {
	    global $servicePacks, $sessionid, $client;

        // Create list of Service Packs containing 'Voice Messaging User' service
        require_once("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");
        unset($_SESSION["vmServicePacks"]);
        $i = 0;
        foreach ($servicePacks as $servicePack) {
            $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]). "</serviceProviderId>";
            $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

            foreach ($xml->command->userServiceTable->row as $key => $value){
                if (strval($value->col[0]) == "Voice Messaging User") {
                    $_SESSION["vmServicePacks"][$i++] = $servicePack;
                    break;
                }
            }
        }
        if (isset($_SESSION["vmServicePacks"])) {
            $_SESSION["vmServicePacksFlat"] = join(",", $_SESSION["vmServicePacks"]);
        }
    }

    function getPrimaryUserAttribute($userId, $attribute) {
	    global $users;

        for ($i = 0; $i < count($users); $i++) {
	        if ($users[$i]["id"] == $userId) {
	            return $users[$i][$attribute];
            }
        }
        return "";
    }
    function searchForId($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['id'] == $id) {
                return true;
            }
        }
        return false;
    }

	function getResourceForVoicePasswordCreation($attr, $value) {
        switch ($attr) {
            case ResourceNameBuilder::DN:       return isset($_SESSION["userInfo"]["phoneNumber"]) ? $_SESSION["userInfo"]["phoneNumber"] : "";
            case ResourceNameBuilder::EXT:      return $_SESSION["userInfo"]["extension"];
            case ResourceNameBuilder::USR_CLID: return isset($_SESSION["userInfo"]["callingLineIdPhoneNumber"]) ? $_SESSION["userInfo"]["callingLineIdPhoneNumber"] : "";
            case ResourceNameBuilder::GRP_DN:   return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:   return $_SESSION["groupId"];
            case ResourceNameBuilder::MAC_ADDR: return isset($_SESSION["userInfo"]["macAddress"]) ? $_SESSION["userInfo"]["macAddress"] : "";
        }
    }

    function getResourceForLineportCreation($attr) {
        switch ($attr) {
            //case ResourceNameBuilder::DN:               return isset($_SESSION["userInfo"]["phoneNumber"]) ? $_SESSION["userInfo"]["phoneNumber"] : "";
            //case ResourceNameBuilder::EXT:              return isset($_SESSION["userInfo"]["extension"]) ? $_SESSION["userInfo"]["extension"] : "";
            case ResourceNameBuilder::USR_CLID:         return isset($_SESSION["userInfo"]["callingLineIdPhoneNumber"]) ? $_SESSION["userInfo"]["callingLineIdPhoneNumber"] : "";
            case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:           return $_SESSION["groupId"];
            case ResourceNameBuilder::SYS_DFLT_DOMAIN:  return $_SESSION["systemDefaultDomain"];
            case ResourceNameBuilder::DEFAULT_DOMAIN:   return trim($_SESSION["defaultDomain"]);
            case ResourceNameBuilder::GRP_PROXY_DOMAIN: return trim($_SESSION["proxyDomain"]);
            case ResourceNameBuilder::FIRST_NAME:       return isset($_SESSION["userInfo"]["firstName"])  ? $_SESSION["userInfo"]["firstName"] : "";
            case ResourceNameBuilder::LAST_NAME:        return isset($_SESSION["userInfo"]["lastName"]) ? $_SESSION["userInfo"]["lastName"] : "";
            case ResourceNameBuilder::DEV_NAME:         return isset($_SESSION["userInfo"]["lastName"]) && $_SESSION["userInfo"]["deviceName"] != "" ? $_SESSION["userInfo"]["deviceName"] : "";
            case ResourceNameBuilder::MAC_ADDR:         return isset($_SESSION["userInfo"]["macAddress"]) && $_SESSION["userInfo"]["macAddress"] != "" ? $_SESSION["userInfo"]["macAddress"] : "";
            default:                                    return "";
        }
    }

    function getResourceForNameCreation($attr) {
	    global $surgemailDomain;

        switch ($attr) {
            case ResourceNameBuilder::DN:               return isset($_SESSION["userInfo"]["phoneNumber"]) ? $_SESSION["userInfo"]["phoneNumber"] : "";
            case ResourceNameBuilder::EXT:              return isset($_SESSION["userInfo"]["extension"]) ? $_SESSION["userInfo"]["extension"] : "";
            case ResourceNameBuilder::USR_CLID:         return isset($_SESSION["userInfo"]["callingLineIdPhoneNumber"]) ? $_SESSION["userInfo"]["callingLineIdPhoneNumber"] : "";
            case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:           return $_SESSION["groupId"];
            case ResourceNameBuilder::SYS_DFLT_DOMAIN:  return $_SESSION["systemDefaultDomain"];
            case ResourceNameBuilder::DEFAULT_DOMAIN:   return trim($_SESSION["defaultDomain"]);
            case ResourceNameBuilder::SRG_DOMAIN:       return $surgemailDomain;
            case ResourceNameBuilder::GRP_PROXY_DOMAIN: return trim($_SESSION["proxyDomain"]);
            case ResourceNameBuilder::FIRST_NAME:       return isset($_SESSION["userInfo"]["firstName"])  ? $_SESSION["userInfo"]["firstName"] : "";
            case ResourceNameBuilder::LAST_NAME:        return isset($_SESSION["userInfo"]["lastName"]) ? $_SESSION["userInfo"]["lastName"] : "";
            case ResourceNameBuilder::DEV_NAME:         return isset($_SESSION["userInfo"]["lastName"]) && $_SESSION["userInfo"]["deviceName"] != "" ? $_SESSION["userInfo"]["deviceName"] : "";
            case ResourceNameBuilder::MAC_ADDR:         return isset($_SESSION["userInfo"]["macAddress"]) && $_SESSION["userInfo"]["macAddress"] != "" ? $_SESSION["userInfo"]["macAddress"] : "";
            default:                                    return "";
        }
    }

    // Create surgemail username from the first formula.
    // Enforce fallback if there is no second criteria/formula.
    function getSurgeMailAddress() {
	    global $surgeMailIdCriteria1, $surgeMailIdCriteria2;

        $surgeMailNameBuilder1 = new SurgeMailNameBuilder($surgeMailIdCriteria1,
            $_SESSION["userInfo"]["phoneNumber"],
            $_SESSION["userInfo"]["extension"],
            $_SESSION["userInfo"]["callingLineIdPhoneNumber"],
            isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "",
            $_SESSION["systemDefaultDomain"],
            $_SESSION["userInfo"]["firstName"],
            $_SESSION["userInfo"]["lastName"],
            $surgeMailIdCriteria2 == "");    // force fallback only if second criteria does not exist
        if (! $surgeMailNameBuilder1->validate()) {}
        do {
            $attr = $surgeMailNameBuilder1->getRequiredAttributeKey();
            $surgeMailNameBuilder1->setRequiredAttribute($attr, getResourceForNameCreation($attr));
        } while ($attr != "");

        $surgemailName = $surgeMailNameBuilder1->getName();
        if ($surgemailName == "") {
            // Create surgemail name from the second formula. At this point we know that there is second formula,
            // because without second formula name would be forcefully resolved in the first formula.

            $surgeMailNameBuilder2 = new SurgeMailNameBuilder($surgeMailIdCriteria2,
                $_SESSION["userInfo"]["phoneNumber"],
                $_SESSION["userInfo"]["extension"],
                $_SESSION["userInfo"]["callingLineIdPhoneNumber"],
                isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "",
                $_SESSION["systemDefaultDomain"],
                $_SESSION["userInfo"]["firstName"],
                $_SESSION["userInfo"]["lastName"],
                true);
            if (! $surgeMailNameBuilder2->validate()) {}
            do {
                $attr = $surgeMailNameBuilder2->getRequiredAttributeKey();
                $surgeMailNameBuilder2->setRequiredAttribute($attr, getResourceForNameCreation($attr));
            } while ($attr != "");

            $surgemailName = $surgeMailNameBuilder2->getName();
        }
        return $surgemailName;
    }

    // Build Voice Mail Password. Enforce fallback
    function getVoicePortalPassCode() {
	    global $bwVoicePortalPasswordType, $userVoicePortalPasscodeFormula;

        if ($bwVoicePortalPasswordType == "Formula") {
            $voicePasswordBuilder = new VoicePasswordNameBuilder($userVoicePortalPasscodeFormula,
                $_SESSION["userInfo"]["phoneNumber"],
                $_SESSION["userInfo"]["extension"],
                $_SESSION["userInfo"]["callingLineIdPhoneNumber"],
                isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "",
                true);
            if (! $voicePasswordBuilder->validate()) {}
            do {
                $attr = $voicePasswordBuilder->getRequiredAttributeKey();
                $voicePasswordBuilder->setRequiredAttribute($attr, getResourceForNameCreation($attr));
            } while ($attr != "");

            $portalPassWord = $voicePasswordBuilder->getName();
        } else {
            $portalPassWord = setVoicePassword();
        }
        $portalPassWord = setVoicePassword();  //Added by Anshu due to the postcode error (Must be numeric) @05 March 2018 
        return $portalPassWord;
    }

    function getSCALinePortName($scaPhoneNumber, $scaExtension) {
        global $scaLinePortCriteria1, $scaLinePortCriteria2;

        if ($scaPhoneNumber != "") {
            if (substr($scaPhoneNumber,0,3) == "+1-") {
                $scaPhoneNumber = substr($scaPhoneNumber, 3);
            }
        }

        $linePort = "";
        $linePortBuilder1 = new LineportNameBuilder($scaLinePortCriteria1,
            $scaPhoneNumber,
            $scaExtension,
            getResourceForLineportCreation(ResourceNameBuilder::USR_CLID),
            getResourceForLineportCreation(ResourceNameBuilder::GRP_DN),
            getResourceForLineportCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
            $scaLinePortCriteria2 == "");  // force fallback only if second criteria does not exist

        if (! $linePortBuilder1->validate()) {
            // TODO: Implement resolution when lineport input formula has errors
        }

        do {
            $attr = $linePortBuilder1->getRequiredAttributeKey();
            $linePortBuilder1->setRequiredAttribute($attr, getResourceForLineportCreation($attr));
        } while ($attr != "");

        $linePort = $linePortBuilder1->getName();

        if ($linePort == "") {
            // Create lineport name builder from the second formula. At this point we know that there is second
            // formula, because without second formula name would be forcefully resolved in the first formula.

            $linePortBuilder2 = new LineportNameBuilder($scaLinePortCriteria2,
                $scaPhoneNumber,
                $scaExtension,
                getResourceForLineportCreation(ResourceNameBuilder::USR_CLID),
                getResourceForLineportCreation(ResourceNameBuilder::GRP_DN),
                getResourceForLineportCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
                true);

            if (! $linePortBuilder2->validate()) {
                // TODO: Implement resolution when User ID input formula has errors
            }

            do {
                $attr = $linePortBuilder2->getRequiredAttributeKey();
                $linePortBuilder2->setRequiredAttribute($attr, getResourceForLineportCreation($attr));
            } while ($attr != "");

            $linePort = $linePortBuilder2->getName();
        }

        return str_replace(" ", "_", $linePort);
    }

        
    //express License premssion set value custom Profile/Phone Profile
    if(!isset($_POST["customProfile"]) && $license["customProfile"] =="false"){
        $_POST["customProfile"] = "None";
    }
        $userChange = array("firstName", "lastName", "callingLineIdFirstName", "callingLineIdLastName", "emailAddress", "callingLineIdPhoneNumber", "timeZone", "networkClassOfService", "addressLocation");
	
        if($ociVersion=="20" || $ociVersion=="21" || $ociVersion=="22"){
            $userChange[] = "nameDialingFirstName";
            $userChange[] = "nameDialingLastName";        
        }
        
        $department = array("department");
	$addressChange = array("addressLine1", "addressLine2", "city", "stateOrProvince", "zipOrPostalCode");
	$speedDial8Change = array("speedDial8");
	$speedDial100Change = array("speedCode");
	$voiceManagementChange = array(    
        "resetPortal",
		"isActive",
		"alwaysRedirectToVoiceMail",
		"busyRedirectToVoiceMail",
		"noAnswerRedirectToVoiceMail",
		"serverSelection",
		"userServer",
		"mailboxIdType",
		"mailboxURL",
		"noAnswerNumberOfRings",
		"processing",
		"usePhoneMessageWaitingIndicator",
		"voiceMessageDeliveryEmailAddress",
		"sendVoiceMessageNotifyEmail",
		"voiceMessageNotifyEmailAddress",
		"sendCarbonCopyVoiceMessage",
		"voiceMessageCarbonCopyEmailAddress",
		"transferOnZeroToPhoneNumber",
		"transferPhoneNumber"
	);
	$mailBoxLimitChanges = array("mailBoxLimit");
	$hotelHostChange = array("hotelHostisActive");
	$hotelChange = array("hotelisActive", "hotelHostId");
	$phoneChange = array("phoneNumber", "extension");
	$activateNumber = array("uActivateNumber");
	$thirdPartyChange = array("thirdPartyVoiceMail");
	$deviceChange = array("macAddress", "deviceType", "customProfile", "rebuildPhoneFiles", "resetPhone", "modTagBundle", "linePortDomainResult");
	$callTransferChange = array(
		"isRecallActive",
		"recallNumberOfRings",
		"useDiversionInhibitorForBlindTransfer",
		"useDiversionInhibitorForConsultativeCalls",
		"enableBusyCampOn",
		"busyCampOnSeconds"	    
	);
	$cfaChange = array("cfaActive", "cfaForwardToPhoneNumber", "cfaIsRingSplashActive");
	$cfbChange = array("cfbActive", "cfbForwardToPhoneNumber");
	$cfnChange = array("cfnActive", "cfnForwardToPhoneNumber", "cfnNumberOfRings");
	$cfrChange = array("cfrActive", "cfrForwardToPhoneNumber");
	$dndChange = array("dnd", "dndRingSplash");
	$simultaneousRingChange = array("simultaneousRingIsActive", "doNotRingIfOnCall");
	$primaryChange = array("primary");
	$callingLineIdPolicyChange = array("useUserCLIDSetting","clidPolicy","emergencyClidPolicy", "useGroupName", "allowDepartmentCLIDNameOverride",
	    "allowAlternateNumbersForRedirectingIdentity", "allowConfigurableCLIDForRedirectingIdentity", "blockCallingNameForExternalCalls",
	    "useUserCallLimitsSetting", "useMaxSimultaneousCalls", "maxSimultaneousCalls", "useMaxSimultaneousVideoCalls", "maxSimultaneousVideoCalls", "useMaxCallTimeForAnsweredCalls",
	    "maxCallTimeForAnsweredCallsMinutes", "useMaxCallTimeForUnansweredCalls", "maxCallTimeForUnansweredCallsMinutes", "useMaxConcurrentRedirectedCalls", "maxConcurrentRedirectedCalls","useMaxConcurrentFindMeFollowMeInvocations", "maxConcurrentFindMeFollowMeInvocations",
	    "useMaxFindMeFollowMeDepth", "maxFindMeFollowMeDepth", "maxRedirectionDepth", "useUserDCLIDSetting", "enableDialableCallerID", "allowDepartmentCLIDNameOverride"
	);
	$cpgGroupName= array("cpgGroupName");
	
	$ocpSettings = array("userPermissionsGroup", "userPermissionsLocal", "userPermissionsTollfree", "userPermissionsToll", "userPermissionsInternational", "userPermissionsOperatorAssisted", "userPermissionsOperatorChargDirAssisted", "userPermissionsSpecialService1", "userPermissionsSpecialService2", "userPermissionsPremiumServices1", "userPermissionsPremiumServices2", "userPermissionsCasual", "userPermissionsUrlDialing", "userPermissionsUnknown", "useCustomSettings");
	
	$policyChange = array(
	    
	);
    $postArray = $_POST;
	$postUserId = $userId = $_POST["userId"];
	unset($_POST["userId"]);

	$deviceName = isset($_SESSION["userInfo"]["deviceName"]) ? $_SESSION["userInfo"]["deviceName"] : "";

	if(!array_key_exists("uActivateNumber", $_POST)){
		$_POST['uActivateNumber'] = "No";
	}
	$message = "<label class=\"labelTextGrey\" style=\"font-size:14px;\">The following changes are complete:</label><br><br>";
	$message .= "<ul class=\"uiDialogLabel labelTextGrey\">";

	
	function checkAnyChangesInCallControl($cfaChange, $cfnChange, $dndChange, $postArray)
	{
	    $isCfaChange = 0;
	    $isCfnChange = 0;
	    $isDndChange = 0;
// 	    print_r($_SESSION["userInfo"]); exit;
	    foreach ($cfaChange as $key => $value)
	    {
	        if($postArray[$value] != $_SESSION["userInfo"][$value]) {
	            $isCfaChange = 1;
	        }
	        if($isCfaChange){ break; }
	    }
	  //  print_r($isCfaChange); exit;
	    foreach ($cfnChange as $key => $value)
	    {
	        if($postArray[$value] != $_SESSION["userInfo"][$value]) {
	            $isCfnChange = 1;
	        }
	        if($isCfnChange){ break; }
	    }
	    foreach ($dndChange as $key => $value)
	    {
	        if($postArray[$value] != $_SESSION["userInfo"][$value]) {
	            $isDndChange = 1;
	        }
	        if($isDndChange){ break; }
	    }
	    
	    return $isCfaChange || $isCfnChange || $isDndChange;
	}
	
	/* Start User's Custom Tag Operations*/
	$resultCustomTagArr =  processCustomTagArr($_SESSION['usersProvisionedCustomTags'], $_SESSION['assignCustomTagToUserForTable']);
	if(! empty($resultCustomTagArr)) {
	    $customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
	    $resultCustomTagArr = $customTags->customTagOperation($resultCustomTagArr);
	    $customTagsRebuiltResponse = $customTags->resetAndRebuiltDevice($_POST, $_SESSION["userInfo"]["deviceName"]);
	     
	    $changeLogRes = $customTags->userCustomTagChangeLogOperations($resultCustomTagArr, $postUserId);
	    
	    $message .= $customTags->statusMessageForCustomTag($resultCustomTagArr, $customTagsRebuiltResponse);
	    if($customTagsRebuiltResponse) {
	        $message .= "<li> After Custom Tag Operations : Rebuild Phone Files has been changed to true.</li>";
	        $message .= "<li> After Custom Tag Operations : Reset the Phone has been changed to true.</li>";
	    }
	}
	
	/* End User's Custom Tag Operations*/
	
	$hasCallControlChanged = checkAnyChangesInCallControl($cfaChange, $cfnChange, $dndChange, $postArray);
	if(isset($_POST["deviceTypeResult"])) {
	    $_POST["deviceType"] = $_POST["deviceTypeResult"];
    }
    unset($_POST["deviceTypeResult"]);
    
    // For Clear MAC Address (Device not posted from front end).
    if(isset($_POST["macAddressClearMac"])) {
        $_POST["deviceType"] = $_SESSION["userInfo"]["deviceType"];
    }
    if(isset($_POST["doClearMacAddress"]) && $_POST["doClearMacAddress"] == "true") {
        unset($_POST["doClearMacAddress"]);
        unset($_POST["macAddressClearMac"]);
        $_POST["rebuildPhoneFiles"] = "true";
        $_POST["resetPhone"] = "true";
        
    } else if(isset($_POST["doClearMacAddress"]) && $_POST["doClearMacAddress"] == "false") {
        unset($_POST["doClearMacAddress"]);
        unset($_POST["macAddressClearMac"]);
        unset($_POST["macAddress"]);
    }    
    
    if(isset($_POST["userTypeResult"]) && $_POST["userTypeResult"] == "analog" && isset($_POST["portNumber"]) && $_POST["portNumber"] != "") {
        $_POST["portNumber"] = $_POST["portNumber"];
    } else if( (isset($_POST["userTypeResult"]) && $_POST["userTypeResult"] != "analog") && 
        (isset($_POST["portNumberSIP"]) && $_POST["portNumberSIP"] != "") ) 
    {
        $_POST["portNumber"] = $_POST["portNumberSIP"];
    } else if( $_POST["userTypeResult"] == "digital" && $_POST["supportPortNumberType"] == "static" && !isset($_POST["portNumberSIP"]) ) {
        $_POST["portNumber"] = $_SESSION["userInfo"]["portNumber"];
    }
    
    /* Service Pack Operations Start*/
    if( isset($_POST["assignedUserServicesVal"]) ) {
        $changes["userServices"] = "userServices";
    }
    
	foreach ($_POST as $key => $value)
	{
		//check if submitted values differ from previous values; if so, add them to list of changes
		
	    if ((isset($_SESSION["userInfo"][$key]) and $value !== $_SESSION["userInfo"][$key]) or !isset($_SESSION["userInfo"][$key]))
		{
                    
		    if ($key == "portNumber" && $_SESSION["userInfo"][$key] != $value)
		    {
		        $changes[$key] = $value;
		    }
		    if ($key == "deviceIndex" && $_SESSION["userInfo"][$key] != $value)
		    {
		        $changes[$key] = $value;
		    }
                    //Code added @ 12 Sep 2018
                    if ($key == "ccd" && $_SESSION["userInfo"][$key] != $value)
		    {
		        $changes[$key] = $value;
		    }
                    //End code

                    if ($key == "simultaneousRingIsActive" && $_SESSION["userInfo"][$key] != $value){
                         $changes["simultaneousRingPhoneNumber"] = $value;
                    }
                    if ($key == "doNotRingIfOnCall" && $_SESSION["userInfo"][$key] != $value){
                         $changes["simultaneousRingPhoneNumber"] = $value;
                    }
                    if ($key == "simultaneousRingPhoneNumber"){
                        $SimRingArrForChngLog = array();
                        $simRingArr = array();
                        for ($c = 0; $c < 10; $c++){ 
                            if($_POST["simultaneousRingPhoneNumber"][$c]!=""){
                                $simRingArr = $_POST["simultaneousRingPhoneNumber"][$c];
                                $simRingReq = $_POST["answerConfirmationRequired"][$c] ? $_POST["answerConfirmationRequired"][$c] : 'false';
                                $SimRingArrForChngLog[$c] = $_POST["simultaneousRingPhoneNumber"][$c]."-".$simRingReq;
                            }
                        }
                        
                       //check if list of simultaneous ring numbers has changed; if so, make note of changes for logging purposes
                        if(!empty($simRingArr))
                        {
                            $oldNumbers      = "";
                            $newNumbers      = "";
                            for ($e = 0; $e < 10; $e++)
                            {
                                if (isset($_SESSION["userInfo"]["simultaneousRingNumber"]))
                                {
                                    $oldNumbers .= $_SESSION["userInfo"]["simultaneousRingNumber"][$e]["phoneNumber"] . ":" . ($_SESSION["userInfo"]["simultaneousRingNumber"][$e]["answerConfirmationRequired"] == "false" ? "" : $_SESSION["userInfo"]["simultaneousRingNumber"][$e]["answerConfirmationRequired"]) . "; ";
                                }
                                $newNumbers .= $_POST["simultaneousRingPhoneNumber"][$e] . ":" . $_POST["answerConfirmationRequired"][$e]. "; ";
                            }
                            if ($oldNumbers !== $newNumbers)
                            {
                                $changes[$key] = $SimRingArrForChngLog;
                            }
                        }
                        //End code
                        
                        
		    }
                    if ($key == "criteriaNameDup"){
                        //$changes["simultaneousRingPhoneNumber"] = "yes";                        
                        //Check if criteria is added
                        if(isset($_SESSION['addSimRingCriteria']) && !empty($_SESSION['addSimRingCriteria'])){
                            $change = "yes";    
                        }
                        //Check if criteria is edited
                        if(isset($_SESSION['editSimRingCriteria']) && !empty($_SESSION['editSimRingCriteria'])){
                            $change = "yes";    
                        }
                        //Check if criteria is deleted
                        if(isset($_SESSION['deleteSimRingCriteria']) && !empty($_SESSION['deleteSimRingCriteria'])){
                            $change = "yes";    
                        }
                        //Check applied & unapplied criteria for Sim Ring
                        $aCrtaCount  = count($_SESSION["userInfo"]["activeCriteriaNameList"]);
                        $pCrtaCount  = count($_POST['criteriaName']);
                        if($aCrtaCount>$pCrtaCount){
                            $criteriaNameDiff = array_diff($_SESSION["userInfo"]["activeCriteriaNameList"], $_POST['criteriaName']);
                        }
                        else{
                            $criteriaNameDiff = array_diff($_POST['criteriaName'], $_SESSION["userInfo"]["activeCriteriaNameList"]);
                        }
                        if(empty($_POST['criteriaName']) && !empty($_SESSION["userInfo"]["activeCriteriaNameList"])){
                            $change = "yes";  
                        }
                        if(!empty($criteriaNameDiff)){   
                            $change = "yes";
                        }                                           
                        if($change=="yes"){
                            $changes["simultaneousRingPhoneNumber"] = "yes";
                        }
                        //End code     
                    }
                    
                    //End code
                    if ($key == "portNumber" && $_SESSION["userInfo"][$key] != $value)
		    {
		        $changes[$key] = $value;
		    }
		    
		    if (in_array($key, $userChange))
			{
				$changes["user"][$key] = $value;
			}
			if (in_array($key, $department))
			{
				$changes["department"][$key] = $value;
			}
			if (in_array($key, $addressChange))
			{
				$changes["address"][$key] = $value;
			}
			if (in_Array($key, $speedDial8Change))
			{
				$changes["speedDial8"][$key] = $value;
			}
			if (in_Array($key, $speedDial100Change))
			{
				$sD100Old = "";
				$sD100New = "";
				if (isset($_SESSION["userInfo"]["speedDial100"]))
				{
					foreach ($_SESSION["userInfo"]["speedDial100"] as $k => $v)
					{
						$sD100Old .= "Speed Code " . str_pad($v["speedCode"], 2, "0", STR_PAD_LEFT) . ": " . $v["phone"] . " / " . $v["description"] . "; ";
					}
				}
				for ($i = 0; $i < count($_POST["speedCode"]); $i++)
				{
					if ($_POST["speedCode"][$i] !== "NIL")
					{
						$sD100New .= "Speed Code " . $_POST["speedCode"][$i] . ": " . $_POST["phone"][$i] . " / " . $_POST["description"][$i] . "; ";
					}
				}

				if ($sD100Old !== $sD100New)
				{
					$changes["speedCode"][$key] = $value;
				}
			}
			if (in_Array($key, $voiceManagementChange))
			{
				$changes["voiceManagement"][$key] = $value;
			}
			if (in_Array($key, $mailBoxLimitChanges))
			{
			    if($key == "mailBoxLimit") {
			        if ($value == "Use Group Default" && $_SESSION["userInfo"]["mailBoxLimit"] == "true") {
			            continue;
			        }
			    }
			    $changes["mailBoxLimit"][$key] = $value;
			}
			
			if (in_Array($key, $hotelHostChange))
			{
				$changes["hotelHost"][$key] = $value;
			}
			if (in_array($key, $hotelChange))
			{
				$changes["hotel"][$key] = $value;
			}
			if (in_array($key, $phoneChange))
			{
				$changes["phone"][$key] = $value;
			}
			if (in_array($key, $activateNumber))
			{
				$changes["uActivateNumber"][$key] = $value;
			}
			if (in_array($key, $thirdPartyChange))
			{
				$changes["thirdParty"][$key] = $value;
			}
			if (in_array($key, $deviceChange))
			{
			    if ( ($key == "rebuildPhoneFiles" || $key == "resetPhone") && $value == "false" ) {
			        // do nothing
                } else {
                    $changes["device"][$key] = $value;
                }
			}
			if (in_Array($key, $callTransferChange))
			{
				$changes["callTransfer"][$key] = $value;
			}
			if (in_array($key, $cfaChange))
			{
				$changes["cfa"][$key] = $value;
				//add both true/false and phone number values to changes array; otherwise, the XML generates an error
				if ($key == "cfaForwardToPhoneNumber")
				{
					$changes["cfa"]["cfaForwardToPhoneNumber"] = $_POST["cfaForwardToPhoneNumber"];
				}
				if ($key == "cfaActive")
				{
					$changes["cfa"]["cfaActive"] = $_POST["cfaActive"];
				}
				if ($key == "cfaIsRingSplashActive")
				{
				    $changes["cfa"]["cfaIsRingSplashActive"] = $_POST["cfaIsRingSplashActive"];
				}
				
			}
			if (in_array($key, $cfbChange))
			{
				$changes["cfb"][$key] = $value;
				if ($key == "cfbActive")
				{
					$changes["cfb"]["cfbForwardToPhoneNumber"] = $_POST["cfbForwardToPhoneNumber"];
				}
				if ($key == "cfbForwardToPhoneNumber")
				{
					$changes["cfb"]["cfbActive"] = $_POST["cfbActive"];
				}
				if ($key == "cfbForwardToPhoneNumber")
				{
				    $changes["cfb"]["cfbActive"] = $_POST["cfbActive"];
				}
			}
			if (in_array($key, $cfnChange))
			{
				$changes["cfn"][$key] = $value;
							
				if ($key == "cfnForwardToPhoneNumber")
				{
					$changes["cfn"]["cfnForwardToPhoneNumber"] = $_POST["cfnForwardToPhoneNumber"];
				}
				if ($key == "cfnActive")
				{
					$changes["cfn"]["cfnActive"] = $_POST["cfnActive"];
				}
				if ($key == "cfnNumberOfRings")
				{
				    $changes["cfn"]["cfnNumberOfRings"] = $_POST["cfnNumberOfRings"];
				}
			}
			if (in_array($key, $cfrChange))
			{
				$changes["cfr"][$key] = $value;
				if ($key == "cfrActive")
				{
					$changes["cfr"]["cfrForwardToPhoneNumber"] = $_POST["cfrForwardToPhoneNumber"];
				}
				if ($key == "cfrForwardToPhoneNumber")
				{
					$changes["cfr"]["cfrActive"] = $_POST["cfrActive"];
				}
				if ($key == "cfrActive")
				{
				    $changes["cfr"]["cfrForwardToPhoneNumber"] = $_POST["cfrForwardToPhoneNumber"];
				}
			}
			if (in_array($key, $callingLineIdPolicyChange))
			{
				$changes["callingLineIdPolicy"][$key] = $value;
			}
			if (in_array($key, $dndChange))
			{
				$changes["dnd"][$key] = $value;
			}
			if (in_Array($key, $simultaneousRingChange))
			{
				$changes["simultaneousRing"][$key] = $value;
			}
			if (in_array($key, $primaryChange))
			{
				$changes["primary"][$key] = $value;
			}
			if (in_array($key, $cpgGroupName))
			{
				$changes["cpgGroupName"][$key] = $value;
			}
			if (in_array($key, $ocpSettings))
			{
			    $changes["ocpSettings"][$key] = $value;
			}
			
			
		}
		if($key == "userPassword"){
				$changes["webAccessPassword"] = $_POST["input-password"];
		}
		if($key == "userPasscode"){
				$changes["voiceMailPasscode"] = $_POST["input-passcode"];
		}
		
	}
	//add primary line/port to list of changes in case list of shared call appearance users changes
	if (isset($_POST["scaUsers"]) and $_POST["scaUsers"] !== "")
	{
		$changes["primary"]["primary"] = $_POST["primary"];
	}
	
	if ($_POST["resetPortal"] == "true")
	{
		$changes["portal"] = "true";
	}
	if ($_POST["voiceMessaging"] == "Yes" && !isset($_SESSION['userInfo']['voiceMessagingUser']))
	{
	    $changes["portal"] = "true";
	}
        
        if ($_POST["sharedCallAppearance"] == "Yes")
	{
	    $changes["sharedCallAppearance"] = $_POST["sharedCallAppearance"];
	}
        if ($_POST["polycomPhoneServices"] == "Yes")
	{
	    $changes["polycomPhoneServices"] = $_POST["polycomPhoneServices"];
	}
        
	if ($_POST["resetWeb"] == "true")
	{
		$changes["web"] = "true";
	}
	//if ($_POST["resetDevice"] == "true" or (isset($_POST["scaUsers"]) and $_POST["scaUsers"] !== ""))
	if ($_POST["resetDevice"] == "true")
	{
		$changes["resetDevice"] = "true";
	}
	if (isset($_POST["monitoredUsers"]))
	{
		if ($_POST["monitoredUsers"] == "monitoredUsers;")
		{
			$changes["blf"] = "NIL";
		}
		else
		{
			$a = 0;
			$exp = explode(";", $_POST["monitoredUsers"]);
			foreach ($exp as $k => $v)
			{
				if ($v !== "monitoredUsers" and $v !== "")
				{
					$exp2 = explode(":", $v);
					$changes["blf"][$a]["id"] = $exp2[0];
					$changes["blf"][$a]["name"] = $exp2[1];
					$changes["blf"][$a]["linePort"] = $exp2[2];
					//$changes["blf"][$a]["linePort"] = $exp2[2];
					$a++;
				}
			}
		}
	}

	if(isset($_POST["scaUsers"]) and $_POST["scaUsers"] !== "")
	{
		$sharedCallAppearance = $grpServiceList->compareSessionScaUser();
		$postScaUsers = explode(";", $_POST["scaUsers"]);
		
		array_pop($postScaUsers);
		array_pop($postScaUsers);
		
		$aa = 0;
		//make the assign and unassign array for service pack assign to user in EX-615
		$scaOperationArray = $grpServiceList->assignUnassignScaUser($sharedCallAppearance, $postScaUsers);
		if(count($scaOperationArray["unAssign"]) > 0){
			$b = 0;
			foreach ($scaOperationArray["unAssign"] as $k => $v)
			{
				if ($v !== "scaUsers" and $v !== "")
				{
					$exp2 = explode(":", $v);
					$changes["scaServicePackUnassign"][$b] = $exp2[0];
					$a++;
				}
			}
		}
		
		if ($_POST["scaUsers"] == "scaUsers;")
		{
			$changes["sca"] = "NIL";
		}
		else
		{
			$a = 0;
			$exp = explode(";", $_POST["scaUsers"]);
			foreach ($exp as $k => $v)
			{
				if ($v !== "scaUsers" and $v !== "")
				{
					$exp2 = explode(":", $v);
					$changes["sca"][$a]["id"] = $exp2[0];
					$changes["sca"][$a]["name"] = $exp2[1];
					$changes["sca"][$a]["linePort"] = $exp2[2];
					
					$changes["scaServicePack"][$a] = $exp2[0];
					$a++;
				}
			}
		}
		
	}
	
	if (isset($_POST["servicePack"])) {
        // determine which selected service packs will be added to user
        $i = 0;
        
        foreach ($_POST["servicePack"] as $pack) {
            if (! in_array($pack, $_SESSION["userInfo"]["servicePacksAssigned"])) {
                $changes["servicePackAssigned"][$i++] = $pack;
                $changes["servicePack"] = implode(",", $_POST["servicePack"]);  //Code added @ 04 July 2018
            }
        }
        
        // determine which selected service packs will be removed from user
        $i = 0;
        
        if($_SESSION["userInfo"]["servicePacksAssigned"][0]!=""){
        foreach ($_SESSION["userInfo"]["servicePacksAssigned"] as $pack) {
            if (! in_array($pack, $_POST["servicePack"])) {
                $changes["servicePackUnassigned"][$i++] = $pack;
                $changes["servicePack"] = implode(",", $_POST["servicePack"]);  //Code added @ 04 July 2018
            }
        }
        }
        
    }

    if(isset($changes["device"]) && $changes["device"]["deviceType"] == $_SESSION["userInfo"]["deviceType"]){
	    unset($changes["device"]["deviceType"]);
	} 
	
	if ( isset($_POST['resetPhone_sca']) && $_POST['resetPhone_sca'] == "true")
	{
	    $changes['scaRebuiltReset']['resetPhone_sca']  = 'true';
	}
	if ( isset($_POST['rebuildPhoneFiles_sca']) && $_POST['rebuildPhoneFiles_sca'] == "true" )
	{
	    $changes['scaRebuiltReset']['rebuildPhoneFiles_sca']  = 'true';
	}
	if(isset($_POST["userCallingLineIdBlockingOverride"]) && $_POST["userCallingLineIdBlockingOverride"] !=""){
	    if($_POST["userCallingLineIdBlockingOverride"] != $_SESSION["userInfo"]["userCallingLineIdBlockingOverride"]){
	        $changes["userCallingLineIdBlockingOverride"] = $_POST["userCallingLineIdBlockingOverride"];
	    }
	}
	
	/* line Port */
	foreach( $_SESSION["userInfo"]["devicePortAssignmentDetail"] as $portKey => $portValue) {
	    if( $portValue["primary"] == "true") {
	        $oldLinePortPrimary = $portValue["linePort"];
	        break;
	    }
	}
	if( $_POST["deviceLinePortPrimary"] != $oldLinePortPrimary) {
	    $changes["deviceLinePortPrimary"] = $_POST["deviceLinePortPrimary"];
	}
	/* ---------------- */
	if( isset($_POST["linePortTableOrderedData"]) && $_POST["linePortTableOrderedData"] != "") {
	    $changes["reorderLinePort"] = explode(";", rtrim($_POST["linePortTableOrderedData"], ";"));
	}

	
	foreach ($changes as $key => $value)
	{
	    
                if($key == "deviceLinePortPrimary") {
                    $userOpObj = new UserOperations();
                    $linePortPrimaryRes = $userOpObj->groupAccessDeviceModifyUserRequest($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["userInfo"]["deviceName"], $value);
                    if( empty($linePortPrimaryRes["Error"]) ) {
                        $changeLog[$a]["field"] = "Device Primary Line/Port";
                        $changeLog[$a]["old"] = $oldLinePortPrimary;
                        $changeLog[$a]["new"] = $value;
                        $a++;
                        $message .= "<li> Device Primary Line/Port has been changed to " . $value . "</li>";
                    } else {
                        $message .= "<li style='color:red'> " . $portOpResponse["status"] . "</li>";
                    }
                    
                }
	    
	            if($key == "reorderLinePort") {
	                $portOpResponse = deviceLinePortOperations($value);
	                if( $portOpResponse["status"] == "success") {
	                    // Create log data
	                        foreach( $_SESSION["userInfo"]["devicePortAssignmentDetail"] as $portKey => $portValue) {
	                            $changeLog[$a]["field"] = $portValue["linePort"];
	                            $changeLog[$a]["old"] = $oldPortValue = $portValue["port"];
	                            $changeLog[$a]["new"] = $newPortValue = (array_search($portValue["linePort"], $value) + 1 );
	                            $a++;
	                            
	                            $message .= "<li> Device Line Port " . $portValue["linePort"] . " has been set to port " . ($newPortValue + 1) . "</li>";
	                        }
	                } else {
	                    $message .= "<li style='color:red'> " . $portOpResponse["status"] . "</li>";
	                }
	            }
	            
	            if($key == "userServices") {
	                /* Service Pack Operations Start*/
	                $serviceOpRes = userServicesOperations($_POST, $userId, $deviceName);
	                $message .= $serviceOpRes["message"];
	            }
            
                //Code added @ 04 July 2018
                if ($key == "servicePack")
				{
                    $value = $value ? $value : "None";
                    if(empty($_POST["servicePack"])){
                        $_POST["servicePack"] = array("None");
					}

                    $message .= "<li>Service Pack has been changed to " . $value . ".</li>";
                    $changeLog[$a]["field"] = $key;
                    $changeLog[$a]["old"] = implode(",", $_SESSION["userInfo"]["servicePacksAssigned"]);
                    $changeLog[$a]["new"] = implode(",", $_POST["servicePack"]);
                    $a++;
                }
                //End Code
                
                if ($key == "sharedCallAppearance")
                { 
                    if(($srvceObj->servicePacksHaveSCAService($_POST["servicePack"])) && (!$srvceObj->servicePacksHaveSCAService($_SESSION["userInfo"]["servicePacksAssigned"])))
                    {
                        $message .= "<li>Shared Call Appearance changed to " . $value . ".</li>";
                        $changeLog[$a]["field"] = $key;
                        $changeLog[$a]["old"] = "No";
                        $changeLog[$a]["new"] = "Yes";
                        $a++;
                    }
                }
                //Code added @ 11 Sep 2018
                if ($key == "polycomPhoneServices")
                { 
                    if(($srvceObj->checkservicePacksHasPolycomPhoneService($_POST["servicePack"])) && (!$srvceObj->checkservicePacksHasPolycomPhoneService($_SESSION["userInfo"]["servicePacksAssigned"])))
                    {
                        $message .= "<li>Polycom Phone Service changed to " . $value . ".</li>";
                        $changeLog[$a]["field"] = $key;
                        $changeLog[$a]["old"] = "No";
                        $changeLog[$a]["new"] = "Yes";
                        $a++;
                    }
                }
                //End code
            
		if ($key == "department")
		{
			$deptModHeader = xmlHeader($sessionid, "UserModifyRequest17sp4");
			$deptModHeader .= "<userId>" . $userId . "</userId>";
			if ($_POST["department"])
			{
				$deptModHeader .= "<department xsi:type=\"GroupDepartmentKey\">";
				foreach ($value as $k => $v)
				{
					$deptModHeader .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
					$deptModHeader .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
					$deptModHeader .= "<name>" . $v . "</name>";
				}
				$deptModHeader .= "</department>";
			}
			else
			{
				$deptModHeader .= "<department xsi:type=\"GroupDepartmentKey\" xsi:nil=\"true\" />";
			}
			$deptModFooter = xmlFooter();
			$deptMod = $deptModHeader . $deptModFooter;
		}
		if ($key == "user")
		{
                     
			$userModHeader = xmlHeader($sessionid, "UserModifyRequest17sp4");
			$userModHeader .= "<userId>" . $userId . "</userId>";
                        $userValues = "";
                        if(isset($value["lastName"])) {
                            $userValues .= "<lastName>".$value["lastName"]."</lastName>";
                        }
                        if(isset($value["firstName"])) {
                             $userValues .= "<firstName>".$value["firstName"]."</firstName>";
                        }
                        if(isset($value["callingLineIdLastName"])) {
                            $userValues .= "<callingLineIdLastName>".$value["callingLineIdLastName"]."</callingLineIdLastName>";
                        }
                        
                        if(isset($value["callingLineIdFirstName"])) {
                            $userValues .= "<callingLineIdFirstName>".$value["callingLineIdFirstName"]."</callingLineIdFirstName>";
                        }
                        
                        if($ociVersion=="20" || $ociVersion=="21" || $ociVersion=="22"){
                            if(isset($value["nameDialingFirstName"]) || isset($value["nameDialingLastName"])) {
                                
                                if($value["nameDialingFirstName"]!="" || $value["nameDialingLastName"]!=""){
                                    $userValues .= "<nameDialingName>";
                                    $userValues .= "<nameDialingLastName>" . trim($_POST["nameDialingLastName"]) . "</nameDialingLastName>";
                                    $userValues .= "<nameDialingFirstName>" . trim($_POST["nameDialingFirstName"]) . "</nameDialingFirstName>";
                                    $userValues .= "</nameDialingName>"; 
                                }
                                if($value["nameDialingFirstName"]=="" && $value["nameDialingLastName"]==""){                                    
                                    $userValues .= "<nameDialingName xsi:type=\"NameDialingName\" xsi:nil=\"true\" />";
                                }
                                unset($value["nameDialingFirstName"]);
                                unset($value["nameDialingLastName"]);
                            }
                        }
                        
                        if(isset($value["callingLineIdPhoneNumber"])) {
                            if($value["callingLineIdPhoneNumber"]!="")
                                 $userValues .= "<callingLineIdPhoneNumber>".$value["callingLineIdPhoneNumber"]."</callingLineIdPhoneNumber>";
                             else                            
                                $userValues .= "<callingLineIdPhoneNumber xsi:type=\"DN\" xsi:nil=\"true\" />";
                             
                        }
                        
                        if( isset($value["emailAddress"]) ) {
                                $userValues .= $value["emailAddress"] != "" ? "<emailAddress>" . $value["emailAddress"] . "</emailAddress>" : "<emailAddress xsi:nil=\"true\" />";
                        }
                        
                        if(isset($value["addressLocation"])) {
                             if($value["addressLocation"]!="")
                                 $userValues .= "<addressLocation>".$value["addressLocation"]."</addressLocation>";
                             else                            
                                $userValues .= "<addressLocation xsi:type=\"AddressLocation\" xsi:nil=\"true\" />";                             
                        }
                        if(isset($value["networkClassOfService"])) {
                             $userValues .= "<networkClassOfService>".$value["networkClassOfService"]."</networkClassOfService>";
                        }
                        
                        
                        
			
                        unset($value["firstName"]);
                        unset($value["lastName"]);
                        unset($value["callingLineIdLastName"]);
                        unset($value["callingLineIdFirstName"]);
                        unset($value["callingLineIdPhoneNumber"]);
                        unset($value["addressLocation"]);
                        unset($value["networkClassOfService"]);
                        
                        /*
			foreach ($value as $k => $v)
			{
				if (($k == "callingLineIdPhoneNumber" || $k == emailAddress) and $v == "")
				{
					$userValues .= "<" . $k . " xsi:nil=\"true\" />";
				}
				else
				{
					$userValues .= "<" . $k . ">" . $v . "</" . $k . ">";
				}
			} */
                        
                        $userModFooter = xmlFooter();
			$userMod = $userModHeader . $userValues . $userModFooter;
		}
                
                //Code added @ 16 Nov 2018 for Sim Ring Feature
                if($key == "simultaneousRingPhoneNumber"){

                        $criteriaNameListOnly = array();    
                        $modifySimRing = "";
                        //echo "The following changes are complete:<br /><br />";                        
                        if(!empty($_SESSION["userInfo"]["crtiaNameList"])){
                            $criteriaNameListOnly = $_SESSION["userInfo"]["crtiaNameList"];
                        }  
                        if(!empty($_SESSION['addSimRingCriteria'])){
                            
                            foreach($_SESSION['addSimRingCriteria'] as $ky1 => $arrayAddSimRing){
                                $simRingAddCriteriaListResponse =  $objSimRingMgmt->addSimRingCreteria($arrayAddSimRing);
                                $simRingAddCriteriaSuccess      =  $simRingAddCriteriaListResponse['Success'];
                                //Start code for Change Log for Sim Ring Criteria
                                if($simRingAddCriteriaListResponse['Success']=="Success"){        
                                            $phoneList = " ";
                                            if(!empty($arrayAddSimRing["criteriaIncomingPhone"])){
                                                $phoneList = implode("<br />", $arrayAddSimRing["criteriaIncomingPhone"]);
                                            }
                                            $changeLogArr  = array(
                                                     'serviceId'                   => $_SESSION["sp"],
                                                     'simRingCriteriaDescription'  => $arrayAddSimRing["criteriaName"],
                                                     'timeSchedule'                => $arrayAddSimRing["timeScheduleName"]." - ".$arrayAddSimRing["timeScheduleLevel"],
                                                     'holidaySchedule'             => $arrayAddSimRing["holidayScheduleName"]." - ".$arrayAddSimRing["holidayScheduleLevel"],
                                                     'callFrom'                    => $arrayAddSimRing["simRingIncomingCallsFrom"],
                                                     'privateNumber'               => $arrayAddSimRing["simRingIncomingPhoeFromPrivate"],
                                                     'unavailableNumber'           => $arrayAddSimRing["simRingIncomingPhoeFromUnavailable"],
                                                     'specificPhoneNumbers'        => $phoneList                        
                                             );    
                                             $changeLogObj   = new ChangeLogUtility($arrayAddSimRing["criteriaName"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
                                             $module         = "Add Sim Ring Criteria";
                                             $tableName      = "simRingCriteriaAddChanges";
                                             $entityName     = $arrayAddSimRing["criteriaName"];
                                             $changeResponse = $changeLogObj->changeLogAddUtility($module, $entityName, $tableName, $changeLogArr);                    

                                             //if($arrayAddSimRing["criteriaName"]!=""){
                                             $message .="<b>Criteria Name: </b>".$arrayAddSimRing["criteriaName"]." Added Successfully<br />";
                                             //}
                                 }else{
                                             echo "<font color='red'>".$simRingAddCriteriaListResponse['Error']."</font>";
                                 }
                                //End code for Change Log for Sim Ring Criteria
                            }
                        }
                        
                        if(!empty($_SESSION['editSimRingCriteria'])){
                            foreach($_SESSION['editSimRingCriteria'] as $ky2 => $arrayEditSimRing){
                                $simRingEditCriteriaListResponse   = $objSimRingMgmt->updateSimRingCriteria($arrayEditSimRing);
                                $simRingEditCriteriaSuccess =  $simRingEditCriteriaListResponse['Success'];
                                $oldCriteriaName = $arrayEditSimRing['oldCriteriaName'];
                                $newCriteriaName = $arrayEditSimRing['criteriaName'];
                                
                                $message .="<b>Criteria Name: </b>".$arrayEditSimRing["oldCriteriaName"]." Modified Successfully<br />";
                                //Start code for Change Log for Sim Ring Criteria
                                if($simRingEditCriteriaListResponse['Success']=="Success"){
                                    $changesArr    = array();     
                                    $changeLogObj  = new ChangeLogUtility($arrayEditSimRing["criteriaName"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
                                    foreach($_SESSION['simRingCriteriaList'] as $keyEditCriterRow => $rowEditCriterRow){ 
                                            if($arrayEditSimRing["oldCriteriaName"]==$keyEditCriterRow){
                                                    $oldEditCriteriaArr = $rowEditCriterRow;
                                            }
                                    }
                                    
                                    foreach ($arrayEditSimRing as $keyEditSimRing => $valueEditSimRing){
                                        foreach($oldEditCriteriaArr as $sessKey => $sessValue){
                                            if($keyEditSimRing == $sessKey){
                                                if($valueEditSimRing <> $sessValue){
                                                        if(is_array($valueEditSimRing)){
                                                            $valueEditSimRing = implode(", ", $valueEditSimRing);
                                                            $sessValue        = implode(", ", $sessValue);
                                                        }else{
                                                            $valueEditSimRing = $valueEditSimRing;
                                                            $sessValue = $sessValue;
                                                        }
                                                        $changeLogObj->createChangesArray($keyEditSimRing, $sessValue, $valueEditSimRing);

                                                }
                                            }
                                        }
                                    }
                                    
                                    $module         = "Modify Sim Ring Criteria";
                                    $tableName      = "simRingCriteriaModChanges";
                                    $entityName     = $arrayEditSimRing["criteriaName"];
                                    $changeResponse = $changeLogObj->changeLogModifyUtility($module, $entityName, $tableName); 
                                        
                                }else{
                                            echo "<font color='red'>".$simRingEditCriteriaListResponse['Error']."</font>";
                                } 
                                //End code for Change Log for Sim Ring Criteria
                            }
                        }             
                    
                        
                        if(!empty($_SESSION['deleteSimRingCriteria'])){
                            
                            foreach($_SESSION['deleteSimRingCriteria'] as $ky3 => $arrayDelSimRing){
                                
                                $userId                 = $arrayDelSimRing['userId'];
                                $criteriaName           = $arrayDelSimRing['criteriaName'];
        
                                $deleteSimRingResponse  = $objSimRingMgmt->userModifyDeleteSimRing($userId, $criteriaName);
                                $delErrorMsg            = $deleteSimRingResponse->command->summaryEnglish;
                                if($delErrorMsg!=""){
                                    //echo "<span style='color:red;'>".$delErrorMsg."</span><br />";
                                }else{
                                    $message .="<b>Criteria Name: </b>".$criteriaName." Removed Successfully<br />";
                                }
                                //Start code for Change Log for Delete Sim Ring Criteria 
                                $changeLogObj   = new ChangeLogUtility($criteriaName, $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
                                $changeLogObj->module     = "Delete Sim Ring Criteria";
                                $changeLogObj->entityName = $criteriaName;
                                $changeLogObj->changeLog();

                                $changeLogObj->modTableName = "simRingCriteriaModChanges"; //table name
                                $changeLogObj->serviceId    = $userId; //user id
                                $changeLogObj->entityName   = $criteriaName; //creteria name
                                $changeLogObj->tableDelChanges();
                                //End code for Change Log for Delete Sim Ring Criteria
                            }
                        }
                        
                        $simRingPhoneList = array();   
                        for ($l = 0; $l < 10; $l++)
                        {         
                            if($_POST["simultaneousRingPhoneNumber"][$l]!=""){
                                $simRingPhoneList[] = $_POST["simultaneousRingPhoneNumber"][$l];
                            }
                        }   
                        
                        
                        $xmlinput  = xmlHeader($sessionid, "UserSimultaneousRingPersonalModifyRequest17");
                        $xmlinput .= "<userId>".$userId."</userId>";
                        $xmlinput .= "<isActive>".$_POST['simultaneousRingIsActive']."</isActive>";
                        $xmlinput .= "<doNotRingIfOnCall>".$_POST['doNotRingIfOnCall']."</doNotRingIfOnCall>"; 
                        
                        if(!empty($simRingPhoneList)){
                            $xmlinput .= "<simultaneousRingNumberList>";                      
                            for ($i = 0; $i < 10; $i++){
                                    if($_POST["simultaneousRingPhoneNumber"][$i]!=""){
                                        if($_POST["answerConfirmationRequired"][$i]=="true" || $_POST["answerConfirmationRequired"][$i]=="on"){
                                            $ansConfmReq = "true";
                                        }
                                        if($_POST["answerConfirmationRequired"][$i]=="false" || $_POST["answerConfirmationRequired"][$i]==""){
                                            $ansConfmReq = "false";
                                        }                                    
                                        $xmlinput .= "<simultaneousRingNumber>";
                                        $xmlinput .= "<phoneNumber>" . $_POST["simultaneousRingPhoneNumber"][$i] . "</phoneNumber>";
                                        $xmlinput .= "<answerConfirmationRequired>".$ansConfmReq."</answerConfirmationRequired>";
                                        $xmlinput .= "</simultaneousRingNumber>";
                                    }
                            }
                            $xmlinput .= "</simultaneousRingNumberList>";
                        }else{
                            $xmlinput .= "<simultaneousRingNumberList xsi:nil='true'></simultaneousRingNumberList>";   
                        }

                        
                        if(!empty($_POST["criteriaName"])){ 

                            $simRingListData             = $objSimRingMgmt->getSimRingCrietriaList($userId);
                            $simRingCriteriaNameArr      = $simRingListData['Success']["crtiaNameArr"];  
                            
                            foreach($simRingCriteriaNameArr as $v3){
                                if(in_array($v3, $_POST["criteriaName"])){
                                    $xmlinput .= "<criteriaActivation>";
                                    $xmlinput .= "<criteriaName>" . $v3 . "</criteriaName>";
                                    $xmlinput .= "<isActive>true</isActive>";
                                    $xmlinput .= "</criteriaActivation>";
                                }else{
                                    $xmlinput .= "<criteriaActivation>";
                                    $xmlinput .= "<criteriaName>" . $v3 . "</criteriaName>";
                                    $xmlinput .= "<isActive>false</isActive>";
                                    $xmlinput .= "</criteriaActivation>";
                                }

                            } 
                        }
                        
                        if(empty($_POST['criteriaName'])){
                            $simRingListData             = $objSimRingMgmt->getSimRingCrietriaList($userId);
                            $simRingCriteriaNameArr      = $simRingListData['Success']["crtiaNameArr"]; 
                            if(!empty($simRingCriteriaNameArr)){
                                foreach($simRingCriteriaNameArr as $v3){
                                    $xmlinput .= "<criteriaActivation>";
                                    $xmlinput .= "<criteriaName>" . $v3 . "</criteriaName>";
                                    $xmlinput .= "<isActive>false</isActive>";
                                    $xmlinput .= "</criteriaActivation>";
                                }
                            }
                        }
                        

                        $xmlinput .= xmlFooter();                        
                        $response = $client->processOCIMessage(array("in0" => $xmlinput));
                        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                        
                        $data = "";
                        if (count($xml->command->attributes()) > 0){                                                
                                foreach ($xml->command->attributes() as $x => $y){
                                        if ($y == "Error"){  
                                                $errorMsg = "error";
                                                $data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);								
                                        }
                                }                                                
                        }
                        if($errorMsg=="error"){
                              echo $data;
                        }
                        if($errorMsg!="error"){
                            
                                //Change log for change criteria 
                                $changeLogObj  = new ChangeLogUtility($userId, $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
                                
                                if($_POST['simultaneousRingIsActive']!=$_SESSION["userInfo"]["simultaneousRingIsActive"]){
                                    $message .= "<li>Simultaneous Ring Personal has been changed to " . $_POST['simultaneousRingIsActive'] . ".</li>";
                                    $changeLogObj->createChangesArray("simultaneousRingIsActive", $_SESSION["userInfo"]["simultaneousRingIsActive"], $_POST['simultaneousRingIsActive']);
                                }
                                
                                if($_POST['doNotRingIfOnCall']!=$_SESSION["userInfo"]["doNotRingIfOnCall"]){
                                    $message .= "<li>Do not ring if already on call has been changed to " . $_POST['doNotRingIfOnCall'] . ".</li>";                                    
                                    $changeLogObj->createChangesArray("doNotRingIfOnCall", $_SESSION["userInfo"]["doNotRingIfOnCall"], $_POST['doNotRingIfOnCall']);
                                }
                                
                                $aCrtaCount    = count($_SESSION["userInfo"]["activeCriteriaNameList"]);
                                $pCrtaCount    = count($_POST['criteriaName']);
                                if($aCrtaCount>$pCrtaCount){
                                    $criteriaNameDiff = array_diff($_SESSION["userInfo"]["activeCriteriaNameList"], $_POST['criteriaName']);
                                }
                                else{
                                    $criteriaNameDiff = array_diff($_POST['criteriaName'], $_SESSION["userInfo"]["activeCriteriaNameList"]);
                                }
                                
                                if(!empty($criteriaNameDiff)){
                                    
                                    $simRingOldCriteriaStr = implode("<br />", $_SESSION["userInfo"]["activeCriteriaNameList"]);
                                    $simRingNewCriteriaStr = implode("<br />", $_POST['criteriaName']);
                                    
                                    $message .= "<li>Simultaneous Ring Personal Criteria changed to <br /><b>" . $simRingNewCriteriaStr . ".</b></li>";
                                    
                                    /*$changeLog[$a]["field"] = "criteriaName";
                                    $changeLog[$a]["old"]   = $simRingOldCriteriaStr;
                                    $changeLog[$a]["new"]   = $simRingNewCriteriaStr;
                                    $a++;*/
                                    $changeLogObj->createChangesArray("criteriaName", $simRingOldCriteriaStr, $simRingNewCriteriaStr);
                                }
                                //End code
                            
                            
                                //Change log for changed phone number list 
                                for ($i = 0; $i < 10; $i++)
                                {
                                    if (isset($_SESSION["userInfo"]["simultaneousRingNumber"]))
                                    {
                                        $oldNumbers .= $_SESSION["userInfo"]["simultaneousRingNumber"][$i]["phoneNumber"] . "-" . ($_SESSION["userInfo"]["simultaneousRingNumber"][$i]["answerConfirmationRequired"] == "false" ? "" : $_SESSION["userInfo"]["simultaneousRingNumber"][$i]["answerConfirmationRequired"]) . "; ";
                                    }
                                    if(!empty($simRingPhoneList)){
                                        $newNumbers .= $_POST["simultaneousRingPhoneNumber"][$i] . "-" . $_POST["answerConfirmationRequired"][$i]. "; ";
                                    }
                                }
                                if ($oldNumbers !== $newNumbers)
                                {
                                    if(isset($_SESSION["userInfo"]['simultaneousRingNumber'])){
                                        $oldDataArr = array();
                                        foreach($_SESSION["userInfo"]['simultaneousRingNumber'] as $k2 => $v2){
                                            $oldDataArr[$k2] = $v2['phoneNumber']."-".$v2['answerConfirmationRequired'];
                                        }
                                        $simRingOldData = implode(", ", $oldDataArr);
                                    }                                
                                    //$simRingNewData = implode(", ", $value);
                                    $simRingNewData = implode(", ", $SimRingArrForChngLog);
                                    $message .= "<li>Simultaneous Ring Personal Phone Numbers changed to <br /><b>" . $simRingNewData . ".</b></li>";
                                    /*$changeLog[$a]["field"] = "simultaneousRingPhoneNumber";
                                    $changeLog[$a]["old"]   = $simRingOldData;
                                    $changeLog[$a]["new"]   = $simRingNewData;
                                    $a++;*/
                                    $changeLogObj->createChangesArray("simultaneousRingPhoneNumber", $simRingOldData, $simRingNewData);
                                }
                                
                                $module         = "Modify Sim Ring";
                                $tableName      = "simRingModChanges";
                                $entityName     = $_SESSION["userInfo"]["lastName"].", ".$_SESSION["userInfo"]["firstName"];
                                $changeResponse = $changeLogObj->changeLogModifyUtility($module, $entityName, $tableName); 
                                //End code
                        }
                }
                //End code
                
                
                //Code added @ 12 Sep 2018
                if($key=="ccd"){                    
                        if ($_POST["polycomPhoneServices"] == "Yes" && isset($_SESSION["userInfo"]['deviceName']))
                        {
                                //sleep(2);
                                $xmlinput = xmlHeader($sessionid, "UserServiceIsAssignedRequest");
                                $xmlinput .= "<userId>".$userId."</userId>";
                                $xmlinput .= "<serviceName>Polycom Phone Services</serviceName>";
                                $xmlinput .= xmlFooter();
                                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                                $data = "";

                                if (strval($xml->command->isAssigned) == "true")
                                {
                                        $xmlinput = xmlHeader($sessionid, "UserPolycomPhoneServicesModifyRequest");
                                        $xmlinput .= "<userId>" . $userId . "</userId>";
                                        $xmlinput .= "<accessDevice>
                                                                <deviceLevel>Group</deviceLevel>
                                                                <deviceName>" . $_SESSION["userInfo"]['deviceName'] . "</deviceName>
                                                        </accessDevice>";
                                        $xmlinput .= "<integratePhoneDirectoryWithBroadWorks>true</integratePhoneDirectoryWithBroadWorks>";
                                        $xmlinput .= "<includeUserPersonalPhoneListInDirectory>true</includeUserPersonalPhoneListInDirectory>";
                                        $xmlinput .= "<includeGroupCustomContactDirectoryInDirectory>true</includeGroupCustomContactDirectoryInDirectory>";
                                        if ($_POST["ccd"])
                                        {
                                                $xmlinput .= "<groupCustomContactDirectory>" . htmlspecialchars($_POST["ccd"]) . "</groupCustomContactDirectory>";
                                        }
                                        else
                                        {
                                                $xmlinput .= "<groupCustomContactDirectory xsi:nil=\"true\" />";
                                        }
                                        $xmlinput .= xmlFooter();
                                        $response = $client->processOCIMessage(array("in0" => $xmlinput));
                                        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);


                                        if (count($xml->command->attributes()) > 0)
                                        {                                        
                                                foreach ($xml->command->attributes() as $a1 => $b)
                                                {
                                                    if ($b == "Error" || $b == "Warning")
                                                        {   
                                                                $errorMsg = "error"; 
                                                                $data = $userId . " Configuring Additional Services failed.";
                                                                $data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);							
                                                        }
                                                }                                        

                                        }

                                        if ($_POST["ccd"])
                                        {
                                                //retrieve list of users from custom contact directory and append new user to list
                                                $xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryGetRequest17");
                                                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
                                                $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
                                                $xmlinput .= "<name>" . htmlspecialchars($_POST["ccd"]) . "</name>";
                                                $xmlinput .= xmlFooter();
                                                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                                                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

                                                $ccdUsersList = "";
                                                foreach ($xml->command->userTable->row as $key11 => $value11)
                                                {
                                                        $ccdUsersList .= "<entry><userId>" . strval($value11->col[0]) . "</userId></entry>";
                                                }
                                                $ccdUsersList .= "<entry><userId>" . $userId . "</userId></entry>";

                                                $xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryModifyRequest17");
                                                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
                                                $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
                                                $xmlinput .= "<name>" . htmlspecialchars($_POST["ccd"]) . "</name>";
                                                $xmlinput .= "<entryList>" . $ccdUsersList . "</entryList>";
                                                $xmlinput .= xmlFooter();
                                                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                                                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);


                                                if (count($xml->command->attributes()) > 0)
                                                {                                                
                                                        foreach ($xml->command->attributes() as $a2 => $b)
                                                        {
                                                                if ($b == "Error")
                                                                {  
                                                                        $errorMsg = "error";
                                                                        $data = $userId . " Configuring Additional Services failed.";
                                                                        $data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);								
                                                                }
                                                        }                                                
                                                }
                                        }
                                }
                                if($errorMsg=="error"){
                                        echo $data;
                                }
                                if($errorMsg!="error"){
                                        
                                        $message .= "<li>Custom Contact Directory changed to " . $value . ".</li>";
                                        $changeLog[$a]["field"] = "ccd";
                                        $changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
                                        $changeLog[$a]["new"] = $value;
                                        $a++;                                        
                                }
                        }
                }
                //End code
		if ($key == "address")
		{
			$addModHeader = xmlHeader($sessionid, "UserModifyRequest17sp4");
			$addModHeader .= "<userId>" . $userId . "</userId>";
			$addModHeader .= "<address>";
			$addValues = "";
			foreach ($value as $k => $v)
			{
				if ($v == "")
				{
					$addValues .= "<" . $k . " xsi:nil=\"true\" />";
				}
				else
				{
					$addValues .= "<" . $k . ">" . $v . "</" . $k . ">";
				}
			}
			$addModFooter = "</address>";
			$addModFooter .= xmlFooter();
			$addMod = $addModHeader . $addValues . $addModFooter;
		}
		
		/*if ($key == "callingLineIdPolicy")
		{
			$clidPolicy = xmlHeader($sessionid, "UserCallProcessingModifyPolicyRequest14sp7");
			$clidPolicy .= "<userId>" . $userId . "</userId>";
			if (isset($changes[$key]["useUserCLIDSetting"])) {
				$clidPolicy .= "<useUserCLIDSetting>" . $changes[$key]["useUserCLIDSetting"] . "</useUserCLIDSetting>";
			}
			if (isset($changes[$key]["clidPolicy"])) {
				$clidPolicy .= "<clidPolicy>" . $changes[$key]["clidPolicy"] . "</clidPolicy>";
				$clidPolicy .= "<emergencyClidPolicy>" . $changes[$key]["clidPolicy"] . "</emergencyClidPolicy>";
			}
			$clidPolicy .= xmlFooter();
		}*/
		//if ($key == "callingLineIdPolicy")
		//{
		
		//}
		if($key == "callingLineIdPolicy"){
		    $policyObject = new GetUserCallProcessingInfo();
		    $resp = $policyObject->modifyCallProcessingInfo($postUserId, $changes["callingLineIdPolicy"]);
		}
		if ($key == "voiceManagement")
		{
			if (isset($_SESSION["userInfo"]["thirdPartyVoiceMail"]) and $_SESSION["userInfo"]["thirdPartyVoiceMail"] == "true")
			{
				$voiceManagement = xmlHeader($sessionid, "UserThirdPartyVoiceMailSupportModifyRequest");
			}
			else
			{
				$voiceManagement = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
			}
			$voiceManagement .= "<userId>" . $userId . "</userId>";
			if (isset($_POST["isActive"]))
			{
				$voiceManagement .= "<isActive>" . $_POST["isActive"] . "</isActive>";
			}
			if (isset($_SESSION["userInfo"]["thirdPartyVoiceMail"]) and $_SESSION["userInfo"]["thirdPartyVoiceMail"] == "true")
			{
				if ($_POST["busyRedirectToVoiceMail"])
				{
					$voiceManagement .= "<busyRedirectToVoiceMail>" . $_POST["busyRedirectToVoiceMail"] . "</busyRedirectToVoiceMail>";
				}
				if ($_POST["noAnswerRedirectToVoiceMail"])
				{
					$voiceManagement .= "<noAnswerRedirectToVoiceMail>" . $_POST["noAnswerRedirectToVoiceMail"] . "</noAnswerRedirectToVoiceMail>";
				}
				if ($_POST["serverSelection"])
				{
					$voiceManagement .= "<serverSelection>" . $_POST["serverSelection"] . "</serverSelection>";
				}
				if ($_POST["userServer"])
				{
					$voiceManagement .= "<userServer>" . $_POST["userServer"] . "</userServer>";
				}
				else
				{
					$voiceManagement .= "<userServer xsi:nil=\"true\" />";
				}
				if ($_POST["mailboxIdType"])
				{
					$voiceManagement .= "<mailboxIdType>" . $_POST["mailboxIdType"] . "</mailboxIdType>";
				}
				if ($_POST["mailboxURL"])
				{
					$voiceManagement .= "<mailboxURL>" . $_POST["mailboxURL"] . "</mailboxURL>";
				}
				else
				{
					$voiceManagement .= "<mailboxURL xsi:nil=\"true\" />";
				}
				$voiceManagement .= "<noAnswerNumberOfRings>" . $_POST["noAnswerNumberOfRings"] . "</noAnswerNumberOfRings>";
				if ($_POST["alwaysRedirectToVoiceMail"])
				{
					$voiceManagement .= "<alwaysRedirectToVoiceMail>" . $_POST["alwaysRedirectToVoiceMail"] . "</alwaysRedirectToVoiceMail>";
				}
			}
			else
			{
			   if (isset($_POST["processing"]))
				{
					$voiceManagement .= "<processing>" . $_POST["processing"] . "</processing>";
				}
				if ($_POST["voiceMessageDeliveryEmailAddress"])
				{
					$voiceManagement .= "<voiceMessageDeliveryEmailAddress>" . $_POST["voiceMessageDeliveryEmailAddress"] . "</voiceMessageDeliveryEmailAddress>";
				}
				else
				{
					$voiceManagement .= "<voiceMessageDeliveryEmailAddress xsi:nil=\"true\" />";
				}
				if ($_POST["usePhoneMessageWaitingIndicator"])
				{
					$voiceManagement .= "<usePhoneMessageWaitingIndicator>" . $_POST["usePhoneMessageWaitingIndicator"] . "</usePhoneMessageWaitingIndicator>";
				}
				if ($_POST["sendVoiceMessageNotifyEmail"])
				{
					$voiceManagement .= "<sendVoiceMessageNotifyEmail>" . $_POST["sendVoiceMessageNotifyEmail"] . "</sendVoiceMessageNotifyEmail>";
				}
				if ($_POST["voiceMessageNotifyEmailAddress"])
				{
					$voiceManagement .= "<voiceMessageNotifyEmailAddress>" . $_POST["voiceMessageNotifyEmailAddress"] . "</voiceMessageNotifyEmailAddress>";
				}
				else
				{
					$voiceManagement .= "<voiceMessageNotifyEmailAddress xsi:nil=\"true\" />";
				}
				if ($_POST["sendCarbonCopyVoiceMessage"])
				{
					$voiceManagement .= "<sendCarbonCopyVoiceMessage>" . $_POST["sendCarbonCopyVoiceMessage"] . "</sendCarbonCopyVoiceMessage>";
				}
				if ($_POST["voiceMessageCarbonCopyEmailAddress"])
				{
					$voiceManagement .= "<voiceMessageCarbonCopyEmailAddress>" . $_POST["voiceMessageCarbonCopyEmailAddress"] . "</voiceMessageCarbonCopyEmailAddress>";
				}
				else
				{
					$voiceManagement .= "<voiceMessageCarbonCopyEmailAddress xsi:nil=\"true\" />";
				}
				if ($_POST["transferOnZeroToPhoneNumber"])
				{
					$voiceManagement .= "<transferOnZeroToPhoneNumber>" . $_POST["transferOnZeroToPhoneNumber"] . "</transferOnZeroToPhoneNumber>";
				}
				if ($_POST["transferPhoneNumber"])
				{
					$voiceManagement .= "<transferPhoneNumber>" . $_POST["transferPhoneNumber"] . "</transferPhoneNumber>";
				}
				else
				{
					$voiceManagement .= "<transferPhoneNumber xsi:nil=\"true\" />";
				}
				if ($_POST["alwaysRedirectToVoiceMail"])
				{
					$voiceManagement .= "<alwaysRedirectToVoiceMail>" . $_POST["alwaysRedirectToVoiceMail"] . "</alwaysRedirectToVoiceMail>";
				}
				if ($_POST["busyRedirectToVoiceMail"])
				{
					$voiceManagement .= "<busyRedirectToVoiceMail>" . $_POST["busyRedirectToVoiceMail"] . "</busyRedirectToVoiceMail>";
				}
				if ($_POST["noAnswerRedirectToVoiceMail"])
				{
					$voiceManagement .= "<noAnswerRedirectToVoiceMail>" . $_POST["noAnswerRedirectToVoiceMail"] . "</noAnswerRedirectToVoiceMail>";
				}
			}
			$voiceManagement .= xmlFooter();
			
		}
		
		if($key == "mailBoxLimit"){
		    require_once("/var/www/lib/broadsoft/adminPortal/users/VMUsersServices.php");
		    $vmAdvancedObj = new VMUsersServices();
		    $vmAdvancedResp = $vmAdvancedObj->modifyUsersVoiceMessagingAdvancedServices($postUserId, $value);
		    if($vmAdvancedResp["Error"] == ""){
		        if(count($changes["mailBoxLimit"]) > 0){
		            foreach ($changes["mailBoxLimit"] as $key => $value)
		            {
		                $message .= "<li>Mailbox Limit has been changed to " . $value . ".</li>";
		                $changeLog[$a]["field"] = $key;
		                $changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
		                $changeLog[$a]["new"] = $value;
		                $a++;
		            }
		        }
		    }
		}
		
		if($key == "ocpSettings"){
		    require_once ("/var/www/lib/broadsoft/adminPortal/ocpFeatures/OCPOperations.php");
		    $objOCP = new OCPOperations();
		    $ocpDataResp = $objOCP->userCallingPlanModifyOriginatingRequest($postUserId, $value);
		    
		    if($ocpDataResp["Error"] == ""){
		        if(count($changes["ocpSettings"]) > 0){
		            
		            foreach ($changes["ocpSettings"] as $key => $value)
		            {
		                
		                $message .= "<li> " . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
		                
		                $changeLog[$a]["field"] = $key;
		                $changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
		                $changeLog[$a]["new"] = $value;
		                $a++;
		            }
		        }
		    }
		}
		
		if ($key == "portal")
		{
            // Build Voice Mail Password. Enforce fallback
            // ----------------------------------------------------------------------------------------------------
            if ($bwVoicePortalPasswordType == "Formula") {
                require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
                require_once("/var/www/lib/broadsoft/adminPortal/VoicePasswordNameBuilder.php");

                $voicePasswordBuilder = new VoicePasswordNameBuilder($userVoicePortalPasscodeFormula,
                    getResourceForVoicePasswordCreation(ResourceNameBuilder::DN, $value),
                    getResourceForVoicePasswordCreation(ResourceNameBuilder::EXT, $value),
                    getResourceForVoicePasswordCreation(ResourceNameBuilder::USR_CLID, $value),
                    getResourceForVoicePasswordCreation(ResourceNameBuilder::GRP_DN, $value),
                    true);

                if (! $voicePasswordBuilder->validate()) {
                    // TODO: Implement validation resolution
                }

                do {
                    $attr = $voicePasswordBuilder->getRequiredAttributeKey();
                    $voicePasswordBuilder->setRequiredAttribute($attr, getResourceForVoicePasswordCreation($attr, $value));
                } while ($attr != "");

                $portalPassWord = $voicePasswordBuilder->getName();
                //Code added @ 08 May 2019 regarding Ex-1318
                if(is_numeric($portalPassWord) && strlen($portalPassWord) < 6){
                    $portalPassWord = "476983";
                }
                if(!is_numeric($portalPassWord)){
                    $portalPassWord = "476983";
                }
                //End code
            } else {
                $portalPassWord = setVoicePassword();
            }
            if(is_numeric($portalPassWord)){
                $portalPassWord = $portalPassWord;
            }else{
                $criteria = $userVoicePortalPasscodeFormula;
                preg_match('#\((.*?)\)#', $criteria, $match);
                $var = $match[1];
                $portalPassWord = "";
                for($i = 1; $i<= $var; $i++){
                    $portalPassWord .= $i;
                }
            }
//            $portalPassWord = setVoicePassword();
			$portalPass = xmlHeader($sessionid, "UserPortalPasscodeModifyRequest");
			$portalPass .= "<userId>" . $userId . "</userId>";
			$portalPass .= "<newPasscode>" . $portalPassWord . "</newPasscode>";
			$portalPass .= xmlFooter();
		}
		if ($key == "web")
		{
			$webPassWord = setWebPassword((int)$_SESSION["groupRules"]["minPasswordLength"]);
			$webPass = xmlHeader($sessionid, "UserModifyRequest17sp4");
			$webPass .= "<userId>" . $userId . "</userId>";
			$webPass .= "<newPassword>" . $webPassWord . "</newPassword>";
			$webPass .= xmlFooter();
		}
		if ($key == "resetDevice")
		{
			$resetDevice = xmlHeader($sessionid, "GroupAccessDeviceResetRequest");
			$resetDevice .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
			$resetDevice .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
			$resetDevice .= "<deviceName>" . $_SESSION["userInfo"]["deviceName"] . "</deviceName>";
			$resetDevice .= xmlFooter();
		}
		if ($key == "hotelHost")
		{
			$hotelHost = xmlHeader($sessionid, "UserHotelingHostModifyRequest");
			$hotelHost .= "<userId>" . $userId . "</userId>";
			$hotelHost .= "<isActive>" . $value["hotelHostisActive"] . "</isActive>";
			$hotelHost .= xmlFooter();
		}
		if ($key == "hotel")
		{
			$hotelGuest = xmlHeader($sessionid, "UserHotelingGuestModifyRequest");
			$hotelGuest .= "<userId>" . $userId . "</userId>";
			if (isset($value["hotelisActive"]))
			{
				$hotelGuest .= "<isActive>" . $value["hotelisActive"] . "</isActive>";
			}
			if ($value["hotelisActive"] !== "false")
			{
				if ($value["hotelHostId"] == "")
				{
					$hotelGuest .= "<hostUserId xsi:nil=\"true\" />";
				}
				else
				{
					$hotelGuest .= "<hostUserId>" . $value["hotelHostId"] . "</hostUserId>";
				}
			}
			$hotelGuest .= xmlFooter();
		}
		if ($key == "phone")
		{ 
			$phoneModHeader = xmlHeader($sessionid, "UserModifyRequest17sp4");
			$phoneModHeader .= "<userId>" . $userId . "</userId>";
			$phoneValues = "";
			foreach ($value as $k => $v)
			{
				if ($k == "phoneNumber" and $v == "")
				{
					$phoneValues .= "<" . $k . " xsi:nil=\"true\" />";
				}
				else
				{
					$phoneValues .= "<" . $k . ">" . $v . "</" . $k . ">";
				}
			}
			$phoneModFooter = xmlFooter();
			$phoneMod = $phoneModHeader . $phoneValues . $phoneModFooter;
		}
		if($key == "uActivateNumber"){
			$activateMod = "Yes";
		}
		if ($key == "thirdParty")
		{
			if ($value["thirdPartyVoiceMail"] == "true")
			{
				//assign third-party service
				$thirdPartyMod_1 = xmlHeader($sessionid, "UserServiceAssignListRequest");

				//turn third-party service on
				$thirdPartyMod_3 = xmlHeader($sessionid, "UserThirdPartyVoiceMailSupportModifyRequest");
				$thirdPartyMod_3 .= "<userId>" . $userId . "</userId>";
				$thirdPartyMod_3 .= "<isActive>true</isActive>";
				$thirdPartyMod_3 .= "<mailboxIdType>URL</mailboxIdType>";
				$thirdPartyMod_3 .= "<mailboxURL>" . $_POST["phoneNumber"] . "@pingtone.com</mailboxURL>";
				$thirdPartyMod_3 .= xmlFooter();
			}
			else
			{
				//unassign third-party service
				$thirdPartyMod_1 = xmlHeader($sessionid, "UserServiceUnassignListRequest");

				$emailAddress = $_SESSION["userInfo"]["deviceName"] . "@" . $surgemailDomain;
				$surgemailemail = $_SESSION["userInfo"]["deviceName"];
				$emailPassword = setEmailPassword();

				//set advanced voice management
				$thirdPartyMod_3 = xmlHeader($sessionid, "UserVoiceMessagingUserModifyAdvancedVoiceManagementRequest");
				$thirdPartyMod_3 .= "<userId>" . $userId . "</userId>";
				$thirdPartyMod_3 .= "<groupMailServerEmailAddress>" . $emailAddress . "</groupMailServerEmailAddress>";
				$thirdPartyMod_3 .= "<groupMailServerUserId>" . $emailAddress . "</groupMailServerUserId>";
				$thirdPartyMod_3 .= "<groupMailServerPassword>" . $emailPassword . "</groupMailServerPassword>";
				$thirdPartyMod_3 .= xmlFooter();
			}
			$thirdPartyMod_1 .= "<userId>" . $userId . "</userId>";
			$thirdPartyMod_1 .= "<serviceName>Third-Party Voice Mail Support</serviceName>";
			$thirdPartyMod_1 .= xmlFooter();
			//turn voice management on or off, depending on third-party choice
			$thirdPartyMod_2 = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
			$thirdPartyMod_2 .= "<userId>" . $userId . "</userId>";
			$thirdPartyMod_2 .= "<isActive>" . ($value["thirdPartyVoiceMail"] == "true" ? "false" : "true") . "</isActive>";
			$thirdPartyMod_2 .= xmlFooter();
		}
		if ($key == "callTransfer")
		{
			$callTransfer = xmlHeader($sessionid, "UserCallTransferModifyRequest");
			$callTransfer .= "<userId>" . $userId . "</userId>";
			if (isset($value["isRecallActive"]))
			{
				$callTransfer .= "<isRecallActive>" . $value["isRecallActive"] . "</isRecallActive>";
			}
			if (isset($value["recallNumberOfRings"]))
			{
				$callTransfer .= "<recallNumberOfRings>" . $value["recallNumberOfRings"] . "</recallNumberOfRings>";
			}
			if (isset($value["useDiversionInhibitorForBlindTransfer"]))
			{
				$callTransfer .= "<useDiversionInhibitorForBlindTransfer>" . $value["useDiversionInhibitorForBlindTransfer"] . "</useDiversionInhibitorForBlindTransfer>";
			}
			if (isset($value["useDiversionInhibitorForConsultativeCalls"]))
			{
				$callTransfer .= "<useDiversionInhibitorForConsultativeCalls>" . $value["useDiversionInhibitorForConsultativeCalls"] . "</useDiversionInhibitorForConsultativeCalls>";
			}
			if (isset($value["enableBusyCampOn"]))
			{
				$callTransfer .= "<enableBusyCampOn>" . $value["enableBusyCampOn"] . "</enableBusyCampOn>";
			}
			if (isset($value["busyCampOnSeconds"]))
			{
				$callTransfer .= "<busyCampOnSeconds>" . $value["busyCampOnSeconds"] . "</busyCampOnSeconds>";
			}
			$callTransfer .= xmlFooter();
		}
		if ($key == "cfa")
		{
		    $cfa = xmlHeader($sessionid, "UserCallForwardingAlwaysModifyRequest");
			$cfa .= "<userId>" . $userId . "</userId>";
			if(isset($value["cfaActive"]) && $value["cfaActive"] != ""){
			    $cfa .= "<isActive>" . $value["cfaActive"] . "</isActive>";
			}
			
			if (isset($value["cfaForwardToPhoneNumber"]) && $value["cfaForwardToPhoneNumber"] == "")
			{
				$cfa .= "<forwardToPhoneNumber xsi:nil=\"true\" />";
			}
			else if(isset($value["cfaForwardToPhoneNumber"]) && $value["cfaForwardToPhoneNumber"] != "")
			{
				$cfa .= "<forwardToPhoneNumber>" . $value["cfaForwardToPhoneNumber"] . "</forwardToPhoneNumber>";
			}
			if(isset($value["cfaIsRingSplashActive"])) {
			    $cfa .= "<isRingSplashActive>" . $value["cfaIsRingSplashActive"] . "</isRingSplashActive>";
			}
			$cfa .= xmlFooter();
		}
		if ($key == "cfb")
		{
			$cfb = xmlHeader($sessionid, "UserCallForwardingBusyModifyRequest");
			$cfb .= "<userId>" . $userId . "</userId>";
			$cfb .= "<isActive>" . $value["cfbActive"] . "</isActive>";
			if ($value["cfbForwardToPhoneNumber"] == "")
			{
				$cfb .= "<forwardToPhoneNumber xsi:nil=\"true\" />";
			}
			else
			{
				$cfb .= "<forwardToPhoneNumber>" . $value["cfbForwardToPhoneNumber"] . "</forwardToPhoneNumber>";
			}
			$cfb .= xmlFooter();
		}
		if ($key == "cfn")
		{
		    if($value["cfnNumberOfRings"] == "") {
		        $value["cfnNumberOfRings"] = 0;
		    }
		   // print_r($value["cfnNumberOfRings"]); exit;
			$cfn = xmlHeader($sessionid, "UserCallForwardingNoAnswerModifyRequest");
			$cfn .= "<userId>" . $userId . "</userId>";
			if(isset($value["cfnActive"]) && $value["cfnActive"] != ""){
			    $cfn .= "<isActive>" . $value["cfnActive"] . "</isActive>";
			}
			if (isset($value["cfnForwardToPhoneNumber"]) && $value["cfnForwardToPhoneNumber"] == "")
			{
				$cfn .= "<forwardToPhoneNumber xsi:nil=\"true\" />";
			}
			else if(isset($value["cfnForwardToPhoneNumber"]) && $value["cfnForwardToPhoneNumber"] != "") {
				$cfn .= "<forwardToPhoneNumber>" . $value["cfnForwardToPhoneNumber"] . "</forwardToPhoneNumber>";
			}
			if(isset($value["cfnNumberOfRings"])) {
			    $cfn .= "<numberOfRings>" . $value["cfnNumberOfRings"] . "</numberOfRings>";
			}
			$cfn .= xmlFooter();
		}
		if ($key == "cfr")
		{
			$cfr = xmlHeader($sessionid, "UserCallForwardingNotReachableModifyRequest");
			$cfr .= "<userId>" . $userId . "</userId>";
			$cfr .= "<isActive>" . $value["cfrActive"] . "</isActive>";
			if (isset($value["cfrForwardToPhoneNumber"]) && $value["cfrForwardToPhoneNumber"] == "")
			{
				$cfr .= "<forwardToPhoneNumber xsi:nil=\"true\" />";
			}
			else if(isset($value["cfrForwardToPhoneNumber"]))
			{
				$cfr .= "<forwardToPhoneNumber>" . $value["cfrForwardToPhoneNumber"] . "</forwardToPhoneNumber>";
			}
			$cfr .= xmlFooter();
		}
		if ($key == "dnd")
		{  
		    //print_r($changes["dnd"]); exit;
			$dnd = xmlHeader($sessionid, "UserDoNotDisturbModifyRequest");
			$dnd .= "<userId>" . $userId . "</userId>";
			if(isset($value["dnd"]) && $value["dnd"] != ""){
			    $dnd .= "<isActive>" . $value["dnd"] . "</isActive>";
			}
			
			if(isset($value["dndRingSplash"])) {
			    $dnd .= "<ringSplash>" . $value["dndRingSplash"] . "</ringSplash>";
			}
			$dnd .= xmlFooter();
		}
		if ($key == "simultaneousRing")
		{
			$simultaneousRing = xmlHeader($sessionid, "UserSimultaneousRingPersonalModifyRequest17");
			$simultaneousRing .= "<userId>" . $userId . "</userId>";
			if (isset($value["simultaneousRingIsActive"]))
			{
				$simultaneousRing .= "<isActive>" . $value["simultaneousRingIsActive"] . "</isActive>";
			}
			if (isset($value["doNotRingIfOnCall"]))
			{
				$simultaneousRing .= "<doNotRingIfOnCall>" . $value["doNotRingIfOnCall"] . "</doNotRingIfOnCall>";
			}
			$simultaneousRing .= xmlFooter();
		}
		if ($key == "blf")
		{
			$blf = xmlHeader($sessionid, "UserBusyLampFieldModifyRequest");
			$blf .= "<userId>" . $userId . "</userId>";

			if ($value == "NIL")
			{
				$blf .= "<monitoredUserIdList xsi:nil=\"true\" />";
			}
			else
			{
				$blf .= "<monitoredUserIdList>";
				foreach ($value as $k => $v)
				{
					foreach ($v as $kk => $vv)
					{
						if ($kk == "id")
						{
							$blf .= "<userId>" . $vv . "</userId>";
						}
					}
				}
				$blf .= "</monitoredUserIdList>";
			}
			$blf .= xmlFooter();
		}
		if ($key == "sca")
		{
			for ($a = 0; $a < count($_SESSION["userInfo"]["sharedCallAppearanceUsers"]); $a++)
			{
				if ($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["endpointType"] == "Shared Call Appearance")
				{
					$scaDelete = xmlHeader($sessionid, "UserSharedCallAppearanceDeleteEndpointListRequest14");
					$scaDelete .= "<userId>" . $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["id"] . "</userId>";
					$scaDelete .= "<accessDeviceEndpoint>";
					$scaDelete .= "<accessDevice>";
					$scaDelete .= "<deviceLevel>Group</deviceLevel>";
					$scaDelete .= "<deviceName>" . $_SESSION["userInfo"]["deviceName"] . "</deviceName>";
					$scaDelete .= "</accessDevice>";
					$scaDelete .= "<linePort>" . $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["linePort"] . "</linePort>";
					$scaDelete .= "</accessDeviceEndpoint>";
					$scaDelete .= xmlFooter();
					$sca[] = $scaDelete;
				}
			}

            //retrieve default domain
            $domain = $_SESSION["defaultDomain"];
           // $deviceDetails = $vdmOperationObj->getUserDeviceDetail($_SESSION["sp"], $_SESSION["groupId"], $postArray["deviceName"]);
			if (isset($value))
			{
			    $scaUtilityData="";
			    if($systemLinePortFlag == "true"){
			        $vdmOperationsArr = $vdmOperationObj->getAllUserOfDevice($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["userInfo"]["deviceName"]);
			       
			        if($vdmOperationsArr["Error"] == ""){
			            $scaUtilityData = $scaUtilityObj->scaFreePortCalculation($vdmOperationsArr["Success"], (int)$deviceDetails["Success"]["numberOfPorts"]);
			        }
			    }
			    if(count($value) > count($scaUtilityData)){
			     
			       $endValue = end($scaUtilityData)+1;
			       
			        for($v=count($scaUtilityData); $v<count($value); $v++){
			            $scaUtilityData[] = $endValue;
			        }
			    }
			    
				foreach ($value as $k => $v)
				{
				    
					$scaInsert = xmlHeader($sessionid, "UserSharedCallAppearanceAddEndpointRequest14sp2");
					$userId = "";
					foreach ($v as $kk => $vv)
					{
						if ($kk == "id")
						{
							$scaInsert .= "<userId>" . $vv . "</userId>";
							$userId = $vv;
						}
					}
					$scaInsert .= "<accessDeviceEndpoint>";
					$scaInsert .= "<accessDevice>";
					$scaInsert .= "<deviceLevel>Group</deviceLevel>";
					$scaInsert .= "<deviceName>" . $_SESSION["userInfo"]["deviceName"] . "</deviceName>";
					$scaInsert .= "</accessDevice>";
					foreach ($v as $kk => $vv)
					{
						if ($kk == "linePort")
						{
                            $primaryPhoneNumber = getPrimaryUserAttribute($userId, "phoneNumber");
                            $primaryExtension = getPrimaryUserAttribute($userId, "extension");

                            $linePort = getSCALinePortName($primaryPhoneNumber, $primaryExtension);
                            $scaInsert .= "<linePort>" . $linePort . "</linePort>";
						}
					}
					
					if($systemLinePortFlag == "true"){
					    if($scaUtilityData != ""){
					        $scaInsert .= "<portNumber>" . $scaUtilityData[$k] . "</portNumber>";
					    }
					}
					
					$scaInsert .= "</accessDeviceEndpoint>";
                    $scaInsert .= "<isActive>" . $scaIsActive . "</isActive>";
                    $scaInsert .= "<allowOrigination>" . $scaAllowOrigination . "</allowOrigination>";
                    $scaInsert .= "<allowTermination>" . $scaAllowTermination . "</allowTermination>";
					$scaInsert .= xmlFooter();
					$sca[] = $scaInsert;

					// Modify Shared Call Appearance
                    $scaModify  = xmlHeader($sessionid, "UserSharedCallAppearanceModifyRequest");
                    $scaModify .= "<userId>" . $userId . "</userId>";
                    $scaModify .= "<alertAllAppearancesForClickToDialCalls>" . $scaAlertClickToDial . "</alertAllAppearancesForClickToDialCalls>";
                    $scaModify .= "<alertAllAppearancesForGroupPagingCalls>" . $scaAllowGroupPaging . "</alertAllAppearancesForGroupPagingCalls>";
                    $scaModify .= "<allowSCACallRetrieve>" . $scaAllowCallRetrieve . "</allowSCACallRetrieve>";
                    $scaModify .= "<multipleCallArrangementIsActive>" . $scaMultipleCallArrangement . "</multipleCallArrangementIsActive>";
                    $scaModify .= "<allowBridgingBetweenLocations>" . $scaAllowBridging . "</allowBridgingBetweenLocations>";
                    $scaModify .= "<bridgeWarningTone>" . $scaBridgeWarningTone . "</bridgeWarningTone>";
                    $scaModify .= "<enableCallParkNotification>" . $scaCallParkNotification . "</enableCallParkNotification>";
                    $scaModify .= xmlFooter();
                    $sca[] = $scaModify;

                    // Rebuild Device
                    $scaRebuild  = xmlHeader($sessionid, "GroupCPEConfigRebuildDeviceConfigFileRequest");
                    $scaRebuild .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
                    $scaRebuild .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
                    $scaRebuild .= "<deviceName>" . $_SESSION["userInfo"]["deviceName"] . "</deviceName>";
                    $scaRebuild .= xmlFooter();
                    $sca[] = $scaRebuild;

                    // Reset Device
                    $scaReset  = xmlHeader($sessionid, "GroupCPEConfigResetDeviceRequest");
                    $scaReset .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
                    $scaReset .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
                    $scaReset .= "<deviceName>" . $_SESSION["userInfo"]["deviceName"] . "</deviceName>";
                    $scaReset .= xmlFooter();
                    $sca[] = $scaReset;
                }
			}
			
			
		}
		
		if($key == "scaServicePack"){ 
			//code to assign service pack to user
			
			$changeSCAAssignLog = array();
			$grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
			$servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
			$servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray);
			$scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);
			
			
			foreach($scaServiceArray as $ssa=>$ssv){
				$pos = strpos($ssv->col[0], "Shared Call Appearance");
				if($pos === false){
				}else{
					if($ssv->count == "1"){
						$servicePackToBeAssigned = $servicePackArray[$ssa][0];
					}
				}
			}
			
			
			if(count($value) > 0){		
				foreach($value as $scaUserkey=>$scaUserVal){
					/*Check User Service Pack checked or not..*/
					$userServices = new userServices();
					$userSessionservicePackArray = $userServices->getUserServicesAll($scaUserVal, $_SESSION["sp"]);
					$servicesSessionArray = $grpServiceList->getServicePackServicesListUsers($userSessionservicePackArray["Success"]["ServicePack"]["assigned"]);
					$scaSessionServiceArray = $grpServiceList->checkScaUserServiceInServicepack($servicesSessionArray);
					
					//code ends
					if(count($scaSessionServiceArray) == 0){
						$responseServicePackUser = $grpServiceList->assignServicePackUser($scaUserVal, $servicePackToBeAssigned);
						if(empty($responseServicePackUser["Error"])){
							$message .= "<li>".$servicePackToBeAssigned." SCA service pack has been assigned to " . $scaUserVal. ".</li>";
							
						}
					}
					//code ends for assign service pack to user
				}
				$services = new Services();
				$oldScaAssignUsersId = array();
				$newValue = array();
				
				$oldScaUsers = $services->compareSessionScaUser();
				$oldScaUsersId = array();
							
				foreach($oldScaUsers["IdArray"] as $oKey=>$oVal){
					$oldScaUsersIdArray = explode(":", $oVal);
					$oldScaAssignUsersId[] = $oldScaUsersIdArray[0].": ".$servicePackToBeAssigned;
					
				}
				foreach($value as $kVal){
					$newValue[] = $kVal.": ".$servicePackToBeAssigned;
				}
				$changeSCAAssignLog[0]["field"] = "scaServicePackAssign";
				$changeSCAAssignLog[0]["old"] = implode(", ", $oldScaAssignUsersId);
				$changeSCAAssignLog[0]["new"] = implode(", ", $newValue);
				$a++;
			}			
		}
		if($key == "scaServicePackUnassign"){ 
			
			$grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
			$servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
			$userservicePackArray = array();
			$servicePackToBeUnAssigned = array();
			
			
			if(count($scaOperationArray["unAssign"]) > 0){
				$messageStrLI = "";
				foreach ($scaOperationArray["unAssign"] as $uKey => $uVal)
				{
					if ($uVal !== "scaUsers" and $uVal !== "")
					{
						$exp2 = explode(":", $uVal);
						$uid = $exp2[0];
						
							$userServices = new userServices();
							$userservicePackArray = $userServices->getUserServicesAll($exp2[0], $_SESSION["sp"]);
							$servicesArray = $grpServiceList->getServicePackServicesListUsers($userservicePackArray["Success"]["ServicePack"]["assigned"]);
							$scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);
							foreach($scaServiceArray as $ssa=>$ssv){
								$pos = strpos($ssv->col[0], "Shared Call Appearance");
								if($pos === false){
								}else{
									if($ssv->count == "1"){
										$servicePackToBeUnAssigned[] = $userservicePackArray["Success"]["ServicePack"]["assigned"][$ssa];
									}
								}
							}
							
							foreach($servicePackToBeUnAssigned as $servicePackToBeUnAssignedVal){
								$unAssignScaResponse = $grpServiceList->unAssignServicePackUser($uid, $servicePackToBeUnAssignedVal);
								if(empty($unAssignScaResponse["Error"])){
									$messageStrLI = "<li>".$servicePackToBeUnAssignedVal." SCA service pack has been un assigned to " . $uVal. ".</li>";
									
								}
								$unAssignId[] = $uid.": ".$servicePackToBeUnAssignedVal;
							}
						
					}
					$messageStr[] = $messageStrLI;
				}
					foreach($messageStr as $strKey=>$strVal){
						$message .= $strVal;
					}
				
					$servicesArray = "";
					$changeSCAUnassignLog = array();
					
					$services = new Services();
					$oldScaUsers = $services->compareSessionScaUser();
					$oldScaUnAssignUsersId = array();
					foreach($oldScaUsers["IdArray"] as $oKey=>$oVal){
						$oldScaUsersIdArray = explode(":", $oVal);
						$oldScaUnAssignUsersId[] = $oldScaUsersIdArray[0];
					}
					foreach($value as $kVal){
						$newValue[] = $kVal.": ".$servicePackToBeAssigned;
					}
					
					$changeSCAUnassignLog[0]["field"] = "unAssignScaServicePack";
					//$changeSCAUnassignLog[0]["old"] = implode(",", $oldScaUnAssignUsersId);
					$changeSCAUnassignLog[0]["old"] = "None";
					$changeSCAUnassignLog[0]["new"] = implode(", ", $unAssignId);
					$a++;
				
			}
			//code ends
		}
		if ($key == "primary")
		{ 
			$primary = xmlHeader($sessionid, "GroupAccessDeviceModifyUserRequest");
			$primary .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
			$primary .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
			$primary .= "<deviceName>" . $_SESSION["userInfo"]["deviceName"] . "</deviceName>";
			$primary .= "<linePort>" . $value["primary"] . "</linePort>";
			$primary .= "<isPrimaryLinePort>true</isPrimaryLinePort>";
			$primary .= xmlFooter();
			
		}
        
        if ($key == "device")
		{
		    if(isset($_POST["userTypeResult"]) && $_POST["userTypeResult"] == "analog")
		    {
		        $_SESSION["userInfo"]["deviceType"] = $_POST["deviceType"];
		        require_once ("/var/www/html/Express/userMod/analogUserData.php");
		        $dop = new DeviceOperations();
		        
		        $deviceNameForAnalog = getDeviceName($_POST['deviceIndex']);
		       
		        if( !deviceExists($deviceNameForAnalog)) {
    		        $postArray = array();
    		        $postArray['sp'] = $_SESSION["sp"];
    		        $postArray['groupId'] = $_SESSION["groupId"];
    		        $postArray['deviceName'] = $deviceNameForAnalog;
    		        $postArray['deviceType'] = $_SESSION["userInfo"]["deviceType"];
    		        $postArray['macAddress'] = isset($_POST["macAddress"]) ? $_POST["macAddress"]: "";
    		        $addResponse = $dop->deviceAddRequest($postArray);
    		        if(empty($addResponse['Error'])) {
    		            $deviceNameForAnalog = $addResponse['Success'];
    		        }
		        }
		    } 
		    
		    //code start done for EX-760 to remove the user from SAS Test User when device assign to user is none
		    if(empty($_POST["deviceType"])){ 
		    	
		    	$deleteArray[] = $userId;
		    	if(count($deleteArray) > 0){
		    		require_once("/var/www/lib/broadsoft/adminPortal/sasOperation/sasOperation.php"); //Added 26 Oct 2018
		    		
		    		$fileName = "../../../SASTestingUser/SASTestUsers.csv";
		    		$sasObject = new sasOperation();
		    		$sasObject->deleteFromCSVFile($deleteArray, $fileName);
		    	}
		    }
		    //code ends done for EX-760 to remove the user from SAS Test User when device assign to user is none
		    include("rebuildDevice.php");
		    $device = "true";
		}
		
// 		//deviceIndex
		if($key == "deviceIndex" && !isset($changes['device']))
		{	
		    if(isset($_POST["userTypeResult"]) && $_POST["userTypeResult"] == "analog")
		    { 
		        $changes["device"]["deviceType"] = $_POST["deviceType"] = $_SESSION["userInfo"]["deviceType"];
		        require_once ("/var/www/html/Express/userMod/analogUserData.php");
		        $dop = new DeviceOperations();
		        
		        $deviceNameForAnalog = getDeviceName($changes['deviceIndex']);
		        if( !deviceExists($deviceNameForAnalog)) {
		            $postArray = array();
		            $postArray['sp'] = $_SESSION["sp"];
		            $postArray['groupId'] = $_SESSION["groupId"];
		            $postArray['deviceName'] = $deviceNameForAnalog;
		            $postArray['deviceType'] = $_SESSION["userInfo"]["deviceType"];
		            $postArray['macAddress'] = isset($_POST["macAddress"]) ? $_POST["macAddress"]: "";
		            $addResponse = $dop->deviceAddRequest($postArray);
		            if(empty($addResponse['Error'])) {
		                $deviceNameForAnalog = $addResponse['Success'];
		            }
		        }
		    } 
		    include("rebuildDevice.php");
		    $device = "true";
		    
		}
		
// 		//portNumber
		if ($_POST["userTypeResult"] == "analog" && $key == "portNumber" && !isset($changes['device']) && !isset($changes['deviceIndex']))
		{
		    $deviceNameForAnalog = $_SESSION["userInfo"]["deviceName"];
		    $changes["device"]["deviceType"] = $_POST["deviceType"] = $_SESSION["userInfo"]["deviceType"];
		    include("rebuildDevice.php");
		    $device = "true";
		}

	   //portNumber for SIP Device
		if ($_POST["userTypeResult"] == "digital" && $key == "portNumber" && !isset($changes['device']))
		{
		    $deviceName = $_SESSION["userInfo"]["deviceName"];
		    $changes["device"]["deviceType"] = $_POST["deviceType"] = $_SESSION["userInfo"]["deviceType"];
		    include("rebuildDevice.php");
		    $device = "true";
		}
		
		if ($key == "speedDial8")
		{
			$speedDial8 = xmlHeader($sessionid, "UserSpeedDial8ModifyListRequest");
			$speedDial8 .= "<userId>" . $userId . "</userId>";
			foreach ($value as $k => $v)
			{
				for ($i = 2; $i <= 9; $i++)
				{
					$speedDial8 .= "<speedDialEntry>";
					$speedDial8 .= "<speedCode>" . $i . "</speedCode>";
					if (trim($v[$i]["phoneNumber"]) == "")
					{
						$speedDial8 .= "<phoneNumber xsi:nil=\"true\" />";
					}
					else
					{
						$speedDial8 .= "<phoneNumber>" . $v[$i]["phoneNumber"] . "</phoneNumber>";
					}
					if (trim($v[$i]["name"]) == "")
					{
						$speedDial8 .= "<description xsi:nil=\"true\" />";
					}
					else
					{
						$speedDial8 .= "<description>" . htmlspecialchars($v[$i]["name"]) . "</description>";
					}
					$speedDial8 .= "</speedDialEntry>";
				}
			}
			$speedDial8 .= xmlFooter();
		}
		if ($key == "speedCode")
		{
			//delete existing codes
			$speedDial100_1 = xmlHeader($sessionid, "UserSpeedDial100DeleteListRequest");
			$speedDial100_1 .= "<userId>" . $userId . "</userId>";
			if (isset($_SESSION["userInfo"]["speedDial100"]))
			{
				foreach ($_SESSION["userInfo"]["speedDial100"] as $k => $v)
				{
					$speedDial100_1 .= "<speedCode>" . str_pad($v["speedCode"], 2, "0", STR_PAD_LEFT) . "</speedCode>";
				}
			}
			$speedDial100_1 .= xmlFooter();

			//readd codes
			if (count($_POST["speedCode"]) > 1)
			{
				$speedDial100_2 = xmlHeader($sessionid, "UserSpeedDial100AddListRequest");
				$speedDial100_2 .= "<userId>" . $userId . "</userId>";
				for ($i = 0; $i < count($_POST["speedCode"]); $i++)
				{
					if ($_POST["speedCode"][$i] !== "NIL")
					{
						$speedDial100_2 .= "<speedDialEntry>";
						$speedDial100_2 .= "<speedCode>" . $_POST["speedCode"][$i] . "</speedCode>";
						$speedDial100_2 .= "<phoneNumber>" . $_POST["phone"][$i] . "</phoneNumber>";
						if (trim($_POST["description"][$i]) !== "")
						{
							$speedDial100_2 .= "<description>" . $_POST["description"][$i] . "</description>";
						}
						$speedDial100_2 .= "</speedDialEntry>";
					}
				}
				$speedDial100_2 .= xmlFooter();
			}
		}
		//set call processing policy
		if ($key == "user" or $key == "phone")
		{
			foreach ($value as $k => $v)
			{
				if ($k == "callingLineIdPhoneNumber" or $k == "phoneNumber")
				{
					$callProcessingMod = xmlHeader($sessionid, "UserCallProcessingModifyPolicyRequest14sp7");
					$callProcessingMod .= "<userId>" . $userId . "</userId>";
					if ($_POST["callingLineIdPhoneNumber"] !== $_POST["phoneNumber"])
					{
					   //$callProcessingMod .= "<useUserCLIDSetting>true</useUserCLIDSetting>";
					  // $callProcessingMod .= "<clidPolicy>Use Configurable CLID</clidPolicy>";
					  // $callProcessingMod .= "<emergencyClidPolicy>Use Configurable CLID</emergencyClidPolicy>";
					    $callProcessingMod .= "<useUserCLIDSetting>" .$_SESSION["userInfo"]["useUserCLIDSetting"] . "</useUserCLIDSetting>";
					    $callProcessingMod .= "<clidPolicy>" .$_SESSION["userInfo"]["clidPolicy"] . "</clidPolicy>";
					    $callProcessingMod .= "<emergencyClidPolicy>" .$_SESSION["userInfo"]["emergencyClidPolicy"] . "</emergencyClidPolicy>";
					}
// 					else
// 					{
// 						$callProcessingMod .= "<useUserCLIDSetting>false</useUserCLIDSetting>";
// 						$callProcessingMod .= "<clidPolicy>Use DN</clidPolicy>";
// 						$callProcessingMod .= "<emergencyClidPolicy>Use DN</emergencyClidPolicy>";
// 					}
					$callProcessingMod .= xmlFooter();
				}
			}
		}
		
		
		if($key == "cpgGroupName")
		{
			$gcpObject = new GroupCallPickupUsers($userId);
			$gcpChangeLog["key"] = "cpgGroupName";
			$gcpChangeLog["old"] = $gcpNameOld = $gcpObject->findCallPickupGroupByUsername();
			$gcpChangeLog["new"] = $_POST['cpgGroupName'];
			$gpName ="";
			if(isset($gcpNameOld) && !empty($gcpNameOld) && !empty($_POST['cpgGroupName'])){
				$gpName = $gcpNameOld;
				$Obj = new GroupCallPickupUsers($gpName);
				$gcpExistingUserinGroup = $Obj->getUsersInCallPickupGroup();
				if(($key = array_search($userId, $gcpExistingUserinGroup)) !== false) {
					unset($gcpExistingUserinGroup[$key]);
				}

				//unassing by removing the user from call pickup group
				$status = $Obj->modifyUserCallPickupGroup($gcpExistingUserinGroup);

				 // $message will not be set.
				if($status == "Success"){
					sleep(2);
					$Object = new GroupCallPickupUsers($_POST["cpgGroupName"]);
					$gcpExistingUserinGroup = $Object->getUsersInCallPickupGroup();
					array_push($gcpExistingUserinGroup, $userId);

					$status = $Object->modifyUserCallPickupGroup($gcpExistingUserinGroup);
					if($status == "Success" && $gcpNameOld <> $_POST['cpgGroupName']){
						//if(status == error) then
						//set gcpChangeLog == null
						//$message will not be set.
						$message .= "<li>Call Pickup Group " .$gcpNameOld. " has been changed to " .$_POST["cpgGroupName"]. " Call Pickup Group.</li>";
					}else{
						$gcpChangeLog = array("key"=>"", "old"=>"", "new"=>"");
						$message .= "";
					}
				}else{
					$gcpChangeLog = array("key"=>"", "old"=>"", "new"=>"");
					$message .= "";
				}

			}else if(isset($gcpNameOld) && !empty($gcpNameOld) && empty($_POST['cpgGroupName'])){
				$gpName = $gcpNameOld;
				$Obj = new GroupCallPickupUsers($gpName);
				$gcpExistingUserinGroup = $Obj->getUsersInCallPickupGroup();
				if(($key = array_search($userId, $gcpExistingUserinGroup)) !== false) {
					unset($gcpExistingUserinGroup[$key]);
				}
				//unassing by removing the user from call pickup group
				$status = $Obj->modifyUserCallPickupGroup($gcpExistingUserinGroup);
				if($status == "Success"){
					$message .= "<li>".$userId. " has been removed from " .$gcpNameOld. " Call Pickup Group.</li>";
				}
			}else if(empty($gcpNameOld) && !empty($_POST['cpgGroupName'])){
				$Obj = new GroupCallPickupUsers($_POST["cpgGroupName"]);
				$gcpExistingUserinGroup = $Obj->getUsersInCallPickupGroup();
				array_push($gcpExistingUserinGroup, $userId);
				$status = $Obj->modifyUserCallPickupGroup($gcpExistingUserinGroup);
				if($status == "Success"){
					$message .= "<li>".$userId. " has been assigned to " .$_POST["cpgGroupName"]. " Call Pickup Group.</li>";
				}
				 // $message will not be set.
			}

		}
		if ($key == "servicePackAssigned") {
            $servicePackAddReq = xmlHeader($sessionid, "UserServiceAssignListRequest");
            $servicePackAddReq .= "<userId>" . $userId . "</userId>";
            foreach ($value as $servicePack) {
                $servicePackAddReq .= "<servicePackName>" . htmlspecialchars($servicePack) . "</servicePackName>";
            }
            $servicePackAddReq .= xmlFooter();
        }
        if ($key == "servicePackUnassigned") {
            // get Surge Mail User ID before VM Service will potentially be unassigned.
            $assignedServices = new UserServiceGetAssignmentList($userId);
            if ($assignedServices->hasService("Voice Messaging User")) {
                $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetAdvancedVoiceManagementRequest14sp3");
                $xmlinput .= "<userId>" . $userId . "</userId>";
                $xmlinput .= xmlFooter();
                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                $surgeMailUserId = isset($xml->command->groupMailServerUserId) ? strval($xml->command->groupMailServerUserId) : "";
            }
            
            if(!empty($value[0])){
	            $servicePackRemoveReq = xmlHeader($sessionid, "UserServiceUnassignListRequest");
	            $servicePackRemoveReq .= "<userId>" . $userId . "</userId>";
	            foreach ($value as $servicePack) {
	                $servicePackRemoveReq .= "<servicePackName>" . htmlspecialchars($servicePack) . "</servicePackName>";
	            }
	            $servicePackRemoveReq .= xmlFooter();
            }
        }
        if($key == "webAccessPassword"){
        	
        	//if reset default checked and web access password is changed
        	if (isset($_POST["resetWebDefault"])) {
        		$value = setWebPassword((int)$_SESSION["groupRules"]["minPasswordLength"]);
        		$passwordChange = $userPassword->modifyPasswordInfo($postUserId, $value);
        	}else{ 
        		//if password is changed without reset default
        		$value = $_POST["input-password"];
        		$passwordChange = $userPassword->updateResetPasswordDefault($postUserId, $value);
        	}
        	
        	if(empty($passwordChange["Error"])){
        		$message .= "<li> " . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
        		$changeLog["field"] = $key;
        		$changeLog["old"] = "None";
        		$changeLog["new"] = $value;
        	}else{
        		
        		$message .= "<li style='color:red'> " . $_SESSION["userModNames"][$key] . " has not been changed to " . $value. " : Error : ".$passwordChange["Error"]." .</li>";
        	}
        }
        if($key == "voiceMailPasscode"){
        
        	$passcodeChange["Success"] = "";
        	$passcodeChange["Error"] = "";
        	
        	//passcode generate default if reset checkbox checked
        	if (isset($_POST["resetPassDefault"])) {
                    
        		$value = $userPassword->updateResetPasscodeDefault($bwVoicePortalPasswordType, $userVoicePortalPasscodeFormula);
                        //Code added @ 08 May 2019 regarding Ex-1318
                        if(is_numeric($value) && strlen($value) < 6){
                            $value = "476983";
                        }
                        if(!is_numeric($value)){
                            $value = "476983";
                        }
                        //End code
                        
                        
                }else{ 
        		//if not checked
        		$value = $_POST["input-passcode"];
        	}
                
		$portalPassWord1 = $value;
                        
                        /*if(is_numeric($portalPassWord1))
                        {
                             $portalPassWord1 = $portalPassWord1;
                        }
                        else
                        {
                             $criteria = $userVoicePortalPasscodeFormula;
                             preg_match('#\((.*?)\)#', $criteria, $match);
                             $var = $match[1];
                             $portalPassWord1 = "";
                             for($i = 1; $i<= $var; $i++){
                                 $portalPassWord1 .= $i;
                             }
                        }*/
        	//ends
       		$passcodeChange = $userPassword->modifyPortalPasscodeInfo($postUserId, $portalPassWord1);
       		if(empty($passcodeChange["Error"])){ 
        		$message .= "<li> " . $_SESSION["userModNames"][$key] . " has been changed to " . $portalPassWord1 . ".</li>";
       		}else{ 
        		$message .= "<li style='color:red'> " . $_SESSION["userModNames"][$key] . " has not been changed to " . $portalPassWord1. " : Error : ".$passcodeChange["Error"]." .</li>";
        	}
        }
	}

    $a = 0;
	if (isset($deptMod))
	{
		$response = $client->processOCIMessage(array("in0" => $deptMod));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			foreach ($changes["department"] as $key => $value)
			{
				if ($value == "")
				{
					$value = "None";
				}
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}

	if (isset($userMod))
	{
		$response = $client->processOCIMessage(array("in0" => $userMod));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			foreach ($changes["user"] as $key => $value)
			{
				if ($value == "")
				{
					$value = "None";
				}
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}

	/*if (isset($clidPolicy)) {
		$response = $client->processOCIMessage(array("in0" => $clidPolicy));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult != "Error") {
			foreach ($changes["callingLineIdPolicy"] as $key => $value) {
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key];
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}*/
	
	if (count($changes["callingLineIdPolicy"]) > 0) {
	        foreach ($changes["callingLineIdPolicy"] as $key => $value) {
	            $message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
	            $changeLog[$a]["field"] = $key;
	            $changeLog[$a]["old"] = $_SESSION["userInfo"][$key];
	            $changeLog[$a]["new"] = $value;
	            $a++;
	        }
	}

	if (isset($addMod))
	{
		$response = $client->processOCIMessage(array("in0" => $addMod));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			foreach ($changes["address"] as $key => $value)
			{
				if ($value == "")
				{
					$value = "None";
				}
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}

	if (isset($servicePackAddReq)) {
	    $initialUserServicesAssignmentList = new UserServiceGetAssignmentList($userId);
        $response = $client->processOCIMessage(array("in0" => $servicePackAddReq));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if(isset($xml->command->summary) && $xml->command->summary !=""){
            foreach ($changes["servicePackAssigned"] as $pack) {
                $message .= "<li style='color:red' >Service Pack: " . $pack . " can not assign to user.</li>";
                //Code added @ 03 Jan 2019
                $message .= "<li style='color:red' >" . strval($xml->command->summaryEnglish)."</li>";                
                if(stripos(strval($xml->command->detail), "Service pack") !==false){
                    
                }else{
                    $serviceList      = $srvceObj->getSericesListOfServicePack($pack);                                                    
                    $serviceErrorList = $srvceObj->getServiceLicenseErrorOnAssignServicePackToUser($_SESSION["groupId"], $serviceList);
                    foreach($serviceErrorList as $errorServiceName => $serviceErrorMessage){
                        $message .= "<li style='color:red' >$errorServiceName : $serviceErrorMessage</li>";
                    }
                }                               
                //End code
            }
       }else{
           foreach ($changes["servicePackAssigned"] as $pack) {
               $message .= "<li>Service Pack: " . $pack . " has been assigned to the user.</li>";
           }
       }
        
    
    }

    if (isset($servicePackRemoveReq)) {
	    if ($initialUserServicesAssignmentList == null) {
            $initialUserServicesAssignmentList = new UserServiceGetAssignmentList($userId);
        }
        $response = $client->processOCIMessage(array("in0" => $servicePackRemoveReq));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errorResult = readError($xml);
        if ($errorResult !== "Error") {
            foreach ($changes["servicePackUnassigned"] as $pack) {
                $message .= "<li>Service Pack: " . $pack . " has been unassigned from the user.</li>";
            }
        }
    }

    if (isset($servicePackAddReq) || isset($servicePackRemoveReq)) {
        determineVMServicePacks();
        $finalUserServicesAssignmentList = new UserServiceGetAssignmentList($userId);

        // process BLF service initialization
        if ( ! $initialUserServicesAssignmentList->hasService("Busy Lamp Field") && $finalUserServicesAssignmentList->hasService("Busy Lamp Field")) {
            $blfModify = new ModifyBLFService($userId, $deviceName);
        }

        // Process VM service initialization
        if ( ! $initialUserServicesAssignmentList->hasService("Voice Messaging User") && $finalUserServicesAssignmentList->hasService("Voice Messaging User")) {
            $emailAddress = getSurgeMailAddress();
            $passCode = getVoicePortalPassCode();
            $vmService = new ModifyVMService($userId);
            $vmService->add($passCode, $emailAddress);
        }

        // Remove Surge Mail on VM Service removal
        if ( $initialUserServicesAssignmentList->hasService("Voice Messaging User") && ! $finalUserServicesAssignmentList->hasService("Voice Messaging User")) {
            $vmService = new ModifyVMService($userId);
            $vmService->remove($surgeMailUserId);
        }
    }

	if (isset($voiceManagement))
	{
		$response = $client->processOCIMessage(array("in0" => $voiceManagement));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			foreach ($changes["voiceManagement"] as $key => $value)
			{
				if ($value == "")
				{
					$value = "None";
				}
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}

	if (isset($portalPass))
	{
		$response = $client->processOCIMessage(array("in0" => $portalPass));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			$message .= "<li>Your new " . $_SESSION["userModNames"]["portal"] . " is: " . $portalPassWord . "</li>";
			$changeLog[$a]["field"] = "portal";
			$changeLog[$a]["old"] = str_repeat("*", 6); //don't display password
			$changeLog[$a]["new"] = str_repeat("*", strlen($portalPassWord)); //don't display password
			$a++;
		}
	}

	if (isset($webPass))
	{
		$response = $client->processOCIMessage(array("in0" => $webPass));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			$message .= "<li>Your new " . $_SESSION["userModNames"]["web"] . " is: " . $webPassWord . "</li>";
			$changeLog[$a]["field"] = "web";
			$changeLog[$a]["old"] = str_repeat("*", 9); //don't display password
			$changeLog[$a]["new"] = str_repeat("*", strlen($webPassWord)); //don't display password
			$a++;
		}
	}

	if (isset($hotelHost))
	{
		$response = $client->processOCIMessage(array("in0" => $hotelHost));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			foreach ($changes["hotelHost"] as $key => $value)
			{
				if ($value == "")
				{
					$value = "None";
				}
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}

	if (isset($hotelGuest))
	{
		$response = $client->processOCIMessage(array("in0" => $hotelGuest));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			foreach ($changes["hotel"] as $key => $value)
			{
				if ($value == "")
				{
					$value = "None";
				}
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}

	if (isset($phoneMod))
	{
		$response = $client->processOCIMessage(array("in0" => $phoneMod));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			foreach ($changes["phone"] as $key => $value)
			{
				if ($value == "")
				{
					$value = "None";
				}
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
			
			
		}
	}
	if(isset($activateMod) || isset($phoneMod)){
		
		require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
		
		if(!empty($_POST["phoneNumber"])){
			if($_SESSION["userInfo"]["uActivateNumber"] == "No"){
				$oldValue = "Deactivated";
			}else{
				$oldValue = "Activated";
			}
			
			if($_POST["uActivateNumber"] == "No"){
				$newValue = "Deactivated";
			}else{
				$newValue = "Activated";
			}
			
			if($_POST["phoneNumber"] !== $_SESSION["userInfo"]["phoneNumber"]){
				if($_POST["uActivateNumber"] == "Yes"){
					$uDn = new Dns ();
					$dnPostArray = array(
							"sp" => $_SESSION['sp'],
							"groupId" => $_SESSION['groupId'],
							"phoneNumber" => array($_POST['phoneNumber'])
					);
					
					if($_POST["uActivateNumber"] == "Yes"){
						$dnsActiveResponse = $uDn->activateDNRequest($dnPostArray);
						
						$changeLog[$a]["field"] = $key;
						$changeLog[$a]["old"] = $oldValue;
						$changeLog[$a]["new"] = $newValue;
						$message .= "<li>Activate Number has been changed from " . $oldValue . " to " . $newValue. ".</li>";
					}
				}
			}else{ 
				if($_POST["uActivateNumber"] !== $_SESSION["userInfo"][$key]){
					$uDn = new Dns ();
					$dnPostArray = array(
							"sp" => $_SESSION['sp'],
							"groupId" => $_SESSION['groupId'],
							"phoneNumber" => array($_POST['phoneNumber'])
					);
					if($_POST["uActivateNumber"] == "Yes"){ //echo "Yes"; die;
						$dnsActiveResponse = $uDn->activateDNRequest($dnPostArray);
					}else{ //echo "No"; die;
						$dnsActiveResponse = $uDn->deActivateDNRequest($dnPostArray);
					}
					
					$changeLog[$a]["field"] = $key;
					$changeLog[$a]["old"] = $oldValue;
					$changeLog[$a]["new"] = $newValue;
					$message .= "<li>Activate Number has been changed from " . $oldValue . " to " . $newValue. ".</li>";
				}
			}
		}
	}
	if (isset($thirdPartyMod_1))
	{
		$response = $client->processOCIMessage(array("in0" => $thirdPartyMod_1));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			$response = $client->processOCIMessage(array("in0" => $thirdPartyMod_2));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			$errorResult = readError($xml);
			if ($errorResult !== "Error")
			{
                                $sugeObj = new SurgeMailOperations();
                            
				$response = $client->processOCIMessage(array("in0" => $thirdPartyMod_3));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
				$errorResult = readError($xml);
				if ($errorResult !== "Error")
				{
					if (isset($surgemailemail))
					{
						$surgemailPost = array ("cmd" => "cmd_user_login",
									"lcmd" => "user_delete",
									"show" => "simple_msg.xml",
									"username" => $surgemailUsername,
									"password" => $surgemailPassword,
									"lusername" => $surgemailemail,
									"lpassword" => $emailPassword,
									"uid" => $userid);
						//$result = http_post_fields($surgemailURL, $surgemailPost);
                                                $srgeRsltDelUsr = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPost); //Added @ 12 May 2018

						$surgemailPost = array ("cmd" => "cmd_user_login",
									"lcmd" => "user_create",
									"show" => "simple_msg.xml",
									"username" => $surgemailUsername,
									"password" => $surgemailPassword,
									"lusername" => $surgemailemail,
									"lpassword" => $emailPassword,
									"uid" => $userid);
						//$result = http_post_fields($surgemailURL, $surgemailPost);
                                                $srgeRsltCrteUsr = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPost); //Added @ 12 May 2018
                                                
					}
					foreach ($changes["thirdParty"] as $key => $value)
					{
						if ($value == "")
						{
							$value = "None";
						}
						$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
						$changeLog[$a]["field"] = $key;
						$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
						$changeLog[$a]["new"] = $value;
						$a++;
					}
				}
			}
		}
	}

	if (isset($callTransfer))
	{
		$response = $client->processOCIMessage(array("in0" => $callTransfer));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			foreach ($changes["callTransfer"] as $key => $value)
			{
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}

	if (isset($cfa))
	{
		$response = $client->processOCIMessage(array("in0" => $cfa));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
		    foreach($changes["cfa"] as $key1 => $value1){
		        if($_SESSION["userInfo"]["cfaForwardToPhoneNumber"] == $value1){
		            unset($changes["cfa"][$key1]);
		        }
		    }
		    
			foreach ($changes["cfa"] as $key => $value)
			{
				if ($value == "")
				{
					$value = "None";
				}
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}

	if (isset($cfb))
	{
		$response = $client->processOCIMessage(array("in0" => $cfb));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
		    foreach($changes["cfb"] as $key1 => $value1){
		        if($_SESSION["userInfo"]["cfbForwardToPhoneNumber"] == $value1){
		            unset($changes["cfb"][$key1]);
		        }
		    }
			foreach ($changes["cfb"] as $key => $value)
			{
			   if ($value == "")
				{
					$value = "None";
				}
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}

	if (isset($cfn))
	{
		$response = $client->processOCIMessage(array("in0" => $cfn));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
		    foreach($changes["cfn"] as $key1 => $value1){
		        if($_SESSION["userInfo"]["cfnForwardToPhoneNumber"] == $value1){
		            unset($changes["cfn"][$key1]);
		        }
		    }
			foreach ($changes["cfn"] as $key => $value)
			{
				if ($value == "")
				{
					$value = "None";
				}
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}

	if (isset($cfr))
	{
		$response = $client->processOCIMessage(array("in0" => $cfr));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
		    foreach($changes["cfr"] as $key1 => $value1){
		        if($_SESSION["userInfo"]["cfrForwardToPhoneNumber"] == $value1){
		            unset($changes["cfr"][$key1]);
		        }
		    }
			foreach ($changes["cfr"] as $key => $value)
			{
				if ($value == "")
				{
					$value = "None";
				}
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}

	if (isset($blf))
	{
		$response = $client->processOCIMessage(array("in0" => $blf));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			$message .= "<li>Your new list of Busy Lamp Fields users is: ";
			if ($changes["blf"] != "NIL")
			{
				$blfMessage = "";
				$blfUSERS = "";
				for ($b = 0; $b < count($changes["blf"]); $b++)
				{
				    $blfMessage .= $changes["blf"][$b]["name"] ." ".$changes["blf"][$b]["linePort"]. "; ";
					$blfUSERS .= $changes["blf"][$b]["name"] . "&";
				}
				$blfMessage = substr($blfMessage, 0, -2);
			}
			else
			{
				$blfMessage = "None";
			}
			$message .= $blfMessage . ".</li>";
			$changeLog[$a]["field"] = "monitoredUsers";
			if (isset($_SESSION["userInfo"]["monitoredUsers"]))
			{
				$oldBlfUSERS = "";
				foreach ($_SESSION["userInfo"]["monitoredUsers"] as $key => $value)
				{
					$oldBlfUSERS .= $value["name"] . "&";
				}
			}
			if (isset($oldBlfUSERS))
			{
				$changeLog[$a]["old"] = $oldBlfUSERS;
			}
			else
			{
				$changeLog[$a]["old"] = "None&";
			}
			if (isset($blfUSERS) and $blfUSERS != "")
			{
				$changeLog[$a]["new"] = $blfUSERS;
			}
			else
			{
				$changeLog[$a]["new"] = "None&";
			}
			$a++;
		}
	}

	if (isset($sca))
	{ 
		foreach ($sca as $scaValue)
		{
		    
			$response = $client->processOCIMessage(array("in0" => $scaValue));
			
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			$errorResult = readError($xml);
		}
			$message .= "<li>Your new list of Shared Call Appearance users is: ";
			if ($changes["sca"] != "NIL")
			{
			    $userIdMod = $userId;
			    $userId = $postUserId;
			    
			    
			    require_once("/var/www/lib/broadsoft/adminPortal/getUserInfo.php");
			    $userId = $userIdMod;
				$scaMessage = "";
				$unassignArray = array();
				if(count($changes["sca"]) > 0){
				    $i = 0;
				    foreach($changes["sca"] as $key => $value){
				        $ifExist = searchForId($value["id"], $userInfo['sharedCallAppearanceUsers']);
				        if(!$ifExist){
				            $unassignArray[$i]['id'] = $value["id"];
				            $unassignArray[$i]['name'] = $value["name"];
				            unset($changes["sca"][$key]);
				            $i++;
				        }

				    }
				}
				
				$changes["sca"] = array_values($changes["sca"]);
				$sessionServicePackToBeAssigned = "";
				for ($b = 0; $b < count($changes["sca"]); $b++)
				{
// 					$userServices = new userServices();
// 					$userSessionservicePackArray = $userServices->getUserServicesAll($changes["sca"][$b]["id"], $_SESSION["sp"]);
// 					$servicesSessionArray = $grpServiceList->getServicePackServicesListUsers($userSessionservicePackArray["Success"]["ServicePack"]["assigned"]);
// 					//echo "User Array=><br/>";print_R($userSessionservicePackArray);//die;
// 					$scaSessionServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesSessionArray);
// 					//echo "user SCA service pack ==> <br/>";print_R($scaSessionServiceArray);
// 					foreach($scaSessionServiceArray as $ssa=>$ssv){
// 						$pos = strpos($ssv->col[0], "Shared Call Appearance");
// 						if($pos === false){
// 						}else{
// 							if($ssv->count == "1"){
// 								$sessionServicePackToBeAssigned = $userservicePackArray["Success"]["ServicePack"]["assigned"][$ssa];
// 								$sessionServicePackToBeAssigned = "( SCA : Assign Service Pack : ".$changes["sca"][$b]["id"].", ".$userSessionservicePackArray["Success"]["ServicePack"]["assigned"][$ssa]." )";
// 							}
// 						}
// 					}
					//echo $sessionServicePackToBeAssigned;die;
					$scaMessage .= $changes["sca"][$b]["name"] ." ".$changes["sca"][$b]["linePort"]. $sessionServicePackToBeAssigned."; ";
				}
				$scaMessage = substr($scaMessage, 0, -2);
			}
			else
			{
				$scaMessage = "None";
			}
			
			$message .= $scaMessage . ".</li>";
			$changeLog[$a]["field"] = "scaUsers";
			if (isset($_SESSION["userInfo"]["sharedCallAppearanceUsers"]))
			{
				$oldScaUSERS = "";
				for ($b = 0; $b < count($_SESSION["userInfo"]["sharedCallAppearanceUsers"]); $b++)
				{ $sessionServicePackToBeAssigned = "";
					if ($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$b]["endpointType"] == "Shared Call Appearance")
					{
						$oldScaUSERS .= $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$b]["name"] .$sessionServicePackToBeAssigned . "; ";
					}
				}
				$oldScaUSERS = substr($oldScaUSERS, 0, -2);
			}
			if(count($unassignArray) > 0){
			    $unAssignArrayNew = array();
			    foreach($unassignArray as $key => $value){
			        $unAssignArrayNew[] = $value["name"];
			    }
			    $message .= "<li>The SCA can not Assigned: " . implode('; ', $unAssignArrayNew) . ".</li>";
			}
			if (isset($oldScaUSERS) and $oldScaUSERS != "")
			{
				$changeLog[$a]["old"] = $oldScaUSERS;
			}
			else
			{
				$changeLog[$a]["old"] = "None";
			}
			if (isset($scaMessage) and $scaMessage != "")
			{
				$changeLog[$a]["new"] = $scaMessage;
			}
			else
			{
				$changeLog[$a]["new"] = "None";
			}
			$a++;
//		}

			
	}

	if (isset($dnd))
	{
		$response = $client->processOCIMessage(array("in0" => $dnd));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			foreach ($changes["dnd"] as $key => $value)
			{
				if ($value == "")
				{
					$value = "None";
				}
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}

	if (isset($simultaneousRing))
	{
		$response = $client->processOCIMessage(array("in0" => $simultaneousRing));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			foreach ($changes["simultaneousRing"] as $key => $value)
			{
				/*$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;*/
			}
		}
	}

	if (isset($primary))
	{
		$response = $client->processOCIMessage(array("in0" => $primary));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			foreach ($changes["primary"] as $key => $value)
			{
				if ($value == "")
				{
					$value = "None";
				}
				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
				$changeLog[$a]["new"] = $value;
				$a++;
			}
		}
	}

	/* sca rebuild and reset */
	if( isset($changes['scaRebuiltReset']) ) {
	    require_once("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig.php");
	    if( isset($changes['scaRebuiltReset']["resetPhone_sca"]) || isset($changes['scaRebuiltReset']["rebuildPhoneFiles_sca"]) ) {
	        $message .= sCARebuiltAndResetDevicePhone($key, $changes);
	    }
	}
	
	
	if (isset($changes["deviceIndex"]))
	{
	        $value = $changes["deviceIndex"];
            if ($changes["deviceIndex"] == "")
	        {
	            $value = "None";
	        }
	        $message .= "<li>" . "Device Index" . " has been changed to " . $value . ".</li>";
	        $changeLog[$a]["field"] = "deviceIndex";
	        $changeLog[$a]["old"] = $_SESSION["userInfo"]["deviceIndex"];
	        $changeLog[$a]["new"] = $changes["deviceIndex"];
	}
	

	if (isset($changes["portNumber"]))
	{
	    $value = $changes["portNumber"];
	    if ($changes["portNumber"] == "")
	    {
	        $value = "None";
	    }
	    $message .= "<li>" . "Port Number" . " has been changed to " . $value . ".</li>";
	    $changeLog[$a]["field"] = "portNumber";
	    $changeLog[$a]["old"] = $_SESSION["userInfo"]["portNumber"];
	    $changeLog[$a]["new"] = $changes["portNumber"];
	}
	
	if (isset($device))
	{
		foreach ($changes["device"] as $key => $value)
		{
			if ($value == "")
			{
				$value = "None";
			}
			if($key == "modTagBundle"){
			    $message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to:  " . implode(",", $changes["device"][$key]) . ".</li>";
			    $changeLog[$a]["field"] = $key;
			    $changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? implode(",", $_SESSION["userInfo"][$key]) : "None";
			    $changeLog[$a]["new"] = implode(",", $changes["device"][$key]);
			    $a++;
			}else{
			    if( ($key == "resetPhone" || $key == "rebuildPhoneFiles") && $value == "false") {
			        continue;
			    }
			    $message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
			    $changeLog[$a]["field"] = $key;
			    $changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
			    $changeLog[$a]["new"] = $value;
			    $a++;
			}
			
		}
	}

	if (isset($speedDial8))
	{
		$response = $client->processOCIMessage(array("in0" => $speedDial8));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			foreach ($changes["speedDial8"] as $key => $value)
			{
				$sD8Old = "";
				$sD8New = "";
				for ($i = 2; $i <= 9; $i++)
				{
					if ($_SESSION["userInfo"]["speedDial8"][$i]["name"] or $_SESSION["userInfo"]["speedDial8"][$i]["phoneNumber"])
					{
						$sD8Old .= "Speed Code " . $i . ": " . $_SESSION["userInfo"]["speedDial8"][$i]["phoneNumber"] . " / " . $_SESSION["userInfo"]["speedDial8"][$i]["name"] . "; ";
					}
					if ($value[$i]["name"] or $value[$i]["phoneNumber"])
					{
						$sD8New .= "Speed Code " . $i . ": " . $value[$i]["phoneNumber"] . " / " . $value[$i]["name"] . "; ";
					}
				}
				if ($sD8Old)
				{
					$sD8Old = substr($sD8Old, 0, -2);
				}
				else
				{
					$sD8Old = "None";
				}
				if ($sD8New)
				{
					$sD8New = substr($sD8New, 0, -2);
				}
				else
				{
					$sD8New = "None";
				}

				$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $sD8New . ".</li>";
				$changeLog[$a]["field"] = $key;
				$changeLog[$a]["old"] = $sD8Old;
				$changeLog[$a]["new"] = $sD8New;
				$a++;
			}
		}
	}

	if (isset($speedDial100_1))
	{
		$response = $client->processOCIMessage(array("in0" => $speedDial100_1));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			if (isset($speedDial100_2))
			{
				$response = $client->processOCIMessage(array("in0" => $speedDial100_2));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
				$errorResult = readError($xml);
			}
			if ($errorResult !== "Error")
			{
				foreach ($changes["speedCode"] as $key => $value)
				{
					$sD100Old = "";
					$sD100New = "";
					if (isset($_SESSION["userInfo"]["speedDial100"]))
					{
						foreach ($_SESSION["userInfo"]["speedDial100"] as $k => $v)
						{
							$sD100Old .= "Speed Code " . str_pad($v["speedCode"], 2, "0", STR_PAD_LEFT) . ": " . $v["phone"] . " / " . $v["description"] . "; ";
						}
					}
					for ($i = 0; $i < count($_POST["speedCode"]); $i++)
					{
						if ($_POST["speedCode"][$i] !== "NIL")
						{
							$sD100New .= "Speed Code " . $_POST["speedCode"][$i] . ": " . $_POST["phone"][$i] . " / " . $_POST["description"][$i] . "; ";
						}
					}

					if ($sD100Old)
					{
						$sD100Old = substr($sD100Old, 0, -2);
					}
					else
					{
						$sD100Old = "None";
					}
					if ($sD100New)
					{
						$sD100New = substr($sD100New, 0, -2);
					}
					else
					{
						$sD100New = "None";
					}

					$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $sD100New . ".</li>";
					$changeLog[$a]["field"] = $key;
					$changeLog[$a]["old"] = $sD100Old;
					$changeLog[$a]["new"] = $sD100New;
					$a++;
				}
			}
		}
	}

	if (isset($callProcessingMod))
	{
		$response = $client->processOCIMessage(array("in0" => $callProcessingMod));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
	}

	if (isset($resetDevice))
	{
		$response = $client->processOCIMessage(array("in0" => $resetDevice));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if ($errorResult !== "Error")
		{
			$message .= "<li>Your device has been reset.</li>";
			$changeLog[$a]["field"] = "resetDevice";
			$changeLog[$a]["old"] = "";
			$changeLog[$a]["new"] = "true";
			$a++;
		}
	}


	if(isset($gcpChangeLog) && count($gcpChangeLog) > 0){
		if($gcpChangeLog["old"] <> $gcpChangeLog["new"]){
			$changeLog[$a]["field"] = "cpgGroupName";
			$changeLog[$a]["old"] = $gcpChangeLog["old"];
			$changeLog[$a]["new"] = $gcpChangeLog["new"];
			$a++;
		}
	}

	
    if($serviceOpRes["isServiceModified"]) {
        if( $serviceOpRes["changeLog"] != "") {
            $changeLog[$a]["field"] = "Assigned User Services";
            $changeLog[$a]["old"] = $serviceOpRes["changeLog"]["old"];
            $changeLog[$a]["new"] = $serviceOpRes["changeLog"]["new"];
            $a++;
        }
    }
	
	if(isset($_SESSION["softPhoneListToAdd"]) && !empty($_SESSION["softPhoneListToAdd"])){
	    $_POST["softPhoneAdd"] = "Add";
	    $_POST["softPhoneListToAdd"] = $_SESSION["softPhoneListToAdd"];
	}
	
	///////// Soft Phone Code   ////////////////
	if(isset($_POST["softPhoneAdd"]) && $_POST["softPhoneAdd"] == "Add"){
	    if(isset($_POST["softPhoneListToAdd"]) && count($_POST["softPhoneListToAdd"]) > 0){
	        //File To Add SoftPhone
	        //print_r("sdsdsdsdsdsd");
	        //print_r($_POST["softPhoneListToAdd"]); echo"HHHHH";
	        require_once("/var/www/html/Express/userMod/softPhone/softPhoneAddOperations.php");
	        //echo "session"; print_r($_SESSION["userInfo"]);
	        //echo "postdata"; print_r($_POST);
	    }
	}
	
	if(isset($_POST["softPhoneDeviceName"]) && count($_POST["softPhoneDeviceName"]) > 0){
	    require_once("/var/www/html/Express/userMod/softPhone/softPhoneDeleteOperations.php");
	}
	
	if(isset($_SESSION["softPhoneMod"]) && $_SESSION["softPhoneMod"] == "softPhoneMod"){
	    if(isset($_SESSION["softPhoneModData"]) && count($_SESSION["softPhoneModData"]["changes"]) > 0){
	        require_once("/var/www/html/Express/userMod/softPhone/softPhoneModifyOperations.php");
	    }
	}
	
	if(isset($changes["userCallingLineIdBlockingOverride"])){
	    $assignedServices = new UserServiceGetAssignmentList($userId);
	    if (!$assignedServices->hasService("Calling Line ID Delivery Blocking")) {
	        $servicesClidBlock = array("Calling Line ID Delivery Blocking");
	        $assingedSerRes = $srvceObj->assignServicesToUser($userId, $servicesClidBlock);
	        if($assingedSerRes["Success"] == "Success"){
	            $asingedCLIB = $srvceObj->userCallingLineIDDeliveryBlockingModifyRequest($userId,$changes["userCallingLineIdBlockingOverride"]);
	            if($changes["userCallingLineIdBlockingOverride"] == "true"){
	                $changeClidTemp = "Block";
	            }else{
	                $changeClidTemp = "Unblock";
	            }
	            if($asingedCLIB["Success"] == "Success"){
	                $message .= "<li>Calling Line ID Blocking has been changed to " . $changeClidTemp . ".</li>";
	                $changeLog[$a]["field"] = "userCallingLineIdBlockingOverride";
	                $changeLog[$a]["old"] = $_SESSION["userInfo"]["userCallingLineIdBlockingOverride"] == "true" ? "Block" : "Unblock";
	                $changeLog[$a]["new"] = $changeClidTemp;
	                $a++;
	            }else{
	                $message .= "<li style='color:red'> " . $asingedCLIB["Error"] . "</li>";
	            }
	            
	        }else{
	            $message .= "<li style='color:red'>There is an error to assign the service: Calling Line ID Blocking</li>";
	        }
	    }else{
	        $asingedCLIB = $srvceObj->userCallingLineIDDeliveryBlockingModifyRequest($userId,$changes["userCallingLineIdBlockingOverride"]);
	        if($changes["userCallingLineIdBlockingOverride"] == "true"){
	            $changeClidTemp = "Block";
	        }else{
	            $changeClidTemp = "Unblock";
	        }
	        if($asingedCLIB["Success"] == "Success"){
	            $message .= "<li>Calling Line ID Blocking has been changed to " . $changeClidTemp . ".</li>";
	            $changeLog[$a]["field"] = "userCallingLineIdBlockingOverride";
	            $changeLog[$a]["old"] = $_SESSION["userInfo"]["userCallingLineIdBlockingOverride"] == "true" ? "Block" : "Unblock";
	            $changeLog[$a]["new"] = $changeClidTemp;
	            $a++;
	        }else{
	            $message .= "<li style='color:red'> " . $asingedCLIB["Error"] . "</li>";
	        }
	    }
	}
	
	
	$message .= "</ul>";
	echo $message;
	
	//Code added @ 22 Jan 2019 Regarding EX-1043
        if (isset($changeLog))
        {
            if(count($changeSCAAssignLog) > 0 && count($changeSCAUnassignLog) > 0){
			$changeScaLogArray = array_merge($changeSCAAssignLog, $changeSCAUnassignLog);
            }elseif(count($changeSCAAssignLog) > 0 && count($changeSCAUnassignLog) == 0){
                    $changeScaLogArray = $changeSCAAssignLog;
            }elseif(count($changeSCAAssignLog) == 0 && count($changeSCAUnassignLog) > 0){
                    $changeScaLogArray = $changeSCAUnassignLog;
            }else{
                    $changeScaLogArray = array();
            }
            
            $changeLog      = array_merge($changeLog, $changeScaLogArray);            
            $cLUObj         = new ChangeLogUtility($userId, $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
            $module         = "User Modify";
            $tableName      = "userModChanges";
            $entityName     = $_SESSION["userInfo"]["lastName"].", ".$_SESSION["userInfo"]["firstName"];
            $changeLogArr   = array(); 
            
            $cLUObj->module       = $module;
            $cLUObj->entityName   = $entityName;
            $cLUObj->modTableName = $tableName;
            $lastId = $cLUObj->changeLog();
            
            //echo "<pre>Change Log - ";
            
            foreach($changeLog as $changeLogRow){
                
                if ($changeLogRow['field'] == "monitoredUsers")
		{                        
                        $fieldName = $_SESSION["userModNames"][$changeLogRow["field"]];
                        $changeLogArr[$fieldName]['oldValue'] = $lastId;
                        $changeLogArr[$fieldName]['newValue'] = $lastId;

                        $exp = explode("&", $changeLogRow["old"]);
                        foreach ($exp as $k => $v)
                        {
                                if ($v !== "")
                                {
                                        $insert = "INSERT into blfs (blfId, groupId, userName, state, blfUser)";
                                        $insert .= " VALUES ('" . $lastId . "', '" . $_SESSION["groupId"] . "', '" . $userId . "', 'oldValue', '" . $v . "')";
                                        $fth = $db->query($insert);
                                }
                        }
                        $exp = explode("&", $changeLogRow["new"]);
                        foreach ($exp as $k => $v)
                        {
                                if ($v !== "")
                                {
                                        $insert  = "INSERT into blfs (blfId, groupId, userName, state, blfUser)";
                                        $insert .= " VALUES ('" . $lastId . "', '" . $_SESSION["groupId"] . "', '" . $userId . "', 'newValue', '" . $v . "')";
                                        $fth = $db->query($insert);
                                }
                        }
                }
                elseif($changeLogRow['field']=="scaServicePackAssign")
                {
                        $changeLogArr['SCA : Assign Service Pack']['oldValue'] = $changeLogRow['old'];
                        $changeLogArr['SCA : Assign Service Pack']['newValue'] = $changeLogRow['new'];
                }
                elseif($changeLogRow['field']=="unAssignScaServicePack")
                {
                        $changeLogArr['SCA : Unassign Service Pack']['oldValue'] = $changeLogRow['old'];
                        $changeLogArr['SCA : Unassign Service Pack']['newValue'] = $changeLogRow['new'];
                }
                elseif($changeLogRow['field']=="ccd")
                {
                        $changeLogArr['Custom Contact Directory']['oldValue'] = $changeLogRow['old'];
                        $changeLogArr['Custom Contact Directory']['newValue'] = $changeLogRow['new'];
                }
                else
                {
                        $changeLogArr[$changeLogRow['field']]['oldValue'] = $changeLogRow['old'];
                        $changeLogArr[$changeLogRow['field']]['newValue'] = $changeLogRow['new'];
                }
            }
            $cLUObj->changesArr = $changeLogArr;
            $changeResponse = $cLUObj->tableModChanges(); 
        }
        //End code
        /*
	if (isset($changeLog)) {
	    $date = date("Y-m-d H:i:s");
	    $query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
	    $query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', 'User Modify', '" . $_SESSION["groupId"] . "', '" . $_SESSION["userInfo"]["lastName"] . ", " . $_SESSION["userInfo"]["firstName"] . "')";
	    $sth = $expProvDB->expressProvLogsDb->query($query);
	}

        
	$lastId = $expProvDB->expressProvLogsDb->lastInsertId();
	$query = "INSERT into userModChanges (id, serviceId, entityName, field, oldValue, newValue) ";
	if (isset($changeLog))
	{ 
		//array_values($changeLog);
		
		if(count($changeSCAAssignLog) > 0 && count($changeSCAUnassignLog) > 0){
			$changeScaLogArray = array_merge($changeSCAAssignLog, $changeSCAUnassignLog);
		}elseif(count($changeSCAAssignLog) > 0 && count($changeSCAUnassignLog) == 0){
			$changeScaLogArray = $changeSCAAssignLog;
		}elseif(count($changeSCAAssignLog) == 0 && count($changeSCAUnassignLog) > 0){
			$changeScaLogArray = $changeSCAUnassignLog;
		}else{
			$changeScaLogArray = array();
		}
		//$changeScaLogArray = array_merge($changeSCAAssignLog, $changeSCAUnassignLog);
		$changeLog = array_merge($changeLog, $changeScaLogArray);		
		for ($a = 0; $a < count($changeLog); $a++)
		{
			$values = "VALUES ('" . $lastId . "', '" . $userId . "', '" . $_SESSION["userInfo"]["lastName"] . ", " . $_SESSION["userInfo"]["firstName"] . "', ";
			if ($changeLog[$a]["field"] == "monitoredUsers")
			{
				$values .= "'" . $_SESSION["userModNames"][$changeLog[$a]["field"]] . "', '" . $lastId . "', '" . $lastId . "')";
				$Query = $query . $values;
				$pth = $expProvDB->expressProvLogsDb->query($Query);
				
				$exp = explode("&", $changeLog[$a]["old"]);
				foreach ($exp as $k => $v)
				{
					if ($v !== "")
					{
						$insert = "INSERT into blfs (blfId, groupId, userName, state, blfUser)";
						$insert .= " VALUES ('" . $lastId . "', '" . $_SESSION["groupId"] . "', '" . $userId . "', 'oldValue', '" . $v . "')";
						$fth = $db->query($insert);
					}
				}
				$exp = explode("&", $changeLog[$a]["new"]);
				foreach ($exp as $k => $v)
				{
					if ($v !== "")
					{
						$insert = "INSERT into blfs (blfId, groupId, userName, state, blfUser)";
						$insert .= " VALUES ('" . $lastId . "', '" . $_SESSION["groupId"] . "', '" . $userId . "', 'newValue', '" . $v . "')";
						$fth = $db->query($insert);
					}
				}
			}else if($changeLog[$a]["field"] == "scaServicePackAssign"){
				$values .= "'SCA : Assign Service Pack', '" . addslashes($changeLog[$a]["old"]) . "', '" . addslashes($changeLog[$a]["new"]) . "')";
				$Query = $query . $values;
				$tth = $expProvDB->expressProvLogsDb->query($Query);
			}else if($changeLog[$a]["field"] == "unAssignScaServicePack"){
				$values .= "'SCA : Unassign Service Pack', '" . addslashes($changeLog[$a]["old"]) . "', '" . addslashes($changeLog[$a]["new"]) . "')";
				$Query = $query . $values;
				$tth = $expProvDB->expressProvLogsDb->query($Query);
				//echo "<br/>";
			}else if($changeLog[$a]["field"] == "ccd"){
				$values .= "'Custom Contact Directory', '" . addslashes($changeLog[$a]["old"]) . "', '" . addslashes($changeLog[$a]["new"]) . "')";
				$Query = $query . $values;
				$tth = $expProvDB->expressProvLogsDb->query($Query);
			}
			else
			{ 
				$values .= "'" . $_SESSION["userModNames"][$changeLog[$a]["field"]] . "', '" . addslashes($changeLog[$a]["old"]) . "', '" . addslashes($changeLog[$a]["new"]) . "')";
				$Query = $query . $values;
				$tth = $expProvDB->expressProvLogsDb->query($Query);
			}
		}
	}
	*/
	
        function userServicesOperations($POST, $userId, $deviceName) {
            $message = "";
            $changeLog = "";
            $isServiceModified = false;
            //$assignedUserServicesVal = explode(";", rtrim($POST['assignedUserServicesVal'], ";"));
            $assignedUserServicesVal = ($POST['assignedUserServicesVal'] != "") ? explode(";", rtrim($POST['assignedUserServicesVal'], ";")) : array();
            $serviceObj = new ServiceAndServicePackUtil($userId, $deviceName);
            $servicesToBeOperated = $serviceObj->userServiceOperation($POST);
            //if( empty($servicesToBeOperated['Error']) ) {
            if( !empty($servicesToBeOperated['Success']) ) {
                $changeLog = $serviceObj->builChangeLogDataForServices($assignedUserServicesVal);
                foreach($servicesToBeOperated['Success'] as $successMsg) {
                    $isServiceModified = true;
                    $message .= "<li>" . $successMsg . "</li>"; }
            } else {
                foreach($servicesToBeOperated['Error'] as $errMsg) {
                    $message .= "<li style='color:red'> " . $errMsg . "</li>";
                }
            }
        
            return array("isServiceModified" => $isServiceModified, "changeLog" => $changeLog, "message" => $message);
    }
    
	function addCustomTagToUsersDevice($tagName, $tagValue) {
	    $customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
	    $resp = $customTags->addCustomTag($tagName, $tagValue);
	    if ($resp == "") {
	        return true;
	    } else {
	        return $resp;
	    }
	}
	
	function deleteCustomTag($tagName) {
	    $customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
	    $resp = $customTags->deleteCustomTag($tagName);
	    if ($resp == "") {
	        return true;
	    } else {
	        return $resp;
	    }
	}
	
	function modifyCustomTag($tagName, $tagValue) {
	    $customTags = new UserCustomTagManager($_SESSION["userInfo"]["deviceName"],$_SESSION["userInfo"]["deviceType"]);
	    $resp = $customTags->modifyCustomTag($tagName, $tagValue);
	    if ($resp == "") {
	        return true;
	    } else {
	        return $resp;
	    }
	}
	
	function processCustomTagArr($usersProvisionedCustomTags ,$usersAvailableCustomTags){
	    $customTag = array();
	    foreach ($usersAvailableCustomTags as $key => $Value){
	        if( ! isset($Value['groupLevel']) ) {
	            if(! isset($usersProvisionedCustomTags[$key])) {
	                $customTag["add"][] =    $usersAvailableCustomTags[$key];
	            } 
	            else if( isset($usersProvisionedCustomTags[$key]) && isset($usersProvisionedCustomTags[$key]['groupLevel']) && ! isset($usersAvailableCustomTags[$key]['groupLevel']) ) {
	                $customTag["add"][] =    $usersAvailableCustomTags[$key];
	            }
	            else if(isset($usersProvisionedCustomTags[$key]) && $usersAvailableCustomTags[$key]["customTagValue"] != $usersProvisionedCustomTags[$key]["customTagValue"]) {
	                $Value['oldValue'] = $usersProvisionedCustomTags[$key]['customTagValue'];
	                $customTag["modify"][] = $Value;
	            }
	        }
	    }
	    
	    foreach ($usersProvisionedCustomTags as $key => $Value1){
	        if( ! isset($Value1['groupLevel']) && ! isset($usersAvailableCustomTags[$key]))
	        {
	            $customTag["delete"][] = $Value1;
	        }
	        
	    }
	    return  $customTag;
	}
	
	function rebuiltAndResetDevicePhone($rebuildPhoneFiles, $resetPhone, $deviceName) {
	    $rebuildOption ="";
	    if ($rebuildPhoneFiles) {
	        $rebuildOption = "rebuildOnly";
	    }
	    if ($resetPhone) {
	        $rebuildOption = $rebuildOption == "" ? "resetOnly" : "rebuildAndReset";
	    }
	        rebuildAndResetDeviceConfig($deviceName, $rebuildOption);
	}
	
	function sCARebuiltAndResetDevicePhone($key, $changes) {
	    $message = "";
	    $scaresetPhone = isset($changes['scaRebuiltReset']["resetPhone_sca"]) ? true : false;
	    $scaRebuildPhoneFiles = isset($changes['scaRebuiltReset']["rebuildPhoneFiles_sca"]) ? true : false;
	    if( $scaresetPhone || $scaRebuildPhoneFiles){	            	            
	            $response = rebuiltAndResetDevicePhone($scaRebuildPhoneFiles, $scaresetPhone, $_SESSION["userInfo"]["deviceName"]);
	            //$errorResult = readError($response);
	           // if ($errorResult !== "Error")
	           // {
	                if($scaRebuildPhoneFiles){
	                    $message .= "<li>SCA Rebuild the Phone has been changed to true.</li>";
	                }
	                
	                if($scaresetPhone){
	                    $message .= "<li>SCA Reset Phone Files has been changed to true.</li>";
	                }
	                
	            //}
	        }
	        
	    return $message;
	}
	
	function deviceLinePortOperations($reorderLinePort) {
	    $userOpObj = new UserOperations();
	    $response = "";
	    $linePortReorderRes = $userOpObj->groupCPEConfigReorderDeviceLinePortsRequest($reorderLinePort, $_SESSION["userInfo"]['deviceName'], $_SESSION["sp"], $_SESSION["groupId"]);
	    if( empty($linePortReorderRes["Error"]) ) {
	        $response = "success";
	    } else {
	        $response = $linePortReorderRes["Error"];
	    }
	    
	    return array("status" => $response);
	}
?>
<div id="result"></div>
