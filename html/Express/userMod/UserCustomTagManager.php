<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
require_once("/var/www/lib/broadsoft/adminPortal/deviceConfigCustomTagManagement.php");
require_once ("/var/www/lib/broadsoft/adminPortal/customTagManagement.php");
require_once("/var/www/lib/broadsoft/adminPortal/util/sasUtil.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
/**
 * Created by Karl.
 * Date: 11/14/2016
 */
class UserCustomTagManager
{
    private $deviceName;
    public $numberOfTags = 0;
    private $customTags;
    private $tagSpec;
    private $deviceType;
    private $groupCustomTags = array();
    
    private static function actionAdd() { return "actionAdd"; }
    private static function actionDel() { return "actionDel"; }
    private static function actionMod() { return "actionMod"; }

  public function __construct($deviceName, $deviceType) {
    	$this->deviceName = $deviceName;
    	$this->deviceType= $deviceType;
    	$customTagManager = new CustomTagManagement();
    	$this->tagSpec = $customTagManager->getTagList($deviceType);
    	$this->getUserCustomTags();
    	$this->getGroupCustomTag();
    	$this->formatCustomTagsTable();
    }



   
    // Code for redesign;
    public function getGroupCustomTag() {
        $dCCTM = new DeviceConfigCustomTagManagement();
        $groupCustomTagList = $dCCTM->getCustomTagsListsByDeviceType($this->deviceType);
        if(empty($groupCustomTagList["Error"])) {
            $this->groupCustomTags = $groupCustomTagList["Success"];
        }   
    }
    
    public function checkCustomTagType($tagName, $tagValue) {
        $tagType = "";
        if($this->isTagExistInTagSpecTable($tagName, $tagValue)){
            $tagType = "In-Spec";
        }
        /*else if($this->isTagExistInGroupCustomTagList($tagName, $tagValue)) {
            $tagType = "Group";
        } 
        */
        else {
            $tagType = "Custom";
        }
        return $tagType;
    }
        
    public function isTagExistInTagSpecTable($tagName, $tagValue) { 
        global $db;
        //Code added @ 16 July 2019
        $whereCndtn = "";
        if($_SESSION['cluster_support']){
            $selectedCluster = $_SESSION['selectedCluster'];
            $whereCndtn = " AND clusterName ='$selectedCluster' ";
        }
        //End code
        $lookupDeviceType = $this->deviceType;
        $tagArray = array();
        $lookupQuery = "select customTagSpec from systemDevices where deviceType = '".$this->deviceType."' $whereCndtn ";
        $result1 = $db->query($lookupQuery);
        while($row1 = $result1->fetch(PDO::FETCH_ASSOC)) {
            $lookupDeviceType= isset($row1["customTagSpec"]) ? $row1["customTagSpec"] : $this->deviceType;
        }
        
//         print_r($lookupDeviceType); echo"HHHH"; exit;
        $queryTag = "select * from customTagSpec where tagName = '".$tagName."' and deviceType ='".$lookupDeviceType."' " ;
        $result = $db->query($queryTag);
        if($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $tagArray = $row;
        }
        
        if(! empty($tagArray) ) {
            if($tagArray["tagName"] == $tagName) {
                if($tagArray["tagType"] == "0") {
                    return true;
                } else if($tagArray["tagType"] == "1") {
                    if($tagArray["option1Value"] == $tagValue ||
                        $tagArray["option2Value"] == $tagValue ||
                        $tagArray["option3Value"] == $tagValue ||
                        $tagArray["option4Value"] == $tagValue ||
                        $tagArray["option5Value"] == $tagValue ||
                        $tagArray["option6Value"] == $tagValue ||
                        $tagArray["option7Value"] == $tagValue ||
                        $tagArray["option8Value"] == $tagValue ||
                        $tagArray["option9Value"] == $tagValue ||
                        $tagArray["option10Value"] == $tagValue ||
                        $tagArray["option11Value"] == $tagValue ||
                        $tagArray["option12Value"] == $tagValue ||
                        $tagArray["option13Value"] == $tagValue ||
                        $tagArray["option14Value"] == $tagValue
                        ) {
                            return true;
                        }
                        
                } else if($tagArray["tagType"] == "2") {
                    if ($tagArray["tagValueRange"] != "") {
                        $tagValueRange = explode("-", $tagArray["tagValueRange"]);
                        if($tagValue >= $tagValueRange[0] && $tagValue <= $tagValueRange[1] ) {
                            return true;
                        }
                    }
                    
                }
            }
        }
        return false;
    }
    
    public function isTagExistInGroupCustomTagList($tagName, $tagValue) {
            foreach($this->groupCustomTags as $tagKey1 => $tagValue1) {
                if($tagValue1["tagName"] == $tagName && $tagValue1["tagValue"] == $tagValue) {
                    return true;
                }
                
            }
        return false;
    }
    // Code for redesign;
    
    /**
     * add custom tag to the device and to the list of custom tags
     * @param $tagName
     * @param $tagValue
     * @return string
     */
    
    public function addCustomTag($tagName, $tagValue) {
        global $sessionid, $client;

        if (($result = $this->validate(self::actionAdd(), $tagName, $tagValue)) != "") {
            return $result;
        }

        $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagAddRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $this->deviceName . "</deviceName>";
        $xmlinput .= "<tagName>" . $tagName . "</tagName>";
        $xmlinput .= "<tagValue>" . $tagValue . "</tagValue>";

        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (readError($xml) != "") {
             return "OCI Error:GroupAccessDeviceCustomTagAddRequest";
        }

        $this->numberOfTags++;

        // refresh custom tags
        $this->getUserCustomTags();
        return "";
    }


    /**
     * modify custom tag in the device and in the list of custom tags
     * @param $tagName
     * @param $tagValue
     * @return string
     */
    public function modifyCustomTag($tagName, $tagValue) {
        global $sessionid, $client;

        if (($result = $this->validate(self::actionMod(), $tagName, $tagValue)) != "") {
            return $result;
        }

        $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagModifyRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $this->deviceName . "</deviceName>";
        $xmlinput .= "<tagName>" . $tagName . "</tagName>";
        $xmlinput .= "<tagValue>" . $tagValue . "</tagValue>";

        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (readError($xml) != "") {
            return "OCI Error:GroupAccessDeviceCustomTagModifyRequest";
        }

        // refresh custom tags
        $this->getUserCustomTags();
        return "";
    }


    /**
     * delete custom tag from the device and from the list of custom tags
     * @param $tagName
     * @return string
     */
    public function deleteCustomTag($tagName) {
        global $sessionid, $client;

        $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagDeleteListRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $this->deviceName . "</deviceName>";
        $xmlinput .= "<tagName>" . $tagName . "</tagName>";

        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (readError($xml) != "") {
            return "OCI Error:GroupAccessDeviceCustomTagDeleteListRequest";
        }

        $this->numberOfTags--;

        // refresh custom tags
        $this->getUserCustomTags();
        return "";
    }


    public function getCustomTagValue($tagName) {
    	if(isset($this->customTags)) {
		    foreach ($this->customTags as $name => $value) {
			    if ($name == $tagName) {
				    return $value;
			    }
		    }
	    }

        return "";
    }


    public function formatCustomTagsTable() {
        
        $sortedCustomTag = array();
        $longDescription = array();
        $arrayForTable = ($this->customTags);
        $sasUtil = new SasUtil();
        if(count($arrayForTable) > 0){
            foreach ($arrayForTable as $key => $value){
                $isSasUser = $sasUtil->isSASUser($key);
                if($isSasUser){
                    unset($arrayForTable[$key]);
                }
            }
            
        }
        foreach ($arrayForTable as $name => $value) {
            $shortDescription = $this->getShortDescription($name);
            $longDescription = $this->getLongDescription($name);
            
            //             $shortDescription = $this->getShortDescription($name);
            //             $longDescription = $this->getLongDescription($name);
            
            $sortedCustomTag[$name]['customTagName'] = $name;
            $sortedCustomTag[$name]['customTagValue'] = $value;
            $sortedCustomTag[$name]['customTagShortDesc'] = $shortDescription != null ? $shortDescription : $name;
            $sortedCustomTag[$name]['customTagLongDesc'] = $longDescription != null ? $longDescription : $name;
            
        }
        //         natsort($shortDescriptionArr);
        $sortedCustomTag = $this->pushGroupLevelCustomTag($sortedCustomTag);
        if(! isset($_SESSION['usersProvisionedCustomTags'])) {
            $_SESSION['usersProvisionedCustomTags'] = $sortedCustomTag;
            $_SESSION['assignCustomTagToUserForTable'] = $sortedCustomTag;
        }
        
        return $this->createUserCustomTagTable($sortedCustomTag);
    }
    
    
    public function createUserCustomTagTable($sortedCustomTag) {
        
        // format header
        $htmlTable  = "<table border=\"1\" class=\"dataTable leftDesc tablesorter scroll tagTableHeight\" id=\"userCustomTagsTable\" style=\"width: 100%; border: thin solid #ddd;\">";
        $htmlTable .= "<thead><tr>";
        $htmlTable .= "<th style=''><strong>Tag Name</strong></th>";
        $htmlTable .= "<th style=''><strong>Tag Type</strong></th>";
        $htmlTable .= "<th style=''><strong>Tag Value</strong></th>";
        $htmlTable .= "</tr></thead>";
        
        // format body
        $htmlTable .= "<tbody>";
//         if ($this->numberOfTags == 0) {
        if (count($sortedCustomTag) == 0) {
            $htmlTable .= "<tr><td colspan='3'>--No Custom Tags--</td></tr>";
        }
        else {
            foreach($sortedCustomTag as $name => $value) {
                if(isset($value['groupLevel']) && $value['groupLevel'] == "Yes") {
                    $customTagType = "Group";
                } else {
                    $customTagType = $this->checkCustomTagType($value['customTagName'], $value['customTagValue']);
                }
                $tagDesc = ($customTagType == "In-Spec") ? $value['customTagShortDesc'] . " ( ". ltrim(rtrim($value['customTagName'], "%"), "%") . " )" : $value['customTagName'];
                $tagDesc = ltrim(rtrim($tagDesc, "%"), "%");
                
                $pointerEvents = $customTagType == "Group" ? "pointer-events: none;" : "";
                $valueId = "userCustomTagValue".$value['customTagName'];
                $htmlTable .= "<tr data-tagType='". $customTagType ."' style='". $pointerEvents ."' title='".$value['customTagLongDesc']."' id=\"" . $value['customTagName']. "\" onmouseover=\"highlightRow(this, true)\" onmouseout=\"highlightRow(this, false)\" onclick=\"evalRow(this)\">";
                $htmlTable .= "<td style='' >" . $tagDesc . "</td>";
                $htmlTable .= "<td style='' >" . $customTagType. "</td>";
                $htmlTable .= "<td id=\"" . $valueId . "\" style='' value=".$value['customTagValue']." >" . $value['customTagValue']. "</td>";
                $htmlTable .= "</tr>";
            }
        }
        
        $htmlTable .= "</tbody>";
        $htmlTable .= "</table>";
        
        return $htmlTable;
    }
    
    public function pushGroupLevelCustomTag($sortedCustomTag) {
        foreach($this->groupCustomTags as $tagKey => $tagVal) {
            if( ! isset($sortedCustomTag[$tagVal['tagName']]) ) {
                $sortedCustomTag[$tagVal['tagName']]['customTagName'] = $tagVal['tagName'];
                $sortedCustomTag[$tagVal['tagName']]['customTagValue'] = $tagVal['tagValue'];
                $sortedCustomTag[$tagVal['tagName']]['customTagShortDesc'] = $tagVal['tagName'];
                $sortedCustomTag[$tagVal['tagName']]['customTagLongDesc'] = $tagVal['tagName'];
                $sortedCustomTag[$tagVal['tagName']]['groupLevel'] = "Yes";
            }
            
        }
//         print_r($sortedCustomTag); echo"nnnnnn"; exit;
        return $sortedCustomTag;
    }
    
    function getShortDescription($tagName) {
    	$shortd = $tagName == "%Express_Custom_Profile_Name%" ? "" : null;
		if(!empty($tagName)){

			if(count($this->tagSpec)>0){
				foreach ($this->tagSpec as $key => $value){
					if($value['tagName'] == $tagName){
						return $value['shortDescription'];
					}
				}
			}
		}
		return $shortd;
    }
    function getLongDescription($tagName) {
    	$longd = $tagName == "%Express_Custom_Profile_Name%" ? "" : null;
    	if(!empty($tagName)){

    		if(count($this->tagSpec)>0){
    			foreach ($this->tagSpec as $key => $value){
    				if($value['tagName'] == $tagName){
    					return $value['description'];
    				}
    			}
    		}
    	}
    	return $longd;
    }

	public function getUserCustomTagList(){
		$this->getUserCustomTags();
		return $this->customTags;
	}
    /**
     * Read custom tags from a device associated with a user.
     * The read tags will be stored in Session.
     */
    private function getUserCustomTags() {
        global $sessionid, $client;

        // get custom tags from back-end
        $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagGetListRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $this->deviceName . "</deviceName>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (isset($xml->command->deviceCustomTagsTable->row)) {
            if (isset($this->customTags)) {
                unset($this->customTags);
            }

            foreach ($xml->command->deviceCustomTagsTable->row as $key => $value) {
                $this->customTags[strval($value->col[0])] = strval($value->col[1]);
                if( strval($value->col[0]) != "%Express_Custom_Profile_Name%" ) {
                    $this->numberOfTags++;
                }
            }
            
        }
    }


    private function hasTag($tagName) {
        foreach ($this->customTags as $name => $value) {
            if ($name == $tagName) {
                return true;
            }
        }

        return false;
    }


    private function validate($operation, $tagName, $tagValue = "") {
        //TODO: More validation: the tag must not start with %BW and have no other % in between
        if ($tagName[0] != "%" || substr($tagName, -1) != "%" || strlen($tagName) <= 2) {
            return "Invalid tag name: expected: %name% .";
        }

        if ($tagValue == "" && $operation != self::actionDel()) {
            return "Tag value cannot be an empty string";
        }

        switch ($operation) {
            case self::actionAdd():
                if ($this->hasTag($tagName)) {
                    return "Tag '" . $tagName . "' already exists.";
                }
            break;
        }

        return "";
    }
    
    public function statusMessageForCustomTag($resultCustomTagArr, $customTagsRebuiltResponse) {
        $message = "";
        if(isset($resultCustomTagArr['add']) && !empty($resultCustomTagArr['add'])) {
            $addedTags = "";
            $notAddedTags = "";
            foreach($resultCustomTagArr['add'] as $key1 => $addCustomTag){
                if( isset($addCustomTag['status']) && $addCustomTag['status'] == "success") {
                    $addedTags .= $addCustomTag['customTagName'] . "-" . $addCustomTag['customTagValue'] . ",";
                } else {
                    $notAddedTags .= $addCustomTag['customTagName'] . "-" . $addCustomTag['errorMsg'] . "<br> ,";
                }
            }
            if( $addedTags != "" ) {
                $message .= "<li>Added device profile custom tags : " . rtrim($addedTags, ",") . ".</li>";
            }
            if( $notAddedTags != "" ) {
                $message .= "<li style='color:red'>Eror in Add device profile custom tags : " . rtrim($notAddedTags, ",") . ".</li>";
            }
        }
        
        if(isset($resultCustomTagArr['modify']) && !empty($resultCustomTagArr['modify'])){
            $modifiedTags = "";
            $notModifiedTags = "";
            foreach($resultCustomTagArr['modify'] as $key2 => $modifyCustomTag){
                if( isset($modifyCustomTag['status']) && $modifyCustomTag['status'] == "success") {
                    $modifiedTags .= $modifyCustomTag['customTagName'] . "-" . $modifyCustomTag['customTagValue'] . ",";
                } else {
                    $notModifiedTags .= $modifyCustomTag['customTagName'] . "-" . $modifyCustomTag['errorMsg'] . "<br> ,";
                }
            }
            
            if( $modifiedTags != "" ) {
                $message .= "<li>Modified device profile custom tags : " . rtrim($modifiedTags, ",") . ".</li>";
            }
            if( $notModifiedTags != "" ) {
                $message .= "<li style='color:red'>Eror in Modify device profile custom tags : " . rtrim($notModifiedTags, ",") . ".</li>";
            }
        }
        
        if(isset($resultCustomTagArr['delete']) && !empty($resultCustomTagArr['delete'])){
            $deletedTags = "";
            $notDeletedTags = "";
            foreach($resultCustomTagArr['delete'] as $key3 => $delCustomTag) {
                if( isset($delCustomTag['status']) && $delCustomTag['status'] == "success") {
                    $deletedTags .= $delCustomTag['customTagName'] . "-" . $delCustomTag['customTagValue'] . ",";
                } else {
                    $notDeletedTags .= $delCustomTag['customTagName'] . "-" . $delCustomTag['errorMsg'] . "<br> ,";
                }
            }
            
            if( $deletedTags != "" ) {
                $message .= "<li>Deleted device profile custom tags : " . rtrim($deletedTags, ",") . ".</li>";
            }
            if( $notDeletedTags != "" ) {
                $message .= "<li style='color:red'>Eror in delete device profile custom tags : " . rtrim($notDeletedTags, ",") . ".</li>";
            }
        }
        
        return $message;
    }
    
    public function customTagOperation($resultCustomTagArr) {
        $customTagStatusArr = array();
        if(isset($resultCustomTagArr['add']) && !empty($resultCustomTagArr['add'])){
            foreach($resultCustomTagArr['add'] as $key1 => $addCustomTag){
                $response = "";
                $response = addCustomTagToUsersDevice($addCustomTag["customTagName"], $addCustomTag["customTagValue"]);
                if($response) {
                    $resultCustomTagArr['add'][$key1]['status'] = "success";
                } else {
                    $resultCustomTagArr['add'][$key1]['status'] = "failure";
                    $resultCustomTagArr['add'][$key1]['errorMsg'] = $response;
                }
            }
        }
        
        if(isset($resultCustomTagArr['modify']) && !empty($resultCustomTagArr['modify'])){
            foreach($resultCustomTagArr['modify'] as $key2 => $modifyCustomTag){
                $response = "";
                $response = modifyCustomTag($modifyCustomTag["customTagName"], $modifyCustomTag["customTagValue"]);
                if($response) {
                    $resultCustomTagArr['modify'][$key2]['status'] = "success";
                } else {
                    $resultCustomTagArr['modify'][$key2]['status'] = "failure";
                    $resultCustomTagArr['modify'][$key2]['errorMsg'] = $response;
                }
            }
        }
        
        if(isset($resultCustomTagArr['delete']) && !empty($resultCustomTagArr['delete'])){
            foreach($resultCustomTagArr['delete'] as $key3 => $delCustomTag){
                $response = "";
                $response = deleteCustomTag($delCustomTag["customTagName"]);
                if($response) {
                    $resultCustomTagArr['delete'][$key3]['status'] = "success";
                } else {
                    $resultCustomTagArr['delete'][$key3]['status'] = "failure";
                    $resultCustomTagArr['delete'][$key3]['errorMsg'] = $response;
                }
            }
        }
        return $resultCustomTagArr;
    }
    
    public function resetAndRebuiltDevice($post, $deviceName) {
        require_once("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig.php");
        $rebuildOption = "";
        if (isset($post["rebuildPhoneFiles_CustomTag"])) {
            $rebuildOption = "rebuildOnly";
        }
        if (isset($post["resetPhone_CustomTag"])) {
            $rebuildOption = $rebuildOption == "" ? "resetOnly" : "rebuildAndReset";
        }
        rebuildAndResetDeviceConfig($deviceName, $rebuildOption);
        return true;
    }
    
    public function userCustomTagChangeLogOperations ($resultCustomTagArr, $userId) {
        if(isset($resultCustomTagArr['add']) && !empty($resultCustomTagArr['add'])) {
            foreach($resultCustomTagArr['add'] as $addCustomTag){
                if($addCustomTag['status'] == "success"){
                    $changeLogObj = new ChangeLogUtility($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
                    $changeLogArr = array();
                    $changeLogArr  = array(
                        'serviceId'   => $userId,
                        'deviceType'  => $this->deviceType,
                        'deviceName'  => $this->deviceName,
                        'tagName'     => $addCustomTag['customTagName'],
                        'tagValue'    => $addCustomTag['customTagValue']
                    );
                    $module         = "User Modify (Add User's device profile Custom Tags)";
                    $tableName      = "userCustomTagsAddChanges";
                    $entityName     = $userId;
                    $changeResponse = $changeLogObj->changeLogAddUtility($module, $entityName, $tableName, $changeLogArr);
                }
            }
        }
        
        /*start modify custom tag validation show */
        if(isset($resultCustomTagArr['modify']) && !empty($resultCustomTagArr['modify'])){
            foreach($resultCustomTagArr['modify'] as $modifyCustomTag){
                if($modifyCustomTag['status'] == "success"){
                    $changeLogObj = new ChangeLogUtility($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
                    $changeLogObj->deviceName  = $_SESSION["userInfo"]["deviceName"];
                    $changeLogObj->module       = "User Modify (Modify User's device profile Custom Tags)";
                    $changeLogObj->entityName   = $modifyCustomTag['customTagName'];
                    $changeLogObj->changeLog();
                    $changeLogObj->modTableName = "userCustomTagsModChanges";
                    $changeLogObj->serviceId    = $userId;
                    $changeLogObj->deviceType = $this->deviceType;
                    $changeLogArr  = $changeLogObj->createChangesArray($modifyCustomTag['customTagName'], $modifyCustomTag['oldValue'], $modifyCustomTag['customTagValue']);
                    $changeLogObj->tableUserCustomTagModChanges();
                }
            }
        }
        
        if(isset($resultCustomTagArr['delete']) && !empty($resultCustomTagArr['delete'])) {
            foreach($resultCustomTagArr['delete'] as $delCustomTag){
                if($delCustomTag['status'] == "success"){
                    $changeLogObj = new ChangeLogUtility($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
                    $changeLogObj->deviceName  = $_SESSION["userInfo"]["deviceName"];
                    $changeLogObj->module       = "User Modify (Delete User's device profile Custom Tags)";
                    $changeLogObj->entityName   = $delCustomTag['customTagName'];
                    $changeLogObj->changeLog();
                    $changeLogObj->modTableName = "userCustomTagsModChanges";
                    $changeLogObj->serviceId    = $userId;
                    $changeLogObj->entityName   = $delCustomTag['customTagName'];
                    $changeLogObj->deviceType = $this->deviceType;
                    $changeLogArr  = $changeLogObj->createChangesArray($delCustomTag['customTagName'], "", $delCustomTag['customTagValue']);
                    $changeLogObj->tableUserCustomTagModChanges();
                }
            }
        }
    }
}
