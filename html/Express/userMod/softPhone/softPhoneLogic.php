<?php
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
require_once ("/var/www/html/Express/util/formDataUtil.php");
//require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ocpFeatures/OCPOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
$sipSoftPhoneLookup = new DBLookup ( $db, "systemDevices", "deviceType", "softPhoneType" );
$objSoft = new OCPOperations();

$userObject  =   new UserOperations();
$userId         =   $_SESSION ["userInfo"]["userId"];
$deviceObjSoftPhone = new DeviceOperations();
$softPhoneUserName1="";
$softPhonePassword1="";

$userObject = $userObject->getDeviceListByUserId($userId);
//print_r($userObject);
if(count($userObject["Success"]["deviceList"]) > 0){
    foreach ($userObject["Success"]["deviceList"] as $genKey => $genVal){
        
        if ($sipSoftPhoneLookup->get (trim($genVal["deviceType"])) == "counterPath" || $sipSoftPhoneLookup->get (trim($genVal["deviceType"])) == "generic") {
            
        }else{
            unset($userObject["Success"]["deviceList"][$genKey]);
        }
    }
}
//print_r(count($userObject["Success"]["deviceList"]));
if(count($userObject["Success"]["deviceList"]) == 0){
    unset($userObject["Success"]["deviceList"]);
}
array_values($userObject["Success"]["deviceList"]);
//print_r($userObject);exit;

if($_POST["funcAction"]=="showSofPhoneList"){
    //$userObject = $userObject->getDeviceListByUserId($userId);
    //print_r($userObject);
    if(count($userObject["Success"]["deviceList"]) > 0){
        foreach ($userObject["Success"]["deviceList"] as $nKey => $nVal){
            $softPhoneType = $sipSoftPhoneLookup->get (trim($nVal["deviceType"]));
            $userObject["Success"]["deviceList"][$nKey]["softPhoneType"] = $softPhoneType;
                //$vdmobj = new VdmOperations();
                //$tagListResp = $vdmobj->getDeviceCustomTags($_SESSION["sp"], $_SESSION["groupId"], $nVal["deviceName"]);
               
                $devUrl = $deviceObjSoftPhone->getDeviceFileUrl($nVal["deviceName"]);
                
                if(isset($nVal["deviceName"]) && $nVal["deviceName"] != ""){
                    $respVal = $objSoft->getDevicesByUsingUserId($userId);
                    
                    if(empty($respVal["Error"])){
                        foreach ($respVal["Success"] as $arrKey => $arrVal){
                            if($arrKey == $nVal["deviceName"]){
                                $softPhoneUserName1 = $arrVal["userName"];
                                $softPhonePassword1 = $arrVal["password"];
                            }
                            
                        }
                    }
                    
                }
                
                $filePath = $devUrl["Success"];
                //preg_match("/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/", $filePath, $matches);
                //$findIPFromStr = $matches[0];
                $privateIp = $xspPrivateIP;
                //$privateIp      = "10.100.10.207";
                //$changeURL = str_replace($findIPFromStr, $privateIp, $filePath);
                
                //$var1 = "https://69.169.36.217:80/dms/Counterpath_Bria_-_Desktop/configuration.txt";
                $findIPFromStr = parse_url($filePath, PHP_URL_HOST);
                //$privateIp      = "10.100.10.207";
                $changeURL = str_replace($findIPFromStr, $privateIp, $filePath);
                //print_r($changeURL);
                
                $valRes = curlgetFile($changeURL, $softPhoneUserName1, $softPhonePassword1);
                //print_r($valRes);
                if($valRes["status"] == "200"){
                    $info = parseConfigFile($valRes["result"]);
                    $userObject["Success"]["deviceList"][$nKey]["accountUserName"] = trim($info["account_username"]);
                    $userObject["Success"]["deviceList"][$nKey]["accountEmail"] = trim($info["account.userEmailAddress"]);
                    $userObject["Success"]["deviceList"][$nKey]["accountProfile"] = $info["account_profile"];
                    // $userObject["Success"]["deviceList"][$nKey]["domain"] = $tagListResp["Success"]["%ctrpathdomain%"][0];
                    //print_r($info);
                }
            
        }
        $_SESSION["counterPathDevices"] = $userObject["Success"]["deviceList"];
    }
    echo  json_encode($userObject["Success"]);
}

if($_POST["funcAction"]=="softphoneAdd"){
    $formDataUtil = new FormDataUtil();
    
    $postData = $formDataUtil->getFormDataNameValue($_POST["data"]);
    //print_r($postData);
    $newdata =  array (
        'action' => 'add',
        'softPhoneType' => $_POST['softPhoneType'],
        'label' => 'Group',
        'deviceName' => $_SESSION["softPhoneDeviceNameMod"],
        'deviceType' => $postData["softPhoneDeviceType"],
        'port' => "",
        'softPhoneDeviceTypeLinePort' => $postData["softPhoneDeviceTypeLinePort"],
        'softPhoneDeviceAccUserName' => $postData['softPhoneDeviceAccUserName'],
        'softPhoneDeviceAccUserPass' => $postData['softPhoneDeviceAccUserPass'],
        'accountUserName' => $postData['accountUserName'],
        'accountProfile' => $postData['accountProfile'],
        'softPhoneAccountEmailAddress' => $postData['softPhoneAccountEmailAddress'],
        'rebuildSoftPhoneFiles' => $postData['rebuildSoftPhoneFiles'],
        'resetSoftPhone' => $postData['rebuildSoftPhoneFiles'],
        'linePort' => $_SESSION["softPhoneDeviceLinePort"]
    );
    //print_r($newdata);
    if(isset($_SESSION["devicesListToAdd"]) && count($_SESSION["devicesListToAdd"]) > 0){
        $userObject = $_SESSION["devicesListToAdd"];
    }else{
        $userObject = $userObject;
        
    }
    
    $userObject["Success"]["deviceList"][]= $newdata;
    $_SESSION["devicesListToAdd"] = $userObject;
    $_SESSION["softPhoneListToAdd"][] = $newdata;
    //print_r($userObject);
    
    echo  json_encode($userObject["Success"]);
}

function curlgetFile($URL, $userName, $password){
    //$URL = 'http://10.100.10.207:80/dms/Counterpath_Bria_with_Broadworks/configuration.txt';
    //$userName = "9810090020";
    //$password = "Admin123";
    $arr = array();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$URL);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_USERPWD, "$userName:$password");
    $result=curl_exec ($ch);
    //print_r($result);
    $arr["result"]=$result;
    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    $arr["status"]=$status_code;
    ///print_r($status_code);
    curl_close ($ch);
    return $arr;
}

function parseConfigFile($result){
    $tmpResultArr = explode("\n", $result);
    unset($tmpResultArr[0]);
    unset($tmpResultArr[1]);
    unset($tmpResultArr[2]);
    $customizeResultArr = array();
    
    $count = 0;
    
    foreach($tmpResultArr as $ky => $vl){
        if($vl!=""){
            $tmpArr = explode("=", $vl);
            $customizeResultArr[$tmpArr[0]]  = str_replace('"', "", $tmpArr[1]);
        }
        $count = $count + 1;
    }
    return $customizeResultArr;
}
 
?>
    
   