<?php
    require_once("/var/www/html/Express/config.php");
    require_once ("/var/www/lib/broadsoft/login.php");
    checkLogin();
    require_once("/var/www/lib/broadsoft/adminPortal/counterPathStretto/CPSOperations.php");
    $cpsobj = new CPSOperations();
    $_SESSION ["softPhoneAddDialog"] = array (
        "softPhoneFalse"                  => "Soft Phone",
        "softPhoneDeviceType"             => "Soft Phone Device Type",
        "softPhoneDeviceTypeLinePort"     => "Soft Phone Line/Port Domain",
        "softPhoneDeviceAccUserName"      => "Soft Phone Device Access UserName",
        "softPhoneDeviceAccUserPass"      => "Soft Phone Device Access Password",
        "accountUserName"                 => "Account UserName",
        "accountProfile"                  => "Account Profile",
        "softPhoneAccountEmailAddress"    => "Account Email Address",
        "rebuildSoftPhoneFiles" => "Rebuild Soft Phone Files",
        "resetSoftPhone" => "Reset Soft Phone"
    );
    //echo "1234344"; print_r($_SESSION ["softPhoneAddDialog"]);
    require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");

    require_once ("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
    
    $objectSimRing  =   new UserOperations();
    
    $data = $errorTableHeader;	    
    $error = 0;
    if(isset($_POST["softPhoneNameType"]) && $_POST["softPhoneNameType"] == "generic"){
        unset($_POST["accountProfile"]);
    }
    //unset($_POST["softPhoneNameType"]);

$nonRequired = array("softPhoneFalse","softPhoneDeviceAccUserPass", "softPhoneDeviceAccUserName", "softPhoneAccountEmailAddress", "accountUserName");
foreach ($_POST as $key => $value)
    {
      
        if ($value !== ""){
            $bg = CHANGED;
        }else{
            $bg = UNCHANGED;
        }
        
        if($key == "accountUserName" && $_POST["softPhoneDeviceType"] != ""){
            if($_POST["softPhoneNameType"] == "counterPath" && $value ==""){
                $error = 1;
                $bg = INVALID;
                $value = "Soft Phone Account UserName can not be blank.";
            }
        }
        
        if ($key == "accountUserName" and $_POST["softPhoneNameType"] == "counterPath"){
            if($value != ""){
                $cpsReslt = $cpsobj->counterPathAccountGet(trim($value));
                if($cpsReslt == "true"){
                    $error = 1;
                    $bg = INVALID;
                    $value = $_SESSION["userAddNames"][$key] . " is already exist.";
                }
            }
        }
        
        if ($key == "softPhoneDeviceAccUserName" and $value != ""){	   
            $userId         =   $_SESSION ["userInfo"]["userId"];
            $objectSimRing = $objectSimRing->getDeviceListByUserId($userId);
           
            
            foreach($objectSimRing['Success']['deviceList'] as $checkDeviceAccessUserName){
                $softPhoneName = $_POST['softPhoneDeviceAccUserName'];
                if($softPhoneName == $checkDeviceAccessUserName['deviceName'])
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Soft Phone Device Access UserName is already exist.";
                }
                
            }
		}
		if($key == "softPhoneNameType"){
		    continue;
		}
		
		if($key == "softPhoneDeviceType"){
		    require_once ("/var/www/html/Express/util/deviceNameUtil.php");
		    require_once ("/var/www/html/Express/userMod/softPhone/linePortUtil.php");
		    require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
		    $_POST["deviceType"] = $_POST["softPhoneDeviceType"];
		    $_SESSION['softPhoneLinePortVal'] = $_POST["softPhoneDeviceTypeLinePort"];
		    $softPhoneDeviceNameMod = getDeviceNamefromCriteria($deviceNameDID1, $deviceNameDID2);
		    $_SESSION["softPhoneDeviceNameMod"] = replaceDevNameWithSubstituteSoftPhone($softPhoneDeviceNameMod);
		    $_SESSION["softPhoneDeviceLinePort"] = getLinePortName($softPhoneLinePortCriteria1, $softPhoneLinePortCriteria2);
		    
		    unset($_POST["deviceType"]);
		    $deviceOperationObj = new DeviceOperations;
		    
		    $checkDeviceNameExist = $deviceOperationObj->GroupAccessDeviceExistanceCheck($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["softPhoneDeviceNameMod"]);
		    //print_r($checkDeviceNameExist);
		    if(isset($checkDeviceNameExist["Success"][0]["deviceName"]) && !empty($checkDeviceNameExist["Success"][0]["deviceName"])){
		        $error = 1;
		        $bg = INVALID;
		        //$value = "Device already exists. Please delete the device ". $checkDeviceNameExist["Success"][0]["deviceName"];
		        $value = "Device already exists. Please delete the device ".$_SESSION["softPhoneDeviceNameMod"];
		    }
		}
        
		if (!in_array($key, $nonRequired) and $value == "")
		{
		    $error = 1;
		    $bg = INVALID;
		    $value = $_SESSION["DeviceCustomTagAddNames"][$key] . " is a required field.";
		}
		
		if ($key == "softPhoneDeviceAccUserPass")
		{
		    if ($value)
		    {
		        $value = str_repeat("*", strlen($value)); //don't display password
		    }
		}
		
		if ($bg != UNCHANGED)
		{
		    $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:" . $bg . ";\">" . $_SESSION ["softPhoneAddDialog"][$key] . "</td><td class=\"errorTableRows\">";
			if (is_array($value))
			{
			    foreach ($value as $k => $v)
				{
				   $data .= $v . "<br>";
				}
			}			
			else if ($value !== "")
			{
                                if($key=="softPhoneDeviceAccUserName"){
                                    if($value=="Soft Phone Device Access UserName is required." || $value=="Soft Phone Device Access UserName is already exist."){
                                        $data .= $value;
                                    }else{
                                        $data .= $value;
                                    }
                                    
                                }else{
                                    $data .= $value;
                                }
			}
			else
			{
				$data .= "None";
			}
			$data .= "</td></tr>";
		}
	}
	$data .= "</table>";
	echo $error . $data;
	function getLinePortDevTypeLookupSoftPhone() {
	    global $db;
	    $deviceSubList = array();
            //Code added @ 16 July 2019
            $whereCndtn = "";
            if($_SESSION['cluster_support']){
                $selectedCluster = $_SESSION['selectedCluster'];
                $whereCndtn = " WHERE clusterName ='$selectedCluster' ";
            }
            //End code
	    $qryLPLU = "select * from systemDevices $whereCndtn ";
	    $select_stmt = $db->prepare($qryLPLU);
	    $select_stmt->execute();
	    $i = 0;
	    while($rs = $select_stmt->fetch())
	    {
	        $deviceSubList[$i]['deviceType'] = $rs['deviceType'];
	        $deviceSubList[$i]['substitution'] = $rs['nameSubstitution'];
	        $i++;
	    }
	    return $deviceSubList;
	}
	
	function replaceDevNameWithSubstituteSoftPhone($deviceName) {
	    $newDeviceName = $deviceName;
	    $subList = getLinePortDevTypeLookupSoftPhone();
	    foreach($subList as $key => $value) {
	        if(strpos($deviceName, $value['deviceType']) !== false) {
	            $newDeviceName = str_replace($value['deviceType'], $value['substitution'], $deviceName);
	        }
	    }
	    return $newDeviceName;
	}
?>
