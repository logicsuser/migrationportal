<?php 
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/adminPortal/sca/SCAOperations.php");
require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
require_once("/var/www/lib/broadsoft/adminPortal/counterPathStretto/CPSOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ocpFeatures/OCPOperations.php");

require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
$expProvDB = new ExpressProvLogsDB();			

$objSoft = new OCPOperations();
$vdmobj = new VdmOperations();

$cpsobj = new CPSOperations();
$scaOpsObj = new SCAOperations();
$devObj = new DeviceOperations();
$softPhoneDatArr = array();
$key = 0;
$providerId = $_SESSION["sp"];
$groupId = $_SESSION["groupId"];
$softPhoneUserName1="";
$softPhonePassword1="";
foreach ($_POST["softPhoneDeviceName"] as $softPhoneDeleteKey => $softPhoneDeviceVal){
    $expDevVal = explode(",", $softPhoneDeviceVal);
    $softPhoneDatArr[$key]["deviceName"] = $expDevVal[0];
    $softPhoneDatArr[$key]["linePort"] = $expDevVal[1];
    $softPhoneDatArr[$key]["softPDeviceType"] = $expDevVal[2];
    $softPhoneDatArr[$key]["softPhoneType"] = $expDevVal[3];
    $key++;
}

if(count($softPhoneDatArr) > 0){
    foreach ($softPhoneDatArr as $softUnAssignKey => $softUnAssignVal){
        //$tagListResp = $vdmobj->getDeviceCustomTags($_SESSION["sp"], $_SESSION["groupId"], $softUnAssignVal["deviceName"]);
        if($softUnAssignVal["softPhoneType"] == "counterPath"){
            $devUrl = $devObj->getDeviceFileUrl($softUnAssignVal["deviceName"]);
            
            if(isset($softUnAssignVal["deviceName"]) && $softUnAssignVal["deviceName"] != ""){
                $respVal = $objSoft->getDevicesByUsingUserId($postUserId);
                
                if(empty($respVal["Error"])){
                    foreach ($respVal["Success"] as $arrKey => $arrVal){
                        if($arrKey == $softUnAssignVal["deviceName"]){
                            $softPhoneUserName1 = $arrVal["userName"];
                            $softPhonePassword1 = $arrVal["password"];
                        }
                        
                    }
                }
                
            }
            
            $filePath = $devUrl["Success"];
            //preg_match("/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/", $filePath, $matches);
            //$findIPFromStr = $matches[0];
            
            $privateIp = $xspPrivateIP;
            
            //$privateIp      = "10.100.10.207";
            //$changeURL = str_replace($findIPFromStr, $privateIp, $filePath);
            
            //$var1 = "https://69.169.36.217:80/dms/Counterpath_Bria_-_Desktop/configuration.txt";
            $findIPFromStr = parse_url($filePath, PHP_URL_HOST);
            //$privateIp      = "10.100.10.207";
            $changeURL = str_replace($findIPFromStr, $privateIp, $filePath);
            //print_r($changeURL);
            
            $valRes = curlgetFile($changeURL, $softPhoneUserName1, $softPhonePassword1);
            //print_r($valRes);
            if($valRes["status"] == "200"){
                $info = parseConfigFile($valRes["result"]);
                //print_r($info);
            }else{
                $message .= "<li style='color:red;'>Unable to read configuration from Broadworks. Please contact support.</li>";
                continue;
            }
            
            $arrayToProcess["cpsArray"]["userName"] = trim($info["account_username"]);
            $cpsDelResp = $cpsobj->counterPathAccountDelete($arrayToProcess);
        }
        
        
        //if($cpsDelResp == "true"){
            $scaUnAssignResp = $scaOpsObj->deleteDevicesFromSCA($postUserId, $softUnAssignVal["deviceName"], $softUnAssignVal["linePort"]);
            if($scaUnAssignResp["Error"] != ""){
                $message .= "<li style='color:red;'>There is an Error: ".$softUnAssignVal["deviceName"]." can not be un assigned from SCA Device List.</li>";
            }else{
                $message .= "<li>".$softUnAssignVal["deviceName"]." Device Un Assigned as SCA Device from User ".$postUserId.".</li>";
                $devDelResp = $devObj->getDeleteDevice($softUnAssignVal["deviceName"], $providerId, $groupId);
                if($devDelResp["Error"] != ""){
                    $message .= "<li style='color:red;'>There is an Error: ".$softUnAssignVal["deviceName"]." can not be deleted, ".$devDelResp["Error"].".</li>";
                }else{
                    $message .= "<li>".$softUnAssignVal["deviceName"]." Device has been deleted.</li>";
                    
                    $date = date("Y-m-d H:i:s");
                    $query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
                    $query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', 'Soft Phone Delete', '" . $_SESSION["groupId"] . "', '" . $_SESSION["userInfo"]["lastName"] . ", " . $_SESSION["userInfo"]["firstName"] . "')";
                    $sth = $expProvDB->expressProvLogsDb->query($query);
                    
                    $lastId = $expProvDB->expressProvLogsDb->lastInsertId();
                    
                    $query = "INSERT into softPhoneModChanges (id, serviceId, entityName, field, oldValue, newValue) ";
                    $query .= "VALUES ('" . $lastId . "', '" . $softUnAssignVal["deviceName"] . "', '" . $userId . "', ";
                    $query .= " '', '', '')";
                    //print_r($query);
                    $sth = $expProvDB->expressProvLogsDb->query($query);
                    
                }
            }
        //}
    }
}
function curlgetFile($URL, $userName, $password){
    //$URL = 'http://10.100.10.207:80/dms/Counterpath_Bria_with_Broadworks/configuration.txt';
    //$userName = "9810090020";
    //$password = "Admin123";
    $arr = array();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$URL);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_USERPWD, "$userName:$password");
    $result=curl_exec ($ch);
    //print_r($result);
    $arr["result"]=$result;
    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    $arr["status"]=$status_code;
    ///print_r($status_code);
    curl_close ($ch);
    return $arr;
}

function parseConfigFile($result){
    $tmpResultArr = explode("\n", $result);
    unset($tmpResultArr[0]);
    unset($tmpResultArr[1]);
    unset($tmpResultArr[2]);
    $customizeResultArr = array();
    
    $count = 0;
    
    foreach($tmpResultArr as $ky => $vl){
        if($vl!=""){
            $tmpArr = explode("=", $vl);
            $customizeResultArr[$tmpArr[0]]  = str_replace('"', "", $tmpArr[1]);
        }
        $count = $count + 1;
    }
    return $customizeResultArr;
}
?>