<?php
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once("/var/www/lib/broadsoft/adminPortal/counterPathStretto/CPSOperations.php");
$cpsobj = new CPSOperations();
$_SESSION ["softPhoneAddDialog"] = array (
    "softPhoneFalse"                  => "Soft Phone",
    "softPhoneDeviceType"             => "Soft Phone Device Type",
    "softPhoneDeviceTypeLinePort"     => "Soft Phone Line/Port Domain",
    "softPhoneDeviceAccUserName"      => "Soft Phone Device Access UserName",
    "softPhoneDeviceAccUserPass"      => "Soft Phone Device Access Password",
    "accountUserName"                 => "Account UserName",
    "accountProfile"                  => "Account Profile",
    "softPhoneAccountEmailAddress"    => "Account Email Address",
    "rebuildSoftPhoneFiles" => "Rebuild Soft Phone Files",
    "resetSoftPhone" => "Reset Soft Phone"
);

$changes = 0;
$error = 0;
//print_r($_POST);
//print_r($_SESSION["counterPathDevices"]);
$arrayForDevice = array();
$changedArray = array();

if(isset($_SESSION["counterPathDevices"]) && !empty($_SESSION["counterPathDevices"])){
    
    foreach ($_SESSION["counterPathDevices"] as $devkey => $devval ){
        if($devval["deviceType"] == $_POST["softPhoneDeviceType"]){
            $arrayForDevice = $devval;
        }
    }
    $data = $errorTableHeader;
    foreach ($_POST as $key => $value){
        if($key == "softPhoneDeviceTypeLinePort" ){
            $linePortName = $arrayForDevice["linePort"];
            $var1 = explode("@", $linePortName);
            if($value != $var1[1]){
                $changes++;
                $bg = CHANGED;
                $changedArray["changes"]["linePortDomain"] = $value;
                $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["softPhoneAddDialog"][$key] . "</td><td class=\"errorTableRows\">".$value."</td>";
            }
        }
        
        /*if($key == "accountUserName" && ($value != $arrayForDevice["accountUserName"])){
            $changes++;
            $bg = CHANGED;
            $changedArray["changes"]["accountUserName"] = $value;
            $changedArray["changes"]["oldAccountUserName"] = $arrayForDevice["accountUserName"];
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["softPhoneAddDialog"][$key] . "</td><td class=\"errorTableRows\">".$value."</td>";
        }*/
        
        if($key == "accountUserName" && $_POST["softPhoneNameType"] == "counterPath"){
            if($value == ""){
                $changes++;
                $error = 1;
                $bg = INVALID;
                $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["softPhoneAddDialog"][$key] . "</td><td class=\"errorTableRows\">Soft Phone Account UserName can not be blank.</td>";
            }else if($value != $arrayForDevice["accountUserName"]){
                $cpsReslt = $cpsobj->counterPathAccountGet(trim($value));
                if($cpsReslt == "true"){
                    $changes++;
                    $error = 1;
                    $bg = INVALID;
                    //$value = $_SESSION["userAddNames"][$key] . " is already exist.";
                    $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["softPhoneAddDialog"][$key] . "</td><td class=\"errorTableRows\">".$value." is already exist.</td>";
                }else{
                    $changes++;
                    $bg = CHANGED;
                    $changedArray["changes"]["accountUserName"] = $value;
                    $changedArray["changes"]["oldAccountUserName"] = $arrayForDevice["accountUserName"];
                    $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["softPhoneAddDialog"][$key] . "</td><td class=\"errorTableRows\">".$value."</td>";
                }
                
            }
        }
        
        if($key == "softPhoneAccountEmailAddress" && ($value != $arrayForDevice["accountEmail"])){
            $changes++;
            $bg = CHANGED;
            $changedArray["changes"]["softPhoneAccountEmailAddress"] = $value;
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["softPhoneAddDialog"][$key] . "</td><td class=\"errorTableRows\">".$value."</td>";
        }
        if($key == "rebuildSoftPhoneFiles" && $value == "true"){
            $changes++;
            $bg = CHANGED;
            $changedArray["changes"]["rebuildSoftPhoneFiles"] = $value;
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["softPhoneAddDialog"][$key] . "</td><td class=\"errorTableRows\">".$value."</td>";
        }
        if($key == "resetSoftPhone" && $value == "true"){
            $changes++;
            $bg = CHANGED;
            $changedArray["changes"]["resetSoftPhone"] = $value;
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["softPhoneAddDialog"][$key] . "</td><td class=\"errorTableRows\">".$value."</td>";
        }
        
        if($key == "softPhoneDeviceAccUserName" || $key == "softPhoneDeviceAccUserPass"){
           if($key == "softPhoneDeviceAccUserName" && $value != ""){
               $changes++;
               $bg = CHANGED;
               $changedArray["changes"]["softPhoneDeviceAccUserName"] = $value;
               $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["softPhoneAddDialog"][$key] . "</td><td class=\"errorTableRows\">".$value."</td>";
           }
           if($key == "softPhoneDeviceAccUserPass" && $value != ""){
               $changes++;
               $bg = CHANGED;
               $changedArray["changes"]["softPhoneDeviceAccUserPass"] = $value;
               $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["softPhoneAddDialog"][$key] . "</td><td class=\"errorTableRows\">*******</td>";
           }
            
        }
        
        
    }
    
    
    
    
    
}

if(count($changedArray["changes"]) > 0){
    $_SESSION["softPhoneMod"] = "softPhoneMod";
    $changedArray["details"]["deviceName"]=$arrayForDevice["deviceName"];
    $changedArray["details"]["softPhoneNameType"]=$_POST["softPhoneNameType"];
    $changedArray["details"]["deviceType"]=$_POST["softPhoneDeviceType"];
    $changedArray["original"] = $_POST;
    $_SESSION["softPhoneModData"] = $changedArray;
}
//print_r($changedArray);

if ($changes == 0)
{
    $error = 1;
    $data .= "<td class=\"errorTableRows ccccc\">Device Update </td><td class=\"errorTableRows\">No Chnages</td>";
}
$data .= "</table>";
echo $error . $data;

/*echo "werer".$changes;
if($changes == 0){
    $error=1;
    $data1 = $errorTableHeader;
    $data1 .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:#00AC3E;\">Device Update</td><td class=\"errorTableRows\">No Chnages</td>";
    $data1 .= "</table>";
    echo $error . $data1;
}else{
    echo $error . $data;
}*/

/*if($changes){
    $error=0;
    echo $error . $data;
}else{
    $error=1;
    $data1 = $errorTableHeader;
    $data1 .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:#00AC3E;\">Device Update</td><td class=\"errorTableRows\">No Chnages</td>";
    $data1 .= "</table>";
    echo $error . $data1;
    
}*/

?>
