<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
require_once("/var/www/lib/broadsoft/adminPortal/DeviceNameBuilder.php");
require_once("/var/www/lib/broadsoft/adminPortal/LineportNameBuilder.php");
require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
require_once("/var/www/lib/broadsoft/adminPortal/sca/SCAOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
$syslevelObj = new sysLevelDeviceOperations();

require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
$expProvDB = new ExpressProvLogsDB();

unset($_SESSION['softPhoneLinePortVal']);
unset($_SESSION['deviceNameVal']);
unset($_SESSION["softPhoneDeviceTypeVal"]);
require_once("/var/www/lib/broadsoft/adminPortal/counterPathStretto/CPSOperations.php");
$cpsobj = new CPSOperations();

require_once ("/var/www/lib/broadsoft/adminPortal/ocpFeatures/OCPOperations.php");
$objSoft = new OCPOperations();
$deviceFileGet = false;
$cpsAdded = false;
$deviceAdded = false;
$deviceFaillure = false;

require_once("/var/www/lib/broadsoft/adminPortal/customTagManagement.php");
$objCustomTagMgMt = new CustomTagManagement();
require_once("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig.php");

$softPhoneUserName = "";
$softPhonePassword = "";
$softPhoneUserName1 = "";
$softPhonePassword1 = "";
$softPhoneRollBack = array();

if(isset($_SESSION ["userInfo"] ["deviceName"]) && $_SESSION ["userInfo"] ["deviceName"] != ""){
    $respVal = $objSoft->getDevicesByUsingUserId($postUserId);
    if(empty($respVal["Error"])){
        foreach ($respVal["Success"] as $arrKey => $arrVal){
            if($arrKey == $_SESSION ["userInfo"] ["deviceName"]){
                $softPhoneUserName = $arrVal["userName"];
                $softPhonePassword = $arrVal["password"];
            }
            
        }
    }
    
}

$deviceObjSoftPhone = new DeviceOperations();
$scaOpsObj = new SCAOperations();

$changeSCAAssignLog = array();
$grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
$servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
$servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray);
$scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);


foreach($scaServiceArray as $ssa=>$ssv){
    $pos = strpos($ssv->col[0], "Shared Call Appearance");
    if($pos === false){
    }else{
        if($ssv->count == "1"){
            $servicePackToBeAssigned = $servicePackArray[$ssa][0];
        }
    }
}

$userServices = new userServices();
$userSessionservicePackArray = $userServices->getUserServicesAll($postUserId, $_SESSION["sp"]);
$servicesSessionArray = $grpServiceList->getServicePackServicesListUsers($userSessionservicePackArray["Success"]["ServicePack"]["assigned"]);
$scaSessionServiceArray = $grpServiceList->checkScaUserServiceInServicepack($servicesSessionArray);

//code ends
if(count($scaSessionServiceArray) == 0){
    $responseServicePackUser = $grpServiceList->assignServicePackUser($postUserId, $servicePackToBeAssigned);
    if(empty($responseServicePackUser["Error"])){
        $message .= "<li>".$servicePackToBeAssigned." SCA service pack has been assigned to " . $postUserId. ".</li>";
        
    }
}

function getResourceForSoftPhoneLinePortCreation($attr) {
    
    switch ($attr) {
        case ResourceNameBuilder::DN:               return $_SESSION["userInfo"]["phoneNumber"] != "" ? $_SESSION["userInfo"]["phoneNumber"] : "";
        case ResourceNameBuilder::EXT:              return $_SESSION["userInfo"]["extension"];
        case ResourceNameBuilder::USR_CLID:         return $_SESSION["userInfo"]["callingLineIdPhoneNumber"];
        case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
        case ResourceNameBuilder::GRP_ID:           return str_replace(" ", "_", trim($_SESSION["groupId"]));
        case ResourceNameBuilder::SYS_DFLT_DOMAIN:  return $_SESSION["systemDefaultDomain"];
        case ResourceNameBuilder::DEFAULT_DOMAIN:   return $_SESSION["defaultDomain"];
        case ResourceNameBuilder::GRP_PROXY_DOMAIN: return $_SESSION['softPhoneLinePortVal'];
        case ResourceNameBuilder::FIRST_NAME:       return $_SESSION["userInfo"]["firstName"];
        case ResourceNameBuilder::LAST_NAME:        return $_SESSION["userInfo"]["lastName"];
        case ResourceNameBuilder::DEV_NAME:         return isset($_SESSION['deviceNameVal']) && $_SESSION['deviceNameVal'] != "" ? $_SESSION['deviceNameVal'] : "";
        case ResourceNameBuilder::MAC_ADDR:         return isset($_SESSION["userInfo"]["macAddress"]) && $_SESSION["userInfo"]["macAddress"] != "" ? $_SESSION["userInfo"]["macAddress"] : "";
        case ResourceNameBuilder::ENT:              return strtolower(trim($_SESSION["sp"]));
        case ResourceNameBuilder::USR_ID:
            $name = isset($_SESSION["userId"]) ? $_SESSION["userId"] : "";
            if ($name == "") {
                return "";
            }
            $nameSplit = explode("@", $name);
            return $nameSplit[0];
            break;
        default:                                    return "";
    }
}

function getResourceForSoftDeviceNameCreation($attr) {
    switch ($attr) {
        case ResourceNameBuilder::DN:               return $_SESSION["userInfo"]["phoneNumber"] != "" ? $_POST["phoneNumber"] : "";
        case ResourceNameBuilder::EXT:              return $_SESSION["userInfo"]["extension"];
        case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
        case ResourceNameBuilder::GRP_ID:           return $_SESSION["groupId"];
        case ResourceNameBuilder::FIRST_NAME:       return $_SESSION["userInfo"]["firstName"];
        case ResourceNameBuilder::LAST_NAME:        return $_SESSION["userInfo"]["lastName"];
        case ResourceNameBuilder::DEV_TYPE:         return $_SESSION["softPhoneDeviceTypeVal"];
        case ResourceNameBuilder::MAC_ADDR:         return $_SESSION["userInfo"]["macAddress"];
        case ResourceNameBuilder::INT_1:
        case ResourceNameBuilder::INT_2:            return $_POST["deviceIndex"];
        default: return "";
    }
}

foreach ($_POST["softPhoneListToAdd"] as $key => $value){
    $_SESSION['softPhoneLinePortVal'] = $value["softPhoneDeviceTypeLinePort"];
    $_SESSION['deviceNameVal'] = $value["deviceName"];
    $_SESSION["softPhoneDeviceTypeVal"] = $value["deviceType"];
    $softPhoneArray = array();
    $softPhoneArray["sp"] = $_SESSION["sp"];
    $softPhoneArray["groupId"] = $_SESSION["groupId"];
    $softPhoneArray["userId"] = $postUserId;
    $softPhoneArray["deviceType"] = $value["deviceType"];
    $softPhoneArray["deviceName"] = $value["deviceName"];
    
    $setName = $syslevelObj->getTagSetNameByDeviceType($value["deviceType"], $ociVersion);
    $systemDefaultTagVal = "";
    if($setName["Success"] != "" && !empty($setName["Success"])){
        $defaultSettags = $syslevelObj->getDefaultTagListByTagSet($setName["Success"]);
        $phoneTemplateName = "%COUNTERPATH_SBC%";
        if(array_key_exists($phoneTemplateName, $defaultSettags["Success"])){
            $systemDefaultTagVal = $defaultSettags["Success"]["%COUNTERPATH_SBC%"][0];
        }
    }
    //echo "111111111"; print_r($systemDefaultTagVal);exit;
    $payloadData = "account.notification._userEmailAddress=<express_email_value>
account1Sip.accountName=<express_account_name>
account1Sip.credentials.authorizationName=<express_authorizationName_value>
account1Sip.credentials.password=<express_password_value>
account1Sip.credentials.username=<express_username_value>
account1Sip.domain=<express_domain_value>
account1Sip.proxy=<express_proxy_value>";
    
    if($softPhoneUserName == "" && $softPhonePassword == ""){
        $deviceAccessUserNameBuilder = new DeviceNameBuilder($deviceAccessUserName1,
            getResourceForSoftDeviceNameCreation(ResourceNameBuilder::DN),
            getResourceForSoftDeviceNameCreation(ResourceNameBuilder::EXT),
            getResourceForSoftDeviceNameCreation(ResourceNameBuilder::GRP_DN),
            $deviceAccessUserName2 == "");    // force fallback only if second criteria does not exist
            
            if (! $deviceAccessUserNameBuilder->validate()) {
                // TODO: Implement resolution when Device name input formula has errors
            }
            
            do {
                $attr = $deviceAccessUserNameBuilder->getRequiredAttributeKey();
                $deviceAccessUserNameBuilder->setRequiredAttribute($attr, getResourceForSoftDeviceNameCreation($attr));
            } while ($attr != "");
            
            $softPhoneDeviceAccessUserName = $deviceAccessUserNameBuilder->getName();
            
            if ($softPhoneDeviceAccessUserName == "") {
                $deviceAccessUserNameBuilder = new DeviceNameBuilder($deviceAccessUserName2,
                    getResourceForSoftDeviceNameCreation(ResourceNameBuilder::DN),
                    getResourceForSoftDeviceNameCreation(ResourceNameBuilder::EXT),
                    getResourceForSoftDeviceNameCreation(ResourceNameBuilder::GRP_DN),
                    true);
                
                if (! $deviceAccessUserNameBuilder->validate()) {
                    // TODO: Implement resolution when Device name input formula has errors
                }
                
                do {
                    $attr = $deviceAccessUserNameBuilder->getRequiredAttributeKey();
                    $deviceAccessUserNameBuilder->setRequiredAttribute($attr, getResourceForSoftDeviceNameCreation($attr));
                } while ($attr != "");
                
                $softPhoneDeviceAccessUserName = $deviceAccessUserNameBuilder->getName();
            }
            
            // Build Device Access Password - fallback definitions are based on Device Name fallback
            // ----------------------------------------------------------------------------------------
            $deviceAccessPasswordBuilder = new DeviceNameBuilder($deviceAccessPassword,
                getResourceForSoftDeviceNameCreation(ResourceNameBuilder::DN),
                getResourceForSoftDeviceNameCreation(ResourceNameBuilder::EXT),
                getResourceForSoftDeviceNameCreation(ResourceNameBuilder::GRP_DN),
                true);
            
            if (! $deviceAccessPasswordBuilder->validate()) {
                // TODO: Implement resolution when Device name input formula has errors
            }
            
            do {
                $attr = $deviceAccessPasswordBuilder->getRequiredAttributeKey();
                $deviceAccessPasswordBuilder->setRequiredAttribute($attr, getResourceForSoftDeviceNameCreation($attr));
            } while ($attr != "");
            
            $softPhoneDeviceAccessPassword = $deviceAccessPasswordBuilder->getName();
            
            $softPhoneArray["deviceAccessPassword"] = $softPhoneDeviceAccessPassword;
            
            if ($value["softPhoneDeviceAccUserName"] !== "" and $value["softPhoneDeviceAccUserPass"] !== "")
            {
                $softPhoneDeviceAccessUserName = $value["softPhoneDeviceAccUserName"];
                $softPhoneDeviceAccessPassword = $value["softPhoneDeviceAccUserPass"];
            }
            
            $softPhoneArray["deviceAccessUserName"] = $softPhoneDeviceAccessUserName."-softphone";
            $softPhoneArray["deviceAccessPassword"] = $softPhoneDeviceAccessPassword;
    }else{
        $softPhoneArray["deviceAccessUserName"] = $softPhoneUserName."-softphone";
        $softPhoneArray["deviceAccessPassword"] = $softPhonePassword;
    }
    
    //Device Access UserName and Password
    
        
        //LinePort creation
        $linePort = "";
        $linePortBuilder1 = new LineportNameBuilder($softPhoneLinePortCriteria1,
            getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::DN),
            getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::EXT),
            getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::USR_CLID),
            getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::GRP_DN),
            getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
            $softPhoneLinePortCriteria2 == "");  // force fallback only if second criteria does not exist
            
            if (! $linePortBuilder1->validate()) {
                // TODO: Implement resolution when lineport input formula has errors
            }
            
            do {
                $attr = $linePortBuilder1->getRequiredAttributeKey();
                $linePortBuilder1->setRequiredAttribute($attr, getResourceForSoftPhoneLinePortCreation($attr));
            } while ($attr != "");
            
            $linePort = $linePortBuilder1->getName();
            
            if ($linePort == "") {
                // Create lineport name builder from the second formula. At this point we know that there is second
                // formula, because without second formula name would be forcefully resolved in the first formula.
                
                $linePortBuilder2 = new LineportNameBuilder($softPhoneLinePortCriteria2,
                    getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::DN),
                    getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::EXT),
                    getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::USR_CLID),
                    getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::GRP_DN),
                    getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
                    true);
                
                if (! $linePortBuilder2->validate()) {
                    // TODO: Implement resolution when User ID input formula has errors
                }
                
                do {
                    $attr = $linePortBuilder2->getRequiredAttributeKey();
                    $linePortBuilder2->setRequiredAttribute($attr, getResourceForSoftPhoneLinePortCreation($attr));
                } while ($attr != "");
                
                $linePort = $linePortBuilder2->getName();
            }
            
            $softPhoneLinePort = str_replace(" ", "_", $linePort);
            
            $softPhoneArray["linePort"] = $softPhoneLinePort;
            
            $softPhoneArray["cpsArray"]["groupName"] = $counterpathGroupName;
            $softPhoneArray["cpsArray"]["userName"] = $value["accountUserName"];
            $softPhoneArray["cpsArray"]["profileName"] = $value["accountProfile"];
            $softPhoneArray["cpsArray"]["userEmailAddress"] = $value["softPhoneAccountEmailAddress"];
            $softPhoneArray["cpsArray"]["accountName"] = $value["deviceName"];
            $softPhoneArray["cpsArray"]["authorizationName"] = $_SESSION["userId"];
            $softPhoneArray["cpsArray"]["password"] = $softPhoneDeviceAccessPassword;
            $softPhoneArray["cpsArray"]["crndusername"] = $softPhoneLinePort;
            $softPhoneArray["cpsArray"]["domain"] = $value["softPhoneDeviceTypeLinePort"];
            $softPhoneArray["cpsArray"]["proxy"] = $systemDefaultTagVal;
            
            if(!empty($softPhoneArray["deviceName"])){
                $devAddResp = $deviceObjSoftPhone->deviceAddRequest($softPhoneArray);
               
                if(!empty($devAddResp["Error"])){
                    $message .= "<li style='color:red;'>There is an Error ".$softPhoneArray["deviceName"]." ".$devAddResp["Error"].".</li>";
                    $softPhoneRollBack["deviceAdd"]["status"] = "failed";
                    $softPhoneRollBack["deviceAdd"]["deviceName"] = $softPhoneArray["deviceName"];
                }else{
                    $softPhoneRollBack["deviceAdd"]["status"] = "added";
                    $softPhoneRollBack["deviceAdd"]["deviceName"] = $softPhoneArray["deviceName"];
                    $message .= "<li>".$softPhoneArray["deviceName"]." Device Added Successfully.</li>";
                    $addDeviceAsSCA = $scaOpsObj->addDeviceToUserAsSCA($softPhoneArray["userId"], $softPhoneArray["deviceName"], $softPhoneArray["linePort"], "false", "");
                    $softPhoneRollBack["deviceSCAAssign"]["deviceName"] = $softPhoneArray["deviceName"];
                    if(!empty($addDeviceAsSCA["Error"])){
                        $softPhoneRollBack["deviceSCAAssign"]["status"] = "failed";
                        
                        $message .= "<li style='color:red;'>There is an Error ".$softPhoneArray["deviceName"]." ".$addDeviceAsSCA["Error"].".</li>";
                    }else{
                        $softPhoneRollBack["deviceSCAAssign"]["status"] = "added";
                        $deviceAdded = true;
                        $message .= "<li>".$softPhoneArray["deviceName"]." Device Assigned as SCA Device on User ".$postUserId.".</li>";
                    }
                }
                
                
            }
            
            if($value["softPhoneType"] == "counterPath" && !empty($softPhoneArray["deviceName"])){
                
                $deviceName = $softPhoneArray["deviceName"];
                $customTagArray["groupName"] = $softPhoneArray["cpsArray"]["groupName"];
                $customTagArray["CPUsername"] = $softPhoneArray["cpsArray"]["userName"];
                $customTagArray["profileName"] = $softPhoneArray["cpsArray"]["profileName"];
                $customTagArray["ctrpathuserEmailAddress"] = $softPhoneArray["cpsArray"]["userEmailAddress"];
                foreach($customTagArray as $keyCpsRow => $valCpsRow){
                    $tagName     = "%".$keyCpsRow."%";
                    $tagValue    = $valCpsRow;
                    $customTagResultResponse = $objCustomTagMgMt->addDeviceCustomTagDuplicate($deviceName, $tagName, $tagValue);
                }
            }
            
            //Device Rebuild and Reset code
            //if(($value["rebuildSoftPhoneFiles"] == "true" || $value["resetSoftPhone"] == "true") && $deviceAdded){
            if(empty($devAddResp["Error"])){
                rebuildAndResetDeviceConfig($deviceName, "rebuildAndReset");
                $message .= "<li>Device has been Rebuild/Reset Successfully.</li>";
            }
            
            //}
            //sleep(10);
            //Counter path Account Add Code
            if($value["softPhoneType"] == "counterPath"){
                $devUrl = $deviceObjSoftPhone->getDeviceFileUrl($value["deviceName"]);
                
                if(isset($value["deviceName"]) && $value["deviceName"] != ""){
                    $respVal = $objSoft->getDevicesByUsingUserId($postUserId);
                    
                    if(empty($respVal["Error"])){
                        foreach ($respVal["Success"] as $arrKey => $arrVal){
                            if($arrKey == $value["deviceName"]){
                                $softPhoneUserName1 = $arrVal["userName"];
                                $softPhonePassword1 = $arrVal["password"];
                            }
                            
                        }
                    }
                    
                }
                
                $filePath = $devUrl["Success"];
                //preg_match("/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/", $filePath, $matches);
                //$findIPFromStr = $matches[0];
                
                $privateIp = $xspPrivateIP;
                //$privateIp      = "10.100.10.207";
                //$changeURL = str_replace($findIPFromStr, $privateIp, $filePath);
                
                //$var1 = "https://69.169.36.217:80/dms/Counterpath_Bria_-_Desktop/configuration.txt";
                $findIPFromStr = parse_url($filePath, PHP_URL_HOST);
                //$privateIp      = "10.100.10.207";
                $changeURL = str_replace($findIPFromStr, $privateIp, $filePath);
                //print_r($changeURL);
                
                //$valRes = curlgetFile($changeURL, $softPhoneUserName1, $softPhonePassword1);
                $valRes = getRecursiveData($changeURL, $softPhoneUserName1, $softPhonePassword1, $sleep = 0);
                //print_r($valRes);
                if($valRes["status"] == "200"){
                    $info = parseConfigFile($valRes["result"]);
                    $deviceFileGet = true;
                    //print_r($info);
                }
                                
               // $info = parse_ini_file('configuration.txt');
                
                $softPhoneArray["cpsArray"]["groupName"] = trim($info["account_groupname"]);
                $softPhoneArray["cpsArray"]["userName"] = trim($info["account_username"]);
                $softPhoneArray["cpsArray"]["profileName"] = trim($info["account_profile"]);
                $softPhoneArray["cpsArray"]["userEmailAddress"] = trim($info["account.userEmailAddress"]);
                $softPhoneArray["cpsArray"]["accountName"] = trim($info["account1Sip.accountName"]);
                //$softPhoneArray["cpsArray"]["authorizationName"] = $_SESSION["userId"];
                $softPhoneArray["cpsArray"]["authorizationName"] = trim($info["account1Sip.credentials.authorizationName"]);
                
                //$softPhoneArray["cpsArray"]["password"] = $softPhoneDeviceAccessPassword;
                $softPhoneArray["cpsArray"]["password"] = trim($info["account1Sip.credentials.password"]);
                $softPhoneArray["cpsArray"]["crndusername"] = trim($info["account1Sip.credentials.username"]);
                $softPhoneArray["cpsArray"]["domain"] = trim($info["account1Sip.domain"]);
                $softPhoneArray["cpsArray"]["proxy"] = trim($info["account1Sip.proxy"]);
               // print_r($softPhoneArray); 
                $softPhoneRollBack["cps"]["uName"] = $softPhoneArray["cpsArray"]["userName"];
                if($deviceFileGet){
                    $cpsResult = $cpsobj->counterPathAccountAdd($softPhoneArray, $payloadData);
                }else{
                    $message .= "<li style='color:red;'>Unable to read configuration from Broadworks. Please contact support.</li>";
                }
                //$cpsResult = $cpsobj->counterPathAccountAdd($softPhoneArray, $payloadData);
               // print_r($cpsResult);
                if($cpsResult["status"] == "200" && $deviceFileGet){
                    $cpsAdded = true;
                    $softPhoneRollBack["cps"]["status"] = "added";
                    $message .= "<li>".$softPhoneArray["cpsArray"]["userName"]." Counter Path Account Added Successfully.</li>";
                }else{
                    
                    $softPhoneRollBack["cps"]["status"] = "failed";
                    $message .= "<li style='color:red;'>".$softPhoneArray["cpsArray"]["userName"]." Counter Path Account was attempted to be Add but failed.</li>";
                }
            }
            
           
            // Roll Back Code
            if(!empty($softPhoneRollBack)){
                if($softPhoneRollBack["deviceAdd"]["status"] == "failed" || $softPhoneRollBack["deviceSCAAssign"]["status"] == "failed" || $softPhoneRollBack["cps"]["status"] == "failed"){
                    if($softPhoneRollBack["cps"]["status"] == "failed"){
                        if($value["softPhoneType"] == "counterPath"){
                            $scaUnAssign = $scaOpsObj->deleteDevicesFromSCA($softPhoneArray["userId"], $softPhoneArray["deviceName"], $softPhoneArray["linePort"]);
                            if($scaUnAssign["Error"] == ""){
                                $message .= "<li style='color:red;'>".$softPhoneArray["deviceName"]." unsaaigned successfully.</li>";
                                $deviceDelResp = $deviceObjSoftPhone->getDeleteDevice($softPhoneRollBack["deviceSCAAssign"]["deviceName"], $_SESSION["sp"], $_SESSION["groupId"]);
                                $deviceFaillure = true;
                                if(!empty($deviceDelResp["Error"])){
                                    $message .= "<li style='color:red;'>".$softPhoneArray["deviceName"]." ".$deviceDelResp["Error"].".</li>";
                                }else{
                                    $message .= "<li style='color:red;'>".$softPhoneArray["deviceName"]." deleted successfully.</li>";
                                }
                            }else{
                                $message .= "<li style='color:red;'>".$softPhoneArray["deviceName"]." ".$scaUnAssign["Error"].".</li>";
                            }
                            
                        }
                        
                    }
                    /*if($value["softPhoneType"] == "counterPath"){
                        $cpsDelResult = $cpsobj->counterPathAccountDelete($softPhoneArray);
                        $deviceFaillure = true;
                        if($cpsDelResult != "true"){
                            $message .= "<li style='color:red;'>".$softPhoneArray["cpsArray"]["userName"]." Counter Path Account was attempted to be Delete but failed.</li>";
                            
                        }else{
                            
                            $message .= "<li style='color:red;'>".$softPhoneArray["cpsArray"]["userName"]." Counter Path Account deleted succesfully.</li>";
                        }
                    }*/
                    if($softPhoneRollBack["deviceSCAAssign"]["status"] == "failed"){
                        $deviceDelResp = $deviceObjSoftPhone->getDeleteDevice($softPhoneRollBack["deviceSCAAssign"]["deviceName"], $_SESSION["sp"], $_SESSION["groupId"]);
                        $deviceFaillure = true;
                        if(!empty($deviceDelResp["Error"])){
                            $message .= "<li style='color:red;'>".$softPhoneArray["deviceName"]." ".$deviceDelResp["Error"].".</li>";
                        }else{
                            $message .= "<li style='color:red;'>".$softPhoneArray["deviceName"]." deleted successfully.</li>";
                        }
                    }
                    
                }
            }
            
            
            //Change Log Code
            if(!$deviceFaillure){
                if($value["softPhoneType"] == "generic"){
                    if($deviceAdded){
                        $date = date("Y-m-d H:i:s");
                        $query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
                        $query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', 'Soft Phone Added', '" . $_SESSION["groupId"] . "', '".$softPhoneArray["deviceName"]."')";
                        $sth = $expProvDB->expressProvLogsDb->query($query);
                        
                        $lastId = $expProvDB->expressProvLogsDb->lastInsertId();
                        
                        $insert = "INSERT into softPhoneAddChanges (id, softPhoneAcctDeviceType, softPhoneAcctDeviceName, softPhoneAcctLinePortDomain)";
                        $insert .= " VALUES ('" . $lastId . "','" .
                            $softPhoneArray["deviceType"] . "', '" .
                            $softPhoneArray["deviceName"] . "', '" .
                            $softPhoneArray["cpsArray"]["domain"] . "')";
                            //print_r($insert);
                            $fiq = $expProvDB->expressProvLogsDb->query($insert);
                    }
                }else if($value["softPhoneType"] == "counterPath"){
                    if($deviceAdded && $cpsAdded){
                        $date = date("Y-m-d H:i:s");
                        $query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
                        $query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', 'Counter Path Account Added', '" . $_SESSION["groupId"] . "', '".$softPhoneArray["deviceName"]."')";
                        $sth = $expProvDB->expressProvLogsDb->query($query);
                        
                        $lastId = $expProvDB->expressProvLogsDb->lastInsertId();
                        
                        $insert = "INSERT into counterPathAddChanges (id, counterPathAcctGroupname, counterPathAcctProfilename, counterPathAcctUsername, counterPathAcctEmailAddr, counterPathAcctAuthName, counterPathAcctSipDomain, counterPathAcctSipProxy, counterPathAcctDeviceType, counterPathAcctDeviceName, counterPathAcctLinePortDomain)";
                        $insert .= " VALUES ('" . $lastId . "','" .
                            $counterpathGroupName . "', '" .
                            $softPhoneArray["cpsArray"]["profileName"] . "', '" .
                            $softPhoneArray["cpsArray"]["userName"] . "', '" .
                            $softPhoneArray["cpsArray"]["userEmailAddress"] . "', '" .
                            $softPhoneArray["cpsArray"]["authorizationName"] . "', '" .
                            $softPhoneArray["cpsArray"]["crndusername"] . "', '" .
                            $softPhoneArray["cpsArray"]["proxy"] . "', '" .
                            $softPhoneArray["deviceType"] . "', '" .
                            $softPhoneArray["cpsArray"]["domain"] . "', '" . "')";
                            //print_r($insert);
                            $fiq = $expProvDB->expressProvLogsDb->query($insert);
                    }
                }
            }
            
}

function curlgetFile($URL, $userName, $password){
    //$URL = 'http://10.100.10.207:80/dms/Counterpath_Bria_with_Broadworks/configuration.txt';
    //$userName = "9810090020";
    //$password = "Admin123";
    $arr = array();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$URL);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_USERPWD, "$userName:$password");
    $result=curl_exec ($ch);
    //print_r($result);
    $arr["result"]=$result;
    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    $arr["status"]=$status_code;
    ///print_r($status_code);
    curl_close ($ch);
    return $arr;
}

function parseConfigFile($result){
    $tmpResultArr = explode("\n", $result);
    unset($tmpResultArr[0]);
    unset($tmpResultArr[1]);
    unset($tmpResultArr[2]);
    $customizeResultArr = array();
    
    $count = 0;
    
    foreach($tmpResultArr as $ky => $vl){
        if($vl!=""){
            $tmpArr = explode("=", $vl);
            $customizeResultArr[$tmpArr[0]]  = str_replace('"', "", $tmpArr[1]);
        }
        $count = $count + 1;
    }
    return $customizeResultArr;
}

function getRecursiveData ($changeURL, $softPhoneUserName1, $softPhonePassword1, $sleep){
    //$sleep++;
    //echo "Sleep is". $sleep."<br />";
    $valRes1 = curlgetFile($changeURL, $softPhoneUserName1, $softPhonePassword1);
    //print_r($valRes1);
    if($valRes1["result"] == "") {
        sleep(1);
        if($sleep <= 9) {
            //echo "2229";
            $sleep++;
            $valRes1 = getRecursiveData($changeURL, $softPhoneUserName1, $softPhonePassword1, $sleep);
        }
        
    }
    //print_r($valRes1);
    return $valRes1;
}

?>