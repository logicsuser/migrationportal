<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
require_once("/var/www/lib/broadsoft/adminPortal/DeviceNameBuilder.php");
require_once("/var/www/lib/broadsoft/adminPortal/LineportNameBuilder.php");
require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
require_once("/var/www/lib/broadsoft/adminPortal/sca/SCAOperations.php");
require_once("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
$syslevelObj = new sysLevelDeviceOperations();

require_once ("/var/www/lib/broadsoft/adminPortal/ocpFeatures/OCPOperations.php");
$objSoft = new OCPOperations();

require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
$expProvDB = new ExpressProvLogsDB();	

//print_r($_SESSION["softPhoneModData"]);exit;
$arrayToProcessChanges = $_SESSION["softPhoneModData"]["changes"];
$arrayToProcessDetails = $_SESSION["softPhoneModData"]["details"];
$arrayToProcessOriginal = $_SESSION["softPhoneModData"]["original"];

$setName = $syslevelObj->getTagSetNameByDeviceType($arrayToProcessDetails["deviceType"], $ociVersion);
$systemDefaultTagVal = "";
if($setName["Success"] != "" && !empty($setName["Success"])){
    $defaultSettags = $syslevelObj->getDefaultTagListByTagSet($setName["Success"]);
    $phoneTemplateName = "%COUNTERPATH_SBC%";
    if(array_key_exists($phoneTemplateName, $defaultSettags["Success"])){
        $systemDefaultTagVal = $defaultSettags["Success"]["%COUNTERPATH_SBC%"][0];
    }
}

unset($_SESSION['softPhoneLinePortVal']);
unset($_SESSION['deviceNameVal']);
unset($_SESSION["softPhoneDeviceTypeVal"]);
require_once("/var/www/lib/broadsoft/adminPortal/counterPathStretto/CPSOperations.php");
$cpsobj = new CPSOperations();

$sp = $_SESSION["sp"];
$groupId = $_SESSION["groupId"];

function getResourceForSoftPhoneLinePortCreation($attr) {
    
    switch ($attr) {
        case ResourceNameBuilder::DN:               return $_SESSION["userInfo"]["phoneNumber"] != "" ? $_SESSION["userInfo"]["phoneNumber"] : "";
        case ResourceNameBuilder::EXT:              return $_SESSION["userInfo"]["extension"];
        case ResourceNameBuilder::USR_CLID:         return $_SESSION["userInfo"]["callingLineIdPhoneNumber"];
        case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
        case ResourceNameBuilder::GRP_ID:           return str_replace(" ", "_", trim($_SESSION["groupId"]));
        case ResourceNameBuilder::SYS_DFLT_DOMAIN:  return $_SESSION["systemDefaultDomain"];
        case ResourceNameBuilder::DEFAULT_DOMAIN:   return $_SESSION["defaultDomain"];
        case ResourceNameBuilder::GRP_PROXY_DOMAIN: return $_SESSION['softPhoneLinePortVal'];
        case ResourceNameBuilder::FIRST_NAME:       return $_SESSION["userInfo"]["firstName"];
        case ResourceNameBuilder::LAST_NAME:        return $_SESSION["userInfo"]["lastName"];
        case ResourceNameBuilder::DEV_NAME:         return isset($_SESSION['deviceNameVal']) && $_SESSION['deviceNameVal'] != "" ? $_SESSION['deviceNameVal'] : "";
        case ResourceNameBuilder::MAC_ADDR:         return isset($_SESSION["userInfo"]["macAddress"]) && $_SESSION["userInfo"]["macAddress"] != "" ? $_SESSION["userInfo"]["macAddress"] : "";
        case ResourceNameBuilder::ENT:              return strtolower(trim($_SESSION["sp"]));
        case ResourceNameBuilder::USR_ID:
            $name = isset($_SESSION["userId"]) ? $_SESSION["userId"] : "";
            if ($name == "") {
                return "";
            }
            $nameSplit = explode("@", $name);
            return $nameSplit[0];
            break;
        default:                                    return "";
    }
}

//foreach ($_POST["softPhoneListToAdd"] as $key => $value){

$_SESSION['softPhoneLinePortVal'] = $arrayToProcessChanges["linePortDomain"];
$_SESSION['deviceNameVal'] = $arrayToProcessDetails["deviceName"];
$_SESSION["softPhoneDeviceTypeVal"] = $arrayToProcessDetails["deviceType"];
$softPhoneArray = array();
$softPhoneArray["sp"] = $_SESSION["sp"];
$softPhoneArray["groupId"] = $_SESSION["groupId"];
$softPhoneArray["userId"] = $postUserId;
$softPhoneArray["deviceType"] = $arrayToProcessDetails["deviceType"];
$softPhoneArray["deviceName"] = $arrayToProcessDetails["deviceName"];

$counterPathAdded = false;
$counterPathModified = false;
$softPhoneArray["deviceAccessUserName"]="";

if(isset($arrayToProcessChanges["softPhoneDeviceAccUserName"]) && !empty($arrayToProcessChanges["softPhoneDeviceAccUserName"])){
    $softPhoneArray["deviceAccessUserName"] = $arrayToProcessChanges["softPhoneDeviceAccUserName"]."-softphone";
}
if(isset($arrayToProcessChanges["softPhoneDeviceAccUserPass"]) && !empty($arrayToProcessChanges["softPhoneDeviceAccUserPass"])){
    $softPhoneDeviceAccessPassword = $softPhoneArray["deviceAccessPassword"] = $arrayToProcessChanges["softPhoneDeviceAccUserPass"];
}

$payloadData = "account.notification._userEmailAddress=<express_email_value>
account1Sip.accountName=<express_account_name>
account1Sip.credentials.authorizationName=<express_authorizationName_value>
account1Sip.credentials.password=<express_password_value>
account1Sip.credentials.username=<express_username_value>
account1Sip.domain=<express_domain_value>
account1Sip.proxy=<express_proxy_value>";

//LinePort creation
$linePort = "";
$linePortBuilder1 = new LineportNameBuilder($softPhoneLinePortCriteria1,
    getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::DN),
    getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::EXT),
    getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::USR_CLID),
    getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::GRP_DN),
    getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
    $softPhoneLinePortCriteria2 == "");  // force fallback only if second criteria does not exist
    
    if (! $linePortBuilder1->validate()) {
        // TODO: Implement resolution when lineport input formula has errors
    }
    
    do {
        $attr = $linePortBuilder1->getRequiredAttributeKey();
        $linePortBuilder1->setRequiredAttribute($attr, getResourceForSoftPhoneLinePortCreation($attr));
    } while ($attr != "");
    
    $linePort = $linePortBuilder1->getName();
    
    if ($linePort == "") {
        // Create lineport name builder from the second formula. At this point we know that there is second
        // formula, because without second formula name would be forcefully resolved in the first formula.
        
        $linePortBuilder2 = new LineportNameBuilder($softPhoneLinePortCriteria2,
            getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::DN),
            getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::EXT),
            getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::USR_CLID),
            getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::GRP_DN),
            getResourceForSoftPhoneLinePortCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
            true);
        
        if (! $linePortBuilder2->validate()) {
            // TODO: Implement resolution when User ID input formula has errors
        }
        
        do {
            $attr = $linePortBuilder2->getRequiredAttributeKey();
            $linePortBuilder2->setRequiredAttribute($attr, getResourceForSoftPhoneLinePortCreation($attr));
        } while ($attr != "");
        
        $linePort = $linePortBuilder2->getName();
    }
    
    $softPhoneLinePort = str_replace(" ", "_", $linePort);
    
    $softPhoneArray["linePort"] = $softPhoneLinePort;

    if($arrayToProcessOriginal["softPhoneNameType"] == "counterPath"){
    require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
    $vdmobj = new VdmOperations();
    $tagListResp = $vdmobj->getDeviceCustomTags($_SESSION["sp"], $_SESSION["groupId"], $arrayToProcessDetails["deviceName"]);
    
    if(isset($arrayToProcessChanges["linePortDomain"]) || isset($arrayToProcessChanges["accountUserName"]) || isset($arrayToProcessChanges["softPhoneAccountEmailAddress"])){
        
        if(isset($arrayToProcessChanges["accountUserName"]) && !empty($arrayToProcessChanges["accountUserName"])){
            $deviceFileGet = false;
            
            $customTagsArrayDataAdd = array(
                "%groupName%" => $counterpathGroupName,
                "%CPUsername%" => $arrayToProcessChanges["accountUserName"],
                "%profileName%" => $arrayToProcessOriginal["accountProfile"],
                "%ctrpathuserEmailAddress%" => $arrayToProcessOriginal["softPhoneAccountEmailAddress"]
            );
            
            
            foreach ($customTagsArrayDataAdd as $tagKey => $tagVal){
                $delResp[] = $vdmobj->deleteDeviceCustomTag($sp, $groupId, $arrayToProcessDetails["deviceName"], $tagKey);
            }
            foreach ($customTagsArrayDataAdd as $addKey =>$addVal){
                $addResp[] = $vdmobj->addDeviceCustomTag($sp, $groupId, $arrayToProcessDetails["deviceName"], $addKey, $addVal);
            }
            rebuildAndResetDeviceConfig($arrayToProcessDetails["deviceName"], "rebuildAndReset");
            //sleep(10);
            
            $deviceObjSoftPhone = new DeviceOperations();
            $devUrl = $deviceObjSoftPhone->getDeviceFileUrl($arrayToProcessDetails["deviceName"]);
            
            if(isset($arrayToProcessDetails["deviceName"]) && $arrayToProcessDetails["deviceName"] != ""){
                $respVal = $objSoft->getDevicesByUsingUserId($postUserId);
                
                if(empty($respVal["Error"])){
                    foreach ($respVal["Success"] as $arrKey => $arrVal){
                        if($arrKey == $arrayToProcessDetails["deviceName"]){
                            $softPhoneUserName1 = $arrVal["userName"];
                            $softPhonePassword1 = $arrVal["password"];
                        }
                        
                    }
                }
                
            }
            
            $filePath = $devUrl["Success"];
            //preg_match("/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/", $filePath, $matches);
            //$findIPFromStr = $matches[0];
            
            $privateIp = $xspPrivateIP;
            //$privateIp      = "10.100.10.207";
            //$changeURL = str_replace($findIPFromStr, $privateIp, $filePath);
            
            //$var1 = "https://69.169.36.217:80/dms/Counterpath_Bria_-_Desktop/configuration.txt";
            $findIPFromStr = parse_url($filePath, PHP_URL_HOST);
            //$privateIp      = "10.100.10.207";
            $changeURL = str_replace($findIPFromStr, $privateIp, $filePath);
            //print_r($changeURL);
            
            //$valRes = curlgetFile($changeURL, $softPhoneUserName1, $softPhonePassword1);
            $valRes = getRecursiveData($changeURL, $softPhoneUserName1, $softPhonePassword1, $sleep = 0);
            //print_r($valRes);
            if($valRes["status"] == "200"){
                $info = parseConfigFile($valRes["result"]);
                $deviceFileGet = true;
                //print_r($info);
            }
            
            //$info = parse_ini_file('configuration.txt');
            
            $softPhoneArray["cpsArray"]["groupName"] = trim($info["account_groupname"]);
            $softPhoneArray["cpsArray"]["userName"] = $arrayToProcessChanges["accountUserName"];
            $softPhoneArray["cpsArray"]["profileName"] = trim($info["account_profile"]);
            $softPhoneArray["cpsArray"]["userEmailAddress"] = trim($info["account.userEmailAddress"]);
            $softPhoneArray["cpsArray"]["accountName"] = trim($info["account1Sip.accountName"]);
            $softPhoneArray["cpsArray"]["authorizationName"] = trim($info["account1Sip.credentials.authorizationName"]);
            $softPhoneArray["cpsArray"]["password"] = trim($info["account1Sip.credentials.password"]);
            $softPhoneArray["cpsArray"]["crndusername"] = trim($info["account1Sip.credentials.username"]);
            $softPhoneArray["cpsArray"]["domain"] = trim($info["account1Sip.domain"]);
            $softPhoneArray["cpsArray"]["proxy"] = trim($info["account1Sip.proxy"]);
           // print_r($softPhoneArray);
            $successMessage = false;
            
            
            
            if($deviceFileGet){
                $cpsResult = $cpsobj->counterPathAccountAdd($softPhoneArray, $payloadData);
            }else{
                $message .= "<li style='color:red;'>Unable to read configuration from Broadworks. Please contact support.</li>";
            }
            
            if($cpsResult["status"] == "200" && $deviceFileGet){
                $message .= "<li>".$softPhoneArray["cpsArray"]["userName"]." Counter Path Account Added Successfully.</li>";
                $successMessage = true;
            }else{
                $message .= "<li style='color:red;'>".$softPhoneArray["cpsArray"]["userName"]." Counter Path Account was attempted to be Add but failed.</li>";
            }
            
            /*if($cpsResult["status"] == "200" && $cpsResult["isexist"] == "true"){
                $cpsDelResult = $cpsobj->counterPathAccountDelete($softPhoneArray);
                
                if($cpsDelResult != "true"){
                    $message .= "<li style='color:red;'>".$softPhoneArray["cpsArray"]["userName"]." Counter Path Account was attempted to be Delete but failed.</li>";
                    
                }else{
                    $cpsAddResult = $cpsobj->counterPathAccountAdd($softPhoneArray, $payloadData);
                    if($cpsAddResult["status"] == "200" && $cpsAddResult["isexist"] == "false"){
                        $message .= "<li>".$softPhoneArray["cpsArray"]["userName"]." Counter Path Account Added Successfully.</li>";
                        $successMessage = true;
                    }
                    
                }
            }*/
            if($successMessage){
                /*$customTagsArrayDataAdd = array(
                    "%groupName%" => $softPhoneArray["cpsArray"]["groupName"],
                    "%CPUsername%" => $softPhoneArray["cpsArray"]["userName"],
                    "%profileName%" => $softPhoneArray["cpsArray"]["profileName"],
                    "%ctrpathuserEmailAddress%" => $softPhoneArray["cpsArray"]["userEmailAddress"]
                );*/
                
                $softPhoneArrayDelOld = array();
                $softPhoneArrayDelOld["cpsArray"]["userName"] = $arrayToProcessChanges["oldAccountUserName"];
                $cpsDelExistResult = $cpsobj->counterPathAccountDelete($softPhoneArrayDelOld);
                if($cpsDelExistResult == "true"){
                    $counterPathAdded = true;
                    /*$sp = $_SESSION["sp"];
                    $groupId = $_SESSION["groupId"];
                    foreach ($customTagsArrayDataAdd as $tagKey => $tagVal){
                        $delResp[] = $vdmobj->deleteDeviceCustomTag($sp, $groupId, $arrayToProcessDetails["deviceName"], $tagKey);
                    }
                    foreach ($customTagsArrayDataAdd as $addKey =>$addVal){
                        $addResp[] = $vdmobj->addDeviceCustomTag($sp, $groupId, $arrayToProcessDetails["deviceName"], $addKey, $addVal);
                    }*/
                    
                    $message .= "<li>".$softPhoneArrayDelOld["cpsArray"]["userName"]." Counter Path Account Deleted Successfully.</li>";
                }else{
                    $message .= "<li style='color:red;'>".$softPhoneArrayDelOld["cpsArray"]["userName"]." Counter Path Account was attempted to be Delete but failed.</li>";
                    
                }
            }
        }else{
            $deviceObjSoftPhone = new DeviceOperations();
            $devUrl = $deviceObjSoftPhone->getDeviceFileUrl($arrayToProcessDetails["deviceName"]);
            
            if(isset($arrayToProcessDetails["deviceName"]) && $arrayToProcessDetails["deviceName"] != ""){
                $respVal = $objSoft->getDevicesByUsingUserId($postUserId);
                
                if(empty($respVal["Error"])){
                    foreach ($respVal["Success"] as $arrKey => $arrVal){
                        if($arrKey == $arrayToProcessDetails["deviceName"]){
                            $softPhoneUserName1 = $arrVal["userName"];
                            $softPhonePassword1 = $arrVal["password"];
                        }
                        
                    }
                }
                
            }
            
            $filePath = $devUrl["Success"];
            //preg_match("/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/", $filePath, $matches);
            //$findIPFromStr = $matches[0];
            
            $privateIp = $xspPrivateIP;
            //$privateIp      = "10.100.10.207";
            //$changeURL = str_replace($findIPFromStr, $privateIp, $filePath);
            
            //$var1 = "https://69.169.36.217:80/dms/Counterpath_Bria_-_Desktop/configuration.txt";
            $findIPFromStr = parse_url($filePath, PHP_URL_HOST);
            //$privateIp      = "10.100.10.207";
            $changeURL = str_replace($findIPFromStr, $privateIp, $filePath);
            //print_r($changeURL);
            
            //$valRes = curlgetFile($changeURL, $softPhoneUserName1, $softPhonePassword1);
            $valRes = getRecursiveData($changeURL, $softPhoneUserName1, $softPhonePassword1, $sleep = 0);
            //print_r($valRes);
            if($valRes["status"] == "200"){
                $info = parseConfigFile($valRes["result"]);
                
                //print_r($info);
            }
            //$info = parse_ini_file('configuration.txt');
            $softPhoneArray["cpsArray"]["groupName"] = trim($info["account_groupname"]);
            $softPhoneArray["cpsArray"]["userName"] = trim($info["account_username"]);
            $softPhoneArray["cpsArray"]["profileName"] = trim($info["account_profile"]);
            $softPhoneArray["cpsArray"]["userEmailAddress"] = $arrayToProcessOriginal["softPhoneAccountEmailAddress"];
            $softPhoneArray["cpsArray"]["accountName"] = trim($info["account1Sip.accountName"]);
            $softPhoneArray["cpsArray"]["authorizationName"] = trim($info["account1Sip.credentials.authorizationName"]);
            $softPhoneArray["cpsArray"]["password"] = trim($info["account1Sip.credentials.password"]);
            $softPhoneArray["cpsArray"]["crndusername"] = trim($info["account1Sip.credentials.username"]);
            $softPhoneArray["cpsArray"]["domain"] = trim($info["account1Sip.domain"]);
            $softPhoneArray["cpsArray"]["proxy"] = trim($info["account1Sip.proxy"]);
            
            $modArray = array();
            
            $cpsModResult = $cpsobj->counterPathAccountModify($softPhoneArray, $payloadData);
            if($cpsModResult == "true"){
                $counterPathModified = true;
                //echo "1- ";print_r($arrayToProcessChanges);
                foreach($arrayToProcessChanges as $changeKey => $changeVal){
                    /*if($changeKey == "linePortDomain"){
                        $modArray["%ctrpathdomain%"] = $changeVal;
                    }*/
                    if($changeKey == "softPhoneAccountEmailAddress"){
                        $modArray["%ctrpathuserEmailAddress%"] = $changeVal;
                    }
                    /*if($changeKey == "softPhoneDeviceAccUserPass"){
                        $modArray["%ctrpathpassword%"] = $changeVal;
                    }*/
               }
                //echo "2- ";print_r($modArray);
                foreach ($modArray as $modKey => $modVal){
                    $processArr["spId"] = $_SESSION["sp"];
                    $processArr["groupId"] = $_SESSION["groupId"];
                    $processArr["deviceName"] = $arrayToProcessDetails["deviceName"];
                    $processArr["tagName"] = $modKey;
                    $processArr["tagValue"] = $modVal;
                    $modifyresp[] = $vdmobj->modfiyCustomTags($processArr);
                }
                rebuildAndResetDeviceConfig($arrayToProcessDetails["deviceName"], "rebuildAndReset");
                //echo "3- ";print_r($modifyresp);
                $message .= "<li>".$arrayToProcessOriginal["accountUserName"]." Counter Path Account Modified Successfully.</li>";
            }
            
            
        }
        /*if(isset($arrayToProcessChanges["softPhoneAccountEmailAddress"]) && !empty($arrayToProcessChanges["softPhoneAccountEmailAddress"])){
            $userObj = new UserOperations();
            $userArray = array();
            $userArray["userId"] = $postUserId;
            $userArray["emailAddress"] = $arrayToProcessChanges["softPhoneAccountEmailAddress"];
            $userResp = $userObj->modifyUserDetails($userArray);
            //print_r($userResp);
        }*/
       
        
        
    }
}

if($arrayToProcessChanges["softPhoneDeviceAccUserName"] || $arrayToProcessChanges["softPhoneDeviceAccUserPass"]){
    $deviceObjSoftPhone = new DeviceOperations();
    $arrData["sp"] = $_SESSION["sp"];
    $arrData["groupId"] = $_SESSION["groupId"];
    $arrData["modifiedDevice"] = $arrayToProcessDetails["deviceName"];
    $arrData["deviceAccessUserName"] = $softPhoneArray["deviceAccessUserName"];
    $arrData["deviceAccessPassword"] = $arrayToProcessChanges["softPhoneDeviceAccUserPass"];
    
    $devModResp = $deviceObjSoftPhone->deviceModifyRequest($arrData);
    if(empty($devModResp["Error"])){
        $message .= "<li>".$arrayToProcessDetails["deviceName"]." Device Access UserName and Password Modified Successfully.</li>";
    }
    
}

//if(($arrayToProcessChanges["rebuildSoftPhoneFiles"] == "true" || $arrayToProcessChanges["resetSoftPhone"] == "true") && $counterPathAdded || $counterPathModified){
if($arrayToProcessChanges["rebuildSoftPhoneFiles"] == "true" || $arrayToProcessChanges["resetSoftPhone"] == "true"){
    require_once("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig.php");
    rebuildAndResetDeviceConfig($arrayToProcessDetails["deviceName"], "rebuildAndReset");
    $message .= "<li>".$arrayToProcessDetails["deviceName"]." Device has been Rebuild/Reset.</li>";
}

if($counterPathAdded){
    $date = date("Y-m-d H:i:s");
    $query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
    $query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', 'Counter Path Account Added', '" . $_SESSION["groupId"] . "', '".$arrayToProcessDetails["deviceName"]."')";
    $sth = $expProvDB->expressProvLogsDb->query($query);
    
    $lastId = $expProvDB->expressProvLogsDb->lastInsertId();
    
    $insert = "INSERT into counterPathAddChanges (id, counterPathAcctGroupname, counterPathAcctProfilename, counterPathAcctUsername, counterPathAcctEmailAddr, counterPathAcctAuthName, counterPathAcctSipDomain, counterPathAcctSipProxy, counterPathAcctDeviceType, counterPathAcctDeviceName, counterPathAcctLinePortDomain)";
    $insert .= " VALUES ('" . $lastId . "','" .
        $counterpathGroupName . "', '" .
        $softPhoneArray["cpsArray"]["profileName"] . "', '" .
        $softPhoneArray["cpsArray"]["userName"] . "', '" .
        $softPhoneArray["cpsArray"]["userEmailAddress"] . "', '" .
        $softPhoneArray["cpsArray"]["authorizationName"] . "', '" .
        $softPhoneArray["cpsArray"]["crndusername"] . "', '" .
        $softPhoneArray["cpsArray"]["proxy"] . "', '" .
        $softPhoneArray["deviceType"] . "', '" .
        $softPhoneArray["cpsArray"]["domain"] . "', '" . "')";
        //print_r($insert);
    $fiq = $expProvDB->expressProvLogsDb->query($insert);
}

if($counterPathModified){
    $date = date("Y-m-d H:i:s");
    $query = "INSERT into changeLog (userName, date, module, enterpriseId, groupId, entityName)";
    $query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', 'Soft Phone Modify', '" . $_SESSION["sp"] . "', '" . $_SESSION["groupId"] . "', '" . $_SESSION["userInfo"]["lastName"] . ", " . $_SESSION["userInfo"]["firstName"] . "')";
    $sth = $expProvDB->expressProvLogsDb->query($query);
    
    $lastId = $expProvDB->expressProvLogsDb->lastInsertId();
    
    foreach ($modArray as $changeKey => $changeVal){
        $query = "INSERT into softPhoneModChanges (id, serviceId, entityName, field, oldValue, newValue) ";
        $query .= "VALUES ('" . $lastId . "', '" . $userId . "', '" . $arrayToProcessDetails["deviceName"] . "', ";
        $query .= " 'Custom Tag', '".$tagListResp["Success"][$changeKey][0]."', '".$changeVal. "')";
        //print_r($query);
        $sth = $expProvDB->expressProvLogsDb->query($query);
    }
}

function curlgetFile($URL, $userName, $password){
    //$URL = 'http://10.100.10.207:80/dms/Counterpath_Bria_with_Broadworks/configuration.txt';
    //$userName = "9810090020";
    //$password = "Admin123";
    $arr = array();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$URL);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_USERPWD, "$userName:$password");
    $result=curl_exec ($ch);
    //print_r($result);
    $arr["result"]=$result;
    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    $arr["status"]=$status_code;
    ///print_r($status_code);
    curl_close ($ch);
    return $arr;
}

function parseConfigFile($result){
    $tmpResultArr = explode("\n", $result);
    unset($tmpResultArr[0]);
    unset($tmpResultArr[1]);
    unset($tmpResultArr[2]);
    $customizeResultArr = array();
    
    $count = 0;
    
    foreach($tmpResultArr as $ky => $vl){
        if($vl!=""){
            $tmpArr = explode("=", $vl);
            $customizeResultArr[$tmpArr[0]]  = str_replace('"', "", $tmpArr[1]);
        }
        $count = $count + 1;
    }
    return $customizeResultArr;
}

function getRecursiveData ($changeURL, $softPhoneUserName1, $softPhonePassword1, $sleep){
    //$sleep++;
    //echo "Sleep is". $sleep."<br />";
    $valRes1 = curlgetFile($changeURL, $softPhoneUserName1, $softPhonePassword1);
    //print_r($valRes1);
    if($valRes1["result"] == "") {
        sleep(1);
        if($sleep <= 9) {
            //echo "2229";
            $sleep++;
            $valRes1 = getRecursiveData($changeURL, $softPhoneUserName1, $softPhonePassword1, $sleep);
        }
        
    }
    //print_r($valRes1);
    return $valRes1;
}

?>