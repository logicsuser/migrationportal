<script type="text/javascript">
$(function() {
	$("#showSoftPhoneFildsChk").hide();
	$("#selectSoftPhoneDeviceType").change(function(){
	  var softPhoneSelectedval = $("#selectSoftPhoneDeviceType").val();
	  if(softPhoneSelectedval){
		  var softPType = $("#selectSoftPhoneDeviceType").find(':selected').data('type');
		  $("#showSoftPhoneFildsChk").show();
		  if(softPType == "counterPath"){
      		$(".genericHide").show();
      		$("#softPhoneNameType").val(softPType);
          }else{
          	$(".genericHide").hide();
          	$("#softPhoneNameType").val(softPType);
          }
	  }else{
		  $("#showSoftPhoneFildsChk").hide();
		  $("#softPhoneNameType").val("");
	  }
	  
});
});
</script>
<?php
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once ("/var/www/lib/broadsoft/adminPortal/getGroupDomains.php");
//print_r($groupDomains);
require_once ("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/html/Express/util/getAuthorizeDeviceType.php");

require_once ("/var/www/lib/broadsoft/adminPortal/getDevices.php");
require_once ("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");

/*$customDevices = array ();
if ($useCustomDeviceList == "true") {
    $query = "select deviceName from customDeviceList";
    $result = $db->query ( $query );
    
    while ( $row = $result->fetch () ) {
        $customDevices [] = $row ["deviceName"];
    }
}

$sipGatewayLookup = new DBLookup ( $db, "devices", "deviceName", "sipGateway" );
$sipSoftPhoneLookup = new DBLookup ( $db, "devices", "deviceName", "softPhoneType" );
$authDeviceTypeInstance = new GetAuthorizeDeviceType();
$deviceTypesSoftPhones = $authDeviceTypeInstance->getSoftPhoneDeviceType($nonObsoleteDevices, $sipGatewayLookup, $customDevices, $sipSoftPhoneLookup, $useCustomDeviceList);
*/
require_once ("/var/www/html/Express/util/getAuthorizeDeviceTypes.php");
if(isset($_REQUEST["userId"]) && $_REQUEST["userId"] != ""){
    $userObject  =   new UserOperations();
    $userDeviceList = $userObject->getDeviceListByUserId($_REQUEST["userId"]);
    $devArray = array();
    if($userDeviceList["Error"] ==""){
        if(isset($userDeviceList["Success"]["deviceList"]) && !empty($userDeviceList["Success"]["deviceList"])){
            foreach ($userDeviceList["Success"]["deviceList"] as $dKey => $dValue){
                $devArray["deviceType"][] = $dValue["deviceType"];
            }
               
            foreach ($devArray["deviceType"] as $devKey => $devVal){
                foreach ($deviceTypesSoftPhones as $softKey => $softVal){
                    if($softVal["softDeviceType"] == $devVal){
                        unset($deviceTypesSoftPhones[$softKey]);
                    }
                }
            }
        }
    }
}

function softPhoneGroupDomain($securityDomainPattern) {
    
    global $groupDefaultDomain, $groupDomains;
    global $staticProxyDomain, $softPhoneLinePortCriteria1;
    
    $softPhoneProxyDomainType = "";
    //echo $criteriaVar;
    if (strpos($softPhoneLinePortCriteria1, "groupProxyDomain") === false) {
        $softPhoneProxyDomainType = "Default";
    }else{
        $softPhoneProxyDomainType = "Static";
    }
    //print_r($softPhoneProxyDomainType);
    $selectedDomain = "";
    switch ($softPhoneProxyDomainType) {
        case "Static" :
            $selectedDomain = $staticProxyDomain;
            break;
        case "Default" :
            $selectedDomain = $groupDefaultDomain;
            break;
    }
    
    if ($selectedDomain == "") {
        $selectedDomain = $groupDefaultDomain;
    }
    
    if(count($groupDomains) > 0){
        foreach ($groupDomains as $key => $value){
            if (strpos($value, $securityDomainPattern) !== false) {
                unset($groupDomains[$key]);
            }
        }
    }
    $groupDomains = array_values($groupDomains);
    
    for($i = 0; $i < count ( $groupDomains ); $i ++) {
        echo "<option value=\"" . $groupDomains [$i] . "\"";
        if ($groupDomains [$i] == $selectedDomain) {
            echo " selected";
        }
        echo ">" . $groupDomains [$i];
        if ($groupDomains [$i] == $groupDefaultDomain) {
            echo " (default domain)";
        }
        echo "</option>";
    }
}

//echo "12233434";print_r(softPhoneGroupDomain($securityDomainPattern));
?>

<div id ="modAddSoftPhoneHtml">
	<form name ="softPhoneAddFormData" id ="softPhoneAddFormData" class="fcorn-registerTwo">
	
		<div id="hideSoftPhoneDeviceType">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="labelText" for="language">Soft Phone Device Type: <span class="required">*</span></label>
						
							<div class="dropdown-wrap">
								<select name="softPhoneDeviceType" id="selectSoftPhoneDeviceType" class="fullWidthHundred">
									<option value = ""></option>
									<?php 
									foreach ($deviceTypesSoftPhones as $softPhoneKey => $softPhoneVal){
										echo "<option value = '".$softPhoneVal["softDeviceType"]."' data-type='".$softPhoneVal["softPhoneType"]."'>".$softPhoneVal["softDeviceType"]."</option>";
									}
    						?>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
		
		<div id="showSoftPhoneFildsChk">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="language">Soft Phone Line/Port Domain: <span class="required">*</span></label>
					
						<div class="dropdown-wrap">
    						<select name="softPhoneDeviceTypeLinePort" id="softPhoneDeviceTypeLinePort" class="modalDropDownClass">
    							<?php softPhoneGroupDomain($securityDomainPattern); ?>
    						</select>
						</div>
					</div>
				</div>
			
			
				<div id="hideSoftPhoneUserNamePassword">
					<div class="col-md-6">
    					<div class="form-group">
    						<label class="labelText" for="language">Soft Phone Device Access UserName:</label>
    						<input type="text" name="softPhoneDeviceAccUserName" id="softPhoneDeviceAccUserName" size="40" maxlength="60">
    					</div>
					</div><!-- after  -->
				</div>
			</div><!--row-->
			
			<div class="row">
				
				<div id="hideSoftPhoneUserNamePassword">
					<div class="col-md-6">
    					<div class="form-group">
    						<label class="labelText" for="language">Soft Phone Device Access Password:</label>
    						<input type="password" name="softPhoneDeviceAccUserPass" id="softPhoneDeviceAccUserPass" size="40" maxlength="60">
    					</div>
					</div><!-- after  -->
				</div>
			
			
				<div class="col-md-6 genericHide">
					<div class="form-group">
						<label class="labelText" for="language">Account UserName: <span class="required">*</span></label>
						<input type="text" name="accountUserName" id="accountUserName" size="40" maxlength="60">
					</div>
				</div> 
			
			</div><!--second row closed-->
			
			
			<div class="row">
				 
            
            <!-- Account ProfileName -->
                <div class="col-md-6 genericHide form-group">
               		<div class="from-group">
               			<label class="labelText">Account Profile:<span class="required">*</span></label>
               			 <div class="dropdown-wrap">
               			 	<select name="accountProfile" id="accountProfile" class="modalDropDownClass">
    							<option value="GOLD">GOLD</option>
    	            		</select>
               			 </div>
               		</div>
               	</div>
            <!-- end account profileName -->
		 
			<!-- Account Account Email Address -->
    				
                <div class="col-md-6 genericHide form-group">
               		<div class="from-group">
               			<label class="labelText">Account Email Address:</label>
               			<input type="text" name="softPhoneAccountEmailAddress"
        					id="softPhoneAccountEmailAddress" value="" class="modalInputClass">
               		</div>
               	</div>
            <!-- end account Account Email Address --> 
            </div>
		
			<div class="">
				<input type="checkbox" name="rebuildSoftPhoneFiles" id="rebuildSoftPhoneFiles" value="true">
				<label for="rebuildSoftPhoneFiles"><span></span></label>
				<label class="labelText">Rebuild Device Files</label>
			</div>
					
    		 <div class="">
				<input type="checkbox" name="resetSoftPhone" id="resetSoftPhone" value="true">
				<label for="resetSoftPhone"><span></span></label>
				<label class="labelText">Reset Device Files</label>
    		</div>
    		
	 </div><!-- end soft phone flids chk -->
		<input type="hidden" name="softPhoneNameType" id="softPhoneNameType" value="">
	</form>
</div> 