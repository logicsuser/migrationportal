<?php 

function getLinePortName($softPhoneLinePortCriteria1, $softPhoneLinePortCriteria2){
    //LinePort creation
    require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/LineportNameBuilder.php");
    $linePort = "";
    $linePortBuilder1 = new LineportNameBuilder($softPhoneLinePortCriteria1,
        getResourceForSoftPhoneLinePortCreationUtil(ResourceNameBuilder::DN),
        getResourceForSoftPhoneLinePortCreationUtil(ResourceNameBuilder::EXT),
        getResourceForSoftPhoneLinePortCreationUtil(ResourceNameBuilder::USR_CLID),
        getResourceForSoftPhoneLinePortCreationUtil(ResourceNameBuilder::GRP_DN),
        getResourceForSoftPhoneLinePortCreationUtil(ResourceNameBuilder::SYS_DFLT_DOMAIN),
        $softPhoneLinePortCriteria2 == "");  // force fallback only if second criteria does not exist
        
        if (! $linePortBuilder1->validate()) {
            // TODO: Implement resolution when lineport input formula has errors
        }
        
        do {
            $attr = $linePortBuilder1->getRequiredAttributeKey();
            $linePortBuilder1->setRequiredAttribute($attr, getResourceForSoftPhoneLinePortCreationUtil($attr));
        } while ($attr != "");
        
        $linePort = $linePortBuilder1->getName();
        
        if ($linePort == "") {
            // Create lineport name builder from the second formula. At this point we know that there is second
            // formula, because without second formula name would be forcefully resolved in the first formula.
            
            $linePortBuilder2 = new LineportNameBuilder($softPhoneLinePortCriteria2,
                getResourceForSoftPhoneLinePortCreationUtil(ResourceNameBuilder::DN),
                getResourceForSoftPhoneLinePortCreationUtil(ResourceNameBuilder::EXT),
                getResourceForSoftPhoneLinePortCreationUtil(ResourceNameBuilder::USR_CLID),
                getResourceForSoftPhoneLinePortCreationUtil(ResourceNameBuilder::GRP_DN),
                getResourceForSoftPhoneLinePortCreationUtil(ResourceNameBuilder::SYS_DFLT_DOMAIN),
                true);
            
            if (! $linePortBuilder2->validate()) {
                // TODO: Implement resolution when User ID input formula has errors
            }
            
            do {
                $attr = $linePortBuilder2->getRequiredAttributeKey();
                $linePortBuilder2->setRequiredAttribute($attr, getResourceForSoftPhoneLinePortCreationUtil($attr));
            } while ($attr != "");
            
            $linePort = $linePortBuilder2->getName();
        }
        
        $softPhoneLinePort = str_replace(" ", "_", $linePort);
        
        return $softPhoneLinePort;
}

function getResourceForSoftPhoneLinePortCreationUtil($attr) {
    
    switch ($attr) {
        case ResourceNameBuilder::DN:               return $_SESSION["userInfo"]["phoneNumber"] != "" ? $_SESSION["userInfo"]["phoneNumber"] : "";
        case ResourceNameBuilder::EXT:              return $_SESSION["userInfo"]["extension"];
        case ResourceNameBuilder::USR_CLID:         return $_SESSION["userInfo"]["callingLineIdPhoneNumber"];
        case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
        case ResourceNameBuilder::GRP_ID:           return str_replace(" ", "_", trim($_SESSION["groupId"]));
        case ResourceNameBuilder::SYS_DFLT_DOMAIN:  return $_SESSION["systemDefaultDomain"];
        case ResourceNameBuilder::DEFAULT_DOMAIN:   return $_SESSION["defaultDomain"];
        case ResourceNameBuilder::GRP_PROXY_DOMAIN: return $_SESSION['softPhoneLinePortVal'];
        case ResourceNameBuilder::FIRST_NAME:       return $_SESSION["userInfo"]["firstName"];
        case ResourceNameBuilder::LAST_NAME:        return $_SESSION["userInfo"]["lastName"];
        case ResourceNameBuilder::DEV_NAME:         return isset($_SESSION['softPhoneDeviceNameMod']) && $_SESSION['softPhoneDeviceNameMod'] != "" ? $_SESSION['softPhoneDeviceNameMod'] : "";
        case ResourceNameBuilder::MAC_ADDR:         return isset($_SESSION["userInfo"]["macAddress"]) && $_SESSION["userInfo"]["macAddress"] != "" ? $_SESSION["userInfo"]["macAddress"] : "";
        case ResourceNameBuilder::ENT:              return strtolower(trim($_SESSION["sp"]));
        case ResourceNameBuilder::USR_ID:
            $name = isset($_SESSION["userId"]) ? $_SESSION["userId"] : "";
            if ($name == "") {
                return "";
            }
            $nameSplit = explode("@", $name);
            return $nameSplit[0];
            break;
        default:                                    return "";
    }
}
?>