var softPhonePos ="";
	$("#softPhoneDelBtn").hide() ;
	$("#showSoftPhoneFildsChk").hide();
	$("#modSoftPhoneAdd").click(function(){
		softPhonePos ="Create" ;
	    //var softPhone = $("#modAddSoftPhoneHtml").html();
	    var userId = $("#userId").val();
	    
	    $.ajax({
            url: "userMod/softPhone/addSoftPhoneDialog.php",
            type:'GET',
            data:{"userId":userId},
            success: function(result)
            {
            	$("#softPhoneDialog").html(result);
        	    $("#softPhoneDialog").dialog("open");
        	    var email = $("#emailAddress").val();
    	    	$("#softPhoneAccountEmailAddress").val(email);
            }
        });
	 });

var isModify = "";
	/* Delete button event on click*/

	//$("tr#softPhoneTable td input.softPhonedelCheckbox").click(function (){ 
	/*$(document).on("click", "tr#softPhoneTable td input.softPhonedelCheckbox", function() {
		$("#softPhoneDelBtn").show() ;
		if($('input.softPhonedelCheckbox:checked').length >0){
			softPhonePos = "Delete" ;
			 $("#softPhoneDelBtn").show() ;
			}else{
				$("#softPhoneDelBtn").hide() ;
	     }
	});*/
	/*soft phone delete button on click */

	/*update softPhone */

	/* check validation update */
var isCheckBoxIsClicked_CustomTag = false;
function checkBoxDelBtnShowNavo(status) {
	isCheckBoxIsClicked_CustomTag = status;
}

	$(document).on("click", "tr#softPhoneTable", function(event) {
		softPhonePos = "Update";
		$("#softPhoneDialog").html("");
		$("#selectSoftPhoneDeviceType").prop("disabled", false);
		//$("#softPhoneDeviceTypeLinePort").prop("disabled", false);
		if (!isCheckBoxIsClicked_CustomTag) {	
		//if (event.target.type !== 'checkbox') {
			var port  				= $(this).closest('tr#softPhoneTable').find('.softPhonedelCheckbox').data("port");
			var updateDeviceName   = $(this).closest('tr#softPhoneTable').find('.softPhonedelCheckbox').data("name");
			var updateDeviceType   = $(this).closest('tr#softPhoneTable').find('.softPhonedelCheckbox').data("device");
			var accountUserName   = $(this).closest('tr#softPhoneTable').find('.softPhonedelCheckbox').data("accountusername");
			var accountEmail   = $(this).closest('tr#softPhoneTable').find('.softPhonedelCheckbox').data("accountemail");
			var linePort   = $(this).closest('tr#softPhoneTable').find('.softPhonedelCheckbox').data("lineport");
			var strArray = linePort.split("@");
			var domain   = $(this).closest('tr#softPhoneTable').find('.softPhonedelCheckbox').data("domain");
			$.ajax({
	            url: "userMod/softPhone/addSoftPhoneDialog.php",
	            type:'GET',
	            success: function(result)
	            {
	            	$("#softPhoneDialog").html(result);
	        	    $("#softPhoneDialog").dialog("open");
	        	    if (domain == "undefined") {
	        	    	
	        	    }else{
	        	    	$("#softPhoneDeviceTypeLinePort").val(domain);
	        	    }
	        	    if (accountEmail == "undefined" || accountEmail == "") {
	        	    	var email = $("#emailAddress").val();
	        	    	$("#softPhoneAccountEmailAddress").val(email);
	        	    }else{
	        	    	$("#softPhoneAccountEmailAddress").val(accountEmail);
	        	    }
	        	    $("#selectSoftPhoneDeviceType").val(updateDeviceType);
	        	    $("#selectSoftPhoneDeviceType").trigger("change");
	        	    $("#accountUserName").val(accountUserName);
	        	    
	        	    $("#selectSoftPhoneDeviceType").prop("disabled", true);
	        	    $("#softPhoneDeviceTypeLinePort").prop("disabled", true);
	        	    
	        	    //$("#softPhoneDeviceTypeLinePort").prop("disabled", true);
	            }
	        });
			
			/*var port  				= $(this).closest('tr#softPhoneTable').find('.softPhonedelCheckbox').data("port");
			var updateDeviceName   = $(this).closest('tr#softPhoneTable').find('.softPhonedelCheckbox').data("name");
			var updateDeviceType   = $(this).closest('tr#softPhoneTable').find('.softPhonedelCheckbox').data("device");
			$("#softPhonePort").attr('value',port);
			$("#softPhoneDeviceType").attr('value',updateDeviceType);
			$("#softPhoneDeviceName").attr('value',updateDeviceName);
			
			$("#hiddenSoftPhonePort").attr('value',port);
			$("#hiddenSoftPhoneDeviceType").attr('value',updateDeviceType);
			$("#hiddenSoftPhoneDeviceName").attr('value',updateDeviceName);
			
			var updateData 	=	$("#updateSoftPhoneDialog").html() ;
			$("#softPhoneDialog").html(updateData) ;
			$("#softPhoneDialog").dialog("open");*/
		 }
	});

	/*end update validation */
	/*end soft phone */

	/*end Device Configuration Custom Tags dialog*/

	/*get save custom tag records */
	var successResonseGetSoftPhoneList = function (data){
		//alert("success"+data) ;
	//	var tableCls   = "";
	            var tableData  = "";
	            var deviceType = "";
	            var result     = JSON.parse(data);
	            var deviceList = result.deviceList;
	            var deviceTypeClass = "SortingClass";
	            var newClassForSorting = "";            
	            delete result.deviceList; //Removing last element mean deviceType from array 
	            tableCls = "softPhoneTableDiv";
	            if(deviceList!=undefined){ 
	                    tableData         += "<table border='1' class='"+tableCls+" tableAlignMentDesign scroll tablesorter tagTableHeight' id='softPhoneTable'>";
	                    tableData         += "<thead> ";
	                    tableData         += "<tr>  ";

	                    tableData         += "<th id='disableTh' class='header'>Delete</th>";
	                    tableData         += "<th class='header thsmall' style='height: auto !important;'>Line/Port</th>";
	                    tableData         += "<th class='header thsmall' style='height: auto !important;'>Device Name</th>";
	                    tableData         += "<th class='header thsmall' style='height: auto !important;'>Device Type</th>";
	                    tableData         += "</tr>";

	                    tableData         += "</thead>";
	                    tableData         += "<tbody id='customTagTable'>"; 
	                    //var pos = 1;
	                    $.each(deviceList, function(key, value){  
	                         var port = value.port;
	                         var deviceName = value.deviceName;  
	                         var deviceType = value.deviceType;  
	                         var linePort = value.linePort;
	                         var accountUserName = value.accountUserName;
	                         var accountEmail = value.accountEmail;
	                         var domain = value.domain;
	                         var softPhoneType = value.softPhoneType;
	                         tableData += "<tr class='tableTrAlignment' style='' id='softPhoneTable'>";
	                         tableData += "<td class='deleteCheckboxtd thSno header'><input name='softPhoneDeviceName[]' value='"+deviceName+","+linePort+","+deviceType+","+softPhoneType+"' type='checkbox' style='width:17px' class='softPhonedelCheckbox' id='" +deviceName+ "' data-port='" +port+ "' data-name='" +deviceName+ "' data-device='" +deviceType+ "' data-accountusername = '"+accountUserName+"' data-accountemail = '"+accountEmail+"' data-lineport = '"+linePort+"' data-domain='"+domain+"'><label for='" +deviceName+ "'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label></td>";
	                         tableData += "<td class='header thsmall'>"+ linePort + "</td>";
	                         tableData += "<td class='header thsmall'>"+ deviceName + "</td>";
	                         tableData += "<td class='header thsmall'>"+ deviceType + "</td>";

	                         tableData += "</tr>"; 
	                         //pos++;
	                    })
	                    tableData         += "</tbody>";
	                    tableData         += "</table>";
	                    
	                    $("#softPhoneTable").html(tableData);
	                    $("#softPhoneTable").tablesorter();
	                    
	                   // $("."+tableCls).tablesorter();
	                    
	            }else{  
	            	
	            	
	            	
	                    $("#softPhoneTable").html("<div class='col-md-12'><div class='form-group' style='text-align: center;'><b style='font-weight:bold;'>--- No SoftPhone Device found ---</b></div></div>");
	            }
	            
	            $("#customConfigTagDeviceNameDiv").show();
	            $("#deviceCustomTagCongLoader").hide() ;
	};

	var errorSoftPhoneResponseMsg = function (error){	
		//alert("error - "+error);
	        alert('Please wait a while some data loading is in process');
	};



	var getSoftPhoneList = function (){
	$("#customConfigTagDeviceNameDiv").hide();
	$("#deviceCustomTagCongLoader").show() ;
        $.ajax({
                method:"POST",
                  url:"userMod/softPhone/softPhoneLogic.php",
                  data:{"funcAction":"showSofPhoneList"},
         }).then(successResonseGetSoftPhoneList,errorSoftPhoneResponseMsg);
	};
	
	var getSoftPhoneListAdd = function (){
		var softPhoneType = $("#selectSoftPhoneDeviceType").find(':selected').data('type');
		var dataToSend = $("form#softPhoneAddFormData").serializeArray();
		$.ajax({
			type: "POST",
			url:"userMod/softPhone/softPhoneLogic.php",
			data:{"funcAction":"softphoneAdd", "data":dataToSend, "softPhoneType" : softPhoneType},
			success: function(result) {
				successResonseGetSoftPhoneList(result);
				$("#updateSoftPhoneDialog").dialog("close");
			}
		});
	        /*$.ajax({
	                type:"POST",
	                url:"userMod/softPhone/softPhoneLogic.php",
	                data:{"funcAction":"softphoneAdd", "data":dataToSend},
	         }).then(successResonseGetSoftPhoneList,errorSoftPhoneResponseMsg);*/
	};
	
$(function() {
	
	getSoftPhoneList();
	 /* dialog box for 'Add/Modify/Delete Device Configuration Custom Tags' */  
		  $("#softPhoneDialog").dialog({
	          autoOpen: false,
	          width: 800,
	          modal: true,
	          title: "Soft Phone Dialog",
	          position: { my: "top", at: "top" },
	          resizable: false,
	          closeOnEscape: false,
	          buttons: {
	        	  Update: function() {
	        		  $("html, body").animate({ scrollTop: 0 }, 600);
	        		  $("#selectSoftPhoneDeviceType").prop("disabled", false);
	        		  $("#softPhoneDeviceTypeLinePort").prop("disabled", false);
	        		  
	            	  var dataToSend = $("form#softPhoneAddFormData").serializeArray();
	                  $.ajax({
	                      type: "POST",
	                      url: "userMod/softPhone/checkDataUpdateSoftPhone.php",
	                      data: dataToSend,
	                      success: function(result)
	                      {                 
	                          //$("#devicecustomTagDialog").dialog("open");
	                          //$("#softPhoneDialog").dialog("close");
	                          result = result.trim();                      	
	                          if (result.slice(0, 1) == 1){
	                              $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
	                          }else{
	                              $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
	                          }
	                          isModify = "modify";
	                          $("#updateSoftPhoneDialog").html(result.slice(1));
	                          $("#softPhoneDialog").dialog("close");
	                          $("html, body").animate({ scrollTop: 0 }, 600);
                              $("#updateSoftPhoneDialog").dialog("open");
	                          
	                          //softPhonePos = "Create";
	                          //$("#softPhoneDialog").html(result.slice(1));
	                          //$("#softPhoneDialog").dialog("open");
	                          //$("#softPhoneDialog").dialog("open");
                              $(":button:contains('Complete')").show().addClass('subButton');
                              $(":button:contains('Cancel')").show().addClass('cancelButton');
	                          $(":button:contains('Create')").hide();
	                          $(":button:contains('Close')").hide();
	                          $(":button:contains('Update')").hide();
	                          $(":button:contains('Confirm')").hide();
	                          $(":button:contains('More Changes')").hide();
	                          $(":button:contains('Return To Main')").hide();
	                          /*var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
	                          getCustomTagList(deviceTypeNameValue);*/
	                      }
	                });  
	            	   
	             },
	            
	            	Create: function() {
	            		$("html, body").animate({ scrollTop: 0 }, 600);
                        var dataToSend = $("form#softPhoneAddFormData").serializeArray();
                        var pageUrl = "checkData.php"; 
                        $.ajax({
                          type: "POST",
                          url: "userMod/softPhone/"+pageUrl,
                          data: dataToSend,
                          success: function(result)
                          {

                              result = result.trim();
                              if (result.slice(0, 1) == 1)
                              {
                                  $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                              }
                              else
                              {
                                  $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                              }
                              $("#updateSoftPhoneDialog").html(result.slice(1));
                              $("#softPhoneDialog").dialog("close");
                              $("html, body").animate({ scrollTop: 0 }, 600);
                              $("#updateSoftPhoneDialog").dialog("open");

                              $(":button:contains('Complete')").show().addClass('subButton');;
                              $(":button:contains('Cancel')").show().addClass('cancelButton');;
                              $(":button:contains('More Changes')").hide();
                              $(":button:contains('Return To Main')").hide();
                              $(":button:contains('Create')").hide();
                              // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                          }
            });  
    	 	 
	             },
	              Close: function() {
	                  $(this).dialog("close");
	              },              
	              Cancel: function() {
	                  $(this).dialog("close");
	              }

	          },
	          open: function() {
					setDialogDayNightMode($(this));
	        	  	$(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
       		     	$(".ui-dialog-buttonpane button:contains('Create')").button().hide();
       		     	$(".ui-dialog-buttonpane button:contains('Close')").button().hide();
	        	   	$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
	        	    $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
	        	   
	        	    if(softPhonePos == "Create"){
	        		   $(".ui-dialog-buttonpane button:contains('Create')").button().show().addClass('subButton');;
	        		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');;
	        		}
	        	   
	        	   if(softPhonePos == "Update"){
	        		   $(".ui-dialog-buttonpane button:contains('Update')").button().show().addClass('subButton');;
	        		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');;
	        	   }
	        	   
	        	   if(softPhonePos == "Delete"){
	        		   $(".ui-dialog-buttonpane button:contains('Confirm')").button().show().addClass('subButton');;
	        		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');;
	        	   }
	         }
	      });
		  
		  
		  /* updateSoftPhoneDialog update softPhone */
		  $("#updateSoftPhoneDialog").dialog({
	          autoOpen: false,
	          width: 800,
	          modal: true,
	          title: "Update Soft Phone Dialog",
	          position: { my: "top", at: "top" },
	          resizable: false,
	          closeOnEscape: false,
	          buttons: {
	        	  	
	        	Create: function() {
	                            
	        	},
	             Update: function() {
	                 
	            	   
	             },
	            Confirm: function() {   
	            	 
	             },
	             Complete: function(){
		            	
	            	 if(isModify == "modify"){
	            		 $(this).dialog("close");
	            		 $("html, body").animate({ scrollTop: 0 }, 600);
	            	 }else{
	            		 getSoftPhoneListAdd();
	            	 }
	            	 
	             },
	             Close: function() {
	                 $(this).dialog("close");
	             },              
	             Cancel: function() {
	              $("#softPhoneDialog").dialog("open");
	              $(this).dialog("close");
	             }
	             

	          },
	         
        	  open: function() {
					setDialogDayNightMode($(this));
	        	  	$(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
       		     	$(".ui-dialog-buttonpane button:contains('Create')").button().hide();
       		     	$(".ui-dialog-buttonpane button:contains('Close')").button().hide();
	        	   	$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
	        	    $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
	        	   
	        	    if(softPhonePos == "Create"){
	        		   $(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');;
	        		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');;
	        		}
	        	   
	        	   if(softPhonePos == "Update"){
	        		   $(".ui-dialog-buttonpane button:contains('Update')").button().show().addClass('subButton');;
	        		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');;
	        	   }
	        	   
	        	   if(softPhonePos == "Delete"){
	        		   $(".ui-dialog-buttonpane button:contains('Confirm')").button().show().addClass('subButton');;
	        		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');;
	        	   }
	         }
	       
	      });
		  
});// end main function 
 
/* end get save custom tag records */