<?php 
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
$blfUserData = array();
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/adminPortal/devices/deviceTypeManagement.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
$devobj = new DeviceTypeManagement();
$enterpriseDeviceTypeList = $devobj->getEnterpriseTableDevices($_SESSION["sp"]);
$deviceTypeList = $devobj->getSystemTableDevices();
//print_r($deviceTypeList);
require_once ("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
$userObj = new UserOperations();



//if (!isset($_SESSION["autoFill"]))
//{
    if($_POST["seachLabel"] == "enterprise"){
        
        $xmlinput = xmlHeader($sessionid, "UserGetListInServiceProviderRequest");
    }else{
        $xmlinput = xmlHeader($sessionid, "UserGetListInGroupRequest");
    }
    
    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
    if($_POST["seachLabel"] == "group"){
        $xmlinput .= "<GroupId>" . htmlspecialchars($_SESSION["groupId"]) . "</GroupId>";
    }
    
    $xmlinput .= xmlFooter();
    $response = $client->processOCIMessage(array("in0" => $xmlinput));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    
    foreach ($xml->command->userTable->row as $key => $value)
    {
        if(strval($value->col[0]) == $_POST["userId"]){
            continue;
        }
        if(isset($_SESSION["userInfo"]["monitoredUsers"])){
            foreach ($_SESSION["userInfo"]["monitoredUsers"] as $mkey => $mvalue){
                if($mvalue["id"] == strval($value->col[0])){
                    
                    continue 2;
                    
                }
            }
        }
        
        $userAssigned = $userObj->getUserDetail(strval($value->col[0]));
        if($userAssigned["Success"]["deviceType"] !=""){
            if(array_key_exists($userAssigned["Success"]["deviceType"], $enterpriseDeviceTypeList)){
                if($enterpriseDeviceTypeList[$userAssigned["Success"]["deviceType"]]["enabled"] != "true" || $enterpriseDeviceTypeList[$userAssigned["Success"]["deviceType"]]["blf"] == "No"){
                    continue;
                }
                
            }else if(array_key_exists($userAssigned["Success"]["deviceType"], $deviceTypeList)){
                if($deviceTypeList[$userAssigned["Success"]["deviceType"]]["enabled"]!="true" || $deviceTypeList[$userAssigned["Success"]["deviceType"]]["blf"] == "No"){
                    continue;
                }
            }
        }
        
        if($_POST["seachLabel"] == "enterprise"){
            $name = strval($value->col[2]) . ", " . strval($value->col[3]);
        }else{
            $name = strval($value->col[1]) . ", " . strval($value->col[2]);
        }
        if($_POST["seachLabel"] == "enterprise"){
            if (strval($value->col[5])){
                $phoneNumber = substr(strval($value->col[5]), 3) . "x" . strval($value->col[11]) . " - " . $name;
            }else{
                $phoneNumber = strval($value->col[0]) . " - " . $name;
            }
        }else{
            if (strval($value->col[4])){
                $phoneNumber = substr(strval($value->col[4]), 3) . "x" . strval($value->col[10]) . " - " . $name;
            }else{
                $phoneNumber = strval($value->col[0]) . " - " . $name;
            }
        }
        
        $blfUserData[$phoneNumber] = strval($value->col[0]);
    }
    
    if (isset($blfUserData))
    {
        ksort($blfUserData);
    }
    //print_r($_SESSION["autoFill"]);
//}

if (isset($blfUserData))
{
    foreach ($blfUserData as $key => $value)
    {
        echo $key . ":";
    }
}
?>