<?php 
class UserModifyDBOperations{
    
    function getAllPhoneProfiles(){
        global $db;
        
        $phoneProfileArray = array();
        $stmt = $db->prepare("SELECT * FROM phoneProfilesLookup");
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i =0;
        foreach($row as $key => $val) {
            $phoneProfileArray[$i][] = $val->phoneProfile;
            $phoneProfileArray[$i][] = $val->value;
            $i++;
        }
        
        return $phoneProfileArray;
    }
    
    
    function getAllTagBundles() {
        global $db;
        $tagBundleArray = array();
        
        $stmt = $db->prepare("SELECT `tagBundle` FROM deviceMgmtTagBundles");
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i =0;
        foreach($row as $key => $val) {
            if(!in_array($val->tagBundle, $tagBundleArray)){
                $tagBundleArray[$i] = $val->tagBundle;
                
                $i++;
            }
            
        }
        return $tagBundleArray;
    }
    
    
    
    
    function getAllCustomTagsFromTagBundles($tagBundle)
    {
        global $db;
        
        $customTagArray = array();
        $tagBundle = join("','", $tagBundle);
        
        $stmt = $db->prepare("SELECT `tag`, `tagValue` FROM deviceMgmtTagBundles where `tagBundle` in('$tagBundle')");
        $stmt->execute();
        $customTagArrayRel = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($customTagArrayRel as $key => $value){
            $customTagArray[strval($value->tag)] = strval($value->tagValue);
        }
        
        return $customTagArray;
    }
    
    function getAllCustomProfiles(){
        global $db;
        
        $customProfileArray = array();
        
        $stmt = $db->prepare("SELECT * FROM customProfiles");
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i =0;
        foreach($row as $key => $val) {
            $customProfileArray[$i++] = $val->deviceType;
            
        }
        
        return $customProfileArray;
    }
    
    //Code added @ 03 May 2018 
    //Check deveice type in device table for display custom tag
    function checkDeviceTypeForCustomTagDisplay($deviceType){        
        global $db;
	$return = "false";
        //Code added @ 16 July 2019
        if($_SESSION['cluster_support']){
            $selectedCluster = $_SESSION['selectedCluster'];
            $stmt = $db->prepare('SELECT vdmTemplate FROM systemDevices WHERE deviceType = ? AND clusterName = ?');
            $stmt->execute(array($deviceType, $selectedCluster));
        }else{
            $stmt = $db->prepare('SELECT vdmTemplate FROM systemDevices WHERE deviceType = ?');
            $stmt->execute(array($deviceType));
        }
	$row = $stmt->fetch(PDO::FETCH_OBJ);
	if ($row) {
		$return = $row->vdmTemplate;
	}
	return $return;
    }
    //End Code
}
?>