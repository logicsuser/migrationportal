<?php
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");

$flag = 0;
$servicesArr = array();
$spId    = $_SESSION["sp"];
$groupId = $_SESSION["groupId"];


$userServicePack = array();
$allServicePack  = array();

if(isset($_POST['act']) && $_POST['act']=="retrieveServicePackToAdd")
{
    $userServicePack = $_POST['userServicePack'];
    $allServicePack  = $_POST['allServicePack'];

    $servicePackWithVM = checkVMServiceInUserSelectedServicePack($userServicePack, $client, $sessionid, $spId);
    if($servicePackWithVM=="NONE")
    {
        echo $servicePackWithVMToSelected = checkVMServiceInUserSelectedServicePack($allServicePack, $client, $sessionid, $spId);
    }
    else 
    {
        echo "NONE";
    }
}


if(isset($_POST['act']) && $_POST['act']=="retrieveServicePackToDel")
{
    $userServicePack   = $_POST['userServicePack'];
    $servicePackWithVM = checkVMServiceInUserSelectedServicePack($userServicePack, $client, $sessionid, $spId);
    if($servicePackWithVM!="NONE")
    {
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId). "</serviceProviderId>";
        $xmlinput .= "<servicePackName>" . $servicePackWithVM . "</servicePackName>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                
        if(strval($xml->command->userServiceTable->row->col[0])=="Voice Messaging User" && count($xml->command->userServiceTable->row)=="1")
        {
            echo $servicePackWithVM;
        }
        else
        {
            echo "NONE";
        }
    }
    else
    {
        echo "NONE";
    }
}



function checkVMServiceInUserSelectedServicePack($servicePacks, $client, $sessionid, $spId)
{
        $servicePacksWithVMServiceArr = explode(",", $_SESSION["vmServicePacksFlat"]); 
        //$servicePacksWithVMServiceArr = array("sol_express");       
        for($i=0;$i<count($servicePacks);$i++)
        {
            if(in_array($servicePacks[$i], $servicePacksWithVMServiceArr))
            {
                $flag = $flag + 1;
                $servicePackWithVMService = $servicePacks[$i];
            }
        }
        if($flag==1)
        {    
            $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId). "</serviceProviderId>";
            $xmlinput .= "<servicePackName>" . $servicePackWithVMService . "</servicePackName>";
            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            foreach ($xml->command->userServiceTable->row as $key => $value){
                if (strval($value->col[0]) == "Voice Messaging User") {
                    return $servicePackWithVMService;
                }
            }
        }
        else 
        {
            return "NONE";
        }
}

?>