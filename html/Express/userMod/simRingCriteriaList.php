<?php 
    require_once("/var/www/html/Express/config.php");
    require_once ("/var/www/lib/broadsoft/login.php");
    checkLogin();
    require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/simRingCriteriaManagement.php");
    $objSimRingCriteriaMgmt = new SimRingCriteriaManagement();
    require_once("/var/www/lib/broadsoft/adminPortal/getAllSchedules.php");
    $simRingCriteriaListArr = array();
    $_POST['userId']  = $_SESSION["userInfo"]['userId'];
    if(isset($_POST['userId']) && $_POST['userId']!=""){
        $exitsSimRingCriteraArr = array();
        $userId                                         = $_POST['userId'];
        $simRingListData                                = $objSimRingCriteriaMgmt->getSimRingCrietriaList($userId);
        $simRingListDataArr                             = $simRingListData['Success'];   
        $_SESSION["userInfo"]["criteriaTableList"]      = $simRingListData['Success']['criteriaTableArr'];        
        $_SESSION["userInfo"]["activeCriteriaNameList"] = $simRingListData['Success']['activeCriteriaNameArr']; 
        $_SESSION["userInfo"]["crtiaNameList"]          = $simRingListData['Success']['crtiaNameArr'];
        
        $exitsSimRingCriteraArr                         = $simRingListData['Success']['criteriaTableArr'];
        $completeExistsSimRingCriteraArr                = $exitsSimRingCriteraArr;
        //Code to start mrrge array element into list to show only        
        if(isset($_SESSION['addSimRingCriteria']) && !empty($_SESSION['addSimRingCriteria'])){
                //$completeExistsSimRingCriteraArr = array();
                foreach($_SESSION['addSimRingCriteria'] as $row){
                        $tempAddRowArr[] = $row;
                }
               if(!empty($exitsSimRingCriteraArr)){
                   $completeExistsSimRingCriteraArr = array_merge($tempAddRowArr, $exitsSimRingCriteraArr);
               }else{
                   $completeExistsSimRingCriteraArr = $tempAddRowArr;
               }
        }
        
        if(isset($_SESSION['deleteSimRingCriteria']) && !empty($_SESSION['deleteSimRingCriteria'])){
            
            foreach($completeExistsSimRingCriteraArr as $keyExist => $rowExist){            
                    foreach($_SESSION['deleteSimRingCriteria'] as $keyDel => $rowDel){
                        if($rowExist['criteriaName']==$rowDel['criteriaName']){                            
                            unset($completeExistsSimRingCriteraArr[$keyExist]);
                        }
                    }
            }  
        }
        
        if(isset($_SESSION['editSimRingCriteria']) && !empty($_SESSION['editSimRingCriteria'])){
            
            foreach($completeExistsSimRingCriteraArr as $keyExist => $rowExist){            
                    foreach($_SESSION['editSimRingCriteria'] as $keyMod => $rowMod){
                        if($rowExist['criteriaName']==$rowMod['oldCriteriaName']){                            
                            unset($completeExistsSimRingCriteraArr[$keyExist]);
                            $completeExistsSimRingCriteraArr[$keyExist] = $rowMod;
                        }
                    }
            }  
        }
        
        //echo "<pre>criteriaTableTmpArr - ";
        //print_r($completeExistsSimRingCriteraArr);
        //die();
        
        $_SESSION["userInfo"]["crtiaNameListInSession"]    = $completeExistsSimRingCriteraArr;
        $simRingListDataArr['criteriaTableTmpArr']         = $completeExistsSimRingCriteraArr;
        
        echo  json_encode($simRingListDataArr);
    }
?>      
           