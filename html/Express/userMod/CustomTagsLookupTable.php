<?php

/**
 * Created by Karl.
 * Date: 4/27/2017
 */
class CustomTagsLookupTable extends HTMLTable
{
    private static function Attr_Parameter()            { return "parameter";}
    private static function Attr_TagName()              { return "tagName";}
    private static function Attr_DefaultValue()         { return "defaultValue";}
    private static function Attr_Description()          { return "description";}
    private static function Attr_Short_Description()    { return "shortDescription";}
    private static function Attr_AltOption1()           { return "altOption1";}
    private static function Attr_AltOption2()           { return "altOption2";}
    private static function Attr_AltOption3()           { return "altOption3";}
    private static function Attr_AltOption4()           { return "altOption4";}

    private static function Attr_Table()                { return "customTagsLookup";}

    private $db;
    private $type = "";


    public function __construct($db, $type) {
        parent::__construct();

        $this->db   = $db;
        $this->type = $type;

        $this->addAttribute(self::Attr_Parameter(),         "Parameter");
        $this->addAttribute(self::Attr_TagName(),           "Tag Name / Value");
        $this->addAttribute(self::Attr_DefaultValue(),      "Default Value");
        $this->addAttribute(self::Attr_Description(),       "Description");
        $this->addAttribute(self::Attr_Short_Description(), "Short Description");
        $this->addAttribute(self::Attr_AltOption1(),        "Alt Option 1");
        $this->addAttribute(self::Attr_AltOption2(),        "Alt Option 2");
        $this->addAttribute(self::Attr_AltOption3(),        "Alt Option 3");
        $this->addAttribute(self::Attr_AltOption4(),        "Alt Option 4");
    }

    public function setType($type) {
        $this->type = $type;

        unset($this->data);
        $this->numRecords = 0;
    }


    /**
     * mandatory implementation with abstract prototype in parent class
     */
    protected function executeAction() {
        //Query: select * from customTagsLookup where type='polycomVVX';
        $query = "select * from " . self::Attr_Table() . " where type='" . $this->type . "'";

        $result = $this->db->query($query);
        while ($row = $result->fetch()) {
            $this->data[$this->numRecords][self::Attr_Parameter()]         = $row[self::Attr_Parameter()];
            $this->data[$this->numRecords][self::Attr_TagName()]           = $row[self::Attr_TagName()];
            $this->data[$this->numRecords][self::Attr_DefaultValue()]      = $row[self::Attr_DefaultValue()];
            $this->data[$this->numRecords][self::Attr_Description()]       = $row[self::Attr_Description()];
            $this->data[$this->numRecords][self::Attr_Short_Description()] = $row[self::Attr_Short_Description()];
            $this->data[$this->numRecords][self::Attr_AltOption1()]        = $row[self::Attr_AltOption1()];
            $this->data[$this->numRecords][self::Attr_AltOption2()]        = $row[self::Attr_AltOption2()];
            $this->data[$this->numRecords][self::Attr_AltOption3()]        = $row[self::Attr_AltOption3()];
            $this->data[$this->numRecords][self::Attr_AltOption4()]        = $row[self::Attr_AltOption4()];
            $this->numRecords++;
        }

    }
}
