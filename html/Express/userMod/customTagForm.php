<?php
/**
 * HTML content of device's Custom Tag dialog form
 * Created by Karl.
 * Date: 11/14/2016
 */

$mode = "create";

$tagName = "";
$tagValue = "";

// Retrieve tag information in update mode
if (isset($_POST["customTagName"])) {
	$mode = "update";
	
	$tagName  = $_POST["customTagName"];
	$tagValue = $_POST["customTagValue"];
}

?>

<script>

    function evalTagDialog(txtBox) {
//         $("#AllowOutOfSpecCheckBoxDiv").show();
        var enableCreate = "disable";
        if($('#tagInputFieldValue').is(':visible')) {
            enableCreate = $('#tagValue').val().length > 0 ? "enable" : "disable";
        } else if ($('#tagSelectFieldValue').is(':visible')) {
        	enableCreate = $('#tagSelectFieldValue option:selected').val() !== ''? "enable" : "disable";
        } else if ($('#tagRangeFieldValue').is(':visible')) {
            var min = parseInt($('#tagRangeInputField').attr('min'));
            var max = parseInt($('#tagRangeInputField').attr('max'));
            var tagVal = parseInt($('#tagRangeInputField').val());
            enableCreate = (tagVal >= min && tagVal <= max) ? "enable" : "disable";
        } else if($('#AllowOutOfSpecValue').is(':visible')) {
        	$('#AllowOutOfSpecValue').prop("checked", false);
        	enableCreate = $('#AllowOutOfSpecValue').val() != "" ? "enable" : "disable";
         }
        $(".ui-dialog-buttonpane button:contains('Create')").button(enableCreate);
        $(".ui-dialog-buttonpane button:contains('Update')").button(enableCreate);
        
// 		if($("#tagShortDescription option:selected").val() == "") {
// 			$("#AllowOutOfSpecCheckBoxDiv").hide();
// 		} else {
// 			$("#AllowOutOfSpecCheckBoxDiv").show();
// 		}

		if( $("#tagShortDescription option:selected").val() != "") {
			$("#AllowOutOfSpecCheckBoxDiv").show();
		} else {
			$("#AllowOutOfSpecCheckBoxDiv").hide();
		}


		
    }

    $(function() {
        $('#AllowOutOfSpecValue').keyup(function() {
            if (this.value.match(/[^a-zA-Z0-9 ]/g)) {
                this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
            }
        });
    });
</script>

<div style="font-size: 12px">
    <form action="#" method="POST" name="customTagForm" id="customTagForm" class="">
        <!-- Tag name  -->
    <div class="row" style="">
        <div class="col-md-12">
        <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="tagName" class="labelText">Tag name <?php echo $mode == "create" ? "<span class=\"required\">*</span> " : ""; ?></label><br>
                    <div class="dropdown-wrap">
                        <select Name="tagName" onmouseover="this.title=this.options[this.selectedIndex].title" id="tagShortDescription" onchange="evalTagDialog(this)" <?php echo ($mode == "update" ? " disabled" : ""); ?>>
               		 	</select>
					</div>
        	   </div>
            </div>
			
			</div>
    </div>
    
    <div class="row" style="">
        <div class="col-md-12">
            <div class="col-md-3" id = "AllowOutOfSpecCheckBoxDiv" style="display:none;margin-top: 25px;">
                    <div class="form-group">
                        <div class="" style="margin-left: 15px;">
                            <input type="checkbox" name ="AllowOutOfSpecCheckBox" id ="AllowOutOfSpecCheckBox" onclick = "AllowOutOfSpecTag(this)">
    						<label for="AllowOutOfSpecCheckBox"><span></span></label>
    						<label for="" class="labelText" style="vertical-align: top;"> Allow out-of-spec tag <br />values </label>
    					</div>
            	   </div>
            </div>
            <div id="allTagValuesDiv">
                <div class="col-md-6" id = "tagInputFieldValue" style="display:none">
            	<div class="form-group">
                    <label for="tagValue" class="labelText">Tag Value</label><br />
            	   <div class="" id="customTagInputFieldValue" style=""></div>
                   <span id="inputName" style="font-size: 10px;" class="labelTextGrey">Enter Alphanumeric value</span>
                </div>
                </div>
                <!-- Tag value  -->
                <div class="col-md-6" id = "tagSelectFieldValue" style="display:none">
                    <div class="form-group">
                	   <label for="tagValue" class="labelText">Tag Value</label>
                       <div class="dropdown-wrap">
                			<select name="tagValue" onmouseover="this.title=this.options[this.selectedIndex].title" id ="tagOptions" onchange="evalTagDialog(this)"></select>
    					</div>
                    </div>
                </div>
            
                <div class="col-md-6" id = "tagRangeFieldValue" style="display:none">
                    <div class="form-group">
                	<label for="tagValue" class="labelText">Tag Value</label><br />
                	<div class="" id="tagRangeFieldVisible" style=""></div>
                    <span id="rangeName" style="font-size: 10px;"></span>
                    </div>
                </div>
    		</div>
    		<!-- Out-Of-Spec tag value -->
			<div class="col-md-6" id = "AllowOutOfSpecValDiv" style="display:none">
            	<div class="form-group">
                	<label for="" class="labelText">Tag Value</label><br />
                	<div class="">
                		<input type="text" name="AllowOutOfSpecValue" id="AllowOutOfSpecValue" oninput="evalTagDialog(this)" class="field" required pattern="[a-zA-Z0-9]+"/>
                	</div>
                	<span style="font-size: 10px;" class="labelTextGrey">Enter Alphanumeric value</span>
                 </div>
            </div>
		</div>
    </div>
		
<!--         </div> -->
<!--     </div> -->
    </form>
</div>
