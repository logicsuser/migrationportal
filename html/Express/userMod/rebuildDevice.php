<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
require_once ("/var/www/html/Express/config.php");
    const customProfileTable = "customProfiles";
    const customProfileName  = "customProfileName";
    const customTagName      = "customTagName";
    const customTagValue     = "customTagValue";
    const customProfileType  = "deviceType";

    const ExpressCustomProfileName = "%Express_Custom_Profile_Name%";
    const ExpressTagBundleName = "%Express_Tag_Bundle%";
    const ExpressPhoneProfileName = "%phone-template%";

    require_once("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig.php");
    require_once("/var/www/html/Express/userMod/UserCustomTagManager.php");
    require_once("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
    require_once ("/var/www/html/Express/userMod/analogUserData.php");
    require_once ("/var/www/html/Express/userMod/UserModifyDBOperations.php");
    require_once ("/var/www/html/Express/util/formDataArrayDiff.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
    $syslevelObj = new sysLevelDeviceOperations();
    $setName = $syslevelObj->getTagSetNameByDeviceType($_SESSION ["userInfo"] ["deviceType"], $ociVersion);
    $systemDefaultTagVal = "";
    if($setName["Success"] != "" && !empty($setName["Success"])){
        $defaultSettags = $syslevelObj->getDefaultTagListByTagSet($setName["Success"]);
        $phoneTemplateName = "%phone-template%";
        if(array_key_exists($phoneTemplateName, $defaultSettags["Success"])){
            $systemDefaultTagVal = $defaultSettags["Success"]["%phone-template%"][0];
        }
    }
    
    
    $diffObj = new FormDataArrayDiff();
    if(!array_key_exists("modTagBundle", $_POST)){
        $_POST['modTagBundle'] = array();
    }

    function addDeviceCustomTag($deviceName, $tagName, $tagValue) {
        global $sessionid, $client;

        $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagAddRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= "<tagName>" . $tagName . "</tagName>";
        $xmlinput .= "<tagValue>" . $tagValue . "</tagValue>";

        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $errMsg = "";
        if (readError($xml) != "") {
            $errMsg = "Failed to add custom tag to device " . $deviceName . " .";
            $errMsg .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
            if ($xml->command->detail)
            {
                $errMsg .= "%0D%0A" . strval($xml->command->detail);
            }
        }
        return $errMsg;
    }


    function addDeviceCustomTags($deviceType, $deviceName, $customProfile, $tagBundleArray) {
        
        global $db;
        $userOpDBOperation = new UserModifyDBOperations();
        // No custom tags can be selected while building a user
        // ----------------------------------------------------
        //if ($customProfile == "None") { return ""; }
        
        $tagToBeAdd = $userOpDBOperation->getAllCustomTagsFromTagBundles($tagBundleArray);
        if(count($tagToBeAdd) > 0){
            foreach ($tagToBeAdd as $keyToAdd => $valToAdd){
                if (($errMsg = addDeviceCustomTag($deviceName, $keyToAdd, $valToAdd)) != "") {
                    return $errMsg;
                }
            }
        }
        if(count($tagBundleArray) > 0){
            $bundlleVal = implode(";", $tagBundleArray);
            if (($errMsg = addDeviceCustomTag($deviceName, ExpressTagBundleName, $bundlleVal)) != "") {
                return $errMsg;
            }
        }
        if(!empty($customProfile)){
            if($systemDefaultTagVal != $customProfile){
                if (($errMsg = addDeviceCustomTag($deviceName, ExpressPhoneProfileName, $customProfile)) != "") {
                    return $errMsg;
                }
                
            }
            if (($errMsg = addDeviceCustomTag($deviceName, ExpressCustomProfileName, $customProfile)) != "") {
                return $errMsg;
            }
            
            
        }
        
        // get custom tags from database for a specific custom profile
        // and add them to the device
        // -----------------------------------------------------------
        //$query = "select " . customTagName . ", " . customTagValue . " from " . customProfileTable . " where " . customProfileType . "='" . $deviceType . "' and " . customProfileName . "='" . $customProfile . "'";
        //$results = $db->query($query);

        /*while ($row = $results->fetch()) {
            if (($errMsg = addDeviceCustomTag($deviceName, $row[customTagName], $row[customTagValue])) != "") {
                return $errMsg;
            }
        }*/

        // finally add custom tag with the custom profile name for the reporting purpose
        /*if (($errMsg = addDeviceCustomTag($deviceName, ExpressCustomProfileName, $customProfile)) != "") {
            return $errMsg;
        }
        if(count($tagBundleArray) > 0){
            $bundlleVal = implode(":", $tagBundleArray); 
            if (($errMsg = addDeviceCustomTag($deviceName, ExpressTagBundleName, $bundlleVal)) != "") {
                return $errMsg;
            }
        }*/

        return "";
    }


    function removeDeviceCustomTags($deviceType, $deviceName, $customProfile, $tagBundleArray) {
        
        $existingCustomTag = array(ExpressPhoneProfileName => "", ExpressCustomProfileName => "");
        //global $db;
        //$userOpDBOperation = new UserModifyDBOperations();
		
        //$existingCustomTag = $userOpDBOperation->getAllCustomTagsFromTagBundles($deviceType, $deviceName);
        $tagManager = new UserCustomTagManager($deviceName,$deviceType);
        //$existingCustomTag = $tagManager->getUserCustomTagList();
        if(count($existingCustomTag) > 0){
        	foreach ($existingCustomTag as $key => $val){
        		$resp = $tagManager->deleteCustomTag($key);
        	}
        }
    }
    
    function addDeviceCustomTagsIfBundleChanged($deviceType, $deviceName, $tagToBeAddIfBundleChanged){
        //global $db;
        //$userOpDBOperation = new UserModifyDBOperations();
        // No custom tags can be selected while building a user
        // ----------------------------------------------------
        //if ($customProfile == "None") { return ""; }
        
        //$tagToBeAdd = $userOpDBOperation->getAllCustomTagsFromTagBundles($deviceType, $deviceName);
        if(count($tagToBeAddIfBundleChanged) > 0){
            foreach ($tagToBeAddIfBundleChanged as $keyToAdd => $valToAdd){
                if (($errMsg = addDeviceCustomTag($deviceName, $keyToAdd, $valToAdd)) != "") {
                    return $errMsg;
                }
            }
        }
        
        return "";
    }
    
    function removeDeviceCustomTagsIfTagBundleChanged($deviceType, $deviceName, $tagToDeleteArray) {
        global $db;
        //$userOpDBOperation = new UserModifyDBOperations();
        
        //$existingCustomTag = $userOpDBOperation->getAllCustomTagsFromTagBundles($deviceType, $deviceName);
        $tagManager = new UserCustomTagManager($deviceName,$deviceType);
        //$existingCustomTag = $tagManager->getUserCustomTagList();
        if(count($tagToDeleteArray) > 0){
            foreach ($tagToDeleteArray as $key => $val){
                $resp = $tagManager->deleteCustomTag($key);
            }
        }
    }


    function getResourceForUserIdCreation($attr) {

        switch ($attr) {
            case ResourceNameBuilder::DN:               return $_SESSION["userInfo"]["phoneNumber"];
            case ResourceNameBuilder::EXT:              return $_SESSION["userInfo"]["extension"];
            case ResourceNameBuilder::USR_CLID:         return $_SESSION["userInfo"]["callingLineIdPhoneNumber"];
            case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:           return $_SESSION["groupId"];
            case ResourceNameBuilder::SYS_DFLT_DOMAIN:  return $_SESSION["systemDefaultDomain"];
            case ResourceNameBuilder::DEFAULT_DOMAIN:   return $_SESSION["defaultDomain"];
            case ResourceNameBuilder::GRP_PROXY_DOMAIN: return $_POST["linePortDomainResult"];
            case ResourceNameBuilder::FIRST_NAME:       return $_SESSION["userInfo"]["firstName"];
            case ResourceNameBuilder::LAST_NAME:        return $_SESSION["userInfo"]["lastName"];
            case ResourceNameBuilder::DEV_NAME:         return $_SESSION["userInfo"]["deviceName"];
            case ResourceNameBuilder::MAC_ADDR:
                $macChange = isset($changes["device"]["macAddress"]);
                $macAddress = $macChange ? $changes["device"]["macAddress"] : $_POST["macAddress"];
                return $macAddress;
                break;
            /*
            //TODO: Revisit requirements for using UserID in macros
            case ResourceNameBuilder::USR_ID:
                $name = isset($_SESSION["userId"]) ? $_SESSION["userId"] : "";
                if ($name == "") {
                    return "";
                }
                $nameSplit = explode("@", $name);
                return $nameSplit[0];
                break;
             */
            default: return "";
        }
    }


    function getResourceForDeviceNameCreation($attr) {
        switch ($attr) {
            case ResourceNameBuilder::DN:               return $_SESSION["userInfo"]["phoneNumber"];
            case ResourceNameBuilder::EXT:              return $_SESSION["userInfo"]["extension"];
            case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:           return $_SESSION["groupId"];
            case ResourceNameBuilder::FIRST_NAME:       return $_SESSION["userInfo"]["firstName"];
            case ResourceNameBuilder::LAST_NAME:        return $_SESSION["userInfo"]["lastName"];
            case ResourceNameBuilder::DEV_TYPE:
                $deviceChange = isset($changes["device"]["deviceType"]);
                $deviceType = $deviceChange ? $changes["device"]["deviceType"] : $_POST["deviceType"];
                return $deviceType;
                break;
            case ResourceNameBuilder::MAC_ADDR:
                $macChange = isset($changes["device"]["macAddress"]);
                $macAddress = $macChange ? $changes["device"]["macAddress"] : $_POST["macAddress"];
                return $macAddress;
                break;
            case ResourceNameBuilder::INT_1:
            case ResourceNameBuilder::INT_2:            return isset($_SESSION["userInfo"]["deviceIndex"]) ? $_SESSION["userInfo"]["deviceIndex"] : "";
            //TODO: Device Index must be implemented in getUserInfo
            default: return "";
        }
    }

    function getNumberOfAssignedPorts($device) {
        global $ociVersion, $sessionid, $client;

        $ociCmd = "GroupAccessDeviceGetRequest18sp1";
        $xmlinput = xmlHeader($sessionid, $ociCmd);
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $device . "</deviceName>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        return strval($xml->command->numberOfAssignedPorts);
    }

    function deleteDeviceOnUserRemoval($deviceType) {
        global $db, $deleteAnalogUserDevice;

        $sipGatewayLookup = new DBLookup($db, "systemDevices", "deviceType", "phoneType");
        $deviceIsAnalog = $sipGatewayLookup->get($deviceType) == "Analog";

        // short-circuit for Audio-Codes
        $deviceIsAudioCodes = substr($deviceType, 0, strlen("AudioCodes-")) == "AudioCodes-";

        if ($deviceIsAnalog || $deviceIsAudioCodes) {
            return $deleteAnalogUserDevice == "true";
        }

        // device is not for analog user
        return true;
    }

    function removeDeviceFromUser($sessionId, $userId, $deviceName, $client)
    {
        //unlink user and device
        $xmlinput = xmlHeader($sessionId, "UserModifyRequest17sp4");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= "<endpoint xsi:nil=\"true\" />";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errorResult = readError($xml);
        if (getNumberOfAssignedPorts($deviceName) == 0 && deleteDeviceOnUserRemoval($_SESSION["userInfo"]["deviceType"]) && deviceExists($deviceName)) {
            //delete the device
            $xmlinput = xmlHeader($sessionId, "GroupAccessDeviceDeleteRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
            $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
            $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            $errorResult = readError($xml);
            if($errorResult == "") {
                resetDevices($deviceName);
            }
            return $errorResult;
        }
        return "";
    }
    
    function compareExistingTagsWithBundleTags($existingCustomTag, $tagToBeAddIfBundleChanged){
        $diffrencedArray = array();
        
        foreach ($tagToBeAddIfBundleChanged as $key => $val){
            if(!array_key_exists($key, $existingCustomTag)){
                $diffrencedArray["add"][$key] = $val;
            }else{
                $diffrencedArray["modify"][$key] = $val;
            }
        }
        return $diffrencedArray;
    }
    
    require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

    require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/DeviceNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/LineportNameBuilder.php");
    //print_r($_POST);
    $deleteOldDevice = isset($changes["device"]["deviceType"]) && $changes["device"]["deviceType"] != $_POST["previousDeviceType"] ? 1 : 0;
    $deviceChange = isset($changes["device"]["deviceType"]);
    $macChange = isset($changes["device"]["macAddress"]);
    $customProfileChange = isset($changes["device"]["customProfile"]);
    $deviceType = $deviceChange ? $changes["device"]["deviceType"] : $_POST["deviceType"];
    $macAddress = $macChange ? $changes["device"]["macAddress"] : $_POST["macAddress"];
    $tagBundleChanged = false;
    
    $linePortModified = isset($_POST["linePortDomainResult"]) && !isset($changes['device']['deviceType']) ? true : false;
    if($linePortModified && $_SESSION["userInfo"]["linePort"] != "") {
        $linePort = explode("@", $_SESSION["userInfo"]["linePort"]);
        $newLinePort = $linePort[0] . "@" . $_POST["linePortDomainResult"];
        modifyLinePort($userId, $_SESSION["userInfo"]["deviceName"], $newLinePort);
    }
    
   // if(count($_POST["modTagBundle"]) > 0){
        $diffArr = $diffObj->diffInTwoArray($_POST["modTagBundle"], $_SESSION ["userInfo"]["modTagBundle"]);
       // print_r($diffArr);
        if((isset($diffArr["assigned"]) && count($diffArr["assigned"]) > 0) || (isset($diffArr["removed"]) && count($diffArr["removed"]) > 0)){
            $tagBundleChanged = true;
        }
   // }

    //get user's device name
    $userHasDevice = true;

	if ($ociVersion == "17")
	{
		$xmlinput = xmlHeader($sessionid, "UserGetRequest17sp4");
	}
	else if ($ociVersion == "20")
	{
		$xmlinput = xmlHeader($sessionid, "UserGetRequest20");
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "UserGetRequest19");
	}
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

    $deviceName = isset($xml->command->accessDeviceEndpoint->accessDevice->deviceName) ? strval($xml->command->accessDeviceEndpoint->accessDevice->deviceName) : "";
	if (strlen($deviceName) < 1)
	{
        $userHasDevice = false;
	}

	// Device change scenarios
	// - Assigning a new device to a user previously without a device
	// - Un-assigning a device from a user
	// - Assigning different device (option previously supported)
    // - Changing MAC address (option previously supported)

	if ($deviceChange)
	// Changing device or adding a device to user previously with no device
	{
	    if ($userHasDevice && $deleteOldDevice)
        {
            // Unlink previous device from a user
            $errorResult = removeDeviceFromUser($sessionid,$userId,$deviceName,$client);
        }

        // Build the device if device type not cleared
        if ($deviceType != "")
        {
            require_once("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
            require_once("/var/www/lib/broadsoft/adminPortal/getDevices.php");

            // Build Device Name.
            // ----------------------------------------------------------------------------------------
            // Choose formula based on device type: DID or analog.
            // For Analog type always enforce fallback
            // For DID type, enforce fallback if there is no second criteria/formula.

            // Build devices lists: digitalVoIPDevices and analogGatewayDevices
            $deviceTypesDigitalVoIP = array();
            $deviceTypesAnalogGateways = array();
            $sipGatewayLookup = new DBLookup($db, "systemDevices","deviceType", "phoneType");

            foreach ($nonObsoleteDevices as $key => $value) {
                if ($sipGatewayLookup->get($value) == "Analog") {
                    $deviceTypesAnalogGateways[] = $value;
                } else {
                    $deviceTypesDigitalVoIP[] = $value;
                }
            }

            $deviceIsAnalog = in_array($deviceType, $deviceTypesAnalogGateways);
            $forceResolve = $deviceIsAnalog ? true : $deviceNameDID2 == "";

            $deviceNameBuilder = new DeviceNameBuilder($deviceIsAnalog ? $deviceNameAnalog : $deviceNameDID1,
                getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
                getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
                getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
                $forceResolve);

            if (! $deviceNameBuilder->validate()) {
                // TODO: Implement resolution when Device name input formula has errors
            }

            do {
                $attr = $deviceNameBuilder->getRequiredAttributeKey();
                $deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
            } while ($attr != "");

            $deviceName = $deviceNameBuilder->getName();

            if ($deviceName == "") {
                // Create Device Name builder from the second formula (DID type only).
                // At this point we know that there is second formula, because without second formula
                // name would be forcefully resolved in the first formula.

                $deviceNameBuilder = new DeviceNameBuilder($deviceNameDID2,
                    getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
                    getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
                    getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
                    true);

                if (! $deviceNameBuilder->validate()) {
                    // TODO: Implement resolution when Device name input formula has errors
                }

                do {
                    $attr = $deviceNameBuilder->getRequiredAttributeKey();
                    $deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
                } while ($attr != "");

                $deviceName = $deviceNameBuilder->getName();
            }
            
            if(isset($_POST["userTypeResult"]) && $_POST["userTypeResult"] == "analog") {
                $deviceName = $deviceNameForAnalog;
            }
            /* Substitute device name */
            if(isset($_POST["userTypeResult"]) && $_POST["userTypeResult"] != "analog")
            {
                $deviceName = replaceDevNameWithSubstitute($deviceName);
            }
            $_SESSION["userInfo"]["deviceName"] = $deviceName;

//             // check Device is exist or not
            $executeAddDeviceRequest = ( isset($_POST["userTypeResult"]) && $_POST["userTypeResult"] != "analog" &&
                                            ! deviceExists($deviceName)
                                        );
            
                if($executeAddDeviceRequest) {
            
                $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceAddRequest14");
                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
                $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
                $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
                $xmlinput .= "<deviceType>" . $deviceType . "</deviceType>";
                if ($macAddress != "")
                {
                    $xmlinput .= "<macAddress>" . $changes["device"]["macAddress"] . "</macAddress>";
                }
                $xmlinput .= xmlFooter();
                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                $errorResult = readError($xml);
    
                // Add Device Custom Tags (user level)
                $errorResult = addDeviceCustomTags($deviceType, $deviceName, $_POST["customProfile"], $_POST["modTagBundle"]);
                if ($resultMsg != "") {
                    $data = $resultMsg;
                }
            }
            
            //custom tag
            /*$errorResult = addDeviceCustomTags($deviceType, $deviceName, $_POST["customProfile"]);
            if ($resultMsg != "") {
                $data = $resultMsg;
            }*/
            //link user and device
            
//          $lineport = $_SESSION["userInfo"]["linePort"];
            $lineport = "";
            if ($lineport == "") {

                // Build Line Port.
                // ----------------------------------------------------------------------------------------------------

                // Create lineport name from the first formula.
                // Enforce fallback if there is no second criteria/formula.
                // --------------------------------------------------------
                $linePortBuilder1 = new LineportNameBuilder($linePortCriteria1,
                    getResourceForUserIdCreation(ResourceNameBuilder::DN),
                    getResourceForUserIdCreation(ResourceNameBuilder::EXT),
                    getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID),
                    getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN),
                    getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
                    $linePortCriteria2 == "");  // force fallback only if second criteria does not exist

                if (! $linePortBuilder1->validate()) {
                    // TODO: Implement resolution when lineport input formula has errors
                }

                do {
                    $attr = $linePortBuilder1->getRequiredAttributeKey();
                    $linePortBuilder1->setRequiredAttribute($attr, getResourceForUserIdCreation($attr));
                } while ($attr != "");

                $lineport = $linePortBuilder1->getName();
                if ($lineport == "") {
                    // Create lineport name builder from the second formula. At this point we know that there is second
                    // formula, because without second formula name would be forcefully resolved in the first formula.

                    $linePortBuilder2 = new LineportNameBuilder($linePortCriteria2,
                        getResourceForUserIdCreation(ResourceNameBuilder::DN),
                        getResourceForUserIdCreation(ResourceNameBuilder::EXT),
                        getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID),
                        getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN),
                        getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
                        true);

                    if (! $linePortBuilder2->validate()) {
                        // TODO: Implement resolution when User ID input formula has errors
                    }

                    do {
                        $attr = $linePortBuilder2->getRequiredAttributeKey();
                        $linePortBuilder2->setRequiredAttribute($attr, getResourceForUserIdCreation($attr));
                    } while ($attr != "");

                    $lineport = $linePortBuilder2->getName();
                }
            }
			$lineport = str_replace(" ", "_", $lineport);
            // Build Device Access User Name - fallback definitions are based on Device Name fallback
            // ----------------------------------------------------------------------------------------
            // Enforce fallback if there is no second criteria/formula.

            $deviceAccessUserNameBuilder = new DeviceNameBuilder($deviceAccessUserName1,
                getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
                getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
                getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
                $deviceAccessUserName2 == "");    // force fallback only if second criteria does not exist

            if (! $deviceAccessUserNameBuilder->validate()) {
                // TODO: Implement resolution when Device name input formula has errors
            }

            do {
                $attr = $deviceAccessUserNameBuilder->getRequiredAttributeKey();
                $deviceAccessUserNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
            } while ($attr != "");

            $derivedDeviceAccessUserName = $deviceAccessUserNameBuilder->getName();

            if ($derivedDeviceAccessUserName == "") {
                $deviceAccessUserNameBuilder = new DeviceNameBuilder($deviceAccessUserName2,
                    getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
                    getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
                    getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
                    true);

                if (! $deviceAccessUserNameBuilder->validate()) {
                    // TODO: Implement resolution when Device name input formula has errors
                }

                do {
                    $attr = $deviceAccessUserNameBuilder->getRequiredAttributeKey();
                    $deviceAccessUserNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
                } while ($attr != "");

                $derivedDeviceAccessUserName = $deviceAccessUserNameBuilder->getName();
            }

            // Build Device Access Password - fallback definitions are based on Device Name fallback
            // ----------------------------------------------------------------------------------------
            $deviceAccessPasswordBuilder = new DeviceNameBuilder($deviceAccessPassword,
                getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
                getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
                getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
                true);

            if (! $deviceAccessPasswordBuilder->validate()) {
                // TODO: Implement resolution when Device name input formula has errors
            }

            do {
                $attr = $deviceAccessPasswordBuilder->getRequiredAttributeKey();
                $deviceAccessPasswordBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
            } while ($attr != "");
            
            $derivedDeviceAccessPassword = $deviceAccessPasswordBuilder->getName();
        
            $xmlinput = xmlHeader($sessionid, "UserModifyRequest17sp4");
            $xmlinput .= "<userId>" . $userId . "</userId>";
            $xmlinput .= "<endpoint>
					<accessDeviceEndpoint>
						<accessDevice>
							<deviceLevel>Group</deviceLevel>";
            
             $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
             $xmlinput .= "</accessDevice>
						<linePort>" . $lineport . "</linePort>";
						
             if( $_POST["portNumber"] != "") {
			   $xmlinput .= "<portNumber>" .$_POST["portNumber"]. "</portNumber>";
			}
            
		    $xmlinput .="</accessDeviceEndpoint></endpoint>";
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            $errorResult = readError($xml);

            //set the primary line to true
            $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyUserRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
            $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
            $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
            $xmlinput .= "<linePort>" . $lineport . "</linePort>";
            $xmlinput .= "<isPrimaryLinePort>true</isPrimaryLinePort>";
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            $errorResult = readError($xml);

            sleep(2);

            $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyRequest14");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
            $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
            $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";

            if ($deviceIsAnalog && $analogAccessAuthentication == "false") {
                $xmlinput .= "<useCustomUserNamePassword>false</useCustomUserNamePassword>";
            }
            else {
                $xmlinput .= "<useCustomUserNamePassword>true</useCustomUserNamePassword>";
                $xmlinput .= "<accessDeviceCredentials>
							<userName>" . $derivedDeviceAccessUserName . "</userName>
							<password>" . $derivedDeviceAccessPassword . "</password>
						</accessDeviceCredentials>";
            }
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            $errorResult = readError($xml);

            /*
                        //set custom credentials for device
                        if (strstr($deviceType, " "))
                        {
                            $substrMenu = explode(" ", $deviceType);
                        }
                        else if (strstr($deviceType, "_"))
                        {
                            $substrMenu = explode("_", $deviceType);
                        }
                        else
                        {
                            $substrMenu = explode("-", $deviceType);
                        }
                        $substrMenu = $substrMenu[0];
                        $select = "SELECT customCredentials, username, password from customCredentials where deviceManufacturer='" . $substrMenu . "'";
                        $result = $db->query($select);
                        while ($row = $result->fetch())
                        {
                            if ($row["customCredentials"] == "y")
                            {
                                $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyRequest14");
                                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
                                $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
                                $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
                                $xmlinput .= "<useCustomUserNamePassword>true</useCustomUserNamePassword>";
                                $xmlinput .= "<accessDeviceCredentials>
                                        <userName>" . $row["username"] . "</userName>
                                        <password>" . $row["password"] . "</password>
                                    </accessDeviceCredentials>";
                                $xmlinput .= xmlFooter();
                                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                                $errorResult = readError($xml);
                            }
                        }
             */
        }
	}
	else
	// MAC change or custom profile change
	{
        if ($deviceType != "")
        {
            if ($macChange) {
                // Change MAC address
                $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyRequest14");
                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
                $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
                $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
                $xmlinput .= $macAddress != "" ? "<macAddress>" . $macAddress . "</macAddress>" : "<macAddress xsi:nil=\"true\" />";
                $xmlinput .= xmlFooter();
                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                $errorResult = readError($xml);
            }

            // Change Custom Tags
            if ($customProfileChange) {
                $userOpDBOperation = new UserModifyDBOperations();
                $tagManager = new UserCustomTagManager($deviceName,$deviceType);
                $existingCustomTag = $tagManager->getUserCustomTagList();
                
                // Remove old custom profile tags
                $resultMsg = removeDeviceCustomTags($deviceType, $deviceName, $_SESSION["userInfo"]["customProfile"], $_SESSION ["userInfo"]["modTagBundle"]);
                if ($resultMsg != "") {
                    $data = $resultMsg;
                }
            
                
                if($systemDefaultTagVal != $_POST["customProfile"]){
                    if (($errMsg = addDeviceCustomTag($deviceName, ExpressPhoneProfileName, $_POST["customProfile"])) != "") {
                        return $errMsg;
                    }
                    
                }
                if (($errMsg = addDeviceCustomTag($deviceName, ExpressCustomProfileName, $_POST["customProfile"])) != "") {
                    return $errMsg;
                }
                
            

                // Add new custom profile tags
                //$resultMsg = addDeviceCustomTags($deviceType, $deviceName, $_POST["customProfile"], "");
                //if ($resultMsg != "") {
                    //$data = $resultMsg;
                //}
                if(isset($diffArr["removed"]) && count($diffArr["removed"]) > 0){
                    $tagToBeDeleteIfBundleChanged = $userOpDBOperation->getAllCustomTagsFromTagBundles($diffArr["removed"]);
                    
                    $resultMsg = removeDeviceCustomTagsIfTagBundleChanged($deviceType, $deviceName, $tagToBeDeleteIfBundleChanged);
                    if ($resultMsg != "") {
                        $data = $resultMsg;
                    }
                }
                if(isset($diffArr["assigned"]) && count($diffArr["assigned"]) > 0){
                   
                    $tagToBeAddIfBundleChanged = $userOpDBOperation->getAllCustomTagsFromTagBundles($diffArr["assigned"]);
                    
                    $diffrencedArray = compareExistingTagsWithBundleTags($existingCustomTag, $tagToBeAddIfBundleChanged);
                    if(isset($diffrencedArray["add"]) && count($diffrencedArray["add"]) > 0){
                        $resultMsg = addDeviceCustomTagsIfBundleChanged($deviceType, $deviceName, $diffrencedArray["add"]);
                        if ($resultMsg != "") {
                            $data = $resultMsg;
                        }
                    }
                    if(isset($diffrencedArray["modify"]) && count($diffrencedArray["modify"]) > 0){
                        foreach ($diffrencedArray["modify"] as $key => $val){
                            $resultMsg = $tagManager->modifyCustomTag($key, $val);
                            if ($resultMsg != "") {
                                $data = $resultMsg;
                            }
                        }
                    }
                    
                }
                
                $arr = array(ExpressTagBundleName => "");
                $arrBundleChange = array(ExpressTagBundleName => implode(";", $_POST["modTagBundle"]));
                removeDeviceCustomTagsIfTagBundleChanged($deviceType, $deviceName, $arr);
                if(count($_POST["modTagBundle"]) > 0){
                    addDeviceCustomTagsIfBundleChanged($deviceType, $deviceName, $arrBundleChange);
                }
            }
            if($tagBundleChanged && !$customProfileChange){
                $tagManager = new UserCustomTagManager($deviceName,$deviceType);
                $existingCustomTag = $tagManager->getUserCustomTagList();
                $userOpDBOperation = new UserModifyDBOperations();
                
                $arr = array(ExpressTagBundleName => "");
                $arrBundleChange = array(ExpressTagBundleName => implode(";", $_POST["modTagBundle"]));
                removeDeviceCustomTagsIfTagBundleChanged($deviceType, $deviceName, $arr);
                if(count($_POST["modTagBundle"]) > 0){
                    addDeviceCustomTagsIfBundleChanged($deviceType, $deviceName, $arrBundleChange);
                }
                
                if(isset($diffArr["removed"]) && count($diffArr["removed"]) > 0){
                    $tagToBeDeleteIfBundleChanged = $userOpDBOperation->getAllCustomTagsFromTagBundles($diffArr["removed"]);
                   
                    $resultMsg = removeDeviceCustomTagsIfTagBundleChanged($deviceType, $deviceName, $tagToBeDeleteIfBundleChanged);
                    if ($resultMsg != "") {
                        $data = $resultMsg;
                    }
                }
                /*if(isset($diffArr["assigned"]) && count($diffArr["assigned"]) > 0){
                    $tagToBeAddIfBundleChanged = $userOpDBOperation->getAllCustomTagsFromTagBundles($diffArr["assigned"]);
                   
                    $resultMsg = addDeviceCustomTagsIfBundleChanged($deviceType, $deviceName, $tagToBeAddIfBundleChanged);
                    if ($resultMsg != "") {
                        $data = $resultMsg;
                    }
                }*/
                
                if(isset($diffArr["assigned"]) && count($diffArr["assigned"]) > 0){
                    
                    $tagToBeAddIfBundleChanged = $userOpDBOperation->getAllCustomTagsFromTagBundles($diffArr["assigned"]);
                    
                    $diffrencedArray = compareExistingTagsWithBundleTags($existingCustomTag, $tagToBeAddIfBundleChanged);
                    if(isset($diffrencedArray["add"]) && count($diffrencedArray["add"]) > 0){
                        $resultMsg = addDeviceCustomTagsIfBundleChanged($deviceType, $deviceName, $diffrencedArray["add"]);
                        if ($resultMsg != "") {
                            $data = $resultMsg;
                        }
                    }
                    if(isset($diffrencedArray["modify"]) && count($diffrencedArray["modify"]) > 0){
                        foreach ($diffrencedArray["modify"] as $key => $val){
                            $resultMsg = $tagManager->modifyCustomTag($key, $val);
                            if ($resultMsg != "") {
                                $data = $resultMsg;
                            }
                        }
                    }
                    
                }
                
            }
        }
	}

	if(isset($_POST["userTypeResult"]) && $_POST["userTypeResult"] == "analog") {
	    // Delete previous Device.
	    if($_POST["previousDeviceName"] != "" && $deviceNameForAnalog != $_POST["previousDeviceName"]) {
	        $isDeviceDeleted = deleteOlderDevice($_POST["previousDeviceName"]);
	        if($isDeviceDeleted == "Success") {
	            $message = $_POST["previousDeviceName"]. " Device Has been deleted.";
	        }
	    }
	}
	
	if ($userHasDevice) {
        $rebuildOption = "";
        
        if (isset($changes["device"]["rebuildPhoneFiles"])) {
            $rebuildOption = "rebuildOnly";
        }
        if (isset($changes["device"]["resetPhone"])) {
            $rebuildOption = $rebuildOption == "" ? "resetOnly" : "rebuildAndReset";
        }
		
        if ((isset($changes["device"]["rebuildPhoneFiles"]) && $changes["device"]["rebuildPhoneFiles"] == "true") && (isset($changes["device"]["resetPhone"]) && $changes["device"]["resetPhone"] == "true")) {
        	rebuildAndResetDeviceConfig($deviceName, $rebuildOption);
        }else{
        	if(!isset($changes["device"]["rebuildPhoneFiles"])){
        		$changes["device"]["rebuildPhoneFiles"] = "false";
        	}
        	if(!isset($changes["device"]["resetPhone"])){
        		$changes["device"]["resetPhone"] = "false";
        	}
	        $sp = $_SESSION["sp"];
	        $groupId = $_SESSION["groupId"];
	        
	        $deviceName = isset($changes["device"]["deviceName"]) ? $changes["device"]["deviceName"] : $deviceName;
	        rebuildResetDeviceUserModify($changes["device"], $sp, $groupId, $deviceName);
        }
    }
    
    // Delete older deviec when device updated
    function deleteOlderDevice($previousDeviceName)
    {
            global $db, $deleteAnalogUserDevice;
            global $sessionid, $client;
            $deviceName = $previousDeviceName;
            $response = "";
            if ($deviceName != "" && getNumberOfAssignedPorts($deviceName) == 0 && $deleteAnalogUserDevice)
            {
                if(deviceExists($deviceName))
                {
                    $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceDeleteRequest");
                    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
                    $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
                    $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
                    $xmlinput .= xmlFooter();
                    $response = $client->processOCIMessage(array("in0" => $xmlinput));
                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                    if(readError($xml) !=""){
                        $response = $xml;
                    }else{
                        $response = "Success";
                    }
                }
                
            }
            return $response;
    }

    /*Start New Code*/
    function getLinePortDevTypeLookup() {
        global $db;
        $deviceSubList = array();
        //Code added @ 16 July 2019
        $whereCndtn = "";
        if($_SESSION['cluster_support']){
            $selectedCluster = $_SESSION['selectedCluster'];
            $whereCndtn = " WHERE clusterName ='$selectedCluster' ";
        }
        //End code
		
        $qryLPLU = "select * from systemDevices $whereCndtn ";
        $select_stmt = $db->prepare($qryLPLU);
        $select_stmt->execute();
        $i = 0;
        while($rs = $select_stmt->fetch())
        {
            $deviceSubList[$i]['deviceType'] = $rs['deviceType'];
            $deviceSubList[$i]['substitution'] = $rs['nameSubstitution'];
            $i++;
        }
        return $deviceSubList;
    }
    
    function replaceDevNameWithSubstitute($deviceName) {
        $newDeviceName = $deviceName;
        $subList = getLinePortDevTypeLookup();
        foreach($subList as $key => $value) {
            if(strpos($deviceName, $value['deviceType']) !== false) {
                $newDeviceName = str_replace($value['deviceType'], $value['substitution'], $deviceName);
            }
        }
        return $newDeviceName;
    }
    
    function modifyLinePort($userId, $deviceName, $lineport) {
        global $sessionid, $client;
        
        $xmlinput = xmlHeader($sessionid, "UserModifyRequest17sp4");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= "<endpoint>
					<accessDeviceEndpoint>
						<accessDevice>
							<deviceLevel>Group</deviceLevel>";
        
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= "</accessDevice>
						<linePort>" . $lineport . "</linePort>";
        if( isset($_POST["portNumber"]) && $_POST["portNumber"] != "") {
            $xmlinput .= "<portNumber>" .$_POST["portNumber"]. "</portNumber>";
        }
        $xmlinput .="</accessDeviceEndpoint></endpoint>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        $errMsg = "";
        if (readError($xml) != "") {
            $errMsg = "Failed to Modify LinePort of Device Profile" . $deviceName . " .";
            $errMsg .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
            if ($xml->command->detail)
            {
                $errMsg .= "%0D%0A" . strval($xml->command->detail);
            }
        }
        return $errMsg;
    }
    /*End New Code*/
?>