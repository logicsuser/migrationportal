<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();

	$exp = explode("\r\n", $_POST["data"]);
	unset($exp[count($exp) - 1]); //remove blank value from end of array
	unset($exp[0]); //remove header row
?>
<script>
	<?php
		foreach ($exp as $v)
		{
				$e = explode(",", $v);

				$dropdown = "";
				for ($i = 0; $i <= 99; $i++)
				{
					$pad = str_pad($i, 2, "0", STR_PAD_LEFT);
					$dropdown .= "<option value=\\\"" . $pad . "\\\"";
					if ($pad == str_pad($e["0"], 2, "0", STR_PAD_LEFT))
					{
						$dropdown .= " SELECTED";
					}
					$dropdown .= ">" . $pad . "</option>";
				}
			?>

			$(".speedDials").append("<div class=\"row\">");
			$(".speedDials").append("<div class=\"col-md-3\"><div class=\"form-group\"><div class=\"dropdown-wrap\"><select name=\"speedCode[]\" id=\"speedCode[]\"><?php echo $dropdown; ?></select></div></div></div>");
			$(".speedDials").append("<div class=\"col-md-3\"><div class=\"form-group\"><input type=\"text\" name=\"phone[]\" value=\"<?php echo $e["1"]; ?>\" size=\"35\" maxlength=\"161\" id=\"phone[]\"></div></div>");
			$(".speedDials").append("<div class=\"col-md-3\"><div class=\"form-group\"><input type=\"text\" name=\"description[]\" value=\"<?php echo $e["2"]; ?>\" size=\"25\" maxlength=\"25\" id=\"description[]\"></div></div>");
			$(".speedDials").append("<div class=\"col-md-3\"><div class=\"form-group\">&nbsp;</div></div>");
			$(".speedDials").append("</div>");
			<?php
		}
	?>
</script>
