<?php
       
        if(isset($_POST['clusterName']) && $_POST['clusterName'] != ""){
             $selectedClusterName = $_POST['clusterName'];
             require_once("/var/www/lib/broadsoft/adminPortal/loginClusterBise.php");                
             $sessionid = $sessionidForSelCluster;
             $client    = $clientForSelCluster;
             $_SESSION['selectedCluster'] = $_POST['clusterName'];
        }else{
            require_once("/var/www/lib/broadsoft/login.php");
        }
        
	
	checkLogin();

	require_once "functions.php";
    //echo "<pre>"; print_r($_POST); die;
	if (isset($_POST["Selsp"]) && $_POST["Selsp"] != "") {
	    $_SESSION["sp"] = $_POST["Selsp"];
    } else {
	    unset($_SESSION["sp"]);
    }

    $serP = $_SESSION["sp"];
    
    //     $_POST['groupName'] = htmlspecialchars($_POST['groupName']);
    require_once("/var/www/lib/broadsoft/adminPortal/getAllGroups.php");    
    if (! in_array($_POST["groupName"],$allGroups)) {
        echo "1";
	    setSuperUserPermissions();            
            //Code added @ 22 Feb 2019
            if($_SESSION['superUser'] == "3")
            {
                //unset($_SESSION["permissions"]);            
                if($_SESSION['superUser'] =="3"){ 
                    $queryEFP = "SELECT pf.*, p.sp from permissions p LEFT JOIN `enterpriseFeaturesPermissions` pf ON pf.userId = p.userId where p.userId='" . $_SESSION["adminId"] . "'";
                 }        
                $sthEFP = $db->query($queryEFP);        
                while ($rs = $sthEFP->fetch())
                {
                        if($rs['modifyFlag'] == NULL || $rs['modifyFlag'] == ""){
                            $rs['modifyFlag'] = "no";
                        }                
                        if($rs['addDeleteFlag'] == NULL || $rs['addDeleteFlag'] == ""){
                            $rs['modifyFlag'] = "no";
                        }                
                        $_SESSION["permissions"][$rs["permissionId"]] = "1_".$rs['modifyFlag']."_".$rs['addDeleteFlag'];
               }
            }
            //End code
            //print_r($_SESSION["permissions"]);
    }
    else {  
        unset($_SESSION["permissions"]);
        
        $_SESSION["groupId"] = $_POST["groupName"];
        $_SESSION["previousSP"] = $_SESSION["sp"];
        //check if service provider is an enterprise
        $xmlinput = xmlHeader($sessionid, "ServiceProviderGetRequest17sp1");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $_SESSION["enterprise"] = strval($xml->command->isEnterprise);

        // Create list of Service Packs containing 'Voice Messaging User' service
        require_once("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");
        unset($_SESSION["vmServicePacks"]);
        $i = 0;
        foreach ($servicePacks as $servicePack) {
            $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]). "</serviceProviderId>";
            $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

            foreach ($xml->command->userServiceTable->row as $key => $value){
                if (strval($value->col[0]) == "Voice Messaging User") {
                    $_SESSION["vmServicePacks"][$i++] = $servicePack;
                    break;
                }
            }
        }
        if (isset($_SESSION["vmServicePacks"])) {
            $_SESSION["vmServicePacksFlat"] = join(",", $_SESSION["vmServicePacks"]);
        }
        
        
        
        //Code added @ 10 July 2018
        //unset($_SESSION["scaServicePacksArr"]);
        //unset($_SESSION["scaServicePacksStr"]);
        foreach ($servicePacks as $key => $servicePack) 
        {
            $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]). "</serviceProviderId>";
            $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);                
            foreach ($xml->command->userServiceTable->row as $key => $value)
            {
                $pos = strpos(strval($value->col[0]), "Shared Call Appearance");
                if($pos === false)
                {
                    //continue;
                }
                else
                {                        
                    $scaServicePackArray[$count] = $servicePack;
                    $count++;
                }
            }
        }
        $scaServicePackArray = array_unique($scaServicePackArray);
        $_SESSION["scaServicePacksArr"] = $scaServicePackArray;
        $_SESSION["scaServicePacksStr"] = join(",", $scaServicePackArray);
        //End Code

        // Get Group Policies and Rules
        $xmlinput = xmlHeader($sessionid, "GroupPasswordRulesGetRequest16");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $_SESSION["groupRules"]["minPasswordLength"] = strval($xml->command->minLength);

        // get system default domain
        $xmlinput = xmlHeader($sessionid, "SystemDomainGetListRequest");
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $_SESSION["systemDefaultDomain"] = strval($xml->command->systemDefaultDomain);

        // get groupDomain
        //TODO: Implement check whether requires group domain is checked - this will also require resolving userID name builder to fallback from groupDomain to defaultDomain
        //MySQL database connections
        require_once("/var/www/html/Express/db.php");

        // Get Group Info
        require_once("/var/www/lib/broadsoft/adminPortal/getGroupInfo.php");
        require_once("/var/www/lib/broadsoft/adminPortal/DBSimpleReader.php");
        require_once("/var/www/lib/broadsoft/adminPortal/DBLookup.php");

        // Determine Group Default Domain
        $systemConfig = new DBSimpleReader($db, "systemConfig");
        $_SESSION["defaultDomain"] = $systemConfig->get("groupsDomainType") == "Static" ?
            $systemConfig->get("staticDomain") : $_SESSION["groupInfo"]["defaultDomain"];

        // Determine Group Proxy Domain
        switch ($proxyDomainType) {
            case "Static":
                $_SESSION["proxyDomain"] = $staticProxyDomain;
                break;
            case "Default":
                $_SESSION["proxyDomain"] = $_SESSION["defaultDomain"];
                break;
        }

        // Permission fetched from featuresPermissions table.
        if($_SESSION['superUser'] == "0"){    
            $queryPF = "SELECT pf.permissionId, p.sp from permissions p LEFT JOIN featuresPermissions pf ON pf.userId = p.userId where p.userId='" . $_SESSION["adminId"] . "'";
            if ($securityDomainPattern == "") {
                $queryPF .= " and p.groupId='" . $_POST["groupName"] . "'";
            }
            
            $sthPF = $db->query($queryPF);
            while ($row = $sthPF->fetch())
            { 
                $_SESSION["permissions"][$row["permissionId"]] = 1;
            
                if ($securityDomainPattern == "") {
                    $_SESSION["sp"] = $row["sp"];
                } 
            }
        }      
        
        if($_SESSION['superUser'] =="3"){ 
             $queryPF = "SELECT pf.*, p.sp from permissions p LEFT JOIN `enterpriseFeaturesPermissions` pf ON pf.userId = p.userId where p.userId='" . $_SESSION["adminId"] . "'";
            
            $sthPF = $db->query($queryPF);
        
            while ($row = $sthPF->fetch())
            {

                if($_SESSION['superUser']=="3"){ 

                    if ($securityDomainPattern == "") {
                        if(trim($_SESSION["sp"]) == ""){
                            $_SESSION["sp"] = $row["sp"];
                        }
                        //$_SESSION["sp"] = $row["sp"];
                        //$_SESSION["modifyFlag"] = $row["modifyFlag"];
                        //$_SESSION["addDeleteFlag"] = $row["addDeleteFlag"];
                    } 

                    if($row['modifyFlag'] == NULL || $row['modifyFlag'] == ""){
                        $row['modifyFlag'] = "no";
                    }

                    if($row['addDeleteFlag'] == NULL || $row['addDeleteFlag'] == ""){
                        $row['modifyFlag'] = "no";
                    }

                    $_SESSION["permissions"][$row["permissionId"]] = "1_".$row['modifyFlag']."_".$row['addDeleteFlag'];
                   
                }            
            } 
        } 
	setSuperUserPermissions();
        //to set group extension limit as requirement given by client developed by Sollogics
        require_once("/var/www/lib/broadsoft/adminPortal/getGroupExtensionLength.php");
        require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");

        $extLen = new GroupExtensionLength();
        $extensionLengthResponse= $extLen->getGroupExtensionLength();
        $_SESSION['groupExtensionLength']['min'] = intval($extensionLengthResponse['Success']->minExtensionLength);
        $_SESSION['groupExtensionLength']['max'] = intval($extensionLengthResponse['Success']->maxExtensionLength);
        $_SESSION['groupExtensionLength']['default'] = intval($extensionLengthResponse['Success']->defaultExtensionLength);
        //end set group extension limit        
    }

    //Save the SP and Group Id in database for use with next login.
	set_group_selected_user();
     echo "0";
     
     

    function setSuperUserPermissions() {       
	    if ($_SESSION["superUser"] == "1" && isset($_SESSION["sp"])) {
                        $_SESSION["permissions"]["basicUser"] = "1";
                        $_SESSION["permissions"]["resetPasswords"] = "1";
                        $_SESSION["permissions"]["deleteUsers"] = "1";
                        $_SESSION["permissions"]["changeDevice"] = "1";
                        $_SESSION["permissions"]["clearMACAddress"] = "1";
                        $_SESSION["permissions"]["voiceManagement"] = "1";
                        $_SESSION["permissions"]["changeHotel"] = "1";
                        $_SESSION["permissions"]["changeForward"] = "1";
                        $_SESSION["permissions"]["changeblf"] = "1";
                        $_SESSION["permissions"]["changeSCA"] = "1";
                        $_SESSION["permissions"]["modhuntgroup"] = "1";
                        $_SESSION["permissions"]["addusers"] = "1";
                        $_SESSION["permissions"]["modifyUsers"] = "1";
                        $_SESSION["permissions"]["modcallCenter"] = "1";
                        $_SESSION["permissions"]["groupWideUserModify"] = "1";
                        $_SESSION["permissions"]["aamod"] = "1";
                        $_SESSION["permissions"]["announcements"] = "1";
                        $_SESSION["permissions"]["modCcd"] = "1";
                        $_SESSION["permissions"]["modschedule"] = "1";
                        $_SESSION["permissions"]["viewcdrs"] = "1";
                        $_SESSION["permissions"]["viewusers"] = "1";
                        $_SESSION["permissions"]["viewnums"] = "1";
                        $_SESSION["permissions"]["viewChangeLog"] = "1";
                        $_SESSION["permissions"]["adminUsers"] = "1";
                        $_SESSION["permissions"]["vdmLight"] = "1";
			$_SESSION["permissions"]["modifyGroup"] = "1";
			$_SESSION["permissions"]["callPickupGroup"] = "1";
			$_SESSION["permissions"]["vdmAdvanced"] = "1";
			$_SESSION["permissions"]["scaOnlyDevice"] = "1";
			$_SESSION["permissions"]["siteReports"] = "1";
			$_SESSION["permissions"]["sasTest"] = "1";
			$_SESSION["permissions"]["expressSheets"] = "1";

			$_SESSION["permissions"]["groupBasicInfo"] = "1";
			$_SESSION["permissions"]["groupDomains"] = "1";
			$_SESSION["permissions"]["groupServices"] = "1";
			$_SESSION["permissions"]["groupDN"] ="1";
			$_SESSION["permissions"]["groupPolicies"] = "1";
			$_SESSION["permissions"]["groupNCOS"] = "1";
			$_SESSION["permissions"]["groupVoicePortal"] = "1";
			$_SESSION["permissions"]["voiceMailPasscode"] = "1";

			$_SESSION["permissions"]["activateNumber"] = "1";

			$_SESSION["permissions"]["devCustomTags"]  =  "1";
			$_SESSION["permissions"]["srvSpeedDial8"]  =  "1";
			$_SESSION["permissions"]["srvSpeedDial100"] =  "1";

			$_SESSION["permissions"]["devVdmSoftKeys"] = "1";
			$_SESSION["permissions"]["devVdmCodecs"] = "1";
			$_SESSION["permissions"]["devVdmFeatures"] = "1";
			
			//Policies permissions.
			$_SESSION["permissions"]["userPolicyCallLimits"]   = "1";
			$_SESSION["permissions"]["userPolicyCLID"]  = "1";
			$_SESSION["permissions"]["userPolicyEmergencyCalls"] = "1";
			$_SESSION["permissions"]["userPolicyIncomingCLID"] =  "1";
                        
			//Code added @ 02 May 2018
			$_SESSION["permissions"]["timeZone"] = "1";
			$_SESSION["permissions"]["userNCOS"] = "1";
			$_SESSION["permissions"]["userCallingLineIdNumber"] = "1";
			$_SESSION ["permissions"]["servicePacks"] = "1";
			//End Code			
                        $_SESSION["permissions"]["userAddress"] = "1";
                        $_SESSION ["permissions"]["deviceInventory"] = "1";
                        
                        //Code added @ 20 August 2018
			$_SESSION["permissions"]["ocpGroup"] = "1";
			$_SESSION["permissions"]["ocpUser"] = "1";
			/*device config custom tag */
			$_SESSION["permissions"]["deviceConfigCustomTags"] = "1";
                        $_SESSION["permissions"]["simRing"] = "1"; //Code added @ 25 Nov 2018
                        
                        
                        //superUser 3 permission added , enterprise feature permission
                        $_SESSION["permissions"]["basicInformation"]            = "1_yes_yes";
                        $_SESSION["permissions"]["domains"]                     = "1_yes_no";
                        $_SESSION["permissions"]["securityDomains"]             = "1_yes_no";
                        $_SESSION["permissions"]["services"]                    = "1_yes_no";
                        $_SESSION["permissions"]["DNs"]                         = "1_yes_yes";
                        $_SESSION["permissions"]["callProcessingPolicies"]      = "1_yes_no";
                        $_SESSION["permissions"]["networkClassesofService"]     = "1_yes_no";
                        $_SESSION["permissions"]["voicePortalService"]          = "1_yes_no";
                        $_SESSION["permissions"]["outgoingCallingPlan"]         = "1_yes_no";
                        $_SESSION["permissions"]["users"]                       = "1_no_no";
                        $_SESSION["permissions"]["devices"]                     = "1_no_yes";
                        $_SESSION["permissions"]["entAnnouncements"]            = "1_yes_yes";
                        $_SESSION["permissions"]["entServicePacks"]             = "1_yes_yes";
                        $_SESSION["permissions"]["cdr"]                         = "1_no_no";
                        $_SESSION["permissions"]["broadWorksLicenseReport"]     = "1_no_no";
                        $_SESSION["permissions"]["phoneProfiles"]               = "1_no_no";
                        $_SESSION["permissions"]["changeLog"]                   = "1_no_no";
                        $_SESSION["permissions"]["administrators"]              = "1_yes_yes";
                        $_SESSION["permissions"]["adminLogs"]                   = "1_no_no";
                        
                        
                        //Code added @ 05 March 2019
                        $_SESSION["permissions"]["groupAdminUserPermission"]        = "1_yes_yes";   //Group Admin User Permissions
                        $_SESSION["permissions"]["groupAdminServicesPermission"]    = "1_no_no";     //Group Admin Services Permissions
                        $_SESSION["permissions"]["groupAdminDevicesPermission"]     = "1_yes_yes";   //Group Admin Devices Permissions
                        $_SESSION["permissions"]["groupAdminSupervisoryPermission"] = "1_no_no";     //Group Admin Supervisory Permissions
                        //End code
                        
                        
                        //enterprise feature permission
                        $_SESSION["permissions"]["entPermBasicInformation"]             = "1_yes_yes";
                        $_SESSION["permissions"]["entPermDomains"]                      = "1_yes_yes";                        
                        $_SESSION["permissions"]["entPermExpressAdministrators"]        = "1_yes_yes";
                        $_SESSION["permissions"]["entPermDNs"]                          = "1_yes_yes";
                        $_SESSION["permissions"]["entPermBWAdministrators"]             = "1_yes_yes";
                        
                        $_SESSION["permissions"]["entPermSecurityDomains"]              = "1_yes_no";
                        $_SESSION["permissions"]["entPermServices"]                     = "1_yes_no";
                        $_SESSION["permissions"]["entPermCallProcessingPolicies"]       = "1_yes_no";
                        $_SESSION["permissions"]["entPermDialPlanPolicy"]               = "1_yes_no";
                        $_SESSION["permissions"]["entPermVoiceMessaging"]               = "1_yes_no";
                        $_SESSION["permissions"]["entPermVoicePortal"]                  = "1_yes_no";
                        $_SESSION["permissions"]["entPermNetworkClassesofService"]      = "1_yes_no";
                        $_SESSION["permissions"]["entPermRoutingProfile"]               = "1_yes_no";
                        $_SESSION["permissions"]["entPermPasswordRules"]                = "1_yes_no";
                        //end permission 
                        
                        
                        
                        /*
                        //Code added @ 04 March 2019
                        $_SESSION["permissions"]["basicInfoUser"]               = "1_yes_no";
                        $_SESSION["permissions"]["entDeleteUsers"]              = "1_no_yes";
                        $_SESSION["permissions"]["entAddUsers"]                 = "1_no_yes";
                        $_SESSION["permissions"]["entModifyUsers"]              = "1_yes_no";
                        $_SESSION["permissions"]["groupWideUsersModify"]        = "1_no_yes";
                        //$_SESSION["permissions"]["entViewUsers"]                = "1_no_no";
                        $_SESSION["permissions"]["userExpressSheets"]           = "1_no_yes";
                        $_SESSION["permissions"]["entActivateNumber"]           = "1_yes_no";
                        $_SESSION["permissions"]["userServicePacks"]            = "1_yes_no";
                        $_SESSION["permissions"]["entUserAddress"]              = "1_yes_no";
                        $_SESSION["permissions"]["outgoingCallingPlanUser"]     = "1_yes_no";
                        $_SESSION["permissions"]["entTimeZone"]                 = "1_yes_no";
                        $_SESSION["permissions"]["networkClassofService"]       = "1_yes_no";
                        $_SESSION["permissions"]["incomingCallerIDPolicy"]      = "1_yes_no";
                        $_SESSION["permissions"]["emergencyCallsPolicy"]        = "1_yes_no";
                        $_SESSION["permissions"]["callingLineIDPolicy"]         = "1_yes_no";
                        $_SESSION["permissions"]["callLimitsPolicy"]            = "1_yes_no";
                        $_SESSION["permissions"]["callingLineIDPhoneNumber"]    = "1_yes_no";                        
                        $_SESSION["permissions"]["entServices"]                 = "1_no_no";                        
                        $_SESSION["permissions"]["phoneDevice"]                 = "1_yes_no";
                        //$_SESSION["permissions"]["readOnlyDeviceInformation"]   = "1_no_no";
                        $_SESSION["permissions"]["deviceManagement"]            = "1_yes_no";
                        $_SESSION["permissions"]["entClearMACAddress"]          = "1_yes_no";
                        $_SESSION["permissions"]["advancedDeviceManagement"]    = "1_yes_no";
                        $_SESSION["permissions"]["scaOnlyDevices"]              = "1_no_yes";
                        $_SESSION["permissions"]["customTags"]                  = "1_no_yes";
                        $_SESSION["permissions"]["entDeviceConfigCustomTags"]   = "1_no_yes";
                        $_SESSION["permissions"]["deviceManagementFeatures"]    = "1_yes_no";
                        $_SESSION["permissions"]["deviceManagementSoftKeys"]    = "1_yes_no";
                        $_SESSION["permissions"]["deviceManagementCodecs"]      = "1_yes_no";
                        $_SESSION["permissions"]["entDeviceInventory"]          = "1_no_yes";                        
                        $_SESSION["permissions"]["supervisory"]                 = "1_no_no";
                        //End code 
                        */           
	    }
            
            //Code added @ 20 Feb 2019
            if ($_SESSION["superUser"] == "3" && isset($_SESSION["sp"])) {
                    //Managing All Group Level Permissions on Group Main Page
                    /*
                    $_SESSION["permissions"]["basicUser"]                       = "1";           //Users  basicInfoUser  		    
		    $_SESSION["permissions"]["deleteUsers"]                     = "1";           //Users  entDeleteUsers
                    $_SESSION["permissions"]["viewusers"]                       = "1";           //Users  viewusers
		    $_SESSION["permissions"]["expressSheets"]                   = "1";           //Users  userExpressSheets
		    $_SESSION["permissions"]["addusers"]                        = "1";           //Users  entAddUsers
		    $_SESSION["permissions"]["modifyUsers"]                     = "1";           //Users  entModifyUsers
                    $_SESSION["permissions"]["groupWideUserModify"]             = "1";           //Users  groupWideUsersModify
                    $_SESSION["permissions"]["userPolicyCallLimits"]            = "1";           //Users  callLimitsPolicy
                    $_SESSION["permissions"]["userPolicyCLID"]                  = "1";           //Users  callingLineIDPolicy
                    $_SESSION["permissions"]["userPolicyEmergencyCalls"]        = "1";           //Users  emergencyCallsPolicy
                    $_SESSION["permissions"]["userPolicyIncomingCLID"]          =  "1";          //Users  incomingCallerIDPolicy
                    $_SESSION["permissions"]["timeZone"]                        = "1";           //Users  entTimeZone
                    $_SESSION["permissions"]["userNCOS"]                        = "1";           //Users  networkClassofService
                    $_SESSION["permissions"]["userCallingLineIdNumber"]         = "1";           //Users  callingLineIDPhoneNumber
                    $_SESSION ["permissions"]["servicePacks"]                   = "1";           //Users  userServicePacks
                    $_SESSION["permissions"]["userAddress"]                     = "1";           //Users  entUserAddress
                    $_SESSION["permissions"]["activateNumber"]                  = "1";           //Users  entActivateNumber
                    $_SESSION["permissions"]["ocpUser"]                         = "1";           //Users  outgoingCallingPlanUser
                                        
                    
                    $_SESSION["permissions"]["aamod"]                           = "1";            //Services	
                    $_SESSION["permissions"]["changeblf"]                       = "1";            //Services
                    $_SESSION["permissions"]["modcallCenter"]                   = "1";            //Services
                    $_SESSION["permissions"]["modhuntgroup"]                    = "1";            //Services            
		    $_SESSION["permissions"]["modCcd"]                          = "1";            //Services   
		    $_SESSION["permissions"]["modschedule"]                     = "1";            //Services   		                          
                    $_SESSION["permissions"]["callPickupGroup"]                 = "1";            //Services   
                    $_SESSION["permissions"]["srvSpeedDial8"]                   = "1";            //Services  
                    $_SESSION["permissions"]["srvSpeedDial100"]                 = "1";            //Services  
                    $_SESSION["permissions"]["ocpGroup"]                        = "1";            //Services
                    $_SESSION["permissions"]["simRing"]                         = "1";            //Services                    
		    $_SESSION["permissions"]["changeForward"]                   = "1";            //Services
		    $_SESSION["permissions"]["changeHotel"]                     = "1";            //Services                    
		    $_SESSION["permissions"]["changeSCA"]                       = "1";	          //Services	
                    $_SESSION["permissions"]["voiceManagement"]                 = "1";            //Services
                    
                    
                    $_SESSION["permissions"]["scaOnlyDevice"]                   = "1";             //Devices   //SCA-Only Devices                 
                    $_SESSION["permissions"]["devCustomTags"]                   =  "1";            //Devices   //Custom Tags
                    $_SESSION["permissions"]["devVdmSoftKeys"]                  = "1";             //Devices   //Device Management - Soft Keys
                    $_SESSION["permissions"]["devVdmCodecs"]                    = "1";             //Devices   //Device Management - Codecs
                    $_SESSION["permissions"]["devVdmFeatures"]                  = "1";             //Devices   //Device Management – Features
                    $_SESSION["permissions"]["deviceConfigCustomTags"]          = "1";             //Devices   //Device Config. Custom Tags
                    $_SESSION["permissions"]["vdmLight"]                        = "1";             //Devices   //Device Management
                    $_SESSION["permissions"]["vdmAdvanced"]                     = "1";             //Devices   //Advanced Device Management
                    $_SESSION["permissions"]["changeDevice"]                    = "1";             //Devices   //Phone Devices 
		    $_SESSION["permissions"]["clearMACAddress"]                 = "1";             //Devices   //Clear MAC Address		      
                    $_SESSION ["permissions"]["deviceInventory"]                = "1";             //Devices   //Device Inventory
                    
                    
                    $_SESSION["permissions"]["sasTest"]                         = "1";              //Supervisory
                    $_SESSION["permissions"]["siteReports"]                     = "1";              //Supervisory
                    $_SESSION["permissions"]["viewnums"]                        = "1";              //Supervisory
                    $_SESSION["permissions"]["voiceMailPasscode"]               = "1";              //Supervisory  
                    $_SESSION["permissions"]["resetPasswords"]                  = "1";              //Supervisory
                    
                    $_SESSION["permissions"]["announcements"]                   = "1";              //Supervisory
                    $_SESSION["permissions"]["viewcdrs"]                        = "1";              //Supervisory
                    $_SESSION["permissions"]["viewChangeLog"]                   = "1";              //Supervisory
                    */
            }
            //End code
    }
    
?>
