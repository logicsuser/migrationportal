<?php
require_once("/var/www/html/Express/config.php");
checkLogin();
require_once("/var/www/html/Express/adminCheckFilter.php");
//unset($_SESSION["groupId"]);
redirectUser("enterprise");
$_SESSION["redirectUrl"] = "enterprise";
set_group_selected_user();
require_once("header.php");

/*
echo "<br /> bw_ews - ".$bw_ews;
echo "<br /> bw_ews - ".$bwuserid;																	//TODO: CHANGE NAME $bwuserid to $bwUserName
echo "<br /> bwuserid - ".$bwpasswd;																	//TODO: CHANGE NAME $bwpasswd to $bwPassword
echo "<br /> ociVersion - ".$ociVersion;																	//TODO: CHANGE NAME $ociVersion to $bwRelease
echo "<br /> bwAuthlevel - ".$bwAuthlevel;                              
echo "<br /> bwProtocol - ".$bwProtocol;   
echo "<br /> bwPort - ".$bwPort;       
echo "<br /> clusterName - ".$clusterName;
echo "<br /> bwAppServer - ".$bwAppServer;
*/

//echo "<br />SelectedCluster - ".$_SESSION['selectedCluster'];

?>

<script>
var module ='' ;
var headerNavigation ='';
var isCheckDataModify = false;
var checkResult = function(status) {
	isCheckDataModify = status;
}
var previousModPos = '';
var getPreviousModuleId = function(previousMod){
	previousModPos = previousMod ;
}
  function getAssignNumbersList(dataToSend) {
	var assignNumber = "";
    var tempArr = [];
    var dataToSendTemp = $("form#groupModify").serializeArray();
    dataToSendTemp.forEach(function(value,i) {
        if(value.name == "assignNumbers[]") {
        	tempArr.push(i);
			assignNumber += value.value + ";;";
        }
    });
    dataToSend.push({name:'assignNumbers', value: assignNumber});
    return dataToSend;
}

    $(function () {

        // *Start* Disable Browser's Back Button.
       
        var href = location.href;
    	var urlLastSegment = href.match(/([^\/]*)\/*$/)[1];
    	
        history.pushState(null, null, urlLastSegment);
        window.addEventListener('popstate', function () {
            history.pushState(null, null, urlLastSegment);
        });
        // *End*

        
        $("#changeGroup").click(function () {
            window.location.replace("chooseGroup.php");
        });

        var sp = "<?php echo $_SESSION["sp"]; ?>";
        var group = "<?php if(isset($_SESSION["groupId"])) { echo $_SESSION["groupId"]; }?>";
        var superUser = "<?php echo $_SESSION["superUser"]; ?>";

        //if(superUser !== "1" || sp === "" || sp === "None") { // code commenyted @ 20 Feb 2019
        var userType = [1,3];
        if((userType.indexOf(superUser) >= 0) || sp === "" || sp === "None") {
            window.location.replace("chooseGroup.php");
        }

		var sourceSwap = function () {
    		var $this = $('#'+this.id).find('.ImgHoverIcon');
    		if($('#'+this.id).find('.activeNav').length == 1 || this.id == previousActiveMenu){
    			return false;
    		}		
            var newSource = $this.data('alt-src');
            $this.data('alt-src', $this.attr('src'));
            $this.attr('src', newSource);
        }


        var activeImageSwap = function()
        {
           currentClickedDiv = this.id   
           if(previousActiveMenu != currentClickedDiv){
    			//active
          		if(previousActiveMenu != "")
        		{
        			var $thisPrev = $('#'+previousActiveMenu).find('.ImgHoverIcon');
        	        var newSource = $thisPrev.data('alt-src');
        	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
        	        $thisPrev.attr('src', newSource);
        		}
        		// inactive tab
        		if(currentClickedDiv != ""){
        			var $thisCurrent = $('#'+currentClickedDiv).find('.ImgHoverIcon');
        			var newImgSource = $thisCurrent.data('alt-src');
        			$thisCurrent.data('alt-src', newImgSource);
        			$thisCurrent.attr('src', $thisCurrent.attr('src'));
        			previousActiveMenu = currentClickedDiv;
            	}
            }
    	}
        
    	$('.rolloverIcon').hover(sourceSwap, sourceSwap);
    	$('.rolloverIcon').click(activeImageSwap, activeImageSwap);

    	
        var activeSideNavBar = function (module)
		{
    		previousActiveMenu = module;
			 var clickedClass = $("." + module + "_SideMenu"); //create swap image class name 
					clickedClass.addClass("activeNav");
					var thisId = clickedClass.find("img");
				        var newSource = thisId.data('alt-src');
				        thisId.data('alt-src', thisId.attr('src'));
				        thisId.attr('src', newSource);
		}
		
		
		var headerRedirectLink = function(module){
		//	$("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
           // $("#loading").show();
        if(module == "main_page_link"){ window.location.href = 'main.php'; }
			else if(module == "enterprise_main_page_link"){ window.location.href = 'main_enterprise.php'; }
			else if(module == "logout"){ window.location.href = '/Express/index.php'; }
			else if(module == "redirectHeaderNavResetPass"){ window.location.href = '/Express/reset_password.php'; }
			else if(module =="chooseGroupModal"){ $.ajax({url: "changeGroupDialog/index.php", success: function(result){
               $("#contentBody").html(result);
               $("#helpUrl").show();
               $("#chooseGroupDialogModal").modal('show'); 
    	}}); }
			else if(module =="logoImage"){ window.location.href = 'main_enterprise.php'; }
	 	
		}
		
	   var moduleFunctionRedirect = function(module) {
        	$("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
            $("#loading").show();
            $(".navMenu").removeClass("activeNav");
           // var module = $(this).attr("id");
            $(this).addClass("activeNav");
        	 var userType = '';
        	<?php if($_SESSION['superUser'] == 1){?>
                    userType = 'superUser';
                <?php }else if($_SESSION['superUser'] == 3){ ?>
                    userType = 'superUser';
                <?php }else{?>
                    userType = '<?php echo $_SESSION['adminType']?>';
        	<?php }?>
        	 activeSideNavBar(module);
        	  $.ajax({
	                 type: "POST",
	                 url: "navigate.php",
	                 data: {module: module},
	                 success: function (result) {

// 	                	 alert("qqqq");
// 	                     if( result.indexOf("fail_over_dialog_on_OciFailure") > -1) {
// 	                     	alert("wwwww");
// 	                     	fail_over_dialog_on_OciFailure();
// 	                    	}
	                    	
	                     $("#loading").hide();
	                     $("#mainBody").html(result);
	                     //pageGetHelp(module);
	                     $('#helpUrl').attr('data-module', module);
	                     $('#helpUrl').attr('data-adminType', userType);
	                 }
	             });
		} //end navigate redirection method

		$('.navigate,.RedirectNavHeader').click(function () {
			if(previousModPos == "enterpriseMod"){
				if($('#enterpriseModify').length){
					isCheckDataModify = true;
				}
				
			}
			 module = $(this).attr("id");
                         headerNavigation = this.className;
                          if(isCheckDataModify ==true){
	 			 if(module !='groupModify' ||  module =='groupModify'){
	 				console.log(previousModPos);		 
                if(previousModPos =="groupModify" || previousModPos =="updateServicePack" || previousModPos == "enterpriseMod"){
				 		
                if(previousModPos =="updateServicePack"){
                   
                    dataToSendSerial =  $("form#servicePackModDialogForm").serializeArray();
                    urlData = "servicePacks/validate.php";
                    ajaxData = "spadd" ;
                     
                }else if(previousModPos == "enterpriseMod"){
                	if($("#assignedNCS option").length > 0){
            			$('#assignedNCS option').prop('selected', true);
            		}
            		if($("#assignedDefault option").length > 0 && $('#assignedDefault option:selected').length == 0){
            			$('#assignedDefault option').prop('selected', true);
            		}
            		var dataToSend = $("form#enterpriseModify").not("#enterpriseGroupServiceTable, #enterpriseUserServiceTable").serializeArray();
            		dataToSend = makeServicePostData(dataToSend);
            		dataToSend = getUnassignNumbers(dataToSend);
            		ajaxData = dataToSend;
            		urlData = "enterprise/modifyValidate.php";
                } else{
                
                $("#groupDetails").show();
	  							var selectGroupId = $("#selectGroup").val();
	  							var groupArray = selectGroupId.split(" ");
	  					        var selectGroupId = groupArray[0];
	  					    	var groupId = $("#groupId").val();
	  					    	var timeZone = $("#timeZone").val();
	  					    	var userLimit = $("#userLimit").val();
	  					    	var minExtensionLength = $("#minExtensionLength").val();
	  					    	var maxExtensionLength = $("#maxExtensionLength").val();
	  					    	var defaultExtensionLength = $("#defaultExtensionLength").val();
	  					    	$("#activateNewAssigned").removeAttr("disabled");
	  					    	var modifyGroupId = $("#modifyGroupId").val();
	  					    	var spanGroupId = $("#spanGroupId").text();  
	  					    	 
	  					    	//if($("#assignNumbers option").length > 0 && $('#assignNumbers option:selected').length == 0){
	  					    	//$('#assignNumbers option').prop('selected', true); //Code commented @ 13 March 2019 regarding EX-1093
	  					    		//}
	  					    		if($("#assignedNCS option").length > 0 && $('#assignedNCS option:selected').length == 0){
	  					    			$('#assignedNCS option').prop('selected', true);
	  					    		}
	  					    		if($("#assignedDefault option").length > 0 && $('#assignedDefault option:selected').length == 0){
	  					    			$('#assignedDefault option').prop('selected', true);
	  					    		}
	  					    		if($("#assignedNCS option").length > 0 || $('#assignedNCS option:selected').length == 0){
	  					    			$('#assignedNCS option').prop('selected', true);
	  					    		}

	  						   // dataToSendSerial =  $("form#groupModify").serializeArray();
	  						//	urlData = "groupManage/groupModify/validate.php";
	  						//	ajaxData = {compare : 1, formdata : dataToSendSerial} ;
                                                                
                                                                
                                                                var dataToSendSerial = $("form#groupModify :input[name != 'assignNumbers[]']").serializeArray();
                                                                //dataToSendSerial = getAssignNumbersList(dataToSendSerial);
  						  //  dataToSendSerial =  $("form#groupModify").serializeArray();
  							urlData = "groupManage/groupModify/validate.php";
  							ajaxData = {compare : 1, formdata : dataToSendSerial, prevmodule: 'preventChanges'} ;
                                                                
	  						}
}
	  						$.ajax({
			        		type: "POST",
			        		url: urlData,
			        		data: ajaxData,
			        		success: function(result) {
	 							var errorExist = result.search("#FB6071");
	 							var noChangesExist = result.search("No Changes");
	 							var messageDialog = "Modify Group";
								 var noChangesExistSP = result.search("You have made no changes");
								 var enterpriseNoChanges = result.search("No changes");
								 if(previousModPos =="updateServicePack"){var messageDialog = "Service Pack";}
								 if(previousModPos =="enterpriseMod"){var messageDialog = "Enterprise";}
								if(noChangesExist =="No Changes" || noChangesExist >0 || noChangesExistSP =="You have made no changes" || enterpriseNoChanges > -1){
	 								 checkResult(false) ;
                                                                       
									 //moduleFunctionRedirect(module);
									 if(headerNavigation== 'RedirectNavHeader'){
                                                                            
                                                                                headerRedirectLink(module);
                                                                        }
                                                                        else{
                                                                                  
                                                                                 moduleFunctionRedirect(module);
                                                                        }
				    			}else{
				    				checkResult(true) ;
	    		        			$("#checkDataChangesModulePrevent").html('Are you sure you want to leave? <br/> You might lose any change you have made for this '+messageDialog+'.');
	    	        				$("#checkDataChangesModulePrevent").dialog('open');
	    	        				return false ;
	    		        		}
			    			 }
			        	});
					 } // end check return status data check
                                         else{
					if(headerNavigation== 'RedirectNavHeader'){
                                                                              
                                                                                headerRedirectLink(module);
                                                                        }
                                        //	getuserModToVdm("navToVDMLightModule") ;
						moduleFunctionRedirect(module);
					}
					
				}
				if(isCheckDataModify ==false){
				 if(headerNavigation== 'RedirectNavHeader'){
				 
                                        headerRedirectLink(module);
				}
				else{
					moduleFunctionRedirect(module);
				}
	
 			}

		});

		//dialog box code 
		  $("#checkDataChangesModulePrevent").dialog({
		  	autoOpen: false,
		  	width: 600,
		  	modal: true,
		  	position: { my: "top", at: "top" },
		  	resizable: false,
		  	closeOnEscape: false,
		  	buttons: {
		  		"Yes": function() {
		  			checkResult(false);
		  			 if(previousModPos =="navToVDMLightModule"){
						 getuserModToVdm("navToVDMLightModule") ;
						 getDeviceManageMentResultType(vdmDeviceName,vdmDeviceType,vdmType);
						 moduleFunctionRedirect(module);
					  }
		  		 	else if(headerNavigation== 'RedirectNavHeader'){
								headerRedirectLink(module);
					}
					else{
						moduleFunctionRedirect(module);
					}
		  			$(this).dialog("close");
				},
		  		"No": function() {
		  			checkResult(true);
		  			$(this).dialog("close");
		  			return false; 
		  		} 
		  	},  
		      open: function() {
                        $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019
			setDialogDayNightMode($(this));
		      	$(".ui-dialog-buttonpane button:contains('Yes')").button().show().addClass("subButton");
		          $(".ui-dialog-buttonpane button:contains('No')").button().show().addClass("cancelButton");
		      }
		  });

		  //

        var pageGetHelp = function (module) {
            var userType = '';
		   <?php if($_SESSION['superUser'] == 1){?>
                        userType = 'superUser';
                   <?php }else if($_SESSION['superUser'] == 3){?>     
                       userType = 'superUser';
            <?php }else{?>
                      userType = '<?php echo $_SESSION['adminType']?>';
	   <?php }?>
            $.ajax({
                type: "POST",
                url: "help.php",
                data: {userType: userType, modId: module},
                success: function (result) {
                    if (result == "") {
                        $("#helpUrl").removeAttr('href');
                        return false;
                    }
                    var obj = jQuery.parseJSON(result);
                    if (obj != "error") {
                        $("#helpUrl").attr('href', '../../../helpUrl/' + obj);
                        $("#helpUrl").attr('target', '_blank');
                    }

                }
            });
        };
        //pageGetHelp('main');
        if ($.fn.button.noConflict) {
        	var bootstrapButton = $.fn.button.noConflict();
        	$.fn.bootstrapBtn = bootstrapButton;
        	}	
        
    });
</script>

<style>
	.disabled {
		pointer-events:none;
		opacity:0.6;
	}
</style>
<script>
function makeServicePostData(dataToSend) {
	/* Group Service from data */
	var response = {};
	response.groupServicePackAuth = createCheckBoxDataString($("input[name^='groupServicePackAuth']"));
	response.groupServicePackLimit = createCheckBoxDataString($("input[name^='groupServicePackLimit']"));
	response.groupServicePackQnt = createInputFieldDataString($("input[name^='groupServicePackQnt']"));

	/* User Service from data */
	response.userServicePackAuth = createCheckBoxDataString($("input[name^='userServicePackAuth']"));
	response.userServicePackLimit = createCheckBoxDataString($("input[name^='userServicePackLimit']"));
	response.userServicePackQnt = createInputFieldDataString($("input[name^='userServicePackQnt']"));
	
	dataToSend.push({"name": "groupServicePackAuth", "value" : response.groupServicePackAuth},
			{"name": "groupServicePackLimit", "value" : response.groupServicePackLimit},
			{"name": "groupServicePackQnt", "value" : response.groupServicePackQnt},
			{"name": "userServicePackAuth", "value" : response.userServicePackAuth},
			{"name": "userServicePackLimit", "value" : response.userServicePackLimit},
			{"name": "userServicePackQnt", "value" : response.userServicePackQnt}
	);
	
	return dataToSend;
}


function getUnassignNumbers(dataToSend) {
    var unassignNumber = "";
		$("#availableRanges option").each(function(){
		unassignNumber += $(this).val() + ";;";
                
                });
      if($('input.dnsServiceLimitCheckBox:checked').length >0){
                   debugger ;
            var unassignNumber = "";
                     $(".dnsServiceLimitCheckBox:checked").each(function() {
                     var dnsValue  =  $(this).attr("data-value") ;
                    var dnsUnAssignArray = [dnsValue];
                    
                    var range = "";
		var range1 = "";
		var range2 = "";
		var assignedRanges = dnsUnAssignArray;		
		for (var i = 0; i < assignedRanges.length; i++) {
			//$("#assignedRanges option[value='"+assignedRanges[i]+"']").remove();			
			range = assignedRanges[i].split(" - ");
			range1 = parseInt(range[0].replace("+1-", "")); 
			if(range[1]){
				range2 = parseInt(range[1].replace("+1-", ""));
			}else{
				range2 = "";
			}
			if(range1 != "" && range2 == ""){
				unassignNumber += "+1-"+range1 + ";;";
                                 
			}else if(range1 != "" && range2 != ""){
				for(j = parseInt(range1); j < parseInt(range2) + 1; j++){
					unassignNumber += "+1-"+ j + ";;";
                                }
			}
		}
                    
                    
                    
                    
           });
                    
                }/*else{
                    
                    $("#availableRanges option").each(function(){
		unassignNumber += $(this).val() + ";;";
	});
                } */
	dataToSend.push({name:'unassignNumber', value: unassignNumber});
    return dataToSend;
}
/*
function getDelUnassignNumbers(dataToSend) {
    if($('input.dnsServiceLimitCheckBox:checked').length >0){
                   debugger ;
            var unassignNumber = "";
                     $(".dnsServiceLimitCheckBox:checked").each(function() {
                     var dnsValue  =  $(this).attr("data-value") ;
                    var dnsUnAssignArray = [dnsValue];
                    
                    var range = "";
		var range1 = "";
		var range2 = "";
		var assignedRanges = dnsUnAssignArray;		
		for (var i = 0; i < assignedRanges.length; i++) {
			//$("#assignedRanges option[value='"+assignedRanges[i]+"']").remove();			
			range = assignedRanges[i].split(" - ");
			range1 = parseInt(range[0].replace("+1-", "")); 
			if(range[1]){
				range2 = parseInt(range[1].replace("+1-", ""));
			}else{
				range2 = "";
			}
			if(range1 != "" && range2 == ""){
				unassignNumber += "+1-"+range1 + ";;";
                                // html += '<option value="+1-'+range1+'">+1-' + range1 + '</option>';
			}else if(range1 != "" && range2 != ""){
				for(j = parseInt(range1); j < parseInt(range2) + 1; j++){
					unassignNumber += "+1-"+ j + ";;";
                                     //html += '<option value="+1-'+j+'">+1-' + j + '</option>';
				}
			}
		}
                    
                    
                    
                    
           });
                    
                }
                
                dataToSend.push({name:'unassignNumber', value: unassignNumber});
    return dataToSend;
} */



</script>
<div id="mainBody" class="header_scroll">   
        <?php if(isset($license["Enterprises"]) &&  $license["Enterprises"] == "true" && 
                ($basicInformationLogPermission == "1" || $outgoingCallingPlanLogPermission == "1" 
                || $domainsLogPermission == "1" || $securityDomainsLogPermission == "1" 
                || $servicesLogPermission == "1"  || $dnsLogPermission == "1" 
                || $callProcessingPoliciesLogPermission == "1" 
                || $networkClassesofServiceLogPermission == "1" || $voicePortalServiceLogPermission == "1")){ ?>
	<h2 class="grpText">Enterprises / Groups</h2>
	<?php } else{
	   ?>
		<h2 class="grpText">Groups</h2>
	<?php }?>     
        
         <div style="" class="icons-div macdLst">
		<ul style="" class="feature-list">
			<?php //if ($_SESSION["superUser"] == "1" and $permissionsDelUsrAndGroup == "true") { ?><!--li><a href="#" class="navMenu navigate" id="modGroup"><img src="images/icon-modify-hunt-group.png" /><br />Modify Group</a></li--><?php //} ?>
			<?php if ($_SESSION["permissions"]["modhuntgroup"] == "1") { ?>
				<li style="display: none"><a href="#" class="navMenu navigate" id="modHunt"><img class="rolloverImg" src="images/NewIcon/modfy_group.png" data-alt-src="images/NewIcon/modfy_group_rollover.png"/><br/>Hunt Groups</a></li>
			<?php } ?>
			<?php if ($_SESSION["permissions"]["aamod"] == "1") { ?>
				<li style="display: none"><a href="#" class="navMenu navigate" id="modAA"><img class="rolloverImg" src="images/NewIcon/modify_auto_attendant.png" data-alt-src="images/NewIcon/modify_auto_attendant_rollover.png"/><br/>Auto Attendants</a></li>
			<?php } ?>
			<?php if ($_SESSION["permissions"]["modcallCenter"] == "1") { ?>
				<li style="display: none"><a href="#" class="navMenu navigate" id="modCallCenter"><img class="rolloverImg" src="images/icon-call-center.png"/><br/>Call Centers</a></li>
			<?php } ?>
			<?php if ($cpgFieldVisible == "true") { ?>
				<li style="display: none"><a href="#" class="navMenu navigate" id="modGrpCallPickup"><img class="rolloverImg" src="images/icon-group-call-pickup.png"/><br/>Call Pickup Groups</a></li>
			<?php } ?>
			<?php if ($_SESSION["permissions"]["modschedule"] == "1") { ?>
				<li style="display: none"><a href="#" class="navMenu navigate" id="modSched"><img class="rolloverImg" src="images/icon-schedule.png"/><br/>Schedules</a></li>
			<?php } ?>
			
			
                <?php 
                //if(isset($license["Enterprises"]) &&  $license["Enterprises"] == "true") 
                if($enterpriseModulePermissionLevel)
                {
                        echo "<li ><a href='#' class='navMenu navigate' id='Enterprises'><img class='rolloverImg' src='images/NewIcon/enterprise_admin.png' data-alt-src='images/NewIcon/enterprise_admin_rollover.png' /><br/>Enterprises</a></li>";        
                }
                if($basicInformationLogPermission == "1" || $outgoingCallingPlanLogPermission == "1" 
                        || $domainsLogPermission == "1" || $securityDomainsLogPermission == "1" 
                        || $servicesLogPermission == "1"  || $dnsLogPermission == "1" 
                        || $callProcessingPoliciesLogPermission == "1" 
                        || $networkClassesofServiceLogPermission == "1" || $voicePortalServiceLogPermission == "1"){ ?>
                                <li ><a href="#" class="navMenu navigate" id="groupModify"><img class="rolloverImg" src="images/NewIcon/groups.png" data-alt-src="images/NewIcon/groups_rollover.png"/><br/>Groups</a></li>
                <?php } ?>
        </ul>
	</div>
       
        <?php 
        
          if($userPermission == "1" || $devicesLogPermission =="1" || $entAnnouncementsLogPermission == "1" 
             || $entServicePacksLogPermission =="1" || $cdrLogPermission == "1" 
             || $phoneProfilesLogPermission == "1" || $broadWorksLicenseReportLogPermission == "1" 
             || $changeLogLogPermission == "1" || $_SESSION["permissions"]["sasTest"] == "1"){              
              echo "<h2 class='inventoryText'>Inventory</h2>";              
          }
        ?>
	
        
        
	<div style="" class="icons-div inventorylst">
		<ul style="" class="feature-list">                    
                        <?php                           
                        if($userPermission == "1"){ 
                            ?>
                        <li><a href="#" class="navMenu navigate" id="usersEnterprise"><img class="rolloverImg" src="images/NewIcon/users_admin.png" data-alt-src="images/NewIcon/users_admin_rollover.png" /><br/>Users</a></li>
                        <?php } ?>             
                        <?php   if($_SESSION["superUser"] == "1" || $devicesLogPermission =="1" && (!isset($_SESSION["groupId"]) || $_SESSION["groupId"] == "")){ ?>
                         <li><a href="#" class="navMenu navigate" id="modDevice"><img class="rolloverImg" src="images/NewIcon/device_inventory.png" data-alt-src="images/NewIcon/device_inventory_rollover.png" /><br />Device Inventory</a></li>
                        <?php } ?>                        
                        <?php   if(($_SESSION['superUser'] == "1" || $entAnnouncementsLogPermission == "1") && $_SESSION['groupId'] == ""){ ?>
                         <li><a href="#" class="navMenu navigate" id="announcement"><img class="rolloverImg" src="images/NewIcon/announcement.png" data-alt-src="images/NewIcon/announcement_rollover.png" /><br/>Announcements</a></li>
                         <?php } ?>
                         <?php   if(($_SESSION['superUser'] == "1" || $entServicePacksLogPermission =="1") && $_SESSION['groupId'] == ""){ ?>
                         <li><a href="#" class="navMenu navigate" id="servicePackModule"><img class="rolloverImg" src="images/NewIcon/service-pack-purple.png" data-alt-src="images/NewIcon/service-pack-orange.png" /><br/>Service Pack</a></li>
                         <?php } ?>
                         <!-- <li><a href="#" class="navMenu navigate" id="servicePacks"><img class="rolloverImg" src="images/NewIcon/announcement.png" data-alt-src="images/NewIcon/announcement_rollover.png" /><br/>Service Packs</a></li> -->

			

			<?php if (true) { ?><!--li><a href="#" class="navMenu navigate" id="deviceMgmt" style="display: none"><img src="images/icon-phone-tags.png" /><br />Device Management</a></li--><?php } ?>
			<?php if ($_SESSION["permissions"]["modifyUsers"] == "1") { ?>
			<!--     <li><a href="#" class="navMenu navigate" id="findMac"><img src="images/icon-address.png"/><br/>Find MAC Addresses</a></li>  --> <?php } ?>
			<?php if ($_SESSION["permissions"]["viewnums"] == "1") { ?>
				<li style="display:none"><a href="#" class="navMenu navigate" id="dids"><img class="rolloverImg" src="images/icon-numbers.png"/ ><br/>View Numbers</a></li><?php } ?>
			<?php if ($_SESSION["permissions"]["viewChangeLog"] == "1") { ?>
				<li style="display :none"><a href="#" class="navMenu navigate" id="changeLog"><img class="rolloverImg" src="images/icon-change-log.png"/><br/>Change Logs</a></li><?php } ?>
			
                       <?php //if ($_SESSION['superUser'] == 1 && $_SESSION["permissions"]["viewcdrs"] == "1" && ! is_null($billDB) ) { ?><!--<li><a href="#" class="navMenu navigate" id="cdrsEnterprise"><img class="rolloverImg" src="images/NewIcon/call_records.png" data-alt-src="images/NewIcon/call_records_rollover.png" /><br />CDRs</a></li> --><?php //} ?>
                        <?php if (($_SESSION['superUser'] == 1 || $_SESSION['superUser'] == 3) && $cdrLogPermission == "1" && ! is_null($billDB) ) { ?><li><a href="#" class="navMenu navigate" id="cdrsEnterprise"><img class="rolloverImg" src="images/NewIcon/call_records.png" data-alt-src="images/NewIcon/call_records_rollover.png" /><br />CDRs</a></li><?php } ?>
                        
                        
			<?php if ($_SESSION["permissions"]["sasTest"] == "1" || $phoneProfilesLogPermission == "1" || $broadWorksLicenseReportLogPermission == "1" || $changeLogLogPermission == "1")
			      {
                                if((isset($license["BroadWorksLicenseReport"]) && $license["BroadWorksLicenseReport"] == "true") or (isset($license["customProfile"]) && $license["customProfile"] == "true") && checkCustomProfiles())
                                {
                                    ?>
                                        <li><a href="#" class="navMenu navigate" id="siteReport">
                                                <img class="rolloverImg" src="images/NewIcon/site_management.png" data-alt-src="images/NewIcon/site_management_rollover.png" /><br/> Site Management</a></li>
                                    <?php
                                }
                                 //changeLog event module ent
                                 //if ($_SESSION["permissions"]["viewChangeLog"] == "1" ) {
                                if ($changeLogLogPermission == "1") {
                                    ?>
                                        <li><a href="#" class="navMenu navigate" id="entChangeLog">
                                                <img class="rolloverImg" src="images/NewIcon/view_change_log.png" data-alt-src="images/NewIcon/view_change_log_rollover.png" />
                                                <br />Change Logs</a></li><?php                                                 
                                    }
				}
				?>

		</ul>
	</div>
    <?php if($administratorsLogPermission == "1" || $adminLogsLogPermission == "1"){ ?>
           <h2 style="" class="usersText">Administrators</h2>
           <div style="" class="icons-div userLst">
                   <ul style="" class="feature-list">
                   <?php   if($_SESSION["superUser"] == "1" && $administratorsLogPermission == "1") { ?>
                            <li><a href="#" class="navMenu navigate" id="adminUserEnt"><img class="rolloverImg" src="images/NewIcon/administrator.png" data-alt-src="images/NewIcon/administrator_rollover.png" /><br />Administrators</a></li>
                            <?php } 
                            if($_SESSION["superUser"] == "3" && $administratorsLogPermission == "1"){ ?>
                            <li><a href="#" class="navMenu navigate" id="groupAdminUser"><img class="rolloverImg" src="images/NewIcon/administrator.png" data-alt-src="images/NewIcon/administrator_rollover.png" /><br />Administrators</a></li>
                           <?php } 
                           $adminLogsArr       = explode("_", $_SESSION["permissions"]["adminLogs"]);
                           $adminLogPermission = $adminPrmnsArr[0];
                           if ($adminLogsLogPermission == "1") { ?>
                           <li><a href="#" class="navMenu navigate" id="adminLogs">
                           <img class="rolloverImg" src="images/NewIcon/adminlog.png" data-alt-src="images/NewIcon/adminlog_rollover.png" /><br/>
                           Admin Logs</a></li>
                           <?php } ?>
                   </ul>
           </div>
     <?php  } ?>     
</div>
<div id='checkDataChangesModulePrevent'></div>