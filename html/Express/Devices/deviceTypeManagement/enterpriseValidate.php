<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/devices/deviceTypeManagement.php");
$devobj = new DeviceTypeManagement();
$enterpriseDeviceTypeList = $devobj->getEnterpriseTableDevices($_SESSION["sp"]);
$systemTableData = $devobj->getSystemTableDevices();
//print_r($_POST);
$error = 0;
$postDevices = array();
if(count($_POST) > 0){
    foreach ($_POST["enterpriseData"] as $pKey => $pValue){
        $postDevices[$pValue["deviceType"]]["enabled"] =  $pValue["enabled"];
        if(isset($pValue["blf"]) && !empty($pValue["blf"])){
            $postDevices[$pValue["deviceType"]]["blf"] =  $pValue["blf"];
        }else{
            $postDevices[$pValue["deviceType"]]["blf"] =  "No";
        }
        
    }
}
$changedArray["system"] = array();
$changedArray["enterprise"] = array();
if(count($postDevices) > 0){
    foreach ($postDevices as $kKey => $kValue){
        if(array_key_exists($kKey, $enterpriseDeviceTypeList)){
            if($kValue["enabled"] !=$enterpriseDeviceTypeList[$kKey]["enabled"]){
                $changedArray["enterprise"][$kKey]["enabled"]=$kValue["enabled"];
            }
            if($kValue["blf"] !=$enterpriseDeviceTypeList[$kKey]["blf"]){
                $changedArray["enterprise"][$kKey]["blf"]=$kValue["blf"];
            }
        }else if(array_key_exists($kKey, $systemTableData)){
            if($kValue["enabled"] !=$systemTableData[$kKey]["enabled"]){
                $changedArray["system"][$kKey]["enabled"]=$kValue["enabled"];
            }
            if($kValue["blf"] !=$systemTableData[$kKey]["blf"]){
                $changedArray["system"][$kKey]["blf"]=$kValue["blf"];
            }
        }
    }
}
//print_r($changedArray);
//print_r($enterpriseDeviceTypeList);
//$systemTableData = $devobj->getSystemTableDevices();

$changeArrayMerged = array_merge($changedArray["system"],$changedArray["enterprise"]);
if(count($changeArrayMerged) > 0){
    $data = $errorTableHeader;
    foreach ($changeArrayMerged as $key => $value)
    {
        
        /*if ($value !== ""){
         $bg = CHANGED;
         }else{
         $bg = UNCHANGED;
         }*/
        $bg = CHANGED;
        
        
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:#72ac5d;vertical-align:middle;\">" . $key . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    if($k == "phoneType" && $v=="" && $value["enabled"]=="true"){
                        $error = 1;
                        $bg = INVALID;
                        $v = "Phone type can not be empty if device is enabled.";
                        
                    }
                    
                    if($k == "phoneType" && $v=="Soft Phone"){
                        if($value["softPhoneType"] == ""){
                            $error = 1;
                            $bg = INVALID;
                            $v = "Soft phone type can not be empty if phone type is selected as 'Soft Phone'.";
                        }
                        
                    }
                    
                    if($k == "fxoPorts" && $v !=""){
                        if($value["phoneType"] != "Analog"){
                            $error = 1;
                            $bg = INVALID;
                            $v = "FXO Ports can be set only for anolog devices.";
                        }
                    }
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}else{
    $error = 1;
    $bg = INVALID;
    $data .= "You have made no changes.";
}

$data .= "</table>";
echo $error . $data;
?>