var syncDeviceTypesData = function(){
	$("#systemLabelDeviceTable").hide();
	$("#enterpriseLabelDeviceTable").hide();
	$("#allTablesDivData").hide();
	$("#searchDeviceOptionDevType").hide();
	$("#deviceTypeLoading").show();
	$("#systemSubmit").hide();
	$.ajax({
		type: "POST",
		url: "Devices/deviceTypeManagement/syncDeviceTypes.php",
		success: function(result)
		{
			$("#deviceTypeLoading").hide();
			$("#allTablesDivData").html(result);
			$("#allTablesDivData").show();
			//$("#systemSubmit").show();
			//successResonseSystemTableDraw(result);
		}

	});
};

/*$(window).resize(function () {
	$(".DTFC_LeftWrapper .triggerColumnSystemTable").trigger("click");
	$(".DTFC_LeftWrapper .triggerColumnSystemTable").trigger("click");
});*/
$(function(){
	var completeAction = "";
	$("#enterpriseLabelDeviceTable").hide();
	$("#systemLabelDeviceTable").show();
	$("input[name='searchDevicelevel']").click(function() {
        var selValue = $(this).val();
        if(selValue == "system"){
        	$("#enterpriseLabelDeviceTable").hide();
        	$("#systemLabelDeviceTable").show();
        }else{
        	$("#enterpriseLabelDeviceTable").show();
        	$("#systemLabelDeviceTable").hide();
        }
        
    });
	//$("#deviceTypeTable").tablesorter();
	//$("#deviceTypeTableEnterprise").tablesorter();
	$("#device_type_search_tab").click(function() {
		syncDeviceTypesData();
	});
	
	
	$("#deviceTypeManagementDialog").dialog({
        autoOpen: false,
        width: 800,
        modal: true,
        title: "Device Type Management Modify",
        position: { my: "top", at: "top" },
        resizable: false,
        closeOnEscape: false,
        buttons: {
           Complete: function(){
        	   pendingProcess.push("Device Type Management Modify");
        	   $("html, body").animate({ scrollTop: 0 }, 600);
        	   if(completeAction =="modifySystemDeviceTypes.php"){
        		   var dataToSend = $("form#systemDevicesTableForm").serializeArray();
        	   }else{
        		   var dataToSend = $("form#enterPriseDeviceTableForm").serializeArray();
        	   }
        	   
               //var pageUrl = "modifySystemDeviceTypes.php"; 
        	   var pageUrl = completeAction;
               $.ajax({
                 type: "POST",
                 url: "Devices/deviceTypeManagement/"+pageUrl,
                 data: dataToSend,
                 success: function(result)
                 {
                	 if(foundServerConErrorOnProcess(result, "Device Type Management Modify")) {
     					return false;
                     }
                     /*result = result.trim();
                     if (result.slice(0, 1) == 1)
                     {
                         $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                     }
                     else
                     {
                         $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                     }*/
                     $("#deviceTypeManagementDialog").html(result);
                     //$("#deviceTypeManagementDialog").dialog("close");
                     $("html, body").animate({ scrollTop: 0 }, 600);
                     $("#deviceTypeManagementDialog").dialog("open");

                     $(":button:contains('Complete')").hide();
                     //$(":button:contains('Cancel')").show().addClass('cancelButton');
                     $(":button:contains('More Changes')").show().addClass('moreChangesButton');
                     $(":button:contains('Return To Main')").show().addClass('moreChangesButton');
                     $(":button:contains('Create')").hide();
                     $(":button:contains('Cancel')").hide();
                     
                     // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                 }
               });
           },
           'More Changes': function(){
        	   $("html, body").animate({ scrollTop: 0 }, 600);
        	   $(this).dialog("close");
        	   syncDeviceTypesData();
           },
           "Return To Main" : function(){
               location.href="main_enterprise.php";
           },
            Cancel: function() {
                $(this).dialog("close");
            }

        },
        open: function() {
        setDialogDayNightMode($(this));
      	$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
      	$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
      	$(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
      	$(".ui-dialog-buttonpane button:contains('Return To Main')").button().hide();
      	   
       }
    });
	
	$(document).on("click", "#systemSubmit", function(event) {
		debugger;
	//$("#systemSubmit").click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
        var dataToSend = $("form#systemDevicesTableForm").serializeArray();
        var pageUrl = "Devices/deviceTypeManagement/validate.php"; 
        $.ajax({
          type: "POST",
          url: pageUrl,
          data: dataToSend,
          success: function(result)
          {

              result = result.trim();
              if (result.slice(0, 1) == 1)
              {
                  $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
              }
              else
              {
                  $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");   
                  completeAction = "modifySystemDeviceTypes.php";
              }
              $("#deviceTypeManagementDialog").html(result.slice(1));
              //$("#deviceTypeManagementDialog").dialog("close");
              $("html, body").animate({ scrollTop: 0 }, 600);
              $("#deviceTypeManagementDialog").dialog("open");
              $(":button:contains('Complete')").show().addClass('subButton');;
              $(":button:contains('Cancel')").show().addClass('cancelButton');;
              $(":button:contains('More Changes')").hide();
              $(":button:contains('Return To Main')").hide();
             // $("#deviceTypeManagementDialog").dialog("option","title", "Service Pack Dialog");
          }
        });  
	});
	
	$(document).on("click", "#enterpriseSubmit", function(event) {
		//$("#systemSubmit").click(function(){
			$("html, body").animate({ scrollTop: 0 }, 600);
	        var dataToSend = $("form#enterPriseDeviceTableForm").serializeArray();
	        var pageUrl = "Devices/deviceTypeManagement/enterpriseValidate.php"; 
	        $.ajax({
	          type: "POST",
	          url: pageUrl,
	          data: dataToSend,
	          success: function(result)
	          {

	              result = result.trim();
	              if (result.slice(0, 1) == 1)
	              {
	                  $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
	              }
	              else
	              {
	                  $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled"); 
	                  completeAction = "modifyEnterpriseDevices.php";
	              }
	              $("#deviceTypeManagementDialog").html(result.slice(1));
	              //$("#deviceTypeManagementDialog").dialog("close");
	              $("html, body").animate({ scrollTop: 0 }, 600);
	              $("#deviceTypeManagementDialog").dialog("open");
	              $(":button:contains('Complete')").show().addClass('subButton');;
	              $(":button:contains('Cancel')").show().addClass('cancelButton');;
	              $(":button:contains('More Changes')").hide();
	              $(":button:contains('Return To Main')").hide();
	             // $("#deviceTypeManagementDialog").dialog("option","title", "Service Pack Dialog");
	          }
	        });  
		});
	
	//$('#downloadCSVDeviceType').click(function() {
	$(document).on("click", "#downloadCSVDeviceType", function(event) {	
        var titles = [];
		var data = [];
		var TableData = [];
		$('#deviceTypeTable th').each(function() { 
           // if($(this).text() != "Enabled"){
               titles.push( $.trim($(this).text()) ); 
            //}//table id here
   
        });
        var TableData = [];  
        $('#deviceTypeTable tbody tr:visible').find('input,select').each(function(){
        	if($(this).context.className == "statusOfDevice"){
                return;
            }
            if($(this).context.className == "systemTableCheckbox"){
                if($(this).prop("checked") == true){
                	TableData.push("Yes");
                }else{
                	TableData.push("No");
                }
                
            }
            else if( $(this).context.className == "selectVdmvdmTemplate" || $(this).context.className == "selectsoftPhoneType") {
            	TableData.push($(this).find('option:selected').text());
			}
            else {
            	TableData.push( $.trim($(this).val()) );
            }
        });   

		var CSVString = prepCSVRow(titles, titles.length, '');
		CSVString = prepCSVRow(TableData, titles.length, CSVString);
		
		var fileName = "SystemDevicetypeCSV.csv";
		var blob = new Blob(["\ufeff", CSVString]);
		if (navigator.msSaveBlob) { // IE 10+
			navigator.msSaveBlob(blob, fileName);
		}else{
			var downloadLink = document.createElement("a");
		    
		    var url = URL.createObjectURL(blob);
		    downloadLink.href = url;
		    downloadLink.download = fileName;
		    
		    
		    document.body.appendChild(downloadLink);
		    downloadLink.click();
		    document.body.removeChild(downloadLink);
		}


});
	
	//$("#sampleDeviceCSV").click(function() {
	$(document).on("click", "#sampleDeviceCSV", function(event) {
        var titles = [];
		    var data = [];
		    var TableData = [];
		    $('#deviceTypeTable th').each(function() {
			    	if( ( $(this).text() == "Custom Tag Spec" && customTagsLicense == "false") ||
			    			($(this).text() == "Soft Phone Type" && softPhoneLicense == "false") ||
			    			( $(this).text() == "VDM" && vdmLiteLicense == "false" )
					 ) {
						return;
					 } 
                  titles.push( $.trim($(this).text()) ); 
           });
		    var CSVString = prepCSVRow(titles, titles.length, '');
		    CSVString = prepCSVRow(TableData, titles.length, CSVString);
		    var fileName = "SampleDevicetypeCSV.csv";
		    var blob = new Blob(["\ufeff", CSVString]);
		    if (navigator.msSaveBlob) { // IE 10+
	        	navigator.msSaveBlob(blob, fileName);
	        }else{
	        	var downloadLink = document.createElement("a");
			    
			    var url = URL.createObjectURL(blob);
			    downloadLink.href = url;
			    downloadLink.download = fileName;
			    document.body.appendChild(downloadLink);
			    downloadLink.click();
			    document.body.removeChild(downloadLink);
		    }
		    
	});
	
	//$('#downloadCSVEntDeviceType').click(function() {
	$(document).on("click", "#downloadCSVEntDeviceType", function(event) {
        var titles = [];
        var data = [];
        var TableData = [];
        $('#deviceTypeTableEnterprise th').each(function() { 
                    //if($(this).text() != "Enabled"){
                       titles.push($(this).text()); 
                    //}//table id here
    	   
                });
        var TableData = [];  
        $('#deviceTypeTableEnterprise tbody tr:visible').find('input, select').each(function(){
            if($(this).context.className == "enterpriseTableCheckbox"){
                return;
            }
            if($(this).context.className == "statusOfDevice"){
                return;
            }
            if($(this).context.className == "enterpriseTableCheckboxClass"){
            	if($(this).prop("checked") == true){
                	TableData.push("Yes");
                }else{
                	TableData.push("No");
                }
            }else{
            	TableData.push($(this).val());
            }
             
        });   

        var CSVString = prepCSVRow(titles, titles.length, '');
        CSVString = prepCSVRow(TableData, titles.length, CSVString);

        var fileName = "EnterpriseDevicetypeCSV.csv";
        var blob = new Blob(["\ufeff", CSVString]);
        if (navigator.msSaveBlob) { // IE 10+
        	navigator.msSaveBlob(blob, fileName);
        }else{
        	var downloadLink = document.createElement("a");
            
            var url = URL.createObjectURL(blob);
            downloadLink.href = url;
            downloadLink.download = fileName;
            
            
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
        
        
    });
	
	$(document).on("click", "input[name='searchDevicelevel']", function(event) {
	//$("input[name='searchDevicelevel']").click(function() {
        var selValue = $(this).val();
        if(selValue == "system"){
        	$("#enterpriseLabelDeviceTable").hide();
        	$("#systemLabelDeviceTable").show();	
        	$("#downloadCSVEntDeviceType").hide();	/* Download CSV for Enterprise */
        	$("#downloadCSVDeviceType").show();		/* Download CSV for System */
        	//$(".DTFC_LeftWrapper .triggerColumnSystemTable").trigger("click");
        	//$(".DTFC_LeftWrapper .triggerColumnSystemTable").trigger("click");
        	$(".sampleDeviceCSVDiv").show();
        }else{
        	$("#enterpriseLabelDeviceTable").show();
        	$("#systemLabelDeviceTable").hide();
        	$("#downloadCSVDeviceType").hide();		/* Download CSV for Syster */
        	$("#downloadCSVEntDeviceType").show();	/* Download CSV for Enterprise */
        	$(".sampleDeviceCSVDiv").hide();
        }
        filterDeviceTypeTable();
    });
	
	$(document).on("click", "#searchDeviceBtn", function(event) {
	//$("#searchDeviceBtn").click(function() {
		filterDeviceTypeTable();
	});
	
	$(document).on("click", "input[name='showViewRadioBtn']", function(event) {
	//$("input[name='showViewRadioBtn']").change(function() {
		filterDeviceTypeTable();
	});

});

/* Create an array with the values of all the input boxes in a column 
$.fn.dataTable.ext.order['dom-text'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).val();
    } );
}*/
 
/* Create an array with the values of all the input boxes in a column, parsed as numbers 
$.fn.dataTable.ext.order['dom-text-numeric'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).val() * 1;
    } );
}*/
 
/* Create an array with the values of all the select options in a column 
$.fn.dataTable.ext.order['dom-select'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('select', td).val();
    } );
}*/
 
/* Create an array with the values of all the checkboxes in a column */
/*$.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).prop('checked') ? '1' : '0';
    } );
}*/


    function createSelectOps(selectedVal) {
	   	 var selectOps = "";
	   	 selectOps += '<select name ="" id="test1">';
	   	 selectOps +=	'<option value=""></option>';
	   	 selectOps +=	'<option '+ selectedVal == "SIP Phone" ? "selected":"" +' value="SIP Phone">SIP Phone</option>';
	   	 selectOps +=	'<option value="Analog">Analog</option>';
	   	 selectOps +=	'<option value="Soft Phone">Soft Phone</option>';
	   	 selectOps +='</select>';
	   	 return selectOps;
    }
    
    function prepCSVRow(arr, columnCount, initial) {
	    	var row = '';
	    	var delimeter = ',';
	    	var newLine = '\r\n';
	
	    	function splitArray(_arr, _count) {
		    	var splitted = [];
		    	var result = [];
		    	_arr.forEach(function(item, idx) {
		    	if ((idx + 1) % _count === 0) {
		    	splitted.push(item);
		    	result.push(splitted);
		    	splitted = [];
		    	} else {
		    	splitted.push(item);
		    	}
		    	});
		    	return result;
	    	}
	    	var plainArr = splitArray(arr, columnCount);
	
	    	plainArr.forEach(function(arrItem) {
		    	arrItem.forEach(function(item, idx) {
		    		row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
		    	});
		    	row += newLine;
	    	});
	    	return initial + row;
    	}
    
    function filterDeviceTypeTable() { //debugger;
		$("#deviceLoadingBar").show();
	 	var searchlevel = $("input[name='searchDevicelevel']:checked").val();
	 	
		if(searchlevel == "system") {
			$("#systemLabelDeviceTable").hide();
			filterDeviceType();
			setTimeout(function(){
				$("#systemLabelDeviceTable").show();
				$("#deviceLoadingBar").hide();
				$(".DTFC_LeftWrapper .triggerColumnSystemTable").trigger("click");
	        	$(".DTFC_LeftWrapper .triggerColumnSystemTable").trigger("click");
	        	var visibleTableRow = $("#systemDevicesTableForm").find(".DTFC_LeftWrapper tbody tr:visible").length;
	    		var totalTableRow = $("#systemDevicesTableForm").find(".DTFC_LeftWrapper tbody tr").length;
	    		var startIndex = visibleTableRow == 0 ? "0" : "1";
	    		//$("#deviceTypeTable_info").html("Showing " + startIndex + " to "+ visibleTableRow +" of "+totalTableRow +" entries");
                        $("#deviceTypeTable_info").html("Showing " + startIndex + " to "+ visibleTableRow +" of "+totalTableRow +" entries");
				var allDeviceTableRows = $("#systemDevicesTableForm").find(".DTFC_LeftWrapper tbody");
				checkDataFoundOrNot(allDeviceTableRows);
	    	}, 500);
			
	   } else if(searchlevel == "enterprise") {
			$("#enterpriseLabelDeviceTable").hide();
                        
                        
                        var visibleTableRow = $("#enterPriseDeviceTableForm").find(".DTFC_LeftWrapper tbody tr:visible").length;
	    		var totalTableRow = $("#enterPriseDeviceTableForm").find(".DTFC_LeftWrapper tbody tr").length;
	    		var startIndex = visibleTableRow == 0 ? "0" : "1";
	    		//$("#deviceTypeTable_info").html("Showing " + startIndex + " to "+ visibleTableRow +" of "+totalTableRow +" entries");
                        $("#deviceTypeTable_info").html("Showing " + startIndex + " to "+ visibleTableRow +" of "+totalTableRow +" entries");
                        
                        
                        
			searchDeviceTypeTableOnEnt();
			setTimeout(function(){
				$("#enterpriseLabelDeviceTable").show();
				$("#deviceLoadingBar").hide();
				var allEntTableRows = $("#deviceTypeTableEnterprise tbody");
				checkDataFoundOrNot(allEntTableRows);
	    	}, 500);
		}
    }
    
    function filterDeviceType() {
    	var selectedView = $("input[name='showViewRadioBtn']:checked").val();
        var deviceTypeSearched = $("#searchDeviceType").val().toLowerCase();
        var totalTableRow = $("#systemDevicesTableForm").find(".DTFC_LeftWrapper tbody tr").length
        var rowNo = 0;
    	$("#systemDevicesTableForm").find(".DTFC_LeftWrapper tbody tr").filter(function() {
    		
    		var deviceEnabled = $(this).find("td:eq(0)").find(".statusOfDevice").val();		
    	      	if(
    				selectedView == "All" && deviceTypeSearched == "" ||
    				(selectedView == "Enabled" && deviceTypeSearched != "") && (deviceEnabled == "true" && $(this).find("td:eq(1)").text().toLowerCase().search(deviceTypeSearched) != -1) ||
    				(selectedView == "Enabled" && deviceTypeSearched == "") && (deviceEnabled == "true") ||
    				(selectedView == "All" && deviceTypeSearched != "") && ( $(this).find("td:eq(1)").text().toLowerCase().search(deviceTypeSearched) != -1)
    	      	  ) {
    	      		$(this).toggle(true);
    	      		$("#deviceTypeTable tbody").find("tr:eq(" + rowNo + ")").show();
    	         } else {
    	        	 $(this).toggle(false);
    	        	 $("#deviceTypeTable tbody").find("tr:eq(" + rowNo + ")").hide();
    	         }
    			rowNo++;			
    	});
        
    }    
    
    function checkDataFoundOrNot(allTableRows) {
    	if( allTableRows.find("tr:visible").length <= 0 ) {
    		if(allTableRows.find(".noDataFound").length > 0) {
    			allTableRows.find(".noDataFound").show();
    	    } else {
    	    	allTableRows.append("<tr class='noDataFound'><td colspan=2>No Device Type Found</td></tr>");
    	    }
    	} else if(allTableRows.find(".noDataFound").length > 0) {
    		allTableRows.find(".noDataFound").hide();
    	}	
    }
    
    function searchDeviceTypeTableOnEnt() {
        var selectedView = $("input[name='showViewRadioBtn']:checked").val();
        var deviceTypeSearched = $("#searchDeviceType").val().toLowerCase();
    	$("#deviceTypeTableEnterprise tbody tr").filter(function() {
    		var deviceEnabled = $(this).find("td:eq(0)").find(".statusOfDevice").val();
          	if(
    			selectedView == "All" && deviceTypeSearched == "" ||
    			(selectedView == "Enabled" && deviceTypeSearched != "") && (deviceEnabled == "true" && $(this).find("td:eq(1)").text().toLowerCase().search(deviceTypeSearched) != -1) ||
    			(selectedView == "Enabled" && deviceTypeSearched == "") && (deviceEnabled == "true") ||
    			(selectedView == "All" && deviceTypeSearched != "") && ( $(this).find("td:eq(1)").text().toLowerCase().search(deviceTypeSearched) != -1)
          	  ) {
          		$(this).toggle(true);
             } else {
            	 $(this).toggle(false);
             }
        });    
    }