<style>
    #systemLabelDeviceTable .dataTables_scrollBody, #systemLabelDeviceTable .DTFC_LeftBodyWrapper{
        margin-top:-10px;
    }
#systemLabelDeviceTable .dataTables_scrollHead thead tr th, #systemLabelDeviceTable .DTFC_LeftHeadWrapper thead tr th {
    padding: 17px;
    border-right: 1px solid #fff;
    color: #fff;
    font-size: 12px;
    text-align: left;
}
#systemLabelDeviceTable .DTFC_LeftHeadWrapper thead{
    background-color: #5d81ac !important;
}
.coll2, .coll13{
    min-width:250px !important;
    /*width:250px !important;*/
    border-bottom: 1px solid #ccc;
}

.coll2 select, .coll13 select, .coll2 input, .coll13 input{
width:230px !important;
}
.coll6{
    min-width:200px !important;
    /*width:200px !important;*/
    border-bottom: 1px solid #ccc;
}
.coll6 select, .coll6 input{
width:180px !important;
}
.coll8{
/*width:60px !important;*/
    min-width:60px !important;
    border-bottom: 1px solid #ccc;
}
.coll8 select, .coll8 input{
    width: 40px !important;
}
.coll3, .coll4, .coll5, .coll7, .coll10, .coll11, .coll12, .coll9{
    /*width:150px !important;*/
    min-width:150px !important;
    border-bottom: 1px solid #ccc;
}
.coll3 select, .coll3 input, .coll4 select, .coll4 input, .coll5 select, .coll5 input, .coll7 select, .coll7 input .coll10 select, .coll10 input, .coll11 select, .coll11 input, .coll12 select, .coll12 input, .coll9 select, .coll9 input{
width:130px !important;
}
.coll1{
    min-width:60px !important;
    /*width:60px !important;*/
        border-left: 1px solid #ccc;
        border-bottom: 1px solid #ccc;
}
.coll14{
    min-width:350px !important;
    /*width:350px !important;*/
    border-bottom: 1px solid #ccc;
    border-right: 1px solid #ccc;
}
.coll14 select, .coll14 input{
    width: 350px !important;
}
.DTFC_LeftBodyLiner{
    overflow:hidden !important;
}
#deviceTypeTable input, #deviceTypeTable select{
    width:100% !important;
}
#deviceTypeTable_wrapper .DTFC_LeftBodyWrapper tbody tr td{
background:#eef0f3;
border-top: 1px solid #ccc !important;
}
#deviceTypeTable tbody tr td{
background:#eef0f3;
border-top: 1px solid #ccc !important;

}
.sysCheck{
margin:0;
}

</style>
<link rel="stylesheet" type="text/css" href="/Express/js/jquery.dataTables.min.css" media="screen">
<link rel="stylesheet" type="text/css" href="/Express/js/fixedColumns.dataTables.min.css" media="screen">
<style>
input[type=checkbox] + label span, input[type=checkbox]:checked + label span{
	height:30px !important;
	width:30px !important;
}

    div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
    #systemLabelDeviceTable .dataTables_scrollBody, #systemLabelDeviceTable .DTFC_LeftBodyWrapper{
        margin-top:-10px;
    }
#systemLabelDeviceTable .dataTables_scrollHead thead tr th, #systemLabelDeviceTable .DTFC_LeftHeadWrapper thead tr th {
    padding: 17px;
    border-right: 1px solid #fff;
    color: #fff;
    font-size: 12px;
    text-align: left;
}
#systemLabelDeviceTable .DTFC_LeftHeadWrapper thead{
    background-color: #5d81ac !important;
}

.coll1{
    min-width:60px !important;
}

.dataTables_scrollHeadInner{
padding-left:0 !important;
}
#deviceTypeTable input, #deviceTypeTable select{
    width:100% !important;
}
#deviceTypeTable_wrapper{
    min-height:500px;
}
</style>
<script src="Devices/deviceTypeManagement/js/deviceTypeManagement.js"></script>
<script src="Devices/deviceTypeManagement/js/parser.js"></script>
<div class="loading" id="deviceTypeLoading" style="margin:0;"><img src="/Express/images/ajax-loader.gif"></div>


<div id="allTablesDivData"></div>
<div id="deviceTypeManagementDialog" class="dialogClass"></div>