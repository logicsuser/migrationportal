<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/devices/deviceTypeManagement.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
$groundId = isset($_SESSION["groupId"]) ? $_SESSION["groupId"] : "None";
$cLUObj = new ChangeLogUtility($_SESSION['sp'], $groundId, $_SESSION["loggedInUserName"]);
$devobj = new DeviceTypeManagement();
$systemTableData = $devobj->getSystemTableDevices();
$vdmArray = $devobj->getVDMTemplate();
//print_r($_POST);exit;
$error = 0;
$nonRequired = array();
$postArray = array();
if(count($_POST["systemData"]) > 0){
    foreach ($_POST["systemData"] as $key => $value){
        if($value["enabled"] == "true"){
            $postArray[$value["deviceType"]]["enabled"] = $value["enabled"];
        }else{
            $postArray[$value["deviceType"]]["enabled"] = "false";
        }
        
        $postArray[$value["deviceType"]]["phoneType"] = $value["phoneType"];
        if(isset($license["softPhone"]) && $license["softPhone"] == "true"){
            $postArray[$value["deviceType"]]["softPhoneType"] = $value["softPhoneType"];
        }
        
        if(isset($value["blf"]) && !empty($value["blf"])){
            $postArray[$value["deviceType"]]["blf"] = $value["blf"];
        }else{
            $postArray[$value["deviceType"]]["blf"] = "No";
        }
        if(isset($license["customTags"]) && $license["customTags"] == "true"){
            $postArray[$value["deviceType"]]["customTagSpec"] = $value["customTagSpec"];
        }
        
        if(isset($license["vdmLite"]) && $license["vdmLite"] == "true"){
            $systemvdm = $vdmArray[$systemTableData[$value["deviceType"]]["vdmTemplate"]];
            if($value["vdmTemplate"] != $systemvdm){
                $postArray[$value["deviceType"]]["vdmTemplate"] = $value["vdmTemplate"];
            }
        }
        /*if(isset($license["vdmLite"]) && $license["vdmLite"] == "true"){
            //echo "system data";print_r($systemTableData[$value["deviceType"]]["vdmTemplate"]);
            //echo "value"; print_r($value["vdmTemplate"]);
           // $systemTableData[$value["deviceType"]]["vdmTemplate"] = $systemTableData[$value["deviceType"]]["vdmTemplate"];
            if( $value["vdmTemplate"] <> $systemTableData[$value["deviceType"]]["vdmTemplate"]) {
                $postArray[$value["deviceType"]]["vdmTemplate"] = $value["vdmTemplate"];
            }
        }*/
        
        $postArray[$value["deviceType"]]["ports"] = $value["ports"];
        $postArray[$value["deviceType"]]["fxoPorts"] = $value["fxoPorts"];
        $postArray[$value["deviceType"]]["sasLines"] = $value["sasLines"];
        if(isset($value["scaOnly"]) && !empty($value["scaOnly"])){
            $postArray[$value["deviceType"]]["scaOnly"] = $value["scaOnly"];
        }else{
            $postArray[$value["deviceType"]]["scaOnly"] = "No";
        }
        if(isset($value["sasTest"]) && !empty($value["sasTest"])){
            $postArray[$value["deviceType"]]["sasTest"] = $value["sasTest"];
        }else{
            $postArray[$value["deviceType"]]["sasTest"] = "No";
        }
        
        $postArray[$value["deviceType"]]["nameSubstitution"] = $value["nameSubstitution"];
        $postArray[$value["deviceType"]]["description"] = $value["description"];
    }
}


//$diffArray = check_diff_multi($systemTableData, $postArray);
//echo "db table data"; print_r($systemTableData);
//echo "post data"; print_r($postArray);exit;
$i = 0;
$changedArray = array();
if(count($postArray) > 0){
    foreach ($postArray as $changeKey => $changeVal){
        if($changeVal["enabled"] != $systemTableData[$changeKey]["enabled"]){
            $changedArray[$changeKey] = $changeVal;
           
        }else if($changeVal["enabled"]=="false" && $systemTableData[$changeKey]["enabled"]=="false"){
            continue;
        }
        
        else{
            if($changeVal["phoneType"] != $systemTableData[$changeKey]["phoneType"]){
                $changedArray[$changeKey]["phoneType"] = $changeVal["phoneType"];
              
            }
            if(isset($license["softPhone"]) && $license["softPhone"] == "true"){
                if($changeVal["softPhoneType"] != $systemTableData[$changeKey]["softPhoneType"]){
                    $changedArray[$changeKey]["softPhoneType"] = $changeVal["softPhoneType"];
                   
                }
            }
            
            if($changeVal["blf"] != $systemTableData[$changeKey]["blf"]){
                $changedArray[$changeKey]["blf"] = $changeVal["blf"];
             
            }
            if(isset($license["customTags"]) && $license["customTags"] == "true"){
                if($changeVal["customTagSpec"] != $systemTableData[$changeKey]["customTagSpec"]){
                    $changedArray[$changeKey]["customTagSpec"] = $changeVal["customTagSpec"];
                    
                }
            }
            if(isset($license["vdmLite"]) && $license["vdmLite"] == "true"){
                $systemTableData[$changeKey]["vdmTemplate"] = $systemTableData[$changeKey]["vdmTemplate"] == "0" ? "" : $systemTableData[$changeKey]["vdmTemplate"];
                if($changeVal["vdmTemplate"] != $systemTableData[$changeKey]["vdmTemplate"]){
                    $changedArray[$changeKey]["vdmTemplate"] = $changeVal["vdmTemplate"];
                 
                }
            }
            
            if($changeVal["ports"] != $systemTableData[$changeKey]["ports"]){
                $changedArray[$changeKey]["ports"] = $changeVal["ports"];
                
                
            }
            if($changeVal["fxoPorts"] != $systemTableData[$changeKey]["fxoPorts"]){
                $changedArray[$changeKey]["fxoPorts"] = $changeVal["fxoPorts"];
                 
                
            }
            if($changeVal["sasLines"] != $systemTableData[$changeKey]["sasLines"]){
                $changedArray[$changeKey]["sasLines"] = $changeVal["sasLines"];
                
            }
            if($changeVal["scaOnly"] != $systemTableData[$changeKey]["scaOnly"]){
                $changedArray[$changeKey]["scaOnly"] = $changeVal["scaOnly"];
               
            }
            if($changeVal["sasTest"] != $systemTableData[$changeKey]["sasTest"]){
                $changedArray[$changeKey]["sasTest"] = $changeVal["sasTest"];
             
            }
            if($changeVal["nameSubstitution"] != $systemTableData[$changeKey]["nameSubstitution"]){
                $changedArray[$changeKey]["nameSubstitution"] = $changeVal["nameSubstitution"];
             
            }
            if($changeVal["description"] != $systemTableData[$changeKey]["description"]){
                $changedArray[$changeKey]["description"] = $changeVal["description"];
            }
        } 
       // $i++ ; 
    } 
    $i++ ; 
}

if(count($systemTableData) > 0){
    foreach ($systemTableData as $sysKey => $sysVal){
        if($sysVal["enabled"] == "true"){
            if(!array_key_exists($sysKey, $postArray)){
               $changedArray[$sysKey]["enabled"] = "false";
            }
        }
    }
}

$changedArray = updatePortNumber($changedArray, $ociVersion);

// print_r($changedArray);
$successData = array();
if(count($changedArray) > 0){
     global $db;
    
     //Code added @ 16 July 2019
     $whereCndtn = "";
     if($_SESSION['cluster_support']){ 
        $selectedCluster = $_SESSION['selectedCluster'];
        $whereCndtn = " AND clusterName ='$selectedCluster' ";
     }
     //End code
    
    foreach ($changedArray as $key => $value){
        $sql = "UPDATE systemDevices SET ";
        /*if(isset($value["enabled"])){
            $sql .= " enabled='".$value["enabled"]."'";
        }*/
        //$i=0;
        //$len = count($value);        
        if(!empty($value)){
            foreach ($value as $key1 => $value1){
                //if($value1 != ""){
                    $sql .= $key1."='".$value1."'";
                    $sql .= ",";
                    
                //}
                
            }
        }
        $sql = rtrim($sql, ",");
        //$sql .= " WHERE deviceType='".$key."'";  //Code commented due to EX-1425 Fixes
        $sql .= " WHERE deviceType='".$key."' $whereCndtn ";    //Code Added due to EX-1425 Fixes
        
        $stmt= $db->prepare($sql);
        if($stmt->execute()){
            $successData[$key] = "success";
            
        }else{
            $successData[$key] = "failed";
        }
    }
    
}
if(count($successData) > 0){
    foreach ($successData as $successKey => $successValue){
        if($successValue == "success"){
            echo "<p style='text-align:center;'>".$successKey." has been updated successfully.</p>";
                
        }else{
            echo "<p style='text-align:center;color:red'>".$successKey." updation failed.</p>";
        }
    }
    
}
$changeLogArray = array();
if(count($changedArray) > 0){
    foreach($changedArray as $key => $value){
        if(isset($value["enabled"]) && $value["enabled"] == "true"){
            foreach ($value as $key1 => $value1){
                
                if($value1 != ""){
                    
                    $changeLogArray[$key][$key1]["oldValue"]=""; 
                    $changeLogArray[$key][$key1]["newValue"]=$value1;
                    if($key1 == "enabled"){
                        $changeLogArray[$key][$key1]["oldValue"]="false"; 
                    }
                }
                
            }
            
            
        }else if(isset($value["enabled"]) && $value["enabled"] == "false"){
            $changeLogArray[$key]["enabled"]["newValue"]="false";
            $changeLogArray[$key]["enabled"]["oldValue"]="true";
        }else{
            foreach ($value as $key2 => $value2){
               // if($key2 == "phoneType"){
                    $changeLogArray[$key][$key2]["newValue"]=$value2;
                    $changeLogArray[$key][$key2]["oldValue"]=$systemTableData[$key][$key2];
                //}
            }
            //$changeLogArray[$key]=
        }
    }
}
//print_r($changeLogArray);
//exit;
foreach($changeLogArray as $deviceTypeKey => $valueChange){

    $cLUObj = new ChangeLogUtility($deviceTypeKey, $groundId, $_SESSION["loggedInUserName"]);
foreach($valueChange as $changeField => $finalVal){
           $module ="Device Type Management Modify";
          $cLUObj->createChangesArray($changeField , $finalVal['oldValue'], $finalVal['newValue']); 
          
       }
       
       $cLUObj->changeLogModifyUtility($module, $deviceTypeKey , $tableName = "deviceTypeManagementChanges");
    
      
}

function updatePortNumber($changedArray, $ociVersion) {
    $devobj = new DeviceTypeManagement();
    
    foreach($changedArray as $deviceKey => $deviceValue) {
        if($deviceValue['enabled'] == "true") {
            $deviceDetailsArray = $devobj->getDeviceTypesDetails(array($deviceKey), $ociVersion);
            $changedArray[$deviceKey]["ports"] = $deviceDetailsArray["Success"][$deviceKey]["numberOfPorts"];
        }
    }
    
    return $changedArray;
}

?>