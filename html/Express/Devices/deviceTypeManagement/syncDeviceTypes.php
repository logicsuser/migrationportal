<?php 
 require_once ("/var/www/lib/broadsoft/login.php");
 checkLogin ();
 require_once("/var/www/html/Express/config.php");
 require_once ("/var/www/lib/broadsoft/adminPortal/devices/deviceTypeManagement.php");
 require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
 $existInDB = array();
 $notExistInDB = array();
 $enabledDevicetypeList = array();
 $existInDBNotInBW = array();
 $devobj = new DeviceTypeManagement();
 
 //
 //$time_start = microtime(true); 
 $deviceTypeList = $devobj->getAllSystemDeviceTypes();
 
 $vdmArray = $devobj->getVDMTemplate();
 $systemTableDeviceTypeList = $devobj->getSystemTableDevicesForDiff();
 $notExistInDB = flip_isset_diff($deviceTypeList["Success"], $systemTableDeviceTypeList);
 $existInDBNotInBW = flip_isset_diff($systemTableDeviceTypeList, $deviceTypeList["Success"]);
 if(count($existInDBNotInBW) > 0){
     $existInDBNotInBWToDel = array();
     foreach ($existInDBNotInBW as $key => $value){
         $existInDBNotInBWToDel[$value] = $value;
     }
 }
 
 if(count($notExistInDB) > 0){
     $notExistInDBToAdd = array();
     $deviceDetailsArray = $devobj->getDeviceTypesDetails($notExistInDB, $ociVersion);
     $notExistInDBToAdd = $deviceDetailsArray["Success"];
 }
 
 if(count($notExistInDBToAdd) > 0){
     global $db;
     $i = 0;
     $len = count($notExistInDBToAdd);     
     //Code added @ 16 July 2019 regarding EX-1425
     if($_SESSION['cluster_support'])
     {
        $selectedCluster = $_SESSION['selectedCluster'];
        $query = "INSERT INTO systemDevices (`clusterName`, `deviceType`, `ports`) VALUES";
        foreach ($notExistInDBToAdd as $key => $value)
        {
            $query .= "('".$selectedCluster ."', '".$key ."', '". $value["numberOfPorts"]."')";
            if($i != $len - 1)
            {
                $query .= ", ";
            }
            $i++;
        }        
     }else{
         $query = "INSERT INTO systemDevices (`deviceType`, `ports`) VALUES";
         foreach ($notExistInDBToAdd as $key => $value){
            $query .= "('".$key ."', '". $value["numberOfPorts"]."')";
            if($i != $len - 1){
                $query .= ", ";
            }
            $i++;
        }
     }
     
     //Code commented @ 16 July 2019 regarding EX-1425
     /*$query = "INSERT INTO systemDevices (`deviceType`, `ports`) VALUES";
         foreach ($notExistInDBToAdd as $key => $value){
            $query .= "('".$key ."', '". $value["numberOfPorts"]."')";
            if($i != $len - 1){
                $query .= ", ";
            }
            $i++;
        }
     */     
     $db->exec($query);
 }
 
 if(count($existInDBNotInBWToDel) > 0){
     global $db;
     $n = 0;
     $len = count($existInDBNotInBWToDel);
     
     //Code added @ 16 July 2019
     $whereCndtn = " AND 1=1 ";
     if($_SESSION['cluster_support']){ 
        $selectedCluster = $_SESSION['selectedCluster'];
        $whereCndtn = " AND clusterName ='$selectedCluster' ";
     }
     //End code
     
     $sql = "DELETE FROM systemDevices WHERE deviceType IN (";
     foreach ($existInDBNotInBWToDel as $key => $value){
         $sql .= "'".$key ."'";
         if($n != $len - 1){
             $sql .= ", ";
         }
         $n++;
     }
     //$sql .= ") ";            //Code commented due EX-1425 Fixes
     $sql .= ") $whereCndtn ";  //Code Added due EX-1425 Fixes
     $db->exec($sql);
 }
// 
 
 $systemTableAllData = $devobj->getSystemTableDevices();
 if(count($systemTableAllData) > 0){
     foreach ($systemTableAllData as $dkey => $dvalue){
         if($dvalue["enabled"] == "true"){
             $enabledDevicetypeList[$dkey] = $dvalue;
         }
     }
     
 }

 $enterpriseDeviceTypeList = $devobj->getEnterpriseTableDevices($_SESSION["sp"]);
// print_r($enabledDevicetypeList);
 if(count($enterpriseDeviceTypeList) > 0){
     foreach ($enabledDevicetypeList as $key => $value){
         if(array_key_exists($key, $enterpriseDeviceTypeList)){
             $enabledDevicetypeList[$key]["enabled"]=$enterpriseDeviceTypeList[$key]["enabled"];
             $enabledDevicetypeList[$key]["blf"]=$enterpriseDeviceTypeList[$key]["blf"];
             $enabledDevicetypeList[$key]["existinenterprise"]="Yes";
         }else{
             $enabledDevicetypeList[$key]["existinenterprise"]="No";
         }
     }
 }
 
 function flip_isset_diff($b, $a) {
     $at = array_flip($a);
     $d = array();
     foreach ($b as $i)
         if (!isset($at[$i]))
             $d[] = $i;
             
             return $d;
 }
 //$time_end = microtime(true);
 //$execution_time = ($time_end - $time_start);
 
 //execution time of the script
 //echo '<b>Total Execution Time:</b> '.$execution_time.' Sec';
?>

<script type="text/javascript">

var customTagsLicense = '<?php if ( isset($license["customTags"]) && $license["customTags"] == "false"){ echo "false"; } else { echo "true"; } ?>';
var softPhoneLicense = '<?php if ( isset($license["softPhone"]) && $license["softPhone"] == "false"){ echo "false"; } else { echo "true"; } ?>';
var vdmLiteLicense = '<?php if ( isset($license["vdmLite"]) && $license["vdmLite"] == "false"){ echo "false"; } else { echo "true"; } ?>';

$(function(){
	$("#enterpriseLabelDeviceTable").hide();
	$("#systemLabelDeviceTable").show();
	$("#downloadCSVDeviceType").show();
	$("#downloadCSVEntDeviceType").hide();

	if( "<?php echo $_SESSION['superUser']; ?>" == 3) {
		$("#enterpriseLabelDeviceTable").show();
    	$("#systemLabelDeviceTable").hide();
    	$("#downloadCSVDeviceType").hide();		/* Download CSV for Syster */
    	$("#downloadCSVEntDeviceType").show();	/* Download CSV for Enterprise */
    	$(".sampleDeviceCSVDiv").hide();		
	}
	
	$('.systemTableCheckbox').click(function(){
		var row = $(this).closest('tr');
        if($(this).prop("checked") == true){
        	$(row).find('select').prop("disabled",false);
        	$(row).find('input').prop("disabled",false); 
        }
        else if($(this).prop("checked") == false){
        	$(row).find('select').prop("disabled",true);
        	$(row).find('input').prop("disabled",true);
        	$(row).find('input[type="checkbox"]').prop("disabled",false);
        }
    });
	

});

</script>
<form name="searchDeviceOptionDevType" id="searchDeviceOptionDevType" class="">
	<div class="searchDeviceOptionDiv">
    	<?php if( $_SESSION['superUser'] != 3 ) { ?>
    	    <div class="row">
			<div class="form-group" style="text-align: center">
				<label class="labelText" for="limitSearchTo" style="margin-right: 20px;">Search In :</label>
				<input type="radio" class="searchlevel" name="searchDevicelevel" id="searchDevicelevelSys" value="system" style="width: 5%;" checked>
				<label for="searchDevicelevelSys"><span></span></label> 
				<label class="labelText" for="deviceClassVoIP">System</label> 
				<input type="radio" class="searchlevel" name="searchDevicelevel" id="searchDevicelevelEnt" value="enterprise" class="searchlevelIn" style="width: 5%;">
				<label for="searchDevicelevelEnt"><span></span></label> 
				<label class="labelText" for="deviceClassAnalogGateway">Enterprise</label>
			</div>
		</div>
    	<?php } else { ?>
    			<div style="display: none">
    	    		<input type="radio" class="searchlevel" name="searchDevicelevel" id="searchDevicelevelEnt" value="enterprise" class="searchlevelIn" style="width: 5%;" checked>
					<label for="searchDevicelevelEnt"><span></span></label> 
				</div>
    	<?php } ?>

		<div class="row" style="">
        	<div class="">
        		<div class="form-group" style="margin: 0 auto;;width:830px">
        			<label class="labelText" for="searchVal">Search Device Type</label><br>
        			<span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
        			<input style="width:100%" type="text" class="autoFill magnify ui-autocomplete-input" name="searchDeviceType" id="searchDeviceType" size="55" value="" autocomplete="off">
        		</div>
        	</div>	
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group alignBtn">
						<input type="button" name="searchDeviceBtn" id="searchDeviceBtn" value="Search" class="subButton">
				</div>
			</div>
		</div>
		<div class="row sampleDeviceCSVDiv">
			<div class="col-md-12">
				<div style="clear:right;">&nbsp;</div>
				<strong><label style="margin-bottom: 5px" for="file" class="labelTextGrey">Please upload a CSV or XLSX file below:</label></strong>
				<div class="" style="margin-bottom: 5px;">
                <input style="color:#9c9c9c;width: 82px;" type="file" class="inputfile labelTextGrey" name="deviceTypeFile" id="deviceTypeFile" onchange="handle_files(this.files)" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.csv,text/csv">							   
				</div>
				<div><a id="sampleDeviceCSV"><label class="labelText" style="margin-left:0;cursor: pointer;">Click here</label></a>&nbsp;<label class="labelTextGrey">for a sample device type template in CSV format</label></div>
			</div>
		</div>
		<!-- <div class="row">
			<div class="col-md-12">
				
				<div class="form-group alignBtn">
					<input type="button" name="" id=""
						value="Search" class="subButton">
				</div>
			</div>
		</div>-->
	</div>

</form>

<div class="row" style="padding-top:25px;">
    	<div class="col-md-4">
    		<label class="labelText"> Show: </label> <br>
    		<input name="showViewRadioBtn" id="showAllView" value="All" checked="" type="radio">
    		<label for="showAllView"><span></span></label>
    		<label class="labelText filterMargin" for="">Show All</label>
    		
    		<input name="showViewRadioBtn" id="showEnabledView" value="Enabled" style="width: 5%;" type="radio"> 
    		<label for="showEnabledView"><span></span></label>
    		<label class="labelText filterMargin" for="">Show Enabled</label>
    	</div>
		<div class="col-md-4">
			<div class="deviceLoadingBar" id="deviceLoadingBar" style="text-align: center;display:none">
				<img src="/Express/images/ajax-loader.gif">
			</div>
		</div>
		<div class="col-md-4">
    		<div class="downloadCSVDeviceType" id="downloadCSVDeviceType">
        		<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" style="cursor:pointer;">
                                        					<br><span>Download<br>CSV</span>
            </div>
        	<div class="downloadCSVEntDeviceType" id="downloadCSVEntDeviceType" style="margin-bottom:10px;">
        		<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" style="cursor:pointer;">
                                        					<br><span>Download<br>CSV</span>
            </div>
		</div>
</div>
	
<div class="row" id="systemLabelDeviceTable" style="margin:0 auto;width:100%;">
    
    <?php 
    $customTagVisibility = "";
    if(isset($license["customTags"]) && $license["customTags"] == "false"){
        $customTagVisibility = "none";
    } 
    $softPhoneVisibility = "";
    if(isset($license["softPhone"]) && $license["softPhone"] == "false"){
        $softPhoneVisibility = "none";
    }
    $vdmVisibility = "";
    if(isset($license["vdmLite"]) && $license["vdmLite"] == "false"){
        $vdmVisibility = "none";
    }
    ?>
    
<form name="systemDevicesTableForm" id="systemDevicesTableForm" style="width:100%;">
	<table id="deviceTypeTable" class="stripe row-border order-column customDataTable" style="width:100%;">
    		<thead>
    			<tr>
    				<th class="coll1" >Enabled</th>
    				<th class="triggerColumnSystemTable coll2">Device Type</th>
    				<th class="coll3">Phone Type</th>
    				<th class="coll4" style="display: <?php echo $softPhoneVisibility; ?>;">Soft Phone Type</th>
    				<th class="coll5">BLF</th>
    				<th class="coll6" style="display: <?php echo $customTagVisibility; ?>;">Custom Tag Spec</th>
    				<th class="coll7" style="display: <?php echo $vdmVisibility; ?>;">VDM</th>
    				<th class="coll8">Ports</th>
    				<th class="coll9">FXO Ports</th>
    				<th class="coll10">SAS Lines</th>
    				<th class="coll11">SCA Only</th>
    				<th class="coll12">SAS Test</th>
    				<th class="coll13">Name Substitution</th>
    				<th class="coll14">Description</th>
    			</tr>
    		</thead>
    		<tbody style="border:none !important;">
    		<?php 
    		$k=0;
    		foreach ($systemTableAllData as $nkey => $nvalue){ 
    		$checkBoxId = "systemCheckbox".$k;
    		$phoneTypeId = "phoneType_".$k;
    		$blfId = "blf_".$k;
    		?>
    		    <tr id="systetDeviceTableRow">
            	    <td class="coll1"><span style="display:none;"><?php echo $nvalue["enabled"]; ?></span>
            	    <input <?php if($nvalue["enabled"] == "true"){echo "checked";}else{echo "";} ?> type="checkbox" name="systemData[<?php echo $k; ?>][enabled]" id="<?php echo $checkBoxId; ?>" class="systemTableCheckbox" value="true"><label class="sysCheck" for="<?php echo $checkBoxId; ?>"><span></span></label>
            	    	<input type="hidden" class="statusOfDevice" value="<?php echo $nvalue["enabled"]; ?>">
            	    </td>
            	    <td class="coll2"><?php echo $nkey; ?><input type="hidden"name="systemData[<?php echo $k; ?>][deviceType]" value="<?php echo $nkey; ?>"></td>
            	    <td class="coll3"><span style="display:none;"><?php echo $nvalue["phoneType"]; ?></span>
            	    	<div class="dropdown-wrap">
            	    	<select name ="systemData[<?php echo $k; ?>][phoneType]" id="<?php echo $phoneTypeId; ?>" onchange="getSelectedVal(this);" <?php if($nvalue["enabled"] == "false"){echo "disabled";}else{echo "";} ?>>
            	    		<option value=""></option>
            	    		<option value="SIP Phone" <?php if($nvalue["phoneType"] == "SIP Phone"){echo "selected"; } ?>>SIP Phone</option>
            	    		<option value="Analog"<?php if($nvalue["phoneType"] == "Analog"){echo "selected"; } ?>>Analog</option>
            	    		<?php if(isset($license["softPhone"]) && $license["softPhone"] == "true"){ ?>
            	    		<option value="Soft Phone"<?php if($nvalue["phoneType"] == "Soft Phone"){echo "selected"; } ?>>Soft Phone</option>
            	    		<?php } ?>
            	    		
            	    	</select>
            	    	</div>
            	    </td>
            	    <td class="coll4" style="display: <?php echo $softPhoneVisibility; ?>;">
            	    <span style="display:none;"><?php echo $nvalue["softPhoneType"]; ?></span>
            	    <div class="dropdown-wrap">
            	    	<select name ="systemData[<?php echo $k; ?>][softPhoneType]" class="selectsoftPhoneType" id="" <?php if($nvalue["enabled"] == "false"){echo "disabled";}else{echo "";} ?>>
            	    		<option value=""></option>
            	    		<option value="generic"<?php if($nvalue["softPhoneType"] == "generic"){echo "selected"; } ?>>Generic</option>
            	    		<option value="counterPath" <?php if($nvalue["softPhoneType"] == "counterPath"){echo "selected"; } ?>>Counterpath</option>
            	    	</select>
            	    	</div>
            	    </td>
            	    
            	     <td class="coll5">
            	     <span style="display:none;"><?php echo $nvalue["blf"]; ?></span>
            	     <div class="dropdown-wrap">
            	    	<select name ="systemData[<?php echo $k; ?>][blf]" id="<?php echo $blfId; ?>" <?php if($nvalue["enabled"] == "false"){echo "disabled";}else{echo "";} ?>>
            	    		<option value="No" <?php if($nvalue["blf"] == "No"){echo "selected"; } ?>>No</option>
            	    		<option value="Yes" <?php if($nvalue["blf"] == "Yes"){echo "selected"; } ?>>Yes</option>
            	    		
            	    	</select>
            	    	</div>
            	    </td>
            	    <td class="coll6" style="display: <?php echo $customTagVisibility; ?>;">
            	    <span style="display:none;"><?php echo $nvalue["customTagSpec"]; ?></span>
            	    	<input type="text" name="systemData[<?php echo $k; ?>][customTagSpec]" id="" value="<?php echo $nvalue["customTagSpec"]; ?>" <?php if($nvalue["enabled"] == "false"){echo "disabled";}else{echo "";} ?>>
            	    </td>
            	    <td class="coll7" style="display: <?php echo $vdmVisibility; ?>;">
            	    <span style="display:none;"><?php echo $nvalue["vdmTemplate"]; ?></span>
            	    <div class="dropdown-wrap">
            	    	<select name ="systemData[<?php echo $k; ?>][vdmTemplate]" class="selectVdmvdmTemplate" id="" <?php if($nvalue["enabled"] == "false"){echo "disabled";}else{echo "";} ?>>
            	    	<option></option>
            	    	<?php
            	    	foreach ($vdmArray as $vdmKey => $vdmValue){ ?>
            	    	   <option <?php if($nvalue["vdmTemplate"] == $vdmKey){echo "selected"; } ?> value="<?php echo $vdmKey; ?>"><?php echo $vdmValue; ?></option> 
            	    	<?php }
            	    	?>
            	    	</select>
            	    	</div>
            	    </td>
            	    
            	    <td class="coll8">
            	    	<?php echo $nvalue["ports"]; ?><input type="hidden" name="systemData[<?php echo $k; ?>][ports]" value="<?php echo $nvalue["ports"]; ?>">
            	    </td>
            	    <td class="coll9">
            	    <span style="display:none;"><?php echo $nvalue["fxoPorts"]; ?></span>
            	    	<input type="text" name="systemData[<?php echo $k; ?>][fxoPorts]" id="" value="<?php echo $nvalue["fxoPorts"]; ?>" <?php if($nvalue["enabled"] == "false"){echo "disabled";}else{echo "";} ?>>
            	    </td>
            	    <td class="coll10">
            	    <span style="display:none;"><?php echo $nvalue["sasLines"]; ?></span>
            	    	<input type="text" name="systemData[<?php echo $k; ?>][sasLines]" id="" value="<?php echo $nvalue["sasLines"]; ?>" <?php if($nvalue["enabled"] == "false"){echo "disabled";}else{echo "";} ?>>
            	    </td>
            	    <td class="coll11">
            	    <span style="display:none;"><?php echo $nvalue["scaOnly"]; ?></span>
            	    <div class="dropdown-wrap">
            	    	<select name ="systemData[<?php echo $k; ?>][scaOnly]" id="" <?php if($nvalue["enabled"] == "false"){echo "disabled";}else{echo "";} ?>>
            	    		<option value="No" <?php if($nvalue["scaOnly"] == "No"){echo "selected"; } ?>>No</option>
            	    		<option value="Yes" <?php if($nvalue["scaOnly"] == "Yes"){echo "selected"; } ?>>Yes</option>
            	    	</select>
            	    	</div>
            	    </td>
            	    <td class="coll12">
            	    <span style="display:none;"><?php echo $nvalue["sasTest"]; ?></span>
            	    <div class="dropdown-wrap">
            	    	<select name ="systemData[<?php echo $k; ?>][sasTest]" id="" <?php if($nvalue["enabled"] == "false"){echo "disabled";}else{echo "";} ?>>
            	    		<option value="No" <?php if($nvalue["sasTest"] == "No"){echo "selected"; } ?>>No</option>
            	    		<option value="Yes"<?php if($nvalue["sasTest"] == "Yes"){echo "selected"; } ?>>Yes</option>
            	    	</select>
            	    	</div>
            	    </td>
            	    <td class="coll13">
            	    <span style="display:none;"><?php echo $nvalue["nameSubstitution"]; ?></span>
            	    	<input type="text" name="systemData[<?php echo $k; ?>][nameSubstitution]" id="" value="<?php echo $nvalue["nameSubstitution"]; ?>" <?php if($nvalue["enabled"] == "false"){echo "disabled";}else{echo "";} ?>>
            	    </td>
            	    <td class="coll14">
            	    <span style="display:none;"><?php echo $nvalue["description"]; ?></span>
            	    	<input type="text" name="systemData[<?php echo $k; ?>][description]" id="" value="<?php echo $nvalue["description"]; ?>" <?php if($nvalue["enabled"] == "false"){echo "disabled";}else{echo "";} ?>>
            	    </td>
    	    	</tr>
    		<?php 
    		$k++;
    		} ?>
    		    
    	    	
    	    	      
    	    </tbody>
    	</table>
    	<div class="row">
			<div class="form-group">
				<div class="col-md-4"></div>
				<div class="col-md-4" style="text-align:center;">
					<input type="button" id="systemSubmit" value="Confirm Settings" class="subButton">
				</div>
				<div class="col-md-4" style="text-align:right;">
					
				</div>
			</div>
		</div>
    	</form>
	</div>
	
	<div class="row" id="enterpriseLabelDeviceTable" style="margin:0 auto;">	
	<form name="enterPriseDeviceTableForm" id="enterPriseDeviceTableForm">
		<table id="deviceTypeTableEnterprise" class="scroll tablesorter">
    		<thead>
    			<tr>
    				
    				<th class=" " style="width:8%;">Enabled</th>
    				<th class=" " style="width:20%;">Device Type</th>
    				<th class=" " style="width:13%;">Phone Type</th>
    				<th class=" " style="width:13%;">BLF</th>
    				<th class=" " style="width:11%;">Ports</th>
    				<th class=" " style="width:35%;">Description</th>
    			</tr>
    		</thead>
    		<tbody style="border:none !important;">
    		<?php if(count($enabledDevicetypeList) > 0){ ?>
    		    
    		
    		<?php 
    		$i = 0;
    		foreach ($enabledDevicetypeList as $tkey => $tvalue){
    		    $checkBoxIdEnt = "checkBoxEnabledEnt".$i;
    		    $disabled="";
    		    if($tvalue["existinenterprise"] == "Yes"){
    		        $disabled = "";
    		    }else if($tvalue["blf"] == "No"){
    		        $disabled = "disabled";
    		    }
    		
    		?>
    		<tr>
            	    <td style="width:8%;" class=""><input class="enterpriseTableCheckbox" type="hidden" name="enterpriseData[<?php echo $i; ?>][enabled]" value="false"><input class="enterpriseTableCheckboxClass" type="checkbox" <?php if($tvalue["enabled"] == "true"){echo "checked"; }?> name="enterpriseData[<?php echo $i; ?>][enabled]" value="true" id="<?php echo $checkBoxIdEnt; ?>"><label for="<?php echo $checkBoxIdEnt; ?>"><span></span></label>
            	    	<input type="hidden" class="statusOfDevice" value="<?php echo $tvalue["enabled"]; ?>">
            	    </td>
            	    <td class=" " style="width:20%;"><?php echo $tkey; ?><input type="hidden"name="enterpriseData[<?php echo $i; ?>][deviceType]" value="<?php echo $tkey; ?>"></td>
            	    <!--<td class=" " style="width:13%;">
            	    	<div class="dropdown-wrap" style="width: 100%;">
            	    	<select name ="enterpriseData[<?php // echo $i; ?>][phoneType]" id="" disabled>
            	    		<option value=""></option>
            	    		<option value="SIP Phone" <?php  //if($tvalue["phoneType"] == "SIP Phone"){echo "selected"; }?>>SIP Phone</option>
            	    		<option value="Analog" <?php // if($tvalue["phoneType"] == "Analog"){echo "selected"; }?>>Analog</option>
            	    		<option value="Soft Phone" <?php // if($tvalue["phoneType"] == "Soft Phone"){echo "selected"; }?>>Soft Phone</option>
            	    	</select>
            	    	</div>
            	    </td>-->
            	    <td class=" " style="width:13%;">
                       <?php echo $tvalue["phoneType"]; ?>
                        <input type="hidden" name ="enterpriseData[<?php echo $i; ?>][phoneType]" id="" value="<?php echo $tvalue["phoneType"]; ?>" disabled>
                    </td> 
            	     <td class=" " style="width:13%;">
            	     <div class="dropdown-wrap" style="width: 100%;">
            	    	<select name ="enterpriseData[<?php echo $i; ?>][blf]" id="" <?php echo $disabled; ?>>
            	    		<option value="No" <?php if($tvalue["blf"] == "No"){echo "selected"; }?>>No</option>
            	    		<option value ="Yes" <?php if($tvalue["blf"] == "Yes"){echo "selected"; }?>>Yes</option>
            	    		
            	    	</select>
            	    	</div>
            	    </td>
            	    
            	    <td class=" " style="width:11%;">
            	    	<?php echo $tvalue["ports"]; ?><input type="hidden" value="<?php echo $tvalue["ports"]; ?>">
            	    </td>
            	    <td class=" " style="width:35%;">
            	    	<input type="text" name="enterpriseData[<?php echo $i; ?>][description]" id="" value="<?php echo $tvalue["description"]; ?>" disabled>
            	    </td>
            	    
    	    	</tr> 
    		<?php
    		$i++;
            } ?>
    		<?php }else{ ?>
    		    <tr>
    		    <td>No Data Found</td>
    		    </tr>
    		<?php } ?> 
    	    </tbody>
    	</table>
    	
    	<?php 
    	if(count($enabledDevicetypeList) > 0){ ?>
    	    <div class="row">
			<div class="form-group">
				<div class="col-md-4"></div>
				<div class="col-md-4" style="text-align:center;">
					<input type="button" id="enterpriseSubmit" value="Confirm Settings" class="subButton">
				</div>
				<div class="col-md-4" style="text-align:right;">
					
				</div>
			</div>
		</div>
    	<?php } ?>
    	
    	</form>
	</div>

<div id="bulkDeviceModValidationDialog">
	<div class="dialogLoadingBar" id="dialogLoadingBar" style="text-align: center;display:none">
		<img src="/Express/images/ajax-loader.gif">
	</div>
	<div id="csvDeviceData"></div>
</div>

<script type="text/javascript">

$(document).ready(function() {

	    var deviceTypeTable = $('#deviceTypeTable').DataTable( {
	        scrollY:        "450px",
	        scrollX:        true,
	        scrollCollapse: true,
	        paging:         false,
	        searching: 		false,
	        
	        /*"columns": [
	        	{ "orderDataType": "dom-checkbox" },
		        null,
				{ "orderDataType": "dom-select" },
				{ "orderDataType": "dom-select" },
				{ "orderDataType": "dom-select" }, //blf
				{ "orderDataType": "dom-text", type: 'string' },
				{ "orderDataType": "dom-select" }, //vdm
				null,
				{ "orderDataType": "dom-text-numeric" },
				{ "orderDataType": "dom-text-numeric" }, //saslines
				{ "orderDataType": "dom-select" },
				{ "orderDataType": "dom-select" }, //sastest
				{ "orderDataType": "dom-text", type: 'string' },
				{ "orderDataType": "dom-text", type: 'string' }
				
	        ],*/
	        fixedColumns:   {
	            leftColumns: 2
	        },
	        "initComplete": function(){
	        	setTimeout(function(){
	        		$(".DTFC_LeftWrapper .triggerColumnSystemTable").trigger("click"); 
		        }, 500);
	            
	          }
	    } ); 

	    deviceTypeTable
	    .on( 'preDraw', function () {
	        startTime = new Date().getTime();
	    } )
	    .on( 'draw.dt', function () {
	    	//setTimeout(function(){
	    		$("#systemDevicesTableForm").find(".DTFC_LeftBodyLiner").scrollTop(0);
	    		//var scrollTbl = $("#systemDevicesTableForm").find(".dataTables_scrollBody").scrollLeft();
		    	//$("#systemDevicesTableForm").find(".dataTables_scrollBody").scrollTop(0);
		    //}, 1000);
	    	
	    } );

	    $('#searchDeviceType').on('keypress', function(e) { 
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) {
				e.preventDefault();
					$('#searchDeviceBtn').trigger('click');
			  }
		});
		
		
				
	    $('.sysCheck').click(function(){
	    	if($(this).closest('tr').find('input[type="checkbox"]').prop('checked') == true){
	    		$(this).closest('tr').find('input[type="checkbox"]').prop('checked', false);
			}else if($(this).closest('tr').find('input[type="checkbox"]').prop('checked') == false){
				$(this).closest('tr').find('input[type="checkbox"]').prop('checked', true);
			}
    	});

	    $("#deviceTypeTableEnterprise").tablesorter({
	    	widgetOptions : {
	            group_checkbox: [ "checked", "unchecked" ]
	        },
	    	headers: {
	    		0: { sorter: "checkbox", filter: "parsed" },
	    		2: { sorter: "select", filter: "parsed" },
				3: { sorter: "select", filter: "parsed" },
				5: { sorter: "inputs", filter: "parsed" }
			}
		});

} );

function getSelectedVal(sel)
{
	var selId = sel.id.split("_");
	var idNum = "blf_"+selId[1];
	if(sel.value == "SIP Phone"){
		
		$("#"+idNum).val("Yes");
	}else{
		$("#"+idNum).val("No");
	}
    
}

$("#bulkDeviceModValidationDialog").dialog({
	autoOpen: false,
	width: 800,
	modal: true,
	position: { my: "top", at: "top" },
	resizable: false,
	closeOnEscape: false,
	
	buttons: {
		"Cancel": function() {
			$(this).dialog("close");
		},
		"Modify Device Types": function(){
			pendingProcess.push("Device Types Management Modify");
			$("#dialogLoadingBar").show();
			$(":button:contains('Modify Device Types')").hide();
			$(":button:contains('Cancel')").hide();
			$.ajax({
	            url: "Devices/deviceTypeManagement/uploadDevicesCSV.php",
	            cache: false,
	            data: {action: 'updateDeviceDetails'},
	            type: 'POST',
	            success: function (result) {
	            	if(foundServerConErrorOnProcess(result, "Device Types Management Modify")) {
						return false;
	               	}
	               $("#dialogLoadingBar").hide();
	               $("#csvDeviceData").prepend(result);
	               $(":button:contains('More Changes')").show();
	           	   $(":button:contains('Return to Main')").show();
	            }
			});
			
		},
		"More Changes": function() {
			$("html, body").animate({ scrollTop: 0 }, 600);
			$(this).dialog("close");
     	    syncDeviceTypesData();
		},
		"Return to Main": function() {
			location.href="main_enterprise.php";
		}
	},
    open: function() {
		setDialogDayNightMode($(this));
    	$(":button:contains('Modify Device Types')").addClass("subButton").show();
    	$(":button:contains('Cancel')").addClass("cancelButton").show();
    	$(":button:contains('More Changes')").addClass("cancelButton");
    	$(":button:contains('Return to Main')").addClass("cancelButton");
    	$(":button:contains('More Changes')").hide();
    	$(":button:contains('Return to Main')").hide();
    	$(this).closest(".ui-dialog").find(".ui-dialog-titlebar-close").addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only").html("<span class='ui-button-icon-primary ui-icon ui-icon-closethick'></span>");
    	
    }
});

$("#deviceTypeFile").click(function () {
    $("#deviceTypeFile").val("");
});

function handle_files(files) {
	$("#dialogLoadingBar").show();
    for (i = 0; i < files.length; i++) {
        file = files[i];
        console.log(file);
        var form_data = new FormData();
        form_data.append('file', file);
        form_data.append('action', "uploadDeviceFile");
		$("#bulkDeviceModValidationDialog").dialog("open");
		$("#csvDeviceData").html("");
        $("#deviceTypeFile").prop("disabled", true);
        $.ajax({
            url: "Devices/deviceTypeManagement/uploadDevicesCSV.php",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'POST',
            success: function (result) {
               $("#dialogLoadingBar").hide();
               if ( result.indexOf("Please correct errors and resubmit") != -1 || result.indexOf("Column Name Mismatch") != -1 || result.indexOf("No Device Type Data Found for update") != -1 || result.indexOf("Upload only CSV/XLSX file") != -1 )
        		{
        			$(":button:contains('Modify Device Types')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
        		}
        		else
        		{
        			$(":button:contains('Modify Device Types')").removeAttr("disabled").removeClass("ui-state-disabled");
        		}
               $("#csvDeviceData").html(result);
               $("#deviceTypeFile").prop("disabled", false);
            }
        });

    }
}
</script>