<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/devices/deviceTypeManagement.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
$groundId = isset($_SESSION["groupId"]) ? $_SESSION["groupId"] : "None";
$cLUObj = new ChangeLogUtility($_SESSION['sp'], $groundId, $_SESSION["loggedInUserName"]);
$devobj = new DeviceTypeManagement();
$enterpriseDeviceTypeList = $devobj->getEnterpriseTableDevices($_SESSION["sp"]);
$systemTableData = $devobj->getSystemTableDevices();
//print_r($_POST);
$error = 0;
$postDevices = array();
if(count($_POST) > 0){
    foreach ($_POST["enterpriseData"] as $pKey => $pValue){
        $postDevices[$pValue["deviceType"]]["enabled"] =  $pValue["enabled"];
        if(isset($pValue["blf"]) && !empty($pValue["blf"])){
            $postDevices[$pValue["deviceType"]]["blf"] =  $pValue["blf"];
        }else{
            $postDevices[$pValue["deviceType"]]["blf"] =  "No";
        }
        
    }
}
$changedArray["system"] = array();
$changedArray["enterprise"] = array();
if(count($postDevices) > 0){
    foreach ($postDevices as $kKey => $kValue){
        if(array_key_exists($kKey, $enterpriseDeviceTypeList)){
            if($kValue["enabled"] !=$enterpriseDeviceTypeList[$kKey]["enabled"]){
                $changedArray["enterprise"][$kKey]["enabled"]=$kValue["enabled"];
            }
            if($kValue["blf"] !=$enterpriseDeviceTypeList[$kKey]["blf"]){
                $changedArray["enterprise"][$kKey]["blf"]=$kValue["blf"];
            }
        }else if(array_key_exists($kKey, $systemTableData)){
            if($kValue["enabled"] !=$systemTableData[$kKey]["enabled"]){
                $changedArray["system"][$kKey]["enabled"]=$kValue["enabled"];
            }
            if($kValue["blf"] !=$systemTableData[$kKey]["blf"]){
                $changedArray["system"][$kKey]["blf"]=$kValue["blf"];
            }
        }
    }
}
$entArray["remove"] = array();
$entArray["modify"] = array();
$entArray["add"] = array();
if(count($changedArray["enterprise"]) > 0){
    foreach ($changedArray["enterprise"] as $key => $value){
        if(($postDevices[$key]["enabled"] == $systemTableData[$key]["enabled"]) && ($postDevices[$key]["blf"] == $systemTableData[$key]["blf"])){
            $entArray["remove"][$key]="remove";
        }else{
            if(isset($value["enabled"])){
                $entArray["modify"][$key]["enabled"]=$value["enabled"];
            }
            if(isset($value["blf"])){
                $entArray["modify"][$key]["blf"]=$value["blf"];
            }
        }
    }
}

if(count($changedArray["system"]) > 0){
    foreach ($changedArray["system"] as $key => $value){
        if(isset($value["enabled"])){
            $entArray["add"][$key]["enabled"]=$value["enabled"];
        }else{
            $entArray["add"][$key]["enabled"]=$systemTableData[$key]["enabled"];
        }
        if(isset($value["blf"])){
            $entArray["add"][$key]["blf"]=$value["blf"];
        }else{
            $entArray["add"][$key]["blf"]=$systemTableData[$key]["blf"];
        }
        $entArray["add"][$key]["phoneType"]=$systemTableData[$key]["phoneType"];
        $entArray["add"][$key]["ports"]=$systemTableData[$key]["ports"];
        $entArray["add"][$key]["description"]=$systemTableData[$key]["description"];
        $entArray["add"][$key]["enterpriseId"]=$_SESSION["sp"];
        $entArray["add"][$key]["clusterName"]=$clusterName;
    }
}
$successData1= array();
if(count($entArray["remove"]) > 0){
    global $db;
    //Code added @ 17 July 2019
    $whereCndtn = " ";
    if($_SESSION['cluster_support']){ 
       $selectedCluster = $_SESSION['selectedCluster'];
       $whereCndtn = " AND clusterName ='$selectedCluster' ";
    }
    //End code
    $n = 0;
    $len = count($entArray["remove"]);
    $sql = "DELETE FROM enterpriseDevices WHERE deviceType IN (";
    foreach ($entArray["remove"] as $key => $value){
        $successData1[$key] = "success";
        $sql .= "'".$key ."'";
        if($n != $len - 1){
            $sql .= ", ";
        }
        $n++;
    }
    $sql .= ") $whereCndtn";
    $db->exec($sql);
}
$successData2= array();
if(count($entArray["modify"]) > 0){
    global $db;
    
    //Code added @ 17 July 2019
    $whereCndtn = " ";
    if($_SESSION['cluster_support']){ 
       $selectedCluster = $_SESSION['selectedCluster'];
       $whereCndtn = " AND clusterName ='$selectedCluster' ";
    }
    //End code
    
    foreach ($entArray["modify"] as $key => $value){
        $sql = "UPDATE enterpriseDevices SET ";
        /*if(isset($value["enabled"])){
         $sql .= " enabled='".$value["enabled"]."'";
         }*/
        $i=0;
        $len = count($value);
        if(!empty($value)){
            foreach ($value as $key1 => $value1){
                $sql .= $key1."='".$value1."'";
                if($i != $len - 1){
                    $sql .= ", ";
                }else{
                    $sql .= " ";
                }
                $i++;
            }
        }
        $sql .= "WHERE deviceType='".$key."' $whereCndtn ";
        $stmt= $db->prepare($sql);
        if($stmt->execute()){
            $successData2[$key] = "success";
        }else{
            $successData2[$key] = "failed";
        }
    }
    
}

$successData3= array();
if(count($entArray["add"]) > 0){//to do
    global $db;
    //Code added @ 17 July 2019
    $selectedCluster = "";
    if($_SESSION['cluster_support']){
       $selectedCluster = $_SESSION['selectedCluster'];
    }
    //End code
    $i = 0;
    $len = count($entArray["add"]);
    $query = "INSERT INTO enterpriseDevices (`clusterName`, `enterpriseId`, `enabled`, `deviceType`, `phoneType`, `blf`, `ports`, `description`) VALUES";
    foreach ($entArray["add"] as $key => $value){
        $successData3[$key] = "success";
        $query .= "('".$selectedCluster ."', '".$value["enterpriseId"] ."', '".$value["enabled"] ."', '".$key ."', '".$value["phoneType"] ."', '".$value["blf"] ."', '". $value["ports"]."', '".$value["description"] ."')";
        if($i != $len - 1){
            $query .= ", ";
        }
        $i++;
    }
    $db->exec($query);
}

if(count($successData1) > 0){
    foreach ($successData1 as $successKey => $successValue){
        if($successValue == "success"){
            echo "<p style='text-align:center;'>".$successKey." has been updated successfully.</p>";
        }else{
            echo "<p style='text-align:center;color:red'>".$successKey." updation failed.</p>";
        }
    }
}

if(count($successData2) > 0){
    foreach ($successData2 as $successKey => $successValue){
        if($successValue == "success"){
            echo "<p style='text-align:center;'>".$successKey." has been updated successfully.</p>";
        }else{
            echo "<p style='text-align:center;color:red'>".$successKey." updation failed.</p>";
        }
    }
}
if(count($successData3) > 0){
    foreach ($successData3 as $successKey => $successValue){
        if($successValue == "success"){
            echo "<p style='text-align:center;'>".$successKey." has been updated successfully.</p>";
        }else{
            echo "<p style='text-align:center;color:red'>".$successKey." updation failed.</p>";
        }
    }
}



/*changeLog */
 
$changeLogEnterPriseArray=array();
foreach($entArray as $key => $value){
    if($key =="remove"){
        foreach ($value as $key1 => $value1){
           if(isset($value1["enabled"])){
               if($value1["enabled"] == "true"){
                   $changeLogEnterPriseArray[$key1]["enabled"]["oldValue"]="false";
                   $changeLogEnterPriseArray[$key1]["enabled"]["newValue"]="true";
               }else{
                   $changeLogEnterPriseArray[$key1]["enabled"]["oldValue"]="true";
                   $changeLogEnterPriseArray[$key1]["enabled"]["newValue"]="false";
               }
           }
           
           if(isset($value1["blf"])){
               if($value1["blf"] == "Yes"){
                   $changeLogEnterPriseArray[$key1]["enabled"]["oldValue"]="No";
                   $changeLogEnterPriseArray[$key1]["enabled"]["newValue"]="Yes";
               }else{
                   $changeLogEnterPriseArray[$key1]["enabled"]["oldValue"]="Yes";
                   $changeLogEnterPriseArray[$key1]["enabled"]["newValue"]="No";
               }
           }
            
        }  
                
                
    }
    if($key =="modify"){
        foreach ($value as $key1 => $value1){
            if(isset($value1["blf"])){
                if($value1["blf"] == "Yes"){
                    $changeLogEnterPriseArray[$key1]["blf"]["oldValue"]="No";
                    $changeLogEnterPriseArray[$key1]["blf"]["newValue"]="Yes";
                }else{
                   $changeLogEnterPriseArray[$key1]["blf"]["oldValue"]="Yes";
                    $changeLogEnterPriseArray[$key1]["blf"]["newValue"]="No"; 
                }
                
            }
            if(isset($value1["enabled"])){
                if($value1["enabled"] == "true"){
                    $changeLogEnterPriseArray[$key1]["enabled"]["oldValue"]="false";
                    $changeLogEnterPriseArray[$key1]["enabled"]["newValue"]="true";
                }else{
                   $changeLogEnterPriseArray[$key1]["enabled"]["oldValue"]="true";
                    $changeLogEnterPriseArray[$key1]["enabled"]["newValue"]="false"; 
                }
            }
        }  
                
    }
    
    if($key =="add"){
        foreach ($value as $key1 => $value1){
            if(isset($value1["blf"])){
                if($value1["blf"] == "No"){
                    $changeLogEnterPriseArray[$key1]["blf"]["oldValue"]="Yes";
                    $changeLogEnterPriseArray[$key1]["blf"]["newValue"]="No";
                }
            }
            if(isset($value1["enabled"])){
                if($value1["enabled"] != "true"){
                    $changeLogEnterPriseArray[$key1]["enabled"]["oldValue"]="true";
                    $changeLogEnterPriseArray[$key1]["enabled"]["newValue"]="false";
                }
                
            }
            
        }  
    }
}
foreach($changeLogEnterPriseArray as $deviceTypeKey => $valueChange){
    $cLUObj = new ChangeLogUtility($deviceTypeKey, $groundId, $_SESSION["loggedInUserName"]);
    foreach($valueChange as $changeField => $finalVal){
        $module ="Device Type Management Modify";
        $cLUObj->createChangesArray($changeField , $finalVal['oldValue'], $finalVal['newValue']); 
    }
    $cLUObj->changeLogModifyUtility($module, $deviceTypeKey , $tableName = "deviceTypeManagementChanges");
}

?>