<?php
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/devices/deviceTypeManagement.php");
$devobj = new DeviceTypeManagement();
$systemTableData = $devobj->getSystemTableDevices();
$vdmArray = $devobj->getVDMTemplate();

$error = 0;
$nonRequired = array();
$postArray = array();
if(count($_POST["systemData"]) > 0){
    foreach ($_POST["systemData"] as $key => $value){
        if($value["enabled"] == "true"){
            $postArray[$value["deviceType"]]["enabled"] = $value["enabled"];
        }else{
            $postArray[$value["deviceType"]]["enabled"] = "false";
        }
        
        $postArray[$value["deviceType"]]["phoneType"] = $value["phoneType"];
        if(isset($license["softPhone"]) && $license["softPhone"] == "true"){
            $postArray[$value["deviceType"]]["softPhoneType"] = $value["softPhoneType"];
        }
        
        if(isset($value["blf"]) && !empty($value["blf"])){
            $postArray[$value["deviceType"]]["blf"] = $value["blf"];
        }else{
            $postArray[$value["deviceType"]]["blf"] = "No";
        }
        if(isset($license["customTags"]) && $license["customTags"] == "true"){
            $postArray[$value["deviceType"]]["customTagSpec"] = $value["customTagSpec"];
        }
        if(isset($license["vdmLite"]) && $license["vdmLite"] == "true"){
            $postArray[$value["deviceType"]]["vdmTemplate"] = $vdmArray[$value["vdmTemplate"]];
        }
        
        
        $postArray[$value["deviceType"]]["ports"] = $value["ports"];
        $postArray[$value["deviceType"]]["sasLines"] = $value["sasLines"];
        $postArray[$value["deviceType"]]["fxoPorts"] = $value["fxoPorts"];
        if(isset($value["scaOnly"]) && !empty($value["scaOnly"])){
            $postArray[$value["deviceType"]]["scaOnly"] = $value["scaOnly"];
        }else{
            $postArray[$value["deviceType"]]["scaOnly"] = "No";
        }
        if(isset($value["sasTest"]) && !empty($value["sasTest"])){
            $postArray[$value["deviceType"]]["sasTest"] = $value["sasTest"];
        }else{
            $postArray[$value["deviceType"]]["sasTest"] = "No";
        }
        
        $postArray[$value["deviceType"]]["nameSubstitution"] = $value["nameSubstitution"];
        $postArray[$value["deviceType"]]["description"] = $value["description"];
    }
}


//$diffArray = check_diff_multi($systemTableData, $postArray);
//echo "db table data"; print_r($systemTableData);
//echo "post data"; print_r($postArray);exit;
$changedArray = array();
if(count($postArray) > 0){
    foreach ($postArray as $changeKey => $changeVal){
        if($changeVal["enabled"] != $systemTableData[$changeKey]["enabled"]){
            $changedArray[$changeKey] = $changeVal;
        }else if($changeVal["enabled"]=="false" && $systemTableData[$changeKey]["enabled"]=="false"){
            continue;
        }else{
            if($changeVal["phoneType"] != $systemTableData[$changeKey]["phoneType"]){
                $changedArray[$changeKey]["phoneType"] = $changeVal["phoneType"];
            }
            if(isset($license["softPhone"]) && $license["softPhone"] == "true"){
                if($changeVal["softPhoneType"] != $systemTableData[$changeKey]["softPhoneType"]){
                    $changedArray[$changeKey]["softPhoneType"] = $changeVal["softPhoneType"];
                }
            }
            
            if($changeVal["blf"] != $systemTableData[$changeKey]["blf"]){
                $changedArray[$changeKey]["blf"] = $changeVal["blf"];
            }
            if(isset($license["customTags"]) && $license["customTags"] == "true"){
                if($changeVal["customTagSpec"] != $systemTableData[$changeKey]["customTagSpec"]){
                    $changedArray[$changeKey]["customTagSpec"] = $changeVal["customTagSpec"];
                }
            }
            if(isset($license["vdmLite"]) && $license["vdmLite"] == "true"){
                $systemvdm = $vdmArray[$systemTableData[$changeKey]["vdmTemplate"]];
                if($changeVal["vdmTemplate"] != $systemvdm){
                    $changedArray[$changeKey]["vdmTemplate"] = $changeVal["vdmTemplate"];
                }
            }
            
            if($changeVal["ports"] != $systemTableData[$changeKey]["ports"]){
                $changedArray[$changeKey]["ports"] = $changeVal["ports"];
            }
            if($changeVal["sasLines"] != $systemTableData[$changeKey]["sasLines"]){
                $changedArray[$changeKey]["sasLines"] = $changeVal["sasLines"];
            }
            if($changeVal["fxoPorts"] != $systemTableData[$changeKey]["fxoPorts"]){
                $changedArray[$changeKey]["fxoPorts"] = $changeVal["fxoPorts"];
            }
            if($changeVal["scaOnly"] != $systemTableData[$changeKey]["scaOnly"]){
                $changedArray[$changeKey]["scaOnly"] = $changeVal["scaOnly"];
            }
            if($changeVal["sasTest"] != $systemTableData[$changeKey]["sasTest"]){
                $changedArray[$changeKey]["sasTest"] = $changeVal["sasTest"];
            }
            if($changeVal["nameSubstitution"] != $systemTableData[$changeKey]["nameSubstitution"]){
                $changedArray[$changeKey]["nameSubstitution"] = $changeVal["nameSubstitution"];
            }
            if($changeVal["description"] != $systemTableData[$changeKey]["description"]){
                $changedArray[$changeKey]["description"] = $changeVal["description"];
            }
        }
    }
}

if(count($systemTableData) > 0){
    foreach ($systemTableData as $sysKey => $sysVal){
        if($sysVal["enabled"] == "true"){
            if(!array_key_exists($sysKey, $postArray)){
                $changedArray[$sysKey]["enabled"] = "false";
            }
        }
    }
}
//print_r($changedArray);exit;
if(count($changedArray) > 0){
    $changedArray = updatePortNumber($changedArray, $ociVersion);
    $data = $errorTableHeader;
    foreach ($changedArray as $key => $value)
    {
        
        /*if ($value !== ""){
         $bg = CHANGED;
         }else{
         $bg = UNCHANGED;
         }*/
        $bg = CHANGED;
        
       
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:#72ac5d;vertical-align:middle;\">" . $key . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    if($k == "phoneType" && $v=="" && $value["enabled"]=="true"){
                            $error = 1;
                            $bg = INVALID;
                            $v = "Phone type can not be empty if device is enabled.";
                        
                    }
                    
                    if($k == "phoneType" && $v=="Soft Phone"){
                        if($value["softPhoneType"] == ""){
                            $error = 1;
                            $bg = INVALID;
                            $v = "Soft phone type can not be empty if phone type is selected as 'Soft Phone'.";
                        }
                        
                    }/* else if( !isset($changedArray[$key]['phoneType']) && $value["softPhoneType"] == ""){
                            $error = 1;
                            $bg = INVALID;
                            $v = "Soft phone type can not be empty if phone type is selected as 'Soft Phone'.";
					}*/
                    
                    if($k == "phoneType" && $v !="Soft Phone"){
                        if($value["softPhoneType"] != ""){
                            $error = 1;
                            $bg = INVALID;
                            $v = "Soft phone type can be set if phone type is selected as 'Soft Phone'.";
                        } else if( !isset($changedArray[$key]['softPhoneType']) && 
									($systemTableData[$key]["softPhoneType"] != "" && $value["phoneType"] != "Soft Phone") 
						) {
							$error = 1;
                            $bg = INVALID;
                            $v = "Soft phone type can be set if phone type is selected as 'Soft Phone'.";
						}
                        
                    } 
                    /* Added by Pankaj*/
                    if($k == "softPhoneType" && $v != ""){
                        if($systemTableData[$key]["phoneType"] != "Soft Phone" && $value["phoneType"] != "Soft Phone" ){
                            $error = 1;
                            $bg = INVALID;
                            $v = "Soft phone type can be set if phone type is selected as 'Soft Phone'.";
                        }
                        
                    }
                    
                    if($k == "fxoPorts" && $v !=""){
                        if(isset($value["phoneType"])){
                            $phoneTypeCal1 = $value["phoneType"];
                        }else{
                            $phoneTypeCal1 = $postArray[$key]["phoneType"];
                        }
//                         if(isset($value["phoneType"])){
                            $portCal1 = $postArray[$key]["ports"];
//                         }
                        
                        if($phoneTypeCal1 !== "Analog" || ($v > $portCal1)){
                            $error = 1;
                            $bg = INVALID;
                            $v = "FXO Ports can be set only for anolog devices and value of fxo ports can not be greater than ports.";
                        }
                    }
                    
                    if($k == "fxoPorts" && $v ==""){
                        if(isset($value["phoneType"])){
                            $phoneTypeCal1 = $value["phoneType"];
                        }else{
                            $phoneTypeCal1 = $postArray[$key]["phoneType"];
                        }
                        if(isset($value["phoneType"])){
                            $portCal1 = $value["ports"];
                        }else{
                            $portCal1 = $postArray[$key]["ports"];
                        }
                        if($phoneTypeCal1 == "Analog"){
                            $error = 1;
                            $bg = INVALID;
                            $v = "FXO Ports can not be empty for anolog devices.";
                        }
                    }
                    
                    if($k == "sasLines" && $v !=""){
                        if(isset($value["fxoPorts"])){
                            $fxoPortCal = $value["fxoPorts"];
                        }else{
                            $fxoPortCal = $postArray[$key]["fxoPorts"];
                        }
                        if(isset($value["phoneType"])){
                            $phoneTypeCal = $value["phoneType"];
                        }else{
                            $phoneTypeCal = $postArray[$key]["phoneType"];
                        }
                        
                        if($phoneTypeCal != "Analog" || ($v > $fxoPortCal)){
                            $error = 1;
                            $bg = INVALID;
                            $v = "SAS Lines can be set for only Anolog devices and value of sas lines can not be greater that fxo ports.";
                        }
                    }
                    
                    if($k == "sasLines" && $v ==""){
                        if(isset($value["fxoPorts"])){
                            $fxoPortCal = $value["fxoPorts"];
                        }else{
                            $fxoPortCal = $postArray[$key]["fxoPorts"];
                        }
                        if(isset($value["phoneType"])){
                            $phoneTypeCal = $value["phoneType"];
                        }else{
                            $phoneTypeCal = $postArray[$key]["phoneType"];
                        }
                        
                        if($phoneTypeCal == "Analog"){
                            $error = 1;
                            $bg = INVALID;
                            $v = "SAS Lines can not be empty for Anolog devices.";
                        }
                    }
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}else{
    $error = 1;
    $bg = INVALID;
    $data .= "You have made no changes.";
}

$data .= "</table>";
echo $error . $data;

function updatePortNumber($changedArray, $ociVersion) {
    $devobj = new DeviceTypeManagement();
    
    foreach($changedArray as $deviceKey => $deviceValue) {
        if($deviceValue['enabled'] == "true") {
            $deviceDetailsArray = $devobj->getDeviceTypesDetails(array($deviceKey), $ociVersion);
            $changedArray[$deviceKey]["ports"] = $deviceDetailsArray["Success"][$deviceKey]["numberOfPorts"];
        }
    }
    
    return $changedArray;
}
?>