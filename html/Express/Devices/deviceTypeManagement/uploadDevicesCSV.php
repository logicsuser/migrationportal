<?php 
require_once("/var/www/html/Express/config.php");
checkLogin();
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/devices/deviceTypeManagement.php");

$devobj = new DeviceTypeManagement();
$systemTableData = $devobj->getSystemTableDevices();
$vdmArray = $devobj->getVDMTemplate();

$columConstants = array(
    "enabled",
    "deviceType",
    "phoneType",
    "softPhoneType",
    "blf",
    "customTagSpec",
    "vdmTemplate",
    "ports",
    "fxoPorts",
    "sasLines",
    "scaOnly",
    "sasTest",
    "nameSubstitution",
    "description"
);


$i = 0;
foreach($columConstants as $value) {
    if($value == "vdmTemplate" && (!isset($license["vdmLite"]) || $license["vdmLite"] != "true") ) {
        continue;
    }
    if($value == "customTagSpec" && ( !isset($license["customTags"]) || $license["customTags"] != "true") ) {
        continue;
    }
    if($value == "softPhoneType" && ( !isset($license["softPhone"]) || $license["softPhone"] != "true") ) {
        continue;
    }
    define($value, $i);
    $i++;
}

$deviceFields = array();
$deviceFieldsAll = array(
    array("enabled", "Enabled"),
    array("deviceType", "Device Type"),
    array("phoneType", "Phone Type"),
    array("softPhoneType", "Soft Phone Type"),
    array("blf", "BLF"),
    array("customTagSpec", "Custom Tag Spec"),
    array("vdmTemplate", "VDM"),
    array("ports", "Ports"),
    array("fxoPorts", "FXO Ports"),
    array("sasLines", "SAS Lines"),
    array("scaOnly", "SCA Only"),
    array("sasTest", "SAS Test"),
    array("nameSubstitution", "Name Substitution"),
    array("description", "Description")
);
    $i = 0;
    foreach($deviceFieldsAll as $key => $value) {
        if($value[0] == "vdmTemplate" && (!isset($license["vdmLite"]) || $license["vdmLite"] != "true") ) {
            continue;
        }
        if($value[0] == "customTagSpec" && ( !isset($license["customTags"]) || $license["customTags"] != "true") ) {
            continue;
        }
        if($value[0] == "softPhoneType" && ( !isset($license["softPhone"]) || $license["softPhone"] != "true") ) {
            continue;
        }
        $deviceFields[$i] = $value;
        $i++;
    }
    
    $allowedEnabled = $sasTest = array("Yes", "No");
    $allowedPhoneType = array("SIP Phone", "Analog", "Soft Phone");
    $allowedSoftPhoneType = array("Generic", "Counterpath");
    $allowedSoftPhoneTypeVal = array("Generic" => "generic", "Counterpath" => "counterpath");
    $allowedBlf = array("Yes", "No");
    $allowedVdmTemplate = $vdmArray;
    $allowedScaOnly = array("Yes", "No");
    $allowedSasTest = array("Yes", "No");
    
    
/* Start Update device */    
    if( isset($_POST['action']) && $_POST['action'] == "updateDeviceDetails" ) {
        $updateDeviceTypesChanges = updateDeviceTypesChanges($_SESSION["modifiedDevices"], $deviceFields, $allowedSoftPhoneTypeVal);
        unset($_SESSION["modifiedDevices"]);
        $updateSuccess = false;
        foreach ($updateDeviceTypesChanges as $changeKey => $changeVal) {
            if($changeVal == "failed") {
                echo "<div class='alert alert-danger'><strong>Failed! </strong>".$changeKey." updation failed.</div>";
            } else if($changeVal == "success") {
                $updateSuccess = true;
//                 echo "<p style='text-align:center;'>".$changeKey." has been updated successfully.</p>";
            }
        }
        
        if($updateSuccess) {
            echo  "<div class='alert alert-success'><strong>Success! </strong> Device Types have been updated successfully.</div>";
        }
        
        die;
    }
 /* End Update device */

    /* Process Upload Device. */
    $is_file_upload = false;
    $exp = array();

    if (file_exists($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
        
        $is_file_upload = true;
        $tmp_file_name = tempnam(sys_get_temp_dir(), 'BULK_DEVICE_TYPES');
        $file_type = $_FILES['file']['type'];
        $path = $_FILES['file']['name'];
        $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
        
        if( ($file_type != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" && $ext != "xlsx" && $file_type != "text/csv" && $ext != "csv") ) {
            echo "<div class='alert alert-danger'><strong>Error! </strong> File type is not supported. Upload only CSV/XLSX file.</div>";
            die;
        }
        
        move_uploaded_file(
            $_FILES['file']['tmp_name'], $tmp_file_name
            );
        
        if (file_exists($tmp_file_name) && ($file_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $ext == "xlsx")) {
            
            try {
                /** PHPExcel */
                require_once("/var/www/phpToExcel/Classes/PHPExcel.php");
                require_once("/var/www/phpToExcel/Classes/PHPExcel/IOFactory.php");
                
                $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                //Load the file
                $objPHPExcel = $objReader->load($tmp_file_name);
                //Get number of Sheets
                $sheetCount = $objPHPExcel->getSheetCount();
                //Set the Last Sheet as Active
                $objPHPExcel->setActiveSheetIndex($sheetCount - 1);
                //Read the data into an array
                $sheet = $objPHPExcel->getActiveSheet();
                $maxCell = $sheet->getHighestRowAndColumn();
                //$max_col = PHPExcel_Cell::stringFromColumnIndex(count($fields) - 1);
                $max_col = $maxCell['column'];
                
                for ($row = 1; $row <= $maxCell['row']; $row++) {
                    $next_row = $row + 1;
                    $sheet_row = $sheet->rangeToArray("A$row:$max_col$next_row", null, true, false);
                    if ($sheet_row && count($sheet_row) > 0) {
                        if(!array_filter($sheet_row[0])) {
                            break;
                        }
                        $exp[] = $sheet_row[0];
                    }
                }
                
                $fie_headers = $exp[0];
                if (count($exp) > 0) {
                    array_shift($exp);
                }
                $file_rows = $exp;
                
            } catch (Exception $e) {
                error_log($e->getMessage());
            }
            //Remove the Header
            if (count($exp) > 0) {
                array_shift($exp);
            }
            
        } else if (file_exists($tmp_file_name) && ($file_type == "text/csv" || $ext == "csv")) {
            
            try {
                /** PHPExcel */
                require_once("/var/www/phpToExcel/Classes/PHPExcel.php");
                require_once("/var/www/phpToExcel/Classes/PHPExcel/IOFactory.php");
                
                $objReader = new PHPExcel_Reader_CSV();
                $objPHPExcel = $objReader->load($tmp_file_name);
                $objReader->setSheetIndex(0);
                
                //Read the data into an array
                $exp = $objPHPExcel->getActiveSheet()->toArray(null, false, true, false);
                //Remove the Header
                $fie_headers = $exp[0];
                if (count($exp) > 0) {
                    array_shift($exp);
                }
                $file_rows = $exp;
                
            } catch (Exception $e) {
                error_log($e->getMessage());
            }
        }
        
    }
    
    $filteredHeader = filterHeaderUsingLicense($fie_headers, $license);
    $fie_headers = $filteredHeader["newHeaders"];
    $ignoreKeys = $filteredHeader["ignoreKeys"];
    $modifiedDevices = getModifiedDevices($file_rows, $systemTableData, $vdmArray, $allowedSoftPhoneTypeVal, $ignoreKeys);
    $header_col_num = 1;
    $header_error = '';
    foreach ($deviceFields as $key => $field) {
        $column_header = $fie_headers[$key];
        if($field[1] != $column_header) {
            $header_error .= "Column $header_col_num should be \"{$field[1]}\", but your sheet has \"" . $column_header . "\"<br/>";
        }
        $header_col_num++;
    }
    
    if($header_error) {
        ?>
		<div class="alert alert-danger">
			<h2>Column Name Mismatch</h2>
			<?php echo $header_error ?>
		</div>
		<?php
		exit;
	}
	?>
	
	<table align="center" class="table tablesorter table-bordered table-striped dataTable" style="width:96%;" id="devicesTableOutput">
	<thead>
		<?php
		foreach ($deviceFields as $key => $headers) {
			echo "<th class=\"thsmallExSheet\" style=\"width:5%;\">" . $headers[1] . "</th>";
		}
		?>
	</thead>
	
	<tbody>
	<?php 	
    $error = "0";
    $finalDeviceTypeData = array();
    foreach ($modifiedDevices as $rowKey => $file_row) {
            $file_row = $file_row['detail'];
            echo "<tr>";
            foreach ($file_row as $elKey => $elVal) {
                $class = "good";
                $errorMsg = "";
                
                if( !array_key_exists($elKey, $deviceFields) ) {
                    unset($file_row[$elKey]);
                    continue;
                }
                
                if ($elKey == enabled)
                {
                    if( $elVal == "false") {
                        $elVal = "No";
                    } else if( $elVal == "true" ) {
                        $elVal = "Yes";
                    }
                    if ( !in_array($elVal, $allowedEnabled) ) {
                        $class = "bad";
                        $error = "1";
                        $errorMsg = $elVal . " is not a valid " . $deviceFields[$elKey][1] . " value ,";
                        $errorMsg .= "Expected " . implode(" or ", $allowedEnabled). ".";
                    }
                    
                }
                
                if ($elKey == deviceType)
                {
                    if( ! isset($systemTableData[$file_row[deviceType]]) ) {
                        $class = "bad";
                        $error = "1";
                        $errorMsg = $elVal . " is not a valid " . $deviceFields[$elKey][1] . ".";
                    }
                    
                }
                if ($elKey == ports)
                {
                    if ( $elVal <> $systemTableData[$rowKey]['ports']) {
                        $class = "bad";
                        $error = "1";
                        $errorMsg = "You can not change the port, ";
                        $errorMsg .= "Expected " . $systemTableData[$rowKey]['ports'] . ".";
                    }
                    
                }
                
                if($elKey == fxoPorts) {
                    if($elVal != ""){
                        if(isset($file_row[phoneType])){
                            $phoneTypeCal1 = $file_row[phoneType];
                        }
                        if(isset($file_row[phoneType])){
                            $portCal1 = $file_row[ports];
                        }
                        if($phoneTypeCal1 != "Analog" || ($elVal > $portCal1)){
                            $class = "bad";
                            $error = "1";
                            $errorMsg = "FXO Ports can be set only for anolog devices and value of fxo ports can not be greater than ports.";
                        }
                    }
                    
                    if($elVal ==""){
                        if(isset($file_row[phoneType])){
                            $phoneTypeCal1 = $file_row[phoneType];
                        }
                        if(isset($file_row[phoneType])){
                            $portCal1 = $file_row[ports];
                        }
                        if($phoneTypeCal1 == "Analog"){
                            $class = "bad";
                            $error = "1";
                            $errorMsg = "FXO Ports can not be empty for anolog devices.";
                        }
                    }
                }
                
                if($elKey == sasLines) {
                    if($elVal !=""){
                        if(isset($file_row[fxoPorts])){
                            $fxoPortCal = $file_row[fxoPorts];
                        }
                        if(isset($file_row[phoneType])){
                            $phoneTypeCal = $file_row[phoneType];
                        }
                        
                        if($phoneTypeCal != "Analog" || ($elVal > $fxoPortCal)){
                            $class = "bad";
                            $error = "1";
                            $errorMsg = "SAS Lines can be set for only Anolog devices and value of sas lines can not be greater that fxo ports.";
                        }
                    }
                    
                    if($elVal == ""){
                        if(isset($file_row[fxoPorts])){
                            $fxoPortCal = $file_row[fxoPorts];
                        }
                        if(isset($file_row[phoneType])){
                            $phoneTypeCal = $file_row[phoneType];
                        }
                        
                        if($phoneTypeCal == "Analog"){
                            $class = "bad";
                            $error = "1";
                            $errorMsg = "SAS Lines can not be empty for Anolog devices.";
                        }
                    }
                }
                
                if ($elKey == phoneType)
                {
                    if ( $elVal != "" && !in_array($elVal, $allowedPhoneType) ) {
                        $class = "bad";
                        $error = "1";
                        $errorMsg = $elVal . " is not a valid " . $deviceFields[$elKey][1] . " ,";
                        $errorMsg .= "Expected " . implode(" or ", $allowedPhoneType). ".";
                    }
                    else {
                        if($elVal == "" && $file_row[enabled] == "true"){
                            $class = "bad";
                            $error = "1";
                            $errorMsg = "Phone type can not be empty if device is enabled.";
                        }
                        if($elVal == "Soft Phone"){
                            if( $file_row[softPhoneType] == ""){
                                $class = "bad";
                                $error = "1";
                                $errorMsg = "Soft phone type can not be empty if phone type is selected as 'Soft Phone'.";
                            }
                        }
                        
                        if($elVal !="Soft Phone"){
                            if($file_row[softPhoneType] != ""){
                                $class = "bad";
                                $error = "1";
                                $errorMsg = "Soft phone type can be set if phone type is selected as 'Soft Phone'.";
                            }
                        }
                    }
                }
                
                if (defined('softPhoneType') && $elKey == softPhoneType)
                {
                    if ( $elVal != "" && !in_array($elVal, $allowedSoftPhoneType) ) {
                        $class = "bad";
                        $error = "1";
                        $errorMsg = $elVal . " is not a valid " . $deviceFields[$elKey][1] . " ,";
                        $errorMsg .= "Expected " . implode(" or ", $allowedSoftPhoneType). ".";
                    }
                    
                }
                if ($elKey == blf)
                {
                    if ( !in_array($elVal, $allowedBlf) ) {
                        $class = "bad";
                        $error = "1";
                        $errorMsg = $elVal . " is not a valid " . $deviceFields[$elKey][1] . " ,";
                        $errorMsg .= "Expected " . implode(" or ", $allowedBlf). ".";
                    }
                    
                }
                if (defined('vdmTemplate') && $elKey == vdmTemplate)
                {
                    if ( $elVal != "" && !in_array($elVal, $allowedVdmTemplate) ) {
                        $class = "bad";
                        $error = "1";
                        $errorMsg = $elVal . " is not a valid " . $deviceFields[$elKey][1] . " ,";
                        $errorMsg .= "Expected " . implode(" or ", $allowedVdmTemplate). ".";
                    }
                    
                }
                if ($elKey == scaOnly)
                {
                    if ( !in_array($elVal, $allowedScaOnly) ) {
                        $class = "bad";
                        $error = "1";
                        $errorMsg = $elVal . " is not a valid " . $deviceFields[$elKey][1] . " ,";
                        $errorMsg .= "Expected " . implode(" or ", $allowedScaOnly). ".";
                    }
                    
                }
                if ($elKey == sasTest)
                {
                    if ( !in_array($elVal, $allowedSasTest) ) {
                        $class = "bad";
                        $error = "1";
                        $errorMsg = $elVal . " is not a valid " . $deviceFields[$elKey][1] . " ,";
                        $errorMsg .= "Expected " . implode(" or ", $allowedSasTest). ".";
                    }
                    
                }
                
                echo "<td class=\"" . $class . "\" title=\"" . $errorMsg . "\">";
                echo "<input class=\"noBorder\" title=\"" . $errorMsg . "\" type=\"text\" value=\"" . $elVal . "\" style=\"width:98%;\" readonly=\"readonly\">";
                echo "</td>";
            }
            echo "</tr>";
            
            /* Make Final Array to Update */
            if(defined('vdmTemplate')) {
                $file_row[vdmTemplate] = array_search($file_row[vdmTemplate], $vdmArray);
            }
            unset($file_row[ports]);
            unset($file_row[deviceType]);
            $finalDeviceTypeData[$rowKey] = $file_row;
	}
	
	if (isset($error) and $error == "1") {
	    echo "<tr><td colspan=\"100%\" align=\"center\">";
	    echo "<label class=\"labelText\">Please correct errors and resubmit.</label>";
	    echo "</td></tr>";
	} 
	else if( count($modifiedDevices) <= 0) {
	    echo "<tr><td colspan=\"100%\" align=\"center\">";
	    echo "<label class=\"labelText\">No Device Type Data Found for update.</label>";
	    echo "</td></tr>";
	}
	else { 
	    $_SESSION["modifiedDevices"] = $finalDeviceTypeData;
	}
?>
</tbody>
</table>

<?php 
function getModifiedDevices($file_rows, $systemTableData, $vdmArray, $allowedSoftPhoneTypeVal, $ignoreKeys) {
    $changedArray = array();
    foreach($file_rows as $rowKey => $file_row) {
        if( count($ignoreKeys) > 0 ) {
            $file_row = deleteItemFromRowData($file_row, $ignoreKeys);
        }
        if( isset($systemTableData[$file_row[deviceType]]) ) {
            if( $file_row[enabled] == "No") {
                $file_row[enabled] = "false";
            } else if( $file_row[enabled] == "Yes" ) {
                $file_row[enabled] = "true";
            }
            $oldvdmTemplate = $systemTableData[$file_row[deviceType]]['vdmTemplate'];
            if(
                $systemTableData[$file_row[deviceType]]['enabled'] != $file_row[enabled] ||
                $systemTableData[$file_row[deviceType]]['phoneType'] != $file_row[phoneType] ||
                defined('softPhoneType') && $systemTableData[$file_row[deviceType]]['softPhoneType'] != $allowedSoftPhoneTypeVal[$file_row[softPhoneType]] ||
                $systemTableData[$file_row[deviceType]]['blf'] != $file_row[blf] ||
                defined('customTagSpec') && $systemTableData[$file_row[deviceType]]['customTagSpec'] != $file_row[customTagSpec] ||
                defined('vdmTemplate') && $vdmArray[$oldvdmTemplate] != $file_row[vdmTemplate] ||
                $systemTableData[$file_row[deviceType]]['ports'] != $file_row[ports] ||
                $systemTableData[$file_row[deviceType]]['fxoPorts'] != $file_row[fxoPorts] ||
                $systemTableData[$file_row[deviceType]]['sasLines'] != $file_row[sasLines] ||
                $systemTableData[$file_row[deviceType]]['scaOnly'] != $file_row[scaOnly] ||
                $systemTableData[$file_row[deviceType]]['sasTest'] != $file_row[sasTest] ||
                $systemTableData[$file_row[deviceType]]['nameSubstitution'] != $file_row[nameSubstitution] ||
                $systemTableData[$file_row[deviceType]]['description'] != $file_row[description]
              ) {
                  
                $changedArray[$file_row[deviceType]]["detail"] = $file_row;
            }
            
        } else {
            $changedArray[$file_row[deviceType]]["detail"] = $file_row;
            $changedArray[$file_row[deviceType]]["deviceNotFound"] = "Device Type Not Found";
        }
    }
    return $changedArray;
}

function updateDeviceTypesChanges($changedArray, $deviceFields, $allowedSoftPhoneTypeVal) {
    global $db;
    
    $successData = array();
    //Code added @ 16 July 2019
    $whereCndtn = "";
    if($_SESSION['cluster_support']){
        $selectedCluster = $_SESSION['selectedCluster'];
        $whereCndtn = " AND clusterName ='$selectedCluster' ";
    }
    //End code
    foreach ($changedArray as $key => $value){
        $sql = "UPDATE systemDevices SET ";
        if(!empty($value)){
            foreach ($value as $key1 => $value1){
                if( defined('softPhoneType') && $key1 == softPhoneType) { $value1 = $allowedSoftPhoneTypeVal[$value1]; }
                $sql .= $deviceFields[$key1][0]."='".$value1."'";
                $sql .= ",";
            }
        }
        $sql = rtrim($sql, ",");
        $sql .= " WHERE deviceType='".$key."' $whereCndtn ";
        $stmt= $db->prepare($sql);
        if($stmt->execute()){
            $successData[$key] = "success";
        }else{
            $successData[$key] = "failed";
        }
    }
    
    return $successData;
}

function filterHeaderUsingLicense($fie_headers, $license) {
    $newHeaders = array();
    $ignoreKeys = array();
    $j = 0;
    foreach($fie_headers as $key => $value) {
        if( $value == "VDM" && (!isset($license["vdmLite"]) || $license["vdmLite"] != "true") ) {
            $ignoreKeys[] = $key;
            continue;
        }
        if( $value == "Custom Tag Spec" && (!isset($license["customTags"]) || $license["customTags"] != "true") ) {
            $ignoreKeys[] = $key;
            continue;
        }
        if( $value == "Soft Phone Type" && (!isset($license["softPhone"]) || $license["softPhone"] != "true") ) {
            $ignoreKeys[] = $key;
            continue;
        }
        
        $newHeaders[$j] = $value;
        $j++;
    }
    return array("newHeaders" => $newHeaders, "ignoreKeys" => $ignoreKeys);
}

function deleteItemFromRowData($file_row, $ignoreKeys){
    $k = 0;
    $tempArray = array();
    foreach ($file_row as $rowKey => $rowVal) {
        if( in_array($rowKey, $ignoreKeys) ) {
            continue;
        }
        $tempArray[$k] = $rowVal;
        $k++;
    }
    return $tempArray;
}

?>