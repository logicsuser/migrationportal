<?php
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/GroupLevelOperations.php");

$searchCriteria = $_POST["searchCriteria"];
$searchLevel = $_POST["searchlevel"];

$searchValue = $_POST['searchDeviceVal'];
$mode = isset($_POST['mode']) ? $_POST['mode'] : "";
//$providerId = $_SESSION["sp"];
$providerId = trim($_SESSION["sp"]);
$groupId = isset($_SESSION["groupId"]) ? $_SESSION["groupId"] : "";
$dataLimit = 200;

$dop = new DeviceOperations();

$allDeviceList = array();
$tempArray = array();
global $searchDeviceErrMsg;
$searchDeviceErrMsg = "";

$criteriaArr = array("deviceName", "deviceType", "macAddress", "ipAddress", "scaOnlyDevice");
if(! isset($_SESSION["allDeviceDetail"]))
{
    $deviceList = $dop->searchDevicesSystemLevel($searchValue, DeviceOperations::ALL);
  
    if(empty($deviceList["Error"])){
        if(is_array($deviceList["Success"])){
            $_SESSION["allDeviceDetail"] = $deviceList["Success"];
        }
    }else{
        $searchDeviceErrMsg = $deviceList["Error"];
    }
    
}

 
 if ($searchLevel == "group") {
     if(isset($_SESSION["allDeviceDetail"])){
         
         if($searchValue == ""){
             foreach($_SESSION["allDeviceDetail"] as $deviceKey1 => $deviceVal1)
             {
                 if($deviceVal1["providerId"] == $providerId && $deviceVal1["groupId"] == $groupId){
                     $tempArray = checkScaDevice($searchCriteria, $deviceVal1, $tempArray);
                 }
                 
             }
         }
         else
         {
             if ($searchCriteria == "none") {
                 foreach($_SESSION["allDeviceDetail"] as $deviceKey1 => $deviceVal1)
                 {
                     if($deviceVal1["providerId"] == $providerId && $deviceVal1["groupId"] == $groupId){
                         if(stripos($deviceVal1["All"], $searchValue) !== false){
                             $tempArray[] = $deviceVal1;
                         }
                     }
                     
                 }
             }else if(in_array($searchCriteria, $criteriaArr)){
                 foreach($_SESSION["allDeviceDetail"] as $deviceKey1 => $deviceVal1)
                 {
                     if($deviceVal1["providerId"] == $providerId && $deviceVal1["groupId"] == $groupId){
                         if($searchCriteria == "ipAddress"){
                             if(stripos($deviceVal1["netAddress"], $searchValue) !== false){
                                 $tempArray[] = $deviceVal1;
                             }
                         }
                         else if($searchCriteria == "scaOnlyDevice"){
                             if(stripos($deviceVal1["deviceName"], $searchValue) !== false){
                                 $tempArray = checkScaDevice($searchCriteria, $deviceVal1, $tempArray);
                             }
                         }
                         else if(stripos($deviceVal1[$searchCriteria], $searchValue) !== false){
                             $tempArray[] = $deviceVal1;
                         }
                     }
                 }
             }
         }
         
         
         
     }
 }
 else if ($searchLevel == "enterprise") {
     if(isset($_SESSION["allDeviceDetail"])){
         
         if($searchValue == ""){
             foreach($_SESSION["allDeviceDetail"] as $deviceKey1 => $deviceVal1)
             {
                 if($deviceVal1["providerId"] == $providerId){
                     $tempArray = checkScaDevice($searchCriteria, $deviceVal1, $tempArray);
                 }
             }
         }
         else
         {
             if ($searchCriteria == "none") {
                 foreach($_SESSION["allDeviceDetail"] as $deviceKey1 => $deviceVal1)
                 {
                     if($deviceVal1["providerId"] == $providerId){
                         if(stripos($deviceVal1["All"], $searchValue) !== false){
                             $tempArray[] = $deviceVal1;
                         }
                     }
                 }
             }else if(in_array($searchCriteria, $criteriaArr)){
                 foreach($_SESSION["allDeviceDetail"] as $deviceKey1 => $deviceVal1)
                 {
                     if($deviceVal1["providerId"] == $providerId){
                         if($searchCriteria == "ipAddress"){
                             if(stripos($deviceVal1["netAddress"], $searchValue) !== false){
                                 $tempArray[] = $deviceVal1;
                             }
                         }
                         else if($searchCriteria == "scaOnlyDevice"){
                             if(stripos($deviceVal1["deviceName"], $searchValue) !== false){
                                 $tempArray = checkScaDevice($searchCriteria, $deviceVal1, $tempArray);
                             }
                         }
                         else if(stripos($deviceVal1[$searchCriteria], $searchValue) !== false){
                             $tempArray[] = $deviceVal1;
                         }
                     }
                     
                 }
             }
         }
     }
 }else if ($searchLevel == "system") {
     
     if(isset($_SESSION["allDeviceDetail"]))
     {
         if($searchValue == "")
         {
             foreach($_SESSION["allDeviceDetail"] as $deviceKey1 => $deviceVal1)
             {
                 $tempArray = checkScaDevice($searchCriteria, $deviceVal1, $tempArray);
             }
         }
         else
         {
             if ($searchCriteria == "none") {
                 foreach($_SESSION["allDeviceDetail"] as $deviceKey1 => $deviceVal1)
                 {
                     if(stripos($deviceVal1["All"], $searchValue) !== false){
                         $tempArray[] = $deviceVal1;
                     }
                 }
             }else if(in_array($searchCriteria, $criteriaArr)){
                 foreach($_SESSION["allDeviceDetail"] as $deviceKey1 => $deviceVal1)
                 {
                     if($searchCriteria == "ipAddress"){
                         if(stripos($deviceVal1["netAddress"], $searchValue) !== false){
                             $tempArray[] = $deviceVal1;
                         }
                     }
                     else if($searchCriteria == "scaOnlyDevice"){
                         if(stripos($deviceVal1["deviceName"], $searchValue) !== false){
                             $tempArray = checkScaDevice($searchCriteria, $deviceVal1, $tempArray);
                         }
                     }
                     else if(stripos($deviceVal1[$searchCriteria], $searchValue) !== false){
                         $tempArray[] = $deviceVal1;
                     }
                 }
             }
         }
        
         
     }
 }
 

$deviceDataWithUserInfo = getUserDetailOfDeviceName($tempArray);
processDeviceData($deviceDataWithUserInfo, $dataLimit, $searchLevel, $searchDeviceErrMsg);


function processDeviceData($deviceDataWithUserInfo, $dataLimit, $searchLevel, $searchDeviceErrMsg)
{
    $numberOfDevice = count($deviceDataWithUserInfo);
    if ($numberOfDevice > $dataLimit) {
        echo "There are " . $numberOfDevice . " matching devices - please narrow your search";
    } else if ($searchDeviceErrMsg != "") {
        echo "ErrorMessageFromOci ||" . $searchDeviceErrMsg;
    } else {
        createDeviceTable($deviceDataWithUserInfo, $searchLevel);
    }
}

function getUserDetailOfDeviceName($allDeviceList)
{
    $dop = new DeviceOperations();
    global $searchDeviceErrMsg;
    $userDetail = array();
    
    foreach ($allDeviceList as $key => $devicValue) {
        if ($devicValue["providerId"] != "" && $devicValue["groupId"] != "" && $devicValue["deviceName"] != "") {
            $returnedUserDetail = $dop->getAllUsersforDevice($devicValue["providerId"], $devicValue["groupId"], $devicValue["deviceName"]);
            if (empty($returnedUserDetail["Error"])) {
                if ($returnedUserDetail["Success"] != "") {
                    $allDeviceList[$key]["userDetail"] = $returnedUserDetail["Success"];
                }
            } else {
                $searchDeviceErrMsg = $returnedUserDetail["Error"];
            }
        }
    }
    return $allDeviceList;
}

function checkScaDevice($searchCriteria, $deviceVal1, $tempArray)
{
    if($searchCriteria == "scaOnlyDevice"){
        $exlodedDeviceName = explode(".", $deviceVal1["deviceName"]);
        if(isset($exlodedDeviceName[1]) && $exlodedDeviceName[1] == "sca") {
            $tempArray[] = $deviceVal1;
        }
    }else{
        $tempArray[] = $deviceVal1;
    }
    
    return $tempArray;
    
}


function createDeviceTable($deviceDataWithUserInfo, $searchLevel)
{
    ?>
    <script>
		 // tooltip
    	$(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

<!-- <div style="zoom: 1;" class="viewDetail autoHeight viewDetailNew"> -->
<div style="zoom: 1;" class="viewDetail autoHeight">
<div style="width: 100%;" id="selectCheckDiv">
	<div style="" class="checkAllDevices">
	<input type="checkbox" id="selectAllChk" class="selectAl_Chk" />
	<label for="selectAllChk"><span></span></label>
	<b><label class="labelText">Select All</label></b> 
        <label class="customTooltip" style="margin-left: 5px; cursor: pointer;"data-toggle="tooltip" data-placement="top" title="(you may also select Devices by clicking checkboxes in the table, then selecting
        	Delete actions from Delete button)"><img class="infoIcon" src="images/NewIcon/info_icon.png"> 
        
        
        	
        </label>
	</div>
	</div>
	<table id="deviceTable" class="scroll tablesorter dataTable" style="width: 100%; margin: 0;">
		<thead>
			<tr>
				<th class="thSno"></th>				
			<?php if ($searchLevel != "group") { ?>
                <th class="header thsmall">Enterprise</th>
				<th class="header thsmall">Group</th>
			<?php } ?>

				<th class="header thsmall">Device Name</th>
				<th class="header thsmall">Device Type</th>
				<th class="header thsmall">Mac Address</th>
				<th class="header thsmall">IP Address</th>
				<th class="header thsmall">Phone Number</th>
				<th class="header thsmall">Extension</th>
				<th class="header thsmall">First Name</th>
				<th class="header thsmall">Last Name</th>
			</tr>
		</thead>
		<tbody>
	    
	    
	    <?php
    
    if (is_array($deviceDataWithUserInfo)) 
    {
    	$r = 0;
        foreach ($deviceDataWithUserInfo as $deviceKey => $deviceValue)
        {
            $enterpriseId = strval($deviceValue['providerId']);
            $groupId = strval($deviceValue['groupId']);
            
            $deviceName = strval($deviceValue['deviceName']);
            $deviceType = strval($deviceValue['deviceType']);
            $macAddress = strval($deviceValue['macAddress']);
            $netAddress = strval($deviceValue['netAddress']);
            
            $deviceNameLink = "";
            $phoneNumberLink = "";
            $phoneNumber = "";
            $ext = "";
            $firstName = "";
            $lastName = "";
            $userId = "";
            
            $isScaDeviceName = explode(".", $deviceName);
            if(isset($isScaDeviceName[1]) && $isScaDeviceName[1] == "sca")
            {
                if($searchLevel == "group")
                {
                    $deviceNameLink = "<a href='#' class='scaUserModLink' data-type='".$deviceType."' id='" . $deviceName . "'>" . $deviceName . "</a>";
                    
                }
                else{
                    $deviceNameLink = $deviceName; 
                }
                
               
            } else {
                if (isset($deviceValue["userDetail"]) && is_array($deviceValue["userDetail"]) 
                    && ! empty($deviceValue["userDetail"])) {
                    foreach ($deviceValue["userDetail"] as $userDetail) {
                        
                        $isPrimary = $userDetail['endpointType'];
                        
                        if ($isPrimary == "Primary") {
                            $firstName = $userDetail["firstName"];
                            $lastName = $userDetail["lastName"];
                            $phoneNumber = $userDetail["phoneNumber"];
                            $ext = $userDetail["extn"];
                            $userId = $userDetail["userId"];
                            if($searchLevel == "group"){
                                $deviceNameLink = "<a href='#' class='primaryUserModLink' data-phone='".$phoneNumber."' data-extension='".$ext."' id='" . $userId . "' data-lastname='" . $lastName . "' data-firstname='" . $firstName . "' >" . $deviceName . "</a>";
                                $phoneNumberLink = "<a href='#' class='primaryUserModLink' data-phone='".$phoneNumber."' data-extension='".$ext."' id='" . $userId . "' data-lastname='" . $lastName . "' data-firstname='" . $firstName . "' >" . $phoneNumber . "</a>";
                            } else {
                                $deviceNameLink = $deviceName;
                                $phoneNumberLink = $phoneNumber;
                            }
                        }else{
                            $deviceNameLink = $deviceName; 
//                         	$deviceNameLink = "<a href='#' class='scaUserModLink' id='" . $userId . "' data-lastname='" . $lastName . "' data-firstname='" . $firstName . "' >" . $deviceName . "</a>";
//                         	$phoneNumberLink = "<a href='#' class='scaUserModLink' id='" . $userId . "' data-lastname='" . $lastName . "' data-firstname='" . $firstName . "' >" . $phoneNumber . "</a>";
                        	
                        }
                    }
                }else{
                    $firstName ="";
                    $lastName = "";
                    $phoneNumber = "";
                    $ext = "";
                    $userId = "";
                    $deviceNameLink = $deviceName;
                }
            }
            
            echo "<tr>";
            echo "<td class=\"thSno\"><input type=\"checkbox\" class=\"checkDeviceListBox\" data-devicedata='$groupId---$enterpriseId' name=\"".$deviceName."\" id=\"deviceCheckBox".$r."\" value=\"true\"/>
            <label for=\"deviceCheckBox".$r."\"><span></span></label>
            </td>";
            if ($searchLevel != "group") {
                echo "<td class=\"macTable thsmall\">" . $enterpriseId . "&nbsp;</td>";
                echo "<td class=\"macTable thsmall\">" . $groupId . "&nbsp;</td>";
            }
            echo "<td class=\"macTable thsmall\">" . $deviceNameLink . "&nbsp;</td>";
            echo "<td class=\"macTable thsmall\">" . $deviceType . "&nbsp;</td>";
            echo "<td class=\"macTable thsmall\">" . $macAddress . "&nbsp;</td>";
            echo "<td class=\"macTable thsmall\">" . $netAddress . "&nbsp;</td>";
            echo "<td class=\"macTable thsmall\">" . $phoneNumberLink . "&nbsp;</td>";
            echo "<td class=\"macTable thsmall\">" . $ext . "&nbsp;</td>";
            echo "<td class=\"macTable thsmall\">" . $firstName . "&nbsp;</td>";
            echo "<td class=\"macTable thsmall\">" . $lastName . "&nbsp;</td>";
            echo "</tr>";
            $r++;
        }
    }
    
    ?>
	    
	    </tbody>
	</table>
</div>

<?php
}

?>