<?php
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/GroupLevelOperations.php");

// enterprise
if( $_POST['searchlevel'] == "enterprise" ) {
    $_POST['restrictToEnterprise'] = $_SESSION["sp"];
}
    
if( isset($_POST["filterApplied"]) && $_POST["filterApplied"] == "true" ) {
    $appliedFilter = formateAppliedFilterItems($_POST);
    //$xmlinput = createInputTags($appliedFilter);
    
} else {
    $appliedFilter = new stdClass;
    $appliedFilter->restrictToEnterprise = $_POST['restrictToEnterprise'];
    $appliedFilter->selectedDeviceType = $_POST['selectedDeviceType'];
}


$dop = new DeviceOperations();
$allDevicesData = $dop->systemAccessDeviceGetAllRequest_for_QuickFilter($appliedFilter);
if( empty($allDevicesData["Error"]) ) {
    createDeviceTable($allDevicesData["Success"], $_POST['searchlevel']);
} else {
    echo '<div class="row">
            <div id="enterpriseUserSearchResults" class="col-xs-12"><div class="container" style="margin-top: 10px;">
                <div class="alert alert-danger">' . $allDevicesData["Error"] . '</div></div>
            </div>
        </div>';
//     echo json_encode($allDevicesData["Error"]);    
}


function formateAppliedFilterItems($POST) {
    $appliedFilter = new stdClass;
    $appliedFilter->searchlevel = $POST['searchlevel'];
    $appliedFilter->restrictToEnterprise = $POST['restrictToEnterprise'];
    $appliedFilter->selectedDeviceType = $POST['selectedDeviceType'];
    $appliedFilter->responseSizeLimit = $POST['responseSizeLimit'];
    $appliedFilter->allFilters = array();
    foreach ( $POST['filterItem'] as $itemKey => $itemValue) {
        if ( $POST['filterItemValue'][$itemKey] != "") {
            $appliedFilter->allFilters[$itemValue][] = array (
                "filterItemMode" =>$POST['filterItemMode'][$itemKey],
                "filterItemValue" => $POST['filterItemValue'][$itemKey]
            );
        }
    }
    
    return $appliedFilter;
}

function createDeviceTable($devicesData, $searchLevel)
{
    $devicesAddDelLogPerm = false;
    $devicesLogsArr = explode("_", $_SESSION["permissions"]["devices"]);
    $devicesAddDelLogPermission  = $devicesLogsArr[2];
    if($devicesAddDelLogPermission == "yes"){
        $devicesAddDelLogPerm = true;
    }
    ?>
    <script>
		 // tooltip
    	$(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();

            var total_count = <?= count($devicesData); ?>;
            var deviceTable_quickSearch_DT;
            //var downloadCSVDiv = $('#downloadCSV_quickSearchDiv').html();
            var downloadCSVDiv = '<button type="button" class="btn btn-link" style="float:right;margin-right: 0px;" id="downloadCSV_quickSearch"><img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png"><br/><span>Download<br>CSV</span></button>';
            //var deleteDeviceDiv = $('#deleteBtn_quickSearch').html();
             var deleteDeviceDiv = '<div style="float:right;margin-left: 20px;display:none" class="deleteUsers" id="deleteDevice_quickSearch"><img src="images/icons/delete_selected_users_icon.png" data-alt-src="images/icons/delete_selected_users_icon_over.png"><br><span>Delete<br>Selected<br>Devices</span></div>';

             function initCompleteEnterpriseUsers() {
                $('.deviceTable_quickSearch_bottom_bar .retrieving_info').html('Retrieved all ' + total_count + ' devices');
                var DT_top_bar_column_4 = $('.deviceTable_quickSearch_top_bar .column4');
                var DT_top_bar_column_3 = $('.deviceTable_quickSearch_top_bar .column3');
                DT_top_bar_column_4.prepend(downloadCSVDiv);

                <?php if($devicesAddDelLogPerm) { ?>
                	DT_top_bar_column_3.prepend(deleteDeviceDiv);
                <?php } ?>
                $("#deviceTable_quickSearch_length").append($(".checkAllDevices"));
            }
            
            function drawEnterpriseSearchUserTable() {

                //Skip if it is not the same page/action.
                //if(current_Session !== enterpriseSearch_Session) return;

                deviceTable_quickSearch_DT = $('#deviceTable_quickSearch').DataTable({
                    scrollY: 435,
                    destroy: true,
                    scrollX: true,
                    paging: true,
                    pagingType: "dk_express_pagination",
                    lengthMenu: [50, 100, 200],
                    searching: false,
                    autoWidth: false,
                    deferRender: false,
                    order: [],
                    columnDefs: [
                        { "width": "5%", "targets": 0 },
                        { "width": "10%", "targets": 1 },
                        { "width": "10%", "targets": 2 },
                        { "width": "10%", "targets": 3 },
                        { "width": "25%", "targets": 4 },
                        { "width": "15%", "targets": 5 },
                        { "width": "10%", "targets": 6 },
                        { "width": "8%", "targets": 7 },
                        { "width": "7%", "targets": 8 }
                    ],

                    info: true,
                    language: {
                        emptyTable: " ",
                        infoEmpty: "<?php echo $total_count ? "" : "No Devices Found"?>",
                        lengthMenu: "Show<br/> _MENU_ <br/>devices",
                        info: "Showing _START_ to _END_ of _TOTAL_ devices",
                        oPaginate: {
                            sPageJumpText: "Jump to pg ",
                            //sPageJumpGoBtn: 'GO'
                        }
                    },

                    dom: '<"wrapper"<"deviceTable_quickSearch_top_bar dataTables_top_bar"<"column1"><"column2"><"column3"><"column4"l>>'
                        + 't'
                        + '<"deviceTable_quickSearch_bottom_bar dataTables_bottom_info_bar"<"retrieving_info"><"currently_showing_info"i>p>>',

                    "initComplete": initCompleteEnterpriseUsers
                });

               // $("#deviceTable_quickSearch thead").hide();
            }
            
            drawEnterpriseSearchUserTable();

			$("#downloadCSV_quickSearch").click(function() {
            
        			  var titles = [];
        			  var TableData = [];
        			  $('#deviceTable_quickSearch th').each(function() {	//table id here
        			    titles.push($(this).text());
        			  });

        			  if (deviceTable_quickSearch_DT) {
                          var data = deviceTable_quickSearch_DT.rows().data().toArray();
                          $.each(data, function (index, row) {
                              //console.log(row);
                              $.each(row, function (c_index, value) {
                                  //if (c_index !== 0) {
                                  	value = value.replace(/&nbsp;/g, '');
                                      TableData.push(value);
                                  //}
                              });
                          });
                      }
        			  
        			  
        			  var CSVString = prepCSVRow(titles, titles.length, '');
        			  CSVString = prepCSVRow(TableData, titles.length, CSVString);

        			  var blob = new Blob(["\ufeff", CSVString]);
        			  var fileName = "DeviceSearchResult.csv";
        			  if (navigator.msSaveBlob) { // IE 10+
        		        	navigator.msSaveBlob(blob, fileName);
        		      }else{
        		    	  var downloadLink = document.createElement("a");
        				  
        				  var url = URL.createObjectURL(blob);
        				  downloadLink.href = url;
        				  downloadLink.download = fileName;

        				  
        				  document.body.appendChild(downloadLink);
        				  downloadLink.click();
        				  document.body.removeChild(downloadLink);
        		      }
        			 
        			  
        			});

            function prepCSVRow(arr, columnCount, initial) {
  			  var row = '';
  			  var delimeter = ',';
  			  var newLine = '\r\n';

  			  function splitArray(_arr, _count) {
  			    var splitted = [];
  			    var result = [];
  			    _arr.forEach(function(item, idx) {
  			      if ((idx + 1) % _count === 0) {
  			        splitted.push(item);
  			        result.push(splitted);
  			        splitted = [];
  			      } else {
  			        splitted.push(item);
  			      }
  			    });
  			    return result;
  			  }
  			  var plainArr = splitArray(arr, columnCount);
  			 
  			  plainArr.forEach(function(arrItem) {
  			    arrItem.forEach(function(item, idx) {
  			    	if(idx != 0){	
  			    		row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
  			    	}
  			    });
  			    row += newLine;
  			  });
  			  return initial + row;
  			}
			
        });
    </script>

<!-- <div style="zoom: 1;" class="viewDetail autoHeight viewDetailNew"> -->
<div style="zoom: 1;" class="viewDetail autoHeight">
<div style="width: 100%;" id="selectCheckDiv">
	<div style="" class="checkAllDevices">
	<input type="checkbox" id="selectAllChk_quickSearch" class="selectAl_Chk" />
	<label for="selectAllChk_quickSearch"><span></span></label>
	<b><label class="labelText">Select All</label></b> 
        <label class="customTooltip" style="margin-left: 5px; cursor: pointer;"data-toggle="tooltip" data-placement="top" title="(you may also select Devices by clicking checkboxes in the table, then selecting
        	Delete actions from Delete button)"><img class="infoIcon" src="images/NewIcon/info_icon.png"> 
        
        
        	
        </label>
	</div>
	</div>
	<table id="deviceTable_quickSearch" class="table table-bordered stripe row-border order-column customDataTable" style="width: 100%; margin: 0;">
		<thead>
			<tr>
				<th class=""></th>				
			<?php //if ($searchLevel != "group") { ?>
                <th class="">Enterprise</th>
				<th class="">Group</th>
				<th class="">Is Provider</th>
			<?php //} ?>

				<th class="">Device Name</th>
				<th class="">Device Type</th>
				<th class="">Mac Address</th>
				<th class="">IP Address</th>
				<th class="">Status</th>
			</tr>
		</thead>
		<tbody>
	    
	    
	    <?php
    
    if (is_array($devicesData)) 
    {
    	$r = 0;
        foreach ($devicesData as $deviceKey => $deviceValue)
        {
            $enterpriseId = strval($deviceValue['providerId']);
            $groupId = strval($deviceValue['groupId']);
            $isProvider = strval($deviceValue['isProvider']);
            $deviceName = strval($deviceValue['deviceName']);
            $deviceType = strval($deviceValue['deviceType']);
            $macAddress = strval($deviceValue['macAddress']);
            $netAddress = strval($deviceValue['netAddress']);
            $status = strval($deviceValue['status']);
            
            $deviceNameLink = "";
            $phoneNumberLink = "";
            $phoneNumber = "";
            $ext = "";
            $firstName = "";
            $lastName = "";
            $userId = "";
                                    
            echo "<tr>";
            echo "<td style=\"width:5%\" class=\"\"><input type=\"checkbox\" class=\"checkDeviceListBox_quickSearch\" data-devicedata='$groupId---$enterpriseId' name=\"".$deviceName."\" id=\"deviceCheckBox".$r."\" value=\"true\"/>
            <label for=\"deviceCheckBox".$r."\"><span></span></label>
            </td>";
            //if ($searchLevel != "group") {
            echo "<td style=\"width:10%\" class=\"\">" . $enterpriseId . "&nbsp;</td>";
            echo "<td style=\"width:10%\" class=\"\">" . $groupId . "&nbsp;</td>";
            echo "<td style=\"width:10%\" class=\"\">" . $isProvider . "&nbsp;</td>";
            //}
            echo "<td style=\"width:25%\" class=\"\">" . $deviceName . "&nbsp;</td>";
            echo "<td style=\"width:15%\" class=\"\">" . $deviceType . "&nbsp;</td>";
            echo "<td style=\"width:10%\" class=\"\">" . $macAddress . "&nbsp;</td>";
            echo "<td style=\"width:8%\" class=\"\">" . $netAddress . "&nbsp;</td>";
            echo "<td style=\"width:7%\" class=\"\">" . $status . "&nbsp;</td>";
            
            echo "</tr>";
            $r++;
        }
    }
    
    ?>
	    
	    </tbody>
	</table>
</div>

<?php
}

?>