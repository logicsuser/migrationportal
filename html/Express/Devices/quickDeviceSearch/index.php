<?php
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once("/var/www/html/Express/usersEnterprise/GetUsersInSystem.php");
require_once '/var/www/lib/broadsoft/adminPortal/DeviceOperations.php';
$gUISystem = new GetUsersInSystem();

$devOpObj = new DeviceOperations();
$systemDevices = $devOpObj->systemAccessDeviceTypeGetListRequest();
//---------------------------------------------------------------------------
// Build Service Provider Drop down options
$serviceProviders = $gUISystem->getUsersServerProviderList();
function getUsersServerProviderList() {
    global $serviceProviders;
    
    $str = "";
    if (count($serviceProviders) > 0) {
        $str = "<option value=\"\">None</option>";
        foreach ($serviceProviders as $providerId => $spName) {
            $str .= "<option value=\"" . $providerId . "\">" . "$spName" . "</option>";
        }
    }
    return $str;
}

function getSystemDeviceTypeList() {
    global $systemDevices;
    
    $str = "";
	$systemAllDevices = $systemDevices["Success"];
    if (count($systemAllDevices) > 0) {
        $str = "<option value=\"\" selected>All Devices</option>";
        foreach ($systemAllDevices as $deviceKey => $deviceValue) {
            $str .= "<option value=\"" . $deviceValue . "\">" . "$deviceValue" . "</option>";
        }
    }
    return $str;
}

//---------------------------------------------------------------------------
?> 
<style>
    .filterApplyHead {
        float: right;
        color: #BD8383;
        font-weight: bold;
        font-size: 12px;
        vertical-align: middle;
        width: 15%;
        margin-top: -70px;
        text-align: right;
        margin-right: 15px;
    }
    .filterApplyHead img {
        width: 25px;
        padding-right: 5px;
    }
</style>
<style type="text/css">
		div.dataTables_wrapper {
			width: 90%;
			margin: 0 auto;
		}

		table.dataTable tbody th, table.dataTable tbody td {
			text-align: left;
		}

		table.dataTable thead th, table.dataTable thead td {
			text-align: left !important;
		}
 td.dataTables_empty {
			display: none;
		}

		/*end code */
                 /*code comment by sollogics developer */
		/*table.dataTable.no-footer thead tr .sorting_asc {
			background-image: url(/Express/images/blue/desc.gif) !important;
			background-position: 98% 48%;
		}

               
		table.dataTable.no-footer thead tr .sorting_desc {

			background-image: url(/Express/images/blue/asc.gif);
			background-position: 98% 48%;
		}

		table.dataTable thead .sorting {
			background-image: url(/Express/images/blue/bg.gif);
			background-position: 98% 48%;
		} */

                /*end code commented */
		/* #deviceTable_quickSearch_wrapper table thead tr th 
		{
			border-top: 2px solid #d8dada !important;
			border-bottom: 2px solid #d8dada !important;
			border-right: 2px solid #d8dada !important;
			color: #fff !important;
		} */

		#deviceTable_quickSearch_wrapper .dataTables_wrapper.no-footer .dataTables_scrollBody {
			border-bottom: none;
		}

		#deviceTable_quickSearch_wrapper .DTFC_LeftBodyWrapper,
		#deviceTable_quickSearch_wrapper .dataTables_scrollBody {
			/*margin-top: -26px;
			padding-top: 25px;*/
			border-top: 1px solid #fff;
                          height: auto !important;
                        max-height: 435px !important;
			/*min-height: 100px;*/
		}
              
                .dataTables_wrapper .dataTables_paginate .paginate_button {
                    color: #fff !important;
                }
		/* table.customDataTable thead, .dataTables_scrollHeadInner, table.customDataTable thead th {
			background-color: #5d81ac;
			border-top: 2px solid #d8dada !important;
			border-bottom: 2px solid #d8dada !important;
			border-right: 2px solid #d8dada !important;
			color: #fff !important;
		} */
                
                /*code add by sollogics developer */
                #deviceTable_quickSearch_wrapper .customDataTable thead, #deviceTable_quickSearch_wrapper .dataTables_scrollHeadInner, #deviceTable_quickSearch_wrapper .customDataTable thead th {
                    background-color: #5d81ac;
                    border-right: 2px solid #d8dada !important;
                    color: #fff !important;
                   /* padding-left: 18px !important;*/
                }
                /*end code */

		#enterpriseUserSearchResults .userIdVal:hover td {
			background-color: #dcfac9 !important;
			cursor: pointer;
		}

		#enterpriseUserSearchResults table.dataTable tbody td {
			font-size: 11px;
		}

		input[type="button"][disabled], button[disabled] {
			pointer-events: visible;
			cursor: not-allowed;
		}

		/* #deviceTable_quickSearch_wrapper .dataTables_scrollHeadInner table {
			margin-bottom: 0;
		} */

                /*code added by sollogics developer */
                #deviceTable_quickSearch_wrapper .dataTables_scrollHeadInner table {
			margin-bottom: 0;
                        border-collapse: collapse !important;
		}
                /*end code */
		/* 100% Width*/
		#deviceTable_quickSearch_wrapper .dataTables_scrollHeadInner {
			width: 100% !important;
			padding-left: 0 !important;
		}
		#deviceTable_quickSearch_wrapper .dataTables_scrollHeadInner table {
			width: 100% !important;
			/*padding-right: 1.4%;*/
			padding-right: 20px;
		}
		#deviceTable_quickSearch_wrapper .dataTables_scrollHeadInner table tr th:last-child {
			border-right-color: transparent;
		}
		#deviceTable_quickSearch_wrapper .dataTables_scrollHeadInner table tr th {
			border-bottom-color: transparent;
		}
		#deviceTable_quickSearch_wrapper .dataTables_scrollBody {
			width: 100% !important;
		}
		#deviceTable_quickSearch_wrapper .dataTables_scrollBody table {
			width: 100% !important;
		}
		#deviceTable_quickSearch_wrapper .dataTables_scrollBody table tr td {
			height: 25px;
		}

		#deviceTable_quickSearch_length select {
			width: 65px !important;
		}

		.paging_dk_express_pagination .paginate_jump_input {
			width: 65px !important;
		}

		/* Datatables - This should have been in a common css file*/
		.dataTables_bottom_info_bar {
			text-align: center;
		}
		.dataTables_bottom_info_bar .retrieving_info {
			display: inline-block;
			float: left;
			padding-top: 0.755em;
		}
		.retrieving_info {
			color: #6ea0dc !important;
			font-weight: 700;
		}
		.dataTables_bottom_info_bar .currently_showing_info {
			display: inline-block;
		}
		.dataTables_info {
			color: #6ea0dc !important;
			font-weight: 700;
		}
		.dataTables_wrapper .dataTables_paginate {
			float: right;
			text-align: right;
			padding-top: 0.25em;
                            color: #6ea0dc !important;
		}

		 a.paginate_button {
            background-color: #5d81ac !important;
            color: #fff !important;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: white !important;
            border: 1px solid #FFB200 !important;
            background-color: #FFB200 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #FFB200), color-stop(100%, none)) !important;
            background: -webkit-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: -moz-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: -ms-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: -o-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: linear-gradient(to bottom, #FFB200 0%, #FFB200 100%) !important;
        }
         
 
        .dataTables_wrapper .dataTables_paginate .paginate_button:active {
            background: #ffb200 ! important;
            border-bottom: 1px solid #ffb200 !important;  
        }
        

        a.paginate_button.paginate_number.active {
            background: #ffb200 ! important;
            border-bottom: 1px solid #ffb200 !important;  
        }
        
        .dataTables_wrapper .dataTables_paginate .paginate_button{
            color: #fff !important;
        }
        
       a.paginate_button.paginate_number .paginate_button .paginate_number{
            color: #fff !important;
        }
        a.paginate_button.paginate_number .paginate_button .paginate_number:active{
            color: #fff !important;
        }
		/* Datatables - ends */

		#downloadCSV, #downloadCSV:hover {
			text-decoration: none;
			padding: 0;
		}

		/* Scrollbar */
		/*::-webkit-scrollbar {
			-webkit-appearance: scrollbarbutton-up;
			width: 20px;
		}
		::-webkit-scrollbar-thumb {
			background-color: #c0c0c0;
			-webkit-box-shadow: 0 0 1px rgba(255, 255, 255, .5);
		}

		::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px #f1f1f1;
			-webkit-border-radius: 0;
			border-radius: 0;
			background-color: #f1f1f1;
		}*/
		/* Scrollbar - Ends*/
                /*code added by sollogics developer */
                .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
                    border: 0px solid #ddd;
                }
                table#deviceTable_quickSearch_wrapper tbody tr td:first-child {
                    border-left: 2px solid #d8dada;
                }
                
                table.deviceTable_quickSearch_wrapper tbody tr td:first-child,table#deviceTable_quickSearch_wrapper tbody tr td:first-child {
                    border-left: 1px solid #cccccc;
                    text-align: center;
                   /* padding: 2px 10px 0px 8px !important;*/
                }
                
                table.deviceTable_quickSearch tbody tr td, table#deviceTable_quickSearch tbody tr td {
                    /* background-color: #FFF; */
                    /* border-right: 2px solid #d8dada; */
                    border-top: 2px solid #d8dada !important;
                    border-bottom: 2px solid #d8dada !important;
                    background-color: #eeeeee;
                   /*  padding: 8px 22px;*/
                       padding: 2px 20px;
                }
                
                #deviceTable_quickSearch_wrapper .dataTables_scrollBody table tr td:first-child {
                    border-left: 2px solid #d8dada;
                }
                  table.deviceTable_quickSearch{border: 0px solid;}
               
              
                  table.deviceTable_quickSearch thead tr th {
                    height: 30px;
}  

                table.deviceTable_quickSearch thead th, table.dataTable thead td {
                   padding: 0px 17px 18px;
                   
                }
                 
                table.deviceTable_quickSearch thead th, table.dataTable tbody td {
                   padding: 0px 17px 18px;
                }
                .dataTables_wrapper.no-footer .dataTables_scrollBody {
                    border-bottom: 0px solid #eef0f3;
                }
               table.customDataTable thead tr:after{
               display: block !important;
               visibility:visible;
               }
			   
			   #deviceTable_quickSearch_wrapper .dataTables_scrollBody thead {
				   display: none;
			   }
                           
                .paginate_jump_text{
                    color: #6ea0dc !important;
                    font-weight: 700;
                }
                /* end code */
	</style>
<script src="Devices/quickDeviceSearch/quickDeviceSearch.js"></script>

					<div class="" id="deviceSearchEnterpriseBanner">
						<!--<div class="filterMainHeadDiv">Quick Device Search</div> -->
						<div class="filterApplyHead" style="display: none;"><img src="/Express/images/icons/filter_icon_lrg.png"><span>Quick Filters Applied<span></span></span></div>
					</div>
					 

					<form name="enterpriseDeviceSearchForm" id="enterpriseDeviceSearchForm" class="">
						<div class="searchDeviceOptionDiv">
							<div class="search_device">
					
									<?php
									$checkedEnt = "";
									$checkedGroup = isset ( $_SESSION ["groupId"] ) ? "checked" : "";
									if (! isset ( $_SESSION ["groupId"] ) && $_SESSION["superUser"] == "1") {
										$checkedEnt = ! isset ( $_SESSION ["groupId"] ) ? "checked" : "";
										?>
							<div class="col-md-12">
								<!--new code -->
                                                            
                                                            <div class="form-group" style="margin: 0 auto; width: 830px;">
                                                                <label class="col-xs-4 col-sm-3 text-left control-label labelText">Search In:</label>
                                                                <div class="col-xs-8 col-sm-8">
                                                                        <input type="radio" class="searchInRadio searchlevel_quickFilter" name="searchlevel" value="system" id="searchlevelSys_quickFilter">
                                                                        <label for="searchlevelSys_quickFilter"><span></span></label><lable class="labelText">System</lable>
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <input type="radio" class="searchInRadio searchlevel_quickFilter" name="searchlevel" value="enterprise" id="searchlevelEnt_quickFilter" class="searchlevelIn" <?php echo $checkedEnt; ?> style="width: 5%;">&nbsp;&nbsp;&nbsp;
                                                                        <label for="searchlevelEnt_quickFilter"><span></span></label><label class="labelText">Enterprise</label>
                                                                </div>
                                                            </div>
                                                            <!--end code -->
                                                            
                                                            <!--new code restricted ent --> 
                                                            <div class="form-group" id="restrictToEnterpriseDiv" style="display: none; margin: 0 auto; width: 830px;">
                                                                <label class="col-md-3 text-left control-label labelText">Restrict to Enterprise:</label>
                                                                    <div class="col-md-6">
                                                                        <div class="dropdown-wrap">
                                                                            <select class="form-control" name="restrictToEnterprise" id="restrictToEnterprise" style="width: 100% !important;"><?php echo getUsersServerProviderList(); ?></select>
                                                                        </div>
                                                                </div>
                                                            </div>
															 
															 <div class="form-group" id="selectedDeviceTypeDiv" style="margin: 85px auto; width: 830px;">
                                                                <label class="col-md-3 text-left control-label labelText">Selecte Device Type:</label>
                                                                    <div class="col-md-6">
                                                                        <div class="dropdown-wrap">
                                                                            <select class="form-control" name="selectedDeviceType" id="selectedDeviceType" style="width: 100% !important;"><?php echo getSystemDeviceTypeList(); ?></select>
                                                                        </div>
                                                                </div>
                                                            </div>
                                                            
                                                            <!--end new code restricted ent -->
                                                             
                                                            <div class="">
                                                                <div id="enterpriseUsersQuickFiltersBox" style="float: right;">
                                                                    <div class="row" style="margin: 0 auto;">
                                                                            <button type="button"
                                                                            class="filterTitleBtn cancelButton"
                                                                            id="enterpriseDeviceSearchQuickFiltersStatusTitleBtn">
                                                                            <span id="enterpriseUsersQuickFiltersStatusTitleText" style="color: inherit;">Quick Filters</span>
                                                                            <span id="enterpriseDeviceSearchQuickFiltersStatusTitleIcon" class="glyphicon glyphicon-chevron-right fIcon" style="color: inherit;"></span>
                                                                            </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
								 <?php } ?>	
								
							</div>

							 
							
						</div>
						
					</form>
					
	<!-- Quick Filters - Start -->
	<div id="enterpriseDeviceSearchQuickFiltersDiv" class="container" style="clear:both;padding-top:12px;padding-bottom: 20px;margin: 0 auto;display: none; background: none !important;">
		<div class="divCFAforStyle fcorn-registerTwo">
			<div class="row">
				<h2 class="subBannerCustom">Quick Filters</h2>
				<div class="col-xs-12">
					<form id="enterpriseDeviceSearchQuickFiltersForm">
						<div id="enterpriseDeviceSearchQuickFilters">
						</div>
						<br/>
						<div class="row fcorn-registerTwo adminAddBtnDiv">
							<div class="col-md-2" style="width:21% !important">
								<div class="form-group">
									<label class="labelText">Response Size Limit</label>
									<div class="dropdown-wrap">
										<select style="width:100% !important" id="enterpriseDeviceSearch_responseSizeLimit" name="responseSizeLimit">
											<option value="">Unlimited</option>
											<option value="100">100</option>
											<option value="500">500</option>
											<option value="1000">1000</option>
											<option value="5000">5000</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="row text-center">
							<button type="button"
							        id="enterpriseDeviceSearchFiltersFormSubmitBtn"
							        class="subButton"
							        style="margin-bottom:6px; margin-top:18px; padding:9px 15px; border-radius:4px;">Apply Filters
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
					
					<!-- end  -->
 
					 
						<div class="form-group">
							<div class="loading" id="loadingBar_quickSearch" style="margin-top:0%;">
								<img src="/Express/images/ajax-loader.gif">
							</div>
							<div id="deviceListDiv_quickSearch"></div>
							
						</div>
				 
				
				