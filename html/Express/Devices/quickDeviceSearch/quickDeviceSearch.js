
$(function(){
	
	//$(document).find('#searchDevices_quickFilter').trigger('click');
	$("#loadingBar_quickSearch").show();
	searchDevices_quickFilter();
	$(".searchlevel_quickFilter").click(function() {
		$(".dkFBResetFiltersBtn").trigger("click");
		$('#restrictToEnterprise').val("");
		$('#selectedDeviceType').val("");
		var thisValue = $(this).val();
		$("#restrictToEnterpriseDiv").toggle(thisValue == "system" ? true : false);
		//renderFilterHtml();
	});
	
	function renderFilterHtml() {
		buildDKFilters('#enterpriseDeviceSearchQuickFilters', eqf_params);
	}
	
	var eqf_params = {
            'filters': {
                'deviceName': {
                    'title': 'Device Name',
                    'modes': ['Contains', 'Starts With', 'Equal To']
                },
                'ipAddress': {
                    'title': 'IP Address',
                    'modes': ['Contains', 'Starts With', 'Equal To']
                },
                'macAddress': {
                    'title': 'MAC Address',
                    'modes': ['Contains', 'Starts With', 'Equal To']
                },
                'groupId': {
                    'title': 'Group Id',
                    'modes': ['Contains', 'Starts With', 'Equal To']
                }
            },
            'resetCallback': resetDeviceSearchQuickFilters,
            'allowMultiple': true
        };
	
		
		renderFilterHtml();
		$('#enterpriseDeviceSearchQuickFiltersStatusTitleBtn').click(function () {
            toggleEnterpriseQuickFiltersDiv(null);
        });
		
		$('form#enterpriseDeviceSearchQuickFiltersForm').keypress( function( e ) {
	        var code = e.keyCode || e.which;
	        if( code === 13 ) {
	            e.preventDefault();
	            $( "#enterpriseDeviceSearchFiltersFormSubmitBtn").trigger("click");
	        }
	    });
	
	    function toggleEnterpriseQuickFiltersDiv(status) {
	    	if( typeof status === "undefined") {
				status = null;
			}

            var enterpriseDeviceSearchQuickFiltersDivObj = $('#enterpriseDeviceSearchQuickFiltersDiv');

            if (status != null) {
            	enterpriseDeviceSearchQuickFiltersDivObj.toggle(status);
            } else {
            	enterpriseDeviceSearchQuickFiltersDivObj.toggle();
            }

            if (enterpriseDeviceSearchQuickFiltersDivObj.is(":visible")) {
                $('#enterpriseDeviceSearchQuickFiltersStatusTitleIcon').addClass('glyphicon-chevron-down');
            } else {
                $('#enterpriseDeviceSearchQuickFiltersStatusTitleIcon').removeClass('glyphicon-chevron-down');
            }

        }
	    
	    function resetDeviceSearchQuickFilters() {
            $('#enterpriseDeviceSearchQuickFiltersForm select[name="responseSizeLimit"]').val('');
        }
	    
	  //Apply Filters Button Clicked
        $('#enterpriseDeviceSearchFiltersFormSubmitBtn').click(function () {
            $("#deviceListDiv_quickSearch").html("<div style='padding-top: 50px;text-align: center;'><img src=\"/Express/images/ajax-loader.gif\"></div>");

            $has_filters = false;
            $('#enterpriseDeviceSearchQuickFiltersForm .dkFB_Filter_Item_Value_Input').each(function () {
                var filter_value = $(this).val();
                if(filter_value && filter_value.trim().length > 0) {
                    $has_filters = true;
                    return 0;
                }
            });
            if($('#enterpriseDeviceSearch_responseSizeLimit').val().length) {
                $has_filters = true;
            }

            var dataToSend = $('#enterpriseDeviceSearchForm').serialize() + '&' + $("#enterpriseDeviceSearchQuickFiltersForm").serialize();
            //if($has_filters) {
            	dataToSend += "&filterApplied=true";
            //}
            $.post("/Express/Devices/quickDeviceSearch/searchDevicesQuickFilter.php", dataToSend, function (data) {
                $('#deviceListDiv_quickSearch').html(data);
                if($has_filters) {
                    $("#deviceSearchEnterpriseBanner .filterApplyHead").show();
                } else {
                    $("#deviceSearchEnterpriseBanner .filterApplyHead").hide();
                }
            });

            toggleEnterpriseQuickFiltersDiv(true);
        });
	    
	    
		//$("#tabs").tabs();
		//$("#loadingBar_quickSearch").hide();
		//$("#quickDevice_downloadCSVBtn").hide();
		$(document).find("#quickDevice_downloadCSVBtn").hide();
		$("#deleteBtn_quickSearch").hide();
		$("#selectCheckDiv_quickSearch").hide();
		
		
		var sourceSwapIcons = function() {
			var thisId = $(this).find("img");	
	        var newSource = thisId.data('alt-src');
	        thisId.data('alt-src', thisId.attr('src'));
	        thisId.attr('src', newSource);
	    }
	    
		$('#downloadCSV_quickSearch, #deleteDevice_quickSearch, #addDevice').hover(sourceSwapIcons, sourceSwapIcons);
		
		$("input[name='searchlevel'], select[name='restrictToEnterprise'], select[name='selectedDeviceType']").change(function(){ 
			//$("#deviceSearchEnterpriseBanner .filterApplyHead").hide();
			searchDevices_quickFilter();
			toggleEnterpriseQuickFiltersDiv(false);
			//$('#searchDevices_quickFilter').trigger('click');
		});
		
		
		//$('#searchDevices_quickFilter').click(function(){
		
		$("#loadingOnChange").hide();
		 	
	$(document).on("click", "#selectAllChk_quickSearch", function(){

		if ($('#selectAllChk_quickSearch').is(':checked')) {
			$(".checkDeviceListBox_quickSearch").each(function () {
		        var id = $(this).attr("id");
		        $("#"+id).prop('checked', true);
		    });
			$("#deleteDevice_quickSearch").show();
//			$("#deleteBtn_quickSearch").show();
//			$("#deleteDevice_quickSearch").show();
		}else{
			$(".checkDeviceListBox_quickSearch").each(function () {
		        var id = $(this).attr("id");
		        $("#"+id).prop('checked', false);
		    });
			$("#deleteDevice_quickSearch").hide();
//			$("#deleteDevice_quickSearch").hide();
//			$("#deleteBtn_quickSearch").hide();
		}
	});
	
	var deleteDevice = function(){
		$("#loading2").show();
		$("#dialogSearchScanDevice").dialog("option", "title", "Request Complete");
		$("#dialogSearchScanDevice").html('Deleting devices...<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');		    	
		$("#dialogSearchScanDevice").dialog("open");
		$(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
		$(":button:contains('Cancel')").attr("disabled", "disabled").addClass("ui-state-disabled cancelButton");
		$(":button:contains('Delete')").addClass("deleteButton");
		$(":button:contains('Cancel')").addClass("cancelButton");
		$(":button:contains('Done')").addClass("subButton");
		var deleteDeviceArray   = [];
                var devicedataArray     = [];
		$(".checkDeviceListBox_quickSearch").each(function () {
	        var id = $(this).attr("id");
                var devicedatatmp       = "";
	        if ($('#'+id).is(':checked')) {
	        	var deviceName = $('#'+id).attr('name');
	        	deleteDeviceArray.push(deviceName);
                        devicedatatmp = $('#'+id).attr('data-devicedata');
                        var devicedata = deviceName+"---"+devicedatatmp;
                        devicedataArray.push(devicedata);
	        }
	        });
		
		if(deleteDeviceArray.length > 0){
			pendingProcess.push("Delete Device");
			$.ajax({
				type: "POST",
				url:"Devices/deleteDevice.php",
				data: {deviceArray : deleteDeviceArray, deviceDataArray : devicedataArray},
				success: function(result)
				{
					if(foundServerConErrorOnProcess(result, "Delete Device")) {
						return false;
	                }
					var obj = jQuery.parseJSON(result);
					if(obj.Error){
						$("#dialogSearchScanDevice").html(obj.Error);    	
						$(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
						$(":button:contains('Cancel')").removeAttr("disabled").removeClass("ui-state-disabled"); 
					}else{
						$("#dialogSearchScanDevice").html(obj.Success);    	
						buttonsShowHide('Delete', 'hide');
						buttonsShowHide('Cancel', 'hide');
						//buttonsShowHide('Done', 'show');
						$(":button:contains('Done')").addClass("subButton").show();
						$("#loading2").hide();
						searchDevices_quickFilter();
						//$('#searchDevices_quickFilter').trigger('click');
					}
				}
			});
		}
	}
	
	
	
	var buttonsShowHide = function(b,e){
		if(e == "show"){
			$(".ui-dialog-buttonpane button:contains("+b+")").button().show();
		}else{
		 	$(".ui-dialog-buttonpane button:contains("+b+")").button().hide();
		}
	};
            $("#dialogSearchScanDevice").dialog({
                    autoOpen: false,
                    width: 800,
                    modal: true,
                    position: { my: "top", at: "top" },
                    resizable: false,
                    closeOnEscape: false,
                    buttons: {
                            "Cancel": function() {
                                    $(this).dialog("close");
                                    //$("#loading2").hide();
                            },
                            "Delete": function(){
                                    deleteDevice();
                                    toggleEnterpriseQuickFiltersDiv(false);
                            },
                            "Done": function(){
                            		searchDevices_quickFilter();
                                    //$('#searchDevices_quickFilter').trigger("click");
                                    $(this).dialog("close");
                            },
                    },
            open: function() {
                    //buttonsShowHide('Cancel', 'show');
                    //buttonsShowHide('Delete', 'show');
                    $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019
                    setDialogDayNightMode($(this));
                    $(":button:contains('Delete')").addClass("deleteButton").show();
                    $(":button:contains('Cancel')").addClass("cancelButton").show();
                    buttonsShowHide('Done', 'hide');
                    $(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
                    $(":button:contains('Cancel')").removeAttr("disabled").removeClass("ui-state-disabled");
            }
            });	
        //}
	
    $(document).on("click", "#deleteDevice_quickSearch", function() {    
		var num = $(".checkDeviceListBox_quickSearch:checked").length;
                
		$("#dialogSearchScanDevice").dialog("option", "title", "Request Complete");
		$("#dialogSearchScanDevice").html('All selected '+ num +' devices will be removed');		    	
		$("#dialogSearchScanDevice").dialog("open");
		
	});
	
	$(document).on("change", ".checkDeviceListBox_quickSearch", function(){
		var num = $(".checkDeviceListBox_quickSearch:checked").length;
		if(num > 0){
			$( "#selectAllChk_quickSearch" ).prop( "checked", false );
//			$("#deleteBtn_quickSearch").show();
			$("#deleteDevice_quickSearch").show();
		}else{
			
//			$("#deleteBtn_quickSearch").hide();
			//$("#deleteDevice_quickSearch").hide();
			$(document).find("#deleteDevice_quickSearch").hide();
			//$("#selectAllChk_quickSearch").prop('checked', false);
		}
	});
	
});


function searchDevices_quickFilter() {
	$("#deviceSearchEnterpriseBanner .filterApplyHead").hide();
	$(document).find("#quickDevice_downloadCSVBtn").hide();
//	$("#quickDevice_downloadCSVBtn").hide();
	$("#deleteBtn_quickSearch").hide();
		//$("#warningErrorMsg").html("");
		var searchVal = $("#searchDeviceVal").val();
		//if(searchVal != "")
		//{
			$("#loadingBar_quickSearch").show();
			$("#deviceListDiv_quickSearch").hide();
			var formData = $('form#enterpriseDeviceSearchForm').serializeArray();
			$.ajax({
				type: "POST",
				url:"Devices/quickDeviceSearch/searchDevicesQuickFilter.php",
				data: formData,
				success: function(result)
				{
					if(foundServerConErrorOnProcess(result, "")) {
    					return false;
                  	}
					if(result.indexOf("please narrow your search") >= 0){
						$("#deviceListDiv_quickSearch").html(result);
					}else if(result.indexOf("ErrorMessageFromOci") >= 0){
						res = result.split("||");
						alert(res[1]);
					}else{
						$("#deviceListDiv_quickSearch").html(result);
						$("#deviceTable_quickSearch").tablesorter();
						tableRowCount = $('#deviceTable_quickSearch tr').length;
    					if(tableRowCount == 1)
        				{
    						//$("#deleteDevice_quickSearch").hide();
    						$("#deleteDevice_quickSearch").hide();
    						$(".checkAllDevices").hide();
        					$("#deviceTable_quickSearch").append("<tr><td colspan ='7'> No Devices Found </td></tr>");
        				}else{
        					$(document).find("#quickDevice_downloadCSVBtn").show();
        				}
    					
    					$("#deviceListDiv_quickSearch").show();	
    					//$("#warningError").hide();
					}
					 
					$("#loadingBar_quickSearch").hide();
					$("#selectCheckDiv_quickSearch").show();
				}
		

			});
		//}
//});
}

function tabClicked(title)
{
	$("#deviceTabName").html(title);
};