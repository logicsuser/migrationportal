<?php
	require_once("/var/www/html/Express/config.php");
        require_once ("/var/www/lib/broadsoft/login.php");
    checkLogin();

    require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/deviceConfigCustomTagManagement.php");
    $objCustomTagMgmt = new DeviceConfigCustomTagManagement();

    require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");   //Code added @ 10 July 2018
    $srvceObj = new Services();
        
    
    $data    = $errorTableHeader;
    $act     = "";
    $changes = "";    
    
    if(isset($_POST['action']) && $_POST['action']=="resetRebuildCustomTagOnDeviceType")
    {
        unset($_POST["action"]);
        //If rebuildResetDevice visibility is not set then don't show on this page
        if($rebuildResetDevice != "true"){
            unset($_POST['rebuildPhoneFiles']);
            unset($_POST['resetPhone']);
        }
        
        $act = "checkData";
        if($_POST['rebuildPhoneFiles'] == "true" && $_POST['resetPhone'] == "true"){
            $changes = $changes + 1;
            $data .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Devices will be rebuild and reset after successful modification</b></td></tr>';
        }else if($_POST['rebuildPhoneFiles'] == "true" && $_POST['resetPhone'] == ""){
            $changes = $changes + 1;
            $data .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Devices will be rebuild after successful modification</b></td></tr>';
        }else if($_POST['rebuildPhoneFiles'] == "" && $_POST['resetPhone'] == "true"){
            $changes = $changes + 1;
            $data .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Devices will be reset after successful modification</b></td></tr>';
        }
    }
    
    $error = 0;
        
    foreach ($_POST as $key => $value)
    {
                // Show device type only if specified
		if ($value != "")
		{
			$bg = CHANGED;
                }
		else
		{
			$bg = UNCHANGED;
		}
                        
		if ($bg != UNCHANGED)
		{
                        
			$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["DeviceCustomTagAddNames"][$key] . "</td><td class=\"errorTableRows\">";
			if (is_array($value))
			{
                               if($key=="customTagDel"){
                                   $changes = $changes + 1;
                                    foreach ($value as $k => $v)
                                    {
                                            $data .= "%".$v."%<br>";
                                    }
                                }
                                else
                                {
                                    foreach ($value as $k => $v)
                                    {
                                            $data .= $v . "<br>";
                                    }
                                }
			}			
			else if ($value !== "")
			{
                                
                                if($key=="tagName"){
                                    if($value=="Custom Tag is required." || $value=="Custom Tag is aleady exist."){
                                        $data .= $value;
                                    }else{
                                        $data .= "%".$value."%";
                                    }
                                    
                                }else{
                                    $data .= $value;
                                }
			}
			else
			{
				$data .= "None";
			}
			$data .= "</td></tr>";
		}
	}
        
        //Show complete custom tag for rebuild reset device type
        if($act && ($act=="checkData")){
            if(isset($_SESSION['addCustomTagOnDeviceType']) && !empty($_SESSION['addCustomTagOnDeviceType'])){
                $changes = $changes + 1;
                $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Device Config. Custom Tags Added</td><td class=\"errorTableRows\">";
                foreach($_SESSION['addCustomTagOnDeviceType'] as $row){
                    $data .= $row['tagName']. " - ". $row['tagValue']." ,<br />";
                }
                $data .= "</td></tr>";
             }
             if(isset($_SESSION['editCustomTagOnDeviceType']) && !empty($_SESSION['editCustomTagOnDeviceType'])){
                $changes = $changes + 1;
                $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Device Config. Custom Tags Modified</td><td class=\"errorTableRows\">";
                foreach($_SESSION['editCustomTagOnDeviceType'] as $row){
                    $data .= $row['tagName']. " - ".$row['tagValue']." ,<br />";
                }
                $data .= "</td></tr>";
             }
             
        }
        
         $data .= "</td></tr>";
        //End code
	$data .= "</table>";        
        if ($changes == 0)
        {
            $error = 1;
            $data = "You have made no changes.";
        }
	echo $error . $data;
        
        
?>
