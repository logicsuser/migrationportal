/*check delete event validation */
var checkRebuildDialogCondition = "";
var customTagDeviceTagPos = "";
var rebuildReset = "";
var showConfirmDialogOnly = "";
var checkForRebuildResetByAction = "";
$(".deleteDeviceConfigCustomTag").hide() ;
$(document).on("click", "table#deviceConfigCustomTable tbody tr#customUpdate td input.deleteCheckbox", function() {
	$(".deleteDeviceConfigCustomTag").show() ;
	if($('input.deleteCheckbox:checked').length >0){
		 var customTagDeviceTagPos = "delete" ;
		 $(".deleteDeviceConfigCustomTag").hide() ;
		 $("#rebuildPhoneFiles").prop("checked",true);
         $("#resetPhone").prop("checked",true);
		}else{
			$(".deleteDeviceConfigCustomTag").hide() ;
			 $("#rebuildPhoneFiles").prop("checked",false);
	         $("#resetPhone").prop("checked",false);
     }
	 
 
});
/*end delete check event */

/* check validation update */


var isCheckBoxIsClicked_CustomTag = false;
function checkBoxDelBtnShowNavo(status) {
	isCheckBoxIsClicked_CustomTag = status;
}

$(document).on("click", "table#deviceConfigCustomTable tbody tr#customUpdate td", function(event) {
	//if (event.target.type='checkbox') { 
	if (!isCheckBoxIsClicked_CustomTag) { 
		if($(this).hasClass("adminTableCol") || $(this).hasClass("deleteCheckboxtd")){
			 customTagDeviceTagPos ="update" ;
			 var tagDeviceValueUpdate  = $(this).closest('tr#customUpdate').find('.deleteCheckbox').data("device-id");
			 var tagNameUpdate         = $(this).closest('tr#customUpdate').find('.deleteCheckbox').data("tag-id");
			 var tagValueUpdate        = $(this).closest('tr#customUpdate').find('.deleteCheckbox').data("tagval-id");
	 
			 $("#UpdatedeviceType").attr("value",tagDeviceValueUpdate);
			 tagNameUpdate = "%"+tagNameUpdate+"%";
			 $("#levelShowTagVal").html(tagNameUpdate);                         
			 $("#tagName").attr("value",tagNameUpdate);
		     $("#tagValueId").attr("value",tagValueUpdate);
	         $("#hiddenOldTagValue").attr("value",tagValueUpdate);
		     
		     
			 var customTagResultUpdate = $("#update_device_custom_tag_dialog").html();        
		     $("#devicecustomTagDialog").html(customTagResultUpdate);
		     $("#devicecustomTagDialog").dialog("open");
		}
	}	
});

/*end update validation */



var customTagDeviceTagPos = "";
/* show device config custom tag list */
function deviceConfigCustomTag(listShowAction, checkForRebuildResetByAction){
    
    var deviceTypeNameValue = document.getElementById("select_device_Config_Custom_Tags").value;
    $("#customConfigTagDeviceNameDiv").hide();
    $("#custome_tag_deviceName_html").hide();
    if(deviceTypeNameValue =='None'){
			$("#customConfigTagDeviceNameDiv").hide();
			$("#custome_tag_deviceName_html").hide();
    }else{
    	//getCustomTagList(deviceTypeNameValue);
    //	document.getElementById("custome_tag_deviceName_html").innerHTML = deviceTypeNameValue;        
        $("#deviceTypeCustomTag").val(deviceTypeNameValue);  
        $("#deviceTypeForDownloadCSV").val(deviceTypeNameValue);   
    	$("#customConfigTagDeviceNameDiv").show();
    	$("#custome_tag_deviceName_html").show();
    	getCustomTagList(deviceTypeNameValue, listShowAction);
        
        if(checkForRebuildResetByAction=="actionTaken"){
            $("#rebuildPhoneFiles").prop("checked",true);
            $("#resetPhone").prop("checked",true);
        }else{
            $("#rebuildPhoneFiles").prop("checked",false);
            $("#resetPhone").prop("checked",false);
        }
        
        if(listShowAction==""){
            $(".effectedDeviceLable").html("");
        }
    }    
}
/* unwanted code */
/* $(document).on("click", ".deleteCheckbox", function() {
	 if($('input.deleteCheckbox:checked').length >0){
		 var customTagDeviceTagPos = "delete" ;
		 $(".deleteDeviceConfigCustomTag").show() ;
		}else{
   	 $(".deleteDeviceConfigCustomTag").hide() ;
      }
	
});
$(".deleteDeviceConfigCustomTag").hide() ; */
//$("table tbody td input.deleteCheckbox").click(function(){
//	  alert('s');
//	 $("#select_count").html($("input.deleteCheckbox:checked").length+" Selected");
//	 if($('input.deleteCheckbox:checked').length >0){
//		 var customTagDeviceTagPos = "delete" ;
//		 $(".deleteDeviceConfigCustomTag").show() ;
//		}else{
//   	 $(".deleteDeviceConfigCustomTag").hide() ;
//      }
//   });


/* end */
/*end unwanted code */
/* click title button device configuration custom tag button */
$("#Device_Configuration_Custom_Tags").click(function(){
	 resetCofigCustomTag() ;
	 /*Device Type select dropdown 'None' on page load */
	 $('#select_device_Config_Custom_Tags').val("None");
	 /* end select 'None' into dropdown list  */
	$("#deviceTabName").html("Device Configuration Custom Tags");
	 
});
/*end */
 
var resetCofigCustomTag = function(){
	$('#select_device_Config_Custom_Tags').val("None");
	$('#customConfigTagDeviceNameDiv').hide();
	$('#custome_tag_deviceName_html').hide();
	$("#loading2").hide();
}; 
 
/*update custom tab value */
/*un wanted code */
/*
$("table#deviceConfigCustomTable tbody tr#customUpdate").click(function (){
	customTagDeviceTagPos ="update" ;
	 //$("#devicecustomTagDialog").html(customTagDeviceTagPos);
         
	 var tagNameUpdate          =  	$(this).find('.deleteCheckbox').data("tag-id");
	 var tagDeviceValueUpdate   =  	$(this).find('.deleteCheckbox').data("device-id");
	 var tagValueUpdate         =  	$(this).find('.deleteCheckbox').data("tagval-id");
	 
	 $("#tagValueData").val(tagValueUpdate);	 
	 $("#showHideTagName").hide();
	 $("#tagName").val(tagNameUpdate);
	 //var deviceTypeNameValusse = document.getElementById("resultVal").value = tagDeviceValueUpdate;
	
        $("#lavelShowUpdateTagName").show();
	$("#levelShowTagVal").html(tagDeviceValueUpdate);
	var customTagResultUpdate = $("#device_custom_tag_dialog").html();        
        $("#devicecustomTagDialog").html(customTagResultUpdate);
        $("#devicecustomTagDialog").dialog("open");
}); 
/*end */

/* click 'Add' button device config custom tag show dialog */	
$("#customConfigTagDeviceNameDiv").hide();
$("#custome_tag_deviceName_html").hide();

$("#addDeviceConfigCustomTagBtn").click(function(){
	customTagDeviceTagPos = "create";
        var customTagDialogState =  $("#customTagDialogState").val("create");
        var customTagResult = $("#device_custom_tag_dialog").html();
        $("#devicecustomTagDialog").dialog("open");
        $("#devicecustomTagDialog").html(customTagResult);
        $("#lavelShowUpdateTagValue").hide();
    });
/* end */
        


/*reset and rebuild dialog on click event */

$("#rebuildResetCustomTagsOnDeviceType").click(function () {

	//customTagDeviceTagPos = "resetRebuild";
  showConfirmDialogOnly = "";   
	customTagDeviceTagPos = "resetRebuild";
	
	if($('input.deleteCheckbox:checked').length >0){
		showConfirmDialogOnly = "yes";
		
		customTagDeviceTagPos = "delete";
         var customTagSelectedList = [];
            $(".deleteCheckbox:checked").each(function() {
                  customTagSelectedList.push( {
                           tagName: $( this ).data( 'tag-id' ),
                        deviceType: $( this ).data( 'device-id' )
                      });
           });	
          $.ajax({
                    method:"post",
                    url:"Devices/DeviceConfCustomTags/manageCustomTag.php",
                    data:{checkedCustomTag:customTagSelectedList, "funcType":"checkEffectedDecice"},
                }).success(function(res){                	
                  $("#createConfirmCustomeTag").dialog("close");
                  $("#devicecustomTagDialog").dialog("open");
                  $("#devicecustomTagDialog").html(res);
                              
                });
	 }
	
  
  
  
  
  
  
  if(showConfirmDialogOnly!="yes"){
  
	var dataToSend = $("form#device_Config_Custom_Tags").serializeArray();	
	
	$.ajax({
        type: "POST",
        url: "Devices/DeviceConfCustomTags/validateData.php",
        data: dataToSend,
        success: function(result)
        {
        	if(foundServerConErrorOnProcess(result, "")) {
				return;
          	}
            result = result.trim();
            if (result.slice(0, 1) == 1)
            {
                $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
            }
            else
            {
                $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled subButton");                                 
            }
            $("#createConfirmCustomeTag").html(result.slice(1));
            $("#devicecustomTagDialog").dialog("close");
            $("#createConfirmCustomeTag").dialog("open");
           
            $(":button:contains('Complete')").show();
            $(":button:contains('Cancel')").show();
            $(":button:contains('More Changes')").hide();
            $(":button:contains('Return To Main')").hide();
            $(":button:contains('Create')").hide();
            var deviceTypeNameValue = $("#deviceTypeCustomTag").val();
            var deviceTypeNameValue = document.getElementById("select_device_Config_Custom_Tags").value;
            //getCustomTagList(deviceTypeNameValue);
            checkRebuildDialogCondition = "rebuild";
        }
	});
  }
});
/*end code here */


$(function() {
	
 /* dialog box for 'Add/Modify/Delete Device Configuration Custom Tags' */  
	  $("#devicecustomTagDialog").dialog({
          autoOpen: false,
          width: 700,
          modal: true,
          title: "Device Configuration Custom Tags",
          position: { my: "top", at: "top" },
          resizable: false,
          closeOnEscape: false,
          buttons: {
        	  	
        		Create: function() {
                            var dataToSend = $("form#add_device_config_custom_tag").serializeArray();
                            var pageUrl = "checkData.php"; 
                            $.ajax({
                              type: "POST",
                              url: "Devices/DeviceConfCustomTags/"+pageUrl,
                              data: dataToSend,
                              success: function(result)
                              {
                            	  if(foundServerConErrorOnProcess(result, "")) {
       	        					return;
       	                      	  }
                                  result = result.trim();
                                  if (result.slice(0, 1) == 1)
                                  {
                                      $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
                                  }
                                  else
                                  {
                                      $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled subButton");                                 
                                  }
                                  $("#createConfirmCustomeTag").html(result.slice(1));
                                  $("#devicecustomTagDialog").dialog("close");
                                  $("#createConfirmCustomeTag").dialog("open");

                                  $(":button:contains('Complete')").show();
                                  $(":button:contains('Cancel')").show();
                                  $(":button:contains('More Changes')").hide();
                                  $(":button:contains('Return To Main')").hide();
                                  $(":button:contains('Create')").hide();
                                  // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                                  var deviceTypeNameValue = document.getElementById("select_device_Config_Custom_Tags").value;
                                  //  getCustomTagList(deviceTypeNameValue);
                      }
                });  
        	},
             Update: function() {
                 
            	  var dataToSend = $("form#update_device_config_custom_tag").serializeArray();
                  $.ajax({
                      type: "POST",
                      url: "Devices/DeviceConfCustomTags/checkDataModify.php",
                      data: dataToSend,
                      success: function(result)
                      {                       
                          //$("#devicecustomTagDialog").dialog("open");
                          $("#createConfirmCustomeTag").dialog("open");
                          result = result.trim();                      	
                          if (result.slice(0, 1) == 1){
                              $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                          }else{
                              $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                         }
                         
                          $("#createConfirmCustomeTag").html(result.slice(1));
                          $("#devicecustomTagDialog").dialog("close");
                          $("#createConfirmCustomeTag").dialog("open");
                          
                          $(":button:contains('Complete')").show();
                          $(":button:contains('Cancel')").show();
                          $(":button:contains('More Changes')").hide();
                          $(":button:contains('Return To Main')").hide();
                          checkRebuildDialogCondition = "updateCustomTags";
                          /*var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                          getCustomTagList(deviceTypeNameValue);*/
                      }
                }); 
             },
              
            	Confirm: function() { 
               	 
                	var dataToSend = $("form#device_Config_Custom_Tags").serializeArray();	
                	showConfirmDialogOnly = "";
                	$.ajax({
                        type: "POST",
                        url: "Devices/DeviceConfCustomTags/validateData.php",
                        data: dataToSend,
                        success: function(result)
                        {        	
                            result = result.trim();
                            if (result.slice(0, 1) == 1)
                            {
                                $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                            }
                            else
                            {
                                $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                            }
                            $("#createConfirmCustomeTag").html(result.slice(1));
                            $("#devicecustomTagDialog").dialog("close");
                            $("#createConfirmCustomeTag").dialog("open");
                            $(":button:contains('Complete')").show().addClass("subButton");;
                            $(":button:contains('Cancel')").show().addClass("cancelButton");;
                            $(":button:contains('More Changes')").hide();
                            $(":button:contains('Return To Main')").hide();
                            $(":button:contains('Create')").hide();
                            checkRebuildDialogCondition = "rebuild";
                         }
                	});
              },
              Close: function() {
                  $(this).dialog("close");
                  resetCofigCustomTag();
              },              
              Cancel: function() {
               //  deviceConfigCustomTag();
                  $(this).dialog("close");
              }

          },
          open: function() {
        	$("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019
                setDialogDayNightMode($(this));
        	 if(customTagDeviceTagPos =="create"){
        		 customTagDeviceTagPos ="create";
        		 checkRebuildDialogCondition ="addCustomTags" ;
	        	  $(".ui-dialog-buttonpane button:contains('Create')").button().show().addClass("subButton");
	        	  $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass("cancelButton");
	        	  $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
	        	  $(".ui-dialog-buttonpane button:contains('Close')").button().hide();
	        	  $(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
	        	  $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
                }if(customTagDeviceTagPos =="update"){
                         checkRebuildDialogCondition = "updateTagName";
        		 $(".ui-dialog-buttonpane button:contains('hide')").button().show();
        		 $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass("cancelButton");
        		 $(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
        		 $(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
           	  	 $(".ui-dialog-buttonpane button:contains('Close')").button().hide();
                         $(".ui-dialog-buttonpane button:contains('Update')").button().show().addClass("subButton");
           	  	 $(".ui-dialog-buttonpane button:contains('Create')").button().hide();
           	  	 $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
           	  	 
           	}if(customTagDeviceTagPos =="delete"){
                    
        		  checkRebuildDialogCondition = "deleteTagName";
        		  $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass("cancelButton");
        		  $(".ui-dialog-buttonpane button:contains('Confirm')").button().show().addClass("subButton");
        		  $(".ui-dialog-buttonpane button:contains('Create')").button().hide();
	        	  $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
	        	  $(".ui-dialog-buttonpane button:contains('Close')").button().hide();
	        	  $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
        	 }
        	 
        	 if(customTagDeviceTagPos =="resetRebuild"){
                     
	        	  checkRebuildDialogCondition = "rebuild";
	       		  $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass("cancelButton");
	       		  $(".ui-dialog-buttonpane button:contains('Confirm')").button().show().addClass("subButton");;
	       		  $(".ui-dialog-buttonpane button:contains('Create')").button().hide();
	        	  $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
	        	  $(".ui-dialog-buttonpane button:contains('Close')").button().hide();
	        	  $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
                }
          }
      });
	  

	  
	  /*DIALOG SUCCESS  **/	  
	  $("#createConfirmCustomeTag").dialog({
                autoOpen: false,
                width: 800,
                modal: true,
                title: "Device Configuration Custom Tags",
                position: { my: "top", at: "top" },
                resizable: false,
                closeOnEscape: false,
                buttons: {
                            Complete: function() {
                            			pendingProcess.push("Modify Device Configuration Custom Tags");
                                       //alert('checkRebuildDialogCondition - '+checkRebuildDialogCondition);
                                       if(checkRebuildDialogCondition =="rebuild"){
						 var dataToSend = $("form#device_Config_Custom_Tags").serializeArray();
                                                 rebuildReset = "Yes";
                                                 checkRebuildDialogCondition = "";
                                                 checkForRebuildResetByAction = "";
					}if(checkRebuildDialogCondition =="addCustomTags"){
						 var dataToSend = $("form#add_device_config_custom_tag").serializeArray();
                                                 var rebuildReset = "";
                                                 checkForRebuildResetByAction = "actionTaken";
                                       }if(checkRebuildDialogCondition =="updateCustomTags"){
						 var dataToSend = $("form#update_device_config_custom_tag").serializeArray();
                                                 var rebuildReset = "";
                                                 checkForRebuildResetByAction = "actionTaken";
                                        }
                                        $.ajax({
                                                    type: "POST",
                                                    url: "Devices/DeviceConfCustomTags/manageCustomTag.php",
                                                    data: dataToSend,
                                                    success: function(res)
                                                    {
                                                    	if(foundServerConErrorOnProcess(res, "Modify Device Configuration Custom Tags")) {
                        			    					return false;
                        			                  	}
                                                       $("#devicecustomTagDialog").dialog("close");
                                                        $("#createConfirmCustomeTag").dialog("close");
                                                        $("#createConfirmCustomeTag").html(res);
                                                        $(".ui-dialog-titlebar-close", this.parentNode).hide();
                                                       // $(".deleteDeviceConfigCustomTag").hide() ;
                                                       // $("#select_count").html($("input.deleteCheckbox:checked").length+" Selected");
                                                        deviceConfigCustomTag(checkRebuildDialogCondition, checkForRebuildResetByAction); 
                                                       //     $(".ui-dialog-buttonpane button:contains('More Changes')").button().show().addClass('cancelButton');
                                                        
                                                        if(checkForRebuildResetByAction=="actionTaken"){
                                                            $("#rebuildPhoneFiles").prop("checked",true);
                                                            $("#resetPhone").prop("checked",true);
                                                        }else{
                                                            $("#rebuildPhoneFiles").prop("checked",false);
                                                            $("#resetPhone").prop("checked",false);
                                                        }
                                                        
                                                        if(rebuildReset=="Yes"){
                                                            //$("#dialog").append(returnLink);
                                                            $("#createConfirmCustomeTag").dialog("open");
                                                            $(".ui-dialog-buttonpane button:contains('More Changes')").button().show().addClass('cancelButton');
                                                            $(":button:contains('Complete')").hide();
                                                            $(":button:contains('Cancel')").hide();
                                                            $("#createConfirmCustomeTag").append(returnLink);
                                                            $(".effectedDeviceLable").html("");
                                                        }else{
                                                            var resArr = res.split("-");
                                                            var affectedDevice = resArr[1];
                                                            $(".effectedDeviceLable").html(affectedDevice);
                                                        }
                                                    }
		                            }); 
                            },                            
                           'More Changes': function() {
                                    //alert('checkRebuildDialogCondition - '+checkRebuildDialogCondition);
                                    $(this).dialog("close");
                                    $(".deleteDeviceConfigCustomTag").hide() ;
                               	   // $("#select_count").html($("input.deleteCheckbox:checked").length+" Selected");
                                    deviceConfigCustomTag(checkRebuildDialogCondition, checkForRebuildResetByAction);
                            },
                            Cancel: function() {
                              //resetCofigCustomTag();
                              //deviceConfigCustomTag();
                               $(this).dialog("close");
                            }
                         },
                open: function() {
                    $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019		
                    setDialogDayNightMode($(this));
                    $(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass("subButton");;
                    $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass("cancelButton");;
                    $(".ui-dialog-buttonpane button:contains('Close')").button().hide();
                    $(".ui-dialog-buttonpane button:contains('Create')").button().hide();
                    $(".ui-dialog-buttonpane button:contains('Return To Main')").button().hide();
                }
      });
});

/*end Device Configuration Custom Tags dialog*/

/*get save custom tag records */

var successGetList = function (data){
			if(foundServerConErrorOnProcess(data, "")) {
				return;
			}
            var tableCls   = "";
            var tableData  = "";
            var deviceType = "";
            var result     = JSON.parse(data);
            var deviceType = result.deviceType;
            var deviceTypeClass = deviceType;
            var newClassForSorting = "";            
            delete result.deviceType; //Removing last element mean deviceType from array 
            tableCls = "deviceConfigCustomTable_"+deviceTypeClass;
            if(deviceType!=undefined){                
                    tableData         += "<table class='"+tableCls+" scroll' id='deviceConfigCustomTable'>";
                    tableData         += "<thead> ";
                    tableData         += "<tr style='margin-top:10px !important;margin-bottom:10px !important'>  ";
                    tableData         += "<th class='thSno header'>Delete</th>";
                    tableData         += "<th class='thSno'>Tag Name</th>";
                    tableData         += "<th class='thSno'>Tag Value</th>";
                    tableData         += "<th class='thSno thSnoImageTh'></th>";
                    tableData         += "</tr> ";
                    tableData         += "</thead>";
                    tableData         += "<tbody id='customTagTable'>"; 
                    $.each(result, function(key, value){  
                         var tmpTagName = value.tagName;                     
                         var tagName = tmpTagName.substr(1, tmpTagName.length-2);
                        // tableData += "<tr class='spacer'></tr>" ;
                         tableData += "<tr style='' id='customUpdate'>";
                         tableData += "<td class='deleteCheckboxtd'><input type='checkbox'  class='deleteCheckbox' name='customTagDel[]' value='" +tagName+ "' data-tag-id='" +tagName+ "' data-tagval-id='" +value.tagValue+ "' data-device-id='" +deviceType+ "' id='" +tagName+ "'";
                         
                         
                         tableData += "><label for='" +tagName+ "'><span  onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label></td>";
                         
                         
                         tableData += "<td class='adminTableCol thSno header'>"+ tagName + "</td>";
                         tableData += "<td class='adminTableCol thSno header' >"+ value.tagValue + "</td>";
                         tableData += "<td class='adminTableCol thSnoImageTh header' ><img src='images/icons/eyes_rest.png' style='padding-left:90%' class='imageHoverAlign'></td>";
                         tableData += "</tr>";
                      //  tableData += "<tr class='spacer'></tr>" ;
                         
                    })
                    tableData         += "</tbody>";
                    tableData         += "</table>";
                    $(".customTagListMessage").html("");
                    $("#mainOuterDivForCustomTag").show();
                                       
                    $(".customTagListData").html(tableData);
                    $(".customTagListData").show(); 
                   /* table shorter code
                    * waiting for permission
                    */ 
                   /*  $("."+tableCls).tablesorter(); */ 
                    $("."+tableCls).tablesorter();
            }else{                
                    $(".customTagListData").hide();
                    $("#mainOuterDivForCustomTag").hide();
                    $(".hideRebuildWithNoRecord").hide();
                    $(".customTagListMessage").html("No Device Custom Tags");
            }
            
            $("#customConfigTagDeviceNameDiv").show();
            $("#deviceCustomTagCongLoader").hide() ;
};

var errorMsg = function (error){	
	//alert("error - "+error);
        alert('Please wait a while some data loading is in process');
};



var getCustomTagList = function (deviceType, listShowAction){
	$("#customConfigTagDeviceNameDiv").hide();
	$("#deviceCustomTagCongLoader").show() ;
	
            $.ajax({
                    method:"POST",
                      url:"Devices/DeviceConfCustomTags/customTagList.php",
                      data:{"deviceType":deviceType, "listShowAction":listShowAction},
             }).then(successGetList,errorMsg);
};

/* end get save custom tag records */

$("#downloadCSV_Basic").click(function()
{        
        $("#downloadCSVForm").submit();
});