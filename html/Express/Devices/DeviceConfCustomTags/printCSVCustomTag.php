<?php
    require_once("/var/www/html/Express/config.php");
    require_once ("/var/www/lib/broadsoft/login.php");
    checkLogin();    
    require_once ("/var/www/lib/broadsoft/adminPortal/deviceConfigCustomTagManagement.php");
    

    if(isset($_POST['deviceTypeForDownloadCSV']) && $_POST['deviceTypeForDownloadCSV']!="")
    { 
        $objCustomTagMgmt = new DeviceConfigCustomTagManagement();
        $deviceType  = $_POST['deviceTypeForDownloadCSV'];
        $message     = "Device Type, Tag Name, Tag Value \n";
        
        $fileName = $_SESSION["groupId"]."-$deviceType-".rand(1092918, 9281716);
    
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment;filename=\"" . $fileName . ".csv\"");
        header("Content-Transfer-Encoding: binary");        

        $customTagList = $objCustomTagMgmt->getCustomTagsListsByDeviceType($deviceType);
        $customTagArr  = $customTagList['Success'];        
        
        if(!empty($customTagArr))
        {
            foreach($customTagArr as $ky => $rowVal)
            {

                $customTagName = $rowVal['tagName'];
                $customTagVal  = $rowVal['tagValue'];
                if($rowVal['tagValue']=="")
                {           
                   $customTagVal = "None";
                }
                $message .=                    
                    "=\"" . $deviceType . "\"," .
                    "=\"" . $customTagName .  "\"," .
                    "=\"" . $customTagVal . "\"\n";
            }
        }
        //echo $message; die;
        $fp = fopen("php://output", "a");
        fwrite($fp, $message);
        fclose($fp);
    }

    
?>
