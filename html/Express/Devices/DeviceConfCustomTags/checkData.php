<?php
    require_once("/var/www/html/Express/config.php");
    require_once ("/var/www/lib/broadsoft/login.php");
    checkLogin();

    require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/deviceConfigCustomTagManagement.php");
    $objCustomTagMgmt = new DeviceConfigCustomTagManagement();

    require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");   //Code added @ 10 July 2018
    $srvceObj = new Services();
    
    $data = $errorTableHeader;	    
    
    /*if(isset($_POST['action']) && $_POST['action']=="resetRebuildCustomTagOnDeviceType")
    {
        unset($_POST["action"]);
        if($_POST['rebuildPhoneFiles'] == "true" && $_POST['resetPhone'] == "true"){
            $data .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild and reset after successful modification</b></td></tr>';
        }else if($_POST['rebuildPhoneFiles'] == "true" && $_POST['resetPhone'] == "false"){
            $data .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild after successful modification</b></td></tr>';
        }else if($_POST['rebuildPhoneFiles'] == "false" && $_POST['resetPhone'] == "true"){
            $data .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be reset after successful modification</b></td></tr>';
        }
    }*/
    
    

    $error = 0;
    if(isset($_POST['addAction']) && $_POST['addAction']=="addGroupLevelDeviceCustomeTag")
    {
        $nonRequired = array("tagName", "tagValue");
        unset($_POST["addGroupLevelDeviceCustomeTag"]);
        unset($_POST["addAction"]);
    }
    
    
    foreach ($_POST as $key => $value)
    {
        // Show device type only if specified
        if ($key == "deviceTypeResult") {
            if ($value == "") {
                $nonRequired[] = "deviceTypeResult";
            }
        }
        
        if ($key == "tagValue") {
            if ($value == "") {
                $value = "None";
            }
        }

		if ($value !== "")
		{
			$bg = CHANGED;
		}
		else
		{
			$bg = UNCHANGED;
		}

		if (!in_array($key, $nonRequired) and $value == "")
		{
			$error = 1;
			$bg = INVALID;
			$value = $_SESSION["DeviceCustomTagAddNames"][$key] . " is a required field.";
		}
                
		if ($key == "deviceType" and $value == "")
		{			
			$error = 1;
                        $bg = INVALID;
                        $value = $_SESSION["DeviceCustomTagAddNames"][$key] . " is required.";
		}
                if ($key == "tagName" and $value == "")
		{     
			$error = 1;
                        $bg    = INVALID;
                        $value = "Custom Tag Name is required.";
		}
                
                if ($key == "tagName" and $value != "")
		{	                        
                        $customTagList = $objCustomTagMgmt->getCustomTagsList($_POST['deviceType']);
                        $customTagName = "%".$_POST['tagName']."%";
                        if(in_array($customTagName, $customTagList['Success']))
                        {
                            $error = 1;
                            $bg = INVALID;                   
                            $value = "Custom Tag Name is already exist.";
                        }
		}
        
        
		if ($bg != UNCHANGED)
		{
			$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["DeviceCustomTagAddNames"][$key] . "</td><td class=\"errorTableRows\">";
			if (is_array($value))
			{
				foreach ($value as $k => $v)
				{
					$data .= $v . "<br>";
				}
			}			
			else if ($value !== "")
			{
                                if($key=="tagName"){
                                    if($value=="Custom Tag Name is required." || $value=="Custom Tag Name is already exist."){
                                        $data .= $value;
                                    }else{
                                        $data .= "%".$value."%";
                                    }
                                    
                                }else{
                                    $data .= $value;
                                }
			}
			else
			{
				$data .= "None";
			}
			$data .= "</td></tr>";
		}
	}
	$data .= "</table>";
	echo $error . $data;
?>
