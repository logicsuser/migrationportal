<?php 
    require_once("/var/www/html/Express/config.php");
    require_once ("/var/www/lib/broadsoft/login.php");
    checkLogin();
    require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/deviceConfigCustomTagManagement.php");
    $objCustomTagMgmt = new DeviceConfigCustomTagManagement();
    $_SESSION['existCustomTagOnDeviceType'] = array();
    
    server_fail_over_debuggin_testing(); /* for fail Over testing. */
    
    $completeCustomTagExistsArr = array();
    $customTagExistsArr         = array();
    $tempAddRowArr              = array();
    $tempModifyRowArr           = array();    
    
    if(isset($_POST['deviceType']) && $_POST['deviceType']!=""){
        
        $deviceType           = $_POST['deviceType'];
        $customTagList        = $objCustomTagMgmt->getCustomTagsListsByDeviceType($deviceType);
        $cstmTmpTagExistsArr  = $customTagList['Success'];
       
       if(isset($_POST['listShowAction']) && ($_POST['listShowAction']=="")){
            unset($_SESSION['addCustomTagOnDeviceType']);
            unset($_SESSION['editCustomTagOnDeviceType']);
        }
        
        if(!empty($cstmTmpTagExistsArr)){
            foreach($cstmTmpTagExistsArr as $key=>$val){                
                $tempArr['tagName']                 = $val['tagName'];
                $tempArr['tagValue']                = $val['tagValue'];       
                $tempArr['deviceType']              = $deviceType;
                $customTagExistsArr[]               = $tempArr;
            }
            $completeCustomTagExistsArr             = $customTagExistsArr;            
        }
        
        if(isset($_POST['listShowAction']) && ($_POST['listShowAction']!="")){
            //Code to start mrrge array element into mail list to show only        
            if(isset($_SESSION['addCustomTagOnDeviceType']) && !empty($_SESSION['addCustomTagOnDeviceType'])){
                $completeCustomTagExistsArr = array();
                foreach($_SESSION['addCustomTagOnDeviceType'] as $row){
                    if($row['deviceType']==$deviceType){
                        $tempAddRowArr[] = $row;
                    }
                }
               $completeCustomTagExistsArr = array_merge($customTagExistsArr, $tempAddRowArr);
               $_SESSION['existWithAddCustomTagOnDeviceType'] = $completeCustomTagExistsArr;
            }
            
            if(isset($_SESSION['editCustomTagOnDeviceType']) && !empty($_SESSION['editCustomTagOnDeviceType'])){                 
                foreach($completeCustomTagExistsArr as $keyExist => $rowExist){            
                    foreach($_SESSION['editCustomTagOnDeviceType'] as $keyMod => $rowMod){
                        if($rowExist['tagName']==$rowMod['tagName']){
                            if($rowExist['tagValue']!=$rowMod['tagValue']){
                                unset($completeCustomTagExistsArr[$keyExist]);
                                $tempArr = array();
                                $tempArr['tagName']        = $rowExist['tagName'];
                                $tempArr['tagValue']       = $rowMod['tagValue'];
                                $tempArr['tagOldValue']    = $rowExist['tagValue'];
                                $tempArr['deviceType']     = $rowExist['deviceType'];
                                $completeCustomTagExistsArr[$keyExist] = $tempArr;
                            }
                        }
                    }
                }               
            }
        }        
        
        if(!empty($completeCustomTagExistsArr)){            

            foreach ($completeCustomTagExistsArr as $key1 => $row1){
                $vc_array_name[$key1] = $row1['tagName'];
            }
            array_multisort($vc_array_name, SORT_ASC, $completeCustomTagExistsArr);
            $completeCustomTagExistsArr['deviceType']  = $deviceType; 
        }
        
        $_SESSION['existCustomTagOnDeviceType']       = $customTagExistsArr;
        $_SESSION['completeCustomTagOnDeviceType']    = $completeCustomTagExistsArr;
      
        echo  json_encode($completeCustomTagExistsArr);
        
    }
?>      
           