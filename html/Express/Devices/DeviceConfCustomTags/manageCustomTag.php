<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/deviceConfigCustomTagManagement.php");
require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");

$objDvceOpratn      = new DeviceOperations();
$objCustomTagMgmt   = new DeviceConfigCustomTagManagement();

//echo "<pre>POST - ";
//print_r($_POST);
//die();
//$_SESSION['deleteRecordTagName'] = array() ;
//Code start Add Custom Tag code @ 10 Oct 2018
if(isset($_POST['addAction']) && $_POST['addAction']=="addGroupLevelDeviceCustomeTag"){
    
    $addErrorMsg   = "";
    $addSuccesMsg  = "";
    $success       = "";
    $deviceType    = $_POST['deviceType'];
    $tagName       = "%".$_POST['tagName']."%";
    $tagValue      = $_POST['tagValue'];
    
    $customTagList = $objCustomTagMgmt->getCustomTagsList($deviceType);      
    //Check custom tag exists in group if not then add
    $tempTotalRows = array();    
    if(!in_array($tagName, $customTagList['Success'])){
        
        if(isset($_SESSION['addCustomTagOnDeviceType']) && !empty($_SESSION['addCustomTagOnDeviceType'])){
            $tempTotalRows                          =  $_SESSION['addCustomTagOnDeviceType'];
            $tempCustomTagAddArr                    =  array();
            $tempCustomTagAddArr['tagName']         =  $tagName;
            $tempCustomTagAddArr['tagValue']        =  $tagValue;  
            $tempCustomTagAddArr['deviceType']      =  $deviceType;  
            $tempTotalRows[]                        =  $tempCustomTagAddArr;
            $_SESSION['addCustomTagOnDeviceType']   =  $tempTotalRows;
        }else{
            $tempTotalRows                          =  array();
            $tempCustomTagAddArr                    =  array();
            $tempCustomTagAddArr['tagName']         =  $tagName;
            $tempCustomTagAddArr['tagValue']        =  $tagValue; 
            $tempCustomTagAddArr['deviceType']      =  $deviceType;  
            $tempTotalRows[]                        =  $tempCustomTagAddArr;
            $_SESSION['addCustomTagOnDeviceType']   =  $tempTotalRows;
        }        
        $success = 1;
        $dvceListResponse   = $objDvceOpratn->GroupAccessDeviceGetListRequest($_SESSION["sp"], $_SESSION["groupId"], $deviceType);
        $noOfEffectedDevice = count($dvceListResponse['Success']);
        $addSuccesMsg       = "Adding Custom Tag - $noOfEffectedDevice devices will be affected. <br />";        
        echo $addSuccesMsg;          
        
    }else{
            //Error
    }  
}
//End code Aadd Custom Tag

//Code start for modify custon tag
if(isset($_POST['updateAction']) && $_POST['updateAction'] == "updateGroupLevelDeviceCustomeTag"){
    
        $deviceType        = trim($_POST['UpdatedeviceType']);
        //$tagName           = "%".trim($_POST['updateTagName'])."%";
        $tagName           = trim($_POST['updateTagName']);
        $tagValue          = trim($_POST['updateTagValue']);   
        $tagOldValue       = trim($_POST['hiddenOldTagValue']);
        $tempTotalModRows  = array();
    
        if(isset($_SESSION['editCustomTagOnDeviceType']) && !empty($_SESSION['editCustomTagOnDeviceType'])){
            $tempTotalModRows                       =  $_SESSION['editCustomTagOnDeviceType'];
            $tempCustomTagModArr                    =  array();
            $tempCustomTagModArr['tagName']         =  $tagName;
            $tempCustomTagModArr['tagValue']        =  $tagValue;   
            $tempCustomTagModArr['tagOldValue']     =  $tagOldValue;   
            $tempCustomTagModArr['deviceType']      =  $deviceType;
            $tempTotalModRows[]                     =  $tempCustomTagModArr;
            $_SESSION['editCustomTagOnDeviceType']  =  $tempTotalModRows;
        }else{
            $tempTotalModRows                       =  array();
            $tempCustomTagModArr                    =  array();
            $tempCustomTagModArr['tagName']         =  $tagName;
            $tempCustomTagModArr['tagValue']        =  $tagValue; 
            $tempCustomTagModArr['tagOldValue']     =  $tagOldValue;   
            $tempCustomTagModArr['deviceType']      =  $deviceType;  
            $tempTotalModRows[]                     =  $tempCustomTagModArr;
            $_SESSION['editCustomTagOnDeviceType']  =  $tempTotalModRows;
        }
        
        $success = 1;
        $dvceListResponse   = $objDvceOpratn->GroupAccessDeviceGetListRequest($_SESSION["sp"], $_SESSION["groupId"], $deviceType);
        $noOfEffectedDevice = count($dvceListResponse['Success']);
        $modSuccesMsg       = "Modifying Custom Tag - $noOfEffectedDevice devices will be affected <br />";    
        echo $modSuccesMsg; 
}
//End code
/*
if($_POST['funcType'] =="deleteDeviceCustomTag"){
    $serviceProvider = $_SESSION["sp"] ;
    if(isset($_POST['checkedCustomTag'])) {
        
            if($rebuildResetDevice == "true"){
                $rebuildPhoneFiles  = $_POST['rebuildPhoneFiles'];
                $resetPhone         = $_POST['resetPhone'];
            }else{
                $rebuildPhoneFiles  = "true";
                $resetPhone         = "true";
            }
            $delErrorMsg = "";
            $successDelCustomTagArr = array();
            $changeLogObj = new ChangeLogUtility($_SESSION["sp"],$_SESSION["groupId"], $_SESSION["loggedInUserName"]);
            foreach($_POST['checkedCustomTag'] as $key=>$value) {          
                $deviceType          = $value['deviceType'];
                $tagName             = "%".$value['tagName']."%";
                $_SESSION['deleteCustomTag']=  $tagName ;
                $responseDelResponse = $objCustomTagMgmt->groupAccessDeviceDeleteCustomTag($deviceType, $tagName);
                $delErrorMsg         = $responseDelResponse->command->summaryEnglish;
                if($delErrorMsg!=""){
                   echo "<span style='color:red;'>".$delErrorMsg."</span><br />";
                }else{                   
                   echo "$tagName deleted successfully <br />";
                   $changeLogObj->module     = "Delete Device Config CustomTags";
                   $changeLogObj->entityName = $tagName;
                   $changeLogObj->changeLog();
                   
                   $changeLogObj->modTableName = "deviceConfigCustomTagsModChanges";
                   $changeLogObj->serviceId    = $deviceType;
                   $changeLogObj->entityName   = $tagName;
                   $changeLogObj->tableDelChanges();
                }
            }            
            
            $rebuildResetResponseArr =  $objCustomTagMgmt->rebuildResetDeviceType($deviceType, $rebuildPhoneFiles, $resetPhone);

            if(!empty($rebuildResetResponseArr['rebuildResponse'])){
              if(!empty($rebuildResetResponseArr['rebuildResponse']['Error'])){
                  echo "$tagName deleted successfully <br />";
                  $changeLogObj->module     = "Delete Device Config CustomTags";
                  echo "<span style='color:red;'>".$rebuildResetResponseArr['rebuildResponse']['Error']."</span><br />";
              }if(!empty($rebuildResetResponseArr['rebuildResponse']['Success'])){
                  echo "$deviceType - Rebuild Successfully <br />";
              }
           }
           if(!empty($rebuildResetResponseArr['resetResponse'])){
              if(!empty($rebuildResetResponseArr['resetResponse']['Error'])){
                  echo "$tagName deleted successfully <br />";
                  $changeLogObj->module     = "Delete Device Config CustomTags";
                  echo "<span style='color:red;'>".$rebuildResetResponseArr['resetResponse']['Error']."<br /></span><br />";
              }if(!empty($rebuildResetResponseArr['resetResponse']['Success'])){
                  echo "$deviceType - Reset Successfully <br />";
              }
           }
    }
}  */

if($_POST['action'] == "resetRebuildCustomTagOnDeviceType"){
    
    $addErrorMsg        = "";
    $deviceType         = $_POST['deviceType'];
    
    if($rebuildResetDevice == "true"){
        $rebuildPhoneFiles  = $_POST['rebuildPhoneFiles'];
        $resetPhone         = $_POST['resetPhone'];
    }else{
        $rebuildPhoneFiles  = "true";
        $resetPhone         = "true";
    }
    
    echo $successMsg = "The following changes are complete:<br /><br />";
    if(isset($_SESSION['addCustomTagOnDeviceType']) && !empty($_SESSION['addCustomTagOnDeviceType'])){
            $customTagCompleteAddArr = array();            
            foreach($_SESSION['addCustomTagOnDeviceType'] as $row){
                $tagName        = $row['tagName'];
                $tagValue       = $row['tagValue'];
                $xmlAddResponse = $objCustomTagMgmt->addDeviceCustomTag($deviceType, $tagName, $tagValue);
                $addErrorMsg    = $xmlAddResponse->command->summaryEnglish;                
                if($addErrorMsg!=""){
                    echo "<span style='color:red;'>".$addErrorMsg."<span><br />";
                }else{                    
                    echo "$tagName added successfully <br />";
                    $changeLogArr  = array(
                        'serviceId'   =>$_SESSION["sp"],
                        'deviceType'  => $deviceType,
                        'tagName'     => $tagName,
                        'tagValue'    => $tagValue
                    );    
                    $changeLogObj   = new ChangeLogUtility($deviceType, $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
                    $module         = "Add Device Config CustomTags";
                    $tableName      = "deviceConfigCustomTagsAddChanges";
                    $entityName     = $tagName;
                    $changeResponse = $changeLogObj->changeLogAddUtility($module, $entityName, $tableName, $changeLogArr);                    
                }
            }
    }
    
    if(isset($_SESSION['editCustomTagOnDeviceType']) && !empty($_SESSION['editCustomTagOnDeviceType'])){
            $customTagCompleteAddArr = array();
            $a            = 0;            
            $successMod   = "";
            $changeLogObj = new ChangeLogUtility($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
            foreach($_SESSION['editCustomTagOnDeviceType'] as $row){
                $tagName        = $row['tagName'];
                $tagValue       = $row['tagValue'];
                $tagOldValue    = $row['tagOldValue'];
                $xmlModResponse = $objCustomTagMgmt->updateDeviceCustomTag($deviceType, $tagName, $tagValue);
                if(empty($xmlModResponse["Error"])){
                    $changeLogArr       = array(); 
                    
                    $successMod         = "1";
                    echo $addSuccesMsg  = "$tagName' s value modified to $tagValue <br />";
                    
                    $changeLogObj->module            = "Modify Device Config CustomTags";
                    $changeLogObj->entityName        = $tagName;
                    $changeLogObj->changeLog();
                    $changeLogObj->modTableName      = "deviceConfigCustomTagsModChanges";
                    $changeLogObj->serviceId         = $deviceType;
                    $changeLogArr                    = $changeLogObj->createChangesArray("tagValue", $tagOldValue, $tagValue);
                    $changeLogObj->tableModChanges();                    
                    $a = $a+1;
                }else{
                    echo "<span style='color:red;'>".$xmlModResponse["Error"]."</span><br />";
                }
            }
    }
    
    
        if(isset($_POST["customTagDel"]) && !empty($_POST["customTagDel"])){
            
        
            $customTagCompleteAddArr = array();
            $a            = 0;
            $successMod   = "";
            $changeLogObj = new ChangeLogUtility($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
            foreach($_POST["customTagDel"] as $value) {
              $deviceType =  $_POST['deviceType'];              
                $tagName  = "%".$value."%";
                $responseDelResponse = $objCustomTagMgmt->groupAccessDeviceDeleteCustomTag($deviceType, $tagName);
              
                $delErrorMsg         = $responseDelResponse->command->summaryEnglish;
                if($delErrorMsg!=""){
                    echo "<span style='color:red;'>".$delErrorMsg."</span><br />";
                }else{
                    echo "$tagName deleted successfully <br />";
                    $changeLogObj->module     = "Delete Device Config CustomTags";
                    $changeLogObj->entityName = $tagName;
                    $changeLogObj->changeLog();
                    
                    $changeLogObj->modTableName = "deviceConfigCustomTagsModChanges";
                    $changeLogObj->serviceId    = $deviceType;
                    $changeLogObj->entityName   = $tagName;
                    $changeLogObj->tableDelChanges();
                }
            } 
    }
    
    
    $rebuildResetResponseArr =  $objCustomTagMgmt->rebuildResetDeviceType($deviceType, $rebuildPhoneFiles, $resetPhone);
    if(!empty($rebuildResetResponseArr['rebuildResponse'])){
        if(!empty($rebuildResetResponseArr['rebuildResponse']['Error'])){
            echo "<span style='color:red;'>".$rebuildResetResponseArr['rebuildResponse']['Error']."</span><br />";
        }if(!empty($rebuildResetResponseArr['rebuildResponse']['Success'])){
            echo "$deviceType Rebuild Successfully <br />";
        }
    }
    
    if(!empty($rebuildResetResponseArr['resetResponse'])){
        if(!empty($rebuildResetResponseArr['resetResponse']['Error'])){
            echo "<span style='color:red;'>".$rebuildResetResponseArr['resetResponse']['Error']."</span><br />";
        }if(!empty($rebuildResetResponseArr['resetResponse']['Success'])){
            echo "$deviceType Reset Successfully <br />";
        }
    }
}

if($_POST['funcType'] =="checkEffectedDecice"){
    $count       = 0;
    foreach($_POST['checkedCustomTag'] as $key=>$value) {   
       // print_r($_POST['checkedCustomTag']); exit ;
        $deviceType     = $value['deviceType'];
        $count          = $count+1;
    }
    $dvceListResponse   = $objDvceOpratn->GroupAccessDeviceGetListRequest($_SESSION["sp"], $_SESSION["groupId"], $deviceType);
    $noOfEffectedDevice = count($dvceListResponse['Success']);
    if($count==1){
        echo "Deleting Custom Tag - $noOfEffectedDevice devices will be affected.";
    }else{
        echo "Deleting Custom Tag(s) - $noOfEffectedDevice devices will be affected.";
    }
    
    
}
?>  
    
   