<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
//require_once ("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
//require_once ("/var/www/lib/broadsoft/adminPortal/getDevices.php");
require_once ("/var/www/html/Express/util/getAuthorizeDeviceTypes.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
//$sipGatewayLookup = new DBLookup ( $db, "devices", "deviceName", "sipGateway" );
include("updateDeviceConfigCustomTag.php");

foreach ($systemTableAllData as $key => $value){
    if($value["enabled"] == "true"){
        $deviceTypeAuthorizeDevice[] = $key;
    }
}

asort($deviceTypeAuthorizeDevice);
//$combineArray = array_merge($deviceTypesAnalogGateways, $deviceTypesDigitalVoIP);
//$deviceTypeAuthorizeDevice = array_merge($deviceTypesAnalogGateways, $deviceTypesDigitalVoIP, );

//$groupId = isset ( $_SESSION ["groupId"] ) ? $_SESSION ["groupId"] : "";
//$serviceProvider = isset ( $_SESSION ["sp"] ) ? $_SESSION ["sp"] : "";
//echo $groupId ;echo"<br/>";
//echo $serviceProvider ;exit;

$_SESSION ["DeviceCustomTagAddNames"] = array (
		"deviceType"            => "Device Type",
		"tagName"               => "Device Config. Custom Tag Name",
		"tagValue"              => "Device Config. Custom Tag Value",
        "rebuildPhoneFiles"     => "Rebuild Device Files",
        'customTagDel'          => "Device Config. Custom Tags Deleted",
        "resetPhone"            => "Reset Devices"
);

?>
 
<!-- Add css path for cutom tag config 

<link rel="stylesheet" type="text/css" href="Devices/DeviceConfCustomTags/css/deviceCustomConfig.css"> -->
<!-- end -->

<!-- Add js path for cutom tag config  -->
<script src="/Express/js/bootstrap/bootstrap.min.js"></script>
<script src="Devices/DeviceConfCustomTags/js/device_config_custom_tag.js"></script>



<div class="selectContainer">
	<div class="row twoColWidth">
    	 <form action="Devices/DeviceConfCustomTags/printCSVCustomTag.php" method="POST" name="downloadCSVForm" id="downloadCSVForm" >	
    	 	<input type="hidden" name="selectedUserForDownload" id="selectedUserForDownload" value="" />
            <input type="hidden" name="deviceTypeForDownloadCSV" id="deviceTypeForDownloadCSV" value="" />
    	</form>
    	 	
	 	
	 	<form id="device_Config_Custom_Tags" name="add_device_config_custom_tag">
	 		<input type="hidden" name="action" value="resetRebuildCustomTagOnDeviceType" />
    		<div class=" centerDesc centerDescNew" style="display: block">
    			<div class="form-group">
        			<label for="department" class="labelText" style="margin-left: 8px;">Device Type</label>
					<div class="dropdown-wrap">
						<select name="deviceType" id="select_device_Config_Custom_Tags" class="" onchange="deviceConfigCustomTag('')" style="width:100% !important">
                                <option value="None" selected>None</option>
                                <?php 
                                foreach ($deviceTypeAuthorizeDevice as $deviceTypeIndex => $deviceTypeValue){
                                    echo "<option value = '".$deviceTypeValue."'>".$deviceTypeValue."</option>";
                                }
                                ?>
            			</select>
					</div>
					<div id="deviceCustomTagCongLoader" class="loading"><img src="/Express/images/ajax-loader.gif"></div>
				</div>
			</div>
			
		
		
		
		<!-- new table show div  -->
		
		 <div id="customConfigTagDeviceNameDiv">
		 	<div class="row twoColWidth" id="" style="display:block;margin-top: 70px;">
		 		<div class="col-md-12">
		 			<div class="divCFAforStyle">
        		      <!-- button add / delete work -->
        		 		<div class="row buttonBoxAlignment">
        					<div class="col-md-4"></div>
        						
        					<div class="col-md-6" style="vertical-align: center;">
        						<input type="button" name="" id="addDeviceConfigCustomTagBtn" value="Add Group Custom Tag" class="cancelButton">
        					</div>
        				
        					<div class="col-md-2">
        					    <input type="button" id="deleteDeviceConfigCustomTag" class="deleteBtn deleteSmallButton deleteDeviceConfigCustomTag" value="Delete" >
        					</div>
        				</div>
        		       <!-- end button add / delete work -->
                                <!-- </div>	-->
                                
                                <div class="customTagListData">
                                </div>
                                
        		 	</div>
        		 	<div id="mainOuterDivForCustomTag">
        		 			<!-- reset rebuild div start -->
        		 			<div id="customTagDownloadRebuilResetDiv">
        		 				<!-- download csv -->
                                                <div class="col-md-2" name="downloadCSV" id="downloadCSV_Basic" value="" style="float:right;width:auto !important;margin: 0 auto;padding-top: 3px;">
                            				<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" /><br><span>Download<br>CSV</span>
                            			</div>
            		 				 <!-- end download csv -->
            		 				 <?php 
                                        $cssStyle = "style='display: none;'";
                                        if($rebuildResetDevice == "true"){                                            
                                            $cssStyle = "style='display: block; margin-top:20px;'";
                                        }
                                     ?>
                                     <!-- css style div start -->
                                     <div id="" <?php echo $cssStyle; ?> ><!-- css style div start -->                                     	
                                     	<!-- rebuild and reset checkbox -->
                                      <div>
                               				<input type="checkbox" name="rebuildPhoneFiles" id="rebuildPhoneFiles" value="true" onclick=""  class=""><label for="rebuildPhoneFiles"><span></span></label>
                                 			<label class="labelText">Rebuild Device Files</label>
                        			  </div> 
                        			 
                        			 <div>
                            				<input type="checkbox" name="resetPhone" id="resetPhone" value="true" onclick="" class=""><label for="resetPhone"><span></span></label>
                            				<label class="labelText">Reset Devices</label>
                        			 </div> 
                                       <!-- end rebuild and reset checkbox -->
                                     </div><!-- end css style div -->
            		 			      <!-- end css style div -->
            		 			  
        		 				</div><!-- end reset rebuild div -->
        		 			
        		 		    <!-- show effective device message -->
        		 			<div class="effectedDeviceLable" style="clear: both; color: red; font-weight: 700; text-align: center;">&nbsp;</div>
        		 			<!-- end show effective device message -->
        		 		
        		 			<!-- submit button -->
        		 			<div class="row buttonBoxAlignment">
        						<div class="col-md-4"></div>
        						
        						<div class="col-md-6" style="vertical-align: center;">
                        			<input type="button" name="" class="subButton" id="rebuildResetCustomTagsOnDeviceType" value="Submit">
                        		</div>
        				
            					<div class="col-md-2"></div>
        					</div>
        		 			<!-- end submit button div -->
        		 		</div><!-- hide record rebuild and reset if no data available close -->		
        		 	<div class="customTagListMessage" style="clear: both; text-align: center;">&nbsp;</div>
		 		</div><!-- div col-md-12 end  -->
		 	</div>
		 </div><!-- customConfigTagDeviceNameDiv close -->
		
		<!-- end new function div show -->
	  </form>
	</div>
</div>	
		
		
		
		
<!-- dialog box for add device conf. cutom tag -->
<div id="device_custom_tag_dialog" style="display:none;"> 
	<form id="add_device_config_custom_tag" name="add_device_config_custom_tag" style="width:860px; margin:0 auto !important">                                      
		<input type="hidden" name="addAction" value="addGroupLevelDeviceCustomeTag">
		<input type="hidden" name="deviceType" id="deviceTypeCustomTag" value="">
 	 		
        <div  class="row" style="text-align: center">
        	<div class="col-md-12">
        		<div class="col-md-3"></div>
         	 	<div class="col-md-6">
         			<label for="" class="labelText">Tag Name :</label>
         			<div class="form-group">
         				<input type="text" name="tagName" id="tagName" value="" >
         			</div>
         		</div>
        	</div>
        </div>
        <div class="row"style="text-align: center">
        	<div class="col-md-12">
        		<div class="col-md-3"></div>
        		<div class="col-md-3">
         			<label for="department" class="labelText" style="margin-left: 8px;">Tag Value :</label>
         			<div class="form-group">
         				<input type="text" name="tagValue" id="tagValueId" value="" />
         			</div>
         	 	</div>
        	</div>
        </div>
   </form>
</div>
<!-- End code -->
<div id="createConfirmCustomeTag" class="dialogClass"></div>
<div id="deleteDevicecustomTagDialog" class="dialogClass"></div>


<div id="devicecustomTagDialog" class="dialogClass ui-dialog-content ui-widget-content" style="width: auto; min-height: 0px; max-height: none; height: auto;"></div>
<div id="dialogSearchScanDevice" class="dialogClass"></div> 

<script>
   /* code added for ie specific issue */
    $(document).on({
    mouseover: function () {
      $(this).closest('tr#customUpdate').find('.imageHoverAlign').attr('src', '/Express/images/icons/eyes_down.png');
    },
    mouseout: function () {
         $(this).closest('tr#customUpdate').find('.imageHoverAlign').attr('src', '/Express/images/icons/eyes_rest.png');
    }
}, "tr#customUpdate"); 
</script>

