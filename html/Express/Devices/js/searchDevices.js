
$(function(){
		$("#tabs").tabs();
		$("#loadingBar").hide();
		$("#downloadCSVBtn").hide();
		$("#deleteBtn").hide();
		$("#selectCheckDiv").hide();
		
		var sourceSwapIcons = function() {
			var thisId = $(this).find("img");	
	        var newSource = thisId.data('alt-src');
	        thisId.data('alt-src', thisId.attr('src'));
	        thisId.attr('src', newSource);
	    }
	    
		$('#downloadCSV, #deleteDevice, #addDevice').hover(sourceSwapIcons, sourceSwapIcons);
		
		var activeImageSwap = function()
		{	previousActiveMenu	= "modDevice";
			 currentClickedDiv = "userMod";       
		  
				//active
		  		if(previousActiveMenu != "")
				{ 
		  			 $("#modDevice").removeClass("activeNav");
		  		 	$(".navMenu").removeClass("activeNav");
					var $thisPrev = $("#modDevice").find('.ImgHoverIcon');
			        var newSource = $thisPrev.data('alt-src');
			        $thisPrev.data('alt-src', $thisPrev.attr('src'));
			        $thisPrev.attr('src', newSource);
			       
				}
				// inactive tab
				if(currentClickedDiv != ""){
					$("#userMod").addClass("activeNav");
					var $thisPrev = $("#userMod").find('.ImgHoverIcon');
			        var newSource = $thisPrev.data('alt-src');
			        $thisPrev.data('alt-src', $thisPrev.attr('src'));
			        $thisPrev.attr('src', newSource);
					 previousActiveMenu = currentClickedDiv;
					
		    	}
		   
		}
		
		
		$('#searchDeviceVal').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) {
				e.preventDefault();
				//return false;\
				var searchVal = $("#searchDeviceVal").val();
				if(searchVal != "")
				{
					$('#searchDevices').trigger('click');
				}
			  }
			});
		
		$('#searchDevices').click(function(){
			$("#downloadCSVBtn").hide();
			$("#deleteBtn").hide();
				$("#warningErrorMsg").html("");
				var searchVal = $("#searchDeviceVal").val();
				//if(searchVal != "")
				//{
					$("#loadingBar").show();
					$("#deviceListDiv").hide();
					var formData = $('form#searchDeviceOption').serializeArray();
					$.ajax({
						type: "POST",
						url:"Devices/searchDevice.php",
						data: formData,
						success: function(result)
						{
							if(foundServerConErrorOnProcess(result, "")) {
		    					return false;
		                  	}
							if(result.indexOf("please narrow your search") >= 0){
								$("#warningErrorMsg").html(result);
							}else if(result.indexOf("ErrorMessageFromOci") >= 0){
								res = result.split("||");
								alert(res[1]);
							}else{
								//$("#deviceTable").show();
								$("#warningErrorMsg").html("");
								$("#deviceListDiv").html(result);
								$("#deviceTable").tablesorter();
	    						tableRowCount = $('#deviceTable tr').length;
	        					if(tableRowCount == 1)
	            				{
	        						$("#deleteDevice").hide();
	        						$(".checkAllDevices").hide();
	            					$("#deviceTable").append("<tr><td colspan ='7'> No Devices Found </td></tr>");
	            				}else{
	            					$("#deleteDevice").show();
	            					$("#downloadCSVBtn").show();
	            				}
	        					
	        					$("#deviceListDiv").show();	
	        					$("#warningError").hide();
							}
							 
							$("#loadingBar").hide();
							$("#selectCheckDiv").show();
						}
				

					});
				//}
		});

		$("#loadingOnChange").hide();
		
		$(document).on("click", ".primaryUserModLink", function()
				{
					var userId = $(this).attr("id");
					var fname = $(this).attr("data-firstname");
					var lname = $(this).attr("data-lastname");
					
					var extension = $(this).attr("data-extension");
					var phone = $(this).attr("data-phone");
					if($.trim(phone) != ""){
						userIdValue1 = phone+"x"+extension;
					}else{
						userIdValue1 = userId;
					}
					var userIdValue = userIdValue1+" - "+lname+", "+fname;
					
					$.ajax({
							type: "POST",
							url: "userMod/userMod.php",
							data: { searchVal: userId, userFname : fname, userLname : lname },
							success: function(result)
							{

								$("#mainBody").html(result);
								$("#searchVal").val(userIdValue);

								$("#deviceSearchBanner").html("User Modify");
								$('#helpUrl').attr('data-module', "userMod");
								
								$("#go").trigger("click");
								activeImageSwap();
							}

						});
		});
		
		$(document).on("click", ".scaUserModLink", function(event){
			console.log(event.target);
			var dataId = event.target.id;
			var dataType=$(this).attr("data-type");
			$("#tabs").tabs( "option", "active", 1);
			$("#scaSearchDeviceVal").val(dataId+' ('+dataType+')');
			$('#selectScaSearchDeviceVal').trigger('click');
			$("#newDevice").hide();
			$("#deviceTabName").html("SCA-Only Devices");
		});
		
		 
		// Download Data as Csv file
		
		$('#downloadDeviceCSV').click(function() {
			  var titles = [];
			  var data = [];
			  $('#deviceTable th').each(function() {	//table id here
			    titles.push($(this).text());
			  });

			
			  $('#deviceTable td').each(function() {	//table id here
			    data.push($(this).text());
			  });
			  
			  
			  var CSVString = prepCSVRow(titles, titles.length, '');
			  CSVString = prepCSVRow(data, titles.length, CSVString);

			  var blob = new Blob(["\ufeff", CSVString]);
			  var fileName = "DeviceSearchResult.csv";
			  if (navigator.msSaveBlob) { // IE 10+
		        	navigator.msSaveBlob(blob, fileName);
		      }else{
		    	  var downloadLink = document.createElement("a");
				  
				  var url = URL.createObjectURL(blob);
				  downloadLink.href = url;
				  downloadLink.download = fileName;

				  
				  document.body.appendChild(downloadLink);
				  downloadLink.click();
				  document.body.removeChild(downloadLink);
		      }
			 
			  
			});

			
			function prepCSVRow(arr, columnCount, initial) {
			  var row = '';
			  var delimeter = ',';
			  var newLine = '\r\n';

			  function splitArray(_arr, _count) {
			    var splitted = [];
			    var result = [];
			    _arr.forEach(function(item, idx) {
			      if ((idx + 1) % _count === 0) {
			        splitted.push(item);
			        result.push(splitted);
			        splitted = [];
			      } else {
			        splitted.push(item);
			      }
			    });
			    return result;
			  }
			  var plainArr = splitArray(arr, columnCount);
			 
			  plainArr.forEach(function(arrItem) {
			    arrItem.forEach(function(item, idx) {
			    	if(idx != 0){	
			    		row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
			    	}
			    });
			    row += newLine;
			  });
			  return initial + row;
			}
			 
			// End 
	
	
	
	$(document).on("click", "#selectAllChk", function(){

		if ($('#selectAllChk').is(':checked')) {
			$(".checkDeviceListBox").each(function () {
		        var id = $(this).attr("id");
		        $("#"+id).prop('checked', true);
		    });
			$("#deleteBtn").show();
		}else{
			$(".checkDeviceListBox").each(function () {
		        var id = $(this).attr("id");
		        $("#"+id).prop('checked', false);
		    });
			$("#deleteBtn").hide();
		}
	});
	
	var deleteDevice = function(){
		$("#loading2").show();
		$("#dialogSearchScanDevice").dialog("option", "title", "Request Complete");
		$("#dialogSearchScanDevice").html('Deleting devices...<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');		    	
		$("#dialogSearchScanDevice").dialog("open");
		$(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
		$(":button:contains('Cancel')").attr("disabled", "disabled").addClass("ui-state-disabled cancelButton");
		$(":button:contains('Delete')").addClass("deleteButton");
		$(":button:contains('Cancel')").addClass("cancelButton");
		$(":button:contains('Done')").addClass("subButton");
		var deleteDeviceArray   = [];
                var devicedataArray     = [];
		$(".checkDeviceListBox").each(function () {
	        var id = $(this).attr("id");
                var devicedatatmp       = "";
	        if ($('#'+id).is(':checked')) {
	        	var deviceName = $('#'+id).attr('name');
	        	deleteDeviceArray.push(deviceName);
                        devicedatatmp = $('#'+id).attr('data-devicedata');
                        var devicedata = deviceName+"---"+devicedatatmp;
                        devicedataArray.push(devicedata);
	        }
	        });
		
		if(deleteDeviceArray.length > 0){
			pendingProcess.push("Delete Device");
			$.ajax({
				type: "POST",
				url:"Devices/deleteDevice.php",
				data: {deviceArray : deleteDeviceArray, deviceDataArray : devicedataArray},
				success: function(result)
				{
					if(foundServerConErrorOnProcess(result, "Delete Device")) {
						return false;
	                }
					var obj = jQuery.parseJSON(result);
					if(obj.Error){
						$("#dialogSearchScanDevice").html(obj.Error);    	
						$(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
						$(":button:contains('Cancel')").removeAttr("disabled").removeClass("ui-state-disabled"); 
					}else{
						$("#dialogSearchScanDevice").html(obj.Success);    	
						buttonsShowHide('Delete', 'hide');
						buttonsShowHide('Cancel', 'hide');
						//buttonsShowHide('Done', 'show');
						$(":button:contains('Done')").addClass("subButton").show();
						$("#loading2").hide();
						$('#searchDevices').trigger('click');
					}
				}
			});
		}
	}
	
	
	
	var buttonsShowHide = function(b,e){
		if(e == "show"){
			$(".ui-dialog-buttonpane button:contains("+b+")").button().show();
		}else{
		 	$(".ui-dialog-buttonpane button:contains("+b+")").button().hide();
		}
	};
        
        if($('#dialogSearchScanDevice').is(':ui-dialog')) {
        
        }else{
            $("#dialogSearchScanDevice").dialog({
                    autoOpen: false,
                    width: 800,
                    modal: true,
                    position: { my: "top", at: "top" },
                    resizable: false,
                    closeOnEscape: false,
                    buttons: {
                            "Cancel": function() {
                                    $(this).dialog("close");
                                    //$("#loading2").hide();
                            },
                            "Delete": function(){
                                    deleteDevice();
                            },
                            "Done": function(){
                                    $('#searchDevices').trigger("click");
                                    $(this).dialog("close");
                            },
                    },
            open: function() {
                    //buttonsShowHide('Cancel', 'show');
                    //buttonsShowHide('Delete', 'show');
                    $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019
                    setDialogDayNightMode($(this));
                    $(":button:contains('Delete')").addClass("deleteButton").show();
                    $(":button:contains('Cancel')").addClass("cancelButton").show();
                    buttonsShowHide('Done', 'hide');
                    $(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
                    $(":button:contains('Cancel')").removeAttr("disabled").removeClass("ui-state-disabled");
            }
            });	
        }
	
	$("#deleteDevice").click(function(){
		var num = $(".checkDeviceListBox:checked").length;
                
                if($('#dialogSearchScanDevice').is(':ui-dialog')) {
                    
                }else{                     
                     $("#dialogSearchScanDevice").dialog({
                            autoOpen: false,
                            width: 800,
                            modal: true,
                            position: { my: "top", at: "top" },
                            resizable: false,
                            closeOnEscape: false,
                            buttons: {
                                    "Cancel": function() {
                                            $(this).dialog("close");
                                            //$("#loading2").hide();
                                    },
                                    "Delete": function(){
                                            deleteDevice();
                                    },
                                    "Done": function(){
                                            $('#searchDevices').trigger("click");
                                            $(this).dialog("close");
                                    },
                            },
                    open: function() {
                            //buttonsShowHide('Cancel', 'show');
                            //buttonsShowHide('Delete', 'show');
                            $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019
                            setDialogDayNightMode($(this));
                            $(":button:contains('Delete')").addClass("deleteButton").show();
                            $(":button:contains('Cancel')").addClass("cancelButton").show();
                            buttonsShowHide('Done', 'hide');
                            $(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
                            $(":button:contains('Cancel')").removeAttr("disabled").removeClass("ui-state-disabled");
                    }
                    });	
                }
                
		$("#dialogSearchScanDevice").dialog("option", "title", "Request Complete");
		$("#dialogSearchScanDevice").html('All selected '+ num +' devices will be removed');		    	
		$("#dialogSearchScanDevice").dialog("open");
		
	});
	
	$(document).on("change", ".checkDeviceListBox", function(){
		var num = $(".checkDeviceListBox:checked").length;
		if(num > 0){
			$( "#selectAllChk" ).prop( "checked", false );
			$("#deleteBtn").show();
		}else{
			
			$("#deleteBtn").hide();
			//$("#selectAllChk").prop('checked', false);
		}
	});
	
});
function tabClicked(title)
{
	$("#deviceTabName").html(title);
};