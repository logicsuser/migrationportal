<?php
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
unset ( $_SESSION ["allDeviceDetail"] );
require_once ("/var/www/html/Express/scaDevices/js/scaOnlyDevice_js.php");
?>
<script src="Devices/js/searchDevices.js"></script>

<style>
.selectInputRadio input {
	width: auto;
}
table#deviceTable tbody tr{
border: 1px solid #D8DADA !important;
    display: flex;
    margin-top: 2px;
    border-left: 1px;
    border-right: 1px;

}
table#deviceTable tbody tr td {
    border-right: none;
}

table.tablesorter tbody {
    border: none;
}
table#deviceTable{
    border-right:1px solid #ddd;
    border-bottom:1px solid #ddd;
}
/*
table#deviceTable thead tr th {
     padding: 6px !important;  
}
*/
.selectInputRadio {
	margin-right: 15px;
}

.dataTables_wrapper .dataTables_paginate {
			float: right;
			text-align: right;
			padding-top: 0.25em;
                            color: #6ea0dc !important;
		}

</style>

<h2 class="adminUserText" id="deviceSearchBanner">Device Inventory</h2>
<div class="selectContainer padding-top-zero">
	<div class="">
		<div class="devicesMainDiv">
			<div id="tabs">
			<div class="divBeforeUl">
				<ul class="deviceSearchTab">
					<?php if(isset($_SESSION["groupId"])) {?>
					<li><a href="#main_device_search" id="device_search_button"
						class="tabs" onclick="tabClicked('Device Search')">Device Search</a></li>
					<?php } ?>
					<?php 
					if($_SESSION["sp"] !="" && ( ! isset($_SESSION["groupId"]) || $_SESSION["groupId"]=="") ){ ?>
				    	<li><a href="#quick_device_search" id="quick_device_search_button"
						class="tabs" onclick="tabClicked('Device Search')">Device Search</a></li>
				    <?php } ?>
					<?php if(isset($_SESSION["groupId"]) && isset($_SESSION["permissions"]["scaOnlyDevice"]) && $_SESSION["permissions"]["scaOnlyDevice"]== "1"){?>
					<li><a href="#SCA_Only_Devices" id="scaOnly_devices" class="tabs" onclick="tabClicked('SCA-Only Devices')">SCA-Only
							Devices</a></li> <?php } ?>
                    <?php if((isset($_SESSION["groupId"]) && $_SESSION["groupId"]!="" && isset($_SESSION["permissions"]["deviceConfigCustomTags"]) && $_SESSION["permissions"]["deviceConfigCustomTags"]== "1") ){?>		
					 <li><a href="#Device_Configuration_Custom_Tag_search" id="Device_Configuration_Custom_Tags"
				class="tabs" onclick="tabClicked('Device Configuration Custom Tags')">Device Config. Custom Tags</a></li>	<?php } ?>
				<?php 
				if($_SESSION["sp"] !="" && $_SESSION["groupId"]==""){ ?>
				    <li><a href="#device_type_search" id="device_type_search_tab"
				class="tabs" onclick="tabClicked('Device Types')">Device Types</a></li>
				<?php }
				?>
				
				</ul>
		
				<h2 class="adminUserText" id="deviceTabName">Device Search</h2>
				 
			</div>
				<h2 class="adminUserText" id=""></h2>
				<!-- <div style="font-size: 12px; width: 100%; text-align: center">&nbsp;</div> -->
				<!-- Site Report -->
				<?php if(isset($_SESSION["groupId"])) { ?>
				<div id="main_device_search" class="userDataClass">
					<form name="searchDeviceOption" id="searchDeviceOption" class="">
						<div class="searchDeviceOptionDiv">
							<div class="search_device">
					
									<?php
									$checkedEnt = "";
									$checkedGroup = isset ( $_SESSION ["groupId"] ) ? "checked" : "";
									if (! isset ( $_SESSION ["groupId"] ) && $_SESSION["superUser"] == "1") {
										$checkedEnt = ! isset ( $_SESSION ["groupId"] ) ? "checked" : "";
										?>
							<div class="row">
								<div class="form-group" style="text-align:center">
									<label class="labelText" for="limitSearchTo" style="margin-right: 20px;">Search In :</label>
									<input type="radio" class="searchlevel" name="searchlevel" id="searchlevelSys" value="system" style="width: 5%;"><label for="searchlevelSys"><span></span></label>
									<label class="labelText" for="deviceClassVoIP">System</label>
									<input type="radio" class="searchlevel" name="searchlevel" id="searchlevelEnt" value="enterprise" class="searchlevelIn"
										<?php echo $checkedEnt; ?> style="width: 5%;"><label for="searchlevelEnt"><span></span></label>
										<label class="labelText" for="deviceClassAnalogGateway">Enterprise</label>
								</div>
							</div>
								 <?php } ?>	
									<div class="row">
									<div class="col-md-6">
									
										<input type="radio" name="searchlevel" class="searchlevelIn"
												value="group" style="display: none"
												<?php echo $checkedGroup; ?> style="width:5%;" />
										
										<div class="form-group">
											<label class="labelText" for="limitSearchTo">Limit Search to:</label><br />
											<div class="dropdown-wrap">   
											<select name="searchCriteria" id="searchCriteria">
												<option value="none">None</option>
												<option value="deviceName">Device Name</option>
												<option value="deviceType">Device Type</option>
												<option value="macAddress">MAC Address</option>
												<option value="ipAddress">IP Address</option>
												<option value="scaOnlyDevice">SCA-Only Device</option> 
											</select>
									</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group" id="searchDeviceDiv">
											<label class="labelText">Search Devices:</label>
											<span class="required">*</span><br /> <input type="text"
												name="searchDeviceVal" id="searchDeviceVal" size="35" class="magnify">
												<div style="text-align: justify; color: red"
												id="warningErrorMsg"></div>
											<div class="col span_12" id="loadingOnChange">
												<img src="/Express/images/ajax-loader.gif">
											</div>
										</div>
									</div>
								
								</div>
								
								<div class="row">
									<div class="col-md-12">
										<!--search result-->
										<div class="form-group alignBtn">
												<input type="button" name="searchDevices" id="searchDevices" value="Search" class="subButton">
										</div>
									</div>
								</div>
							</div>

							 
							
						</div>
						
					</form>
					<!-- <div class="row" style="">
						<div class="form-group">
                        	<div id="downloadCSVBtn" style="float: right; display: block;">
                                 <div id="downloadDeviceCSV" style="float: right; display: block;">
                                    <div id="downloadCSV" style="display: block;">
                                	    <img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png">
                                		<br><span>Download<br>CSV</span>
                                    </div>
                                 </div>
                             </div>
                            </div>-->
                        
                        <!--New code found(delete button)-->
    					<!--	<div id="deleteBtn" style="float: right;">
        						<div style="float: right;" class="deleteUsers" id="deleteDevice">
        							<img src="images/icons/delete_selected_users_icon.png" 
        							data-alt-src="images/icons/delete_selected_users_icon_over.png">
        							<br><span>Delete<br>Selected<br>Users</span>
        						</div>
    						</div>
						</div>
					</div>-->
					<!-- add code by SKS -->
					<div class="row delDownIconDiv" >
							<!-- <div class="col-md-6"></div> -->
							<div class="col-md-6 csvFileDownloadAlign">
								 
		 							<!-- <div class="col-md-10"></div> -->
		 							<div class="col-md-1 csvFileDownloadAlign">
										<div id="downloadCSVBtn" class="csvFileDownloadAlign">
                                 			<div id="downloadDeviceCSV" class="csvFileDownloadAlign">
                                    			<div id="downloadCSV" style="display: block; margin-top: 10px;">
                                	    			<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png">
                                					<br><span>Download<br>CSV</span>
                                    			</div>
                                 			</div>
                             			</div>
									</div>
		
                                                <?php 
                                                    $devicesLogsArr = explode("_", $_SESSION["permissions"]["devices"]);
                                                    $devicesAddDelLogPermission  = $devicesLogsArr[2];                    
                                                    if($devicesAddDelLogPermission == "yes"){ ?>
							<div class="col-md-1" style="float: right; display: block;">
								<div id="deleteBtn">
        							<div style="float: right;margin-top: 32px;" class="deleteUsers" id="deleteDevice">
        								<img src="images/icons/delete_selected_users_icon.png" data-alt-src="images/icons/delete_selected_users_icon_over.png">
        								<br><span>Delete<br>Selected<br>Devices</span>
        							</div>
    							</div>
							</div>
                                                    <?php } ?>
						</div>
					</div>
					
					<!-- end  -->
 
					 
						<div class="form-group">
							<div class="loading" id="loadingBar" style="margin-top:0%;">
								<img src="/Express/images/ajax-loader.gif">
							</div>
							<div id="deviceListDiv"></div>
							
						</div>
				 
				</div>
		<?php } ?>
		
		<?php if( !isset($_SESSION["groupId"])) { ?>
		<div id="quick_device_search" class="quick_device_search">
        	<?php 
        	   require_once ("/var/www/html/Express/Devices/quickDeviceSearch/index.php");
        	?>
        </div>
		<?php } ?>
		
		<div id="SCA_Only_Devices" class="SCA_Only_Devices"main_device_search
			style="display: none;">
    		<?php
    		require_once ("/var/www/html/Express/scaDevices/index.php");
    		?>
		</div>
                                
        <!-- Device Config Custom Tags -->	
        <?php 
            if((isset($_SESSION["groupId"]) && $_SESSION["groupId"]!="" && isset($_SESSION["permissions"]["deviceConfigCustomTags"]) && $_SESSION["permissions"]["deviceConfigCustomTags"]== "1") ){
        ?>
            <div id="Device_Configuration_Custom_Tag_search">	
            <?php
                    require_once ("/var/www/html/Express/Devices/DeviceConfCustomTags/deviceConfigCustomTags.php");
            ?>
            </div>	
        <?php } ?>
        <?php 
        if($_SESSION["sp"] != "" && $_SESSION["groupId"] == ""){ ?>
            <div id="device_type_search" class="device_type_search">
            <?php
            require_once ("/var/www/html/Express/Devices/deviceTypeManagement/deviceTypeIndex.php");
            ?>
        </div>
        <?php }
        ?>
        
        
    <!-- end  -->
    </div>


		</div>
	</div>
</div>
<div id="dialogSearchScanDevice" class="dialogClass"></div>

