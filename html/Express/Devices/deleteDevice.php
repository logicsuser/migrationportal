<?php
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
include("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");  //Code added @28 May 2018

/* $serviceProviderId = $_SESSION["sp"];
$groupId = $_SESSION["groupId"];*/

$deleteResponse = array();
$deviceObj = new DeviceOperations();

if(count($deviceObj) > 0){
	$r = 0;
	$d = 0;
	$successStr = "";
        
	foreach($_POST["deviceArray"] as $key=>$val){
            
                //Code added @ 18 Dec 2018                
                foreach($_POST["deviceDataArray"] as $recrd){
                    $tmpArr = explode("---", $recrd);
                    if($val==$tmpArr[0]){
                        $groupId = $tmpArr[1];
                        $serviceProviderId = $tmpArr[2];
                    }
                }
                if($serviceProviderId==""){
                    $serviceProviderId = $_SESSION["sp"];
                }    
                //End code
            
		$userList[$val] = $deviceObj->getAllUsersforDevice($serviceProviderId, $groupId, $val);                
		if(empty($userList[$val]["Error"])){
			if(count($userList[$val]["Success"]) == 0){
				
				$deleteResponse = $deviceObj->getDeleteDevice($val, $serviceProviderId, $groupId);
				if(empty($deleteResponse["Error"])){
					//$deleteResponse["Success"] = "Success";
                                        //Code added @ 28 May 2018  
                                        $deviceId = $val;
                                        $cLUObj = new ChangeLogUtility($deviceId, $groupId, $_SESSION["loggedInUserName"]);                                        
                                        $cLUObj->changeLogDeleteUtility("Delete Device", $deviceId, "deviceInventoryModChanges", "");
                                        //End Code
                                    $r++;
				}
			}else{
				$d++;
			}
		}
	}
	//echo $r."==".$d; die;
	if($r > 0){
		$successStr .= $r." Device Deleted Successfully<br/>";
	}
	if($d > 0){
		//$successStr .= "Devices not assigned to users shall be deleted. ".$d." Devices not deleted (devices being deleted must not be assigned to users)";
		$successStr .="Devices assigned to users cannot be deleted from this page: $d devices not deleted.";
	}
	
	$deleteResponse["Success"] = $successStr;
}
unset($_SESSION["allDeviceDetail"]);
echo json_encode($deleteResponse); 
?> 