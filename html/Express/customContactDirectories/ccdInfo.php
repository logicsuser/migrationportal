<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	if ($_POST["ccdValue"] == "newCcd")
	{
		$ccdName = "";
	}
	else
	{
		$ccdName = $_POST["ccdValue"];
	}

	require_once("/var/www/lib/broadsoft/adminPortal/getCustomContactDirectoryInfo.php");
?>
<script>
	$(function() {
		var ccdName = "<?php echo $ccdName; ?>";

		if (ccdName == "")
		{
			$("#endUserId").html("New Custom Contact Directory");
		}
		else
		{
			$("#endUserId").html(ccdName);
		}

		$("#dialogCCD").dialog({
			autoOpen: false,
			width: 800,
			modal: true,
			position: { my: "top", at: "top" },
			resizable: false,
			closeOnEscape: false,
			buttons: {
				"Complete": function() {
					var dataToSend = $("form#ccdEdit").serializeArray();
					$.ajax({
						type: "POST",
						url: "customContactDirectories/ccdDo.php",
						data: dataToSend,
						success: function(result) {
							$("#dialogCCD").dialog("option", "title", "Request Complete");
							$(".ui-dialog-titlebar-close", this.parentNode).hide();
							$(".ui-dialog-buttonpane", this.parentNode).hide();
							$("#dialogCCD").html(result);
							$("#dialogCCD").append(returnLink);
						}
					});
				},
				"Cancel": function() {
					$(this).dialog("close");
				}
			}
		});

		$("#ccdButtonEdit").click(function()
		{
			var dataToSend = $("form#ccdEdit").serializeArray();
			$.ajax({
				type: "POST",
				url: "customContactDirectories/checkData.php",
				data: dataToSend,
				success: function(result) {
					$("#dialogCCD").dialog("open");
					if (result.slice(0, 1) == "1")
					{
						$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
					}
					else
					{
						$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
					}
					result = result.slice(1);
					$("#dialogCCD").html(result);
				}
			});
		});

		$("#sortable1, #sortable2").sortable({
			placeholder: "ui-state-highlight",
			connectWith: ".connectedSortable",
			cursor: "crosshair",
			update: function(event, ui)
			{
				var ccdUsers = "";
				var order = $("#sortable2").sortable("toArray");
				for (i = 0; i < order.length; i++)
				{
					ccdUsers += order[i] + "; ";
				}
				$("#ccdUsers").val(ccdUsers);
			}
		}).disableSelection();
	});
</script>
<div class="subBanner"><?php echo $ccdName == "" ? "Create New Custom Contact Directory" : "Edit Custom Contact Directory"; ?></div>
<div class="vertSpacer">&nbsp;</div>

<form name="ccdEdit" id="ccdEdit">
	<div class="centerDesc">
		<span class="required">*</span> <label for="newCcdName">Directory Name</label>
		<span class="spacer">&nbsp;</span>
		<input type="text" name="newCcdName" id="newCcdName" size="40" maxlength="40" value="<?php echo $ccdName; ?>">
	</div>
	<div class="vertSpacer">&nbsp;</div>

	<div style="width:850px; margin: 0 auto;">
		<div class="leftScrollableDesc" style="text-align:center;">Available Users</div>
		<div class="rightScrollableDesc" style="text-align:center;">Current Users</div>

		<div class="leftScrollableDesc">
			<div class="scrollableList">
				<ul id="sortable1" class="connectedSortable">
					<?php
						if (isset($availableUsers))
						{
							for ($a = 0; $a < count($availableUsers); $a++)
							{
								$id = $availableUsers[$a]["id"] . ":" . $availableUsers[$a]["name"];
								echo "<li class=\"ui-state-default\" id=\"" . $id . "\">" . $availableUsers[$a]["name"] . "</li>";
							}
						}
					?>
				</ul>
			</div>
		</div>
		<div class="rightScrollableDesc">
			<div class="scrollableList">
				<ul id="sortable2" class="connectedSortable">
					<?php
						$ccdUsers = "";
						if (isset($users))
						{
							for ($a = 0; $a < count($users); $a++)
							{
								$id = $users[$a]["id"] . ":" . $users[$a]["name"];
								echo "<li class=\"ui-state-highlight\" id=\"" . $id . "\">" . $users[$a]["name"] . "</li>";
								$ccdUsers .= $id . "; ";
							}
						}
					?>
				</ul>
			</div>
		</div>
	</div>
	<div class="vertSpacer">&nbsp;</div>

	<div class="centerDesc">
		<input type="button" id="ccdButtonEdit" value="Confirm Settings">
		<input type="hidden" name="ccdUsers" id="ccdUsers" value="<?php echo $ccdUsers; ?>">
		<?php
			if ($ccdName != "")
			{
				?>
				<input type="hidden" name="editCcd" id="editCcd" value="<?php echo $ccdName; ?>">
				<?php
			}
		?>
	</div>
</form>
<div id="dialogCCD" class="dialogClass"></div>
