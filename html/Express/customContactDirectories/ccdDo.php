<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
	$expProvDB = new ExpressProvLogsDB();
	
	$ccdName = $_POST["newCcdName"];
	$newList = "";
	if ($_POST["ccdUsers"] !== "")
	{
		$exp = explode("; ", $_POST["ccdUsers"]);
		$ccdUsersList = "";
		foreach ($exp as $v)
		{
			if ($v !== "")
			{
				$exp2 = explode(":", $v);
				$newList .= $exp2[1] . "; ";
				$ccdUsersList .= "<entry><userId>" . $exp2[0] . "</userId></entry>";
			}
		}
	}
	if ($newList == "")
	{
		$newList = "None";
	}

	if (isset($_POST["editCcd"]))
	{
		//modify existing directory
		$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryModifyRequest17");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<name>" . htmlspecialchars($_POST["editCcd"]) . "</name>";
		$xmlinput .= "<newName>" . htmlspecialchars($ccdName) . "</newName>";

		if (isset($ccdUsersList))
		{
			$xmlinput .= "<entryList>" . $ccdUsersList . "</entryList>";
		}
		else
		{
			$xmlinput .= "<entryList xsi:nil=\"true\" />";
		}
	}
	else
	{
		//create new directory
		$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryAddRequest17");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<name>" . htmlspecialchars($ccdName) . "</name>";

		if (isset($ccdUsersList))
		{
			$xmlinput .= $ccdUsersList;
		}
	}
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	readError($xml);

	echo "The following custom contact directory has been " . (isset($_POST["editCcd"]) ? "modified" : "created") . ":<br><br>";

	$date = date("Y-m-d H:i:s");
	$query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
	$query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', '" . (isset($_POST["editCcd"]) ? "Edit" : "New") . " Custom Contact Directory', '" . $_SESSION["groupId"] . "', '" . addslashes($_POST["newCcdName"]) . "')";
	
	$sth = $expProvDB->expressProvLogsDb->query($query);
	$lastId = $expProvDB->expressProvLogsDb->lastInsertId();

	$insert = "INSERT into ccdModChanges (id, serviceId, field, newValue)";
	$insert .= " VALUES ('" . $lastId . "', '" . addslashes($_POST["newCcdName"]) . "', '" . $_SESSION["ccdNames"]["ccdUsers"] . "', '" . $newList . "')";
	$sth = $expProvDB->expressProvLogsDb->query($insert);

	echo "<ul>";
	foreach ($_POST as $key => $value)
	{
		if ($key !== "editCcd")
		{
			echo "<li>" . $_SESSION["ccdNames"][$key] . ": ";
			if ($value)
			{
				echo $value;
			}
			else
			{
				echo "None";
			}
			echo "</li>";
		}
	}
	echo "</ul>";
?>
