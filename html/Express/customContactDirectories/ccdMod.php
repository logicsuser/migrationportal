<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$_SESSION["ccdNames"] = array(
		"newCcdName" => "Directory Name",
		"ccdUsers" => "Users"
	);
?>
<script>
	$(function() {
		$("#module").html("> Modify Custom Contact Directories: ");
		$("#endUserId").html("");

		$("#ccdChoice").change(function()
		{
			$("#ccdData").html("");
			var id = $("#ccdChoice").val();

			if (id !== "")
			{
				$.ajax({
					type: "POST",
					url: "customContactDirectories/ccdInfo.php",
					data: { ccdValue: id },
					success: function(result) {
						$("#ccdData").html(result);
					}
				});
			}
			else
			{
				$("#endUserId").html("");
			}
		});
	});
</script>
<div class="subBanner">Choose Custom Contact Directory to Modify</div>
<div class="vertSpacer">&nbsp;</div>

<form method="POST" id="ccdMod">
	<div class="centerDesc">
		<label for="ccdChoice">Custom Contact Directory</label>
		<span class="spacer">&nbsp;</span>
		<div class="dropdown-wrap">   
		<select name="ccdChoice" id="ccdChoice">
			<option value=""></option>
			<?php
				require_once("/var/www/lib/broadsoft/adminPortal/getAllCustomContactDirectories.php");
				if (isset($directories))
				{
					foreach ($directories as $key => $value)
					{
						echo "<option value=\"" . $value . "\">" . $value . "</option>";
					}
				}
			?>
			<option value="newCcd">--Create New Directory--</option>
		</select>
			</div>
	</div>
</form>
<div id="ccdData"></div>
