<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$hidden = array("editCcd");
	$required = array("newCcdName");

	$data = $errorTableHeader;

	$error = 0;
	foreach ($_POST as $key => $value)
	{
		if (!in_array($key, $hidden)) //hidden form fields that don't change don't need to be checked or displayed
		{
			$bg = CHANGED;

			if (in_array($key, $required))
			{
				if ($value == "")
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["ccdNames"][$key] . " is a required field.";
				}
			}
			if ($key == "newCcdName")
			{
				if (preg_match("[\"]", $value))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["ccdNames"][$key] . " must not include double quotes.";
				}
				else
				{
					//check if directory already exists
					$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryGetRequest17");
					$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
					$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
					$xmlinput .= "<name>" . htmlspecialchars($value) . "</name>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

					if ($xml->command->userTable->row and ((isset($_POST["editCcd"]) and $value !== $_POST["editCcd"]) or !isset($_POST["editCcd"])))
					{
						$error = 1;
						$bg = INVALID;
						$value = $_SESSION["ccdNames"][$key] . " already exists.";
					}
				}
			}

			$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["ccdNames"][$key] . "</td><td class=\"errorTableRows\">";
			if ($value)
			{
				$data .= $value;
			}
			else
			{
				$data .= "None";
			}
			$data .= "</td></tr>";
		}
	}
	echo $error . $data;
?>
