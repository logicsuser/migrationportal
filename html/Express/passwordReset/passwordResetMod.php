<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();

	$_SESSION["passwordResetNames"] = array(
		"password1" => "Password",
		"password2" => "Retype Password"
	);

	require_once("/var/www/html/Express/header.php");
?>
<script>
	$(function() {
		$("#passwordResetDialog").dialog({
			autoOpen: false,
			width: 800,
			modal: true,
			position: { my: "top", at: "top" },
			resizable: false,
			closeOnEscape: false,
			buttons: {
				"Complete": function() {
					var dataToSend = $("#passwordReset").serialize();
					$.ajax({
						type: "POST",
						url: "passwordResetDo.php",
						data: dataToSend,
						success: function(result)
						{
							$("#mainBody").html(result);
							window.location.replace("../chooseGroup.php");
						}
					});
				},
				"Cancel": function() {
					$(this).dialog("close");
				}
			}
		});

		$("#changePassword").click(function()
		{
			var dataToSend = $("#passwordReset").serialize();
			$.ajax({
				type: "POST",
				url: "checkData.php",
				data: dataToSend,
				success: function(result)
				{
					
					if (result.slice(0, 1) == "1")
					{
						$("#passwordResetDialog").dialog("open");
						$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
						$(":button:contains('Cancel')").addClass("cancelButton").show();
					}
					else
					{
						successResetPassword();
						$(":button:contains('Cancel')").addClass("cancelButton").show();
						$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled").addClass("subButton");
					}
					$("#passwordResetDialog").html(result.slice(1));
				}
			});
		});

/* ex-448 Weird Behavior on Password Expiration*/
		var successResetPassword = function(){
 
			var dataToSend = $("#passwordReset").serialize();
			$.ajax({
				type: "POST",
				url: "passwordResetDo.php",
				data: dataToSend,
				success: function(result)
				{
					$("#mainBody").html(result);
					window.location.replace("../chooseGroup.php");
				}
			});
		}
	});
/*end */	
</script>

<div id="mainBody">
<div class="login-bg">
	<div class="fadeInUp" id="bodyForm">
		<img src="../images/icons/express_nuovo_logo_login2.png" />
		<div style="width:100%;text-align:center;">
		<form action="#" method="POST" name="passwordReset" id="passwordReset">
			<div id="passwordResetForm1">
				<div class="col-md-12 resetPassDiv"><small>Your password has expired and must be reset.<small></div>
				<div class="">
					<div class="form-group">
						<label for="password1" class="labelText">Password:<span class="required">*</span> </label>
						<input type="password" name="password1" id="password1" size="35" />
					</div>
				</div>
				<div class="">
					<div class="form-group">
						<label for="password2" class="labelText">Retype Password:<span class="required">*</span></label>
						<input type="password" name="password2" id="password2" size="35" />
					</div>
				</div>

				<div class="col-md-12 loginSubmitDiv">
					<input type="button" name="changePassword" id="changePassword" value="Reset Password">
				</div>
			</div>
		</form>
		</div>
	</div>
	<div id="passwordResetDialog" class="dialogClass"></div>
</div>
</div>

