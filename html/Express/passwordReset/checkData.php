<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();
        
        require_once ("/var/www/lib/broadsoft/adminPortal/PreventPasswordOperation.php");
        $objPPO = new PreventPasswordOperation($_SESSION["loggedInUserName"], md5($_POST["password1"]));
        
	$data = $errorTableHeader;
	$error = 0;
	foreach ($_POST as $key => $value)
	{
		if ($value)
		{
			$bg = CHANGED;
		}
		else
		{
			$bg = UNCHANGED;
		}

		$select = "SELECT u.userName from users u where u.password='" . md5($value) . "' and id=" . $_SESSION["adminId"];
		$lookup = $db->query($select);

		//check if both passwords are entered
		if ($value == "")
		{
			$error = 1;
			$bg = INVALID;
			$value = $_SESSION["passwordResetNames"][$key] . " is a required field.";
		}
		//check that new password is different from old
		/* else if ($lookup->fetch(MYSQL_ASSOC))
		{
			$error = 1;
			$bg = INVALID;
			$value = $_SESSION["passwordResetNames"][$key] . " must be different from your previous password.";
		} */
		//check for characters
		else if (!(preg_match("/[A-Z]+/", $value) and preg_match("/[a-z]+/", $value) and preg_match("/[0-9]+/", $value)))
		{
			$error = 1;
			$bg = INVALID;
			$value = $_SESSION["passwordResetNames"][$key] . " must include at least one uppercase letter, one lowercase letter, and one number.";
		}
//		else if (preg_match("/[&]+/", $value))
//		{
//			$error = 1;
//			$bg = INVALID;
//			$value = $_SESSION["passwordResetNames"][$key] . " must not include the following character: &";
//		}
		//check that both passwords are the same
		else if ($key == "password2" and $value !== $_POST["password1"])
		{
			$error = 1;
			$bg = INVALID;
			$value = "Passwords must be the same.";
		}
                else if($objPPO->passwordReuseValidationBasedOnPreventPassConfig())
                {
			$error = 1;
			$bg = INVALID;
			$value = $_SESSION["passwordResetNames"][$key] . " has been used already. Try another.";
		}
		else
		{
			$value = str_repeat("*", strlen($value)); //don't display password
		}

		$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["passwordResetNames"][$key] . "</td><td class=\"errorTableRows\">";
		$data .= $value;
		$data .= "</td></tr>";
	}

	$data .= "</table>";
	echo $error . $data;
?>
