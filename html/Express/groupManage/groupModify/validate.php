<?php

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupOperation.php");
require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupHelper.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");

$changeString = "";
$domainArray = array();
$newAssignDomain= array();
$newUnAssignDomain= array();
$newSecurityAssignDomain= array();
$newSecurityUnAssignDomain= array();
$assignDomainCompareOutput = array();
$assignSecurityDomainCompareOutput = array();


 
if(!empty($_POST['formdata'][0]['value'])){
	$tabName = $_POST['formdata'][0]['value'];
	$_SESSION["tabName"] = $tabName;
}else if(!empty($_POST['formdata'][1]['value'])){
	if($_POST['formdata'][1]['value'] == "modify"){
		unset($_SESSION["tabName"]);
	}
}
if(!empty($_POST['compare'])){
	
	$sp = $_SESSION['sp'];
	$groupId = $_POST['formdata'][2]['name'];
	
	//initialize class
	$groupValidate = new GroupOperation($sp, $groupId);
	
	$changeArray = array();
	//initialize session array for all tabs
	$groupValidate->setTabSessionArray();
	
	$msgArray = $groupValidate->groupErrorMsgArray();
	$postData = $groupValidate->formatPostData($_POST['formdata']);
	
	foreach($_POST['formdata'] as $key => $val) {
	    if($val['name'] == "assignNumbers") {
	        $postData['assignNumbers'] =  explode(";;", rtrim($val['value'] ,";;") ) ;
	    }
	}
	//echo"Data is"; print_r($postData); exit;
	//show change and validate data
	$tabArray = $groupValidate->tabsInputArray();        
        
	foreach($_SESSION['groupInfoData'] as $key=>$val){
                //print_r($postData);
                //exit;
		foreach($postData as $key1=>$val1){
			if($key == $key1){
			    if($val <> $val1){			    
					//if($val1 == ""){$val1 = "Null";}
					if(in_array($key, $tabArray['basicInfoArray'])){ $_SESSION['isBasicGroupInfoUpdated'] = true;}
                                        if(in_array($key, $tabArray['ocpSettingsArray'])){ $_SESSION['isOutgoingCallPlanUpdated'] = true;}
					if(in_array($key, $tabArray['routingProfileArray'])){ $_SESSION['isRoutingInfoUpdated'] = true;}
					//if(in_array($key, $assignDomainArray) && (count($assignDomainCompareOutput) > 0 || count($assignSecurityDomainCompareOutput) > 0)){ $_SESSION['isAssignDomainUpdated'] = true;}
					if(in_array($key, $tabArray['callProcessingArray'])){ $_SESSION['isCallProcessingUpdated'] = true;}
					if(in_array($key, $tabArray['voicePortalArray'])){ $_SESSION['isVoicePortalUpdated'] = true;}
					if(in_array($key1, $tabArray['extensionLengthArray'])){ $_SESSION['isExtensionLengthUpdated'] = true;}
					if(isset($postData['extensionLength']) && $_SESSION['groupInfoData']['extensionLength'] <> $postData['extensionLength']) 
					{
					    $_SESSION['isExtensionLengthUpdated'] = true; 
					}
					
					$changeArray[$key]['old'] = $val;
					$changeArray[$key]['new'] = $val1;
					$changeArray[$key]['key'] = $key1;
					$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
					
					if($key1 == "groupId" && ($val1 == "")){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = 'Group ID is required field';}
					if($key1 == "userLimit" && ($val1 == "")){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = 'User Limit is required field';}
					if($key1 == "defaultExtensionLength" && ($val1 == "")){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = 'Default Extension is required field';}
					if($key1 == "minExtensionLength" && ($val1 == "")){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = 'Min Extension is required field';}
					if($key1 == "maxExtensionLength" && ($val1 == "")){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = 'Max Extension is required field';}
					if($key1 == "extensionLength" && ($val1 == "")){$_SESSION['isExtensionLengthUpdated'] = true; $backgroundColor = 'background:#ac5f5d;';$errorMessage = 'Extension Length is required field';}
					if($key1 == "maxFindMeFollowMeDepth" && ($val1 < 0 || $val1 > 10)){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = '( Value should be between 1 and 10 )';}
					if(($key1 == "maxSimultaneousCalls" || $key1 == "maxSimultaneousVideoCalls") && ($val1 < 0 || $val1 > 500)){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = '( Value should be between 1 and 500 )';}
					if(($key1 == "maxCallTimeForAnsweredCallsMinutes") && ($val1 < 5 || $val1 > 2880)){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = '( Value should be between 5 and 2880 )';}
					if(($key1 == "maxCallTimeForUnansweredCallsMinutes") && ($val1 < 1 || $val1 > 60)){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = '( Value should be between 1 and 60 )';}
					if(($key1 == "maxConcurrentRedirectedCalls") && ($val1 < 1 || $val1 > 32)){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = '( Value should be between 1 and 32 )';}
					if(($key1 == "maxRedirectionDepth") && ($val1 < 1 || $val1 > 15)){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = '( Value should be between 1 and 15 )';}
					if(($key1 == "maxConcurrentFindMeFollowMeInvocations") && ($val1 < 1 || $val1 > 32)){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = '( Value should be between 1 and 32 )';}
					if(($key1 == "maxConcurrentFindMeFollowMeInvocations") && ($val1 < 1 || $val1 > 32)){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = '( Value should be between 1 and 32 )';}
					if(($key1 == "callingLineIdName") && (strlen($val1) < 1 || strlen($val1) > 80)){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = '( Length should be between 1 and 80 )';}
					//if(($key1 == "callingLineIdPhoneNumber") && (strlen($val1) < 1 || strlen($val1) > 23)){ $backgroundColor = 'background:#ac5f5d; display:none;';$errorMessage = '( Length should be between 1 and 23 )';}
					if(($key1 == "timeZone") && (strlen($val1) < 1 || strlen($val1) > 127)){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = 'Time Zone is a required field';}
					if(($key1 == "defaultDomain") && (strlen($val1) < 1 || strlen($val1) > 80)){ $backgroundColor = 'background:#ac5f5d;';$errorMessage = '( Length should be between 1 and 80 )';}
					if($key1 <> "domainAssigned" && $key1 <> "securityDomainAssigned" && $key1 <> "assignNumbers" && $key1 <> "servicePackAuth" && $key1 <> "servicePackAssign" && $key1 <> "groupServiceAuth" && $key1 <> "groupServiceAssign" && $key1 <> "userServiceAuth" && $key1 <> "assignedNCS" && $key1 <> "assignedDefault" && $key1 <> "tabName"){
						$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray[$key].'</td><td class="errorTableRows">'.$val1.' '. $errorMessage.' </td></tr>';
					}
					
					if($key1 == "servicePackAuth"){
						$servicePackAuthObj = new Services();
						$servicePackList = $servicePackAuthObj->getUpdateServicePackAuth($postData[$key1]);
						if(count($servicePackList['newAuth']) > 0){
							$_SESSION['isServicePackAuthAssignUpdated'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isServicePackAuthAssignUpdated'].'</td><td class="errorTableRows">'.implode(", ", $servicePackList['newAuth']).' '. $errorMessage.' </td></tr>';
						}
						if(count($servicePackList['unAuth']) > 0){
							$_SESSION['isServicePackAuthUnAssignUpdated'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isServicePackAuthUnAssignUpdated'].'</td><td class="errorTableRows">'. implode(", ", $servicePackList['unAuth']).' '. $errorMessage .' </td></tr>';
						}
					}
					
					if($key1 == "servicePackAssign"){
						$_SESSION['isServicePackAsgnAssignUpdated'] = false;
						$servicePackAssignObj = new Services();
						$servicePackUpdatedList = $servicePackAssignObj->getServicePackAsgnAssign($postData[$key1]);
						//print_r($servicePackUpdatedList);
						if(count($servicePackUpdatedList['newSpAssign']) > 0){
							$_SESSION['isServicePackAsgnAssignUpdated'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isServicePackAsgnAssignUpdated'].'</td><td class="errorTableRows">'.implode(", ", $servicePackUpdatedList['newSpAssign']).' '. $errorMessage.' </td></tr>';
						}
						if(count($servicePackUpdatedList['spUnAssign']) > 0){
							$_SESSION['isServicePackAsgnUnassignUpdated'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isServicePackAsgnUnassignUpdated'].'</td><td class="errorTableRows">'. implode(", ", $servicePackUpdatedList['spUnAssign']).' '. $errorMessage .' </td></tr>';
						}
					}
					
					if($key1 == "groupServiceAuth"){
						$groupServiceObj = new Services();
						$groupServiceList = $groupServiceObj->getGroupServiceAuth($postData[$key1]);
						if(count($groupServiceList['newGroupAuth']) > 0){
							$_SESSION['isGroupServiceAuthUpdated'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isGroupServiceAuthUpdated'].'</td><td class="errorTableRows">'. implode(", ", $groupServiceList['newGroupAuth']).' '. $errorMessage .' </td></tr>';
						}
						if(count($groupServiceList['groupUnAuth'])>0){
							$_SESSION['isGroupServiceUnAuthUpdated'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isGroupServiceUnAuthUpdated'].'</td><td class="errorTableRows">'. implode(", ", $groupServiceList['groupUnAuth']).' '. $errorMessage .' </td></tr>';
						} //echo $changeString; die;
					}
					
					if($key1 == "groupServiceAssign"){
						$groupServiceObj = new Services();
						$groupServiceAssignList = $groupServiceObj->getGroupServiceAssign($postData[$key1]);
						if(count($groupServiceAssignList['newGroupAssign']) > 0){
							$_SESSION['isGroupServiceAssignUpdated'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isGroupServiceAssignUpdated'].'</td><td class="errorTableRows">'. implode(", ", $groupServiceAssignList['newGroupAssign']).' '. $errorMessage .' </td></tr>';
						}
						if(count($groupServiceAssignList['GroupUnAssign'])>0){
							$_SESSION['isGroupServiceUnassignUpdated'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isGroupServiceUnassignUpdated'].'</td><td class="errorTableRows">'. implode(", ", $groupServiceAssignList['GroupUnAssign']).' '. $errorMessage .' </td></tr>';
						}
					}
					
					if($key1 == "userServiceAuth"){
						$userServiceObj = new Services();
						$userServiceAssigned = $userServiceObj->getUserServiceAuth($postData[$key1]);//code here
						if(count($userServiceAssigned['newUserAuth']) > 0){
							$_SESSION['isUserServiceAuthUpdated'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isUserServiceAuthUpdated'].'</td><td class="errorTableRows">'. implode(", ", $userServiceAssigned['newUserAuth']).' '. $errorMessage .' </td></tr>';
						}
						if(count($userServiceAssigned['userUnAuth'])>0){
							$_SESSION['isUserServiceUnAuthUpdated'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isUserServiceUnAuthUpdated'].'</td><td class="errorTableRows">'. implode(", ", $userServiceAssigned['userUnAuth']).' '. $errorMessage .' </td></tr>';
						}
					}
					
					//if($key1 == "assignNumbers"){
                                        if($key1 == "assignNumbers" && (!isset($_POST['prevmodule']) || $_POST['prevmodule'] != "preventChanges")){
                                            
					    //echo "Array 1-";print_r($postData[$key1]);
					    $sesAr = explode(",", $_SESSION['groupInfoData']['assignNumbers']);
					    //echo "Array 2-";print_r($_SESSION['groupInfoData']['assignNumbers']);
					    $assignNumbersArray['newAssign'] = flip_isset_diff($postData[$key1], $sesAr);
					    $assignNumbersArray['unAssign'] = flip_isset_diff($sesAr, $postData[$key1]);
						//$dnsObj = new Dns();
						//$assignNumbersArray = $dnsObj->getUpdatePostDns($postData[$key1]);
						//echo "<pre>"; print_r($assignNumbersArray); die;
						if(count($assignNumbersArray['newAssign']) > 0 && $assignNumbersArray['newAssign'][0] <> ""){ 
							$_SESSION['isDnsAssignUpdated'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isDnsAssignUpdated'].'</td><td class="errorTableRows">'. implode(", ", $assignNumbersArray['newAssign']) .' '. $errorMessage.' </td></tr>';

							if(array_key_exists("activateNewAssigned",$postData) && array_key_exists("activateAllAssigned",$postData)){
								continue;
							}else if(array_key_exists("activateNewAssigned",$postData)){
								$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Number(s) will be activated</td><td class="errorTableRows">'. implode(", ", $assignNumbersArray['newAssign']) .' '. $errorMessage.' </td></tr>';
							}
						}
						if(count($assignNumbersArray['unAssign']) > 0 && $assignNumbersArray['unAssign'][0] <> ""){ 
							$_SESSION['isDnsUnAssignUpdated'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isDnsUnAssignUpdated'].'</td><td class="errorTableRows">'. implode(", ", $assignNumbersArray['unAssign']) .' '. $errorMessage.' </td></tr>';
						}
					}
					
					if($key1 == "assignedNCS"){
						$ncsObj = new Services();
						$ncsAssignedArray = $ncsObj->compareAssignedNcs($postData[$key1]);
						if(count($ncsAssignedArray['newAssign']) > 0){
							$_SESSION['isNcsAssigned'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isNcsAssigned'].'</td><td class="errorTableRows">'. implode(", ", $ncsAssignedArray['newAssign']) .' '. $errorMessage.' </td></tr>';
						}
						if(count($ncsAssignedArray['unAssign']) > 0){
							$_SESSION['isNcsUnAssigned'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isNcsUnAssigned'].'</td><td class="errorTableRows">'. implode(", ", $ncsAssignedArray['unAssign']) .' '. $errorMessage.' </td></tr>';
						}
					}
					if($key1 == "assignedDefault"){
						$ncsDefaultAssignedObj = new Services();
						$ncsDefaultAssigned = $ncsDefaultAssignedObj->compareAssignedDefaultNcs($postData[$key1]);
						if(count($ncsDefaultAssigned['newAssign']) > 0){
							$_SESSION['isNcsDefaultAssigned'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isNcsDefaultAssigned'].'</td><td class="errorTableRows">'. implode(", ", $ncsDefaultAssigned['newAssign']) .' '. $errorMessage.' </td></tr>';
						}
						if(count($ncsDefaultAssigned['unAssign']) > 0){
							$_SESSION['isNcsDefaultUnAssigned'] = true;
							$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$msgArray['isNcsDefaultUnAssigned'].'</td><td class="errorTableRows">'. implode(", ", $ncsDefaultAssigned['unAssign']) .' '. $errorMessage.' </td></tr>';
						}
					}
					
					if($key1 == "domainAssigned"){
						$domainArray['domainAssignList'] = $val1;
						$domainArray['defaultDomain'] = $_SESSION['groupInfoData']['defaultDomain'];
						$assignedDomains = explode(";", substr($val1, 0, -1));
						$val1 = implode(", ", $assignedDomains);
						
						//assign domain
							foreach($assignedDomains as $key2=>$val2){
								if(!in_array($val2, $_SESSION['groupInfoData']['domainAssigned'])){
									$newAssignDomain[] = $val2;
								}
							}
						foreach($_SESSION['groupInfoData']['domainAssigned'] as $key4=>$val4){
							if(!in_array($val4, $assignedDomains)){
								$newUnAssignDomain[] = $val4;
							}
						}
					}
					if($key1 == "securityDomainAssigned"){
						$domainArray['securityDomainAssignList'] = $val1;
						$domainArray['defaultDomain'] = $_SESSION['groupInfoData']['defaultDomain'];
						$securityAssignedDomains = explode(";", substr($val1, 0, -1));
						//echo "<pre>"; print_r($securityAssignedDomains); die;
						if(count($securityAssignedDomains) > 0 && $securityAssignedDomains[0] !== "Nul"){
							$val1 = implode(", ", $securityAssignedDomains);
							//assign security domain
							foreach($securityAssignedDomains as $key3=>$val3){
								if(!in_array($val3, $_SESSION['groupInfoData']['securityDomainAssigned'])){
									$newSecurityAssignDomain[] = $val3;
								}
							}
							foreach($_SESSION['groupInfoData']['securityDomainAssigned'] as $key5=>$val5){
								if(!in_array($val5, $securityAssignedDomains)){
									$newSecurityUnAssignDomain[] = $val5;
								}
							}
						}
					}
					$domainUpdateResponse = $groupValidate->modifyAssignDomain($domainArray);
				}
			}
		}
	}
	
	$numbersActivation = false;
	if(array_key_exists("activateAllAssigned",$postData)){
		if(count($postData["assignNumbers"]) > 0){
			$numbersActivation = true;
			$numbers = implode(", ", $postData["assignNumbers"]);
			$changeString .= '<tr><td class="errorTableRows" style="background:#72ac5d">Number(s) will be Activate</td><td class="errorTableRows">'.$numbers.' </td></tr>';
		}
	}

	if(count($newAssignDomain) > 0 && !empty($newAssignDomain[0])){
		$_SESSION['isAssignDomainUpdated'] = true;
		$newAssignDomainStr = implode(", ", $newAssignDomain);
		$changeString .= '<tr><td class="errorTableRows" style="background:#72ac5d">'.$msgArray['domainAssigned'].'</td><td class="errorTableRows">'.$newAssignDomainStr.' </td></tr>';
	}
	if(count($newSecurityAssignDomain) > 0 && !empty($newSecurityAssignDomain[0])){
		$_SESSION['isAssignDomainUpdated'] = true;
		$newSecurityAssignDomainStr = implode(", ", $newSecurityAssignDomain);
		$changeString .= '<tr><td class="errorTableRows" style="background:#72ac5d;">'.$msgArray['securityDomainAssigned'].'</td><td class="errorTableRows">'.$newSecurityAssignDomainStr .' </td></tr>';
	}
	if(count($newUnAssignDomain) > 0 && !empty($newUnAssignDomain[0])){
		$_SESSION['isAssignDomainUpdated'] = true;
		$newUnAssignDomainStr = implode(", ", $newUnAssignDomain);
		$changeString .= '<tr><td class="errorTableRows" style="background:#72ac5d">'.$msgArray['domainUnAssignList'].' </td><td class="errorTableRows">'.$newUnAssignDomainStr.' </td></tr>';
	}
	if(count($newSecurityUnAssignDomain) > 0 && !empty($newSecurityUnAssignDomain[0])){
		$_SESSION['isAssignDomainUpdated'] = true;
		$newSecurityUnAssignDomainStr = implode(", ", $newSecurityUnAssignDomain);
		$changeString .= '<tr><td class="errorTableRows" style="background:#72ac5d;">'.$msgArray['securityDomainUnAssignList'].'</td><td class="errorTableRows">'.$newSecurityUnAssignDomainStr .' </td></tr>';
	}

if($_SESSION['isAssignDomainUpdated'] == false && $_SESSION['isDnsAssignUpdated'] == false &&
	    $_SESSION['isDnsUnAssignUpdated'] == false && $_SESSION['isBasicGroupInfoUpdated'] == false && $_SESSION['isOutgoingCallPlanUpdated'] == false && 
		$_SESSION['isRoutingInfoUpdated'] == false && $_SESSION['isCallProcessingUpdated'] == false &&
		$_SESSION['isServicePackAuthAssignUpdated'] == false && $_SESSION['isServicePackAuthUnAssignUpdated'] == false &&
		$_SESSION['isGroupServiceAuthUpdated'] == false && $_SESSION['isGroupServiceUnAuthUpdated'] == false &&
		$_SESSION['isGroupServiceAssignUpdated'] == false && $_SESSION['isGroupServiceUnassignUpdated'] == false &&
		$_SESSION['isUserServiceAuthUpdated'] == false && $_SESSION['isUserServiceUnAuthUpdated'] == false &&
		$_SESSION['isNcsAssigned'] == false && $_SESSION['isNcsDefaultAssigned'] == false &&
		$_SESSION['isNcsUnAssigned'] == false && $_SESSION['isNcsDefaultUnAssigned'] == false &&
		$_SESSION['isVoicePortalUpdated'] == false && $_SESSION['isExtensionLengthUpdated'] == false &&
		$numbersActivation == false){
		$changeString = '<tr><td class="errorTableRows" style="background:#72ac5d;">Group Modify Changes </td><td class="errorTableRows">No Changes </td></tr>';
	}
	
	echo $changeString;
	
}

function flip_isset_diff($b, $a) {
    $at = array_flip($a);
    $d = array();
    foreach ($b as $i)
        if (!isset($at[$i]))
            $d[] = $i;
            
            return $d;
}

?>