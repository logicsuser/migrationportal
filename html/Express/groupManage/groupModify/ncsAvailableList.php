<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");

$_POST['groupId'] = htmlspecialchars($_POST['groupId']);
if(isset($_POST['groupId']) && !empty($_POST['groupId'])){
	$ncsServiceList = new Services();
	$ncsServiceListResponse = $ncsServiceList->networkClassOfServicesAvailableForAssignToGroup($_POST['groupId']);
	//print_r($ncsServiceListResponse);
	echo json_encode($ncsServiceListResponse);
}
?>