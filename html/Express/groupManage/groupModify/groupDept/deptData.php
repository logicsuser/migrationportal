<?php
	require_once ("/var/www/html/Express/config.php");
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin ();
	
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/department/deptController.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupOperation.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupHelper.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/dns/dnsHelper.php");
	
	$_POST['groupId'] = htmlspecialchars($_POST['groupId']);
	if(isset($_POST['groupId']) && !empty($_POST['groupId'])){
		
		$departmentObj = new DeptController();
		
		$departmentList = $departmentObj->getDepartmentList($_POST['groupId']);
		//echo "<pre>"; print_r($departmentList);
		echo json_encode($departmentList);
		
		/*$dnObj = new GroupOperation($_SESSION['sp'], $_POST['groupId']);
		$dnList = $dnObj->getGroupDnGetListRequest();
		$helperObj = new GroupHelper();
		
		$dnsListAll = $helperObj->getGroupDnGetListRequestHelper($dnList);
		echo "<pre>"; print_r($dnsListAll);
		echo json_encode($dnsListAll);*/
	}
?>