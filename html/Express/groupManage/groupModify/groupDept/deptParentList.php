<?php
	require_once ("/var/www/html/Express/config.php");
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin ();
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/department/deptController.php");
	
	$_POST['groupId'] = htmlspecialchars($_POST['groupId']);
	if(isset($_POST['groupId']) && !empty($_POST['groupId'])){

		$depAddObj = new DeptController();
		$sp = $_SESSION['sp'];
		$groupId = $_POST['groupId'];
		$deptName = $_POST['departmentName'];
		
		$deptParentList = $depAddObj->GroupDepartmentGetAvailableParentListRequest($sp, $groupId, $deptName);
		//echo "<pre>"; print_r($deptParentList);
		echo json_encode($deptParentList);
	}