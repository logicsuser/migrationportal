<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/department/deptController.php");

$_POST['groupId'] = htmlspecialchars($_POST['groupId']);
if(isset($_POST['groupId']) && !empty($_POST['groupId'])){
	$depGetObj = new DeptController();
	$deptGetResp = $depGetObj->getDeptFromGroup($_POST["groupId"], $_POST["departmentName"]);
	
	if(empty($deptGetResp['Error'])){
		$_SESSION['groupDepartment'] = $deptGetResp['Success'];
		$_SESSION['groupDepartment']['departmentName'] = $_POST["departmentName"];
	}
	echo json_encode($deptGetResp);
}