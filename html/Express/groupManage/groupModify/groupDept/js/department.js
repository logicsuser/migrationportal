$(function()
{
	$("#departmentGroupFrm").hide();
	$("#loading3").hide();	
	
	var departmentListResult = function(selectGroupId) {
		$("#departmentGroupFrm").hide();
		var autoCompleteDepartment = new Array();
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/groupDept/deptData.php",
			cache: false,
			data: {groupId : selectGroupId},
			success: function(result) {
				var obj = jQuery.parseJSON(result);
				$('#selectDepartment').empty('');
				if(obj.Success){
					if(obj.Success.department){
						var departmentList = obj.Success.department;
						var deptDropdown = "<option value=''> None </option>";
						for (var i = 0; i < departmentList.length; i++) {
							deptDropdown += "<option value='"+departmentList[i].name+"'>" + departmentList[i].name + "</option>";
						}
						$("#selectDepartment").html(deptDropdown);
					}
				}else{
					$("#selectDepartment").autocomplete({source: []});
				}
			},
		});
	};
	
	departmentListResult($("#isGroupAccess").val());
	
	var departmentListWithParentKey = function(groupId){
		var html = "";
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/groupDept/deptData.php",
			data: {groupId : groupId},
			success: function(result) {
				var obj = jQuery.parseJSON(result);
				var departmentList = obj.Success.department;
				html = '<option value="">None</option>'
				for (var i = 0; i < departmentList.length; i++) {
					html += '<option value="'+ departmentList[i].name +'">' + departmentList[i].fpName + ' </option>';
				}
				$("#parentDepartmentKey").html(html);
			},
		});
	}
	
	var departmentInfo = function(selectGroupId, deptName){
		
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/groupDept/deptGet.php",
			data: {groupId : selectGroupId, departmentName : deptName},
			success: function(result) {
				var obj = jQuery.parseJSON(result);			
				$("#departmentName").val(deptName);
				$("#callingLineIdNameDepartment").val(obj.Success.callingLineIdNameDepartment);
				if(obj.Success.callingLineIdPhoneNumberDepartment){
					var callingLineIdPhoneNumberDepartment = obj.Success.callingLineIdPhoneNumberDepartment;
					$("#callingLineIdPhoneNumberDepartment").val(callingLineIdPhoneNumberDepartment);
				}else{
					$("#callingLineIdPhoneNumberDepartment").val("");
				}
				$("#parentDepartmentKey").val(obj.Success.parentDepartmentKey);
				
				$("#loading3").hide();	
				$("#departmentGroupFrm").show();				
			},
		});
	};
	
	$("#tabs li").click(function(){ 
		var el = $(this);
		if(el.text() == "Department"){
			var selectGroupId = $("#selectGroup").val();
			departmentListResult(selectGroupId);
			$("#selectDepartment").val('');
		}
	});
	
	$(document).on("change", "#selectDepartment", function(){
		$("#saveDepartment").hide();
		$("#modifyDepartment").show();
		$("#loading3").show();	
		$("#departmentGroupFrm").hide();	
		
		var el = $(this);
		var selectDepartmentVal = el.val();
		if(selectDepartmentVal == ""){
			$("#loading3").hide();
			return false;
		}
		$("#parentDepartmentKey").empty('');
//		var selectGroupId = $("#selectGroup").val();
		var selectGroupId = $("#isGroupAccess").val();		
		deptParentListData(selectGroupId, selectDepartmentVal);
		$('#parentDepartmentKey').children('option').show();				
		$("#deleteDepartment").show();
	});
	
	var deptParentListData = function(selectGroupId, selectDepartmentVal){
		var html = '';
		
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/groupDept/deptParentList.php",
			data: {groupId : selectGroupId, departmentName : selectDepartmentVal},
			success: function(result) {
				var obj = jQuery.parseJSON(result);
				html = '<option value="">None</none>';
				if(obj.Success){
					var departmentList = obj.Success.department;
					for (var i = 0; i < departmentList.length; i++) {
						html += '<option value="'+ departmentList[i].name +'">' + departmentList[i].fpName + ' </option>';
					}
				}				
				$("#parentDepartmentKey").html(html);
				departmentInfo(selectGroupId, selectDepartmentVal);
			}
		});
	}
	
	$("#dialogDepartment").dialog({
			autoOpen: false,
			width: 800,
			modal: true,
			position: { my: "top", at: "top" },
			resizable: false,
			closeOnEscape: false,
			buttons: {
				"Complete": function() {
					var depName = $("#departmentName").val();
					if($('#saveDepartment').is(':visible')) {
					    createDepartment();
					}else if($('#modifyDepartment').is(':visible')){
						modifyDepartment();
					}		
				},
				"Cancel": function() {
					$(this).dialog("close");
				},
	            "Done": function() {
	            	$("#departmentName").val('');
	            	$("#parentDepartmentKey").val('');
	            	$("#callingLineIdNameDepartment").val('');
	            	$("#callingLineIdPhoneNumberDepartment").val('');
	            	$("#selectDepartment").val('');
	            	var selectGroupId = $("#isGroupAccess").val();
	            	departmentListResult(selectGroupId);
	            	$(this).dialog("close");              
				},
				"Delete": function(){
					deleteDepartmentFromGroup();
				}
			},
	        open: function() {
				setDialogDayNightMode($(this));
	        	$(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');;
	            $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
	            $(".ui-dialog-buttonpane button:contains('Done')").button().hide();
	            $(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
	        }
	});
	
	$("#saveDepartment, #modifyDepartment").click(function() {
		var dataToSend = $("form#departmentGroupFrm").serializeArray();
		$("#dialogDepartment").html('');
		var sessData = '';
		if($('#saveDepartment').is(':visible')) {
			sessData = 1;
		}else if($('#modifyDepartment').is(':visible')){
			sessData = 0;
		}
		var dpName = $("#departmentName").val();
		//if(dpName){
			$.ajax({
				type: "POST",
				url: "groupManage/groupModify/groupDept/deptValidate.php",
				data: {compare : 1, sessData : sessData, formdata : dataToSend},
				success: function(result) {
					$("#dialogDepartment").dialog("option", "title", "Request Complete");
					$("#dialogDepartment").html('<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="listchange" class="confSettingTable"><tbody><tr><th colspan="2" align="center">Please confirm the settings below and click Complete to process your modifications.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');
					if (!$.trim(result)){
					        $("#listchange").html("No Change");
					}else{
						$("#listchange").append(result);
					}
					$("#dialogDepartment").append('</tbody></table>');
					$("#dialogDepartment").dialog("open");
					if (!$.trim(result)){
						$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
					}
					else if(result.slice(0,1) == '1'){
						$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
					}
					else{
						$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled subButton").show().addClass('subButton');
					}
				}
			});
	//	}	
	});
	
	$("#newDepartment").click(function() {
		var groupId = $("#isGroupAccess").val();
		$("#departmentGroupFrm").show();
		$("#departmentName").val('');
    	$("#parentDepartmentKey").val('');
    	$("#callingLineIdNameDepartment").val('');
    	$("#callingLineIdPhoneNumberDepartment").val('');
    	$("#saveDepartment").show();
		$("#modifyDepartment").hide();
		$("#selectDepartment").val('');
		$("#deleteDepartment").hide();
		departmentListWithParentKey(groupId);
	});
	
	$("#cancelDepartment").click(function() {
		$("#selectDepartment").val('');
		$("#departmentGroupFrm").hide();
		$("#departmentName").val('');
    	$("#parentDepartmentKey").val('');
    	$("#callingLineIdNameDepartment").val('');
    	$("#callingLineIdPhoneNumberDepartment").val('');
	});
	
	var createDepartment = function(){
		var dataToSend = $("form#departmentGroupFrm").serializeArray();
		var groupId = $("#isGroupAccess").val();
		$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
		$("#dialogDepartment").html('Creating the department request....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
        $.ajax({
			type: "POST",
			url: "groupManage/groupModify/groupDept/deptAdd.php",
			//data: dataToSend,
			data: {groupId : groupId, dataToSend : dataToSend},
			success: function(result) {
				$("#dialogDepartment").html('');
				var obj = jQuery.parseJSON(result);
				var deptError = "";
				if(obj.deptAddResp){
					if(obj.deptAddResp.Error){
						$("#dialogDepartment").append('<br/>' + obj.deptAddResp.Error.Detail);
						deptError = "1";
					}else{
						$("#dialogDepartment").append('<br/><b>Department Added Successfully</b>');
					}
				}
				if(deptError == "1"){
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('CancelButton');
					$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
				}else{							
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
					$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
					$(".ui-dialog-buttonpane button:contains('More changes')").button().show().addClass('moreChangesButton');
                    $(".ui-dialog-buttonpane button:contains('Done')").button().show().addClass('subButton');
				}
			}
        });
	}
	
	var modifyDepartment = function(){
		var dataToSend = $("form#departmentGroupFrm").serializeArray();
//		var groupId = $("#selectGroup").val();
		var groupId = $("#isGroupAccess").val();
		var deptName = $("#selectDepartment").val();
		
		$("#dialogDepartment").html('Modifying the department request....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
        $.ajax({
			type: "POST",
			url: "groupManage/groupModify/groupDept/deptModify.php",
			data: {groupId : groupId, deptName : deptName, dataToSend : dataToSend},
			success: function(result) {
				$("#dialogDepartment").html('');
				var obj = jQuery.parseJSON(result);
				var deptModError = "";
				if(obj.deptModify){
					if(obj.deptModify.Error){
						$("#dialogDepartment").append('<br/>' + obj.deptModify.Error.Detail);
						deptModError = "1";
					}else{
						$("#dialogDepartment").append('<br/><b>Department Modified Successfully</b>');
					}
				}
				if(deptModError == "1"){
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
					$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
				}else{							
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
					$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
					$(".ui-dialog-buttonpane button:contains('More changes')").button().show().addClass('moreChangesButton');
                    $(".ui-dialog-buttonpane button:contains('Done')").button().show().addClass('subButton');
				}
			}
        });
	}
	
	$("#deleteDepartment").click(function(){
		var dpName = $("#departmentName").val();
		if(dpName){
			$("#dialogDepartment").dialog("open");
			$("#dialogDepartment").html("Are you sure you want to delete <b>"+dpName+" </b>Department?");
			$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
			$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
			$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
			$(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
			$(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass('deleteButton');
		}
	});	
	
	var deleteDepartmentFromGroup = function(){
		var dpName = $("#departmentName").val();
		var groupId = $("#isGroupAccess").val();
		$("#dialogDepartment").html('Deleting the department....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
		$(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
		$("#loading2").show();  
		if(dpName){
			$.ajax({
				type: "POST",
				url: "groupManage/groupModify/groupDept/deptDelete.php",
				data: {dpName : dpName, groupId : groupId},
				success: function(result) {
					$("#loading2").hide(); 
					$("#dialogDepartment").html("");
					var obj = jQuery.parseJSON(result);
					if(obj.Error){
						$("#dialogDepartment").append('<br/><b>' + obj.Error.Detail+'</b>');
						$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
						$(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass('deleteButton');
					}else{							
						$("#selectDepartment").html("");				
						$("#dialogDepartment").html('Department has been deleted');
						$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
						$(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
						$(".ui-dialog-buttonpane button:contains('Done')").button().show().addClass('subButton');
					}
				}
			});
		}
	}

});
if ($.fn.button.noConflict) {
	var bootstrapButton = $.fn.button.noConflict();
	$.fn.bootstrapBtn = bootstrapButton;
	}