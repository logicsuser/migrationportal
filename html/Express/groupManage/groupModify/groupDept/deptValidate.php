<?php 
	require_once ("/var/www/html/Express/config.php");
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/department/deptController.php");
	
	$_POST['groupId'] = htmlspecialchars($_POST['groupId']);
	$obj = new DeptController();
	$formDataArray = $obj->formDataArray();	
	
	foreach($_POST['formdata'] as $key=>$val){
		$postData[$val['name']] = $val['value'];
	}

	if($_POST['sessData'] == 1){
		unset($_SESSION['groupDepartment']['departmentName']);
	}
	
	if(!empty($_SESSION['groupDepartment']['departmentName'])) {
		$objSession = new DeptController();
		$_SESSION['groupDepartment'] = $objSession->ifDataNotExistInSession($_SESSION['groupDepartment']);
	}
	
	$error = "";
	$changeString = "";
	if($postData["departmentName"] != "") {
    	foreach($postData as $key=>$val) {
    		$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
    		if(!empty($_SESSION['groupDepartment']['departmentName'])) {
    		    foreach($_SESSION['groupDepartment'] as $key1=>$val1) {
    		        if($key == $key1) {
    		            if($val <> $val1) {
    		                if($val == "") {
    		                    //$errorMessage = "Please Fill Departement Name";
    		                    $val="None";
    		                }
    		                //else{
    		                //  $val="None";
    		                // }
    		                $changeString .= '<tr><td class="errorTableRows t1" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
    		            }
    		        }
    		    }
    		}else{
    			if (!empty($val)) {
    				$changeString .= '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
    			}				
    		}					
    	}
    } else {
        $error = 1;
        $backgroundColor = 'background: #ac5f5d;'; $errorMessage = 'Department is Required';
        $changeString .= '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'">'.'Department Name'.'</td><td class="errorTableRows">'. $errorMessage.' </td></tr>';
    }
    echo $error . $changeString;