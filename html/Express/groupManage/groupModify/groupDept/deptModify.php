<?php
	require_once ("/var/www/html/Express/config.php");
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin ();
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/department/deptController.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
        $_POST['groupId'] = htmlspecialchars($_POST['groupId']);
        $cLUObj = new ChangeLogUtility($_POST['groupId'], $_POST['groupId'], $_SESSION["loggedInUserName"]);
	$obj = new DeptController();
	$formDataArray = $obj->formDataArray();
	foreach($_POST['dataToSend'] as $key=>$val){
		$postData[$val['name']] = $val['value'];
	}
	
	if(!empty($_SESSION['groupDepartment']['departmentName'])){
		$objSession = new DeptController();
		$_SESSION['groupDepartment'] = $objSession->ifDataNotExistInSession($_SESSION['groupDepartment']);
	}
	$changeArray = array();
	foreach($postData as $key=>$val){
		foreach($_SESSION['groupDepartment'] as $key1=>$val1){
			if($key == $key1){
				if($val <> $val1){
					if($val == ""){
						$val="Nill";
					}
					$changeArray[$key] = $val;
                                        $cLUObj->createChangesArray($module = $key1, $oldValue =$val1, $newValue =$val);
				}
                        }
                }
	}
	
	if(isset($_POST['groupId']) && !empty($_POST['groupId'])){
		$depModObj = new DeptController();
		$deptModResp['deptModify'] = $depModObj->modifyDepartmentInGroup($_POST['groupId'], $_POST['deptName'], $changeArray);
               
                echo json_encode($deptModResp);
                if($deptModResp['deptModify']['Success']=="Success"){
                    $module = "Group Modify";
                    $cLUObj->changeLogModifyUtility($module, $_POST['groupId'], $tableName = "groupModChanges");
                } 
                
	}