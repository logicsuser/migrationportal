<?php
	require_once ("/var/www/html/Express/config.php");
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin ();
	
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/department/deptController.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
	$_POST['groupId'] = htmlspecialchars($_POST['groupId']);
	if(isset($_POST['groupId']) && !empty($_POST['groupId']) && !empty($_POST['dpName'])){
		$depDelObj = new DeptController();
		$deptDelResp = $depDelObj->deleteDeptFromGroup($_POST["groupId"], $_POST["dpName"]);
                echo json_encode($deptDelResp);
                if($deptDelResp['Success']['Detail']=="Success"){
                  $changeLogObj  = new ChangeLogUtility($_SESSION["sp"], $_POST['groupId'], $_SESSION["loggedInUserName"]);
                  $changeLogObj->module     = "Group Modify Delete Department";
                  $changeLogObj->entityName = $_POST["dpName"];
                  $changeLogObj->changeLog();

                  $changeLogObj->modTableName = "groupModChanges";
                  $changeLogObj->serviceId    = $_POST['groupId'];
                  $changeLogObj->entityName   = $_POST["dpName"];
                  $changeLogObj->tableDelChanges();
                }
                
		
	}