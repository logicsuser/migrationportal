<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupOperation.php");

if(!empty($_POST['groupId'])){
	
	$sp = $_SESSION['sp'];
	//$groupId = htmlspecialchars($_POST['groupId']);
	$groupId = $_POST['groupId'];
	//initialize class
	$group = new GroupOperation($sp, $groupId);
	$postArray['groupId'] = $groupId;
	$usersInGroup = $group->getAllUsersInGroup($postArray);
	$dnInGroup = $group->getGroupDnGetListRequest();
	
	if(count($dnInGroup['Success']) > 0){
		$groupArray['dn'] = count($dnInGroup['Success']);
	}else{
		$groupArray['dn'] = "";
	}
	if(count($usersInGroup) > 0){
		$groupArray['userCount'] = count($usersInGroup);
	}else{
		$groupArray['userCount'] = "";
	}
	
	$groupArray['groupId'] = $_POST['groupId'];
	//print_r($userCount);
	echo json_encode($groupArray);
}