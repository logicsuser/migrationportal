<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupOperation.php");

//new modified code to get group info
require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupHelper.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dnsHelper.php");

//Code added @ 22 Aug 2018
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
if(isset($_POST['groupId']) && !empty($_POST['groupId'])){
	
	//initialize the service provider id and group id and pass in to class constructor
	$sp = $_SESSION['sp'];
	$groupId = $_POST['groupId'];
        
	//Class For Group Information
	$info = new GroupOperation($sp, $groupId);
	$basicInfo = $info->getGroupBasicInfo();

	if(empty($basicInfo['Error'])){ 
		$assignDomainList = $info->getDefaultDomainAssignList();
		$dnList = $info->getGroupDnGetListRequest();
		$routeList = $info->getGroupRoutingProfile();
		$callProcessingInfo = $info->getGroupCallProcessingPolicy();
		$spDomainList = $info->getServiceProviderDomainGetAssignedListRequest();
		$groupDomainAssignedList = $info->getGroupDomainGetAssignedListRequest();
		$groupAvailable = $info->getAvailableDomains($spDomainList, $groupDomainAssignedList);
		$voicePortalResponse = $info->getVoicePortalGroupResponse();
		

        //$domainAvailable = $info->getAvailableDomain($groupAvailable['groupInfoData']['available'], $securityDomainPattern);
		//$domainAssigned = $info->getAvailableDomain($groupDomainAssignedList['Success'], $securityDomainPattern);
		
       //Code added @ 05 Dec 2018 fregarding EX-931
        $domainAvailable = $info->getAvailableDomainsBasedOnSPattern($groupAvailable['groupInfoData']['available'], $securityDomainPattern);
        $domainAssigned = $info->getAvailableDomainsBasedOnSPattern($groupDomainAssignedList['Success'], $securityDomainPattern);
        //End code

                
                
        $securityDomainAvailable = $info->getSecurityDomain($groupAvailable['groupInfoData']['available'], $securityDomainPattern);
		$securityDomainAssigned = $info->getSecurityDomain($groupDomainAssignedList['Success'], $securityDomainPattern);
		//dn class
		$dns = new Dns();
		$extensionResponse = $dns->getGroupExtensionConfigRequest($sp, $groupId);
		
		//Class for Group Helper
		$helper = new GroupHelper();
		$basicInfoHelper = $helper->getGroupBasicInfoHelper($basicInfo, $groupId);
		$assignDomainListHelper = $helper->getAssignDomainListHelper($assignDomainList);
		$dnListHelper = $helper->getGroupDnGetListRequestHelper($dnList);
		$routeListHelper = $helper->getGroupRoutingProfileHelper($routeList);
		$callProcessingHelper = $helper->getGroupCallProcessingPolicyHelper($callProcessingInfo);
		$spDomainListHelper = $helper->getServiceProviderDomainGetAssignedListRequestHelper($spDomainList);
		$groupDomainListHelper = $helper->getGroupDomainGetAssignedListRequestHelper($groupDomainAssignedList);
		$voicePortalResponseHelper = $helper->getVoicePortalGroupResponseHelper($voicePortalResponse);
		$domainAvailableHelper = $helper->getAvailableDomainHelper($domainAvailable, 'available');
		$domainAssignedHelper = $helper->getAvailableDomainHelper($domainAssigned, 'domainAssigned');
		$availableSecurityDomianHelper = $helper->getSecurityDomainHelper($securityDomainAvailable, 'securityDomainAvailable');
		$assignedSecurityDomianHelper = $helper->getSecurityDomainHelper($securityDomainAssigned, 'securityDomainAssigned');
		
                //Code added @ 21 Aug 2018 
                $ocpInfoHelper = $helper->getGroupOutgoingCallingPlanHelper($sp, $groupId);
                //End code
                
                
		//class for DNS helper
		$dnsHelper = new DnsHelper();
		$extensionResponseHelper = $dnsHelper->getGroupExtensionConfigRequestHelper($extensionResponse);
		$extensionLength = $extensionResponseHelper['defaultExtensionLength'] == $extensionResponseHelper['minExtensionLength'] && $extensionResponseHelper['minExtensionLength'] == $extensionResponseHelper['maxExtensionLength'] ? $extensionResponseHelper['defaultExtensionLength'] : "";
		$extensionLengthRes = array("extensionLength" => $extensionLength);
		
                //Code added @ 23 Aug 2018
                $serviceInfo = new Services();
                $assignServiceList = array();
                $assignServiceList = $serviceInfo->GroupServiceGetAuthorizationListRequestForEnterprise($groupId);
                $assignedOCPService = array("assignedOCPService" => "No");
                
                if(isset($assignServiceList) && count($assignServiceList)>0){
                    if(in_array("Outgoing Calling Plan", $assignServiceList['groupServiceAssign']))
                    {
                        $assignedOCPService = array("assignedOCPService" => "Yes");
                    }
                }
                //End code
                
		$groupInfoResponse = array_merge_recursive(
							$basicInfoHelper, 
							$assignDomainListHelper, 
							$dnListHelper, 
							$routeListHelper,
							$callProcessingHelper,
							$spDomainListHelper,
							$groupDomainListHelper,
							$voicePortalResponseHelper,
							$extensionResponseHelper,
							$domainAvailableHelper,
							$domainAssignedHelper,
							$availableSecurityDomianHelper,
							$assignedSecurityDomianHelper,
                                                        $extensionLengthRes,
                                                        $ocpInfoHelper,
                                                        $assignServiceList,
                                                        $assignedOCPService
						);
		if(!empty($_SESSION["tabName"])){
			$groupInfoResponse["tabName"] = $_SESSION["tabName"];
		}		
		$_SESSION['groupInfoData'] = $groupInfoResponse;
	}else{
		$groupInfoResponse = array('Error'=>"Group Not Found");
	}
	echo json_encode($groupInfoResponse);
}

?>