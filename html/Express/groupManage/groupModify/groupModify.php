<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupOperation.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
require_once ("/var/www/lib/broadsoft/adminPortal/voicePortal/voicePortal.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php"); // Added on 23 may 2018

$_POST['assignNumbers'] =  explode(";;", rtrim($_POST['assignNumbers'] ,";;") ) ;

// Added on 23 may 2018
$bscInfoVcePortlClPrsPlcykeyArr = array("groupName", "defaultDomain", "timeZone", "extensionLength" , "minExtensionLength", "maxExtensionLength", "defaultExtensionLength", "userLimit", "callingLineIdName", "locationDialingCode", "callingLineIdDisplayPhoneNumber", "contactName", "contactEmail", "contactNumber", "addressLine1", "addressLine2", "city", "stateOrProvince", "zipOrPostalCode", "country", "routingProfile", "portalLanguage", "portalTimeZone", "voicePortalActiveStatus" ,"portalPhoneNo", "portalExt", "useGroupName", "allowDepartmentCLIDNameOverride", "useGroupCLIDSetting", "clidPolicy", "emergencyClidPolicy", "allowAlternateNumbersForRedirectingIdentity", "allowConfigurableCLIDForRedirectingIdentity", "blockCallingNameForExternalCalls", "useGroupMediaSetting", "mediaPolicySelection", "useGroupCallLimitsSetting", "useMaxSimultaneousCalls", "maxSimultaneousCalls", "useMaxSimultaneousVideoCalls", "maxSimultaneousVideoCalls", "useMaxCallTimeForAnsweredCalls", "maxCallTimeForAnsweredCallsMinutes", "useMaxCallTimeForUnansweredCalls", "maxCallTimeForUnansweredCallsMinutes", "useMaxConcurrentRedirectedCalls", "maxConcurrentRedirectedCalls", "useMaxConcurrentFindMeFollowMeInvocations", "maxConcurrentFindMeFollowMeInvocations", "useMaxFindMeFollowMeDepth", "maxFindMeFollowMeDepth", "maxRedirectionDepth", "useGroupTranslationRoutingSetting", "networkUsageSelection", "enforceGroupCallingLineIdentityRestriction", "allowEnterpriseGroupCallTypingForPrivateDialingPlan", "allowEnterpriseGroupCallTypingForPublicDialingPlan", "overrideCLIDRestrictionForPrivateCallCategory", "useGroupDCLIDSetting", "enableDialableCallerID");
//End Code

//Added on 20 August 2018
$groupOCPSettingskeyArr = array("groupPermissionsGroup", "groupPermissionsLocal", "groupPermissionsTollfree", "groupPermissionsToll", "groupPermissionsInternational", "groupPermissionsOperatorAssisted", "groupPermissionsOperatorChargDirAssisted", "groupPermissionsSpecialService1", "groupPermissionsSpecialService2", "groupPermissionsPremiumServices1", "groupPermissionsPremiumServices2", "groupPermissionsCasual", "groupPermissionsUrlDialing", "groupPermissionsUnknown");
//End Code

$dnsObj = new Dns();
$modify = array();
server_fail_over_debuggin_testing(); /* for fail Over testing. */

if(isset($_POST['groupId']) && !empty($_POST['groupId'])){

//     $_POST['groupId'] = htmlspecialchars($_POST['groupId']);
    if(!empty($_POST['groupName'])) {
        $_POST['groupName'] = $_POST['groupName'];
    }
    
    $_POST['groupId'] = $_POST['groupId'];
        //Code added @ 23 May 2018
        $groundId = isset($_SESSION["groupId"]) ? $_SESSION["groupId"] : $_POST["groupId"];
        $cLUObj = new ChangeLogUtility($_POST['groupName'], $groundId, $_SESSION["loggedInUserName"]);
        $infoChange = "";
        
        foreach($_POST as $ky=>$vl)
        {   
            
            if(in_array($ky, $bscInfoVcePortlClPrsPlcykeyArr))
            {              
                if($_POST[$ky]!=$_SESSION['groupInfoData'][$ky])
                {                    
                    $cLUObj->createChangesArray($module = $ky, $oldValue = $_SESSION['groupInfoData'][$ky], $newValue = $_POST[$ky]);
                    $infoChange = "Yes";                    
                }
            }
            //Code added @ 21 Aug 2018
            if(in_array($ky, $groupOCPSettingskeyArr))
            {              
                if($_POST[$ky]!=$_SESSION['groupInfoData'][$ky])
                {                    
                    $cLUObj->createChangesArray($module = $ky, $oldValue = $_SESSION['groupInfoData'][$ky], $newValue = $_POST[$ky]);
                    $infoChange = "Yes";                    
                }
            }  
            //End cod
        } 
        //End Code
        
	if(!array_key_exists("assignNumbers", $_POST)){
		$_POST['assignNumbers'] = array();
	}
	
	$sp = $_SESSION['sp'];
	$groupIdVal = $_POST['groupId'];
	
	$modifyGroup = new GroupOperation($sp, $groupIdVal);
	
	if($_SESSION['isBasicGroupInfoUpdated'] == true){ 
		$modify['groupModifyResponse'] = $modifyGroup->groupBasicInfoOperation($_POST);
	}
        
        //Code added @ 20 Aug 2018
        if($_SESSION['isOutgoingCallPlanUpdated'] == true){
                    //$modify['groupModifyResponse'] = $modifyGroup->groupBasicInfoOperation($_POST);
                    require_once ("/var/www/lib/broadsoft/adminPortal/ocpFeatures/OCPOperations.php");
		    $objOCP = new OCPOperations();
		    $ocpDataResp = $objOCP->groupCallingPlanModifyOriginatingRequest($sp, $groupIdVal, $_POST);
                    $modify['ocpModifyResponse'] = $ocpDataResp;
        }
        //End code
        
        //print_r($_SESSION['isExtensionLengthUpdated']); exit;
	if(isset($_POST['extensionLength']) && $_SESSION['isExtensionLengthUpdated'] == true) {
	    $extensionArray['minExtensionLength'] = $_POST['extensionLength'];
	    $extensionArray['maxExtensionLength'] = $_POST['extensionLength'];
	    $extensionArray['defaultExtensionLength'] = $_POST['extensionLength'];
	    
	    $extensionArray['groupId'] = $groupIdVal;
		$modify['groupExtensionResponse'] = $dnsObj->modifyGroupExtensionConfigRequest($extensionArray);
	} else if($_SESSION['isExtensionLengthUpdated'] == true) {
		$extensionArray['minExtensionLength'] = $_POST['minExtensionLength'];
		$extensionArray['maxExtensionLength'] = $_POST['maxExtensionLength'];
		$extensionArray['defaultExtensionLength'] = $_POST['defaultExtensionLength'];
		
		$extensionArray['groupId'] = $groupIdVal;
		$modify['groupExtensionResponse'] = $dnsObj->modifyGroupExtensionConfigRequest($extensionArray);
	}
	if($_SESSION['isRoutingInfoUpdated'] == true){
		$modify['routingModifyResponse'] = $modifyGroup->groupRoutingProfileOperation($_POST);
	}
	
	if($_SESSION['isAssignDomainUpdated'] == true){
		$domainAssignArray['domainAssignList'] = $_POST['domainAssigned'];
		$domainAssignArray['securityDomainAssignList'] = $_POST['securityDomainAssigned'];		
		$domainAssignArray['defaultDomain'] = $_POST['defaultDomain'];		
		
		$domainArray = $modifyGroup->modifyAssignDomain($domainAssignArray);
                
		if(isset($domainArray['unAssignDomain']) && count($domainArray['unAssignDomain']) > 0){
			$modify['assignDomainModifyResponse']['unAssign'] = $modifyGroup->updateUnAssignDomain($domainArray['unAssignDomain'], $_POST);
        }
        
		if(isset($domainArray['assignDomain']) && count($domainArray['assignDomain']) > 0){
			$modify['assignDomainModifyResponse']['assign'] = $modifyGroup->updateAssignDomain($domainArray['assignDomain'], $_POST);
        }
                
                //Code added @ 23 May 2018
                $successDomainStatus = "Yes";
                if(isset($modify['assignDomainModifyResponse']['assign']['Error']) && !empty($modify['assignDomainModifyResponse']['assign']['Error']))
                    $successDomainStatus = "No";
                if(isset($modify['assignDomainModifyResponse']['unAssign']['Error']) && !empty($modify['assignDomainModifyResponse']['unAssign']['Error']))
                    $successDomainStatus = "No";
                
                if($successDomainStatus=="Yes")
                {
                    if(count($domainArray['assignDomain']) > 0 || count($domainArray['unAssignDomain']) > 0)
                    {
                        $oldValue = implode(";", $_SESSION['groupInfoData']['domainAssigned']);
                        $newValue = $_POST['domainAssigned'];
                        $cLUObj->createChangesArray($module = "domainAssigned", $oldValue, $newValue);
                    }
                }
                //End Code
	}        
	if($_SESSION['isCallProcessingUpdated'] == true){
		$modify['callProcessingModifyResponse'] = $modifyGroup->groupCallProcessingOperation($_POST);
	}
	if($_SESSION['isDnsAssignUpdated'] == true || $_SESSION['isDnsUnAssignUpdated'] == true ){
		$dnPostArray['phoneNumber'] = $_POST['assignNumbers'];
		$dnPostArray['groupId'] = $groupIdVal;
		
		if(count($dnPostArray['phoneNumber']) > 0){
			$assignNumbersArray = $dnsObj->getUpdatePostDns($dnPostArray['phoneNumber']);
			
			if($_SESSION['isDnsAssignUpdated'] == true){
				$dnPostArray['phoneNumber'] = $assignNumbersArray['newAssign'];
				$modify['dnsResponse']['assign'] = $dnsObj->setGroupDNAssignRequest($dnPostArray);
				if(array_key_exists("activateNewAssigned",$_POST)){
					if(empty($modify['dnsResponse']['assign']['Error'])){
						$modify['dnsActivateResponse'] = $dnsObj->activateDNRequest($dnPostArray);	
					}
				}
				//$modify['dnGroupResp'] = $dnGroupResp;
				
				//to activate dn request when assign the new dn
				//commented for now as client discussion
				/*if(empty($modify['dnsResponse']['assign']['Error'])){
					$modify['dnsActivateResponse']= $dnsObj->activateDNRequest($dnPostArray);
				}*/
			}
			if($_SESSION['isDnsUnAssignUpdated'] == true){
				$dnPostArray['phoneNumber'] = $assignNumbersArray['unAssign'];
				$modify['dnsResponse']['unAssign'] = $dnsObj->unAssignGroupDNAssignRequest($dnPostArray);
			}
		}else{
			if($_SESSION['isDnsUnAssignUpdated'] == true){
				$assignNumbersArray = $dnsObj->getUpdatePostDns($dnPostArray['phoneNumber']);
				$dnPostArray['phoneNumber'] = $assignNumbersArray['unAssign'];
				$modify['dnsResponse']['unAssign'] = $dnsObj->unAssignGroupDNAssignRequest($dnPostArray);
			}
		}
                
                //Code added @ 23 May 2018                
                $successDNSStatus = "Yes";
                if(isset($modify['dnsResponse']['assign']['Error'])   && !empty($modify['dnsResponse']['assign']['Error']))
                    $successDNSStatus = "No";
                if(isset($modify['dnsResponse']['unAssign']['Error']) && !empty($modify['dnsResponse']['unAssign']['Error']))
                    $successDNSStatus = "No";
                
                
                if($successDNSStatus == "Yes")
                {
                    if(count($assignNumbersArray['newAssign']) > 0 || count($assignNumbersArray['unAssign']) > 0)
                    {
                        $oldValue = $_SESSION['groupInfoData']['assignNumbers']; 
                        $newValue = implode(",", $_POST['assignNumbers']); 
                        $cLUObj->createChangesArray($module = "assignNumbers", $oldValue, $newValue);
                    }
                }
                //End Code
	}
       
	// Rebuilt Error Message
	if( isset($modify['dnsResponse']['unAssign']['Error']) && 
				strpos($modify['dnsResponse']['unAssign']['Error'], "[Warning 4251]") !== false)
	{
            //$modify['dnsResponse']['unAssign']['Error'] = $modify['dnsResponse']['unAssign']['Error'] .", Phone number(s) assigned to users or group services. ";
            $errMsg = rtrim($modify['dnsResponse']['unAssign']['Error'], ".");
            $completeErrorMsg = $errMsg."; Phone number(s) assigned to users or group services. "; 
	    $modify['dnsResponse']['unAssign']['Error'] = $completeErrorMsg;
	}
	if($_SESSION['isServicePackAuthAssignUpdated'] == true || $_SESSION['isServicePackAuthUnAssignUpdated'] == true){
		$spPostArray['groupId'] = $groupIdVal;
		$servicePackAuth = array();
		if(array_key_exists("servicePackAuth", $_POST)){                    
                    
			foreach ($_POST['servicePackAuth'] as $key => $value){
				$arrayKey = array_keys($value);
				$arrayNewKey = str_replace("'", "", $arrayKey);
				$servicePackAuth[$key] = $arrayNewKey[0];
			}
		}else{
			$servicePackAuth = array();
		}
		$servicePackAuthObj = new Services();
		$spChanges = $servicePackAuthObj->getUpdateServicePackAuth($servicePackAuth);
		if(count($spChanges['newAuth']) > 0){
			$modify['servicePackResponse']['newAuth'] = $servicePackAuthObj->updateServicePackNewAuth($spChanges['newAuth'], $spPostArray['groupId']);
		}
		if(count($spChanges['unAuth']) > 0){
			$modify['servicePackResponse']['unAuth'] = $servicePackAuthObj->updateServicePackUnAuth($spChanges['unAuth'], $spPostArray['groupId']);
		}
                
                //Code added @ 25 May 2018
                $successSPStatus = "Yes";
                if(isset($modify['servicePackResponse']['newAuth']['Error']) && !empty($modify['servicePackResponse']['newAuth']['Error']))
                    $successSPStatus = "No";
                if(isset($modify['servicePackResponse']['unAuth']['Error']) && !empty($modify['servicePackResponse']['unAuth']['Error']))
                    $successSPStatus = "No";
                
                
                if($successSPStatus == "Yes")
                {
                    if(count($spChanges['newAuth']) > 0 || count($spChanges['unAuth']) > 0)
                    {
                        $oldValue = implode(",", $_SESSION['groupInfoData']['servicePackAuth']['auth']);
                        $newValue = implode(",", $servicePackAuth); 
                        $cLUObj->createChangesArray($module = "servicePackAuth", $oldValue, $newValue);
                    }
                }
                //End code
	}
	
	if($_SESSION['isGroupServiceAuthUpdated'] == true || $_SESSION['isGroupServiceUnAuthUpdated'] == true){
		$groupId['groupId'] = $groupIdVal;
		$groupAuth = array();
		
		if(array_key_exists("groupServiceAuth", $_POST)){
			foreach ($_POST['groupServiceAuth'] as $key => $value){
				$arrayKey = array_keys($value);
				$arrayNewKey = str_replace("'", "", $arrayKey);
				$groupAuth[$key] = $arrayNewKey[0];
			}
		}else{
			$groupAuth = array();
		}
		
		$groupServiceAuthObj = new Services();
		$gpAuthChanges = $groupServiceAuthObj->getGroupServiceAuth($groupAuth);
		if(count($gpAuthChanges['newGroupAuth']) > 0){
			$modify['groupServiceAuthResponse']['authorized'] = $groupServiceAuthObj->updateGroupServiceNewAuth($gpAuthChanges['newGroupAuth'], $groupId['groupId']);
		}
		if(count($gpAuthChanges['groupUnAuth']) > 0){
			$modify['groupServiceUnAuthResponse']['unAuthorized'] = $groupServiceAuthObj->updateGroupServiceUnAuth($gpAuthChanges['groupUnAuth'], $groupId['groupId']);
		}
                
                //Code added @ 25 May 2018
                $successSAuthStatus = "Yes";
                if(isset($modify['groupServiceAuthResponse']['authorized']['Error']) && !empty($modify['groupServiceAuthResponse']['authorized']['Error']))
                    $successSAuthStatus = "No";
                if(isset($modify['groupServiceUnAuthResponse']['unAuthorized']['Error']) && !empty($modify['groupServiceUnAuthResponse']['unAuthorized']['Error']))
                    $successSAuthStatus = "No";                
                
                if($successSAuthStatus == "Yes")
                {
                    if(count($gpAuthChanges['newGroupAuth']) > 0 || count($gpAuthChanges['groupUnAuth']) > 0)
                    {
                        $oldValue = implode(",", $_SESSION['groupInfoData']['groupServiceAuth']);
                        $newValue = implode(",", $groupAuth);
                        $cLUObj->createChangesArray($module = "groupServiceAuth", $oldValue, $newValue);
                    }
                }
                //End code
	}
	
	if($_SESSION['isGroupServiceAssignUpdated'] == true || $_SESSION['isGroupServiceUnassignUpdated'] == true){
		$groupId['groupId'] = $groupIdVal;
		$groupAssign = array();
		if(array_key_exists("groupServiceAssign", $_POST)){
			foreach ($_POST['groupServiceAssign'] as $key => $value){
				$arrayKey = array_keys($value);
				$arrayNewKey = str_replace("'", "", $arrayKey);
				$groupAssign[$key] = $arrayNewKey[0];
			}
		}else{
			$groupAssign = array();
		}
		
		$groupAssignObj = new Services();
		$groupAssignChanges = $groupAssignObj->getGroupServiceAssign($groupAssign);
		//echo "<pre>Assigned Group Services "; print_r($groupAssignChanges); die;
		if(count($groupAssignChanges['newGroupAssign']) > 0){
			
			$modify['groupServiceAssignResponse']['assigned'] = $groupAssignObj->updateGroupAssignService($groupAssignChanges['newGroupAssign'],$groupId['groupId']);
			if(in_array("Voice Messaging Group", $groupAssignChanges['newGroupAssign'])){
				$vpObj = new voicePortal();
				$voicePortalConfigArray['groupId'] = $groupIdVal;	
				$voicePortalConfigArray['sp'] = $sp;
				
				//code to assign voice portal
				require_once ("/var/www/lib/broadsoft/adminPortal/NameAndIDBuilder.php");
				require_once ("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
				
				//echo "<pre>"; print_r($_POST); die;
				
				$idRegex1 = "dt0<groupid>-dat1<groupdn>dat2-<grpclid>dat3-dat4@<defaultDomain>";
				$tokenStr = $idRegex1;
				
				$idValueMap = array();
				$idValueMap["GROUPID"] = $groupIdVal; // replace with group id
				if(isset($_POST['groupName']) && !empty($_POST['groupName'])){
					$idValueMap["GROUPNAME"] = $_POST['groupName']; // replace with group id
				}
				if(isset($_POST['callingLineIdDisplayPhoneNumber']) && !empty($_POST['callingLineIdDisplayPhoneNumber'])){
					$idValueMap["GROUPDN"] = $_POST['callingLineIdDisplayPhoneNumber']; // replace with group dn
				}
				if(isset($_POST['callingLineIdName']) && !empty($_POST['callingLineIdName'])){
					$idValueMap["GRPCLID"] = $_POST['callingLineIdName']; // replace with group clid
				}
				if(isset($_POST['defaultDomain']) && !empty($_POST['defaultDomain'])){
					$idValueMap["DEFAULTDOMAIN"] = $_POST['defaultDomain'];
				}
				
				$regexSet = array(
						ResourceNameBuilder::GRP_DN,
						ResourceNameBuilder::GRP_ID,
						ResourceNameBuilder::GRP_CLID,
						ResourceNameBuilder::DEFAULT_DOMAIN
				);
				
				$tokenStr = $idRegex1;
				
				$id2 = "<groupdn>-<groupid>@<defaultDomain>";		
				
				$nameAndIdBuilder = new NameAndIDBuilder($id2, $idValueMap, $regexSet);
				//$voicePortalConfigArray['voicePortalId'] = $nameAndIdBuilder->getParsedId($id2, $idValueMap);				
				
				$vpId = $vpConfig->get("voicePortalID");
				$vpIdRes = $nameAndIdBuilder->getParsedId($vpId, $idValueMap);
				if(isset($vpIdRes) && empty($vpIdRes)) { //use default id
					$vpId = "<groupid>-VP";
					$vpIdRes = $nameAndIdBuilder->getParsedId($vpId, $idValueMap);
				}
				$voicePortalConfigArray['voicePortalId'] = $vpIdRes; 
				
				$vpName = $vpConfig->get("voicePortalName");
				$vpNameRes = $nameAndIdBuilder->getParsedId($vpName, $idValueMap); 
				if(isset($vpNameRes) && empty($vpNameRes)) { //use default id
					$vpName = "<groupid>-VP-NAME";
					$vpNameRes= $nameAndIdBuilder->getParsedId($vpName, $idValueMap);
				}
				$voicePortalConfigArray['voicePortalName'] = $vpNameRes; 
				
				$vpClidFname = $vpConfig->get("clidFirstName");
				$vpClidFnameRes = $nameAndIdBuilder->getParsedId($vpClidFname, $idValueMap);
				if(isset($vpClidFnameRes) && empty($vpClidFnameRes)) { //use default id
					$vpClidFname = "<groupid>-VP-CLID-FNAME";
					$vpClidFnameRes= $nameAndIdBuilder->getParsedId($vpClidFname, $idValueMap);
				}
				$voicePortalConfigArray['voicePortalCLIDFName'] = $vpClidFnameRes;
				
				$vpClidLname = $vpConfig->get("clidLastName");
				$vpClidLnameRes = $nameAndIdBuilder->getParsedId($vpClidLname, $idValueMap);
				if(isset($vpClidLnameRes) && empty($vpClidLnameRes)) { //use default id
					$vpClidLname = "<groupid>-VP-CLID-LNAME";
					$vpClidFnameRes = $nameAndIdBuilder->getParsedId($vpClidLname, $idValueMap);
				}
				$voicePortalConfigArray['voicePortalCLIDLName'] = $vpClidLnameRes; 				
				
				$modify['voicePortalResponse'] = $vpObj->setVoicePortalAssignRequestWhenAssign($voicePortalConfigArray);
				
				if(empty($modify['voicePortalResponse']['Error'])){
					$assignedVpData = $modifyGroup->getVoicePortalGroupResponse();
					if(empty($assignedVpData['Error'])){
						$modifiyVpId['userId'] = $assignedVpData['Success']['serviceUserId'];
						$modifiyVpId['newUserId'] = str_replace(" ", "", $vpIdRes);
						$modifyIdResponse = $vpObj->modifyVoicePortalId($modifiyVpId);
						if(empty($modifyIdResponse['voicePortalResponse']['Error'])){
							$modify['voicePortalResponse']['Success'] .= "<br/>Voice Portal ID Updated Sucuessfully"; 
						}
					}
					
				}				
				//code ends
				//$vpObj->setVoicePortalModifyRequestWhenAssign($voicePortalConfigArray, $vpConfig);
			}
		}
		if(count($groupAssignChanges['GroupUnAssign']) > 0){
			$modify['groupServiceAssignResponse']['usAssigned'] = $groupAssignObj->updateGroupUnAssignService($groupAssignChanges['GroupUnAssign'],$groupId['groupId']);
		}
                
                //Code added @ 25 May 2018
                $successGrpSrvAssgnStatus = "Yes";                
                if(isset($modify['groupServiceAssignResponse']['usAssigned']['Error']) && !empty($modify['groupServiceAssignResponse']['usAssigned']['Error']))
                    $successGrpSrvAssgnStatus = "No";             
                
                if($successGrpSrvAssgnStatus == "Yes")
                {
                    if(count($groupAssignChanges['newGroupAssign']) > 0 || count($groupAssignChanges['GroupUnAssign']) > 0)
                    {
                        $oldValue = implode(",", $_SESSION['groupInfoData']['groupServiceAssign']);
                        $newValue = implode(",", $groupAssign);
                        $cLUObj->createChangesArray($module = "groupServiceAssign", $oldValue, $newValue);
                    }
                }
                //End code
	}
	
	if($_SESSION['isUserServiceAuthUpdated'] == true || $_SESSION['isUserServiceUnAuthUpdated'] == true){
		$groupId['groupId'] = $groupIdVal;
		$userAssign = array();
		if(array_key_exists("userServiceAuth", $_POST)){
			foreach ($_POST['userServiceAuth'] as $key => $value){
				$arrayKey = array_keys($value);
				$arrayNewKey = str_replace("'", "", $arrayKey);
				$userAssign[$key] = $arrayNewKey[0];
			}
		}else{
			$userAssign = array();
		}
		$userAuthObj = new Services();
		$userAssignChange = $userAuthObj->getUserServiceAuth($userAssign);
		if(count($userAssignChange['newUserAuth']) > 0){
			$modify['userAuthResponse']['authorized'] = $userAuthObj->updateUserAuthService($userAssignChange['newUserAuth'], $groupId['groupId']);
		}
		if(count($userAssignChange['userUnAuth']) > 0){
			$modify['userUnAuthResponse']['Unauthorized'] = $userAuthObj->updateUserUnAuthService($userAssignChange['userUnAuth'], $groupId['groupId']);
		}
                
                //Code added @ 25 May 2018
                $successUsrAuthResStatus = "Yes";
                if(isset($modify['userAuthResponse']['authorized']['Error']) && !empty($modify['userAuthResponse']['authorized']['Error']))
                    $successUsrAuthResStatus = "No";
                if(isset($modify['userUnAuthResponse']['Unauthorized']['Error']) && !empty($modify['userUnAuthResponse']['Unauthorized']['Error']))
                    $successUsrAuthResStatus = "No";             
                
                if($successUsrAuthResStatus == "Yes")
                {
                    if(count($groupAssignChanges['newGroupAssign']) > 0 || count($groupAssignChanges['GroupUnAssign']) > 0)
                    {
                        $oldValue = implode(",", $_SESSION['groupInfoData']['userServiceAuth']);
                        $newValue = implode(",", $userAssign);
                        $cLUObj->createChangesArray($module = "userServiceAuth", $oldValue, $newValue);
                    }
                }
                //End code
	}
	
	//echo "<pre>"; print_r($_SESSION); die;        
        $assignedNCS = $unAssignedNCS = "";
	if($_SESSION['isNcsAssigned'] == true || $_SESSION['isNcsDefaultAssigned'] == true){
		$groupId['groupId'] = $groupIdVal;
		$ncsAssign = array();
		$ncsAssignD = array();
		$ncsAsgn = array();
		$ncsAssignDefault = array();
		if(array_key_exists("assignedNCS", $_POST)){
			foreach ($_POST['assignedNCS'] as $key => $value){
				$ncsAssign[] = $value;
			}
		}else{
			$ncsAssign = array();
		}
		if(array_key_exists("assignedDefault", $_POST)){
			foreach ($_POST['assignedDefault'] as $key => $value){
				$ncsAssignD[] = $value;
			}
		}else{
			$ncsAssignD = array();
		}
		$ncsAsgnObj = new Services();
		$ncsAsgn = $ncsAsgnObj->compareAssignedNcs($ncsAssign);
		$ncsAssignDefault = $ncsAsgnObj->compareAssignedDefaultNcs($ncsAssignD);
		if(count($ncsAsgn['newAssign']) > 0 || count($ncsAssignDefault['newAssign'])){
			$modify['ncsAssignResponse']['assigned'] = $ncsAsgnObj->updateNcsAssignOrDefault($ncsAsgn['newAssign'], $ncsAssignDefault['newAssign'], $groupId['groupId']);
                }
                
                //Code added @ 24 May 2018
                if(count($ncsAsgn['newAssign']) > 0){
                        $assignedNCS = "Yes";
                }
                
                
                $successNCSAssgnStatus = "Yes";
                if(isset($modify['ncsAssignResponse']['assigned']['error']) && !empty($modify['ncsAssignResponse']['assigned']['error']))
                    $successNCSAssgnStatus = "No";
                
                if($successNCSAssgnStatus == "Yes")
                {               
                    if(count($ncsAssignDefault['newAssign'])>0 || count($ncsAssignDefault['unAssign'])>0 )
                    {
                        $oldValue = implode(",", $_SESSION['groupInfoData']['assignedDefault']); 
                        $newValue = implode(",", $_POST['assignedDefault']); 
                        $cLUObj->createChangesArray($module = "assignedDefault", $oldValue, $newValue);
                    }
                }
                //End Code
	}
        
	if($_SESSION['isNcsUnAssigned'] == true){
		$groupId['groupId'] = $groupIdVal;
		$ncsAsgn = array();
		$ncsAssign = array();
		if(array_key_exists("assignedNCS", $_POST)){
			foreach ($_POST['assignedNCS'] as $key => $value){
				$ncsAssign[] = $value;
			}
		}else{
			$ncsAssign = array();
		}
		$ncsUnAssignObj = new Services();
		$ncsAsgn = $ncsUnAssignObj->compareAssignedNcs($ncsAssign);
		$modify['ncsUnassignResp']['unAssigned'] = $ncsUnAssignObj->updateNcsUnassign($ncsAsgn['unAssign'], $groupId['groupId']);
                
                //Code added @ 24 May 2018
                if(count($ncsAsgn['unAssign']) > 0){
                    $unAssignedNCS = "Yes"; 
                }
                //End Code
                
	}
        
        //Code added @ 24 May 2018
        $successUnAsignStatus = "";
        if(isset($modify['ncsUnassignResp']['unAssigned']['error']) && !empty($modify['ncsUnassignResp']['unAssigned']['error']))
            $successUnAsignStatus = "No";

        if($successUnAsignStatus != "No")
        {    
            if($assignedNCS=="Yes" || $unAssignedNCS=="Yes")
            {
                $oldValue = implode(",", $_SESSION['groupInfoData']['assignedNCS']); 
                $newValue = implode(",", $_POST['assignedNCS']); 
                $cLUObj->createChangesArray($module = "assignedNCS", $oldValue, $newValue);
            }
        }
        //End code
	
	$voicePortalPostArray = array();
	if($_SESSION['isVoicePortalUpdated'] == true){
		$vpObj = new voicePortal();
		$voicePortalPostArray['sp'] = $_SESSION['sp'];
		$voicePortalPostArray['portalLanguage'] = $_POST['portalLanguage'];
		$voicePortalPostArray['portalTimeZone'] = $_POST['portalTimeZone'];
		$voicePortalPostArray['portalPhoneNo'] = $_POST['portalPhoneNo'];
		$voicePortalPostArray['portalExt'] = $_POST['portalExt'];
		$voicePortalPostArray['groupId'] = $_POST['groupId'];
                $voicePortalPostArray['voicePortalActiveStatus'] = $_POST['voicePortalActiveStatus'];
		
		$modify['voicePortalResponse'] = $vpObj->setVoicePortalModifyRequest($voicePortalPostArray);
                
	}
	if(!empty($_SESSION["tabName"])){
		$modify["tabName"] = $_SESSION["tabName"];
	}
	
	if(array_key_exists("activateAllAssigned", $_POST)){
		if(count($_POST['assignNumbers']) > 0){
			$dnPostArray['phoneNumber'] = $_POST['assignNumbers'];
			$dnPostArray['groupId'] = $groupIdVal;
			$modify['dnsActivateResponse'] = $dnsObj->activateDNRequest($dnPostArray);
		}
		
	}
        //Code added @ 23 May 2018        
        if($infoChange=="Yes" || $successDomainStatus=="Yes" || $successDNSStatus=="Yes" || $successSPStatus=="Yes" || $successSAuthStatus=="Yes" || $successGrpSrvAssgnStatus=="Yes" || $successUsrAuthResStatus=="Yes" || $successNCSAssgnStatus=="Yes" || $successUnAsignStatus=="Yes")
        {
            $module = "Group Modify";
            $cLUObj->changeLogModifyUtility($module, $_POST['groupName'], $tableName = "groupModChanges");
        }
        //End Code
	
	echo json_encode($modify);
}
?>