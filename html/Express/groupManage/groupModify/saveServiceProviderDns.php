<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");

//Code added @ 28 Jan 2019
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
$expProvDB = new ExpressProvLogsDB();
$groupId = isset($_SESSION['groupId']) ? $_SESSION['groupId'] : "None" ;
$cLUObj = new ChangeLogUtility($_SESSION["sp"], $groupId, $_SESSION["loggedInUserName"]);
//End code


$postPhoneNumber = array();
$postRangePhoneNumber = array();

//echo "<pre>"; print_r($_POST); die;
if(((isset($_POST['minPhoneNumber']) && count($_POST['minPhoneNumber']) > 0) && (isset($_POST['maxPhoneNumber']) && count($_POST['maxPhoneNumber']) > 0)) || (isset($_POST['phoneNumber']) && count($_POST['phoneNumber']) > 0)){
	$info = new Dns();
	
	$addDnsResponse = $info->setDNServiceProviderRequest($_POST);      
    if(empty($addDnsResponse['Error'])){
          $phoneNumbers = "";
                $minPhoneNumbers = "";
                $maxPhoneNumbers = "";
                if(!empty($_POST['phoneNumber'])){
                    $phoneNumbers = implode(",", $_POST['phoneNumber']);
                }
                 if(!empty($_POST['minPhoneNumber'])){
                    $minPhoneNumbers = implode(",", $_POST['minPhoneNumber']);
                }

                if(!empty($_POST['maxPhoneNumber'])){
                    $maxPhoneNumbers = implode(",", $_POST['maxPhoneNumber']);
                }


                $changeLogArr  = array(
                'serviceId'             => $_SESSION["sp"],
                'phoneNumbers'          => $phoneNumbers,
                'minPhoneNumber'        => $minPhoneNumbers, 
                'maxPhoneNumber'        => $maxPhoneNumbers
            ); 

            $module         = "Add DNs";
            $tableName      = "addDNsChanges";
            $entityName     = $_SESSION["sp"];
            $changeResponse = $cLUObj->changeLogAddUtility($module, $entityName, $tableName, $changeLogArr);                    
            //End code
        }
        
	echo json_encode($addDnsResponse);
}
?>