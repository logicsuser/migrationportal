<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupOperation.php");


if(!empty($_POST['deleteGroup'])){
	$deleteGroup['previousGroupIsDeleted'] = "";
	$sp = $_SESSION['sp'];
	//$groupId = htmlspecialchars($_POST['groupId']);
	$groupId = $_POST['groupId'];
	//initialize class
	$group = new GroupOperation($sp, $groupId);
	
	$postArray['groupId'] = $groupId;
	$deleteGroup = $group->getDeleteGroup($postArray);
        
        if($deleteGroup['Error'] == ""){
            if(trim($groupId) == trim($_SESSION["previousGroupId"])){
                unset($_SESSION["previousGroupId"]);
                $deleteGroup['previousGroupIsDeleted'] = "true";
            }
        }        
	echo json_encode($deleteGroup);
}
?>