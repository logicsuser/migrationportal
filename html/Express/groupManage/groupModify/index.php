<?php 
session_start();
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/arrays.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ocpFeatures/OCPOperations.php");


$basicInformationLogsArr                = explode("_", $_SESSION["permissions"]["basicInformation"]);
$basicInformationLogPermission          = $basicInformationLogsArr[0];
$basicInformationLogModPermission       = $basicInformationLogsArr[1];
$basicInformationLogAddDelPermission    = $basicInformationLogsArr[2];

$voicePortalServiceLogsArr              = explode("_", $_SESSION["permissions"]["voicePortalService"]);
$voicePortalServiceLogPermission        = $voicePortalServiceLogsArr[0];
$voicePortalServiceLogModPermission     = $voicePortalServiceLogsArr[1];

$outgoingCallingPlanLogsArr             = explode("_", $_SESSION["permissions"]["outgoingCallingPlan"]);
$outgoingCallingPlanLogPermission       = $outgoingCallingPlanLogsArr[0];
$outgoingCallingPlanLogModPermission    = $outgoingCallingPlanLogsArr[1];

$networkClassesofServiceLogsArr         = explode("_", $_SESSION["permissions"]["networkClassesofService"]);
$networkClassesofServiceLogPermission   = $networkClassesofServiceLogsArr[0];
$networkClasesofServcLogModPermission   = $networkClassesofServiceLogsArr[1];

$callProcessingPoliciesLogsArr           = explode("_", $_SESSION["permissions"]["callProcessingPolicies"]);
$callProcessingPoliciesLogPermission     = $callProcessingPoliciesLogsArr[0];
$callProcessingPoliciesLogModPermission  = $callProcessingPoliciesLogsArr[1];

$securityDomainsLogsArr                  = explode("_", $_SESSION["permissions"]["securityDomains"]);
$securityDomainsLogPermission            = $securityDomainsLogsArr[0];
$securityDomainsLogModPermission         = $securityDomainsLogsArr[1];

$servicesLogsArr                         = explode("_", $_SESSION["permissions"]["services"]);
$servicesLogPermission                   = $servicesLogsArr[0];
$servicesLogModPermission                = $servicesLogsArr[1];

$domainsLogsArr                          = explode("_", $_SESSION["permissions"]["domains"]);
$domainsLogPermission                    = $domainsLogsArr[0];  
$domainsLogModPermission                 = $domainsLogsArr[1];  

$dnsLogsArr                              = explode("_", $_SESSION["permissions"]["DNs"]);
$dnsLogPermission                        = $dnsLogsArr[0];
$dnsLogModPermission                     = $dnsLogsArr[1];
$dnsLogAddDelPermission                  = $dnsLogsArr[2];

?>

<input type="hidden" value="" id="isGroupAccess">
<input type="hidden" name="modifyGroupId" id="modifyGroupId"
	value="<?php if(isset($_POST['groupId']) && !empty($_POST['groupId'])){echo $_POST['groupId'];}?>" />



<input type="hidden" value="<?php echo $basicInformationLogModPermission ?>"       id="basicInformationPrmns" />
<input type="hidden" value="<?php echo $voicePortalServiceLogModPermission ?>"     id="voicePortalServicePrmns" />
<input type="hidden" value="<?php echo $outgoingCallingPlanLogModPermission ?>"    id="outgoingCallingPlanPrmns" />
<input type="hidden" value="<?php echo $networkClasesofServcLogModPermission ?>"   id="networkClassesofServicePrmns" />
<input type="hidden" value="<?php echo $callProcessingPoliciesLogModPermission ?>" id="callProcessingPoliciesPrmns" />
<input type="hidden" value="<?php echo $securityDomainsLogModPermission ?>"        id="securityDomainsPrmns" />
<input type="hidden" value="<?php echo $servicesLogModPermission ?>"               id="servicesPrmns" />
<input type="hidden" value="<?php echo $domainsLogModPermission ?>"                id="domainsPrmns" />
<input type="hidden" value="<?php echo $dnsLogAddDelPermission ?>"                 id="DNsPrmns" />


<style>
    .disabledulli{
    cursor:none !important;
    opacity:0.4;
}

.newCheckBoxImg:checked + label span {
   /* background: url("/Express/icons/polycom_radio.png") no-repeat;*/
    background: url("/Express/icons/green_button.png") no-repeat !important;
    width: 35px !important;
    height: 35px !important;
    margin: -2px 5px 0 0 !important;
    vertical-align: middle;
    cursor: pointer;
}
.newCheckBoxImg + label span {
    display: inline-block;
    background: url("/Express/icons/radial_inactive.png") no-repeat !important;
    width: 35px !important;
    height: 35px !important;
    margin: -2px 5px 0 0 !important;
    vertical-align: middle;
    cursor: pointer;
}

.dnsButtonCenter{padding-top: 175px !important;}
.addAvalableRangeDnsbutton{padding-top: 235px;}
</style>
<script type="text/javascript">
	$(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();         
                //Code added @ 18 March 2019
                if ("<?php echo $basicInformationLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#basic_info :input").prop("disabled", true);
                            $("#basic_info option").prop("disabled", true);
                            $("#basic_info span").prop("disabled", true);
                            $("#basic_info :input").css("background", "#e3e3e3");        
                }
                if ("<?php echo $voicePortalServiceLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#voicePortalServices_1 :input").prop("disabled", true);                                                
                            $("#voicePortalServices_1 span").prop("disabled", true);
                            $("#voicePortalServices_1 :input").css("background", "#e3e3e3"); 
                }
                if ("<?php echo $outgoingCallingPlanLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#ocp_1 :input").prop("disabled", true);
                            $("#ocp_1 option").prop("disabled", true);
                            $("#ocp_1 span").prop("disabled", true);
                            $("#ocp_1 :input").css("background", "#e3e3e3");        
                }
                if ("<?php echo $networkClasesofServcLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#networkServices_1 :input").prop("disabled", true);
                            $("#networkServices_1 span").prop("disabled", true);
                            $("#networkServices_1 :input").css("background", "#e3e3e3");
                            $("#networkServices_1 li").addClass('disabledulli');
                }
                if ("<?php echo $callProcessingPoliciesLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#callPolicy_1 :input").css("background", "#e3e3e3");
                            $("#callPolicy_1 :input[type=checkbox]").addClass('disabledulli');
                            $("#callPolicy_1 :input[type=radio]").addClass('disabledulli');
                            $("#callPolicy_1 span").addClass('disabledulli');
                }
                if ("<?php echo $servicesLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#services_1 :input").prop("disabled", true);                            
                            $("#services_1 span").prop("disabled", true);
                            $("#services_1 :input").css("background", "#e3e3e3");
                            $("#services_1 :input[type=checkbox]").addClass('disabledulli');
                            $("#services_1 :input[type=radio]").addClass('disabledulli');
                            $("#services_1 span").addClass('disabledulli');
                }
                if ("<?php echo $domainsLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#domain_1 :input").prop("disabled", true);                                            
                            $("#domain_1 #sortable_1").addClass('disabledulli');
                            $("#domain_1 #sortable_2").addClass('disabledulli');
                            $("#domain_1 #sortable_1").addClass('disabled');
                            $("#domain_1 #sortable_2").addClass('disabled');
                            $("#domain_1 :input").css("background", "#e3e3e3");
                            $("#domain_1 li").addClass('disabledulli');
                }
                if ("<?php echo $dnsLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#dnsModify button").prop("disabled", true);
                            $("#dnsModify :input").css("background", "#e3e3e3");
                            $("#dnsModify :input[type=checkbox]").addClass('disabledulli');
                            $("#dnsModify :input[type=radio]").addClass('disabledulli');
                            $("#dnsModify span").addClass('disabledulli');
                            $("#dnsModify li").addClass('disabled');
                            
                }

                //End code
                        
                        
	});
	var groupId = "<?php if(isset($_SESSION['groupId'])){ echo $_SESSION['groupId'];} ?>";
	if(groupId){
		$("#isGroupAccess").val(groupId);
	}
	var enterpriseLevel = "<?php if(isset($_SESSION['groupId'])){ echo "false";}else{ echo "true"; } ?>";

</script>

<script type="text/javascript"
	src="groupManage/groupModify/js/groupModify.js"></script>
<script type="text/javascript"
	src="groupManage/groupModify/groupDept/js/department.js"></script>
		
<style>
#groupMainPage {
    min-width: 0px !important;
}

#groupDetails {
	display: none;
}
.removeBdr{
    border: none;
}
.clr2 {
    margin-bottom: 20px;
}
</style>
<?php

if (isset ( $_SESSION ['groupId'] )) {
	?>

<h2 class="adminUserText">Modify Group</h2>
<?php
} else {
	?>
<h2 class="adminUserText">Groups</h2>
<?php
}
?>
<div class="">
    <input type="hidden" id="adminUserType" value="<?php echo $_SESSION["superUser"]; ?>" />
    <input type="hidden" id="clusterNameForGroupMod" value="<?php echo $_SESSION["selectedCluster"]; ?>" />
	<form name="groups" id="groupsFormId" method="POST" class="fcorn-registerTwo" action="#">
				<div class="row">
						<div class="col-md-11 adminSelectDiv">
							<div class="form-group">
								<label class="labelText">Select Group to view or modify</label><br />
								<div class="existingGroup">
									<!-- select name="selectGroup" id="selectGroup"></select-->
									<input type="text" class="autoFill magnify" name="selectGroup"
										id="selectGroup">
										<div id="hidden-stuffGroup" style="height: 0px;"></div>
								</div>
								
							</div>
						</div>

				<?php 
                                    //if ($_SESSION["superUser"] == "1") { 
                                    if ($basicInformationLogPermission == "1" && $basicInformationLogAddDelPermission == "yes") {
                                    ?>
					<div class="col-md-1 adminAddBtnDiv">
						<div class="form-group">
								<p class="register-submit addAdminBtnIcon">
								<img src="images\icons\add_admin_rest.png" data-alt-src="images\icons\add_admin_over.png" name="newGroup" id="newGroup">
								<label class="labelText labelTextOpp"><span>Add</span><span style="margin-left: 18px;">Group</span></label>
									<!-- select style="width: 120px; display:none" name="groupName" id="groupName"></select-->
								</p>
						</div>
					</div>
				<?php } ?>
				<div id="groupMainPageP" style="display: none;text-align: center;margin-top: 50px;" class="col-md-12">
									<input name="groupMainPage" value="Group Main Page" id="groupMainPage" class="cancelButton" type="button">
								</div>
				</div>
				
				<div class="labelTextGrey" style="padding-top: 15px;text-align: center;" id="modifyGrouptitle">
				<p class="subBannerCustom register-submit selectedGroup"><p>
						
				</div>
				
				

	</form>
			<div class="loading" id="loading2">
				<img src="/Express/images/ajax-loader.gif">
			</div>
			<?php if (($_SESSION["superUser"] == "1") || ($_SESSION["permissions"]["groupBasicInfo"] == "1") || ($_SESSION["permissions"]["groupDomains"] == "1") || ($_SESSION["permissions"]["groupServices"] == "1") || ($_SESSION["permissions"]["groupDN"] == "1") || ($_SESSION["permissions"]["groupPolicies"] == "1") || ($_SESSION["permissions"]["groupNCOS"] == "1") || ($_SESSION["permissions"]["groupVoicePortal"] == "1") 
                                 || ($basicInformationLogModPermission == "yes") || $voicePortalServiceLogPermission == "1" || $outgoingCallingPlanLogPermission == "1" ||  $networkClassesofServiceLogPermission == "1" || $callProcessingPoliciesLogPermission == "1" || $securityDomainsLogPermission == "1" || $servicesLogPermission == "1" || $domainsLogPermission == "1" || $dnsLogPermission == "1") { ?>

			<div id="groupDetails">
				<div id="tabs">
					<div class="divBeforeUl">
							<ul class="groupModifyTab" style="margin:0 auto !important">
									<?php 
			if($_SESSION["superUser"] == "1" || (isset($_SESSION["permissions"]["groupBasicInfo"]) || $basicInformationLogPermission == "1")){
			 ?> 
			 
			    <li><a href="#basic_info" class="tabs" id="basicInfo">Basic Info</a></li>
			 <?php
			    
			}
			
			
			if($_SESSION["superUser"] == "1" || ($domainsLogPermission == "1") || (isset($_SESSION["permissions"]["groupDomains"]) && $_SESSION["permissions"]["groupDomains"] == "1")){
			    ?>
			   	<li><a href="#domain_1" class="tabs" id="domain">Domains</a></li>
			 <?php
			    
			}
			
			
			if($_SESSION["superUser"] == "1" || ($servicesLogPermission == "1") ||  (isset($_SESSION["permissions"]["groupServices"]) && $_SESSION["permissions"]["groupServices"] == "1")){
		    ?>
			    <li><a href="#services_1" class="tabs" id="services">Services</a></li>
			 <?php
			    
			}
			
			if($_SESSION["superUser"] == "1" || ($dnsLogPermission == "1") || (isset($_SESSION["permissions"]["groupDN"]) && $_SESSION["permissions"]["groupDN"] == "1")){
			    ?>
			   <li><a href="#dns_1" class="tabs" id="dns">DNs</a></li>
			 <?php
			    
			}
			
		  if($_SESSION["superUser"] == "1" || ($callProcessingPoliciesLogPermission == "1") || (isset($_SESSION["permissions"]["groupPolicies"]) && $_SESSION["permissions"]["groupPolicies"] == "1")){
			    ?>
			    <li><a href="#callPolicy_1" class="tabs" id="callPolicy">Call
 						Processing Policies</a></li>

			 <?php
			    
			}
			
			if($_SESSION["superUser"] == "1" || ($networkClassesofServiceLogPermission == "1") || isset($_SESSION["permissions"]["groupNCOS"]) && $_SESSION["permissions"]["groupNCOS"] == "1"){
			    ?>
			   <li><a href="#networkServices_1" class="tabs" id="networkServices">Network
 						Classes of Service</a></li>

			 <?php
			    
			}
			
			if($_SESSION["superUser"] == "1" || ($voicePortalServiceLogPermission == "1") || isset($_SESSION["permissions"]["groupVoicePortal"]) && $_SESSION["permissions"]["groupVoicePortal"] == "1"){
			    ?>
			   <li><a href="#voicePortalServices_1" class="tabs"
 					id="voicePortalServices">Voice Portal Service</a></li>
			 <?php
			    
			}
			?>
			<?php if($useDepartments== "true") { ?><li><a href="#departmentGroup_1" class="tabs" id="departmentGroup">Departments</a></li><?php } ?>
			
					<?php 
						//Code @ 20 Aug 2018 for Outgoing Calling Plan                            
						//if(in_array("Outgoing Calling Plan", $_SESSION["groupInfoData"]["groupServiceAssign"]) && isset($_SESSION["permissions"]["ocpGroup"]) && $_SESSION["permissions"]["ocpGroup"] == "1")
						if((isset($_SESSION["permissions"]["ocpGroup"]) && $_SESSION["permissions"]["ocpGroup"] == "1") || ($outgoingCallingPlanLogPermission == "1") || $_SESSION["superUser"] == "1")
						{
							// echo "<li id='ocp_div' style='display:none;'><a href=\"#ocp_1\" class=\"tabs\" id=\"ocpFeatures\">Outgoing Calling Plan</a></li>"; 
                                                        echo "<li id='ocp_div'><a href=\"#ocp_1\" class=\"tabs\" id=\"ocpFeatures\">Outgoing Calling Plan</a></li>";
						}
					?> 
						
						</ul>
					</div>
					<form name="groupModify" id="groupModify" class="groupModifyCls" method="POST">
						<input type="hidden" name="tabName" value="" id="tabName"> <input
							type="hidden" name="operation" value="" id="operation">
						<div id="basic_info" class="userDataClass basicInfoFrm" style="display: none;">
							<h2 class="adminUserText">Basic Info</h2>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="groupId">Group Id:</label> 
											<span class="required">*</span><br /> <input type="text"
												name="groupId" id="groupId" value=""><span id="spanGroupId" class="labelTextGrey" style="margin-left:11px;"></span>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="groupName">Group Name:</label><br />
											<input type="text" name="groupName" id="groupName" value="">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="defaultDomain">Default Domain:</label><br />
											<div class="dropdown-wrap">   
												<select name="defaultDomain" id="defaultDomain"></select>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="timeZone">Time Zone:</label>
											<span class="required">*</span>
											<br />
											<div class="dropdown-wrap">   
												<select name="timeZone" id="timeZone"></select>
											</div>
										</div>
									</div>
								</div>
							</div>
				<!--New code found start-->
							<?php 
							if($useExtensionLengthRange == "true") {
    						?>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
										
											<label class="labelText" for="minExtensionLength">Extension Length(Min):</label>
											<span class="required">*</span>
											<br />
											<div class="dropdown-wrap">   
												<select name="minExtensionLength" id="minExtensionLength"></select>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										 
											<label class="labelText" for="maxExtensionLength">Extension Length(Max):</label>
											<span class="required">*</span>
											<br />
											<div class="dropdown-wrap">   
												<select name="maxExtensionLength" id="maxExtensionLength"></select>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										  	<label class="labelText" for="defaultExtensionLength">Extension Length(Default):</label>
										  	<span class="required">*</span>
											  <br />
											  <div class="dropdown-wrap">   
													<select name="defaultExtensionLength"
												id="defaultExtensionLength"></select>
											</div>
										</div>
									</div>
								</div>
							</div> 
						<?php 
    						  } else {
						?>
						<div class="row">
						<div class="col-md-6">
							<div class="form-group">
                                                                <label class="labelText" for="timeZone">Extension Length:</label>
                                                                 <span class="required">*</span>
                                                                        <br />
								<div class="dropdown-wrap">
								<?php 
								$extLength = $_SESSION['groupInfoData']['extensionLength'];
								?>
									<select name="extensionLength" id="extensionLength">
										<option value=""></option>
										<option <?php echo $extLength == "2"?"selected":""?> value="2">2</option>
										<option <?php echo $extLength == "3"?"selected":""?> value="3">3</option>
										<option <?php echo $extLength == "4"?"selected":""?> value="4">4</option>
										<option <?php echo $extLength == "5"?"selected":""?> value="5">5</option>
										<option <?php echo $extLength == "6"?"selected":""?> value="6">6</option>
										<option <?php echo $extLength == "7"?"selected":""?> value="7">7</option>
									</select>
								</div>
							</div>
						</div>
    						<div class="col-md-6">
    							<div class="form-group"></div>
    						</div>
    					</div>
    					 <?php }?>	
						<!-- User Limit code strat here -->  
							<div class="row">
								<div class="">
									
									<div class="col-md-6">
										
										<?php if($userLimitGroup == "true"){?>
											<div class="form-group">
												<label class="labelText" for="userLimit">User Limit:</label><br />
												<input type="text" name="userLimit" id="userLimit"
												value="999999">
											</div>
											<div class="form-group">
												<label class="labelText" for="userLimit">Current Number of Users:</label><br/>
												<!-- <div id="userCount" style="padding: 8px;"></div>  -->
												<span id="userCount" class="labelTextGrey" style="margin-left:11px;"></span>                                                      
											</div>
														
										<?php }else{?>
											<input type="hidden" name="userLimit" id="userLimit" value="" />
										<?php }?>
									</div>
								</div>
							</div>
					    <!-- User limit code end here --> 
	<!--New code found End-->
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group"></div>
									</div>
									<div class="col-md-6">
										<div class="form-group"></div>
									</div>
								</div>
							</div>
							<h2 class="adminUserText">Calling Line ID</h2>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="callingLineIdName">Calling Line
												ID Name:</label><br /> <input type="text"
												name="callingLineIdName" id="callingLineIdName" size="30"
												maxlength="30" value="">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="locationDialingCode">Location
												Dialing Code Number:</label><br /> <input type="text"
												name="locationDialingCode" id="locationDialingCode"
												size="15" maxlength="15" value="">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="callingLineIdName">Calling Line
												ID Group Number:</label><br /> 
												<div class="dropdown-wrap">   
												<select	name="callingLineIdDisplayPhoneNumber" id="callingLineIdDisplayPhoneNumber"></select>
										</div>	
									</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">&nbsp;</div>
									</div>
								</div>
							</div>

							<h2 class="adminUserText">Contact Information</h2>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="contactName">Contact Name:</label><br />
											<input type="text" name="contactName" id="contactName"
												size="30" maxlength="30" value="">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="contactEmail">Contact Phone:</label><br />
											<input type="text" name="contactNumber" id="contactNumber"
												size="30" maxlength="30" value="">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="contactEmail">Contact E-mail:</label><br />
											<input type="text" name="contactEmail" id="contactEmail"
												size="30" maxlength="30" value="">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">&nbsp;</div>
									</div>
								</div>
							</div>
							
							<h2 class="adminUserText">Address Information</h2>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="addressLine1">Address Line1:</label><br />
											<input type="text" name="addressLine1" id="addressLine1"
												size="30" maxlength="30" value="">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="addressLine2">Address Line2:</label><br />
											<input type="text" name="addressLine2" id="addressLine2"
												size="30" maxlength="30" value="">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="city">City:</label><br /> <input
												type="text" name="city" id="city" size="30" maxlength="30"
												value="">
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="stateOrProvince">State /
												Province:</label><br /> 
												<div class="dropdown-wrap">
                									<select name="stateOrProvince" id="stateOrProvince"></select>
                								</div>
										</div>
									</div>
									
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="zipOrPostalCode">Zip / Postal
												Code:</label><br /> <input type="text"
												name="zipOrPostalCode" id="zipOrPostalCode" size="30"
												maxlength="30" value="">
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="country">Country:</label><br />
											<input type="text" name="country" id="country" size="30"
												maxlength="30" value="">
										</div>
									</div>
								 
							</div>
							<h2 class="adminUserText">Routing Profile</h2>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="routingProfile">Routing
												Profile:</label><br />
												<div class="dropdown-wrap">   
													<select name="routingProfile"
												id="routingProfile"></select>
												</div>
										</div>
									</div>
								</div>
							</div>
						</div><!-- end basic info div -->
						<div id="domain_1" class="userDataClass" style="display: none;">
							<h2 class="adminUserText">Available Domains
                                                            <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Drag and drop items back and forth"><img src="images/NewIcon/info_icon.png" /></a>
							</h2>
                                                            <div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignCenter">
											<label class="labelTextGrey" style="text-align: center;">
                                                                                            Available Domains
                                                                                            <!-- <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="You can drag things back and forth"><img src="images/NewIcon/info_icon.png" /></a>  -->
                                                                                        </label><br />
											<div class="form-group scrollableListCustom1">
												<ul id="sortable_1" class="connectedSortable ui-sortable"></ul>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group alignCenter">
											<label class="labelTextGrey" style="text-align: center;">
                                                                                                Assigned Domains
                                                                                                <!-- <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="You can drag things back and forth"><img src="images/NewIcon/info_icon.png" /></a> -->
                                                                                        </label><br />
											<div class="form-group scrollableListCustom2">
												<ul id="sortable_2" class="connectedSortable ui-sortable"></ul>
												<input type="hidden" name="domainAssigned"
													id="domainAssigned" value="" />
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row" id="securityDiv">
								<h2 class="adminUserText">Security Domains</h2>
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignCenter">
											<label class="labelTextGrey" style="text-align: center;">Available
												Domains
												<!-- <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="You can drag things back and forth"><img src="images/NewIcon/info_icon.png" /></a> -->
												</label><br />
											<div class="form-group scrollableListCustom1">
												<ul id="sortable1" class="connectedSortable ui-sortable"></ul>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group alignCenter">
											<label class="labelTextGrey" style="text-align: center;">Assigned
												Domains
												<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="You can drag things back and forth"><img src="images/NewIcon/info_icon.png" /></a>
												</label><br />
											<div class="form-group scrollableListCustom2">
												<ul id="sortable2" class="connectedSortable ui-sortable"></ul>
												<input type="hidden" name="securityDomainAssigned"
													id="securityDomainAssigned" value="" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div><!-- end domain div -->

						<div id="services_1" class="userDataClass" style="display: none;">
							<!-- <h2 class="adminUserText">Services</h2> -->
							<h2 class="adminUserText">Service Pack</h2>
							<div class="row formspace" style="font-size: 12px;">
								<div class="col-md-12">
								<input id="selectAll" value="Select All" type="button" class="selectButton" data-check="service"><input id="deSelectAll"
									value="Deselect All" type="button" class="selectButton" data-check="service">

									</div>
							</div>
							<div class="allServiceTable">
								<div class="spServiceTable" style="margin-bottom: 25px;">
									<table id="servicePackTable"
										class="tablesorter scroll tagTableHeight dataTable" style="margin: 0">
										<thead>
											<th class="thsmall header">Authorize</th>
											<th class="thsmall header">Service Pack</th>
										</thead>
										<tbody></tbody>
									</table>
								</div>
								<h2 class="adminUserText">User Services</h2>
								<div class="row formspace" style="font-size: 12px;">
								<div class="col-md-12">
									<input id="selectAllUserServices" value="Select All"
										type="button" class="selectButton"><input
										id="deSelectAllUserServices" value="Deselect All"
										type="button" class="selectButton">
								</div>
								</div>
								<div class="userServiceTable" style="margin-bottom: 25px;">
									<table id="userServiceTable"
										class="tablesorter scroll tagTableHeight dataTable" style="margin: 0;">
										<thead>
											<th class="thsmall header">Authorize</th>
											<th class="thsmall header">User Services</th>
										</thead>
										<tbody></tbody>
									</table>
								</div>

								<h2 class="adminUserText">Group Services</h2>
								<div class="row formspace" style="font-size: 12px;">
								<div class="col-md-12">
									<input id="selectAllGroupServices" value="Select All"
										type="button" class="selectButton"><input
										id="deSelectAllGroupServices" value="Deselect All"
										type="button" class="selectButton">
								</div>
								</div>

								<div class="groupServiceTable">
									<table id="groupServiceTable"
										class="tablesorter scroll tagTableHeight dataTable" style="margin: 0;">
										<thead>
											<th class="thsmall header">Authorize</th>
											<th class="thsmall header">Assign/Unassign</th>
											<th class="thsmall header">Group Services</th>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div> <!-- end sevice tag div -->

						<div id="dns_1" class="userDataClass" style="display: none;">
							<br/><br/>
							<!--begin dns_1 div-->
                                                    <div class=" "><!-- start mail div -->
                                                    <!-- Code added @ 07 March 2019 -->
                                                    <?php 
                                                    if($dnsLogPermission == "1" && $dnsLogAddDelPermission == "yes"){
                                                    ?>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input type="button" id="addDnsServiceProvider" value="Add DNs to Enterprise >"
											style="width: 180px; font-size: 12px; margin-right: 30px; float:right" class="createNewButton" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
								  		<input type="button" id="addRemoveDnsServiceProvider" value="Remove Enterprise DNs <" style="width: 210px; font-size: 12px;" class="createNewButton" />
									</div>
								</div>
							</div>
                                                    <?php } ?>
                                                    <!-- End code -->
							
								<div id="showAddDnServiceProvider" class="row">
									<div class="">
										<div class="form-group">
											<h2 class="adminUserText">ADD DN</h2>
										</div>
									</div>
									<div class="col-md-7" id="phoneNumberDiv">
										<div class="form-group">
											<div class="col-md-10" style="padding-left:0px;"><label class="labelText">Phone Numbers</label>
												<input class="extraInputRow" type="text" name="phoneNumber"
													value="" style="width:400px !important;float: left" />
											</div>
											<div class="col-md-1" style="width:13% !important;"><label class="labelText">&nbsp;</label><br/>
												<img class="" id="addNumberRow" src="images/add.png"
													style="width: 18px; float: right; margin-top: 17px">
											</div>
										</div>
									</div>
									<div class="col-md-5" id="phoneRangeDiv">
										<div class="row" style="padding-left:0">
											<label class="labelText">Phone Numbers Ranges</label>
											<div class="form-group">
												<div class="col-md-5" style="padding-left:0px">
													<input class="extraInputRow" type="text" name="minPhoneNumber" value="" style="width:145px !important;" />
												</div>
												<div class="col-md-1" style="padding:0 !important; width:20px; margin:0"><br/><span style="margin-top:17px;color:#555 !important;">-</span></div>
												<div class="col-md-5" style="padding-left:0">
													<input class="extraInputRow" type="text" name="maxPhoneNumber" value="" style="width:145px !important;" />
												</div>
												<div class="col-md-1">
													<img class="" id="addNumberRangeRow" src="images/add.png" style="width: 18px; margin-top: 17px">
												</div>
											</div>
										</div>
									</div>
									<div class="clr"></div>
									<div class="col-md-12 dnBtnMargin">
										<div class="col-md-7">
											<div class="form-group">
												<!--<input type="button" id="addServiceProviderDns" class="subButton" value="Add DN" style="width: 100px; float:right">-->
												<!--<input type="button" id="addServiceProviderDns" class="subButton" value="Add DN" style="float:right">-->
												<input type="button" id="addServiceProviderDns" class="resetAll addDNButtonColor" value="Add DN" style="margin-right: 29px;">
											</div>
										</div>
										<div class="col-md-5">
											<div class="form-group">
												<!--<input type="button" class ="cancelButton" id="closeServiceProviderDns" value="Close" style="width: 100px;">-->
												<input type="button" class ="resetAll closeDNButton" id="closeServiceProviderDns" value="Close" style="margin-left: -12px;"/>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label class="labelpadding" id="errorDnsGroup"
											style="font-weight: bold; text-align: center; color: red; width: 100%;"></label>
										</div>
									</div>
								</div> <!-- end showAddDnServiceProvider div -->
								<div id="showRemoveDnServiceProvider" class="row">
									<div class="">
										<div class="form-group">
											<h2 class="adminUserText">Remove DN</h2>
										</div>
									</div>
									<div id="removePhoneNumberDiv" class="col-md-7">
										<div class="form-group">
											<div class="col-md-10" style="padding-left:0px;">
												<label class="labelText">Phone Numbers</label>
													<input class="extraInputRow" type="text" name="removePhoneNumber" value="" style="width:400px !important;float: left;" />
											</div>
											<div class="col-md-1" style="width:13% !important;">
												<br/>
												<img class="" id="addNumberRowRemove" src="images/add.png" style="width: 18px; float: right; margin-top: 19px" />
											</div>
													
										</div>
									</div>
									<div id="removePhoneRangeDiv" class="col-md-5">
										<div class="row" style="padding-left:0px;">
											<label class="labelText">Phone Numbers Ranges</label>
											<div class="form-group">
												<div class="col-md-5" style="padding-left:0px">
													<input class="extraInputRow" type="text" name="minPhoneNumberRemove" value="" style="width: 145px !important;" />
												</div>
												<div class="col-md-1" style="padding:0 !important; width:20px; margin:0">
													<br/>
													<span style="width: 20px; margin-top:17px;color:#555 !important;">-</span>
												</div>
												<div class="col-md-5" style="padding-left:0px">
													<input class="extraInputRow" type="text" name="maxPhoneNumberRemove" value="" style="width: 145px !important;" />
												</div>
												<div class="col-md-1">
													<img id="addNumberRangeRowRemove" src="images/add.png" style="width: 18px; margin-top: 17px" />
												</div>
											</div>
										</div>
									</div>
									<div class="clr"></div>
									<div class="col-md-12">
										<div class="col-md-7">
											<div class="form-group">
												<!--<input type="button" id="removeServiceProviderDns" value="Remove DN" class="deleteButton" style="width: 100px; float:right"/>-->
												<input type="button" id="removeServiceProviderDns" value="Remove DN" class="resetAll removeDNButtonColor" style="margin-right: 29px;"/>
											</div>
										</div>
										<div class="col-md-5">
											<div class="form-group">
												<!-- <input type="button" id="closeServiceProviderDnsRemove" class="cancelButton" value="Close" style="width: 100px;"/>-->
												<input type="button" id="closeServiceProviderDnsRemove" class="resetAll closeDNButton" value="Close" style="margin-left: -12px;"/>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label class="labelText" id="errorDnsRemoveGroup" style="font-weight: bold; text-align: center; color: red; width: 100%;"></label>
										</div>
									</div>
								</div>
								
								
							</div>							
							
							<div class="row">
								<div class="">
									<div class="form-group">
										<h2 class="adminUserText">Assign Phone Numbers</h2>
									</div>
								</div>
							</div>
							<div class="row" id="dnsModify">
								<div class="col-md-12">
									<div class="" style="float:left; padding-right:15px;">
										<div class="form-group alignCenter" style="width:235px;">
											<label class="labelTextGrey">Available Range(s)</label>
										</div>
										<div class="form-group alignCenter dnranges" style="width:235px;">
											<select id="availableRanges" name="availableRanges" multiple
												style="height: 220px; padding:4px !important"></select>
										 
										</div>
									</div>
									<div class="" style="float:left; padding-right:15px;">
										<div class="form-group alignCenter dnsButton addAvalableRangeDnsbutton">
                                                                                        <button type="button" id="addAvailableDns" class="addBlueCircle">
                                                                                            <img src="/Express/images/icons/arrow_Add.png">
                                                                                        </button>										
                                                                                </div>
									</div>
									<div class="" style="float:left; padding-right:15px;">
										<div class="form-group alignCenter" style="width:235px;">
											<label class="labelTextGrey">Available Phone Number(s)</label>
										</div>
										<div class="form-group alignCenter dnranges" style="width:235px;">
											<select id="availableNumbers" name="availableNumbers"
												multiple></select>
											<input type="hidden" name="availableNumberss"
												id="availableNumberss" value="" />
										</div>
									</div>
									<div class="" style="float:left; padding-right:12px;">
										<div class="form-group alignCenter dnsButton dnsButtonCenter">
											    <button type="button" id="addAssign" name="addAvailableNumbers" class="addBlueCircle">
                                                                                                <img src="/Express/images/icons/arrow_Add.png"></button>
                                                                                            <br/> 
                                                                                            <button type="button" id="removeAssign" name="removeAvailableNumbers" class="addRedCircle">
                                                                                                <img src="/Express/images/icons/arrow_remove.png">
                                                                                            </button>
                                                                                                <br /><br /> 
                                                                                              <button type="button" id="addAssignAll" name="addAvailableNumbers" class="addBlueCircle">
                                                                                                  <img src="/Express/images/icons/arrow_add_all.png">
                                                                                              </button>
                                                                                                  <br/>
                                                                                                <button type="button" id="removeAssignAll" name="removeAvailableNumbers" class="addRedCircle" style="margin-left: -4px;">
                                                                                                    <img src="/Express/images/icons/arrow_remove_all.png">
                                                                                                </button>
										</div>
									</div>
									<div class="" style="float:left; ">
										<div class="form-group alignCenter">
											<label class="labelTextGrey" style="text-align: center;">Assign
												to Group</label>
										</div>
										<div class="form-group alignCenter dnranges">
													<select id="assignNumbers"
												name="assignNumbers[]" multiple
												style="height: 250px;"></select>
										</div>
									</div>
									<div class="">
    									<input type="checkbox" id="activateNewAssigned" name="activateNewAssigned">
    									<label for="activateNewAssigned"><span id="activateNewAssignedSpan">&nbsp;</span></label> 
    									<label class="labelText">Activate Newly Assigned Numbers</label><br />
                                                                        
    									<input type="checkbox" id="activateAllAssigned" name="activateAllAssigned">
    									<label for="activateAllAssigned"><span id="activateAllAssignedSpan">&nbsp;</span></label>
    									<label class="labelText">Activate All Assigned Numbers</label>
									</div>
								</div>
							</div>
						</div><!-- end dn_1 div -->

						<div id="callPolicy_1" class="userDataClass"
							style="display: none;" >
							<!--begin basicInfo_1 div-->
							<!--begin address_1 div-->
							<h2 class="adminUserText">Calling Line ID</h2>
							<div style="clear: right;">&nbsp;</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input type="checkbox" name="useGroupName" id="useGroupName"
												size="30" maxlength="30" value="true"> <label
												for="useGroupName"><span></span></label>
											<label class="labelText">Use group name for Calling Line
												Identity</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<?php if($useDepartments == "true") { ?>
											<input type="checkbox" name="allowDepartmentCLIDNameOverride"
												id="allowDepartmentCLIDNameOverride" size="30"
												maxlength="30" value="true" disabled><label
												for="allowDepartmentCLIDNameOverride"><span></span></label>
											<label class="labelText"><span class="textDisable"> Allow
													Department Name Override</span></label>
										<?php }?>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="form-group"></div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useGroupCLIDSetting" type="radio" value="true"
												id="useGroupCLIDSetting1"><label for="useGroupCLIDSetting1"><span></span></label>
											<label class="labelText">Use Group Calling Line Id Policy</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useGroupCLIDSetting" type="radio" checked
												value="false" id="useGroupCLIDSetting2"><label
												for="useGroupCLIDSetting2"><span></span></label> <label
												class="labelText">Use Service Provider/Enterprise Calling
												Line Id Policy</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="form-group"></div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group callProTextAlign">
											<label class="labelText fontHead">Non-Emergency Calls</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<div class="row">
												<div class="col-md-12 alignChkBox">
													<input name="clidPolicy" type="radio" checked
														value="Use DN" id="clidPolicy1"> <label for="clidPolicy1"><span></span></label><label
														class="labelText">Use user phone number for Calling Line
														Identity</label>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12 alignChkBox">
													<input name="clidPolicy" type="radio"
														value="Use Configurable CLID" id="clidPolicy2"><label
														for="clidPolicy2"><span></span></label> <label
														class="labelText">Use configurable CLID for Calling Line
														Identity</label>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12 alignChkBox">
													<input name="clidPolicy" type="radio"
														value="Use Group CLID" id="clidPolicy3"> <label
														for="clidPolicy3"><span></span></label><label
														class="labelText">Use group/department phone number for <br />
														Calling Line Identity</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="form-group"></div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group callProTextAlign">
											<label class="labelText fontHead">Emergency Calls</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<div class="row">
												<div class="col-md-12 alignChkBox">
													<input name="emergencyClidPolicy" id="emergencyClidPolicy1"
														type="radio" checked="" value="Use DN"><label
														for="emergencyClidPolicy1"><span></span></label> <label
														class="labelText">Use user phone number for Calling Line
														Identity</label>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12 alignChkBox">
													<input name="emergencyClidPolicy" id="emergencyClidPolicy2"
														type="radio" value="Use Configurable CLID"><label
														for="emergencyClidPolicy2"><span></span></label> <label
														class="labelText">Use configurable CLID for Calling Line
														Identity</label>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12 alignChkBox">
													<input name="emergencyClidPolicy" id="emergencyClidPolicy3"
														type="radio" value="Use Group CLID"><label
														for="emergencyClidPolicy3"><span></span></label> <label
														class="labelText">Use group/department phone number for <br/>
														Calling Line Identity</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="form-group"></div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="allowAlternateNumbersForRedirectingIdentity"
												id="allowAlternateNumbersForRedirectingIdentity"
												type="checkbox" checked value="true"><label
												for="allowAlternateNumbersForRedirectingIdentity"><span></span></label>
											<label class="labelText">Allow Alternate Numbers for
												Redirecting Identity</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="allowConfigurableCLIDForRedirectingIdentity"
												id="allowConfigurableCLIDForRedirectingIdentity"
												type="checkbox" checked value="true"><label
												for="allowConfigurableCLIDForRedirectingIdentity"><span></span></label>
											<label class="labelText">Allow Configurable CLID for
												Redirecting Identity</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="blockCallingNameForExternalCalls"
												id="blockCallingNameForExternalCalls" type="checkbox"
												value="false"><label for="blockCallingNameForExternalCalls"><span></span></label>
											<label class="labelText">Block Calling Name for External
												Calls</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="form-group"></div>
								</div>
							</div>

							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group callProTextAlign">
											<label class="labelText">Calling Line ID Group Number</label>&nbsp;:&nbsp;<label
												class="labelText" id="clidgnu"></label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group callProTextAlign">
											<label class="labelText">Calling Line ID Group Name</label>&nbsp;:&nbsp;<label
												class="labelText" id="clidgna"></label>
										</div>
									</div>

						</div>
								</div>
								
						
							<div class="row">
								<div class="">
									<div class="form-group">
										<h2 class="adminUserText">Media</h2>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useGroupMediaSetting" id="useGroupMediaSetting1"
												type="radio" value="true"> <label for="useGroupMediaSetting1"><span></span></label>
												<label class="labelText">Use Use Group Media Policy</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useGroupMediaSetting" id="useGroupMediaSetting"
												type="radio" checked value="false"><label
												for="useGroupMediaSetting"><span></span></label> <label
												class="labelText">Use Service Provider/Enterprise Media
												Policy</label>
										</div>
									</div>

								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="form-group"></div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group callProTextAlign">
											<label class="labelText fontHead">Media Policy</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<div class="row">
												<div class="col-md-12 alignChkBox">
													<input name="mediaPolicySelection"
														id="mediaPolicySelection1" type="radio"
														value="Use Uncompressed Codec"><label
														for="mediaPolicySelection1"><span></span></label> <label
														class="labelText">Force Use of Uncompressed Codec</label>
												</div>
											</div>
											<div class="row">
												<div class="col-md-12 alignChkBox">
													<input name="mediaPolicySelection"
														id="mediaPolicySelection2" type="radio" checked
														value="No Restrictions"><label for="mediaPolicySelection2"><span></span></label>
													<label class="labelText">None</label>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="">
									<div class="">
										<div class="form-group">
											<h2 class="adminUserText">Call Limits</h2>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useGroupCallLimitsSetting"
												id="useGroupCallLimitsSetting1" type="radio" value="true"><label
												for="useGroupCallLimitsSetting1"><span></span></label> <label
												class="labelText">Use Group Call Limits Policy</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useGroupCallLimitsSetting"
												id="useGroupCallLimitsSetting2"
												id="useGroupCallLimitsSetting" type="radio" checked
												value="false"><label for="useGroupCallLimitsSetting2"><span></span></label>
											<label class="labelText">Use Service Provider/Enterprise Call
												Limits Policy</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="form-group"></div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useMaxSimultaneousCalls"
												id="useMaxSimultaneousCalls" type="checkbox" value="true"><label
												for="useMaxSimultaneousCalls"><span></span></label> <label
												class="labelText">Enable Maximum Number of Concurrent Calls</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input name="maxSimultaneousCalls" id="maxSimultaneousCalls"
												type="text" size="3" maxlength="3" value="10">&nbsp;&nbsp;<label
												class="labelText">Calls</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useMaxSimultaneousVideoCalls"
												id="useMaxSimultaneousVideoCalls" type="checkbox"
												value="true"><label for="useMaxSimultaneousVideoCalls"><span></span></label>
											<label class="labelText">Enable Maximum Number of Concurrent
												Video Calls</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input name="maxSimultaneousVideoCalls"
												id="maxSimultaneousVideoCalls" type="text" size="3"
												maxlength="3" value="5">&nbsp;&nbsp;<label class="labelText">Video
												Calls</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useMaxCallTimeForAnsweredCalls"
												id="useMaxCallTimeForAnsweredCalls" type="checkbox"
												value="true"><label for="useMaxCallTimeForAnsweredCalls"><span></span></label>
											<label class="labelText">Enable Maximum Duration for Answered
												Calls</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input name="maxCallTimeForAnsweredCallsMinutes"
												id="maxCallTimeForAnsweredCallsMinutes" type="text" size="4"
												maxlength="4" value="600">&nbsp;&nbsp;<label
												class="labelText">Minutes</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useMaxCallTimeForUnansweredCalls"
												id="useMaxCallTimeForUnansweredCalls" type="checkbox"
												value="true"><label for="useMaxCallTimeForUnansweredCalls"><span></span></label>
											<label class="labelText">Enable Maximum Duration for
												Unanswered Calls</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input name="maxCallTimeForUnansweredCallsMinutes"
												id="maxCallTimeForUnansweredCallsMinutes" type="text"
												size="2" maxlength="2" value="2">&nbsp;&nbsp;<label
												class="labelText">Minutes</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useMaxConcurrentRedirectedCalls"
												id="useMaxConcurrentRedirectedCalls" type="checkbox"
												value="true"><label for="useMaxConcurrentRedirectedCalls"><span></span></label>
											<label class="labelText">Enable Maximum Number of Concurrent
												Redirected Calls</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input name="maxConcurrentRedirectedCalls"
												id="maxConcurrentRedirectedCalls" type="text" size="3"
												maxlength="3" value="3">&nbsp;&nbsp;<label class="labelText">Calls</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useMaxConcurrentFindMeFollowMeInvocations"
												id="useMaxConcurrentFindMeFollowMeInvocations"
												type="checkbox" value="true"><label
												for="useMaxConcurrentFindMeFollowMeInvocations"><span></span></label>
											<label class="labelText">Enable Maximum Number of Concurrent <br />
												Find Me/Follow Me Invocations</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input name="maxConcurrentFindMeFollowMeInvocations"
												id="maxConcurrentFindMeFollowMeInvocations" type="text"
												size="3" maxlength="3" value="3">&nbsp;&nbsp;<label
												class="labelText">Invocations</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useMaxFindMeFollowMeDepth"
												id="useMaxFindMeFollowMeDepth" type="checkbox" value="true"><label
												for="useMaxFindMeFollowMeDepth"><span></span></label> <label
												class="labelText">Enable Maximum Find Me/Follow Me Depth</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input name="maxFindMeFollowMeDepth"
												id="maxFindMeFollowMeDepth" type="text" size="3"
												maxlength="3" value="3">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group callProTextAlign">
											<label class="labelText">Maximum Redirection Depth:</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<input name="maxRedirectionDepth" id="maxRedirectionDepth"
												type="text" size="3" maxlength="3" value="10">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<h2 class="adminUserText">Translation and Routing</h2>
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useGroupTranslationRoutingSetting"
												id="useGroupTranslationRoutingSetting1" type="radio"
												value="true"><label for="useGroupTranslationRoutingSetting1"><span></span></label>
											<label class="labelText">Use Group Translation and Routing
												Policy</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useGroupTranslationRoutingSetting"
												id="useGroupTranslationRoutingSetting2" type="radio" checked
												value="false"><label
												for="useGroupTranslationRoutingSetting2"><span></span></label>
											<label class="labelText">Use Service Provider/Enterprise Translation
											<br />and Routing Policy</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="form-group"></div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group callProTextAlign">
											<label class="labelText fontHead">Network Usage</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group alignChkBox">
												<div class="">
													<input name="networkUsageSelection"
														id="networkUsageSelection1" type="radio"
														value="Force All Calls"><label
														for="networkUsageSelection1"><span></span></label> <label
														class="labelText">Force All Calls to use the Network</label>
												</div>
												<div class="">
													<input name="networkUsageSelection"
														id="networkUsageSelection2" type="radio"
														value="Force All Except Extension and Location Calls"><label
														for="networkUsageSelection2"><span></span></label> <label
														class="labelText">Force All Calls to the Network except
														extension/location</label>
												</div>
												<div class="">
													<input name="networkUsageSelection"
														id="networkUsageSelection3" type="radio" checked
														value="Do Not Force Enterprise and Group Calls"><label
														for="networkUsageSelection3"><span></span></label> <label
														class="labelText">Don't Force Enterprise/Group Calls to
														the Network</label>
												</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="enforceGroupCallingLineIdentityRestriction"
												id="enforceGroupCallingLineIdentityRestriction"
												type="checkbox" value="true"><label
												for="enforceGroupCallingLineIdentityRestriction"><span></span></label>
											<label class="labelText">Enforce Group Calling Line Identity
												Restriction</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input
												name="allowEnterpriseGroupCallTypingForPrivateDialingPlan"
												id="allowEnterpriseGroupCallTypingForPrivateDialingPlan"
												type="checkbox" value="true"><label
												for="allowEnterpriseGroupCallTypingForPrivateDialingPlan"><span></span></label>
											<label class="labelText">Allow Group Call Typing For Private
												Dialing Plan</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input
												name="allowEnterpriseGroupCallTypingForPublicDialingPlan"
												id="allowEnterpriseGroupCallTypingForPublicDialingPlan"
												type="checkbox" value="true"><label
												for="allowEnterpriseGroupCallTypingForPublicDialingPlan"><span></span></label>
											<label class="labelText">Allow Group Call Typing For Public
												Dialing Plan</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="overrideCLIDRestrictionForPrivateCallCategory"
												id="overrideCLIDRestrictionForPrivateCallCategory"
												type="checkbox" value="true"><label
												for="overrideCLIDRestrictionForPrivateCallCategory"><span></span></label>
											<label class="labelText">Override CLID Restriction For
												Private Call Category</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="form-group"></div>
								</div>
							</div>
							<div class="row">
								<h2 class="adminUserText">Incoming Caller ID</h2>
								<div class="">
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useGroupDCLIDSetting" id="useGroupDCLIDSetting1"
												type="radio" value="true"><label for="useGroupDCLIDSetting1"><span></span></label>
											<label class="labelText">Use Group Dialable Caller ID Policy</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="useGroupDCLIDSetting" id="useGroupDCLIDSetting2"
												type="radio" checked value="false"><label
												for="useGroupDCLIDSetting2"><span></span></label> <label
												class="labelText">Use Service Provider/Enterprise Dialable
												Caller ID Policy</label>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="form-group"></div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group callProTextAlign">
											<label class="labelText">Dialable Caller ID</label>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group alignChkBox">
											<input name="enableDialableCallerID"
												id="enableDialableCallerID1" type="radio" value="true">
												<label for="enableDialableCallerID1"><span></span></label> <label
												class="labelText">On</label> &nbsp;&nbsp;&nbsp;&nbsp;
												<input name="enableDialableCallerID" id="enableDialableCallerID2"
												type="radio" checked value="false">
												<label for="enableDialableCallerID2"><span></span></label> <label
												class="labelText">Off</label>
										</div>
									</div>
								</div>
							</div>
						</div><!-- end callPolicy_1 end -->

				<!--	</div>
				</div>
			</div>    -->                                
<!-- Ocp code start here -->
				<div id="ocp_1" class="userDataClass" style="display: none;">
				<h2 class="adminUserText">Outgoing Calling Plan</h2>
			    <div class="row" id="useGroupDefaultSetting">
							
							<div class="col-md-12">
                             	<div class="form-group">
                                    <label class="labelText">Call Type:</label><br/>
                                             
                                  	<div class="dropdown-wrap">
                                       <select>
                                    		<option>Originating Calls</option>
                                       </select>
                                  	</div>
                           		</div>
 							</div>
							
				

                    <!-- heading calling line id plan  -->		
            		 <div class="col-md-12">
            		 	<div class="form-group">
            		 		<h2 class="subBannerCustom">Calling Plan Name/Permissions</h2>
            		 	</div>
            		 </div>
            	<!-- end heading    -->	 
            		
            		<!-- group div start --> 
            		 <div class="col-md-6 smIconImg">
            		 	<div class="form-group">
            		 	<label class="labelText">Group</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Calls within the business group"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsGroup" id="groupPermissionsGroup">
									<?php 
									foreach ($ocpArray as $keyocp => $valueocp){                                                                            
										echo "<option value='".$valueocp."' >".$valueocp."</option>";
									}
									?>
    							</select>
            				</div>
            		 	</div>
            		


                <!-- Local div start --> 
            		
            		 	<div class="form-group">
            		 	<label class="labelText">Local</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Calls within the local calling area"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsLocal" id="groupPermissionsLocal">
									<?php 
									foreach ($ocpArray as $keyocp => $valueocp){                                                                            
										echo "<option value='".$valueocp."' >".$valueocp."</option>";
									}
									?>
    							</select>
            				</div>
            		 	</div>
            		
            		 
            		 
            		 <!-- Toll Free div start --> 
            		
            		 	<div class="form-group">
            		 	<label class="labelText">Toll Free</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Calls made to toll free numbers"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsTollfree" id="groupPermissionsTollfree">
									<?php 

									foreach ($ocpArray as $keyocp => $valueocp){
										
										echo "<option value='".$valueocp."' >".$valueocp."</option>";
									}
									?>
								</select>
            				</div>
            		 	</div>
            		

		 
		           <!-- Toll Free div start --> 
            		
            		 	<div class="form-group">
            		 	<label class="labelText">Toll</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Local toll calls"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsToll" id="groupPermissionsToll">
    								<?php 
    								foreach ($ocpArray as $keyocp => $valueocp){
    									
    									echo "<option value='".$valueocp."' >".$valueocp."</option>";
    								}
    								?>
								</select>
            				</div>
            		 	</div>
            		
            		 
            		 
            		 <!-- International div start --> 
            		 
            		 	<div class="form-group">
            		 	<label class="labelText">International</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="International calls"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsInternational" id="groupPermissionsInternational">
									<?php 
									foreach ($ocpArray as $keyocp => $valueocp){
										
										echo "<option value='".$valueocp."' >".$valueocp."</option>";
									}
									?>
								</select>
            				</div>
            		 	</div>
            		
						 
						
                <!-- Operator Assisted div start --> 
					
            		 	<div class="form-group">
            		 	<label class="labelText">Operator Assisted</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Calls made with the chargeable assistance of an operator"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsOperatorAssisted" id="groupPermissionsOperatorAssisted">
									<?php 
									foreach ($ocpArray as $keyocp => $valueocp){                                                                            
										echo "<option value='".$valueocp."' >".$valueocp."</option>";
									}
									?>
								</select>
            				</div>
            		 	</div>
            		
            		 
            		 
            	<!-- Chargeable Dir. Assistance div start --> 
					
            		 	<div class="form-group">
            		 	<label class="labelText">Chargeable Dir. Assistance</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Directory assistance calls"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsOperatorChargDirAssisted" id="groupPermissionsOperatorChargDirAssisted">
									<?php 
									foreach ($ocpArray as $keyocp => $valueocp){
										
										echo "<option value='".$valueocp."' >".$valueocp."</option>";
									}
									?>
								</select>
            				</div>
            		 	</div>
            		
            		
            		
				    </div>
            		 
            		  
					<div class="col-md-6 smIconImg">
					            		
            		<!-- Casual div start --> 
					
            		 	<div class="form-group">
            		 	<label class="labelText">Casual</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="1010XXX chargeable calls. Example: 1010321"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsCasual" id="groupPermissionsCasual">
                                    <?php
                                    foreach ($ocpArray as $keyocp => $valueocp){                                                                            
                                        echo "<option value='".$valueocp."' >".$valueocp."</option>";
                                    }
                                    ?>
                                </select>
            				</div>
            		 	</div>
					<!-- Special Services I div start -->
            		 	<div class="form-group">
            		 	<label class="labelText">Special Services I</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Special Services I (700 Number) calls"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsSpecialService1" id="groupPermissionsSpecialService1">
                                    <?php
                                    foreach ($ocpArray as $keyocp => $valueocp){                                                                            
                                        echo "<option value='".$valueocp."' >".$valueocp."</option>";
                                    }
                                    ?>
                                </select>
            				</div>
            		 	</div>
            		
            		 
            		 
            		 <!-- Special Services II div start --> 
					
            		 	<div class="form-group">
            		 	<label class="labelText">Special Services II</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Special Services II"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsSpecialService2" id="groupPermissionsSpecialService2">
									<?php 
									foreach ($ocpArray as $keyocp => $valueocp){                                                                            
										echo "<option value='".$valueocp."' >".$valueocp."</option>";
									}
									?>
								</select>
            				</div>
            		 	</div>
            		
            		 
							 
							 
                <!-- Premium Services I div start --> 
					
            		 	<div class="form-group">
            		 	<label class="labelText">Premium Services I</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Premium Services I (900 Number) calls"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsPremiumServices1" id="groupPermissionsPremiumServices1">
                                    <?php 
                                    foreach ($ocpArray as $keyocp => $valueocp){                                                                            
                                        echo "<option value='".$valueocp."' >".$valueocp."</option>";
                                    }
                                    ?>
                                </select>
            				</div>
            		 	</div>
            		
            		 
            		 
            		 <!-- Premium Services II div start --> 
					
            		 	<div class="form-group">
            		 	<label class="labelText">Premium Services II</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Premium Services II (976 Number) calls"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsPremiumServices2" id="groupPermissionsPremiumServices2">
                                	<?php
                                        foreach ($ocpArray as $keyocp => $valueocp){                                                                            
                                            echo "<option value='".$valueocp."' >".$valueocp."</option>";
                                        }
                                     ?>
                                </select>
            				</div>
            		 	</div>
            		 
            		 
            		 
            		 <!-- URL Dialing div start --> 
					
            		 	<div class="form-group">
            		 	<label class="labelText">URL Dialing</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Calls from internet"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsUrlDialing" id="groupPermissionsUrlDialing">
                                    <?php
                                        foreach ($ocpArray as $keyocp => $valueocp){                                                                            
                                            echo "<option value='".$valueocp."' >".$valueocp."</option>";
                                        }
                                    ?>
                                </select>
            				</div>
            		 	</div>
            		
            		 
            		 <!-- Unknown div start --> 
					
            		 	<div class="form-group">
            		 	<label class="labelText">Unknown</label>
            		 	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Unknown call type"><img src="images/NewIcon/info_icon.png"></a>
            		 		<div class="dropdown-wrap">
            					<select class="selectClass" name="groupPermissionsUnknown" id="groupPermissionsUnknown">
                                    <?php
                                        foreach ($ocpArray as $keyocp => $valueocp){
                                            echo "<option value='".$valueocp."' >".$valueocp."</option>";
                                        }
                                    ?>
                                </select>
            				</div>
            		 	</div>
            		 </div>

				</div>
          </div>           
		
<!-- Ocp code end here -->
			<!-- </form> -->

						<div id="networkServices_1" class="userDataClass"
							style="display: none; margin:0 auto;">
							<!--begin networkServices_1 div-->
							<h2 class="adminUserText">Network Class of Service</h2>
							<div class="row">
								<div class="col-md-12">
									<div class="" style="float:left;padding-right: 12px;">
                        				<div class="form-group alignCenter" style="width:235px;">
                        					<label class="labelTextGrey">Available NC Service</label><br>
                        				</div>
				
                        				<div class="form-group alignCenter dnranges" style="width:235px;">
                        					<select style="height: 250px !important;width: 230px !important;" name="availableNCSToAssign" id="availableNCSToAssign" multiple=""></select>
                        				</div>
									</div>
			<!-- button arrow  -->
							<div class="" style="float:left;padding-right: 8px;margin-top: 51px;">
								<div class="form-group alignCenter servicesButton ">
                					<button id="addNCS" type="button" class="addBlueCircle"> 
                						<img src="/Express/images/icons/arrow_Add.png">
                					</button>
                    				
                    				<br> 
                    				
                					<button id="removeFromAssigned" type="button" class="addRedCircle"> 
                						<img src="/Express/images/icons/arrow_remove.png">
                					</button>
                					
                                    <br><br> 
                				   
                				   <button id="addNCSAll" type="button" class="addBlueCircle"> 
                						<img src="/Express/images/icons/arrow_add_all.png">
                					</button>
                					
                                  <br>
                                  
                				  <button id="removeAllFromAssigned" type="button" class="addRedCircle" style="margin-left:-4px ;"> 
                						<img src="/Express/images/icons/arrow_remove_all.png">
                				  </button>
			  					</div>
		   					</div>
		   
		   <!-- Assign N/W service -->
									
							<div class="" style="float:left;padding-right: 12px;">
                				<div class="form-group alignCenter" style="width: 230px;">
                					<label class="labelTextGrey ">Assigned NC Service</label>
                				</div>
                				
                				<div class="form-group alignCenter dnranges" style="width: 235px;">
                					<select id="assignedNCS" name="assignedNCS[]" multiple="" style="height: 250px !important;width: 230px !important;"></select>
                				 </div>
							</div>
			
			
							<div class="" style="float:left; padding-right:14px;margin-top: 51px;">
                				<div class="form-group alignCenter servicesButton">
            						 <button id="selectToDefault" type="button" class="addBlueCircle"> 
            							<img src="/Express/images/icons/arrow_Add.png">
            						</button>										
                                 </div>
							</div>
									
									
		  					<div class="" style="float:left; ">
                				<div class="form-group alignCenter">
                			 		<label class="labelTextGrey" style="text-align: center;">Default</label>
                				</div>
										
    							<div class="form-group alignCenter dnranges">
    								<select name="assignedDefault[]" id="assignedDefault" multiple="" style="height: 250px !important;width: 232px !important;"></select>
    							</div>
							</div>
					</div>
							<!-- <div class="col-md-12">
									<div class="" style="width:248px; float:left; padding-right:15px;">
										<div class="form-group alignCenter">
											<label class="labelTextGrey">Available NC Service</label><br />
											<select name="availableNCSToAssign" id="availableNCSToAssign"
												multiple style="height: 250px !important;width:233px !important"></select>
										</div>
									</div>
									<div class="" style="float:left; padding-right:15px;">
										<div class="form-group alignCenter servicesButton">
											<button id="addNCS" type="button" class="addBlueCircle" > 
                                                                                            <img src="/Express/images/icons/arrow_Add.png">
                                                                                        </button>
                                                                                            <br />
                                                                                        <button id="removeFromAssigned" type="button" class="addRedCircle" > 
                                                                                            <img src="/Express/images/icons/arrow_remove.png">
                                                                                        </button>
                                                                                        
                                                                                        <br />
											<br /> 
                                                                                        
                                                                                        <button id="addNCSAll" type="button" class="addBlueCircle"> 
                                                                                            <img src="/Express/images/icons/arrow_add_all.png">
                                                                                        </button>
                                                                                        
                                                                                        <br/>
                                                                                        <button id="removeAllFromAssigned" type="button" class="addRedCircle"> 
                                                                                            <img src="/Express/images/icons/arrow_remove_all.png">
                                                                                        </button>
                                                                                        
                                                                                        
										</div>
									</div>
									<div class="" style="float:left; padding-right:15px;">
										<div class="form-group alignCenter">
											<label class="labelTextGrey ">Assigned NC Service</label><br />
											<select id="assignedNCS" name="assignedNCS[]" multiple="" style="height: 250px !important;width:233px !important"></select>
										</div>
									</div>
									<div class="" style="float:left;">
										<div class="form-group alignCenter servicesButton">
											<!-- <input id="selectToDefault" type="button" value="Select >"
												style=" font-size: 12px; margin: 5px 0;" class="assignNumberAdd" /> 
                                                                                        
                                                                                        <button id="selectToDefault" type="button" class="addBlueCircle"> 
                                                                                            <img src="/Express/images/icons/arrow_Add.png">
                                                                                        </button>
                                                                                        
										</div>
									</div>
									<div class="" style="width:245px; float:left;padding-left:15px;">
										<div class="form-group alignCenter">
											<label class="labelTextGrey">Default</label><br />
											<select name="assignedDefault[]" id="assignedDefault"
												multiple="" style="height: 250px !important;width:233px !important"></select>
										</div>
									</div>
								</div>-->
							</div>	
		 				</div> <!-- end networkServices_1 div -->
					
						<div id="voicePortalServices_1" class="userDataClass"
							style="display: none;">
							<h2 class="adminUserText">Voice Portal Service</h2>

							<!--begin voice portal div-->
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" id="vpNotAssigned" style="color: red;"><b>Group Service
													- ( <i>Voice Messaging Service</i> ) not assigned
											</b></label>
										</div>
									</div>
								</div>
							</div>
                                                        
                                                        <!-- Code added @ 06 June 2019 -->
                                                        <div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">                                                                                    
                                                                                    <input style="float: left;" type="hidden" name="voicePortalActiveStatus" value="false" /> 
                                                                                    <input class="newCheckBoxImg" style="width:20px; float:right;margin: 10px 0;" type="checkbox" name="voicePortalActiveStatus" id="voicePortalSrvce" value="true"
                                                                                     <?php          
                                                                                        $checked = "";
                                                                                        if($_SESSION['groupInfoData']['voicePortalActiveStatus'] == "true"){ 
                                                                                            $checked = "checked"; 
                                                                                        }
                                                                                        echo $checked;
                                                                                     ?> />
                                                                                    <label class="labelClsForVPSrvce" for="voicePortalSrvce"><span class="voicePortalSpanCls"></span></label> 
                                                                                    <label class="labelText fontHead"><b>Enable</b></label>
										</div>
									</div>
								</div>
							</div>
                                                        <!-- Code added @ 06 June 2019 -->
                                                        
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="portalId">Voice Portal ID:</label><br />
											<input type="text" name="portalId" id="portalId" size="35"
												maxlength="30" disabled value="">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="portalId">Voice Portal Name:</label><br />
											<input type="text" name="portalName" id="portalName"
												size="35" maxlength="30" disabled value="">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="callingLineIDFname">Calling
												Line ID First Name:</label><br> <input type="text"
												name="callingLineIDFname" id="callingLineIDFname" size="35"
												maxlength="30" disabled value="">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="callingLineIDLname">Calling
												Line ID Last Name:</label><br> <input type="text"
												name="callingLineIDLname" id="callingLineIDLname" size="35"
												maxlength="30" disabled value="">
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="portalLanguage">Language:</label><br />
											<div class="dropdown-wrap">
											<select name="portalLanguage" id="portalLanguage">
												<option>English</option>
											</select>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="portalTimeZone">Time Zone:</label><br />
											<div class="dropdown-wrap">
											<select name="portalTimeZone" id="portalTimeZone">
												<option>Group Time Zone</option>
											</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="portalPhoneNo">Phone Number:</label><br />
											<div class="dropdown-wrap">
											<select name="portalPhoneNo" id="portalPhoneNo"
												data-defaultlength="">
											</select>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="portalExt">Voice Portal
												Extension:</label><br> <input type="text" name="portalExt"
												id="portalExt" size="" maxlength="" data-min="" />
												<br>
												<label id="portalExtErrorMsg" class="voicPortalExtErrMsgLabel"> &nbsp;</label>
										</div>
									</div>
								</div>
							</div>
						</div> <!-- end voicePortalServices_1 div -->
					</form><!-- add 30-05-2018 --> 

				<div id="departmentGroup_1" class="userDataClass"
						style="display: none;">
						<div class="row">
							<div class="">
								<div class="form-group">
									<h2 class="adminUserText">Departments</h2>
								</div>
							</div>
						</div>

						<form name="department" id="department" method="POST"
							class="fcorn-register container" action="#">
							<div class="row">
								<div class="col-md-11 adminSelectDiv">
									<div class="form-group">
										<label class="labelText">Select Department to view or modify</label><br />
                                                                                <div class="dropdown-wrap" style="line-height: 15px;">
    										<select name="selectDepartment" id="selectDepartment" style="width: 100% !important">
    										</select>
										</div>
									</div>
								</div>
								<div class="col-md-1 adminAddBtnDiv">
									<div class="form-group" id="swapDeptIcon">
										<p class="register-submit addAdminBtnIcon">
										<img src="images\icons\add_admin_rest.png" data-alt-src="images\icons\add_admin_over.png" name="newDepartment" id="newDepartment">
										</p>
										<!--<label class="labelText labelTextOpp"><span>Add</span><span>Departments</span></label>-->
										<label class="labelText labelTextOpp"><span>Add</span><span>Department</span></label>
									</div>
								</div>
							</div>
						</form>
						<div class="loading" id="loading3">
							<img src="/Express/images/ajax-loader.gif">
						</div>
						<form name="departmentGroupFrm" id="departmentGroupFrm"
							method="POST">
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="groupId">Department Name:</label>
											<span class="required">*</span><br /> <input type="text"
												name="departmentName" id="departmentName" value="">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="parentDepartmentKey">Parent
												Department Key:</label><br />
												<div class="dropdown-wrap">
													<select	name="parentDepartmentKey" id="parentDepartmentKey"></select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="callingLineIdNameDepartment">Calling
												Line ID:</label><br /> <input type="text"
												name="callingLineIdNameDepartment"
												id="callingLineIdNameDepartment" value="">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText"
												for="callingLineIdPhoneNumberDepartment">Calling Line ID
												Phone Number:</label><br />
												<div class="dropdown-wrap">
													<select name="callingLineIdPhoneNumberDepartment" id="callingLineIdPhoneNumberDepartment"></select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group alignBtn">
    								<input id="deleteDepartment" class="deleteButton marginRightButton"
    										value="Delete" type="button" style="display: inline-block;">
    										
    								<input type="button" id="cancelDepartment" value="Cancel"
    									class="cancelButton marginRightButton">
    									
    								<input style="display: none" type="button" id="modifyDepartment"
    									class="completeButton" value="Confirm Setting">	
    										
    								<input type="button" id="saveDepartment" class="subButton marginRightButton"
    									value="Confirm Setting">
    									
    									
								</div>
							</div>
    							
						 <div style="border:1px solid gray"></div>
							
						</form>
					</div>
				</div>
				<div class="row alignBtn">
					<div class = "col-md-2"></div>
                                        <?php
                                           if ($_SESSION["permissions"]["modifyGroup"] == "1") {
                                        ?>
					<div class = "col-md-8" style="text-align: center;">
						<input type="button" id="subButton" class="marginRightButton" value="Confirm Settings">
						<input type="button" id="grpButtonCancel" value="Cancel" class="cancelButton marginRightButton">
						
					</div>
                                        <?php 
                                           }
                                        ?>
                                        <?php
                                           if ($basicInformationLogPermission == "1" && $basicInformationLogAddDelPermission == "yes") {
                                        ?>
					<div class = "col-md-2" style="text-align: right;">
						<input id="subButtonDel" value="Delete Group" type="button" class="deleteButton marginRightButton deleteSmallButton">
					</div>
                                        <?php 
                                           }
                                        ?>
				</div>
				<div class="clr"></div>
				<div class="clr"></div>

	<?php }?>
			</div>
</div>
<div id="dialogMUGMOD" class="dialogClass"></div>
<div id="dialogDepartment" class="dialogClass"></div>
<input id="selectedSP" type="hidden" value="<?php echo $_SESSION["sp"]; ?>">
<input id="selectedGroup" type="hidden">
<input id="prevSelectedGroup" type="hidden" value="">
<div id="groupModifyPreventDialog" class="dialogClass"></div>