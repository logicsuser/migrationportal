var buttonNameClickEvent = '';
var selectGroupId = '';

$(function()
{
	var selectGroupEnabled = function(selectedGroupId){
	 selectGroupId = selectedGroupId;
	}
	//dialog box code 
	 
		  $("#groupModifyPreventDialog").dialog({
		  	autoOpen: false,
		  	width: 600,
		  	modal: true,
		  	position: { my: "top", at: "top" },
		  	resizable: false,
		  	closeOnEscape: false,
		  	buttons: {
		  		"Yes": function() {
		  			checkResult(false);
					 if(buttonNameClickEvent =='newGroup'){newGroup();}
					 if(buttonNameClickEvent =='groupMainPage'){groupMainPageReturn();}
					 if(buttonNameClickEvent =='changeListReturn'){$("#loading2").show(); showGroupInfo(selectGroupId);}
		  			$(this).dialog("close");
				},
		  		"No": function() {
		  			checkResult(true);
					$('#selectGroup').val($('#prevSelectedGroup').val());
					$('#selectGroup').prop('disabled', false);
		  			$(this).dialog("close");
					selectGroupEnabled();
					$("#groupDetails").show();
					return false; 
		  		} 
		  	},  
		      open: function() {
                           setDialogDayNightMode($(this));
		      	$(".ui-dialog-buttonpane button:contains('Yes')").button().show().addClass('subButton');
		          $(".ui-dialog-buttonpane button:contains('No')").button().show().addClass('cancelButton');
		      }
		  });
		/// 
	
	
	$("#modifyGrouptitle").hide();
	$("#groupMainPageP").hide();
	
	$("#groupDetails").hide();
	$("#subButtonDel").hide();
	$("#groupsFormId").hide();
	$("#securityDiv").hide();
	
	//$("#deSelectAll").hide();
	//$("#deSelectAllUserServices").hide();
	//$("#deSelectAllGroupServices").hide();

	$("#vpNotAssigned").show();
	$("#showAddDnServiceProvider").hide();
	$("#tabs").tabs();
	
	var groupJson = "";
        var checkTabsCondition   = "enable";
        var checkForModifyAction = "";
	$("#loading2").show();

	var ajaxComplete = false;

	
	$('#grpButtonCancel').click(function(){
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('#groupDetails').hide();
		$('#selectGroup').prop('disabled', false);
		$('#selectGroup').val('');
		//$("#groupMainPage").hide(); //@20 -may
		$("#modifyGrouptitle").hide();
		$("#groupMainPageP").hide();
	});


	var swapDeptIcon = function() {
		var thisId = $(this).find("img");
		var newImgSource = thisId.data('alt-src');
		thisId.data('alt-src', thisId.attr('src'));
		thisId.attr('src', newImgSource);
	}
	
	$("#swapDeptIcon, .addAdminBtnIcon").hover(swapDeptIcon, swapDeptIcon);

	$("#extensionLength").on("input", function(){
		var extVal = $(this).val();
		if(extVal < 2 || extVal > 7) {
			$(this).val(extVal.slice(0, -1));
		}
	});
	var extensionLenth = function(extensionValue) {		
		//$("#extensionLength").val(extensionValue);
	};

	
	var minLengthFunc = function(minExtensionValue, maxExtensionValue){
		var maxHtml = ""; 
		var min = "2";
		if(maxExtensionValue == "7" ) {
			maxExtensionValue = "6";
		}
		for(mx = parseInt(min); mx <= parseInt(maxExtensionValue) ; mx++){
			maxHtml += '<option value="'+mx+'">' + mx + '</option>';
		}
		$("#minExtensionLength").html(maxHtml);
		$("#minExtensionLength").val(minExtensionValue);
	};	
	
	var maxLengthFunc = function(maxExtensionValue,defaultExtensionValue){
		var maxHtml = "";
		var minSelectedValue = $("#minExtensionLength").val();
		var maxLimit = "7";
		for(mx = minSelectedValue; mx <= parseInt(maxLimit) ; mx++){
			maxHtml += '<option value="'+mx+'">' + mx + '</option>';
		}
		$("#maxExtensionLength").html(maxHtml);
		$("#maxExtensionLength").val(maxExtensionValue);
	};

	var defaultFunc = function(minExtensionValue, maxExtensionValue, defaultSelectedValue){
		
		var defaultHtml = "";
		var minSelectedValue = $("#minExtensionLength").val();
		var maxSelectedValue = $("#maxExtensionLength").val();
		
		for(mx = minSelectedValue; mx <= parseInt(maxSelectedValue) ; mx++){
			defaultHtml += '<option value="'+mx+'">' + mx + '</option>';
		}
		$("#defaultExtensionLength").html(defaultHtml); 
		if(defaultSelectedValue != "" || defaultSelectedValue != "null" ){
			$("#defaultExtensionLength").val(defaultSelectedValue);
		}else{
			$('#defaultExtensionLength option').eq(-1).prop('selected', true);
		}
	
	};
	
	var intializeExtension = function(minExtensionValue,maxExtensionValue,defaultExtensionValue ){
		//extensionLenth(minExtensionValue);
		minLengthFunc(minExtensionValue, maxExtensionValue);
		maxLengthFunc(maxExtensionValue);
		defaultFunc(minExtensionValue, maxExtensionValue,defaultExtensionValue);
	}
	
	$('#minExtensionLength, #maxExtensionLength, #defaultExtensionLength').on('change', function() {
		
		var minSelectedValue = $("#minExtensionLength").val();
		var maxSelectedValue = $("#maxExtensionLength").val();
		var defaultSelectedValue = $("#defaultExtensionLength").val();
		
		minLengthFunc(minSelectedValue, maxSelectedValue);
		maxLengthFunc(maxSelectedValue);
		defaultFunc(minSelectedValue, maxSelectedValue, defaultSelectedValue);
		
	});
	
	var showSelectDeselectToggle = function(){
		debugger;
		if($("#servicePackTable").find("input[type=checkbox]").length  == $("#servicePackTable").find("input[type=checkbox]:checked").length){
			$("#deSelectAll").show();
			$("#selectAll").hide();
		}else{
			$("#deSelectAll").hide();
			$("#selectAll").show();
		}
		if($("#userServiceTable").find("input[type=checkbox]").length  == $("#userServiceTable").find("input[type=checkbox]:checked").length){
			$("#deSelectAllUserServices").show();
			$("#selectAllUserServices").hide();
		}else{
			$("#deSelectAllUserServices").hide();
			$("#selectAllUserServices").show();
		}
		if($("#groupServiceTable").find("input[type=checkbox]").length  == $("#groupServiceTable").find("input[type=checkbox]:checked").length){
			$("#deSelectAllGroupServices").show();
			$("#selectAllGroupServices").hide();
		}else{
			$("#deSelectAllGroupServices").hide();
			$("#selectAllGroupServices").show();
		}
	};


	$('#selectAll').click(function(){
		$(".allServiceTable .spServiceTable .tablesorter tbody tr td input").prop('disabled', false);
		$(".allServiceTable .spServiceTable .tablesorter tbody tr td input").prop('checked', true);
		$('#deSelectAll').show();
		$('#selectAll').hide();
	});
	
	$('#deSelectAll').click(function(){
		$(".allServiceTable .spServiceTable .tablesorter tbody tr td input").prop('checked', false);
		//$(".isGroupAssign").prop('disabled', true);
		$('#deSelectAll').hide();
		$('#selectAll').show();
	});
	
	$('#selectAllUserServices').click(function(){
		$(".allServiceTable .userServiceTable .tablesorter tbody tr td input").prop('disabled', false);
		$(".allServiceTable .userServiceTable .tablesorter tbody tr td input").prop('checked', true);
		$('#deSelectAllUserServices').show();
		$('#selectAllUserServices').hide();
	});
	
	$('#deSelectAllUserServices').click(function(){
		$(".allServiceTable .userServiceTable .tablesorter tbody tr td input").prop('checked', false);
		//$(".isGroupAssign").prop('disabled', true);
		$('#deSelectAllUserServices').hide();
		$('#selectAllUserServices').show();
	});
	
	$('#selectAllGroupServices').click(function(){
		$(".allServiceTable .groupServiceTable .tablesorter tbody tr td input").prop('disabled', false);
		$(".allServiceTable .groupServiceTable .tablesorter tbody tr td input").prop('checked', true);
		$('#deSelectAllGroupServices').show();
		$('#selectAllGroupServices').hide();
	});
	
	$('#deSelectAllGroupServices').click(function(){
		$(".allServiceTable .groupServiceTable .tablesorter tbody tr td input").prop('checked', false);
		$(".isGroupAssign").prop('disabled', true);
		$('#deSelectAllGroupServices').hide();
		$('#selectAllGroupServices').show();
	});
	
	var enableTabs = function(){
		$('#tabs').tabs( "option", "active", 0 );
		$("#tabs").tabs('enable', 0);
		$("#tabs").tabs('enable', 1);
		$("#tabs").tabs('enable', 2);
		$("#tabs").tabs('enable', 3);
		$("#tabs").tabs('enable', 4);
		$("#tabs").tabs('enable', 5);
		$("#tabs").tabs('enable', 6);
		$("#tabs").tabs('enable', 7);
                $("#tabs").tabs('enable', 8);
       };
        
        var activeFirstTabOnModify = function(){
		$('#tabs').tabs( "option", "active", 0 );
	};
        
        
	
	$("#tabs li a").click(function(){ 
		var tabName = $("#tabs .ui-state-active a").attr("id");
		if(tabName == "dns"){
			$("#tabName").val(tabName);
		}
		if(tabName != "dns"){
			$("#closeServiceProviderDns").trigger("click");
			$("#closeServiceProviderDnsRemove").trigger("click");
		}
	});
	
	$('#selectGroup').on('keyup keypress', function(e) {
		  var keyCode = e.keyCode || e.which;
                  //Code added @28 March 2018 to fix the issue EX-458
                  if (keyCode === 38) 
                  {
			var selectGroupId = $("#selectGroup").val();
                        var tmpFormatedGroupId = selectGroupId.replace("<span class='search_separator'>-</span>", "-");
                        $('#selectGroup').val(tmpFormatedGroupId);                        
		  }
                  if (keyCode === 40) 
                  {
			var selectGroupId = $("#selectGroup").val();
                        var tmpFormatedGroupId = selectGroupId.replace("<span class='search_separator'>-</span>", "-");
                        $('#selectGroup').val(tmpFormatedGroupId);                        
		  }
                  //End Code
		  if (keyCode === 13) 
                  {                 
                            //Code added @28 March 2018 to fix the issue EX-458
                            $("#loading2").show();
                            var selectGroupVal = $("#selectGroup").val();                            
                            if(selectGroupVal == "")
                            {
                                    $("#groupDetails").hide();
                                    $("#loading2").hide();
                                    $('#selectGroup').prop('disabled', false);
                                    return false;
                            }
                            var groupArray = selectGroupVal.split("<span class='search_separator'>-</span>");
                            var selectGroupId = jQuery.trim(groupArray[0]);
                            $("#isGroupAccess").val(selectGroupId);
                            departmentListResultForGroup(selectGroupId);                            
                            var tmpFormatedGroupId = selectGroupVal.replace("<span class='search_separator'>-</span>", "-");
                            $('#selectGroup').val(tmpFormatedGroupId);
                            $('#selectGroup').prop('disabled', true);
                            $('#search').prop('disabled', true);
                            showGroupInfo(selectGroupId);   
                            //End Code
                            /* Commented @ 28 March 2018 due to fix issue EX-458
                             e.preventDefault();
                             return false;
                             */ 
		  }
		});
	
	var groupList = function(){
		$("#selectGroup").html('');
		$(".ui-dialog-buttonpane button:contains('Continue')").button().hide();
		var html = "<option value=''>None</option>";
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/groupList.php",
			success: function(result) {
				var obj = jQuery.parseJSON(result);
				for (var i = 0; i < obj.length; i++) {
					html += '<option value="'+ obj[i] +'">' + obj[i] + '</option>';
				}
				
				$("#selectGroup").html(html);
				$("#groupsFormId").show();
				
				var modifyGroupId = $("#modifyGroupId").val();
				if(modifyGroupId != ""){
					$('#selectGroup').val(modifyGroupId);
					getGroupInfo(modifyGroupId); 
				}else{
					$("#loading2").hide();
					
				}
			}
		});
	};
	
	var dnsList = function(){ 
		var html = "";
		var phoneList = "";
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/dns.php",
			success: function(result) {
				var obj = jQuery.parseJSON(result);
				var phoneList = obj.Success;
				for (var i = 0; i < phoneList.length; i++) {
					html += '<option value="'+phoneList[i].phoneNumber+'">' + phoneList[i].phoneNumber + '</option>';		
				}
				$("#availableRanges").html(html);
				
			}
		});
	};
	
	//function for dns
	$("#addAvailableDns").click(function(){
		addAvailableDns();
		var sortingId = "availableNumbers";
		reArrangeSelectSorting(sortingId);
	});
	
	
	
	var addAvailableDns = function(){
		var html = "";
		var range = "";
		var range1 = "";
		var range2 = "";
		var availableRanges = $("#availableRanges").val();		
		for (var i = 0; i < availableRanges.length; i++) {
			$("#availableRanges option[value='"+availableRanges[i]+"']").remove();			
			range = availableRanges[i].split(" - ");
			range1 = parseInt(range[0].replace("+1-", "")); 
			if(range[1]){
				range2 = parseInt(range[1].replace("+1-", ""));
			}else{
				range2 = "";
			}
			if(range1 != "" && range2 == ""){
				html += '<option value="+1-'+range1+'">+1-' + range1 + '</option>';
			}else if(range1 != "" && range2 != ""){
				for(j = parseInt(range1); j < parseInt(range2) + 1; j++){
					html += '<option value="+1-'+j+'">+1-' + j + '</option>';
				}
			}
		}
	$("#availableNumbers").append(html);		
	}
	
	/* function for sorting ex-899*/
	function reArrangeSelectSorting(sortingId) {
		$("#"+sortingId).html($("#" + sortingId + ' option').sort(function(a, b) {
		 	return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
	    }));
	}
	
	$("#addAssign, #addAssignAll").click(function(){ 
		var BtnId = $(this).attr('id');
		if(BtnId == "addAssignAll"){
			$('#availableNumbers option').prop('selected', true);
		}
		addAssignNumber();
		var sortingId = "assignNumbers";
		reArrangeSelectSorting(sortingId);
	});
	
	$("#removeAssign, #removeAssignAll").click(function(){ 
		var BtnId = $(this).attr('id');
		if(BtnId == "removeAssignAll"){
			$('#assignNumbers option').prop('selected', true);
		}
		
		removeAssignNumber();
		var sortingId = "availableNumbers";
		reArrangeSelectSorting(sortingId);
	});
		
	var addAssignNumber = function(){
		html = "";
		range1 = "";

		var availableNumbers = $("#availableNumbers").val();		
		if(availableNumbers){
			for (var i = 0; i < availableNumbers.length; i++) {
				
				range1 = parseInt(availableNumbers[i].replace("+1-", "")); 
				if(range1 != ""){
					html += '<option value="+1-'+range1+'">+1-' + range1 + '</option>';
				}
				$("#availableNumbers option[value='"+availableNumbers[i]+"']").remove();			
			}
		}
		$("#assignNumbers").append(html);
	};

	var removeAssignNumber = function(){
		html = "";
		range1 = "";
		
		var assignNumbers = $("#assignNumbers").val();	
		if(assignNumbers){
			for (var i = 0; i < assignNumbers.length; i++) {
				
				range1 = parseInt(assignNumbers[i].replace("+1-", "")); 
				if(range1 != ""){
					html += '<option value="+1-'+range1+'">+1-' + range1 + '</option>';
				}
				$("#assignNumbers option[value='"+assignNumbers[i]+"']").remove();
				
			}
		}
		$("#availableNumbers").append(html);
	};
	
	var stateListResult = function(){
		var html = "<option value=''>None</option>";
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/stateList.php",
			data: {stateList : 1},
			success: function(result) {
				var obj = jQuery.parseJSON(result);
				for (var i = 0; i < obj.Success.length; i++) {
					html += '<option>' + obj.Success[i] + '</option>';
				}
				$("#stateOrProvince").html(html);
				showGroupListUsingSearch();
			}
		});
	};
	
	var timeZoneResult = function(){ 
			var html = "<option value=''>None</option>";
			$.ajax({
				type: "POST",
				url: "groupManage/groupModify/timeZoneList.php",
				data: {timeZoneList : 1},
				success: function(result) { 
					var obj = jQuery.parseJSON(result); 
					for (var i = 0; i < obj.Success.length; i++) {
						html += '<option value="'+obj.Success[i].name+'">' + obj.Success[i].name + '</option>';
					}
					$("#timeZone").html(html);
					$("#portalTimeZone").html(html);
					stateListResult();
				}
			});
		};
		
	var routingProfileResult = function(){
		var html = "<option value=''>None</option>";
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/routeList.php",
			data: {routingProfileList : 1},
			success: function(result) {
				
				var obj = jQuery.parseJSON(result);
                                if(obj.Success.routingProfile!=undefined){
                                    for (var i = 0; i < obj.Success.routingProfile.length; i++) {
                                        html += '<option value="'+obj.Success.routingProfile[i]+'">' + obj.Success.routingProfile[i] + '</option>';
                                    }
                                    $("#routingProfile").html(html);
                                }
				timeZoneResult();
			}
		});
		
	};
	routingProfileResult();
	
	var availableVoicePortalDnList = function(groupId, vPhone){
		var html = "<option value=''>None</option>";
		
		$.ajax({
			type: "POST",
			data: {groupId : groupId},
			url: "groupManage/groupModify/availableVoicePortalDns.php",
			success: function(result) {
				
				var obj = jQuery.parseJSON(result);
				for (var i = 0; i < obj.Success.length; i++) {
					html += '<option value="'+ obj.Success[i] +'">' + obj.Success[i] + '</option>';
				}
				$("#portalPhoneNo").html(html);
				$("#portalPhoneNo").val(vPhone);
				
			}
		});
	};
	
	//function to get the basic information assigned to group
	var getBasicInfo = function(obj){
		var callingLineIdDisplayPhoneNumberList = '<option value="">None</option>';
		$("#extensionLength").val(obj.extensionLength);
                $("#userCount").html(obj.userCount);
		$("#groupName").val(obj.groupName);					
		$("#userCount").val(obj.userCount);                
		$("#userLimit").val(obj.userLimit);
		$("#callingLineIdName").val(obj.callingLineIdName);
		$("#timeZone").val(obj.timeZone);
		$("#locationDialingCode").val(obj.locationDialingCode);
		$("#contactName").val(obj.contactName);
		$("#contactEmail").val(obj.contactEmail);
		$("#contactNumber").val(obj.contactNumber);
		$("#addressLine1").val(obj.addressLine1);
		$("#addressLine2").val(obj.addressLine2);
		$("#city").val(obj.city);
		$("#stateOrProvince").val(obj.stateOrProvince);
		$("#zipOrPostalCode").val(obj.zipOrPostalCode);
		$("#country").val(obj.country);
		$("#routingProfile").val(obj.routingProfile);
		
		if(obj.callingLineIdDisplayPhoneNumberList && obj.callingLineIdDisplayPhoneNumberList.length > 0){
			for (var i = 0; i < obj.callingLineIdDisplayPhoneNumberList.length; i++) {
				callingLineIdDisplayPhoneNumberList += '<option value="'+obj.callingLineIdDisplayPhoneNumberList[i]+'">' + obj.callingLineIdDisplayPhoneNumberList[i].substr(3) + '</option>';
			}
		}
		
		$("#callingLineIdDisplayPhoneNumber").html(callingLineIdDisplayPhoneNumberList);
		$("#callingLineIdPhoneNumberDepartment").html(callingLineIdDisplayPhoneNumberList);
		
		if(obj.callingLineIdDisplayPhoneNumber == ""){
			$("#callingLineIdDisplayPhoneNumber").val('None');
		}else{ 
			var callingLineIdDisplayPhoneNumber = obj.callingLineIdDisplayPhoneNumber;
			//callingLineIdPhoneNumber = callingLineIdPhoneNumber.substr(2); 
			$("#callingLineIdDisplayPhoneNumber").val(callingLineIdDisplayPhoneNumber);
		}					
		getDomainInfo(obj);
	};
	
	//get the assign domain list, security domain and default domain information assigned to a group
	var getDomainInfo = function(obj){
		
		var assignDomainList = '';
		var htmlAvailable = "";
		var htmlSecurityDomainAvailable = "";
		var htmlSecurityDomainAssigned = "";
		var htmlAssigned = "";
		var textAssigned = "";
		var textSecurityAssigned = "";
		
		if(obj.assignDomainList){
			for (var i = 0; i < obj.assignDomainList.length; i++) {
				assignDomainList += '<option value="'+obj.assignDomainList[i]+'">' + obj.assignDomainList[i] + '</option>';
			}
		}
		$("#defaultDomain").html(assignDomainList);
		$("#defaultDomain").val(obj.defaultDomain);					
		
		if(obj.available){
			for (var i = 0; i < obj.available.length; i++) {
				htmlAvailable += '<li class="ui-state-default" id="'+obj.available[i]+'">' + obj.available[i] + '</li>';
			}
		}
		
		if(obj.domainAssigned){
			var defaultDomain = $("#defaultDomain").val();
			
			for (var j = 0; j < obj.domainAssigned.length; j++) {
				htmlAssigned += '<li class="ui-state-default '+obj.domainAssigned[j]+'" id="'+obj.domainAssigned[j]+'">';
				htmlAssigned += obj.domainAssigned[j];
				if(defaultDomain == obj.domainAssigned[j]){
					htmlAssigned += ' (Default Domain)';
				}
				htmlAssigned += '</li>';
				textAssigned += obj.domainAssigned[j]+';';
			}
		}
		
		if(obj.securityDomainAvailable){
			if(obj.securityDomainAvailable.length > 0){
				for (var k = 0; k < obj.securityDomainAvailable.length; k++) {
					htmlSecurityDomainAvailable += '<li class="ui-state-default" id="'+obj.securityDomainAvailable[k]+'">' + obj.securityDomainAvailable[k] + '</li>';
				}
				$("#securityDiv").show();
			}
		}
		
		if(obj.securityDomainAssigned){
			if(obj.securityDomainAssigned.length > 0){
				for (var l = 0; l < obj.securityDomainAssigned.length; l++) {
					htmlSecurityDomainAssigned += '<li class="ui-state-default" id="'+obj.securityDomainAssigned[l]+'">' + obj.securityDomainAssigned[l] + '</li>';
					textSecurityAssigned += obj.securityDomainAssigned[l] + ';';
				}
				$("#securityDiv").show();
			}
		}
		
		$("#sortable_1").html(htmlAvailable);
		$("#sortable_2").append(htmlAssigned);
		$("#sortable1").html(htmlSecurityDomainAvailable);
		$("#sortable2").append(htmlSecurityDomainAssigned);
		$("#domainAssigned").val(textAssigned);
		$("#securityDomainAssigned").val(textSecurityAssigned);
		getCallProcessingGroupInfo(obj);
	};
        
        
        var getOutgoingCallingPlanInfo = function(obj){
               
                var assignService = obj.assignedOCPService;
                if(assignService=="Yes")
                {
                    $("#ocp_div").show();
                }
		$("#groupPermissionsGroup").val(obj.groupPermissionsGroup);
                $("#groupPermissionsLocal").val(obj.groupPermissionsLocal);
                $("#groupPermissionsTollfree").val(obj.groupPermissionsTollfree);
                $("#groupPermissionsToll").val(obj.groupPermissionsToll);
                $("#groupPermissionsInternational").val(obj.groupPermissionsInternational);
                $("#groupPermissionsOperatorAssisted").val(obj.groupPermissionsOperatorAssisted);
                $("#groupPermissionsOperatorChargDirAssisted").val(obj.groupPermissionsOperatorChargDirAssisted);
                $("#groupPermissionsSpecialService1").val(obj.groupPermissionsSpecialService1);
                $("#groupPermissionsSpecialService2").val(obj.groupPermissionsSpecialService2);
                $("#groupPermissionsPremiumServices1").val(obj.groupPermissionsPremiumServices1);
                $("#groupPermissionsPremiumServices2").val(obj.groupPermissionsPremiumServices2);
                $("#groupPermissionsCasual").val(obj.groupPermissionsCasual);
                $("#groupPermissionsUrlDialing").val(obj.groupPermissionsUrlDialing);
                $("#groupPermissionsUnknown").val(obj.groupPermissionsUnknown);        
                
//                $("#loading2").hide();
//				$("#groupDetails").show();
	};
        
        
	
	//get the call processing information assigned to group
	var getCallProcessingGroupInfo = function(obj){

		$('input[name=useGroupCLIDSetting][value='+obj.useGroupCLIDSetting+']').prop('checked', true);
		$('input[name=useGroupMediaSetting][value="'+obj.useGroupMediaSetting+'"]').prop('checked', true);
		
		$('input[name=useGroupCallLimitsSetting][value="'+obj.useGroupCallLimitsSetting+'"]').prop('checked', true);
		
		$('input[name=useGroupTranslationRoutingSetting][value="'+obj.useGroupTranslationRoutingSetting+'"]').prop('checked', true);
		$('input[name=useGroupDCLIDSetting][value="'+obj.useGroupDCLIDSetting+'"]').prop('checked', true);

		if(obj.useMaxSimultaneousCalls == "true"){
			$('#useMaxSimultaneousCalls').prop('checked', true);
		}
		$("#maxSimultaneousCalls").val(obj.maxSimultaneousCalls);
		if(obj.useMaxSimultaneousVideoCalls == "true"){
			$('#useMaxSimultaneousVideoCalls').prop('checked', true);
		}
		$("#maxSimultaneousVideoCalls").val(obj.maxSimultaneousVideoCalls);
		
		if(obj.useMaxCallTimeForAnsweredCalls == "true"){
			$('#useMaxCallTimeForAnsweredCalls').prop('checked', true);
		}
		$("#maxCallTimeForAnsweredCallsMinutes").val(obj.maxCallTimeForAnsweredCallsMinutes);
		if(obj.useMaxCallTimeForUnansweredCalls == "true"){
			$('#useMaxCallTimeForUnansweredCalls').prop('checked', true);
		}
		$("#maxCallTimeForUnansweredCallsMinutes").val(obj.maxCallTimeForUnansweredCallsMinutes);
		
		$('input[name=mediaPolicySelection][value="'+obj.mediaPolicySelection+'"]').prop('checked', true);
		$('input[name=networkUsageSelection][value="'+obj.networkUsageSelection+'"]').prop('checked', true);

		if(obj.enforceGroupCallingLineIdentityRestriction == "true"){
			$('#enforceGroupCallingLineIdentityRestriction').prop('checked', true);
		}
		if(obj.allowEnterpriseGroupCallTypingForPrivateDialingPlan == "true"){
			$('#allowEnterpriseGroupCallTypingForPrivateDialingPlan').prop('checked', true);
		}
		if(obj.allowEnterpriseGroupCallTypingForPublicDialingPlan == "true"){
			$('#allowEnterpriseGroupCallTypingForPublicDialingPlan').prop('checked', true);
		}
		if(obj.overrideCLIDRestrictionForPrivateCallCategory == "true"){
			$('#overrideCLIDRestrictionForPrivateCallCategory').prop('checked', true);
		} else {
			$('#overrideCLIDRestrictionForPrivateCallCategory').prop('checked', false);
		}
		$("#useEnterpriseCLIDForPrivateCallCategory").val(obj.useEnterpriseCLIDForPrivateCallCategory);
		$("#enableEnterpriseExtensionDialing").val(obj.enableEnterpriseExtensionDialing);
		if(obj.useMaxConcurrentRedirectedCalls == "true"){
			$('#useMaxConcurrentRedirectedCalls').prop('checked', true);
		}
		$("#maxConcurrentRedirectedCalls").val(obj.maxConcurrentRedirectedCalls);
		$("#useMaxFindMeFollowMeDepth").val(obj.useMaxFindMeFollowMeDepth);
		if(obj.useMaxFindMeFollowMeDepth == "true"){
			$('#useMaxFindMeFollowMeDepth').prop('checked', true);
		}
		$("#maxFindMeFollowMeDepth").val(obj.maxFindMeFollowMeDepth);
		$("#maxRedirectionDepth").val(obj.maxRedirectionDepth);
		if(obj.useMaxConcurrentFindMeFollowMeInvocations == "true"){
			$('#useMaxConcurrentFindMeFollowMeInvocations').prop('checked', true);
		}
		$("#maxConcurrentFindMeFollowMeInvocations").val(obj.maxConcurrentFindMeFollowMeInvocations);
		
		if(obj.callingLineIdDisplayPhoneNumber == "" || obj.callingLineIdDisplayPhoneNumber == "+1-"){
			$('input[name=clidPolicy][value="'+obj.clidPolicy+'"]').prop('checked', true);
			$('input[name=emergencyClidPolicy][value="'+obj.emergencyClidPolicy+'"]').prop('checked', true);
			
			//check last radio button false and disabled if no number has been assigned
			$('input:radio[name=clidPolicy][value="Use Group CLID"]').prop('disabled', true);
			$('input:radio[name=emergencyClidPolicy][value="Use Group CLID"]').prop('disabled', true);
			$('input:radio[name=clidPolicy][value="Use Group CLID"]').prop('checked', false);
			$('input:radio[name=emergencyClidPolicy][value="Use Group CLID"]').prop('checked', false);	
		}else{
			$('input:radio[name=clidPolicy][value="Use Group CLID"]').prop('disabled', false);
			$('input:radio[name=emergencyClidPolicy][value="Use Group CLID"]').prop('disabled', false);
			
			if(obj.clidPolicy){
				$('input[name=clidPolicy][value="'+obj.clidPolicy+'"]').prop('checked', true);
			}
			if(obj.emergencyClidPolicy){
				$('input[name=emergencyClidPolicy][value="'+obj.emergencyClidPolicy+'"]').prop('checked', true);
			}
		}
		
		if(obj.allowAlternateNumbersForRedirectingIdentity == "true"){
			$('#allowAlternateNumbersForRedirectingIdentity').prop('checked', true);
		}
		
		if(obj.callingLineIdName != ""){
			$('#useGroupName').removeAttr("disabled");
			if(obj.useGroupName == "true"){
				$('#useGroupName').prop('checked', true);
				$('#allowDepartmentCLIDNameOverride').removeAttr("disabled");
				$('#allowDepartmentCLIDNameOverride').next("label").next("label").find("span").css('opacity','1');
				if(obj.allowDepartmentCLIDNameOverride == "true"){
					$('#allowDepartmentCLIDNameOverride').prop('checked', true);	
					$('#allowDepartmentCLIDNameOverride').val('true');
				}else{
					$('#allowDepartmentCLIDNameOverride').prop('checked', false);
					$('#allowDepartmentCLIDNameOverride').val('false');
				}
			}else{
				$('#useGroupName').prop('checked', false);
				$('#allowDepartmentCLIDNameOverride').prop('checked', false);
				$('#allowDepartmentCLIDNameOverride').prop("disabled", true);
				$('#allowDepartmentCLIDNameOverride').next("label").next("label").find("span").css('opacity','.4');
			}
		}else{
			$('#useGroupName').prop('checked', false);
			$('#useGroupName').prop("disabled", true);
			$('#allowDepartmentCLIDNameOverride').prop('checked', false);
			$('#allowDepartmentCLIDNameOverride').prop("disabled", true);
		}

		if(obj.blockCallingNameForExternalCalls == "true"){
			$('#blockCallingNameForExternalCalls').prop('checked', true);
			$('#blockCallingNameForExternalCalls').val('true');
		}else{
			$('#blockCallingNameForExternalCalls').prop('checked', false);
			$('#blockCallingNameForExternalCalls').val('false');
		}
		$('input[name=enableDialableCallerID][value="'+obj.enableDialableCallerID+'"]').prop('checked', true);
		
		if(obj.allowConfigurableCLIDForRedirectingIdentity == "true"){
			$('#allowConfigurableCLIDForRedirectingIdentity').prop('checked', true);
		}
		$("#allowDepartmentCLIDNameOverride").val(obj.allowDepartmentCLIDNameOverride);
		
		var clidgnuHtml = obj.callingLineIdDisplayPhoneNumber.replace("+1-", "")
		$("#clidgnu").html(clidgnuHtml);
		$("#clidgna").html(obj.callingLineIdName);
		getDnInfo(obj);
	};
	
	//get the dn`s info assign to the group
	var getDnInfo = function(obj){
		
		var assignNumbers = "";
		var dnList = '';
		
		if(obj.assignNumbers && obj.assignNumbers.length > 0){
			var dnsListArray = obj.assignNumbers.split(",");
				for (var i = 0; i < dnsListArray.length; i++) {
					dnList += '<option value="'+ dnsListArray[i] +'">' + dnsListArray[i] + '</option>';
				}
			$("#assignNumbers").html(dnList);
		}		
		getOutgoingCallingPlanInfo(obj);
	}
	
	var getVoicePortalInfo = function(obj, groupId){
		
		if(obj.portalPhoneNo){
			vPhone = obj.portalPhoneNo;
		}else{
			vPhone = '';
		}
		if(obj.serviceUserId){
                        //Code added @ 06 June 2019
                        $("#voicePortalSrvce").prop("disabled", false);    
                        if(obj.voicePortalActiveStatus == "true"){
                                $("#voicePortalSrvce").prop("checked", true);
                        }
                        $(".labelClsForVPSrvce").css("opacity", "1");
                        //End code
			$("#vpNotAssigned").hide();
			$("#portalPhoneNo").prop("disabled", false);
			$("#portalExt").prop("disabled", false);
			$("#portalTimeZone").prop("disabled", false);
			$("#portalLanguage").prop("disabled", false);
			$("#portalLanguage").css("background-color", "#fff");
			$("#portalTimeZone").css("background-color", "#fff");
			$("#portalPhoneNo").css("background-color", "#fff");
		}else{
                        //Code added @ 06 June 2019
                        $("#voicePortalSrvce").prop("disabled", true);
                        $("#voicePortalSrvce").prop("checked", false);
                        $(".labelClsForVPSrvce").css("opacity", "0.6");
                        //End code
			$("#vpNotAssigned").show();
			$("#portalPhoneNo").prop("disabled", true);
			$("#portalExt").prop("disabled", true);
			$("#portalTimeZone").prop("disabled", true);
			$("#portalLanguage").prop("disabled", true);
			$("#portalLanguage").css("background-color", "#e3e3e3");
			$("#portalTimeZone").css("background-color", "#e3e3e3");
			$("#portalPhoneNo").css("background-color", "#e3e3e3");
		}
                
                
                if($("#callProcessingPoliciesPrmns").val() == "no"){
                    $("#portalPhoneNo").prop("disabled", true);
                    $("#portalExt").prop("disabled", true);
                    $("#portalTimeZone").prop("disabled", true);
                    $("#portalLanguage").prop("disabled", true);
                }
                
		availableVoicePortalDnList(groupId, vPhone);
		$("#portalLanguage").val(obj.portalLanguage); 
		$("#portalTimeZone").val(obj.portalTimeZone);
		$("#portalPhoneNo").val(obj.portalPhoneNo);
		$("#portalExt").val(obj.portalExt);
		$("#portalName").val(obj.name);
		$("#portalId").val(obj.serviceUserId);
		$("#callingLineIDFname").val(obj.callingLineIdFirstName);
		$("#callingLineIDLname").val(obj.callingLineIdLastName);		
		
		intializeExtension(obj.minExtensionLength,obj.maxExtensionLength, obj.defaultExtensionLength);
		$("#portalExt").attr('size', obj.maxExtensionLength);
		$("#portalExt").attr('maxlength', obj.maxExtensionLength);
		$("#portalExt").attr('data-min', obj.minExtensionLength);
		$("#portalPhoneNo").attr('data-defaultLength', obj.defaultExtensionLength);
		
	};
	
	//blank the given fields when group change or add new group
	var emptyForm = function(){
		$("#sortable_2").empty();
		$("#sortable2").empty();
		$("#domainAssigned").val('');
		$("#securityDomainAssigned").val('');
		$("#availableNumbers").html('');
		$("#assignNumbers").html('');
	};
	
	//get the selected group information
	var getGroupInfo = function(groupId){ 
		
		emptyForm();
		$.ajax({
			url: "groupManage/groupModify/groupInfo.php",
			cache: false,
			data:{groupId:groupId, mod : 'loadGroup'},
			type: "POST",
			ajaxOptions: { cache: false },
			success: function(result)
			{
				if(foundServerConErrorOnProcess(result, "")) {
 					return false;
				}
				var obj = jQuery.parseJSON(result);
				if(obj.Error){
					//$("#errorGroup").html(obj.Error);
					//$("#divErrorGroup").show();
					alert( "Error:  Group not found: " + groupId);	
					$("#loading2").hide();
					$('#selectGroup').prop('disabled', false);
					$('#search').prop('disabled', false);
				}else{
					$("#modifyGrouptitle").show();
				 checkResult(true);
                                 getPreviousModuleId("groupModify");
					if(enterpriseLevel == "true"){
						$("#loading2").show();
					}
					$(".selectedGroup").html($("#selectGroup").val());
					$('#prevSelectedGroup').val($("#selectGroup").val());
					$("#groupMainPageP").show();
					//$("#groupMainPage").show();
					//enableTabs();
					getBasicInfo(obj);
//					getDomainInfo(obj);
//					getCallProcessingGroupInfo(obj);
//					getDnInfo(obj);

					groupServiceList(groupId);
//					networkClassServicesAvailabelList(groupId);
					getVoicePortalInfo(obj, groupId);
					
					dnsList();
//					networkClassServicesList(groupId);
//                                        getOutgoingCallingPlanInfo(obj);
                                        
                                        //Code added @ 17 Oct 2018
                                        if(checkTabsCondition == "disable"){
                                            enableTabs(); 
                                        }
                                        
                                        //call this function when group has modified
                                        if(checkForModifyAction == "modify"){
                                            activeFirstTabOnModify();
                                        }
                                        
                                        
					//End code
					if(obj.tabName){
						if(obj.tabName == "domain"){
							$("#tabName").val("");
							$("#tabName").val("services");
						}else if(obj.tabName == "services"){
							$("#tabName").val("");
							$("#tabName").val("departmentGroup");
						}else if(obj.tabName == "departmentGroup"){
							$("#tabName").val("");
							$("#tabName").val("dns");
						}else if(obj.tabName == "dns"){
							$("#tabName").val("");
							$("#tabName").val("callPolicy");
						}else if(obj.tabName == "callPolicy"){
							$("#tabName").val("");
							$("#tabName").val("networkServices");
						}else if(obj.tabName == "networkServices"){
							$("#tabName").val("");
							$("#tabName").val("voicePortalServices");
						}else if(obj.tabName == "voicePortalServices"){
							$("#tabName").val("");
							$(".ui-dialog-buttonpane button:contains('Next')").button().hide();
						}
					}
					
					$('#selectGroup').prop('disabled', false);
					$('#search').prop('disabled', false);
					$("#errorGroup").html();
					$("#divErrorGroup").hide();
					var isGroupAccDel = $("#isGroupAccess").val();
					if(enterpriseLevel == "true"){
						$("#subButtonDel").show();
					}else{
						$("#subButtonDel").hide();
					}
					
					$("#spanGroupId").text(decodString(groupId));
					$("#groupId").val(decodString(groupId));						
					$("#modifyGroupId").val(groupId);	
					$("#closeServiceProviderDns").trigger("click");
					$("#closeServiceProviderDnsRemove").trigger("click");
					
//					$("#loading2").hide();
//					$("#groupDetails").show();
				}
			}
		});		
	};
	
	var showGroupInfo = function(selectGroupId){
		 
		$("#groupMainPageP").hide();
		$("#modifyGrouptitle").hide();
		$("#selectedGroup").val(selectGroupId);
		
		$("#groupDetails").hide();
		$("#spanGroupId").show();
		$("#groupId").hide();
		getGroupInfo(selectGroupId);
		var tabName = "";
    	var maxTime = 60000;
    	var time = 0;
    	var interval = setInterval(function () {
    	  if($('#groupDetails').is(':visible')) {
    		$("#tabName").val("");
    		$("#operation").val("modify");
    	    clearInterval(interval);
    	  } else {
    	    if (time > maxTime) {
    	      clearInterval(interval);
    	      return;
    	    }
    	    time += 100;
    	  }
    	}, 200);
	};
	
	/*$("#search").click(function(){
		
		var selectGroupId = $("#selectGroup").val();
		if(selectGroupId == ""){
			$("#groupDetails").hide();
			$("#loading2").hide();
			$('#selectGroup').prop('disabled', false);
			return false;
		}
		
		$("#loading2").show();
		$('#selectGroup').prop('disabled', true);
		$('#search').prop('disabled', true);
		
		showGroupInfo(selectGroupId);
	});*/
	
	var groupServiceList = function(groupId){
		var html = "";
		var groupData="";
		var userData="";
		
		
			$.ajax({
				type: "POST",
				url: "groupManage/groupModify/GetGroupServices.php",
				data: {groupId : groupId},
				success: function(result) {
					
					var obj = jQuery.parseJSON(result);
					
					if(obj.Success){
						if(obj.Success.servicePacksAuthorizationTable){
							var servicePack = obj.Success.servicePacksAuthorizationTable;
	
							for (var i = 0; i < servicePack.length; i++) {
								var servicePackAuthCheck = servicePack[i][1] == "true" ? 'checked' : '';
								var servicePackAssingnCheck = servicePack[i][2] == "true" ? 'checked' : '';
                                                                var servicePermission = "";
                                                                if($("#servicesPrmns").val() == "no"){
                                                                    servicePermission = "class='disabledulli'";
                                                                }
							//	html += '<tr><td><input class="inputCheckbox" type="checkbox" name="servicePackAuth[][\''+servicePack[i][3]+'\']" id="'+servicePack[i][3]+'" value="'+ servicePack[i][1] +'"' + servicePackAuthCheck +' /><label for="'+servicePack[i][3]+'"><span></span></label></td><td>' + servicePack[i][0] + '</td></tr>';
								html += '<tr><td><input  class="inputCheckbox"  type="checkbox" name="servicePackAuth[][\''+servicePack[i][0]+'\']" id="'+servicePack[i][0]+'" value="'+ servicePack[i][1] +'"' + servicePackAuthCheck +' /><label for="'+servicePack[i][0]+'"><span '+servicePermission+'></span></label></td><td>' + servicePack[i][0] + '</td></tr>';
							}
							
						}
						
					}
					
					$("#servicePackTable tbody").html(html);
					 //$("#servicePackTable").tablesorter();
					if(obj.Success){
						if(obj.Success.groupServicesAuthorizationTable){
							var groupService = obj.Success.groupServicesAuthorizationTable;
							
							for (var i = 0; i < groupService.length; i++) {
								var groupAuthCheck = groupService[i][1] == "true" ? 'checked' : '';
								var groupAssignCheck = groupService[i][2] == "true" ? 'checked' : '';
								var inputDisable = groupService[i][1] == "false" ? 'disabled' : '';
                                                                
                                                                var servicePermission = "";
                                                                if($("#servicesPrmns").val() == "no"){
                                                                    servicePermission = "class='disabledulli'";
                                                                }
                                                                
                                                                
								groupData += '<tr><td>'+
									'<input class="inputCheckboxGroup"  type="checkbox" name="groupServiceAuth[][\''+groupService[i][0]+'\']" value="'+ groupService[i][1] +'"' + groupAuthCheck +' id="'+groupService[i][0]+'" /><label for="'+groupService[i][0]+'"><span '+servicePermission+'></span></label></td>'
									
									
									+ '<td><input type="checkbox" class="isGroupAssign"  name="groupServiceAssign[][\''+groupService[i][0]+'\']" value="'+ groupService[i][2] +'"' + groupAssignCheck +' '+inputDisable+' id="groupServiceAssign[][\''+groupService[i][0]+'\']"/><label for="groupServiceAssign[][\''+groupService[i][0]+'\']"><span '+servicePermission+'></span></label></td>'
									+ '<td>' + groupService[i][0] + '</td></tr>';
							}
							 
						}
					}
					$("#groupServiceTable tbody").html(groupData);
					//$("#groupServiceTable").tablesorter();
					if(obj.Success){
						if(obj.Success.userServicesAuthorizationTable){
							var userService = obj.Success.userServicesAuthorizationTable;
							for (var i = 0; i < userService.length; i++) {
								var userAuthCheck = userService[i][1] == "true" ? 'checked' : '';
                                                                var servicePermission = "";
                                                                if($("#servicesPrmns").val() == "no"){
                                                                    servicePermission = "class='disabledulli'";
                                                                }
								userData += '<tr><td><input type="checkbox"  name="userServiceAuth[][\''+userService[i][0]+'\']" value="'+ userService[i][1] +'"' + userAuthCheck +' id="'+userService[i][0]+'" /><label for="'+userService[i][0]+'"><span '+servicePermission+'></span></label></td><td>' + userService[i][0] + '</td></tr>';
							}
							 
						}
					}
					$("#userServiceTable tbody").html(userData);
					networkClassServicesAvailabelList(groupId);
					showSelectDeselectToggle();
				}
				
			});
	};
	
	var networkClassServicesList = function(groupId){
		var ncsAsgnData = "";
		var assignDefault = "";
		
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/networkClassOfServicesList.php",
			data: {groupId : groupId},
			success: function(result) {
				
				var obj = jQuery.parseJSON(result);
				
				if(obj.Success.ncsAssignedToGroupArr){
					var assignedNCS = obj.Success.ncsAssignedToGroupArr;
					for(var i = 0; i < obj.Success.ncsAssignedToGroupArr.length; i++){
						ncsAsgnData += '<option value="'+assignedNCS[i][0]+'" data-bool="'+assignedNCS[i][1]+'">'+assignedNCS[i][0]+'</option>'; 
					}
					$("#assignedNCS").html(ncsAsgnData);
				}
				if(obj.Success.ncsAssignedDefault){
					var asgnDefault = obj.Success.ncsAssignedDefault;
					for(var j = 0; j < asgnDefault.length; j++){
						assignDefault += '<option value="'+asgnDefault[j]+'" >'+asgnDefault[j]+'</option>';
					}
					$("#assignedDefault").html(assignDefault);
				}
				
				checkIfDefaultEmpty();
				$("#loading2").hide();
				$("#groupDetails").show();
			}
			
		});
	}
	
	var checkIfDefaultEmpty = function(){
		if($("#assignedDefault option").length > 0){
			$('#addToDefault').prop('disabled', true);
			$( "#addToDefault" ).addClass( "ui-state-disabled" );
		}
	}
	
	var networkClassServicesAvailabelList = function(groupId){
		
		var ncsData = "";
		$("#availableNCSToAssign").html("");
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/ncsAvailableList.php",
			data: {groupId : groupId},
			success: function(result) {
				var obj = jQuery.parseJSON(result);
				if(obj.availableArrayToAssign){
					var availNcs = obj.availableArrayToAssign;
					for (var i = 0; i < availNcs.length; i++) {
						ncsData += '<option value="'+availNcs[i]+'" >'+availNcs[i]+'</option>';
					}
					$("#availableNCSToAssign").html(ncsData);
				}
				networkClassServicesList(groupId);
			}
			});
	}
	
	$("#addNCS, #addNCSAll").click(function(){ 
		var BtnId = $(this).attr('id');
		if(BtnId == "addNCSAll"){
			$('#availableNCSToAssign option').prop('selected', true);
		}
		addNcsToAssignList();
	});
	
	var addNcsToAssignList = function(){
		var html = "";
		var data = "";

		var availableNcs = $("#availableNCSToAssign").val();	
		if(availableNcs){
			for (var i = 0; i < availableNcs.length; i++) {
				html += '<option value="'+availableNcs[i]+'">' +availableNcs[i]+ '</option>';
				
				$("#availableNCSToAssign option[value='"+availableNcs[i]+"']").remove();			
			}
		}
		$("#assignedNCS").append(html);
	}
	
	$("#removeFromAssigned, #removeAllFromAssigned").click(function(){ 
		var BtnId = $(this).attr('id');
		if(BtnId == "removeAllFromAssigned"){
			$('#assignedNCS option').prop('selected', true);
		}
		removeNcsFromAssigned();
	});
	
	$("#selectToDefault").click(function(){
		var asgnData="";
		var assignedNcsSelected = $("#assignedNCS").val();
		for (var i = 0; i < assignedNcsSelected.length; i++) {
			asgnData += '<option value="'+assignedNcsSelected[i]+'">' +assignedNcsSelected[i]+ '</option>';
		}
		$('#assignedDefault').find('option').remove();
		$("#assignedDefault").append(asgnData);
	});
	
	var removeNcsFromAssigned = function(){
		var html = "";
		var data = "";

		var assignedNcs = $("#assignedNCS").val();
		var defVal = $("#assignedDefault").find("option:first-child").val();

		for (var i = 0; i < assignedNcs.length; i++) {
			var val = assignedNcs[i];
			if(defVal != val){
				html += '<option value="'+assignedNcs[i]+'">' +assignedNcs[i]+ '</option>';
				$("#assignedNCS option[value='"+assignedNcs[i]+"']").remove();
			}
		}
		$("#availableNCSToAssign").append(html);
	}
	
	$("#addToDefault").click(function(){ 
		addNcsToDefault();
	});
	
	var addNcsToDefault = function(){
		var html = "";

		var availableNcsDefault = $("#assignedNCS").val();		
		for (var i = 0; i < availableNcsDefault.length; i++) {
			html += '<option value="'+availableNcsDefault[i]+'">' +availableNcsDefault[i]+ '</option>';			
		}
		$("#assignedDefault").append(html);
		if($("#assignedDefault option").length > 0){
			$('#addToDefault').prop('disabled', true);
			$( "#addToDefault" ).addClass( "ui-state-disabled" );
		}
	}
	
	$("#removeFromDefault").click(function(){ 
		removeNcsToDefault();
	});
	
	var removeNcsToDefault = function(){
		var html = "";

		var availableNcsDefault = $("#assignedDefault").val();		
		for (var i = 0; i < availableNcsDefault.length; i++) {
			html += '<option value="'+availableNcsDefault[i]+'">' +availableNcsDefault[i]+ '</option>';
			
			$("#assignedDefault option[value='"+availableNcsDefault[i]+"']").remove();	
			if($("#assignedDefault option").length == 0){
				$('#addToDefault').prop('disabled', false);
				$( "#addToDefault" ).removeClass( "ui-state-disabled" );
			}
		}
		//$("#assignedNCS").append(html);
	}
	
	$("#sortable_1, #sortable_2").sortable({
		placeholder: "ui-state-highlight",
		connectWith: "#sortable_2, #sortable_1",
		cursor: "crosshair",
		update: function(event, ui)
		{
			var domainAssignList = "";
			var order = $("#sortable_2").sortable("toArray");
			for (i = 0; i < order.length; i++)
			{
				domainAssignList += order[i] + ";";
			}
			$("#domainAssigned").val(domainAssignList);
		},
		receive: function(event, ui){
			var defaultDomainValue = $("#defaultDomain").val();
	        if(ui.item.attr('id') == defaultDomainValue){   
	            $(ui.placeholder).addClass('ui-state-error');                    
	            $(ui.sender).sortable('cancel');
	        }
	    }
	}).disableSelection();
	
	$("#sortable1, #sortable2").sortable({
		placeholder: "ui-state-highlight",
		connectWith: "#sortable1, #sortable2",
		cursor: "crosshair",
		update: function(event, ui)
		{
			var securityDomainAssignList = "";
			var order = $("#sortable2").sortable("toArray");
			for (i = 0; i < order.length; i++)
			{
				securityDomainAssignList += order[i] + ";";
			}
			$("#securityDomainAssigned").val(securityDomainAssignList);
		},
		receive: function(event, ui){
			var defaultDomainValue = $("#defaultDomain").val();
	        if(ui.item.attr('id') == defaultDomainValue){   
	            $(ui.placeholder).addClass('ui-state-error');                    
	            $(ui.sender).sortable('cancel');
	        }
	    }
	}).disableSelection();
	
	$("#blockCallingNameForExternalCalls").click(function(){
		if($(this).prop("checked") == true){
			$('#blockCallingNameForExternalCalls').val('true');
		}else{
			$('#blockCallingNameForExternalCalls').val('false');
		}	
	});
	
	$("#allowDepartmentCLIDNameOverride").click(function(){
		if($(this).prop("checked") == true){
			$('#allowDepartmentCLIDNameOverride').val('true');
		}else{
			$('#allowDepartmentCLIDNameOverride').val('false');
		}	
	});
	
	$("#dialogMUGMOD").dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		open: function(event) {
			$("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019
                        setDialogDayNightMode($(this));
			$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('subButton');
			$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
		},
		buttons: {
			"Complete": function() {
				var selectGroupId = $("#selectGroup").val();
				if(selectGroupId != ""){
					var groupIdVal = $("#spanGroupId").text();
					var groupNameVal = $("#groupName").val();
					var combinedGroupIdName = groupIdVal + ' <span class="search_separator">-</span> ' + groupNameVal;
					$("#modifyGroupId").val(combinedGroupIdName);
					modifyGroup();
				}else{
					/*var groupIdVal = $("#groupId").val();
					var groupNameVal = $("#groupName").val();
					var combinedGroupIdName = groupIdVal + ' <span class="search_separator">-</span> ' + groupNameVal;
					$("#modifyGroupId").val(combinedGroupIdName);*/
					createGroup();
				}				
			},
			"Cancel": function() {
				$("#loading2").show();
				$("#groupDetails").hide();
				var selectGroup = $("#modifyGroupId").val();
				var groupArray = selectGroup.split('<span class="search_separator">-</span>');
		        var selectGroup = jQuery.trim(groupArray[0]);
				
				if(selectGroup != ""){
					$(this).dialog("close");    
					getGroupInfo(selectGroup);
				}else{
					$("#loading2").hide();
					$("#groupDetails").show();
					$(this).dialog("close");		
				}
				$('html, body').animate({scrollTop: '0px'}, 300);
			},
			"Delete": function() {
				$("#dialogMUGMOD").html('Deleting the group....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
				$(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
				$("#loading2").show();    	
                        $.ajax({
					type: "POST",
					url: "groupManage/groupModify/deleteGroup.php",
					data: {groupId : $("#groupId").val(), deleteGroup : 1},
					success: function(result) {
						$("#loading2").hide(); 
						var obj = jQuery.parseJSON(result);
						if(obj.Error){
							$("#dialogMUGMOD").html(obj.Error);
							$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
							$(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass('deleteButton');
						}else{							
							$("#dialogMUGMOD").html('Group has been deleted');
							$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
							$(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
							$(".ui-dialog-buttonpane button:contains('Done')").button().show().addClass('subButton');
							$(".ui-dialog-buttonpane button:contains('Next')").button().hide();
							$(".ui-dialog-buttonpane button:contains('Return to Main')").button().show().addClass('cancelButton');
							$("#selectGroup").val('');
						}
                                                if(obj.previousGroupIsDeleted == "true"){
                                                    $(document).find('#goToPreviousGroup').hide();
                                                }
					}
				});
			},
            "More changes": function() {
            	$('html, body').animate({scrollTop: '0px'}, 300);
            	$("#loading2").show();   
            	$('#selectGroup').prop('disabled', 'disabled');
            	$('#search').prop('disabled', 'disabled');
            	$("#groupDetails").hide(); 
            	var modifyGroupId = $("#modifyGroupId").val();
            	$("#selectedGroup").val(modifyGroupId);
            	//console.log(modifyGroupId);
            	$("#groupsFormId").hide();
            	$('#selectGroup').val(decodString(modifyGroupId));
            	showGroupListUsingSearch();
            	$(this).dialog("close");   
            	$("#activateAllAssigned").prop('checked', false);
            	$("#activateNewAssigned").prop('checked', false);
            	$("#activateNewAssignedSpan").css("opacity","1");
//            	showSelectDeselectToggle();
             
            },
            "Done": function() {                
                //location.href="groupManage/groupModify/index.php";
            	
                $("#modifyGroupId").val('');
            	$("#spanGroupId").html('');
            	showGroupListUsingSearch();
            	$("#groupDetails").hide();
            	$(this).dialog("close");
            	$('html, body').animate({scrollTop: '0px'}, 300);
            },
            "Continue": function() {
            	$('html, body').animate({scrollTop: '0px'}, 300);
            	$("#loading2").show();   
            	$('#selectGroup').prop('disabled', 'disabled');
            	$('#search').prop('disabled', 'disabled');
            	
            	$("#groupDetails").hide();
            	var modifyGroupId = $("#modifyGroupId").val();
            	showGroupListUsingSearch();
				//getGroupInfo(modifyGroupId);
            	$('#selectGroup').val(modifyGroupId);
            	$(this).dialog("close");
            	
			},
			"Next" : function(){
				$('html, body').animate({scrollTop: '0px'}, 300);
            	$("#loading2").show();   
            	$('#selectGroup').prop('disabled', 'disabled');
            	$('#search').prop('disabled', 'disabled');
            	
            	$("#groupDetails").hide();
            	var modifyGroupId = $("#modifyGroupId").val();
            	showGroupListUsingSearch();
            	$('#selectGroup').val(modifyGroupId);
            	$(this).dialog("close"); 
            	var tabName = "";
            	var maxTime = 60000;
            	var time = 0;
            	var interval = setInterval(function () {
            	  if($('#groupDetails').is(':visible')) {
            		tabName = $("#tabName").val();
                  	groupModWizardNext(tabName);
            	    clearInterval(interval);
            	  } else {
            	    if (time > maxTime) {
            	      clearInterval(interval);
            	      return;
            	    }
            	    time += 100;
            	  }
            	}, 200);
			},
			"Ok" : function(){
				$(this).dialog("close"); 
			},
            "Return to Main": function() {
            	if(enterpriseLevel == "false"){
            		location.href="main.php";
	            }else{
	            	location.href="main_enterprise.php";
	            }            	
			}
		},
        open: function() {
            $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019	
            setDialogDayNightMode($(this));
            $(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');
            $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
            $(".ui-dialog-buttonpane button:contains('More changes')").button().hide();
            $(".ui-dialog-buttonpane button:contains('Done')").button().hide();
            $(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
            $(".ui-dialog-buttonpane button:contains('Continue')").button().hide();
            $(".ui-dialog-buttonpane button:contains('Next')").button().hide();
            $(".ui-dialog-buttonpane button:contains('Ok')").button().hide();
            $(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide();
        }
	});
	
	var groupModWizardNext = function(data){
		var maxTime = 60000;
    	var time = 0;
    	var interval = setInterval(function () {
    	  if($('#groupDetails').is(':visible')) {
				if(data == "basicInfo"){
					$('#tabs').tabs( "option", "active", 0 );
				}else if(data == "domain" ){
					$('#tabs').tabs( "option", "active", 1 );
				}else if(data == "services" ){
					$('#tabs').tabs( "option", "active", 2 );
				}else if(data == "departmentGroup" ){
					$('#tabs').tabs( "option", "active", 3 );
				}else if(data == "dns" ){
					$('#tabs').tabs( "option", "active", 4 );
				}else if(data == "callPolicy" ){
					$('#tabs').tabs( "option", "active", 5 );
				}else if(data == "networkServices" ){
					$('#tabs').tabs( "option", "active", 6 );
				}else if(data == "voicePortalServices" ){
					$(".ui-dialog-buttonpane button:contains('Next')").button().hide();
				}
    	    clearInterval(interval);
    	  } else {
    	    if (time > maxTime) {
    	      clearInterval(interval);
    	      return;
    	    }
    	    time += 100;
    	  }
    	}, 200);
	}
	
	var createGroup = function(){
		pendingProcess.push("Add Group");
		$('#tabs').tabs( "option", "active", 0 );
		$('#selectGroup').prop('disabled', 'disabled');
		$("#dialogMUGMOD").html('Creating the group request....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
		$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
		var dataToSend = $("form#groupModify").serializeArray();
        $.ajax({
			type: "POST",
			url: "groupManage/groupAdd/groupAdd.php",
			//data: dataToSend,
			data: dataToSend,
                        beforeSend: function(){
                            $("html, body").animate({ scrollTop: 0 }, 0); //Code added @07 Feb 2019
                        },
			success: function(result) {
				if(foundServerConErrorOnProcess(result, "Add Group")) {
					return false;
              	}
				$("#loading2").hide();
				$("#dialogMUGMOD").html('');
				var obj = jQuery.parseJSON(result); 
				var groupError = "";
				if(obj.groupAddResponse){
					if(obj.groupAddResponse.Error){
						$("#dialogMUGMOD").append('<br/>' + obj.groupAddResponse.Error.Detail);
						groupError = "1";
					}else{
						$("#dialogMUGMOD").append('<br/>Group Created Successfully');
						//$("#createdGroupId").val(obj.groupAddResponse.groupId);
						
						var groupIdVal = obj.groupAddResponse.groupId;
						var groupNameVal = obj.groupAddResponse.groupName;
						var combinedGroupIdName = groupIdVal + ' <span class="search_separator">-</span> ' + groupNameVal;
						
						$("#modifyGroupId").val(combinedGroupIdName);
					}
				}
				
				if(obj.routingResponse){
					if(obj.routingResponse.Error){
						$("#dialogMUGMOD").append('<br/>' + obj.routingResponse.Error);
						extensionError = "1";
					}else{
						$("#dialogMUGMOD").append('<br/>Routing Profile Added Successfully');
					}
				}

				if(obj.tabName){
					$("#tabName").val("");
					$("#tabName").val("domain");
				}
				if(groupError == "1"){
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
					$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
				}else{							
					$("#dialogMUGMOD").append('<br/><b>Group has been created.Click on More Changes to add information to group</b>');
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
					$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled").addClass('subButton');;
					$(".ui-dialog-buttonpane button:contains('More changes')").button().show().addClass('cancelButton');
                    $(".ui-dialog-buttonpane button:contains('Done')").button().hide();
                    $(".ui-dialog-buttonpane button:contains('Continue')").button().hide();
                    $(".ui-dialog-buttonpane button:contains('Next')").button().hide();
                    $(".ui-dialog-buttonpane button:contains('Return to Main')").button().show().addClass('cancelButton');
					}
					$('#selectGroup').prop('disabled', false);
				}
        	
		});
	};
	
	var modifyGroup = function(){
		pendingProcess.push("Modify Group");
                checkForModifyAction = "modify";
		$("#activateNewAssigned").removeAttr("disabled");
		$('#selectGroup').prop('disabled', 'disabled');
		$("#dialogMUGMOD").html('Modifying the group request....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
		$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");

		//var dataToSend = $("form#groupModify").serializeArray();
    	var dataToSend = $("form#groupModify :input[name != 'assignNumbers[]']").serializeArray();
    	dataToSend = getAssignNumbers(dataToSend);
        $.ajax({
			type: "POST",
			url: "groupManage/groupModify/groupModify.php",
			data: dataToSend,
                        beforeSend: function(){
                            $("html, body").animate({ scrollTop: 0 }, 0); //Code added @07 Feb 2019
                        },
			success: function(result) {
				if(foundServerConErrorOnProcess(result, "Modify Group")) {
					return false;
              	}
				$("#loading2").hide();
				$("#dialogMUGMOD").html('');
				
				var obj = jQuery.parseJSON(result);
				var groupError = "";
				var routingError = "";
				var domainError = "";
				var dnError = "";
				var dnActivateError = "";
				var callProcessingError = "";
				var gpError = "";
				var UAuthError = "";
				var UUnAuthError = "";
				var spNewAuthError = "";
				var spUnAuthError = "";
				var grpAssignError = "";
				var grpUnAuth = "";
				var voicePortalError = "";
				var ncsUnAssignedError = "";
				var ncsAssignedError = "";
			
				if(obj.groupModifyResponse){
					if(obj.groupModifyResponse.Error){
						$("#dialogMUGMOD").append('<br/>' + obj.groupModifyResponse.Error.Detail);
						groupError = "1";
					}else{
						$("#dialogMUGMOD").append('<br/>Basic Information Updated Successfully');
					}
				}
                                
                                if(obj.ocpModifyResponse){
					if(obj.ocpModifyResponse.Error){
						$("#dialogMUGMOD").append('<br/>' + obj.groupModifyResponse.Error.Detail);
						groupError = "1";
					}else{
						$("#dialogMUGMOD").append('<br/>Outgoing Calling Plan Information Updated Successfully');
					}
				}
				
				if(obj.groupExtensionResponse){
					if(obj.groupExtensionResponse.Error){
						$("#dialogMUGMOD").append('<br/>' + obj.groupExtensionResponse.Error);
						extensionError = "1";
					}else{
						$("#dialogMUGMOD").append('<br/>Extension Length Updated Successfully');
					}
				}
				
				if(obj.routingModifyResponse){
					if(obj.routingModifyResponse.Error){
						$("#dialogMUGMOD").append('<br/>' + obj.routingModifyResponse.Error.Detail);
						routingError = "1";
					}else{
						$("#dialogMUGMOD").append('<br/>Routing Profile Updated Successfully');
					}
				}
				
				if(obj.assignDomainModifyResponse){
					if(obj.assignDomainModifyResponse.assign){
						if(obj.assignDomainModifyResponse.assign.Error[0]){
							$("#dialogMUGMOD").append('<br/>' + obj.assignDomainModifyResponse.assign.Error[0]);
							domainError = "1";
						}else{
							$("#dialogMUGMOD").append('<br/>Assign Domain Updated Successfully');
						}
					}
					if(obj.assignDomainModifyResponse.unAssign){
						if(obj.assignDomainModifyResponse.unAssign.Error[0]){
							$("#dialogMUGMOD").append('<br/>' + obj.assignDomainModifyResponse.unAssign.Error[0]);
							domainError = "1";
						}else{
							$("#dialogMUGMOD").append('<br/>Domain Unassign Updated Successfully');
						}
					}
				}
				
				if(obj.callProcessingModifyResponse){
					if(obj.callProcessingModifyResponse.Error){
						if(obj.callProcessingModifyResponse.Error.Detail){
							$("#dialogMUGMOD").append('<br/>' + obj.callProcessingModifyResponse.Error.Detail);
						}else{
							$("#dialogMUGMOD").append('<br/>' + obj.callProcessingModifyResponse.Error);
						}
						callProcessingError = "1";
					}else{
						$("#dialogMUGMOD").append('<br/>Call Processing Updated Successfully');
					}
				}
				
				if(obj.dnsResponse){
					if(obj.dnsResponse.assign){
						if(obj.dnsResponse.assign.Error){
							$("#dialogMUGMOD").append('<br/>' + obj.dnsResponse.assign.Error);
							dnError = "1";
						}else{
							$("#dialogMUGMOD").append('<br/>DN Assign Updated Successfully');
						}
					}
					if(obj.dnsResponse.unAssign){
						if(obj.dnsResponse.unAssign.Error){
							$("#dialogMUGMOD").append('<br/>' + obj.dnsResponse.unAssign.Error);
							dnError = "1";
						}else{
							$("#dialogMUGMOD").append('<br/>DN Unassign Updated Successfully');
						}
					}
				}
				
				if(obj.dnsActivateResponse){
					if(obj.dnsActivateResponse.Error){
						$("#dialogMUGMOD").append('<br/>' + obj.dnsActivateResponse.Error);
						dnActivateError = "1";
					}else{
						$("#dialogMUGMOD").append('<br/>DN Activated Successfully');
					}
				}

				if(obj.groupServiceAuthResponse){
					if(obj.groupServiceAuthResponse.authorized){
						if(obj.groupServiceAuthResponse.authorized.Error){
							$("#dialogMUGMOD").append('<br/>' + obj.groupServiceAuthResponse.authorized.Error);
							gpError = "1";
						}else{
							$("#dialogMUGMOD").append('<br/>Group Service Authorized Successfully');
						}
					}
				}
				if(obj.groupServiceUnAuthResponse){
					if(obj.groupServiceUnAuthResponse.unAuthorized){
						if(obj.groupServiceUnAuthResponse.unAuthorized.Error){
							$("#dialogMUGMOD").append('<br/>' + obj.groupServiceUnAuthResponse.unAuthorized.Error);
							grpUnAuth = "1";
						}else{
							$("#dialogMUGMOD").append('<br/>Group Service UnAuthorized Successfully');
						}
					}
				}
				if(obj.userAuthResponse){
					if(obj.userAuthResponse.authorized){
						if(obj.userAuthResponse.authorized.Error){
							$("#dialogMUGMOD").append('<br/>' + obj.userAuthResponse.authorized.Error.Detail);
							UAuthError = "1";
						}else{
							$("#dialogMUGMOD").append('<br/>User Service Authorized Successfully');
						}
					}
				}
				if(obj.userUnAuthResponse){
					if(obj.userUnAuthResponse.Unauthorized){
						if(obj.userUnAuthResponse.Unauthorized.Error){
							$("#dialogMUGMOD").append('<br/>' + obj.userUnAuthResponse.Unauthorized.Error);
							UUnAuthError = "1";
						}else{
							$("#dialogMUGMOD").append('<br/>User Service UnAuthorized Successfully');
						}
					}
				}
				if(obj.servicePackResponse){
					if(obj.servicePackResponse.newAuth){
						if(obj.servicePackResponse.newAuth.Error){
							$("#dialogMUGMOD").append('<br/>' + obj.servicePackResponse.newAuth.Error.Detail);
							spNewAuthError = "1";
						}else{
							$("#dialogMUGMOD").append('<br/>Service Pack Authorized Successfully');
						}
					}
					if(obj.servicePackResponse.unAuth){
						if(obj.servicePackResponse.unAuth.Error){
							$("#dialogMUGMOD").append('<br/>' + obj.servicePackResponse.unAuth.Error.Detail);
							spUnAuthError = "1";
						}else{
							$("#dialogMUGMOD").append('<br/>Service Pack UnAuthorized Successfully');
						}
					}
				}
				if(obj.groupServiceAssignResponse){
					if(obj.groupServiceAssignResponse.assigned){
						if(obj.groupServiceAssignResponse.assigned.Error){
							$("#dialogMUGMOD").append('<br/>' + obj.groupServiceAssignResponse.assigned.Error);
							grpAssignError = "1";
						}else{
							$("#dialogMUGMOD").append('<br/>Group Service Assigned Successfully');
						}
					}
					if(obj.groupServiceAssignResponse.usAssigned){
						if(obj.groupServiceAssignResponse.usAssigned.Error){
							$("#dialogMUGMOD").append('<br/>' + obj.groupServiceAssignResponse.usAssigned.Error);
							grpAssignError = "1";
						}else{
							$("#dialogMUGMOD").append('<br/>Group Service UnAssigned Successfully');
						}
					}
				}
				
				if(obj.voicePortalResponse){
					if(obj.voicePortalResponse.Error){
							$("#dialogMUGMOD").append('<br/>' + obj.voicePortalResponse.Error);
							voicePortalError = "1";
					}else{
						$("#dialogMUGMOD").append('<br/>Voice Portal Updated Successfully');
					}
				}
				if(obj.ncsAssignResponse){
					if(obj.ncsAssignResponse.assigned.Error){
						$("#dialogMUGMOD").append('<br/>' + obj.ncsAssignResponse.assigned.Error);
						ncsAssignedError = "1";
					}else{
						$("#dialogMUGMOD").append('<br/>NCS Assigned Successfully');
					}
					
				}
				if(obj.ncsUnassignResp){
					if(obj.ncsUnassignResp.unAssigned.Error){
						$("#dialogMUGMOD").append('<br/>' + obj.ncsUnassignResp.unAssigned.Error.Detail);
						ncsUnAssignedError = "1";
					}else{
						$("#dialogMUGMOD").append('<br/>NCS UnAssigned Successfully');
					}
				}
				if(obj.tabName){
					$(".ui-dialog-buttonpane button:contains('Next')").button().hide();
				}else{
					$(".ui-dialog-buttonpane button:contains('Next')").button().hide();
				}
				
				if(groupError == "1" || routingError == "1" || domainError == "1" || callProcessingError == "1" || dnError == "1" || UAuthError == "1" || UUnAuthError == "1" || spNewAuthError == "1" || spUnAuthError == "1" || gpError == "1" || grpAssignError =="1" || grpUnAuth == "1" || voicePortalError == "1" || ncsAssignedError =="1" || ncsUnAssignedError == "1" || dnActivateError == "1"){
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
				}else{
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();	
					$(".ui-dialog-buttonpane button:contains('Done')").button().show().addClass('subButton');
				}
					$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled").addClass('subButton');
					$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
					$(".ui-dialog-buttonpane button:contains('More changes')").button().show().addClass('cancelButton');
					$(".ui-dialog-buttonpane button:contains('Return to Main')").button().show().addClass('cancelButton');
					$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
				
					$('#selectGroup').prop('disabled', false);
					$("#activateNewAssignedSpan").css("opacity","1");
				},
		});
	};

    $("#subButton").click(function() {
    	
    	$("#loading2").show();
		$("#groupDetails").hide();
		var selectGroupId = $("#selectGroup").val();
		var groupArray = selectGroupId.split(" ");
        var selectGroupId = groupArray[0];
    	var groupId = $("#groupId").val();
    	var timeZone = $("#timeZone").val();
    	var userLimit = $("#userLimit").val();
    	var minExtensionLength = $("#minExtensionLength").val();
    	var maxExtensionLength = $("#maxExtensionLength").val();
    	var defaultExtensionLength = $("#defaultExtensionLength").val();
    	$("#activateNewAssigned").removeAttr("disabled");
    	var modifyGroupId = $("#modifyGroupId").val();
    	
    	var spanGroupId = $("#spanGroupId").text();
    	$("#dialogMUGMOD").html('');
    	$(".ui-dialog-buttonpane button:contains('Next')").button().hide();
    	if(modifyGroupId == ""){
    		validateCreateGroup();
    		$('#selectGroup').prop('disabled', 'disabled');
    	}else{
    		
    		//if($("#assignNumbers option").length > 0 && $('#assignNumbers option:selected').length == 0){
    			$('#assignNumbers option').prop('selected', true);
    		//}
    		if($("#assignedNCS option").length > 0 && $('#assignedNCS option:selected').length == 0){
    			$('#assignedNCS option').prop('selected', true);
    		}
    		if($("#assignedDefault option").length > 0 && $('#assignedDefault option:selected').length == 0){
    			$('#assignedDefault option').prop('selected', true);
    		}
    		if($("#assignedNCS option").length > 0 || $('#assignedNCS option:selected').length == 0){
    			$('#assignedNCS option').prop('selected', true);
    		}

    		compareChangesResult();
    	}
    	$('html, body').animate({scrollTop: '0px'}, 300);
	});
 	 
	
    //checkResult(true); // this function used for user modify or group if any changes and switch to different module.It is define in main.php and main_enterprise.php.
  // getPreviousModuleId("groupModify"); // this function used for check previous id it is define in main.php and main_enterprise.php.
    	    
    var compareChangesResult = function(){
    	//var dataToSend = $("form#groupModify").serializeArray();
    	var dataToSend = $("form#groupModify :input[name != 'assignNumbers[]']").serializeArray();
    	dataToSend = getAssignNumbers(dataToSend);
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/validate.php",
			data: {compare : 1, formdata : dataToSend},
			success: function(result) {
				$("#dialogMUGMOD").dialog("open");
				$("#loading2").hide();
				$("#groupDetails").show();
				var errorExist = result.search("#ac5f5d");
				var noChangesExist = result.search("No Changes");
				
				if(errorExist > 0 || noChangesExist > 0){
					$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
				}else{
					$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled").show().addClass('subButton');
				}
				$(".ui-dialog-buttonpane button:contains('Continue')").button().hide();
				$("#dialogMUGMOD").dialog("option", "title", "Request Complete");
				$("#dialogMUGMOD").html('<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelist" class="confSettingTable"><tbody><tr><th colspan="2" align="center">Please confirm the settings below and click Complete to process your modifications.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');		    	
				$("#changelist").append(result);
				$("#dialogMUGMOD").append('</tbody></table>');
				
			}
		});
	};
	
    var validateCreateGroup = function(){
    	var dataToSend = $("form#groupModify").serializeArray();
		$.ajax({
			type: "POST",
			url: "groupManage/groupAdd/validate.php",
			data: {compare : 1, formdata : dataToSend},
			success: function(result) { 
				$("#loading2").hide();
				$("#groupDetails").show();
				var errorExist = result.search("#ac5f5d");
				var noChangesExist = result.search("No Changes");
				
				if(errorExist > 0 || noChangesExist > 0){
					$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
				}else{
					$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled subButton");
				}
				$("#dialogMUGMOD").dialog("option", "title", "Request Complete");
				$("#dialogMUGMOD").html('<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelist" class="confSettingTable"><tbody><tr><th colspan="2" align="center">Please confirm the settings below and click Complete to process your modifications.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');
		    	
				$("#changelist").append(result);
				$("#dialogMUGMOD").append('</tbody></table>');
				$("#dialogMUGMOD").dialog("open");
			}
		});
	};
	
	$("#delGroup").click(function(){
		
		$("#dialogMUGMOD").dialog("open");
		$("#dialogMUGMOD").dialog("option", "title", "Request Delete");
		
		$("#dialogMUGMOD").html("Are you sure you want to delete this group?");
		$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
		$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
		$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
		$(".ui-dialog-buttonpane button:contains('Next')").button().hide();
		$(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass('deleteButton');
		$(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
		
	});	
	
	$("#useGroupName").click(function(){
		if ($("#useGroupName").is(':checked')){
			$('#allowDepartmentCLIDNameOverride').attr( "disabled", false );
			$('.textDisable').css( "opacity", "1" );
		}else{
			$('#allowDepartmentCLIDNameOverride').attr( "disabled", true );
			$('#allowDepartmentCLIDNameOverride').attr('checked', false);
			$('.textDisable').css( "opacity", "0.4" );
		}
	});
	if ($("#useGroupName").is(':checked')){
		$('.textDisable').css( "opacity", "1" );
	}else{
		$('.textDisable').css( "opacity", "0.4" );
	}

	
	var basicGroupForm = function(){
		$("#extensionLength").val("");
		$("#spanGroupId").hide();
		var isGroup = $("#isGroupAccess").val();
		if(enterpriseLevel == "true"){
			$("#groupId").show();
		}else{
			$("#groupId").hide();
		}		
	}
	var domainGroupForm = function(){
		$("#sortable_2").empty();
		$("#sortable2").empty();
		var domainList = "";
		 
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/domains.php",
			success: function(result) {
				var obj = jQuery.parseJSON(result);
				
				for (var i = 0; i < obj.Success.domain.length; i++) {
					domainList += '<option value="'+obj.Success.domain[i]+'">' + obj.Success.domain[i] + '</option>';
				}
				$("#defaultDomain").html(domainList);
				$("#defaultDomain").val(obj.Success.serviceProviderDefaultDomain);
			}
		});
	}
	
	var callProcessingForm = function(){
		$("#useGroupName").prop("disabled", true);
		$("#allowDepartmentCLIDNameOverride").prop("disabled", true);
		
		$('input[name=useGroupCLIDSetting][value=false]').prop('checked', 'checked');
		$('input[name=clidPolicy][value="Use DN"]').prop('checked', 'checked');
		$('input[name=emergencyClidPolicy][value="Use DN"]').prop('checked', 'checked');
		$('input[name=useGroupMediaSetting][value="false"]').prop('checked', 'checked');
		$('input[name=mediaPolicySelection][value="No Restrictions"]').prop('checked', 'checked');
		$('input[name=useGroupCallLimitsSetting][value="false"]').prop('checked', 'checked');
		$('input[name=useGroupTranslationRoutingSetting][value="false"]').prop('checked', 'checked');
		$('input[name=networkUsageSelection][value="Do Not Force Enterprise and Group Calls"]').prop('checked', 'checked');
		$('input[name=useGroupDCLIDSetting][value="false"]').prop('checked', 'checked');
		$('input[name=enableDialableCallerID][value="false"]').prop('checked', 'checked');
		
		$('#allowAlternateNumbersForRedirectingIdentity').prop('checked', true);
		$('#allowConfigurableCLIDForRedirectingIdentity').prop('checked', true);
		$('#blockCallingNameForExternalCalls').prop('checked', false);
		$('#useMaxSimultaneousCalls').prop('checked', false);
		$('#useMaxSimultaneousVideoCalls').prop('checked', false);
		$('#useMaxCallTimeForAnsweredCalls').prop('checked', false);
		$('#useMaxCallTimeForUnansweredCalls').prop('checked', false);
		$('#useMaxConcurrentRedirectedCalls').prop('checked', false);
		$('#useMaxConcurrentFindMeFollowMeInvocations').prop('checked', false);
		$('#useMaxFindMeFollowMeDepth').prop('checked', false);
		$('#enforceCallingLineIdentityRest').prop('checked', false);
		$('#allowEnterpriseGroupCallTypingForPrivateDialingPlan').prop('checked', false);
		$('#allowEnterpriseGroupCallTypingForPublicDialingPlan').prop('checked', false);
		$('#overrideCLIDRestrictionForPrivateCallCategory').prop('checked', false);

		$('#maxSimultaneousCalls').val(10);
		$('#maxSimultaneousVideoCalls').val(5);
		$('#maxCallTimeForAnsweredCallsMinutes').val(600);
		$('#maxCallTimeForUnansweredCallsMinutes').val(2);
		$('#maxConcurrentRedirectedCalls').val(3);
		$('#maxConcurrentFindMeFollowMeInvocations').val(3);
		$('#maxFindMeFollowMeDepth').val(3);
		$('#maxRedirectionDepth').val(3);
	}
	
	var serviceGroupForm = function(){
		$(".serviceColumn div:nth-child(3) input").prop('disabled', 'disabled');
		$('#selectAll').click(function(){
			$(".serviceColumn div:first-child input").prop('checked', true);
			$(".serviceColumn div:nth-child(3) input").prop('disabled', false);
		});
		$('#deSelectAll').click(function(){
			$(".serviceColumn div:first-child input").prop('checked', false);
			$(".serviceColumn div:nth-child(3) input").prop('disabled', 'disabled');
			$(".serviceColumn div:nth-child(3) input").prop('checked', false);
		});
		$(".serviceColumn div:first-child input").click(function() {
			this.parentNode.parentNode.querySelector('[type="text"]').disabled = !this.checked;
			//this.parentNode.parentNode.querySelector('[type="checkbox"]').prop("disabled", "false");
			$(this).parent('div').parent('div').find('.span_8').find('input[type=checkbox]').prop('disabled', false);			
		});

	};

	var createGroupForm = function(){ 
		$('#selectGroup').prop('disabled', 'disabled');
		
		$("#groupMainPageP").hide();
		$("input[type=text]").val('');
		$("#modifyGroupId").val('');
		$("#spanGroupId").text('');
		$("select").prop("selectedIndex", 0);
		$("#subButtonDel").hide();
		$("#tabName").val("basicInfo");
		$("#operation").val("groupAdd");
		$("#tabs").tabs({disabled: [0,1,2,3,4,5,6,7,8]});		
		
		$("#userLimit").val('999999');

		basicGroupForm();
		callProcessingForm();
		serviceGroupForm();
		domainGroupForm();
		$('#selectGroup').prop('disabled', false);
		$("#groupDetails").show();
		$("#loading2").hide();
		$('html, body').animate({scrollTop: '0px'}, 300);
		$(".ui-dialog-buttonpane button:contains('Next')").button().hide();
	};
	
	/* FUNCTION USE FOR CHECK GROUP CHANGES */
	
	function checkGroupInfoHasChange(currentClickEvent){
		buttonNameClickEvent = currentClickEvent;
		  $("#groupModifyPreventDialog").dialog('close');
		//$("#groupDetails").show();
		//var selectGroupId = $("#selectGroup").val();
		//var groupArray = selectGroupId.split(" ");
		//var selectGroupId = groupArray[0];
		var selectGroupId = $("#isGroupAccess").val();
		var groupId = $("#groupId").val();
		var timeZone = $("#timeZone").val();
		var userLimit = $("#userLimit").val();
		var minExtensionLength = $("#minExtensionLength").val();
		var maxExtensionLength = $("#maxExtensionLength").val();
		var defaultExtensionLength = $("#defaultExtensionLength").val();
		$("#activateNewAssigned").removeAttr("disabled");
		var modifyGroupId = $("#modifyGroupId").val();
		var spanGroupId = $("#spanGroupId").text();  
		 
		//if($("#assignNumbers option").length > 0 && $('#assignNumbers option:selected').length == 0){
		//$('#assignNumbers option').prop('selected', true); //Code commented @ 13 March 2019 regarding EX-1093
			//}
			if($("#assignedNCS option").length > 0 && $('#assignedNCS option:selected').length == 0){
				$('#assignedNCS option').prop('selected', true);
			}
			if($("#assignedDefault option").length > 0 && $('#assignedDefault option:selected').length == 0){
				$('#assignedDefault option').prop('selected', true);
			}
			if($("#assignedNCS option").length > 0 || $('#assignedNCS option:selected').length == 0){
				$('#assignedNCS option').prop('selected', true);
			} 
//debugger ;

                        var dataToSendSerial = $("form#groupModify :input[name != 'assignNumbers[]']").serializeArray();
                        //dataToSendSerial = getAssignNumbers(dataToSendSerial);
			//var dataToSendSerial =  $("form#groupModify").serializeArray();
			var urlData = "groupManage/groupModify/validate.php";
			var ajaxData = {compare : 1, formdata : dataToSendSerial, prevmodule: 'preventChanges'};
						
			$.ajax({
				type: "POST",
				url: urlData,
				data: ajaxData,
				success: function(result) {
					$('#selectGroup').prop('disabled', true);
					var errorExist = result.search("#FB6071");
					var errorExistError = result.search("#ac5f5d");
					var noChangesExist = result.search("No Changes");
					var messageDialog = "Modify Group";										

				if(errorExist > 0 || noChangesExist > 0 || errorExistError >0){                         
						 checkResult(false);
						 if(buttonNameClickEvent =='newGroup'){newGroup();}
						 if(buttonNameClickEvent =='groupMainPage'){groupMainPageReturn();}
						 if(buttonNameClickEvent =='changeListReturn'){$("#loading2").show(); showGroupInfo(selectGroupId);}
						 $("#groupModifyPreventDialog").dialog('close');
						
					}else{									
						$("#groupModifyPreventDialog").html('Are you sure you want to leave? <br/> You might lose any change you have made for this Modify Group ');
						$("#groupModifyPreventDialog").dialog('open');
					}
			  }
			});
		}
	
	var groupMainPageReturn = function(){ 
	var searchData    = $("#selectedGroup").val();        
        var clusterName   = $("#clusterNameForGroupMod").val();
        searchData = searchData.split('<span class="search_separator">-</span>');
        searchData = $.trim(searchData[0]);
        if (searchData != "") {
            var groupName = decodString(searchData);
            var selectedSP = $("#selectedSP").val();
            $.ajax({
                type: "POST",
                url: "setGroup.php",
                data: {groupName: groupName, Selsp: selectedSP, clusterName: clusterName},
                success: function (result) { 
                    console.log(result);
                    result = result.trim();
                    if (result.slice(0, 1) == "1") {
                        alert("Error:  Group not found: " + groupName);
                    } else {
                        updateLastAccessed({groupName:groupName, Selsp:selectedSP, clusterName: clusterName});
                        $("#logoImage").attr("href", "main.php");
                        window.location.replace("main.php");
                    }
                }
            });
        }
	}
 
 var newGroup = function(){
      
		$("#groupMainPageP").hide() ;
				$("#modifyGrouptitle").hide() ;
					 checkTabsCondition = "disable";
				createGroupForm();
				$(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
				var maxTime = 60000;
				var time = 0;
				var interval = setInterval(function () {
				  if($('#groupDetails').is(':visible')) {
					  $("#tabs").tabs('enable', 0);
					  $( "#tabs" ).tabs({ active: 0 });
					clearInterval(interval);
				  } else {
					if (time > maxTime) {
					  clearInterval(interval);
					  return;
					}
					time += 100;
				  }
				}, 200);
				intializeExtension(4,4,4);
	}
	

	
$("#newGroup").click(function(){
	if(isCheckDataModify == true){
		$("#groupModifyPreventDialog").dialog('close');
		checkGroupInfoHasChange('newGroup'); 
	}else{
            checkResult(false);
            newGroup();
        }
	
});
	
	/* $("#newGroup").click(function(){
		checkResult(false); // this function used for user modify or group if any changes and switch to different module.It is define in main.php
		//ex-983
	 		$("#groupMainPageP").hide() ;
	 		$("#modifyGrouptitle").hide() ;
             checkTabsCondition = "disable";
		createGroupForm();
		$(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
		var maxTime = 60000;
    	var time = 0;
    	var interval = setInterval(function () {
    	  if($('#groupDetails').is(':visible')) {
    		  $("#tabs").tabs('enable', 0);
    		  $( "#tabs" ).tabs({ active: 0 });
    	    clearInterval(interval);
    	  } else {
    	    if (time > maxTime) {
    	      clearInterval(interval);
    	      return;
    	    }
    	    time += 100;
    	  }
    	}, 200);
    	intializeExtension(4,4,4);
	}); */

	$("#addDnsServiceProvider").click(function(){
		$("#errorDnsGroup").html('');
		$("#showAddDnServiceProvider").slideDown();
		$("#addDnsServiceProvider").hide();
		$("#closeServiceProviderDnsRemove").trigger("click");
	});
	
	$("#closeServiceProviderDns").click(function(){
		$("#showAddDnServiceProvider").slideUp();
		$("#addDnsServiceProvider").show();
		$("input[name=minPhoneNumber]").val('');
		$("input[name=maxPhoneNumber]").val('');
		$("input[name=phoneNumber]").val('');
		$(".extraAddRow").remove();
	});
	
	$("#addServiceProviderDns").click(function(){ 
		
		
		var phoneNumberValues = [];	
		var inputs = $("input[name=phoneNumber]");	
		if(inputs.length > 0){
			for(var i = 0; i < inputs.length; i++){
				if($(inputs[i]).val() != ""){
					phoneNumberValues.push($(inputs[i]).val());
				}
			}
		}
		
		
		var phoneRangeNumberValues = [];
		phoneRangeNumberValues['min'] = [];	 
		phoneRangeNumberValues['max'] = [];	 
		var inputRmin = $("input[name=minPhoneNumber]");	
		var inputRmax = $("input[name=maxPhoneNumber]");	
		if(inputRmin.length > 0 && inputRmax.length > 0){
			for(var i = 0; i < inputRmin.length; i++){
				if($(inputRmin[i]).val() != "" && $(inputRmax[i]).val() != ""){
					phoneRangeNumberValues['min'].push($(inputRmin[i]).val());
					phoneRangeNumberValues['max'].push($(inputRmax[i]).val());
					$("#phoneRangeDiv").find('div').eq(i).find('input[name=minPhoneNumber]').css('border', '1px solid #000');
					$("#phoneRangeDiv").find('div').eq(i).find('input[name=maxPhoneNumber]').css('border', '1px solid #000');
				}else if($(inputRmin[i]).val() == "" && $(inputRmax[i]).val() != ""){
					$("#phoneRangeDiv").find('div').eq(i).find('input[name=minPhoneNumber]').css('border', '1px solid red');
					$("#phoneRangeDiv").find('div').eq(i).find('input[name=maxPhoneNumber]').css('border', '1px solid #000');
					return false;
				}else if($(inputRmin[i]).val() != "" && $(inputRmax[i]).val() == ""){
					$("#phoneRangeDiv").find('div').eq(i).find('input[name=maxPhoneNumber]').css('border', '1px solid red');
					$("#phoneRangeDiv").find('div').eq(i).find('input[name=minPhoneNumber]').css('border', '1px solid #000');
					return false;
				}
			}
		}
		//console.log(phoneNumberValues.length);
		//console.log(phoneRangeNumberValues);
		if(phoneNumberValues.length > 0 || phoneRangeNumberValues['min'].length > 0){
			//$("#errorDnsGroup").html('Adding the DNs....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
			$("#dialogMUGMOD").dialog("open");
			$("#dialogMUGMOD").dialog("option", "title", "DN's request");
			$("#dialogMUGMOD").html('Adding DN\'s to your Service Provider....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
			
			$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
			$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();

		}else{
			return false;
		}
		
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/saveServiceProviderDns.php",
			data: {phoneNumber : phoneNumberValues, minPhoneNumber : phoneRangeNumberValues['min'], maxPhoneNumber : phoneRangeNumberValues['max']},
			success: function(result) { 
				var obj = jQuery.parseJSON(result); 
				dnsList();
				if(obj.Error){
					//$("#errorDnsGroup").html(obj.Error.summaryEnglish);		
					$("#dialogMUGMOD").html(obj.Error);
					$(".ui-dialog-buttonpane button:contains('Continue')").button().show().addClass('subButton');
				}else{
					//$("#errorDnsGroup").html(obj.Success);
					$(".ui-dialog-buttonpane button:contains('Continue')").button().show().addClass('subButton');
					$("input[name=phoneNumber]").val('');
					$("input[name=minPhoneNumber]").val('');
					$("input[name=maxPhoneNumber]").val('');
					 setTimeout(function(){
						$("#dialogMUGMOD").html(obj.Success);
						$("#showAddDnServiceProvider").slideUp();
						$("#addDnsServiceProvider").show();						
					}, 3000);					
				}
			}
		});
		
	});
	
	$("#addNumberRow").click(function(){
		var el = $(this);
		html = '<div class="form-group extraAddRow"><div class="col-md-10 extraAddRow extraDivRow" style="padding-left:0px;"><input class="extraInputRow" type="text" name="phoneNumber" value="" style="width:400px !important;" /></div><div class="col-md-1" style="width:13% !important"><img class="" id="deleteNumberRow" src="images/minus.png" style="width:18px; float:right; margin-top:21px"></div></div>'; 
		el.parent('div').parent('div').parent('div').append(html);
	});
	
	$(document).on("click", "#deleteNumberRow", function(){
		
		var el = $(this);
		el.parent('div').parent('div').remove();
	});
	
	//to create and delete the number range row
	$("#addNumberRangeRow").click(function(){
		var el = $(this);
		 
		html = '<div class="form-group extraAddRow"><div class="extraAddRow extraDivRow col-md-5" style="padding-left:0px"><input class="extraInputRow" type="text" name="minPhoneNumber" value="" style="width:145px !important"/></div><div class="col-md-1" style="padding:0 !important; width:20px; margin:0;color:#555 !important;"><br/>-</div><div class="col-md-5" style="padding-left:0"><input type="text" class="extraInputRow" name="maxPhoneNumber" value="" style="width:145px !important"/></div>';
		html +=	'<div class="col-md-1"><img class="" id="deleteNumberRangeRow" src="images/minus.png" style="width:18px; margin-top:17px"></div></div>'; 
		el.parent('div').parent('div').parent('div').append(html);
	});
	
	$(document).on("click", "#deleteNumberRangeRow", function(){
		var el = $(this);
		el.parent('div').parent('div').remove();
	});
	
	$(document).on("click", ".inputCheckbox", function(){
		   var elems = $(this).parents('tr').find(':checkbox');
		   if($(this).parents('tr').find('input:checkbox:first').is(':checked')){
			   elems.not($(this)).attr('disabled',false);
			   elems.not($(this)).prop('checked', false);
		   }else{
			   elems.not($(this)).attr('disabled',true);
			   elems.not($(this)).prop('checked', false);
		   }
	});

	$(document).on("click", ".inputCheckboxGroup", function(){
		   var elems = $(this).parents('tr').find(':checkbox');
		   if($(this).parents('tr').find('input:checkbox:first').is(':checked')){
			   elems.not($(this)).attr('disabled',false);
			   elems.not($(this)).prop('checked', false);
		   }else{
			   elems.not($(this)).attr('disabled',true);
			   elems.not($(this)).prop('checked', false);
		   }
		   
		   
	});
	$("#subButtonDel").click(function(){
		$("#dialogMUGMOD").dialog("open");
		$("#dialogMUGMOD").html('Checking if User and DN exist in the group....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
		//$(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled");
		$(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
		$("#loading2").show();   
		var html = "";
        $.ajax({
			type: "POST",
			url: "groupManage/groupModify/checkDeleteGroup.php",
			data: {groupId : $("#groupId").val()},
			success: function(result) {
				$("#loading2").hide(); 
				var obj = jQuery.parseJSON(result);
				if(obj.userCount || obj.dn){
					html = "<b>Cannot delete group "+obj.groupId+" for the following resons:</b><br /><br />";
					if(obj.userCount){
						html += "<li style='margin-left:10px;'> " + obj.userCount+ " users are provisioned in the group</li>";
						//$("#dialogMUGMOD").html(html);
						$(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
					}
					if(obj.dn){
						html += "<li style='margin-left:10px;'> " + obj.dn+ " numbers are provisioned in the group</li>";
						//$("#dialogMUGMOD").append(html);
						$(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
					}
					$("#dialogMUGMOD").html(html);
				}else{
					$("#dialogMUGMOD").html("");
					$(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
					$("#dialogMUGMOD").html("Are you sure you want to delete this group?");
				}
			}
		});
		//$("#dialogMUGMOD").html("Are you sure you want to delete this group?");
		$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
		$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
		$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
		//$(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
		$(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass('deleteButton');
		
	});	
	
	var extensionBuilder = function(num, extLen){
		var numLen = num.length;
		var ind = numLen - extLen;
		var extension = num.substring(ind);
		$("#portalExt").val(extension);
	}
	
	var extensionLengthChecker = function(min, max) {	
		var extensionIdVal = $("#portalExt").val();
		var extensionIdLength = $("#portalExt").val().length;
		$("#portalExtErrorMsg").html('');
		$("#subButton").prop("disabled", false);
		$("#portalExt").attr("style", "border: 1px solid #002c60 !important;");
		if(extensionIdLength > 0) {
			if(extensionIdLength < min || extensionIdLength > max) {
				$("#subButton").prop("disabled", true);
				$("#portalExt").attr("style", "border: 1px solid #ac5f5d !important;");
	//			$("#portalExt").css({"border": "1px solid #ac5f5d important"});
	//			$("#portalExt").style.setProperty( 'border', '1px solid #ac5f5d', 'important' );
	//			border: 1px solid red !important;
	
	//    		$("#dialogMUGMOD").dialog("open");
	//    		$("#dialogMUGMOD").dialog("option", "title", "Validation Error");
	//    		$("#dialogMUGMOD").html('Extension length should be minimum ' + min + ' and maximum ' + max);
				$("#portalExtErrorMsg").html('Extension length should be minimum ' + min + ' and maximum ' + max);
	//			$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
	//			$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
	//			$(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
	//			$(".ui-dialog-buttonpane button:contains('Ok')").button().show().addClass('subButton');
				$("#loading2").hide();
	    		return false;    	
			}
		}
	}
	
	$("#portalPhoneNo").change(function(){
		var el = $(this); 
		var phoneNumberVal = el.val();
		var extensioNdefaultLen = el.attr("data-defaultlength");
		extensionBuilder(phoneNumberVal, extensioNdefaultLen);
	});
	
	$("#portalExt").blur(function(){
		var el = $(this); 
		var minLen = el.attr('data-min');
		var maxLen = el.attr('maxlength');
		
		extensionLengthChecker(minLen, maxLen);
	});
	 
	var showGroupListUsingSearch = function(){ 
		//var html = "";
		//$("#selectGroup").html('');
		$("#modifyGrouptitle").hide();
		$("#groupMainPageP").hide();
		var autoComplete = new Array();
	    $.ajax({
	        type: "POST",
	        url: "groupManage/groupModify/groupList.php",
	        success: function(result)
	        {
	        	var obj = jQuery.parseJSON(result);
				for (var i = 0; i < obj.length; i++) {
					autoComplete[i] = obj[i];
				}
	            $("#selectGroup").autocomplete({
	                source: autoComplete,
	                appendTo: "#hidden-stuffGroup"
	            //}).autocomplete( "widget" ).addClass( "groupAutoClass" );
	            }).data("ui-autocomplete")._renderItem = function (ul, item) {
	                ul.addClass('groupAutoClass'); //Ul custom class here
					if(item.value.search('<span class="search_separator">-</span>') > 0){
						return $("<li></li>")
						.append("<a href='#'>" + item.value + "</a>")
						.data("ui-autocomplete-item", item)
						.appendTo(ul);
					}else{
						return $("<li class='ui-menu-item'></li>")
		                .append("<a href='#'>" + item.label + "</a>")
		                .data("ui-autocomplete-item", item)
		                .appendTo(ul);						
					}
	            };
	            
	            
	            
	            var isGroupAcc = $("#isGroupAccess").val();
	            if(enterpriseLevel == "false"){
	            	$("#groupsFormId").hide();
	            }else{
	            	$("#groupsFormId").show();
	            	$("#loading2").hide();
	            }
				$("#groupsFormId").hide();
				var modifyGroupId = $("#modifyGroupId").val();
				
				if(modifyGroupId != ""){
					var mGroupId = modifyGroupId;  
					$('#selectGroup').val(decodString(modifyGroupId.replace('<span class="search_separator">-</span>', '-')));
					var groupArray = modifyGroupId.split('<span class="search_separator">-</span>');
					var modifyGroupId = decodString(jQuery.trim(groupArray[0]));
					getGroupInfo(modifyGroupId);
					
					$("#operation").val("modify");
				}else{
					$("#loading2").hide();
					
				}

			        //$("#groupsFormId").show();
				if(enterpriseLevel == "false"){
					$("#groupsFormId").hide();
			    }else{
					 $("#groupsFormId").show();
					 $("#loading2").hide();
			   }
				
			//	$("#loading2").show();
				$("#groupDetails").hide();
	        }
	    });
	};
	
	var departmentListResultForGroup = function(selectGroupId) {
		$("#departmentGroupFrm").hide();
		var autoCompleteDepartment = new Array();
		
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/groupDept/deptData.php",
			cache: false,
			data: {groupId : selectGroupId},
			success: function(result) {
				console.log(result);
				var obj = jQuery.parseJSON(result);
				$('#selectDepartment').empty('');
				if(obj.Success){
					if(obj.Success.department){
						var departmentList = obj.Success.department;
						var deptDropdown = "<option value=''> None </option>";
						for (var i = 0; i < departmentList.length; i++) {
							deptDropdown += "<option value='"+departmentList[i].name+"'>" + departmentList[i].name + "</option>";
						}
						$("#selectDepartment").html(deptDropdown);
					}
				}else{
					$("#selectDepartment").autocomplete({source: []});
				}
			},
		});
	};
	
	var gropListResponseReturn =  function(){
		$("#groupModifyPreventDialog").dialog('close');
		var groupArray = selectGroupId.split('<span class="search_separator">-</span>');
		var selectGroupId = decodString(jQuery.trim(groupArray[0]));
//        var selectGroupId = groupArray[0];
        $("#isGroupAccess").val(selectGroupId);
		departmentListResultForGroup(selectGroupId);
		var groupVal = decodString(el[0].innerHTML.replace('<span class="search_separator">-</span>', '-'));
		$('#selectGroup').val(decodString(groupVal));
		$('#selectGroup').prop('disabled', true);
		$('#search').prop('disabled', true);
		 selectGroupEnabled(selectGroupId);
		checkGroupInfoHasChange('changeListReturn');
	}
	
	
	
	$(document).on("click", ".groupAutoClass > li > a", function(){
		//debugger ;
		$("#groupModifyPreventDialog").dialog('close');
                checkResult(false);// 04_march
		var el = $(this);
		var selectGroupId = el.html();
		if(selectGroupId == ""){
			$("#groupDetails").hide();
			$("#loading2").hide();
		
			return false;
		}
		$("#groupDetails").hide();
		var groupArray = selectGroupId.split('<span class="search_separator">-</span>');
		var selectGroupId = decodString(jQuery.trim(groupArray[0]));
//        var selectGroupId = groupArray[0];
        $("#isGroupAccess").val(selectGroupId);
		departmentListResultForGroup(selectGroupId);
		var groupVal = decodString(el[0].innerHTML.replace('<span class="search_separator">-</span>', '-'));
		$('#selectGroup').val(decodString(groupVal));
		$('#selectGroup').prop('disabled', true);
		$('#search').prop('disabled', true);
		selectGroupEnabled(selectGroupId);
		checkGroupInfoHasChange('changeListReturn');
		
	});
	
	
	/* $(document).on("click", ".groupAutoClass > li > a", function(){
		var el = $(this);
		$("#loading2").show();
		var selectGroupId = el.html();
		if(selectGroupId == ""){
			$("#groupDetails").hide();
			$("#loading2").hide();
			$('#selectGroup').prop('disabled', false);
			return false;
		}
		
		var groupArray = selectGroupId.split('<span class="search_separator">-</span>');
		var selectGroupId = decodString(jQuery.trim(groupArray[0]));
//        var selectGroupId = groupArray[0];
        $("#isGroupAccess").val(selectGroupId);
		departmentListResultForGroup(selectGroupId);
		var groupVal = decodString(el[0].innerHTML.replace('<span class="search_separator">-</span>', '-'));
		$('#selectGroup').val(decodString(groupVal));
		$('#selectGroup').prop('disabled', true);
		$('#search').prop('disabled', true);
		
		showGroupInfo(selectGroupId);
	}); */
	
	var isGroup = "";
	isGroup = $("#isGroupAccess").val();
	
	if(isGroup){
		enableTabs();
		getGroupInfo(isGroup);
		$("#groupsFormId").hide();
		$("#groupId").hide();
		$("#subButtonDel").hide();
		$("#grpButtonCancel").hide();
	}
	
	$("#showRemoveDnServiceProvider").hide();
	
	$("#addRemoveDnsServiceProvider").click(function(){
		$("#errorDnsRemoveGroup").html('');
		$("#showRemoveDnServiceProvider").slideDown();
		$("#addRemoveDnsServiceProvider").hide();
		$("#closeServiceProviderDns").trigger("click");
	});
	
	$("#closeServiceProviderDnsRemove").click(function(){
		$("#showRemoveDnServiceProvider").slideUp();
		$("#addRemoveDnsServiceProvider").show();
		$("input[name=minPhoneNumberRemove]").val('');
		$("input[name=maxPhoneNumberRemove]").val('');
		$("input[name=removePhoneNumber]").val('');
		$(".extraRemoveRow").remove();
	});
	
	$("#removeServiceProviderDns").click(function(){ 
		
		var phoneNumberValues = [];	
		var inputs = $("input[name=removePhoneNumber]");	
		if(inputs.length > 0){
			for(var i = 0; i < inputs.length; i++){
				if($(inputs[i]).val() != ""){
					phoneNumberValues.push($(inputs[i]).val());
				}
			}
		}
		
		var phoneRangeNumberValues = [];
		phoneRangeNumberValues['min'] = [];	 
		phoneRangeNumberValues['max'] = [];	 
		var inputRmin = $("input[name=minPhoneNumberRemove]");	
		var inputRmax = $("input[name=maxPhoneNumberRemove]");	
		if(inputRmin.length > 0 && inputRmax.length > 0){
			for(var i = 0; i < inputRmin.length; i++){
				if($(inputRmin[i]).val() != "" && $(inputRmax[i]).val() != ""){
					phoneRangeNumberValues['min'].push($(inputRmin[i]).val());
					phoneRangeNumberValues['max'].push($(inputRmax[i]).val());
					$("#removePhoneRangeDiv").find('div').eq(i).find('input[name=minPhoneNumberRemove]').css('border', '1px solid #000');
					$("#removePhoneRangeDiv").find('div').eq(i).find('input[name=maxPhoneNumberRemove]').css('border', '1px solid #000');
				}else if($(inputRmin[i]).val() == "" && $(inputRmax[i]).val() != ""){
					$("#removePhoneRangeDiv").find('div').eq(i).find('input[name=minPhoneNumberRemove]').css('border', '1px solid red');
					$("#removePhoneRangeDiv").find('div').eq(i).find('input[name=maxPhoneNumberRemove]').css('border', '1px solid #000');
					return false;
				}else if($(inputRmin[i]).val() != "" && $(inputRmax[i]).val() == ""){
					$("#removePhoneRangeDiv").find('div').eq(i).find('input[name=maxPhoneNumberRemove]').css('border', '1px solid red');
					$("#removePhoneRangeDiv").find('div').eq(i).find('input[name=minPhoneNumberRemove]').css('border', '1px solid #000');
					return false;
				}
			}
		}
		//console.log(phoneNumberValues.length);
		//console.log(phoneRangeNumberValues);
		if(phoneNumberValues.length > 0 || phoneRangeNumberValues['min'].length > 0){
			//$("#errorDnsGroup").html('Adding the DNs....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
			$("#dialogMUGMOD").dialog("open");
			$("#dialogMUGMOD").dialog("option", "title", "DN's request");
			$("#dialogMUGMOD").html('Removing DN\'s from your Service Provider....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
			
			$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
			$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();

		}else{
			return false;
		}
		
		$.ajax({
			type: "POST",
			url: "groupManage/groupModify/deleteServiceProviderDns.php",
			data: {phoneNumber : phoneNumberValues, minPhoneNumber : phoneRangeNumberValues['min'], maxPhoneNumber : phoneRangeNumberValues['max']},
			success: function(result) { 
                var obj = jQuery.parseJSON(result); 
				dnsList();
				if(obj.Error){
					//$("#errorDnsGroup").html(obj.Error.summaryEnglish);		
					$("#dialogMUGMOD").html(obj.Error);
					$(".ui-dialog-buttonpane button:contains('Continue')").button().show().addClass('subButton');
				}else{
					//$("#errorDnsGroup").html(obj.Success);
					$(".ui-dialog-buttonpane button:contains('Continue')").button().show().addClass('subButton');
					$("input[name=removePhoneNumber]").val('');
					$("input[name=minPhoneNumberRemove]").val('');
					$("input[name=maxPhoneNumberRemove]").val('');
					 setTimeout(function(){
						$("#dialogMUGMOD").html(obj.Success);
						$("#showRemoveDnServiceProvider").slideUp();
						$("#addRemoveDnsServiceProvider").show();	
						dnsList();
					}, 3000);					
				}
			}
		});
		
	});
	
	$("#addNumberRowRemove").click(function(){
		var el = $(this);
		html = '<div class="extraRemoveRow form-group"><div class="col-md-10" style="padding-left:0"><input class="extraInputRow" type="text" name="removePhoneNumber" value="" style="width:400 !important; float:left" /></div><div class="col-md-1" style="width:13% !important"><br/><img class="" id="deleteNumberRowRemove" src="images/minus.png" style="width:18px; float:right;"></div></div>'; 
		el.parent('div').parent("div").parent('div').append(html);
	});
	
	$(document).on("click", "#deleteNumberRowRemove", function(){
		var el = $(this);
		el.parent('div').parent('div').remove();
	});
	
	//to create and delete the number range row
	$("#addNumberRangeRowRemove").click(function(){
		var el = $(this);
		html = '<div class="extraRemoveRow form-group"><div class="col-md-5" style="padding-left:0px;"><input class="extraInputRow" type="text" name="minPhoneNumberRemove" value="" style="width:145px !important"/></div><div class="col-md-1" style="padding:0 !important; width:20px; margin:0"><br/><span style="width:20px; margin-top:17px;color:#555 !important;">-</span></div><div class="col-md-5" style="padding-left:0px;"><input class="extraInputRow" type="text" name="maxPhoneNumberRemove" value="" style="width:145px !important;"/></div>';
		html +=	'<div class="col-md-1"><img class="" id="deleteNumberRangeRowRemove" src="images/minus.png" style="width:18px; margin-top:17px;"></div>'; 
		el.parent('div').parent('div').parent('div').append(html);
	});
	
	$(document).on("click", "#deleteNumberRangeRowRemove", function(){
		var el = $(this);
		el.parent('div').parent('div').remove();
	});
	
	$("#activateAllAssigned").click(function(){
		if($('#activateAllAssigned').is(':checked')){
			$("#activateNewAssigned").prop('checked', true);
			$("#activateNewAssigned").prop('disabled', true);
			$("#activateNewAssignedSpan").attr("style", "opacity: .45");
		}else{
			$("#activateNewAssigned").prop('checked', false);
			$("#activateNewAssigned").prop('disabled', false);
			$("#activateNewAssignedSpan").attr("style", "opacity: 1");
		}
	});
	
	// Go To Group Main Page
	$("#groupMainPage").click(function () {
        //if(isCheckDataModify == true){
		//	checkGroupInfoHasChange('groupMainPage');
	//}
        
        if(isCheckDataModify == true){
		$("#groupModifyPreventDialog").dialog('close');
                checkGroupInfoHasChange('groupMainPage');
	}else{
            checkResult(false);
           groupMainPageReturn();
        }
	
 });
	 
	
	/* $("#groupMainPage").click(function () {
		checkResult(false);
		var searchData = $("#selectedGroup").val();
        searchData = searchData.split('<span class="search_separator">-</span>');
        searchData = $.trim(searchData[0]);
        if (searchData != "") {
            var groupName = decodString(searchData);
            var selectedSP = $("#selectedSP").val();
            $.ajax({
                type: "POST",
                url: "setGroup.php",
                data: {groupName: groupName, Selsp: selectedSP},
                success: function (result) { 
                    console.log(result);
                    result = result.trim();
                    if (result.slice(0, 1) == "1") {
                        alert("Error:  Group not found: " + groupName);
                    } else {
                        $("#logoImage").attr("href", "main.php");
                        window.location.replace("main.php");
                    }
                }
            });
        }
    }); */
	
	// End Go To Group Main Page

});

function updateLastAccessed(dataToSend) {
	$.ajax({
        type: "POST",
        url: "changeGroupDialog/setChangeGroupDetails.php",
			data: {module: "clickedPrevChangeGroup", spId: dataToSend.Selsp, groupId: dataToSend.groupName, clusterName: dataToSend.clusterName},
			success: function (result) {
				console.log(result);                              
         }
     });
}

function getAssignNumbers(dataToSend) {
    var assignNumber = "";
    var tempArr = [];
    var dataToSendTemp = $("form#groupModify").serializeArray();
    dataToSendTemp.forEach(function(value,i) {
        if(value.name == "assignNumbers[]") {
        	tempArr.push(i);
			assignNumber += value.value + ";;";
        }
    });
    dataToSend.push({name:'assignNumbers', value: assignNumber});
    return dataToSend;
}

function decodString(encodedStr) {
	var parser = new DOMParser;
	var dom = parser.parseFromString(
	    '<!doctype html><body>' + encodedStr,
	    'text/html');
	var decodedString = dom.body.textContent;
	return decodedString;
}

if ($.fn.button.noConflict) {
	var bootstrapButton = $.fn.button.noConflict();
	$.fn.bootstrapBtn = bootstrapButton;
	}