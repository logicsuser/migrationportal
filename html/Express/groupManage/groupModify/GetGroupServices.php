<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");

$_POST['groupId'] = htmlspecialchars($_POST['groupId']);
if(isset($_POST['groupId']) && !empty($_POST['groupId'])){
	$array = array();
	$grpServiceList = new Services();
	$grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_POST['groupId']);
	
	function sortGroupServices($a,$b)
	{
		return ($a[0]<= $b[0]) ? -1 : 1;
	}
	usort($grpServiceListResponse["Success"]["groupServicesAuthorizationTable"], "sortGroupServices");

	function sortUserService($a,$b)
	{
		return ($a[0]<= $b[0]) ? -1 : 1;
	}
	usort($grpServiceListResponse["Success"]["userServicesAuthorizationTable"], "sortUserService");
	
	function sortServicePackTable($a,$b)
	{
		return ($a[0]<= $b[0]) ? -1 : 1;
	}
	usort($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"], "sortServicePackTable");

	echo json_encode($grpServiceListResponse);
}
?>