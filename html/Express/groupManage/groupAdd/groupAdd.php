<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupAdd/groupAdd.php");
require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupOperation.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
$objServices = new Services();

require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php"); //Added @ 31 May 2018

$info = new GroupAdd(0);
$modify = array();

if(!empty($_SESSION["tabName"])){
	$modify["tabName"] = $_SESSION["tabName"];
	$_SESSION["isGroupCreate"] = true;
}

$userArray = array("0" => "Outgoing Calling Plan");

server_fail_over_debuggin_testing(); /* testing server fail over*/

if(isset($_POST['groupId']) && !empty($_POST['groupId'])){
    $_POST['groupId'] = $_POST['groupId'];
    $_POST['groupName'] = $_POST['groupName'];
    
	$modify['groupAddResponse'] = $info->groupAddRequest($_POST);
	
	if(empty($modify['groupAddResponse']['Error'])){
		$dnsObj = new Dns();
// 		$extensionArray['minExtensionLength'] = $_POST['minExtensionLength'];
// 		$extensionArray['maxExtensionLength'] = $_POST['maxExtensionLength'];
// 		$extensionArray['defaultExtensionLength'] = $_POST['defaultExtensionLength'];

		$extensionArray['minExtensionLength'] = $_POST['extensionLength'];
		$extensionArray['maxExtensionLength'] = $_POST['extensionLength'];
		$extensionArray['defaultExtensionLength'] = $_POST['extensionLength'];
		$extensionArray['groupId'] = $modify['groupAddResponse']['groupId'];
		$modify['groupExtensionResponse'] = $dnsObj->modifyGroupExtensionConfigRequest($extensionArray);
		
		if(!empty($_POST['routingProfile'])){
			$sp = $_SESSION['sp'];
			$groupId = $_POST['groupId'];
			$postArray['routingProfile'] = $_POST['routingProfile'];
			
			$modifyGroup = new GroupOperation($sp, $groupId);
			
			$modify['routingResponse'] = $modifyGroup->groupRoutingProfileOperation($postArray);
		}
		
		$groupIdCreated = $modify['groupAddResponse']['groupId'];
		
		$resp = $objServices->updateGroupServiceNewAuth($userArray, $groupIdCreated);
		
		if($resp["Success"] == "Success"){
		    $resp1 = $objServices->updateGroupAssignService($userArray, $groupIdCreated);
		    
        }      
                //Manage Change log to maintain group add request @ 31 May 2018
                $changeLog = array();
                /*change log 1179 */
                if( (isset($_POST['minExtensionLength']) && $_POST['minExtensionLength'] !="") && 
                    (isset($_POST['maxExtensionLength']) && $_POST['maxExtensionLength'] !="") && 
                    (isset($_POST['defaultExtensionLength']) && $_POST['defaultExtensionLength'] !="") 
                )
                {
                    $_POST["extensionLength"] = $_POST['minExtensionLength'] . "," . $_POST['maxExtensionLength'] . "," . $_POST['defaultExtensionLength'];
                } 
                 	
		foreach($_POST as $key=>$val){
                //Code added @ 30 Jan 2019 regarding Add Group was not added due to these keys    
                $ocpKeyArr = array("groupPermissionsGroup", 
                                    "groupPermissionsLocal",
                                    "groupPermissionsTollfree",
                                    "groupPermissionsToll",
                                    "groupPermissionsInternational",
                                    "groupPermissionsOperatorAssisted",
                                    "groupPermissionsOperatorChargDirAssisted",
                                    "groupPermissionsCasual",
                                    "groupPermissionsSpecialService1",
                                    "groupPermissionsSpecialService2",
                                    "groupPermissionsPremiumServices1",
                                    "groupPermissionsPremiumServices2",
                                    "groupPermissionsUrlDialing",
                                    "groupPermissionsUnknown"
                                );
                    //End code
                    //if($key!="tabName" && $key!="operation"){ //Code commented
                  /*  if($key!="tabName" && $key!="operation" && !in_array($key, $ocpKeyArr)){ //Condition added in code added
                        $changeLog[$key] = is_array($val) ? implode(",", $val) : $val;
                    } */
                
                if($key!="tabName" && $key!="operation"  && $key!="defaultExtensionLength" && $key!="minExtensionLength" && $key!="maxExtensionLength" && !in_array($key, $ocpKeyArr)){ //Condition added in code added
                      $changeLog[$key] = is_array($val) ? implode(",", $val) : $val;
                    }
                }
                
                $changeLogObj = new ChangeLogUtility($_POST['groupId'], $_POST['groupId'], $_SESSION["loggedInUserName"]);
                $module = "Add Group";
                $tableName = "groupAddChanges";
                $entity = $_POST["groupName"];
                $changeResponse = $changeLogObj->changeLogAddUtility($module, $entity, $tableName, $changeLog);
                //End Code 
		
	}
	echo json_encode($modify);
}
?>