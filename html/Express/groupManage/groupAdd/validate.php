<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupAdd/groupAdd.php");

if(!empty($_POST['formdata'][0]['value'])){
	$tabName = $_POST['formdata'][0]['value'];
	$_SESSION["tabName"] = $tabName;
}
//print_r($tabName);
//echo "<pre>"; print_r($_POST); //die;
$info = new GroupAdd(0);
	$formDataArray = array(
		"defaultDomain" => "Default Domain",
		"userLimit" => "User Limit",
		"userCount" => "User Count",
		"groupName" => "Group Name",
		"groupId" => "Group ID",
		"timeZone" => "Time Zone",
		"timeZoneDisplayName" => "Time Zone Display Name",
		"locationDialingCode" => "Location Dialing Code",
		"contactName" => "Contact Name",
		"contactEmail" => "Contact Email",
		"contactNumber" => "Contact Number",
		"addressLine1" => "Address Line1",
		"addressLine2" => "Address Line2",
		"city" => "City",
		"stateOrProvince" => "State / Province",
		"zipOrPostalCode" => "Zip / Postal Code",
		"country" => "Country",
		"useGroupName" => "Use group name for Calling Line Identity",
	    "extensionLength" => "Extension Length",
		"minExtensionLength" => "Minimum Extension Length",
		"maxExtensionLength" => "Maximum Extension Length",
		"defaultExtensionLength" => "Default Extension Length",
		"routingProfile" => "Routing Profile"
	);

// 	$_POST['formdata']['minExtensionLength'] = $_POST['formdata']['extensionLength'];
// 	unset($_POST['formdata']['extensionLength']);
	
	foreach($_POST['formdata'] as $key=>$val){
		//if(!empty($val['value'])){
			$postData[$val['name']] = $val['value'];
		//}
	}
	
	$changeString = "";
	//echo "<pre>"; print_r($postData);print_r($formDataArray);
	$show = "";
	foreach($postData as $key=>$val){ 
		$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
		if (array_key_exists($key, $formDataArray)) {
			if($key == "groupId" && $val == ""){
				$backgroundColor = "background:#ac5f5d"; $errorMessage = "Group ID is a required field"; $show = "";
				$changeString .= '<tr '.$show.'><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
			}
			if($key == "timeZone" && $val == ""){
				$backgroundColor = "background:#ac5f5d"; $errorMessage = "Time Zone is a required field"; $show = "";
				$changeString .= '<tr '.$show.'><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
			}
			if($key == "extensionLength" && $val == ""){
			    $backgroundColor = "background:#ac5f5d"; $errorMessage = "Extension Length is a required field"; $show = "";
			    $changeString .= '<tr '.$show.'><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
			}
			if($val <> ""){
				//$show = "style='display:none'";
				$changeString .= '<tr '.$show.'><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';			
			}
		}
	}
	echo $changeString;
?>