<?php 
if(isset($_SESSION["groupId"])){
    $isGroupAccess = $_SESSION["groupId"];
}else{
    $isGroupAccess = "";
}
require_once ("/var/www/html/Express/phoneProfiles/PhoneProfileDBOperations.php");
$objP = new PhoneProfileDBOperations();
?>
<script>
$(function(){
	var getDeviceTypeData = function (){
		var url = "siteReport/getAllCustomProfileData.php";
		var html = "<option value=''>All</option>";
		$.ajax({
			type: "POST",
			url: url,
			data: {allDeviceTypeData : 'all'},
			success: function(result){
				var obj = jQuery.parseJSON(result);
				for (var i = 0; i < obj.length; i++) {
					html += '<option value="'+ obj[i] +'">' + obj[i] + '</option>';
				}
				$("#ppDeviceType").html(html);
			}
		});
	};
	getDeviceTypeData();

	var getCustomProfilesData = function (){
		var url = "siteReport/getAllCustomProfileData.php";
		var html = "<option value='all'>All</option>";
		$.ajax({
			type: "POST",
			url: url,
			data: {allProfileData : 'all'},
			success: function(result){
				var obj = jQuery.parseJSON(result);
				for (var i = 0; i < obj.length; i++) {
					html += '<option value="'+ obj[i] +'">' + obj[i] + '</option>';
				}
				$("#ppProfileType").html(html);
			}
		});
	};
	getCustomProfilesData();

	$("#ppDeviceType").change(function(){
		var url = "siteReport/getAllCustomProfileData.php";
		var html = "<option value='all'>All</option>";
		var deviceType = $("#ppDeviceType").val();
		$.ajax({
			type: "POST",
			url: url,
			data: {selectedDeviceTypeData : deviceType},
			success: function(result){
				var obj = jQuery.parseJSON(result);
				for (var i = 0; i < obj.length; i++) {
					html += '<option value="'+ obj[i] +'">' + obj[i] + '</option>';
				}
				$("#ppProfileType").html("");
				$("#ppProfileType").html(html);
			}
		});
	});

	//$("#ppSelecterInfo").hide();
	$("#downloadCustomProfileCSV").hide();
	$("#searchPhoneProfile").click(function()
		{
			var url = "siteReport/getPhoneProfileData.php";
			var dataCondition = "<?php echo $isGroupAccess; ?>";
			//alert(dataCondition);
			if(dataCondition == ""){
				var dataToSend = $("form#phoneProfileForm").serializeArray();
			}else{
				var dataToSend = $("form#siteManagement").serializeArray();
			}
			
			$("#loading3").show();
			$("#ppTable").html("");
			$("#downloadCustomProfileCSV").hide();
			//$("#ppSelecterInfo").hide();
			$.ajax({
				type: "POST",
				url: url,
				data: dataToSend,
				success: function(result)
				{
					if(foundServerConErrorOnProcess(result, "")) {
    					return false;
                  	}
					$("#loading3").hide();
					$("#ppTable").html(result);
					var errorExist = result.search("Please narrow your search");
					if(errorExist > 0){
						//$("#ppSelecterInfo").hide();
						$("#downloadCustomProfileCSV").hide();
					}else{
						//$("#ppSelecterInfo").show();
						//$('#ppSelecterInfo input').trigger("change");
						$("#downloadCustomProfileCSV").show();
					}
				}
		});
	});

	/*$('#ppSelecterInfo input').on('change', function() {
		if($('input[name=ppSummaryView]:checked', '#ppSelecterInfo').val() == "summaryView"){
			$(".registeredColumn").hide();
			$(".phoneColumn").hide();
			$(".extColumn").hide();
			$(".fNameColumn").hide();
			$(".lNameColumn").hide();
		}else if($('input[name=ppSummaryView]:checked', '#ppSelecterInfo').val() == "detailedView"){
			$(".registeredColumn").show();
			$(".phoneColumn").show();
			$(".extColumn").show();
			$(".fNameColumn").show();
			$(".lNameColumn").show();
		}
	});*/

	//$(".primaryUserModLinkPhoneProfile").click(function(){
	$(document).on("click", ".primaryUserModLinkPhoneProfile", function(){
		var userId = $(this).attr("id");
		var fname = $(this).attr("data-firstname");
		var lname = $(this).attr("data-lastname");

		var extension = $(this).attr("data-extension");
		var phone = $(this).attr("data-phone");
		if($.trim(phone) != ""){
			userIdValue1 = phone+"x"+extension;
		}else{
			userIdValue1 = userId;
		}
		var userIdValue = userIdValue1+" - "+lname+", "+fname;
		
		$.ajax({
				type: "POST",
				url: "userMod/userMod.php",
				//data: { searchVal: userId, userFname : fname, userLname : lname },
				success: function(result)
				{
					$("#mainBody").html(result);
					//$("#deviceSearchBanner").html("User Modify");
					$("#searchVal").val(userIdValue);
					setTimeout(function() {
						$("#go").trigger("click");
					}, 2000);
					$('#helpUrl').attr('data-module', "userMod");
					activeImageSwapFunction();
				}

			});
	});

	$('#downloadCustomProfileCSV').click(function() {
	    var titles = [];
	    var data = [];
	    $('#ppScrollTable th').each(function() {    //table id here
		    if($(this).is(":visible")) {
		    	titles.push($(this).text());
			}
	    });
	    
	    
	    $('#ppScrollTable td').each(function() {    //table id here
	    	if($(this).is(":visible")) {
	    		data.push($(this).text());
			}
	    });
	    
	    
	    var CSVString = prepCSVRow(titles, titles.length, '');
	    CSVString = prepCSVRow(data, titles.length, CSVString);

	    var fileName = "CustomProfileCSV.csv";
	    var blob = new Blob(["\ufeff", CSVString]);
	    if (navigator.msSaveBlob) { // IE 10+
        	navigator.msSaveBlob(blob, fileName);
        }else{
        	var downloadLink = document.createElement("a");
    	    
    	    var url = URL.createObjectURL(blob);
    	    downloadLink.href = url;
    	    downloadLink.download = fileName;
    	    
    	    document.body.appendChild(downloadLink);
    	    downloadLink.click();
    	    document.body.removeChild(downloadLink);
        }
	    
	    
	});

	
});
function prepCSVRow(arr, columnCount, initial) {
	var row = '';
	var delimeter = ',';
	var newLine = '\r\n';

	function splitArray(_arr, _count) {
	var splitted = [];
	var result = [];
	_arr.forEach(function(item, idx) {
	if ((idx + 1) % _count === 0) {
	splitted.push(item);
	result.push(splitted);
	splitted = [];
	} else {
	splitted.push(item);
	}
	});
	return result;
	}
	var plainArr = splitArray(arr, columnCount);

	plainArr.forEach(function(arrItem) {
	arrItem.forEach(function(item, idx) {
	row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
	});
	row += newLine;
	});
	return initial + row;
}

</script>
<script>
var activeImageSwapFunction = function()
{	
	previousActiveMenu	= "siteManagement";
	 currentClickedDiv = "userMod";       
  
		//active
  		if(previousActiveMenu != "")
		{ 
  		 	$(".navMenu").removeClass("activeNav");
			var $thisPrev = $("#siteManagement").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);
	       
		}
		// inactive tab
		if(currentClickedDiv != ""){
			$("#userMod").addClass("activeNav");
			var $thisPrev = $("#userMod").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);
			 previousActiveMenu = currentClickedDiv;
			
    	}
   
}
</script>
<link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v15">
<!--<script src="/Express/js/bootstrap.min.js"></script>-->
<script src="/Express/js/bootstrap/bootstrap.min.js"></script>
<script src="/Express/js/jquery-1.17.0.validate.js"></script>
<script src="/Express/js/jquery.loadTemplate.min.js"></script>
<style>
.glyphicon {
    position: relative;
    top: 1px;
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: normal;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
</style>
<div class="row" style="width:830px;margin:0 auto;">
	
    			<form name ="phoneProfileForm" id ="phoneProfileForm" class="">
    			<?php 
    			if($isGroupAccess == "" && $_SESSION["superUser"] == "1"){?>
        			<div class="form-group">
        				<label class="labelText" for="phoneProfileSearchlevel">Search In:</label>
        				<input type="radio" class="phoneProfileSearchlevel" name="phoneProfileSearchlevel" id="phoneProfileSearchlevelSys" value="system" style="width: 5%;">
        				<label for="phoneProfileSearchlevelSys" style="padding-left:35px;"><span></span></label>
        				<label class="labelText" for="deviceClassVoIP">System</label> 
        				<input type="radio" class="phoneProfileSearchlevel" name="phoneProfileSearchlevel" id="phoneProfileSearchlevelEnt" value="enterprise" checked="" style="width: 5%;">
        				<label for="phoneProfileSearchlevelEnt" style="padding-left:25px;"><span></span></label> 
        				<label class="labelText" for="deviceClassAnalogGateway">Enterprise</label>
        			</div>
    			   
    			<?php } ?>
    				<div class="form-group">
        				<label class="labelText" for="ppDeviceType">Device Type:</label>
        				
    					<div class="dropdown-wrap">
    						<select name="ppDeviceType" id="ppDeviceType" class="" style="width:100% !important;">
        						
    						</select>
    					</div>
    				</div>
    				
    				<div class="form-group">
        				<label class="labelText" for="limitSearchTo">Phone Profile:</label>
        				
    					<!-- <div class="col span_12 dropdown-wrap"> -->
                                        <div class="dropdown-wrap">
    						<select name="ppProfileType" id="ppProfileType1" class="" style="width:100% !important;">
    						<option value="all">All</option>
    						<?php 
    						$respPhone = $objP->getAllPhoneProfiles();
    						if(count($respPhone) > 0){
    						    foreach ($respPhone as $key => $value){
    						        echo "<option value = ".$value[1].">".$value[0]."</option>";
    						    }
    						    
    						}
    						?>
    						<!-- <option value="DN">DN</option>
    						<option value="EXT">EXT</option>
    						<option value="AA">AA</option>
    						<option value="Phone">Phone</option> -->
        						
    						</select>
    					</div>
    				</div>
    				
					<div class="form-group">
					<label class="labelText" for="tagBunddle">Tag Bundle:</label>
        				
        				<!-- <div class="col span_12 dropdown-wrap">  -->
                                        <div class="dropdown-wrap">
    					<select name="tagBunddle" id="tagBunddle" onchange="" style="width:100% !important;">
								<option value="">All</option>
        						<?php 
        						$respBundle = $objP->getAllTagBundles();
        						if(count($respBundle) > 0){
        						    foreach ($respBundle as $key => $value){
        						        echo "<option value = ".$value.">".$value."</option>";
        						    }
        						    
        						}
        						?>
                				<!-- <option value="nhForward">nhForward</option>
                				<option value="Forward">Forward</option>
                				<option value="N-Way">N-Way</option>
                				<option value="Pickup">Pickup</option>
                				<option value="TCP">TCP</option>
                				<option value="ACD">ACD</option> -->
                			</select>
    					</div>
        				
        			</div>
    				
    				<div class="centerDesc" style="min-height:120px;">
						<p class="register-submit alignCenter">
							<input type="button" name="searchPhoneProfile" id="searchPhoneProfile" value="Search">
						</p>
					</div>
    				
    			</form>
    			<div class="loading" id="loading3" style="margin-top:2%;">
						<img src="/Express/images/ajax-loader.gif">
					</div>
			
			</div>
			
			<div class="fcorn-register">
				<!-- <div id="ppSelecterInfo">
        			<div class="col span_24 selectInputRadio averistar-bs3">
        				<input type="radio" checked class="ppSummaryView" name="ppSummaryView" id="ppSummaryView" value="summaryView" style="width: 25px;">
        				<label class="labelpadding" for="deviceClassVoIP">Summary View</label>
        				<input type="radio" class="ppDetailedView" name="ppSummaryView" id="ppDetailedView" value="detailedView" style="width: 25px;">
        				<label class="labelpadding" for="deviceClassAnalogGateway">Detailed View</label>
        				<button id="infoIconES" href="#generate_notes" class="btn btn-link no-focus" title="Instructions" data-toggle="collapse"><i
												class="glyphicon glyphicon-info-sign"></i></button>
						<div id="generate_notes" class="collapse">
						<div class="alert alert-info" role="alert">Detailed View may take significantly longer time to complete - consider Summary View option.</div>
					</div>
        			</div>
        			
					
        		</div> -->
				<div style="float:right;margin-bottom:15px;" class="downloadCSVCustomProfile" id="downloadCustomProfileCSV">
					<!-- <input type="button" id="downloadCustomProfileCSV" value="Download as CSV" style="width:165px;"> -->
					<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" style="cursor:pointer;">
                                					<br><span>Download<br>CSV</span>
				</div>
				<div id="ppTable">
    					
				</div>
				
				
			</div>