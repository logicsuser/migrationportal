<?php 
class PhoneProfileDBOperations{
    
   function getAllPhoneProfiles(){
        global $db;
        
        $phoneProfileArray = array();
        $stmt = $db->prepare("SELECT * FROM phoneProfilesLookup");
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i =0;
        foreach($row as $key => $val) {
            $phoneProfileArray[$i][] = $val->phoneProfile;
            $phoneProfileArray[$i][] = $val->value;
            $i++;
        }
        
        return $phoneProfileArray;
    }
    
    
    function getAllTagBundles() {
        global $db;
        $tagBundleArray = array();
        
        $stmt = $db->prepare("SELECT `tagBundle` FROM deviceMgmtTagBundles");
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i =0;
        foreach($row as $key => $val) {
            if(!in_array($val->tagBundle, $tagBundleArray)){
                $tagBundleArray[$i] = $val->tagBundle;
                
                $i++;
            }
            
        }
        return $tagBundleArray;
    }   
}
?>