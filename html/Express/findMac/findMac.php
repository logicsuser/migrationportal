<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();
?>
<script>
	$(function()
	{
		$("#module").html("> Find MAC Address");
		$("#endUserId").html("");

		function search(event)
		{
			event.preventDefault();
			$("#macResult").html("");
			$("#macResult").css("border", "0px");
			$("#macResult").css("overflow-y", "auto");
			$("#loading2").show();
			var formData = $("form#searchOption").serializeArray();
			var mac = $("#searchVal").val();
			if(mac == ""){
				$("#loading2").hide();
				return false;
			}
			$.ajax({
				type: "POST",
				data: formData,
				url: "findMac/getDataByMac.php",
				success: function(result)
				{
					$("#loading2").hide();
					$("#macResult").html(result);
					$("#macResult").css("border", "1px solid");
					$("#macResult").css("overflow-y", "scroll");
				}
			});
		}

		$("#go").click(function(event)
		{
			search(event);
		});

		$("input").keypress(function(event)
		{
			if (event.which == 13)
			{
				search(event);
			}
		});
	});
</script>

<style>
.selectInputRadio input {
    width: auto;
}
.selectInputRadio{margin-right: 15px;}

#macResult {
    width: 90%;
}
</style>


<div class="subBanner">Find MAC Address</div>
<div style="font-size:12px; width: 100%; text-align: center">&nbsp;</div>
<div style="font-size:12px; width: 100%; text-align: center">To search all MAC addresses, type space and click 'Search'</div>
<div class="vertSpacer">&nbsp;</div>

<div class="formPadding">
	<form name="searchOption" id="searchOption" method="POST" class="fcorn-register container">
	
		<div style=""> 
			<div class="col span_7">
        		<span style="margin-right: 15px"> <b> Search By </b> </span>
			</div>
			<div class="col span_12">
        		<span  class="selectInputRadio">
        			<label for = "macByGroup">Group</label>
            		<input type="radio" name="searchmac" id="macByGroup" value="macByGroup" checked/>
        		</span>
        		<span class="selectInputRadio">
            		<label for = "macByEnterprise">Enterprise</label>
            		<input type="radio" name="searchmac" id="macByEnterprise" value="macByEnterprise" />
        		</span>
        		<span class="selectInputRadio">
            		<label for = "macBySystem">System</label>
            		<input type="radio" name="searchmac" id="macBySystem" value="macBySystem" />
        		</span>
    		</div>
        </div>
         <div class="vertSpacer">&nbsp;</div>
		<div class="col span_7"><label for="searchVal"><b>Search By MAC Address</b></label></div>
		<div class="col span_12"><input type="text" name="searchVal" id="searchVal" size="32" value="<?php echo $_GET["userId"]; ?>"></div>
		<div class="centerDesc">
			<p class="register-submit">
				<input type="button" name="go" id="go" value="Search">
			</p>
		</div>
	</form>
	<div class="vertSpacer">&nbsp;</div>
    <div class="vertSpacer">&nbsp;</div>
    <div class="vertSpacer">&nbsp;</div>

	<div class="loading" id="loading2"><img src="/Express/images/ajax-loader.gif"></div>
	<div id="macResult"></div>
</div>
