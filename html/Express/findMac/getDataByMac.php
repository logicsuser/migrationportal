<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	require_once("/var/www/lib/broadsoft/adminPortal/getDataByMac.php");
	require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
?>

	<script>
	    $(function()
	    {
	        $("#macSearch").tablesorter();
	    });
	 </script>

<?php 


$returnData = array();
$searchType = $_POST['searchmac'];

$getDataByMac = new getDataByMac();

if($searchType == "macByGroup") 
{
    $allDevices = $getDataByMac->getDeviceListOfGroup($_SESSION["sp"], $_SESSION["groupId"], $_POST['searchVal']);
}
else if($searchType == "macByEnterprise") 
{
    $allDevices = $getDataByMac->getDeviceListOfEnterprise($_SESSION["sp"], $_POST['searchVal']);
}
else if($searchType == "macBySystem")
{
    $allDevices = $getDataByMac->getDeviceListOfSystem($_POST['searchVal']);
}


$count = 0;
if(count($allDevices['Success']) > 0)
{
    foreach($allDevices['Success'] as $deviceKey=> $deviceValueArr)
    {
        if(count($deviceValueArr) > 0)
        {
            if(($deviceValueArr['spId'] != ""  &&  $deviceValueArr['grpId'] != "") && $deviceValueArr['mac'] != "")
            {
                $returnData[$count]['deviceInfo'] = $deviceValueArr;
                $returnData[$count]['userInfo'] = getUserForDevice($deviceValueArr, $getDataByMac);
                $count++;
            }
            
        }
    }
}



//Create Table function
createTable($returnData, $searchType);



function getUserForDevice($deviceValueArr, $getDataByMac)
{
    $returnArray = Array();    
    
    $allUsersArray = $getDataByMac->getAllUserOfDevice($serviceProviderId = $deviceValueArr['spId'], $groupId = $deviceValueArr['grpId'], $deviceName = $deviceValueArr['deviceName']);
    
    if(count($allUsersArray['Success']) > 0)
    {
        $returnArray = $allUsersArray['Success'];
    }
    return $returnArray;
}


		
	
function createTable($tableDataArray, $searchType)
{
   
	?>
	    
	    <div style="zoom:1;">
	    <table id="macSearch" class="tablesorter" style="width:100%;margin:0;">
	    <thead>
	    <tr>
	    <?php
	    if($searchType == "macByEnterprise" || $searchType == "macBySystem"){
	        echo'<th class=\"entClass\" style="width:12%;">Enterprise</th>';
	        echo'<th style="width:12%;">Group</th>';
	    }
	    ?>
	    <th style="width:10%;">Phone Number</th>
	    <th style="width:10%;">Extension</th>
	    <th style="width:13%;">First Name</th>
	    <th style="width:13%;">Last Name</th>
	    <th style="width:15%;">Device Type</th>
	    <th style="width:15%;">MAC Address</th>
	    </tr>
	    </thead>
	    <tbody>
	    
	    
	    <?php 
	    foreach($tableDataArray as $dataKey => $dataValue)
	    {
	        $devType = strval($dataValue['deviceInfo']['deviceType']);
	        $mac = strval($dataValue['deviceInfo']['mac']);
	        
	        if($searchType == "macByEnterprise" || $searchType == "macBySystem"){
	            $providerId = strval($dataValue['deviceInfo']['spId']);
	            $groupId = strval($dataValue['deviceInfo']['grpId']);
	        }
	        foreach($dataValue['userInfo'] as $key => $value)
	        {
	            $phoneNumber = strval($value['phoneNumber']);
	            $firstName = strval($value['firstName']);
	            $lastName = strval($value['lastName']);
	            $extension = strval($value['extn']);
	            
	            echo "<tr>";
	            if($searchType == "macByEnterprise" || $searchType == "macBySystem"){
	                echo "<td class=\"macTable\" style=\"width:12%;\">" . $providerId . "</td>";
	                echo "<td class=\"macTable\" style=\"width:12%;\">" . $groupId . "</td>";
	            }
	            echo "<td class=\"macTable\" style=\"width:10%;\">" . $phoneNumber . "</td>";
	            echo "<td class=\"macTable\" style=\"width:10%;\">" . $extension . "</td>";
	            echo "<td class=\"macTable\" style=\"width:13%;\">" . $firstName . "</td>";
	            echo "<td class=\"macTable\" style=\"width:13%;\">" . $lastName . "</td>";
	            echo "<td class=\"macTable\" style=\"width:15%;\">" . $devType . "</td>";
	            echo "<td class=\"macTable\" style=\"width:15%;\">" . $mac . "</td>";
	            echo "</tr>";
	        
	        }
	        
	    }
	    
	    ?>
	    
	    </tbody>
	</table>
</div>
	    
	<?php 
	
}
	
	?>  
	