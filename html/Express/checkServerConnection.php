<?php 
require_once("/var/www/html/Express/config.php");
// require_once("/var/www/lib/broadsoft/login.php");
// checkLogin();
require_once("/var/www/lib/broadsoft/ServerConnectionUtil.php");
$serConnUtil = new ServerConnectionUtil();

$GLOBALS['serversDetail'] = $serConnUtil->getServerDetails();

if( isset($_POST['action']) && $_POST['action'] == "checkDefaultServerConnection") {
		$response = "";
		$serverConnectionStatus = checkServerIsActive($GLOBALS['serversDetail']);
			if( $serverConnectionStatus->defaultServerConnected ) {
				$response = "Default Server is Connected";
            } else {
                $response = "Not Connected to default server";				
            }
			
	echo $response;
	die;
}

if( isset($_POST['action']) && $_POST['action'] == "connect_to_default_server") {
	$_SESSION["do_not_load_fail_over_action"] = "true";
	$_SESSION['server_connection_detail']['isDefaultSerConnected'] = "true";
	$_SESSION['server_connection_detail']['current_server_connection'] = "default";
	
	echo "Default Server is Connected successfully";
	die;
}

if( isset($_POST['action']) && $_POST['action'] == "connect_to_alternate_server") {
	$_SESSION["do_not_load_fail_over_action"] = "true";
	$pendingProcess = $_POST['pendingProcess'];
	$_SESSION['server_connection_detail']['isAlternateSerConnected'] = "true";
	$_SESSION['server_connection_detail']['current_server_connection'] = "alternate";
	
	$server_detail = "";
	if(!filter_var($GLOBALS['serversDetail']->defaultAppServer->ip, FILTER_VALIDATE_IP)) {
	    $server_detail = $GLOBALS['serversDetail']->defaultAppServer->ip;
	} else {
	    $server_detail = $GLOBALS['serversDetail']->defaultAppServer->serverType;
	}
	$changeLogData = array();
	$changeLogData[] = array('connection_status' => "Disconnected", "status_message" => "Express cannot connect to default Feature Server", "server_detail" => $server_detail);
    //$changeLogData[] = array('connection_status' => "Connected", "status_message" => "Express switched to alternate server", "server_detail" => $GLOBALS['serversDetail']->alternateAppServer->ip);
	
	foreach($pendingProcess as $pKey => $process) {
		serverFailOverLog($changeLogData, $process);
	}
	
	echo "Alternate Server is Connected successfully";
	die;
}


if( isset($_POST['action']) && $_POST['action'] == "all_connection_failed") {
    $pendingProcess = $_POST['pendingProcess'];
    $server_detail = "";
    if(!filter_var($GLOBALS['serversDetail']->alternateAppServer->ip, FILTER_VALIDATE_IP)) {
        $server_detail = $GLOBALS['serversDetail']->alternateAppServer->ip;
    } else {
        $server_detail = $GLOBALS['serversDetail']->alternateAppServer->serverType;
    }
    
    $changeLogData = array();
    $changeLogData[] = array('connection_status' => "Disconnected", "status_message" => "Express cannot connect to alternate server", "server_detail" => $server_detail);
	//serverFailOverLog($changeLogData, "");
	
    foreach($pendingProcess as $pKey => $process) {
        serverFailOverLog($changeLogData, $process);
    }
	echo "Default Server is Connected successfully";
	die;
}


function serverFailOverLog($changeLogData, $process) {
    require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
    $module         = $process ." Failed due to Server Disconnected.";
    $tableName      = "serversFailOverChanges";
    $entityName     = "BW app server connection";
	$groupId = isset($_SESSION["groupId"]) ? $_SESSION["groupId"] : "None";
    $cLUObj         = new ChangeLogUtility($_SESSION["sp"], $groupId, $_SESSION["loggedInUserName"]);
    $changeResponse = $cLUObj->changeLogFailOverServerAddUtility($module, $entityName, $tableName, $changeLogData);
}
?>