<?php
require_once("/var/www/html/Express/config.php");

checkLogin();

//Check if the user need to reset his password
if(isset($_SESSION['RESET_PASSWORD_MESSAGE'])) {

	header("Location: ./reset_password.php");
	exit;
}

$spNameInSession = $_SESSION["sp"];
//---------------------------------------------------------------------------
function showServiceProvider()
{
    //if (isset($_SESSION["spList"]) && count($_SESSION["spList"]) > 1) {
    if (isset($_SESSION["spListNameId"]) && count($_SESSION["spListNameId"]) > 0) {
		return true;
	}

	return isset($_SESSION["superUser"]) and $_SESSION["superUser"] == "1";
        //return isset($_SESSION["superUser"]) && ($_SESSION["superUser"] == "1" || $_SESSION["superUser"] == "3");
}


//---------------------------------------------------------------------------
function getServiceProvidersList()
{
     
	global $sps;
	global $getSPName ;
	//	if (isset($_SESSION["spList"]) && count($_SESSION["spList"] > 1)) {
	if (isset($_SESSION['spListNameId']) && count($_SESSION['spListNameId'] > 1)) {
	    $serviceProviders = $_SESSION['spListNameId'];
	} else {
	    $serviceProviders = $getSPName;
	}
	$str = "";
	if(count($serviceProviders) > 0){
		$str = "<option value=\"\"></option>";
		foreach ($serviceProviders as $v) {
		  $providerId = explode('<span>-</span>',$v); //ex-774
		  $spIdwithName = str_replace('<span>-</span>', ' - ', $v);
		  $currentspId = $providerId[0];
		  $providerName = isset($providerId[1]) ? $providerId[1] : "";
		  $str .= "<option value=\"".$currentspId."\" data-spName=\"".$providerName."\">" . $spIdwithName . "</option>";
		}
	}
	return $str;
}


//---------------------------------------------------------------------------
function getAdminGroups()
{
	global $groupsList;
	return $groupsList;
}


//---------------------------------------------------------------------------
function groupExceedSearchThreshold()
{
	global $numberOfGroups;
	//USPS change
	return $numberOfGroups > 0;
}

function checkSecurityDomainNonSuperUser($userId){
	global $db;
	
	$permissionsQuery = "SELECT securityDomain from permissions where userId = ?";
	$permissionsQueryStmt = $db->prepare($permissionsQuery);
	$permissionsQueryStmt->execute(array($userId));
	$permissionsRow = $permissionsQueryStmt->fetch(PDO::FETCH_ASSOC);
	return $permissionsRow["securityDomain"];
	
}
//error_log(print_r($_SESSION, true));

$passExpTime = "";
if($_SESSION["superUser"] == "1") {
    $passExpTime = $passExpiration_Super;
} else {
    $passExpTime = $passExpiration_RegUser;
}
$checkSecurityDomainValue = checkSecurityDomainNonSuperUser($_SESSION["adminId"]);
$checkSecurityDomainSPValue = getServiceProvidersList();

require_once("/var/www/lib/broadsoft/login.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ServiceProviderOperations.php");
/* Check SP and Group is Valid */
$spOp = new ServiceProviderOperations();
if( isset($_SESSION["continue_session"]) && ! $spOp->isPreviousSessionValid($_SESSION) && $_SESSION["superUser"] !== "2" ) {
    unset($_SESSION["continue_session"]);
}


//Code added @13 June 2019
if($_SESSION["superUser"] == "1"){
    if(!isset($_SESSION["continue_session"])){
        $bwClusters = $license["bwClusters"];
        $clusterSupport  =  (isset($bwClusters) && $bwClusters == "true") && $enableClusters == "true" ? true : false;
        if($clusterSupport){
               $_SESSION['selectedCluster']  = $clusterName;
        }
    }
}
//End code
//die();

//Continue Previous Session
if (isset($_SESSION["continue_session"]) && $_SESSION["continue_session"] && !isset($_GET["change_group"])) {
    
    if (isset($_SESSION["dayDifference"]) and $_SESSION["dayDifference"] >= $passExpTime and $_SESSION["superUser"] !== "2") {

	    //Add Admin Log Entry - Start
	    require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
	    $adminLogs = new adminLogs();
	    $log = array(
		    'adminUserID' => $_SESSION["adminId"],
		    'eventType' => 'PASSWORD_EXPIRATION',
		    'adminUserName' => $_SESSION["loggedInUserName"],
		    'adminName' => $_SESSION["loggedInUser"],
		    'updatedBy' => $_SESSION["adminId"],
		    'details' => array(
			    'Password Age' => $_SESSION["dayDifference"] . ' days',
			    'Due for' => ($_SESSION["dayDifference"] - $passExpTime) . ' days'
		    )
	    );
	    $adminLogs->addLogEntry($log);
	    //Add Admin Log Entry - End

        header("Location: passwordReset/passwordResetMod.php");
        exit;
    }
    unset($_SESSION["continue_session"]);

    $no_header = true;
    require_once("header.php");
    
	$redirect_url = null;
	$userTypeArr = array("1", "3");
	if (isset($_SESSION["sp"]) && isset($_SESSION["groupId"])) {
	    if($_SESSION['adminReportMessage']) {
	        //if($_SESSION["superUser"] <> "1"){                
                if(!in_array($_SESSION["superUser"], $userTypeArr)){
	            $redirect_url = "";
	        }else{
	            $redirect_url = "main.php";
	        }
	    } else if((empty($checkSecurityDomainValue) || empty($_SESSION["securityDomain"]) || empty($checkSecurityDomainSPValue)) && (isset($_SESSION['showAdminReportMsg']) && $_SESSION['showAdminReportMsg'])){
			//if($_SESSION["superUser"] <> "1"){
                        if(!in_array($_SESSION["superUser"], $userTypeArr)){
				$redirect_url = "";
			}else{
				$redirect_url = "main.php";
			}
		}else{
			$redirect_url = "main.php";
		}
	} 
        else if(($_SESSION["superUser"] == "1" || $_SESSION["superUser"] == "3") && isset($_SESSION["sp"])) 
        {
		$redirect_url = "main_enterprise.php";
	}
        //unset($_SESSION["continue_session"]);

	if($redirect_url == "main.php") {
	    unset($_SESSION["continue_session"]);
        ?>
		<div id="mainBody" style="width: 100%;clear:both;">

			<div id="loading" class="loading" style="display: block;"><img src="/Express/images/ajax-loader.gif"></div>
		</div>
		<script>
            $.ajax({
                type: "POST",
                url: "setGroup.php",
                data: {groupName: "<?php echo $_SESSION["groupId"] ?>", Selsp: "<?php echo $_SESSION["sp"] ?>"},
                success: function (result) {
                    window.location.replace("<?php echo $redirect_url ?>");
                }
            });
		</script>
		<?php
		exit;
	}else if($redirect_url == "main_enterprise.php") {
	    unset($_SESSION["continue_session"]);
	   ?>
		<div id="mainBody" style="width: 100%;">
			<div id="loading" class="loading" style="display: block;"><img src="/Express/images/ajax-loader.gif"></div>
		</div>
		<script>
            $.ajax({
                type: "POST",
                url: "setGroup.php",
                data: {Selsp: "<?php echo $_SESSION["sp"] ?>"},
                success: function (result) {
                    window.location.replace("<?php echo $redirect_url ?>");
                }
            });
		</script>
		<?php
		exit;
	}else {
	    ?>
	    <script>window.location.replace("sysAdmin/sysAdminMod.php");</script> 
	    <?php
	}
}
unset($_SESSION["continue_session"]);
require_once("/var/www/html/Express/adminCheckFilter.php");
redirectUser("choosegroup");
$_SESSION["redirectUrl"] = "choosegroup";
//redirect user to password reset page if password last changed 90 days ago

if (isset($_SESSION["dayDifference"]) and $_SESSION["dayDifference"] >= $passExpTime and $_SESSION["superUser"] !== "2") {

	//Add Admin Log Entry - Start
	require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
	$adminLogs = new adminLogs();
	$log = array(
		'adminUserID' => $_SESSION["adminId"],
		'eventType' => 'PASSWORD_EXPIRATION',
		'adminUserName' => $_SESSION["loggedInUserName"],
		'adminName' => $_SESSION["loggedInUser"],
		'updatedBy' => $_SESSION["adminId"],
		'details' => array(
			'Password Age' => $_SESSION["dayDifference"] . ' days',
			'Due for' => ($_SESSION["dayDifference"] - $passExpTime) . ' days'
		)
	);
	$adminLogs->addLogEntry($log);
	//Add Admin Log Entry - End

	header("Location: passwordReset/passwordResetMod.php");
	exit;
}

//redirect system administrator to sysadmin page
if (isset($_SESSION["superUser"]) and $_SESSION["superUser"] == "2") {
    $_SESSION["redirectUrl"] = "sysAdmin";
	header("Location: sysAdmin/sysAdminMod.php");
	exit;
}

//require_once("/var/www/lib/broadsoft/login.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");
//unset($_SESSION["groupId"]);
if(isset($_SESSION["superUser"]) and $_SESSION["superUser"] == "1"){
	unset($_SESSION["sp"]);
}
unset($_SESSION["groupInfo"]);
//$_SESSION["sp"] = "None";
set_group_selected_user();

require_once("/var/www/lib/broadsoft/adminPortal/getServiceProviders.php");
$no_header = true;
require_once("header.php");

$numberOfGroups = 0;
$groupsList = "";

// if ($_SESSION["superUser"] == "0" && isset($_SESSION["spListNameId"])) {
// 	$numberOfGroups = count($_SESSION["groups"]);
// 	$groupsList .= "<option value=\"\"></option>";
// 	for ($a = 0; $a < count($_SESSION["groups"]); $a++) {
// 		$groupsList .= "<option value=\"" . $_SESSION["groups"][$a] . "\">" . $_SESSION["groups"][$a] . "</option>";
// 	}
// }

?>
<script>

    //*Start* Disable Browser's Back Button.
    var href = location.href;
    var urlLastSegment = href.match(/([^\/]*)\/*$/)[1];
    
    history.pushState(null, null, urlLastSegment);
    window.addEventListener('popstate', function () {
        history.pushState(null, null, urlLastSegment);
    });
    // *End*

    var selectedSPName = "";
    var superUser = "<?php echo $_SESSION["superUser"]; ?>";
    var selectedSP = "<?php echo(isset($_SESSION["sp"]) ? $_SESSION["sp"] : ""); ?>";
    var securityDomainPattern = "<?php echo $securityDomainPattern; ?>";
    var groupSearchString = "";
    
    /*
    alert('superUser - '+superUser);
    alert('selectedSP - '+selectedSP);
    alert('securityDomainPattern - '+securityDomainPattern);
    */
    $("#securityDomainCheckNonSuperUserDiv").hide();
    $("#securityDomainGroupCheckNonSuperUserDiv").hide();
    $(function () {
        
        $("#helpUrl").hide();
        if (selectedSP != "") {
            getSearchData();
        }
        $("#logoImage").removeAttr("href");
		
		$("#enterprise_main_page_href").click(function() {
			setPreviousChangeGroupDetails("enterprise", data = {spId: selectedSP, spName: selectedSPName});
		});
		
        $("#search").click(function () { 
            
            var searchData = document.getElementById("searchGroupsVal").value; 
            if (searchData != "") {
            	var groupArr = searchData.split('<span class="search_separator">-</span>');
                var groupName = decodString($.trim(groupArr[0]));
                var selGroupName = typeof groupArr[1] != "undefined" ? decodString($.trim(groupArr[1])) : "";
                //alert("SP:" + selectedSP + " Grp:" + groupName);
                $('#searchGroups').val(groupSearchString);
                $.ajax({
                    type: "POST",
                    url: "setGroup.php",
                    data: {groupName: groupName, Selsp: selectedSP},
                    success: function (result) { 
                        console.log(result);
                        result = result.trim();
                        if (result.slice(0, 1) == "1") {
                            alert("Error:  Group not found: " + groupName);
                        } else {
                        	setPreviousChangeGroupDetails("main", data = {spId: selectedSP, spName: selectedSPName, groupId: groupName, selGroupName: selGroupName});
                            $("#logoImage").attr("href", "main.php");
                            window.location.replace("main.php");
                        }
                    }
                });
            }
        });
		
		$('#searchGroups').on('keyup keypress keydown', function(e) {

/*Old code*/
			var resultSearch = $("#searchGroups").val();
			if(resultSearch =='')
			{
				$("#search").prop('disabled',true);
			}else{
				$("#search").prop('disabled',false);
			}
	/*Old code*/		
		
			  var keyCode = e.keyCode || e.which;
              //Code added @28 March 2018 to fix the issue EX-458
              if (keyCode === 38)
              {
            	  var tmpStrSearch = $("#searchGroups").val();
                  var tmpStr = $.trim(tmpStrSearch);
                  $('#searchGroups').val(decodString(tmpStr.replace('<span class="search_separator">-</span>', '-')));
              }
              
              if (keyCode === 40)
              { 
                  var tmpStrSearch = $("#searchGroups").val();
                  var tmpStr = $.trim(tmpStrSearch);
                  $('#searchGroups').val(decodString(tmpStr.replace('<span class="search_separator">-</span>', '-')));
              }
              if (keyCode === 9)
              { 
                  if($(".ui-state-focus").length){
                  	var tmpStrs = $(".ui-state-focus").html();
                  	var tmpStr =  $.trim(tmpStrs);
                  }else{
					var tmpStr = $.trim(groupSearchString);
                  }
                  groupSearchString = $.trim(tmpStr);
                  $(document).find(".ui-state-focus").addClass("tabGroup");
                  $('#searchGroupsVal').val(tmpStr);
                  $('#searchGroups').val(decodString(tmpStr.replace('<span class="search_separator">-</span>', '-')));
                  $('#searchGroups').trigger("blur");
              }
              //End code

			  if (keyCode === 13) {
				e.preventDefault();
                //var searchVal = $("#searchGroups").val(); //Commented on 28 March 2018 due to fix the issue EX-458
                //Code added @28 March 2018 to fix the issue EX-458                                
                var searchTmpNewStr = $("#searchGroups").val();
                var tmpNewStr = $.trim(searchTmpNewStr);
                if($(".ui-state-focus").length) {
                  	var tmpNewStrs = $(".ui-state-focus").html();
                  	var tmpNewStr = $.trim(tmpNewStrs);
                  }else{
					var tmpNewStr = $.trim(groupSearchString);
					
                  }
                groupSearchString = tmpNewStr;
                $('#searchGroupsVal').val(tmpNewStr);
				var searchVal = tmpNewStr.replace('<span class="search_separator">-</span>', '-'); 
				$('#searchGroups').trigger('blur');
                                //End Code
				if(searchVal != "") {            
					var searchGroupName = $.trim(searchVal);                          
					$('#searchGroups').val(decodString(searchGroupName));
					if(tmpNewStr.search('span') != -1) {
						var searchtmpNewStr = $.trim(tmpNewStr); 
						$('#searchGroupsVal').val(searchtmpNewStr);
					}
					$('#search').trigger('click');
				}
			}
		});
		
		$("#searchGroups").blur(function(e){
        	var el = $(this).val(); 
        	if(el != "") {
        		var seachIdValue = el;
        		groupSearchString = $.trim(seachIdValue);
            }
        	if($(".ui-state-focus").length){
              	var searchValRes = $(".ui-state-focus").html();
              	var searchVal = $.trim(searchValRes);
              }else{
				var searchVal = $.trim(groupSearchString);
              }
            
			if(searchVal.search('span') != -1) {
				$('#searchGroupsVal').val(searchVal);
			}
			var groudVal = groupSearchString.replace('<span class="search_separator">-</span>', '-');
			if(groudVal != "") {
				var groudVals = $.trim(groudVal);                          
				$('#searchGroups').val(decodString(groudVals));
				$('#search').trigger('click');
				 
			} else {
				var groudValgroupSearchString = $.trim(groupSearchString);                          
				 $('#searchGroups').val(decodString(groudValgroupSearchString));
			}    
					
        });

        $("#groupName").change(function () {
            var groupName = decodString($("#groupName").val());
            var Selsp = $("#Selsp").val();
            //alert("SP:" + Selsp + " Grp:" + groupName);
            $.ajax({
                type: "POST",
                url: "setGroup.php",
                data: {groupName: groupName, Selsp: Selsp},
                success: function (result) 
		{
                    window.location.replace("main.php");
                }
            });
        });

        
        
        //$("#searchBlock").hide();
        $("#Selsp").change(function () {
            $("#searchGroups").val('');
            //$("#enterprise_main_page_link").hide();
            selectedSP = $(this).val();
            selectedSPName = $('option:selected', this).attr('data-spName');
            if (selectedSP != "") {
                getSearchData();
            }

            //if (superUser == "1" || securityDomainPattern != "") {
            if (selectedSP != "") {
                $("#groupName").empty();
                $.ajax({
                    type: "POST",
                    url: "adminUsers/getAllGroups.php",
                    data: {getSP: selectedSP, chooseGroup: "true"},
                    success: function (result) {
                    	$("#securityDomainCheckNonSuperUserDiv").hide();
							$("#groupName").append("<option value=\"\"></option>");
	                        $("#groupName").append(result);
	                        var numGroups = occurrences(result, "</option>", false);	                        
	                        if (numGroups > 0) {
	                            $("#searchBlock").show();
	                        }else {
	                            $("#chooseGroup").show();
	                            $("#searchBlock").hide();
	                            //$("#enterprise_main_page_link").show();
	 							//$("#securityDomainGroupCheckNonSuperUserDiv").show();
	                        }
                    }
                });
            } else if(selectedSP == "") {
            	$("#chooseGroup").show();
                $("#searchBlock").hide();
                //$("#securityDomainGroupCheckNonSuperUserDiv").show();
             }

            //If SuperUser, set the Enterprise and show the "Enterprise Main Page" link
//             $("#enterprise_main_page_link").hide();
            //if (selectedSP != "" && superUser == "1") { //code commented @ 20 Feb 2019
            if (selectedSP != "" && (superUser == "1" || superUser == "3")) { //Code added @ 20 Feb 2019
                var Selsp = $("#Selsp").val();
                $.ajax({
                    type: "POST",
                    url: "setGroup.php",
                    data: {Selsp: Selsp},
                    success: function (result) {
                        $("#enterprise_main_page_link").show();
                    }
                });
            }
            

        });
        var selectedSPInSession = "<?php echo $spNameInSession; ?>";
        if(selectedSPInSession != "None"){
        	$("#Selsp").val(selectedSPInSession);
            $("#Selsp").trigger("change");
        };

        $(document).on("click", ".groupAutoClass > li > a", function(){
    		var el = $(this);
    		var groupSearchStrings = el[0].innerHTML; 
    		$('#searchGroups').val(decodString(el[0].innerHTML.replace('<span class="search_separator">-</span>', '-')));
    		groupSearchString = $.trim(groupSearchStrings);
    		$('#searchGroupsVal').val(groupSearchString);
    		$('#searchGroups').trigger('blur');
	 	});
        
    });

    function getSearchData() {
    	$("#search").prop('disabled',true);
        var autoComplete = new Array();
        //alert("SP:" + selectedSP + " Selected Groups:" + selectedGroups);
        $.ajax({
            type: "POST",
            url: "getGroupsSearchData.php",
            data: {serviceProvider: selectedSP},
            success: function (result) {
                var explode = result.split(":");
                for (var a = 0; a < explode.length; a++) {
                    autoComplete[a] = explode[a];
                }
                $("#searchGroups").autocomplete({
                    source: autoComplete
                //});
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
	                ul.addClass('groupAutoClass'); //Ul custom class here
					if(item.value.search('<span class="search_separator">-</span>') > 0){
						return $("<li></li>")
						.append("<a href='#'>" + item.value + "</a>")
						.data("ui-autocomplete-item", item)
						.appendTo(ul);
					}else{
						return $("<li class='ui-menu-item'></li>")
		                .append("<a href='#'>" + item.label + "</a>")
		                .data("ui-autocomplete-item", item)
		                .appendTo(ul);						
					}
	            };
            }
        });
    }

	function setPreviousChangeGroupDetails(module, data) {
        
        
                var superUsr        = "<?php echo $_SESSION["superUser"]; ?>";
                var selectedCluster = "<?php echo $_SESSION['selectedCluster']; ?>";
                if(superUsr == "1")
                {                    
                    if( module == "main" ) {
			var dataToSend = { clusterName: selectedCluster, spId: data.spId, spName: data.spName, groupId: data.groupId, groupName: data.selGroupName };
                    } else if(module == "enterprise") {
                            var dataToSend = { clusterName: selectedCluster, spId: data.spId, spName: data.spName };
                    }                    
                }else{
                    if( module == "main" ) {
			var dataToSend = { spId: data.spId, spName: data.spName, groupId: data.groupId, groupName: data.selGroupName };
                    } else if(module == "enterprise") {
                            var dataToSend = { spId: data.spId, spName: data.spName };
                    }
                }
		
		$.ajax({
			type: "POST",
			url: "changeGroupDialog/setChangeGroupDetails.php",
			data: dataToSend,
			success: function (result) {
				console.log(result);
			}
		});
	}

    function occurrences(string, subString, allowOverlapping) {
        string += "";
        subString += "";
        if (subString.length <= 0) return (string.length + 1);

        var n = 0,
            pos = 0,
            step = allowOverlapping ? 1 : subString.length;

        while (true) {
            pos = string.indexOf(subString, pos);
            if (pos >= 0) {
                ++n;
                pos += step;
            } else break;
        }
        return n;
    }

    function decodString(encodedStr) {
    	var parser = new DOMParser;
    	var dom = parser.parseFromString(
    	    '<!doctype html><body>' + encodedStr,
    	    'text/html');
    	var decodedString = dom.body.textContent;
    	return decodedString;
    }
</script>
<link rel="stylesheet" href="/Express/css/custom_bootstrap.css">
<style>
    .ui-autocomplete{
    margin-top: 23.5% !important;
    width: 400px !important;
    max-height: 300px !important;
    position: fixed;
    height: auto !important;
    overflow-y: auto;
  
    }
</style>
<div id="mainBody">
	<div class="login-bg">
		<div class="fadeInUp" id="bodyForm" style="width: 460px">
			<span style="margin-right: 45px"><img src="images/icons/express_nuovo_logo_login2.png" /></span>
			<h4 class="centerDesc">
				<strong style="color: #ffffff; line-height: 1.4;">
					Welcome 
                                        <?php echo (isset($_SESSION["superUser"]) && $_SESSION["superUser"] == 1) ? " Super User " : ""; ?>
                                        <?php echo (isset($_SESSION["superUser"]) && $_SESSION["superUser"] == 3) ? " Enterprise Admin " : ""; ?>
					<br/>
					<?php echo isset($_SESSION["loggedInUser"]) ? $_SESSION["loggedInUser"] : ""; ?>
				</strong>
			</h4>
				<br/>
				<br/>
			<?php 
                        $userTypeList = array("1", "3");
                        //if($_SESSION["superUser"] <> "1"){                        
                        if(!in_array($_SESSION["superUser"], $userTypeList)){
                            ?>
				<div id="securityDomainCheckNonSuperUserDiv">	
					<?php 
					if($_SESSION['showAdminReportMsg']) {
					if((empty($checkSecurityDomainValue) || empty($_SESSION["securityDomain"]) || empty($checkSecurityDomainSPValue))){?>
							<h5>
								<strong style="color: #ffffff; line-height: 1.4; text-align: center; width: 100%; display: block;" class="centerDesc">
									<?php echo 'Cannot login to this account. Please contact Administrator.<br/><br/><a href="/Express/index.php" class="" style="float: right">		
										<span class="whiteClr">Switch User</span>
										</a>';die;
									?>
								</strong>
							</h5>
					<?php } }?>
					
					
					<?php if($_SESSION['adminReportMessage']) {
					?>
							<h5>
								<strong style="color: #ffffff; line-height: 1.4; text-align: center; width: 100%; display: block;" class="centerDesc">
									<?php echo 'Cannot login to this account. Please contact Administrator.<br/><br/><a href="/Express/index.php" class="" style="float: right">		
										<span class="whiteClr">Switch User</span>
										</a>';die;
									?>
								</strong>
							</h5>
					<?php } ?>
					
				</div>
				<div id="securityDomainGroupCheckNonSuperUserDiv" style="display:none">	
					<h5>
						<strong style="color: #ffffff; line-height: 1.4; text-align: center; width: 100%; display: block;" class="centerDesc">
							Cannot login to this account. Please contact Administrator.<br/><br/>
						</strong>
					</h5>
				</div>
			<?php }?>
                               
                                <div style="width:100%;text-align:center;">
				<form name="chooseGroup" id="chooseGroup" method="POST">
                                    
                                    <div class="row" style="display: <?php echo showServiceProvider() ? "block" : "none"; ?>">
						<div class="col-md-12">
                            <div class="form-group">
                            <label class="labelText">Select Enterprise:</label>
                            <div class="dropdown-wrap">   
							<select class="form-control blue-dropdown" name="Selsp" id="Selsp"><?php echo getServiceProvidersList(); ?></select>
                            </div>
</div>
                        </div>
						
					</div>

			<div class="row" name="groupBlock" id="groupBlock" style="display: none">
		                <div class="col-md-12">
		                    <div class="col-md-3"></div>
		                    <div class="col-md-6">
		                    <div class="form-group">
                                <label class="labelText">Select Group:</label>
                                <div class="dropdown-wrap">   
		                        <select class="form-control blue-dropdown" name="groupName" id="groupName"><?php echo getAdminGroups(); ?></select>
</div>
                            </div>
		                    </div>
		                    <div class="col-md-3"></div>
		                </div>
		            </div>

            <div class="row" name="searchBlock" id="searchBlock" style="display: <?php echo (groupExceedSearchThreshold() ? " block" : " none"); ?>">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="labelText" style="">Search for Group ID/Name:</label>
                        <input class="form-control blue-dropdown magnify" type="text" class="autoFill" name="searchGroups" id="searchGroups" style="overflow:scroll;">
						<input class="form-control" type="hidden" name="searchGroupsVal" id="searchGroupsVal">
                    </div>
                </div>
                <div class="col-md-12">
               
                <div class="form-group">
		        <div class="register-submit subBtnDiv">
                        <!-- <input type="button" name="search" id="search" class="subButton" value="Select"> -->
                        <input type="button" name="search" id="search" value="Select" class="submitBtn">
                    </div>
                </div>
                </div>
            </div>
            </form>
            <div class="row eMPSU">
            <div class="">
                <div class="col-md-6 paddingZero">
					 <div class="form-group" id="enterprise_main_page_link" style="float: left; display: none;">
    			<a id="enterprise_main_page_href" href="main_enterprise.php" class="pull-left m-t-mini">
    					<span class="whiteClr">Enterprise Main Page</span>
    				</a>
                    </div>
                </div>
                <div class="col-md-6 paddingZero">
                    <div class="form-group" style="float: right">
    				<a href="/Express/index.php" class="pull-left m-t-mini">
    					<span class="whiteClr">Switch User</span>
    				</a>
                    </div>
                </div>
            </div>
            </div>
	<div id="userData"></div>
</div>
       </div>
	<div class="copyright">
		Copyright © AveriStar All rights reserved.
	</div>
    </div>
</div>