<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
	require_once("/var/www/html/Express/config.php");
	checkLogin();

	require_once("/var/www/lib/broadsoft/adminPortal/getGroupClid.php");
    	require_once("/var/www/lib/broadsoft/adminPortal/getGroupDomains.php");
 	require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once("/var/www/lib/broadsoft/adminPortal/groupCallPickup.php");

    const csvFirstName      = "0";
    const csvLastName       = "1";
    const csvPhoneNumber    = "4";
    const csvCallerId       = "5";
    const csvExtension      = "6";
    const csvDeviceType     = "17";
    const csvLinePortDomain = "19";
    const csvMACAddress     = "20";
    const csvDeviceIndex    = "21";
    const csvPortNumber     = "22";

    const customProfileTable = "customProfiles";
    const customProfileName  = "customProfileName";
    const customProfileType  = "deviceType";


    function hasCustomProfile($deviceType, $customProfileName) {
        global $db;

        $query = "select count(*) from " . customProfileTable . " where " . customProfileType . "='" . $deviceType . "' and " . customProfileName . "='" . $customProfileName . "'";
        $result = $db->query($query);
        return $result->fetchColumn() > 0;
    }


    function getResourceForDeviceNameCreation($attr, $e) {
        switch ($attr) {
            case ResourceNameBuilder::DN:               return isset($e[csvPhoneNumber]) && $e[csvPhoneNumber] != "" ? $e[csvPhoneNumber] : "";
            case ResourceNameBuilder::EXT:              return isset($e[csvExtension]) && $e[csvExtension] != "" ? $e[csvExtension] : "";
            case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:           return $_SESSION["groupId"];
            case ResourceNameBuilder::FIRST_NAME:       return $e[csvFirstName];
            case ResourceNameBuilder::LAST_NAME:        return $e[csvLastName];
            case ResourceNameBuilder::DEV_TYPE:         return $e[csvDeviceType];
            case ResourceNameBuilder::MAC_ADDR:         return $e[csvMACAddress];
            case ResourceNameBuilder::INT_1:
            case ResourceNameBuilder::INT_2:            return $e[csvDeviceIndex];
            case ResourceNameBuilder::USR_ID:
                $name = isset($_SESSION["userId"]) ? $_SESSION["userId"] : "";
                if ($name == "") {
                    return "";
                }
                $nameSplit = explode("@", $name);
                return $nameSplit[0];
                break;
            default: return "";
        }
    }

    /**
     * Get resource value that can be used to build User ID
     * @param $attr                 - resource type
     * @param $e                    - user information from users bulk list
     * @param string $deviceName    - device name if needed to build User ID
     * @return string               - requested resource value
    */
    function getResourceForUserIdCreation($attr, $e, $deviceName = "") {
        global  $surgemailDomain;

        switch ($attr) {
            case ResourceNameBuilder::DN:               return isset($e[csvPhoneNumber]) && $e[csvPhoneNumber] != "" ? $e[csvPhoneNumber] : "";
            case ResourceNameBuilder::EXT:              return isset($e[csvExtension]) && $e[csvExtension] != "" ? $e[csvExtension] : "";
            case ResourceNameBuilder::USR_CLID:         return isset($e[csvCallerId]) && $e[csvCallerId] != "" ? $e[csvCallerId] : "";
            case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:           return str_replace(" ", "_", trim($_SESSION["groupId"]));
            case ResourceNameBuilder::SYS_DFLT_DOMAIN:  return $_SESSION["systemDefaultDomain"];
            case ResourceNameBuilder::DEFAULT_DOMAIN:   return $_SESSION["defaultDomain"];
            case ResourceNameBuilder::GRP_PROXY_DOMAIN: return $e[csvLinePortDomain];
            case ResourceNameBuilder::SRG_DOMAIN:       return $surgemailDomain;
            case ResourceNameBuilder::FIRST_NAME:       return $e[csvFirstName];
            case ResourceNameBuilder::LAST_NAME:        return $e[csvLastName];
            case ResourceNameBuilder::DEV_NAME:         return $deviceName != "" ? $deviceName : "";                        // TODO: Make sure $deviceName is passed
            case ResourceNameBuilder::MAC_ADDR:         return strlen($e[csvMACAddress]) == 12 ? $e[csvMACAddress] : "";
			case ResourceNameBuilder::ENT:         		return strtolower(trim($_SESSION["sp"])); 
            case ResourceNameBuilder::USR_ID:
                $name = isset($_SESSION["userId"]) ? $_SESSION["userId"] : "";
                if ($name == "") {
                    return "";
                }
                $nameSplit = explode("@", $name);
                return $nameSplit[0];
                break;
            default: return "";
        }
    }


?>
<script>
	$(function() {
		$("#bulkDiv").tooltip();
		$("#bulkUserTableId").tablesorter();
		var datastring = $("form#bulkUsers").serializeArray();

		$("#subBulk").click(function()
		{
			$("#loadingBulk").dialog("open");
			$("#loadingBulk").html("<div class=\"loading\" id=\"loading3\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
			$("#loading3").show();

			$.ajax({
				type: "POST",
				url: "bulk/buildBulk.php",
				data: datastring,
				success: function(result)
				{
					$("#loading3").hide();
					$(".ui-dialog-buttonpane", this.parentNode).hide();
					$("#loadingBulk").html(result);
					$("#loadingBulk").append(returnLink);
				}
			});
		});

		$("#loadingBulk").dialog(
		{
			autoOpen: false,
			width: 1400,
			modal: true,
			resizable: false,
			position: { my: "top", at: "top" },
			closeOnEscape: false,
			open: function(event, ui)
			{
				setDialogDayNightMode($(this));
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
			}
		});
	});
</script>
<?php


	$fields = array(
		"0" => array("firstName", "First Name"),
		"1" => array("lastName", "Last Name"),
		"2" => array("callingLineIdFirstName", "Calling Line ID First Name"),
		"3" => array("callingLineIdLastName", "Calling Line ID Last Name"),
		"4" => array("phoneNumber", "Phone Number"),
		"5" => array("callingLineIdPhoneNumber", "Calling Line ID Phone Number"),
		"6" => array("extension", "Extension"),
		"7" => array("department", "Department"),
		"8" => array("addressLine1", "Address"),
		"9" => array("addressLine2", "Suite"),
		"10" => array("city", "City"),
		"11" => array("state", "State/Province"),
		"12" => array("zip", "Zip/Postal Code"),
		"13" => array("timeZone", "Time_Zone"),
		"14" => array("language", "Language"),
		"15" => array("emailAddress", "Email Address"),
		"16" => array("servicePack", "Service Pack"),
		"17" => array("deviceType", "Device Type"),
		"18" => array("customProfile", "Custom Profile"),
		"19" => array("linePortDomain", "LinePort Domain"),
		"20" => array("macAddress", "MAC Address"),
		"21" => array("deviceIndex", "Device Index"),
		"22" => array("portNumber", "Port Number"),
		"23" => array("voiceMessaging", "Voice Messaging"),
		"24" => array("polycomPhoneServices", "Polycom Phone Services"),
		"25" => array("deviceAccessUserName", "Device Access User Name"),
		"26" => array("deviceAccessPassword", "Device Access Password"),
		"27" => array("callingLineIdPolicy", "Calling Line ID Policy"),
		"28" => array("networkClassOfService", "Network Class of Service"),
		"29" => array("callPickupGroup", "Call Pickup Group"),
		"30" => array(".", ".."));


	$is_file_upload = false;

	$exp = array();

	if (file_exists($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

		$is_file_upload = true;

		$tmp_file_name = tempnam(sys_get_temp_dir(), 'BULK');

		$file_type = $_FILES['file']['type'];

		$path = $_FILES['file']['name'];
		$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

		//print_r($_FILES['file']);

		move_uploaded_file(
			$_FILES['file']['tmp_name'], $tmp_file_name
		);

		if (file_exists($tmp_file_name) && ($file_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $ext == "xlsx")) {

			try {
				/** PHPExcel */
				require_once("/var/www/phpToExcel/Classes/PHPExcel.php");
				require_once("/var/www/phpToExcel/Classes/PHPExcel/IOFactory.php");

				$objReader = PHPExcel_IOFactory::createReader('Excel2007');
				//Load the file
				$objPHPExcel = $objReader->load($tmp_file_name);
				//Get number of Sheets
				$sheetCount = $objPHPExcel->getSheetCount();
				//Set the Last Sheet as Active
				$objPHPExcel->setActiveSheetIndex($sheetCount - 1);
				//Read the data into an array
				$sheet = $objPHPExcel->getActiveSheet();
				$maxCell = $sheet->getHighestRowAndColumn();

				//$max_col = PHPExcel_Cell::stringFromColumnIndex(count($fields) - 1);
				$max_col = $maxCell['column'];

				for ($row = 1; $row <= $maxCell['row']; $row++) {
					$next_row = $row + 1;
					$sheet_row = $sheet->rangeToArray("A$row:$max_col$next_row", null, true, false);
					if ($sheet_row && count($sheet_row) > 0) {
						if(!array_filter($sheet_row[0])) {
							break;
						}
						$exp[] = $sheet_row[0];
					}
				}

			} catch (Exception $e) {
				error_log($e->getMessage());
			}

			//echo count($exp);
			//echo "<pre>";
			//print_r($exp);
			//echo "</pre>";
			//exit;

			//Remove the Header
			if (count($exp) > 0) {
				array_shift($exp);
			}

		} else if (file_exists($tmp_file_name) && ($file_type == "text/csv" || $ext == "csv")) {

			try {
				/** PHPExcel */
				require_once("/var/www/phpToExcel/Classes/PHPExcel.php");
				require_once("/var/www/phpToExcel/Classes/PHPExcel/IOFactory.php");

				$objReader = new PHPExcel_Reader_CSV();
				$objPHPExcel = $objReader->load($tmp_file_name);
				$objReader->setSheetIndex(0);

				//Read the data into an array
				$exp = $objPHPExcel->getActiveSheet()->toArray(null, false, true, false);

				//Remove the Header
				if (count($exp) > 0) {
					array_shift($exp);
				}
				//echo "<pre>";
				//print_r($sheetData);
				//echo "</pre>";
			} catch (Exception $e) {
				error_log($e->getMessage());
			}
		}

	} else if( isset($_POST["data"]) ) {

		$exp = explode("\r\n", $_POST["data"]);
		unset($exp[count($exp) - 1]); //remove blank value from end of array
		unset($exp[0]); //remove header row

	}


	$textFields = array("0", "1", "2", "3", "7", "8", "9", "10", "11");
	$requiredFields = array("0", "1", "2", "3", "6", "13", "16", "23", "24", "27");
	if ($portalRequiresClidNumber == "true")
	{
		$requiredFields[] = "5";
	}
	if ($bwRequireAddress == "true")
	{
		$requiredFields[] = "8";
		$requiredFields[] = "10";
		$requiredFields[] = "11";
		$requiredFields[] = "12";
	}
?>
<div id="bulkDiv">
	<form name="bulkUsers" id="bulkUsers" method="POST" action="#">
		<table align="center" class="tablesorter bulkUserTable table" id="bulkUserTableId" style="width:96%;">
			<tr>
				<?php
					foreach ($fields as $headers)
					{
						echo "<th style=\"width:5%;\">" . $headers[1] . "</th>";
					}
				?>
			</tr>
			<?php
				require_once("/var/www/lib/broadsoft/login.php");
				require_once("/var/www/lib/broadsoft/adminPortal/getAvailableNumbers.php");
				require_once("/var/www/lib/broadsoft/adminPortal/allNumbers.php");
				require_once("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");
				require_once("/var/www/lib/broadsoft/adminPortal/getAllDevices.php");
				require_once("/var/www/lib/broadsoft/adminPortal/getDepartments.php");
				require_once("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");
				require_once("/var/www/lib/broadsoft/adminPortal/getTimeZones.php");
                require_once("/var/www/lib/broadsoft/adminPortal/getNetworkClassesOfService.php");
				require_once("/var/www/lib/broadsoft/adminPortal/getLanguages.php");
				require_once("/var/www/lib/broadsoft/adminPortal/getDevices.php");

				//BUILD VARIABLES for username, lineport, etc.
                require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
                require_once("/var/www/lib/broadsoft/adminPortal/UserIdNameBuilder.php");
                require_once("/var/www/lib/broadsoft/adminPortal/LineportNameBuilder.php");
                require_once("/var/www/lib/broadsoft/adminPortal/DeviceNameBuilder.php");
                require_once("/var/www/lib/broadsoft/adminPortal/DBLookup.php");

				$a = 0;
				foreach ($userCheck->command->userTable->row as $key => $value)
				{
					$extensions[$a] = strval($value->col[10]);
					$a++;
				}

				echo "<input type=\"hidden\" name=\"groupId\" id=\"groupId\" value=\"" . $_SESSION["groupId"] . "\">
					<input type=\"hidden\" name=\"sp\" id=\"sp\" value=\"" . $_SESSION["sp"] . "\">";

				if (count($exp) == "0")
				{
					//if there are no entries in the .csv file, generate an error
					$error = "1";
				}
				else
				{
                    //retrieve default domain
                    $defaultDomain = $_SESSION["defaultDomain"];

					foreach ($exp as $v)
					{
                        $sipGateway = "false";
						if ($is_file_upload) {
							$e = $v;
						} else {
							$e = explode(",", $v);
						}
						echo "<tr>";

                        $hasDeviceType = $e[csvDeviceType] != "";

                        $deviceName = "";
                        $derivedDeviceAccessUserName = "";
                        $derivedDeviceAccessPassword = "";

                        if ($hasDeviceType) {
                            // Build Device Name.
                            // ----------------------------------------------------------------------------------------
                            // Choose formula based on device type: DID or analog.
                            // For Analog type always enforce fallback
                            // For DID type, enforce fallback if there is no second criteria/formula.

                            $sipGatewayLookup = new DBLookup($db, "devices", "deviceName", "sipGateway");
                            $sipGateway = $sipGatewayLookup->get($e[csvDeviceType]);
                            $forceResolve = $sipGateway == "true" ? true : $deviceNameDID2 == "";

                            $deviceNameBuilder = new DeviceNameBuilder($sipGateway == "true" ? $deviceNameAnalog : $deviceNameDID1,
                                getResourceForDeviceNameCreation(ResourceNameBuilder::DN, $e),
                                getResourceForDeviceNameCreation(ResourceNameBuilder::EXT, $e),
                                getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN, $e),
                                $forceResolve);

                            if (! $deviceNameBuilder->validate()) {
                                // TODO: Implement resolution when Device name input formula has errors
                            }

                            do {
                                $attr = $deviceNameBuilder->getRequiredAttributeKey();
                                $deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr, $e));
                            } while ($attr != "");

                            $deviceName = $deviceNameBuilder->getName();

                            if ($deviceName == "") {
                                // Create Device Name builder from the second formula (DID type only).
                                // At this point we know that there is second formula, because without second formula
                                // name would be forcefully resolved in the first formula.

                                $deviceNameBuilder = new DeviceNameBuilder($deviceNameDID2,
                                    getResourceForDeviceNameCreation(ResourceNameBuilder::DN, $e),
                                    getResourceForDeviceNameCreation(ResourceNameBuilder::EXT, $e),
                                    getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN, $e),
                                    true);

                                if (! $deviceNameBuilder->validate()) {
                                    // TODO: Implement resolution when Device name input formula has errors
                                }

                                do {
                                    $attr = $deviceNameBuilder->getRequiredAttributeKey();
                                    $deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr, $e));
                                } while ($attr != "");

                                $deviceName = $deviceNameBuilder->getName();
                            }


                            // Build Device Access User Name - fallback definitions are based on Device Name fallback
                            // ----------------------------------------------------------------------------------------
                            // Enforce fallback if there is no second criteria/formula.

                            $deviceAccessUserNameBuilder = new DeviceNameBuilder($deviceAccessUserName1,
                                getResourceForDeviceNameCreation(ResourceNameBuilder::DN, $e),
                                getResourceForDeviceNameCreation(ResourceNameBuilder::EXT, $e),
                                getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN, $e),
                                $deviceAccessUserName2 == "");    // force fallback only if second criteria does not exist

                            if (! $deviceAccessUserNameBuilder->validate()) {
                                // TODO: Implement resolution when Device name input formula has errors
                            }

                            do {
                                $attr = $deviceAccessUserNameBuilder->getRequiredAttributeKey();
                                $deviceAccessUserNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr, $e));
                            } while ($attr != "");

                            $derivedDeviceAccessUserName = $deviceAccessUserNameBuilder->getName();

                            if ($derivedDeviceAccessUserName == "") {
                                $deviceAccessUserNameBuilder = new DeviceNameBuilder($deviceAccessUserName2,
                                    getResourceForDeviceNameCreation(ResourceNameBuilder::DN, $e),
                                    getResourceForDeviceNameCreation(ResourceNameBuilder::EXT, $e),
                                    getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN, $e),
                                    true);

                                if (! $deviceAccessUserNameBuilder->validate()) {
                                    // TODO: Implement resolution when Device name input formula has errors
                                }

                                do {
                                    $attr = $deviceAccessUserNameBuilder->getRequiredAttributeKey();
                                    $deviceAccessUserNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr, $e));
                                } while ($attr != "");

                                $derivedDeviceAccessUserName = $deviceAccessUserNameBuilder->getName();
                            }

                            // Build Device Access Password - fallback definitions are based on Device Name fallback
                            // ----------------------------------------------------------------------------------------
                            $deviceAccessPasswordBuilder = new DeviceNameBuilder($deviceAccessPassword,
                                getResourceForDeviceNameCreation(ResourceNameBuilder::DN, $e),
                                getResourceForDeviceNameCreation(ResourceNameBuilder::EXT, $e),
                                getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN, $e),
                                true);

                            if (! $deviceAccessPasswordBuilder->validate()) {
                                // TODO: Implement resolution when Device name input formula has errors
                            }

                            do {
                                $attr = $deviceAccessPasswordBuilder->getRequiredAttributeKey();
                                $deviceAccessPasswordBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr, $e));
                            } while ($attr != "");

                            $derivedDeviceAccessPassword = $deviceAccessPasswordBuilder->getName();
                        }


                        // Build User ID.
                        // --------------------------------------------------------------------------------------------

                        // Create user ID builder from the first formula.
                        // Enforce fallback if there is no second criteria/formula.
                        // --------------------------------------------------------
                        $userId = "";
                        $userIdWithoutDomain = "";
                        $userIdBuilder1 = new UserIdNameBuilder($userIdCriteria1,
                            getResourceForUserIdCreation(ResourceNameBuilder::DN, $e),
                            getResourceForUserIdCreation(ResourceNameBuilder::EXT, $e),
                            getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID, $e),
                            getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN, $e),
                            getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $e),
							getResourceForUserIdCreation(ResourceNameBuilder::ENT, $e),
                            $userIdCriteria2 == "");    // force fallback only if second criteria does not exist

                        if (! $userIdBuilder1->validate()) {
                            // TODO: Implement resolution when User ID input formula has errors
                        }

                        do {
                            $attr = $userIdBuilder1->getRequiredAttributeKey();
                            $userIdBuilder1->setRequiredAttribute($attr, getResourceForUserIdCreation($attr, $e, $deviceName));
                        } while ($attr != "");

                        $userId = $userIdBuilder1->getName();
                        $userIdWithoutDomain = $userIdBuilder1->getNameWithoutDomain();

                        if ($userId == "") {
                            // Create user ID builder from the second formula. At this point we know
                            // that there is second formula, because without second formula name
                            // would be forcefully resolved in the first formula.

                            $userIdBuilder2 = new UserIdNameBuilder($userIdCriteria2,
                                getResourceForUserIdCreation(ResourceNameBuilder::DN, $e),
                                getResourceForUserIdCreation(ResourceNameBuilder::EXT, $e),
                                getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID, $e),
                                getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN, $e),
                                getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $e),
								getResourceForUserIdCreation(ResourceNameBuilder::ENT, $e),
                                true);

                            if (! $userIdBuilder2->validate()) {
                                // TODO: Implement resolution when User ID input formula has errors
                            }

                            do {
                                $attr = $userIdBuilder2->getRequiredAttributeKey();
                                $userIdBuilder2->setRequiredAttribute($attr, getResourceForUserIdCreation($attr, $e, $deviceName));
                            } while ($attr != "");

                            $userId = $userIdBuilder2->getName();
                            $userIdWithoutDomain = $userIdBuilder2->getNameWithoutDomain();
                        }

                        $_SESSION["userId"] = $userId;
                        $blfUser = $userIdWithoutDomain . "-blf@" . $defaultDomain;


                        $linePort = "";

                        if ($hasDeviceType) {
                            // Build Line Port.
                            // ----------------------------------------------------------------------------------------

                            // Create lineport name from the first formula.
                            // Enforce fallback if there is no second criteria/formula.
                            // --------------------------------------------------------
                            $linePortBuilder1 = new LineportNameBuilder($linePortCriteria1,
                                getResourceForUserIdCreation(ResourceNameBuilder::DN, $e),
                                getResourceForUserIdCreation(ResourceNameBuilder::EXT, $e),
                                getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID, $e),
                                getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN, $e),
                                getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $e),
                                $linePortCriteria2 == "");  // force fallback only if second criteria does not exist

                            if (! $linePortBuilder1->validate()) {
                                // TODO: Implement resolution when lineport input formula has errors
                            }

                            do {
                                $attr = $linePortBuilder1->getRequiredAttributeKey();
                                $linePortBuilder1->setRequiredAttribute($attr, getResourceForUserIdCreation($attr, $e, $deviceName));
                            } while ($attr != "");

                            $linePort = $linePortBuilder1->getName();

                            if ($linePort == "") {
                                // Create lineport name builder from the second formula. At this point we know that
                                // there is second formula, because without second formula name would be forcefully
                                // resolved in the first formula.

                                $linePortBuilder2 = new LineportNameBuilder($linePortCriteria2,
                                    getResourceForUserIdCreation(ResourceNameBuilder::DN, $e),
                                    getResourceForUserIdCreation(ResourceNameBuilder::EXT, $e),
                                    getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID, $e),
                                    getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN, $e),
                                    getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $e),
                                    true);

                                if (! $linePortBuilder2->validate()) {
                                    // TODO: Implement resolution when User ID input formula has errors
                                }

                                do {
                                    $attr = $linePortBuilder2->getRequiredAttributeKey();
                                    $linePortBuilder2->setRequiredAttribute($attr, getResourceForUserIdCreation($attr, $e, $deviceName));
                                } while ($attr != "");

                                $linePort = $linePortBuilder2->getName();
                            }
							$linePort = str_replace(" ", "_", $linePort);
                        }


						foreach ($e as $key => $value)
						{
							$value .= '';
							$class = "good";
							$errorMsg = "";
							if (in_array($key, $textFields) and strlen($value) > 0)
							{
								if (!preg_match("/^[ A-Za-z0-9\s-]+$/", $value))
								{
									$class = "bad";
									$error = "1";
									$errorMsg = $fields[$key][1] . " has a bad character.";
								}
							}
							if (in_array($key, $requiredFields))
							{
								if (strlen($value) < 1)
								{
									$class = "bad";
									$value = "&nbsp;";
									$error = "1";
									$errorMsg = $fields[$key][1] . " is a required field.";
								}
							}
							if ($key == "4") //phone numbers
							{
								if ($value !== "" and !in_array($value, $availableNumbers))
								{
									$class = "bad";
									$error = "1";
									$errorMsg = $value . " is not an available " . $fields[$key][1] . ".";
								}
                                if ($hasDeviceType)
                                {
                                    if ($sipGateway == "false" && in_array($deviceName, $devices))
                                    {
                                        $class = "bad";
                                        $error = "1";
                                        $errorMsg = "This number creates a device name that is already in use.";
                                    }
                                }
//								if (in_array($phExt, $chkUserId))
                                if (isset($chkUserId) && in_array($userId, $chkUserId))
								{
									$class = "bad";
									$error = "1";
									$errorMsg = "This number creates a user ID that is already in use.";
								}
							}
							if ($key == "5") //calling line ID phone numbers
							{
								if ($value !== "" and !in_array($value, $allNumbers))
								{
									$class = "bad";
									$error = "1";
									$errorMsg = $value . " is not an available " . $fields[$key][1] . ".";
								}
							}
							if ($key == "6") //extensions
							{
								if (isset($extensions) && in_array($value, $extensions))
								{
									$class = "bad";
									$error = "1";
									$errorMsg = $fields[$key][1] . " " . $value . " already in use.";
								}else{
									if (!is_numeric($value))
									{
										$error = 1;
										$class = "bad";
										$value = "Extension must be numeric.";
									}
									//min length and max length
									$minLen = intval($_SESSION['groupExtensionLength']['min']);
									$maxLen = intval($_SESSION['groupExtensionLength']['max']);

									if (strlen($value) < $minLen or strlen($value) > $maxLen)
									{
										$error = 1;
										$class = "bad";
										if ($minLen == $maxLen)
										{
											$errorMsg = "Extension must be " . $minLen . " digits.";
										}
										else
										{
											$errorMsg = "Extension must be between " . $minLen . " and " . $maxLen . " digits.";
										}
									}
								}
							}
							if ($key == "7") //departments
							{
								if (isset($departments) && !in_array($value, $departments) and strlen($value) > 0)
								{
									$class = "bad";
									$error = "1";
									$errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
								}
							}
							if ($key == "13") //time zones
							{
								if (!in_array($value, $timeZones) and strlen($value) > 0)
								{
									$class = "bad";
									$error = "1";
									$errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
								}
							}
							if ($key == "14") //languages
							{
								if (!in_array($value, $languages) and strlen($value) > 0)
								{
									$class = "bad";
									$error = "1";
									$errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
								}
							}
							if ($key == "15" && $value != "") //email addresses
							{
                                if (! filter_var($value, FILTER_VALIDATE_EMAIL)) {
                                    $class = "bad";
                                    $error = "1";
                                    $errorMsg = $value . " doesn&#39;t seem like a valid " . $fields[$key][1] . ".";
                                }
							}
							if ($key == "16") //service packs
							{
								$servicePackArray = explode(";", $value);
								for ($i = 0; $i < count($servicePackArray); $i++)
								{
									if (!in_array($servicePackArray[$i], $servicePacks))
									{
										$class = "bad";
										$error = "1";
										$errorMsg = $servicePackArray[$i] . " is not a valid " . $fields[$key][1] . ".";
									}
								}
							}
                            if ($hasDeviceType)
                            {
                                if ($key == "17") //device types
                                {
                                    if (!in_array($value, $nonObsoleteDevices))
                                    {
                                        $class = "bad";
                                        $error = "1";
                                        $errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
                                    }
                                }
                                if ($key == "18") { //custom profiles
                                    if ($value != "" && $e[csvDeviceType] != "" && ! hasCustomProfile($e[csvDeviceType], $value))
                                    {
                                        $class = "bad";
                                        $error = "1";
                                        $errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
                                    }
                                }
                                if ($key == "20") //MAC addresses
                                {
                                    if (!preg_match("/^[ A-Fa-f0-9]+$/", $value) and strlen($value) > 0)
                                    {
                                        $class = "bad";
                                        $error = "1";
                                        $errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
                                    }
                                    if (strlen($value) > 0 and strlen($value) != "12")
                                    {
                                        $class = "bad";
                                        $error = "1";
                                        $errorMsg = $value . " is not 12 characters.";
                                    }

                                    $xmlinput = xmlHeader($sessionid, "SystemAccessDeviceGetAllRequest");
                                    $xmlinput .= "<searchCriteriaDeviceMACAddress>
											<mode>Equal To</mode>
											<value>" . $value . "</value>
											<isCaseInsensitive>true</isCaseInsensitive>
										</searchCriteriaDeviceMACAddress>";
                                    $xmlinput .= xmlFooter();
                                    $response = $client->processOCIMessage(array("in0" => $xmlinput));
                                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

                                    $count = count($xml->command->accessDeviceTable->row);
                                    if ($count > 0)
                                    {
                                        $deviceName = strval($xml->command->accessDeviceTable->row->col[3]);
                                        $usedByGroup = strval($xml->command->accessDeviceTable->row->col[2]);
                                        $usedBySP = strval($xml->command->accessDeviceTable->row->col[0]);
                                        $class = "bad";
                                        $error = "1";
                                        $errorMsg = $value . " used by " . $deviceName . " in Group " . $usedByGroup . " in SP " . $usedBySP . ".";
                                    }
                                }
                                if ($key == "21") { // Device Index
                                    if (!ctype_digit($value . '') || (int)$value < 0) {
                                        $class = "bad";
                                        $error = "1";
                                        $errorMsg = "Expected 0 or a non-negative number";
                                    }
                                }
                                if ($key == "22") { // Port Number
                                    if (!ctype_digit($value .'') || (int)$value < 0) {
                                        $class = "bad";
                                        $error = "1";
                                        $errorMsg = "Expected 0 or a non-negative number";
                                    }
                                }
                            }
							if ($key == "23" or $key == "24") //voice messaging/Polycom phone services
							{
								if (strtoupper($value) != "YES" and strtoupper($value) != "NO")
								{
									$class = "bad";
									$error = "1";
									$errorMsg = $fields[$key][1] . " must be either yes or no.";
								}
							}
							if ($key == "26") //device access password
							{
								if ($value)
								{
//									$value = str_repeat("*", strlen($value)); //don't display password
								}
							}
							if ($key == "27") //calling line ID policy
							{
								if (strtoupper($value) != "USER" and strtoupper($value) != "GROUP")
								{
									$class = "bad";
									$error = "1";
									$errorMsg = $fields[$key][1] . " must be either user or group.";
								}
							}
                            if ($key == "28") //Network Classes of Services
                            {
                                if (!in_array($value, $networkClassesOfService) and strlen($value) > 0)
                                {
                                    $class = "bad";
                                    $error = "1";
                                    $errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
                                }
                            }
                            if ($key == "19") { // linePortDomain
                                if ($value == "" && $hasDeviceType) {
                                    $class = "bad";
                                    $error = "1";
                                    $errorMsg = "Required field when device is specified.";
                                }
                                if ($value != "" && ! in_array($value, $groupDomains)) {
                                    $class = "bad";
                                    $error = "1";
                                    $errorMsg = $fields[$key][1] . " does not match any group domain.";
                                }
                            }
                            if($key == "29"){
								$groupCP = new GroupCallPickup();
								$groupCallPickupList = $groupCP->showGroupCallPickupInstanceCSV();
                            	if(isset($groupCallPickupList)){
	                            	if($value != "" && ! in_array($value, $groupCallPickupList)){
	                            		$class = "bad";
	                            		$error = "1";
	                            		$errorMsg = $fields[$key][1] . " does not exist.";
	                            	}
                            	}
                            }
							echo "<td class=\"" . $class . "\">";
                            echo "<input class=\"noBorder\" title=\"" . $errorMsg . "\" type=\"text\" name=\"users[" . $userId . "][" . $fields[$key][0] . "]\" id=\"users[" . $userId . "][" . $fields[$key][0] . "]\" value=\"" . $value . "\" style=\"width:98%;\" readonly=\"readonly\">";
							echo "</td>";
						}
                        echo " <input type=\"hidden\" name=\"users[" . $userId . "][userId]\" id=\"users[" . $userId . "][userId]\" value=\"" . $userId . "\">";
                        echo " <input type=\"hidden\" name=\"users[" . $userId . "][deviceName]\" id=\"users[" . $userId . "][deviceName]\" value=\"" . $deviceName . "\">";
                        echo " <input type=\"hidden\" name=\"users[" . $userId . "][derivedDeviceAccessUserName]\" id=\"users[" . $userId . "][derivedDeviceAccessUserName]\" value=\"" . $derivedDeviceAccessUserName . "\">";
                        echo " <input type=\"hidden\" name=\"users[" . $userId . "][derivedDeviceAccessPassword]\" id=\"users[" . $userId . "][derivedDeviceAccessPassword]\" value=\"" . $derivedDeviceAccessPassword . "\">";
                        echo " <input type=\"hidden\" name=\"users[" . $userId . "][linePort]\" id=\"users[" . $userId . "][linePort]\" value=\"" . $linePort . "\">";
                        echo " <input type=\"hidden\" name=\"users[" . $userId . "][blfUser]\" id=\"users[" . $userId . "][blfUser]\" value=\"" . $blfUser . "\">";

						echo "</tr>";
					}
				}

				echo "<tr><td colspan=\"100%\">&nbsp;</td></tr>";
				echo "<tr><td colspan=\"100%\">&nbsp;</td></tr>";
				echo "<tr><td colspan=\"100%\" align=\"center\">";
				if (isset($error) and $error == "1")
				{
					echo "Please correct errors and resubmit.";
				}
				else
				{
					echo "<input class=\"subButton\" type=\"button\" name=\"subBulk\" id=\"subBulk\" value=\"Build Users\">";
				}
				echo "</td></tr>";
			?>
		</table>
	</form>
</div>
<div id="loadingBulk"></div>
