<?php
    /**
     * User: Karl
     * Date: 5/1/2016
     * Time: 6:25 PM
     */

    require_once("/var/www/lib/broadsoft/login.php");
    require_once("/var/www/html/Express/config.php");
    checkLogin();
    require_once("/var/www/lib/broadsoft/adminPortal/arrays.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");

?>

<script>
    var retLink = "<p style='font-size: 12px'><a href=\"main.php\">Click here to return to the beginning</a></p>";

    $(function() {
        $("#grpWideDiv").tooltip();
        var datastring = $("form#grpWideForm").serializeArray();
        $("#modifyBulk").click(function() {
            $("#loadingModifyBulk").dialog("open");
            $("#loadingModifyBulk").html("<div class=\"loading\" id=\"loading4\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
            $("#loading4").show();

            $.ajax({
                type: "POST",
                url: "bulk/executeGroupWideModifications.php",
                data: datastring,
                success: function(result)
                {
                    $("#loading4").hide();
                    $(".ui-dialog-buttonpane", this.parentNode).hide();
                    $("#loadingModifyBulk").html(result);
                    $("#loadingModifyBulk").append(retLink);
                }
            });
        });

        $("#loadingModifyBulk").dialog( {
            autoOpen: false,
            width: 1400,
            modal: true,
            resizable: false,
            position: { my: "top", at: "top" },
            closeOnEscape: false,
            open: function(event, ui)
            {
				setDialogDayNightMode($(this));
                $(".ui-dialog-titlebar-close", ui.dialog).hide();
            }
        });
    });
</script>

<?php
    const F_Required = 0;
    const F_Validation = 1;
    const F_Key = 2;
    const F_Name = 3;

    const Val_UidCFA = "validateUidCFA";                        // Validate UID and Call Forwarding Always Service Assignment
    const Val_UidCFNR = "validateUidCFNR";                      // Validate UID and Call Forwarding Not Reachable Service Assignment
    const Val_UidAuth = "validateUidAuth";                      // Validate UID and Authentication Service Assignment
    const Val_Uid = "validateUid";                              // Validate UID presence only
    const Val_TrueFalse = "validateTrueFalse";                  // Validate True/False attribute
    const Val_FwdPhNum = "validateFwdPhNum";                    // Validate Forwarding Phone Number
    const Val_AuthPassword = "validateAuthPassword";            // Validate Authentication Password
    const Val_AvailUserService = "validateAvailUserService";    // Validate User Service availability for assignment
    const Val_AvailServicePack = "validateAvailServicePack";    // Validate Service Pack name
    const Val_None = "validateNone";                            // No validation required

    const Bulk_CFA = "gwModsCFA";
    const Bulk_CFNR = "gwModsCFNR";
    const Bulk_UsrAuth = "gwModsAuth";
    const Bulk_AssignSwPck = "gwModsSwPck";

    $is_file_upload = false;

    $requiresServiceAuthorizationModes = array(Bulk_CFA, Bulk_CFNR, Bulk_UsrAuth);

    $cfaFields = array(
        // Array: Required attribute, Validation Method, Attribute key, Attribute full description
        "0" => array(true,  Val_UidCFA,       "userId",               "User ID"),
        "1" => array(true,  Val_TrueFalse,    "isActive",             "Active"),
        "2" => array(false, validateFwdPhNum, "forwardToPhoneNumber", "Forward To Phone Number"),
        "3" => array(false, Val_TrueFalse,    "isRingSplashActive",   "Ring Splash Active")
    );

    $cfnrFields = array(
        // Array: Required attribute, Validation Method, Attribute key, Attribute full description
        "0" => array(true,  Val_UidCFNR,      "userId",               "User ID"),
        "1" => array(true,  Val_TrueFalse,    "isActive",             "Active"),
        "2" => array(false, validateFwdPhNum, "forwardToPhoneNumber", "Forward To Phone Number")
    );

    $userAuthenticationFields = array(
        // Array: Required attribute, Validation Method, Attribute key, Attribute full description
        "0" => array(true, Val_UidAuth,      "userId",      "User ID"),
        "1" => array(true, Val_None,         "userName",    "SIP Authentication User Name"),
        "2" => array(true, Val_AuthPassword, "newPassword", "Authentication Password")
    );

    $userAssignServicePackFields = array(
        // Array: Required attribute, Validation Method, Attribute key, Attribute full description
        "0" => array(true,  Val_Uid,              "userId",            "User ID"),
        "1" => array(false, Val_AvailUserService, "serviceName",       "User Service Name"),
        "2" => array(false, Val_TrueFalse,        "assignServiceName", "Assign Service"),
        "3" => array(false, Val_AvailServicePack, "servicePackName",   "Service Pack Name"),
        "4" => array(false, Val_TrueFalse,        "assignServicePack", "Assign Service Pack")
    );

    $isError = false;

    switch ($_POST["bulkType"]) {
        case Bulk_CFA:
            $attrFields = $cfaFields;
            break;
        case Bulk_CFNR:
            $attrFields = $cfnrFields;
            break;
        case Bulk_UsrAuth:
            require_once ("/var/www/lib/broadsoft/adminPortal/getAuthenticationPasswordRules.php");
            $attrFields = $userAuthenticationFields;
            break;
        case Bulk_AssignSwPck:
            require_once("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");
            $attrFields = $userAssignServicePackFields;
            break;
    }


    // get available for processing rows from the .csv file
    // ----------------------------------------------------
    function getBulkRows()
    {

    	global $is_file_upload;

	    $exp = array();

	    if (file_exists($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

		    $is_file_upload = true;

		    $tmp_file_name = tempnam(sys_get_temp_dir(), 'BULK');

		    $file_type = $_FILES['file']['type'];

		    $path = $_FILES['file']['name'];
		    $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

		    //print_r($_FILES['file']);

		    move_uploaded_file(
			    $_FILES['file']['tmp_name'], $tmp_file_name
		    );

		    if (file_exists($tmp_file_name) && ($file_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $ext == "xlsx")) {

			    try {
				    /** PHPExcel */
				    require_once("/var/www/phpToExcel/Classes/PHPExcel.php");
				    require_once("/var/www/phpToExcel/Classes/PHPExcel/IOFactory.php");

				    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
				    //Load the file
				    $objPHPExcel = $objReader->load($tmp_file_name);
				    //Get number of Sheets
				    $sheetCount = $objPHPExcel->getSheetCount();
				    //Set the Last Sheet as Active
				    $objPHPExcel->setActiveSheetIndex($sheetCount - 1);
				    //Read the data into an array
				    $sheet = $objPHPExcel->getActiveSheet();
				    $maxCell = $sheet->getHighestRowAndColumn();

				    //$max_col = PHPExcel_Cell::stringFromColumnIndex(count($fields) - 1);
				    $max_col = $maxCell['column'];

				    for ($row = 1; $row <= $maxCell['row']; $row++) {
					    $next_row = $row + 1;
					    $sheet_row = $sheet->rangeToArray("A$row:$max_col$next_row", null, true, false);
					    if ($sheet_row && count($sheet_row) > 0) {
						    if(!array_filter($sheet_row[0])) {
							    break;
						    }
						    $exp[] = $sheet_row[0];
					    }
				    }

			    } catch (Exception $e) {
				    error_log($e->getMessage());
			    }

			    //echo count($exp);
			    //echo "<pre>";
			    //print_r($exp);
			    //echo "</pre>";
			    //exit;

			    //Remove the Header
			    if (count($exp) > 0) {
				    array_shift($exp);
			    }

		    } else if (file_exists($tmp_file_name) && ($file_type == "text/csv" || $ext == "csv")) {

			    try {
				    /** PHPExcel */
				    require_once("/var/www/phpToExcel/Classes/PHPExcel.php");
				    require_once("/var/www/phpToExcel/Classes/PHPExcel/IOFactory.php");

				    $objReader = new PHPExcel_Reader_CSV();
				    $objPHPExcel = $objReader->load($tmp_file_name);
				    $objReader->setSheetIndex(0);

				    //Read the data into an array
				    $exp = $objPHPExcel->getActiveSheet()->toArray(null, false, true, false);

				    //Remove the Header
				    if (count($exp) > 0) {
					    array_shift($exp);
				    }
				    //echo "<pre>";
				    //print_r($sheetData);
				    //echo "</pre>";
			    } catch (Exception $e) {
				    error_log($e->getMessage());
			    }
		    }

	    } else if( isset($_POST["data"]) ) {

		    $exp = explode("\r\n", $_POST["data"]);
		    unset($exp[count($exp) - 1]); //remove blank value from end of array
		    unset($exp[0]); //remove header row

	    }

	    return $exp;
    }


    // back-end validation of User ID.
    // Function returns error message if a user with requested User ID is not provisioned in the group
    // -----------------------------------------------------------------------------------------------
    function validateUserId($userId)
    {
        global $sessionid, $client;

        $xmlInput = xmlHeader($sessionid, "UserGetListInGroupRequest");
        $xmlInput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlInput .= "<GroupId>" . htmlspecialchars($_SESSION["groupId"]) . "</GroupId>";
        $xmlInput .= "<searchCriteriaUserId>";
        $xmlInput .= "<mode>Equal To</mode>";
        $xmlInput .= "<value>" . $userId . "</value>";
        $xmlInput .= "<isCaseInsensitive>false</isCaseInsensitive>";
        $xmlInput .= "</searchCriteriaUserId>";
        $xmlInput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlInput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        return ! isset($xml->command->userTable->row) ? "Invalid User ID: " . $userId : "";
    }


    // back-end validation whether a service is assigned to the user
    // also validates whether it's a valid User
    // returns true is the service: $service is assigned to user:userId, otherwise false
    // ---------------------------------------------------------------------------------
    function validateServiceAssignment($userId, $service)
    {
        $errMsg = validateUserId($userId);
        if ($errMsg != "") {return $errMsg;}

        $services = getUserAssignedUserServices($userId);
        $hasService = $services != null && in_array($service, $services);
        if (! $hasService) {
            return "Service '" . $service . "' is not assigned to this user.";
        }

        return "";
    }


    // Get all User Services in Back-End to verify whether the requested service is authorized
    // for assignment to a User.
    // Function returns empty string if service is authorized, otherwise error is returned.
    // ---------------------------------------------------------------------------------------
    function validateUserServiceAuthorization($userService)
    {
        global $userServices;

        if ($userService == "") {return "";}
        return ! in_array($userService, $userServices) ?
            "Service " . $userService . " is not authorized for assignment to the user" : "";
    }


    // Get all Service Packs in Back-End to verify whether the requested service pack is authorized
    // for assignment to a User.
    // Function returns empty string if service pack is authorized, otherwise error is returned.
    // --------------------------------------------------------------------------------------------
    function validateServicePackAuthorization($servicePack)
    {
        global $servicePacks;

        if ($servicePack == "") {return "";}
        return ! in_array($servicePack, $servicePacks) ?
            "Service Pack " . $servicePack . " is not authorized for assignment to the user" : "";
    }

    // Validate Authentication Password (length) based on authentication password rules
    // retrieved from BroadWorks
    // --------------------------------------------------------------------------------
    function validateAuthenticationPassword($value)
    {
        global $authenticationPasswordMinLength;
        if ($value == "") {return "";}
        return strlen($value) < $authenticationPasswordMinLength ?
            "Password length must be at least " . $authenticationPasswordMinLength . " characters." : "";
    }


    // Check for the content of a mandatory field
    // ------------------------------------------
    function validateMandatoryField($fields, $key, $value)
    {
        if ($value != "") {return "";}

        $attr = $fields[$key];
        return $attr[F_Required] ? $attr[F_Name] . " is a required field" : "";
    }


    // Check for the content of a true/false field
    // Empty field is not an error, as a true/false field may not be a mandatory field
    // -------------------------------------------------------------------------------
    function validateTrueFalseField($value)
    {
        if ($value == "") {return "";}
        return $value != "true" && $value != "false" ? "Expected 'true' or 'false'" : "";
    }

    // Validate true/false User Services / Service Packs assignment fields
    // -------------------------------------------------------------------
    function validateTrueFalseFieldForAuthorizations($value, $authorizations)
    {
        $service = "";
        // To avoid repetitions validate Service Pack first as it is added to $authorizations after User Services
        if (isset($authorizations["servicePack"])) {
            $service = "Service Pack";
            $hasContent = $authorizations["servicePack"] != "";
        }
        else if (isset($authorizations["userService"])) {
            $service = "User Service";
            $hasContent = $authorizations["userService"] != "";
        }
        else {return "";}

        // Error if no true/false assignment attribute is not set but service attribute has been set
        if ($value == "") {
            return $hasContent ? "Assign " . $service . " Name must be set if " . $service . " Name is specified" : "";
            }
        // Error if true/false assignment attribute is set but service attribute has not been set.
        else {
            return ! $hasContent ? "Assign " . $service . " Name is not allowed if " . $service . " Name is not specified" : "";
        }
    }


    // Syntax validation for Forward To Phone Number attribute
    // Also verifies Forward To number against a list of restricted numbers defined in BroadWorks
    // ------------------------------------------------------------------------------------------
    function validateForwardingPhoneNumber($number, $serviceActive)
    {
        global $restrictedNumbers;

        if ($number == "") {
            return $serviceActive == "true" ? "Phone number is required if service is being activated." : "";
        }

        if (!is_numeric($number)) {
            return "Phone number must be:  -, 0-9 or a E.164 formatted number.";
        }

        foreach ($restrictedNumbers as $k => $v) {
            if (substr($number, 0, strlen($k)) === $k and strlen($number) > strlen($k) + 5) {
                return $number . " is a restricted number";
            }
        }
    }

    // create CSV table header
    // This function is used for HTML rendering, so the built header is echoed
    // -----------------------------------------------------------------------
    function createHeader($fields, $colWidth)
    {
        $header = "<tr>";
        foreach ($fields as $content) {
            $header .= "<th style=\"font-weight: bolder; font-size: 12px; width:" . $colWidth . ";\">" . $content[F_Name] . "</th>";
        }

        $header .= "</tr>";
        echo $header;
    }


    // Confirmation header names that would be used in creating confirmation table during modifying users
    // --------------------------------------------------------------------------------------------------
    function createConfirmationHeaderInputs($fields)
    {
        $index = 0;
        foreach ($fields as $attributes) {
            $dataVar = "header[" . $index . "]";
            echo "<input type=\"hidden\" name=\"" . $dataVar . "\" id=\"" . $dataVar . "\" value=\"" . $attributes[F_Name] . "\">";
            $index++;
        }
    }


    // Hidden modification mode carried into processing modifications
    // --------------------------------------------------------------
    function createModeInput()
    {
        echo "<input type=\"hidden\" name=\"mode\" id=\"mode\" value=\"" . $_POST["bulkType"] . "\">";
    }


    // complete Add/Modify Bulk Users form based on validation error status:
    // - if succes  - submit button is created
    // - if failure - informative message is created
    // ---------------------------------------------------------------------
    function processError()
    {
        global $isError;
        $row = "<tr><td style=\"color:#9c9c9c\" colspan=\"100%\" align=\"center\">";
        $row .= $isError ? "<label class=\"labelTextGrey\"> Please correct errors and resubmit." :
            "<input class=\"subButton\" style=\"font-size: 12px\" type=\"button\" name=\"modifyBulk\" id=\"modifyBulk\" value=\"Modify Users\">";
        $row .= "</td></tr>";

        echo $row;
    }


    // Build the table with values from CSV file. Validate CSV file data.
    // The input is a parsing table for a specific CSV file
    // The function returns true is all data in the table validates, otherwise false
    // This function is used for HTML rendering, so the built table is echoed
    // -----------------------------------------------------------------------------
    function buildBulkTable($fields)
    {
        global $requiresServiceAuthorizationModes, $is_file_upload;
        $table = "";
        $success = true;

        $rows = getBulkRows();
        if (count($rows) == 0) {
            return false; // Error if no rows detected
        }

        $index = 0;
        foreach ($rows as $row) {
            $table .= "<tr>";

            if($is_file_upload) {
	            $col = $row;
            } else {
	            $col = explode(",", $row);
            }

            $lastTrueFalse = false;
            $authorizations = array();
            foreach ($col as $key => $value) {
	            $value .= '';
                $class = "good";
                $errMsg = validateMandatoryField($fields, $key, $value);
                if ($errMsg != "") {goto finalize;}

                $attr = $fields[$key];
                $name = $attr[F_Key];
                switch ($attr[F_Validation]) {
                    case Val_UidCFA:            $errMsg = validateServiceAssignment($value, "Call Forwarding Always"); break;
                    case Val_UidCFNR:           $errMsg = validateServiceAssignment($value, "Call Forwarding Not Reachable"); break;
                    case Val_UidAuth:           $errMsg = validateServiceAssignment($value, "Authentication"); break;
                    case Val_Uid:               $errMsg = validateUserId($value); break;
                    case Val_FwdPhNum:          $errMsg = validateForwardingPhoneNumber($value, $lastTrueFalse); break;
                    case Val_AuthPassword:      $errMsg = validateAuthenticationPassword($value); break;
                    case Val_AvailUserService:
                        $authorizations["userService"] = $value;
                        $errMsg = validateUserServiceAuthorization($value);
                        break;
                    case Val_AvailServicePack:
                        $authorizations["servicePack"] = $value;
                        $errMsg = validateServicePackAuthorization($value);
                        if ($value == "" && $authorizations["userService"] == "") {
                            $errMsg = "At least one: User Service Name or Service Pack Name must be provided";
                        }
                        break;
                    case Val_TrueFalse:
                        $v = strtolower($value);
                        $lastTrueFalse = $v;
                        $errMsg = validateTrueFalseField($v);
                        if ($errMsg == "") {
                            $errMsg = validateTrueFalseFieldForAuthorizations($v, $authorizations);
                        }
                        break;
                    case Val_None:
                        break;
                }

                finalize:
                if ($errMsg != "") {
                    $class = "bad";
                    $success = false;
                }

                $dataVar = "data[" . $index . "][" . $name . "]";

                $table .= "<td class=\"" . $class . "\">";
                $table .= "<input class=\"noBorder\" title=\"" . $errMsg . "\" type=\"text\" name=\"" . $dataVar . "\" id=\"" . $dataVar . "\" value=\"" . $value . "\" style=\"width:100% !important;\" readonly=\"readonly\">";
                $table .= "</td>";
            }
            $index++;
            $table .= "</tr>";
        }
        echo $table;
        return $success;
    }

?>

<div style="" id="grpWideDiv">
    <form name="grpWideForm" id="grpWideForm" method="POST" action="#">
        <?php createModeInput(); ?>
        <?php createConfirmationHeaderInputs($attrFields); ?>
        <table style="width: 100% !important" align="center" class="grpWideTable table">
<!--             <tr><td colspan=\"100%\">&nbsp;</td></tr> -->
            <?php createHeader($attrFields, "20%"); ?>
            <?php $isError = ! buildBulkTable($attrFields); ?>
            <!-- <tr><td colspan=\"100%\">&nbsp;</td></tr>
            <tr><td colspan=\"100%\">&nbsp;</td></tr>-->
            <?php processError(); ?>
        </table>
    </form>
</div>
<div id="loadingModifyBulk"></div>
