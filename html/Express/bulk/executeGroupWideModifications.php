<?php
    /**
     * User: Karl
     * Date: 5/6/2016
     * Time: 11:38 AM
     */

    require_once("/var/www/html/Express/config.php");
    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();

    require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
    $expProvDB = new ExpressProvLogsDB();
    $GLOBALS['expressProvLogsDb'] = $expProvDB->expressProvLogsDb;
    
    const Bulk_CFA = "gwModsCFA";
    const Bulk_CFNR = "gwModsCFNR";
    const Bulk_UsrAuth = "gwModsAuth";
    const Bulk_AssignSwPck = "gwModsSwPck";


    /*
     * Add entry to AveriStar Change Log
     */
    function createChangeLogEntry($userLastFirstName)
    {
        global $sth, $db;
        $expressProvLogsDb = $GLOBALS['expressProvLogsDb'];
        $query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
        $query .= " VALUES ('" .
            $_SESSION["loggedInUserName"] . "', '" .
            date("Y-m-d H:i:s") .
            "', 'Users:Group-Wide Modify', '" .
            $_SESSION["groupId"] . "', '" .
            $userLastFirstName .
            "')";
            $sth = $expressProvLogsDb->query($query);
    }


    /*
     * Add entry to AveriStar Users Modifications Log
     */
    function createUserModificationLogEntry($userId, $userLastFirstName, $field, $newValue)
    {
        global $pth, $db;
        $expressProvLogsDb = $GLOBALS['expressProvLogsDb'];
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES,TRUE);
        $lastId = $expressProvLogsDb->lastInsertId();
        $query = "INSERT into userModChanges (id, serviceId, entityName, field, oldValue, newValue) ";
        $query .= "VALUES ('" .
            $lastId . "', '" .
            $userId . "', '" .
            $userLastFirstName . "', '" .
            $field . "', '" .
            "N/A" . "', '" .
            $newValue .
            "')";
            $pth = $expressProvLogsDb->query($query);
    }


    /*
     * Get BroadWorks Request for service assignment/unassignment based on the input from the form
     * Returns 'UserServiceAssignListRequest' or 'UserServiceUnassignListRequest'
     * or empty string if the name of service or service pack is not set
     */
    function getServiceAssignmentRequest($name, $assignment)
    {
        if ($name == "None") {return ""; }
        return $assignment == "true" ? "UserServiceAssignListRequest" : "UserServiceUnassignListRequest";
    }

    /*
     * Get User's Last Name and First Name
     * Returns: "Last Name, First Name"
     */
    function getUserName($userId)
    {
        global $ociVersion, $sessionid, $client;

        if ($ociVersion == "17") {
            $xmlinput = xmlHeader($sessionid, "UserGetRequest17sp4");
        } else if ($ociVersion == "20") {
            $xmlinput = xmlHeader($sessionid, "UserGetRequest20");
        } else {
            $xmlinput = xmlHeader($sessionid, "UserGetRequest19");
        }
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $lastName = strval($xml->command->lastName);
        $firstName = strVal($xml->command->firstName);

        return $lastName . ", " . $firstName;
    }


    // Back-end modification of Call Forwarding services:
    // - Call Forwarding Always
    // - Call Forwarding Not Reachable
    // Parameters: modification data and service to be modified
    // --------------------------------------------------------
    function modifyCallForwarding($data, $serviceKey)
    {
        global $sessionid, $client;

        $userId = $data["userId"];
        $isActive = strtolower($data["isActive"]);
        $phNumber = $data["forwardToPhoneNumber"];
        if ($serviceKey == Bulk_CFA) {
            $ringSplashActive = strtolower($data["isRingSplashActive"]);
        }

        $ociRequest = $serviceKey == Bulk_CFA ?
            "UserCallForwardingAlwaysModifyRequest" : "UserCallForwardingNotReachableModifyRequest";

        $xmlInput = xmlHeader($sessionid, $ociRequest);
        $xmlInput .= "<userId>" . $userId . "</userId>";
        $xmlInput .= "<isActive>" . $isActive . "</isActive>";
        $xmlInput .= $phNumber == "" ?    "<forwardToPhoneNumber xsi:nil=\"true\" />" :
            "<forwardToPhoneNumber>" . $phNumber . "</forwardToPhoneNumber>";

        // Ring Splash only for CFA
        if ($serviceKey == Bulk_CFA && $ringSplashActive != "") {
            $xmlInput .= "<isRingSplashActive>" . $ringSplashActive . "</isRingSplashActive>";
        }

        $xmlInput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlInput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        readError($xml);

        // Log changes into Change Log Database
        $phoneNumberLog = $phNumber == "" ? "None" : $phNumber;
        $userLastFirstName = getUserName($userId);
        $serviceName = $serviceKey == Bulk_CFA ? "Call Forwarding Always" : "Call Forwarding Not Reachable";
        $serviceAbbr = $serviceKey == Bulk_CFA ? "CFA" : "CFNR";

        createChangeLogEntry($userLastFirstName);
        createUserModificationLogEntry($userId, $userLastFirstName, $serviceName . " Active", $isActive);
        createUserModificationLogEntry($userId, $userLastFirstName, $serviceAbbr . ":Fwd Number", $phoneNumberLog);
        if ($serviceKey == Bulk_CFA) {
            createUserModificationLogEntry($userId, $userLastFirstName, $serviceAbbr . ":Ring Reminder", $ringSplashActive);
        }
    }


    // Back-end modification of User Authentication Service
    // Parameters: modification data: User ID, User Name, Password
    // -----------------------------------------------------------
    function resetAuthenticationPassword($data)
    {
        global $sessionid, $client;
        $userId = $data["userId"];
        $userName = $data["userName"];
        $password = $data["newPassword"];
        $viewedPassword = str_repeat("*", strlen($password));

        $xmlInput = xmlHeader($sessionid, "UserAuthenticationModifyRequest");
        $xmlInput .= "<userId>" . $userId . "</userId>";
        $xmlInput .= "<userName>" . $userName . "</userName>";
        $xmlInput .= "<newPassword>" . $password . "</newPassword>";

        $xmlInput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlInput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        readError($xml);

        $userLastFirstName = getUserName($userId);

        // Log changes into Change Log database
        createChangeLogEntry($userLastFirstName);
        createUserModificationLogEntry($userId, $userLastFirstName, "Authentication User Name", $userId);
        createUserModificationLogEntry($userId, $userLastFirstName, "Authentication Password", $viewedPassword);
    }


    // Back-end Authorization and Unahthorization of User Services and Service Packs
    // Parameters: modification data: User ID, User Service Name, authorization/unauthorization flag
    // of User Service, Service Pack Name, and authorization/unauthorization flag of Service Pack
    // ---------------------------------------------------------------------------------------------
    function authorizeServicePacks($data)
    {
        global $sessionid, $client;

        $userId = $data["userId"];
        $serviceName = $data["serviceName"];
        $userServiceAssignmentFlag = $data["assignServiceName"];
        $servicePackName = $data["servicePackName"];
        $servicePackAssignmentFlag = $data["assignServicePack"];

        $servicePackAssignmentRequest = getServiceAssignmentRequest($servicePackName, $servicePackAssignmentFlag);
        $userServiceAssignmentRequest = getServiceAssignmentRequest($serviceName, $userServiceAssignmentFlag);

        $servicePackProcessed = false;
        $userLastFirstName = getUserName($userId);

        // Log changes into Change Log Database
        createChangeLogEntry($userLastFirstName);

        if ($serviceName != "") {
            // Either user service assignment or unassignment will be processed
            $xmlInput = xmlHeader($sessionid, $userServiceAssignmentRequest);
            $xmlInput .= "<userId>" . $userId . "</userId>";
            $xmlInput .= "<serviceName>" . $serviceName . "</serviceName>";

            // log entry
            $action = $userServiceAssignmentRequest == "UserServiceAssignListRequest" ?
                "Assign User Service" : "Unassign User Service";
            createUserModificationLogEntry($userId, $userLastFirstName, $action, $serviceName);

            // Service Pack assignment/unassignment will be processed together with user service
            // assignment/unassignment only if both require the same operation - assignment or unassignment
            if ($servicePackName != "" && $servicePackAssignmentRequest == $userServiceAssignmentRequest) {
                $xmlInput .= "<servicePackName>" . $servicePackName . "</servicePackName>";
                $servicePackProcessed = true;

                // log entry
                $action = $servicePackAssignmentRequest == "UserServiceAssignListRequest" ?
                    "Assign Service Pack" : "Unassign Service Pack";
                createUserModificationLogEntry($userId, $userLastFirstName, $action, $servicePackName);
            }
            $xmlInput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlInput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            readError($xml);
        }

        if ($servicePackName != "" && ! $servicePackProcessed) {
            // log entry
            $action = $servicePackAssignmentRequest == "UserServiceAssignListRequest" ?
                "Assign Service Pack" : "Unassign Service Pack";
            createUserModificationLogEntry($userId, $userLastFirstName, $action, $servicePackName);

            $xmlInput = xmlHeader($sessionid, $servicePackAssignmentRequest);
            $xmlInput .= "<userId>" . $userId . "</userId>";
            $xmlInput .= "<servicePackName>" . $servicePackName . "</servicePackName>";

            $xmlInput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlInput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            readError($xml);
        }
    }


    // Display Title of modification operation
    // ---------------------------------------
    function displayTitle()
    {
        switch ($_POST["mode"]) {
            case Bulk_CFA:         echo "User service 'Call Forwarding Always' has been modified for the following users:"; break;
            case Bulk_CFNR:        echo "User service 'Call Forwarding Not Reachable' has been modified for the following users:"; break;
            case Bulk_UsrAuth:     echo "User Authentication passwords have been reset for the following users:"; break;
            case Bulk_AssignSwPck: echo "User Services and/or Service Packs have been assigned/unassigned for the following users:"; break;
        }
    }


    // Create confirmation table header (derived from posted Validation form)
    // ----------------------------------------------------------------------
    function createHeader()
    {
        $header = "<tr>";
        foreach ($_POST["header"] as $key => $name) {
            $header .= "<th style=\"width: 10%; font-weight: bold\">" . $name . "</th>";
        }
        $header .= "</tr>";

        echo $header;
    }


    // Process users modifications derived from CSV file. The function will:
    // - issue request on the backend to modify users
    // - echoes confirmation table
    // ---------------------------------------------------------------------
    function processModifications()
    {
        foreach ($_POST["data"] as $row) {
            switch ($_POST["mode"]) {
                case Bulk_CFA:         modifyCallForwarding($row, Bulk_CFA); break;
                case Bulk_CFNR:        modifyCallForwarding($row, Bulk_CFNR); break;
                case Bulk_UsrAuth:     resetAuthenticationPassword($row); break;
                case Bulk_AssignSwPck: authorizeServicePacks($row); break;
            }

            $table = "<tr>";
            foreach ($row as $value) {
                $table .= "<td>" . $value . "</td>";
            }
            $table .= "</tr>";
            echo $table;
        }
    }
?>

<div style="font-size: 12px" class="formPadding">
    <div style="font-size: 16px; font-weight: bolder; text-align: center">
        <?php displayTitle(); ?>
    </div>
    <table border="1" align="center" class="bulkModifyTable bulkUserTable table" style="width:96%;">
        <tr><td colspan=\"100%\">&nbsp;</td></tr>
        <tr><td colspan=\"100%\">&nbsp;</td></tr>
        <?php createHeader(); ?>
        <?php processModifications(); ?>
    </table>
</div>
