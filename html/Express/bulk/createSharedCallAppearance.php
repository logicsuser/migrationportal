<?php
/**
 * Created by Karl.
 * Date: 10/12/2016
 */
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();

    require_once("/var/www/html/Express/bulk/SCAManager.php");

    $scaManager = new SCAManager("csvUsers");
?>

<div class="formPadding">
    The following Shared Call Appearances have been created:<br><br>
    <table border="1" align="center" class="bulkSCATable" style="width:96%;">
        <tr>
            <?php echo $scaManager->createTableHeader(); ?>
        </tr>
        <?php echo $scaManager->processBulk(); ?>
    </table>
</div>
