<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	require_once("/var/www/lib/broadsoft/adminPortal/GroupCallPickupUsers.php");
	require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");

    const customProfileTable = "customProfiles";
    const customProfileName  = "customProfileName";
    const customTagName      = "customTagName";
    const customTagValue     = "customTagValue";
    const customProfileType  = "deviceType";

    const ExpressCustomProfileName = "%Express_Custom_Profile_Name%";

    function addDeviceCustomTag($deviceName, $tagName, $tagValue) {
        global $sessionid, $client;

        $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagAddRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= "<tagName>" . $tagName . "</tagName>";
        $xmlinput .= "<tagValue>" . $tagValue . "</tagValue>";

        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $errMsg = "";
        if (readError($xml) != "") {
            $errMsg = "Failed to add custom tag to device " . $deviceName . " .";
            $errMsg .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
            if ($xml->command->detail)
            {
                $errMsg .= "%0D%0A" . strval($xml->command->detail);
            }
        }
        return $errMsg;
    }


    function addDeviceCustomTags($deviceType, $deviceName, $customProfile) {
        global $db;

        // No custom tags can be selected while building a user
        // ----------------------------------------------------
        if ($customProfile == "") { return ""; }
        if ($deviceType == "") { return ""; }

        // get custom tags from database for a specific custom profile
        // and add them to the device
        // -----------------------------------------------------------
        $query = "select " . customTagName . ", " . customTagValue . " from " . customProfileTable . " where " . customProfileType . "='" . $deviceType . "' and " . customProfileName . "='" . $customProfile . "'";
        $results = $db->query($query);

        //$customTags = array();
        while ($row = $results->fetch()) {
            if (($errMsg = addDeviceCustomTag($deviceName, $row[customTagName], $row[customTagValue])) != "") {
                return $errMsg;
            }
        }

        // finally add custom tag with the custom profile name for the reporting purpose
        if (($errMsg = addDeviceCustomTag($deviceName, ExpressCustomProfileName, $customProfile)) != "") {
            return $errMsg;
        }

        return "";
    }

    /**
     * Get resource value that can be used to build Surgemail name
     * @param $attr     - resource type
     * @param $value    - user information from users bulk list
     * @return string   - requested resource value
     */
    function getResourceForSurgeMailNameCreation($attr, $value) {
        global $surgemailDomain;

        switch ($attr) {
            case ResourceNameBuilder::DN:               return isset($value["phoneNumber"]) && $value["phoneNumber"] != "" ? $value["phoneNumber"] : "";
            case ResourceNameBuilder::EXT:              return isset($value["extension"]) && $value["extension"] != "" ? $value["extension"] : "";
            case ResourceNameBuilder::USR_CLID:         return isset($value["callingLineIdPhoneNumber"]) && $value["callingLineIdPhoneNumber"] != "" ? $value["callingLineIdPhoneNumber"] : '';
            case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:           return str_replace(" ", "_", trim($_SESSION["groupId"]));
            case ResourceNameBuilder::SYS_DFLT_DOMAIN:  return $_SESSION["systemDefaultDomain"];
            case ResourceNameBuilder::SRG_DOMAIN:       return $surgemailDomain;
            case ResourceNameBuilder::FIRST_NAME:       return $value["firstName"];
            case ResourceNameBuilder::LAST_NAME:        return $value["lastName"];
            case ResourceNameBuilder::DEV_NAME:         return isset($value["deviceName"]) && $value["deviceName"] != "" ? $value["deviceName"] : "";
            case ResourceNameBuilder::MAC_ADDR:         return isset($value["macAddress"]) && $value["macAddress"] != "" ? $value["macAddress"] : "";
            case ResourceNameBuilder::USR_ID:
                $name = $value["userId"];
                if ($name == "") {
                    return "";
                }
                $nameSplit = explode("@", $name);
                return $nameSplit[0];
                break;
            default: return "";
        }
    }

    /**
     * Get resource value that can be used to build Voice Mail password
     * @param $attr     - resource type
     * @param $value    - user information from users bulk list
     * @return string   - requested resource value
     */
    function getResourceForVoicePasswordCreation($attr, $value) {
        switch ($attr) {
            case ResourceNameBuilder::DN:       return isset($value["phoneNumber"]) && $value["phoneNumber"] != "" ? $value["phoneNumber"] : "";
            case ResourceNameBuilder::EXT:      return isset($value["extension"]) && $value["extension"] != "" ? $value["extension"] : "";
            case ResourceNameBuilder::USR_CLID: return isset($value["callingLineIdPhoneNumber"]) && $value["callingLineIdPhoneNumber"] != "" ? $value["callingLineIdPhoneNumber"] : '';
            case ResourceNameBuilder::GRP_DN:   return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:   return $_SESSION["groupId"];
            case ResourceNameBuilder::MAC_ADDR: return isset($value["macAddress"]) && $value["macAddress"] != "" ? $value["macAddress"] : "";
        }
    }

?>
<div class="formPadding">
	The following users have been added:<br><br>
	<table border="1" align="center" class="bulkUserTable table" style="width:200%;">
		<tr>
			<th style="width:4%;">First Name</th>
			<th style="width:4%;">Last Name</th>
			<th style="width:4%;">Calling Line ID First Name</th>
			<th style="width:4%;">Calling Line ID Last Name</th>
			<th style="width:4%;">Phone Number</th>
			<th style="width:4%;">Calling Line ID Phone Number</th>
			<th style="width:4%;">Extension</th>
			<th style="width:4%;">Department</th>
			<th style="width:4%;">Address</th>
			<th style="width:4%;">Suite</th>
			<th style="width:4%;">City</th>
			<th style="width:4%;">State/Province</th>
			<th style="width:4%;">Zip/Postal Code</th>
			<th style="width:4%;">Time Zone</th>
            <th style="width:4%;">Network Class of Service</th>
			<th style="width:4%;">Language</th>
			<th style="width:4%;">Email Address</th>
			<th style="width:4%;">Service Pack</th>
			<th style="width:4%;">Device Type</th>
			<th style="width:4%;">MAC Address</th>
			<th style="width:4%;">Voice Messaging</th>
			<th style="width:4%;">Polycom Phone Services</th>
			<th style="width:4%;">Device Access User Name</th>
			<th style="width:4%;">Device Access Password</th>
			<th style="width:4%;">Calling Line ID Policy</th>
			<th style="width:4%;">Voice Mail Password</th>
			<th style="width:4%;">Web Password</th>
			<th style="width:4%;">Call Pickup Group</th>
		</tr>
		<?php

            require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
            require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailNameBuilder.php");
            require_once("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
            require_once("/var/www/lib/broadsoft/adminPortal/getAllDevices.php");
            
            foreach ($_POST["users"] as $key => $value)
			{
				$initialPass = setWebPassword((int)$_SESSION["groupRules"]["minPasswordLength"]);

                // Build Voice Mail Password. Enforce fallback
                // ----------------------------------------------------------------------------------------------------
                if ($bwVoicePortalPasswordType == "Formula") {
                    require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
                    require_once("/var/www/lib/broadsoft/adminPortal/VoicePasswordNameBuilder.php");

                    $voicePasswordBuilder = new VoicePasswordNameBuilder($userVoicePortalPasscodeFormula,
                        getResourceForVoicePasswordCreation(ResourceNameBuilder::DN, $value),
                        getResourceForVoicePasswordCreation(ResourceNameBuilder::EXT, $value),
                        getResourceForVoicePasswordCreation(ResourceNameBuilder::USR_CLID, $value),
                        getResourceForVoicePasswordCreation(ResourceNameBuilder::GRP_DN, $value),
                        true);

                    if (! $voicePasswordBuilder->validate()) {
                        // TODO: Implement validation resolution
                    }

                    do {
                        $attr = $voicePasswordBuilder->getRequiredAttributeKey();
                        $voicePasswordBuilder->setRequiredAttribute($attr, getResourceForVoicePasswordCreation($attr, $value));
                    } while ($attr != "");

                    $portalPass = $voicePasswordBuilder->getName();
                } else {
                    $portalPass = setVoicePassword();
                }
                
                if(is_numeric($portalPass)){
                    $portalPass = $portalPass;
                }else{
                    $criteria = $userVoicePortalPasscodeFormula;
                    preg_match('#\((.*?)\)#', $criteria, $match);
                    $var = $match[1];
                    $portalPass = "";
                    for($i = 1; $i<= $var; $i++){
                        $portalPass .= $i;
                    }
                }

				$AuthPass = setAuthPassword();
				$emailPass = setEmailPassword();
                $hasDeviceType = $value["deviceType"] != "";

                // Create surgemail username from the first formula.
                // Enforce fallback if there is no second criteria/formula.
                // -------------------------------------------------------------
                $surgemailName = "";
                $surgeMailNameBuilder1 = new SurgeMailNameBuilder($surgeMailIdCriteria1,
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::DN, $value),
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::EXT, $value),
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::USR_CLID, $value),
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::GRP_DN, $value),
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $value),
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::FIRST_NAME, $value),
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::LAST_NAME, $value),
                    $surgeMailIdCriteria2 == "");    // force fallback only if second criteria does not exist

                if (! $surgeMailNameBuilder1->validate()) {
                    // TODO: Implement resolution when User ID input formula has errors
                }

                do {
                    $attr = $surgeMailNameBuilder1->getRequiredAttributeKey();
                    $surgeMailNameBuilder1->setRequiredAttribute($attr, getResourceForSurgeMailNameCreation($attr, $value));
                } while ($attr != "");

                $surgemailName = $surgeMailNameBuilder1->getName();
                if ($surgemailName == "") {
                    $surgeMailNameBuilder2 = new SurgeMailNameBuilder($surgeMailIdCriteria2,
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::DN, $value),
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::EXT, $value),
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::USR_CLID, $value),
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::GRP_DN, $value),
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $value),
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::FIRST_NAME, $value),
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::LAST_NAME, $value),
                        true);

                    if (! $surgeMailNameBuilder2->validate()) {
                        // TODO: Implement resolution when User ID input formula has errors
                    }

                    do {
                        $attr = $surgeMailNameBuilder2->getRequiredAttributeKey();
                        $surgeMailNameBuilder2->setRequiredAttribute($attr, getResourceForSurgeMailNameCreation($attr, $value));
                    } while ($attr != "");

                    $surgemailName = $surgeMailNameBuilder2->getName();
                }

                $emailAddress = $surgemailName;

				//Build Device
                $sipGateway = false;

				if ($hasDeviceType)
				{
                    // For analog users check whether a device (analog gateway) has already been provisioned
                    $sipGatewayLookup = new DBLookup($db, "devices", "deviceName", "sipGateway");
                    $sipGateway = $sipGatewayLookup->get($value["deviceType"]) == "true";
                    if (! $sipGateway || ! in_array($value["deviceName"], $devices)) {
                        $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceAddRequest14");
                        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
                        $xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
                        $xmlinput .= "<deviceName>" . $value["deviceName"] . "</deviceName>";
                        $xmlinput .= "<deviceType>" . $value["deviceType"] . "</deviceType>";
                        if ($value["macAddress"] !== "")
                        {
                            $xmlinput .= "<macAddress>" . $value["macAddress"] . "</macAddress>";
                        }
                        $xmlinput .= xmlFooter();
                        $response = $client->processOCIMessage(array("in0" => $xmlinput));
                        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

                        // Add Device Custom Tags (user level)
                        addDeviceCustomTags($value["deviceType"], $value["deviceName"], $value["customProfile"]);

                        // Rebuild and reset device after custom tags are added
                        if ($_POST["customProfile"] != "") {
                            rebuildAndResetDeviceConfig($_SESSION["devName"], "rebuildAndReset");
                        }
                    }
				}

				//Build User
				$xmlinput = xmlHeader($sessionid, "UserAddRequest17sp4");
				$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
				$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
				$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
				$xmlinput .= "<lastName>" . $value["lastName"] . "</lastName>";
				$xmlinput .= "<firstName>" . $value["firstName"] . "</firstName>";
				$xmlinput .= "<callingLineIdLastName>" . $value["callingLineIdLastName"] . "</callingLineIdLastName>";
				$xmlinput .= "<callingLineIdFirstName>" . $value["callingLineIdFirstName"] . "</callingLineIdFirstName>";
				if (strlen($value["phoneNumber"]) > 0)
				{
					$xmlinput .= "<phoneNumber>" . $value["phoneNumber"] . "</phoneNumber>";
				}
				$xmlinput .= "<extension>" . $value["extension"] . "</extension>";
				if (strlen($value["callingLineIdPhoneNumber"]) > 0)
				{
					$xmlinput .= "<callingLineIdPhoneNumber>" . $value["callingLineIdPhoneNumber"] . "</callingLineIdPhoneNumber>";
				}
				$xmlinput .= "<password>" . $initialPass . "</password>";
				if (strlen($value["department"]) > 0)
				{
					$xmlinput .= "<department xsi:type=\"GroupDepartmentKey\">
								<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>
								<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>
								<name>" . $value["department"] . "</name>
							</department>";
				}
				if (strlen($value["language"]) > 0)
				{
					$xmlinput .= "<language>" . $value["language"] . "</language>";
				}
				$xmlinput .= "<timeZone>" . $value["timeZone"] . "</timeZone>";
				if ($hasDeviceType)
				{
                    $xmlinput .= "<accessDeviceEndpoint>";
                    $xmlinput .=    "<accessDevice>";
                    $xmlinput .=        "<deviceLevel>Group</deviceLevel>";
                    $xmlinput .=        "<deviceName>" . $value["deviceName"] . "</deviceName>";
                    $xmlinput .=    "</accessDevice>";
                    $xmlinput .=    "<linePort>" . $value["linePort"] . "</linePort>";
                    if ($sipGateway) {
                        $xmlinput .= "<portNumber>" . $value["portNumber"] . "</portNumber>";
                    }
                    $xmlinput .= "</accessDeviceEndpoint>";
				}
				if (strlen($value["emailAddress"]) > 0)
				{
					$xmlinput .= "<emailAddress>" . $value["emailAddress"] . "</emailAddress>";
				}
				if (strlen($value["addressLine1"]) > 0 or strlen($value["addressLine2"]) > 0 or strlen($value["city"]) > 0 or strlen($value["state"]) > 0 or strlen($value["zip"]) > 0)
				{
					$xmlinput .= "<address>";
					if (strlen($value["addressLine1"]) > 0)
					{
						$xmlinput .= "<addressLine1>" . $value["addressLine1"] . "</addressLine1>";
					}
					if (strlen($value["addressLine2"]) > 0)
					{
						$xmlinput .= "<addressLine2>" . $value["addressLine2"] . "</addressLine2>";
					}
					if (strlen($value["city"]) > 0)
					{
						$xmlinput .= "<city>" . $value["city"] . "</city>";
					}
					if (strlen($value["state"]) > 0)
					{
						$xmlinput .= "<stateOrProvince>" . $value["state"] . "</stateOrProvince>";
					}
					if (strlen($value["zip"]) > 0)
					{
						$xmlinput .= "<zipOrPostalCode>" . $value["zip"] . "</zipOrPostalCode>";
					}
					$xmlinput .= "</address>";
				}
                if (strlen($value["networkClassOfService"]) > 0) {
                    $xmlinput .= "<networkClassOfService>" . $value["networkClassOfService"] . "</networkClassOfService>";
                }
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
				if (count($xml->command->attributes()) > 0)
				{
					foreach ($xml->command->attributes() as $a => $b)
					{
						if ($b == "Error")
						{
							$data = "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
							if ($xml->command->detail)
							{
								$data .= "%0D%0A" . strval($xml->command->detail);
							}
						}else{
							if ($value["phoneNumber"]!== "")
							{
									$dnPostArray['phoneNumber'][0] = $value["phoneNumber"];
									$dnPostArray['groupId'] = $_SESSION["groupId"];
									require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
									$dnsObj = new Dns();
									$dnsActiveResponse = $dnsObj->activateDNRequest($dnPostArray);
									
							}
						}
						
					}
				}
				//readError($xml);

				//set call processing policy
				$xmlinput = xmlHeader($sessionid, "UserCallProcessingModifyPolicyRequest14sp7");
				$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
				$xmlinput .= "<useUserCLIDSetting>" . (strtoupper($value["callingLineIdPolicy"]) == "USER" ? "true" : "false") . "</useUserCLIDSetting>";
				$xmlinput .= "<clidPolicy>" . (strtoupper($value["callingLineIdPolicy"]) == "USER" ? "Use Configurable CLID" : "Use DN") . "</clidPolicy>";
				$xmlinput .= "<emergencyClidPolicy>" . (strtoupper($value["callingLineIdPolicy"]) == "USER" ? "Use Configurable CLID" : "Use DN") . "</emergencyClidPolicy>";
				$xmlinput .= "<useGroupName>true</useGroupName>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
				readError($xml);

				//Assign Service Pack
				$xmlinput = xmlHeader($sessionid, "UserServiceAssignListRequest");
				$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
				//$xmlinput .= "<serviceName>Speed Dial 100</serviceName>";
				//$xmlinput .= "<serviceName>Speed Dial 8</serviceName>";
				$servicePackArray = explode(";", $value["servicePack"]);
				for ($i = 0; $i < count($servicePackArray); $i++)
				{
					$xmlinput .= "<servicePackName>" . htmlspecialchars($servicePackArray[$i]) . "</servicePackName>";
				}
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

				//Build BLF URI
				$xmlinput = xmlHeader($sessionid, "UserBusyLampFieldModifyRequest");
				$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
                $xmlinput .= "<listURI>" . $value["blfUser"] . "</listURI>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

				if ($hasDeviceType)
				{
                    //Set Primary Line
					$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyUserRequest");
					$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
					$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
					$xmlinput .= "<deviceName>" . $value["deviceName"] . "</deviceName>";
					$xmlinput .= "<linePort>" . $value["linePort"] . "</linePort>";
					$xmlinput .= "<isPrimaryLinePort>true</isPrimaryLinePort>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

                    //Set PLCM password
                    if (strstr($value["deviceType"], " "))
                    {
                        $substrMenu = explode(" ", $value["deviceType"]);
                    }
                    else if (strstr($value["deviceType"], "_"))
                    {
                        $substrMenu = explode("_", $value["deviceType"]);
                    }
                    else
                    {
                        $substrMenu = explode("-", $value["deviceType"]);
                    }
                    $substrMenu = $substrMenu[0];

                    $deviceAccessUserName = $value["derivedDeviceAccessUserName"];
                    $deviceAccessPassword = $value["derivedDeviceAccessPassword"];

                    if ($value["deviceAccessUserName"] !== "" and $value["deviceAccessPassword"] !== "")
                    {
                        $deviceAccessUserName = $value["deviceAccessUserName"];
                        $deviceAccessPassword = $value["deviceAccessPassword"];
                    }
                    /*
                    else
                    {
                        $select = "SELECT customCredentials, username, password from customCredentials where deviceManufacturer='" . $substrMenu . "'";
                        $result = $db->query($select);
                        while ($row = $result->fetch())
                        {
                            if ($row["customCredentials"] == "y")
                            {
                                $deviceAccessUserName = $row["username"];
                                $deviceAccessPassword = $row["password"];
                            }
                        }
                    }
                     */

                    if (isset($deviceAccessUserName) and isset($deviceAccessPassword))
                    {
                        $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyRequest14");
                        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
                        $xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
                        $xmlinput .= "<deviceName>" . $value["deviceName"] . "</deviceName>";

                        if ($sipGateway  && $analogAccessAuthentication) {
                            $xmlinput .= "<useCustomUserNamePassword>false</useCustomUserNamePassword>";
                        }
                        else {
                            $xmlinput .= "<useCustomUserNamePassword>true</useCustomUserNamePassword>";
                            $xmlinput .= "<accessDeviceCredentials>
								<userName>" . $deviceAccessUserName . "</userName>
								<password>" . $deviceAccessPassword . "</password>
							</accessDeviceCredentials>";
                        }
                        $xmlinput .= xmlFooter();
                        $response = $client->processOCIMessage(array("in0" => $xmlinput));
                        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                    }
				}

				if (strtoupper($value["voiceMessaging"]) == "YES")
                {
                    //Build front end VM
                    $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
                    $xmlinput .= "<userId>" . $value["userId"] . "</userId>";
                    $xmlinput .= "<isActive>true</isActive>";
                    $xmlinput .= xmlFooter();
                    $response = $client->processOCIMessage(array("in0" => $xmlinput));
                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                    //Build VM password
                    $xmlinput = xmlHeader($sessionid, "UserPortalPasscodeModifyRequest");
                    $xmlinput .= "<userId>" . $value["userId"] . "</userId>";
                    $xmlinput .= "<newPasscode>" . $portalPass . "</newPasscode>";
                    $xmlinput .= xmlFooter();
                    $response = $client->processOCIMessage(array("in0" => $xmlinput));
                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

                    //Configure email account
                    $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyAdvancedVoiceManagementRequest");
                    $xmlinput .= "<userId>" . $value["userId"] . "</userId>";
                    $xmlinput .= "<groupMailServerEmailAddress>" . $emailAddress . "</groupMailServerEmailAddress>";
                    $xmlinput .= "<groupMailServerUserId>" . $emailAddress . "</groupMailServerUserId>";
                    $xmlinput .= "<groupMailServerPassword>" . $emailPass . "</groupMailServerPassword>";
                    $xmlinput .= xmlFooter();
                    $response = $client->processOCIMessage(array("in0" => $xmlinput));
                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

                    $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
                    $xmlinput .= "<userId>" . $value["userId"] . "</userId>";
                    $xmlinput .= "<isActive>true</isActive>";
                    $xmlinput .= xmlFooter();
                    $response = $client->processOCIMessage(array("in0" => $xmlinput));
                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

                     //Build SurgeMail
                    $surgemailPost = array ("cmd" => "cmd_user_login",
                        "lcmd" => "user_delete",
                        "show" => "simple_msg.xml",
                        "username" => $surgemailUsername,
                        "password" => $surgemailPassword,
                        "lusername" => $emailAddress,
                        "lpassword" => $emailPass,
                        "uid" => isset($userid) ? $userid : "");
                    $result = http_post_fields($surgemailURL, $surgemailPost);

                    $surgemailPost = array ("cmd" => "cmd_user_login",
                        "lcmd" => "user_create",
                        "show" => "simple_msg.xml",
                        "username" => $surgemailUsername,
                        "password" => $surgemailPassword,
                        "lusername" => $emailAddress,
                        "lpassword" => $emailPass,
                        "uid" => isset($userid) ? $userid : "");
                    $result = http_post_fields($surgemailURL, $surgemailPost);
                }
				if (strtoupper($value["polycomPhoneServices"]) == "YES")
				{
					//Polycom Services
					$xmlinput = xmlHeader($sessionid, "UserServiceIsAssignedRequest");
					$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
					$xmlinput .= "<serviceName>Polycom Phone Services</serviceName>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

					if (strval($xml->command->isAssigned) == "true" and $substrMenu == "Polycom")
					{
						//retrieve custom contact directories
						$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryGetListRequest");
						$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
						$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
						$xmlinput .= xmlFooter();
						$response = $client->processOCIMessage(array("in0" => $xmlinput));
						$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

						$ccdList = "";
						foreach ($xml->command->name as $k => $v)
						{
							$ccdList = "<groupCustomContactDirectory>" . strval($v) . "</groupCustomContactDirectory>";

							//retrieve list of users from custom contact directory and append new user to list
							$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryGetRequest17");
							$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
							$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
							$xmlinput .= "<name>" . htmlspecialchars(strval($v)) . "</name>";
							$xmlinput .= xmlFooter();
							$response = $client->processOCIMessage(array("in0" => $xmlinput));
							$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

							$ccdUsersList = "";
							foreach ($xml->command->userTable->row as $kk => $vv)
							{
								$ccdUsersList .= "<entry><userId>" . strval($vv->col[0]) . "</userId></entry>";
							}
							$ccdUsersList .= "<entry><userId>" . $value["userId"] . "</userId></entry>";

							$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryModifyRequest17");
							$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
							$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
							$xmlinput .= "<name>" . htmlspecialchars(strval($v)) . "</name>";
							$xmlinput .= "<entryList>" . $ccdUsersList . "</entryList>";
							$xmlinput .= xmlFooter();
							$response = $client->processOCIMessage(array("in0" => $xmlinput));
							$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
						}

						$xmlinput = xmlHeader($sessionid, "UserPolycomPhoneServicesModifyRequest");
						$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
						$xmlinput .= "<accessDevice>
									<deviceLevel>Group</deviceLevel>
									<deviceName>" . $value["deviceName"] . "</deviceName>
								</accessDevice>";
						$xmlinput .= "<integratePhoneDirectoryWithBroadWorks>true</integratePhoneDirectoryWithBroadWorks>";
						$xmlinput .= "<includeUserPersonalPhoneListInDirectory>true</includeUserPersonalPhoneListInDirectory>";
						$xmlinput .= "<includeGroupCustomContactDirectoryInDirectory>true</includeGroupCustomContactDirectoryInDirectory>";
						$xmlinput .= $ccdList;
						$xmlinput .= xmlFooter();
						$response = $client->processOCIMessage(array("in0" => $xmlinput));
						$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
					}
				}


				//Auth Settings
				$xmlinput = xmlHeader($sessionid, "UserAuthenticationModifyRequest");
				$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
				$xmlinput .= "<userName>" . $value["deviceName"] . "</userName>";
				$xmlinput .= "<newPassword>" . $AuthPass . "</newPassword>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

                //Turn off international
                /*if ($usrAllowInternationalCalls == "false") {
                    $xmlinput = xmlHeader($sessionid, "UserOutgoingCallingPlanOriginatingModifyRequest");
                    $xmlinput .= "<userId>" . $value["userId"] . "</userId>";
                    $xmlinput .= "<useCustomSettings>true</useCustomSettings>";
                    $xmlinput .= "<userPermissions>";
                    $xmlinput .= "<international>Disallow</international>";
                    $xmlinput .= "</userPermissions>";
                    $xmlinput .= xmlFooter();
                    $response = $client->processOCIMessage(array("in0" => $xmlinput));
                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                }*/

                //Assigning Call Pickup Group to Users
                if(!empty($value["callPickupGroup"])){
                	$Obj = new GroupCallPickupUsers($value["callPickupGroup"]);
                	$gcpExistingUserinGroup = $Obj->getUsersInCallPickupGroup();
                	array_push($gcpExistingUserinGroup, $value["userId"]);
                	$status = $Obj->modifyUserCallPickupGroup($gcpExistingUserinGroup);
                	if($status == "Success"){
                		$Obj = new GroupCallPickupUsers($value["userId"]);
                		$gpName = $Obj->findCallPickupGroupByUsername();
                	}
                }

				/*
				------------------------- DONE WITH BUILD---------------------------------
				*/

				//Get basic user info
				if ($ociVersion == "17")
				{
					$xmlinput = xmlHeader($sessionid, "UserGetRequest17sp4");
				}
				else if ($ociVersion == "20")
				{
					$xmlinput = xmlHeader($sessionid, "UserGetRequest20");
				}
				else
				{
					$xmlinput = xmlHeader($sessionid, "UserGetRequest19");
				}
				$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

				$lastName = strval($xml->command->lastName);
				$firstName = strval($xml->command->firstName);
				$clidLast = strval($xml->command->callingLineIdLastName);
				$clidFirst = strval($xml->command->callingLineIdFirstName);
				$ph = strval($xml->command->phoneNumber);
				$callingLineIdPhoneNumber = strval($xml->command->callingLineIdPhoneNumber);
				$ext = strval($xml->command->extension);
				if (isset($xml->command->department))
				{
					$dept = strval($xml->command->department->name);
				}
				else
				{
					$dept = "";
				}
//				$deviceName = strval($xml->command->accessDeviceEndpoint->accessDevice->deviceName);
//				$linePort = strval($xml->command->accessDeviceEndpoint->linePort);
				$emailAddress = strval($xml->command->emailAddress);
				if (isset($xml->command->address->addressLine1))
				{
					$address1 = strval($xml->command->address->addressLine1);
				}
				else
				{
					$address1 = "";
				}
				if (isset($xml->command->address->addressLine2))
				{
					$address2 = strval($xml->command->address->addressLine2);
				}
				else
				{
					$address2 = "";
				}
				if (isset($xml->command->address->city))
				{
					$city = strval($xml->command->address->city);
				}
				else
				{
					$city = "";
				}
				if (isset($xml->command->address->stateOrProvince))
				{
					$state = strval($xml->command->address->stateOrProvince);
				}
				else
				{
					$state = "";
				}
				if (isset($xml->command->address->zipOrPostalCode))
				{
					$zip = strval($xml->command->address->zipOrPostalCode);
				}
				else
				{
					$zip = "";
				}
				$timeZone = strval($xml->command->timeZone);
				if (isset($xml->command->language))
				{
					$language = strval($xml->command->language);
				}
				else
				{
					$language = "";
				}

                if (isset($deviceType))
                {
                    unset($deviceType);
                }
                if (isset($macAddress))
                {
                    unset($macAddress);
                }

				//Get device info
                if ($hasDeviceType)
                {
                    $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetRequest18sp1");
                    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
                    $xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
                    $xmlinput .= "<deviceName>" . $value["deviceName"] . "</deviceName>";
                    $xmlinput .= xmlFooter();
                    $response = $client->processOCIMessage(array("in0" => $xmlinput));
                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

                    $deviceType = strval($xml->command->deviceType);
                    $macAddress = strval($xml->command->macAddress);
                }

				$xmlinput = xmlHeader($sessionid, "UserServiceGetAssignmentListRequest");
				$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

				$servicePack = "";
				foreach ($xml->command->servicePacksAssignmentTable->row as $k => $v)
				{
					if ($v->col[1] == "true")
					{
						$servicePack[] = strval($v->col[0]);
					}
				}

				echo "<tr>
						<td>" . $firstName . "</td>
						<td>" . $lastName . "</td>
						<td>" . $clidFirst . "</td>
						<td>" . $clidLast . "</td>
						<td>" . $ph . "</td>
						<td>" . $callingLineIdPhoneNumber . "</td>
						<td>" . $ext . "</td>
						<td>" . $dept . "</td>
						<td>" . $address1 . "</td>
						<td>" . $address2 . "</td>
						<td>" . $city . "</td>
						<td>" . $state . "</td>
						<td>" . $zip . "</td>
						<td>" . $timeZone . "</td>
						<td>" . $value["networkClassOfService"] . "</td>
						<td>" . $language . "</td>
						<td>" . $emailAddress . "</td>
						<td>" . implode(", ", $servicePack) . "</td>
						<td>" . (isset($deviceType) ? $deviceType : "") . "</td>
						<td>" . (isset($macAddress) ? $macAddress : "") . "</td>
						<td>" . $value["voiceMessaging"] . "</td>
						<td>" . $value["polycomPhoneServices"] . "</td>
						<td>" . (isset($deviceAccessUserName) ? $deviceAccessUserName : "") . "</td>
						<td>" . (isset($deviceAccessPassword) ? $deviceAccessPassword : "") . "</td>
						<td>" . $value["callingLineIdPolicy"] . "</td>
						<td>" . (strtoupper($value["voiceMessaging"]) == "YES" ? $portalPass : "N/A") . "</td>
						<td>" . $initialPass . "</td>
						<td>" . $gpName . "</td>
					</tr>";
			}
		?>
	</table>
</div>
