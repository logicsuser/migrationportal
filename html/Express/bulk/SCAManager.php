<?php

/**
 * Created by Karl.
 * Date: 10/12/2016
 */
class SCAManager
{
    private $keys = array();
    private $headTitles = array();
    private $numColumns = 0;
    private $postName = "";
    private $backEndErrorMsg = "";

    private $lastUserId = "";
    private $lastAlertAllAppearancesForClickToDialCalls = "";
    private $lastAlertAllAppearancesForGroupPagingCalls = "";
    private $lastAllowSCACallRetrieve = "";
    private $lastMultipleCallArrangementIsActive = "";
    private $lastAllowBridgingBetweenLocations = "";
    private $lastBridgeWarningTone = "";
    private $lastEnableCallParkNotification = "";

    private static function userId()                                    { return "userId"; }
    private static function deviceName()                                { return "deviceName"; }
    private static function linePort()                                  { return "linePort"; }
    private static function isActive()                                  { return "isActive"; }
    private static function allowOrigination()                          { return "allowOrigination"; }
    private static function allowTermination()                          { return "allowTermination"; }
    private static function alertAllAppearancesForClickToDialCalls()    { return "alertAllAppearancesForClickToDialCalls"; }
    private static function alertAllAppearancesForGroupPagingCalls()    { return "alertAllAppearancesForGroupPagingCalls"; }
    private static function allowSCACallRetrieve()                      { return "allowSCACallRetrieve"; }
    private static function multipleCallArrangementIsActive()           { return "multipleCallArrangementIsActive"; }
    private static function enableCallParkNotification()                { return "enableCallParkNotification"; }
    private static function allowBridgingBetweenLocations()             { return "allowBridgingBetweenLocations"; }
    private static function bridgeWarningTone()                         { return "bridgeWarningTone"; }

    public function __construct($postName) {

        $this->postName = $postName;

        $this->addAttribute(self::userId(),                                 "Primary User ID");
        $this->addAttribute(self::deviceName(),                             "SCA Device Name");
        $this->addAttribute(self::linePort(),                               "SCA Lineport");
        $this->addAttribute(self::isActive(),                               "Active");
        $this->addAttribute(self::allowOrigination(),                       "Origination");
        $this->addAttribute(self::allowTermination(),                       "Termination");
        $this->addAttribute(self::alertAllAppearancesForClickToDialCalls(), "Alert Click to Dial");
        $this->addAttribute(self::alertAllAppearancesForGroupPagingCalls(), "Alert Group Paging");
        $this->addAttribute(self::allowSCACallRetrieve(),                   "Allow Call Retrieve");
        $this->addAttribute(self::multipleCallArrangementIsActive(),        "MCA");
        $this->addAttribute(self::enableCallParkNotification(),             "Call Park");
        $this->addAttribute(self::allowBridgingBetweenLocations(),          "Bridging");
        $this->addAttribute(self::bridgeWarningTone(),                      "Bridge Warning Tone");
    }


    public function createTableHeader() {
        $header = "";
        $margin = $this->numColumns == 0 ? 100 : (int)(100 / $this->numColumns);

        for ($i = 0; $i < $this->numColumns; $i++) {
            $header .= "<th style=\"width:" . $margin . "%\">" . $this->headTitles[$i] . "</th>";
        }

        return $header;
    }


    public function processBulk() {
        $row = "";
        foreach ($_POST[$this->postName] as $key => $value ) {
            $row .= "<tr style=\"font-size: 12px\">";
            $this->executeAction($value);

            for ($i = 0; $i < $this->numColumns; $i++) {
                $row .= "<td>" . $value[$this->keys[$i]] . "</td>";
            }

            $row .= "</tr>";

            if ($this->backEndErrorMsg != "") {
                break;
            }
        }

        return $row;
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function executeAction($value) {
        global $sessionid, $client;

        // Add User Shared Call Appearance Endpoint
        $xmlinput  = xmlHeader($sessionid, "UserSharedCallAppearanceAddEndpointRequest14sp2");
        $xmlinput .= "<userId>" . $value[self::userId()] . "</userId>";
        $xmlinput .= "<accessDeviceEndpoint>";
        $xmlinput .=    "<accessDevice>";
        $xmlinput .=        "<deviceLevel>Group</deviceLevel>";
        $xmlinput .=        "<deviceName>" . $value[self::deviceName()] . "</deviceName>";
        $xmlinput .=    "</accessDevice>";
        $xmlinput .=    "<linePort>" . $value[self::linePort()] . "</linePort>";
        $xmlinput .= "</accessDeviceEndpoint>";
        $xmlinput .= "<isActive>" . self::normalize(self::isActive(), $value[self::isActive()]) . "</isActive>";
        $xmlinput .= "<allowOrigination>" . self::normalize(self::allowOrigination(), $value[self::allowOrigination()]) . "</allowOrigination>";
        $xmlinput .= "<allowTermination>" . self::normalize(self::allowTermination(), $value[self::allowTermination()]) . "</allowTermination>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (readError($xml) != "") {
            $this->backEndErrorMsg = "OCI Error";
            return;
        }

        // Modify User Shared Call Appearance Service
        // but skip this process if modification has been performed before with the same parameters
        if ($value[self::userId()] == $this->lastUserId &&
            $value[self::alertAllAppearancesForClickToDialCalls()] == $this->lastAlertAllAppearancesForClickToDialCalls &&
            $value[self::alertAllAppearancesForGroupPagingCalls()] == $this->lastAlertAllAppearancesForGroupPagingCalls &&
            $value[self::allowSCACallRetrieve()] == $this->lastAllowSCACallRetrieve &&
            $value[self::multipleCallArrangementIsActive()] == $this->lastMultipleCallArrangementIsActive &&
            $value[self::allowBridgingBetweenLocations()] == $this->lastAllowBridgingBetweenLocations &&
            $value[self::bridgeWarningTone()] == $this->lastBridgeWarningTone &&
            $value[self::enableCallParkNotification()] && $this->lastEnableCallParkNotification) {

            return;
        }

        $this->lastUserId = $value[self::userId()];
        $this->lastAlertAllAppearancesForClickToDialCalls = $value[self::alertAllAppearancesForClickToDialCalls()];
        $this->lastAlertAllAppearancesForGroupPagingCalls = $value[self::alertAllAppearancesForGroupPagingCalls()];
        $this->lastAllowSCACallRetrieve = $value[self::allowSCACallRetrieve()];
        $this->lastMultipleCallArrangementIsActive = $value[self::multipleCallArrangementIsActive()];
        $this->lastAllowBridgingBetweenLocations = $value[self::allowBridgingBetweenLocations()];
        $this->lastBridgeWarningTone = $value[self::bridgeWarningTone()];
        $this->lastEnableCallParkNotification = $value[self::enableCallParkNotification()];

        $xmlinput  = xmlHeader($sessionid, "UserSharedCallAppearanceModifyRequest");
        $xmlinput .= "<userId>" . $value[self::userId()] . "</userId>";
        $xmlinput .= "<alertAllAppearancesForClickToDialCalls>" . self::normalize(self::alertAllAppearancesForClickToDialCalls(), $value[self::alertAllAppearancesForClickToDialCalls()]) . "</alertAllAppearancesForClickToDialCalls>";
        $xmlinput .= "<alertAllAppearancesForGroupPagingCalls>" . self::normalize(self::alertAllAppearancesForGroupPagingCalls(), $value[self::alertAllAppearancesForGroupPagingCalls()]) . "</alertAllAppearancesForGroupPagingCalls>";
        $xmlinput .= "<allowSCACallRetrieve>" . self::normalize(self::allowSCACallRetrieve(), $value[self::allowSCACallRetrieve()]) . "</allowSCACallRetrieve>";
        $xmlinput .= "<multipleCallArrangementIsActive>" . self::normalize(self::multipleCallArrangementIsActive(), $value[self::multipleCallArrangementIsActive()]) . "</multipleCallArrangementIsActive>";
        $xmlinput .= "<allowBridgingBetweenLocations>" . self::normalize(self::allowBridgingBetweenLocations(), $value[self::allowBridgingBetweenLocations()]) . "</allowBridgingBetweenLocations>";
        $xmlinput .= "<bridgeWarningTone>" . self::normalize(self::bridgeWarningTone(), $value[self::bridgeWarningTone()]) . "</bridgeWarningTone>";
        $xmlinput .= "<enableCallParkNotification>" . self::normalize(self::enableCallParkNotification(), $value[self::enableCallParkNotification()]) . "</enableCallParkNotification>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (readError($xml) != "") {
            $this->backEndErrorMsg = "OCI Error";
            return;
        }

        // Rebuild Device
        $xmlinput  = xmlHeader($sessionid, "GroupCPEConfigRebuildDeviceConfigFileRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $value[self::deviceName()] . "</deviceName>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (readError($xml) != "") {
            $this->backEndErrorMsg = "OCI Error";
            return;
        }

        // Reset Device
        $xmlinput  = xmlHeader($sessionid, "GroupCPEConfigResetDeviceRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $value[self::deviceName()] . "</deviceName>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (readError($xml) != "") {
            $this->backEndErrorMsg = "OCI Error";
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function addAttribute($key, $headTitle) {
        $this->keys[] = $key;
        $this->headTitles[] = $headTitle;

        $this->numColumns++;
    }


    //-----------------------------------------------------------------------------------------------------------------
    private static function normalize($key, $value) {
        switch ($key) {
            case self::isActive(): return strtolower($value);
            case self::allowOrigination(): return strtolower($value);
            case self::allowTermination(): return strtolower($value);
            case self::alertAllAppearancesForClickToDialCalls(): return strtolower($value);
            case self::alertAllAppearancesForGroupPagingCalls(): return strtolower($value);
            case self::allowSCACallRetrieve(): return strtolower($value);
            case self::multipleCallArrangementIsActive(): return strtolower($value);
            case self::enableCallParkNotification(): return strtolower($value);
            case self::allowBridgingBetweenLocations(): return strtolower($value);
            case self::bridgeWarningTone():
                if (strtolower($value) == "none") {
                    return "None";
                }
                elseif (strtolower($value) == "barge-in") {
                    return "Barge-In";
                }
                elseif (strtolower($value) == "barge-in and repeat") {
                    return "Barge-In and Repeat";
                }
            break;
        }
    }
}
