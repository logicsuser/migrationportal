<?php
require_once("/var/www/html/Express/config.php");
checkLogin();
?>
<script>
    var url = "";
    var sampleUrl = "";
    var bulkType = "";

    document.getElementById("file").disabled = true;
    document.getElementById("sampleRef").disabled = true;

    $(function () {
        $("#module").html("> Add Bulk Users");
        $("#endUserId").html("");
    });

    $("#file").click(function () {
        $("#file").val("");
    });

    function handle_files(files) {
    	pendingProcess.push("Provision Express Sheets");
        for (i = 0; i < files.length; i++) {
            file = files[i];
            console.log(file);
            var form_data = new FormData();
            form_data.append('file', file);
            form_data.append('bulkType', bulkType);

            $("#csvData").html("");

            $("#progress_bar").val(0);
            $("#progress_percentage").html("Uploading 0%");
            $("#progress_div").show();

            $("#file").prop("disabled", true);
            $("#gwMods").prop("disabled", true);

            $.ajax({
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();

                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            console.log(percentComplete);
                            $("#progress_bar").val(percentComplete);
                            $("#progress_percentage").html("Uploading ... " + percentComplete + "%");
                            if (percentComplete == 100) {
                                $("#progress_percentage").html("Uploaded 100%. Parsing the file ...");
                            }
                        }
                    }, false);

                    return xhr;
                },
                url: url,
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'POST',
                success: function (result) {
                	if(foundServerConErrorOnProcess(result, "Provision Express Sheets")) {
    					return false;
                  	}
                    $("#csvData").html(result);
                    $("#progress_div").hide();
                    $("#file").prop("disabled", false);
                    $("#gwMods").prop("disabled", false);
                }
            });

        }
    }

    // Invoked by onChange event from a group-wide user modification drop-down menu
    //-----------------------------------------------------------------------------
    function onMenuSelectionChange(select) {
        $('#file').val('');
        var optVal = select.options[select.selectedIndex].value;

        var disabled = optVal == "gwModsNone" ? true : false;
        document.getElementById("file").disabled = disabled;
        document.getElementById("sampleRef").disabled = disabled;

        bulkType = optVal;
        $("#csvData").html("");

        if (optVal == "gwModsNone") {
        }
        else if (optVal == "gwModsAddUsers") {
            url = "bulk/parseCSV.php";
            sampleUrl = "./bulk/sampleAddUsers.csv";
        }
        else if (optVal == "gwModsAddSCA") {
            url = "bulk/parseSCA.php";
            sampleUrl = "./bulk/sampleAddSCA.csv";
        }
        else if (optVal == "gwModsCFA") {
            url = "bulk/parseGroupWideModifications.php";
            sampleUrl = "./bulk/sampleCFA.csv";
        }
        else if (optVal == "gwModsCFNR") {
            url = "bulk/parseGroupWideModifications.php";
            sampleUrl = "./bulk/sampleCFNR.csv";
        }
        else if (optVal == "gwModsAuth") {
            url = "bulk/parseGroupWideModifications.php";
            sampleUrl = "./bulk/sampleUserAuthentication.csv";
        }
        else if (optVal == "gwModsSwPck") {
            url = "bulk/parseGroupWideModifications.php";
            sampleUrl = "./bulk/sampleServiceManagement.csv";
        }

        document.getElementById("sampleRef").href = sampleUrl;

    }

</script>
<style>
.inputfile + .BrowseLabel {
    text-align: center;
    font-size: initial;
}
.BrowseLabel {
padding-left: 0px;
    padding-top: 6px;
}
</style>
<div class="adminUserText">Bulk Modify</div>
<div class="vertSpacer">&nbsp;</div>

<div class="container selectContainer">
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<form method="POST" enctype="multipart/form-data">
				<div style="">
                <div class="dropdown-wrap">   
					<select name="gwMods" id="gwMods" onchange="onMenuSelectionChange(this)" class="form-control">
						<option value="gwModsNone">Select one:</option>
						<option value="gwModsCFA">Modify 'Call Forwarding Always' services</option>
						<option value="gwModsCFNR">Modify 'Call Forwarding Not Reachable' services</option>
						<option value="gwModsAuth">Reset Authentication</option>
						<option value="gwModsSwPck">Services/Service Packs Management</option>
                    </select>
                        </div>
					<div style="clear:right;">&nbsp;</div>
					<div style="clear:right;">&nbsp;</div>
					<strong><label style="margin-bottom: 5px" for="file" class="labelTextGrey">Please upload a CSV or XLSX file below:</label></strong>
					<div class="" style="margin-bottom: 5px;">
                    <input style="color:#9c9c9c;width: 82px;" type="file" class="inputfile labelTextGrey" name="file" id="file" onchange="handle_files(this.files)"
							   accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,.csv,text/csv">							   
					</div>
					<div><a id="sampleRef"><label class="labelText" style="margin-left:0;">Click here</label></a>&nbsp;<label class="labelTextGrey">for a sample user template in CSV format</label></div>
				</div>
			</form>
		</div>
		<div class="col-md-3"></div>
	</div>
	<br/>
	<div id="progress_div" style="display: none;width: 100%;">
		<div id="progress_percentage" style="text-align: center;">0%</div>
		<progress value="0" max="100" id="progress_bar" style="width: 100%;"></progress>
	</div>
	<div id="csvData"></div>
</div>
