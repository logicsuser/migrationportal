<?php
/**
 * Created by Karl.
 * Date: 10/11/2016
 */
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();

    //-------------------------------------------------------------------
    function submitOrReSubmit($error) {
        if ($error) {
            return "Please correct errors and resubmit.";
        }

        return "<input class=\"subButton\" style=\"margin-left: 4px; margin-bottom: 4px\" type=\"button\" name=\"subBulk\" id=\"subBulk\" value=\"Create Shared Call Appearances\">";
    }

    include("/var/www/lib/broadsoft/adminPortal/CSVManager.php");
    include("/var/www/lib/broadsoft/adminPortal/CSVSharedCallAppearance.php");

    require_once("/var/www/lib/broadsoft/adminPortal/getAllDevices.php");
    require_once("/var/www/lib/broadsoft/adminPortal/getGroupDomains.php");
    require_once("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");

    $csvManager = new CSVSharedCallAppearance("csvUsers");

    //Check if a file is uploaded
	if (file_exists($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
		$csvManager->is_file_upload = true;
	}

?>

<script>
    $(function() {
        $("#parseDiv").tooltip();

        var dataString = $("form#parseForm").serialize();

        $("#subBulk").click(function() {
            $("#loadingBulk").dialog("open");
            $("#loadingBulk").html("<div class=\"loading\" id=\"loading3\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
            $("#loading3").show();
            $.ajax({
                type: "POST",
                url: "bulk/createSharedCallAppearance.php",
                data: dataString,
                success: function(result)
                {
                    $("#loading3").hide();
                    $(".ui-dialog-buttonpane", this.parentNode).show();
                    $("#loadingBulk").html(result);
                    $("#loadingBulk").append(returnLink);
                }
            });
        });

        $("#loadingBulk").dialog(
            {
                autoOpen: false,
                width: 1400,
                modal: true,
                resizable: false,
                position: { my: "top", at: "top" },
                closeOnEscape: false,
                open: function(event, ui)
                {
					setDialogDayNightMode($(this));
                    $(".ui-dialog-titlebar-close", ui.dialog).hide();
                    $(".ui-dialog-buttonpane", this.parentNode).hide();
                },
                buttons: {
	                "More changes": function() {
		                $("#csvData").empty();
	                    $(this).dialog("close");
	                    $('html, body').animate({scrollTop: '0px'}, 300);
	                }
                }
            });
    });
</script>

<div id="parseDiv">
    <form style="" name="parseForm" id="parseForm" method="POST" action="">
        <table align="center" class="table bulkSCAAddUser" style="width:100%;">
            <tr><?php echo $csvManager->createTableHeader(); ?></tr>
            <?php echo $csvManager->parse(); ?>
            <tr><td colspan=\"100%\">&nbsp;</td></tr>
        </table>
        <?php echo submitOrReSubmit($csvManager->hasError()); ?>
    </form>
</div>
<div id="loadingBulk"></div>
