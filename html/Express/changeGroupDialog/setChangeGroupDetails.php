<?php
              
        if(isset($_POST['clusterName']) && $_POST['clusterName'] != ""){                
                $flagForClusterBase = "yes";
                $selectedClusterName = $_POST['clusterName'];
                require_once("/var/www/lib/broadsoft/adminPortal/loginClusterBise.php");                
                $sessionid = $sessionidForSelCluster;
                $client    = $clientForSelCluster;
                require_once("/var/www/lib/broadsoft/adminPortal/ServiceProviderOperationsClusterBase.php");
          }else{
            require_once("/var/www/lib/broadsoft/login.php");
            require_once("/var/www/lib/broadsoft/adminPortal/ServiceProviderOperations.php");
            $flagForClusterBase = "no";
        }
	
	checkLogin();
	require_once("/var/www/lib/broadsoft/adminPortal/util/PreviousChangeGroupUtil.php");
	
	if($_POST['module'] == "checkEnterPriseExist") {
            
            if($flagForClusterBase == "yes"){
                    if( $_SESSION['superUser'] == 1 ) {
                        $spExist = getServiceProvidersListSuperClsBase($_POST['selectedSP'], $sessionid, $client);
                    } else {
                        $spExist = getServiceProvidersList($_POST['selectedSP']);
                    }
            }
            if($flagForClusterBase == "no"){
                    if( $_SESSION['superUser'] == 1 ) {
                        $spExist = getServiceProvidersListSuper($_POST['selectedSP']);
                    } else {
                        $spExist = getServiceProvidersList($_POST['selectedSP']);
                    }
            }
	    if( $spExist ){
	        echo "Enterprise is Exist";
	    } else {
	        echo "Enterprise Not Exist";
	    }
	    die;
	}
        
        
	
	if($_POST['module'] == "deleteGroupNotExist") {
	    $preCG = new PreviousChangeGroupUtil();
	    $delResp = $preCG->deleteGroupNotExist($_POST['enterpriseId'], $_POST['groupId'], $_SESSION["adminId"]);
	    die;
	}

	if($_POST['module'] == "deleteSpNotExist") {
	    $preCG = new PreviousChangeGroupUtil();
	    $delResp = $preCG->deleteSpNotExist($_POST['enterpriseId'], $_SESSION["adminId"]);
	    if($delResp) {
	        echo "Enterprise is Deleted.";
	    }
	    die;
	}
	
	if($_POST['module'] == "getChangeGroupDetail") {
	    $preCG = new PreviousChangeGroupUtil();
	    $changeGroupDetail = $preCG->getPreviousChangeGroup();
	    echo json_encode($changeGroupDetail);
	    die;
	}
    
        //Code added @ 14 June 2019
        $clusterName = "";
        if(isset($_POST['clusterName']) && $_POST['clusterName'] !=""){
            $clusterName = $_POST['clusterName'];
            
        }
        //End code
        
	$spId = $_POST['spId'];
	$spName = isset($_POST['spName']) ? $_POST['spName'] : "";
	if( isset($_POST['spId']) && ! isset($_POST['spName'])) {
            if($flagForClusterBase == "yes"){
                $gUS = new ServiceProviderOperationsClusterBase();
                $getSpProfile = $gUS->getServerProviderProfile($spId,$sessionid, $client);
                $spName = isset($getSpProfile['Succcess']) ? strval($getSpProfile['Succcess']->serviceProviderName) : "";
            }
            if($flagForClusterBase == "no"){
                $gUS = new ServiceProviderOperations();
                $getSpProfile = $gUS->getServerProviderProfile($spId);
                $spName = isset($getSpProfile['Succcess']) ? strval($getSpProfile['Succcess']->serviceProviderName) : "";
            }
            
        }
	
	$groupId = isset($_POST['groupId']) ? $_POST['groupId'] : ""; 
	$groupName = isset($_POST['groupName']) ? $_POST['groupName'] : "";
	if( isset($_POST['groupId']) && ! isset($_POST['groupName'])) {
            
            if($flagForClusterBase == "yes"){
                $gUS = new ServiceProviderOperationsClusterBase();
                $getGroupProfile = $gUS->getGroupProfile($spId, $groupId, $sessionid, $client);
                $groupName = isset($getGroupProfile['Succcess']) ? strval($getGroupProfile['Succcess']->groupName) : "";
            }
            if($flagForClusterBase == "no"){
                $gUS = new ServiceProviderOperations();
                $getGroupProfile = $gUS->getGroupProfile($spId, $groupId);
                $groupName = isset($getGroupProfile['Succcess']) ? strval($getGroupProfile['Succcess']->groupName) : "";
            }
            
        }
	
	$preCG = new PreviousChangeGroupUtil();
	$preCG->setChangeGroupDetails($_SESSION["adminId"], $spId, $spName, $groupId, $groupName, $clusterName);
        
        
    
    function getServiceProvidersListSuper($selectedSP) {
        $gUS = new ServiceProviderOperations();
        $sps = $gUS->getSystemServerProviderList();
        
        foreach($sps as $spKey => $spVal) {
            if($spVal == $selectedSP) {
                return true;
                break;
            }
        }
        return false;
    }
    
    function getServiceProvidersList($selectedSP)
    {
        
        global $sps;
        global $getSPName;
        //	if (isset($_SESSION["spList"]) && count($_SESSION["spList"] > 1)) {
        if (isset($_SESSION['spListNameId']) && count($_SESSION['spListNameId'] > 1)) {
            $serviceProviders = $_SESSION['spListNameId'];
        } else {
            $serviceProviders = $getSPName;
        }
        if(count($serviceProviders) > 0){
            foreach ($serviceProviders as $v) {
                $providerId = explode('<span>-</span>',$v); //ex-774
                $spIdwithName = str_replace('<span>-</span>', ' - ', $v);
                $currentspId = $providerId[0];
                if($currentspId == $selectedSP) {
                   return true; 
                }
            }
            
        }
        return false;
    }
    
    
    function getServiceProvidersListSuperClsBase($selectedSP, $sessionid, $client) {
        $gUSCBase = new ServiceProviderOperationsClusterBase();
        $sps = $gUSCBase->getSystemServerProviderList($sessionid, $client);
        
        foreach($sps as $spKey => $spVal) {
            if($spVal == $selectedSP) {
                return true;
                break;
            }
        }
        return false;
    }
    
?>