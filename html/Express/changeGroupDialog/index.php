<?php
require_once("/var/www/html/Express/config.php");

//Code added @ 11 June 2019
require_once("/var/www/lib/broadsoft/adminPortal/util/ConfigUtil.php");
$bwAppServerForCluster = appsServer;    
$configUtilObj = new ConfigUtil($db, $bwAppServerForCluster);
$clusterList = $configUtilObj->getClustersList();
//End code

checkLogin();



//Check if the user need to reset his password
if(isset($_SESSION['RESET_PASSWORD_MESSAGE'])) {
    
    header("Location: ./reset_password.php");
    exit;
}

$spNameInSession = $_SESSION["sp"];
//---------------------------------------------------------------------------
function showServiceProvider()
{
    //if (isset($_SESSION["spList"]) && count($_SESSION["spList"]) > 1) {
   // if (isset($_SESSION["spListNameId"]) && count($_SESSION["spListNameId"]) > 1) {
    if (isset($_SESSION["spListNameId"]) && count($_SESSION["spListNameId"]) > 0) {
        return true;
    }
    
    return isset($_SESSION["superUser"]) and $_SESSION["superUser"] == "1";
}


//---------------------------------------------------------------------------
function getServiceProvidersList()
{
    
    global $sps;
    global $getSPName ;
    
    
    //	if (isset($_SESSION["spList"]) && count($_SESSION["spList"] > 1)) {
    if (isset($_SESSION['spListNameId']) && count($_SESSION['spListNameId'] > 1)) {
        $serviceProviders = $_SESSION['spListNameId'];
    } else {
        $serviceProviders = $getSPName;
    }
    $str = "";
    if(count($serviceProviders) > 0){
        $str = "<option value=\"\"></option>";
        foreach ($serviceProviders as $v) {
            $providerId = explode('<span>-</span>',$v); //ex-774
            $spIdwithName = str_replace('<span>-</span>', ' - ', $v);
            $currentspId = $providerId[0];
            $providerName = isset($providerId[1]) ? $providerId[1] : "";
            $str .= "<option value=\"".$currentspId."\" data-spName=\"".$providerName."\">" . $spIdwithName . "</option>";
        }
    }
    return $str;
}


//---------------------------------------------------------------------------
function getAdminGroups()
{
    global $groupsList;
    return $groupsList;
}


//---------------------------------------------------------------------------
function groupExceedSearchThreshold()
{
    global $numberOfGroups;
    //USPS change
    return $numberOfGroups > 0;
}

function checkSecurityDomainNonSuperUser($userId){
    global $db;
    
    $permissionsQuery = "SELECT securityDomain from permissions where userId = ?";
    $permissionsQueryStmt = $db->prepare($permissionsQuery);
    $permissionsQueryStmt->execute(array($userId));
    $permissionsRow = $permissionsQueryStmt->fetch(PDO::FETCH_ASSOC);
    return $permissionsRow["securityDomain"];
    
}
//error_log(print_r($_SESSION, true));

$passExpTime = "";
if($_SESSION["superUser"] == "1") {
    $passExpTime = $passExpiration_Super;
} else {
    $passExpTime = $passExpiration_RegUser;
}
$checkSecurityDomainValue = checkSecurityDomainNonSuperUser($_SESSION["adminId"]);
$checkSecurityDomainSPValue = getServiceProvidersList();

require_once("/var/www/html/Express/adminCheckFilter.php");
 
require_once("/var/www/lib/broadsoft/login.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");
//unset($_SESSION["groupId"]);
if(isset($_SESSION["superUser"]) and $_SESSION["superUser"] == "1"){
	unset($_SESSION["sp"]);
}
unset($_SESSION["groupInfo"]);
//$_SESSION["sp"] = "None";
set_group_selected_user();

require_once("/var/www/lib/broadsoft/adminPortal/getServiceProviders.php");
//$no_header = true;
//require_once("header.php");

$numberOfGroups = 0;
$groupsList = "";

// if ($_SESSION["superUser"] == "0" && isset($_SESSION["spListNameId"])) {
// 	$numberOfGroups = count($_SESSION["groups"]);
// 	$groupsList .= "<option value=\"\"></option>";
// 	for ($a = 0; $a < count($_SESSION["groups"]); $a++) {
// 		$groupsList .= "<option value=\"" . $_SESSION["groups"][$a] . "\">" . $_SESSION["groups"][$a] . "</option>";
// 	}
// }

?>
<script>

   
    var href = location.href;
    var urlLastSegment = href.match(/([^\/]*)\/*$/)[1];
    
    history.pushState(null, null, urlLastSegment);
    window.addEventListener('popstate', function () {
        history.pushState(null, null, urlLastSegment);
    });
 
    var selectedSPName = "";
    var superUser = "<?php echo $_SESSION['superUser']; ?>";
    var selectedSP = "<?php echo(isset($_SESSION['sp']) ? $_SESSION['sp'] : ''); ?>";
    var securityDomainPattern = "<?php echo $securityDomainPattern; ?>";
    var groupSearchString = "";
    $("#securityDomainCheckNonSuperUserDiv").hide();
    $("#securityDomainGroupCheckNonSuperUserDiv").hide();
    $(function () {
        
        $("#helpUrl").hide();
        if (selectedSP != "") {
            getSearchData();
        }
        $("#logoImage").removeAttr("href");
        $("#search").click(function () { 
            
            var searchData = document.getElementById("searchGroupsVal").value; 
            if (searchData != "") {
            	var groupArr = searchData.split('<span class="search_separator">-</span>');
                var groupName = decodString($.trim(groupArr[0]));
                var selGroupName = typeof groupArr[1] != "undefined" ? decodString($.trim(groupArr[1])) : "";
                //alert("SP:" + selectedSP + " Grp:" + groupName);
                $('#searchGroups').val(groupSearchString);
                $.ajax({
                    type: "POST",
                    url: "setGroup.php",
                    data: {groupName: groupName, Selsp: selectedSP},
                    success: function (result) { 
                        console.log(result);
                        result = result.trim();
                        if (result.slice(0, 1) == "1") {
                            alert("Error:  Group not found: " + groupName);
                        } else {
                        	setPreviousChangeGroupDetails("main", data = {spId: selectedSP, spName: selectedSPName, groupId: groupName, selGroupName: selGroupName});
                            $("#logoImage").attr("href", "main.php");
                            window.location.replace("main.php");
                        }
                    }
                });
            }
        });
		
        $('#searchGroups').on('keyup keypress keydown', function(e) {

        /*Old code*/
			var resultSearch = $("#searchGroups").val();
			if(resultSearch =='')
			{
				$("#search").prop('disabled',true);
			}else{
				$("#search").prop('disabled',false);
			}
	/*Old code*/		
		
			  var keyCode = e.keyCode || e.which;
              //Code added @28 March 2018 to fix the issue EX-458
              if (keyCode === 38)
              {
            	  var tmpStrSearch = $("#searchGroups").val();
                  var tmpStr = $.trim(tmpStrSearch);
                  $('#searchGroups').val(decodString(tmpStr.replace('<span class="search_separator">-</span>', '-')));
              }
              
              if (keyCode === 40)
              { 
                  var tmpStrSearch = $("#searchGroups").val();
                  var tmpStr = $.trim(tmpStrSearch);
                  $('#searchGroups').val(decodString(tmpStr.replace('<span class="search_separator">-</span>', '-')));
              }
              if (keyCode === 9)
              { 
                  if($(".ui-state-focus").length){
                  	var tmpStrs = $(".ui-state-focus").html();
                  	var tmpStr =  $.trim(tmpStrs);
                  }else{
					var tmpStr = $.trim(groupSearchString);
                  }
                  groupSearchString = $.trim(tmpStr);
                  $(document).find(".ui-state-focus").addClass("tabGroup");
                  $('#searchGroupsVal').val(tmpStr);
                  $('#searchGroups').val(decodString(tmpStr.replace('<span class="search_separator">-</span>', '-')));
                  $('#searchGroups').trigger("blur");
              }
              //End code

			  if (keyCode === 13) {
				e.preventDefault();
                //var searchVal = $("#searchGroups").val(); //Commented on 28 March 2018 due to fix the issue EX-458
                //Code added @28 March 2018 to fix the issue EX-458                                
                var searchTmpNewStr = $("#searchGroups").val();
                var tmpNewStr = $.trim(searchTmpNewStr);
                if($(".ui-state-focus").length) {
                  	var tmpNewStrs = $(".ui-state-focus").html();
                  	var tmpNewStr = $.trim(tmpNewStrs);
                  }else{
					var tmpNewStr = $.trim(groupSearchString);
					
                  }
                groupSearchString = tmpNewStr;
                $('#searchGroupsVal').val(tmpNewStr);
				var searchVal = tmpNewStr.replace('<span class="search_separator">-</span>', '-'); 
				$('#searchGroups').trigger('blur');
                                //End Code
				if(searchVal != "") {            
					var searchGroupName = $.trim(searchVal);                          
					$('#searchGroups').val(decodString(searchGroupName));
					if(tmpNewStr.search('span') != -1) {
						var searchtmpNewStr = $.trim(tmpNewStr); 
						$('#searchGroupsVal').val(searchtmpNewStr);
					}
					$('#search').trigger('click');
				}
			}
		});
		
		$("#searchGroups").blur(function(e){
        	var el = $(this).val(); 
        	if(el != "") {
        		var seachIdValue = el;
        		groupSearchString = $.trim(seachIdValue);
            }
        	if($(".ui-state-focus").length){
              	var searchValRes = $(".ui-state-focus").html();
              	var searchVal = $.trim(searchValRes);
              }else{
				var searchVal = $.trim(groupSearchString);
              }
            
			if(searchVal.search('span') != -1) {
				$('#searchGroupsVal').val(searchVal);
			}
			var groudVal = groupSearchString.replace('<span class="search_separator">-</span>', '-');
			if(groudVal != "") {
				var groudVals = $.trim(groudVal);                          
				$('#searchGroups').val(decodString(groudVals));
				$('#search').trigger('click');
				 
			} else {
				var groudValgroupSearchString = $.trim(groupSearchString);                          
				 $('#searchGroups').val(decodString(groudValgroupSearchString));
			}    
					
        });

        $("#groupName").change(function () {
            var groupName = decodString($("#groupName").val());
            var Selsp = $("#Selsp").val();
            //alert("SP:" + Selsp + " Grp:" + groupName);
            $.ajax({
                type: "POST",
                url: "setGroup.php",
                data: {groupName: groupName, Selsp: Selsp},
                success: function (result) 
		{
                    window.location.replace("main.php");
                }
            });
        });

        //$("#searchBlock").hide();
        
        $("#Selsp").change(function () {
        	$("#search").prop('disabled',true);
            $("#searchGroups").val('');
            $('.entMainRedirectLink').css('display','none');
            //$("#enterprise_main_page_link").hide();
            selectedSP = $(this).val();
            selectedSPName = $('option:selected', this).attr('data-spName');
            if (selectedSP != "") {
                getSearchData();
            }
            //alert('Selsp Change Called - ' + selectedSPName);
            //if (superUser == "1" || securityDomainPattern != "") {
            if (selectedSP != "") {
                $("#groupName").empty();
                $.ajax({
                    type: "POST",
                    url: "adminUsers/getAllGroups.php",
                    data: {getSP: selectedSP, chooseGroup: "true"},
                    success: function (result) {
                    	$("#securityDomainCheckNonSuperUserDiv").hide();
							$("#groupName").append("<option value=\"\"></option>");
	                        $("#groupName").append(result);
	                        var numGroups = occurrences(result, "</option>", false);
	                        if (numGroups > 0) { 
	                            $("#searchBlock").show();
	                        }else {
	                            $("#chooseGroup").show();
	                            $("#searchBlock").hide();
	                            $("#enterprise_main_page_link").show();
	                            $('.entMainRedirectLink').css('display','block');
	 							$("#securityDomainGroupCheckNonSuperUserDiv").show();
	                        }
                    }
                });
            } else if(selectedSP == "") {
            	$("#chooseGroup").show();
                $("#searchBlock").hide();
                //$("#securityDomainGroupCheckNonSuperUserDiv").show();
             }

            //If SuperUser, set the Enterprise and show the "Enterprise Main Page" link
           // $('.entMainRedirectLink').css('display','none');
            //$("#enterprise_main_page_link").hide();
            //if (selectedSP != "" && superUser == "1") {
            if (selectedSP != "" && (superUser == "1" || superUser == "3")) {
                var Selsp = $("#Selsp").val();
                $.ajax({
                    type: "POST",
                    url: "setGroup.php",
                    data: {Selsp: Selsp},
                    success: function (result) {
                         
                        //$("#enterprise_main_page_link").show();
                        $('.entMainRedirectLink').css('display','block');
                    }
                });
            }

        });
        
        var selectedSPInSession = "<?php echo $spNameInSession; ?>";
        if(selectedSPInSession != "None"){
        	$("#Selsp").val(selectedSPInSession);
            $("#Selsp").trigger("change");
        };

        $(document).on("click", ".groupAutoClass > li > a", function(){
    		var el = $(this);
    		var groupSearchStrings = el[0].innerHTML; 
    		$('#searchGroups').val(decodString(el[0].innerHTML.replace('<span class="search_separator">-</span>', '-')));
    		groupSearchString = $.trim(groupSearchStrings);
    		$('#searchGroupsVal').val(groupSearchString);
    		$('#searchGroups').trigger('blur');
	 	});
        
    });
    
    
    
    
    

	$("#enterprise_main_page_href").click(function() {
		setPreviousChangeGroupDetails("enterprise", data = {spId: selectedSP, spName: selectedSPName});
	});

	function setPreviousChangeGroupDetails(module, data) {	
            
                var selectedCluster = $('#entClusterNameSel').val();
                if(typeof selectedCluster !== "undefined"){                    
                    if( module == "main" ) {
			var dataToSend = { clusterName: selectedCluster, spId: data.spId, spName: data.spName, groupId: data.groupId, groupName: data.selGroupName };
                    } else if(module == "enterprise") {
                            var dataToSend = { clusterName: selectedCluster, spId: data.spId, spName: data.spName };
                    }                    
                }else{
                    if( module == "main" ) {
			var dataToSend = { spId: data.spId, spName: data.spName, groupId: data.groupId, groupName: data.selGroupName };
                    } else if(module == "enterprise") {
                            var dataToSend = { spId: data.spId, spName: data.spName };
                    }
                }		
		$.ajax({
			type: "POST",
			url: "changeGroupDialog/setChangeGroupDetails.php",
			data: dataToSend,
			success: function (result) {
				console.log(result);
			}
		});
	}
	
    function getSearchData() {
    	$("#search").prop('disabled',true);
        var autoComplete = new Array();
        //alert("SP:" + selectedSP + " Selected Groups:" + selectedGroups);
        $.ajax({
            type: "POST",
            url: "getGroupsSearchData.php",
            data: {serviceProvider: selectedSP},
            success: function (result) {
                var explode = result.split(":");
                for (var a = 0; a < explode.length; a++) {
                    autoComplete[a] = explode[a];
                }
                $("#searchGroups").autocomplete({
                    source: autoComplete
                //});
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
	                ul.addClass('groupAutoClass'); //Ul custom class here
					if(item.value.search('<span class="search_separator">-</span>') > 0){
						return $("<li></li>")
						.append("<a href='#'>" + item.value + "</a>")
						.data("ui-autocomplete-item", item)
						.appendTo(ul);
					}else{
						return $("<li class='ui-menu-item'></li>")
		                .append("<a href='#'>" + item.label + "</a>")
		                .data("ui-autocomplete-item", item)
		                .appendTo(ul);						
					}
	            };
            }
        });
    }

 

     

    function occurrences(string, subString, allowOverlapping) {
        string += "";
        subString += "";
        if (subString.length <= 0) return (string.length + 1);

        var n = 0,
            pos = 0,
            step = allowOverlapping ? 1 : subString.length;

        while (true) {
            pos = string.indexOf(subString, pos);
            if (pos >= 0) {
                ++n;
                pos += step;
            } else break;
        }
        return n;
    }

    function decodString(encodedStr) {
    	var parser = new DOMParser;
    	var dom = parser.parseFromString(
    	    '<!doctype html><body>' + encodedStr,
    	    'text/html');
    	var decodedString = dom.body.textContent;
    	return decodedString;
    }
    
        //Code added @ 13 June 2019
         $("#entClusterNameSel").change(function () {
            $("#searchGroups").val('');
            var selectedCluster = $('#entClusterNameSel').val();
            //alert('selectedCluster - '+selectedCluster);            
            if (selectedCluster != "") {
                //$("#groupName").empty();
                $.ajax({
                    type: "POST",
                    url: "changeConnectionForSelCluster.php",
                    data: {clusterName: selectedCluster, chooseGroup: "true"},
                    success: function (result) {
                        //console.log(result);
                        $('#Selsp').html(result);
                    	//alert('Testing123');
                    }
                });
            }
        });
        //End code
    
</script>
<link rel="stylesheet" href="/Express/css/custom_bootstrap.css">
<style>
    .ui-autocomplete{
    margin-top: 23.5% !important;
    width: 400px !important;
    max-height: 160px !important;
    position: fixed;
    height: auto !important;
    overflow-y: auto;
  
    }
    .groupAutoClass{
    z-index: 9999 !important;
    }
    .sUserTextModal{margin-top:0!important;margin-bottom:0 !important;}
</style>


<div id="">
	<div class="">
		<div class="" id="">
		<h4 class="centerDesc sUserTextModal">
				<strong>
				<label class='labelTextGrey'>	Welcome 
                                    <?php                                     
                                    if(isset($_SESSION["superUser"]) && $_SESSION["superUser"] == 1){
                                        echo "Super User";
                                    }
                                    else if(isset($_SESSION["superUser"]) && $_SESSION["superUser"] == 3){
                                        echo "Enterprise Admin";
                                    }
                                    else{
                                        echo "";
                                    }
                                    ?>
			 	</label>
					<br/>
				 <label class='labelTextGrey'>	<?php echo isset($_SESSION["loggedInUser"]) ? $_SESSION["loggedInUser"] : ""; ?></label>
					
				</strong>
		</h4>
				<br/>
				<br/>
			 
			<div style="width:100%;text-align:center;">
				<form name="chooseGroup" id="chooseGroup" method="POST">
                                    <!--cluster selection-->
                                    <?php 
                                    $clusterSupport  =  (isset($bwClusters) && $bwClusters == "true") && $enableClusters == "true" ? true : false;
                                    if($_SESSION["superUser"] == "1" && $clusterSupport){
                                    ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="labelText">Select Cluster:</label>
                                                    <div class="dropdown-wrap">   
                                                        <?php                                                        
                                                            $clusterIdName  = "entClusterNameSel";
                                                            $clusterClsName = "form-control";
                                                            echo $clusterDropdownHtml = $configUtilObj->createClustersDropdownList($clusterIdName, $clusterClsName ,$clusterList, $_SESSION['selectedCluster']);
                                                        ?>                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php 
                                    }
                                    ?>
                                  <!--end cluster selection -->  
                                    
                                    <div class="row" style="display: <?php echo showServiceProvider() ? "block" : "none"; ?>">
						<div class="col-md-12">
                            <div class="form-group">
                            <label class="labelText">Select Enterprise:</label>
                            <div class="dropdown-wrap" id="enterpriseListDiv">   
                                    <select class="form-control blue-dropdown" name="Selsp" id="Selsp"><?php echo getServiceProvidersList(); ?></select>
                            </div>
</div>
                        </div>
						
					</div>

			<div class="row" name="groupBlock" id="groupBlock" style="display: none">
		                <div class="col-md-12">
		                    <div class="col-md-3"></div>
		                    <div class="col-md-6">
		                    <div class="form-group">
                                <label class="labelText">Select Group:</label>
                                <div class="dropdown-wrap">   
		                        <select class="form-control blue-dropdown" name="groupName" id="groupName"><?php echo getAdminGroups(); ?></select>
</div>
                            </div>
		                    </div>
		                    <div class="col-md-3"></div>
		                </div>
		            </div>

            <div class="row" name="searchBlock" id="searchBlock" style="display: <?php echo (groupExceedSearchThreshold() ? " block" : " none"); ?>">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="labelText" style="">Search for Group ID/Name:</label>
                        <input class="form-control blue-dropdown magnify" type="text" class="autoFill" name="searchGroups" id="searchGroups" style="overflow:scroll;">
						<input class="form-control" type="hidden" name="searchGroupsVal" id="searchGroupsVal">
                    </div>
                </div>
                <div class="col-md-12">
               
                <div class="form-group">
		        <div class="register-submit subBtnDiv">
                        <!-- <input type="button" name="search" id="search" class="subButton" value="Select"> -->
                        <input type="button" name="search" id="search" value="Select" class="submitBtn">
                    </div>
                </div>
                </div>
            </div>
            </form>
            <div class="row eMPSU">
            <div class="" >
                <div class="col-md-6 paddingZero">
					 <div class="form-group entMainRedirectLink" id="enterprise_main_page_link" style="float: left;">
    			<a id="enterprise_main_page_href" href="main_enterprise.php" class="pull-left m-t-mini">
    					<span class="labelText labelTextMargin">Enterprise Main Page</span>
    				</a>
                    </div>
                </div>
                <div class="col-md-6 paddingZero">
                    <div class="form-group" style="float: right">
    				<a href="/Express/index.php" class="pull-left m-t-mini">
    					<span class="labelText">Switch User</span>
    				</a>
                    </div>
                </div>
            </div>
            </div>
	<div id="userData"></div>
</div>
       </div>
	 
    </div>
</div>