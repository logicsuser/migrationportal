<?php

function redirectUser($currentPage) {
    $old_url = isset($_SESSION["redirectUrl"]) ? $_SESSION["redirectUrl"] : "";
    $group = isset($_SESSION["groupId"]) ? $_SESSION["groupId"] : "";
    $sp = isset($_SESSION["sp"]) ? $_SESSION["sp"] : "";;
    if($old_url == "" || $currentPage == $old_url) {
        return true;
    }
    
    if($_SESSION["superUser"] == "2" && $old_url == "choosegroup") {
        return;
    }
    if($_SESSION["superUser"] == "2" && ($currentPage == "enterprise" || $currentPage == "main" )) {
        $_SESSION["redirectUrl"] = "sysAdmin";
        header("Location:sysAdmin/sysAdminMod.php");
        exit;
    }
    
    if($currentPage == "choosegroup" && ($old_url == "enterprise" || $old_url == "main" || $old_url == "choosegroup" || $old_url == "" )) {
		unset($_SESSION["previousGroupId"]);
        $_SESSION["redirectUrl"] = "choosegroup";
        header("Location:chooseGroup.php");
        exit;
    }
    
    if($currentPage == "enterprise" && ($old_url == "enterprise" || $old_url == "choosegroup" )) {
		if($old_url == "choosegroup" && $sp != null || ($old_url == "enterprise" && $sp != null)) {
            unset($_SESSION["groupId"]);
            $_SESSION["redirectUrl"] = "enterprise";
            header("Location:main_enterprise.php");
            exit;
        }
        $_SESSION["redirectUrl"] = "choosegroup";
        header("Location:chooseGroup.php");
        exit;
    }
    
    /* Go to Enterprise main page Only for super user. */
    if(($_SESSION["superUser"] == "1" || $_SESSION["superUser"] == "3") && $currentPage == "enterprise" && ($old_url == "main" )) {
		//print_r($_SESSION["previousGroupId"]); exit;
        $_SESSION["previousGroupId"] = $_SESSION["groupId"];
		unset($_SESSION["groupId"]);
        $_SESSION["redirectUrl"] = "enterprise";
        header("Location:main_enterprise.php");
        exit;
    }
    
    if($currentPage == "enterprise" && ($old_url == "main" )) {
        $_SESSION["redirectUrl"] = "main";
        header("Location:main.php");
        exit;
        
    }
	
    if($currentPage == "main" && ($old_url == "enterprise" )) {
         if($_SESSION["superUser"] == "1" || $_SESSION["superUser"] == "3") {
			 if(!isset($_SESSION["groupId"]) && isset($_SESSION["previousGroupId"])) {
				/* Go to Group main page only for super user. */
				$_SESSION["groupId"] = $_SESSION["previousGroupId"];
				if( isset($_SESSION["previousSP"]) ) {
				    $_SESSION["sp"] = $_SESSION["previousSP"];
				}
				unset($_SESSION["previousGroupId"]);
			} else if(isset($_SESSION["groupId"])) {
				unset($_SESSION["previousGroupId"]);
			}
		}
        if(isset($_SESSION["groupId"]) && isset($_SESSION["sp"])) {
            $_SESSION["redirectUrl"] = "main";
            header("Location:main.php");
            exit;
        } else {
            unset($_SESSION["groupId"]);
            $_SESSION["redirectUrl"] = "enterprise";
            header("Location:main_enterprise.php");
            exit;
        }
        
    }
    
    if($currentPage == "main" && ( $old_url == "main" || $old_url == "choosegroup" )) {
        if($old_url == "choosegroup" && $group != null && $sp != null) {
            $_SESSION["redirectUrl"] = "main";
            header("Location:main.php");
            exit;
        }
        
        if($old_url == "choosegroup" && $group == null ) {
            $_SESSION["redirectUrl"] = "choosegroup";
            header("Location:chooseGroup.php");
            exit;
        }
        
    }
    
    
    if($currentPage == "sysAdmin" && $_SESSION["redirectUrl"] != "sysAdmin" ) {
        if($old_url == "choosegroup") {
            $_SESSION["redirectUrl"] = "choosegroup";
            header("Location:../chooseGroup.php");
            exit;
        }
        if($old_url == "main") {
            $_SESSION["redirectUrl"] = "main";
            header("Location:../main.php");
            exit;
        }
        if($old_url == "enterprise") {
            unset($_SESSION["groupId"]);
            $_SESSION["redirectUrl"] = "enterprise";
            header("Location:../main_enterprise.php");
            exit;
        }
    }
}


//End Code
?>