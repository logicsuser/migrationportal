<?php
/**
 * Created by Karl.
 * Date: 4/20/2017
 */
    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();
    
    //echo "<pre>Groups - ";
    //print_r($_SESSION["groups"]);
    
    unset($_SESSION["groupsSearch"]);
    $selectedGroups = array();
    if (isset($_POST["selectedGroups"]) && $_POST["selectedGroups"] != "") {
        $selectedGroups = explode(",", $_POST["selectedGroups"]);
    }

    $xmlinput = xmlHeader($sessionid, "GroupGetListInServiceProviderRequest");
    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["serviceProvider"]) . "</serviceProviderId>";
    $xmlinput .= xmlFooter();
    $response = $client->processOCIMessage(array("in0" => $xmlinput));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

    foreach ($xml->command->groupTable->row as $key => $value) {
        $groupId = strval($value->col[0]);

        if (isset($_SESSION["groups"]) && ! in_array($groupId, $_SESSION["groups"])) {
            continue;
        }

        //$searchData = $groupId;
        $searchData = $groupId . " ";
        if (strval($value->col[1])) {
            //$searchData .= " - " . strval($value->col[1]);
            //$searchData .= "- " . strval($value->col[1]);
            $searchData .= '<span class="search_separator">-</span> ' . strval($value->col[1]);
        }

        $_SESSION["groupsSearch"][$searchData] = $groupId;

        if (isset($_SESSION["groupsSearch"])) {
            ksort($_SESSION["groupsSearch"]);
        }
    }

    if (isset($_SESSION["groupsSearch"])) {
        foreach ($_SESSION["groupsSearch"] as $key => $value) {
            echo $key . ":";
        }
    }
?>

