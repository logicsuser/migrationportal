<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();
	require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
	$expProvDB = new ExpressProvLogsDB();
	/*change log new code added ../ */
        
        require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");
        $changeLogData = get_change_logs($_SESSION['ChangeLogfilters'], "desc");
        
        /*end code */
	$fileName = $_SESSION["groupId"] . rand(1092918, 9281716);
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment;filename=\"" . $fileName . ".csv\"");
	header("Content-Transfer-Encoding: binary");

// 	$query = "SELECT id, userName, date, module,enterpriseId, groupId, entityName from changeLog order by id desc"; //commented as ../
//	$query = "SELECT changeLog.*, CONCAT(firstName, ' ' , lastName) as modified_by FROM expressProvLogs.changeLog LEFT JOIN adminPortal.users on users.userName = changeLog.userName order by id desc"; //commented as ../
//	$fth = $expProvDB->expressProvLogsDb->query($query); 
	$message = "S.No.,Date,Enterprise ID ,Group ID,Module,Entity Name,Modified By\n";
	$count = 1;
	//while ($row = $fth->fetch()) // commented as ../
        foreach($changeLogData as $row)
	{
                $enterpriseId = isset($row["enterpriseId"]) ? $row["enterpriseId"] : 'None';
		$groupId      = isset($row["groupId"]) ? $row["groupId"] : 'None';	
		$modified_by = $row["modified_by"] != "" ? $row["modified_by"] : $row["userName"];
		
		$message .=
		    "=\"" . $count . "\"," .
			"=\"" . $row["date"] . "\"," .
			"=\"" . $enterpriseId . "\"," .
			"=\"" . $groupId . "\"," .
			"=\"" . $row["module"] . "\"," .
			"\"=\"\"" . $row["entityName"] . "\"\"\"," .
			"=\"" . $modified_by . "\"\n";
		$count++;
	}
	$fp = fopen("php://output", "a");
	fwrite($fp, $message);
	fclose($fp);
?>
