<?php
require_once("/var/www/html/Express/config.php");
checkLogin();
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");
require_once("/var/www/lib/broadsoft/login.php");
require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
require_once("/var/www/html/Express/userMod/UserCustomTagManager.php");
$expProvDB = new ExpressProvLogsDB();


function showClusterAndfqdnInfoONDetailsLog($clusterName, $bwAppServer, $fqdn, $showLogData)
{
        if($bwAppServer == "primary"){
            $bwAppServer = "Primary";
        }
        if($bwAppServer == "secondary"){
            $bwAppServer = "Secondary";
        }

        if($clusterName != "" && $clusterName != "None"){
            $showLogData.="<tr>";
            $showLogData.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
            $showLogData.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
            $showLogData.="</tr>";
        }        

        if($fqdn != ""){
                if($fqdn == "None"){
                    $showLogData.="<tr>";
                    $showLogData.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                    $showLogData.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                    $showLogData.="</tr>";
                }
                if($fqdn != "None"){
                    $showLogData.="<tr>";
                    $showLogData.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                    $showLogData.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                    $showLogData.="</tr>";
                }
        }               
        return $showLogData;
}


/*end service provider list */
$addAminTempArray = array();
$deleteAminTempArray = array();
$modifyAminTempArray = array();
$GLOBALS['serviceId'] = '';
                                $log = "";
                                $a           = 0;
				$id          = $_REQUEST["changeLogId"];
                                //echo "<br />Md - ".$_REQUEST["changeLogModuleName"];
                                $module      = $_REQUEST["changeLogModuleName"];                                
                                $entity_Name = $_REQUEST["entity_Name"];
                                
				//$log .= "<div style='width: 80%; border: 1px solid red;'>";
                                if ($module == "User Modify" or $module == "Device Type Management Modify" or $module == "Soft Phone Modify" or $module == "Hunt Group Modify" or $module == "Auto Attendant Modify" or $module == "Call Center Modify" or $module == "Device Management Modification" or $module == "Delete User" or $module == "SCA-Only Devices Modification" or $module == "Modify Announcement" or $module == "Group Modify" or $module == "Change User Status" or $module == "Users:Group-Wide Modify" or $module == "Modify Device Config CustomTags" or $module == "Modify Sim Ring" or  $module == "Modify Sim Ring Criteria" or $module == "Service Pack Modify" or $module == "Enterprise Modify" or $module == "User Modify (Modify User's device profile Custom Tags)" or $entity_Name == "BW app server connection") {

                                        if ($module == "User Modify") {
						$tableName = "userModChanges";
					}else if ($module == "Group Modify"){
                                            $tableName = "groupModChanges";
                                        }else if ($module == "Service Pack Modify"){
                                            $tableName = "servicePackModChanges";
                                        }else if($module == "Enterprise Modify"){
                                            $tableName = "enterpriseModifyChanges";
                                        }else if ($module == "Modify Sim Ring Criteria"){
                                            $tableName = "simRingCriteriaModChanges";
                                        }else if ($module == "Modify Sim Ring"){
                                            $tableName = "simRingModChanges";
                                        }else if ($module == "Users:Group-Wide Modify"){
                                            $tableName = "userModChanges";
                                        }else if ($module == "Hunt Group Modify") {
						$tableName = "huntgroupModChanges";
					} else if ($module == "Auto Attendant Modify") {
						$tableName = "aaModChanges";
					}else if($module == "Soft Phone Modify"){
					    $tableName = "softPhoneModChanges";
					} else if ($module == "Call Center Modify") {
						$tableName = "callCenterModChanges";
					} else if($module == "Device Management Modification") {
					    $tableName = "deviceMgmtModChanges";					    
					} else if ($module == "Delete User") {
					    $tableName = "callCenterModChanges";
					}else if ($module == "Change User Status") {
					    $tableName = "userModChanges";
					} else if ($module == "Add Announcement") {
					    $tableName = $ociVersion == "19" ? "announcementAddChanges" : "announcementAddChanges21";
                                        } else if ($module == "SCA-Only Devices Modification") {
                                            $tableName = "deviceInventoryModChanges";
                                        } else if ($module == "Modify Announcement") {
                                            $tableName = "announcementModChanges";
                                        }else if ($module == "Call Pickup Groups Add") {
                                            $tableName = "callPickupGroupAddChanges";
                                        }                                        
                                        else if ($module == "Modify Device Config CustomTags") {
                                            $tableName = "deviceConfigCustomTagsModChanges";
                                        }
                                        else if ($module == "User Modify (Modify User's device profile Custom Tags)") {  
                                            $tableName = "userCustomTagsModChanges";
                                        }
                                        
                                        else if ($module == "Device Type Management Modify"){
                                            $tableName = "deviceTypeManagementChanges";
                                        }
                                        else if ($entity_Name == "BW app server connection"){
                                            $tableName = "serversFailOverChanges";
                                        }
                                        
                                        //$select = "SELECT * from " . $tableName . " where id='" . $id . "'";
                                        $select = "SELECT mod_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from $tableName as mod_tbl INNER JOIN changeLog as clog_tbl ON mod_tbl.id=clog_tbl.id  WHERE mod_tbl.id='$id'  ";
                                        $gth = $expProvDB->expressProvLogsDb->query($select);
                                        
					while ($r = $gth->fetch()) {
					    
                                                if($module == "Enterprise Modify") {
                                                    $serviceIdForAdmin = getServiceIdForAnnouncement($r, $ociVersion, $entity_Name, $module);
                                                    if( strpos($r['field'], "(Add Administrator):")) {
                                                        $addAdminLog = explode("(Add Administrator):", $r['field']);
                                                        $addAminTempArray[$addAdminLog[0]][$addAdminLog[1]] = $r['newValue'];
                                                        continue;
                                                    } else if( strpos($r['field'], "(Delete Administrator):")) {
                                                        $delAdminLog = explode("(Delete Administrator):", $r['field']);
                                                        $deleteAminTempArray[$delAdminLog[0]][$delAdminLog[1]] = $r['newValue'];
                                                        continue;
                                                    } else if( strpos($r['field'], "(Modify Administrator):")) {
                                                        $modAdminLog = explode("(Modify Administrator):", $r['field']);
                                                        $modifyAminTempArray[$modAdminLog[0]][$modAdminLog[1]]['newValue'] = $r['newValue'];
                                                        $modifyAminTempArray[$modAdminLog[0]][$modAdminLog[1]]['oldValue'] = $r['oldValue'];
                                                        continue;
                                                    }
                                                }
					    					    
						if ($a == 0) {

						    $r["serviceId"] = $GLOBALS['serviceId'] =  getServiceIdForAnnouncement($r, $ociVersion, $entity_Name, $module);
        
						    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
						    $log.="<tbody>";
                                                    
                                                    //Code added @ 22 May 2019
                                                    $showData = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                                    $log .= $showData;
                                                    //End code
                                                    
                                                    
						    $log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '&nbsp;</td>';
						    $log.="</tr>";
                                                    
						    $log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $entity_Name . '&nbsp;</td>';
						    $log.="</tr>";
						    
						    if ($module == "User Modify (Modify User's device profile Custom Tags)") {
						        $log.="<tr>";
						        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Type</td>';
						        $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceType"] . '</td>';
						        $log.="</tr>";
						        
						        $log.="<tr>";
						        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Name</td>';
						        $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceName"] . '</td>';
						        $log.="</tr>";
						        
						        $customTags = new UserCustomTagManager($r["deviceName"], $r["deviceType"]);
						        $customTagType = $customTags->checkCustomTagType($r["tagName"], $r["newValue"]);
						        $log.="<tr>";
						        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Tag Type</td>';
						        $log.='<td class="errorTableRows" style="width:50%">' . $customTagType . '</td>';
						        $log.="</tr>";
						        
						        $r["field"] = "Tag Value";
						    }
						    
						    $log.="</tbody>";
						    $log.="</table>"; 
                                                    //table for callPickgroup
						    
						    if( $r["field"] == "Deleted DN\'s" || $r["field"] == "Added DN\'s" ) { 
						        $log.='<table style="width:830px; margin:0 auto;margin-top:30px" class="scroll tablesorter dataTable no-footer tableHeadHeight" cellspacing="0" role="grid" aria-describedby="example_info">';
						        //$log.='<table class="scroll tablesorter dataTable no-footer" style="width:830px; margin: 0 auto; margin-top:30px">';
						        $log.="<thead>";
						        $log.='<tr>';
						        $log.='<th class="thsmall" style="text-align: left !important;">Field Name</th>';
						        $log.='<th class="thsmall" style="text-align: left !important;">Value</th>';
						        $log.="</tr>";
						        $log.="</thead>";
						        $log.="<tbody>";
						        $a = 1;
						        $log.='<tr>';
						        $log.='<th class="errorTableRows" style="width:50%;color:#9c9c9c !important">' . $r["field"] . '</th>';
						        $log.='<th></th>';
                                                        $log.='<th class="errorTableRows" style="width:50%">' . $r["newValue"] . '</th>';
						        $log.="</tr>";
						        
						        continue;
						    }
						    if( $entity_Name == "BW app server connection" ) {
						        $log.='<table style="width:830px; margin:0 auto;margin-top:30px" class="scroll tablesorter dataTable no-footer tableHeadHeight" cellspacing="0" role="grid" aria-describedby="example_info">';
						        $log.="<thead>";
						        $log.='<tr>';
						        $log.='<th class="thsmall" style="text-align: left !important;">Connection Status</th>';
						        $log.='<th class="thsmall" style="text-align: left !important;">Status Message</th>';
						        $log.='<th class="thsmall" style="text-align: left !important;">Server Detail</th>';
						        $log.="</tr>";
						        $log.="</thead>";
						        $log.="<tbody>";
						        $a = 1;
						        $log.='<tr>';
						        $log.='<th class="errorTableRows" style="width:50%;color:#9c9c9c !important">' . $r["connection_status"] . '</th>';
						        $log.='<th class="errorTableRows" style="width:50%">' . $r["status_message"] . '</th>';
						        $log.='<th class="errorTableRows" style="width:50%">' . $r["server_detail"] . '</th>';
						        $log.="</tr>";
						        
						        continue;
						    }
						    else {
						        $log.='<table style="width:830px; margin:0 auto;margin-top:30px" class="scroll tablesorter dataTable no-footer tableHeadHeight" cellspacing="0" role="grid" aria-describedby="example_info">';
						        //$log.='<table class="scroll tablesorter dataTable no-footer" style="width:830px; margin: 0 auto; margin-top:30px">';
						        $log.="<thead>";
						        $log.='<tr>';
						        $log.='<th class="thsmall" style="text-align: left !important;">Field Name</th>';
						        $log.='<th class="thsmall" style="text-align: left !important;">Old Value</th>';
						        $log.='<th class="thsmall" style="text-align: left !important;">New Value</th>';
						        $log.="</tr>";
						        $log.="</thead>";
						        $log.="<tbody>";
						        $a = 1;
						        $log.='<tr>';
						    }
						    
						   
						}
						
						if ($r["oldValue"] == "") {
						     $r["oldValue"] = "&nbsp;";
						}
						if ($r["newValue"] == "") {
						     $r["newValue"] = "&nbsp;";
						}

						$log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["field"].'</th>';
						//$log .= "<div class=\"changeLeftB3\">" . $r["field"] . "</div>";


						if ($r["field"] == "Busy Lamp Fields") {
							$blfOld = "";
							$blfNew = "";
							$blfSelect = "SELECT state, blfUser from blfs where blfId='" . $id . "'";
							$blfResults = $db->query($blfSelect);

							while ($blfRow = $blfResults->fetch()) {
								if ($blfRow["state"] == "oldValue") {
									$blfOld .= $blfRow["blfUser"] . "; ";
								} else if ($blfRow["state"] == "newValue") {
									$blfNew .= $blfRow["blfUser"] . "; ";
								}
							}


							$log.='<th class="changeLogDilogTableWidth change thsmall">'.substr($blfOld, 0, -2).'</th>';
							$log.='<th class="changeLogDilogTableWidth change thsmall">'.substr($blfNew, 0, -2).'</th>';
						} 
                                                else if($r["field"] == "Announcement Type"){
                                                    
                                                       if($ociVersion == "19") {
						        $aOld = "";
						        $aNew = "";
						        $aSelect = "SELECT * from announcement_types where announcement_type_id = '" . $r["newValue"]. "'";
						        $aResults = $db->query($aSelect);
						        $gTthF = $aResults->fetch();						        
						        
                                                        $log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["oldValue"].'</th>';
                                                        $log.='<th class="changeLogDilogTableWidth change thsmall">'.$gTthF["name"].'</th>';
                                                        
						    } else {						        
                                                        $log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["oldValue"].'</th>';
                                                        $log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["announcementType"].'</th>';
						    }
						}
                                                else {
						    $log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["oldValue"].'</th>';
						    $log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["newValue"].'</th>';

						}
						$log.='</tr>';
					}
					
					//$log .= "<div class=\"vertSpacer\">&nbsp;</div>";
					
					//$log.="</thead>";
					$log.="</tbody>";
                                        $log.="</table>";
				}
				if ($module == "User Add") {
				    //$select = "SELECT * from userAddChanges where id = '" . $id . "'";
                                    $select = "SELECT usr_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from userAddChanges as usr_add_tbl INNER JOIN changeLog as clog_tbl ON usr_add_tbl.id=clog_tbl.id  WHERE usr_add_tbl.id='$id'  ";
				    $gth = $expProvDB->expressProvLogsDb->query($select);

					while ($r = $gth->fetch()) {
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
                                            //Code added @ 22 May 2019
                                            $showDataUser = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                            $log .= $showDataUser;
                                            //End code                                            
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Username</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["lastName"] . ", " . $r["firstName"] . '</td>';
					    $log.="</tr>";
        
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Name Dialing First Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["nameDialingFirstName"] . '</td>';
					    $log.="</tr>";
        
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Name Dialing Last Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["nameDialingLastName"] . '</td>';
					    $log.="</tr>";
        
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Phone Number</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["phoneNumber"] ? $r["phoneNumber"] : "None") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceType"] . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Email Address</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' .($r["emailAddress"] ? $r["emailAddress"] : "None"). '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Address</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' .($r["address1"] ? $r["address1"] : "None"). '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Suite</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' .($r["address2"] ? $r["address2"] : "None"). '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">City</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["city"] ? $r["city"] : "None") . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">State/Province</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["state"] ? $r["state"] : "None") . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Zip/Postal Code</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["zip"] ? $r["zip"] : "None") . '</td>';
					    $log.="</tr>";
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Pack</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["servicePacks"] ? $r["servicePacks"] : "None") . '</td>';
					    $log.="</tr>";
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Voice Messaging</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["voiceMessaging"] ? $r["voiceMessaging"] : "None") . '</td>';
					    $log.="</tr>";
                                            
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Shared Call Appreance</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["sharedCallAppreance"] ?$r["sharedCallAppreance"] : "None") . '</td>';
					    $log.="</tr>";
                                            
                                            
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Department</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["department"] ? $r["department"] : "None") . '</td>';
					    $log.="</tr>";
                                   
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Polycom Phone Services</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["polycomPhoneServices"] ? $r["polycomPhoneServices"] : "None") . '</td>';
					    $log.="</tr>";
						
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Custom Contact Directory</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["customContactDirectory"] ? $r["customContactDirectory"] : "None") . '</td>';
					    $log.="</tr>";						
					
					    $log.="</tbody>";
					    $log.="</table>";	
					}
					
					/* New Code found for change log*/
					
					/* Add User Soft Phone detail*/
					$selectSoft = "SELECT * from softPhoneAddChanges where id = '" . $id . "'";
					$gthSoft = $expProvDB->expressProvLogsDb->query($selectSoft);
					while ($rSoft = $gthSoft->fetch()) {

					    
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Soft Phone Account Device Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rSoft["softPhoneAcctDeviceType"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Soft Phone Account Device Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rSoft["softPhoneAcctDeviceName"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Soft Phone Account Line Port Domain</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rSoft["softPhoneAcctLinePortDomain"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="</tbody>";
					    $log.="</table>";
					     
					    /* $log .= "<div class=\"changeLeftB\">SoftPhone Acct Device Type</div>";
					    $log .= "<div class=\"changeD\">" . $rSoft["softPhoneAcctDeviceType"] . "&nbsp;</div>";
					    $log .= "<div class=\"changeB\">SoftPhone Acct Device Name</div>";
					    $log .= "<div class=\"changeD\">" . $rSoft["softPhoneAcctDeviceName"] . "&nbsp;</div>";
					    $log .= "<div class=\"changeLeftB\">SoftPhone Acct LinePort Domain</div>";
					    $log .= "<div class=\"changeD\">" . $rSoft["softPhoneAcctLinePortDomain"] . "&nbsp;</div>";
					    $log .= "<div class=\"vertSpacer\">&nbsp;</div>"; */

					}
					
					/* Add User Counter Path Detail*/
					$selectCounterPath = "SELECT * from counterPathAddChanges where id = '" . $id . "'";
					$gthCounterPath = $expProvDB->expressProvLogsDb->query($selectCounterPath);
					while ($rCounterPath = $gthCounterPath->fetch()) {

					    
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Group Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctGroupname"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Profile Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctProfilename"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account User Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctUsername"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Email Address</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctEmailAddr"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Authentication Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctAuthName"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account SIP Domain</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctSipDomain"] . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Proxy</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctSipProxy"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Device Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctDeviceType"] . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Device Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctDeviceName"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Line Port Domain</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctLinePortDomain"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="</tbody>";
					    $log.="</table>";
					      
					}	
					/* End New Code found for change log*/
					
				}
				 
				 
				 if($module == "Soft Phone Added"){
				    /* Add User Soft Phone detail*/
				    //$selectSoft = "SELECT * from softPhoneAddChanges where id = '" . $id . "'";
				    $selectSoft = "SELECT sftphone_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from softPhoneAddChanges as sftphone_add_tbl INNER JOIN changeLog as clog_tbl ON sftphone_add_tbl.id=clog_tbl.id  WHERE sftphone_add_tbl.id='$id'  ";
                                    
                                    $gthSoft = $expProvDB->expressProvLogsDb->query($selectSoft);
				    while ($rSoft = $gthSoft->fetch()) {
                                                        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                                        $log.="<tbody>";
                                                        
                                                        //Code added @ 23 May 2019
                                                        $showDataSoftPhone = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                                        $log .= $showDataSoftPhone;
                                                        //End code
						 
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SoftPhone Acct Device Type</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rSoft["softPhoneAcctDeviceType"] . '</td>';
							$log.="</tr>";
						
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SoftPhone Acct Device Name</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rSoft["softPhoneAcctDeviceName"] . '</td>';
							$log.="</tr>";
						
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SoftPhone Acct LinePort Domain</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rSoft["softPhoneAcctLinePortDomain"] . '</td>';
							$log.="</tr>";
						
						$log.="</tbody>";
					    $log.="</table>";
						 
				    }
				}
                                
                                if($module == "Service Pack Add"){
				    
				    /* Add User Counter Path Detail*/
				    //$selectServicePack = "SELECT * from servicePackAddChanges where id = '" . $id . "'";
                                    $selectServicePack = "SELECT usr_sp_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from servicePackAddChanges as usr_sp_tbl INNER JOIN changeLog as clog_tbl ON usr_sp_tbl.id=clog_tbl.id  WHERE usr_sp_tbl.id='$id'  ";
                                    
                                    
				    $gthServicePack = $expProvDB->expressProvLogsDb->query($selectServicePack);
				    while ($rServicePack = $gthServicePack->fetch()) {
				        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
						//
							//Code added @ 22 May 2019
                                                        $showDataSp = showClusterAndfqdnInfoONDetailsLog($rServicePack["clusterName"], $rServicePack["bwAppServer"], $rServicePack["fqdn"], $log);                                          
                                                        $log .= $showDataSp;
                                                        //End code
                                                        
                                                        $log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Pack Name</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["spName"] . '</td>';
							$log.="</tr>";
						
						
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Pack Description</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["spDescription"] . '</td>';
							$log.="</tr>";
							
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Available For Use</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["availableForUse"] . '</td>';
							$log.="</tr>";
							
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Pack Quantity</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["servicePackQuantity"] . '</td>';
							$log.="</tr>";
                                                        
                                                        $log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Pack User ServiceList</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . str_replace("; ", ", ", $rServicePack["servicePackUserServiceList"]) . '</td>';
							$log.="</tr>";
						 
						$log.="</tbody>";
					    $log.="</table>";
					}
				}
				
				
				if($module == "Enterprise Add"){
				    
				    /* Add User Counter Path Detail*/
				    //$selectServicePack = "SELECT * from enterpriseAddChanges where id = '" . $id . "'";
                                    $selectEnt = "SELECT usr_ent_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from enterpriseAddChanges as usr_ent_tbl INNER JOIN changeLog as clog_tbl ON usr_ent_tbl.id=clog_tbl.id  WHERE usr_ent_tbl.id='$id'  ";
                                    
                                    $getEnterprise = $expProvDB->expressProvLogsDb->query($selectEnt);
				    while ($rServicePack = $getEnterprise->fetch()) {
                                        
				        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
				        $log.="<tbody>";
				        //
                                        
                                        //Code added @ 22 May 2019
                                        $showDataEnt = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                        $log .= $showDataEnt;
                                        //End code
                                        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Enterprise Id</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["enterpriseId"] . '</td>';
				        $log.="</tr>";
				        
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Enterprise Name</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["enterpriseName"] . '</td>';
				        $log.="</tr>";
				        
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Default Domain</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["defaultDomain"] . '</td>';
				        $log.="</tr>";
				        
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Contact Name</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["contactName"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Contact Phone</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["contactPhone"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Contact Email</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["contactEmail"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Support Email</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["supportEmail"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Address Line 1</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["addressLine1"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Address Line 2</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["addressLine2"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">City</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["city"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">State/Province</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["stateProvince"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Zip/Postal Code</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["zipPostalCode"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Country</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $rServicePack["country"] . '</td>';
				        $log.="</tr>";
				        
				        
				        $log.="</tbody>";
				        $log.="</table>";
				    }
				}
                                
                                
                                
				
                                if($module == "Add DNs"){
				    
				    /* Add User Counter Path Detail*/
				    //$selectDNs = "SELECT * from addDNsChanges where id = '" . $id . "'";
                                    $selectDNs = "SELECT add_dns_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from addDNsChanges as add_dns_tbl INNER JOIN changeLog as clog_tbl ON add_dns_tbl.id=clog_tbl.id  WHERE add_dns_tbl.id='$id'  ";
                                    
				    $gthDNs = $expProvDB->expressProvLogsDb->query($selectDNs);
				    while ($rDNs = $gthDNs->fetch()) {
				        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
						//
                                                        
                                                        //Code added @ 23 May 2019
                                                        $showDataDns = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                                        $log .= $showDataDns;
                                                        //End code
                                                        
                                                        $log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rDNs["serviceId"] . '</td>';
							$log.="</tr>";
                                                        
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Phone Numbers</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rDNs["phoneNumbers"] . '</td>';
							$log.="</tr>";
						
						
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Minimum Phone Numbers</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rDNs["minPhoneNumber"] . '</td>';
							$log.="</tr>";
							
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Maximum Phone Numbers</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rDNs["maxPhoneNumber"] . '</td>';
							$log.="</tr>";
						 
						$log.="</tbody>";
					    $log.="</table>";
					}
				}
                                
                                
				if($module == "Counter Path Account Added"){
				    
				    /* Add User Counter Path Detail*/
				    //$selectCounterPath = "SELECT * from counterPathAddChanges where id = '" . $id . "'";
                                    $selectCounterPath = "SELECT cntr_path_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from counterPathAddChanges as cntr_path_tbl INNER JOIN changeLog as clog_tbl ON cntr_path_tbl.id=clog_tbl.id  WHERE cntr_path_tbl.id='$id'  ";
                                    
                                    $gthCounterPath = $expProvDB->expressProvLogsDb->query($selectCounterPath);
				    while ($rCounterPath = $gthCounterPath->fetch()) {
				        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
						//
                                                        //Code added @ 22 May 2019
                                                        $showDataCntrPath = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                                        $log .= $showDataCntrPath;
                                                        //End code
                                                        
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Group Name</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctGroupname"] . '</td>';
							$log.="</tr>";
						
						
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Profile Name</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctProfilename"] . '</td>';
							$log.="</tr>";
							
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct User Name</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctUsername"] . '</td>';
							$log.="</tr>";
							
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Email Address</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctEmailAddr"] . '</td>';
							$log.="</tr>";
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Authentication Name</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctAuthName"] . '</td>';
							$log.="</tr>";
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct SIP Domain</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctSipDomain"] . '</td>';
							$log.="</tr>";
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Proxy</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctSipProxy"] . '</td>';
							$log.="</tr>";
							
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Device Type</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctDeviceType"] . '</td>';
							$log.="</tr>";
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Device Name</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctDeviceName"] . '</td>';
							$log.="</tr>";
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct LinePort Domain</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctLinePortDomain"] . '</td>';
							$log.="</tr>";
						 
						$log.="</tbody>";
					    $log.="</table>";
					}
				}
				
			/* Group level custom tag */		
		            
                    if ($module == "Add Device Config CustomTags") {
				    //$select = "SELECT * from deviceConfigCustomTagsAddChanges where id = '" . $id . "'";
				    $select = "SELECT dcctag_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from deviceConfigCustomTagsAddChanges as dcctag_tbl INNER JOIN changeLog as clog_tbl ON dcctag_tbl.id=clog_tbl.id  WHERE dcctag_tbl.id='$id'  ";
                                    
                                    $gth = $expProvDB->expressProvLogsDb->query($select);

					while ($r = $gth->fetch()) {
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
                                            //Code added @ 23 May 2019
                                            $showDataDcCusTag = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                            $log .= $showDataDcCusTag;
                                            //End code
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
					    $log.="</tr>";
						
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceType"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Tag Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["tagName"] . '</td>';
					    $log.="</tr>";	
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Tag Value</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["tagValue"] ? $r["tagValue"] : "None") . '</td>';
					    $log.="</tr>";
					
					    $log.="</tbody>";
					    $log.="</table>";	
					}
				}
      
				/* User level custom tag */
				if ($module == "User Modify (Add User's device profile Custom Tags)") {
				    //$select = "SELECT * from userCustomTagsAddChanges where id = '" . $id . "'";
                                    $select = "SELECT usr_CusTag_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from userCustomTagsAddChanges as usr_CusTag_tbl INNER JOIN changeLog as clog_tbl ON usr_CusTag_tbl.id=clog_tbl.id  WHERE usr_CusTag_tbl.id='$id'  ";
                                    
                                    
				    $gth = $expProvDB->expressProvLogsDb->query($select);

					while ($r = $gth->fetch()) {
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
                                            //Code added @ 23 May 2019
                                            $showDataUMAUDPCTag = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                            $log .= $showDataUMAUDPCTag;
                                            //End code
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
					    $log.="</tr>";
						
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceType"] . '</td>';
					    $log.="</tr>";
					    
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceName"] . '</td>';
					    $log.="</tr>";
						
						$customTags = new UserCustomTagManager($r["deviceName"], $r["deviceType"]);
						$customTagType = $customTags->checkCustomTagType($r["tagName"], $r["tagValue"]);
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%"> Tag Type </td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $customTagType . '</td>';
					    $log.="</tr>";
						
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Tag Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["tagName"] . '</td>';
					    $log.="</tr>";	
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Tag Value</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["tagValue"] . '</td>';
					    $log.="</tr>";
					
					    $log.="</tbody>";
					    $log.="</table>";	
					}
					
				}
				
		
				//Code added @ 31 May 2018
				if ($module == "Add Group") {
                                    
				    //$select = "SELECT * from groupAddChanges where id = '" . $id . "'";
                                    $select = "SELECT grp_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from groupAddChanges as grp_add_tbl INNER JOIN changeLog as clog_tbl ON grp_add_tbl.id=clog_tbl.id  WHERE grp_add_tbl.id='$id'  ";
                                    
				    $gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) { 
                                            
                                           
                                            
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
                                            //Code added @ 23 May 2019
                                            $showDataGroup = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                            $log .= $showDataGroup;
                                            //End code
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["groupId"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Group Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["groupName"] ? $r["groupName"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Default Domain</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["defaultDomain"] ? $r["defaultDomain"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Time Zone</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["timeZone"] . '</td>';
					    $log.="</tr>";
					    
					    
                                            if($r['extensionLength']){
                                                $extensionLen = explode(",", $r['extensionLength']);
                                                 if(count($extensionLen)>1){ 
                                                    $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Extension Length(Min)</td>';
                                                        $log.='<td class="errorTableRows" style="width:50%">' .($extensionLen[0] ? $extensionLen[0] : ""). '</td>';
                                                    $log.="</tr>";
                                                    
                                                    $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Extension Length(Max)</td>';
                                                        $log.='<td class="errorTableRows" style="width:50%">' .($extensionLen[1] ? $extensionLen[1] : ""). '</td>';
                                                    $log.="</tr>";
                                                    
                                                    $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Extension Length(Default)</td>';
                                                        $log.='<td class="errorTableRows" style="width:50%">' .($extensionLen[2] ? $extensionLen[2] : ""). '</td>';
                                                    $log.="</tr>";
                                                    
                                               }else{
                                                    $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Extension Length</td>';
                                                        $log.='<td class="errorTableRows" style="width:50%">' .($r["extensionLength"] ? $r["extensionLength"] : ""). '</td>';
                                                    $log.="</tr>";
                                               }
                                            }
                                            
                                            
                                            
                                            /*   $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Extension Length</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' .($r["extensionLength"] ? $r["extensionLength"] : ""). '</td>';
					    $log.="</tr>"; */
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">User Limits</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' .($r["userLimit"] ? $r["userLimit"] : ""). '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Calling Line ID Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' .($r["callingLineIdName"] ? $r["callingLineIdName"] : ""). '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Calling Line ID Group Number</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["callingLineIdDisplayPhoneNumber"] ? $r["callingLineIdDisplayPhoneNumber"] : "") . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Contact Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["contactName"] ? $r["contactName"] : "") . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Contact E-mail</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["contactEmail"] ? $r["contactEmail"] : "") . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Contact Phone</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["contactNumber"] ? $r["contactNumber"] : "") . '</td>';
					    $log.="</tr>";
					 /////////////////////////////////////////////////
					 
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Address Line1</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["addressLine1"] ? $r["addressLine1"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Address Line2</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["addressLine2"] ? $r["addressLine2"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">City</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["city"] ? $r["city"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">State / Province</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["stateOrProvince"] ? $r["stateOrProvince"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Zip / Postal Code</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["zipOrPostalCode"] ? $r["zipOrPostalCode"] : "") . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Country</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["country"] ? $r["country"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="</tbody>";
					    $log.="</table>";
					    
					}
				}
                                //End Code
                                
				/* 
				 *  Site Management Module
				 * Add Sas Testing User 
				 * Show change log details 
				 */
				if ($module == "Add Sas Testing User") {
                                    
				    //$select = "SELECT * from `sitemanagementAddChanges` WHERE `id` = '" . $id . "'";
                                    $select = "SELECT site_magmt_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from sitemanagementAddChanges as site_magmt_add_tbl INNER JOIN changeLog as clog_tbl ON site_magmt_add_tbl.id=clog_tbl.id  WHERE site_magmt_add_tbl.id='$id'  ";
                                    
                                    
				    $gth = $expProvDB->expressProvLogsDb->query($select);
                                    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                    $log.="<tbody>";
                                    
                                    //Code added @ 23 May 2019
                                    $showDataAddSasTestUser = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                    $log .= $showDataAddSasTestUser;
                                    //End code
                                            
				    while ($r = $gth->fetch()) 
                                    {	
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["SAS_NAME"] .'</td>';
					    $log.="</tr>";
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">'. $entity_Name . '</td>';
					    $log.="</tr>";
						
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SAS_IP</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["SAS_IP"] . '</td>';
					    $log.="</tr>";
						
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SAS_NAME</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["SAS_NAME"] . '</td>';
					    $log.="</tr>";
						
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SBC_ADDRESS</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["SBC_ADDRESS"] . '</td>';
					    $log.="</tr>";
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SBC_PORT</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["SBC_PORT"] . '</td>';
					    $log.="</tr>";
					    
				    }
				     $log.="</tbody>"; 
                                    $log.="</table>";
				}
				
				

                                /*
				 * Sim Ring Record
				 * Add Sim Ring Criteria
				 * Show change log details
				 */
                                if ($module == "Add Sim Ring Criteria") {
                                    
				    //$select = "SELECT * from `simRingCriteriaAddChanges` WHERE `id` = '" . $id . "'";
				    $select = "SELECT add_sim_ring_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from simRingCriteriaAddChanges as add_sim_ring_tbl INNER JOIN changeLog as clog_tbl ON add_sim_ring_tbl.id=clog_tbl.id  WHERE add_sim_ring_tbl.id='$id'  ";
                                    
                                    $gth = $expProvDB->expressProvLogsDb->query($select);
                                    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                    $log.="<tbody>";
                                    
                                    
                                    
				    while ($simRingResult = $gth->fetch()) {
				    $unavailableNumber =  $simRingResult['specificPhoneNumbers'];
                                    
                                            //Code added @ 23 May 2019
                                            $showDataSimRing = showClusterAndfqdnInfoONDetailsLog($simRingResult["clusterName"], $simRingResult["bwAppServer"], $simRingResult["fqdn"], $log);                                          
                                            $log .= $showDataSimRing;
                                            //End code

					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $simRingResult["serviceId"] .'</td>';
					    $log.="</tr>";
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Criteria Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">'. $simRingResult["simRingCriteriaDescription"] . '</td>';
					    $log.="</tr>";
						
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Time Schedule</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $simRingResult["timeSchedule"] . '</td>';
					    $log.="</tr>";
						
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Holiday Schedule</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $simRingResult["holidaySchedule"] . '</td>';
					    $log.="</tr>";
						
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Call From</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $simRingResult["callFrom"] . '</td>';
					    $log.="</tr>";
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Private Number</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $simRingResult["privateNumber"] . '</td>';
					    $log.="</tr>";
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Unavailable Number</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $simRingResult["unavailableNumber"] . '</td>';
					    $log.="</tr>";
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Specific PhoneNumbers</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $unavailableNumber . '</td>';
					    $log.="</tr>";
					    
				    }
				     $log.="</tbody>"; 
                                    $log.="</table>";
				}				

				//Code added @ 01 Feb 2019
                                if ($module == "Delete Service Pack") {                                    
				    $tableName = "servicePackModChanges";
                                    
				    //$selDelSrPack = "SELECT * from $tableName WHERE `id` = ? ";
                                    $selDelSrPack = "SELECT sp_mod_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from servicePackModChanges as sp_mod_tbl INNER JOIN changeLog as clog_tbl ON sp_mod_tbl.id=clog_tbl.id  WHERE sp_mod_tbl.id = ?  ";
                                    
                                    $qryDelSrPack = $expProvDB->expressProvLogsDb->prepare($selDelSrPack);
				    $pdoDelSrPack = $qryDelSrPack->execute(array($id));
				    $rs = $qryDelSrPack->fetchAll(PDO::FETCH_ASSOC);
                                    
				    foreach ($rs as $rs1) {
                                        
                                            $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
                                            //Code added @ 23 May 2019
                                            $showDataSpMod = showClusterAndfqdnInfoONDetailsLog($rs1["clusterName"], $rs1["bwAppServer"], $rs1["fqdn"], $log);                                          
                                            $log .= $showDataSpMod;
                                            //End code
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rs1["serviceId"] . '</td>';
					    $log.="</tr>";
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">'. $rs1["entityName"] . '</td>';
					    $log.="</tr>";					   
                                            $log.="</tbody>"; 
                                            $log.="</table>";				        
				    }
				}
                                

                                if ($module == "Delete DNs") {                            

				    $tableName = "removeDNsChanges";
                                    
				    //$selDelSrPack = "SELECT * from " . $tableName . " WHERE `id` = ? ";
                                    $selDelSrPack = "SELECT rmve_dns_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from removeDNsChanges as rmve_dns_tbl INNER JOIN changeLog as clog_tbl ON rmve_dns_tbl.id=clog_tbl.id  WHERE rmve_dns_tbl.id = ?  ";
                                    
                                    $qryDelSrPack = $expProvDB->expressProvLogsDb->prepare($selDelSrPack);
				    $pdoDelSrPack = $qryDelSrPack->execute(array($id));
				    $rs = $qryDelSrPack->fetchAll(PDO::FETCH_ASSOC);
                                    
				    foreach ($rs as $rs1) {
                                        
                                            $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
                                            //Code added @ 23 May 2019
                                            $showDataDelDNs = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                            $log .= $showDataDelDNs;
                                            //End code
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rs1["serviceId"] . '</td>';
					    $log.="</tr>";
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">'. $rs1["entityName"] . '</td>';
					    $log.="</tr>";					   
                                            $log.="</tbody>"; 
                                            $log.="</table>";				        
				    }

				}

				
				
			/*  Delete Sas Testing User
			 * Module Name Delete Sas Testing User
			 * use in place serviceId value table entityName
			*/	
				if ($module == "Delete Sas Testing User") {
				    $tableName = "changeLog";
				    $select = "SELECT * from " . $tableName . " WHERE `id` = ? ";
                                    
				    $pdoRes = $expProvDB->expressProvLogsDb->prepare($select);
				    $pdoArr = $pdoRes->execute(array($id));
				    $rows = $pdoRes->fetchAll(PDO::FETCH_ASSOC);
				    foreach ($rows as $r) {
                                        
                                            $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["entityName"] . '</td>';
					    $log.="</tr>";
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">'. $entity_Name . '</td>';
					    $log.="</tr>";					   
                                            $log.="</tbody>"; 
                                            $log.="</table>";				        
				    }
				}
				/* end */
				
				/*  Delete Device Custom Tag Config.
				 * Module Name Delete Sas Testing User
				 * use in place serviceId value table entityName
				 */
				if ($module == "Delete Device Config CustomTags") {
				    $tableName = "deviceConfigCustomTagsModChanges";
				   // $tableName = "changeLog";
				    //$select = "SELECT * from " . $tableName . " WHERE `id` = ? ";
                                    $select = "SELECT dcctag_mod_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from deviceConfigCustomTagsModChanges as dcctag_mod_tbl INNER JOIN changeLog as clog_tbl ON dcctag_mod_tbl.id=clog_tbl.id  WHERE dcctag_mod_tbl.id = ?  ";
                                    
                                    
				    $pdoRes = $expProvDB->expressProvLogsDb->prepare($select);
				    $pdoArr = $pdoRes->execute(array($id));
				    $rows = $pdoRes->fetchAll(PDO::FETCH_ASSOC);
				    foreach ($rows as $r) {                                        
                                        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                        $log.="<tbody>";
                                        
                                        //Code added @ 23 May 2019
                                        $showDataDCCTag = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                        $log .= $showDataDCCTag;
                                        //End code
                                        
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
                                        $log.="</tr>";
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $r["entityName"] . '</td>';
                                        $log.="</tr>";					   
                                        $log.="</tbody>"; 
                                        $log.="</table>";
				    }
                                    
				}
				/* end */
                                    
				/*  Group Modify Delete Department.
				 * Module Name Delete Sas Testing User
				 * use in place serviceId value table entityName
				 */
				if ($module == "Group Modify Delete Department") {
				    $tableName = "groupModChanges";
				   // $tableName = "changeLog";
				    $select = "SELECT * from " . $tableName . " WHERE `id` = ? ";
				    $pdoRes = $expProvDB->expressProvLogsDb->prepare($select);
				    $pdoArr = $pdoRes->execute(array($id));
				    $rows = $pdoRes->fetchAll(PDO::FETCH_ASSOC);
				    foreach ($rows as $r) {                                        
                                        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                        $log.="<tbody>";
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
                                        $log.="</tr>";
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $r["entityName"] . '</td>';
                                        $log.="</tr>";					   
                                        $log.="</tbody>"; 
                                        $log.="</table>";
				    }
                                    
				}
				/* end */
                                
                                
                                if ($module == "Delete Sim Ring Criteria") {
				    $tableName = "simRingCriteriaModChanges";
				   // $tableName = "changeLog";
				    //$select = "SELECT * from " . $tableName . " WHERE `id` = ? ";
                                    $select = "SELECT sim_ring_mod_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from simRingCriteriaModChanges as sim_ring_mod_tbl INNER JOIN changeLog as clog_tbl ON sim_ring_mod_tbl.id=clog_tbl.id  WHERE sim_ring_mod_tbl.id = ?  ";
                                    
				    $pdoRes = $expProvDB->expressProvLogsDb->prepare($select);
				    $pdoArr = $pdoRes->execute(array($id));
				    $rows = $pdoRes->fetchAll(PDO::FETCH_ASSOC);
				    foreach ($rows as $r) {                                        
                                        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                        $log.="<tbody>";
                                        
                                        //Code added @ 23 May 2019
                                        $showDataSimRing = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                        $log .= $showDataSimRing;
                                        //End code
                                        
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
                                        $log.="</tr>";
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $r["entityName"] . '</td>';
                                        $log.="</tr>";					   
                                        $log.="</tbody>"; 
                                        $log.="</table>";
				    }
                                    
				}
                                
                                

			    
				/* User custom tags delete*/
				if ($module == "User Modify (Delete User's device profile Custom Tags)") {
				    $tableName = "userCustomTagsModChanges";
				    // $tableName = "changeLog";
				    //$select = "SELECT * from " . $tableName . " WHERE `id` = ? ";
                                    $select = "SELECT usr_ctag_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from userCustomTagsModChanges as usr_ctag_tbl INNER JOIN changeLog as clog_tbl ON usr_ctag_tbl.id=clog_tbl.id  WHERE usr_ctag_tbl.id = ?   ";
                                    
                                    
				    $pdoRes = $expProvDB->expressProvLogsDb->prepare($select);
				    $pdoArr = $pdoRes->execute(array($id));
				    $rows = $pdoRes->fetchAll(PDO::FETCH_ASSOC);
				    foreach ($rows as $r) {                                        
                                        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                        $log.="<tbody>";
                                        
                                        //Code added @ 23 May 2019
                                        $showDataUCTag = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                        $log .= $showDataUCTag;
                                        //End code
                                        
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
                                        $log.="</tr>";
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $r["entityName"] . '</td>';
                                        $log.="</tr>";
										
										$log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Type</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $r["deviceType"] . '</td>';
                                        $log.="</tr>";
										
										$log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Name</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $r["deviceName"] . '</td>';
                                        $log.="</tr>";
										
										$customTags = new UserCustomTagManager($r["deviceName"], $r["deviceType"]);
										$customTagType = $customTags->checkCustomTagType($r["tagName"], $r["newValue"]);
										$log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Tag Type</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $customTagType . '</td>';
                                        $log.="</tr>";
										
                                        $log.="</tbody>"; 
                                        $log.="</table>";
				    }
                                    
				}
				
                if ($module == "Delete Announcement") {
				    $tableName = "announcementModChanges";
				     // $tableName = "changeLog";
				    //$select = "SELECT * from " . $tableName . " WHERE `id` = ? ";
                                    $select = "SELECT ann_mod_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from announcementModChanges as ann_mod_tbl INNER JOIN changeLog as clog_tbl ON ann_mod_tbl.id=clog_tbl.id  WHERE ann_mod_tbl.id='$id'  ";
                                    
				    $pdoRes = $expProvDB->expressProvLogsDb->prepare($select);
				    $pdoArr = $pdoRes->execute(array($id));
				    $rows = $pdoRes->fetchAll(PDO::FETCH_ASSOC);
				    foreach ($rows as $r) {
                        $r["serviceId"] = getServiceIdForAnnouncement($r, $ociVersion, $entity_Name, $module);
				        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
						$log.="<tbody>";
                                                
                                                //Code added @ 23 May 2019
                                                $showDataAnnMod = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                                $log .= $showDataAnnMod;
                                                //End code
                                                
						$log.="<tr>";
						$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
						$log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
						$log.="</tr>";
						$log.="<tr>";
						$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
						$log.='<td class="errorTableRows" style="width:50%">'. $r["entityName"] . '</td>';
						$log.="</tr>";					   
						$log.="</tbody>"; 
						$log.="</table>";
				    }
				}
				//End code
				
				if ($module == "New Schedule" or $module == "Soft Phone Delete" or $module == "Edit Schedule" or $module == "Delete Schedule" or $module == "New Event" or $module == "Edit Event" or $module == "Delete Event" or $module == "New Custom Contact Directory" or $module == "Edit Custom Contact Directory" or $module == "Delete User" or $module == "Delete SCA Device"  or $module == "Delete AutoAttendant" or $module == "Delete Device" or $module == "Call Pickup Groups Delete") {
					if ($module == "New Custom Contact Directory" or $module == "Edit Custom Contact Directory") {
						$tableName = "ccdModChanges";
					} else if ($module == "Delete User") {
						$tableName = "userModChanges";
					}else if($module == "Soft Phone Delete"){
					    $tableName = "softPhoneModChanges";
					}else if ($module == "Delete AutoAttendant") {
						$tableName = "aaModChanges";
                                        } else if ($module == "Call Pickup Groups Delete") {
						$tableName = "callPickGroupModchanges";
                                        }else if ($module == "Delete Device") {
					    $tableName = "deviceInventoryModChanges";
					}else if ($module == "Delete SCA Device") {
					    $tableName = "deviceInventoryModChanges";
					}else {
						$tableName = "scheduleModChanges";
					}
					 
					//$select = "SELECT * from " . $tableName . " where id='" . $id . "'";
                                        $select = "SELECT mod_del_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from $tableName as mod_del_tbl INNER JOIN changeLog as clog_tbl ON mod_del_tbl.id=clog_tbl.id  WHERE mod_del_tbl.id='$id'  ";
                                        
					$gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) 
                                        {					    
                                                $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                                $log.="<tbody>";
                                                
                                                //Code added @ 23 May 2019
                                                $showDataModDel = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                                $log .= $showDataModDel;
                                                //End code

                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
                                                $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
                                                $log.="</tr>";

                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
                                                $log.='<td class="errorTableRows" style="width:50%">' . $entity_Name . '</td>';
                                                $log.="</tr>";

                                                $log.="</tbody>"; 
						$log.="</table>";
						 
						 
						if ($module == "New Custom Contact Directory" or $module == "Edit Custom Contact Directory") 
                                                { 
						    $log.='<table class="scroll tablesorter dataTable no-footer tableHeadHeight" style="width:830px; margin: 0 auto; margin-top:30px">';
						    $log.="<thead>";
						    $log.='<tr>';
						    $log.='<th class="thsmall" style="text-align: left !important;">Field Name</th>';
						    $log.='<th class="thsmall" style="text-align: left !important;">New Value</th>';
						   
						    $log.="</tr>";
						    $log.="</thead>";
						    $log.="<tbody>";							
						    $log.="<tr>";							
							
							if ($r["newValue"] == "") 
                                                        {
							     $r["newValue"] = "&nbsp;";
							}
							
							$log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["field"].'</th>';
							$log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["newValue"].'</th>';
							 
							$log.="</tr>";							
							$log.="</tbody>"; //table for field name 
							$log.="</table>";							
						} 
                                                else if ($module == "Delete User") 
                                                {
						    /*$log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $entity_Name . '</td>';
						    $log.="</tr>";*/
						     
                                                } 
                                                else if ($module == "Delete AutoAttendant") 
                                               {
                                                    /*$log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $entity_Name . '</td>';
						    $log.="</tr>";*/
                               
                                                }else if ($module == "Call Pickup Groups Delete") {
                                                    
                                                        /*$log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
                                                        $log.='<td class="errorTableRows" style="width:50%">' . $entity_Name . '</td>';
                                                        $log.="</tr>"; */
							
                                                        //End New work start
                                                }else if ($module == "Delete Device") 
                                                {

                                                    /*$log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $entity_Name . '</td>';
						    $log.="</tr>"; */


                                                } 
                                                else if ($module == "Delete SCA Device") 
                                                {
                                                    /*$log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $entity_Name . '</td>';
						    $log.="</tr>";*/

						} 
                                                else 
                                                {
							if ($module == "New Event" or $module == "Edit Event" or $module == "Delete Event") 
                                                        {							    
							    
							    $log.="<tr>";
							    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Event Name</td>';
							    $log.='<td class="errorTableRows" style="width:50%">' . $r["eventName"] . '</td>';
							    $log.="</tr>";						    
							    
							}
							if ($module == "New Event" or $module == "Edit Event") 
                                                        {
							    
							    $log.="<tr>";
							    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Start Date</td>';
							    $log.='<td class="errorTableRows" style="width:50%">' . $r["startDate"] . '</td>';
							    $log.="</tr>";
							    
							    $log.="<tr>";
							    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Start Time</td>';
							    $log.='<td class="errorTableRows" style="width:50%">' . $r["startTime"] . '</td>';
							    $log.="</tr>";
							    
							    $log.="<tr>";
							    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">End Date</td>';
							    $log.='<td class="errorTableRows" style="width:50%">' . $r["endDate"] . '</td>';
							    $log.="</tr>";
							    
							    $log.="<tr>";
							    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">End Time</td>';
							    $log.='<td class="errorTableRows" style="width:50%">' . $r["endTime"] . '</td>';
							    $log.="</tr>";
							    
							    
							    $log.="<tr>";
							    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Recurs</td>';
							    $log.='<td class="errorTableRows" style="width:50%">' . $r["recurrence"] . '</td>';
							    $log.="</tr>";
							}
						}
						
						$log.="</tbody>"; //service Id and first table
						$log.="</table>";
						///table
					}
				}
				if ($module == "Admin Add" or $module == "Admin Modify" or $module == "Admin Delete") {
					$select = "SELECT * from adminChanges where id='" . $id . "'";
                                        $select = "SELECT admn_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from adminChanges as admn_tbl INNER JOIN changeLog as clog_tbl ON admn_tbl.id=clog_tbl.id  WHERE admn_tbl.id='$id'  ";
                                        
                                        
					$gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) {
					    
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
                                            //Code added @ 23 May 2019
                                            $showDataAdminChanges = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                            $log .= $showDataAdminChanges;
                                            //End code
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["userName"] . '</td>';
					    $log.="</tr>";
					     
                                            if ($module == "Admin Add" or $module == "Admin Modify") {
						    
						    $log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">First Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $r["firstName"] . '</td>';
						    $log.="</tr>";
						    
						    
						    $log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Last Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $r["lastName"] . '</td>';
						    $log.="</tr>";
						    
						    
						    $log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Email Address</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $r["emailAddress"] . '</td>';
						    $log.="</tr>";
							 
						    $log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Disabled</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $r["disabled"] . '</td>';
						    $log.="</tr>";							 
							 
                                                    if ($module == "Admin Add") 
                                                    {
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Super User</td>';
                                                        $log.='<td class="errorTableRows" style="width:50%">' . $r["superUser"] . '</td>';
                                                        $log.="</tr>";
                                                    }
							//$log .= "<div class=\"vertSpacer\">&nbsp;</div>";
						}
						
						$log.="</tbody>";
						$log.="</table>";
					}
				}
				if ($module == "Support Email") {
                                    
					//$select = "SELECT * from supportEmailChanges where id='" . $id . "'";
                                        $select = "SELECT sprt_email_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from supportEmailChanges as sprt_email_tbl INNER JOIN changeLog as clog_tbl ON sprt_email_tbl.id=clog_tbl.id  WHERE sprt_email_tbl.id='$id'  ";
                                        
					$gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) {
					    
                                            //Code added @ 23 May 2019
                                            $showDataSprtEmail = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                            $log .= $showDataSprtEmail;
                                            //End code
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Description</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["description"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Details</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["details"] . '</td>';
					    $log.="</tr>";
					    
					 }
				}

				
				//add announcement
				if ($module == "Add Announcement") {
				    $annTableName = $ociVersion == "19" ? "announcementAddChanges" : "announcementAddChanges21";
                                    
					//$select = "SELECT * from " . $annTableName . " where id = '" . $id . "'";
                                        $select = "SELECT annc_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from $annTableName as annc_add_tbl INNER JOIN changeLog as clog_tbl ON annc_add_tbl.id=clog_tbl.id  WHERE annc_add_tbl.id='$id'  ";
                                        
					$gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) {
					  
                                            if($ociVersion == "19") {
						    $selectType = "SELECT * from announcement_types where announcement_type_id = '" . $r["announcementType"]. "'";
						    $gTth = $db->query($selectType);
						    $gTthF = $gTth->fetch();
						    $annType = $gTthF["name"];
                                            } else {
                                                $annType = $r["announcementType"];
                                            }
                                            
                                            $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                            $log.="<tbody>";
                                            
                                            //Code added @ 23 May 2019
                                            $showDataAnncAdd = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                            $log .= $showDataAnncAdd;
                                            //End code

                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["announcementName"] . '</td>';
					    $log.="</tr>";
        
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Announcement Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["announcementName"] . '</td>';
					    $log.="</tr>";

                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Announcement Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $annType . '</td>';
					    $log.="</tr>";

                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Provider</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["sp"] ? $r["sp"] : "None") . '</td>';
					    $log.="</tr>";

                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Filename</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["filenames"] . '</td>';
					    $log.="</tr>";

                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Group</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["annGroupName"] ? $r["annGroupName"] : "None") . '</td>';
					    $log.="</tr>";
                                            
                                            $log.="</tbody>"; 
                                            $log.="</table>";

					}
				}
                                
                                
                                
                                //Code added for Add Call Pickup Group
                                //Add Call Pickup Groups
                                //New work start
				if ($module == "Call Pickup Groups Add") {
					//$select = "SELECT * from callPickupGroupAddChanges where id = '" . $id . "'";
                                        $select = "SELECT cpg_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from callPickupGroupAddChanges as cpg_add_tbl INNER JOIN changeLog as clog_tbl ON cpg_add_tbl.id=clog_tbl.id  WHERE cpg_add_tbl.id='$id'  ";
                                        
					$gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) {
                                            
                                                $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                                $log.="<tbody>";
                                                
                                                //Code added @ 23 May 2019
                                                $showDataCPG = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                                $log .= $showDataCPG;
                                                //End code
                                                
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
                                                $log.='<td class="errorTableRows" style="width:50%">' . $r["name"] . '</td>';
                                                $log.="</tr>";
                                                
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Call Pickup Group Name</td>';
                                                $log.='<td class="errorTableRows" style="width:50%">' . $r["name"] . '</td>';
                                                $log.="</tr>";
                                                
                                                $log.="</tbody>"; 
                                                $log.="</table>";
											
					}
				}
                                //End Code
                                
				
				//Add Auto Attendant
				if ($module == "Add AutoAttendant") {
					//$select = "SELECT * from autoAttendantAddChanges where id = '" . $id . "'";
                                        $select = "SELECT aa_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from autoAttendantAddChanges as aa_add_tbl INNER JOIN changeLog as clog_tbl ON aa_add_tbl.id=clog_tbl.id  WHERE aa_add_tbl.id='$id'  ";
                                        
					$gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) { 
                                            
                                            $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                            $log.="<tbody>";
                                            
                                            //Code added @ 23 May 2019
                                            $showDataAA = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                            $log .= $showDataAA;
                                            //End code
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["aaName"] . '</td>';
					    $log.="</tr>";
					    
					     
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Announcement Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["aaName"] . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Auto Attendant Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["aaType"] . '</td>';
					    $log.="</tr>";
					    
					     
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Phone Number</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["phoneNumberAdd"] ? $r["phoneNumberAdd"] : "None") . '</td>';
					    $log.="</tr>";
					    

					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Calling Line Id LastName</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["callingLineIdLastName"] ? $r["callingLineIdLastName"] : "None") . '</td>';
					    $log.="</tr>";
					    
					     
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Extension</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["extensionAdd"] ? $r["extensionAdd"] : "None") . '</td>';
					    $log.="</tr>";
					    
					     
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Calling Line Id FirstName</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["callingLineIdFirstName"] ? $r["callingLineIdFirstName"] : "None") . '</td>';
					    $log.="</tr>";
						
						 
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">EV Support</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["eVSupport"] ? $r["eVSupport"] : "None") . '</td>';
					    $log.="</tr>";
					    
					     
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Time Zone</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["timeZone"] ? $r["timeZone"] : "None") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Extension Dialing</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["extensionDialing"] ? $r["extensionDialing"] : "None") . '</td>';
					    $log.="</tr>";
						
						 
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Name Dialing</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["nameDialing"] ? $r["nameDialing"] : "None") . '</td>';
					    $log.="</tr>";
					    
					     
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Name Dialing Entries</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["nameDialingEntries"] ? $r["nameDialingEntries"] : "None") . '</td>';
					    $log.="</tr>";
                                            
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Business Hours Schedule</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["businessHoursSchedule"] ? $r["businessHoursSchedule"] : "None") . '</td>';
					    $log.="</tr>";
                                            
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Holiday Schedule</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["holidaySchedule"] ? $r["holidaySchedule"] : "None") . '</td>';
					    $log.="</tr>";                                            
                                            
                                            if($ociVersion == "21")
                                            {						    
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">First Digit Time out Seconds</td>';
                                                $log.='<td class="errorTableRows" style="width:50%">' . ($r["firstDigitTimeoutSeconds"] ? $r["firstDigitTimeoutSeconds"] : "None") . '</td>';
                                                $log.="</tr>";
                                            }
                                            //$log .= "<div class=\"vertSpacer\">&nbsp;</div>";
                                            
                                            $log.="</tbody>"; 
                                            $log.="</table>";
					}
				}
				
				// Device Inventory Add Sca Device
				if ($module == "Add SCA-Only Devices") {
                                    
				    //$select = "SELECT * from `deviceInventoryAddChanges` WHERE `id` = '" . $id . "'";
                                    $select = "SELECT dInvnt_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.fqdn  from deviceInventoryAddChanges as dInvnt_add_tbl INNER JOIN changeLog as clog_tbl ON dInvnt_add_tbl.id=clog_tbl.id  WHERE dInvnt_add_tbl.id='$id'  ";
                                    
				    $gth = $expProvDB->expressProvLogsDb->query($select);
				    while ($r = $gth->fetch()) {
                                        
                                        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                        $log.="<tbody>";
                                        
                                        //Code added @ 23 May 2019
                                        $showDataDInvt = showClusterAndfqdnInfoONDetailsLog($r["clusterName"], $r["bwAppServer"], $r["fqdn"], $log);                                          
                                        $log .= $showDataDInvt;
                                        //End code

				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceName"] . '</td>';
				        $log.="</tr>";
 
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Type</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceType"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Protocol</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["protocol"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Net Address</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["netAddress"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Port</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["port"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Transport Protocol</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["transportProtocol"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Mac Address</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["macAddress"] . '</td>';
				        $log.="</tr>";				        
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SerialNumber</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["serialNumber"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Description</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["description"] . '</td>';
				        $log.="</tr>";			        
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">STUN Server</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["stunServerNetAddress"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Physical Location</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["physicalLocation"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Shared Call Appearance Users</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["scaUsers"] . '</td>';
				        $log.="</tr>";
                                        
                                        $log.="</tbody>"; 
                                        $log.="</table>";
				        
				    }
				}

				//$log .= "</div>";
				if( count($addAminTempArray) > 0) {
				    $log .= createAdminTable($addAminTempArray, $GLOBALS['serviceId'], $serviceIdForAdmin, $entity_Name, $module = "Added Administrator");
				}
				if( count($deleteAminTempArray) > 0) {
				    $log .= createAdminTable($deleteAminTempArray, $GLOBALS['serviceId'], $serviceIdForAdmin, $entity_Name, $module = "Deleted Administrator");
				}
				if( count($modifyAminTempArray) > 0) {
				    $log .= createAdminTable($modifyAminTempArray, $GLOBALS['serviceId'], $serviceIdForAdmin, $entity_Name, $module = "Modified Administrator");
				}
				
			?>
			
		<?php
		function getServiceIdForAnnouncement($r, $ociVersion, $entity_Name, $module) {
		    if( ($module == "Modify Announcement" || $module == "Delete Announcement") && $ociVersion != "19") {
		        return $entity_Name;
		    } else {
		        return $r["serviceId"];
		    }
		}
		?>
<?php
   echo $log;
  //echo "Test Hello";
  
   function createAdminTable($aminTempArray, $serviceId, $serviceIdForAdmin, $entity_Name, $module) {
       $logTr = "";
       $logAdmin = "";
       
       if( count($aminTempArray) > 0) {
           
           if( $serviceId == "" ) {
               $logAdmin.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
               $logAdmin.="<tbody>";
               $logAdmin.="<tr>";
               $logAdmin.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
               $logAdmin.='<td class="errorTableRows" style="width:50%">' . $serviceIdForAdmin . '&nbsp;</td>';
               $logAdmin.="</tr>";
               $logAdmin.="<tr>";
               $logAdmin.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
               $logAdmin.='<td class="errorTableRows" style="width:50%">' . $entity_Name . '&nbsp;</td>';
               $logAdmin.="</tr>";
               $logAdmin.= "</tbody>";
               $logAdmin.= "</table></br>";
               
               $GLOBALS['serviceId'] = $serviceIdForAdmin;
           }
           
           
           if($module == "Deleted Administrator") {
               foreach($aminTempArray as $adminKey => $adminValue) {
                   $logTr.="<tr>";
                   $logTr.='<td class="errorTableRows" style="color: #9c9c9c !important;width:50%">Aministrator ID</td>';
                   $logTr.='<td class="errorTableRows" style="width:50%">' . $adminValue['Aministrator ID'] . '</td>';
                   $logTr.="</tr>";
               }
               $logAdmin.='<table style="width:830px; margin:0 auto;margin-top:30px" class="scroll tablesorter dataTable no-footer tableHeadHeight" cellspacing="0" role="grid" aria-describedby="example_info">';
               $logAdmin.="<br><strong>".$module."</strong><thead>";
               $logAdmin.='<tr>';
               $logAdmin.='<th class="thsmall" style="text-align: left !important;">Field Name</th>';
               $logAdmin.='<th class="thsmall" style="text-align: left !important;">Value</th>';
               $logAdmin.="</tr>";
               $logAdmin.="</thead>";
               $logAdmin.="<tbody>";
               $logAdmin.= $logTr;
               $logAdmin.="</tbody>";
           }
           else if($module == "Added Administrator") {
               foreach($aminTempArray as $adminKey => $adminValue) {
                   $logTr = "";
                   foreach($adminValue as $adminKey1 => $adminValue1) {
                       $logTr.="<tr>";
                       $logTr.='<td class="errorTableRows" style="color: #9c9c9c !important;width:50%">'.$adminKey1.'</td>';
                       $logTr.='<td class="errorTableRows" style="width:50%">' . $adminValue1 . '</td>';
                       $logTr.="</tr>";
                   }
                   
                   $logAdmin.='<table style="width:830px; margin:0 auto;margin-top:30px" class="scroll tablesorter dataTable no-footer tableHeadHeight" cellspacing="0" role="grid" aria-describedby="example_info">';
                   //$logAdmin.='<table class="scroll tablesorter dataTable no-footer" style="width:830px; margin: 0 auto; margin-top:30px">';
                   $logAdmin.="<strong>".$module."</strong><thead>";
                   $logAdmin.='<tr>';
                   $logAdmin.='<th class="thsmall" style="text-align: left !important;">Field Name</th>';
                   $logAdmin.='<th class="thsmall" style="text-align: left !important;">Value</th>';
                   $logAdmin.="</tr>";
                   $logAdmin.="</thead>";
                   $logAdmin.="<tbody>";
                   $logAdmin.= $logTr;
                   $logAdmin.="</tbody>";
               }
               
           }else if($module == "Modified Administrator") {
               foreach($aminTempArray as $adminKey => $adminValue) {
                   $logTr = "";
                   foreach($adminValue as $adminKey1 => $adminValue1) {
                       $logTr.="<tr>";
                       $logTr.='<td class="errorTableRows" style="color: #9c9c9c !important;width:50%">'.$adminKey1.'</td>';
                       $logTr.='<td class="errorTableRows" style="width:50%">' . $adminValue1['oldValue'] . '</td>';
                       $logTr.='<td class="errorTableRows" style="width:50%">' . $adminValue1['newValue'] . '</td>';
                       $logTr.="</tr>";
                   }
                   
                   $logAdmin.='<table style="width:830px; margin:0 auto;margin-top:30px" class="scroll tablesorter dataTable no-footer tableHeadHeight" cellspacing="0" role="grid" aria-describedby="example_info">';
                   $logAdmin.="<br><strong>".$module."</strong><thead>";
                   $logAdmin.='<tr>';
                   $logAdmin.='<th class="thsmall" style="text-align: left !important;">Field Name</th>';
                   $logAdmin.='<th class="thsmall" style="text-align: left !important;">Old Value</th>';
                   $logAdmin.='<th class="thsmall" style="text-align: left !important;">New Value</th>';
                   $logAdmin.="</tr>";
                   $logAdmin.="</thead>";
                   $logAdmin.="<tbody>";
                   $logAdmin.= $logTr;
                   $logAdmin.="</tbody>";
               }
               
           }
           
       }
       
       return $logAdmin;
   }
?>