<?php
require_once("/var/www/html/Express/config.php");
checkLogin();

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");


require_once("/var/www/lib/broadsoft/login.php");
require_once("/var/www/html/Express/userMod/UserCustomTagManager.php");
require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
$expProvDB = new ExpressProvLogsDB();
//Filters
$filters = array(
                    array('changeLog.enterpriseId', '=', $_SESSION["sp"])
                );

$enterpriseId = $_SESSION['sp'];
$entId        = $_SESSION['sp'];
$startdate    = "";
$enddate      = "";
$module       = "";
$entity_name  = "";
$user_name    = "";
$groupId      = "";
$groupIdName  = "";
$checkedRadio ='';
  if($_POST['module'] =='entChangeLog'){
      $checkedRadio = 'enterprise';
  }
   
if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST['action']=="Show Change Log") {
	
	$filters = array();
    if($_POST['searchChnageLog'] =='system'){
        $checkedRadio = 'system';
    }else{
        $checkedRadio = 'enterprise';
    }
        //Code added @ 24 Jan 2019 
        if (isset($_POST['enterpriseId']) && $_POST['enterpriseId'] != "") {
		$enterpriseId = trim($_POST['enterpriseId']);
                $entId        = trim($_POST['enterpriseId']);
		$query .= " and enterpriseId = '$enterpriseId'";
		$filters[] = array('changeLog.enterpriseId', '=', "$enterpriseId");
	}
        
        if($_POST['enterpriseId'] == ""){
            $entId = "";
            foreach ($filters as $keyFilterRow => $filterRowArr) {              
                if(in_array("changeLog.enterpriseId", $filterRowArr)){
                    unset($filters[$keyFilterRow]);
                }
            }
        }        
        if (isset($_POST['groupId']) && $_POST['groupId'] != "") {
                if($_POST['groupId']!="All Groups"){
                    $groupId     = trim($_POST['groupId']);
                    $groupIdName = trim($_POST['searchGroups']);
                    $query .= " and groupId = '$groupId'";
		    $filters[] = array('changeLog.groupId', '=', "$groupId");
                }
	}        
        //End code
	if (isset($_POST['start_date']) && $_POST['start_date'] != "") {
		$startdate = trim($_POST['start_date']);
		$start_date = date("Y-m-d", strtotime($startdate));
		$filters[] = array('DATE(changeLog.date)', '>=', $start_date);
	}
	if (isset($_POST['end_date']) && $_POST['end_date'] != "") {
		$enddate = trim($_POST['end_date']);
		$end_date = date("Y-m-d", strtotime($enddate));
		$filters[] = array('DATE(changeLog.date)', '<=', $end_date);
	}

	if (isset($_POST['modules']) && $_POST['modules'] != "0") {
		$module = trim($_POST['modules']);
		$query .= " and module LIKE '%{$module}%'";
		$filters[] = array('changeLog.module', '=', "$module");
	}

	if (isset($_POST['entity_name']) && $_POST['entity_name'] != "0") {
		$entity_name = trim($_POST['entity_name']);
		$filters[] = array('changeLog.entityName', '=', "$entity_name");

	}
	if (isset($_POST['user_name']) && $_POST['user_name'] != "0") {
		$user_name = trim($_POST['user_name']);
		$filters[] = array('changeLog.userName', '=', "$user_name");

	}
}
/*session added for changeLog */
$_SESSION['ChangeLogfilters'] = $filters ;
/* end code */
/*get serviceProvider list  */
require_once("/var/www/lib/broadsoft/adminPortal/getServiceProviders.php");
require_once("/var/www/lib/broadsoft/adminPortal/util/CommanUtil.php");
$objUtil = new CommanUtil();
/*get all system level sp list */
$getEnterSPLIST = $objUtil->getsystemLevelEnterPriseList($enterpriseId, $getSPName);
?>

<script src="/Express/enterpriseChangeLog/js/getGroupsListBySP.js"></script>
<link rel="stylesheet" href="/Express/css/custom_bootstrap.css">
<link rel="stylesheet" href="/Express/css/jquery.dataTables.min.css">
<style>
    td.errorTableRows:nth-child(2) {
    color: #9c9c9c !important;
}

.noColumnTable td.errorTableRows:first-child {
   color: #6ea0dc !important;
} 
table#example tbody tr td{display:flex;}
table.dataTable tbody th, table.dataTable tbody td {
     word-break: break-all;
 }
 
div#ui-datepicker-div {
  top: 52px !important;
   left: 15px !important;
}
</style>


<script>
$("#entChangeLogSpSelect").change(function () {
    $('#searchGroups').val('All Groups');
    $('#searchGroup').val('All Groups');
    selectedSP = $(this).val();
    $('#hiddenEnterpriseId').val(selectedSP);
    getGroupList(selectedSP); 
});
</script>
 <script>
     $(function () {
        
        $("#changeLogDialog").dialog({
            
                autoOpen: false,
                width: 1000,
                modal: true,
                position: {my: "top", at: "top"},
                resizable: false,
                closeOnEscape: false,
                open: function(event) {
                        setDialogDayNightMode($(this));
                        $('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('closeButton');
                },
                buttons: {
                        "Close": function()
                        {
                                $(this).dialog("close");
                        }
                }
        });
        
        
        var sourceSwapImage = function() {
            var thisId = this.id;
            var image = $("#"+thisId).children('img');
            var newSource = image.data('alt-src');
            image.data('alt-src', image.attr('src'));
            image.attr('src', newSource);
        }

        $("#downloadCSV1").hover(sourceSwapImage, sourceSwapImage);
        //$('##example_length').after('<div class="dropdown-wrap"></div>');

        $('.ui-dialog-buttonpane').removeClass('.ui-button .ui-widget .ui-state-default .ui-corner-all .ui-button-icon-only .ui-dialog-titlebar-close .ui-state-focus');

        $("#module").html("> View Change Log");
        $("#endUserId").html("");
    
        /* end function code for show details log */
        $(".showDetailLog").click(function (){
                var entChangelogId       = $(this).attr("id");
                var chnageLogModuleId    = $(this).data("module");    
                var changeLogEntityName  = $(this).data("entityname");
                
                $.ajax({
                            type: "POST",
                            url: "enterpriseChangeLog/showDetailLog.php",
                            data: {'changeLogId':entChangelogId , 'changeLogModuleName':chnageLogModuleId, 'entity_Name': changeLogEntityName },
                            beforeSend: function(){
                                $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019
                                $("#changeLogDialog").dialog("open");
                                //$("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
                                $("#changeLogDialog").html("<div style='width: 500px; margin:0px auto; font-size: 12px;'>Loading Change Log Details.... &nbsp;&nbsp; <img src=\"/Express/images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
                             
                             },
                            success: function (result) {
                                $("#changeLogDialog").html(result);
                            }
                });
        });
    });

    $(document).ready(function () {
       var selectedSP = '<?php echo $enterpriseId; ?>';
        $('#hiddenEnterpriseId').val(selectedSP);
        getGroupList(selectedSP) ;
        $("#example").dataTable({
                 "order": [0, "desc"],
                 "paging": true
        });     
        $("#loading").val();        
      //  $('input.date_field').datepicker({dateFormat: 'mm/dd/yy', altFormat: "yy-mm-dd"});
       $('input.date_field').datepicker({dateFormat: 'mm/dd/yy', altFormat: "yy-mm-dd",beforeShow: function(input, obj) {
            $(input).after($(input).datepicker('widget'));
       }});  
    });

    function filterChangeLog(filterForm) {
       var form_data = $(filterForm).serialize();
	   $("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
        $("#loading").show();
       // var form_data = $(filterForm).serialize();
        $.ajax({
            type: "POST",
            url: "enterpriseChangeLog/changes.php",
            data: form_data,
            success: function (result) {
                $("#loading").hide();
                $("#mainBody").html(result);
            }
        });

    }
    
    
    
	 
 /*changeLog event on sytem/Enterprise 
  * code write by sollogics developer 25 jan
  * */        
         
      
         
   $("input[name='searchChnageLog']").on('change', function() {
        var val = $(this).val();
        if(val =='system'){
            $('#hiddenEnterpriseId').val('');
            $('#searchGroups').val('All Groups');
            $('#searchGroup').val('All Groups');
            $('.enterPriseInputDiv').hide();
            getSystemLevelGroupList();
        }
       else{
           
            selectedSP = $("#entChangeLogSpSelect").val();
            $('#hiddenEnterpriseId').val(selectedSP);
            $('#searchGroups').val('All Groups');
            $('#searchGroup').val('All Groups');
            getGroupList(selectedSP);  
            $('.enterPriseInputDiv').show();
        }
         
    });  
        
/*end event code */         
</script>

<?php
if($checkedRadio == 'system') 
{    ?>
        <script>
            $("#changeLogSearchlevelSys").val('system');            
            $('#hiddenEnterpriseId').val('');
            //$('#searchGroups').val('All Groups');
            //$('#searchGroup').val('All Groups');
            $('.enterPriseInputDiv').hide();
            getSystemLevelGroupList();
            //alert('searchChnageLog ');
        </script>
     <?php
}

/*if($checkedRadio == 'enterprise') 
{    ?>
        <script>
          //$("#entChangeLogSpSelect").trigger('change');
          selectedSP = $("#entChangeLogSpSelect").val();
          alert('selectedSP - '+ selectedSP);
            $('#hiddenEnterpriseId').val(selectedSP);
            //$('#searchGroups').val('All Groups');
            //$('#searchGroup').val('All Groups');
            getGroupList(selectedSP);  
            $('.enterPriseInputDiv').show();
           
        </script>
     <?php
}*/
/*end service provider list */
?>

<style>
    .groupAutoClass {
        z-index: 9999 !important;
    }
    
    .ui-autocomplete {
    margin-top: 23.5% !important;
    width: 400px !important;
    max-height: 160px !important;
    position: absulate;
    height: auto !important;
    overflow-y: auto;
}
    
</style> 
<!-- <div class="selectContainer"> -->
<!--change log enterprise ui developement @ Sollogics developer -->
<div class="selectContainer padding-top-zero">

     <div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
         <h2 class="adminUserText" id="">Change Log</h2>
         <div id="entChangeLog" class="userDataClass ui-tabs-panel ui-widget-content ui-corner-bottom" aria-labelledby="device_search_button" role="tabpanel" aria-expanded="true" aria-hidden="false">
             <form name="change_log_form" id="change_log_form" class="" style="width:830px; margin:0 auto;">
                 <input type="hidden" name="action" value="Show Change Log" />
                 <div class="search_entChangeLogDiv">
                     
                     <?php 
                      if($_SESSION["superUser"] == "1"){
                     ?>
                        <div class="row">
                            <div class="form-group" style="text-align:center">
                                <label class="labelText" for="limitSearchTo" style="margin-right: 20px;">Search In :</label>
                              
                              
                                     <input type="radio" class="searchlevel" name="searchChnageLog" id="changeLogSearchlevelSys" value="system" style="width: 5%;" <?php if($checkedRadio=='system'){ echo "checked=checked";}  ?>>
                                <label for="changeLogSearchlevelSys"><span></span></label>
                                 <label class="labelText" for="">System</label>
                                
                               
                                    <input type="radio" class="searchlevel" name="searchChnageLog" id="changeLogSearchlevelEnt" value="enterprise"  style="width: 5%;" <?php if($checkedRadio =='enterprise'){ echo "checked=checked";}  ?>>
                                <label for="changeLogSearchlevelEnt"><span></span></label>
                                  
                                
                                
                                
                                
                                
                                <label class="labelText" for="">Enterprise</label>
                            </div>                      
                        </div>
                     <?php 
                     }
                     ?>

 
                     <div class="row">
                         <?php 
                                $cssStyle = "style='display: block;'";
                                if($checkedRadio == 'system')   {
                                        $cssStyle = "style='display: none;'"; 
                                } 
                         ?>
                         <div class="col-md-6 enterPriseInputDiv" <?php echo  $cssStyle; ?> >
                             <input type="radio" name="searchlevel" class="searchlevelIn" value="group" style="display: none">
                             <div class="form-group">
                                 <label class="labelText" for="limitSearchTo">Enterprise:</label><br>
                                 <div class="dropdown-wrap"> 
                                    <select class="form-control blue-dropdown" name="enterpriseName" id="entChangeLogSpSelect">
                                        <?php echo $getEnterSPLIST; ?>
                                    </select>
                                    <input type="hidden" name="enterpriseId" id="hiddenEnterpriseId" value="<?php echo $_SESSION["sp"]?>" />
                                 </div>
                             </div>
                         </div>
                    
                         <div class="col-md-6">
                             <div class="form-group" id="searchDeviceDiv">
                                 <label class="labelText">Group:</label>
                                 <span class="required">*</span><br> 
                                 
                                 <!-- <input type="text" name="searchChangeGrpVal" id="searchChangeGrpVal" size="35" class="magnify"> -->
                                 <input type="text" class="form-control blue-dropdown magnify" style="overflow:scroll;"  class="autoFill" name="searchGroups" id="searchGroups" <?php if($groupIdName!=""){ echo "value='$groupIdName'"; }else{ echo "value='All Groups'"; } ?>  >
                                 <div id="hidden-stuffChangeLog" style="height: 0px;"></div>
                                 <input class='form-control' type='hidden' name='groupId' id='searchGroup' >
                                 
                                 <div style="text-align: justify; color: red" id="warningErrorMsg"></div>
                                 <div class="col span_12" id="loadingOnChange" style="display: none;">
                                     <img src="/Express/images/ajax-loader.gif">
                                 </div>
                             </div>
                         </div>
                     </div>

                     
                 </div>
                 
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="labelText">Start Date</label>
                                <input value="<?php echo $startdate; ?>" type="text" data-date-format="mm-dd-yyyy" name="start_date" id="start_date" class="form-control date_field">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="labelText">End Date</label>
                                <input value="<?php echo $enddate; ?>" type="text" data-date-format="mm-dd-yyyy" name="end_date" id="end_date" class="form-control date_field">
                        </div>
                    </div>
                </div>
                 
                 
                    <div class="row">
                    <div class="col-md-6">
                            <div class="form-group">
                                    <label class="labelText">Module</label>
                                    <div class="dropdown-wrap">   
                                    <select class="form-control" id="modules" name="modules">
                                            <option value="0">Please Select a Module</option>
                                            <?php
                                            $module_types = get_change_log_modules_enterprise_changelog($entId);

                                            foreach ($module_types as $module_type) {
                                                    ?>
                                                    <option <?php echo $module_type->module == $module ? "SELECTED" : ""; ?> value="<?php echo $module_type->module ?>"><?php echo $module_type->module ?></option>
                                                    <?php
                                            }
                                            ?>
                                            </select>
                                            </div>
                            </div>

                    </div>

                    <div class="col-md-6">
                            <div class="form-group">
                                    <label class="labelText">Modified By</label>
                                    <div class="dropdown-wrap">   
                                    <select class="form-control" id="user_name" name="user_name">
                                            <option value="0">Please Select a User</option>
                                            <?php
                                            $users = get_all_admin_users();
                                            foreach ($users as $user) {
                                                    ?>
                                                    <option <?php echo $user->userName == $user_name ? "SELECTED" : ""; ?> value="<?php echo $user->userName ?>"><?php echo $user->full_name ?></option>
                                                    <?php
                                            }
                                            ?>
                                    </select>
                                    </div>
                            </div>
                    </div>
                </div>
                 
                 <div class="row">
                         <div class="col-md-12">
                             <div class="form-group alignBtn">
                                 <!-- <input type="button" name="searchEntChangeBtn" id="searchEntChangeBtn" value="Search" class="subButton"> -->
                                 <input type="button" onclick="filterChangeLog(this.form);" value="Search" class="subButton"/>
                             </div>
                         </div>
                 </div>

             </form>
         </div>
     </div>
 <!--UI develop end by sollogics developer -->     
   <!-- <div style="clear:both;height:40px;/*width:80%;*/margin:0 auto;text-align:right;margin-bottom:8px;">
	 <input type="" name="downloadCSV" id="downloadCSV1" onclick="location.href='changeLog/printCSV.php';" style="cursor:pointer;"> 
    </div>-->
<div class="" name="downloadCSV" id="downloadCSV1" value="" style="margin-bottom: 8px;">
	<img src="images/icons/download_csv.png"  data-alt-src="images/icons/download_csv_over.png" onclick="location.href='enterpriseChangeLog/printCSV.php';">
	<br>
	<span>Download<br>CSV</span>
	
	</div>
	<div id = "changelogTableId" class="">
	   <!-- Data Append Here -->
	   
		<table class="display table table-bordered table-striped scroll" id="example" cellspacing="0">
			<thead>
			<tr>
				<th class="change thsmall">Date</th>
                                <th class="change thsmall">Enterprise ID</th>
                                <th class="change thsmall">Group ID</th>
				<th class="change thsmall">Module</th>
				<th class="change thsmall">Entity Name</th>
				<th class="change thsmall">Modified By</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$log = "";

			$rows = get_change_logs($filters);
 			//echo "<pre>"; print_r($rows);

			foreach ($rows as $row) {
// 			    print_r($row); 
				$a = 0;
				$id = $row["id"];
                                $enterpriseId = $row["enterpriseId"];
                                if($enterpriseId=="" || $enterpriseId==NULL){
                                    $enterpriseId = "None";
                                }
				echo "<tr>";
				echo "<td class=\"change thsmall\">" . $row["date"] . "</td>";
                                echo "<td class=\"change thsmall\">" . $enterpriseId . "</td>";
                                echo "<td class=\"change thsmall\">" . $row["groupId"] . "</td>";
				// echo "<td class=\"change thsmall\"><a href=\"#\" class=\"showLog\" id=\"" . $id . "\">" . $row["module"] . "</a></td>";
                                echo "<td class=\"change thsmall\"><a href=\"#\" class=\"showDetailLog\" id=\"" . $id . "\" data-module=\"" . $row["module"] . "\"    data-entityname=\"" . $row["entityName"] . "\"         >" . $row["module"] . "</a></td>";
				echo "<td class=\"change thsmall\">" . $row["entityName"] . "</td>";
				echo "<td class=\"change thsmall\">" . ($row["modified_by"] ? $row["modified_by"] : $row["userName"]) . "</td>";
				echo "</tr>";
                                
                                $log .= "<div class=\"hideMe\" id=\"expand_" . $id . "\">";
                                $log .= "</div>";
			}
			?>
			</tbody>
		</table>
	</div>
</div>
<div id="changeLogDialog" class="dialogClass"></div>
	<script>
            $(document).ready(function(){
            //$('#example_length').attr('style', 'width: 00px !important');
            $(".dataTables_length select:before,.dataTables_length label").addClass("dropdown-wrap");   
            }); 
	</script>
