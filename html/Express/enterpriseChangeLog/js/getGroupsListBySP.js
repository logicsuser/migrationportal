$(function() {
    
    //Code added @ 23 Jan 2019 regarding EX-1032
        var groupSearchString = "";
        function decodString(encodedStr) {
            var parser = new DOMParser;
            var dom = parser.parseFromString(
                '<!doctype html><body>' + encodedStr,
                'text/html');
            var decodedString = dom.body.textContent;
            return decodedString;
        }
         $('#searchGroups').on('keyup keypress keydown', function(e) {
              var keyCode = e.keyCode || e.which;
              //Code added @28 March 2018 to fix the issue EX-458
              if (keyCode === 38)
              {
            	  var tmpStrSearch = $("#searchGroups").val();
                  var tmpStr = $.trim(tmpStrSearch);
                  $('#searchGroups').val(decodString(tmpStr.replace('<span class="search_separator">-</span>', '-')));
              }
              
              if (keyCode === 40)
              { 
                    var tmpStrSearch = $("#searchGroups").val();
                    var tmpStr = $.trim(tmpStrSearch);
                    $('#searchGroups').val(decodString(tmpStr.replace('<span class="search_separator">-</span>', '-')));
              }
              if (keyCode === 9)
              { 
                        if($(".ui-state-focus").length){
                              var tmpStrs = $(".ui-state-focus").html();
                              var tmpStr =  $.trim(tmpStrs);
                        }else{
                                              var tmpStr = $.trim(groupSearchString);
                        }
                        groupSearchString = $.trim(tmpStr);
                        $(document).find(".ui-state-focus").addClass("tabGroup");
                        $('#searchGroup').val(tmpStr);
                        $('#searchGroups').val(decodString(tmpStr.replace('<span class="search_separator">-</span>', '-')));
                        //$('#searchGroups').trigger("blur");
              }
              //End code
	      if (keyCode === 13) {
                        e.preventDefault();
                        //var searchVal = $("#searchGroup").val(); //Commented on 28 March 2018 due to fix the issue EX-458
                        //Code added @28 March 2018 to fix the issue EX-458                                
                        var searchTmpNewStr = $("#searchGroups").val();
                        var tmpNewStr = $.trim(searchTmpNewStr);
                        if($(".ui-state-focus").length) {
                               var tmpNewStrs = $(".ui-state-focus").html();
                               var tmpNewStr = $.trim(tmpNewStrs);
                         }else{
                               var tmpNewStr = $.trim(groupSearchString);

                         }
                        groupSearchString = tmpNewStr;
                        $('#searchGroup').val(tmpNewStr);
                        var searchVal = tmpNewStr.replace('<span class="search_separator">-</span>', '-'); 
                        //$('#searchGroups').trigger('blur');
                        //End Code
                        if(searchVal != "") {            
                                var searchGroupName = $.trim(searchVal);                          
                                $('#searchGroups').val(decodString(searchGroupName));
                                if(tmpNewStr.search('span') != -1) {
                                        var searchtmpNewStr = $.trim(tmpNewStr); 
                                        $('#searchGroup').val(searchtmpNewStr);
                                }
                                //$('#search').trigger('click');
                        }
                }
	    });
		
		$("#searchGroups").blur(function(e){
        	var el = $(this).val(); 
        	if(el != "") {
        		var seachIdValue = el;
        		groupSearchString = $.trim(seachIdValue);
                }
        	if($(".ui-state-focus").length){
                        var searchValRes = $(".ui-state-focus").html();
                        var searchVal = $.trim(searchValRes);
                }else{
                        var searchVal = $.trim(groupSearchString);
                }
            
                if(searchVal.search('span') != -1) {
                        //$('#searchGroup').val(searchVal);
                }
                var groudVal = groupSearchString.replace('<span class="search_separator">-</span>', '-');
                if(groudVal != "") {
                        var groudVals = $.trim(groudVal);                          
                        $('#searchGroups').val(decodString(groudVals));

                } else {
                         var groudValgroupSearchString = $.trim(groupSearchString);                          
                         $('#searchGroups').val(decodString(groudValgroupSearchString));
                }   
                
                var groupNameNIdTmp = $('#searchGroups').val();
                console.log(groupNameNIdTmp);
                //alert('Blur  groupNameNId - '+ groupNameNId);
                var tmpStr1       = groupNameNIdTmp.split("-");
                var groupIdN       = tmpStr1[0].trim();
                //alert('Blur  groupId - '+ groupId);
                console.log(groupIdN);
                $('#searchGroup').val(groupIdN);
					
        });
        $(document).on("click", ".groupAutoClass > li > a", function(){
    		var el = $(this);
    		var groupSearchStrings = el[0].innerHTML;
                var groupNameNId = decodString(el[0].innerHTML.replace('<span class="search_separator">-</span>', '-'));
                var tmpStr       = groupNameNId.split("-");
                var groupId      = tmpStr[0].trim();
    		$('#searchGroups').val(decodString(el[0].innerHTML.replace('<span class="search_separator">-</span>', '-')));
    		var groupSearchString = $.trim(groupSearchStrings);
    		$('#searchGroup').val(groupId);
	 });
        //End code
    
    });
        //Code added @ 23 Jan 2019 regarding EX-1032
        function getGroupList(selectedSP) {
        var autoComplete = new Array();
        $.ajax({
                    type: "POST",
                    url: "getGroupsSearchDataEnterpriseLevel.php",
                    data: {serviceProvider: selectedSP},
                    success: function (result) 
                    {
                        var explode = result.split(":");
                        for (var a = 0; a < explode.length; a++) {
                            autoComplete[a] = explode[a];
                        }
                        $("#searchGroups").autocomplete({
                            source: autoComplete,
                            appendTo: "#hidden-stuffChangeLog"
                        }).data("ui-autocomplete")._renderItem = function (ul, item){
                                ul.addClass('groupAutoClass'); //Ul custom class here
                                if(item.value.search('<span class="search_separator">-</span>') > 0){
                                        return $("<li></li>")
                                        .append("<a href='#'>" + item.value + "</a>")
                                        .data("ui-autocomplete-item", item)
                                        .appendTo(ul);
                                }else{
                                        return $("<li class='ui-menu-item'></li>")
                                        .append("<a href='#'>" + item.label + "</a>")
                                        .data("ui-autocomplete-item", item)
                                        .appendTo(ul);						
                                 }
                        };
                    }
                });
        }
        //End code
        
        
        //Code added @ 24 Jan 2019 regarding EX-1032
        function getSystemLevelGroupList() {
        var autoComplete = new Array();
        $.ajax({
                    type: "POST",
                    url: "getGroupsSearchSystemLevelData.php",
                    success: function (result) 
                    {                        
                        var explode = result.split(":");
                        for (var a = 0; a < explode.length; a++) {
                            autoComplete[a] = explode[a];
                        }
                        $("#searchGroups").autocomplete({
                            source: autoComplete
                        }).data("ui-autocomplete")._renderItem = function (ul, item){
                                ul.addClass('groupAutoClass'); //Ul custom class here
                                if(item.value.search('<span class="search_separator">-</span>') > 0){
                                        return $("<li></li>")
                                        .append("<a href='#'>" + item.value + "</a>")
                                        .data("ui-autocomplete-item", item)
                                        .appendTo(ul);
                                }else{
                                        return $("<li class='ui-menu-item'></li>")
                                        .append("<a href='#'>" + item.label + "</a>")
                                        .data("ui-autocomplete-item", item)
                                        .appendTo(ul);						
                                 }
                        };
                        
                    }
                });
        }
        //End code