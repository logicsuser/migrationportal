<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once("/var/www/lib/broadsoft/adminPortal/dns/dns.php");

	function getAvailableNumberRange($numberAssignments) {
	    $availableArr = array();
	    $numbers = array();
	    foreach($numberAssignments as $numberKey => $numberValue) {
	        $numberKeyTemp = explode("-", $numberKey);
	        $numberKeyTemp = $numberKeyTemp[1];
	        if($numberValue['type'] == "Available") {
	            $availableArr[$numberKeyTemp] = $numberValue;
	            $numbers[] = $numberKeyTemp;
	            unset($numberAssignments[$numberKey]);
	        }
	    }
	    $rangeArray = createNumberRange($availableArr, $numbers);
	    foreach($rangeArray as $rangeArrayKey => $rangeArrayVal) {
	        $numberAssignments[$rangeArrayVal]['type'] = "Available";
	    }
	    return $numberAssignments;
	}
	
	function createNumberRange($availableArr, $numbers) {
	    //$numbers = array(1,2, 3, 6, 8, 9);
	    sort($numbers);
	    $groups = array();
	    for($i = 0; $i < count($numbers); $i++)
	    {
	        if($i > 0 && ($numbers[$i - 1] == $numbers[$i] - 1))
	            array_push($groups[count($groups) - 1], $numbers[$i]);
	            else // First value or no match, create a new group
	                array_push($groups, array($numbers[$i]));
	    }
	    
	    $rangeArray = array();
	    foreach($groups as $group)
	    {
	        if(count($group) == 1 || count($group) == 2) // Single value
	            foreach ( $group as $nKey => $nVal){
	                $rangeArray[] = $nVal;
	        }
	        else // Range of values, minimum in [0], maximum in [count($group) - 1]
	            $rangeArray[] = $group[0] . " - <br>" . $group[count($group) - 1];
	    }
	    return $rangeArray;
	}
?>
<script type="text/javascript" src="numbers/js/downloadAsCsv.js"></script>
<script>
	$(function() {
		$("#module").html("> View Numbers");
		$("#endUserId").html("");

		$("#allNumbers").tablesorter();
// 		$("#allNumbersRanges").tablesorter();

		$("#allNumbersRanges").tablesorter({
	         sortList: [[0,0]] 
	    });
		
		$(".numberListRow").click(function()
		{
			if( $(this).find(".numberList").length <= 0 ) {
				return false;
			}
			var thisEl = $(this).find(".numberList");
			var userData = thisEl.attr("id");
			var firstName = thisEl.attr("data-firstname");
			var lastName = thisEl.attr("data-lastname");
			
			var exp = userData.split(":");
			var userId = exp[1];
			var type = exp[0];

			var extension = thisEl.attr("data-extension");
			var phone = thisEl.attr("data-phone");
			if($.trim(phone) != ""){
				userIdValue1 = phone+"x"+extension;
			}else{
				userIdValue1 = userId;
			}
			var userIdValue = userIdValue1+" - "+lastName+", "+firstName;

			if (type == "Normal")
			{
				$("#numberData").html("<div class='col-md-12' id='loader' style='margin-top:10%;text-align:center;'><img src='/Express/images/ajax-loader.gif'></div>");
				$.ajax({
					type: "POST",
					url: "userMod/userMod.php",
                                        beforeSend: function() {
                                            $("#numberData").hide();
                                            $("#numberSubBanner").html("Modify User");
                                            $("#viewNumberLoading").show();
                                        },
					//data: { searchVal: userId, userFname : firstName, userLname : lastName},
					success: function(result)
					{
						$("#numberData").show();
                                                $("#viewNumberLoading").hide();
						$("#numberData").html(result);
						$(".mainBannerModify").hide();
					 	$("#numberSubBanner").html("Modify User");
					 	$("#searchVal").val(userIdValue);
						
						$('#helpUrl').attr('data-module', "userMod");
						setTimeout(function() {
							$("#go").trigger("click");
						}, 2000);
						activeImageSwap(type);
					}
				});
			}
			if (type == "Call Center")
			{
				$.ajax({
					type: "POST",
					url: "callCenters/ccInfo.php",
					data: { ccId: userId },
                                        beforeSend: function() {
                                           $("#numberData").hide();
                                           $("#numberSubBanner").html(type);
                                           $("#viewNumberLoading").show();
                                           $("#numberSubBanner").html("Choose Call Center to Modify");
                                           
                                        },
					success: function(result)
					{
						$("#mainBody").html(result);
                                                $("#viewNumberLoading").hide();
						$('#helpUrl').attr('data-module', "modCallCenter");
						activeImageSwap(type);
					}
				});
			}
			if (type == "Auto Attendant")
			{
				$.ajax({
					type: "POST",
					url: "autoAttendant21/aaMod.php",
					data: { AAId: userId },
                                        beforeSend: function() {
                                            $("#numberData").hide();
                                            $("#numberSubBanner").html("Auto Attendants");
                                            $("#viewNumberLoading").show();
                                        },
					success: function(result)
					{
						$("#mainBody").html(result);
						$('#numberSubBanner').hide();
						$("#viewNumberLoading").hide();
                                                $('#aaInfoLoading').show();
						$('#helpUrl').attr('data-module', "modAA");
 						setTimeout(function(){
 							$('#AAChoice').val(userId);
 							$("#AAChoice").trigger('change');
 						}, 5000);
 						activeImageSwap(type);
						
						//$("#numberSubBanner").html("Auto Attendant");
						
					}
				});
			}
			if (type == "Hunt Group")
			{
				$.ajax({
					type: "POST",
					url: "huntgroups/hgInfo.php",
					data: { hgId: userId },
                                        beforeSend: function() {
                                            $("#numberData").hide();
                                             $("#viewNumberLoading").show();
                                              $("#numberSubBanner").html("Choose Hunt Group to Modify");
                                        },
					success: function(result)
					{
                                                $("#viewNumberLoading").hide();
						$("#mainBody").html(result);
						$('#helpUrl').attr('data-module', "modHunt");
						activeImageSwap(type);
					}
				});
			}
			if (type == "Available")
			{
				var showAvailableNumberRanges = $("#showAvailableNumberRanges").prop('checked');
				if(showAvailableNumberRanges) {
					userId = userId.split(" - ");
					userId = userId[0];
				}
				$.ajax({
					type: "POST",
					url: "userAdd/userAdd.php",
					data: { phoneNumber: userId },
                                        beforeSend: function() {
                                            $("#numberData").hide();
                                            $("#numberSubBanner").html("Add User");
                                            $("#viewNumberLoading").show();
                                        },
					success: function(result)
					{ 
                                                $("#viewNumberLoading").hide();
						$("#mainBody").html(result);
						activeImageSwap(type);
					}
				});
			}
		});

		 var activeImageSwap = function(userType)
		 {
		       
		        if(userType == "Normal"){
		        	previousActiveMenu	= "dids";
			    	currentClickedDiv = "userMod"; 
			     }else if(userType == "Call Center"){
			    	previousActiveMenu	= "dids";
				    currentClickedDiv = "modCallCenter";
				 }else if(userType == "Auto Attendant"){
					previousActiveMenu	= "dids";
				    currentClickedDiv = "modAA";
				 }else if(userType == "Hunt Group"){
					previousActiveMenu	= "dids";
				    currentClickedDiv = "modHunt";
				 }else if(userType == "Available"){
					previousActiveMenu	= "dids";
				    currentClickedDiv = "userAdd";
				 }
		    	      
		    		 
	      		if(previousActiveMenu != "")
	    		{ 
	      		 	$(".navMenu").removeClass("activeNav");
	    			var $thisPrev = $("#"+previousActiveMenu).find('.ImgHoverIcon');
	    	        var newSource = $thisPrev.data('alt-src');
	    	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	    	        $thisPrev.attr('src', newSource);
	    	       
	    		}
	    		// inactive tab
	    		if(currentClickedDiv != ""){
	    			$("#"+currentClickedDiv).addClass("activeNav");
	    			var $thisPrev = $("#"+currentClickedDiv).find('.ImgHoverIcon');
	    	        var newSource = $thisPrev.data('alt-src');
	    	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	    	        $thisPrev.attr('src', newSource);
	    			previousActiveMenu = currentClickedDiv;
	    			$('#helpUrl').attr('data-module',currentClickedDiv); //EX-914 Added code @ 15-11-2018 form Sollogics Developer
	        	}
		       
		    }

		 $("input[name='viewNumberFilter']").click(function() {
			 filterNumberTableData();			
		});

		 $("#showAvailableNumberRanges").change(function() {
			if( $(this).prop('checked') ) {
				 $("#allNumbers").hide();
				 $("#allNumbersRanges").show(); 
			} else {
				 $("#allNumbers").show();
				 $("#allNumbersRanges").hide(); 
			}
			filterNumberTableData();
		});
			
		var onInit = function() {
			$("#showAvailableNumberRanges").prop('checked', true);
			$("#viewNumberFilterAll").prop('checked', true);
			$("#allNumbers").hide();
			$("#allNumbersRanges").show();
			filterNumberTableData();
		}
		onInit();
		
	});
	
	var tableId = "allNumbersRanges";
	var groupId = "<?php echo $_SESSION['groupId']; ?>";
	function filterNumberTableData() {
			var thisVal = $("input[name='viewNumberFilter']:checked").val();
		 	var showAvailableNumberRanges = $("#showAvailableNumberRanges").prop('checked');

		 	if(showAvailableNumberRanges) {
				tableId =  "allNumbersRanges";
			} else {
				tableId =  "allNumbers";
			}
			
			if( thisVal == "All" ) {
				$("#" + tableId + " tbody tr").toggle(true);
			} else if ( thisVal== "Used" ) {
				$("#" + tableId + " tbody tr").filter(function() {
			      	$(this).toggle($(this).find("td:eq(1)").text().indexOf("Available") <= -1)
			    });
			} else if( thisVal== "Available" ) {
				$("#" + tableId + " tbody tr").filter(function() {
			      	$(this).toggle($(this).find("td:eq(1)").text().indexOf("Available") > -1)
			    });
			}

			/* No Data Found */
			$("#" + tableId + " tbody tr").filter(function() {
    			if( $(this).text().indexOf("No Data Available") > -1 ) {
    				$(this).toggle(true);
    			}
		    });
	}
</script>

<?php
    $numberAssignments = array();
	require_once("/var/www/lib/broadsoft/adminPortal/getAllNumberAssignments.php");
?>
<h2 class="addUserText" id="numberSubBanner">Number List</h2>

<div id="numberData" class="selectContainer padding-top-zero">
	 <div class="row delDownIconDiv csvIconDivData">
							<div class="col-md-6">
									<label class="labelText"> Show: </label> <br>
									<input name="viewNumberFilter" id="viewNumberFilterAll" value="All" checked="" type="radio">
                    				<label for="viewNumberFilterAll"><span></span></label>
                    				<label class="labelText filterMargin" for="">All</label>
                    				
                    				<input name="viewNumberFilter" id="viewNumberFilterUsed" value="Used" style="width: 5%;" type="radio"> 
                    				<label for="viewNumberFilterUsed"><span></span></label>
                    				<label class="labelText filterMargin" for="">Used</label>
                    				
                    				<input name="viewNumberFilter" id="viewNumberFilterAvailable" value="Available" style="width: 5%;" type="radio"> 
                    				<label for="viewNumberFilterAvailable"><span></span></label>
                    				<label class="labelText filterMargin" for="">Available</label>
                    				
                    				<input name="showAvailableNumberRanges" id="showAvailableNumberRanges" type="checkbox" value="true" checked="">
        							<label for="showAvailableNumberRanges" class="widthForLabelCheckbox" style="margin-left:30px;"> <span></span></label>
        							<label class="labelText">Show Available Numbers Ranges&nbsp;&nbsp;</label>
    							
							</div>
							<div class="col-md-6 csvFileDownloadAlign">
								 
		 							<div class="col-md-11"></div>
		 							<div class="col-md-1 csvFileDownloadAlign" >
										<div id="downloadCSV1 csvFileDownloadAlign">
                                 			<div id="downloadCSV1 csvFileDownloadAlign">
                                    			<div id="downloadCSV" class="downloadNumbersAsCSV" style="margin-top:0;margin-bottom:10px !important;">
                                	    			<!-- <img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" onclick="location.href='numbers/printCSV.php';"> -->
                                					<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png">
                                					<br><span>Download<br>CSV</span>
                                    			</div>
                                 			</div>
                             			</div>
									</div>
		
		
							 
	
						</div>
			</div>

	<div class="viewDetail autoHeight">
		<div style="zoom:1;">
			<table id="allNumbers" class="tablesorter numbersClass scroll" style="width:100%;margin:0;">
				<thead>
					<tr>
						<th class="thsmallN">Number</th>
						<th class="thsmallN">Type</th>
						<th class="thsmallN">Activated</th>
						<th class="thsmallN">User Id</th>
						
						<th class="thsmallN">Last Name</th>
						<th class="thsmallN">First Name</th>
						<th class="thsmallN">Extension</th>
						<th class="thsmallN">MAC Address</th>
						<th class="thsmallN">Department</th>
					</tr>
				</thead>
				<tbody>
					<?php
// 						$links = array("Auto Attendant", "Call Center", "Hunt Group", "Normal");
						$links = array("Auto Attendant", "Call Center", "Hunt Group", "Normal", "Available");
						if (isset($numberAssignments))
						{
						    if( count($numberAssignments) <= 0 ) {
						        echo "<tr><td colspan=9 style='text-align:center'> No Data Available</td></tr>";
						    } else {
						        foreach ($numberAssignments as $key => $value)
						        {
						            $phoneNumberStatus = "";
						            $userPhoneNumber = "";
						            
						            if(isset($value["userId"]) && !empty($value["userId"])){
						                //get user dn number activation response
						                $userDn = new Dns();
						                $numberActivateResponse = $userDn->getUserDNActivateListRequest($value["userId"]);
						                //echo "<pre>"; print_r($numberActivateResponse); //die;
						                if(empty($numberActivateResponse['Error'])){
						                    $userPhoneNumber = $numberActivateResponse['Success'][0]['phoneNumber'];
						                    $phoneNumberStatus = $numberActivateResponse['Success'][0]['status'];
						                }
						                //code ends
						            }
						            
						            $showHoverEffect = true;
						            if( ! in_array($value["type"], $links)) {
						                  $showHoverEffect = false;
						            }
						            ?>
								<tr class="numberListRow <?php echo !$showHoverEffect ? 'noMouseEvent' : '';?>">
									<?php
										$key = explode("-", $key);
										$key = $key[1];

										if ($value["type"] == "Available" && (isset($_SESSION["permissions"]["addusers"]) and $_SESSION["permissions"]["addusers"] == "1"))
										{
											?>
											<td class="thsmallN"><span class="numberList" id="<?php echo $value["type"] . ":" . $key; ?>"><?php echo $key; ?></span></td>
											<?php
										}
										else
										{
											?>
											<td class="thsmallN"><?php echo $key; ?></td>
											<?php
										}

										if ($value["type"] == "Normal")
										{
											$val = "User";
										}
										else
										{
											$val = $value["type"];
										}
									?>
									<td class="thsmallN"><?php echo $val; ?></td>
									<td class="thsmallN"><?php echo $phoneNumberStatus;?></td>
									<td class="thsmallN">
										<?php
											//Code Added @ 15 March 2019 
											if ($value["type"] == "Normal" && (isset($_SESSION["permissions"]["modifyUsers"]) and $_SESSION["permissions"]["modifyUsers"] == "1"))
											{
													?>
													<span class="numberList" id="<?php echo $value["type"] . ":" . $value["userId"]; ?>" data-firstName="<?php echo $value["firstName"] ?>" data-lastName="<?php echo $value["lastName"] ?>" data-extension="<?php echo $value["extension"] ?>" data-phone="<?php echo $userPhoneNumber ?>"><?php echo $value["userId"]; ?></span>
													<?php
											}
											else if ($value["type"] == "Auto Attendant" && (isset($_SESSION["permissions"]["aamod"]) and $_SESSION["permissions"]["aamod"] == "1"))
											{
													?>
													<span class="numberList" id="<?php echo $value["type"] . ":" . $value["userId"]; ?>" data-firstName="<?php echo $value["firstName"] ?>" data-lastName="<?php echo $value["lastName"] ?>" data-extension="<?php echo $value["extension"] ?>" data-phone="<?php echo $userPhoneNumber ?>"><?php echo $value["userId"]; ?></span>
													<?php
											}
											else if ($value["type"] == "Call Center" && (isset($_SESSION["permissions"]["modcallCenter"]) and $_SESSION["permissions"]["modcallCenter"] == "1"))
											{
													?>
													<span class="numberList" id="<?php echo $value["type"] . ":" . $value["userId"]; ?>" data-firstName="<?php echo $value["firstName"] ?>" data-lastName="<?php echo $value["lastName"] ?>" data-extension="<?php echo $value["extension"] ?>" data-phone="<?php echo $userPhoneNumber ?>"><?php echo $value["userId"]; ?></span>
													<?php
											}
											else if ($value["type"] == "Hunt Group" && (isset($_SESSION["permissions"]["modhuntgroup"]) and $_SESSION["permissions"]["modhuntgroup"] == "1"))
											{
													?>
													<span class="numberList" id="<?php echo $value["type"] . ":" . $value["userId"]; ?>" data-firstName="<?php echo $value["firstName"] ?>" data-lastName="<?php echo $value["lastName"] ?>" data-extension="<?php echo $value["extension"] ?>" data-phone="<?php echo $userPhoneNumber ?>"><?php echo $value["userId"]; ?></span>
													<?php
											}
											else
											{
													echo isset($value["userId"]) ? $value["userId"] : "";
											}
										?>
									</td>
									<td class="thsmallN"><?php echo isset($value["lastName"]) ? $value["lastName"] : ""; ?></td>
									<td class="thsmallN"><?php echo isset($value["firstName"]) ? $value["firstName"] : ""; ?></td>
									<td class="thsmallN"><?php echo isset($value["extension"]) ? $value["extension"] : ""; ?></td>
									<td class="thsmallN"><?php echo isset($value["macAddress"]) ? $value["macAddress"] : ""; ?></td>
									<td class="thsmallN"><?php echo isset($value["department"]) ? $value["department"] : ""; ?></td>
								</tr>
								<?php
							}
						    }
							
						}
					?>
				</tbody>
			</table>
			
            <!-- Range Number List -->
			<table id="allNumbersRanges" class="tablesorter numbersClass scroll" style="width:100%;margin:0;">
				<thead>
					<tr>
						<th class="thsmallN">Number</th>
						<th class="thsmallN">Type</th>
						<th class="thsmallN">Activated</th>
						<th class="thsmallN">User Id</th>
						
						<th class="thsmallN">Last Name</th>
						<th class="thsmallN">First Name</th>
						<th class="thsmallN">Extension</th>
						<th class="thsmallN">MAC Address</th>
						<th class="thsmallN">Department</th>
					</tr>
				</thead>
				<tbody>
					<?php
					
					    $numberAssignmentsRange = getAvailableNumberRange($numberAssignments);
						//$links = array("Auto Attendant", "Call Center", "Hunt Group", "Normal");
					    $links = array("Auto Attendant", "Call Center", "Hunt Group", "Normal", "Available");
						if (isset($numberAssignmentsRange))
						{
						    if( count($numberAssignmentsRange) <= 0 ) {
						        echo "<tr><td colspan=9 style='text-align:center'> No Data Available</td></tr>";
						    } else {
						        foreach ($numberAssignmentsRange as $key => $value)
						        {
						            $phoneNumberStatus = "";
						            $userPhoneNumber = "";
						            
						            if(isset($value["userId"]) && !empty($value["userId"])){
						                //get user dn number activation response
						                $userDn = new Dns();
						                $numberActivateResponse = $userDn->getUserDNActivateListRequest($value["userId"]);
						                //echo "<pre>"; print_r($numberActivateResponse); //die;
						                if(empty($numberActivateResponse['Error'])){
						                    $userPhoneNumber = $numberActivateResponse['Success'][0]['phoneNumber'];
						                    $phoneNumberStatus = $numberActivateResponse['Success'][0]['status'];
						                }
						                //code ends
						            }
						            
						            $showHoverEffect = true;
						            if( ! in_array($value["type"], $links)) {
						                $showHoverEffect = false;
						            }
						            ?>
								<tr class="numberListRow <?php echo !$showHoverEffect ? 'noMouseEvent' : '';?>">
									<?php
									if( $value["type"] != "Available" ) {
									    $key = explode("-", $key);
									    $key = $key[1];
									}

										if ($value["type"] == "Available" && (isset($_SESSION["permissions"]["addusers"]) and $_SESSION["permissions"]["addusers"] == "1"))
										{
											?>
											<td class="thsmallN"><span class="numberList" id="<?php echo $value["type"] . ":" . $key; ?>"><?php echo $key; ?></span></td>
											<?php
										}
										else
										{
											?>
											<td class="thsmallN"><?php echo $key; ?></td>
											<?php
										}

										if ($value["type"] == "Normal")
										{
											$val = "User";
										}
										else
										{
											$val = $value["type"];
										}
									?>
									<td class="thsmallN"><?php echo $val; ?></td>
									<td class="thsmallN"><?php echo $phoneNumberStatus;?></td>
									<td class="thsmallN">

									<?php
											//Code Added @ 15 March 2019 
											if ($value["type"] == "Normal" && (isset($_SESSION["permissions"]["modifyUsers"]) and $_SESSION["permissions"]["modifyUsers"] == "1"))
											{
													?>
													<span class="numberList" id="<?php echo $value["type"] . ":" . $value["userId"]; ?>" data-firstName="<?php echo $value["firstName"] ?>" data-lastName="<?php echo $value["lastName"] ?>" data-extension="<?php echo $value["extension"] ?>" data-phone="<?php echo $userPhoneNumber ?>"><?php echo $value["userId"]; ?></span>
													<?php
											}
											else if ($value["type"] == "Auto Attendant" && (isset($_SESSION["permissions"]["aamod"]) and $_SESSION["permissions"]["aamod"] == "1"))
											{
													?>
													<span class="numberList" id="<?php echo $value["type"] . ":" . $value["userId"]; ?>" data-firstName="<?php echo $value["firstName"] ?>" data-lastName="<?php echo $value["lastName"] ?>" data-extension="<?php echo $value["extension"] ?>" data-phone="<?php echo $userPhoneNumber ?>"><?php echo $value["userId"]; ?></span>
													<?php
											}
											else if ($value["type"] == "Call Center" && (isset($_SESSION["permissions"]["modcallCenter"]) and $_SESSION["permissions"]["modcallCenter"] == "1"))
											{
													?>
													<span class="numberList" id="<?php echo $value["type"] . ":" . $value["userId"]; ?>" data-firstName="<?php echo $value["firstName"] ?>" data-lastName="<?php echo $value["lastName"] ?>" data-extension="<?php echo $value["extension"] ?>" data-phone="<?php echo $userPhoneNumber ?>"><?php echo $value["userId"]; ?></span>
													<?php
											}
											else if ($value["type"] == "Hunt Group" && (isset($_SESSION["permissions"]["modhuntgroup"]) and $_SESSION["permissions"]["modhuntgroup"] == "1"))
											{
													?>
													<span class="numberList" id="<?php echo $value["type"] . ":" . $value["userId"]; ?>" data-firstName="<?php echo $value["firstName"] ?>" data-lastName="<?php echo $value["lastName"] ?>" data-extension="<?php echo $value["extension"] ?>" data-phone="<?php echo $userPhoneNumber ?>"><?php echo $value["userId"]; ?></span>
													<?php
											}
											else
											{
													echo isset($value["userId"]) ? $value["userId"] : "";
											}

									?>

									</td>
									<td class="thsmallN"><?php echo isset($value["lastName"]) ? $value["lastName"] : ""; ?></td>
									<td class="thsmallN"><?php echo isset($value["firstName"]) ? $value["firstName"] : ""; ?></td>
									<td class="thsmallN"><?php echo isset($value["extension"]) ? $value["extension"] : ""; ?></td>
									<td class="thsmallN"><?php echo isset($value["macAddress"]) ? $value["macAddress"] : ""; ?></td>
									<td class="thsmallN"><?php echo isset($value["department"]) ? $value["department"] : ""; ?></td>
								</tr>
								<?php
							}
						  }

						}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="loading" id="viewNumberLoading"><img src="/Express/images/ajax-loader.gif"></div>
