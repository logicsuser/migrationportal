<?php
session_start();
//If two factor is not set, move forward.
if ((!isset($_SESSION["needTwoFactorAuthentication"]) || $_SESSION["needTwoFactorAuthentication"] != 1) && isset($_SESSION["authenticatedUserName"])) {

	require_once("functions.php");
	
	$user = get_user_from_username($_SESSION["authenticatedUserName"]);
/* comment code for ex-634 main_enterprises page redirection if Continue Previous Session is check*/
// 	try { 
// 		$serP = $user->sp_selected;
// 		require_once("/var/www/lib/broadsoft/login.php");
// 		require("/var/www/lib/broadsoft/adminPortal/getAllGroups.php");
// 		if (!is_array($allGroups) || !in_array($user->groupid_selected, $allGroups)) {
// 			//unset($_SESSION["continue_session"]);
// 		}
// 	} catch (Exception $e) {
// 		//unset($_SESSION["continue_session"]);
// 	}
	setLoginSession($user);

	header("Location: ./chooseGroup.php");
	exit;
}

require_once("functions.php");

$user = null;
$error_message = null;

if (isset($_SESSION["twoFactorAuthUserName"])) {
	$user = get_user_from_username($_SESSION["twoFactorAuthUserName"]);
}

if (!$user) {
	header("Location: ./index.php");
	exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['access_code'])) {

	if ($user->access_code == $_POST['access_code']) {

		if (isset($_SESSION["isPasswordReset"]) && isset($_SESSION["newPassword"])) {
			update_user_password($_SESSION["newPassword"], $user->userName);
		}

		setLoginSession($user);
		header("Location: ./chooseGroup.php");
		exit;

	} else {
		$error_message = "Incorrect Code";
	}

} else {

	$smsToken = null;
	$smsSID = null;
	$smsFrom = null;

	if ($systemConfig = get_systemConfig()) {
		$smsToken = $systemConfig->smsToken;
		$smsSID = $systemConfig->smsSID;
		$smsFrom = $systemConfig->smsFrom;
	}

	send_two_factor_sms($user, $smsSID, $smsToken, $smsFrom);
}

$phone_number = $user->user_cell_phone_number;

if (strlen($phone_number) >= 4) {
	$phone_number = ' at ***-***-' . substr($phone_number, -4);
}

require_once("config.php");
require_once("header.php");
?>

<div class="leftMenu"></div>
<div id="mainBody">

	<div class="login-bg">

		<div class="fadeInUp" id="bodyForm" style="height: fit-content;">

			<img src="<?php echo $brandingLogoWebPath; ?>" style="max-height: 120px;width: auto;max-width: 330px;"/>

			<h4 class="centerDesc"><strong>Two Factor Authentication</strong></h4>
			<br/>
			<text>
				A 6 digit code has been sent to you via SMS<?php echo $phone_number ?>.
				<?php
				if (isset($_SESSION["isPasswordReset"])) {
					?>
					Please enter the code below in order to complete your password reset.
					<?php
				} else {
					?>
					Please enter the code below in order to complete your login.
					<?php
				}
				?>
			</text>
			<br/><br/>
			<?php
			if ($error_message) {
				?>
				<div class="error centerDesc"><?php echo $error_message ?></div>
				<br/>
				<?php
			}
			?>
			<form name="authentication" id="login" method="POST">
				<input type="hidden" name="action" id="action" value="text_key">
				<input placeholder="Access Code" type="text" name="access_code" id="access_code" size="6" autocomplete='off'/><br/>

				<br/>
				<input type="submit" style="width:100%;" name="authenticationSubmit" id="authentication_submit" value="Submit">
			</form>
			<br/>
			<text id="resendCode" style="display: none;">Did not get the code yet? <a href="./twoFactorAuthentication.php">Click here to generate a new code.</a></text>

			<div class="clr"></div>
			<?php
			if (isset($_SESSION["isPasswordReset"])) {
				?>
				<a href="reset_password.php" class="pull-left m-t-mini">
					<small>Go Back</small>
				</a>
				<?php
			} else {
				?>
				<a href="index.php" class="pull-left m-t-mini">
					<small>Back to Login</small>
				</a>
				<?php
			}
			?>
		</div>

		<div class="copyright">
			Copyright &copy; AveriStar All rights reserved.
		</div>

	</div>
</div>
<style>
	.error, .error text {

		color: red;
	}
</style>
<script>
    setTimeout(function () {
        $("#resendCode").show();
    }, 15000);
</script>