<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$ccId = $_POST["ccId"];

	$_SESSION["ccNames"] = array(
		"policy" => "Group Policy",
		"queueLength" => "Queue Length",
		"allowCallerToDialEscapeDigit" => "Allow Escape Digit",
		"escapeDigit" => "Escape Digit",
		"codeGroup" => "Call Disposition Codes (Group)",
		"codeACD" => "Call Disposition Codes (ACD)",
		"codeUnavailable" => "Agent Unavailable Codes",
		"enableAgentUnavailableCodes" => "Enable Agent Unavailable Codes",
		"defaultAgentUnavailableCodeOnDND" => "Default code on Do Not Disturb activation",
		"defaultAgentUnavailableCodeOnPersonalCalls" => "Default code on personal calls",
		"defaultAgentUnavailableCodeOnConsecutiveBounces" => "Default code on consecutive bounces",
		"defaultAgentUnavailableCodeOnNotReachable" => "Default code on not reachable",
		"forceUseOfAgentUnavailableCodes" => "Force use of agent unavailable codes",
		"defaultAgentUnavailableCode" => "Default code",
		"ccAgents_100" => "Agents",
		"ccActivateNumber" => "Activate Number",
		"ccPhoneActivateNumber" => "Activate Phone Number"
	);
?>
<script>
	$(function() {
		$("#ccTabs").tabs();
		$("#endUserId").html("<?php echo $ccId; ?>");

		$("#dialogCC").dialog({
			autoOpen: false,
			width: 800,
			modal: true,
			position: { my: "top", at: "top" },
			resizable: false,
			closeOnEscape: false,
			open: function() {
				setDialogDayNightMode($(this));
				$(":button:contains('Complete')").show().addClass('subButton');
				$(":button:contains('More changes')").hide();
				$(":button:contains('Cancel')").show().addClass('cancelButton');
			},
			buttons: {
				"Complete": function() {
					var formData = $("#ccInfo").serialize();
					$.ajax({
						type: "POST",
						url: "callCenters/ccDo.php",
						data: formData,
                                                beforeSend: function(){
                                                    $("html, body").animate({ scrollTop: 0 }, 0); //Code added @07 Feb 2019
                                                },
						success: function(result) {
							$("#dialogCC").dialog("open");
							$("#dialogCC").dialog("option", "title", "Request Complete");
							$("#dialogCC").html(result);
							//$("#dialogCC").append(returnLink);
							$(".ui-dialog-buttonpane button:contains('Complete')").button().hide().addClass('subButton');
							$(".ui-dialog-buttonpane button:contains('Cancel')").button().addClass('cancelButton').hide();
							$(".ui-dialog-buttonpane button:contains('More Changes')").button().show().addClass('cancelButton');
							$(".ui-dialog-buttonpane button:contains('Return to Main')").button().show().addClass('cancelButton');;
							//$(".ui-dialog-titlebar-close", this.parentNode).hide();
							//$(".ui-dialog-buttonpane", this.parentNode).hide();
						 
						
						}
					});
				},	
				 "More Changes": function() {
					 $(this).dialog("close");  
		            	$('html, body').animate({scrollTop: '0px'}, 300);
		            	$("#loading2").show();   
		               $("#ccData").show();
		            	var ccChoice = $("#ccChoice").val();
		            	$("#ccChoice").change(ccChoice);
		            //	getccInfo();
		            	$("#ccChoice").trigger('change');
		            	 $("#ccData").show();
		            //	$("#ccChoice").triger('click'); 
		             
		            //	$(this).dialog("close");   
		            	 
		            },
				"Cancel": function() {
					$(this).dialog("close");
				},
				"Return to Main": function() {
	                location.href="main.php";
				}
			}
		});

		$("#subButton").click(function() {
			pendingProcess.push("Modify Call Center");
			var formData = $("#ccInfo").serialize();
			$.ajax({
				type: "POST",
				url: "callCenters/checkData.php",
				data: formData,
                                beforeSend: function(){
                                    $("html, body").animate({ scrollTop: 0 }, 0); //Code added @07 Feb 2019
                                },
				success: function(result) {
					if(foundServerConErrorOnProcess(result, "Modify Call Center")) {
    					return false;
                  	}
					$("#dialogCC").dialog("open");
					if (result.slice(0, 1) == "1")
					{
						$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
					}
					else
					{
						
						$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled subButton");
					}
					$("#dialogCC").html(result.slice(1));
					$(":button:contains('Complete')").show().addClass('subButton');
					$(":button:contains('Cancel')").show().addClass('cancelButton');
					$(":button:contains('More Changes')").hide().addClass('cancelButton');
					$(":button:contains('Return to Main')").hide().addClass('cancelButton');
				}
			});
		});

		$("#sortable1, #sortable2").sortable({
			placeholder: "ui-state-highlight",
			connectWith: ".connectedSortable",
			cursor: "crosshair",
			update: function(event, ui)
			{
				var ccUsers = "";
				var order = $("#sortable2").sortable("toArray");
				for (i = 0; i < order.length; i++)
				{
					ccUsers += order[i] + ";";
				}
				$("#ccAgents_100").val(ccUsers);
			}
		}).disableSelection();

		function addCodes(codeType)
		{
			$(".codes" + codeType).append("<div class=\"col-md-12\">");
			$(".codes" + codeType).append("<div class=\"col-md-5 codeTypeDiv\"><div class=\"form-group\"><input class=\"form-control\" type=\"text\" name=\"code" + codeType + "[]\" maxlength=\"10\" id=\"code" + codeType + "[]\"></div></div>");
			$(".codes" + codeType).append("<div class=\"col-md-5 codeTypeDiv\"><div class=\"form-group\"><input class=\"form-control\" type=\"text\" name=\"desc" + codeType + "[]\" size=\"45\" maxlength=\"40\" id=\"desc" + codeType + "[]\"></div></div>");
			$(".codes" + codeType).append("<div class=\"col-md-2\"><div class=\"form-group\">&nbsp;</div></div>");
			$(".codes" + codeType).append("</div>");
			
			$(".codes" + codeType).append($("#addCodeBtnDiv" + codeType));
		}

		$("#addCodeGroup").click(function() {
			addCodes("Group");
		});

		$("#addCodeACD").click(function() {
			addCodes("ACD");
		});

		$("#addCodeUnavailable").click(function() {
			addCodes("Unavailable");
		});

		$(".remCode").click(function()
		{
			$(this).parent().parent().parent().remove();
		});

		function enableUnavailableCodes()
		{
			if ($("#enableAgentUnavailableCodes").is(":checked"))
			{
				$("#defaultAgentUnavailableCodeOnDND").removeAttr("disabled");
				$("#defaultAgentUnavailableCodeOnPersonalCalls").removeAttr("disabled");
				$("#defaultAgentUnavailableCodeOnConsecutiveBounces").removeAttr("disabled");
				$("#defaultAgentUnavailableCodeOnNotReachable").removeAttr("disabled");
				$("#forceUseOfAgentUnavailableCodes").removeAttr("disabled");
				enableForcedUnavailableCodes();
			}
			else
			{
				$("#defaultAgentUnavailableCodeOnDND").attr("disabled", "disabled");
				$("#defaultAgentUnavailableCodeOnPersonalCalls").attr("disabled", "disabled");
				$("#defaultAgentUnavailableCodeOnConsecutiveBounces").attr("disabled", "disabled");
				$("#defaultAgentUnavailableCodeOnNotReachable").attr("disabled", "disabled");
				$("#forceUseOfAgentUnavailableCodes").attr("disabled", "disabled");
				enableForcedUnavailableCodes();
			}
		}

		function enableForcedUnavailableCodes()
		{
			if ($("#forceUseOfAgentUnavailableCodes").is(":checked") && $("#enableAgentUnavailableCodes").is(":checked"))
			{
				$("#defaultAgentUnavailableCode").removeAttr("disabled");
			}
			else
			{
				$("#defaultAgentUnavailableCode").attr("disabled", "disabled");
			}
		}

		$("#enableAgentUnavailableCodes").click(function()
		{
			enableUnavailableCodes();
		});

		$("#forceUseOfAgentUnavailableCodes").click(function()
		{
			enableForcedUnavailableCodes();
		});

		enableUnavailableCodes();
		enableForcedUnavailableCodes();
	});
		// tooltip
		$(document).ready(function(){
				$('[data-toggle="tooltip"]').tooltip();   
			});
</script>
<?php
	require_once("/var/www/lib/broadsoft/adminPortal/arrays.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getCallCenterInfo.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getCallCenterUnavailableCodes.php");
	foreach ($callCenterInfo["info"] as $key => $value)
	{
		$_SESSION["callCenterInfo"][$key] = $value;
	}
?>
<form name="ccInfo" id="ccInfo" method="POST" action="#" class="fcorn-registerTwo">
	<div id="ccTabs">
	<div class="divBeforeUl">
		<ul>
			<li><a href="#ccSettings_1" class="tabs" id="ccSettings">Settings</a></li>
			<li><a href="#ccCodesGroup_1" class="tabs" id="ccCodesGroup">Disposition Codes (Group)</a></li>
			<?php 
            if($_SESSION["callCenterInfo"]["info"]["type"] == "Premium"){ ?>
            	<li><a href="#ccCodesACD_1" class="tabs" id="ccCodesACD">Disposition Codes (ACD)</a></li>
            <?php }
            ?>
			<li><a href="#ccCodesUnavailable_1" class="tabs" id="ccCodesUnavailable">Unavailable Codes</a></li>
			<li><a href="#ccCodesUnavailableSettings_1" class="tabs" id="ccCodesUnavailableSettings">Unavailable Codes Settings</a></li>
			<li><a href="#ccAgents_10" class="tabs" id="ccAgents">Agents</a></li>
		</ul>
	</div>
		<input type="hidden" name="ccId" id="ccId" value="<?php echo $ccId; ?>">
		<div id="ccSettings_1" class="">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<h2 class="adminUserText"> Call Center Info </h2>
					</div>
				</div>
			</div>
			
			<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText" for="hgName"> Call Center Name: </label><br/>
							<label class="labelTextGrey phoneNumberMargin"><?php echo $_SESSION["callCenterInfo"]["info"]["name"]; ?></label>
						</div>
					</div>
					
							<?php if(!empty($_SESSION["callCenterInfo"]["info"]["phoneNumber"]))
							{
        					   ?>
            					<div class="col-md-6">
            						<div class="form-group">
            							<label class="labelText" for="hgName"> Phone Number: </label><br/>
            							<label class="labelTextGrey phoneNumberMargin"><?php echo $_SESSION["callCenterInfo"]["info"]["phoneNumber"]; ?></label><br/>
            						</div>
            						<div class="form-group">
        								 <input type="checkbox" name="ccActivateNumber" id="ccActivateNumber" value="Yes" <?php if($_SESSION["callCenterInfo"]["info"]["ccActivateNumber"] == "Yes"){ echo "checked";} ?> style="width:auto"/>
        								 <label for="ccActivateNumber"><span></span></label>
        								 <label class="labelText">Activate Number</label>
        								<input type="hidden" name="ccPhoneActivateNumber" id="ccPhoneActivateNumber" value="<?php echo $_SESSION["callCenterInfo"]["info"]["phoneNumber"]; ?>"/>
            						</div>
            					</div>
    						<?php
						  }
							?>
			</div>
			
		
			<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						<label class="labelText" for="hgName"> Group Policy: </label> <span class="required">*</span>
						<div class="dropdown-wrap">   
						<select name="policy" id="policy">
							<option value=""></option>
							<?php
								foreach ($ccPolicies as $key => $value)
								{
									if ($value == $_SESSION["callCenterInfo"]["info"]["policy"])
									{
										$sel = "SELECTED";
									}
									else
									{
										$sel = "";
									}
									echo "<option value=\"" . $value . "\"" . $sel . ">" . $value . "</option>";
								}
							?>
						</select>
							</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
						<label class="labelText" for="queueLength"> Queue Length: </label> <span class="required">*</span>
						<div class="dropdown-wrap">   
						<select name="queueLength" id="queueLength">
							<option value=""></option>
							<?php
								for ($a = 0; $a <= 525; $a++)
								{
									if ($a == $_SESSION["callCenterInfo"]["info"]["queueLength"])
									{
										$sel = "SELECTED";
									}
									else
									{
										$sel = "";
									}
									echo "<option value=\"" . $a . "\"" . $sel . ">" . $a . "</option>";
								}
							?>
						</select>
							</div>
						</div>
					</div>
			</div>
			
			
			<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						
						<input type="hidden" name="allowCallerToDialEscapeDigit" value="false" />
						<input type="checkbox" name="allowCallerToDialEscapeDigit" id="allowCallerToDialEscapeDigit" value="true"<?php echo $_SESSION["callCenterInfo"]["info"]["allowCallerToDialEscapeDigit"] == "true" ? " checked" : ""; ?>>
						<label for="allowCallerToDialEscapeDigit"><span></span></label>
						<label class="labelText" for="allowCallerToDialEscapeDigit">Allow Escape Digit:</label>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText" for="escapeDigit">Escape Digit</label> <span class="required">*</span>
							<div class="dropdown-wrap">   
							<select name="escapeDigit" id="escapeDigit">
								<option value=""></option>
								<?php
									foreach ($escapeDigits as $key => $value)
									{
										if ($value == $_SESSION["callCenterInfo"]["info"]["escapeDigit"])
										{
											$sel = "SELECTED";
										}
										else
										{
											$sel = "";
										}
										echo "<option value=\"" . $value . "\"" . $sel . ">" . $value . "</option>";
									}
								?>
							</select>
								</div>
						</div>
					</div>
			</div>		
			
		</div>
		
		
		
		<!--end ccSettings_1 div-->
		<?php
    		if($_SESSION["callCenterInfo"]["info"]["type"] == "Premium"){
    		    $codeTypes = array(
    		        "Group" => "Call Disposition Codes (Group)",
    		        "ACD" => "Call Disposition Codes (ACD)",
    		        "Unavailable" => "Agent Unavailable Codes"
    		    );
    		}else{
    		    $codeTypes = array(
    		        "Group" => "Call Disposition Codes (Group)",
    		        "Unavailable" => "Agent Unavailable Codes"
    		    );
    		}
			foreach ($codeTypes as $k => $v)
			{
			?>
				
				<div id="ccCodes<?php echo $k; ?>_1">
				
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<h2 class="adminUserText"> <?php echo $v; ?> </h2>
							</div>
						</div>
					</div>
					
										
					<div class="row">
					  <div class="">
						<div class="form-group">
						  <div>
							 <div class="codes<?php echo $k; ?> ccBox" style="zoom:1;">
							 
							 <div class="marginTopDiv">
								<div style="" class="col-md-5">
									<div class="form-group">
										<label class="labelText" >Code</label><span class="required">*</span>
									</div>
								</div>
								<div style="" class="col-md-5">
									<div class="form-group">
										<label class="labelText" >Description</label>
										<!-- <span class="required">*</span> -->
									</div>
								</div>
								<div style="" class="col-md-2">
									<div class="form-group">
										<label class="labelText" >Remove Code</label>
									</div>
								</div>
							</div>
														
										<?php
											$a = 0;
											if (isset($_SESSION["callCenterInfo"]["info"]["codes" . $k]))
											{
												foreach ($_SESSION["callCenterInfo"]["info"]["codes" . $k] as $key => $value)
												{
														$code = $value["code"];
														$desc = $value["description"];
													?>
													<div class="ccModDiv" id="<?php
															if ($k == "Group")
															{
																echo "a";
															}
															else if ($k == "ACD")
															{
																echo "c";
															}
															else if ($k == "Unavailable")
															{
																echo "b";
															}
														?>_<?php echo $a; ?>">
														<div class="marginTopDiv">
															<div style="" class="col-md-5">
															<div class="form-group">
																<input type="text" class="" name="code<?php echo $k; ?>[]" value="<?php echo $code; ?>" maxlength="10" id="code<?php echo $k; ?>[]" >
															</div>
															</div>
															<div style="" class="col-md-5">
															<div class="form-group">
																<input type="text" class="" name="desc<?php echo $k; ?>[]" value="<?php echo $desc; ?>" size="45" maxlength="40" id="desc<?php echo $k; ?>[]">
															</div>
															</div>
															<div style="" class="col-md-2">
															<div class="form-group">
																<input type="button" class="remCode remCodeBtn deleteBtn removeButtonWidth" value="Remove" id="removeCode<?php echo $k; ?>_<?php echo $a; ?>">
															</div>
															</div>
														</div>
													</div>
													<?php
														$a++;
												}
											}	
										?>
                                    <div class="row" id="addCodeBtnDiv<?php echo $k; ?>">
                                    <div class="col-md-12">
                                    	<div class="col-md-4 marginTopDiv">
                						<div class="form-group">
                						<input class="createNewButton remCodeBtn" type="button" value="Add Code" id="addCode<?php echo $k; ?>">
                						</div>
                					</div>
                                    </div>
                                    </div>
                					
							  </div>
						  </div>
						</div>
					  </div>
					</div>
				</div><!--end ccCodes<?php echo $k; ?>_1 div-->
				<?php
			}
		?>
		<div id="ccCodesUnavailableSettings_1">
			
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<h2 class="adminUserText"> Agent Unavailable Codes Settings </h2>
					</div>
				</div>
			</div>
					

			<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						<div class="marginTopSe">&nbsp;</div>
						<input type="hidden" name="enableAgentUnavailableCodes" value="false" />
						<input class="form-control" type="checkbox" id="enableAgentUnavailableCodes" name="enableAgentUnavailableCodes" value="true"
							<?php
								echo $_SESSION["callCenterInfo"]["info"]["enableAgentUnavailableCodes"] == "true" ? " checked" : "";
								echo count($_SESSION["callCenterInfo"]["info"]["codesUnavailable"]) == 0 ? " disabled=\"true\"" : "";
							?>
						><label for="enableAgentUnavailableCodes"><span></span></label>
						<label class="labelText" for="enableAgentUnavailableCodes">Enable Agent Unavailable Codes</label>
						</div>
					</div>
					
			</div>
			

			<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						<label class="labelText" for="defaultAgentUnavailableCodeOnPersonalCalls">Default code on personal calls</label>
						<div class="dropdown-wrap">   
							<select name="defaultAgentUnavailableCodeOnPersonalCalls" id="defaultAgentUnavailableCodeOnPersonalCalls">
						<option value="">None</option>
						<?php
							if (isset($_SESSION["callCenterInfo"]["info"]["codesUnavailable"]))
							{
								foreach ($_SESSION["callCenterInfo"]["info"]["codesUnavailable"] as $key => $value)
								{
									echo "<option value=\"" . $value["code"] . "\"";
									if ($value["code"] == $_SESSION["callCenterInfo"]["info"]["defaultAgentUnavailableCodeOnPersonalCalls"])
									{
										echo " SELECTED";
									}
									if($value["description"] != ""){
									    echo ">" . $value["code"] . "-" . $value["description"] . "</option>";
									}else{
									    echo ">" . $value["code"] . "</option>";
									}
									
								}
							}
						?>
					</select>
						</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
						<label class="labelText" for="defaultAgentUnavailableCodeOnDND">Default code on Do Not Disturb activation</label>
						<div class="dropdown-wrap">   
							<select name="defaultAgentUnavailableCodeOnDND" id="defaultAgentUnavailableCodeOnDND">
							<option value="">None</option>
							<?php
								if (isset($_SESSION["callCenterInfo"]["info"]["codesUnavailable"]))
								{
									foreach ($_SESSION["callCenterInfo"]["info"]["codesUnavailable"] as $key => $value)
									{
										echo "<option value=\"" . $value["code"] . "\"";
										if ($value["code"] == $_SESSION["callCenterInfo"]["info"]["defaultAgentUnavailableCodeOnDND"])
										{
											echo " SELECTED";
										}
										if($value["description"] != ""){
										    echo ">" . $value["code"] . "-" . $value["description"] . "</option>";
										}else{
										    echo ">" . $value["code"] . "</option>";
										}
									}
								}
							?>
							</select>
							</div>
						</div>
					</div>
			</div>
			

			<div class="row">					
					<div class="col-md-6">
						<div class="form-group">
							<?php
							if ($ociVersion !== "17")
							{
							?>
								<label class="labelText" for="defaultAgentUnavailableCodeOnNotReachable">Default code on not reachable</label>
								<div class="dropdown-wrap">   
									<select name="defaultAgentUnavailableCodeOnNotReachable" id="defaultAgentUnavailableCodeOnNotReachable">
										<option value="">None</option>
										<?php
											if (isset($_SESSION["callCenterInfo"]["info"]["codesUnavailable"]))
											{
												foreach ($_SESSION["callCenterInfo"]["info"]["codesUnavailable"] as $key => $value)
												{
													echo "<option value=\"" . $value["code"] . "\"";
													if ($value["code"] == $_SESSION["callCenterInfo"]["info"]["defaultAgentUnavailableCodeOnNotReachable"])
													{
														echo " SELECTED";
													}
													
													if($value["description"] != "") {
													    echo ">" . $value["code"] . "-" . $value["description"] . "</option>";
													} else {
													    echo ">" . $value["code"] . "</option>";
													}
													
												}
											}
										?>
									</select>
										</div>
								
							<?php
							}
							?>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText" for="defaultAgentUnavailableCodeOnConsecutiveBounces">Default code on consecutive bounces</label>
							<div class="dropdown-wrap">   
								<select name="defaultAgentUnavailableCodeOnConsecutiveBounces" id="defaultAgentUnavailableCodeOnConsecutiveBounces">
							<option value="">None</option>
							<?php
								if (isset($_SESSION["callCenterInfo"]["info"]["codesUnavailable"]))
								{
									foreach ($_SESSION["callCenterInfo"]["info"]["codesUnavailable"] as $key => $value)
									{
										echo "<option value=\"" . $value["code"] . "\"";
										if ($value["code"] == $_SESSION["callCenterInfo"]["info"]["defaultAgentUnavailableCodeOnConsecutiveBounces"])
										{
											echo " SELECTED";
										}
										if($value["description"] != ""){
										    echo ">" . $value["code"] . "-" . $value["description"] . "</option>";
										}else{
										    echo ">" . $value["code"] . "</option>";
										}
										
									}
								}
							?>
							</select>
							</div>
						</div>
					</div>
			</div>	

			
			<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						<label class="labelText" for="defaultAgentUnavailableCode">Default code</label>
						<div class="dropdown-wrap">   
							<select name="defaultAgentUnavailableCode" id="defaultAgentUnavailableCode">
						<option value="">None</option>
						<?php
							if (isset($_SESSION["callCenterInfo"]["info"]["codesUnavailable"]))
							{
								foreach ($_SESSION["callCenterInfo"]["info"]["codesUnavailable"] as $key => $value)
								{
									echo "<option value=\"" . $value["code"] . "\"";
									if ($value["code"] == $_SESSION["callCenterInfo"]["info"]["defaultAgentUnavailableCode"])
									{
										echo " SELECTED";
									}
									if($value["description"] != "") {
									    echo ">" . $value["code"] . "-" . $value["description"] . "</option>";
									} else {
									    echo ">" . $value["code"] . "</option>";
									}
								}
							}
						?>
					</select>
						</div>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="form-group">
						<div class="marginTopSe">&nbsp;</div>
							<input type="hidden" name="forceUseOfAgentUnavailableCodes" value="false" />
							<input class="form-control" type="checkbox" id="forceUseOfAgentUnavailableCodes" name="forceUseOfAgentUnavailableCodes" value="true"
								<?php
									echo $_SESSION["callCenterInfo"]["info"]["forceUseOfAgentUnavailableCodes"] == "true" ? " checked" : "";
								?>
							><label for="forceUseOfAgentUnavailableCodes"><span></span></label>
							<label class="labelText" for="forceUseOfAgentUnavailableCodes">Force use of agent unavailable codes</label>
						</div>
					</div>					
			</div>
				
			
			
		</div>
		
		
		<!--end ccCodesUnavailableSettings_1 div-->
		
		
		<div id="ccAgents_10" class="">
		
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<h2 class="adminUserText"> Agents </h2>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="">
					<div class="col-md-6" style="padding-left:0;">
						<div class="form-group alignCenter">
							<label class="labelTextGrey"> Available Agents <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="You can drag things back and forth"><img class="infoIcon" src="images/NewIcon/info_icon.png"></a></label>
							
							<div class="form-group scrollableListCustom1">
								<ul id="sortable1" class="connectedSortable">
									<?php
										if (isset($_SESSION["callCenterInfo"]["availableAgents"]))
										{
											for ($a = 0; $a < count($_SESSION["callCenterInfo"]["availableAgents"]); $a++)
											{
													$id = $_SESSION["callCenterInfo"]["availableAgents"][$a]["id"] . ":" . $_SESSION["callCenterInfo"]["availableAgents"][$a]["name"];
												?>
												<li class="ui-state-default" id="<?php echo $id; ?>"><?php echo $_SESSION["callCenterInfo"]["availableAgents"][$a]["name"]; ?></li>
												<?php
											}
										}
									?>
								</ul>
							</div>
						
						</div>
					</div>
					
					<div class="col-md-6" style="padding-right:0;">
						<div class="form-group alignCenter">
							<label class="labelTextGrey"> Current Agents <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="You can drag things back and forth"><img class="infoIcon" src="images/NewIcon/info_icon.png"></a></label>
						
							<div class="form-group scrollableListCustom2">
								<ul id="sortable2" class="connectedSortable">
									<?php
										if (isset($_SESSION["callCenterInfo"]["agents"]))
										{
											for ($a = 0; $a < count($_SESSION["callCenterInfo"]["agents"]); $a++)
											{
													$id = $_SESSION["callCenterInfo"]["agents"][$a]["userId"] . ":" . $_SESSION["callCenterInfo"]["agents"][$a]["name"];
												?>
												<li class="ui-state-highlight" id="<?php echo $id; ?>"><?php echo $_SESSION["callCenterInfo"]["agents"][$a]["name"]; ?></li>
												<?php
											}
										}
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>	
			</div>
			
			
			
			
		</div><!--end ccAgents_10 div-->
		<input type="hidden" name="codeGroup[]" id="codeGroup[]" value="NIL">
		<input type="hidden" name="descGroup[]" id="descGroup[]" value="NIL">
		<input type="hidden" name="codeACD[]" id="codeACD[]" value="NIL">
		<input type="hidden" name="descACD[]" id="descACD[]" value="NIL">
		<input type="hidden" name="codeUnavailable[]" id="codeUnavailable[]" value="NIL">
		<input type="hidden" name="descUnavailable[]" id="descUnavailable[]" value="NIL">
		<input type="hidden" name="ccAgents_100" id="ccAgents_100" value="NIL">
	</div>
	<div class="alignBtn" style="text-align:center;"><input type="button" class="subButtonHG" id="subButton" value="Confirm Settings" onClick="window.location='#top'"></div>
</form>
<div id="dialogCC" class="dialogClass"></div>
