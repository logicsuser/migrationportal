<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();

	$hidden = array("ccId", "ccPhoneActivateNumber");
	$required = array("policy", "queueLength", "escapeDigit");

	$data = $errorTableHeader;
	
	if(!array_key_exists("ccActivateNumber", $_POST)){
		$_POST['ccActivateNumber'] = "No";
	}
	
	$changes = 0;
	$error = 0;
	foreach ($_POST as $key => $value)
	{
		if (!in_array($key, $hidden)) //hidden form fields that don't change don't need to be checked or displayed
		{
			if($key == "ccActivateNumber" && $value !== $_SESSION["callCenterInfo"]["info"][$key]){
				
				$changes++;
				$bg = CHANGED;
				if($value == "Yes"){
				    $value = $_SESSION["callCenterInfo"]["info"]["phoneNumber"]. " have been Activated";
				}else{
				    $value = $_SESSION["callCenterInfo"]["info"]["phoneNumber"]. " have been Deactivated";
				}
			}
			
			if ($key !== "codeGroup" and $key !== "descGroup" and $key !== "codeACD" and $key !== "descACD" and $key !== "codeUnavailable" and $key !== "descUnavailable" and $key !== "ccAgents_100" and $value !== $_SESSION["callCenterInfo"]["info"][$key])
			{
				$changes++;
				$bg = CHANGED;
			}
			else
			{
				$bg = UNCHANGED;
			}

//			if ($key == "allowCallerToDialEscapeDigit" and $value == "true" and $_POST["escapeDigit"] == "")
//			{
//				$error = 1;
//				$bg = INVALID;
//				$value = "If " . $_SESSION["ccNames"][$key] . " is true, Escape Digit must be set.";
//			}
			if ($key == "forceUseOfAgentUnavailableCodes" and $value == "true" and $_POST["defaultAgentUnavailableCode"] == "")
			{
				$error = 1;
				$bg = INVALID;
				$value = "A default unavailable code is required when the use of codes is forced.";
			}
			if ($key == "defaultAgentUnavailableCode" and $value !== "" and !in_array($value, $_POST["codeUnavailable"]))
			{
				$error = 1;
				$bg = INVALID;
				$value = $_SESSION["ccNames"][$key] . " cannot be deleted from unavailable codes list.";
			}
			if ($key == "codeGroup" or $key == "codeACD" or $key == "codeUnavailable")
			{
				$oldCodes = "";
				$newCodes = "";
				//check if codes/descriptions changed; if so, make note of changes for logging purposes
				if ($key == "codeGroup")
				{
					foreach ($_SESSION["callCenterInfo"]["info"]["codesGroup"] as $kk => $vv)
					{
						$oldCodes .= $vv["code"] . ":" . $vv["description"] . "; ";
					}
					foreach ($value as $k => $v)
					{
						if ($_POST["codeGroup"][$k] != "NIL")
						{
							$newCodes .= $_POST["codeGroup"][$k] . ":" . $_POST["descGroup"][$k] . "; ";
						}
					}
				}
				if ($key == "codeACD")
				{
					if (isset($_SESSION["callCenterInfo"]["info"]["codesACD"]))
					{
						foreach ($_SESSION["callCenterInfo"]["info"]["codesACD"] as $kk => $vv)
						{
							$oldCodes .= $vv["code"] . ":" . $vv["description"] . "; ";
						}
					}
					foreach ($value as $k => $v)
					{
						if ($_POST["codeACD"][$k] != "NIL")
						{
							$newCodes .= $_POST["codeACD"][$k] . ":" . $_POST["descACD"][$k] . "; ";
						}
					}
				}
				if ($key == "codeUnavailable")
				{
					foreach ($_SESSION["callCenterInfo"]["info"]["codesUnavailable"] as $kk => $vv)
					{
						$oldCodes .= $vv["code"] . ":" . $vv["description"] . "; ";
					}
					foreach ($value as $k => $v)
					{
						if ($_POST["codeUnavailable"][$k] != "NIL")
						{
							$newCodes .= $_POST["codeUnavailable"][$k] . ":" . $_POST["descUnavailable"][$k] . "; ";
						}
					}
				}
				if ($oldCodes !== $newCodes)
				{
					$changes++;
					$bg = CHANGED;
				}

				$codeData = "";
				foreach ($value as $k => $v)
				{
					if ($v != "NIL")
					{
						if (($key == "codeGroup" or $key == "codeACD") and (!is_numeric($v) or ($v < 1000 or $v > 9999)))
						{
							$error = 1;
							$bg = INVALID;
							$bg2 = $bg;
							$description = "Code must be a number from 1000-9999.";
						}
						else if ($key == "codeGroup" and strlen($_POST["descGroup"][$k]) > 0 and !preg_match("/^[ A-Za-z0-9-_]+$/", $_POST["descGroup"][$k]))
						{
							$error = 1;
							$bg = INVALID;
							$bg2 = $bg;
							$description = $_POST["descGroup"][$k] . " Description must be letters, numbers, and dashes only.";
						}
						else if ($key == "codeACD" and strlen($_POST["descACD"][$k]) > 0 and !preg_match("/^[ A-Za-z0-9-_]+$/", $_POST["descACD"][$k]))
						{
							$error = 1;
							$bg = INVALID;
							$bg2 = $bg;
							$description = $_POST["descACD"][$k] . " Description must be letters, numbers, and dashes only.";
						}
						else if ($key == "codeUnavailable" and strlen($v) == 0)
						{
							$error = 1;
							$bg = INVALID;
							$bg2 = $bg;
							$description = "Code is a required field.";
						}
						else
						{
							$bg2 = UNCHANGED;
							if ($key == "codeGroup")
							{
								$description = $_POST["descGroup"][$k];
							}
							if ($key == "codeACD")
							{
								$description = $_POST["descACD"][$k];
							}
							if ($key == "codeUnavailable")
							{
								$description = $_POST["descUnavailable"][$k];
							}
						}
						$codeData .= "<tr bgcolor=\"" . $bg2 . "\"><td class=\"dialogClass\">" . $v . "</td><td class=\"dialogClass\">" . $description . "</td></tr>";
					}
				}
				if ($codeData == "")
				{
					$codeData = "<tr bgcolor=\"" . $bg2 . "\"><td class=\"dialogClass\">None</td></tr>";
				}
				unset($value);
				$value = "<table>" . $codeData . "</table>";
			}
			if ($key == "ccAgents_100")
			{
				if ($value != "NIL")
				{
					$changes++;
					$bg = CHANGED;
					if (strlen($value) > 0)
					{
						$exp = explode(";", $value);
						unset($exp[count($exp) - 1]);
						$agentData = "";
						foreach ($exp as $v)
						{
							$userName = explode(":", $v);
							$userName = $userName[1];
							$agentData .= $userName . "<br>";
						}
						unset($value);
						$value = $agentData;
					}
					else
					{
						$value .= "None";
					}
				}
			}
			//check for required fields
			if (in_array($key, $required) and $value == "")
			{
				$error = 1;
				$bg = INVALID;
				$value = $_SESSION["ccNames"][$key] . " is a required field.";
			}

			if ($bg != UNCHANGED and $key !== "descGroup" and $key !== "descACD" and $key !== "descUnavailable" and !($key == "ccAgents_100" and $value == "NIL"))
			{
				$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["ccNames"][$key] . "</td><td class=\"errorTableRows\">";
				if ($value)
				{
					$data .= $value;
				}
				else
				{
					$data .= "None";
				}
				$data .= "</td></tr>";
			}
		}
	}
	$data .= "</table>";

	if ($changes == 0)
	{
		$error = 1;
		$data = "You have made no changes.";
	}
	echo $error . $data;
?>
