<?php
	
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin ();
	require_once ("/var/www/html/Express/config.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
	require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
	$expProvDB = new ExpressProvLogsDB();
	
	$ccId = $_POST["ccId"];
	unset($_POST["ccId"]);
        
        //Code added @ 22 Jan 2019
        require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
        $cLUObj = new ChangeLogUtility($ccId, $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
        //End code

	
	if(!array_key_exists("ccActivateNumber", $_POST)){
		$_POST['ccActivateNumber'] = "No";
	}
	
	$agentUnavailableCodesFields = array("enableAgentUnavailableCodes", "defaultAgentUnavailableCodeOnDND", "defaultAgentUnavailableCodeOnPersonalCalls", "defaultAgentUnavailableCodeOnConsecutiveBounces", "defaultAgentUnavailableCodeOnNotReachable", "forceUseOfAgentUnavailableCodes", "defaultAgentUnavailableCode");

	$AGxmlinput = xmlHeader($sessionid, "GroupCallCenterModifyAgentListRequest");
	$AGxmlinput .= "<serviceUserId>" . $ccId . "</serviceUserId>";
	/* providing supports till Rel.22 */
	if($bwVersion == "22"){
	    $xmlinput = xmlHeader($sessionid, "GroupCallCenterModifyInstanceRequest22");
	}else{
	    $xmlinput = xmlHeader($sessionid, "GroupCallCenterModifyInstanceRequest19");
	}
	
	$xmlinput .= "<serviceUserId>" . $ccId . "</serviceUserId>";

	echo "<label class=\"labelTextGrey\" style=\"font-size:14px;\">The following changes are complete:</label><br><br>";


	$a = 0;
	$AUCxmlinput = "";
	echo "<ul class=\"uiDialogLabel labelTextGrey\">";
	server_fail_over_debuggin_testing(); /* for fail Over testing. */
	foreach ($_POST as $key => $value)
	{
		if ($key == "codeGroup" or $key == "codeACD" or $key == "codeUnavailable")
		{
			if ($key == "codeGroup")
			{
				$codeType = "Group";
			}
			else if ($key == "codeACD")
			{
				$codeType = "ACD";
			}
			else if ($key == "codeUnavailable")
			{
				$codeType = "Unavailable";
			}

			$oldCodes = "";
			$newCodes = "";
			//check if codes/descriptions changed; if so, make note of changes for logging purposes
			if (isset($_SESSION["callCenterInfo"]["info"]["codes" . $codeType]))
			{
				foreach ($_SESSION["callCenterInfo"]["info"]["codes" . $codeType] as $kk => $vv)
				{
					$oldCodes .= $vv["code"] . ":" . $vv["description"] . "; ";
				}
			}
			foreach ($value as $k => $v)
			{
				if ($_POST["code" . $codeType][$k] != "NIL")
				{
					$newCodes .= $_POST["code" . $codeType][$k] . ":" . $_POST["desc" . $codeType][$k] . "; ";
				}
			}

			if ($oldCodes !== $newCodes)
			{
				if ($oldCodes)
				{
					$oldCodes = substr($oldCodes, 0, -2);
				}
				else
				{
					$oldCodes = "None";
				}
				if ($newCodes)
				{
					$newCodes = substr($newCodes, 0, -2);
				}
				else
				{
					$newCodes = "None";
				}
				echo "<li>" . $_SESSION["ccNames"][$key] . " have changed from " . $oldCodes . " to " . $newCodes . ".</li>";
				$changes[$a][$key]["oldValue"] = $oldCodes;
				$changes[$a][$key]["newValue"] = $newCodes;
				$a++;
			}

			//Delete any codes that need it
			if (isset($_SESSION["callCenterInfo"]["info"]["codes" . $codeType]))
			{
				foreach ($_SESSION["callCenterInfo"]["info"]["codes" . $codeType] as $kk => $vv)
				{
					$found = 0;
					foreach ($value as $k => $v)
					{
						if ($vv["code"] == $v)
						{
							$found = 1;
						}
					}
					if ($found == 0)
					{
						if ($key == "codeUnavailable")
						{
							//check if code is default code; if so, change default code first
							if ($vv["code"] == $_SESSION["callCenterInfo"]["info"]["defaultAgentUnavailableCode"])
							{
								if ($_SESSION["enterprise"] == "true")
								{
									$DPDxmlinput = xmlHeader($sessionid, "EnterpriseCallCenterAgentUnavailableCodeSettingsModifyRequest");
									$DPDxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
								}
								else
								{
									$DPDxmlinput = xmlHeader($sessionid, "GroupCallCenterAgentUnavailableCodeSettingsModifyRequest");
									$DPDxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
									$DPDxmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
								}
								$DPDxmlinput .= "<forceUseOfAgentUnavailableCodes>" . $_POST["forceUseOfAgentUnavailableCodes"] . "</forceUseOfAgentUnavailableCodes>";
								if ($_POST["defaultAgentUnavailableCode"])
								{
									$DPDxmlinput .= "<defaultAgentUnavailableCode>" . $_POST["defaultAgentUnavailableCode"] . "</defaultAgentUnavailableCode>";
								}
								else
								{
									$DPDxmlinput .= "<defaultAgentUnavailableCode xsi:nil=\"true\" />";
								}
								$DPDxmlinput .= xmlFooter();
								$DPDresponse = $client->processOCIMessage(array("in0" => $DPDxmlinput));
								$DPDxml = new SimpleXMLElement($DPDresponse->processOCIMessageReturn, LIBXML_NOWARNING);
							}
						}

						if ($key == "codeGroup")
						{
							if ($_SESSION["enterprise"] == "true")
							{
								$DPDxmlinput = xmlHeader($sessionid, "EnterpriseCallCenterCallDispositionCodeDeleteRequest");
								$DPDxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
							}
							else
							{
								$DPDxmlinput = xmlHeader($sessionid, "GroupCallCenterCallDispositionCodeDeleteRequest");
								$DPDxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
								$DPDxmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
							}
						}
						else if ($key == "codeACD")
						{
							$DPDxmlinput = xmlHeader($sessionid, "GroupCallCenterQueueCallDispositionCodeDeleteRequest");
							$DPDxmlinput .= "<serviceUserId>" . $ccId . "</serviceUserId>";
						}
						else if ($key == "codeUnavailable")
						{
							if ($_SESSION["enterprise"] == "true")
							{
								$DPDxmlinput = xmlHeader($sessionid, "EnterpriseCallCenterAgentUnavailableCodeDeleteRequest");
								$DPDxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
							}
							else
							{
								$DPDxmlinput = xmlHeader($sessionid, "GroupCallCenterAgentUnavailableCodeDeleteRequest");
								$DPDxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
								$DPDxmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
							}
						}
						$DPDxmlinput .= "<code>" . $vv["code"] . "</code>";
						$DPDxmlinput .= xmlFooter();
//print_r($DPDxmlinput);
						$DPDresponse = $client->processOCIMessage(array("in0" => $DPDxmlinput));
						$DPDxml = new SimpleXMLElement($DPDresponse->processOCIMessageReturn, LIBXML_NOWARNING);
					}
				}
			}

			//Add any codes that need it
			foreach ($value as $k => $v)
			{
				if ($v != "NIL")
				{
					$found = 0;
					foreach ($_SESSION["callCenterInfo"]["info"]["codes" . $codeType] as $kk => $vv)
					{
						if ($vv["code"] == $v)
						{
							$found = 1;
						}
					}
					if ($found == 0)
					{
						if ($key == "codeGroup")
						{
							if ($_SESSION["enterprise"] == "true")
							{
								$DPxmlinput = xmlHeader($sessionid, "EnterpriseCallCenterCallDispositionCodeAddRequest");
								$DPxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
							}
							else
							{
								$DPxmlinput = xmlHeader($sessionid, "GroupCallCenterCallDispositionCodeAddRequest");
								$DPxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
								$DPxmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
							}
							$DPxmlinput .= "<code>" . $v . "</code>";
							$DPxmlinput .= "<isActive>true</isActive>";
						}
						else if ($key == "codeACD")
						{
							$DPxmlinput = xmlHeader($sessionid, "GroupCallCenterQueueCallDispositionCodeAddRequest");
							$DPxmlinput .= "<serviceUserId>" . $ccId . "</serviceUserId>";
							$DPxmlinput .= "<code>" . $v . "</code>";
							$DPxmlinput .= "<isActive>true</isActive>";
						}
						else if ($key == "codeUnavailable")
						{
							if ($_SESSION["enterprise"] == "true")
							{
								$DPxmlinput = xmlHeader($sessionid, "EnterpriseCallCenterAgentUnavailableCodeAddRequest");
								$DPxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
							}
							else
							{
								$DPxmlinput = xmlHeader($sessionid, "GroupCallCenterAgentUnavailableCodeAddRequest");
								$DPxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
								$DPxmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
							}
							$DPxmlinput .= "<isActive>true</isActive>";
							$DPxmlinput .= "<code>" . $v . "</code>";
						}
						if ($_POST["desc" . $codeType][$k] !== "")
						{
							$DPxmlinput .= "<description>" . $_POST["desc" . $codeType][$k] . "</description>";
						}
						$DPxmlinput .= xmlFooter();
						$DPresponse = $client->processOCIMessage(array("in0" => $DPxmlinput));
						$DPxml = new SimpleXMLElement($DPresponse->processOCIMessageReturn, LIBXML_NOWARNING);
						readError($DPxml);
					}
				}
			}

			//Modify description of code
			foreach ($_POST["desc" . $codeType] as $k => $v)
			{
				if ($v != "NIL")
				{
					if ((isset($_SESSION["callCenterInfo"]["info"]["codes" . $codeType][$k]["description"]) and $v !== $_SESSION["callCenterInfo"]["info"]["codes" . $codeType][$k]["description"]) or !isset($_SESSION["callCenterInfo"]["info"]["codes" . $codeType][$k]["description"]))
					{
						if ($key == "codeGroup")
						{
							if ($_SESSION["enterprise"] == "true")
							{
								$DPMxmlinput = xmlHeader($sessionid, "EnterpriseCallCenterCallDispositionCodeModifyRequest");
								$DPMxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
							}
							else
							{
								$DPMxmlinput = xmlHeader($sessionid, "GroupCallCenterCallDispositionCodeModifyRequest");
								$DPMxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
								$DPMxmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
							}
						}
						else if ($key == "codeACD")
						{
							$DPMxmlinput = xmlHeader($sessionid, "GroupCallCenterQueueCallDispositionCodeModifyRequest");
							$DPMxmlinput .= "<serviceUserId>" . $ccId . "</serviceUserId>";
						}
						else if ($key == "codeUnavailable")
						{
							if ($_SESSION["enterprise"] == "true")
							{
								$DPMxmlinput = xmlHeader($sessionid, "EnterpriseCallCenterAgentUnavailableCodeModifyRequest");
								$DPMxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
							}
							else
							{
								$DPMxmlinput = xmlHeader($sessionid, "GroupCallCenterAgentUnavailableCodeModifyRequest");
								$DPMxmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
								$DPMxmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
							}
						}
						$DPMxmlinput .= "<code>" . $_POST["code" . $codeType][$k] . "</code>";
						$DPMxmlinput .= "<isActive>true</isActive>";
						if ($_POST["desc" . $codeType][$k] !== "")
						{
							$DPMxmlinput .= "<description>" . $_POST["desc" . $codeType][$k] . "</description>";
						}
						else
						{
							$DPMxmlinput .= "<description xsi:nil=\"true\" />";
						}
						$DPMxmlinput .= xmlFooter();
						$DPMresponse = $client->processOCIMessage(array("in0" => $DPMxmlinput));
						$DPMxml = new SimpleXMLElement($DPMresponse->processOCIMessageReturn, LIBXML_NOWARNING);
					}
				}
			}
		}
		else if ($key !== "ccAgents_100" and $key !== "descGroup" and $key !== "descACD" and $key !== "descUnavailable" and $key !== "ccActivateNumber" and $key !== "ccPhoneActivateNumber" and ((isset($_SESSION["callCenterInfo"]["info"][$key]) and $value !== $_SESSION["callCenterInfo"]["info"][$key]) or !isset($_SESSION["callCenterInfo"]["info"][$key])))
		{
			if (in_array($key, $agentUnavailableCodesFields))
			{
				if ($value == "")
				{
					$AUCxmlinput .= "<" . $key . " xsi:nil=\"true\" />";
				}
				else
				{
					$AUCxmlinput .= "<" . $key . ">" . $value . "</" . $key . ">";
				}
			}
			else
			{
				$xmlinput .= "<" . $key . ">" . $value . "</" . $key . ">";
			}
			if ($_SESSION["callCenterInfo"]["info"][$key] !== "")
			{
				$changes[$a][$key]["oldValue"] = $_SESSION["callCenterInfo"]["info"][$key];
			}
			else
			{
				$changes[$a][$key]["oldValue"] = "None";
			}
			if ($value !== "")
			{
				$changes[$a][$key]["newValue"] = $value;
			}
			else
			{
				$changes[$a][$key]["newValue"] = "None";
			}
			echo "<li>You have changed " . $_SESSION["ccNames"][$key] . " from " . $changes[$a][$key]["oldValue"] . " to " . $changes[$a][$key]["newValue"] . ".</li>";
			$a++;
		}
		else if ($key == "ccAgents_100")
		{
			$oldList = "";
			if (isset($_SESSION["callCenterInfo"]["agents"]))
			{
				for ($b = 0; $b < count($_SESSION["callCenterInfo"]["agents"]); $b++)
				{
					$oldList .= $_SESSION["callCenterInfo"]["agents"][$b]["name"] . "; ";
				}
			}

			if (strlen($value) > 0 and $value !== "NIL")
			{
				$AGxmlinput = xmlHeader($sessionid, "GroupCallCenterModifyAgentListRequest");
				$AGxmlinput .= "<serviceUserId>" . $ccId . "</serviceUserId>";
				$AGxmlinput .= "<agentUserIdList>";
				$exp = explode(";", $value);
				$ccAgents = "";
				foreach ($exp as $v)
				{
					$userExp = explode(":", $v);
					$userName = isset($userExp[1]) ? $userExp[1] : "";
					$userId = $userExp[0];
					if ($userId !== "")
					{
						$ccAgents .= $userName . "; ";
						$AGxmlinput .= "<userId>" . $userId . "</userId>";
					}
					if ($oldList)
					{
						$changes[$a][$key]["oldValue"] = substr($oldList, 0, -2);
					}
					else
					{
						$changes[$a][$key]["oldValue"] = "None";
					}
					$changes[$a][$key]["newValue"] = substr($ccAgents, 0, -2);
				}
				$AGxmlinput .= "</agentUserIdList>";
				$AGxmlinput .= xmlFooter();
				$AGresponse = $client->processOCIMessage(array("in0" => $AGxmlinput));
				$AGxml = new SimpleXMLElement($AGresponse->processOCIMessageReturn, LIBXML_NOWARNING);
				echo "<li>You have changed " . $_SESSION["ccNames"][$key] . " from ";
				if ($oldList)
				{
					echo substr($oldList, 0, -2);
				}
				else
				{
					echo "None";
				}
				echo " to " . substr($ccAgents, 0, -2) . ".</li>";
			}
			if ($value == "")
			{
				$AGxmlinput = xmlHeader($sessionid, "GroupCallCenterModifyAgentListRequest");
				$AGxmlinput .= "<serviceUserId>" . $ccId . "</serviceUserId>";
				$AGxmlinput .= "<agentUserIdList xsi:nil=\"true\"></agentUserIdList>";
				$AGxmlinput .= xmlFooter();
				$AGresponse = $client->processOCIMessage(array("in0" => $AGxmlinput));
				$AGxml = new SimpleXMLElement($AGresponse->processOCIMessageReturn, LIBXML_NOWARNING);

				$changes[$a][$key]["oldValue"] = substr($oldList, 0, -2);
				$changes[$a][$key]["newValue"] = "None";
				echo "<li>You have changed " . $_SESSION["ccNames"][$key] . " from " . substr($oldList, 0, -2) . " to None.</li>";
			}
		}
		else if($key == "ccActivateNumber"){
			
			if (isset($_SESSION["callCenterInfo"]["info"][$key]) && $value !== $_SESSION["callCenterInfo"]["info"][$key]){
				
				if($_SESSION["callCenterInfo"]["info"][$key] == "No"){
					$oldCodes = "Deactivated";
				}else{
					$oldCodes = "Activated";
				}
				
				if($value == "No"){
					$newCodes = "Deactivated";
				}else{
					$newCodes = "Activated";
				}
				
				$changes[$a][$key]["oldValue"] = $oldCodes;
				$changes[$a][$key]["newValue"] = $newCodes;
				$ccDn = new Dns ();
				$dnPostArray = array(
						"sp" => $_SESSION['sp'],
						"groupId" => $_SESSION['groupId'],
						"phoneNumber" => array($_POST['ccPhoneActivateNumber'])
				);
				
				if(!empty($_POST[$key])){
					$dnsActiveResponse = $ccDn->activateDNRequest($dnPostArray);
				}else{
					$dnsActiveResponse = $ccDn->deActivateDNRequest($dnPostArray);
				}
			}
			//echo "<li>You have changed " . $_SESSION["callCenterInfo"]["info"]["phoneNumber"]. " from " . $oldCodes. " to " .$newCodes. ".</li>";
		}
	}
	echo "</ul>";

	if (isset($AUCxmlinput))
	{
		if ($_SESSION["enterprise"] == "true")
		{
			$AUCheader = xmlHeader($sessionid, "EnterpriseCallCenterAgentUnavailableCodeSettingsModifyRequest");
			$AUCheader .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		}
		else
		{
			$AUCheader = xmlHeader($sessionid, "GroupCallCenterAgentUnavailableCodeSettingsModifyRequest");
			$AUCheader .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
			$AUCheader .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		}
		$AUCxmlinput = $AUCheader . $AUCxmlinput . xmlFooter();
		$AUCresponse = $client->processOCIMessage(array("in0" => $AUCxmlinput));
		$AUCxml = new SimpleXMLElement($AUCresponse->processOCIMessageReturn, LIBXML_NOWARNING);
		readError($AUCxml);
	}

	$xmlinput .= xmlFooter();
        //print_r($xmlinput);
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	readError($xml);
        
        if(isset($changes))
        {    
            $changeLogRow = array();
            foreach($changes as $changeLogRow){ 
                foreach($changeLogRow as $fieldName => $fieldRow){
                    $changeLogArr[$fieldName]['oldValue'] = $fieldRow['oldValue'];
                    $changeLogArr[$fieldName]['newValue'] = $fieldRow['newValue'];
                }
            }
        }
        
        $module             = "Call Center Modify";
        $tableName          = "callCenterModChanges";
        $entityName         = $_SESSION["callCenterInfo"]["info"]["name"];
        $cLUObj->changesArr = $changeLogArr;     
        $changeResponse     = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName); 
        
        /*
	$date = date("Y-m-d H:i:s");
	$query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
	$query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', 'Call Center Modify', '" . $_SESSION["groupId"] . "', '" . $_SESSION["callCenterInfo"]["info"]["name"] . "')";
	$sth = $expProvDB->expressProvLogsDb->query($query);
	$lastId = $expProvDB->expressProvLogsDb->lastInsertId();
    
	for ($a = 0; $a < count($changes); $a++)
	{
		$insert = "INSERT into callCenterModChanges (id, serviceId, entityName, field, oldValue, newValue)";
		$field = key($changes[$a]);
		$values = " VALUES ('" . $lastId . "', '" . $ccId . "', '" . $_SESSION["callCenterInfo"]["info"]["name"] . "', '" . $_SESSION["ccNames"][$field] . "', '" . addslashes($changes[$a][$field]["oldValue"]) . "', '" . addslashes($changes[$a][$field]["newValue"]) . "')";
		$query = $insert . $values;
		$sth = $expProvDB->expressProvLogsDb->query($query);
	}
        */
	unset($_SESSION["callCenterInfo"]);
?>
