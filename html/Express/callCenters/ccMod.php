<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
?>
<script>

    function getccInfo()
    {
    	pendingProcess.push("Call Center");
    	$("#ccData").html("");
    	var id = $("#ccChoice").val();
    
    	if (id !== "")
    	{
    		$.ajax({
    			type: "POST",
    			url: "callCenters/ccInfo.php",
    			data: { ccId: id },
    			success: function(result) {
    				if(foundServerConErrorOnProcess(result, "Call Center")) {
    					return false;
                  	}
    				$("#ccData").html(result);
    			}
    		});
    	}
    	else
    	{
    		$("#endUserId").html("");
    	}
    }

	$(function() {
		$("#module").html("> Modify Call Center: ");
		$("#endUserId").html("");		
		$("#ccChoice").change(getccInfo);
		
	});
</script>
<h2 class="adminUserText" >Choose Call Center to Modify</h2>

<div class="adminUserForm">
	
		<form method="POST" id="ccMod" class="fcorn-registerTwo">
			<div class="row">
				<div class="col-md-12 adminSelectDiv">
					<div class="form-group"> 
						<label class="labelText" for="ccChoice">Call Center</label>
						<div class="dropdown-wrap">
							<select name="ccChoice" id="ccChoice" class="form-control selectBlue">
								<option value=""></option>
								<?php
									require_once("/var/www/lib/broadsoft/adminPortal/getAllCallCenters.php");
									if (isset($callCenters))
									{
										foreach ($callCenters as $key => $value)
										{
											echo "<option value=\"" . $value["id"] . "\">" . $value["name"] . "</option>";
										}
									}
								?>
							</select>
						</div>
					</div>
				</div>
			</div>
		</form>
	
	<div id="ccData"></div>
</div>