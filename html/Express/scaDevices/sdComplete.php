<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/sca/SCAOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/NameAndIDBuilder.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
require_once ("/var/www/lib/broadsoft/adminPortal/customTagOperations/ExpressCustomTagUtil.php");
require_once ("/var/www/html/Express/scaDevices/SCADBOperations.php");
include("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");

$dbobj = new SCADBOperations();
require_once ("/var/www/html/Express/util/formDataArrayDiff.php");
$diffObj = new FormDataArrayDiff();

require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
$syslevelObj = new sysLevelDeviceOperations();

require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/util/scaUtility.php");

//Code added @ 17 July 2018
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");   
require_once ("/var/www/lib/broadsoft/adminPortal/services/userServices.php");
$grpServiceList = new Services();
//End code

$scaUtilityObj = new SCAUtility();
$vdmOperationObj = new VdmOperations();

$syslevelObj = new sysLevelDeviceOperations();

//echo "<pre>";
server_fail_over_debuggin_testing(); /* for fail Over testing. */

$serviceId = isset($_POST['modifiedDevice']) ? $_POST['modifiedDevice'] : "";
$cLUObj = new ChangeLogUtility($serviceId, $_SESSION["groupId"], $_SESSION["loggedInUserName"]);

$formDataArray = array(
    "deviceName" => "Device Name",
    "deviceType" => "Device Type",
    "scaCustomProfile"=> "Custom Profile",
    "protocol" => "Protocol",
    "port" => "Port",
    "netAddress" => "Host Name/IP Address",
    "transportProtocol" => "Transport",
    "macAddress" => "MAC Address",
    "serialNumber" => "Serial Number",
    "description" => "Description",
    "outboundProxyServerNetAddress" => "Outbound Proxy Server",
    "stunServerNetAddress" => "STUN Server",
    "physicalLocation" => "Physical Location",
    "customProfile" => "Custom Profile",
    "scaUsers" => "SCA Users",
    "tagBundles" => "Tag Bundles"
);

global $searchDeviceErrMsg;
$deviceResponse["status"] = "success";
$deviceResponse["reason"] = "";
$unAssignFailure = array();
$assignFailure = array();
$searchDeviceErrMsg = "";
$changeString = "";
$postArray = array();
$assignFailCount = 0;
$unassignFailCount = 0;

$providerid = $_POST["sp"] = $_SESSION["sp"];
$groupId = $_POST["groupId"] = $_SESSION["groupId"];
$postArray = $_POST;


if(isset($postArray["tagBundles"][0]) && $postArray["tagBundles"][0] != ""){
    $tagbundleName = implode(";", $postArray["tagBundles"]);
}else{
    $tagbundleName = "";
}

$deviceObj = new DeviceOperations();

if(isset($postArray['modifiedDevice']) && !empty($postArray['modifiedDevice'])){
    
    
    $syslevelArr = $syslevelObj->getSysytemConfigDeviceTypeRequest($postArray['hidDeviceType'], $ociVersion);
    $systemLinePortFlag = false;
    $scaUtilityData = "";
    if(isset($syslevelArr["Success"]->staticLineOrdering)){
        $systemLinePortFlag = strval($syslevelArr["Success"]->staticLineOrdering);
    }
    $deviceDetails = $vdmOperationObj->getUserDeviceDetail($_SESSION["sp"], $_SESSION["groupId"], $postArray['modifiedDevice']);
    
    
    
  
    
    $deviceResponse["modRequest"] = "modRequest";
	$deviceName = $postArray['modifiedDevice'];
	$deviceType = $postArray['hidDeviceType'];
	$customProfile = $postArray["customProfile"];
	$port = $postArray["port"];
	
	$setName = $syslevelObj->getTagSetNameByDeviceType($_POST["hidDeviceType"], $ociVersion);
	$systemDefaultTagVal = "";
	if($setName["Success"] != "" && !empty($setName["Success"])){
	    $defaultSettags = $syslevelObj->getDefaultTagListByTagSet($setName["Success"]);
	    $phoneTemplateName = "%phone-template%";
	    if(array_key_exists($phoneTemplateName, $defaultSettags["Success"])){
	        $systemDefaultTagVal = $defaultSettags["Success"]["%phone-template%"][0];
	    }
	    
	}
	
	
	$userList = $deviceObj->getAllUsersforDevice($providerid, $groupId, $deviceName);
	if( !empty($userList["Error"])){
	    $deviceResponse["status"] = "failure";
	    $deviceResponse["reason"] = "Device Not Found";
	    
	    echo json_encode($deviceResponse); die;
	}
	
	
	if($_SESSION['deviceUpdate'] ==  "true"){
		$deviceModifyResponse = $deviceObj->deviceModifyRequest($postArray);
		if(empty($deviceModifyResponse["Error"])){
			$deviceResponse["Success"] = $postArray['modifiedDevice'];
			/* call to change log */
			$cLUObj->createChangesArrayFromArray($postArray, $_SESSION["sca"]["deviceInfo"], $formDataArray);
			/* End call to change log */
			if($_SESSION["sca"]["deviceInfo"]["customProfile"] <> $_POST["customProfile"]){
			    deleteExpressCutomTags($db, $providerid, $groupId, $deviceName, $deviceType, $customProfile, $systemDefaultTagVal);
			    
			    $cLUObj->createChangesArray($module = $formDataArray["customProfile"], $oldValue = $_SESSION["sca"]["deviceInfo"]["customProfile"] , $newValue = $_POST["customProfile"]);
			}
			if($tagbundleName <> $_SESSION["sca"]["deviceInfo"]["tagBundles"]){
			    $expSession = explode(";", $_SESSION["sca"]["deviceInfo"]["tagBundles"]);
			    $diffArr = $diffObj->diffInTwoArray($_POST["tagBundles"], $expSession);
			    
			    if($diffArr["removed"][0] != ""){
			        $deleteTags = $dbobj->getAllCustomTagsFromTagBundles($diffArr["removed"]);
			        deleteTagsTagBundleChange($db, $providerid, $groupId, $deviceName, $deleteTags);
			    }
			    
			    if($diffArr["assigned"][0] != ""){
			        $addTags = $dbobj->getAllCustomTagsFromTagBundles($diffArr["assigned"]);
			        addTagBundlesCustomTags($db, $providerid, $groupId, $deviceName, $addTags); 
			    }
			    $deleteDTB = array("%Express_Tag_Bundle%" => "");
			    deleteTagsTagBundleChange($db, $providerid, $groupId, $deviceName, $deleteDTB);
			    if($tagbundleName != ""){
			        addExpressTagBundlesDefaultCutomTags($db, $providerid, $groupId, $deviceName, $postArray["tagBundles"]);
			    }
			   
			    $cLUObj->createChangesArray($module = $formDataArray["tagBundles"], $oldValue = str_replace(";", ",", $_SESSION["sca"]["deviceInfo"]["tagBundles"]), $newValue = str_replace(";", ",", $tagbundleName));
			}
				$scaOp = new SCAOperations();
				//new code 
                                if(isset($postArray["scaUsers"]) && $postArray["scaUsers"] != ""){
                                
                                    //Code added @ 19 July 2018  to remove only blank records   
                                    //$scaUsers = explode(";", $postArray["scaUsers"]);   //Code commented on 26 Sep 2018
                                    //Code added @ 26 Sep 2018  to remove only blank records 
                                    $scaUsersArr = array();
                                    $scaUsersArr = explode(";", $postArray["scaUsers"]);   
                                    foreach($scaUsersArr as $tmpKey => $tmpVal)
                                    {
                                        if($tmpVal!=""){
                                            $scaUsers[] = $tmpVal;
                                        }
                                    }
                                    //End Code 
                                    
                                    //Code commented on 26 Sep 2018
                                    /*array_pop($scaUsers);
                                    array_pop($scaUsers);*/
                                    
                                    $sharedCallAppearance = $grpServiceList->compareSessionScaUserForDeviceInvetry();
                                    $aa = 0;                 
                                    $scaOperationArray = $grpServiceList->assignUnassignScaUser($sharedCallAppearance, $scaUsers);
                                    
                                    
                                    if(count($scaOperationArray["unAssign"]) > 0){
                                            $b = 0;
                                            foreach ($scaOperationArray["unAssign"] as $k => $v)
                                            {
                                                    if ($v !== "scaUsers" and $v !== "")
                                                    {
                                                            $exp2 = explode(":", $v);
                                                            $changes["scaServicePackUnassign"][$b] = $exp2[0];
                                                            $a++;
                                                    }
                                            }
                                    }if ($postArray["scaUsers"] == "scaUsers;")
                                    {
                                            $changes["sca"] = "NIL";
                                    }
                                    else
                                    {
                                            $a = 0;
                                            $exp = explode(";", $postArray["scaUsers"]);
                                            foreach ($exp as $k => $v)
                                            {
                                                    if ($v !== "scaUsers" and $v !== "")
                                                    {
                                                            $exp2 = explode(":", $v);
                                                            $changes["sca"][$a]["id"] = $exp2[0];
                                                            $changes["sca"][$a]["name"] = $exp2[1];
                                                            $changes["sca"][$a]["linePort"] = $exp2[2];

                                                            $changes["scaServicePack"][$a] = $exp2[0];
                                                            $a++;
                                                    }
                                            }
                                    }
                                    //End code   
                                    
                                    foreach ($changes as $key => $value)
                                    {
                                        if($key == "scaServicePack"){
                                            
                                            //code to assign service pack to user
                                            //print_R($_SESSION["userInfo"]["sharedCallAppearanceUsers"]);die;
                                            //if($_SESSION["userInfo"]["sharedCallAppearanceUsers"]))
                                            //$grpServiceList = new Services();
                                            $changeSCAAssignLog = array();
                                            $grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
                                            $servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
                                            $servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray);
                                            $scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);
                                            
                                            foreach($scaServiceArray as $ssa=>$ssv){
                                                $pos = strpos($ssv->col[0], "Shared Call Appearance");
                                                if($pos === false){
                                                }else{
                                                    if($ssv->count == "1"){
                                                        $servicePackToBeAssigned = $servicePackArray[$ssa][0];
                                                    }
                                                }
                                            }
                                            
                                            
                                            if(count($value) > 0){
                                                foreach($value as $scaUserkey=>$scaUserVal){
                                                    
                                                    /*Check User Service Pack checked or not..*/
                                                    $userServices = new userServices();
                                                    $userSessionservicePackArray = $userServices->getUserServicesAll($scaUserVal, $_SESSION["sp"]);
                                                    $servicesSessionArray = $grpServiceList->getServicePackServicesListUsers($userSessionservicePackArray["Success"]["ServicePack"]["assigned"]);
                                                    $scaSessionServiceArray = $grpServiceList->checkScaUserServiceInServicepack($servicesSessionArray);

                                                    if(count($scaSessionServiceArray) == 0){
                                                        $responseServicePackUser = $grpServiceList->assignServicePackUser($scaUserVal, $servicePackToBeAssigned);
                                                        if(empty($responseServicePackUser["Error"])){
                                                            $message .= "<li>".$servicePackToBeAssigned." SCA service pack has been assigned to " . $scaUserVal. ".</li>";
                                                        }
                                                    }
                                                    
                                                    //code ends for assign service pack to user
                                                }
                                                $services = new Services();
                                                $oldScaAssignUsersId = array();
                                                $newValue = array();
                                                //print_r($_SESSION["userInfo"]);//die;
                                                $oldScaUsers = $services->compareSessionScaUserForDeviceInvetry();
                                                $oldScaUsersId = array();
                                                //echo "<br/>Assign Old Sca Users=>";print_r($oldScaUsers);echo "<br/>";
                                                foreach($oldScaUsers["IdArray"] as $oKey=>$oVal){
                                                    $oldScaUsersIdArray = explode(":", $oVal);
                                                    $oldScaAssignUsersId[] = $oldScaUsersIdArray[0].": ".$servicePackToBeAssigned;
                                                    
                                                }
                                                foreach($value as $kVal){
                                                    $newValue[] = $kVal.": ".$servicePackToBeAssigned;
                                                }
                                                $changeSCAAssignLog[0]["field"] = "scaServicePackAssign";
                                                $changeSCAAssignLog[0]["old"] = implode(", ", $oldScaAssignUsersId);
                                                $changeSCAAssignLog[0]["new"] = implode(", ", $newValue);
                                                $cLUObj->createChangesArray("scaServicePackAssign", implode(", ", $oldScaAssignUsersId), implode(", ", $newValue));
                                                $a++;
                                            }
                                        }
                                        if($key == "scaServicePackUnassign"){
                                            
                                            $grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
                                            $servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
                                            $userservicePackArray = array();
                                            $servicePackToBeUnAssigned = array();
                                            $newValue = array();
                                            
                                            
                                            if(count($scaOperationArray["unAssign"]) > 0){
                                                foreach ($scaOperationArray["unAssign"] as $uKey => $uVal)
                                                {
                                                    if ($uVal !== "scaUsers" and $uVal !== "")
                                                    {
                                                        $exp2 = explode(":", $uVal);
                                                        $uid = $exp2[0];                                                        
                                                        
                                                        
                                                        $userServices = new userServices();
                                                        $userservicePackArray = $userServices->getUserServicesAll($exp2[0], $_SESSION["sp"]);
                                                        $servicesArray = $grpServiceList->getServicePackServicesListUsers($userservicePackArray["Success"]["ServicePack"]["assigned"]);
                                                        $scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);
                                                                                                                
                                                        
                                                        foreach($scaServiceArray as $ssa=>$ssv){
                                                            $pos = strpos($ssv->col[0], "Shared Call Appearance");
                                                            if($pos === false){
                                                            }else{
                                                                if($ssv->count == "1"){
                                                                    $servicePackToBeUnAssigned[] = $userservicePackArray["Success"]["ServicePack"]["assigned"][$ssa];
                                                                }
                                                            }
                                                        }
                                                                                                                
                                                        foreach($servicePackToBeUnAssigned as $servicePackToBeUnAssignedVal){
                                                            $unAssignScaResponse = $grpServiceList->unAssignServicePackUser($uid, $servicePackToBeUnAssignedVal);                                                                                                               
                                                            if(empty($unAssignScaResponse["Error"])){
                                                                $message .= "<li>".$servicePackToBeUnAssignedVal." SCA service pack has been un assigned to " . $uVal. ".</li>";                                                            
                                                            }
                                                            $unAssignId[] = $uid.": ".$servicePackToBeUnAssignedVal;
                                                        }
                                                    }
                                                }
                                                $servicesArray = "";
                                                $changeSCAUnassignLog = array();
                                                
                                                $services = new Services();
                                                $oldScaUsers = $services->compareSessionScaUserForDeviceInvetry();
                                                $oldScaUnAssignUsersId = array();
                                                foreach($oldScaUsers["IdArray"] as $oKey=>$oVal){
                                                    $oldScaUsersIdArray = explode(":", $oVal);
                                                    $oldScaUnAssignUsersId[] = $oldScaUsersIdArray[0];
                                                }
                                                foreach($value as $kVal){
                                                    $newValue[] = $kVal.": ".$servicePackToBeAssigned;
                                                }
                                                
                                                $changeSCAUnassignLog[0]["field"] = "unAssignScaServicePack";
                                                //$changeSCAUnassignLog[0]["old"] = implode(",", $oldScaUnAssignUsersId);
                                                $changeSCAUnassignLog[0]["old"] = "None";
                                                $changeSCAUnassignLog[0]["new"] = implode(", ", $unAssignId);
                                                $cLUObj->createChangesArray("unAssignScaServicePack", "None", implode(", ", $unAssignId));
                                                $a++;
                                                
                                            }
                                            //code ends
                                        }
                                    }                                      
                                    
                                    $scaUsersStr = str_replace("scaUsers;","", $postArray["scaUsers"]);
                                    
                                    $usersArray  = $deviceObj->newFilterUsers($scaUsersStr);
                                                                        
                                    if(count($usersArray["unAssignUser"])> 0){
                                            foreach($usersArray["unAssignUser"] as $uaKey => $uaVal){
                                                    $explodeUsersInfo = explode(":", $uaVal);
                                                    $scaUserId = $explodeUsersInfo[0];
                                                    $scaUserName = $explodeUsersInfo[1];
                                                    $phoneNummber = $explodeUsersInfo[2];
                                                    $ext = $explodeUsersInfo[3];

                                                    $scaUserLinePort = getUserLinePortFromDevice($userList, $scaUserId);
                                                    $isUserDeleteToSca = $scaOp->deleteDevicesFromSCA($scaUserId, $deviceName, $scaUserLinePort);	
                                                    if(empty($isUserDeleteToSca["Error"])){
                                                            $deviceResponse["userUnassign"] = "Success";
                                                    }else{
                                                        $unAssignFailure[$assignFailCount]['user'] = $scaUserName;
                                                        $unAssignFailure[$assignFailCount]['reason'] = $isUserDeleteToSca["Error"];
                                                        $unassignFailCount++;
                                                            $deviceResponse["userUnassign"] = $isUserDeleteToSca["Error"];
                                                    }
                                            }
                                    }

                                    $addedScaUesrs = "";
                                    if(count($usersArray["assignUser"])> 0){
                                            foreach($usersArray["assignUser"] as $uKey=>$uVal){
                                                
                                                $scaUtilityData="";
                                                if($systemLinePortFlag == "true"){
                                                    
                                                    $vdmOperationsArr = $vdmOperationObj->getAllUserOfDevice($_SESSION["sp"], $_SESSION["groupId"], $deviceName);
                                                 
                                                    if($vdmOperationsArr["Error"] == ""){
                                                        
                                                        $scaUtilityData = $scaUtilityObj->scaPortCalculation($vdmOperationsArr["Success"], (int)$deviceDetails["Success"]["numberOfPorts"]);
                                                    }
                                                }
                                               
                                                    $explodeUsersInfo = explode(":", $uVal);
                                                    $scaUserId = $explodeUsersInfo[0];
                                                    $scaUserName = $explodeUsersInfo[1];
                                                    $phoneNummber = $explodeUsersInfo[2];
                                                    $ext = $explodeUsersInfo[3];

                                                    $linePortFromCriteria = constructLinePort($groupId, $deviceName, $phoneNummber, $ext, $_SESSION["proxyDomain"], $_SESSION["defaultDomain"]);
                                                    $isUserAddedToSca = $scaOp->addDeviceToUserAsSCA($scaUserId, $deviceName, $linePortFromCriteria, $systemLinePortFlag, $scaUtilityData);

                                                    if(empty($isUserAddedToSca["Error"])){
                                                            $deviceResponse["userAssign"][""] = "Success";
                                                            $addedScaUesrs .= $scaUserName ."(".$ext.")" . " ";
                                                    }else{

                                                        $assignFailure[$assignFailCount]['user'] = $scaUserName;
                                                        $assignFailure[$assignFailCount]['reason'] = $isUserAddedToSca["Error"];
                                                        $assignFailCount++;
                                                    }
                                            }
                                    }

                                    $deviceResponse["userAssignFail"] = $assignFailure;
                                    $deviceResponse["userUnAssignFail"] = $unAssignFailure;
                                    if( !empty($assignFailure) || !empty($unAssignFailure)){
                                        $deviceResponse["status"] = "failure";
                                        $deviceResponse["reason"] = "Assign Unassign Failure";
                                    }
                                    if(count($usersArray["unAssignUser"]) > 0 || count($usersArray["assignUser"]) > 0) {
            // 			    $old_scaUser = rtrim(str_replace(";", ",", $_SESSION["sca"]["deviceInfo"]["scaUsers"]), ",");
                                        $old_scaUser = getOldScaUser($_SESSION["sca"]["deviceInfo"]["scaUsers"]);
                                        $new_scaUsers = str_replace(" ", ",", trim($addedScaUesrs));
                                        $cLUObj->createChangesArray($module = $formDataArray["scaUsers"], $oldValue = $old_scaUser, $newValue = $new_scaUsers);
                                    }                                    
                                    
                                    
                                    }
                        //die();
                        
			/* Call to change Log */
			$module = "SCA-Only Devices Modification";
			$cLUObj->changeLogModifyUtility($module, $_POST['modifiedDevice'], $tableName = "deviceInventoryModChanges");
			/* End Call to change Log */
			echo json_encode($deviceResponse);
		}else{		    
                    //print_r($deviceResponse);
                    $deviceResponse["status"] = "failure";
		    $deviceResponse["reason"] = "Error Modifying Device";
		    $deviceResponse["deviceUpdateReason"] = $deviceModifyResponse["Error"];
		    
		    echo json_encode($deviceResponse);
		}
	}
	
}else{
    $deviceResponse["addRequest"] = "addRequest";
	$providerId = $postArray["sp"]= $_SESSION["sp"];
	$groupId = $postArray["groupId"] = $_SESSION["groupId"];
	$setName = $syslevelObj->getTagSetNameByDeviceType($_POST["deviceType"], $ociVersion);
	$systemDefaultTagVal = "";
	if($setName["Success"] != "" && !empty($setName["Success"])){
	    $defaultSettags = $syslevelObj->getDefaultTagListByTagSet($setName["Success"]);
	    $phoneTemplateName = "%phone-template%";
	    if(array_key_exists($phoneTemplateName, $defaultSettags["Success"])){
	        $systemDefaultTagVal = $defaultSettags["Success"]["%phone-template%"][0];
	    }
	    
	}
	
	$deviceNameFromCriteria = $postArray["deviceName"] = constructDeviceName($scaOnlyCriteria, $groupId,
			$postArray["deviceType"], $postArray["macAddress"], $postArray["serialNumber"], $postArray["netAddress"]);
	
	// $postArray["deviceName"] = $deviceNameFromCriteria;
	/* ubstitute device name */
	$postArray["deviceName"] = $deviceNameFromCriteria = replaceDevNameWithSubstitute($deviceNameFromCriteria);
	
	$postArray["deviceAccessUserName"] = constructDeviceUserName($groupId, $postArray["macAddress"]);
	$postArray["deviceAccessPassword"] = $groupId;
	
	
	$deviceAddResponse = $deviceObj->deviceAddRequest($postArray);
	
	$syslevelArr = $syslevelObj->getSysytemConfigDeviceTypeRequest($postArray["deviceType"], $ociVersion);
	$systemLinePortFlag = false;
	$scaUtilityData = "";
	if(isset($syslevelArr["Success"]->staticLineOrdering)){
	    $systemLinePortFlag = strval($syslevelArr["Success"]->staticLineOrdering);
	}

	if(empty($deviceAddResponse["Error"])){
		// Ad users to SCA Start code
		if(isset($postArray["scaUsers"]) && $postArray["scaUsers"] != ""){
		    $scaOp = new SCAOperations();
		    $scaUsers = explode(";", $postArray["scaUsers"]);
                    
                    //Code added @ 17 July 2018
                    $sharedCallAppearance = $grpServiceList->compareSessionScaUserForDeviceInvetry();
                    array_pop($scaUsers);
                    array_pop($scaUsers);
                    $aa = 0;                    
                    $scaOperationArray = $grpServiceList->assignUnassignScaUser($sharedCallAppearance, $scaUsers);
                    if(count($scaOperationArray["unAssign"]) > 0){
                            $b = 0;
                            foreach ($scaOperationArray["unAssign"] as $k => $v)
                            {
                                    if ($v !== "scaUsers" and $v !== "")
                                    {
                                            $exp2 = explode(":", $v);
                                            $changes["scaServicePackUnassign"][$b] = $exp2[0];
                                            $a++;
                                    }
                            }
                    }if ($postArray["scaUsers"] == "scaUsers;")
                    {
                            $changes["sca"] = "NIL";
                    }
                    else
                    {
                            $a = 0;
                            $exp = explode(";", $postArray["scaUsers"]);
                            foreach ($exp as $k => $v)
                            {
                                    if ($v !== "scaUsers" and $v !== "")
                                    {
                                            $exp2 = explode(":", $v);
                                            $changes["sca"][$a]["id"] = $exp2[0];
                                            $changes["sca"][$a]["name"] = $exp2[1];
                                            $changes["sca"][$a]["linePort"] = $exp2[2];

                                            $changes["scaServicePack"][$a] = $exp2[0];
                                            $a++;
                                    }
                            }
                    }
                    //End code  
                    
                    //Code added @ 19 July 2018
                    foreach ($changes as $key => $value)
                    {
                        if($key == "scaServicePack"){
                            
                            //code to assign service pack to user
                            //print_R($_SESSION["userInfo"]["sharedCallAppearanceUsers"]);die;
                            //if($_SESSION["userInfo"]["sharedCallAppearanceUsers"]))
                            //$grpServiceList = new Services();
                            $changeSCAAssignLog = array();
                            $grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
                            $servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
                            $servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray);
                            $scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);
                            
                            foreach($scaServiceArray as $ssa=>$ssv){
                                $pos = strpos($ssv->col[0], "Shared Call Appearance");
                                if($pos === false){
                                }else{
                                    if($ssv->count == "1"){
                                        $servicePackToBeAssigned = $servicePackArray[$ssa][0];
                                    }
                                }
                            }
                            
                            
                            if(count($value) > 0){
                                foreach($value as $scaUserkey=>$scaUserVal){
                                    $responseServicePackUser = $grpServiceList->assignServicePackUser($scaUserVal, $servicePackToBeAssigned);
                                    if(empty($responseServicePackUser["Error"])){
                                        $message .= "<li>".$servicePackToBeAssigned." SCA service pack has been assigned to " . $scaUserVal. ".</li>";
                                        
                                    }
                                    //code ends for assign service pack to user
                                }
                                $services = new Services();
                                $oldScaAssignUsersId = array();
                                $newValue = array();
                                //print_r($_SESSION["userInfo"]);//die;
                                $oldScaUsers = $services->compareSessionScaUserForDeviceInvetry();
                                $oldScaUsersId = array();
                                //echo "<br/>Assign Old Sca Users=>";print_r($oldScaUsers);echo "<br/>";
                                foreach($oldScaUsers["IdArray"] as $oKey=>$oVal){
                                    $oldScaUsersIdArray = explode(":", $oVal);
                                    $oldScaAssignUsersId[] = $oldScaUsersIdArray[0].": ".$servicePackToBeAssigned;
                                    
                                }
                                foreach($value as $kVal){
                                    $newValue[] = $kVal.": ".$servicePackToBeAssigned;
                                }
                                $changeSCAAssignLog[0]["field"] = "scaServicePackAssign";
                                $changeSCAAssignLog[0]["old"] = implode(", ", $oldScaAssignUsersId);
                                $changeSCAAssignLog[0]["new"] = implode(", ", $newValue);
                                $a++;
                            }
                        }
                        if($key == "scaServicePackUnassign"){
                            
                            $grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
                            $servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
                            $userservicePackArray = array();
                            $servicePackToBeUnAssigned = "";
                            
                            
                            if(count($scaOperationArray["unAssign"]) > 0){
                                foreach ($scaOperationArray["unAssign"] as $uKey => $uVal)
                                {
                                    if ($uVal !== "scaUsers" and $uVal !== "")
                                    {
                                        $exp2 = explode(":", $uVal);
                                        $uid = $exp2[0];
                                        
                                        $userServices = new userServices();
                                        $userservicePackArray = $userServices->getUserServicesAll($exp2[0], $_SESSION["sp"]);
                                        $servicesArray = $grpServiceList->getServicePackServicesListUsers($userservicePackArray["Success"]["ServicePack"]["assigned"]);
                                        $scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);
                                        foreach($scaServiceArray as $ssa=>$ssv){
                                            $pos = strpos($ssv->col[0], "Shared Call Appearance");
                                            if($pos === false){
                                            }else{
                                                if($ssv->count == "1"){
                                                    $servicePackToBeUnAssigned = $userservicePackArray["Success"]["ServicePack"]["assigned"][$ssa];
                                                }
                                            }
                                        }
                                        $unAssignScaResponse = $grpServiceList->unAssignServicePackUser($uid, $servicePackToBeUnAssigned);
                                        if(empty($unAssignScaResponse["Error"])){
                                            $message .= "<li>".$servicePackToBeUnAssigned." SCA service pack has been un assigned to " . $uVal. ".</li>";
                                            
                                        }
                                        $unAssignId[] = $uid.": ".$servicePackToBeUnAssigned;
                                        
                                    }
                                }
                                $servicesArray = "";
                                $changeSCAUnassignLog = array();
                                
                                $services = new Services();
                                $oldScaUsers = $services->compareSessionScaUserForDeviceInvetry();
                                $oldScaUnAssignUsersId = array();
                                foreach($oldScaUsers["IdArray"] as $oKey=>$oVal){
                                    $oldScaUsersIdArray = explode(":", $oVal);
                                    $oldScaUnAssignUsersId[] = $oldScaUsersIdArray[0];
                                }
                                foreach($value as $kVal){
                                    $newValue[] = $kVal.": ".$servicePackToBeAssigned;
                                }
                                
                                $changeSCAUnassignLog[0]["field"] = "unAssignScaServicePack";
                                //$changeSCAUnassignLog[0]["old"] = implode(",", $oldScaUnAssignUsersId);
                                $changeSCAUnassignLog[0]["old"] = "None";
                                $changeSCAUnassignLog[0]["new"] = implode(", ", $unAssignId);
                                $a++;
                                
                            }
                            //code ends
                        }
                    }
                    
                    
		    $i = 0;
		    $addedScaUser = "";
		    $deviceDetails = $vdmOperationObj->getUserDeviceDetail($_SESSION["sp"], $_SESSION["groupId"], $postArray["deviceName"]);
		    foreach($scaUsers as $scaUser)
                    {
		       if($scaUser != ""){
		                $userInfo = explode(":", $scaUser);
		                $scaUserId = $userInfo[0];
		                $scaUserName = $userInfo[1];
		                $phoneNummber = $userInfo[2];
		                $ext = $userInfo[3];
$scaUtilityData="";
		                if($systemLinePortFlag == "true"){
		                    
		                    $vdmOperationsArr = $vdmOperationObj->getAllUserOfDevice($_SESSION["sp"], $_SESSION["groupId"], $postArray["deviceName"]);
		                    
		                    if($vdmOperationsArr["Error"] == ""){
		                        $scaUtilityData = $scaUtilityObj->scaPortCalculation($vdmOperationsArr["Success"],(int)$deviceDetails["Success"]["numberOfPorts"]);
		                    }
		                }
		                $linePortFromCriteria = constructLinePort($groupId, $deviceNameFromCriteria, $phoneNummber, $ext, $_SESSION["proxyDomain"], $_SESSION["defaultDomain"]);
		                $isUserAddedToSca = $scaOp->addDeviceToUserAsSCA($scaUserId, $deviceNameFromCriteria, $linePortFromCriteria, $systemLinePortFlag, $scaUtilityData);
		                
		                if(empty($isUserAddedToSca["Error"])){
		                    $deviceResponse["userAssign"] = "Success";
		                    $addedScaUser .= $scaUserName . " ";
		                }else{
		                    $assignFailure[$assignFailCount]['user'] = $scaUserName;
		                    $assignFailure[$assignFailCount]['reason'] = $isUserAddedToSca["Error"];
		                    $assignFailCount++;
		                }
		            $i++;
		        }
		    }
		
                    
                    
                    
                    //End code
                    
                }
                //End code
                
		if(!empty($postArray["customProfile"])){	
		    $addTags = addExpressCutomTags($db, $postArray["sp"], $postArray["groupId"], $postArray["deviceName"], $postArray["deviceType"], $postArray["customProfile"], $systemDefaultTagVal);
			
		}
		
		//tag bundles
		if(!empty($postArray["tagBundles"]) && count($postArray["tagBundles"]) > 0){
		    $tagBundlesTags = $dbobj->getAllCustomTagsFromTagBundles($postArray["tagBundles"]);
		    $addTags = addTagBundlesCustomTags($db, $postArray["sp"], $postArray["groupId"], $postArray["deviceName"], $tagBundlesTags);
		    $addTags = addExpressTagBundlesDefaultCutomTags($db, $postArray["sp"], $postArray["groupId"], $postArray["deviceName"], $postArray["tagBundles"]);
		    
		}
		
		/* Copy to temp array. */
		$postArrayTemp = $postArray;
                unset($postArrayTemp["tagBundles"]);                
		$postArrayTemp["scaUsers"] = str_replace(" ", ",", $addedScaUser);
                if(!empty($postArray["tagBundles"]) && count($postArray["tagBundles"]) > 0){
                    $postArrayTemp["tagBundles"] = implode(",", $postArray["tagBundles"]);
                }
		$addArrayChange = makeAddChangeArray($postArrayTemp);
		
		$deviceResponse["userAssignFail"] = $assignFailure;
		if( !empty($assignFailure) ){
		    $deviceResponse["status"] = "failure";
		    $deviceResponse["reason"] = "Assign Failure";
		    
		    $addArrayChange["scaUsers"] = "";
		}
		
		    $module = "Add SCA-Only Devices";                    
		    $isChangesAdded = $cLUObj->changeLogAddUtility($module, $postArray['deviceName'], $tabelName = "deviceInventoryAddChanges", $addArrayChange);
		    
                    
                    
		echo json_encode($deviceResponse);
	}else{
	    $deviceResponse["status"] = "failure";
	    $deviceResponse["reason"] = "Device Add Fail.";
	    $deviceResponse["deviceFailReason"] = $deviceAddResponse["Error"];
		echo json_encode($deviceResponse);
	}
	
	if($searchDeviceErrMsg != ""){
	    echo $searchDeviceErrMsg;
	}
}



function constructDeviceName($deviceNameCriteria, $groupId, $deviceType, $mac, $sn, $devIp) {
	$idValueMap = array();
	if(isset($deviceType) && !empty($deviceType)){
		$idValueMap[ResourceNameBuilder::DEV_TYPE] =$deviceType;
	}
	
	if(isset($mac) && !empty($mac)){
		$idValueMap[ResourceNameBuilder::MAC] = $mac;
	}
	
	if(isset($groupId) && !empty($groupId)){
		$idValueMap[ResourceNameBuilder::GRP_ID] = $groupId;
	}
	
	if(isset($sn) && !empty($sn)){
	    $idValueMap[ResourceNameBuilder::SN] = $sn;
	}
	
	if(isset($devIp) && !empty($devIp)){
	    $idValueMap[ResourceNameBuilder::DEV_IP] = $devIp;
	}
	
	$regexSet = array(
			ResourceNameBuilder::GRP_ID,
			ResourceNameBuilder::DEV_TYPE,
			ResourceNameBuilder::MAC,
	    ResourceNameBuilder::SN,
	    ResourceNameBuilder::DEV_IP
	);
	
	$nameAndIdBuilder = new NameAndIDBuilder($deviceNameCriteria, $idValueMap, $regexSet);
	$deviceName = $nameAndIdBuilder->getParsedId($deviceNameCriteria, $idValueMap);

	return $deviceName;
}

function constructLinePort($groupId, $devicename, $phoneNummber, $ext, $proxyDomain, $defaultDomain) {
	//$_SESSION["proxyDomain"]
	global $scaLinePortCriteria1,$scaLinePortCriteria2;
	$idValueMap = array();
        
        $proxyDomain = trim($proxyDomain);       //Code added @ 26 Sep 2018 removing white space at before and after
        $defaultDomain = trim($defaultDomain);   //Code added @ 26 Sep 2018 removing white space at before and after
	
	if ($phoneNummber != "") {
	    if (substr($phoneNummber,0,3) == "+1-") {
	        $phoneNummber = substr($phoneNummber, 3);
	    }
	}
	
	if(isset($devicename) && !empty($devicename)){
		$idValueMap[ResourceNameBuilder::DEV_NAME] =$devicename;
	}
	if(isset($phoneNummber) && !empty($phoneNummber)){
		$idValueMap[ResourceNameBuilder::DN] = $phoneNummber;
	}
	
	if(isset($ext) && !empty($ext)){
		$idValueMap[ResourceNameBuilder::EXT] = $ext;
	}
	
	if(isset($groupId) && !empty($groupId)){
		$idValueMap[ResourceNameBuilder::GRP_ID] = $groupId;
	}
	
	if(isset($defaultDomain) && !empty($defaultDomain)){
	    $idValueMap[ResourceNameBuilder::DEFAULT_DOMAIN] = $defaultDomain;
	}
	
	if(isset($proxyDomain) && !empty($proxyDomain)){
	    $idValueMap[ResourceNameBuilder::GRP_PROXY_DOMAIN] = $proxyDomain;
	}
	
	$regexSet = array(
            ResourceNameBuilder::DEV_NAME,
            ResourceNameBuilder::DN,
            ResourceNameBuilder::EXT,
            ResourceNameBuilder::GRP_ID,
            ResourceNameBuilder::DEFAULT_DOMAIN,
            ResourceNameBuilder::GRP_PROXY_DOMAIN
	);
	$nameAndIdBuilder = new NameAndIDBuilder($scaLinePortCriteria1, $idValueMap, $regexSet);
	$linePortName = $nameAndIdBuilder->getParsedId($scaLinePortCriteria1, $idValueMap);
	if(isset($linePortName) && empty($linePortName)) {
		$linePortName= $nameAndIdBuilder->getParsedId($scaLinePortCriteria2, $idValueMap);
	}
	return str_replace(" ", "_", $linePortName);
}


function constructDeviceUserName($groupId, $mac) {
	
	global $deviceAccessUserName1, $deviceAccessUserName2;
	
	$idValueMap = array();
	if(isset($mac) && !empty($mac)){
		$idValueMap[ResourceNameBuilder::MAC] = $mac;
	}
	
	if(isset($groupId) && !empty($groupId)){
		$idValueMap[ResourceNameBuilder::GRP_ID] = $groupId;
	}
	
	$regexSet = array(
			ResourceNameBuilder::MAC,
			ResourceNameBuilder::GRP_ID,
	);
	$nameAndIdBuilder = new NameAndIDBuilder($deviceAccessUserName1, $idValueMap, $regexSet);
	$deviceUname = $nameAndIdBuilder->getParsedId($deviceAccessUserName1, $idValueMap);
	
	if(isset($deviceUname) && empty($deviceUname)) {
		$deviceUname = $nameAndIdBuilder->getParsedId($deviceAccessUserName2, $idValueMap);
	}
	
	if(empty($deviceUname)){
		$deviceUname = $mac;
	}
	
	return $deviceUname;
}

function getUserLinePortFromDevice($userList, $scaUserId)
{
    $scaUserLinePort = "";
    foreach($userList["Success"] as $userKey => $userVal){
        if($userVal["userId"] == $scaUserId){
            $scaUserLinePort = $userVal["linePort"];
            break;
        }
    }
    
    return $scaUserLinePort;
}

function makeAddChangeArray($postArrayTemp) {
   
    unset($postArrayTemp["modifiedDevice"]);
    unset($postArrayTemp["deviceAccessPassword"]);
    unset($postArrayTemp["deviceAccessUserName"]);
    
    unset($postArrayTemp["hidDeviceType"]);
    unset($postArrayTemp["sp"]);
    unset($postArrayTemp["groupId"]);
    
    return $postArrayTemp;
}

function getOldScaUser($sessionScaUsers) {
    $oldScaUsers = "";
    if($sessionScaUsers != "") {
                $explodeUsers = explode(";", rtrim($sessionScaUsers, ";"));
            foreach($explodeUsers as $key => $uVal) {
                $explodeUsersInfo = explode(":", $uVal);
                $scaUserId = $explodeUsersInfo[0];
                $scaUserName = $explodeUsersInfo[1];
                $phoneNummber = $explodeUsersInfo[2];
                $ext = $explodeUsersInfo[3];

                $oldScaUsers .= $scaUserName ."(".$ext.")" . ",";
            }
            $oldScaUsers = rtrim($oldScaUsers, ",");
    }
    
    return $oldScaUsers;
}

/*Start New Code*/
function getLinePortDevTypeLookup() {
    global $db;
    $deviceSubList = array();
    //Code added @ 16 July 2019
    $whereCndtn = "";
    if($_SESSION['cluster_support']){
        $selectedCluster = $_SESSION['selectedCluster'];
        $whereCndtn = " WHERE clusterName ='$selectedCluster' ";
    }
    //End code
    $qryLPLU = "select * from systemDevices $whereCndtn ";
    $select_stmt = $db->prepare($qryLPLU);
    $select_stmt->execute();
    $i = 0;
    while($rs = $select_stmt->fetch())
    {
        $deviceSubList[$i]['deviceType'] = $rs['deviceType'];
        $deviceSubList[$i]['substitution'] = $rs['nameSubstitution'];
        $i++;
    }
    return $deviceSubList;
}

function replaceDevNameWithSubstitute($deviceName) {
    $newDeviceName = $deviceName;
    $subList = getLinePortDevTypeLookup();
    foreach($subList as $key => $value) {
        if(strpos($deviceName, $value['deviceType']) !== false) {
            $newDeviceName = str_replace($value['deviceType'], $value['substitution'], $deviceName);
        }
    }
    return $newDeviceName;
}
/*End New Code*/






?>