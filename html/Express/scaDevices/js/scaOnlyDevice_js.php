<?php
require_once("/var/www/html/Express/config.php");
checkLogin();

?>
<script>

$(function() {
	if ($.fn.button.noConflict) {
		var bootstrapButton = $.fn.button.noConflict();
		$.fn.bootstrapBtn = bootstrapButton;
		}
	$("#tabs").tabs();
	$("#loading2").hide();
	$("#scaDeleteBtn").hide();
	$("#scaNewDevice").hide();
	$("#searchBodyForm").show();
	$("#selectCheckDiv").hide();

	$('#scaSearchDeviceVal').on('keyup keypress', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) {
			e.preventDefault();
			//return false;\
			var scaDevice = $("#scaSearchDeviceVal").val();
			if(scaDevice != "")
			{
				$('#selectScaSearchDeviceVal').trigger('click');
			}
		  }
		});
	
	$('#newDevice').click(function(){
		$("#scaNewDevice").show();
		$("#confirmButton").show();
		$("#searchBodyForm").hide();
		$("#deviceType").prop("disabled", false);
		$("#modifiedDevice").val("");
		$("#scaUsers").val("");
		$("#scaDeleteBtn").hide();
		userList(false);
		$("#customProfile").html("");
		$("#customProfileDiv").hide();
		$("#tagBundles").html("");
		$("#tagBundleDiv").hide();
	});

	$('#addScaButtonCancel').click(function(){
		$("#scaNewDevice").hide();
		$(this).closest('form').find("input[type=text], textarea, select").val("");
		$("#selectCheckDiv").hide();
		 
	});
	
	$("#dialogScanDevice").dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		buttons: {
			"Complete": function() {
				completeDevice();
			},
			"Cancel": function() {
				$(this).dialog("close");
				$("#loading2").hide();
			},
			"Delete": function(){
				pendingProcess.push("Delete Sca Device");
				$("#scaDeviceDisplayName").html("");
				$("#dialogScanDevice").html('Deleting the Sca....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
				$(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled deleteBtn");
				$("#loading2").show();
				    	
               $.ajax({
					type: "POST",
					url: "scaDevices/deleteSca.php",
					data: {scaId : $("#modifiedDevice").val(), deleteSca : 1},
					success: function(result) {
						if(foundServerConErrorOnProcess(result, "Delete Sca Device")) {
	    					return false;
	                  	}
						$("#loading2").hide(); 
						var obj = jQuery.parseJSON(result);
						if(obj.Error){
							$("#dialogScanDevice").html(obj.Error[0]);
							$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
							$(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass('deleteBtn');
						}else{	
							$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
							//$(".ui-dialog-buttonpane button:contains('Done')").button().show();
							$(".ui-dialog-buttonpane button:contains('More Changes')").button().show().addClass('cancelButton');;
							$("#dialogScanDevice").html('SCA has been deleted');
							$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide().addClass('cancelButton');
							$(".ui-dialog-buttonpane button:contains('Delete')").button().hide().addClass('deleteBtn');
							$(".ui-dialog-buttonpane button:contains('Return to Main')").button().show().addClass('returnToMainButton');
							$("#identifyEvent").val("delete");
						}
					}
				});
			},
            "More Changes": function() {   
            	$("#loading2").show();
        		$("#scaNewDevice").hide();
        	//	$("#searchBodyForm").show();
        	//	$("#scaSearchDeviceVal").val('');
        		autocompleteDeviceName();
        		$("#identifyEvent").html('');	
        		//get the value of th eprevious event
               	var identifyEvent = $("#identifyEvent").val();	
               	if(identifyEvent == "delete"){
               		$("#scaOnly_devices").trigger("click");
               	}else if(identifyEvent == "modify"){
                	var deviceName = $("#modifiedDevice").val();
                	getDeviceInfo(deviceName);
               	}
                //
            	$(this).dialog("close");
            	$('html, body').animate({scrollTop: '0px'}, 300);
            },
            "Ok": function() {
            	$(this).dialog("close");
			},
			
		  "Done": function() {
			  resetForm();
            	$("#scaSearchDeviceVal").val('');
				autocompleteDeviceName();
            	$("#scaNewDevice").hide();
            	$("#searchBodyForm").show();
            	$(this).dialog("close");
            	$('html, body').animate({scrollTop: '0px'}, 300);
			},
			"Return to Main": function() {
				$(location).attr('href','main.php');
			},
			"Add Another Device": function() {
				$(this).dialog("close");
				resetForm();
				$("#scaNewDevice").show();
				$("#confirmButton").show();
				$("#searchBodyForm").hide();
				userList(false);
			},
		},
        open: function() {

        	setDialogDayNightMode($(this));
        	$(":button:contains('Complete')").addClass("subButton").show();
        	
        	$(":button:contains('Cancel')").addClass("cancelButton").show();

        	/*$('.ui-dialog-titlebar-close').addClass('ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only');
          	 $('.ui-dialog-titlebar-close').append('<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text">close</span>');*/
        
        	buttonsShowHide('More Changes', 'hide');
        	buttonsShowHide('Ok', 'hide');
        	buttonsShowHide('Delete', 'hide');
        	buttonsShowHide('Return to Main', 'hide');
        	buttonsShowHide('Add Another Device', 'hide');
        }
	});	
	
	var buttonsShowHide = function(b,e){
		if(e == "show"){
			$(".ui-dialog-buttonpane button:contains("+b+")").button().show();
		}else{
		 	$(".ui-dialog-buttonpane button:contains("+b+")").button().hide();
		}
	};

	var isSCAUser = function(userId, scaUserData) {
		var scaUser = false;
		if(!scaUserData) {
			return scaUser;
		}
		if(scaUserData.length > 0){
			for (var i = 0; i < scaUserData.length; i++) {
				if(userId == scaUserData[i].userId) {
					scaUser = true;
					break;
				}
			}
		}
		return scaUser;

		}
	var userList = function (checkSca, scaUserList){
		var deviceType = $("#deviceType").val();
		var userList = "";
		$.ajax({
			type: "POST",
			url: "scaDevices/getUsersToAssign.php",
			data:{},
			success: function(result){
				var obj = jQuery.parseJSON(result);
				console.log(obj);
				if(obj.length > 0){
					for (var i = 0; i < obj.length; i++) {
						if( !isSCAUser(obj[i].userId, scaUserList) ) {
							var userNumAndExt = obj[i].ext;
							if (obj[i].phoneNumber != "") {
                                                var userNum = obj[i].phoneNumber.split("+1-");
                                                userNum = userNum[1];
                                    
	                            userNumAndExt = userNum + "x" + userNumAndExt;
	                        }
							
							//var id = obj[i].userId + ":" + obj[i].firstName + " " + obj[i].lastName + ":" + obj[i].phoneNumber + ":" + obj[i].ext ; //code commented @ 23 July 2018
                                                        var id = obj[i].userId + ":" + obj[i].firstName + " " + obj[i].lastName + ":" + userNumAndExt ;  //code added @ 23 July 2018
							userList += '<li class="ui-state-default" id="'+id+'" value="'+obj[i]+'">' + obj[i].firstName + ' ' +  obj[i].lastName.replace("^\+1-") + "(" + userNumAndExt + ")"+ '</li>';
						}
						
					}
				}
				$("#sortable1").html(userList);
			}
		});
	};
	

	var validateDevice = function(){
		var dataToSend = $("form#sdDeviceForm").serializeArray();
		$.ajax({
			type: "POST",
			url: "scaDevices/sdValidate.php",
			data: dataToSend,
			success: function(result) {
				$("#loading2").hide();
				var errorExist = result.search("Error");
				var noChangesExist = result.search("No Changes");
				
				if(errorExist > -1 || noChangesExist > -1){
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
					$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
				}else{
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
					$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled").addClass('subButton');
				}
				$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
				$("#dialogScanDevice").dialog("option", "title", "Request Complete");
				$("#dialogScanDevice").html('<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelistDevice" class="confSettingTable"><tbody><tr><th colspan="2" align="center">Please confirm the settings below and click Complete to process your modifications.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');		    	
				$("#changelistDevice").append(result);
				$("#dialogScanDevice").append('</tbody></table>');
				$("#dialogScanDevice").dialog("open");
			}
		});
	};
	
	var completeDevice = function(){
		pendingProcess.push("Modify SCA-Only Devices");
		$("#dialogScanDevice").html('');
		var dataToSend = $("form#sdDeviceForm").serializeArray();
		//$("#loading2").show();
		$(".ui-dialog-buttonpane button:contains('Complete')").button().hide().addClass('subButton');
		$("#dialogScanDevice").html('Completing the Request....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
		$.ajax({
			type: "POST",
			url: "scaDevices/sdComplete.php",
			data: dataToSend,
			success: function(result) {
				if(foundServerConErrorOnProcess(result, "Modify SCA-Only Devices")) {
					return false;
              	} 
				$("#dialogScanDevice").html('');
				var obj = jQuery.parseJSON(result);
				if(obj.Error && obj.Error.length > 0){
						$("#dialogScanDevice").append('<br/>' + obj.Error);
						$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
						$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
						$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
						$(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide();
				}else if(obj.addRequest == "addRequest"){
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
					$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Add Another Device')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Done')").button().show().addClass('subButton');

					if(obj.status == "success"){
						$("#dialogScanDevice").append('<br/>Device Addedd Successfully');
					}else if(obj.status =="failure"){
						if( obj.reason == "Device Add Fail."){
							$("#dialogScanDevice").append('<br/>Error Adding Device:' + obj.deviceFailReason);
    					}else if(obj.reason == "Assign Failure"){
    						$("#dialogScanDevice").append('<br/>Device Addedd Successfully');
    						
    						if(obj.userAssignFail && obj.userAssignFail.length > 0){


    							tableForAssignUnAssignUsers(obj.userAssignFail, "Error Assigning Following Users");
    						}
    						
    					}
					
					}
					$("#identifyEvent").val("add");
					
				}else if(obj.modRequest == "modRequest"){
					
					if(obj.status == "success"){
						$("#dialogScanDevice").append('<br/>Device Updated Successfully');
					}
					if( obj.status == "failure" && obj.reason == "Error Modifying Device" ){
						$("#dialogScanDevice").append('<br/>Error Modifying Device:' + obj.deviceUpdateReason);
					} else if(  obj.status == "failure" && obj.reason == "Assign Unassign Failure" ){
						
						if(obj.userAssignFail && obj.userAssignFail.length > 0){
							tableForAssignUnAssignUsers(obj.userAssignFail, "Error Assigning Following Users");
						}

						if(obj.userUnAssignFail && obj.userUnAssignFail.length > 0){
							tableForAssignUnAssignUsers(obj.userUnAssignFail, "Error UnAssining Following Users");
						}
					}
					
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Done')").button().hide().addClass('subButton');
					$(".ui-dialog-buttonpane button:contains('More Changes')").button().show().addClass('cancelButton');
					$(".ui-dialog-buttonpane button:contains('Return to Main')").button().show().addClass('cancelButton');
					$("#identifyEvent").val("modify");
				}
				
			}
		});
	};
	
	$("#sdConfirmSetting").click(function(){
		$("#dialogScanDevice").html('');
		validateDevice();
	});
		
	$("#customProfileDiv").hide();
	$("#tagBundleDiv").hide();
	
	var autocompleteDeviceName = function(){
		
		var deviceList = "";
		
		var autoComplete = new Array();
		$.ajax({
			type: "GET",
			url: "scaDevices/getDeviceNameAutoFill.php",
			success: function(result){
				var explode = result.split(":");
				var deviceNameList = "";
				for (a = 0; a < explode.length; a++)
				{
					autoComplete[a] = explode[a];
					deviceNameList += $.trim(explode[a])+":"; 
				}
				$("#allDeviceList").html(deviceNameList);
				$("#scaSearchDeviceVal").autocomplete({
					source: autoComplete,
					appendTo: "#hidden-stuffSCAOnly"
				}).autocomplete( "widget" ).addClass( "deviceAutoClass" );
			} 			
		});
	};
	autocompleteDeviceName();
	
	var getDeviceInfo = function(deviceName){

		$.ajax({
			type: "POST",
			data: {deviceName : deviceName},
			url: "scaDevices/sdGetDeviceDetail.php",
			success: function(result){

				$("#loading2").hide();
				var obj = jQuery.parseJSON(result);
				// Error case.
				if(obj.Error){
					$("#dialogScanDevice").dialog("open");
					$("#dialogScanDevice").html(obj.Error.reason);
					$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
					resetForm();
					$("#newDevice").show();
					$("#scaDeviceDisplayName").html("");
					return false;
				}
				
				$("#scaNewDevice").show();
				$("#confirmButton").show();

				$("#deviceName").val(obj.deviceName);
				$("#modifiedDevice").val(obj.deviceName);
				$("#deviceType").val(obj.deviceType);
				$("#hidDeviceType").val(obj.deviceType);
				$("#protocol").val(obj.protocol);
				$("#netAddress").val(obj.netAddress);
				$("#port").val(obj.port);
				$("#macAddress").val(obj.macAddress);
				$("#serialNumber").val(obj.serialNumber);
				$("#numberOfPorts").val(obj.numberOfPorts);
				$("#numberOfAssignedPorts").val(obj.numberOfAssignedPorts);
				$("#transportProtocol").val(obj.transportProtocol);
				$("#description").val(obj.description);
				$("#stunServerNetAddress").val(obj.stunServerNetAddress);
				$("#outboundProxyServerNetAddress").val(obj.outboundProxyServerNetAddress);
				$("#physicalLocation").val(obj.physicalLocation);
				
				$("#deviceName").attr("disabled", true);				
				$("#deviceType").attr("disabled", true);				
				//$("#scaNewDevice").show();
				$("#deviceName").attr("readonly", true);
				//loader hide()
				//$("#scaNewDevice").show();
				$("#scaDeleteBtn").show();
				$("#loading2").hide();
				$("#scaNewDevice").show();
				getUserForDevice(obj.deviceName);
				
				if(obj.deviceType != ""){
					getCustomTag(obj.customProfile);
					$("#customProfileDiv").show();
					getTagBundles(obj.tagBundles);
				}
			} 			
		});	
	}
	
	$("#selectScaSearchDeviceVal").on("click", function()
	{
		$("#scaUsers").val("");
		var deviceId = $.trim($("#scaSearchDeviceVal").val());
		if(deviceId == ""){ return false; }
		var deviceName = deviceId.split(" (");
		$("#loading2").show();
	//	$("#scaNewDevice").show();
	//	$("#confirmButton").show();
		$("#searchBodyForm").hide();
		$("#scaDeviceDisplayName").html(deviceName[0]);
		getDeviceInfo(deviceName[0]);
		
 });
	
 	var getDeviceTypeDropDown = function(){
		var deviceList = "<option value=\"\">None</option>";
		$.ajax({
			type: "POST",
			url: "scaDevices/getDeviceTypeDropDown.php",
			data: {},
			success: function(result){
				var obj = jQuery.parseJSON(result);
				for (var i = 0; i < obj.Success.length; i++) {
					deviceList += '<option value="'+obj.Success[i]+'">' + obj.Success[i] + '</option>';
				}
				$("#deviceType").html(deviceList);
			}			
		});
	};
	getDeviceTypeDropDown();
	
	var getCustomTag = function(customTag){
		var html = "<option value = \"\">None</option>";
		var deviceType = $("#deviceType").val();
		$.ajax({
			type: "POST",
			url: "scaDevices/getCustomProfiles.php",
			data:{deviceType:deviceType, dataFor:"profile"},
			success: function(result){
				if(result)  {
    				var obj = jQuery.parseJSON(result);
    				
    				if(obj.length > 0){
    					for (var i = 0; i < obj.length; i++) {
    						html += '<option value="'+obj[i][1]+'">' + obj[i][0] + '</option>';
    					}
    				}else{
    					html += '<option value="">No Profiles</option>';
    				}
    				$("#customProfile").html(html);
    				$("#customProfileDiv").show();
				}else{
					$("#customProfile").html("");
					$("#customProfileDiv").hide();
				}
				
				if(customTag != "" || customTag != 0){
					$("#customProfile").val(customTag);	
					$("#customProfileDiv").show();
				}
				
			}
		});
	};

	var getTagBundles = function(bundles){
		var html = "";
		var deviceType = $("#deviceType").val();
		$.ajax({
			type: "POST",
			url: "scaDevices/getCustomProfiles.php",
			data:{deviceType:deviceType, dataFor:"tagBundle"},
			success: function(result){
                                //debugger;
				if(result)  {
    				var obj = jQuery.parseJSON(result);
    				
    				if(obj.length > 0){
    					for (var i = 0; i < obj.length; i++) {
    						html += '<option value="'+obj[i]+'">' + obj[i] + '</option>';
    					}
    				}else{
    					html += '<option value="">No Profiles</option>';
    				}
    				$("#tagBundles").html(html);
    				$("#tagBundleDiv").show();
				}else{
					$("#tagBundles").html("");
    				$("#tagBundleDiv").hide();
				}
				
				
				if(bundles != "" || bundles != 0){
					var res = bundles.split(";");
					$("#tagBundles").val(res);
					//$("#tagBundles").multiselect("refresh");
					/*for (var j = 0; j < res.length; j++){
						$("#tagBundles").val(bundles[j]);
					}
					
					$("#tagBundles").prop(bundles);*/
					$("#tagBundleDiv").show();	
				}
				
			}
		});
	};
	
	$("#deviceType").on("change", function(){
		getCustomTag(0);
		getTagBundles(0);		
	});

	$("#sortable1, #sortable2").sortable({
		placeholder: "ui-state-highlight",
		connectWith: "#sortable1, #sortable2",
		cursor: "crosshair",
		update: function(event, ui)
		{
			var userList = "";
			var order = $("#sortable2").sortable("toArray");
			for (i = 0; i < order.length; i++)
			{
				userList += order[i] + ";";
			}                        
                        var userList = userList+"scaUsers;";
                        //alert(userList);
			$("#scaUsers").val(userList);
		}
	}).disableSelection();
	
	
	$("#searchDeviceCancel, #scaOnly_devices").click(function(){
		resetForm();
		$("#newDevice").show();
		$("#deviceTabName").html("SCA-Only Devices");
	});
	
	var resetForm = function(){
		$('#sdDeviceForm').val("");
		$("#sdDeviceForm").find("input[type=text], textarea").val("");
		$("#deviceType").val("");
		$("#customProfile").val("");
		$("#protocol").val("");
		$("#transportProtocol").val("");
		$("#sortable1").html("");
		$("#sortable2").html("");
		$('html, body').animate({scrollTop: '0px'}, 300);
		$("#scaNewDevice").hide();
		$("#searchBodyForm").show();
		$("#scaSearchDeviceVal").val("");
		$("#loading2").hide();
		$("#scaDeviceDisplayName").html("");
	};
	
	var resetDeviceForm = function(){
		$('#searchCriteriaDevice').val("");
		$("#searchDeviceOption").find("input[type=text], textarea").val("");
		$('html, body').animate({scrollTop: '0px'}, 300);
		$("#deviceTable").hide();
	};
	
	
	var getUserForDevice = function(deviceName){
		var userLists = "";
		
		$.ajax({
			type: "POST",
			url: "scaDevices/getUserForDevice.php",
			data: {deviceName : deviceName},
			success: function(result){
				//alert('Result Testing - '+ result);
				var obj = result ? jQuery.parseJSON(result): null;
				
				if(obj && obj.length > 0){
					for (var i = 0; i < obj.length; i++) {
						var userNumAndExt = obj[i].ext;                                                
						if (obj[i].phoneNumber != "") {
                                                    //var userNum = obj[i].phoneNumber.split("+1-");
                                                    var userNum = obj[i].phoneNumber;
                                                    userNumAndExt = userNum + "x" + userNumAndExt;
                                                }
						
						//var id = obj[i].userId + ":" + obj[i].firstName + " " + obj[i].lastName + ":" + obj[i].phoneNumber + ":" + obj[i].ext ; //code commented on 23 July 2018
                                                var id = obj[i].userId + ":" + obj[i].firstName + " " + obj[i].lastName + ":" +userNumAndExt ; //code added on 23 July 2018
						userLists += '<li class="ui-state-default" id="'+id+'" value="'+obj[i]+'">' + obj[i].firstName + ' ' +  obj[i].lastName.replace("^\+1-") + "(" + userNumAndExt + ")"+ '</li>';
					}
				}
				
				$("#sortable2").html(userLists);
				var userListH = "";
				var order = $("#sortable2").sortable("toArray");
				for (i = 0; i < order.length; i++)
				{
					userListH += order[i] + ";";
				}
				$("#scaUsers").val(userListH);
				userList(true, obj);
			}			
		});
	};
	
	$("#device_search_button").click(function(){
		resetForm();
		$("#deviceTabName").html("Device Search");
		$("#selectCheckDiv").hide();
	});
	
	$("#device_search_button").click(function(){
		$("#downloadCSVBtn").hide();
		$('#searchCriteria').val("none");
		resetDeviceForm();
	});
	
	$("#scaDeleteBtn").click(function(){
		 
		$("#dialogScanDevice").dialog("open");
		$("#dialogScanDevice").html("Are you sure you want to delete this Sca Only Device?");
		$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
		$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
		$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
		$(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");
		$(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass('deleteBtn');
		$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
		
	});	

	$(".tabs").click(function(){
			$("#scaDeviceDisplayName").html("");
		});


	$("#macAddress").on("input", function()
	{
		
		var macAddress = $("#macAddress").val();
		var macToLowerCase = macAddress.toLowerCase();
		var lastElmt = macToLowerCase.substr(-1);
		if((lastElmt >= "0" && lastElmt <= "9") || (lastElmt >= "a" && lastElmt <= "f")){
			//do something
	    }else{
	   		$("#macAddress").val(macAddress.slice(0,-1));
	    }
		
		  var macAddress = $("#macAddress").val();
		   if(macAddress.length > 0 && macAddress.length < 12){
			   $("#macAddress").attr('style', 'border-color :red !important');
		   }else{
			   $("#macAddress").attr('style', 'border-color :#002c60 !important');
			}
	});


	$(document).on('input', '#serialNumber', function(){
		var el = $(this);
		var val = el.val();
		if(! isNaN(val))
		{
			if(val.length <= 8)
		    {
				$(this).val(val);
		    }
		    else
			{
		    	$(this).val(val.slice(0,8));
		    }
		}
		else
		{
			$(this).val(val.slice(0, -1));
		}
		onSnChange(el.val());
   }); 
	   
	var tableForAssignUnAssignUsers = function(object, headTitle)
	{

		var user = "";
		var reason = "";
		var tabelRow = "";
		
		$.each( object, function( key, value ) {
				user = value.user;
				reason = value.reason;
			 
			tabelRow += '<tr><td class="errorTableRows" style="background:#ac5f5d;">' + user + '</td> <td class="errorTableRows"> '+ reason +' </td></tr>';
        });

		$("#dialogScanDevice").append('<br/><table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelistDevice"> <tbody> <tr><td colspan="2"><table width="100%" cellpadding="5"><tbody><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td> = Assign Unassign Failure</td></tr><tr><td> </td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp</td><td width="50%">&nbsp;</td></tr><tr><td width="50%">'+headTitle+'</td><td width="50%">&nbsp;</td></tr>');		    	
		$("#changelistDevice").append(tabelRow);
		$("#dialogScanDevice").append('</tbody></table>');

	}

	var onSnChange =  function(sn) {
		if(sn.length == 0 || (sn.length == 8 || sn.length == 7) && sn <= 16777215) {
			$("#serialNumber").attr('style', 'border-color :#002c60 !important;');	
			$("#sdConfirmSetting").prop("disabled", false);
			$("#serialNumber").tooltip('disable');
			$("#serialNumber").removeAttr( "title" )
			
		}else if(sn && (sn.length > 8 || sn.length < 7)){
			$("#serialNumber").attr('style', 'border-color :red !important');
			$("#serialNumber").attr("title", "Serial Number length should be 8 or 7");
			$("#sdConfirmSetting").prop("disabled", true);
			$("#serialNumber").tooltip();
		}else if(sn && (sn.length == 7 || sn.length == 8) && sn > 16777215){
			$("#serialNumber").attr('style', 'border-color :red !important');
			$("#serialNumber").attr("title", "Serial number value cannot be greater than 16777215");
			$("#serialNumber").tooltip();
			$("#sdConfirmSetting").prop("disabled", true);
		}
	}
	
});

</script>