<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");

$deviceType = isset($_POST["deviceType"]) ? $_POST["deviceType"] : "";
if($_POST["dataFor"] == "profile"){
    $customProfiles = array();
    
    $deviceObj = new DeviceOperations();
    $customProfiles = $deviceObj->getCustomProfiles($deviceType);
    
    if(count($customProfiles) > 0){
        echo json_encode($customProfiles);
    }
}

if($_POST["dataFor"] == "tagBundle"){
    $tagBundlesArr = array();
    
    $deviceObj = new DeviceOperations();
    $tagBundlesArr = $deviceObj->getAllTagBundlesData($deviceType);
    
    if(count($tagBundlesArr) > 0){
        echo json_encode($tagBundlesArr);
    }
}

//die;
?>