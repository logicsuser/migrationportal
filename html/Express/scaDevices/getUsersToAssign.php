<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/GroupLevelOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");

$deviceObj = new DeviceOperations();

$providerId = $_SESSION["sp"];
$groupId = $_SESSION["groupId"];

$glop = new GroupLevelOperations();
$users = $glop->getUsersInGroup($providerId, $groupId);

$users = $deviceObj->availableUsers($users);
echo json_encode($users);

?>					