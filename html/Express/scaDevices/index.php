
<style>
.rightScrollableDesc {
	width: 36%;
	margin-left: 4%;
}

.leftScrollableDesc {
	clear: both;
	width: 36%;
}

.scrollableList {
	height: 250px;
	width: 100%;
	margin-left: auto;
	margin-right: auto;
	overflow-y: scroll;
}

</style>
<div class="Sub_SCA_Only_Devices" id="Sub_SCA_Only_Devices">
	<div id="modUserBody">
		<div class="searchBodyForm" id="searchBodyForm">
		<h2 class="adminUserText" id=""></h2>
			<form name="scaFormSearchOption" id="scaSearchOption" method="POST" class="">
				<div class="deviceSearchTab">
					<div class="row">
						<div class="col-md-11 adminSelectDiv">
						<div class="form-group">
							<label class="labelText" for="scaSearchDeviceVal">Search Devices:</label><span class="required">*</span><br />
							<input type="text" class="autoFill magnify selectBlue" name="scaSearchDeviceVal"
								id="scaSearchDeviceVal" size="35">
							<div id="hidden-stuffSCAOnly" style="height: 0px;"></div>
						</div>
						</div>
                                            <?php if($devicesAddDelLogPermission == "yes"){ ?>
						<div class="col-md-1 adminAddBtnDiv">
							<div class="form-group">
								<p class="register-submit addAdminBtnIcon" id="addDevice" style="">
									<img src="images\icons\add_admin_rest.png" data-alt-src="images\icons\add_admin_over.png" name="newDevice" id="newDevice" class="">
								</p>
								<label class="labelText labelTextOpp" style=""><span>Add</span><span style="margin: 0 -20px 0 0;">Device</span></label>
							</div>
						</div>
                                            <?php } ?>
					</div>	
					
						
					<div class="col-md-12">
						<div class="form-group alignBtn">
								<input type="button" name="selectScaSearchDeviceVal"
									id="selectScaSearchDeviceVal" value="Get"
									class="submitAndView" />
						</div>
					</div>
						
						
				</div>
			</form>
		</div>
	</div>
</div>

<div class="" id="scaNewDevice">
	<form name="sdDeviceForm" id="sdDeviceForm" method="POST"
		class="deviceFrm">
		<input type="hidden" name="modifiedDevice" id="modifiedDevice"
			value="" />

		<div class="row">
			<div class="">
				<div class="form-group">
					<h2 class="subBannerCustom alignCenter" id="scaDeviceDisplayName"></h2>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="">
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="deviceType">Identity/Device Profile
							Type:</label><span class="required">*</span><br />
							<div class="dropdown-wrap">
								<select name="deviceType" id="deviceType"></select>
							</div>
						<input type="hidden" name="hidDeviceType" id="hidDeviceType"
							value="" />
					</div>
				</div>
				<?php if(isset($license["customProfile"]) && $license["customProfile"] =="true") {?>
				
				<div class="col-md-6">
					<div class="form-group" id="customProfileDiv">
						<label class="labelText" for="customProfile">Custom Profile:</label><br />
						<div class="dropdown-wrap">
						<select name="customProfile" id="customProfile"></select>
						</div>
					</div>
				</div>
				<div class="col-md-6" style="" id="tagBundleDiv">
						<div class="form-group"> 
							<label class="labelText" for=""><b>Device Management Tag Bundles:</b></label>
							<div class="" id="modTagBundleId">
                            	<select name="tagBundles[]" id="tagBundles" size="3" multiple= "">	
                            	</select>
							</div>
						</div>
					</div>
					
					<?php } ?>	
			</div>
		</div>
		<div class="row">
			<div class="">
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="protocol">Protocol:</label><br />
						<div class="dropdown-wrap">
							<select	name="protocol" id="protocol"><option value="SIP 2.0">SIP 2.0</option></select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="netAddress">Host Name/IP Address:</label>
						 <?php 
                                                                if(strpos($scaOnlyCriteria, "DEV_IP") > 0){
                                                                    echo "<span class='required'>*</span>";
                                                                } 
                                                        ?><br />
						<input type="text" name="netAddress" id="netAddress" value="">
					</div>

				</div>
			</div>
		</div>
		
		
		
		<div class="row">
			 
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="protocol">Port:</label><br />
						<input type="text" name="port" id="port" value="" >
					</div>
				</div>				
				
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="transportProtocol">Transport:</label><br />
						<div class="dropdown-wrap">
						<select name="transportProtocol" id="transportProtocol">
							<option value="Unspecified">Unspecified</option>
							<option value="UDP">UDP</option>
							<option value="TCP">TCP</option>
						</select>
						</div>
					</div>
				</div>
			 
		</div>
		
		
		
		<div class="row">
			<div class="">
				
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="macAddress">MAC Address:</label>
						<?php 
                                                                if(strpos($scaOnlyCriteria, "MAC") > 0){
                                                                    echo "<span class='required'>*</span>";
                                                                } 
                                                        ?><br />
						<input type="text" name="macAddress" id="macAddress" value=""
							size="12" maxlength="12">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="serialNumber">Serial Number:</label>
						<?php 
                                                                if(strpos($scaOnlyCriteria, "SN") > 0){
                                                                    echo "<span class='required'>*</span>";
                                                                } 
                                                        ?><br />
						<input type="text" name="serialNumber" id="serialNumber" size="35">
					</div>
				</div>
				
			</div>
		</div>
		<div class="row">
			<div class="">
				
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="description">Description:</label><br />
						<input type="text" name="description" id="description" value=""
							size="35">
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="outboundProxyServerNetAddress">Outbound
							Proxy Server:</label><br /> <input type="text"
							name="outboundProxyServerNetAddress"
							id="outboundProxyServerNetAddress" size="35">
					</div>
				</div>
		</div>
		</div>
		<div class="row">
			<div class="">
				
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="stunServerNetAddress">STUN Server:</label><br />
						<input type="text" name="stunServerNetAddress"
							id="stunServerNetAddress" size="35">
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="physicalLocation">Physical Location:</label><br />
						<input type="text" name="physicalLocation" id="physicalLocation"
							size="35">
					</div>
				</div>
			</div>
		</div>
		 
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6" style="padding-left:0;">
					<div class="form-group alignCenter">
						<label class="labelTextGrey" for="sortable1">Available Users:</label><br />
						<div class="form-group scrollableListCustom1">
							<ul id="sortable1" class="connectedSortable ui-sortable"></ul>
						</div>
					</div>
				</div>
				<div class="col-md-6" style="padding-right:0;">
					<div class="form-group alignCenter">
						<label class="labelTextGrey" for="sortable2">Shared Call
							Appearance Users: </label><br />
						<div class="form-group scrollableListCustom2">
							<ul id="sortable2" class="connectedSortable ui-sortable">
							</ul>
							<input type="hidden" name="scaUsers" id="scaUsers" value="">
						</div>
					</div>
				</div>
			</div>
		</div>
		 <div class="row">
			<div class="form-group alignBtn" id="confirmButton">
				<input type="button" id="scaDeleteBtn" value="Delete SCA" class="deleteBtn marginRightButton">
				<input class="subButton marginRightButton" type="button" id="sdConfirmSetting" value="Confirm Settings">
				<input type="button" id="searchDeviceCancel" value="Cancel" class="cancelButton marginRightButton">
			</div>
		</div>
	</form>
</div>
<div class="row">
			<div class="">
				<div class="form-group">
				<input type="hidden" name="identifyEvent" id="identifyEvent" value="" />
					<div class="loading" id="loading2">
						<img src="/Express/images/ajax-loader.gif">
					</div>
					<div id="deviceListDiv"></div>
				</div>
			</div>
		</div>
<div id="dialogScanDevice" class="dialogClass labelTextGreyHeadingRc"></div>