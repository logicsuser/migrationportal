<?php

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/sca/SCAOperations.php");
include("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");

$providerId = $_SESSION["sp"];
$groupId = $_SESSION["groupId"];
$scaDeviceId = $_POST['scaId'];
$cLUObj = new ChangeLogUtility($scaDeviceId, $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
$deleteResponse['Error'] ="";
$deleteResponse['Success'] ="";
if(!empty($scaDeviceId) && isset($scaDeviceId)){
    $dop = new DeviceOperations();
    $scaDop = new SCAOperations();
    $getAllUsersOfDevice = $dop->getAllUsersforDevice($providerId, $groupId, $scaDeviceId);
    if(count($getAllUsersOfDevice['Success']) > 0){
       foreach ($getAllUsersOfDevice['Success'] as $key => $val){
            $deleteResponse = $scaDop->deleteDevicesFromSCA($val['userId'], $scaDeviceId, $val['linePort']);
        }
    }
    if($deleteResponse['Error'] != ""){
        $deviceScaDeleted['Error'] = $deleteResponse['Error'];
    }else{
        
          $deviceScaDeleted       = $dop->getDeleteDevice($scaDeviceId,$providerId,$groupId);         
          /* delete sca devices changeLog */
          $cLUObj->changeLogDeleteUtility("Delete SCA Device", $scaDeviceId, "deviceInventoryModChanges", "");
    }
}
echo json_encode($deviceScaDeleted);
?>

