<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
$syslevelObj = new sysLevelDeviceOperations();

$deviceObj = new DeviceOperations();
$vdmObj = new VdmOperations();

$deviceName = isset($_POST["deviceName"]) ? $_POST["deviceName"] : "";
//print_r($postArray);
$providerid = $postArray["sp"] = $_SESSION["sp"];
$groupId = $postArray["groupId"] = $_SESSION["groupId"];
$deviceName = $postArray["deviceName"] = $deviceName;

// check device is exist.
checkIsDeviceExist($deviceName);

$deviceInfo = $deviceObj->getDeviceDetailRequest($postArray);

$setName = $syslevelObj->getTagSetNameByDeviceType($deviceInfo ["Success"] ["deviceType"], $ociVersion);
$systemDefaultTagVal = "";
if($setName["Success"] != "" && !empty($setName["Success"])){
    $defaultSettags = $syslevelObj->getDefaultTagListByTagSet($setName["Success"]);
    $phoneTemplateName = "%phone-template%";
    if(array_key_exists($phoneTemplateName, $defaultSettags["Success"])){
        $systemDefaultTagVal = $defaultSettags["Success"]["%phone-template%"][0];
    }
    //print_r($systemDefaultTagVal);
}

$deviceInfo["Success"]["customProfile"] = "";
$deviceInfo["Success"]["scaUsers"] = "";
$deviceInfo["Success"]["tagBundles"] = "";

if(empty($deviceInfo["Error"])){
	
	$customTagsList = $vdmObj->getDeviceCustomTags($providerid, $groupId, $deviceName);
	
	if(empty($customTagsList["Error"])){
		$deviceInfo["Success"]["deviceName"] = trim($deviceName);
		$tagsList = $customTagsList["Success"];
		//if(count($tagsList) > 0){
		    $customProfileVal = isset($tagsList['%phone-template%']) ? $tagsList['%phone-template%'] : "";
			if(!empty($customProfileVal[0])){
				$deviceInfo["Success"]["customProfile"] = $customProfileVal[0];
			}else{
			    $deviceInfo["Success"]["customProfile"] = $systemDefaultTagVal;
			}
			
			$tagBundleval = isset($tagsList['%Express_Tag_Bundle%']) ? $tagsList['%Express_Tag_Bundle%'] : "";
			if(!empty($tagBundleval[0])){
			    $deviceInfo["Success"]["tagBundles"] = $tagBundleval[0];
			}
		//}
	}
	$_SESSION["sca"]["deviceInfo"] = $deviceInfo["Success"];
	//print_r($deviceInfo["Success"]);
	echo json_encode($deviceInfo["Success"]);
	
}

function checkIsDeviceExist($deviceName)
{
    $isDeviceExist = false;
    if(isset($_SESSION["scaOnlyDeviceNameAutoFill"])){
        //foreach ($_SESSION["scaOnlyDeviceNameAutoFill"] as $key => $value)
        //{
        
        if(isset($_SESSION["scaOnlyDeviceNameAutoFill"][$deviceName])){
                $isDeviceExist = true;
              
            }
        //}
    }
   
    if( !$isDeviceExist){
        $deviceInfo["Error"]["reason"] = "<b>" .$deviceName. "</b> Device Not Exist";
        echo json_encode($deviceInfo);
        die;
    }
}
?>