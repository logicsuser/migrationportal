<?php

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");

$deviceType = isset($_POST["deviceType"]) ? $_POST["deviceType"] : "";

//sql for filter devices types
$ct = 0;
$scaOnlyDeviceTypeFilter = array();
$deviceTypeDropDownResult["Error"] = "";
$deviceTypeDropDownResult["Success"] = "";

    //Code added @ 16 July 2019
    $whereCndtn = "";
    if($_SESSION['cluster_support']){
        $selectedCluster = $_SESSION['selectedCluster'];
        $whereCndtn = " AND clusterName ='$selectedCluster' ";
    }
    //End code

$query = "SELECT deviceType FROM systemDevices WHERE scaOnly = 'Yes' $whereCndtn ";

$qwr = $db->query($query);
while ($r = $qwr->fetch()) {
    $scaOnlyDeviceTypeFilter[$ct] = $r["deviceType"];
    $ct++;
}
$deviceTypeListMergeArray = array_values(array_unique ($scaOnlyDeviceTypeFilter));
//code ends

$deviceObj = new DeviceOperations();
$deviceTypeDropDown = $deviceObj->getAllDeviceTypeListOfSystem();
//echo "<pre>"; print_r($deviceTypeDropDown);

if(empty($deviceTypeDropDown["Error"])){
    $deviceTypeDropDownResult["Success"] = array_values(array_intersect($deviceTypeDropDown["Success"], $deviceTypeListMergeArray));
}else{
    $deviceTypeDropDownResult["Error"] = $deviceTypeDropDown["Error"];
}


echo json_encode($deviceTypeDropDownResult);
die;
?>