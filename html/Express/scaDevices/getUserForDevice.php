<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");

$deviceName = isset($_POST["deviceName"]) ? $_POST["deviceName"] : "";
$userList = array();

$serviceProviderId = $_SESSION["sp"];
$groupId = $_SESSION["groupId"];
$usersScaListStr    = "";
$usersScaListStrDup = "";

$deviceObj = new DeviceOperations();
$userList = $deviceObj->getAllUsersforDevice($serviceProviderId, $groupId, $deviceName);
if(empty($userList["Error"])){
	$_SESSION["sca"]["deviceInfo"]["userList"]= $userList["Success"];
	$userNumAndExt = "";
	$userCount = 0;
	foreach($userList["Success"] as $key=>$val){
		
		$usersScaList[$userCount]["userId"] = $val['userId'];
		$usersScaList[$userCount]["lastName"] = $val["lastName"];
		$usersScaList[$userCount]["firstName"] = $val["firstName"];
		$usersScaList[$userCount]["phoneNumber"] = $val["phoneNumber"];
		$usersScaList[$userCount]["ext"] = $val["extn"];
		$userCount++;
		
                //Code commented @ 26 Sep 2018 regarding EX-816 because this session variable named scaUsers is different from post variable scaUsers;
		//$usersScaListStr .= $val["userId"].":".$val["firstName"]." ".$val["lastName"].":".$val["phoneNumber"].":".$val["extn"].";";
                
                //ocde added @ 23 July 2018
                $phoneNExt = $val["extn"];
                if(isset($val["phoneNumber"]) && trim($val["phoneNumber"])!="")
                {
                        if($phoneNExt!=""){
                            $phoneNExt = $val["phoneNumber"]."x".$val["extn"];
                        }else{
                            $phoneNExt = $val["phoneNumber"];
                        }
                }                	
                $usersScaListStrDup .= $val["userId"].":".$val["firstName"]." ".$val["lastName"].":".$phoneNExt.";";	
                //End code
                //Code added @ 26 Sep 2018
                $usersScaListStr    .= $val["userId"].":".$val["firstName"]." ".$val["lastName"].":".$phoneNExt.";";
                //Code end
	}
	
	$_SESSION["sca"]["deviceInfo"]["scaUsers"]   = $usersScaListStr;
        $_SESSION["sca"]["deviceInfo"]["scaUsersDup"]= $usersScaListStrDup;  //code added @ 23 July 2018
	
	if(count($userList["Success"]) > 0){
		echo json_encode($usersScaList);
	}
}
?>