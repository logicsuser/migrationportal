<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
require_once ("/var/www/html/Express/scaDevices/SCADBOperations.php");
require_once ("/var/www/html/Express/util/formDataArrayDiff.php");

//Code added @ 17 July 2018
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/userServices.php");
//End code



$diffObj = new FormDataArrayDiff();
$dbobj = new SCADBOperations();
$device = new DeviceOperations();
$changeString = "";
$_SESSION['deviceUpdate'] = false;
$validateData = $_SESSION["validateData"];

$postArray = array();
$postArray = $_POST;


if(isset($postArray["tagBundles"][0]) && $postArray["tagBundles"][0] != ""){
    $postArray["tagBundles"] = implode(";", $postArray["tagBundles"]);
}else{
    $postArray["tagBundles"] = "";
}

$formDataArray = array(
		"deviceName" => "Device Name",
		"deviceType" => "Device Type",
                "scaCustomProfile"=> "Custom Profile",
		"protocol" => "Protocol",
		"port" => "Port",
		"netAddress" => "Host Name/IP Address",
		"transportProtocol" => "Transport",
		"macAddress" => "MAC Address",
		"serialNumber" => "Serial Number",
		"description" => "Description",
		"outboundProxyServerNetAddress" => "Outbound Proxy Server",
		"stunServerNetAddress" => "STUN Server",
		"physicalLocation" => "Physical Location",
		"customProfile" => "Custom Profile",
		"scaUsers" => "SCA Users",
                "tagBundles" => "Tag Bundles"
		);

//newcode starts
$error = "";
$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
$usersId = "";

/*echo "<pre>Session Data Array - ";
print_r($_SESSION["sca"]);
echo "<br />Post Data Array - ";
print_r($_POST);*/



if(isset($postArray['modifiedDevice']) && !empty($postArray['modifiedDevice'])){    
    foreach($postArray as $key=>$val){
		foreach($_SESSION["sca"]["deviceInfo"] as $key1=>$val1){

			$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';

			if($key == $key1){
				$val = trim($val);                                
                                
				if($val <> $val1){
				    if($key == "tagBundles"){
				        $expPost = explode(";", $postArray["tagBundles"]); 
				        $expSession = explode(";", $_SESSION["sca"]["deviceInfo"]["tagBundles"]);
				      
				        $diffArr = $diffObj->diffInTwoArray($expPost, $expSession);
				        if(isset($diffArr["assigned"]) && $diffArr["assigned"][0] != ""){
				            $assignTag = $dbobj->getAllCustomTagsFromTagBundles($diffArr["assigned"]);
				            $changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Tags to be add</td><td class="errorTableRows">'.implode(", ", array_keys($assignTag)).' </td></tr>';
				           
				        }
				        if(isset($diffArr["removed"]) && $diffArr["removed"][0] != ""){
				            $removeTag = $dbobj->getAllCustomTagsFromTagBundles($diffArr["removed"]);
				            $changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Tags to be delete</td><td class="errorTableRows">'.implode(", ", array_keys($removeTag)).' </td></tr>';
				           
				        }
				    }
					
						$_SESSION['deviceUpdate'] = true;
						//if($key == "netAddress" && $val != ""){ //code commented @ 02 AUg 2018                                       
						if((strpos($scaOnlyCriteria, "DEV_IP") > 0 && ($key == "netAddress")) || ($val !="" && ($key == "netAddress"))){
									if(!filter_var($val, FILTER_VALIDATE_IP)){
											$backgroundColor = "background:#ac5f5d"; $errorMessage = "Host Name/IP Address should be valid IP Address"; $error = "Error";
											//$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
									}
									else if(in_array($val, $validateData["ipAddress"])){
										$backgroundColor = "background:#ac5f5d"; $errorMessage = "Host Name/IP Address already in use"; $error = "Error";
										//$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
									}
						}else if($key == "netAddress" && $val =="" && $postArray["port"] != ""){
							$backgroundColor = "background:#ac5f5d"; $errorMessage = "Must specify hostname/IP address if port is configured."; $error = "Error";
							//$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
						}
					
					if(($key == "port" && $val != "") && ($val < 1025 || $val > 65535)){
					    $backgroundColor = "background:#ac5f5d"; $errorMessage = "Port: Value must be between 1025 and 65535 "; $error = "Error";
                                            //$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
					}
                                        
					
					    //if($key == "macAddress" && $val != ""){ //Code Commented @ 02 Aug 2018 
                                            if((strpos($scaOnlyCriteria, "MAC") > 0 && ($key == "macAddress")) || ($val !="" && ($key == "macAddress"))){
						$macValueLenght = strlen($val);
						if($macValueLenght <> 12){

							$backgroundColor = "background:#ac5f5d"; $errorMessage = "Mac Address should be 12 digit"; $error = "Error";
						
						}
						else if(!ctype_xdigit($val)){
							$backgroundColor = "background:#ac5f5d"; $errorMessage = "Mac Address should be Hexadecimal"; $error = "Error";
						}
						else if(in_array($val, $validateData["macAddress"])){
						    $backgroundColor = "background:#ac5f5d"; $errorMessage = "Mac Address already in use"; $error = "Error";

						}
						
					}
                                        
					//if($key == "serialNumber" && $val != ""){ // Code commented @ 02 Aug 2018
                                        if((strpos($scaOnlyCriteria, "SN") > 0 && ($key == "serialNumber")) || ($val !="" && ($key == "serialNumber"))){
						$macValueLenght = strlen($val);

						
						if(!($macValueLenght >= 7 && $macValueLenght <= 8)){
							$backgroundColor = "background:#ac5f5d"; $errorMessage = "Serial Number should be 7 digit Number"; $error = "Error";
                                                        //$changeString .= '<tr><td class="errorTableRows" style=\"background:#ac5f5d;\">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
						}
					}
                                        
					
					if($key <> "scaUsers"){
						$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
					}
					if($key == "scaUsers"){  
                                                    
                                                    //Code added @ 17 July 2018
                                                    $grpServiceList = new Services();
                                                    //Jeetesh code for check group doesnot have more sca services pack ex-615

                                                    $grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
                                                    $servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
                                                    $servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray);
                                                    $scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);

                                                    $oneServicePack = "false";
                                                    $multipleServicePack = "false";
                                                    $noServicePack = "false";
                                                    $r = 0;
                                                    $errorFlag = 0;
                                                    
                                                    foreach($scaServiceArray as $ssa=>$ssv){
                                                            $pos = strpos($ssv->col[0], "Shared Call Appearance");
                                                            if($pos === false){

                                                            }else{
                                                                    if($ssv->count == 1){
                                                                            $oneServicePack = "true";
                                                                            $r++;
                                                                            //$countMore = "0";
                                                                    }else if($ssv->count > 1){
                                                                            $multipleServicePack = "true";
                                                                            //$countMore = "true";
                                                                    }else{
                                                                            $noServicePack = "true";
                                                                            //$countMore = "0";
                                                                    }
                                                            }

                                                    }

                                                    //code adds to check service pack cases

                                                    //code to compare the session and post array to check the assign and unassign value of sca user
                                                    $sharedCallAppearance = $grpServiceList->compareSessionScaUserForDeviceInvetry();
                                                    $postScaUsers = explode(";", $postArray["scaUsers"]);

                                                    array_pop($postScaUsers);
                                                    array_pop($postScaUsers);

                                                    $aa = 0;
                                                    //make the assign and unassign array for service pack assign to user in EX-615
                                                    $scaUserOperateArray = $grpServiceList->assignUnassignScaUser($sharedCallAppearance, $postScaUsers);
                                                    //print_r($scaUserOperateArray);        	

                                                if ($postArray["scaUsers"] !== "scaUsers;")
                                                {
                                                    //sca services check flag
                                                    $scaArray = array(
                                                        "Shared Call Appearance",
                                                        "Shared Call Appearance 10",
                                                        "Shared Call Appearance 15",
                                                        "Shared Call Appearance 20",
                                                        "Shared Call Appearance 25",
                                                        "Shared Call Appearance 30",
                                                        "Shared Call Appearance 35",
                                                        "Shared Call Appearance 5",
                                                    );
                                                    
                                                    //$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Unassign SCA USers</td><td class="errorTableRows">'.implode(",", $usersArray["unAssignUser"]).' '. $errorMessage.' </td></tr>';
                                                    $exp = explode(";", $postArray["scaUsers"]);

                                                    foreach ($exp as $k => $v)
                                                    { 
                                                        if ($v !== "scaUsers" and $v !== "")
                                                        { 
                                                            
                                                            $exp2 = explode(":", $v);
                                                            $uid = $exp2[0];
                                                            $name = $exp2[1];
                                                            $numberExtension = $exp2[2];
                                                            //echo "<br />Ext   - ".$numberExtension = $exp2[3];
                                                            $respServices = getUserAssignedUserServices($uid);

                                                            $scaServiceCountCheck = array_intersect($scaArray, $respServices);

                                                            //if(!in_array("Shared Call Appearance 35", $respServices)){
                                                            //if(count($scaServiceCountCheck) == 1){
                                                            if((empty($scaServiceCountCheck) && count($scaServiceCountCheck) == 0) && $oneServicePack == "false"){
                                                                $bg = INVALID;
                                                                $errorFlag = 1;
                                                                $error = "Error";
                                                                $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#ac5f5d;\">".$formDataArray[$key]."</td><td class=\"errorTableRows\" style='color:#ff0000'>" . $name . "  (" . $numberExtension . ")". " does not have any SCA User Service assigned.</td></tr>";
                                                    
                                                            }else{                                                                     
                                                                    if($oneServicePack == "true" && $r == "1"){ 
                                                                    $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#72ac5d;\">".$formDataArray[$key]."</td><td class=\"errorTableRows\">" . $name . "  (" . $numberExtension . ")". "</td></tr>";

                                                                    }
                                                                    else
                                                                    { 
                                                                           ////////////////////////////////////////////////////////////
                                                                            $nameStr = $name."(".$numberExtension.")";
                                                                            $nameStr = str_replace(" ", "", $nameStr);
                                                                            $userservicePackArray = array();
                                                                            $userServices = new userServices();
                                                                            $userSessionservicePackArray = $userServices->getUserServicesAll($uid, $_SESSION["sp"]);
                                                                            $servicesSessionArray = $grpServiceList->getServicePackServicesListUsers($userSessionservicePackArray["Success"]["ServicePack"]["assigned"]);
                                                                            $scaSessionServiceArray = $grpServiceList->checkScaUserServiceInServicepack($servicesSessionArray);
                                                                            foreach($scaSessionServiceArray as $ssa=>$ssv){
                                                                                    $pos = strpos($ssv->col[0], "Shared Call Appearance");
                                                                                    if($pos === false){
                                                                                    }else{
                                                                                            $servicePackToBeAssigned = $userSessionservicePackArray["Success"]["ServicePack"]["assigned"][$ssa];
                                                                                    }
                                                                            }
                                                                            if(!in_array($nameStr, $scaUserOperateArray["assignNameArray"])){ 
                                                                                    if(!empty($servicePackToBeAssigned)){
                                                                                            //$data         .= "<tr><td>" . $name . "  (" . $numberExtension . ")". "</td></tr>";
                                                                                            $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#72ac5d;\">".$formDataArray[$key]."</td><td class=\"errorTableRows\">" . $name . "  (" . $numberExtension . ")". "</td></tr>";
                                                                                    }else{                                                                                            
                                                                                            $error = "Error";
                                                                                            $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#ac5f5d;\">".$formDataArray[$key]."</td><td style='color:#ff0000'>Express cannot unambiguously assign Service Pack with SCA service to users(s) : ".$name." (" . $numberExtension . ")</td></tr>";
                                                                                            
                                                                                    }
                                                                            }
                                                                           ////////////////////////////////////////////////////////////
                                                                        
                                                                            
                                                                            /*$nameStr = $name."(".$numberExtension.")";
                                                                            $nameStr = str_replace(" ", "", $nameStr);

                                                                            if(!in_array($nameStr, $scaUserOperateArray["assignNameArray"])){
                                                                                    $errorFlag = 1;
                                                                                    $error = "Error";
                                                                                    
                                                                                    $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#ac5f5d;\">".$formDataArray[$key]."</td><td style='color:#ff0000'>Express cannot unambiguously assign Service Pack with SCA service to users(s) : ".$name." (" . $numberExtension . ")</td></tr>";
                                                                            }*/
                                                                    }
                                                            }
                                                            // $data .= "<tr><td>" . $name . "</td></tr>";
                                                        }
                                                    }                                                    
                                                    if($oneServicePack == "true" && $r == "1")
                                                    {  

                                                    }
                                                    else
                                                    {
                                                            if(count($scaUserOperateArray["unAssign"]) > 0){
                                                                    foreach($scaUserOperateArray["unAssign"] as $unAssignKey=>$unAssignVal){
                                                                            if(count($scaServiceArray) > 1){
                                                                                    $errorFlag = 1;
                                                                                    $error = "Error";                                                                                    
                                                                                    $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#ac5f5d;\">".$formDataArray[$key]."</td><td style='color:#ff0000'>Express cannot unambiguously unassign Service Pack with SCA service to users(s) : ".$unAssignVal."</td></tr>";
                                                                            }
                                                                    }
                                                            }
                                                    }
                                                   //$changeString .="</table></td></tr>"; //Code added @ 18 July 2018
                                                }
                            else if($postArray["scaUsers"] == "scaUsers;" && count($scaUserOperateArray["unAssign"]) > 0)
                                                {
                                                    if(count($scaUserOperateArray["unAssign"]) > 0)
                                                            {                                                                    
                                                                //Code added @ 30 Aug 2018
                                                                $scaArray = array(
                                                                "Shared Call Appearance",
                                                                "Shared Call Appearance 10",
                                                                "Shared Call Appearance 15",
                                                                "Shared Call Appearance 20",
                                                                "Shared Call Appearance 25",
                                                                "Shared Call Appearance 30",
                                                                "Shared Call Appearance 35",
                                                                "Shared Call Appearance 5",
                                                            );
                                                                foreach ($scaUserOperateArray["unAssign"] as $k => $tmpVal)
                                                                { 
                                                                    if ($tmpVal !== "scaUsers" and $tmpVal !== "")
                                                                    {                                                            
                                                                        $exp2 = explode(":", $tmpVal);
                                                                        $uid = $exp2[0];
                                                                        $name = $exp2[1];
                                                                        $numberExtension = $exp2[2];        
                                                                        $respServices = getUserAssignedUserServices($uid);

                                                                        $scaServiceCountCheck = array_intersect($scaArray, $respServices);   
																		
                                                                        if((empty($scaServiceCountCheck) && count($scaServiceCountCheck) == 0)){
                                                                            $bg = INVALID;
                                                                            $errorFlag = 1;
                                                                            $error = "Error";
                                                                            $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#D52B1E;\">".$formDataArray[$key]."</td><td style='color:#ff0000'>" . $name . "  (" . $numberExtension . ")". " does not have any SCA User Service assigned.</td></tr>";

                                                                        }else{    
                                                                                        $nameStr = $name."(".$numberExtension.")";
                                                                                        $nameStr = str_replace(" ", "", $nameStr);
                                                                                        $userservicePackArray = array();
                                                                                        $userServices = new userServices();
                                                                                        $userSessionservicePackArray = $userServices->getUserServicesAll($uid, $_SESSION["sp"]);
                                                                                        
                                                                                        
                                                                                        $userServicePackArr      = $userSessionservicePackArray["Success"]["ServicePack"]["assigned"];
                                                                                        $scaServicePackArr       = $userServices->getServicePacksListHaveScaServices($userServicePackArr);
                                                                                        $scaServicePackCustomArr = $userServices->getServicePacksHasOneScaServiceOnly($scaServicePackArr);
                                                                                                                                                                                
                                                                                        $serverPacksWithOneSCAService = "";
                                                                                        $servicePackSCACounter = 0;
                                                                                        foreach($scaServicePackCustomArr as $keyTmpN => $valTmpN)
                                                                                        {
                                                                                            if($valTmpN['hasOneSCAServiceOnly']=="true")
                                                                                            {
                                                                                                $servicePackSCACounter = $servicePackSCACounter + 1;
                                                                                            }
                                                                                        }                                                                                        
                                                                                        if($servicePackSCACounter>1)
                                                                                        {
                                                                                            $errorFlag = 1;
                                                                                            $error = "Error";
                                                                                            //$data          .= "<tr><td style='color:#ff0000'>Express cannot unambiguously unassign Service Pack with SCA service to users(s) : ".$unAssignVal."</td></tr>";
                                                                                            $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#D52B1E;\">".$formDataArray[$key]."</td><td style='color:#ff0000'>Express cannot unambiguously unassign Service Pack with SCA service to users(s) : ".$tmpVal."</td></tr>";
                                                                                        }
                                                                                        else
                                                                                        {                                                                                           
                                                                                             $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#00AC3E;\">Shared Call Appearances</td><td>None</td></tr>";
                                                                                        }
                                                                                
                                                                        }                                                           
                                                                    }
                                                                }
                                                                //End code
                                                                   
                                                            }
                                                }
                                                else
                                                {
                                                    //$data         .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Shared Call Appearances<td>None</td></tr>";
                                                    $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#72ac5d;\">Shared Call Appearances</td><td>None</td></tr>";
                                                }
                                                //End Code
                                                if($errorFlag ==0){
                                                    $val = str_replace("scaUsers;","", $val);
                                                    //$usersArray = $device->filterUsers($val); 
                                                    $usersArray = $device->newFilterUsers($val);
                                                    if(count($usersArray["assignUser"])> 0){
                                                            //$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Assign SCA USers</td><td class="errorTableRows">'.implode(",", $usersArray["assignUser"]).' '. $errorMessage.' </td></tr>';
                                                    }
                                                    if(count($usersArray["unAssignUser"])> 0){
                                                            //$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Unassign SCA USers</td><td class="errorTableRows">'.implode(",", $usersArray["unAssignUser"]).' '. $errorMessage.' </td></tr>';
                                                    }
                                                }
					}
                                        
                                        
                                        
				}
                                else
                                {                                    
                                        //if($key == "netAddress" && $val != ""){ //code commented @ 02 AUg 2018                                       
                                        if((strpos($scaOnlyCriteria, "DEV_IP") > 0 && ($key == "netAddress") && ($val == ""))){
                                                    
                                                    if(!filter_var($val, FILTER_VALIDATE_IP)){
                                                            $_SESSION['deviceUpdate'] = true;
                                                            $backgroundColor = "background:#ac5f5d"; $errorMessage = "Host Name/IP Address should be valid IP Address"; $error = "Error";
                                                            $changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
                                                    }
                                                    else if(in_array($val, $validateData["ipAddress"])){
                                                        $_SESSION['deviceUpdate'] = true;
                                                        $backgroundColor = "background:#ac5f5d"; $errorMessage = "Host Name/IP Address already in use"; $error = "Error";
                                                        $changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
                                                    }
                                        }else if($key == "netAddress" && $val =="" && $postArray["port"] != ""){
                                            $_SESSION['deviceUpdate'] = true;
                                            $backgroundColor = "background:#ac5f5d"; $errorMessage = "Must specify hostname/IP address if port is configured."; $error = "Error";
                                            $changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
                                        }
					
					if(($key == "port" && $val != "") && ($val < 1025 || $val > 65535)){
                                            $_SESSION['deviceUpdate'] = true;
					    $backgroundColor = "background:#ac5f5d"; $errorMessage = "Port: Value must be between 1025 and 65535 "; $error = "Error";
                                            $changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
					}
                                        //$changeString .= '<tr><td class="errorTableRows" style=\"background:#ac5f5d;\">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
                                }
			}
		}
	}
	if($_SESSION['deviceUpdate'] == false){
		$changeString = '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'">Device</td><td class="errorTableRows">No Changes </td></tr>';
	}
}else{
        
	$changeString = "";
        
	foreach($postArray as $key => $val){
		$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
		if (array_key_exists($key, $formDataArray)) {
			$val = trim($val);
			
			if((strpos($scaOnlyCriteria, "MAC") > 0 && ($key == "macAddress")) || ($val !="" && ($key == "macAddress"))){
			    $macValueLenght = strlen($val);
			    if($macValueLenght <> 12){
			        $backgroundColor = "background:#ac5f5d"; $errorMessage = "Mac Address should be 12 digit"; $error = "Error";
			    }
			    else if(!ctype_xdigit($val)){
			    	$backgroundColor = "background:#ac5f5d"; $errorMessage = "Mac Address should be Hexadecimal"; $error = "Error";
			    }else if(in_array($val, $validateData["macAddress"])){
			        $backgroundColor = "background:#ac5f5d"; $errorMessage = "Mac Address already in use"; $error = "Error";
			    }
			}
			if((strpos($scaOnlyCriteria, "DEV_IP") > 0 && ($key == "netAddress")) || ($val !="" && ($key == "netAddress"))){
				if(!filter_var($val, FILTER_VALIDATE_IP)){
					$backgroundColor = "background:#ac5f5d"; $errorMessage = "Host Name/IP Address should be valid IP Address"; $error = "Error";
				}else if(in_array($val, $validateData["ipAddress"])){
				    $backgroundColor = "background:#ac5f5d"; $errorMessage = "Host Name/IP Address already in use"; $error = "Error";
				}
			}else if($key == "netAddress" && $val =="" && $postArray["port"] != ""){
			        $backgroundColor = "background:#ac5f5d"; $errorMessage = "Must specify hostname/IP address if port is configured."; $error = "Error";
			}
                        
			if(($key == "port" && $val != "") && ($val < 1025 || $val > 65535)){
			    $backgroundColor = "background:#ac5f5d"; $errorMessage = "Port: Value must be >= 1025 and <= 65535"; $error = "Error";
			}
			
			if($key == "deviceType" && $val == ""){
			    $errorFlag = 1;
			    $error = "Error";
			    //$backgroundColor = "background:#D52B1E"; $errorMessage = "Identity/Device profile type can not be empty"; $error = "Error";
			    $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#ac5f5d;\">".$formDataArray[$key]."</td><td class='errorTableRows'>Identity/Device profile type can not be empty</td></tr>";
			}
			
			if(strpos($scaOnlyCriteria, "SN") > 0 && ($key == "serialNumber")){
			    $macValueLenght = strlen($val);

			    //if($macValueLenght <> 7){
                            if(!($macValueLenght >= 7 && $macValueLenght <= 8)){
			        $backgroundColor = "background:#ac5f5d"; $errorMessage = "Serial Number should be 7 digit Number"; $error = "Error";

			    }
			}
                        
                        if($key == "scaUsers" && $val !=""){  
                                                    
                                                    //Code added @ 17 July 2018
                                                    $grpServiceList = new Services();
                                                    //Jeetesh code for check group doesnot have more sca services pack ex-615

                                                    $grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
                                                    $servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
                                                    $servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray);
                                                    $scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);

                                                    $oneServicePack = "false";
                                                    $multipleServicePack = "false";
                                                    $noServicePack = "false";
                                                    $r = 0;
                                                    $errorFlag = 0;
                                                    
                                                    foreach($scaServiceArray as $ssa=>$ssv){
                                                            $pos = strpos($ssv->col[0], "Shared Call Appearance");
                                                            if($pos === false){

                                                            }else{
                                                                    if($ssv->count == 1){
                                                                            $oneServicePack = "true";
                                                                            $r++;
                                                                            //$countMore = "0";
                                                                    }else if($ssv->count > 1){
                                                                            $multipleServicePack = "true";
                                                                            //$countMore = "true";
                                                                    }else{
                                                                            $noServicePack = "true";
                                                                            //$countMore = "0";
                                                                    }
                                                            }

                                                    }

                                                    //code adds to check service pack cases

                                                    //code to compare the session and post array to check the assign and unassign value of sca user
                                                    $sharedCallAppearance = $grpServiceList->compareSessionScaUserForDeviceInvetry();
                                                    $postScaUsers = explode(";", $postArray["scaUsers"]);

                                                    array_pop($postScaUsers);
                                                    array_pop($postScaUsers);

                                                    $aa = 0;
                                                    //make the assign and unassign array for service pack assign to user in EX-615
                                                    $scaUserOperateArray = $grpServiceList->assignUnassignScaUser($sharedCallAppearance, $postScaUsers);
                                                    //print_r($scaUserOperateArray);        	

                                                if ($postArray["scaUsers"] !== "scaUsers;")
                                                {
                                                    //sca services check flag
                                                    $scaArray = array(
                                                        "Shared Call Appearance",
                                                        "Shared Call Appearance 10",
                                                        "Shared Call Appearance 15",
                                                        "Shared Call Appearance 20",
                                                        "Shared Call Appearance 25",
                                                        "Shared Call Appearance 30",
                                                        "Shared Call Appearance 35",
                                                        "Shared Call Appearance 5",
                                                    );
                                                    
                                                    //$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Unassign SCA USers</td><td class="errorTableRows">'.implode(",", $usersArray["unAssignUser"]).' '. $errorMessage.' </td></tr>';
                                                    $exp = explode(";", $postArray["scaUsers"]);

                                                    foreach ($exp as $k => $v)
                                                    { 
                                                        if ($v !== "scaUsers" and $v !== "")
                                                        { 
                                                            $exp2 = explode(":", $v);                                                            
                                                            $uid = $exp2[0];
                                                            $name = $exp2[1];
                                                            $numberExtension = $exp2[2];
                                                            //$numberExtension = $exp2[3];
                                                            $respServices = getUserAssignedUserServices($uid);

                                                            $scaServiceCountCheck = array_intersect($scaArray, $respServices);

                                                            //if(!in_array("Shared Call Appearance 35", $respServices)){
                                                            //if(count($scaServiceCountCheck) == 1){
                                                            if((empty($scaServiceCountCheck) && count($scaServiceCountCheck) == 0) && $oneServicePack == "false"){
                                                                $bg = INVALID;
                                                                $errorFlag = 1;
                                                                $error = "Error";
                                                                $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#ac5f5d;\">".$formDataArray[$key]."</td><td style='color:#ff0000'>" . $name . "  (" . $numberExtension . ")". " does not have any SCA User Service assigned.</td></tr>";
                                                    
                                                            }else{                                                                     
                                                                    if($oneServicePack == "true" && $r == "1"){ 
                                                                    $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#72ac5d;\">".$formDataArray[$key]."</td><td>" . $name . "  (" . $numberExtension . ")". "</td></tr>";

                                                                    }else{ 
                                                                            $nameStr = $name."(".$numberExtension.")";
                                                                            $nameStr = str_replace(" ", "", $nameStr);

                                                                            if(!in_array($nameStr, $scaUserOperateArray["assignNameArray"])){
                                                                                    $errorFlag = 1;
                                                                                    $error = "Error";
                                                                                    //$data .= "<tr><td style='color:#ff0000'>Express cannot unambiguously assign Service Pack with SCA service to users(s) : ".$name." (" . $numberExtension . ")</td></tr>";
                                                                                    $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#ac5f5d;\">".$formDataArray[$key]."</td><td style='color:#ff0000'>Express cannot unambiguously assign Service Pack with SCA service to users(s) : ".$name." (" . $numberExtension . ")</td></tr>";
                                                                            }
                                                                    }
                                                            }
                                                            // $data .= "<tr><td>" . $name . "</td></tr>";
                                                        }
                                                    }                                                    
                                                    if($oneServicePack == "true" && $r == "1")
                                                    {  

                                                    }
                                                    else
                                                    {
                                                            if(count($scaUserOperateArray["unAssign"]) > 0){
                                                                    foreach($scaUserOperateArray["unAssign"] as $unAssignKey=>$unAssignVal){
                                                                            if(count($scaServiceArray) > 1){
                                                                                    $errorFlag = 1;
                                                                                    $error = "Error";                                                                                    
                                                                                    $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#ac5f5d;\">".$formDataArray[$key]."</td><td style='color:#ff0000'>Express cannot unambiguously unassign Service Pack with SCA service to users(s) : ".$unAssignVal."</td></tr>";
                                                                            }
                                                                    }
                                                            }
                                                    }
                                                   //$changeString .="</table></td></tr>"; //Code added @ 18 July 2018
                                                }
                                                else if($postArray["scaUsers"] == "scaUsers;" && count($scaUserOperateArray["unAssign"]) > 0)
                                                {
                                                    if($oneServicePack == "true" && $r >= 1){
                                                            //$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $backgroundColor . ";\">Shared Call Appearances<td>None</td></tr>";
                                                            $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#72ac5d;\">Shared Call Appearances</td><td>None</td></tr>";
                                                    }else{
                                                            if(count($scaUserOperateArray["unAssign"]) > 0)
                                                            {
                                                                    //$data         .= "<tr><td class=\"errorTableRows\" style=\"background:" . $backgroundColor . ";\">" . $_SESSION["userModNames"][$key] . "</td><td class=\"errorTableRows\"><table class=\"dialogClass\">";
                                                                    //$changeString .= "<tr><td class=\"errorTableRows\" style=\"background:" . $backgroundColor . ";\">" . $_SESSION["userModNames"][$key] . "</td><td class=\"errorTableRows\"><table class=\"dialogClass\">";
                                                                    foreach($scaUserOperateArray["unAssign"] as $unAssignKey=>$unAssignVal){
                                                                            if(count($scaServiceArray) > 1){
                                                                                    $errorFlag = 1;
                                                                                    $error = "Error";                                                                                    
                                                                                    $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#ac5f5d;\">".$formDataArray[$key]."</td><td style='color:#ff0000'>Express cannot unambiguously unassign Service Pack with SCA service to users(s) : ".$unAssignVal."</td></tr>";
                                                                            }
                                                                    }
                                                                   //$changeString .="</table></td></tr>"; //Code added @ 18 July 2018 
                                                            }
                                                    }
                                                }
                                                else
                                                {
                                                    //$data         .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Shared Call Appearances<td>None</td></tr>";
                                                    $changeString .= "<tr><td class=\"errorTableRows\" style=\"background:#72ac5d;\">Shared Call Appearances</td><td>None</td></tr>";
                                                }
                                                //End Code
                                                if($errorFlag ==0){
                                                    $val = str_replace("scaUsers;","", $val);
                                                    $usersArray = $device->filterUsers($val); 
                                                    if(count($usersArray["assignUser"])> 0){
                                                            //$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Assign SCA USers</td><td class="errorTableRows">'.implode(",", $usersArray["assignUser"]).' '. $errorMessage.' </td></tr>';
                                                    }
                                                    if(count($usersArray["unAssignUser"])> 0){
                                                            //$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Unassign SCA USers</td><td class="errorTableRows">'.implode(",", $usersArray["unAssignUser"]).' '. $errorMessage.' </td></tr>';
                                                    }
                                                }
					}
                        
			
			
			
                        
                        //Code added @ 20 July 2018
                        //End code  
                        if($key != "scaUsers")
                        {                            
                            $changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
                        }
		}
	}
}

//newcode ends
echo $error.$changeString;
?>