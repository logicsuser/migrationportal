<?php
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once("/var/www/html/Express/adminCheckFilter.php");

//Catch Modify User Request
if (isset($_REQUEST['modify_user']) && $_REQUEST['modify_user']) {
    $_SESSION['goto_modify_user'] = $_REQUEST['modify_user'];
}

redirectUser("main");
$_SESSION["redirectUrl"] = "main";

// for call pickUp
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");

$groupId = $_SESSION['groupId'];
$grpServiceList = new Services();
$grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($groupId);

function sortGroupServices($a,$b)
{
    return ($a[0]<= $b[0]) ? -1 : 1;
}
usort($grpServiceListResponse["Success"]["groupServicesAuthorizationTable"], "sortGroupServices");
$_SESSION["groupServicesAuthorizationTable"] = "";
foreach($grpServiceListResponse["Success"]["groupServicesAuthorizationTable"] as $gsauthKey => $gsauthVal){
    if($gsauthVal[0] == "Call Pickup" && $gsauthVal[2] == "true"){
        $_SESSION["groupServicesAuthorizationTable"] = "1";
    }
}

/*@ 30-05-2019
 * servicepack sca
 * servicepack voice messaging
 */
function determineServicePacksAvailable() {
    global $sessionid, $client;

    // Create list of Service Packs containing 'Voice Messaging User' service
    require_once("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");
    unset($_SESSION["vmServicePacks"]);        
    $scaServicePackArray = array();
    $i = 0;
    foreach ($servicePacks as $servicePack) {
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]). "</serviceProviderId>";
        $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        foreach ($xml->command->userServiceTable->row as $key => $value){
            if (strval($value->col[0]) == "Voice Messaging User") {
                $_SESSION["vmServicePacks"][$i++] = $servicePack;
            }
            $pos = strpos(strval($value->col[0]), "Shared Call Appearance");
            if($pos === false)
            {
                //continue;
            }
            else
            {                        
                $scaServicePackArray[$i++] = $servicePack;
            }

        }
    }
    if (!empty($_SESSION["vmServicePacks"])) {
        $_SESSION["vmServicePacks"] = array_values($_SESSION["vmServicePacks"]);
        $_SESSION["vmServicePacksFlat"] = join(",", $_SESSION["vmServicePacks"]);
    }

    $scaServicePackArray = array_unique($scaServicePackArray);
    $scaServicePackArray = array_values($scaServicePackArray);
    $_SESSION["scaServicePacksArr"] = $scaServicePackArray;
    $_SESSION["scaServicePacksStr"] = join(",", $scaServicePackArray);
    //End Code
       
}   
determineServicePacksAvailable();
require_once("header.php");
 
?>

 
<script>

var previousActiveMenu = "";
var currentClickedDiv = "";
$(document).ready(function () {
	var showBtnVdmVideo  = '<?php echo $license["vdmLite"]; ?>';
	 if(showBtnVdmVideo !="true"){
		 $("#modVdm").prop('disabled', true);
		 $('#modVdm').attr('disabled','disabled');
	 }
});

var module ='' ;
var redirectNoChanges = '' ;
var isCheckDataModify = false;
var checkResult = function(status) { // this function used for user modify or group if any changes and switch to different module.
	isCheckDataModify = status;
}
var previousModPos = '';
var getPreviousModuleId = function(previousMod){  //this function used for check previous module id 
	previousModPos = previousMod ;
}

var checkUserToVdmPos ='' ;
var getuserModToVdm = function(checkUserToVdm){
	checkUserToVdmPos = checkUserToVdm ;
}
var ajaxData ='';
var dataToSendSerial = "";
var urlData = "";
var vdmDeviceName ='';
var vdmDeviceType ='';
var vdmType ='';
var headerNavigation ="";
var getDeviceManageMentResultType = function(vdmDeviceName,vdmDeviceType,vdmType){
	vdmDeviceName 	=vdmDeviceName ;
	vdmDeviceType 	=vdmDeviceType ;
	vdmType 		=vdmType ;
}
    $(function() {
    
    	// *Start* Disable Browser's Back Button.
    	var mainPageRefreshed = 0;
        var href = location.href;
    	var urlLastSegment = href.match(/([^\/]*)\/*$/)[1];
    	
        history.pushState(null, null, urlLastSegment);
        window.addEventListener('popstate', function () {
            history.pushState(null, null, urlLastSegment);
        });
        // *End*
        
        $("#changeGroup").click(function()
        {
            window.location.replace("chooseGroup.php");
        });

        var sp = "<?php echo $_SESSION["sp"]; ?>";
        var group = "<?php echo $_SESSION["groupId"]; ?>";

        if (sp === "" || group === "") {
            window.location.replace("chooseGroup.php");
        }
     // function used for redirect url on header
		var headerRedirectLink = function(module){
		  if(module == "main_page_link"){ window.location.href = 'main.php'; }
			else if(module == "enterprise_main_page_link"){ window.location.href = 'main_enterprise.php'; }
			else if(module == "logout"){ window.location.href = '/Express/index.php'; }
			//else if(module == "logoutDropD"){ window.location.href = '/Express/index.php'; }
			else if(module == "redirectHeaderNavResetPass"){ window.location.href = '/Express/reset_password.php'; }
			else if(module =="chooseGroupModal"){ $.ajax({url: "changeGroupDialog/index.php", success: function(result){
               $("#contentBody").html(result);
               $("#helpUrl").show();
               $("#chooseGroupDialogModal").modal('show'); 
    	}}); }
			else if(module =="logoImage"){ window.location.href = 'main.php'; }
	 	
		}
		
                function getAssignNumbersList(dataToSend) {
	var assignNumber = "";
    var tempArr = [];
    var dataToSendTemp = $("form#groupModify").serializeArray();
    dataToSendTemp.forEach(function(value,i) {
        if(value.name == "assignNumbers[]") {
        	tempArr.push(i);
			assignNumber += value.value + ";;";
        }
    });
    dataToSend.push({name:'assignNumbers', value: assignNumber});
    return dataToSend;
}
	//function used for redirect url on module	
        var moduleFunctionRedirect = function(module) {
        	 $("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
             $("#loading").show();
            $(".navMenu").removeClass("activeNav");
           // var module = $(this).attr("id");
            $(this).addClass("activeNav");
        	if(mainPageRefreshed == 0) {
        		activeSideNavBar(module);
        	}
            var userType = '';
        	<?php if($_SESSION['superUser'] == 1){?>
            userType = 'superUser';
        	<?php }else if($_SESSION['superUser'] == 3){?>
                    userType = 'superUser';
                <?php }else{?>
            userType = '<?php echo $_SESSION['adminType']?>';
        	<?php }?>
        	 activeSideNavBar(module);
        	 var ajaxFormData ='' ;
        	 if(checkUserToVdmPos =='navToVDMLightModule' && redirectNoChanges == 'ChangesUserMod'){
			previousModPos = '';
			vdmDeviceName 	=	vdmDeviceName ;
        	vdmDeviceType 	=	vdmDeviceType ;
        	vdmType 		=	vdmType ;
            ajaxFormData =  { module: "modVdm",deviceName: vdmDeviceName,vdmDeviceType: vdmDeviceType,vdmType: vdmType};
			}else{
				 var ajaxFormData = { module: module } ;
			}
			 $.ajax({
                type: "POST",
                url: "navigate.php",
                data: ajaxFormData,
                success: function(result)
                {
//                 	if(foundServerConErrorOnProcess(result, "")) {
//     					return false;
//                  }
                	mainPageRefreshed = 1;
                    $("#loading").hide();
                   if(checkUserToVdmPos =='navToVDMLightModule' && redirectNoChanges == 'ChangesUserMod'){

                    	checkUserToVdmPos = '';
                    	checkUserToVdmPos = '';
                    	var deviceForVdm = $("#deviceTypeDigital").val();
                    	$("#searchOption").html("");
    					$("#userData").html("");
    					$(".searchBodyForm").hide();
    					$(".mainBannerModify").hide();
    					$("#usersBanner").hide();
    					$("#modUserBody .vertSpacer").hide();
    	 				$(".subBanner").html("Device Management");
    					$("#loading").hide();
    					$('#helpUrl').attr('data-module', module);
    					$('#helpUrl').attr('data-adminType', userType);
    					$("#mainBody").html(result);
    					$("#selectedDeviceType").val(deviceForVdm);
    					$(window).scrollTop(0);
				}
                 else{
                    	$('#helpUrl').attr('data-module', module);
                        $('#helpUrl').attr('data-adminType', userType);
                        $("#mainBody").html(result);
                    }
                }
            });
		} //end navigate redirection method

		 $(document).on('click', '.navToVDMLightModule,.navigate,.RedirectNavHeader', function () {
			 module = $(this).attr("id");
			 headerNavigation = this.className;
			if($(this).hasClass('navToVDMLightModule')){
				previousModPos ="navToVDMLightModule";
				redirectNoChanges ='ChangesUserMod';
				checkResult(true) ;
			 }else{
				 redirectNoChanges ='NoChangesVerify';
			 }
			
			  if(isCheckDataModify ==true){
 					if(module != 'userMod' || module !='groupModify' || module == 'userMod' || module =='groupModify'){
 							if(previousModPos =="groupModify"){
							$("#groupDetails").show();
  							var selectGroupId = $("#selectGroup").val();
  							var groupArray = selectGroupId.split(" ");
  					        var selectGroupId = groupArray[0];
  					    	var groupId = $("#groupId").val();
  					    	var timeZone = $("#timeZone").val();
  					    	var userLimit = $("#userLimit").val();
  					    	var minExtensionLength = $("#minExtensionLength").val();
  					    	var maxExtensionLength = $("#maxExtensionLength").val();
  					    	var defaultExtensionLength = $("#defaultExtensionLength").val();
  					    	$("#activateNewAssigned").removeAttr("disabled");
  					    	var modifyGroupId = $("#modifyGroupId").val();
  					    	var spanGroupId = $("#spanGroupId").text();  
  					    	 
  					    	//if($("#assignNumbers option").length > 0 && $('#assignNumbers option:selected').length == 0){
  					    	//$('#assignNumbers option').prop('selected', true); //Code commented @ 13 March 2019 regarding EX-1093
  					    		//}
  					    		if($("#assignedNCS option").length > 0 && $('#assignedNCS option:selected').length == 0){
  					    			$('#assignedNCS option').prop('selected', true);
  					    		}
  					    		if($("#assignedDefault option").length > 0 && $('#assignedDefault option:selected').length == 0){
  					    			$('#assignedDefault option').prop('selected', true);
  					    		}
  					    		if($("#assignedNCS option").length > 0 || $('#assignedNCS option:selected').length == 0){
  					    			$('#assignedNCS option').prop('selected', true);
  					    		}
 

                                                        var dataToSendSerial = $("form#groupModify :input[name != 'assignNumbers[]']").serializeArray();
                                                        //dataToSendSerial = getAssignNumbersList(dataToSendSerial);
  						  //  dataToSendSerial =  $("form#groupModify").serializeArray();
  							urlData = "groupManage/groupModify/validate.php";
  							ajaxData = {compare : 1, formdata : dataToSendSerial, prevmodule: 'preventChanges'};



  						//    dataToSendSerial =  $("form#groupModify").serializeArray();
  						//	urlData = "groupManage/groupModify/validate.php";
  						//	ajaxData = {compare : 1, formdata : dataToSendSerial} ;
  						}

  						else if(previousModPos =="userMod" || previousModPos =="navToVDMLightModule"){
							dataToSendSerial =  $("form#userMod").serializeArray();
  							urlData 		 =  "userMod/checkData.php";
  							ajaxData 		 =  dataToSendSerial;
  						    
  						}
  						else{}
  						$.ajax({
		        		type: "POST",
		        		url: urlData,
		        		data: ajaxData,
		        		success: function(result) {
 							var errorExist = result.search("#FB6071");
 							var noChangesExist = "";
 							var messageDialog = '';
 							var chkPreviousMoudle ='';
							if(previousModPos =="userMod" || previousModPos =="navToVDMLightModule"){

								noChangesExist = result.search("You have made no changes.");
								messageDialog = "Modify User";
 								}else if(previousModPos =="groupModify"){

								noChangesExist = result.search("No Changes");
								messageDialog = "Modify Group";
 							}
							if(noChangesExist =="No Changes" || noChangesExist =="You have made no changes." || noChangesExist >0){
 								  checkResult(false) ;
								  if(previousModPos =="navToVDMLightModule"){
									 getuserModToVdm("navToVDMLightModule") ;
									 getDeviceManageMentResultType(vdmDeviceName,vdmDeviceType,vdmType);
                                     moduleFunctionRedirect(module);
								  }
	    	            	else if(headerNavigation== 'RedirectNavHeader'){
								headerRedirectLink(module);
							}
						else{
								moduleFunctionRedirect(module);
						}
			}else{
					checkResult(true) ;
					$("#checkDataChangesModulePrevent").html('Are you sure you want to leave? <br/> You might lose any change you have made for this '+messageDialog+'.');
					$("#checkDataChangesModulePrevent").dialog('open');
					return false ;
    		        		}
		    			 }
		        	});
				}  // end check return status data check
				else{
				//	getuserModToVdm("navToVDMLightModule") ;
					moduleFunctionRedirect(module);
				}
			}
			if(isCheckDataModify ==false){
				 
				if(headerNavigation== 'RedirectNavHeader'){
					 headerRedirectLink(module);
				}
				else{
					moduleFunctionRedirect(module);
				}
	
  			}
 		});
        
		//dialog box code 
		  $("#checkDataChangesModulePrevent").dialog({
		  	autoOpen: false,
		  	width: 600,
		  	modal: true,
		  	position: { my: "top", at: "top" },
		  	resizable: false,
		  	closeOnEscape: false,
		  	buttons: {
		  		"Yes": function() {
		  			checkResult(false);
		  			 if(previousModPos =="navToVDMLightModule"){
						 getuserModToVdm("navToVDMLightModule") ;
						 getDeviceManageMentResultType(vdmDeviceName,vdmDeviceType,vdmType);
						 moduleFunctionRedirect(module);
					  }
		  		 	else if(headerNavigation== 'RedirectNavHeader'){
								headerRedirectLink(module);
					}
					else{
						moduleFunctionRedirect(module);
					}
		  			$(this).dialog("close");
				},
		  		"No": function() {
		  			checkResult(true);
		  			$(this).dialog("close");
		  			return false; 
		  		} 
		  	},  
		      open: function() {
			$("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019	  
                        setDialogDayNightMode($(this));
		      	$(".ui-dialog-buttonpane button:contains('Yes')").button().show().addClass('subButton');
		          $(".ui-dialog-buttonpane button:contains('No')").button().show().addClass('cancelButton');
		      }
		  });
		/// 
		
		var activeSideNavBar = function(module)
		{
		 var clickedClass = $("." + module + "_SideMenu");
		 var $this = clickedClass.find('.ImgHoverIcon');
		 clickedClass.addClass("activeNav");
		}
        
        var pageGetHelp = function(module){
            var userType = '';
			<?php if($_SESSION['superUser'] == 1){?>
            userType = 'superUser';
                        <?php }else if($_SESSION['superUser'] == 3){ ?>
            userType = 'superUser';
            <?php }else{?>
            userType = '<?php echo $_SESSION['adminType']?>';
			<?php }?>
            $.ajax({
                type: "POST",
                url: "help.php",
                data: { userType: userType, modId : module},
                success: function(result)
                {
                    if(result == ""){
                        $("#helpUrl").removeAttr('href');
                        return false;
                    }
                    var obj = jQuery.parseJSON(result);
                    if(obj != "error"){
                        $("#helpUrl").attr('href', '../../../helpUrl/'+obj);
                        $("#helpUrl").attr('target', '_blank');
                    }

                }
            });
        };
        //pageGetHelp('main');
    });
   
    if ($.fn.button.noConflict) {
    	var bootstrapButton = $.fn.button.noConflict();
    	$.fn.bootstrapBtn = bootstrapButton;
    	}		 
</script>
 
<style>
.disabled {
    pointer-events:none; 
    opacity:0.6;    
}
</style>

<div style="text-align:center;" id="mainBody" class="header_scroll">
	
	<?php 
        //echo "<br />Super User - ".$_SESSION["superUser"];
        //print_r($_SESSION["permissions"]);
	if($userModuleExist = $_SESSION["permissions"]["viewusers"] == "1" || $_SESSION["permissions"]["addusers"] == "1" || $_SESSION["permissions"]["modifyUsers"] == "1" || $_SESSION["permissions"]["groupWideUserModify"] == "1" || $_SESSION["superUser"] == "1" || $_SESSION["permissions"]["expressSheets"] == "1"){?>
		<h2 style="" class="usersText">Users</h2>
	<?php }?>

	<div style="" class="icons-div userLst">
		<ul style="" class="feature-list">
			<?php if ($_SESSION["permissions"]["viewusers"] == "1") { ?><li><a href="#" class="navMenu navigate" id="users"><img class="rolloverImg" src="images/NewIcon/icon_viewmodifyuser.png" data-alt-src="images/NewIcon/icon_viewmodifyuser_rollover.png" /><br />Users</a></li><?php } ?>
			<?php if ($_SESSION["permissions"]["addusers"] == "1") { ?><li><a href="#" class="navMenu navigate userAddText" id="userAdd"><img class="rolloverImg" src="images/NewIcon/icon_adduser.png" data-alt-src="images/NewIcon/icon_adduser_rollover.png" /><br />Add User</a></li><?php } ?>
			<?php if ($_SESSION["permissions"]["modifyUsers"] == "1") { ?><li><a href="#" class="navMenu navigate userModText" id="userMod"><img class="rolloverImg" src="images/NewIcon/modify_user.png" data-alt-src="images/NewIcon/modify_user_rollover.png"/><br/>Modify User</a></li><?php } ?>
			<?php if ($_SESSION["superUser"] == "1" || $_SESSION["permissions"]["groupWideUserModify"] == "1") { ?><li style="display:none !important;"><a href="#" class="navMenu navigate" id="bulk"><img class="rolloverImg" src="images/NewIcon/icon_addmodifybulkuser.png" data-alt-src="images/NewIcon/icon_addmodifybulkuser_over.png" /><br />Bulk Modify</a></li><?php } ?>
			<?php if ($_SESSION["permissions"]["adminUsers"] == "1" and $_SESSION["superUser"] == "0") { ?><li><a href="#" class="navMenu navigate" id="adminUser"><img class="rolloverImg" src="images/NewIcon/administrator.png" data-alt-src="images/NewIcon/administrator_rollover.png" /><br />Administrators</a></li><?php } ?>
			<?php if ($_SESSION["superUser"] == "1" || $_SESSION["permissions"]["expressSheets"] == "1") { ?><li><a href="#" class="navMenu navigate" id="expressSheets"><img class="rolloverImg" src="images/NewIcon/express-sheets.png" data-alt-src="images/NewIcon/express-sheets_rollover.png" /><br />Express Sheets</a></li><?php } ?>
		</ul>
	</div> 
	

	<?php 

	if($groupSerModuleExist = ($_SESSION["superUser"] == "1")  || ($_SESSION["permissions"]["modifyGroup"] == "1") || ($_SESSION["permissions"]["groupBasicInfo"] == "1") || 
	    ($_SESSION["permissions"]["groupDomains"] == "1") || ($_SESSION["permissions"]["groupServices"] == "1") || 
	    ($_SESSION["permissions"]["groupDN"] == "1") || ($_SESSION["permissions"]["groupPolicies"] == "1") || 
	    ($_SESSION["permissions"]["groupNCOS"] == "1") || ($_SESSION["permissions"]["groupVoicePortal"] == "1") && isset($_SESSION["permissions"]["modifyGroup"]) && $_SESSION["permissions"]["modifyGroup"] == "1" || 
	    $_SESSION["permissions"]["aamod"] == "1" || $_SESSION["permissions"]["modhuntgroup"] == "1" || $_SESSION["permissions"]["modcallCenter"] == "1" ||
	    $_SESSION["permissions"]["modschedule"] == "1" || $cpgFieldVisible == "true" && (isset($_SESSION["permissions"]["callPickupGroup"]) && $_SESSION["permissions"]["callPickupGroup"]== "1" && $_SESSION["groupServicesAuthorizationTable"] == "1")){ ?>
			<h2 style="" class="macdText">Group/Services</h2>
	<?php } ?>
	<div style="" class="icons-div macdLst">
		<ul style="" class="feature-list">
			
			<?php if ( ($_SESSION["superUser"] == "1")  || ($_SESSION["permissions"]["modifyGroup"] == "1")  || ($_SESSION["permissions"]["groupBasicInfo"] == "1") || ($_SESSION["permissions"]["groupDomains"] == "1") || ($_SESSION["permissions"]["groupServices"] == "1") || ($_SESSION["permissions"]["groupDN"] == "1") || ($_SESSION["permissions"]["groupPolicies"] == "1") || ($_SESSION["permissions"]["groupNCOS"] == "1") || ($_SESSION["permissions"]["groupVoicePortal"] == "1") && isset($_SESSION["permissions"]["modifyGroup"]) && $_SESSION["permissions"]["modifyGroup"] == "1"){?>
			<li><a href="#" class="navMenu navigate" id="groupModify"><img class="rolloverImg" src="images/NewIcon/modfy_group.png" data-alt-src="images/NewIcon/modfy_group_rollover.png" /><br />Modify Group</a></li>
			<?php }?>
			<?php if ($_SESSION["permissions"]["aamod"] == "1") { ?><li><a href="#" class="navMenu navigate" id="modAA"><img class="rolloverImg" src="images/NewIcon/modify_auto_attendant.png" data-alt-src="images/NewIcon/modify_auto_attendant_rollover.png" /><br />Auto Attendants</a></li><?php } ?>
			<?php if ($_SESSION["permissions"]["modhuntgroup"] == "1") { ?><li><a href="#" class="navMenu navigate" id="modHunt"><img class="rolloverImg" src="images/NewIcon/modify_hunt_group.png" data-alt-src="images/NewIcon/modify_hunt_group_rollover.png" /><br />Hunt Groups</a></li><?php } ?>
			<?php if ($_SESSION["permissions"]["modcallCenter"] == "1") { ?><li><a href="#" class="navMenu navigate" id="modCallCenter"><img class="rolloverImg" src="images/NewIcon/modify_call_center.png" data-alt-src="images/NewIcon/modify_call_center_rollover.png" /><br />Call Centers</a></li><?php } ?>
			<?php if ($_SESSION["permissions"]["modschedule"] == "1") { ?><li><a href="#" class="navMenu navigate" id="modSched"><img class="rolloverImg" src="images/NewIcon/schedule.png" data-alt-src="images/NewIcon/schedule_rollover.png" /><br />Schedules</a></li><?php } ?>
			<?php if ($cpgFieldVisible == "true" && (isset($_SESSION["permissions"]["callPickupGroup"]) && $_SESSION["permissions"]["callPickupGroup"]== "1" && $_SESSION["groupServicesAuthorizationTable"] == "1")) { ?>
			<li><a href="#" class="navMenu navigate" id="modGrpCallPickup"><img  class="rolloverImg" src="images/NewIcon/pick_up_groups.png" data-alt-src="images/NewIcon/pick_up_groups_rollover.png" /><br />Call Pickup Groups</a></li><?php }?>
		</ul>
	</div>
                        
	<?php
        //if($_SESSION["permissions"]["modifyUsers"] == "1" || $_SESSION["permissions"]["viewnums"] == "1" || $_SESSION["permissions"]["viewChangeLog"] == "1" || $_SESSION["permissions"]["viewcdrs"] == "1" || $_SESSION["permissions"]["vdmLight"] == "1"){ 
        if($invernotryPermissionLevel){ 
        ?>
            <h2 style="" class="inventoryText">Inventory</h2>
	<?php }	?>
	<div style="" class="icons-div inventorylst">
		<ul style="" class="feature-list">
			<?php if (true) { ?><!--li><a href="#" class="navMenu navigate" id="deviceMgmt" style="display: none"><img class="rolloverImg" src="images/NewIcon/device_management.png" data-alt-src="images/NewIcon/device_management_rollover.png" /><br />Device Management</a></li--><?php } ?>
			<?php if ($_SESSION["permissions"]["modifyUsers"] == "1") { ?> <!--  <li><a href="#" class="navMenu navigate" id="findMac"><img class="rolloverImg" src="images/NewIcon/find_mac_address.png" data-alt-src="images/NewIcon/find_mac_address_rollover.png" /><br />Find MAC Addresses</a></li>  --> <?php } ?>
			<?php if ($_SESSION["permissions"]["viewnums"] == "1") { ?><li><a href="#" class="navMenu navigate" id="dids"><img class="rolloverImg" src="images/NewIcon/find_numbers.png" data-alt-src="images/NewIcon/find_numbers_rollover.png" /><br />View Numbers</a></li><?php } ?>
                        <?php                            
				if (isset($_SESSION["permissions"]["deviceInventory"]) and $_SESSION["permissions"]["deviceInventory"] == "1")
				{
					?>
					<li><a href="#" class="navMenu navigate" id="modDevice"><img class="rolloverImg" src="images/NewIcon/device_inventory.png" data-alt-src="images/NewIcon/device_inventory_rollover.png" /><br />Device Inventory</a></li>
					<?php 
				}
			?>
			
			<?php if ($_SESSION["permissions"]["announcements"] == "1") { ?><li><a href="#" class="navMenu navigate" id="announcement"><img class="rolloverImg" src="images/NewIcon/announcement.png" data-alt-src="images/NewIcon/announcement_rollover.png" /><br/>Announcements</a></li><?php } ?>
			
			<?php if ($_SESSION["permissions"]["viewChangeLog"] == "1") { ?><li><a href="#" class="navMenu navigate" id="changeLog"><img class="rolloverImg" src="images/NewIcon/view_change_log.png" data-alt-src="images/NewIcon/view_change_log_rollover.png" /><br />Change Logs</a></li><?php } ?>
			
			<?php if ($_SESSION["permissions"]["viewcdrs"] == "1" and ! is_null($billDB)) { ?><li><a href="#" class="navMenu navigate" id="cdrs"><img class="rolloverImg" src="images/NewIcon/call_records.png" data-alt-src="images/NewIcon/call_records_rollover.png" /><br />CDRs</a></li><?php } ?>
			
			
			<?php if(isset($license["vdmLite"]) && $license["vdmLite"] =="true"){
			    
			    if (isset($_SESSION["permissions"]["vdmLight"]) and $_SESSION["permissions"]["vdmLight"] == "1"){?>
			    <li><a href="#" class="navMenu navigate" id="modVdm"><img  class="rolloverImg" src="images/NewIcon/vdv_lite.png" data-alt-src="images/NewIcon/vdv_lite_rollover.png" /><br />Device Management</a></li>
			    
			     <?php } }
			     else{
			        echo'<li class="disabled" style="display:none"><a href="#" class="navMenu navigate" id="modVdm"><img  class="rolloverImg" src="images/NewIcon/vdv_lite.png" data-alt-src="images/NewIcon/vdv_lite_rollover.png" disbled/><br />Device Management</a></li>';
			         
			     }
			     ?>
			
			<?php if (($_SESSION["permissions"]["sasTest"] == "1") or ($_SESSION["permissions"]["siteReports"] == "1")) { ?><li><a href="#" class="navMenu navigate" id="siteManagement"><img class="rolloverImg" src="images/NewIcon/site_management.png" data-alt-src="images/NewIcon/site_management_rollover.png" /><br />Site Management</a></li><?php } ?>			
 
			
		</ul>
	</div>
</div>
<div id='checkDataChangesModulePrevent'></div>
<?php
if (isset($_SESSION['goto_modify_user']) && $_SESSION['goto_modify_user']) {
	?>
	<script type="application/javascript">
        $().ready(function () {
            var userId = '<?php echo $_SESSION['goto_modify_user'] ?>';
            $.ajax({
                type: "POST",
                url: "userMod/userMod.php",
                success: function (result) {
                    $("#mainBody").html(result);
                    $("#searchVal").val(userId);

                    setTimeout(function () {
                        $("#go").trigger("click");
                    }, 2000);

                    $('#helpUrl').attr('data-module', "userMod");

                    $("#userMod").addClass("activeNav");
                    var $thisPrev = $("#userMod").find('.ImgHoverIcon');
                    var newSource = $thisPrev.data('alt-src');
                    $thisPrev.data('alt-src', $thisPrev.attr('src'));
                    $thisPrev.attr('src', newSource);
                }

            });
        });
	</script>
	<?php
	unset($_SESSION['goto_modify_user']);
}
?>
