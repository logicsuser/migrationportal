<?php
/**
 * Created by Dipin Krishna.
 * Date: 11/16/18
 * Time: 5:55 PM
 */
?>
<?php
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once("/var/www/html/Express/usersEnterprise/GetUsersInSystem.php");
$gUISystem = new GetUsersInSystem();
//---------------------------------------------------------------------------
// Build Service Provider Drop down options
$serviceProviders = $gUISystem->getUsersServerProviderList();
function getUsersServerProviderList() {
	global $serviceProviders;

	$str = "";
	if (count($serviceProviders) > 0) {
		$str = "<option value=\"\">None</option>";
		foreach ($serviceProviders as $providerId => $spName) {
			$str .= "<option value=\"" . $providerId . "\">" . "$spName" . "</option>";
		}
	}
	return $str;
}

//---------------------------------------------------------------------------

?>
<link rel="stylesheet" type="text/css" href="/Express/js/jquery.dataTables.min.css" media="screen">
<style>
	.filterMainHeadDiv {
		width: 60%;
		margin-left: 20%;
		float: left;
		text-align: center;
	}
	.filterApplyHead {
		float: left;
		color: #BD8383;
		font-weight: bold;
		font-size: 12px;
		vertical-align: middle;
		width: 15%;
		padding-top: 6px;
		text-align: right;
	}
	.filterApplyHead img {
		width: 25px;
		padding-right: 5px;
	}
        
        a.paginate_button {
            background-color: #5d81ac !important;
            color: #fff !important;
        }
        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
            color: white !important;
            border: 1px solid #FFB200 !important;
            background-color: #FFB200 !important;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #FFB200), color-stop(100%, none)) !important;
            background: -webkit-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: -moz-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: -ms-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: -o-linear-gradient(top, #FFB200 0%, #FFB200 100%) !important;
            background: linear-gradient(to bottom, #FFB200 0%, #FFB200 100%) !important;
        }
         
 
        .dataTables_wrapper .dataTables_paginate .paginate_button:active {
            background: #ffb200 ! important;
            border-bottom: 1px solid #ffb200 !important;  
        }
        

        a.paginate_button.paginate_number.active {
            background: #ffb200 ! important;
            border-bottom: 1px solid #ffb200 !important;  
        }
        
        .dataTables_wrapper .dataTables_paginate .paginate_button{
            color: #fff !important;
        }
</style>

<h2 class="userListTxt" id="usersEnterpriseBanner">
	<div class="filterMainHeadDiv">Users</div>
	<div class="filterApplyHead" style="display: none;"><img src="/Express/images/icons/filter_icon_lrg.png"><span>Quick Filters Applied<span></span></span></div>
</h2>
<div class="vertSpacer spacerHide">&nbsp;</div>

<div class="">
	<!-- Search Box - Start -->
	<form class="form-horizontal container" id="enterpriseUserSearchForm" style="width: 90%;margin: auto; background: none !important;">
		<input name="action" value="GetAllUser" type="hidden"/>

		<div class="row">
			<div class="">

				<div class="form-group" style="margin: 0 auto; width: 830px;">
					<label class="col-xs-4 col-sm-3 text-left control-label labelText">Search In:</label>
					<div class="col-xs-8 col-sm-8">
						<input type="radio" class="searchInRadio" name="search_in" value="system" id="searchInSystem">
						<label for="searchInSystem"><span></span></label><lable class="labelText">System</lable>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" class="searchInRadio" name="search_in" value="enterprise" id="searchInEnterprise" CHECKED>&nbsp;&nbsp;&nbsp;
						<label for="searchInEnterprise"><span></span></label><label class="labelText">Enterprise</label>
					</div>
				</div>

				<div class="form-group" id="restrictToEnterpriseDiv" style="display: none; margin: 0 auto; width: 830px;">
					<label class="col-md-3 text-left control-label labelText">Restrict to Enterprise:</label>
					<div class="col-md-6">
                                            <div class="dropdown-wrap">
						<select class="form-control" name="restrictToEnterprise" id="restrictToEnterprise" style="width: 100% !important;"><?php echo getUsersServerProviderList(); ?></select>
                                            </div>
                                        </div>
				</div>

				<!--
				<div class="form-group">
					<label class="col-xs-8 col-sm-6 text-left">Search Mode:</label>
					<div class="col-xs-16 col-sm-16">
						<input type="radio" class="searchModeRadio" name="search_mode" value="enterprise" id="searchModeEnterprise" CHECKED>&nbsp;&nbsp;&nbsp;Enterprises
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" class="searchModeRadio" name="search_mode" value="group" id="searchModeGroup">&nbsp;&nbsp;&nbsp;Groups
					</div>
				</div>
				-->

			</div>
			<!-- Quick Filters Btn - Start -->
			<div class="">
				<div id="enterpriseUsersQuickFiltersBox" style="float: right;">
					<div class="row" style="margin: 0 auto;">
						<button type="button"
						        class="filterTitleBtn cancelButton"
						        id="enterpriseUsersQuickFiltersStatusTitleBtn" style="margin-top: -30px;">
							<span id="enterpriseUsersQuickFiltersStatusTitleText" style="color: inherit;">Quick Filters</span>
							<span id="enterpriseUsersQuickFiltersStatusTitleIcon" class="glyphicon glyphicon-chevron-right fIcon" style="color: inherit;"></span>
						</button>
					</div>
				</div>
			</div>
			<!-- Quick Filters Btn - End -->

		</div>

		<div class="row">
			<div class="form-group">
				<div class="col-xs-12 text-center">
					<button type="button" class="subButton" id="enterpriseUserSearchFormSubmitBtn">Search</button>
				</div>
			</div>
		</div>

	</form>
	<!-- Search Box - End -->

	<!-- Quick Filters - Start -->
	<div id="enterpriseUsersQuickFiltersDiv" class="container" style="clear:both;padding-top:12px;padding-bottom: 20px;margin: 0 auto;display: none; background: none !important;margin-top:30px !important;">
		<div class="divCFAforStyle fcorn-registerTwo">
			<div class="row">
				<h2 class="subBannerCustom">Quick Filters</h2>
				<div class="col-xs-12">
					<form id="enterpriseUsersQuickFiltersForm">
						<div id="enterpriseUsersQuickFilters">
						</div>
						<br/>
						<div class="row fcorn-registerTwo adminAddBtnDiv">
							<div class="col-md-2" style="width:21% !important">
								<div class="form-group">
									<label class="labelText">Response Size Limit</label>
									<div class="dropdown-wrap">
										<select style="width:100% !important" id="enterpriseUsers_responseSizeLimit" name="responseSizeLimit">
											<option value="">Unlimited</option>
											<option value="100">100</option>
											<option value="500">500</option>
											<option value="1000">1000</option>
											<option value="5000">5000</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="row text-center">
							<button type="button"
							        id="enterpriseUsersFiltersFormSubmitBtn"
							        class="subButton"
							        style="margin-bottom:6px; margin-top:18px; padding:9px 15px; border-radius:4px;">Apply Filters
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
      //on key enter page prevent reload
       $('form#enterpriseUsersQuickFiltersForm').keypress( function( e ) {
            var code = e.keyCode || e.which;
            if( code === 13 ) {
                e.preventDefault();
                $( "#enterpriseUsersFiltersFormSubmitBtn" ).trigger("click");
            }
        });
    //end code 
    
    var eqf_params = {
            'filters': {
                'UserFirstName': {
                    'title': 'User First Name',
                    'modes': ['Contains', 'Starts With', 'Equal To']
                },
                'UserLastName': {
                    'title': 'User Last Name',
                    'modes': ['Contains', 'Starts With', 'Equal To']
                },
                'Extension': {
                    'title': 'Extension',
                    'modes': ['Contains', 'Starts With', 'Equal To']
                },
                'Dn': {
                    'title': 'DN',
                    'modes': ['Contains', 'Starts With', 'Equal To']
                },
                'UserId': {
                    'title': 'User ID',
                    'modes': ['Contains', 'Starts With', 'Equal To']
                },
                'EmailAddress': {
                    'title': 'User Email Address',
                    'modes': ['Contains', 'Starts With', 'Equal To']
                }
            },
            'resetCallback': resetEnterpriseQuickFilters,
            'allowMultiple': true
        };

        buildDKFilters('#enterpriseUsersQuickFilters', eqf_params);

        $('#enterpriseUsersQuickFiltersStatusTitleBtn').click(function () {
            toggleEnterpriseQuickFiltersDiv();
        });

        function toggleEnterpriseQuickFiltersDiv(status) {

            status = status || null;

            var enterpriseUsersQuickFiltersDivObj = $('#enterpriseUsersQuickFiltersDiv');

            if (status != null) {
                enterpriseUsersQuickFiltersDivObj.toggle(status);
            } else {
                enterpriseUsersQuickFiltersDivObj.toggle();
            }

            if (enterpriseUsersQuickFiltersDivObj.is(":visible")) {
                $('#enterpriseUsersQuickFiltersStatusTitleIcon').addClass('glyphicon-chevron-down');
            } else {
                $('#enterpriseUsersQuickFiltersStatusTitleIcon').removeClass('glyphicon-chevron-down');
            }

        }

        function resetEnterpriseQuickFilters() {
            $('#enterpriseUsersQuickFiltersForm select[name="responseSizeLimit"]').val('');
        }

        toggleEnterpriseQuickFiltersDiv();
	</script>
	<!-- Quick Filters - End -->

	<div class="row">
		<div id="enterpriseUserSearchResults" class="col-xs-12"></div>
	</div>
</div>
<div class="row" style="padding: 20px">&nbsp;</div>
<script type="application/javascript">

    var enterpriseSearch_Session = 0;
var checkchangeRestricEnt = "";
    $().ready(function () {
         
        //Toggle 'Restrict to Enterprise' drop down when 'Search In' option is switched
       $('.searchInRadio').change(function () {
            var checkEnterPrise = $(this).val();
            $(".dkFBResetFiltersBtn").trigger("click");
            if(checkEnterPrise=="enterprise"){
               checkchangeRestricEnt = "";
               $( "#enterpriseUserSearchFormSubmitBtn" ).trigger("click");
            }else{
                 checkchangeRestricEnt = "";
                $('#restrictToEnterprise').val("");
                $( "#enterpriseUserSearchFormSubmitBtn" ).trigger("click");
            } 
            //$('#restrictToEnterprise').prop('disabled', $('#searchInEnterprise').is(':checked'));
             $('#restrictToEnterprise').val("");
            $('#restrictToEnterpriseDiv').toggle($('#searchInSystem').is(':checked'));
          //  toggleEnterpriseQuickFiltersDiv(false); // old code
        });
        
    $("#restrictToEnterprise").change(function () {
       $('#restrictToEnterprise').prop('disabled', $('#searchInEnterprise').is(':checked'));
        checkchangeRestricEnt ="changesEnt";
       $( "#enterpriseUserSearchFormSubmitBtn" ).trigger("click");

    });
        //Search Button Clicked
        $('#enterpriseUserSearchFormSubmitBtn').click(function () {
        	pendingProcess.push("Enterprise Users");
            $("#enterpriseUserSearchResults").html("<div style='padding-top: 50px;text-align: center;'><img src=\"/Express/images/ajax-loader.gif\"></div>");

            enterpriseSearch_Session++;

            $.post("/Express/usersEnterprise/getUsers.php", $('#enterpriseUserSearchForm').serialize(), function (data) {
            	if(foundServerConErrorOnProcess(data, "Enterprise Users")) {
					return false;
                }
                $('#enterpriseUserSearchResults').html(data);
            });
           // toggleEnterpriseQuickFiltersDiv(false);
            $("#usersEnterpriseBanner .filterApplyHead").hide();
            $('#enterpriseUsersQuickFiltersStatusTitleIcon').removeClass('glyphicon-chevron-down');
            $('#enterpriseUsersQuickFiltersDiv').hide();
        });

        //Apply Filters Button Clicked
        $('#enterpriseUsersFiltersFormSubmitBtn').click(function () {

            $("#enterpriseUserSearchResults").html("<div style='padding-top: 50px;text-align: center;'><img src=\"/Express/images/ajax-loader.gif\"></div>");

            enterpriseSearch_Session++;

            $has_filters = false;
            $('#enterpriseUsersQuickFiltersForm .dkFB_Filter_Item_Value_Input').each(function () {
                var filter_value = $(this).val();
                if(filter_value && filter_value.trim().length > 0) {
                    $has_filters = true;
                    return 0;
                }
            });
            if($('#enterpriseUsers_responseSizeLimit').val().length) {
                $has_filters = true;
            }

            var dataToSend = $('#enterpriseUserSearchForm').serialize() + '&' + $("#enterpriseUsersQuickFiltersForm").serialize();
            $.post("/Express/usersEnterprise/getUsers.php", dataToSend, function (data) {
                $('#enterpriseUserSearchResults').html(data);
                if($has_filters) {
                    $("#usersEnterpriseBanner .filterApplyHead").show();
                } else {
                    $("#usersEnterpriseBanner .filterApplyHead").hide();
                }
            });
            $('#enterpriseUsersQuickFiltersStatusTitleIcon').addClass('glyphicon-chevron-down');
            $('#enterpriseUsersQuickFiltersDiv').show();
        //    toggleEnterpriseQuickFiltersDiv(false); //old code
        });


            var checkedRadio = $("input[type=radio][name=search_in]:checked").val();
            if(checkedRadio=="enterprise"){
               checkchangeRestricEnt = "";
               $("#enterpriseUserSearchFormSubmitBtn").hide();
               $( "#enterpriseUserSearchFormSubmitBtn" ).trigger("click");
            } 
    });
     
</script>