<?php
/**
 * Created by Dipin Krishna.
 * Date: 11/20/18
 * Time: 11:07 PM
 */
?>
<tr id='<?php echo $enterpriseUserRow['userId']; ?>'
    data-groupname="<?php echo $enterpriseUserRow['groupId']; ?>"
    data-sp="<?php echo $enterpriseUserRow['sp']; ?>" <?php echo $_SESSION ["permissions"]["modifyUsers"] == "1" ? "class='userIdVal'" : "" ?>>
	<td><?php echo $enterpriseUserRow['sp']; ?></td>
	<?php
	if (!isset($showGroupColumn) || $showGroupColumn) {
		?>
		<td><?php echo $enterpriseUserRow['groupId']; ?></td>
	<?php
	}
	?>
	<td><?php echo $enterpriseUserRow['userId']; ?></td>
	<td><?php echo $enterpriseUserRow['activated']; ?></td>
	<td><?php echo $enterpriseUserRow['userName']; ?></td>
	<td><?php echo $enterpriseUserRow['phoneNumber']; ?></td>
	<td><?php echo $enterpriseUserRow['ext']; ?></td>
</tr>