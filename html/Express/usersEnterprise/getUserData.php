<?php
/**
 * Created by Dipin Krishna.
 * Date: 11/21/18
 * Time: 12:10 AM
 */
?>
<?php
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once("/var/www/html/Express/usersEnterprise/GetUsersInSystem.php");
$gUISystem = new GetUsersInSystem();

$searchCriteria_token = $_POST['searchCriteria_token'];
$searchCriteria = $_SESSION["enterprise_user_search_criteria_$searchCriteria_token"];

//Search Mode
$searchMode = isset($_POST['search_mode']) ? $_POST['search_mode'] : '';

//Show or Hide Group Column
$showGroupColumn = true;
if ($searchMode == 'enterprise') {
	$showGroupColumn = false;
}

//error_log(print_r($_SESSION["enterprise_user_search_criteria_$searchCriteria_token"], true));

if (isset($searchCriteria['ServiceProvider'])) {

	if (!isset($_SESSION["enterprise_user_search_Groups_$searchCriteria_token"])) {

		$serP = $searchCriteria['ServiceProvider'];
		require_once ("/var/www/lib/broadsoft/adminPortal/getAllGroups.php");

		if (isset($allGroups)) {
			//Save group array to session
			$_SESSION["enterprise_user_search_Groups_$searchCriteria_token"] = $allGroups;
		}

	}

	if (isset($_SESSION["enterprise_user_search_Groups_$searchCriteria_token"])) {
		$groupsInSystem = $_SESSION["enterprise_user_search_Groups_$searchCriteria_token"];
	}


	//Check if we have any group left to process
	if (isset($groupsInSystem) && is_array($groupsInSystem) && count($groupsInSystem)) {

		//Pop a group
		$groupSelected = array_pop($groupsInSystem);

		//Save the rest if the group array back into Session
		$_SESSION["enterprise_user_search_Groups_$searchCriteria_token"] = $groupsInSystem;

		//$searchCriteria['ServiceProvider'] = $groupSelected['sp'];
		//$searchCriteria['GroupId'] = $groupSelected['groupId'];

		$spId = '';
		$groupId = '';

		$next_spId = '';
		$next_groupId = '';

		$nextGroup = is_array($groupsInSystem ) ? array_pop($groupsInSystem) : null;
		//if($nextGroup) {
		//print_r($nextGroup);
		//}

		if (isset($searchCriteria['ServiceProvider'])) {
			$spId = $searchCriteria['ServiceProvider'];
			$groupId = $groupSelected;

			$next_spId = $searchCriteria['ServiceProvider'];
			$next_groupId = $nextGroup ? $nextGroup : null;

			unset($searchCriteria['ServiceProvider']);
		} else {
			$spId = $groupSelected['sp'];
			$groupId = $groupSelected['groupId'];

			$next_spId = $nextGroup ? $nextGroup['sp'] : '';
			$next_groupId = $nextGroup ? $nextGroup['groupId'] : null;
		}

		//error_log(print_r($searchCriteria, true));

		$userList = $gUISystem->getAllUsersInGroup($spId, $groupId, $searchCriteria);

	}

} else {

	//if($searchMode == 'enterprise') {

	if (!isset($_SESSION["enterprise_user_search_serviceProviders_$searchCriteria_token"])) {

		$serviceProviders = $gUISystem->getUsersServerProviderList();
		$serviceProviders = array_keys($serviceProviders);
		$_SESSION["enterprise_user_search_serviceProviders_$searchCriteria_token"] = $serviceProviders;

	}

	if (isset($_SESSION["enterprise_user_search_serviceProviders_$searchCriteria_token"])) {
		$serviceProviders = $_SESSION["enterprise_user_search_serviceProviders_$searchCriteria_token"];
	}

	//error_log(print_r($serviceProviders, true));

	//Check if we have any sp left to process
	if (isset($serviceProviders) && is_array($serviceProviders) && count($serviceProviders)) {

		//Pop a group
		$spId = array_pop($serviceProviders);

		//Save the rest if the group array back into Session
		$_SESSION["enterprise_user_search_serviceProviders_$searchCriteria_token"] = $serviceProviders;

		$next_spId = is_array($serviceProviders ) ? array_pop($serviceProviders) : null;

		$searchCriteria['ServiceProvider'] = $spId;

		//error_log(print_r($searchCriteria, true));

		$userList = $gUISystem->getAllUsersInSystem($searchCriteria, true);

	}

	/*
	} else {

		if (!isset($_SESSION["enterprise_user_search_Groups_$searchCriteria_token"])) {

			require_once("/var/www/lib/broadsoft/adminPortal/getAllGroupsInSystem.php");

			if (isset($allGroupsInSystem)) {
				//Save group array to session
				$_SESSION["enterprise_user_search_Groups_$searchCriteria_token"] = $allGroupsInSystem;
			}
			//$serviceProviders = $gUISystem->getUsersServerProviderList();
			//$serviceProviders = array_keys($serviceProviders);
			//$_SESSION["enterprise_user_search_serviceProviders_$searchCriteria_token"] = $serviceProviders;

		}

		if (isset($_SESSION["enterprise_user_search_Groups_$searchCriteria_token"])) {
			$groupsInSystem = $_SESSION["enterprise_user_search_Groups_$searchCriteria_token"];
		}

	}
	*/
}

if ($userList && empty($userList['Error'])) {

	if (is_array($userList['Success'])) {

		$len = count($userList['Success']);
		$i = 0;

		foreach ($userList['Success'] as $key => $enterpriseUserRow) {
			include "displayUserRow.php";
			$i++;

			if ($i < $len) {
				echo "++++----++++";
			}
		}
	}

	echo "++++++++++++++++++++";
	if($next_spId) {
		echo " ($next_spId" . ( $next_groupId ? " - $next_groupId" : "") . ")";
	}
	exit;

} else {
//Error
}


echo "Done";
?>