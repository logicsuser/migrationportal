<?php
/**
 * Created by Dipin Krishna.
 * Date: 11/20/18
 * Time: 8:26 PM
 */

require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/GroupLevelOperations.php");
require_once("/var/www/lib/broadsoft/adminPortal/dns/dns.php");


class GetUsersInSystem
{

	public $userListInGroup = array();
	public $department = "";
	public $error = null;

	public function getAllUsersInGroup($spId, $groupId, $searchCriteria = null) {

		global $sessionid, $client;

		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";

		$xmlinput = xmlHeader($sessionid, "UserGetListInGroupRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId) . "</serviceProviderId>";
		$xmlinput .= "<GroupId>" . htmlspecialchars($groupId) . "</GroupId>";

		//User Filters
		$xmlinput .= $this->buildSearchCriteria($searchCriteria);

		$xmlinput .= xmlFooter();
		//print_r($xmlinput);
		//error_log(print_r($xmlinput, true));
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//error_log(print_r($xml, true));
		$errMsg = readErrorXmlGenuine($xml);
		if ($errMsg != "") {
			$errorMsg = str_replace("%0D%0A", "", trim($errMsg));
			preg_match('/Please narrow your search, there are ([0-9]+) matching records/', $errorMsg, $output_array);
			if ($output_array && isset($output_array[1])) {
				$userCount = $output_array[1];
			}
		} else {
			$returndata = array();
			$userCount = 0;
			foreach ($xml->command->userTable->row as $key => $value) {

				$returndata[$userCount]["groupId"] = $groupId;
				$returndata[$userCount]["sp"] = $spId;
				$returndata[$userCount]["userId"] = strval($value->col[0]);
				$returndata[$userCount]["lastName"] = strval($value->col[1]);
				$returndata[$userCount]["firstName"] = strval($value->col[2]);
				$returndata[$userCount]["department"] = strval($value->col[3]);
				$returndata[$userCount]["phoneNumber"] = strval($value->col[4]);
				$returndata[$userCount]["activated"] = strval($value->col[5]);
				$returndata[$userCount]["emailAddress"] = strval($value->col[6]);
				$returndata[$userCount]["ext"] = strval($value->col[10]);

				$returndata[$userCount]['userName'] = $returndata[$userCount]["firstName"] . " " . $returndata[$userCount]["lastName"];

				if ($returndata[$userCount]["activated"] == 'true') {
					$returndata[$userCount]["activated"] = 'Yes';
				} else if ($returndata[$userCount]["activated"] == 'false') {
					$returndata[$userCount]["activated"] = 'No';
				} else {
					$returndata[$userCount]["activated"] = '';
				}

				$userCount++;

			}
			$returnResponse["Success"] = $returndata;
		}

		return $returnResponse;

	}

	public function getAllUsersInSystem($searchCriteria = null, $displayUserRow = false) {

		global $sessionid, $client;

		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";

		$xmlinput = xmlHeader($sessionid, "UserGetListInSystemRequest");

		//User Filters
		$xmlinput .= $this->buildSearchCriteria($searchCriteria);

		$xmlinput .= xmlFooter();
		//print_r($xmlinput);
		//error_log(print_r($xmlinput, true));
		//$start_time = time();
		//error_log("started at : " . $start_time);
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//error_log(print_r($xml, true));
		$errMsg = readErrorXmlGenuine($xml);
		//error_log("Response took: " . ($start_time - time()) . " seconds");
		if ($errMsg != "") {
			$errorMsg = str_replace("%0D%0A", "", trim($errMsg));
			preg_match('/Please narrow your search, there are ([0-9]+) matching records/', $errorMsg, $output_array);
			if ($output_array && isset($output_array[1])) {
				$userCount = $output_array[1];
			}
		} else {
			$returndata = array();
			$userCount = 0;
			//error_log(print_r($xml->command->userTable, true));
			foreach ($xml->command->userTable->row as $key => $value) {
				$userId = strval($value->col[0]);

				//From System
				$returndata[$userCount]["userId"] = strval($value->col[0]);
				$returndata[$userCount]["groupId"] = strval($value->col[1]);
				$returndata[$userCount]["sp"] = strval($value->col[2]);
				$returndata[$userCount]["lastName"] = strval($value->col[3]);
				$returndata[$userCount]["firstName"] = strval($value->col[4]);
				$returndata[$userCount]["department"] = strval($value->col[5]);
				$returndata[$userCount]["phoneNumber"] = strval($value->col[6]);
				$returndata[$userCount]["activated"] = strval($value->col[7]);
				$returndata[$userCount]["emailAddress"] = strval($value->col[8]);
				$returndata[$userCount]["ext"] = strval($value->col[12]);

				$returndata[$userCount]['userName'] = $returndata[$userCount]["firstName"] . " " . $returndata[$userCount]["lastName"];

				if ($returndata[$userCount]["activated"] == 'true') {
					$returndata[$userCount]["activated"] = 'Yes';
				} else if ($returndata[$userCount]["activated"] == 'false') {
					$returndata[$userCount]["activated"] = 'No';
				} else {
					$returndata[$userCount]["activated"] = '';
				}

				//Print the User Row
				if($displayUserRow) {
					$enterpriseUserRow = $returndata[$userCount];
					include "displayUserRow.php";
				}

				$userCount++;
			}
			//error_log("Processing took: " . (time() - $start_time) . " seconds");
			$returnResponse["Success"] = $returndata;
		}

		return $returnResponse;

	}

	public function getUserCount($searchCriteria = null) {

		global $sessionid, $client;

		$userCount = 0;

		$xmlinput = xmlHeader($sessionid, "UserGetListInSystemRequest");

		$xmlinput .= "<responseSizeLimit>1</responseSizeLimit>";

		//User Filters
		$xmlinput .= $this->buildSearchCriteria($searchCriteria);

		$xmlinput .= xmlFooter();
		//print_r($xmlinput);
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//print_r($xml);
		$errMsg = readErrorXmlGenuine($xml);
		if ($errMsg != "") {
			$errorMsg = str_replace("%0D%0A", "", trim($errMsg));
			preg_match('/Please narrow your search, there are ([0-9]+) matching records/', $errorMsg, $output_array);
			if ($output_array && isset($output_array[1])) {
				$userCount = $output_array[1];
			}
		} else {
			$returndata = array();
			foreach ($xml->command->userTable->row as $key => $value) {
				$userCount++;
			}
		}

		return $userCount;

	}

	public function buildSearchCriteria($searchCriteria) {
		$xmlinput = '';

		if ($searchCriteria) {

			//error_log(print_r($searchCriteria, true));

			if (isset($searchCriteria['UserLastName'])) {
				foreach ($searchCriteria['UserLastName'] as $searchItem) {
					$xmlinput .= "<searchCriteriaUserLastName>";
					$xmlinput .= "<mode>{$searchItem['mode']}</mode>";
					$xmlinput .= "<value>{$searchItem['value']}</value>";
					$xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
					$xmlinput .= "</searchCriteriaUserLastName>";
				}
			}

			if (isset($searchCriteria['UserFirstName'])) {
				foreach ($searchCriteria['UserFirstName'] as $searchItem) {
					$xmlinput .= "<searchCriteriaUserFirstName>";
					$xmlinput .= "<mode>{$searchItem['mode']}</mode>";
					$xmlinput .= "<value>{$searchItem['value']}</value>";
					$xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
					$xmlinput .= "</searchCriteriaUserFirstName>";
				}
			}

			if (isset($searchCriteria['Dn'])) {
				foreach ($searchCriteria['Dn'] as $searchItem) {
					$xmlinput .= "<searchCriteriaDn>";
					$xmlinput .= "<mode>{$searchItem['mode']}</mode>";
					$xmlinput .= "<value>{$searchItem['value']}</value>";
					$xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
					$xmlinput .= "</searchCriteriaDn>";
				}
			}

			if (isset($searchCriteria['EmailAddress'])) {
				foreach ($searchCriteria['EmailAddress'] as $searchItem) {
					$xmlinput .= "<searchCriteriaEmailAddress>";
					$xmlinput .= "<mode>{$searchItem['mode']}</mode>";
					$xmlinput .= "<value>{$searchItem['value']}</value>";
					$xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
					$xmlinput .= "</searchCriteriaEmailAddress>";
				}
			}

			if (isset($searchCriteria['ServiceProvider'])) {
				$ServiceProvider = htmlentities($searchCriteria['ServiceProvider']);
				$xmlinput .= "<searchCriteriaExactServiceProvider><serviceProviderId>$ServiceProvider</serviceProviderId></searchCriteriaExactServiceProvider>";
			}

			if (isset($searchCriteria['GroupId'])) {
				$xmlinput .= "<searchCriteriaGroupId>{$searchCriteria['GroupId']}</searchCriteriaGroupId>";
			}

			if (isset($searchCriteria['UserId'])) {
				foreach ($searchCriteria['UserId'] as $searchItem) {
					$xmlinput .= "<searchCriteriaUserId>";
					$xmlinput .= "<mode>{$searchItem['mode']}</mode>";
					$xmlinput .= "<value>{$searchItem['value']}</value>";
					$xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
					$xmlinput .= "</searchCriteriaUserId>";
				}
			}

			if (isset($searchCriteria['Extension'])) {
				foreach ($searchCriteria['Extension'] as $searchItem) {
					$xmlinput .= "<searchCriteriaExtension>";
					$xmlinput .= "<mode>{$searchItem['mode']}</mode>";
					$xmlinput .= "<value>{$searchItem['value']}</value>";
					$xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
					$xmlinput .= "</searchCriteriaExtension>";
				}
			}

		}

		return $xmlinput;
	}

	public function getUsersServerProviderList() {

		global $sessionid;
		global $client;

		$serviceProviders = array();
		$xmlinput = xmlHeader($sessionid, "ServiceProviderGetListRequest");
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$a = 0;
		foreach ($xml->command->serviceProviderTable->row as $k => $v) {
			if (isset($v->col[1]) && !empty($v->col[1])) {
				$serviceProviders[strval($v->col[0])] = strval($v->col[0]) . " - " . strval($v->col[1]); //ex-774
			} else {
				$serviceProviders[strval($v->col[0])] = strval($v->col[0]);
			}
			$a++;
		}

		uksort($serviceProviders, 'strcasecmp');

		return $serviceProviders;
	}
}