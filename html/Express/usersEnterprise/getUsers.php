<?php
/**
 * Created by Dipin Krishna.
 * Date: 11/19/18
 * Time: 8:19 PM
 */
?>
<?php
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once("/var/www/html/Express/usersEnterprise/GetUsersInSystem.php");

$gUISystem = new GetUsersInSystem();

if (isset($_POST['action']) && $_POST['action'] == "GetAllUser") {

	//User Filters - Start
	$searchCriteria = null;
	if (isset($_POST['filterItem'])) {

		//error_log(print_r($_POST['filterItem'], true));
		foreach ($_POST['filterItem'] as $key => $filterItem) {
			if (isset($_POST['filterItemValue'][$key]) && ($_POST['filterItemValue'][$key] || $_POST['filterItemMode'][$key] == 'Equals to')) {
				if (!isset($searchCriteria[$filterItem])) {
					$searchCriteria[$filterItem] = array();
				}
				$searchCriteria[$filterItem][] = array('mode' => $_POST['filterItemMode'][$key], 'value' => $_POST['filterItemValue'][$key]);
			}
		}

	}

	if ($_POST['search_in'] == 'system') {

		if (isset($_POST['restrictToEnterprise']) && $_POST['restrictToEnterprise']) {
			$searchCriteria['ServiceProvider'] = $_POST['restrictToEnterprise'];
		}

	} else {
		$searchCriteria['ServiceProvider'] = $_SESSION['sp'];
            
	}

	//error_log(print_r($searchCriteria, true));
	//User Filters - End

	$userCount = $gUISystem->getUserCount($searchCriteria);

	if (isset($_POST['responseSizeLimit']) && $_POST['responseSizeLimit'] > 0) {
		//$searchCriteria['responseSizeLimit'] = $_POST['responseSizeLimit'];

		if ($_POST['responseSizeLimit'] < $userCount) {
			?>
			<div class='container' style="margin-top: 10px;">
				<div class="alert alert-danger">There are <?php echo $userCount; ?> matching users - please narrow your search.</div>
			</div>
			<?php
			exit;
		}
	}

	$searchCriteria_token = time() + rand();
	$_SESSION["enterprise_user_search_criteria_$searchCriteria_token"] = $searchCriteria;

	//Search Mode
	$searchMode = isset($_POST['search_mode']) ? $_POST['search_mode'] : '';

	//Show or Hide Group Column
	$showGroupColumn = true;
	if ($searchMode == 'enterprise') {
		$showGroupColumn = false;
	}

	$retrievedAllUsers = false;
	?>

	<!--<table class="table table-bordered stripe row-border order-column " id="enterpriseSearchUserTable" cellspacing="0" style="width:100%;"> -->
<!--code add by sollogics developer -->
<style>
   
</style>
<table class="table table-bordered stripe row-border order-column customDataTable enterpriseSearchUserTable" id="enterpriseSearchUserTable" cellspacing="0" style="width:100%;margin-top: 10px;">
<!--end -->
		<thead>
		<!--<tr>
			<th>Enterprise&nbsp;&nbsp;&nbsp;</th>
			<?php
			if ($showGroupColumn) {
				?>
			<th>Group&nbsp;&nbsp;&nbsp;</th>
			<?php
			}
			?>
			<th>User Id&nbsp;&nbsp;&nbsp;</th>
			<th>Activated&nbsp;&nbsp;&nbsp;</th>
			<th>User Name&nbsp;&nbsp;&nbsp;</th>
			<th>Phone Number&nbsp;&nbsp;&nbsp;</th>
			<th>Extension&nbsp;&nbsp;&nbsp;</th>
		</tr> -->
                    
                    <tr>
			<th>Enterprise&nbsp;&nbsp;&nbsp;</th>
			<?php
			if ($showGroupColumn) {
				?>
			<th>Group</th>
			<?php
			}
			?>
			<th>User Id</th>
			<th>Activated</th>
			<th>User Name</th>
			<th>Phone Number</th>
			<th style="border-right: none !important;">Extension</th>
                    </tr> 
		</thead>
		<tbody>
		<?php
		if ($userCount <= 10000) {
			$userListInGroup = $gUISystem->getAllUsersInSystem($searchCriteria, true);
			if (empty($userListInGroup['Error'])) {

				if (is_array($userListInGroup['Success'])) {

					//foreach ($userListInGroup['Success'] as $key => $enterpriseUserRow) {
					//	include "displayUserRow.php";
					//}

					$retrievedAllUsers = true;

				}

			} else {
				//Error
			}
		}
		?>
		</tbody>
	</table>
	<div class="usersDetailedListTable_action_buttons" style="display: none;">
		<button type="button" class="btn btn-link" style="float:right;margin-top: 4px !important;" id="downloadCSV">
			<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png">
			<br/>
			<span>Download<br>CSV</span>
		</button>
	</div>

	<script type="application/javascript">

        var total_count = <?= $userCount ?>;
        var enterpriseSearchUserTable_DT;
        var current_Session = enterpriseSearch_Session;
        var loadUserData = 1;
        ;
        var downloadCSVDiv = $('#downloadCSV');

        function initCompleteEnterpriseUsers() {

            //Check If Data needs to be loaded, else skip.
            if (!loadUserData) return;

            //console.log(current_Session + " = " + enterpriseSearch_Session);

	        //Skip if it is not the same page/action.
            if (current_Session !== enterpriseSearch_Session) return;
 
			<?php
			if ($retrievedAllUsers) {
			?>      if(total_count > 0){
                                    $("#appendedNoResultTr").remove();
                                    $("#enterpriseSearchUserTable_info").show();
                                    $(".odd").show();
                                }else{
                                    $(".odd").hide();
                                    $("table#enterpriseSearchUserTable tbody").append("<tr><td id='appendedNoResultTr' colspan='7' style='text-align:center'>No Users Found</td></tr>");
                                    $("#enterpriseSearchUserTable_info").hide();
                                }
                                $('.enterpriseUsersTable_bottom_bar .retrieving_info').html('Retrieved all ' + total_count + ' users');
			<?php
			} else {
			?>
	        $("#downloadCSV").prop('disabled', true);
            $('.enterpriseUsersTable_bottom_bar .retrieving_info').html('Retrieving 50 of ' + total_count + ' users     ' + '<img style="width:50px;" src="/Express/images/ajax-loader.gif"/>');
            getUserData("<?php echo $searchCriteria_token ?>");
			<?php
			}
			?>

            var DT_top_bar_column_4 = $('.enterpriseUsersTable_top_bar .column4');
            DT_top_bar_column_4.prepend(downloadCSVDiv);

        }


        //Get User Data
        function getUserData(searchCriteria_token) {

            //Skip if it is not the same page/action.
            if (current_Session !== enterpriseSearch_Session) return;

            var dataToSend = {'searchCriteria_token': searchCriteria_token, 'search_mode': '<?php echo $searchMode ?>'};
            $.post("/Express/usersEnterprise/getUserData.php", dataToSend, function (data) {
                if (current_Session !== enterpriseSearch_Session) return;
                if (data && data !== 'Done') {

                    var rowsAndNextGroup = data.split("++++++++++++++++++++");
                    var nextGroup = '';

                    var dataRows = rowsAndNextGroup[0];
                    if(rowsAndNextGroup.length > 1) {
	                    nextGroup = '<br/>' + rowsAndNextGroup[1];
                    }

                    //console.log('dataRows: ' + dataRows + ' , ' + nextGroup);
                    if(dataRows) {
                        var TRs = dataRows.split("++++----++++");

                        $.each(TRs, function (key, TR) {

                            if(current_Session !== enterpriseSearch_Session) return;

                            //console.log( key + ": " + TR );
	                        enterpriseSearchUserTable_DT.row.add($(TR));

                        });
                    }

                    if(current_Session !== enterpriseSearch_Session) return;

                    enterpriseSearchUserTable_DT.draw(false);

                    var count = enterpriseSearchUserTable_DT.page.info().recordsTotal;
                    count = (count + 50) <= total_count ? count + 50 : total_count;
                    $('.enterpriseUsersTable_bottom_bar .retrieving_info').html('Retrieving ' + count + ' of ' + total_count + ' users     '
	                    + '<img style="width:50px;" src="/Express/images/ajax-loader.gif">' + nextGroup);

	                getUserData(searchCriteria_token);


                } else {
                    $('.enterpriseUsersTable_bottom_bar .retrieving_info').html('Retrieved all ' + total_count + ' users');
                    $("#downloadCSV").prop('disabled', false);
                }
            });

        }

        $().ready(function () {

            $('#downloadCSV').click(function () {

                //Skip if it is not the same page/action.
                if(current_Session !== enterpriseSearch_Session) return;

                //var btn_text = $(this).val();
                //$(this).val('Downloading ...');                
                var titles = [];
                var TableData = [];
                var i = 0;

                $('#enterpriseSearchUserTable th').each(function () {    //table id here
                    //if (i > 0) {
                        titles.push($(this).text());
                    //}
                    i++;
                });

                if (enterpriseSearchUserTable_DT) {
                    var phoneNumber;
                    var data = enterpriseSearchUserTable_DT.rows().data().toArray();
                    $.each(data, function (index, row) {
                        $.each(row, function (c_index, value) {
                            //if (c_index !== 0) {                           
                                //Code added @ 29 July 2019 Regarding EX-1445
                                if(c_index == 5 && value != ""){
                                    phoneNumber = " "+value;
                                    TableData.push(phoneNumber);
                                }else{
                                    TableData.push(value);
                                }
                                //End code
                                //TableData.push(value); //Code commented Regarding EX-1445
                            //}                            
                        });
                    });
                }

                var CSVString = prepCSVRow(titles, titles.length, '');
                CSVString = prepCSVRow(TableData, titles.length, CSVString);

                var blob = new Blob(["\ufeff", CSVString]);
                var fileName = "USERS_<?php echo time() ?>" + ".csv";
                if (navigator.msSaveBlob) { // IE 10+
                	navigator.msSaveBlob(blob, fileName);
                }else{
                	var downloadLink = document.createElement("a");
                    
                    var url = URL.createObjectURL(blob);
                    downloadLink.href = url;
                    downloadLink.download = fileName;

                    document.body.appendChild(downloadLink);
                    downloadLink.click();
                    document.body.removeChild(downloadLink);
                }

                

            });


            //Navigate to Modify User
	        $(document).on("click", ".userIdVal", function(event)
	        {

                //Skip if it is not the same page/action.
                if(current_Session !== enterpriseSearch_Session) return;

	            //console.log('current_Session : ' + current_Session);
                event.stopPropagation();

                $("#mainBody").html("<div style='padding-top: 50px;text-align: center;'><img src=\"/Express/images/ajax-loader.gif\"></div>");

                current_Session = 0;
                var userId = $(this).attr("id");

                var groupName = $(this).data("groupname");
                var Selsp = $(this).data("sp");

                //console.log("SP:" + Selsp + " Grp:" + groupName);
                //console.log('userId: ' + userId);

                $.ajax({
                    type: "POST",
                    url: "setGroup.php",
                    data: {groupName: groupName, Selsp: Selsp},
                    success: function (result) {

                        window.location.replace('/Express/main.php?modify_user=' + userId);

                        //$('<form method="POST" action="/Express/main.php" style="display:none;"><input name="modify_user" value="' + userId +'"/></form>').appendTo('body').submit();

						/*
                        $.ajax({
                            type: "POST",
                            url: "userMod/userMod.php",
                            //data: { searchVal: userId, userFname : fname, userLname : lname },
                            success: function(result)
                            {
                                $("#mainBody").html(result);
                                $("#searchVal").val(userId);

                                setTimeout(function() {
                                    $("#go").trigger("click");
                                }, 2000);

                                $('#helpUrl').attr('data-module', "userMod");
                            }

                        });
                        */

                    }
                });

            });

        });

        function prepCSVRow(arr, columnCount, initial) {
            var row = '';
            var delimeter = ',';
            var newLine = '\r\n';

            function splitArray(_arr, _count) {
                var splitted = [];
                var result = [];
                _arr.forEach(function (item, idx) {
                    item = item.replace(/\"/g, '\\"');
                    item = '"' + item + '"';
                    if ((idx + 1) % _count === 0) {
                        splitted.push(item);
                        result.push(splitted);
                        splitted = [];
                    } else {
                        splitted.push(item);
                    }
                });
                return result;
            }

            var plainArr = splitArray(arr, columnCount);

            plainArr.forEach(function (arrItem) {
                arrItem.forEach(function (item, idx) {
                    row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
                });
                row += newLine;
            });
            return initial + row;
        }


        function drawEnterpriseSearchUserTable() {

            //Skip if it is not the same page/action.
            if(current_Session !== enterpriseSearch_Session) return;

            enterpriseSearchUserTable_DT = $('#enterpriseSearchUserTable').DataTable({
                scrollY: 435,
                destroy: true,
                scrollX: true,
                 paging: true,
                pagingType: "dk_express_pagination",
                lengthMenu: [50, 100, 200],
                searching: false,
                autoWidth: false,
                deferRender: false,
                order: [],
                columnDefs: [<?php
	                if ($showGroupColumn) {
	                ?>
                    { "width": "10%", "targets": 0 },
                    { "width": "15%", "targets": 1 },
                    { "width": "30%", "targets": 2 },
                    { "width": "10%", "targets": 3 },
                    { "width": "20%", "targets": 4 },
                    { "width": "13%", "targets": 5 },
                    { "width": "12%", "targets": 6 }
	                <?php
	                } else {
	                ?>
                    { "width": "10%", "targets": 0 },
                    { "width": "35%", "targets": 1 },
                    { "width": "20%", "targets": 2 },
                    { "width": "20%", "targets": 3 },
                    { "width": "13%", "targets": 4 },
                    { "width": "12%", "targets": 5 }
	                <?php
	                }
	                ?>
                ],

                info: true,
                language: {
                    emptyTable: " ",
                    infoEmpty: "<?php echo $userCount ? "Loading ..." : "No Users Found"?>",
                    lengthMenu: "Show<br/> _MENU_ <br/>users",
                    info: "Showing _START_ to _END_ of _TOTAL_ users",
                    oPaginate: {
                        sPageJumpText: "Jump to pg ",
                        //sPageJumpGoBtn: 'GO'
                    }
                },

                dom: '<"wrapper"<"enterpriseUsersTable_top_bar dataTables_top_bar"<"column1"><"column2"><"column3"><"column4"l>>'
                    + 't'
                    + '<"enterpriseUsersTable_bottom_bar dataTables_bottom_info_bar"<"retrieving_info"><"currently_showing_info"i>p>>',

                "initComplete": initCompleteEnterpriseUsers
            });

        }
        drawEnterpriseSearchUserTable();

        /* Don't need this anymore since I found a css fix.
        //On Window Re-size
        $(window).resize(function() {
            if (current_Session === enterpriseSearch_Session) {
                if(enterpriseSearchUserTable_DT) {
                    loadUserData = 0;
                    drawEnterpriseSearchUserTable();
                }
            }
        });
        */

	</script>
	<!--[if IE]>
	<style>
		table.dataTable tbody td {
			padding: 2px 10px !important;
		}
	</style>
	<![endif]-->
	<style type="text/css">
		td, td > .userIdVal {
			white-space: normal;
			word-break: break-all;
		}

		div.dataTables_wrapper {
			width: 90%;
			margin: 0 auto;
		}

		table.dataTable tbody th, table.dataTable tbody td {
			text-align: left;
		}

		table.dataTable thead th, table.dataTable thead td {
			text-align: left !important;
		}
 td.dataTables_empty {
			display: none;
		}

		/*end code */
                 /*code comment by sollogics developer */
		/*table.dataTable.no-footer thead tr .sorting_asc {
			background-image: url(/Express/images/blue/desc.gif) !important;
			background-position: 98% 48%;
		}

               
		table.dataTable.no-footer thead tr .sorting_desc {

			background-image: url(/Express/images/blue/asc.gif);
			background-position: 98% 48%;
		}

		table.dataTable thead .sorting {
			background-image: url(/Express/images/blue/bg.gif);
			background-position: 98% 48%;
		} */

                /*end code commented */
		/* #enterpriseSearchUserTable_wrapper table thead tr th 
		{
			border-top: 2px solid #d8dada !important;
			border-bottom: 2px solid #d8dada !important;
			border-right: 2px solid #d8dada !important;
			color: #fff !important;
		} */

		#enterpriseSearchUserTable_wrapper .dataTables_wrapper.no-footer .dataTables_scrollBody {
			border-bottom: none;
		}

		#enterpriseSearchUserTable_wrapper .DTFC_LeftBodyWrapper,
		#enterpriseSearchUserTable_wrapper .dataTables_scrollBody {
			/*margin-top: -26px;
			padding-top: 25px;*/
			border-top: 1px solid #fff;
                          height: auto !important;
                        max-height: 435px !important;
			/*min-height: 100px;*/
		}
                
               /* .dataTables_wrapper .dataTables_paginate .paginate_button {
                    color: #6ea0dc !important;
                }*/
		/* table.customDataTable thead, .dataTables_scrollHeadInner, table.customDataTable thead th {
			background-color: #5d81ac;
			border-top: 2px solid #d8dada !important;
			border-bottom: 2px solid #d8dada !important;
			border-right: 2px solid #d8dada !important;
			color: #fff !important;
		} */
                
                /*code add by sollogics developer */
                table.customDataTable thead, .dataTables_scrollHeadInner, table.customDataTable thead th {
                    background-color: #5d81ac;
                    border-right: 2px solid #d8dada !important;
                    color: #fff !important;
                   /* padding-left: 18px !important;*/
                }
                /*end code */

		#enterpriseUserSearchResults .userIdVal:hover td {
			background-color: #dcfac9 !important;
			cursor: pointer;
		}

		#enterpriseUserSearchResults table.dataTable tbody td {
			font-size: 11px;
		}

		input[type="button"][disabled], button[disabled] {
			pointer-events: visible;
			cursor: not-allowed;
		}

		/* #enterpriseSearchUserTable_wrapper .dataTables_scrollHeadInner table {
			margin-bottom: 0;
		} */

                /*code added by sollogics developer */
                #enterpriseSearchUserTable_wrapper .dataTables_scrollHeadInner table {
			margin-bottom: 0;
                        border-collapse: collapse !important;
		}
                /*end code */
		/* 100% Width*/
		#enterpriseSearchUserTable_wrapper .dataTables_scrollHeadInner {
			width: 100% !important;
			padding-left: 0 !important;
		}
		#enterpriseSearchUserTable_wrapper .dataTables_scrollHeadInner table {
			width: 100% !important;
			/*padding-right: 1.4%;*/
			padding-right: 20px;
		}
		#enterpriseSearchUserTable_wrapper .dataTables_scrollHeadInner table tr th:last-child {
			border-right-color: transparent;
		}
		#enterpriseSearchUserTable_wrapper .dataTables_scrollHeadInner table tr th {
			border-bottom-color: transparent;
		}
		#enterpriseSearchUserTable_wrapper .dataTables_scrollBody {
			width: 100% !important;
		}
		#enterpriseSearchUserTable_wrapper .dataTables_scrollBody table {
			width: 100% !important;
		}
		#enterpriseSearchUserTable_wrapper .dataTables_scrollBody table tr td {
			height: 25px;
		}

		#enterpriseSearchUserTable_length select {
			width: 65px !important;
		}

		.paging_dk_express_pagination .paginate_jump_input {
			width: 65px !important;
		}

		/* Datatables - This should have been in a common css file*/
		.dataTables_bottom_info_bar {
			text-align: center;
		}
		.dataTables_bottom_info_bar .retrieving_info {
			display: inline-block;
			float: left;
			padding-top: 0.755em;
		}
		.retrieving_info {
			color: #6ea0dc !important;
			font-weight: 700;
		}
		.dataTables_bottom_info_bar .currently_showing_info {
			display: inline-block;
		}
		.dataTables_info {
			color: #6ea0dc !important;
			font-weight: 700;
		}
		.dataTables_wrapper .dataTables_paginate {
			float: right;
			text-align: right;
			padding-top: 0.25em;
		}

		/*a.paginate_button {
			background-color: transparent !important;
		} */
		/* Datatables - ends */

		#downloadCSV, #downloadCSV:hover {
			text-decoration: none;
			padding: 0;
		}

		/* Scrollbar */
		::-webkit-scrollbar {
			-webkit-appearance: scrollbarbutton-up;
			width: 20px;
		}
		::-webkit-scrollbar-thumb {
			background-color: #c0c0c0;
			-webkit-box-shadow: 0 0 1px rgba(255, 255, 255, .5);
		}

		::-webkit-scrollbar-track {
			-webkit-box-shadow: inset 0 0 6px #f1f1f1;
			-webkit-border-radius: 0;
			border-radius: 0;
			background-color: #f1f1f1;
		}
		/* Scrollbar - Ends*/
                /*code added by sollogics developer */
                .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
                    border: 0px solid #ddd;
                }
                table#enterpriseSearchUserTable_wrapper tbody tr td:first-child {
                    border-left: 2px solid #d8dada;
                }
                
                table.enterpriseSearchUserTable_wrapper tbody tr td:first-child,table#enterpriseSearchUserTable_wrapper tbody tr td:first-child {
                    border-left: 1px solid #cccccc;
                    text-align: center;
                   /* padding: 2px 10px 0px 8px !important;*/
                }
                
                table.enterpriseSearchUserTable tbody tr td, table#enterpriseSearchUserTable tbody tr td {
                    /* background-color: #FFF; */
                    /* border-right: 2px solid #d8dada; */
                    border-top: 2px solid #d8dada !important;
                    border-bottom: 2px solid #d8dada !important;
                    background-color: #eeeeee;
                   /*  padding: 8px 22px;*/
                       padding: 2px 20px;
                }
                
                #enterpriseSearchUserTable_wrapper .dataTables_scrollBody table tr td:first-child {
                    border-left: 2px solid #d8dada;
                }
                  table.enterpriseSearchUserTable{border: 0px solid;}
               
              
                  table.enterpriseSearchUserTable thead tr th {
                    height: 30px;
}  

                table.enterpriseSearchUserTable thead th, table.dataTable thead td {
                   padding: 0px 17px 18px;
                   
                }
                 
                table.enterpriseSearchUserTable thead th, table.dataTable tbody td {
                   padding: 0px 17px 18px;
                }
                .dataTables_wrapper.no-footer .dataTables_scrollBody {
                    border-bottom: 0px solid #eef0f3;
                }
               table.customDataTable thead tr:after{
               display: block !important;
               visibility:visible;
               }
                /* end code */
	</style>
	<?php
}