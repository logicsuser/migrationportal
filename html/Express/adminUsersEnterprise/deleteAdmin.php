<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();

	require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");
	require_once("/var/www/lib/broadsoft/adminPortal/util/PreviousChangeGroupUtil.php");
	
	if(isset($_POST["userId"])) {
		$oldUserData = get_user_info($_POST["userId"], true);
	}

	//$deleteQuery = "DELETE from users where id='" . $_POST["userId"] . "'";
	//$deleteResult = $db->query($deleteQuery);
	$stmt = $db->prepare("SELECT `userName`,`superUser` FROM `users` WHERE `id`= ?");
        $stmt->execute(array($_POST["userId"]));
        $countRows =  $stmt->rowCount();
        $user = $stmt->fetch();
         
        if($countRows > 0) {
            if($user['superUser'] =='0' || $user['superUser'] =='1'){
                $delete_stmt = $db->prepare("DELETE from `featuresPermissions` where `userId` = ?");
                $delete_stmt->execute(array($_POST["userId"]));
            }
            
            if($user['superUser'] =='3'){
                $delete_stmt = $db->prepare("DELETE from `enterpriseFeaturesPermissions` where `userId` = ?");
                $delete_stmt->execute(array($_POST["userId"]));
            }
        }
        
        
        
	$delete_stmt = $db->prepare("DELETE from `users` where `id` = ?");
	$delete_stmt->execute(array($_POST["userId"]));
	
	//$deleteQuery = "DELETE from permissions where id='" . $_POST["userId"] . "'";
	//$deleteResult = $db->query($deleteQuery);
	
	 /* ex-824 @ 20-09-2018*/
	$delete_stmt = $db->prepare("DELETE from `permissions` where `userId` = ?");
	$delete_stmt->execute(array($_POST["userId"]));
	//End code
	
	//Code added @ 16 July 2018 to delete permissions from Features Permissions table
    //$delete_stmt = $db->prepare("DELETE from featuresPermissions where userId = ?");
   // $delete_stmt->execute(array($_POST["userId"]));
        //End code
	
	//Code added @ 25 Sep 2018 to delete row filter user table
    $delete_filter_stmt = $db->prepare("DELETE from `user_filters` where `userid` = ?");
    $delete_filter_stmt->execute(array($_POST["userId"]));
    //End code
    /* Start Deletion previous change group info */
    $preCG = new PreviousChangeGroupUtil();
    $changeGroupDetail = $preCG->deleteChangeGroupOfAdmin($_POST["userId"]);
    /* End Deletion previous change group info */
    
	//Add Admin Log Entry - Start
	if(isset($_POST["userId"])) {
		require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
		$adminLogs = new adminLogs();
                
                $superUser      = $_SESSION["superUser"];
                $adminEmail     = $_SESSION["emailAddress"];
                $adminTypeName  = $_SESSION["adminType"];
                $adminUserName  = $_SESSION["loggedInUserName"];
                $lastActivity   = date("m/d/Y", $_SESSION["LAST_ACTIVITY"]);
                $tmpArr         = explode(" ", $_SESSION["loggedInUser"]);
                $adminFirstName = $tmpArr[0];
                $adminLastName  = $tmpArr[1];
                
		$log = array(
			'adminUserID' => $oldUserData['id'],
			'eventType' => 'DELETE_ADMIN',
			'adminUserName' => $oldUserData["userName"],
			'adminName' => $oldUserData["firstName"] . ' ' . $oldUserData["lastName"],
			'updatedBy' => $_SESSION["adminId"],
			'details' => array(
				'Deleted By'    => $_SESSION["loggedInUser"],
				'User Name'     => $adminUserName,
				'First Name'    => $adminFirstName,
				'Last Name'     => $adminLastName,
				'Email Address' => $adminEmail,
				'Super User'    => (isset($superUser) and $superUser == "1") ? "Yes" : "No",
				'Disabled'      => (isset($oldUserData["disabled"]) and $oldUserData["disabled"] == "1") ? "Yes" : "No",
				'Admin Type'    => $adminTypeName,
				"Last Login"    => $lastActivity
			)
		);
		$adminLogs->addLogEntry($log);
	}
	//Add Admin Log Entry - End

	echo "Administrator has been deleted.";
?>
