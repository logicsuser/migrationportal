<?php
	//require_once("/var/www/lib/broadsoft/login.php");
	//checkLogin();

        if(isset($_POST['clusterName']) && $_POST['clusterName'] != ""){
            $selectedClusterName = $_POST['clusterName'];
        }
        require_once("/var/www/lib/broadsoft/adminPortal/loginClusterBise.php");
        $sessionid = $sessionidForSelCluster;
        $client    = $clientForSelCluster;
        
        
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	//require_once ("/var/www/lib/broadsoft/adminPortal/domains/domains.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getSystemDomains.php");
	
	if(isset($_POST["action"]) && $_POST["action"] == "getSysSecurityDomainList") {
	        global $systemDomains, $permissions, $securityDomainPattern;
	        $systemDomainsList = array();
	        foreach ($systemDomains as $key => $value){
	            if(strpos($value, $securityDomainPattern) !== false){
	                $systemDomainsList[] = $value;
	            }
	        }
	        
	        foreach ($systemDomainsList as $domain) {
	            echo $domain ." ". ":";
	        }
	        die;
	}
	
    function getSecurityDomainGroups($sp, $securityDomain, $sessionid, $client) {
        //global $sessionid, $client;
        $groups = array();

        // get list of Groups that have security domain assigned
        $xmlinput = xmlHeader($sessionid, "ServiceProviderDomainGetAssignedGroupListRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($sp) . "</serviceProviderId>";
        $xmlinput .= "<domain>" . $securityDomain . "</domain>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $a = 0;
        foreach ($xml->command->groupTable->row as $key => $value)
        {
            $groups[$a] = strval($value->col[0]);
            $a++;
        }
        return $groups;
    }

	if (isset($_POST["chooseGroup"]) && $_POST["chooseGroup"] == "true") {
        if ($_SESSION["superUser"] == "1") {
            $serP = $_POST["getSP"];
            require_once("/var/www/lib/broadsoft/adminPortal/getAllGroups.php");
            $groups = $allGroups;
            //$options = "SU Option";
        }
        else {
            // Security Domains
            $groups = getSecurityDomainGroups($_POST["getSP"], $_SESSION["securityDomain"], $sessionidForSelCluster, $clientForSelCluster);
            //$options = "SP:" . $_POST["getSP"] . " Sec:" . $_SESSION["securityDomain"];
        }

        $options = "";
        foreach ($groups as $v) {
            $options .= "<option value=\"" . $v . "\">" . $v . "</option>";
        }
        echo $options;
    }
    else {
	    // retaining old stuff

        $serP = $_POST["getSP"];
        $options = "";
        require_once("/var/www/lib/broadsoft/adminPortal/getAllGroups.php");

        if (isset($allGroups))
        {
            foreach ($allGroups as $v)
            {
                if (isset($_POST["dropdown"]) and $_POST["dropdown"] == "true")
                {
                    $options .= "<option value=\"" . $v . "\">" . $v . "</option>";
                }
                else
                {
                    $options .= "<li class=\"ui-state-default\" id=\"" . $v . "\">" . $v . "</li>";
                }
            }
        }
        echo $options;
    }

    function getServiceProviderDomainListRes($sp, $sessionidForSelCluster, $clientForSelCluster) {
        //$dns = new domains();
        $dnslist = getServiceProviderDomainListResClusterBise($sp, $sessionidForSelCluster, $clientForSelCluster);
        return $dnslist;
    }
    
    function getServiceProviderDomainListResClusterBise($spId, $sessionid, $client){
	    //global  $sessionid, $client;
	    
	    $getDomainResponse["Error"] = "";
	    $getDomainResponse["Success"] = "";
	    
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderDomainGetAssignedListRequest");
	    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId) . "</serviceProviderId>";
	    $xmlinput .= xmlFooter();
	    
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    if (readErrorXmlGenuine($xml) != "") {
	        $getDomainResponse["Error"] = strval($xml->command->summaryEnglish);
	    }else{
	        $getDomainResponse["Success"]['serviceProviderDefaultDomain'] = strval($xml->command->serviceProviderDefaultDomain);
	        $a = 0;
	        foreach ($xml->command->domain as $key => $value)
	        {
	            $domain[$a] = strval($value);
	            $a++;
	        }
	        $getDomainResponse["Success"]['domain'] = $domain;
	    }
	    return $getDomainResponse;
    }
	
    
?>
