<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$getSP = $_POST["getSP"];
	$getGroup = $_POST["getGroup"];
	$options = "";

	//retrieve list of admins for the selected group
	$xmlinput = xmlHeader($sessionid, "GroupAdminGetListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($getSP) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($getGroup) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	if (isset($xml->command->groupAdminTable->row))
	{
		foreach ($xml->command->groupAdminTable->row as $key => $value)
		{
			$admins[$a]["id"] = strval($value->col[0]);
			$admins[$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
			$a++;
		}
	}

	if (isset($admins))
	{
		$admins = subval_sort($admins, "name");
		foreach ($admins as $key => $value)
		{
			$options .= "<option value=\"" . $value["id"] . "\">" . $value["id"] . " (" . $value["name"] . ")</option>";
		}
	}

	echo $options;
?>
