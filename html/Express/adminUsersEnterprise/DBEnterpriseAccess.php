<?php

/**
 * Created by Karl.
 * Date: 1/21/2017
 *
 * This Class handles the following Enterprise Access scenarios
 * - All Enterprises
 * - Multiple Enterprises
 * - One Enterprise:  All Groups
 * - One Enterprise:  Multiple Groups
 * - One Enterprise:  One Group
 */
class DBEnterpriseAccess
{
    private $db;
    private $txt = "";
    private $newRecord = true;

    private $adminID;
    private $rdAllEnterprises;
    private $wrAllEnterprises;
    private $rdAllGroups;
    private $wrAllGroups;
    private $rdEnterprisesID = array();
    private $wrEnterprisesID = array();
    private $rdGroupsID = array();
    private $wrGroupsID = array();

    private static function tableName() { return "enterpriseAccess"; }

    private static function attrAdminID()        { return "adminID"; }
    private static function attrAllEnterprises() { return "allEnterprises"; }
    private static function attrAllGroups()      { return "allGroups"; }
    private static function attrEnterpriseID()   { return "enterpriseID"; }
    private static function attrGroupID()        { return "groupID"; }

    /**
     * DBAccessPrivileges constructor.
     * @param $db
     * @param string $adminID
     */
    public function __construct($db, $adminID = "") {
        $this->db = $db;
        $this->adminID = $adminID;

        $this->get();
    }

    public function getTxt() {
        return $this->txt;
    }


    /*************************************************
     * @return mixed
     */
    public function getAllEnterprisesFlag() {
        return $this->rdAllEnterprises;
    }


    /*************************************************
     * @param $flag
     */
    public function setAllEnterprisesFlag($flag) {
        $this->wrAllEnterprises = $flag == true ? "true" : "false";
    }


    /*************************************************
     * @return mixed
     */
    public function getAllGroupsFlag() {
        return $this->rdAllGroups;
    }


    /*************************************************
     * @param $flag
     */
    public function setAllGroupsFlag($flag) {
        $this->wrAllGroups = $flag == true ? "true" : "false";
    }


    /*************************************************
     * @return array
     */
    public function getEnterprises() {
        return $this->rdEnterprisesID;
    }


    /*************************************************
     * @param $enterpriseList
     */
    public function setEnterprises($enterpriseList) {
        $this->wrEnterprisesID = $enterpriseList;
    }


    /*************************************************
     * @return array
     */
    public function getGroups() {
        return $this->rdGroupsID;
    }


    /*************************************************
     * @param $groupList
     */
    public function setGroups($groupList) {
        $this->wrGroupsID = $groupList;
    }


    /*************************************************
     * @param $sourceAdminID
     */
    public function cloneEnterpriseAccess($sourceAdminID) {
        $cloneEA = new DBEnterpriseAccess($this->db, $sourceAdminID);

        $this->wrAllEnterprises = $cloneEA->getAllEnterprisesFlag();
        $this->wrAllGroups = $cloneEA->getAllGroupsFlag();
        $this->wrEnterprisesID = $cloneEA->getEnterprises();
        $this->wrGroupsID = $cloneEA->getGroups();
    }


    /*************************************************
     * Order of validating changes
     * - modify entries with groups if changed - for multiple groups delete entries with all groups and add new groups
     * - modify 'all groups' indicator if changed - delete entries with all groups and add entry with 'all groups'
     * - modify entries with enterprises if changed for multiple enterprises delete entries with all enterprises and add new ones
     * - modify 'all enterprises' indicator if changed
     */
    public function write() {
        if ($this->wrAllEnterprises == "" && $this->wrAllGroups == "" && $this->wrEnterprisesID == "" && $this->wrGroupsID == "") {
            return true;
        }
$this->txt .= " write:";
        // Get enterpriseID to process group changes if any
        $enterpriseID = "";
        if (count($this->wrEnterprisesID) == 1) {
            $enterpriseID = $this->wrEnterprisesID[0];
        }
        elseif (count($this->rdEnterprisesID) == 1) {
            $enterpriseID = $this->rdEnterprisesID[0];
        }

        $numGroups = count($this->wrGroupsID);
        if ($numGroups > 0) {
            if ($enterpriseID == "") { return false; }
            return $this->newRecord ? $this->processGroupsInsert($enterpriseID) : $this->processGroupsUpdate($enterpriseID);
        }

        // At this point we know that groups were not modified - check whether 'all groups' indicator has been enabled

        if ($this->wrAllGroups != "" && $this->wrAllGroups == "true") {
            if ($enterpriseID == "") { return false; }
            return $this->newRecord ? $this->processAllGroupsFlagInsert($enterpriseID) : $this->processAllGroupsFlagUpdate($enterpriseID);
        }

        // At this point changes must be in enterprises

        $numEnterprises = count($this->wrEnterprisesID);
        if ($numEnterprises > 0) {
            return $this->newRecord ? $this->processEnterprisesInsert() : $this->processEnterprisesUpdate();
        }

        // Finally check for All Enterprises flag

        if ($this->wrAllEnterprises != "") {
            return $this->newRecord ? $this->processAllEnterprisesFlagInsert() : $this->processAllEnterprisesFlagUpdate();
        }

        return true;
    }

    /*************************************************
     *
     */
    public function delete() {
        //delete from enterpriseAccess where adminID='test4';

        $query = "delete from " . self::tableName() . " where " . self::attrAdminID() . "='" . $this->adminID . "'";
        $this->db->query($query);
    }

    //-----------------------------------------------------------------------------------------------------------------
    private function get() {
//      select * from accessPrivileges where adminID='someAdminID'

        $query = "select * from " . self::tableName() . " where " . self::attrAdminID() . "='" . $this->adminID . "'";
$this->txt = $query;
        $result = $this->db->query($query);

        while ($row = $result->fetch()) {
            if ($row[self::attrAllEnterprises()] != "") {
                $this->rdAllEnterprises = $row[self::attrAllEnterprises()];
            }

            if ($row[self::attrAllGroups()] != "") {
                $this->rdAllGroups = $row[self::attrAllGroups()];
            }

            if ($row[self::attrEnterpriseID()] != "") {
                $this->rdEnterprisesID[] = $row[self::attrEnterpriseID()];
            }

            if ($row[self::attrGroupID()] != "") {
                $this->rdGroupsID[] = $row[self::attrGroupID()];
            }

            $this->newRecord = false;
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function processGroupsUpdate($enterpriseID) {
        // Delete all existing entries for this adminID
        $this->delete();

        // then insert all new entries
        return $this->processGroupsInsert($enterpriseID);
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function processGroupsInsert($enterpriseID) {
        // build Query
        //INSERT INTO `enterpriseAccess` (`adminID`,`enterpriseID`,`groupID`) VALUES ('test1','EST','2087435');

        for ($i = 0; $i < count($this->wrGroupsID); $i++) {
            $query  = "insert into " . self::tableName() . " (`" . self::attrAdminID() . "`, `";
            $query .= self::attrEnterpriseID() . "`, `" . self::attrGroupID() . "`)";
            $query .= " values ('" . $this->adminID . "', '" . $enterpriseID . "', '" . $this->wrGroupsID[$i] . "')";
$this->txt .= $query . " * ";
            $this->db->query($query);
        }

        return true;
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function processEnterprisesUpdate() {
        // Delete all existing entries for this adminID
        $this->delete();

        // then insert all new entries
        return $this->processEnterprisesInsert();
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function processEnterprisesInsert() {
        // Query:
        // insert into enterpriseAccess (`adminID`,`enterpriseID`) values ('test2','PST');

        for ($i = 0; $i < count($this->wrEnterprisesID); $i++) {
            $query = "insert into " . self::tableName() . " (`" . self::attrAdminID() . "`, `" . self::attrEnterpriseID() . "`)";
            $query .= " values ('" . $this->adminID . "', '" . $this->wrEnterprisesID[$i] . "')";
$this->txt .= $query . " * ";
            $this->db->query($query);
        }
        return true;
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function processAllGroupsFlagUpdate($enterpriseID) {
        // Delete all existing entries for this adminID
        $this->delete();

        // then insert all new entries
        return $this->processAllGroupsFlagInsert($enterpriseID);
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function processAllGroupsFlagInsert($enterpriseID) {
        // Query
        // insert into enterpriseAccess (`adminID`,`allGroups`,`enterpriseID`) values ('test1','true','EST');

        $query  = "insert into " . self::tableName() . " (`" . self::attrAdminID() . "`, `";
        $query .= self::attrAllGroups() . "`, `" . self::attrEnterpriseID() . "`)";
        $query .= " values ('" . $this->adminID . "', '" . $this->wrAllGroups . "', '" . $enterpriseID . "')";
$this->txt .= $query;
        $this->db->query($query);
        return true;
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function processAllEnterprisesFlagUpdate() {
        // Delete all existing entries for this adminID
        $this->delete();

        // then insert all new entries
        return $this->processAllEnterprisesFlagInsert();
        return true;
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function processAllEnterprisesFlagInsert() {
        // Query:
        // insert into enterpriseAccess (`adminID`,`allEnterprises`) values ('test3','true');

        $query  = "insert into " . self::tableName() . " (`" . self::attrAdminID() . "`, `" . self::attrAllEnterprises() . "`)";
        $query .= " values ('" . $this->adminID . "', '" . $this->wrAllEnterprises . "')";
$this->txt .= $query;
        $this->db->query($query);
        return true;
    }
}
