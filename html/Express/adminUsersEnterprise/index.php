<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();

	//label names for form fields
	$_SESSION["adminUsersNames"] = array(
		"superUser" => "Super User",
		"disabled" => "Disable User",
                "entAdministratorClusterId" => "Cluster Name",
                "entGroupAdministratorClusterId" => "Cluster Name",
                "entUpdateAdministratorClusterId" => "Cluster Name",
		"bsSp" => "Service Provider",
		"bsGroupId" => "Group",
		"userName" => "Username",
		"userNameUpdate" => "Username",
		"password" => "Password",
	    "verifyPassword" => "Verify Password",
		"passwordUpdate" => "Password",
		"name" => "Name",
		"nameUpdate" => "Name",
		"emailAddress" => "Email Address",
		"emailAddressUpdate" => "Email Address",
		"user_cell_phone_number" => "Cell Phone Number",
		"use_sms_every_login" => "Use SMS every login",
		"use_sms_on_password_reset" => "Require SMS after password reset only",
		"user_cell_phone_numberUpdate" => "Cell Phone Number",
		"use_sms_every_loginUpdate" => "Use SMS every login",
		"use_sms_on_password_resetUpdate" => "Require SMS after password reset only",
		"Selsp" => "Service Provider",
		"sp" => "Service Provider",
		"groups1" => "Groups",
		"groups_1" => "Groups",
                "enterpriseAssignUser" => "Service Provider",
                "enterpriseAssignUserUpdate" => "Service Provider",
		"perms" => "Permissions",
		"permsUpdate" => "Permissions",
                "entperms" => "Enterprise Permissions",
		"entpermsUpdate" => "Enterprise Permissions",
                "securityDomain" => "Security Domain",
                "checkPermissions" => "Admin Type",
		"use_ldap_authentication" => "Require SMS after password reset only",
                "groupBasicInfo" => "Group Basic Info",
                "groupDomains" => "Group Domains",
                "groupServices" => "Group Services",
                "groupDN" => "Group DNs",
                "groupPolicies" => "Group Call Processing Policies",
                "groupNCOS" => "Group NCOS",
                "groupVoicePortal" => "Group Voice Portal"
	);

	//label names for permissions
	$_SESSION["fieldNames"] = array(
		"basicUser" => "Basic Info",
		"resetPasswords" => "Web Portal Password",
		"voiceMailPasscode" => "Voice Mail Passcode",
                "deleteUsers" => "Delete Users",
		"changeDevice" => "Phone Device",
                "changeDevice" => "Phone Device",
                "voiceManagement" => "Voice Management",
		"changeHotel" => "Hoteling",
		"changeForward" => "Call Control",
		"changeblf" => "BLF",
		"changeSCA" => "Shared Call Appearance",
		"modhuntgroup" => "Modify Hunt Group",
		"addusers" => "Add Users",
		"modifyUsers" => "Modify Users",
		"modcallCenter" => "Modify Call Center",
                "groupWideUserModify" => "Group-wide Users Modify",
		"aamod" => "Auto Attendant",
                "announcements" => "Announcements",
		"modCcd" => "Modify Custom Contact Directory",
		"modschedule" => "Modify Schedule",
		"viewcdrs" => "View CDRs",
		"viewusers" => "View Users",
		"viewnums" => "View Numbers",
		"viewChangeLog" => "View Change Log",
		"adminUsers" => "Admin Users",
                "vdmLight" => "Device Management",
                "modifyGroup" => "Modify Group",
                "callPickupGroup" => "Call Pickup Group",
                "vdmAdvanced" => "Device Management Advanced",
                "scaOnlyDevice" => "SCA-Only Devices",
                "siteReports" => "Site Reports",
                "sasTest" => "SAS Test",
                "expressSheets" => "Express Sheets",
                "groupBasicInfo" => "Group Basic Info",
                "groupDomains" => "Group Domains",
                "groupServices" => "Group Services",
                "groupDN" => "Group DNs",
                "groupPolicies" => "Group Call Processing Policies",
                "groupNCOS" => "Group NCOS",
                "groupVoicePortal" => "Group Voice Portal",
                "announcements" => "Announcements",
                "clearMACAddress" => "Clear MAC Address",
                "servicePacks" => "Service Packs",
	);
        
        $_SESSION["permissionAction"] = array(
		"administrators"                    => array("modify"=>"yes", "addDelete"=>"yes"),
                "adminLogs"                         => array("modify"=>"no",  "addDelete"=>"no"),
                "basicInformation"                  => array("modify"=>"yes", "addDelete"=>"yes"),
                "outgoingCallingPlan"               => array("modify"=>"yes", "addDelete"=>"no"),
                "voicePortalService"                => array("modify"=>"yes", "addDelete"=>"no"),
                "networkClassesofService"           => array("modify"=>"yes", "addDelete"=>"no"),
                "callProcessingPolicies"            => array("modify"=>"yes", "addDelete"=>"no"),
                "DNs"                               => array("modify"=>"yes", "addDelete"=>"yes"),
                "services"                          => array("modify"=>"yes", "addDelete"=>"no"),
                "securityDomains"                   => array("modify"=>"yes", "addDelete"=>"no"),
                "domains"                           => array("modify"=>"yes", "addDelete"=>"no"),
                "changeLog"                         => array("modify"=>"no", "addDelete"=>"no"),
                "phoneProfiles"                     => array("modify"=>"no", "addDelete"=>"no"),
                "broadWorksLicenseReport"           => array("modify"=>"no", "addDelete"=>"no"),
                "cdr"                               => array("modify"=>"no", "addDelete"=>"no"),
                "entServicePacks"                   => array("modify"=>"yes", "addDelete"=>"yes"),
                "entAnnouncements"                  => array("modify"=>"yes", "addDelete"=>"yes"),
                "devices"                           => array("modify"=>"no", "addDelete"=>"yes"),
                "users"                             => array("modify"=>"no", "addDelete"=>"no"),            
                "groupAdminUserPermission"          => array("modify"=>"yes", "addDelete"=>"yes"),
                "groupAdminServicesPermission"      => array("modify"=>"no", "addDelete"=>"no"),
                "groupAdminDevicesPermission"       => array("modify"=>"yes", "addDelete"=>"yes"),
                "groupAdminSupervisoryPermission"   => array("modify"=>"no", "addDelete"=>"no"),
            
            //enterpirse feature permission
                "entPermBasicInformation"           => array("modify"=>"yes", "addDelete"=>"yes"),
                "entPermDomains"                    => array("modify"=>"yes", "addDelete"=>"yes"),
                "entPermSecurityDomains"            => array("modify"=>"yes", "addDelete"=>"no"),
                "entPermServices"                   => array("modify"=>"yes", "addDelete"=>"no"),
                "entPermDNs"                        => array("modify"=>"yes", "addDelete"=>"yes"),
                "entPermBWAdministrators"           => array("modify"=>"yes", "addDelete"=>"yes"),
                "entPermCallProcessingPolicies"     => array("modify"=>"yes", "addDelete"=>"no"),
                "entPermDialPlanPolicy"             => array("modify"=>"yes", "addDelete"=>"no"),
                "entPermVoiceMessaging"             => array("modify"=>"yes", "addDelete"=>"no"),
                "entPermVoicePortal"                => array("modify"=>"yes", "addDelete"=>"no"),
                "entPermNetworkClassesofService"    => array("modify"=>"yes", "addDelete"=>"no"),
                "entPermRoutingProfile"             => array("modify"=>"yes", "addDelete"=>"no"),
                "entPermPasswordRules"              => array("modify"=>"yes", "addDelete"=>"no"),
                "entPermExpressAdministrators"      => array("modify"=>"yes", "addDelete"=>"yes")
	);
         
        //Code added @ 07 May 2018 take permissions and description from permissionsCategories
        function createPermissionFieldsName()
        {
            global $db; 
            $permissionArr = array();
	    $query = "select * from permissionsCategories";
	    $result = $db->query($query);
	    while ($row = $result->fetch()) {
	        $permissionArr[$row["permissionId"]] = $row["description"];
	    }
            return $permissionArr;
        }        
        $_SESSION["fieldNamesList"] = createPermissionFieldsName();
        //End code
        
        //Code added @ 07 May 2018 take permissions and description from permissionsCategories
        function createEnterprisePermissionFieldsName()
        {
            global $db; 
            $permissionArr = array();
	    $query = "select * from enterprisePermissionsCategories";
	    $result = $db->query($query);
	    while ($row = $result->fetch()) {
	        $permissionArr[$row["permissionId"]] = $row["description"];
	    }
            return $permissionArr;
        }        
        $_SESSION["entPsmnFieldNamesList"] = createEnterprisePermissionFieldsName();
        //End code
        
        
        function hasSecurityDomain() {
            global $securityDomainPattern;
            
            return $securityDomainPattern != "";
        }
?>

<script type="text/javascript" src="/Express/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/Express/js/dataTables.fixedColumns.min.js"></script>

<link rel="stylesheet" type="text/css" href="/Express/js/jquery.dataTables.min.css" media="screen">
<link rel="stylesheet" type="text/css" href="/Express/js/fixedColumns.dataTables.min.css" media="screen">

<style>
    .hover{
        background-color: #dcfac9 !important;
    }
</style>
<script type="text/javascript" src="adminUsersEnterprise/js/downloadAsCsv.js"></script>
<script>
    
        //Code added  @ 28 Feb 2019 regarding row hover effect for admin table
        $(document).on({
                mouseenter: function () {
                    var trIndex = $(this).index()+1;
                    $("table.dataTable").each(function(index) {                        
                    $(this).find("tr:eq("+trIndex+")").each(function(index) {                        
                        $(this).find("td").addClass("hover");
                });
                });
                }, 
                mouseleave: function () {
                    var trIndex = $(this).index()+1;
                    $("table.dataTable").each(function(index) {
                    $(this).find("tr:eq("+trIndex+")").each(function(index) {
                        $(this).find("td").removeClass("hover");
                }); 
                }); 
                }
                }, ".dataTables_wrapper tr");
          // End code

	$(function()
	{
		$("#downloadCSVBtn").hide();
		$("#loading2").hide();
		$("#modAdminListTable").hide();
		
		$("#module").html("> Admin User Modify: ");
		$("#endUserId").html("");

		$("#newAdminDialog").dialog($.extend({}, defaults, {
			width: 1100,
			open: function(event) {
				setDialogDayNightMode($(this));
				$('.ui-dialog-buttonpane').find('button:contains("Create Admin")').addClass('createButton').addClass('btnAlignCenter');
				$('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('closeButton');
				$('.ui-dialog-buttonpane').addClass('alignCenterBtn');
				
			},
			buttons: {
				"Create Admin": function()
				{
					var formData = $("form#newAdminForm").serialize();
					$.ajax({
						type: "POST",
						url: "adminUsersEnterprise/checkData.php",
						data: formData,
						success: function(result)
						{
							$("#dialog").dialog("open");
							if (result.slice(0, 1) == "1")
							{
								$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
							}
							else
							{
								$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
							}
							result = result.slice(1);
							$(":button:contains('Add/Modify Another Admin')").hide().addClass('subButton');
							$(":button:contains('Complete')").show().addClass('subButton');
							$(":button:contains('Cancel')").show().addClass('cancelButton');
							$("#dialog").html(result);
						}
					});
				},
				"Close": function()
				{
					$(this).dialog("close");
				}
			}
		}));

		$("#newAdmin").click(function()
				{
					$.ajax({
						type: "POST",
						url: "adminUsersEnterprise/newAdmin.php",
						success: function(result)
						{
							$("#newAdminDialog").dialog("open");
							$("#newAdminDialog").html(result);
		                                        $('.ui-button').trigger('blur').removeClass('ui-state-focus');
						}
					});
				});

				/*
				var adminList = function(){
				var autoComplete = new Array();
		        	$.ajax({
		        		type: "POST",
		        		url: "adminUsers/adminList.php",
		        		success: function(result) {
		        			var obj = jQuery.parseJSON(result);
		        			for (var i = 0; i < obj.length; i++) {
		        				autoComplete[i] = obj[i];
		        			}
		                    $("#selectAdminN").autocomplete({
		                        source: autoComplete
		                    }).data("ui-autocomplete")._renderItem = function (ul, item) {
		        
		                        ul.addClass('adminAutoClass'); //Ul custom class here
		        				if(item.value.search("Super User") > 0){
		        	                return $("<li></li>")
		        	                .addClass(item.adminAutoClassLi) //item based custom class to li here
		        	                .append("<a href='#' class='aClassSuper'>" + item.label + "</a>")
		        	                .data("ui-autocomplete-item", item)
		        	                .appendTo(ul);
		        				}else if(item.value.search("<span></span>") > 0){
		        					 return $("<li></li>")
		        		             .addClass(item.adminAutoClassLi) //item based custom class to li here
		        		             .append("<a href='#' style='background-color:lightgray'>" + item.label + "</a>")
		        		             .data("ui-autocomplete-item", item)
		        		             .appendTo(ul);
		        				}else{
		        					 return $("<li></li>")
		        	                .addClass(item.adminAutoClassLi) //item based custom class to li here
		        	                .append("<a href='#'>" + item.label + "</a>")
		        	                .data("ui-autocomplete-item", item)
		        	                .appendTo(ul);
		        				}
		                    };
		        		}
		        	});
		        };
				*/
				//adminList();
				$('#selectAdminN').on('keyup keypress', function(e) {
					  var keyCode = e.keyCode || e.which;
					  if (keyCode === 13) {
						  $("#loading2").show();
						  $(".ui-menu-item").hide();
						  $('#selectAdminN').autocomplete('close');
						 e.preventDefault();
						var selectUserId = $("#selectAdminN").val();
						 
						// var userArray = selectUserId.split("-");
						 var userArray = selectUserId.split("<span>-</span>");
						 var selectUserId = jQuery.trim(userArray[0]);
						getUserId(selectUserId);
					//	$('#selectAdminN').val(el[0].innerHTML.replace('<span></span>', ''));
						$('#selectAdminN').prop('disabled', true);
					  }
					});
				var getUserId = function(userName){
					$("#loading2").show();
					$.ajax({
						type: "POST",
						url: "adminUsersEnterprise/getUserId.php",
						data: { userName: userName },
						success: function(result)
						{
							
							userInfo(result);
							//$("#loading2").hide();
						}
					});
				};
						
				var userInfo = function(userIdNType){
  					if (userIdNType !== "")
					{                                               
                                                var resArr   = userIdNType.split("_");                                              
                                                var userId   = resArr[0];
                                                var userType = resArr[1];
                                                                                              
						$.ajax({
							type: "POST",
							url: "adminUsersEnterprise/adminUserData.php",
							data: { userId: userId, superUser: userType },
							beforeSend: function(){
								$("#loading2").show();
							},
							success: function(result)
							{		
								$("#loading2").hide();				
								$("#groupUsers").html(result);
							}
						});
					}
					else
					{
						//$("#loading2").hide();
						$("#endUserId").html("");
					} 
  					//$("#loading2").hide();                                  
					$('#selectAdminN').prop('disabled', false); 
				};
				
				/*
				$(document).on("click", ".adminAutoClass > li > a", function(){
					$("#groupUsers").html("");
					var el = $(this);

					$("#loading2").show();
					var selectUserId = el.html();
					if(selectUserId == ""){
						$("#groupUsers").hide();
						$("#loading2").hide();
						$('#selectAdminN').prop('disabled', false);
						return false;
					}
					 alert(selectUserId);
					var userArray = selectUserId.split("-");
					var selectUserId = jQuery.trim(userArray[0]);
					getUserId(selectUserId);
					$('#selectAdminN').val(el[0].innerHTML.replace('<span></span>', ''));
					$('#selectAdminN').prop('disabled', true);
				});
				*/

				$('#selectAdminN').on('keyup keypress keydown', function(e) {
					  var keyCode = e.keyCode || e.which;
		                         
					  if (keyCode === 13) {
						e.preventDefault();
						$('#searchAdmin').trigger('click');
					  }
				});

		
		$(document).on("click", ".goToAdminModPage", function() {
			$("#downloadCSVBtn").hide();
			$("#groupUsers").show();
			$("#modAdminListTable").hide();
			$("#groupUsers").html("");
			var el = $(this);

			$("#loading2").show();
			var selectUserId = $(this).attr('data-href');
			if(selectUserId == ""){
				$("#groupUsers").hide();
				//$("#loading2").hide();
				$('#selectAdminN').prop('disabled', false);
				return false;
			}
			//var userArray = selectUserId.split("-");
			var userArray = selectUserId.split("<span>-</span>");
			var selectUserId = jQuery.trim(userArray[0]);
			getUserId(selectUserId);
			//$('#selectAdminN').val(el[0].innerHTML.replace('<span></span>', ''));
			$('#selectAdminN').prop('disabled', true);
		});
                
                //Code added @ 28 Feb 2019
                $(document).on("click", ".goToAdmnModPage", function() {
			$("#downloadCSVBtn").hide();
			$("#groupUsers").show();
			$("#modAdminListTable").hide();
			$("#groupUsers").html("");
			var el = $(this);
                        
			$("#loading2").show();
			//var selectUserId = $(this).attr('data-href');
                        var selectUserId = $(this).attr('data-adminId');
			if(selectUserId == ""){
				$("#groupUsers").hide();
				//$("#loading2").hide();
				$('#selectAdminN').prop('disabled', false);
				return false;
			}
			//var userArray = selectUserId.split("-");
			var userArray = selectUserId.split("<span>-</span>");
			var selectUserId = jQuery.trim(userArray[0]);
			getUserId(selectUserId);
			//$('#selectAdminN').val(el[0].innerHTML.replace('<span></span>', ''));
			$('#selectAdminN').prop('disabled', true);
		});
                //End code

		$("#searchAdmin").click(function() {

			try {
				$("#adminListTable").dataTable().fnDestroy();
			}
			catch(Exc) {
				$('#searchAdmin').trigger('click');
			}

			$("#downloadCSVBtn").hide();
			$("#adminListTableBody").empty();
			$("#groupUsers").hide();
			$("#loading2").show();
			$("#modAdminListTable").hide();
			
			if($("#adminLevelSystem").is(":checked")) {
				var level = "system";
			} else if($("#adminLevelEnt").is(":checked")) {
				var level = "enterprise";
			}
			var selectAdminN = $("#selectAdminN").val();
			$.ajax({
				type: "POST",
				url: "adminUsersEnterprise/getAdminList.php",
				data: { level: level, selectAdminN: selectAdminN },
				success: function(result)
				{
					$("#loading2").hide();
					$("#modAdminListTable").show();
					var result = JSON.parse(result);
					var tableData = "";
					if(result.length > 0) {
						$.each(result, function(index, value) {

							tableData += "<tr class='goToAdmnModPage' data-adminId='" +value.modAdminHref+ "' >";
	    						tableData += "<td class='adminTableCol'>"+ value.UserName + "<input type='hidden' name='userType' id='userType' value='"+ value.SuperUser +"' /></td>";
	    						tableData += "<td class='adminTableCol'>"+ value.name + "</td>";
	    						tableData += "<td class='adminTableCol'>"+ value.adminType + "</td>";
	    						tableData += "<td class='adminTableCol'>"+ value.emailAddress + "</td>";

	    						if(value.securityDomain === "False") {
                                                            var spName = "";                                  
                                                            if(value.sp === null){
                                                                spName = ""; 
                                                            }else{
                                                                spName = value.sp; 
                                                            }
	    							tableData += "<td class='adminTableCol'>"+ spName + "</td>";
		    						tableData += "<td class='adminTableCol'>"+ value.groupList + "</td>";
		    					} else {
		    						tableData += "<td class='adminTableCol'>"+ value.securityDomain + "</td>";
			    				}
	    						
								tableData += "<td class='adminTableCol'>"+ value.Disabled + "</td>";
								tableData += "<td class='adminTableCol'>"+ value.failedLogins + "</td>";
								tableData += "<td class='adminTableCol'>"+ value.lastLogin + "</td>";
								tableData += "<td class='adminTableCol'>"+ value.user_cell_phone_number + "</td>";
	    						tableData += "<td class='adminTableCol'>"+ value.use_sms_every_login + "</td>";
								tableData += "<td class='adminTableCol'>"+ value.require_sms_to_reset + "</td>";
	    						tableData += "<td class='adminTableCol'>"+ value.use_ldap_authentication + "</td>";
	    						 
							tableData += "</tr>";
						});
						$("#downloadCSVBtn").show();
					} else {
						var tableLength = $("#adminListTable").find('th').length;
						tableData += "<tr><td>No Admin Found.</td>";
						for(i = 0; i < tableLength - 1; i++) {
							tableData += "<td></td>";
						}
						tableData += "</tr>";
					}
					$("#adminListTableBody").html(tableData);
					createDataTableToAdmin();
					//$("#adminListTable").tablesorter();
				//	$("table.dataTable thead .sorting").trigger("click");
				}
			});
			
		});
                
                $("#searchAdmin").trigger("click");
		//alert("load triggered");
		$(".triggerColumn").trigger("click"); 
	});
</script>


<h2 class="adminUserText">Administrators</h2>
<form name="adminUsers" id="adminUsers" method="POST" class="fcorn-registerTwo" action="#">
<div class="row">
  <div class="">
    <div class="col-md-11 adminSelectDiv">
            <div class="form-group">
            	
                <label class="labelText">Search Admin to view or modify</label></br>
				<input type="text" class="autoFill magnify" name="selectAdminN" id="selectAdminN" style="width:100%!important;">
				
		</div>
    </div>
	<div class="col-md-1 adminAddBtnDiv">
    	<div class="form-group">
			<?php
			$administratorsServiceLogsArr         = explode("_", $_SESSION["permissions"]["administrators"]);
                        $administratorsAddDelLogPermission    = $administratorsServiceLogsArr[2];	
                        
                        if ($administratorsAddDelLogPermission == "yes")
				{
					echo "<p class=\"register-submit addAdminBtnIcon\">";
					echo "<img src=\"images\icons\add_admin_rest.png\" name=\"newAdmin\" id=\"newAdmin\">";
					echo "</p>";
                                       echo "<label class='labelText labelTextOpp'><span>Add</span><span>Administrators</span></label>";
				}
			?>
				
		</div>
	</div>
	<div class="col-md-12">
		<div class="register-submit alignBtn">
			<input type="button" name="searchAdmin" id="searchAdmin" value="Search">
		</div>
	</div> 
	</div>
</div>	
</form>
<!-- Download as CSV Button -->
	<div class="fcorn-register" style="width:90%; margin: 0 auto;">
		<div id="downloadCSVBtn" id="downloadCSV" style="display: none;">
			<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" id="downloadAdminCSV">
	 		<br><span>Download<br>CSV</span>
		</div>
	</div>
		
<!-- table -->
<div id="modAdminListTable" style="display: none;">
	<div class="">
		<table id="adminListTable" class="stripe row-border order-column customDataTable" style="width:100%">
			<thead>
				<tr>
					<th class="adminTableCol triggerColumn">Username</th>
					<th class="adminTableCol">Name</th>
					<th class="adminTableCol">Admin Type</th>
					<th class="adminTableCol">Email</th>
					<?php if(! hasSecurityDomain()) {?>
					<th class="adminTableCol">Enterprise</th>
					<th class="adminTableCol">Groups</th>
					<?php } else { ?>
					<th class="adminTableCol">Security Domain</th>
					<?php } ?>
					<th class="adminTableCol">Disabled</th>
					<th class="adminTableCol">Failed Login Attempts</th>
    				<th class="adminTableCol">Last Login</th>
					<th class="adminTableCol">Cell Phone number</th>
					<th class="adminTableCol">Use SMS Every Login</th>
					<th class="adminTableCol">Require SMS to Reset</th>
					<th class="adminTableCol">LDAP Authentication</th>
				</tr>
			</thead>
			<tbody id ="adminListTableBody"></tbody>
		</table>
	</div>
</div>
		
	<input type="hidden" name="selectUserId" id="selectUserId" value="" />
	<div id="groupUsers"></div>
	<div id="newAdminDialog"></div>
	<div id="dialog"></div>
	<div class="loading" id="loading2">
		<img src="/Express/images/ajax-loader.gif">
	</div>
	
<style>
     table#adminListTable tbody tr:hover {
			background-color: #dcfac9 !important;
                        border-right:     2px solid #DA9873;
			cursor: pointer;
		}
td, td > .goToAdminModPage { white-space: normal; word-break: break-all; }
	table#adminListTable{margin: 0;}
		
	table#adminListTable tbody td{
	  /* border-top: 1px solid #D8DADA !important;
       border-bottom: 1px solid #D8DADA !important;*/
	   border-right: 1px solid #d8dada;
	}
	 div.dataTables_wrapper {
        width: 90%;
        margin: 0 auto;
    }
	
	table.dataTable tbody th, table.dataTable tbody td {text-align: left;}
    table.dataTable thead th, table.dataTable thead td{text-align:left !important;}
	
	    table.dataTable tbody td{
		padding: 4px 10px;
		vertical-align: middle;
	}

	.adminTableCol {
	    min-width: 120px !important;
	}
	.DTFC_LeftBodyWrapper, .dataTables_scrollBody {
    margin-top: 5px;
}
table.dataTable.display tbody tr.even>.sorting_1, table.dataTable.order-column.stripe tbody tr.even>.sorting_1 {
    background-color: #fff;
}
table.dataTable.display tbody tr.odd>.sorting_1, table.dataTable.order-column.stripe tbody tr.odd>.sorting_1 {
    background-color: #f9f9f9;
}
	table.dataTable thead th {
    border-right: 1px solid #D8DADA !important;
    border-bottom: 1px solid #D8DADA !important;
    border-top: 1px solid #D8DADA !important;
}
	 table.dataTable thead th:first-child{border-left: 1px solid #D8DADA !important;}
	 
	.dataTables_scrollBody > .dataTable thead th{border-top:none !important}
	.DTFC_LeftBodyLiner > .dataTable thead th{border-top:none !important}
 
    table.dataTable.no-footer thead tr .sorting_asc{
	background-image: url(/Express/images/icons/arrows_active_up.png)  !important;
	background-position: 98% 48%;}
	
    table.dataTable.no-footer thead tr .sorting_desc {
    background-image: url(/Express/images/icons/arrows_active_down.png);
    background-position: 98% 48%;}
	
    table.dataTable thead .sorting {
    background-image: url(/Express/images/icons/arrows_rest.png);
    background-position: 98% 48%;
}
 table.customDataTable{font-size:11px !important;}
 table.customDataTable thead, .dataTables_scrollHeadInner, table.customDataTable thead th {background-color: #5d81ac;}
 table.customDataTable{border-spacing: 0 !important;}
 #adminListTableBody tr td:first-child{border-left: 1px solid #D8DADA !important;border-right: 1px solid #D8DADA;}
 .dataTables_wrapper.no-footer .dataTables_scrollBody {
    border-bottom: 1px solid #D8DADA;
}
.DTFC_LeftBodyLiner{overflow: hidden !important;width:auto !important; }

@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
/* IE10+ CSS styles go here */
	#adminListTableBody tr{height:38px !important;}
	table.dataTable tbody td{padding : 2px 10px !important;}
}

#adminListTableBody tr{
    /* height:35px !important; */
    cursor: pointer;
}
</style>



<script type="text/javascript">

	function createDataTableToAdmin() {
		
		var table5 = $("#adminListTable").dataTable({
             scrollY:        500,
             scrollX:        true,
             scrollCollapse: true,
             paging:         false,
    		 fixedColumns:   {
    					leftColumns: 1
    		 },
    		 retrieve: true, 
    		 info: false,               
             searching: false,
             		 
         });
		$(".triggerColumn").trigger("click");
		$("table.dataTable tbody td").css("padding", "2px 10px");
  	}
	// This will execute whenever the window is resized
	$(window).resize(function() {  
        $(".triggerColumn").trigger("click"); 
        $("table.dataTable tbody td").css("padding", "2px 10px");
				
    }); 
    
    
</script>