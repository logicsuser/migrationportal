<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	require_once("/var/www/html/Express/functions.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/domains/domains.php");
	//$level = $_POST['level'];
	$selectAdminN = $_POST['selectAdminN'];
	$spListArray = array();
	$users = array();
	$adminList = array();
	
	//$dnsList = getServiceProviderDomainListRes();
	
	$sel = "SELECT distinct u.userName, u.id, u.firstName, u.lastName, u.superUser, u.disabled, u.adminType, 
            u.emailAddress, u.user_cell_phone_number, u.use_sms_every_login, u.use_ldap_authentication, 
            u.failedLogins, u.lastLogin, u.use_sms_on_password_reset, p.securityDomain, p.sp, p.groupId from users u";
	//if ($level == 'enterprise')
	//{
		//$sel .= " left join permissions p on u.id=p.userId where p.sp='" . $_SESSION["sp"] . "'";
	    $sel .= " left join permissions p on u.id=p.userId ";
	//}
	$sel .= " order by u.lastName, u.firstName, u.userName";
	$rqw = $db->query($sel);
	while ($r = $rqw->fetch(MYSQL_ASSOC))
	{
	    
	    /*
	     if ($level == 'enterprise')
	    {
	        if(! isAdminExistInSP($level, $r['securityDomain'], $r, $_SESSION["sp"], $spListArray)) {
	            continue;
	        }
	    } 
	     */
	    
	    /* Filter Admin Data */
	    if(!applyFilterForAdmin($selectAdminN, $r)) {
            continue;
        }
	    
		$users[$r["id"]]["name"] = $r["firstName"] . " " . $r["lastName"];
		$users[$r["id"]]["firstName"] = $r["firstName"];
		$users[$r["id"]]["lastName"] = $r["lastName"];
		$users[$r["id"]]["UserName"] = $r["userName"];
		$users[$r["id"]]["SuperUser"] = $r["superUser"];
		$users[$r["id"]]["Disabled"] = $r["disabled"] == "0" ? "No" : "Yes";
		$users[$r["id"]]["adminType"] = $r["adminType"];
		$users[$r["id"]]["emailAddress"] = $r["emailAddress"];
		$users[$r["id"]]["user_cell_phone_number"] = str_replace("-","",$r["user_cell_phone_number"]);
		$users[$r["id"]]["adminTypeIndex"] = $r["adminTypeIndex"];
		$users[$r["id"]]["securityDomain"] = $r["securityDomain"] == null ? "" : $r["securityDomain"];
		$users[$r["id"]]["sp"] = $r["sp"];
		$users[$r["id"]]["groupId"][] = $r["groupId"];
		$users[$r["id"]]["use_sms_every_login"] = $r["use_sms_every_login"] == "0" ? "No" : "Yes";
		$users[$r["id"]]["use_ldap_authentication"] = $r["use_ldap_authentication"] == "0" ? "No" : "Yes";
		$users[$r["id"]]["failedLogins"] = $r["failedLogins"];
// 		$users[$r["id"]]["lastLogin"] = $r["lastLogin"];
		$users[$r["id"]]["require_sms_to_reset"] = $r["use_sms_on_password_reset"] == "0" ? "No" : "Yes";
		$users[$r["id"]]["lastLogin"] = $r["lastLogin"] == null ? "" : $r["lastLogin"];
		
	}
	
	foreach ($users as $key => $value)
	{
		if ($value["SuperUser"] == "2") {
			continue;
		}
		
		if ($value["SuperUser"] == "1")
		{
			$value['adminType'] = "Super User";
		} 
                
                if ($value["SuperUser"] == "3")
		{
			$value['adminType'] = "Enterprise Admin";
		} 
		
		if(! hasSecurityDomain()) {
		    $value["securityDomain"] = "False";
		}
		 
// 		if ($value["Disabled"] == "1")
// 		{
// 			$disabled = "<span></span>";
// 		}else{
// 			$disabled = "";
// 		}
		
		$value['groupList'] = implode(",", $value['groupId']);
		//$value["modAdminHref"] = $value["UserName"] . " - " . $value["name"];
		$value["modAdminHref"] = $value["UserName"] . "<span>-</span>" . $value["name"];
// 		$value["modAdminHref"] = $value["UserName"] . " - " . $value["name"] . $su . $disabled;
		$adminList[] = $value;
	}
	 
	usort($adminList, 'querySort');
	
	print_r(json_encode($adminList));

	
/*-------------------------------------------------------------------------------------------------------------*/
	function querySort ($x, $y) {
	    return strcasecmp($x['name'], $y['name']);
	}
	
	/*
	 function isAdminExistInSP($level, $securityDomain, $row, $sessionSP, $spListArray) {
	    global $client, $sessionid;
    	    if($row["superUser"] == "1") {
    	        return true;
    	    } else {
    	        $spList = array();
    	        if($securityDomain != "") {
    	            if(isset($spListArray[$securityDomain])) {
    	                $spList = $spListArray[$securityDomain];
    	            } else {
    	                $spList = getSecurityDomainSp($securityDomain, $client, $sessionid);
    	                $spListArray[$securityDomain] = $spList;
    	            }
    	            
    	        } else if($row['sp'] != "") {
    	            $spList[] = $row['sp'];
    	        }
    	        if(in_array($sessionSP, $spList)) {
    	            return true;
    	        }
    	        return false;
    	    }
	}
	 */
	
	function applyFilterForAdmin($selectAdminN, $r) {
	    $adminExist = true;
	    if($selectAdminN != "") {
	        $name = $r["firstName"] . " " . $r["lastName"];
	        //$adminType = $r["superUser"] == "1" ? "Super User" : $r['adminType'];
                if($r["superUser"] == "1"){
                    $adminType = "Super User";
                }else if($r["superUser"] == "3"){
                    $adminType = "Enterprise Admin";
                }else{
                    $adminType = $r['adminType'];
                }
    	     if(
    	         stripos($r["userName"], $selectAdminN) !== false ||
    	         stripos($adminType, $selectAdminN) !== false ||
    	         stripos($r["user_cell_phone_number"], $selectAdminN) !== false ||
    	         stripos($name, $selectAdminN) !== false
    	         
	          ) {
	            $adminExist = true;
	        } else {
	            $adminExist = false;
	        } 
	    }
	    
	    return $adminExist;
	}
	
	function getServiceProviderDomainListRes() {
	    $dns = new domains();
	    $dnslist = $dns->getServiceProviderDomainListRes($_SESSION['sp']);
	    return $dnslist;
	}
	
	function hasSecurityDomain() {
	    global $securityDomainPattern;
	    
	    return $securityDomainPattern != "";
	}
?>
