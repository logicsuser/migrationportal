<!-- User Filters -->
<link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v11">
<!--<script src="/Express/js/bootstrap.min.js"></script>-->
<script src="/Express/js/bootstrap/bootstrap.min.js"></script>
<script src="/Express/js/jquery-1.17.0.validate.js"></script>
<script src="/Express/js/jquery.loadTemplate.min.js"></script>
<script src="userMod/multiDragAndDrop.js"></script>
<script src="userMod/multiDragAndDropList.js"></script>
<!-- admin permission tooltip open  js -->
 <script src="/Express/js/popper.js"></script>
  <!-- end admin -->
	
<style>
.borderUp {
    border-bottom: 1px solid #ccc;
    height: 0 !important;
    margin: 25px 0 !important;
}
.centerDescBtn{text-align: center;
padding-bottom: 30px !important;}
.hideShow{display:none;}
   
  #devicesTitle #usertool4 span.tooltiptextDevices.tooltiptext,#supervisoryTitle #usertool5 span.tooltiptextSupervisory.tooltiptext,#servicesTitle #usertool3 span.tooltiptextServices.tooltiptext{
     bottom: 0% !important;
    top: inherit !important;
 
 }
  
 #groupTitleMod span#spanTooltipGroup:hover{
    visibility: visible;
}
 
 
#usersTitle #usertool1 span.tooltiptextDevices.tooltiptext, #usertool4 span.tooltiptextSupervisory.tooltiptext { 

    bottom: 0% !important;
    top: inherit !important;
}


 #enterprise_available_user1 li.selected, #enterprise_assign_user1 li.selected, #availableUserServiceToAssign li.selected, #assignedUserService li.selected,#sortable_3 li.selected, #sortable_4 li.selected,#sortable1 li.selected, #sortable2 li.selected{
    background:#ffb200;
}

#enterprise_available_user1 li:hover, #enterprise_assign_user1 li:hover , #availableUserServiceToAssign li:hover, #assignedUserService li:hover,#sortable_3 li:hover, #sortable_4 li:hover,#sortable1 li:hover, #sortable2 li:hover{
    background:#ffb200;
    color:white;
}
#sortable1, #sortable2, #sortable_1, #sortable_2 {
    padding-top: 25px;
}


#enterprise_available_user1 , #enterprise_assign_user1,#availableUserServiceToAssign, #assignedUserService,#sortable1, #sortable2{background: #fff;}

#enterprise_available_user1, #enterprise_assign_user1, #availableUserServiceToAssign li, #assignedUserService li,#sortable_3 li, #sortable_4 li,#sortable1 li, #sortable2 li{
	width: 100%;
	cursor:pointer;
	font-size:12px;
	font-weight:bold;
	text-align:center;
	padding:4px 4px 4px 3px;
	margin:0 0 7px -1px;
}



#sortable1 li:first-child, #sortable2 li:first-child, #sortable_1 li:first-child, #sortable_2 li:first-child{margin-top: 0px;}

input.disabled {
	background: #F6F6F6;
}

input#delBut:hover{background:#ac5f5d !important;}
</style>

 <style>
  #spanTooltipUser,#spanTooltipGroup,#spanTooltipservicesCategory,#spanTooltipDevices,#spanTooltipSupervisory{
    margin-left: -1080px !important;
    margin-top: -43px;
}

 #InventoryTitleAdd #usertool7 span.tooltiptextGroups.tooltiptext {
    margin-left: -1080px !important;
    margin-top: -43px;
}


span#AdministratorsTooltip, span#inventoryTooltip,span#groupEntToolTip,span#entPermModToolTip {
    bottom: 45% !important;
    top: inherit;
    margin-left: -375px;;
    /* margin-left: -386px; */
}

 .tooltiptext > div {
            line-height: 22px;
            margin-left: 15px;
}
  </style>
<?php

        $whereCndtn = "";
        require_once("/var/www/html/Express/config.php");
        
        if(isset($_POST['superUser']) && $_POST['superUser']=="0"){            
            $sel  = "SELECT p.*,fp.permissionId, u.id, u.userName, u.firstName, u.lastName, u.emailAddress, u.superUser, u.disabled, u.adminType, u.bsAuthentication,u.clusterName,u.user_cell_phone_number, u.use_sms_every_login, u.use_sms_on_password_reset, u.use_ldap_authentication";
            $sel .= " from users u left join permissions p on u.id=p.userId  LEFT JOIN featuresPermissions fp on fp.userId=u.id where u.id='" . $_POST["userId"] . "' ";
        }
        
        if(isset($_POST['superUser']) && $_POST['superUser']=="1"){            
            $sel  = "SELECT p.*, u.id, u.userName, u.firstName, u.lastName, u.emailAddress, u.superUser, u.disabled, u.adminType, u.bsAuthentication, u.user_cell_phone_number, u.use_sms_every_login, u.use_sms_on_password_reset, u.use_ldap_authentication";
            $sel .= " from users u left join permissions p on u.id=p.userId where u.id='" . $_POST["userId"] . "' ";
        }
        
        if(isset($_POST['superUser']) && $_POST['superUser']=="3"){  
            $sel  = "SELECT p.*, fp.permissionId, fp.modifyFlag, fp.addDeleteFlag, u.id, u.userName, u.firstName, u.lastName, u.emailAddress, u.superUser, u.disabled, u.adminType, u.bsAuthentication,u.clusterName, u.user_cell_phone_number, u.use_sms_every_login, u.use_sms_on_password_reset, u.use_ldap_authentication";
            $sel .= " from users u left join permissions p on u.id=p.userId  LEFT JOIN enterpriseFeaturesPermissions fp on fp.userId=u.id where u.id='" . $_POST["userId"] . "' ";
        }
        
	$qwr = $db->query($sel);
	$a = 0;
        
	while ($r = $qwr->fetch(MYSQL_ASSOC))
	{  
                $_SESSION["adminUsers"]["sp"] = $r["sp"];
		$_SESSION["adminUsers"]["userNameUpdate"] = $r["userName"];
		$_SESSION["adminUsers"]["passwordUpdate"] = "";
		$_SESSION["adminUsers"]["nameUpdate"] = $r["firstName"] . " " . $r["lastName"];
		$_SESSION["adminUsers"]["emailAddressUpdate"] = $r["emailAddress"];
		$_SESSION["adminUsers"]["user_cell_phone_numberUpdate"] = $r["user_cell_phone_number"];
		$_SESSION["adminUsers"]["use_sms_every_loginUpdate"] = $r["use_sms_every_login"];
		$_SESSION["adminUsers"]["use_sms_on_password_resetUpdate"] = $r["use_sms_on_password_reset"];
		$_SESSION["adminUsers"]["superUserUpdate"] = $r["superUser"];
		$_SESSION["adminUsers"]["disabled"] = $r["disabled"];
		$_SESSION["adminUsers"]["bsAuthentication"] = $r["bsAuthentication"];
		$_SESSION["adminUsers"]["securityDomain"] = $r["securityDomain"];
		$_SESSION["adminUsers"]["use_ldap_authentication"] = $r["use_ldap_authentication"];
		$_SESSION["adminUsers"]["checkPermissions"] = $r["adminType"];
                //clusterName
                $_SESSION["adminUsers"]["entUpdateAdministratorClusterId"] = $r["clusterName"];
		//$permissions["groups"][$a] = $r["groupId"];
                
                if($r["superUser"]=="0"){
                    if(!empty($r["groupId"])){
                            if($r["groupId"] == "NULL"){
                                    $permissions["groups"][$a] = "";
                            }else{
                                    $permissions["groups"][$a] = $r["groupId"];
                            }
                    }else{
                            $permissions["groups"][$a] = "";
                    }
                    $a++;
                    if(isset($r["securityDomain"])) {
                        $permissions['securityDomain'] = $r["securityDomain"];
                    }                   
                    $permissions[$r['permissionId']] = 1;                    
                }
                
                if($r["superUser"]=="3"){ 
                    if(!empty($r["sp"])){
                            if($r["sp"] == "NULL"){
                                    $permissions["assignSpList"][$a] = "";
                            }else{
                                    $permissions["assignSpList"][$a] = $r["sp"];
                            }
                    }else{
                            $permissions["assignSpList"][$a] = "";
                    }
                    $a++;
                    if(isset($r["securityDomain"])) {
                        $permissions['securityDomain'] = $r["securityDomain"];
                    }                    
                    $modifyFlag    = $r['modifyFlag'];
                    $addDeleteFlag = $r['addDeleteFlag'];
                    
                    if($r['modifyFlag'] == ""){
                          $modifyFlag = "no";
                    }
                    if($r['addDeleteFlag'] == ""){
                         $addDeleteFlag = "no";
                    }                    
                    $permissions[$r['permissionId']] = "1_".$modifyFlag."_".$addDeleteFlag;                    
                }

	}	
        
        $clusterSupport  =  (isset($bwClusters) && $bwClusters == "true") && $enableClusters == "true" ? true : false;
        if($clusterSupport){            
            $selectedClusterName = $_SESSION["adminUsers"]["entUpdateAdministratorClusterId"];
            require_once("/var/www/lib/broadsoft/adminPortal/loginClusterBise.php");
            $sessionid = $sessionidForSelCluster;
            $client    = $clientForSelCluster;
            require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
            require_once ("/var/www/lib/broadsoft/adminPortal/domains/domains.php");
            require_once("/var/www/lib/broadsoft/adminPortal/getServiceProvidersClusterBise.php");
            
            //Code added @ 11 June 2019
            require_once("/var/www/lib/broadsoft/adminPortal/util/ConfigUtil.php");
            $bwAppServerForCluster = appsServer;    
            $configUtilObj = new ConfigUtil($db, $bwAppServerForCluster);
            $clusterList = $configUtilObj->getClustersList();
            //End code
            
        }
        else{            
            require_once("/var/www/lib/broadsoft/login.php");
            require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
            require_once ("/var/www/lib/broadsoft/adminPortal/domains/domains.php");
            require_once("/var/www/lib/broadsoft/adminPortal/getServiceProviders.php");
        }	
        /*if($_SESSION["adminUsers"]["superUserUpdate"] != "1"){
            $selectedClusterName = $_SESSION["adminUsers"]["entUpdateAdministratorClusterId"];
            require_once("/var/www/lib/broadsoft/adminPortal/loginClusterBise.php");
            $sessionid = $sessionidForSelCluster;
            $client    = $clientForSelCluster;
            require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
            require_once ("/var/www/lib/broadsoft/adminPortal/domains/domains.php");
            require_once("/var/www/lib/broadsoft/adminPortal/getServiceProvidersClusterBise.php");
        }
        if($_SESSION["adminUsers"]["superUserUpdate"] == "1"){
            require_once("/var/www/lib/broadsoft/login.php");
            require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
            require_once ("/var/www/lib/broadsoft/adminPortal/domains/domains.php");
            require_once("/var/www/lib/broadsoft/adminPortal/getServiceProviders.php");
        }
         */     
	checkLogin();
	
	
        
        
        
	function hasSecurityDomain() {
	    global $securityDomainPattern;

        if ($securityDomainPattern == "") {
            return false;
        }
        return $_SESSION["superUser"] != "0" && $_SESSION["adminUsers"]["superUserUpdate"] == "0";
    }
    
        function hasSecurityDomainEnterpriseAdmin() {
	    global $securityDomainPattern;

        if ($securityDomainPattern == "") {
            return false;
        }
        return $_SESSION["superUser"] != "3" && $_SESSION["adminUsers"]["superUserUpdate"] == "3";
    }

	function getSecurityDomains() {
	    global $systemDomains, $permissions, $enterprisePesmission, $securityDomainPattern;
        
	    $securityDomain = $permissions["securityDomain"];
	    $str = "<option value=\"\"";
	    $str .= $securityDomain == "" ? " selected" : "";
	    $str .= ">None</option>";

	    foreach ($systemDomains as $domain) {
            //if (strpos($domain, $securityDomainPattern) !== false ) {
                $str .= "<option value=\"" . $domain . "\"";
                $str .= $domain == $securityDomain ? " selected" : "";
                $str .= ">" . $domain . "</option>";
            //}
        }
        return $str;
    }
        
    function getServiceProvidersList($selectedSp, $sps)
    {
    	
    	
    	if (isset($_SESSION["spList"]) && count($_SESSION["spList"] > 1)) {
    		$serviceProviders = $_SESSION["spList"];
    	} else {
    		$serviceProviders = $sps;
    	}
    	
    	$str = "<option value=\"\"></option>";
    	foreach ($serviceProviders as $v) {
    		$str .= "<option value=\"" . $v . "\"";
    		if($selectedSp == $v){
    			$str .= " selected";
    		}
    		$str .= ">" . $v . "</option>";
    	}
    	return $str;
    }
    

    function createPermissionsRadioButtons() {
	    global $db;
        
	    $query = "select adminTypeIndex from users where userName='" . $_SESSION["adminUsers"]["userNameUpdate"] . "'";
	    $result = $db->query($query);
	    while ($row = $result->fetch()) {
	        $adminType = $row["adminTypeIndex"];
	    }
	    
	    $permissionTypes = array();
	    $query = "select adminTypeIndex, name from adminTypesLookup";
        $result = $db->query($query);
        $i = 0;
        while ($row = $result->fetch()) {
            $permissionTypes[$i]['adminTypeIndex'] = $row["adminTypeIndex"];
            $permissionTypes[$i]['name'] = $row["name"];
            $i++;
        }

        //<div class="diaPN diaP3" style="width: 2%"><input type="radio" name="checkPermissions" id="checkPermissionsDistrictIT" value="districtIT"
        //onclick="updatePermissions(this)"></div>
        //<div class="diaPN diaP3"><label style="margin-left: 4px; margin-right: 16px; margin-top: 4px">District IT</label></div>

        $html = "";
        $options = "";
        if(count($permissionTypes) > 0) {
            for ($i = 0; $i < count($permissionTypes); $i++) {
                $adminTypeIndex = $permissionTypes[$i]['adminTypeIndex'];
                $type = $permissionTypes[$i]['name'];
                
                $selected = $adminType == $adminTypeIndex ? "selected" : "";
                $options .= "<option value='$adminTypeIndex' $selected>$type</option>";
            }
        } else {
            $options .= "<option value='Admin'>Admin</option>";
        }
        
        return $options;
    }

    require_once("/var/www/lib/broadsoft/adminPortal/getSystemDomains.php");

	$systemConfig = new DBSimpleReader($db, "systemConfig");
	$ldapAuthentication = $systemConfig->get("ldapAuthentication");

        
	function checkPermissionExist($db, $userId){
		
		$num_rows = 0;
		//check the user row exist in permissions table
		$checkPermissionsQuery = "SELECT * from permissions p where p.userId='" . $userId. "'";
		$checkPermissionsQueryResult = $db->query($checkPermissionsQuery);
		$rows = $checkPermissionsQueryResult->fetchAll();
		if(count($rows) > 0){
			$num_rows = count($rows);
		}
		return $num_rows;
	}
        
        function getPermissionsFromPermissionsCategories($db) {
	  
	    $fetchPermissions = "SELECT * from permissionsCategories";
	    $fetchPermissionsQueryResult = $db->query($fetchPermissions);
	     
	    $permissionsData = array();
	    while ($fetchPermissionsQueryRow = $fetchPermissionsQueryResult->fetch(MYSQL_ASSOC)) {
	        if($fetchPermissionsQueryRow['category'] == 'Users') {
	            $permissionsData['Users'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'], 
	                                               'description' => $fetchPermissionsQueryRow['description']);	            
	        } else if($fetchPermissionsQueryRow['category'] == 'Supervisory') {
	            $permissionsData['Supervisory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
	                                                       'description' => $fetchPermissionsQueryRow['description']);
	        } else if($fetchPermissionsQueryRow['category'] == 'Services') {
	            $permissionsData['Services'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
	                                                   'description' => $fetchPermissionsQueryRow['description']);
	        } else if($fetchPermissionsQueryRow['category'] == 'Groups') {
	            $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
	                                                 'description' => $fetchPermissionsQueryRow['description']);
	        } else if($fetchPermissionsQueryRow['category'] == 'Devices') {
	            $permissionsData['Devices'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
	                                                  'description' => $fetchPermissionsQueryRow['description']);
	        }
	    }
	    
	    foreach ($permissionsData as $key => $row) {
	        $order = array();
	        foreach ($row as $key1 => $row1) {
	            $order[$key1] = $row1['description'];
	        }
	        
	        array_multisort($order, SORT_ASC, $row);
	        $permissionsData[$key] = $row;
	    }
	    
	    return $permissionsData;
	}
        
        //Code added @ 18 Feb 2019
        function getEnterprisePermissionsFromPermissionsCategories($db) {
    
            $fetchPermissions = "SELECT * from enterprisePermissionsCategories";
            $fetchPermissionsQueryResult = $db->query($fetchPermissions);

            $permissionsData = array();
            while ($fetchPermissionsQueryRow = $fetchPermissionsQueryResult->fetch(MYSQL_ASSOC)) 
            {
                if($fetchPermissionsQueryRow['category'] == 'Group') 
                {
                    $permissionsData['Group'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                        'description' => $fetchPermissionsQueryRow['description']);
                } 
                else if($fetchPermissionsQueryRow['category'] == 'Inventory') 
                {
                    $permissionsData['Inventory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                        'description' => $fetchPermissionsQueryRow['description']);
                }
                else if($fetchPermissionsQueryRow['category'] == 'Administrators') 
                {
                    $permissionsData['Administrators'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                        'description' => $fetchPermissionsQueryRow['description']);
                }
                else if($fetchPermissionsQueryRow['category'] == 'Enterprise') 
                {
                    $permissionsData['Enterprise'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                        'description' => $fetchPermissionsQueryRow['description']);
                }
            }

            foreach ($permissionsData as $key => $row) {
                $order = array();
                foreach ($row as $key1 => $row1) {
                    $order[$key1] = $row1['description'];
                }

                array_multisort($order, SORT_ASC, $row);
                $permissionsData[$key] = $row;
            }

            return $permissionsData;
        }
        //End code
	
	function getDefaultPermissionAdmiNType($db, $adminType){
		$permissionsNew = array();
		

// 		$fetchPermissionDefault = "SELECT `basicUser`, `resetPasswords`, `deleteUsers`, `changeDevice`, `changeHotel`, `changeForward`, `changeblf`, `changeSCA`, `modhuntgroup`, `addusers`, `modifyUsers`, `modcallCenter`, `groupWideUserModify`, `aamod`, `announcements`, `modCcd`, `modschedule`, `viewcdrs`, `viewusers`, `viewnums`, `viewChangeLog`, `adminUsers`, `vdmLight`, `modifyGroup`, `callPickupGroup`, `vdmAdvanced`, `scaOnlyDevice`, `siteReports`, `sasTest`, `expressSheets`, `groupBasicInfo`, `groupDomains`, `groupServices`, `groupDN`, `groupPolicies`, `groupNCOS`, `groupVoicePortal`, `voiceManagement`, `clearMACAddress` FROM `permissionsSets` where adminType = '" . $adminType . "'";
        
		$fetchPermissionDefault = "select dap.* from defaultAdminPermissions dap INNER JOIN adminTypesLookup atl on atl.`adminTypeIndex` = dap.`adminTypeIndex` where atl.name='". $adminType."'";
		$fetchPermissionDefaultQueryResult = $db->query($fetchPermissionDefault);
		
		while ($fetchPermissionDefaultQueryRow = $fetchPermissionDefaultQueryResult->fetch()){
		    $permissionsNew[$fetchPermissionDefaultQueryRow['permissionId']] = 1;
		    
// 			$permissionsNew['basicUser'] = $fetchPermissionDefaultQueryRow['basicUser'];
// 			$permissionsNew['resetPasswords'] = $fetchPermissionDefaultQueryRow['resetPasswords'];
// 			$permissionsNew['deleteUsers'] = $fetchPermissionDefaultQueryRow['deleteUsers'];
// 			$permissionsNew['changeDevice'] = $fetchPermissionDefaultQueryRow['changeDevice'];
// 			$permissionsNew['clearMACAddress'] = $fetchPermissionDefaultQueryRow['clearMACAddress'];
// 			$permissionsNew['voiceManagement'] = $fetchPermissionDefaultQueryRow['voiceManagement'];
// 			$permissionsNew['changeHotel'] = $fetchPermissionDefaultQueryRow['changeHotel'];
// 			$permissionsNew['changeForward'] = $fetchPermissionDefaultQueryRow['changeForward'];
// 			$permissionsNew['changeblf'] = $fetchPermissionDefaultQueryRow['changeblf'];
// 			$permissionsNew['changeSCA'] = $fetchPermissionDefaultQueryRow['changeSCA'];
// 			$permissionsNew['modhuntgroup'] = $fetchPermissionDefaultQueryRow['modhuntgroup'];
// 			$permissionsNew['addusers'] = $fetchPermissionDefaultQueryRow['addusers'];
// 			$permissionsNew['modifyUsers'] = $fetchPermissionDefaultQueryRow['modifyUsers']; 
// 			$permissionsNew['modcallCenter'] = $fetchPermissionDefaultQueryRow['addumodcallCentersers'];
// 			$permissionsNew['groupWideUserModify'] = $fetchPermissionDefaultQueryRow['groupWideUserModify'];
// 			$permissionsNew['aamod'] = $fetchPermissionDefaultQueryRow['aamod'];
// 			$permissionsNew['announcements'] = $fetchPermissionDefaultQueryRow['announcements'];
// 			$permissionsNew['modCcd'] = $fetchPermissionDefaultQueryRow['modCcd'];
// 			$permissionsNew['modschedule'] = $fetchPermissionDefaultQueryRow['modschedule'];
// 			$permissionsNew['viewcdrs'] = $fetchPermissionDefaultQueryRow['viewcdrs'];
// 			$permissionsNew['viewusers'] = $fetchPermissionDefaultQueryRow['viewusers'];
// 			$permissionsNew['viewnums'] = $fetchPermissionDefaultQueryRow['viewnums'];
// 			$permissionsNew['viewChangeLog'] = $fetchPermissionDefaultQueryRow['viewChangeLog'];
// 			$permissionsNew['adminUsers'] = $fetchPermissionDefaultQueryRow['adminUsers'];
// 			$permissionsNew['vdmLight'] = $fetchPermissionDefaultQueryRow['vdmLight'];
// 			$permissionsNew['modifyGroup'] = $fetchPermissionDefaultQueryRow['modifyGroup'];
// 			$permissionsNew['callPickupGroup'] = $fetchPermissionDefaultQueryRow['callPickupGroup'];
// 			$permissionsNew['vdmAdvanced'] = $fetchPermissionDefaultQueryRow['vdmAdvanced'];
// 			$permissionsNew['scaOnlyDevice'] = $fetchPermissionDefaultQueryRow['scaOnlyDevice'];
// 			$permissionsNew['siteReports'] = $fetchPermissionDefaultQueryRow['siteReports'];
// 			$permissionsNew['sasTest'] = $fetchPermissionDefaultQueryRow['sasTest'];
// 			$permissionsNew['expressSheets'] = $fetchPermissionDefaultQueryRow['expressSheets'];
// 			$permissionsNew['groupBasicInfo'] = $fetchPermissionDefaultQueryRow['groupBasicInfo'];
// 			$permissionsNew['groupDomains'] = $fetchPermissionDefaultQueryRow['groupDomains'];
// 			$permissionsNew['groupServices'] = $fetchPermissionDefaultQueryRow['groupServices'];
// 			$permissionsNew['groupDN'] = $fetchPermissionDefaultQueryRow['groupDN'];
// 			$permissionsNew['groupPolicies'] = $fetchPermissionDefaultQueryRow['groupPolicies'];
// 			$permissionsNew['groupNCOS'] = $fetchPermissionDefaultQueryRow['groupNCOS'];
// 			$permissionsNew['groupVoicePortal'] = $fetchPermissionDefaultQueryRow['groupVoicePortal'];
		}
		return $permissionsNew;
	}
        
        function createPermissionCheckboxes($perVal, $permissions, $i) {
	    $html = "";
	    $openAccordian = 0;
	    if($i % 2 == 0) {
	        //$html .= "<tr>";
	    }
	    
	    if (count($permissions) > 0 && isset($permissions[$perVal["permissionId"]]) && $permissions[$perVal["permissionId"]] == "1")
	    {
	        $_SESSION["adminUsers"]["permsUpdate"][$perVal["permissionId"]] = $permissions[$perVal["permissionId"]];
	        $chk = "checked=\"CHECKED\"";
	        $openAccordian = 1;
	    }
	    else
	    {
	        $_SESSION["adminUsers"]["permsUpdate"][$perVal["permissionId"]] = "0";
	        $chk = "";
	    }
	    
	    $html .= "<div class=\"col-md-6\">";
	    $html .= "<input type=\"hidden\" name=\"permsUpdate[" . $perVal["permissionId"] . "]\" value=\"0\">";
	    $html .= "<input style=\"\" type=\"checkbox\" id=\"permsUpdate[" . $perVal["permissionId"] . "]\" " . $chk . " name=\"permsUpdate[" . $perVal["permissionId"] . "]\" value=\"1\"><label for=\"permsUpdate[" . $perVal["permissionId"] . "]\" " . $chk . "><span></span></label>";
	  
	    $html .= "<label class=\"labelText\" for=\"permsUpdate[" . $perVal["permissionId"] . "]\">" . $perVal["description"] . "</label>";
		$html .= "</div>";
	    
	    if($i % 2 != 0) {
	        //$html .= "</tr>";
	    }
	    
	    $returnData = array("html" => $html, "openAccordian" => $openAccordian);
	    return $returnData;
	}
        
        
        function createEnterprisePermissionCheckboxes($perVal, $permissions, $i) {
            $html = "";
	    $openAccordian = 0;
            //echo "<br />i - $i";
            if($i % 2 == 0) {
	        //$html .= "<tr>";
	    }
            	    
	    if (count($permissions) > 0 && isset($permissions[$perVal["permissionId"]]) && strpos($permissions[$perVal["permissionId"]], "1") !== false)
	    {
                $permissionArr           = explode("_", $permissions[$perVal["permissionId"]]);
                $permissionIdVal         = $permissionArr[0];
                $permissionIdModFlag     = $permissionArr[1];
                $permissionIdAddDelFlag  = $permissionArr[2];
                
                if($permissionIdModFlag=="yes")
                    $modCheck = "checked=\"CHECKED\"";
                
                if($permissionIdAddDelFlag=="yes")
                    $addDelCheck = "checked=\"CHECKED\"";
                
	        $_SESSION["adminUsers"]["entpermsUpdate"][$perVal["permissionId"]] = $permissionIdVal;
	        $chk = "checked=\"CHECKED\"";
	        $openAccordian = 1;
	    }
	    else
	    {
	        $_SESSION["adminUsers"]["entpermsUpdate"][$perVal["permissionId"]] = "0";
	        $chk = "";
	    }
            /*checkbox click checked if Modify/Add click */
            
            $permissionName = $perVal['permissionId'];
            $clsName        = "$permissionName";
            /*end code */
            
            $cssStyle = "style='background-color: #D5D5D5; padding:5px 0px; margin-bottom: 2px;'";
            if($i%2 == 0){
                $cssStyle = "style='background-color: #DEDEDE; padding:5px 0px; margin-bottom: 2px;'";
            }
	    $html .= "<div class=\"col-md-12\" $cssStyle>";
            
	    $html .= "<div class=\"col-md-6\" style='float: left;'>";
	    $html .= "<input type=\"hidden\" name=\"entpermsUpdate[" . $perVal["permissionId"] . "]\" value=\"0\">";
	    $html .= "<input style=\"\" type=\"checkbox\" onclick='javascript: unCheckPermission(\"$clsName\");' class='permissionChecked_$permissionName' id=\"entpermsUpdate[" . $perVal["permissionId"] . "]\" " . $chk . " name=\"entpermsUpdate[" . $perVal["permissionId"] . "]\" value=\"1\"><label for=\"entpermsUpdate[" . $perVal["permissionId"] . "]\" " . $chk . "><span></span></label>";
            $html .= "<label class=\"labelText\" for=\"entpermsUpdate[" . $perVal["permissionId"] . "]\">" . $perVal["description"] . "</label>";
            $html .= "</div>";
                        $className = trim("permissionChecked_$permissionName");
            if(isset($_SESSION["permissionAction"]) && $_SESSION["permissionAction"][$perVal["permissionId"]]['modify']=="yes"){
                $html .= "<div class=\"col-md-3\" style='float: left;'>";
                $html .= "<input type=\"hidden\" name=\"entpermsUpdateMod[" . $perVal["permissionId"] . "]\" value=\"0\">";
                $html .= "<input style=\"\" type=\"checkbox\" onclick='javascript: checkPermissionCheck(\"$className\");' class='entpermsMod_$permissionName' id=\"entpermsUpdateMod[" . $perVal["permissionId"] . "]\" " . $modCheck . " name=\"entpermsUpdateMod[" . $perVal["permissionId"] . "]\" value=\"1\"><label for=\"entpermsUpdateMod[" . $perVal["permissionId"] . "]\" " . $chk . "><span></span></label>";
                $html .= "<label class=\"labelText\" for=\"entpermsUpdateMod[" . $perVal["permissionId"] . "]\">Modify</label>";
                $html .= "</div>";
            }
            
            if(isset($_SESSION["permissionAction"]) && $_SESSION["permissionAction"][$perVal["permissionId"]]['addDelete']=="yes"){
                $html .= "<div class=\"col-md-4\" style='float: left;'>";
                $html .= "<input type=\"hidden\" name=\"entpermsUpdateAddDel[" . $perVal["permissionId"] . "]\" value=\"0\">";
                $html .= "<input style=\"\" type=\"checkbox\" onclick='javascript: checkPermissionCheck(\"$className\");' class='entpermsAddDel_$permissionName' id=\"entpermsUpdateAddDel[" . $perVal["permissionId"] . "]\" " . $addDelCheck . " name=\"entpermsUpdateAddDel[" . $perVal["permissionId"] . "]\" value=\"1\"><label for=\"entpermsUpdateAddDel[" . $perVal["permissionId"] . "]\" " . $chk . "><span></span></label>";
                $html .= "<label class=\"labelText\" for=\"entpermsUpdateAddDel[" . $perVal["permissionId"] . "]\">Add/Delete</label>";
                $html .= "</div>";
            }
	    $html .= "</div>";
	    if($i % 2 != 0) {
	        //$html .= "</tr>";
	    }
	    
	    $returnData = array("html" => $html, "openAccordian" => $openAccordian);
	    return $returnData;
	} 
        
        ?>
<script>
    /* Tooltip show/hide on hover & click */

function tooltipShowHideHover(element){
	var dataId = element.children('div').attr('id');
     var isCollapsed= element.find('h4:first').find('a:first').hasClass("collapsed");
     if(isCollapsed) {
    	$('#'+dataId).removeClass("hideShow");
     } else  {
    	 $('#'+dataId).addClass("hideShow");
     }
 };
 function tooltipShowHideClick(element){
	 var dataId = element.children('div').attr('id');
     var isCollapsed= element.find('h4:first').find('a:first').hasClass("collapsed");
     if(isCollapsed) {
    	 $('#'+dataId).addClass("hideShow");
     } else  {
    	 
    	 $('#'+dataId).removeClass("hideShow");
     	}
	 };
	 
 $(".panel-heading").hover(function(){
	 var element = $(this);
	 tooltipShowHideHover(element);
 });
 $(".panel-heading").click(function(){
	 var element = $(this);
	 tooltipShowHideClick(element);
 });

   
    $(function()
	{	  
                        
                        //Code added @ 27 Feb 2019
                        $("#enterprise_available_user1, #enterprise_assign_user1").multisortableList({
			placeholder: "ui-state-highlight",
                        connectWith: "#enterprise_available_user1, #enterprise_assign_user1",
			cursor: "crosshair",
			update: function(event, ui)
			{
				var monUsers = "";
				var order = $("#enterprise_assign_user1").sortable("toArray");
				for (i = 0; i < order.length; i++)
				{
					monUsers += order[i] + ";";
				}
				$("#enterpriseAssignUserUpdate").val(monUsers);
			},
                        stop : function(){
                                            
						getArrayFromLi('#enterprise_available_user1');
						getArrayFromLi('#enterprise_assign_user1');
                                                
					}
		       }).disableSelection();
       
                 var insensitive = function(s1, s2) {
                            var s1lower = s1.toLowerCase();
                            var s2lower = s2.toLowerCase();
                            return s1lower > s2lower? 1 : (s1lower < s2lower? -1 : 0);
                   }

                   //make new html for the ul tag for ordering the li data
                   var getArrayFromLi = function(ulId){

                           var liArray = [];
                           var liNewArrayUsers = [];
                           var liStr = "";

                           $(ulId+" li").each(function(){
                                   var el = $(this);
                                   liArray.push(el.text());
                           });

                           var liArray = getUnique(liArray);
                           var liNewArray = liArray.sort(insensitive);
                           console.log(liNewArray);                           
                           $(ulId).html('');
                           for (i = 0; i < liNewArray.length; i++)
                           {
                                   liStr += '<li class="ui-state-default" id="'+liNewArray[i]+'">'+liNewArray[i]+'</li>';
                           }
                           $(ulId).html(liStr);
                           /*alert('liStr - '+liStr);
                           alert('liNewArray - '+liNewArray);
                           alert('liNewArrayUsers - '+liNewArrayUsers);
                           console.log(liNewArrayUsers);*/
                   }
                   
                   function getUnique(array){
                        var uniqueArray = [];

                        // Loop through array values
                        for(i=0; i < array.length; i++){
                            if(uniqueArray.indexOf(array[i]) === -1) {
                                uniqueArray.push(array[i]);
                            }
                        }
                        return uniqueArray;
                    }
        
        var bootstrapButton = $.fn.button.noConflict()
		$.fn.bootstrapBtn = bootstrapButton;
		
		$("#endUserId").html("<?php echo $_SESSION["adminUsers"]["userNameUpdate"]; ?>");
		
// 		$("#spWithSecurityDomain").change(function(event)
// 		{
// 			var originalEvent = event.originalEvent === undefined ? false : true; /* Difference Between A Human Click And A Trigger Click */
// 			$("#securityDomainOnMod").empty();
// 			var sp = $(this).val();
			//var securityDomain = "<?php //echo $permissions["securityDomain"]; ?>";
// 			$.ajax({
// 				type: "POST",
// 				url: "adminUsersEnterprise/getAllGroups.php",
// 				data: { action: 'getSpSecurityDomainList', getSP: sp},
// 				success: function(result)
// 				{
// 					var result = JSON.parse(result);
// 					var options = "";
// 					if(result.Error == "") {
// 						result = result.Success.domain;
// 						options += "<option value='' selected=''>None</option>";
// 						$.each(result, function(index, value){
// 							if(! originalEvent) {
// 								var sel = securityDomain == value ? 'selected' : '';								
// 							}
// 							options += "<option value='"+value + "' " +sel +">" +value+ "</option>";
// 						});
// 					}
// 					$("#securityDomainOnMod").html(options);
// 				}
// 			});
			 
// 		});

		//$("#dialogUpdate").dialog($.extend({}, defaults, {
		$("#dialogUpdate").dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		open: function(event) {
			setDialogDayNightMode($(this));
			$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('completeButton');
			$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
			$('.ui-dialog-buttonpane').find('button:contains("Add/Modify Another Admin")').addClass('subButton');
		},
			buttons: {
				"Complete": function()
				{
					pendingProcess.push("Modify Administrator");
					var formData = $("form#userAdmin").serialize();
					$.ajax({
						type: "POST",
						url: "adminUsersEnterprise/updateAdmin.php",
						data: formData,
						success: function(result)
						{
							if(foundServerConErrorOnProcess(result, "Modify Administrator")) {
								return false;
			                }
							$("#dialogUpdate").dialog("open");
							$(".ui-dialog-titlebar-close", this.parentNode).hide();
							$(".ui-dialog-buttonpane", this.parentNode).show();
							$(":button:contains('Complete')").hide();
							$(":button:contains('Cancel')").hide();
							$(":button:contains('Add/Modify Another Admin')").show();
							$("#dialogUpdate").html(result);
							$("#dialogUpdate").append(returnLinkToEntMainPage);
						}
					});
				},
				"Cancel": function()
				{
					$(this).dialog("close");
				},
				"Add/Modify Another Admin": function()
				{
					$(this).dialog("close");
					$("html, body").animate({ scrollTop: 0 }, 600);
					$("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
					$("#loading").show();
					$(".navMenu").removeClass("active");
					$(this).addClass("active");
					$.ajax({
						type: "POST",
						url: "navigate.php",
						data: { module: 'adminUserEnt' },
						success: function(result)
						{
							$("#loading").hide();
							$("#mainBody").html(result);
						}
					});
				}
				
			}
		});

		$("#subBut").click(function()
		{


			if (($('#use_sms_on_password_resetUpdate').is(':checked') || $('#use_sms_every_loginUpdate').is(':checked')) && $("#user_cell_phone_numberUpdate_field").val() === "") {
				alert("Cell Phone Number is a required field.");
			}else{

			var formData = $("form#userAdmin").serialize();
			$.ajax({
				type: "POST",
				url: "adminUsersEnterprise/checkData.php",
				data: formData,
				success: function(result)
				{
					$("#dialogUpdate").dialog("open");
					if (result.slice(0, 1) == "1")
					{
						$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
					}
					else
					{
						$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
					}
					result = result.slice(1);
					$(":button:contains('Add/Modify Another Admin')").hide();
					$(":button:contains('Complete')").show();
					$(":button:contains('Cancel')").show();
					$("#dialogUpdate").html(result);
				}

			});
		}
		});
		$("#use_sms_every_loginUpdate").click(function () {
            if ($('#use_sms_every_loginUpdate').is(':checked')) {
             $("#user_cell_phone_numberUpdate").text("*");
             $("#user_cell_phone_numberUpdate").attr('class', 'required');
               
            } else if (!$('#use_sms_every_loginUpdate').is(':checked')) {
             $("#user_cell_phone_numberUpdate").attr('class', 'not_required');
             $("#user_cell_phone_numberUpdate").text("");
            }

        });
        $("#use_sms_on_password_resetUpdate").click(function () {
            if ($('#use_sms_on_password_resetUpdate').is(':checked')) {
             $("#user_cell_phone_numberUpdate").text("*");
             $("#user_cell_phone_numberUpdate").attr('class', 'required');
               
            } else if (!$('#use_sms_on_password_resetUpdate').is(':checked')) {
             $("#user_cell_phone_numberUpdate").attr('class', 'not_required');
             $("#user_cell_phone_numberUpdate").text("");
            }

        });
        $("#use_ldap_authentication").click(function () {
            if ($('#use_ldap_authentication').is(':checked')) {
                $("#passwordUpdate").val("");
                $("#passwordUpdate").prop('placeholder', "N/A - LDAP Enabled");
                $("#passwordUpdate").addClass('disabled');
                $("#passwordUpdate").prop('disabled', true);

                /* verify password */
                $("#verifyPassword").val("");
                $("#verifyPassword").prop('placeholder', "N/A - LDAP Enabled");
                $("#verifyPassword").addClass('disabled');
                $("#verifyPassword").prop('disabled', true);
                
            } else {
                $("#passwordUpdate").val("");
                $("#passwordUpdate").prop('placeholder', "");
                $("#passwordUpdate").removeClass('disabled');
                $("#passwordUpdate").prop('disabled', false);

                $("#verifyPassword").val("");
                $("#verifyPassword").prop('placeholder', "");
                $("#verifyPassword").removeClass('disabled');
                $("#verifyPassword").prop('disabled', false);
            }
        });
		$("#delBut").click(function()
		{
			$("#dialogDelete").dialog($.extend({}, defaults, {
				width: 800,
				open: function(event) {
					setDialogDayNightMode($(this));
					$('.ui-dialog-buttonpane').find('button:contains("Delete")').addClass('deleteButton');
					$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
					$('.ui-dialog-buttonpane').find('button:contains("Add/Modify Another Admin")').addClass('subButton');
				},
				buttons: {
					"Delete": function()
					{
						pendingProcess.push("Delete Administrator");
						var formData = $("form#userAdmin").serialize();
						$.ajax({
							type: "POST",
							url: "adminUsersEnterprise/deleteAdmin.php",
							data: formData,
							success: function(result)
							{
								if(foundServerConErrorOnProcess(result, "Delete Administrator")) {
									return false;
				                }
								$("#dialogDelete").dialog("open");
								$(".ui-dialog-titlebar-close", this.parentNode).hide();
								$(".ui-dialog-buttonpane", this.parentNode).show();
								$(":button:contains('Complete')").hide();
								$(":button:contains('Cancel')").hide();
								$(":button:contains('Delete')").hide();
								$(":button:contains('Add/Modify Another Admin')").show();
								$("#dialogDelete").html(result);
								$("#dialogDelete").append(returnLinkToEntMainPage);
							}
						});
					},
					"Cancel": function()
					{
						$(this).dialog("close");
					},
					"Add/Modify Another Admin": function()
					{
						$(this).dialog("close");
						$("html, body").animate({ scrollTop: 0 }, 600);
						$("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
						$("#loading").show();
						$(".navMenu").removeClass("active");
						$(this).addClass("active");
						$.ajax({
							type: "POST",
							url: "navigate.php",
							data: { module: 'adminUserEnt' },
							success: function(result)
							{
								$("#loading").hide();
								$("#mainBody").html(result);
							}
						});
					}
				}
			}));

			$("#dialogDelete").dialog("open");
			$("#dialogDelete").html("<label class=\"labelTextGrey\">Are you sure you would like to delete administrator \"" + $("#nameUpdate").val() + "</label>"+"\"?");
			$(":button:contains('Add/Modify Another Admin')").hide();
			$(":button:contains('Cancel')").show();
			$(":button:contains('Delete')").show();
		});

                $("#sortable1, #sortable2").multisortableList({
			placeholder: "ui-state-highlight",
			connectWith: "#sortable1,#sortable2",
			cursor: "crosshair",
			update: function(event, ui)
			{
				var monUsers = "";
				var order = $("#sortable2").sortable("toArray");
				for (i = 0; i < order.length; i++)
				{
					if(order.length > 0){
						monUsers += order[i] + ";";
					}
				}
				$("#groups_1").val(monUsers);
			},
                         stop : function(){
                            getArrayFromLi('#sortable1');
                            getArrayFromLi('#sortable2');

                        }
		}).disableSelection();

		//select service provider for group permissions (non-superusers only)
		$("#sp").change(function()
		{
			$("#sortable1").empty();
			$("#sortable2").empty();

			var sp = $(this).val();
			$.ajax({
				type: "POST",
				url: "adminUsersEnterprise/getAllGroups.php",
				data: { getSP: sp },
				success: function(result)
				{
					$("#sortable1").append(result);
					$("#sortable2").empty();
					$("#groups_1").val('');
				}
			});
		});

	});

	function defaultPerCheckBox() {
		 $("#defaultPermission").prop("disabled",false);
		 $("#defaultPermission").prop("checked",false);
	}

	function showHideAccordian(result, permissionRadio) {
		 var selectAll = false;
		 if(permissionRadio != "") {
			 selectAll = permissionRadio == "Select All" ? true : false;
		}
		 
         if($.inArray("Users", result) > -1 || selectAll) {
             
				$("#usersTitleMod #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#userPerDiv").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         if($.inArray("Groups", result) > -1 || selectAll) {
         	$("#groupTitleMod #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#groupPerDiv").addClass("in").attr("aria-expanded","true").css("height", "auto");;
         }
         if($.inArray("Devices", result) > -1 || selectAll) {
         	$("#devicesTitleMod #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#devicesPerDiv").addClass("in").attr("aria-expanded","true").css("height", "auto");;
         }
         if($.inArray("Services", result) > -1 || selectAll) {
         	$("#servicesTitleMod #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#servicesPerDiv").addClass("in").attr("aria-expanded","true").css("height", "auto");;
         }
         if($.inArray("Supervisory", result) > -1 || selectAll) {
         	$("#supervisoryTitleMod #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#supervisoryPerDiv").addClass("in").attr("aria-expanded","true").css("height", "auto");;
         }
        }
	
	var permissionRadio = "";
	$("input[name='permissionRadio']").click(function() {
		permissionRadio =  $(this).val();
		showHideAccordian(result = "", permissionRadio);
	});
        
        var entpermissionRadio = "";
	$("input[name='entpermissionRadio']").click(function() {
		entpermissionRadio =  $(this).val();
		showHideAccordian(result = "", entpermissionRadio);
	});
	
	
	
	   //Code added @ 27 Feb 2019
       //Checked Select All if all group permission is checked
        $("#adminPermissions :checkbox").change(function() {
        var names = {};
        $('#adminPermissions :checkbox').each(function() {
            names[$(this).attr('name')] = true;
        });
        var count = 0;
        $.each(names, function() { 
            count++;
        });
        if ($('#adminPermissions :checkbox:checked').length === count) {
            $("#permissionRadioSelectAll").prop("checked",true);
        }
        }).change();
        
        $("[id^=entpermsUpdate]").click(function() {
                    $("input[name='entpermissionRadio']").prop("checked", false);
        });
        
        function enableDefaultPerCheckBox() {
                     $("#defaultPermission").prop("checked",false).prop("disabled", false);
					 $("#defaultPermission").removeClass("opacityDisabled");
        }
        //End code
	
	
	$("[id^=permsUpdate]").click(function() {
		$("input[name='permissionRadio']").prop("checked", false);
		//defaultPerCheckBox();
		enableDefaultPerCheckBox();
	});
	
	
	$("#defaultPermission").click(function() {
		$("input[name='permissionRadio']").prop("checked", false);
		if($(this).prop('checked') == true){
			var adminType = $('#adminType option:selected').val();
			var value = {'value': adminType}; 
			updatePermissions(value);
		} else {
// 			$("[id^=permsUpdate]").prop("checked", false);
		}
	});

	// accordian
	function getAllPermissionscategories(defaultPermission) {
	 	$("input[name='permissionRadio']").prop("checked", false);
		$(".collapsideDiv").removeClass('in').attr("aria-expanded","false");
		$("[id^=filtersStatusTitle]").addClass("collapsed").attr("aria-expanded","false");
		
		$.ajax({
            type: "POST",
            url: "adminUsersEnterprise/getPermissions.php",
            data: { action: 'getAllPermissionscategories', defaultPermission: defaultPermission},
            success: function(result)
            {
            	var result = JSON.parse(result);
            	showHideAccordian(result, permissionRadio = "");
            }
        });
	}
	
         function updateEnterprisePermissions(radio) {
            if (radio.value == "Select All") {
                $("[id^=entperms]").prop("checked", true);
            }
            else if (radio.value == "Clear") {
                $("[id^=entperms]").prop("checked", false);
            }

            else if(radio.value == "") {
                    $("[id^=entperms]").prop("checked", false);
                    $("input[name='entpermissionRadio']").prop("checked", false);
                            return false;
            }
        }
    
	
	function updatePermissions(radio) {
		if (radio.value == "Select All") {
            $("[id^=permsUpdate]").prop("checked", true);
            defaultPerCheckBox();
        } else if (radio.value == "Clear") {
            $("[id^=permsUpdate]").prop("checked", false);
            defaultPerCheckBox();
        } else if($("#defaultPermission").prop('checked') == false){
			$("input[name='permissionRadio']").prop("checked", false);
			return false;
		}
        else {
            $.ajax({
                type: "POST",
                url: "adminUsersEnterprise/getPermissions.php",
                data: { permissionSet: radio.value },
                success: function(result)
                {
                	$("[id^=permsUpdate]").prop("checked", false);
                	 getAllPermissionscategories(result);
                     var values = result.split(",");
                     for (var i = 0; i < values.length; i++) {

                    	 
                    	 var id = "permsUpdate[" + values[i] + "]";
                    	 
                        // var params = values[i].split(":");
                       // var id = "permsUpdate[" + params[0] + "]";
                     document.getElementById(id).checked = true;
                     }
                }
            });
        }
    }

	    
	autoCompleteFunction();
	function  autoCompleteFunction() {
		var autoComplete = new Array();                
                var selectCluster = $("#entUpdateAdministratorClusterId").val();
		$.ajax({
			type: "POST",
			url: "adminUsersEnterprise/getAllGroupsClusterBise.php",
			data: { action: 'getSysSecurityDomainList', clusterName: selectCluster},
			success: function(result)
			{
				var explode = result.split(":");
				for (a = 0; a < explode.length; a++)
				{
					autoComplete[a] = explode[a];
				}
				$("#securityDomain").autocomplete({
					source: autoComplete,
    				 select: function( event , ui ) {
    					 $(this).val($.trim(ui.item.label));
    					 return false;
    		         }
				});
			}
		});
	}
	
        $(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();
	});
        
        
        //Code added @ 19 June 2019        
        $("#entUpdateAdministratorClusterId").change(function()
        {
                var userType = $("#superUserUpdate").val();
                //alert('userType - '+userType);
                if(userType == "3"){
                        $("#enterprise_available_user1").empty();
                        $("#enterprise_assign_user1").empty();
                        $("#securityDomain").val('');
                        

                        var selectCluster = $("#entUpdateAdministratorClusterId").val();	
                        //alert('selectClusterEnterprise - ' + selectCluster);
                        $.ajax({
                                type: "POST",
                                url: "adminUsersEnterprise/getAllEnterprisesClusterBise.php",
                                data: { action: 'getServiceProviderList', clusterName: selectCluster },
                                success: function(result)
                                {
                                        //console.log(result);
                                        $("#enterprise_available_user1").append(result);
                                }
                        });

                        var autoComplete = new Array();
                        $.ajax({
                                type: "POST",
                                url: "adminUsersEnterprise/getAllEnterprisesClusterBise.php",
                                data: { action: 'getSysSecurityDomainList', clusterName: selectCluster},
                                success: function(result)
                                {
                                        //console.log(result);
                                        var explode = result.split(":");
                                        for (a = 0; a < explode.length; a++)
                                        {
                                                autoComplete[a] = explode[a];
                                        }
                                        $("#securityDomain").autocomplete({
                                                source: autoComplete,
                                                select: function( event , ui ) {
                                                         $(this).val($.trim(ui.item.label));
                                                         return false;
                                         }
                                        });
                                }
                        });
                }
                if(userType == "0"){ 
                        $("#sortable1").empty();
                        $("#sortable2").empty();
                        $("#securityDomain").val('');

                        var selectedCluster = $('#entUpdateAdministratorClusterId').val();
                        //alert('selectedClusterGroup - '+selectedCluster);            
                        if (selectedCluster != "") {

                            $.ajax({
                                type: "POST",
                                url: "changeConnectionForSelCluster.php",
                                data: {clusterName: selectedCluster, chooseGroup: "true"},
                                success: function (result) {
                                    console.log(result);
                                    $('#sp').html(result);
                                    //alert('Testing123');
                                }
                            });

                            var autoComplete = new Array();
                            $.ajax({
                                     type: "POST",
                                     url: "adminUsersEnterprise/getAllGroupsClusterBise.php",
                                     data: { action: 'getSysSecurityDomainList', clusterName: selectedCluster},
                                     success: function(result)
                                     {
                                             //console.log(result);
                                             var explode = result.split(":");
                                             for (a = 0; a < explode.length; a++)
                                             {
                                                     autoComplete[a] = explode[a];
                                             }
                                             $("#securityDomain").autocomplete({
                                                     source: autoComplete,
                                                     select: function( event , ui ) {
                                                              $(this).val($.trim(ui.item.label));
                                                              return false;
                                              }
                                             });
                                     }
                             });
                   }
                }
        });
        //End code
        
        
        
        
</script>
<form name="userAdmin" id="userAdmin" method="POST" action="#">
	
		<input type="hidden" name="userId" id="userId" value="<?php echo $_POST["userId"]; ?>">
		<!-- input type="hidden" name="sp" id="sp" value="<?php //echo $_SESSION["adminUsers"]["sp"]; ?>"-->
		<input type="hidden" name="superUserUpdate" id="superUserUpdate" value="<?php echo $_SESSION["adminUsers"]["superUserUpdate"]; ?>">

	
<div class="row">
	<div class="">	
		<div class="">
			<div class="col-md-6">
    			<?php
    			if ($ldapAuthentication == "true") {
    			?>
    			<div class="form-group">
    				<input type="hidden" name="use_ldap_authentication" value="0" />
    				<input type="checkbox" name="use_ldap_authentication" id="use_ldap_authentication" style="width: auto;" value="1" <?php echo $_SESSION["adminUsers"]["use_ldap_authentication"] ? "checked" : ""; ?>>
    				<label for="use_ldap_authentication"><span></span></label>
    				<label class="labelText" for="use_ldap_authentication">LDAP Authentication</label>
    			</div>
    
    				<?php
    			} else {
    				?>
    				<input type="hidden" name="use_ldap_authentication" id="use_ldap_authentication" value="0" />
    			<?php
    			}
    			?>
			</div>
			
			<div class="col-md-6">	
        		<div class="form-group">
        			<input type="hidden" name="disabled" value="0">
        			<input type="checkbox" name="disabled" id="disabled" value="1" <?php echo $_SESSION["adminUsers"]["disabled"] ? "checked" : ""; ?>><label for="disabled"><span></span></label>
        			<label for="disabled" class="labelText labelTextZero">Disable User</label>
        		</div>
    		</div>
    	</div>

	</div>
</div>

<div class="row">
		<div class="">	
    		<div class="col-md-6">
    			<div class="form-group">
    					
    				<label class="labelText" for="userNameUpdate">Username</label> <span class="required">*</span>
    					<?php //if BroadSoft user, prevent modification of username ?>
    					<?php
    					if($_SESSION["adminUsers"]["bsAuthentication"] == "1") 
    					{
    						?>
    						<input type="hidden" name="userNameUpdate" id="userNameUpdate" value="<?php echo $_SESSION["adminUsers"]["userNameUpdate"]; ?>">
    						<?php
    					}
    					?>
					<input type="text" class="form-control" <?php echo ($_SESSION["adminUsers"]["bsAuthentication"] == "1") ? " disabled style='background: #F6F6F6;' " : " name='userNameUpdate' id='userNameUpdate' " ?> value="<?php echo $_SESSION["adminUsers"]["userNameUpdate"]; ?>" size="35" maxlength="32">
    			</div>
    		</div>
			<div class="col-md-6">
    		<div class="form-group">
    		<label class="labelText" for="user_cell_phone_numberUpdate">Cell Phone Number</label><span class="not_required" id="user_cell_phone_numberUpdate"></span>
    		<input class="form-control" type="tel" name="user_cell_phone_numberUpdate" id="user_cell_phone_numberUpdate_field" value="<?php echo $_SESSION["adminUsers"]["user_cell_phone_numberUpdate"]; ?>" size="35" maxlength="65" required>
    		</div>
    	</div>
    		<!--<div class="col-md-6">
    			<div class="form-group">
    				<label class="labelText" for="passwordUpdate">Password</label>
    			<?php //if BroadSoft or LDAP user, prevent modification of password ?>
    			<input class="form-control" <?php //echo ($_SESSION["adminUsers"]["use_ldap_authentication"] || $_SESSION["adminUsers"]["bsAuthentication"] == "1" ) ? "disabled style='background: #F6F6F6;' value='N/A - LDAP Enabled'" : "type='password'" ?> name="passwordUpdate" id="passwordUpdate" size="35">
    			</div>
    		</div>-->

		</div>
</div>
<div class="row">
<div class="">
	<?php if ($isAdminPasswordFieldVisible == "true") { ?>
		<div class="col-sm-6">
    		<div class="form-group">
    			<label class="labelText" for="passwordUpdate">Password</label>
    				<?php //if BroadSoft or LDAP user, prevent modification of password ?>
    				<input <?php echo ($_SESSION["adminUsers"]["use_ldap_authentication"] || $_SESSION["adminUsers"]["bsAuthentication"] == "1" ) ? "disabled placeholder='N/A - LDAP Enabled' class='form-control disabled'" : "class='form-control'" ?>
    					   type="password"
    					   name="passwordUpdate"
    					   id="passwordUpdate"
    					   size="35">					
    		</div>
		</div>
		<!-- 		verify password -->
		<div class="col-sm-6">
			<div class="form-group">
    			<label class="labelText" for="verifyPassword">Verify Password</label>
    				<?php //if BroadSoft or LDAP user, prevent modification of password ?>
    				<input <?php echo ($_SESSION["adminUsers"]["use_ldap_authentication"] || $_SESSION["adminUsers"]["bsAuthentication"] == "1" ) ? "disabled placeholder='N/A - LDAP Enabled' class='form-control disabled'" : "class='form-control'" ?>
    					   type="password"
    					   name="verifyPassword"
    					   id="verifyPassword"
    					   size="35">					
    		</div>
		</div>
			<?php } ?>
		


</div>
</div>
<div class="row">
	<div class="">
    	
		<div class="col-md-6">
			<div class="form-group">
			<label class="labelText" for="nameUpdate">Name</label> <span class="required">*</span>
			<input class="form-control" type="text" name="nameUpdate" id="nameUpdate" value="<?php echo $_SESSION["adminUsers"]["nameUpdate"]; ?>" size="35" maxlength="65">
			</div>
		</div>
			
    	<div class="col-md-6">
			<div class="form-group">
				<label class="labelText" for="emailAddressUpdate">Email Address</label> <span class="required">*</span>
				<input class="form-control" type="text" name="emailAddressUpdate" id="emailAddressUpdate" value="<?php echo $_SESSION["adminUsers"]["emailAddressUpdate"]; ?>" size="40" maxlength="64">
			</div>
		</div>
	</div>
</div>
		
<div class="row ">
	<div class="">
		
		<div class="col-md-6">
    		<div class="form-group">
    		<input type="hidden" name="use_sms_every_loginUpdate" value="0">
    		<div class="marginTopSe">&nbsp;</div>
    		<input type="checkbox" class="form-control" name="use_sms_every_loginUpdate" id="use_sms_every_loginUpdate" value="1" <?php echo $_SESSION["adminUsers"]["use_sms_every_loginUpdate"] ? "checked" : ""; ?>><label for="use_sms_every_loginUpdate"><span></span></label>
    		<label class="labelText" for="use_sms_every_loginUpdate">Use SMS every login</label>
    		</div>
		</div>
		
	<div class="col-md-6">
			<?php
			if ($_SESSION["adminUsers"]["use_ldap_authentication"]) {
				?>
				<input type="hidden" name="use_sms_on_password_resetUpdate" id="use_sms_on_password_resetUpdate" value="<?php echo $_SESSION["adminUsers"]["use_sms_on_password_resetUpdate"] ? "1" : "0"; ?>" />
				<?php
			} else {
				?>
				<div class="form-group">
				<div class="marginTopSe">&nbsp;</div>
				<input type="hidden" name="use_sms_on_password_resetUpdate" value="0">
				<input type="checkbox" class="checkbox-lg" name="use_sms_on_password_resetUpdate" id="use_sms_on_password_resetUpdate" value="1" <?php echo $_SESSION["adminUsers"]["use_sms_on_password_resetUpdate"] ? "checked" : ""; ?>><label for="use_sms_on_password_resetUpdate"><span></span></label>
				<label class="labelText" for="use_sms_on_password_resetUpdate">Require SMS to reset</label>
				</div>
	
				<?php
			}
			?>
                                </div>
	</div>
</div>
<?php if ($_SESSION["adminUsers"]["superUserUpdate"] == "0"){
    
?>
               
 <div class="groupPermissionBlock">
<div class="vertSpacer initialShow ">&nbsp;</div>
		 <?php echo $_SESSION["adminUsers"]["superUserUpdate"] == "0" ? '<div class="vertSpacer initialShow borderUp">&nbsp;</div>':"";?>
<div class="vertSpacer initialShow">&nbsp;</div>
		 
<!-- New code found start-->
	<div class="row">
            <div>
            <?php 
            if ($_SESSION["adminUsers"]["superUserUpdate"] == "0")
            {
            ?>
	            <div class="col-md-6 form-group">
		            <label class="labelText">Admin Type</label><span class="required">*</span>
		            <div class="dropdown-wrap">
			            <select id="adminType" class="form-control" name="checkPermissions" onchange="updatePermissions(this)">
			            	 <?php echo createPermissionsRadioButtons(); ?>
			            </select>
		            </div>
	            </div>
	        <?php } ?>
	        
	        <?php 
	        unset($permissions["queryString"]);
	        unset($permissions["groupId"]);
	        unset($permissions["userName"]);
	        unset($permissions["firstName"]);
	        unset($permissions["lastName"]);
	        unset($permissions["emailAddress"]);
	        unset($permissions["user_cell_phone_number"]);
	        unset($permissions["use_sms_every_login"]);
	        unset($permissions["use_sms_on_password_reset"]);
	        unset($permissions["superUser"]);
	        unset($permissions["disabled"]);
	        unset($permissions["bsAuthentication"]);
	        unset($permissions["id"]);
	        $serP = $permissions["sp"];
	        if(empty($serP)){
	            $serP = $_SESSION["adminUsers"]["sp"];
	        }
	        require_once("/var/www/lib/broadsoft/adminPortal/getAllGroups.php");                                
	        unset($permissions["sp"]);
	        
	        $getAdminDefaultPermissions = getDefaultPermissionAdmiNType($db, $_SESSION["adminUsers"]["checkPermissions"]);
	        $defaultPermissions = "true";
	        $defaultPermissionsDisabled = "disabled";
	        
	        $tempPer = $permissions;
	        unset($tempPer['securityDomain']);
	        unset($tempPer['groups']);
	        $isBorPerEqual = count($tempPer) == count($getAdminDefaultPermissions);
	        if($isBorPerEqual) {
	            foreach($getAdminDefaultPermissions as $perKey => $perVal) {
	                if(!isset($tempPer[$perKey])) {
	                    $defaultPermissions = "false";
	                    $defaultPermissionsDisabled = "";
	                    break;
	                }
	            }
	        } else {
	            $defaultPermissions = "false";
	            $defaultPermissionsDisabled = "";
	        }
	        ?>
	        
	        <?php 
            if ($_SESSION["adminUsers"]["superUserUpdate"] == "0")
            {
                $opacity = "";
                if($defaultPermissionsDisabled == "disabled") {
                    $opacity = "opacity: 1;";
                }
            ?>
	            <div class="col-md-6">
					<div class="form-group">
					    <div class="marginTopSe">&nbsp;</div>
						<input type="checkbox"
						       class="checkbox-lg"
						       id="defaultPermission"
						       style="width: auto;" <?php echo $defaultPermissionsDisabled; ?>
						       value="1" <?php echo $defaultPermissions == "true" ? "checked" : ""; ?>>
						       <label class="defaultPermCheckLabel" style="<?php echo $opacity;?>" for="defaultPermission"><span></span></label>
						       <label class="labelText">Default Permissions</label>
					</div>
				</div>
				
			<?php } ?>	
				
            </div>
	</div>

        <?php 
            $clusterSupport  =  (isset($bwClusters) && $bwClusters == "true") && $enableClusters == "true" ? true : false;
            if($clusterSupport){
        ?> 
            <div class="row" id="">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="labelText">Select Cluster</label>
                        <div class="dropdown-wrap">   
                        <?php                 
                            $clusterIdName  = "entUpdateAdministratorClusterId";
                            $clusterClsName = "form-control";
                            echo $clusterDropdownHtml = $configUtilObj->createClustersDropdownList($clusterIdName, $clusterClsName ,$clusterList,$_SESSION["adminUsers"]["entUpdateAdministratorClusterId"]);
                        ?>                                                    
                        </div>
                    </div>
                </div>
            </div>
        <?php
             }
        ?>

<div class="row" id="securityDomainDiv" style="display: <?php echo (hasSecurityDomain() ? "block" : "none"); ?>">
    <div class="">
        <div class="col-md-6">
            <div class="form-group">
				<label for="securityDomain" class="labelText">Security Domain</label><span class="required">*</span>   
				<div class="">   
				 <input type="text" class="autoFill" name="securityDomain" id="securityDomain" value="<?php echo trim($permissions['securityDomain']); ?>">
				</div>
            </div>
        </div>
    </div>
</div>

			<?php

				//if not logged in as superuser, or editing a superuser, don't display group selection
				if ($_SESSION["superUser"] == "0" or $_SESSION["adminUsers"]["superUserUpdate"] != "0")
				{
					echo "<div style=\"display:none;\">";
				}

				$groupList = "";                                
				if(!empty($permissions["groups"][0])){
					foreach ($permissions["groups"] as $groupArray)
					{
						$groupList .= $groupArray . ";";
					}
				}
				$_SESSION["adminUsers"]["groups_1"] = $groupList;

				//if not editing a superuser, display service provider
				
				if ($_SESSION["adminUsers"]["superUserUpdate"] == "0")
				{
					?>
					
                <div class="row" id="spDiv" style="display: <?php echo (hasSecurityDomain() ? "none" : "block"); ?>">
                     	<div class="">
                            <div class="col-md-6">
                            <div class="form-group">
                                    	<div class="labelText">Enterprise <span class="required">*</span></div>
                                    	<div class="dropdown-wrap"><select name="sp" id="sp"><?php echo getServiceProvidersList($_SESSION["adminUsers"]["sp"], $sps); ?></select>
                			<?php //echo $_SESSION["adminUsers"]["sp"]; ?>
                						</div>
                    	    </div>
                            </div>
                       	</div>
                 </div>
                	<?php
                	}
                ?>
             
           <?php if ($_SESSION["adminUsers"]["superUserUpdate"] == "0")
			{
	       ?>
			<div id="groupDiv" class="row" style="display:<?php echo (hasSecurityDomain() ? "none" : "block"); ?>">
                 <div class="col-md-12">
        		    <div class="form-group">
                        <label class="labelText">Groups <span class="required">*</span></label>
                        <input type="hidden" name="groups_1" id="groups_1" value="<?php echo $groupList; ?>">
                    </div>
            	 </div>
            <div class="">
    	        <div class="col-md-6 availNCurrentGroup">
                        	<div class="form-group">Available Groups</div>
        		</div>
    			<div class="col-md-6 availNCurrentGroup">
                		<div class="form-group">Current Groups</div>
        		</div>
	    	</div>

    
			<div class="row form-group">
				<div class="col-md-12">
					<div class="col-md-6" style="">
					
							<?php                                                        
							if (isset($allGroups))
							{
								?>
								<div class="form-group scrollableListCustom1">
								<ul id="sortable1" class="connectedSortable connectedSortableCustom">
								<?php
								foreach ($allGroups as $v)
								{
									//display groups user does not have permissions for
									if (!in_array($v, $permissions["groups"]))
									{
										?>
										<li class="ui-state-default" id="<?php echo $v; ?>"><?php echo $v; ?></li>

										<?php
									}
								}
								?>
									</ul>
								</div>
							<?php
							}
							?>
						
					
				</div>
					<div class="col-md-6" style="">
					
							<?php
							if (isset($allGroups))
							{
							?>
								<div class="form-group scrollableListCustom2">
									<ul id="sortable2" class="connectedSortable connectedSortableCustom">
							<?php
								foreach ($allGroups as $v)
								{
									//display groups user has permissions for
									if (in_array($v, $permissions["groups"]))
									{
										?>
										<li class="ui-state-highlight" id="<?php echo $v; ?>"><?php echo $v; ?></li>
										<?php
									}
								}
								?>
									</ul>
								</div>
							<?php
							}
							?>
							
					</div>
				</div>
			</div>
	</div>
	<?php 
	}
	?>
			<?php
				//if not logged in as superuser, or editing a superuser, don't display group selection
				if ($_SESSION["superUser"] == "3" or $_SESSION["adminUsers"]["superUserUpdate"] != "3")
				{
					echo "</div>";
				}

				//if editing a superuser, don't display permissions
				if ($_SESSION["adminUsers"]["superUserUpdate"] == "0")
				{
					?>

				<div class="row">
					<div class="col-md-6" style="padding-left: 0;">
						<div class="col-md-6 voiceMsgTxt">
						 <div class="form-group">
						<label class="labelText">Permissions</label><span class="required">*</span>
							<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="At least one permission must be selected."><img src="images/NewIcon/info_icon.png" /></a>
							<!-- span class="required">*</span-->
                					
						</div>
						</div>
						<div class="col-md-6 voiceMsgRBtn">
						<div class="col-md-6 rBtnYes" style="padding-right: 0;">
							<div class="form-group">
							 <input type="radio" name="permissionRadio"	id="permissionRadioSelectAll" class="checkbox-lg" value="Select All" onclick="updatePermissions(this)">
							 <label class="labelText labelTextMargin" for="permissionRadioSelectAll"><span></span>Select All</label>
							</div>
						</div>
			
						 <div class="col-md-6 rBtnNo" style="padding-right: 0;">
							 <div class="form-group">
								<input type="radio" name="permissionRadio" id="permissionRadioClearAll" class="checkbox-lg" value="Clear" onclick="updatePermissions(this)">
								<label class="labelText" for="permissionRadioClearAll"><span></span>Clear All</label>
							</div>
						</div>
						</div>
					</div>
				</div>

					
					
					
					<!-- Permissions start -->

<?php
// print_r($permissions); exit;
$countCheckPermExist = checkPermissionExist($db, $_POST["userId"]);
if($countCheckPermExist == 0){
    //if not than select the default permission sets
    $permissions = getDefaultPermissionAdmiNType($db, $_SESSION["adminUsers"]["checkPermissions"]);    
}
if(isset($license["vdmLite"]) && $license["vdmLite"] =="false"){
    unset($permissions["vdmLight"]);
    unset( $_SESSION["fieldNames"]["vdmLight"]);
    unset($permissions["vdmAdvanced"]);
    unset( $_SESSION["fieldNames"]["vdmAdvanced"]);
    
    unset($permissions["securityDomain"]);
}
$permissionCategories    = getPermissionsFromPermissionsCategories($db);
$entPermissionCategories = getEnterprisePermissionsFromPermissionsCategories($db);

?>

<!-- Users Permissions -->
	<?php
	
	$i = 0;
	$toolTip = "<span class='tooltiptextUsers tooltiptext' id='spanTooltipUser'>";
	$userCategoryPermissions = "<div>";
	$openAccordian = 0;
	foreach($permissionCategories['Users'] as $perKey => $perVal) {
	    $toolTip .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $createPermission = createPermissionCheckboxes($perVal, $permissions, $i);
	    $userCategoryPermissions .= $createPermission["html"];
	    
	    if($openAccordian == 0) {
	        $openAccordian = $createPermission['openAccordian'];
	    }  
	    $i++;
	}
	$userCategoryPermissions .= "</div>";
	$toolTip .= "</span>";
	?>
	<div id="adminPermissions">
		<div class="">&nbsp;</div>
        <div class="row">
           <div class="col-md-12" style="">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="userPerRow">
                		<div class="panel-heading" role="tab" id="usersTitleMod">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width userPerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#userPerDiv" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="userPerDiv">User Permissions
							    </a>
                			</h4>
        <!--         	tool tip -->
        					<div id="usertool1">
        					<?php echo $toolTip; ?>
        					</div>
                		</div>
                		<div id="userPerDiv" class="collapsideDiv panel-collapse collapse <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="usersTitleMod">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $userCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
		
<!-- Groups Permissions -->
<?php 
	//array_multisort( array_column($permissionCategories['Groups'], "description"), SORT_ASC, $permissionCategories['Groups']);
	$i = 0;
	$toolTipGrpup = "<span class='tooltiptextGroups tooltiptext' id ='spanTooltipGroup'>";
	$groupCategoryPermissions = "<div>";
	$openAccordian = 0;
	foreach($permissionCategories['Groups'] as $perKey => $perVal) {
	    $toolTipGrpup .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $createPermission = createPermissionCheckboxes($perVal, $permissions, $i);
	    $groupCategoryPermissions .= $createPermission["html"];
	    if($openAccordian == 0) {
	        $openAccordian = $createPermission['openAccordian'];
	    }
	    $i++;
	}
	$groupCategoryPermissions .= "</div>";
	$toolTipGrpup .= "</span>";
	?>
	
	<div class="row">
           <div class="col-md-12" style="">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="groupPerRow">
                		<div class="panel-heading" role="tab" id="groupTitleMod">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width groupPerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#groupPerDiv" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="groupPerDiv">Group Permissions
                				   </a>
                			</h4>
        <!--   tool tip --><div id="usertool2">
        					<?php echo $toolTipGrpup; ?>
        					</div>
                		</div>
                		<div id="groupPerDiv" class="collapsideDiv panel-collapse collapse <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="groupTitleMod">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $groupCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        
<!-- 		Services Permissions -->
<?php 
	//array_multisort( array_column($permissionCategories['Services'], "description"), SORT_ASC, $permissionCategories['Services']);
	$i = 0;
	$toolTipServ = "<span class='tooltiptextServices tooltiptext' id='spanTooltipservicesCategory'>";
	$servicesCategoryPermissions = "<table border='1' style='width:80%;'>";
	$openAccordian = 0;
	foreach($permissionCategories['Services'] as $perKey => $perVal) {
	    $toolTipServ .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $servicesCategory = createPermissionCheckboxes($perVal, $permissions, $i);
	    
	    $servicesCategoryPermissions .= $servicesCategory["html"];
	    if($openAccordian == 0) {
	        $openAccordian = $servicesCategory['openAccordian'];
	    }
	    $i++;
	}
	$servicesCategoryPermissions .= "</table>";
	$toolTipServ .= "</span>";
	?>
		<div class="row">
           <div class="col-md-12" style="">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="servicesPerRow">
                		<div class="panel-heading" role="tab" id="servicesTitleMod">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width servicePerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#servicesPerDiv" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="servicesPerDiv">Services Permissions
                				   </a>
                			</h4>
        <!--   tool tip --><div id="usertool3">
        					<?php echo $toolTipServ; ?>
        					</div>
                		</div>
                		<div id="servicesPerDiv" class="collapsideDiv panel-collapse collapse <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="servicesTitleMod">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $servicesCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        

<!-- Devices Permissions -->
<?php 
	//array_multisort( array_column($permissionCategories['Devices'], "description"), SORT_ASC, $permissionCategories['Devices']);
	$i = 0;
	$toolTipDevice = "<span class='tooltiptextDevices tooltiptext' id='spanTooltipDevices'>";
	$devicesCategoryPermissions = "<table border='1' style='width:80%;'>";
	$openAccordian = 0;
	foreach($permissionCategories['Devices'] as $perKey => $perVal) {
	    $toolTipDevice .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $devicesCategory = createPermissionCheckboxes($perVal, $permissions, $i);
	    
	    $devicesCategoryPermissions .= $devicesCategory["html"];
	    if($openAccordian == 0) {
	        $openAccordian = $devicesCategory['openAccordian'];
	    }
	    $i++;
	}
	$devicesCategoryPermissions .= "</table>";
	$toolTipDevice .= "</span>";
	?>
		
		<div class="row">
           <div class="col-md-12" style="">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="devicePerRow">
                		<div class="panel-heading" role="tab" id="devicesTitleMod">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width devicePerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#devicesPerDiv" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="devicesPerDiv">Devices Permissions
                				   </a>
                			</h4>
        <!--   tool tip --><div id="usertool4">
        					<?php echo $toolTipDevice; ?>
        					</div>
                		</div>
                		<div id="devicesPerDiv" class="collapsideDiv panel-collapse collapse <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="devicesTitleMod">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $devicesCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        
<!-- Supervisory Permissions -->
<?php 
	//array_multisort( array_column($permissionCategories['Supervisory'], "description"), SORT_ASC, $permissionCategories['Supervisory']);
	$i = 0;
	$toolTipSupervisory = "<span class='tooltiptextSupervisory tooltiptext' id='spanTooltipSupervisory'>";
	$supervisoryCategoryPermissions = "<table border='1' style='width:80%;'>";
	$openAccordian = 0;
	foreach($permissionCategories['Supervisory'] as $perKey => $perVal) {
	    $toolTipSupervisory .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $supervisoryCategory = createPermissionCheckboxes($perVal, $permissions, $i);
	    
	    $supervisoryCategoryPermissions .= $supervisoryCategory["html"];
	    if($openAccordian == 0) {
	        $openAccordian = $supervisoryCategory['openAccordian'];
	    }
	    $i++;
	}
	$supervisoryCategoryPermissions .= "</table>";
	$toolTipSupervisory .= "</span>";
	?>
		
		<div class="row">
           <div class="col-md-12" style="">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="supervisoryPerRow">
                		<div class="panel-heading" role="tab" id="supervisoryTitleMod">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width superPerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#supervisoryPerDiv" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="supervisoryPerDiv">Supervisory Permissions
							    </a>
                			</h4>
        <!--   tool tip -->
							<div id="usertool5">
								<?php echo $toolTipSupervisory; ?>
							</div>
						</div>
                		<div id="supervisoryPerDiv" class="collapsideDiv panel-collapse collapse <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="supervisoryTitleMod">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $supervisoryCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>     	
	
        
<!-- Permissions end -->
									

					<?php
				}
			?>
					
      
<!-- 	assigned permission -->
<?php 

foreach($permissions as $key => $value)
{
    if($key !="groups" && $key !="securityDomain") {
        echo '<input type="hidden" name="assignedPermissions['.$key.']" value="'. $value. '">';
    }
}
?>

  </div>
        
 </div>
<?php } ?>

   <?php if ($_SESSION["adminUsers"]["superUserUpdate"] == "3") {
       
        foreach($permissions as $key => $value)
        {
            if($key !="assignSpList" && $key !="securityDomain") {
                
                echo '<input type="hidden" name="assignedEntPermissions['.$key.']" value="'. $value. '">';
            }
        }
       
       
   ?>
    <div class="entPermissionBlock">
        
        <?php 
            $clusterSupport  =  (isset($bwClusters) && $bwClusters == "true") && $enableClusters == "true" ? true : false;
            if($clusterSupport){
        ?> 
            <div class="row" id="">
            <div class="col-md-6">
                    <div class="form-group">
                        <label class="labelText">Select Cluster</label>
                        <div class="dropdown-wrap">   
                            <?php   
                                $clusterIdName  = "entUpdateAdministratorClusterId";
                                $clusterClsName = "form-control";
                                echo $clusterDropdownHtml = $configUtilObj->createClustersDropdownList($clusterIdName, $clusterClsName ,$clusterList,$_SESSION["adminUsers"]["entUpdateAdministratorClusterId"]);
                            ?>                                                    
                        </div>
                    </div>
            </div>
            </div>
        <?php
           }
        ?>
        
        
        
        
        
        
    <?php echo $_SESSION["adminUsers"]["superUserUpdate"] == "3" ? '<div class="vertSpacer initialShow borderUp">&nbsp;</div>':"";?>
    
    
    <div class="row" id="securityDomainDiv" style="display: <?php echo (hasSecurityDomainEnterpriseAdmin() ? "block" : "none"); ?>">
    <div class="">
        <div class="col-md-6">
            <div class="form-group">
				<label for="securityDomain" class="labelText">Security Domain</label><span class="required">*</span>   
				<div class="">   
				 <input type="text" class="autoFill" name="securityDomain" id="securityDomain" value="<?php echo trim($permissions['securityDomain']); ?>">
				</div>
            </div>
        </div>
    </div>
   </div>
        
        
        <div class="row" style="display: <?php echo (hasSecurityDomainEnterpriseAdmin() ? "none" : "block"); ?>">
        
                            <div class="initialShow">
                                <label class="labelText" for="Selsp"  style="margin-left:15px;">Enterprise</label><span class="required">*</span>
                            </div>
            
                            <div class="">
                                <div class="col-md-6 availNCurrentGroup">
                                        <div class="form-group initialShow">Available Enterprises</div>
                                </div>
                                 <div class="col-md-6 availNCurrentGroup">
                                        <div class="form-group initialShow">Current Enterprises</div>
                                 </div>
                            </div>
            
                            <div class="col-md-6">
                            <?php
                                        $permissions["assignSpList"] = array_unique($permissions["assignSpList"]);
                                        $assignedSPList = implode(";", $permissions["assignSpList"]);
                                        $assignedSPList .=";";
                                        $_SESSION["adminUsers"]["enterpriseAssignUserUpdate"] = $assignedSPList;
                                        if (isset($sps))
                                        {
                                                ?>
                                                <div class="form-group scrollableListCustom1">
                                                <ul id="enterprise_available_user1" class="connectedSortable connectedSortableCustom">
                                                <?php
                                                foreach ($sps as $v)
                                                {
                                                        //display groups user does not have permissions for
                                                        if (!in_array($v, $permissions["assignSpList"]))
                                                        {
                                                                ?>
                                                                <li class="ui-state-default" id="<?php echo $v; ?>"><?php echo $v; ?></li>

                                                                <?php
                                                        }
                                                }
                                                ?>
                                                        </ul>
                                                </div>
                                        <?php
                                        }
                                        ?>                       
                            </div>
            
                            <div class="col-md-6">
                            <?php
                                    if (isset($sps))
                                    {
                                    ?>
                                            <div class="form-group scrollableListCustom2">
                                                    <ul id="enterprise_assign_user1" class="connectedSortable connectedSortableCustom">
                                    <?php
                                            //$assignedSPList = "";
                                            foreach ($sps as $v)
                                            {
                                                    //display groups user has permissions for
                                                    if (in_array($v, $permissions["assignSpList"]))
                                                    {
                                                            //$assignedSPList .= "$val;"; 
                                                            ?>
                                                            <li class="ui-state-highlight" id="<?php echo $v; ?>"><?php echo $v; ?></li>
                                                            <?php
                                                    }
                                            }
                                            
                                            ?>
                                                    </ul>
                                                    <input type="hidden" name="enterpriseAssignUserUpdate" id="enterpriseAssignUserUpdate" value="<?php echo $assignedSPList; ?>" />
                                            </div>
                                    <?php
                                    }
                                    ?>                           
                             
                            </div>
                       
                        
        </div>
    <div class="vertSpacer initialShow borderUp">&nbsp;</div>
        
        <div class="row initialShow">
		<div class="col-md-6" style="padding-left: 0;">
			<div class="col-md-6 voiceMsgTxt">
			 <div class="form-group">
			<label class="labelText">Permissions</label><span class="required">*</span>
			<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="At least one permission must be selected."><img class="infoIcon" src="images/NewIcon/info_icon.png"></a>
			</div>
			</div>
			<div class="col-md-6 voiceMsgRBtn">
			<div class="col-md-6 rBtnYes" style="padding-right: 0;">
				<div class="form-group">
				 <input type="radio" name="entpermissionRadio" id="entpermissionRadioSelectAll" class="checkbox-lg" value="Select All" onclick="updateEnterprisePermissions(this)">
				 <label class="labelText labelTextMargin" for="entpermissionRadioSelectAll"><span></span>Select All</label>
				</div>
			</div>

			 <div class="col-md-6 rBtnNo" style="padding-right: 0;">
				 <div class="form-group">
					<input type="radio" name="entpermissionRadio" id="entpermissionRadioClearAll" class="checkbox-lg" value="Clear" onclick="updateEnterprisePermissions(this)">
					<label class="labelText" for="entpermissionRadioClearAll"><span></span>Clear All</label>
				</div>
			</div>
			</div>
		</div>
	    </div>
        
        
        <?php 
        $x =0;
        $openAccordian = 0;
        $entPermissionCat = getEnterprisePermissionsFromPermissionsCategories($db);
	$toolTipEntGrpup = "<span class='tooltiptextEntGroups tooltiptext' id='groupEntToolTip'>";
	$entGroupCategoryPermissions = "<div>";        
	foreach($entPermissionCat['Group'] as $entPerKey => $entPerVal) {
           // $entPerVal['description'];
            
            $toolTipEntGrpup .= "<div>".$entPerVal['description']."</div>";
	     $createPermissionsa = createEnterprisePermissionCheckboxes($entPerVal, $permissions, $x);
            
            
            
	    $entGroupCategoryPermissions .= $createPermissionsa["html"];
            if($openAccordian == 0) {
	        $openAccordian = $createPermissionsa['openAccordian'];
	    }
            $x++;
	}
          
	$entGroupCategoryPermissions .= "</div>";
	$toolTipEntGrpup .= "</span>";
	?>	
	<div class="row">	
           <div class="col-md-12 uiModalAccordian">
        		<!-- <div class="averistar-bs3"> -->
                        <div class="averistar-bs3">
                           <div class="panel panel-default panel-forms" id="groupEntPerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="groupEntTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width groupEntPerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#groupEntPerDivAdd1" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="groupEntPerDivAdd1">Group Permissions
                				</a>
                			</h4>
        <!--   tool tip -->
                                <div id="usertool6">
            					<?php echo $toolTipEntGrpup; ?>
                    		</div>
        				</div>
                		<div id="groupEntPerDivAdd1" class="collapsideDivAdd panel-collapse  <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="groupEntPerDivAdd">
                			<div class="panel-body" style="padding: 0px;">
                                <div>
                    				<div class="">
                    						<?php echo $entGroupCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
    
    
    <!--enterPrise group permission -->
     
     <?php 
        $a =0;
        $openAccordian = 0;
        $entPermissionCat = getEnterprisePermissionsFromPermissionsCategories($db);
	$toolTipEntPermTooltip = "<span class='tooltiptextEntPerm tooltiptext' id='entPermModToolTip'>";
	$entPermCategoryPermissions = "<div>";        
	foreach($entPermissionCat['Enterprise'] as $entPerKey => $entPerVal) {
           // $entPerVal['description'];
            
           $toolTipEntPermTooltip .= "<div>".$entPerVal['description']."</div>";
	    $createPermissionsa = createEnterprisePermissionCheckboxes($entPerVal, $permissions, $a);
            
	    $entPermCategoryPermissions .= $createPermissionsa["html"];
            if($openAccordian == 0) {
	        $openAccordian = $createPermissionsa['openAccordian'];
	    }
            $a++;
	}
          
	$entPermCategoryPermissions .= "</div>";
	$toolTipEntPermTooltip .= "</span>";
	?>	
	<div class="row">	
           <div class="col-md-12 uiModalAccordian">
        		<!-- <div class="averistar-bs3"> -->
                        <div class="averistar-bs3">
                           <div class="panel panel-default panel-forms" id="entPerRowParentDiv" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="entPermTitleMod">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width groupEntPerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#entPermModCollapseDiv" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="entPermModCollapseDiv">Enterprise Permissions
                				</a>
                			</h4>
        <!--   tool tip -->
                                <div id="usertool7">
            					<?php echo $toolTipEntPermTooltip; ?>
                    		</div>
        				</div>
                		<div id="entPermModCollapseDiv" class="collapsideDivAdd panel-collapse  <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="entPermModToolTip">
                			<div class="panel-body" style="padding: 0px;">
                                <div>
                    				<div class="">
                    						<?php echo $entPermCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
    
    <!--end enterprise group permission -->

    <?php 
	$y = 0;
        $openAccordian = 0;
        $toolTipInventory = "<span class='tooltiptextInventory tooltiptext' id='inventoryTooltip'>";
	$InventoryCategoryPermissions = "<table border='1' style='width:80%;'>";
	foreach($entPermissionCat['Inventory'] as $invtryPerKey => $invtryPerVal) {
	    $toolTipInventory .= "<div>".$invtryPerVal['description']."</div>";
	    $InventoryCategory = createEnterprisePermissionCheckboxes($invtryPerVal, $permissions, $y);
	    
	    $InventoryCategoryPermissions .= $InventoryCategory["html"];
            if($openAccordian == 0) {
	        $openAccordian = $InventoryCategory['openAccordian'];
	    }
            $y++;
	}
	$InventoryCategoryPermissions .= "</table>";
	$toolTipInventory .= "</span>";
	?>
		
	<div class="row">
		
           <div class="col-md-12 uiModalAccordian">
        		<!-- <div class="averistar-bs3"> -->
                        <div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="InventryPerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="InventoryTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width filtersPerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#InventoryPerDivAdd1" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="InventoryPerDivAdd1">Inventory Permissions
                				   </a>
                			</h4>
        <!--   tool tip -->
        					<div id="usertool8">
            					<?php echo $toolTipInventory; ?>
                    		</div>
        				</div>
                		<div id="InventoryPerDivAdd1" class="collapsideDivAdd panel-collapse  <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="InventoryTitleAdd">
                			<div class="panel-body" style="padding: 0px;">
                                <div>
                    				<div class="">
                    						<?php echo $InventoryCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        
        
        <?php 
                
        $z = 0;
        $openAccordian = 0;
	$toolTipAdministrators = "<span class='tooltiptextAdministrators tooltiptext' id='AdministratorsTooltip'>";
	$AdministratorsCategoryPermissions = "<table border='1' style='width:80%;'>";
	foreach($entPermissionCat['Administrators'] as $admnsPerKey => $admnsPerVal) {
	    $toolTipAdministrators .= "<div>".$admnsPerVal['description']."</div>";
	    $AdministratorsCategory = createEnterprisePermissionCheckboxes($admnsPerVal, $permissions, $z);
            
	    $AdministratorsCategoryPermissions .= $AdministratorsCategory["html"];
            if($openAccordian == 0) {
	        $openAccordian = $AdministratorsCategory['openAccordian'];
	    }
            $z++;
	}
	$AdministratorsCategoryPermissions .= "</table>";
	$toolTipAdministrators .= "</span>";
	?>
		
	<div class="row">
		
           <div class="col-md-12 uiModalAccordian">
        		<!-- <div class="averistar-bs3"> -->
                        <div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="AministratorPerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="AdministratorsTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width aministratorPerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#AdministratorsPerDivAdd1" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="AdministratorsPerDivAdd1">Administrators Permissions
                				   </a>
                			</h4>
        <!--   tool tip -->
        					<div id="usertool9">
            					<?php echo $toolTipAdministrators; ?>
                    		</div>
        				</div>
                		<div id="AdministratorsPerDivAdd1" class="collapsideDivAdd panel-collapse  <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="AdministratorsTitleAdd">
                                    <div class="panel-body" style="padding: 0px;">
                                <div>
                    				<div class="">
                    						<?php echo $AdministratorsCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        
    </div>      
<?php } ?>      
      
  <div class="row">
		<div class="form-group alignBtn">
        			<!-- <p class="register-submit"> -->
        			<input type="button" name="subBut" id="subBut" class="subButton marginRightButton" value="Submit">
        		<!-- </p> -->
        	
        		<?php
                        $administratorsServiceLogsArr         = explode("_", $_SESSION["permissions"]["administrators"]);
                        $administratorsAddDelLogPermission          = $administratorsServiceLogsArr[2];
        			//if logged in as superuser, delete admins (except for system admin)
        			if (($_SESSION["superUser"] == "1" || $_SESSION["superUser"] == "3") and $_SESSION["adminUsers"]["superUserUpdate"] !== "2")
                                    
                                    if (($administratorsAddDelLogPermission=="yes") and $_SESSION["adminUsers"]["superUserUpdate"] !== "2")
        			
                                    
                                    {
        			
        				// echo "<p class=\"register-submit\">";
        				echo "<input type=\"button\" class=\"deleteButton marginRightButton\" id=\"delBut\" name=\"delBut\" value=\"Delete Administrator\">";
        				// echo "</p>";
        			}
        		?>
    	</div>
	</div>
</form>
<script>
function checkPermissionCheck(className){
        var className = "."+className;
        $(className).prop('checked', true);
    }
     
    function unCheckPermission(permissionName){
        var clsNameForMod    = ".entpermsMod_"+permissionName;
        var clsNameForAddDel = ".entpermsAddDel_"+permissionName;
        $(clsNameForMod).prop('checked', false);
        $(clsNameForAddDel).prop('checked', false);
    }

 /* tooltip hover event */
 
 /*end tooltip hover event */
 
 //modify group tooltip changes
 new Popper(
		  document.getElementById('usersTitleMod'),
		  document.getElementById('spanTooltipUser')
            );
	

new Popper(
		  document.getElementById('groupTitleMod'),
		  document.getElementById('spanTooltipGroup')
            );
    
    new Popper(
		  document.getElementById('servicesTitleMod'),
		  document.getElementById('spanTooltipservicesCategory')
            );
		
          new Popper(
		  document.getElementById('devicesTitleMod'),
		  document.getElementById('spanTooltipDevices')
            ); 
    
    new Popper(
		  document.getElementById('supervisoryTitleMod'),
		  document.getElementById('spanTooltipSupervisory')
            );
    
    
</script>
<div id="dialogUpdate" class="dialogClass"></div>
<div id="dialogDelete" class="dialogClass"></div>