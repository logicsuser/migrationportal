<?php

    require_once("/var/www/html/Express/config.php");
    checkLogin();

     $hidden = array("userId", "superUserUpdate");
    //if ($securityDomainPattern == "" || $_POST["superUserUpdate"] == "1" ) {
    if ($securityDomainPattern == "" || ($_POST["superUser"] == "1" || $_POST["superUserUpdate"] == "1") ) {
            $hidden[] = "securityDomain";
    }

    $isSuperUser = (isset($_POST["superUser"]) && $_POST["superUser"] == "1") || (isset($_POST["superUserUpdate"]) && $_POST["superUserUpdate"] == "1");
    $required = array("userName", "userNameUpdate", "name", "nameUpdate", "emailAddress", "emailAddressUpdate", "entAdministratorClusterId","entUpdateAdministratorClusterId");
        
    //Code added @ 15 Feb 2019
    if(($_POST["superUser"] == "3" || $_POST["superUserUpdate"] == "3")){
        $isEnterpriseAdminUser = (isset($_POST["superUser"]) && $_POST["superUser"] == "3") || (isset($_POST["superUserUpdate"]) && $_POST["superUserUpdate"] == "3");
    }

    if(($_POST["superUser"] == "0" || $_POST["superUserUpdate"] == "0")){
        $isGroupAdminUser = (isset($_POST["superUser"]) && $_POST["superUser"] == "0") || (isset($_POST["superUserUpdate"]) && $_POST["superUserUpdate"] == "0");
    }
    //End code

     

     /* ex-800 code added by sollogics @06-dec-2018 */
     
     if((empty($_POST['checkPermissions']) && ($_POST["superUser"] == "0" or $_POST["superUserUpdate"] == "0")  ) || $securityDomainPattern != "" && ($_POST["superUser"] == "0" or $_POST["superUserUpdate"] == "0") )
     {
        $required[] = "checkPermissions";
     }
	
    $assignedPermissions    = isset($_POST["assignedPermissions"]) ? $_POST["assignedPermissions"] : array();
    $assignedEntPermissions = isset($_POST["assignedEntPermissions"]) ? $_POST["assignedEntPermissions"] : array();
    
    
    
    unset($_POST["assignedPermissions"]);
    unset($_POST["permissionRadio"]);
    unset($_POST["entpermissionRadio"]);
    
    //Get Admin type
    if(isset($_POST["checkPermissions"])){
        $queryATI = "select name from adminTypesLookup where adminTypeIndex='" . $_POST["checkPermissions"] . "'";
        $resultATI = $db->query($queryATI);
        while ($row = $resultATI->fetch()) {
            $_POST["checkPermissions"] = $row["name"];
        }
    }
    
    
    $data = $errorTableHeader;

    $changes = 0;
    $error = 0;
    
    //Code added @ 18 Feb 2019  
    if(isset($_POST["superUser"]) && ($_POST["superUser"] == "3" || $_POST["superUserUpdate"] == "3")){ 
        unset($_POST['checkPermissions']);
        unset($_POST['Selsp']);
        unset($_POST['groups1']);
        unset($_POST['groups_1']);
        unset($_POST['perms']);       
        unset($_POST['permsUpdate']);    
        $_POST['securityDomain'] = $_POST['entsecurityDomain'];
        unset($_POST['entsecurityDomain']);
        
        $_POST['entAdministratorClusterId'] = $_POST['entAdministratorClusterId'];
        unset($_POST['entGroupAdministratorClusterId']);
        
    }
    
    if(isset($_POST["superUser"]) && ($_POST["superUser"] == "0" || $_POST["superUserUpdate"] == "0")){ 
        unset($_POST['enterpriseAssignUser']);
        unset($_POST['enterpriseAssignUserUpdate']);
        unset($_POST['entperms']);   
        unset($_POST['entpermsUpdate']);        
        unset($_POST['entpermsMod']);
        unset($_POST['entpermsUpdateMod']);        
        unset($_POST['entpermsAddDel']);
        unset($_POST['entpermsUpdateAddDel']);        
        unset($_POST['entsecurityDomain']);  
        
        $_POST['entGroupAdministratorClusterId'] = $_POST['entGroupAdministratorClusterId'];
        unset($_POST['entAdministratorClusterId']);
    }
    
    if(($_POST["superUser"] == "1")){
        unset($_POST['enterpriseAssignUser']);
        unset($_POST['enterpriseAssignUserUpdate']);
        unset($_POST['entperms']);   
        unset($_POST['entpermsUpdate']);        
        unset($_POST['entpermsMod']);
        unset($_POST['entpermsUpdateMod']);        
        unset($_POST['entpermsAddDel']);
        unset($_POST['entpermsUpdateAddDel']);        
        unset($_POST['entsecurityDomain']); 
        unset($_POST['checkPermissions']);
        unset($_POST['Selsp']);
        unset($_POST['groups1']);
        unset($_POST['groups_1']);
        unset($_POST['perms']);       
        unset($_POST['permsUpdate']);
        unset($_POST['securityDomain']);
        
        unset($_POST['entAdministratorClusterId']);
        unset($_POST['entGroupAdministratorClusterId']);
    }
    //End code
    
    $clusterSupport  =  (isset($bwClusters) && $bwClusters == "true") && $enableClusters == "true" ? true : false;
    if(!$clusterSupport){ 
        unset($_POST['entAdministratorClusterId']);
        unset($_POST['entGroupAdministratorClusterId']);
        unset($_POST['entUpdateAdministratorClusterId']);
    }
    
    //Code added @ 19 April 2019
    if(isset($_POST["securityDomain"]) && ($_POST["securityDomain"] != ""))
    {
        unset($_POST['Selsp']);
        unset($_POST['groups1']);
        unset($_POST['groups_1']);
    }
    //End code
    $verifyPassword = isset($_POST ["verifyPassword"]) ? $_POST ["verifyPassword"] : "";
    if( isset($_POST ["verifyPassword"]) ) {
        unset($_POST ["verifyPassword"]);
    }
    
     foreach ($_POST as $key => $value)
    {

            if (!in_array($key, $hidden)) //hidden form fields that don't change don't need to be checked or displayed
            {
                // for no security domain exist.
                if($_POST["sp"] != "") {
                    if ($key !="permsUpdate" && (isset($_POST["userId"]) and isset($_SESSION["adminUsers"][$key]) and  $value !== $_SESSION["adminUsers"][$key]) or !isset($_POST["userId"]))
                    {
                        $changes++;
                        $bg = CHANGED;
                    }
                    else
                    {
                        $bg = UNCHANGED;
                    }
                }

                    // if security domain exist
                if($_POST["sp"] == "") {
                        if ((isset($_POST["userId"]) and isset($_SESSION["adminUsers"][$key]) and  $value !== $_SESSION["adminUsers"][$key]) or !isset($_POST["userId"]))
                        {
                            $changes++;
                            $bg = CHANGED;
                        }
                        else
                        {
                            $bg = UNCHANGED;
                        }
                    }



                    if (in_array($key, $required) and $value == "")
                    {
                            $error = 1;
                            $bg = INVALID;
                            $value = $_SESSION["adminUsersNames"][$key] . " is a required field.";
                    }
                    else if ($key == "userName" or $key == "userNameUpdate")
                    {
                            //check if username already exists
                            $select = "SELECT u.userName from users u where u.userName='" . $value . "'";
                            if (isset($_POST["userId"]))
                            {
                                    $select .= " and id <> " . $_POST["userId"];
                            }
                            $lookup = $db->query($select);
                            if ($lookup->fetch(MYSQL_ASSOC))
                            {
                                    $error = 1;
                                    $bg = INVALID;
                                    $value = $_SESSION["adminUsersNames"][$key] . " already exists.";
                            }
                            //else if (!preg_match("/^[A-Za-z0-9\.@]+$/", $value))
                            else if (!preg_match("/^[A-Za-z0-9\-_.@]+$/", $value))
                            {
                                    $error = 1;
                                    $bg = INVALID;
                                    $value = $_SESSION["adminUsersNames"][$key] . " must include letters, numbers, \"@\" ,  \".\" , \"_\" , \"-\" only.";
                            }
                            else if ( strpos($value, "@") !== false && $_POST["use_ldap_authentication"] == "1")
                            {
                                $error = 1;
                                $bg = INVALID;
                                $value = $_SESSION["adminUsersNames"][$key] . " for LDAP user can not use domain name ";
                            }
                    }
                    else if ($key == "name" or $key == "nameUpdate")
                    {
                            if (!preg_match("/^[A-Za-z-]+[ ][A-Za-z',\. -]+$/", $value))
                            {
                                    $error = 1;
                                    $bg = INVALID;
                                    $value = $_SESSION["adminUsersNames"][$key] . " must include both a first and last name and be valid (characters such as numbers are not permitted).";
                            }
                    }
                    else if ($key == "password" or $key == "passwordUpdate")
                    {
                        if ($key == "password" and $value == "" && $_POST["use_ldap_authentication"] == "0")
                            {
                                    $error = 1;
                                    $bg = INVALID;
                                    $value = $_SESSION["adminUsersNames"][$key] . " is a required field.";
                            }
                            else if ($value != "" and !(preg_match("/[A-Z]+/", $value) and preg_match("/[a-z]+/", $value) and preg_match("/[0-9]+/", $value)))
                            {
                                    $error = 1;
                                    $bg = INVALID;
                                    $value = $_SESSION["adminUsersNames"][$key] . " must include at least one uppercase letter, one lowercase letter, and one number.";
                            }
                            else if ($value != "" and preg_match("/[&]+/", $value))
                            {
                                    $error = 1;
                                    $bg = INVALID;
                                    $value = $_SESSION["adminUsersNames"][$key] . " must not include an ampersand (\"&\").";
                            }
                            else if ( ($value != "" || $verifyPassword != "") && $verifyPassword != $value)
                            {
                                $error = 1;
                                $bg = INVALID;
                                $value = "Password and Verify Password do not match";
                            }
                            else if ($value)
                            {
                                    $value = str_repeat("*", strlen($value)); //don't display password
                            }
                    }
// 			else if ($key == "emailAddress")
// 			{
//                 // super users' email must be an active domain
//                 $domain = ($groupsDomainType == "Static" && $staticDomain != "") ? $staticDomain : $_SESSION["systemDefaultDomain"];
// 				if (!preg_match("/" . $domain . "/i", $value) and $_POST["superUser"] == "1")
// 				{
// 					$error = 1;
// 					$bg = INVALID;
// 					$value = $_SESSION["adminUsersNames"][$key] . " must include domain \"" . $domain . "\".";
// 				}
// 			}
        else if ($key == "emailAddress") {
            // super users' email must be an active domain
                        if ($groupsDomainType == "Static" && $staticDomain != "" and $_POST["superUser"] == "1") {
                            if (in_array($key, $required) and $value == "") {
                                $error = 1;
                                $bg = INVALID;
                                $value = $_SESSION["adminUsersNames"][$key] . " is a required field.";
                            }
                        }
                    }
                    
        /* cluster */
    /* else if ($key == "entAdministratorClusterId" || $key == "entUpdateAdministratorClusterId") {
          
                        if ($_POST["superUser"] == "3" || $_POST["superUserUpdate"] == "3") {
                            if (in_array($key, $required) and $value == "") {
                                $error = 1;
                                $bg = INVALID;
                                $value = $_SESSION["adminUsersNames"][$key] . " is a required field.";
                            }
                        }
    } */           
                    
        /*cluster end */            
                    
                    
                    
                    else if ($key == "sp")
                    { 
                            if(empty($securityDomainPattern)){
                                    if($value == ""){
                                            $error = 1;
                                            $bg = INVALID;
                                            $value = $_SESSION["adminUsersNames"][$key] . " must not be null.";
                                    }
                            }else{
                                    if(!empty($_POST["userId"])){
                                            $bg = UNCHANGED;
                                            $changes--;
                                    }
                            }

                    }
                    //else if ($key == "Selsp" &&  ) 
                    else if ($key == "Selsp" && $_POST["superUser"] == "0")
                    { 
                            if(empty($securityDomainPattern)){
                                    if($value == ""){
                                            $error = 1;
                                            $bg = INVALID;
                                            $value = $_SESSION["adminUsersNames"][$key] . " must not be null.";
                                    }
                            }else{
                                    if(!empty($_POST["userId"])){
                                            $bg = UNCHANGED;
                                            $changes--;
                                    }
                            }

                    }
                    else if ($key == "groups1" && $securityDomainPattern == "" && $_POST["superUser"] == "0")
                    {
                            if ($value == "" and $_POST["superUser"] !== "1")
                            {
                                    $error = 1;
                                    $bg = INVALID;
                                    $value = $_SESSION["adminUsersNames"][$key] . " must be selected if user is not a Super User.";
                            }
                    }
                    else if ($key == "groups_1" && $securityDomainPattern == "" && $_POST["superUserUpdate"] == "0")
                    {
                            if($_POST["superUserUpdate"] !== "1"){
                                    if ($value == "" and $_POST["superUserUpdate"] !== "1")
                                    //if ($value == "" and (isset($_POST["superUser"]) && $_POST["superUser"] !== "1"))
                                    {
                                            $error = 1;
                                            $bg = INVALID;
                                            $value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
                                    }
                            }
                    }
                    //Code added @ 18 Feb 2019
                    else if ($key == "enterpriseAssignUser" && $securityDomainPattern == "" && $_POST["superUser"] == "3")
                    {
                            if ($value == "" and $_POST["superUser"] !== "1")
                            {
                                    $error = 1;
                                    $bg = INVALID;
                                    $value = $_SESSION["adminUsersNames"][$key] . " must be selected if user is not a Super User.";
                            }
                    }
                    else if ($key == "enterpriseAssignUserUpdate" && $securityDomainPattern == "" && ($_POST["superUserUpdate"] == "3"))
                    {
                            if($_POST["superUserUpdate"] !== "1"){
                                    if ($value == "" and $_POST["superUserUpdate"] !== "1")
                                    {
                                            $error = 1;
                                            $bg = INVALID;
                                            $value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
                                    }
                            }
                    }
                    //End code
                    else if ($key == "securityDomain" && $securityDomainPattern != "") {
                        //if (($value == "None" || $value == "") && $_POST["superUserUpdate"] != "1") {
                        if (($value == "None" || $value == "") && ! $isSuperUser) {
                            $error = 1;
                            $bg = INVALID;
                $value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
                        }else if (($value <> "" && $_SESSION["adminUsers"][$key] == $value) && ! $isSuperUser) {
                            //$changes++;
                            $bg = UNCHANGED;
                        } else if (($value <> "" && $_SESSION["adminUsers"][$key] <> $value) && ! $isSuperUser) {
                //$changes++;
                $bg = CHANGED;
            }
        }
        else if ($key == "user_cell_phone_numberUpdate") { 
                            if (($value == "None" || $value == "") && $_SESSION["adminUsers"][$key] == $value) {
                            //$error = 1;
                            $bg = UNCHANGED;
               // $value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
            }
        }
        /*else if ($key == "checkPermissions") { 
            if($isSuperUser <> "1"){
                                if ($value == "Select All" || $value == "Clear" || $value == "None") {
                        $error = 1;
                        $bg = INVALID;
                        $value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
                    }
            }else{
                    $bg = UNCHANGED;
                    //$error = 1;
                    $changes--;
            }
        }*/
        else if($key == "checkPermissions" && $_POST["superUser"] == "0"){
            if($isSuperUser <> "1"){
                    if ($value == "Select All" || $value == "Clear" || $value == "None") {
                            $error = 1;
                            $bg = INVALID;
                            $value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
                    }
            }else{
                    $bg = UNCHANGED;
                    //$error = 1;
                    $changes--;
            }
        }
                    //else if (($key == "perms" and $_POST["superUser"] !== "1") or ($key == "permsUpdate" and $_POST["superUserUpdate"] !== "1" and $_POST["superUserUpdate"] !== "2"))
                    else if (($key == "perms" and $_POST["superUser"] == "0") or ($key == "permsUpdate" and ! $isSuperUser))
                    {
                        $perHasChanged = false;
                            $setPerms = "false";
                            foreach ($value as $k => $v)
                            {
                                    if ($v == "1")
                                    {
                                            $setPerms = "true";
                                    }
                            }

                            // check permission has changed in modify
                            if($key == "permsUpdate") {
                                foreach ($value as $k => $v)
                                {
                                    if(isset($assignedPermissions[$k]) && $assignedPermissions[$k] <> $v || ( !isset($assignedPermissions[$k]) && $v == "1") ) {
                                        $changes++;
                                        $bg = CHANGED;
                                        $perHasChanged = true;
                                    }
                                }

                            }
                            if ($setPerms !== "true")
                            {
                                    $error = 1;
                                    $bg = INVALID;
                                    $value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
                            }
                    }
                    
                    //Code added @ 18  Feb 2019
                    else if (($key == "entperms" && $_POST["superUser"] == "3") || ($key == "entpermsUpdate" && $isEnterpriseAdminUser))
                    {
                            $entPerHasChanged = false;
                            $setEntPerms = "false";
                            foreach ($value as $k => $v)
                            {
                                    if ($v == "1")
                                    {
                                            $setEntPerms = "true";
                                    }
                            }

                            // check permission has changed in modify
                            if($key == "entpermsUpdate") {
                                foreach ($value as $k => $v)
                                {
                                    //!strpos($assignedEntPermissions[$k], $v)
                                    //if(isset($assignedEntPermissions[$k]) && $assignedEntPermissions[$k] != $v || ( !isset($assignedEntPermissions[$k]) && $v == "1") ) {
                                    //if(isset($assignedEntPermissions[$k]) && !strpos($assignedEntPermissions[$k], $v) || ( !isset($assignedEntPermissions[$k]) && strpos($v, "1")) ) {
                                    if($v == "1"){
                                        if(isset($assignedEntPermissions[$k]) && !(strpos($assignedEntPermissions[$k], $v) !== false) || (!isset($assignedEntPermissions[$k]) && $v == "1") ) {
                                            $changes++;
                                            $bg = CHANGED;
                                            $entPerHasChanged = true;
                                        }
                                        $updateMod             = ($_POST['entpermsUpdateMod'][$k] == "1")? "yes" : "no";
                                        $updateAddDel          = ($_POST['entpermsUpdateAddDel'][$k] == "1")? "yes" : "no";
                                        $entParmsUpdteComplete = $v."_".$updateMod."_".$updateAddDel;                                        
                                        if($assignedEntPermissions[$k] != $entParmsUpdteComplete){
                                            $changes++;
                                            $bg = CHANGED;
                                            $entPerHasChanged = true;
                                        }
                                    }
                                }
                            }
                            if ($setEntPerms !== "true")
                            {
                                    $error = 1;
                                    $bg = INVALID;
                                    $value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
                            }
                    }
                    
                    //echo "<br />entPerHasChanged - $entPerHasChanged <br />";
                    //End code

//			if ($error == 1)
//			{
//				$bg = INVALID;
//			}
//			else
//			{
//				$bg = UNCHANGED;
//			}

                    if ($bg != UNCHANGED)
                    {
                            if (! is_array($value)) //for permissions array
                            {
                                $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["adminUsersNames"][$key] . "</td><td class=\"errorTableRows\">";
                            }				

                            if (is_array($value)) //for permissions array
                            {
                                if($perHasChanged || $key == "perms") {
                                    $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["adminUsersNames"][$key] . "</td><td class=\"errorTableRows\">";
                                }
                                // for add
                                if($key == "perms") {
                                    foreach ($value as $k => $v)
                                    {
                                        if ($v == "1")
                                        {
                                            $data .= $_SESSION["fieldNamesList"][$k] . "<br />"; //Code added @ 07 May 2018 for remove br tags in permissions on administratpor check data page
                                            //$data .= $_SESSION["fieldNames"][$k] . "<br />";
                                        }
                                    }
                                }
                                // For modify
                                if($key == "permsUpdate") {
                                    foreach ($value as $k => $v)
                                    {
                                        if (isset($assignedPermissions[$k]) && $v <> $assignedPermissions[$k] || (!isset($assignedPermissions[$k]) && $v == "1") )
                                        {
                                            //Code commented @ 25 July 2018
                                            //$data .= $_SESSION["fieldNamesList"][$k] . "<br />"; //Code added @ 07 May 2018 for remove br tags in permissions on administratpor check data page
                                            //$data .= $_SESSION["fieldNames"][$k] . "<br />";
                                            //Code added @ 25 July 2018
                                            if($v=="1"){
                                                $data .= "<strong>Added:</strong> ".$_SESSION["fieldNamesList"][$k] . "<br />";
                                            }else{
                                                $data .= "<strong>Removed:</strong> ".$_SESSION["fieldNamesList"][$k] . "<br />";
                                            }
                                            //End code
                                        }
                                    }
                                }
                                
                                //code added @ 18 Feb 2019
                                if($entPerHasChanged || $key == "entperms") {
                                    $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["adminUsersNames"][$key] . "</td><td class=\"errorTableRows\">";
                                }
                                // for add
                                if($key == "entperms") {
                                    foreach ($value as $k => $v)
                                    {
                                        if ($v == "1")
                                        {
                                            $data .= $_SESSION["entPsmnFieldNamesList"][$k]; //Code added @ 07 May 2018 for remove br tags in permissions on administratpor check data page
                                            if(array_key_exists($k, $_POST['entpermsMod']) && $_POST['entpermsMod'][$k]=="1"){
                                                $data .=" [Modify - yes]";
                                            }else{
                                                $data .=" [Modify - no]";
                                            }
                                            if(array_key_exists($k, $_POST['entpermsAddDel']) && $_POST['entpermsAddDel'][$k]=="1"){
                                                $data .=" [Add/Delete - yes]";
                                            }else{
                                                $data .=" [Add/Delete - no]";
                                            }
                                            $data .="<br />";
                                        }
                                    }
                                }
                                // For modify
                                if($key == "entpermsUpdate") {
                                    $tempPrmnsArr = array();
                                    foreach ($value as $k => $v)
                                    {
                                            //if (isset($assignedEntPermissions[$k]) && $v <> $assignedEntPermissions[$k] || (!isset($assignedEntPermissions[$k]) && $v == "1") )
                                            $updateMod             = ($_POST['entpermsUpdateMod'][$k] == "1")? "yes" : "no";
                                            $updateAddDel          = ($_POST['entpermsUpdateAddDel'][$k] == "1")? "yes" : "no";
                                            $entParmsUpdteComplete = $v."_".$updateMod."_".$updateAddDel; 
                                            if(isset($assignedEntPermissions[$k]) && !(strpos($assignedEntPermissions[$k], $v) !== false) || (!isset($assignedEntPermissions[$k]) && $v == "1") )
                                            {                                               
                                                //Code added @ 25 July 2018                                                
                                                if($v=="1"){     
                                                    $tempPrmnsArr[] = $k;
                                                    $data .= "<strong>Added:</strong> ".$_SESSION["entPsmnFieldNamesList"][$k] . " [Modify - $updateMod]  [Add/Delete - $updateAddDel]<br />";
                                                   }
                                                else{
                                                    $data .= "<strong>Removed:</strong> ".$_SESSION["entPsmnFieldNamesList"][$k] . "<br />";
                                                }
                                                //End code
                                            }
                                            if($v=="1"){
                                                if(!in_array($k, $tempPrmnsArr)){
                                                    $updateMod             = ($_POST['entpermsUpdateMod'][$k] == "1")? "yes" : "no";
                                                    $updateAddDel          = ($_POST['entpermsUpdateAddDel'][$k] == "1")? "yes" : "no";
                                                    $entParmsUpdteComplete = $v."_".$updateMod."_".$updateAddDel;                                        
                                                    if($assignedEntPermissions[$k] != $entParmsUpdteComplete){ 
                                                         $data .= "<strong>Modified:</strong> ".$_SESSION["entPsmnFieldNamesList"][$k] . "  [Modify - $updateMod]  [Add/Delete - $updateAddDel] <br />";
                                                    }
                                                }
                                            }
                                    }
                                }
                                //End code

                            }
                            else if ($key == "superUser" or $key == "disabled" or $key == "use_sms_every_login" or $key == "use_sms_on_password_reset" or $key == "use_sms_every_loginUpdate" or $key == "use_sms_on_password_resetUpdate" or $key == "use_ldap_authentication")
                            {
                                    if ($value == "1")
                                    {
                                            $data .= "True";
                                    }
                                    else
                                    {
                                            $data .= "False";
                                    }
                            }
                            else if ($value !== "")
                            {
                                    $data .= $value;
                            }
                            else
                            {
                                    $data .= "None";
                            }
                            $data .= "</td></tr>";
                    }
            }
    }
    $data .= "</table>";
    //echo "Change ".$changes;echo "Error ".$error;
    if ($changes == 0 && $error  == 0)
    {
            $error = 1;
            $data = "You have made no changes.";
    }
    echo $error . $data;
?>
