<!-- User Filters -->
<link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v11">
<!--<script src="/Express/js/bootstrap.min.js"></script>-->
<script src="/Express/js/bootstrap/bootstrap.min.js"></script>
<script src="/Express/js/jquery-1.17.0.validate.js"></script>
<script src="/Express/js/jquery.loadTemplate.min.js"></script>
<script src="userMod/multiDragAndDrop.js"></script>
<script src="userMod/multiDragAndDropList.js"></script>
<!-- admin permission tooltip open  js -->
 <script src="/Express/js/popper.js"></script>
  <!-- end admin -->

  <style>
    #enterprise_available_user li.selected, #enterprise_assign_user li.selected, #availableUserServiceToAssign li.selected, #assignedUserService li.selected,#sortable_1 li.selected, #sortable_2 li.selected,#sortable1 li.selected, #sortable2 li.selected{
    background:#ffb200;
}
#enterprise_available_user li:hover, #enterprise_assign_user li:hover , #availableUserServiceToAssign li:hover, #assignedUserService li:hover,#sortable_1 li:hover, #sortable_2 li:hover,#sortable1 li:hover, #sortable2 li:hover{
    background:#ffb200;
    color:white;
}
 #enterprise_available_user , #enterprise_assign_user,#availableUserServiceToAssign, #assignedUserService,#sortable1, #sortable2{
   padding:25px 0 0 0;
}

#sortable1, #sortable2, #sortable_1, #sortable_2 {
    
    padding-top: 25px !important;
}
 #enterprise_available_user , #enterprise_assign_user,#availableUserServiceToAssign, #assignedUserService,#sortable1, #sortable2{background: #fff;}

    

#availableUserServiceToAssign li, #assignedUserService li,#sortable_1 li, #sortable_2 li,#sortable1 li, #sortable2 li{
	width: 100%;
	cursor:pointer;
	font-size:12px;
	font-weight:bold;
	text-align:center;
	padding:4px 4px 4px 3px;
	margin:0 0 7px -1px;
}
   
      
      
    #groupEntTitleAdd #usertool6 span.tooltiptextUsers.tooltiptext, #InventoryTitleAdd #usertool7 span.tooltiptextGroups.tooltiptext {
    margin-left: -1080px !important;
    margin-top: -43px;
}

   

span#AdministratorsTooltip, span#inventoryTooltip,span#groupEntToolTip,span#entPermToolTip {
    bottom: 45% !important;
    top: inherit;
    margin-left: -375px;;
    /* margin-left: -386px; */
}

 .tooltiptext > div {
            line-height: 22px;
            margin-left: 15px;
}

#sortable1 li:first-child, #sortable2 li:first-child, #sortable_1 li:first-child, #sortable_2 li:first-child{margin-top: 0px;}
 </style>     
      <?php
    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();

function hasSecurityDomain() {
    global $securityDomainPattern;

    return $securityDomainPattern != "";
}

function getSecurityDomains() {
    global $systemDomains, $permissions, $securityDomainPattern;

    $securityDomain = $permissions["securityDomain"];
    $str = "<option value=\"\"";
    $str .= $securityDomain == "" ? " selected" : "";
    $str .= ">None</option>";

    foreach ($systemDomains as $domain) {
        if (strpos($domain, $securityDomainPattern) !== false ) {
            $str .= "<option value=\"" . $domain . "\"";
            $str .= $domain == $securityDomain ? " selected" : "";
            $str .= ">" . $domain . "</option>";
        }
    }
    return $str;
}
//Function changed @ 01 May 2018
function createPermissionsRadioButtons() {
    global $db;
    /*
    $permissionTypes = array();
    $query = "select adminType from permissionsSets";
    $result = $db->query($query);
    while ($row = $result->fetch()) {
        $permissionTypes[] = $row["adminType"];
    }
    */
        $permissionTypes = array();
	$query = "select adminTypeIndex, name from adminTypesLookup";
        $result = $db->query($query);
        $i = 0;
        while ($row = $result->fetch()) {
            $permissionTypes[$i]['adminTypeIndex'] = $row["adminTypeIndex"];
            $permissionTypes[$i]['name'] = $row["name"];
            $i++;
        }
    //<div class="diaPN diaP3" style="width: 2%"><input type="radio" name="checkPermissions" id="checkPermissionsDistrictIT" value="districtIT"
    //onclick="updatePermissions(this)"></div>
    //<div class="diaPN diaP3"><label style="margin-left: 4px; margin-right: 16px; margin-top: 4px">District IT</label></div>

    $html = "";
    $options = "";
    if(count($permissionTypes) > 0) {
        $options = "<option value=''>None</option>";
        /*
        for ($i = 0; $i < count($permissionTypes); $i++) {
            $type = $permissionTypes[$i];
            $options .= "<option value='$type'>$type</option>";
        }*/
        for ($i = 0; $i < count($permissionTypes); $i++) {
                $adminTypeIndex = $permissionTypes[$i]['adminTypeIndex'];
                $type = $permissionTypes[$i]['name']; 
                $options .= "<option value='$adminTypeIndex'>$type</option>";
            }
    } else {
        $options .= "<option value='Admin'>Admin</option>";
    }    
    return $options;
}


function getPermissionsFromPermissionsCategories($db) {
    
    $fetchPermissions = "SELECT * from permissionsCategories";
    $fetchPermissionsQueryResult = $db->query($fetchPermissions);
    
    $permissionsData = array();
    while ($fetchPermissionsQueryRow = $fetchPermissionsQueryResult->fetch(MYSQL_ASSOC)) {
        if($fetchPermissionsQueryRow['category'] == 'Users') {
            $permissionsData['Users'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        } else if($fetchPermissionsQueryRow['category'] == 'Supervisory') {
            $permissionsData['Supervisory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        } else if($fetchPermissionsQueryRow['category'] == 'Services') {
            $permissionsData['Services'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        } else if($fetchPermissionsQueryRow['category'] == 'Groups') {
            $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        } else if($fetchPermissionsQueryRow['category'] == 'Enterprise') {
            $permissionsData['Enterprise'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        } else if($fetchPermissionsQueryRow['category'] == 'Devices') {
            $permissionsData['Devices'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        }
    }
    
    foreach ($permissionsData as $key => $row) {
        $order = array();
        foreach ($row as $key1 => $row1) {
            $order[$key1] = $row1['description'];
        }
        
        array_multisort($order, SORT_ASC, $row);
        $permissionsData[$key] = $row;
    }
    
    return $permissionsData;
}


function getEnterprisePermissionsFromPermissionsCategories($db) {
    
    $fetchPermissions = "SELECT * from enterprisePermissionsCategories";
    $fetchPermissionsQueryResult = $db->query($fetchPermissions);
    
   
    $permissionsData = array();
    while ($fetchPermissionsQueryRow = $fetchPermissionsQueryResult->fetch(MYSQL_ASSOC)) 
    {
        if($fetchPermissionsQueryRow['category'] == 'Group') 
        {
            $permissionsData['Group'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        } 
        
        else if($fetchPermissionsQueryRow['category'] == 'Enterprise') 
        {
            $permissionsData['Enterprise'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
            'description' => $fetchPermissionsQueryRow['description']);
        } 
        else if($fetchPermissionsQueryRow['category'] == 'Inventory') 
        {
            $permissionsData['Inventory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        }
        else if($fetchPermissionsQueryRow['category'] == 'Administrators') 
        {
            $permissionsData['Administrators'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        }
    }
    
    foreach ($permissionsData as $key => $row) {
        $order = array();
        foreach ($row as $key1 => $row1) {
            $order[$key1] = $row1['description'];
        }
        
        array_multisort($order, SORT_ASC, $row);
        $permissionsData[$key] = $row;
    }
    
    return $permissionsData;
}

function createPermissionCheckboxes($perVal) {
    $html = "";
    $chk = "";
    $html .= "<div class=\"col-md-6\">";
    $html .= "<input type=\"hidden\" name=\"perms[" . $perVal['permissionId'] . "]\" value=\"0\">";
    $html .= "<input style=\"\" type=\"checkbox\" id=\"perms[" . $perVal['permissionId'] . "]\" " . $chk . " name=\"perms[" . $perVal['permissionId'] . "]\" value=\"1\"><label for=\"perms[" . $perVal['permissionId'] . "]\" " . $chk . "><span></span></label>";
    
    $html .= "<label class=\"labelText\" for=\"perms[" . $perVal['permissionId'] . "]\">" . $perVal["description"] . "</label>";
    $html .= "</div>";
    
    $returnData = array("html" => $html);
    return $returnData;
}

function createEnterprisePermissionCheckboxes($perVal, $i) {
    $html        = "";
    $chk         = "";
    $modCheck    = "";
    $addDelCheck = "";
     
    $cssStyle = "style='background-color: #D5D5D5; padding:5px 0px; margin-bottom: 2px;'";
    $permissionName = $perVal['permissionId'];
    $clsName        = "$permissionName";    
    
    
    if($i%2 == 0){
        $cssStyle = "style='background-color: #DEDEDE; padding:5px 0px; margin-bottom: 2px;'";
    }
    $html .= "<div  class=' col-md-12' $cssStyle>";
    
    $html .= "<div class=\"col-md-6\">";
    $html .= "<input type=\"hidden\" name=\"entperms[" . $perVal['permissionId'] . "]\" value=\"0\">";
    $html .= "<input style=\"\" type=\"checkbox\" onclick='javascript: unCheckPermission(\"$clsName\");' class='permissionChecked_$permissionName' id=\"entperms[" . $perVal['permissionId'] . "]\" " . $chk . " name=\"entperms[" . $perVal['permissionId'] . "]\" value=\"1\"><label for=\"entperms[" . $perVal['permissionId'] . "]\" " . $chk . "><span></span></label>";
    
    $html .= "<label class=\"labelText\" for=\"entperms[" . $perVal['permissionId'] . "]\">" . $perVal["description"] . "</label>";
    $html .= "</div>";
    
    $className = trim("permissionChecked_$permissionName");
    
    
    if(isset($_SESSION["permissionAction"]) && $_SESSION["permissionAction"][$perVal["permissionId"]]['modify']=="yes"){
        $html .= "<div class=\"col-md-3\" style='float: left;'>";
        $html .= "<input type=\"hidden\" name=\"entpermsMod[" . $perVal["permissionId"] . "]\" value=\"0\">";
        $html .= "<input style=\"\" type=\"checkbox\" onclick='javascript: checkPermissionCheck(\"$className\");' class='entpermsMod_$permissionName' id=\"entpermsMod[" . $perVal["permissionId"] . "]\" " . $modCheck . " name=\"entpermsMod[" . $perVal["permissionId"] . "]\" value=\"1\"><label for=\"entpermsMod[" . $perVal["permissionId"] . "]\" " . $chk . "><span></span></label>";
        $html .= "<label class=\"labelText\" for=\"entpermsMod[" . $perVal["permissionId"] . "]\">Modify</label>";
        $html .= "</div>";
    }

    if(isset($_SESSION["permissionAction"]) && $_SESSION["permissionAction"][$perVal["permissionId"]]['addDelete']=="yes"){
        $html .= "<div class=\"col-md-4\" style='float: left;'>";
        $html .= "<input type=\"hidden\" name=\"entpermsAddDel[" . $perVal["permissionId"] . "]\" value=\"0\">";
        $html .= "<input style=\"\" type=\"checkbox\" onclick='javascript: checkPermissionCheck(\"$className\");' class='entpermsAddDel_$permissionName' id=\"entpermsAddDel[" . $perVal["permissionId"] . "]\" " . $addDelCheck . " name=\"entpermsAddDel[" . $perVal["permissionId"] . "]\" value=\"1\"><label for=\"entpermsAddDel[" . $perVal["permissionId"] . "]\" " . $chk . "><span></span></label>";
        $html .= "<label class=\"labelText\" for=\"entpermsAddDel[" . $perVal["permissionId"] . "]\">Add/Delete</label>";
        $html .= "</div>";
    }
    $html .= "</div>";
  
    $returnData = array("html" => $html);
    return $returnData;
}

require_once("/var/www/lib/broadsoft/adminPortal/getSystemDomains.php");


//Code added @ 11 June 2019
require_once("/var/www/lib/broadsoft/adminPortal/util/ConfigUtil.php");
$bwAppServerForCluster = appsServer;    
$configUtilObj = new ConfigUtil($db, $bwAppServerForCluster);
$clusterList = $configUtilObj->getClustersList();
//End code
?>
<script>
    function checkPermissionCheck(className){
        var className = "."+className;
        $(className).prop('checked', true);
    }
     
    function unCheckPermission(permissionName){
        var clsNameForMod    = ".entpermsMod_"+permissionName;
        var clsNameForAddDel = ".entpermsAddDel_"+permissionName;
        $(clsNameForMod).prop('checked', false);
        $(clsNameForAddDel).prop('checked', false);
    }
   
/* Tooltip show/hide on hover & click */

function tooltipShowHideHover(element){
	var dataId = element.children('div').attr('id');
     var isCollapsed= element.find('h4:first').find('a:first').hasClass("collapsed");
     if(isCollapsed) {
    	$('#'+dataId).removeClass("hideShow");
     } else{
      $('#'+dataId).addClass("hideShow");
     }
 };
 function tooltipShowHideClick(element){
	 var dataId = element.children('div').attr('id');
     var isCollapsed= element.find('h4:first').find('a:first').hasClass("collapsed");
        if(isCollapsed) {
    	 $('#'+dataId).addClass("hideShow");
     } else  {
    	  $('#'+dataId).removeClass("hideShow");
     	}
	 };

	 
	function superUser()
	{ debugger;
		var isSuperUser = $("#superUser").val();
		if (isSuperUser == "1")
		{
			$("#bsSp").attr("disabled", "disabled");
			$("#bsGroupId").attr("disabled", "disabled");
			$("#userNameDropdown").attr("disabled", "disabled");
			$("#userNameText").removeAttr("disabled");
			$("#passwordText").removeAttr("disabled");
			$("#verifyPasswordText").removeAttr("disabled");
			$("#Selsp").attr("disabled", "disabled");
			$("#list1").attr("disabled", "disabled");
			$("#list2").attr("disabled", "disabled");
			$("#sortable_2").attr("disabled", "disabled");
			$("[id^=perms\\[]").attr("disabled", "disabled");
			$(".initialHide").show();
			$(".initialShow").hide();
                         $("#securityDomainDivAdd").hide();
                         $("#entsecurityDomainDivAdd").hide();
                         
		}
		else
		{
			$("#bsSp").removeAttr("disabled");
			$("#bsGroupId").removeAttr("disabled");
			$("#userNameDropdown").removeAttr("disabled");
			$("#userNameText").attr("disabled", "disabled");
			$("#passwordText").attr("disabled", "disabled");
			$("#verifyPasswordText").removeAttr("disabled");
			$("#Selsp").removeAttr("disabled");
			$("#list1").removeAttr("disabled");
			$("#list2").removeAttr("disabled");
			$("#sortable_2").removeAttr("disabled");
			$("[id^=perms\\[]").removeAttr("disabled");
			$(".initialHide").hide();
			$(".initialShow").show();
			if (securityDomainPattern != "") {
                         $("#securityDomainDivAdd").show();
         }
		}
	}
		
		
 $(".panel-heading").hover(function(){
	 var element = $(this);
	 tooltipShowHideHover(element);
 });
 $(".panel-heading").click(function(){
	 var element = $(this);
	 tooltipShowHideClick(element);
 });
 
    var securityDomainPattern = "<?php echo $securityDomainPattern; ?>";
    autoCompleteFunction();
    autoCompleteFunctionForEnterpriseSecurtyDomain();
	$(function()
	{ 
           //code for tooltip hover 
            new Popper(
		  document.getElementById('usersTitleAdd'),
		  document.getElementById('userTooltip')
            );

            new Popper(
		  document.getElementById('groupTitleAdd'),
		  document.getElementById('groupToolTip')
            );
    
            
           /* new Popper(
		  document.getElementById('InventoryTitleAdd'),
		  document.getElementById('inventoryTooltip')
            );*/
 
 
            // tooltip
            $('[data-toggle="tooltip"]').tooltip();
	
            $("#superUserDiv").trigger("click"); //when page load super user auto click or select @sollogics developer
		var bootstrapButton = $.fn.button.noConflict()
		$.fn.bootstrapBtn = bootstrapButton;

        $("#superUser").click(function(){
            if($("#superUser").is(':checked')) {
                $("#permissionsBlock").hide();
                $("#adminTypeAdd").attr("disabled", true);
            } else {
                $("#permissionsBlock").show();
                $("#adminTypeAdd").attr("disabled", false);
            }
        });

        $("#dialog").dialog($.extend({}, defaults, {
			width: 800,
			open: function(event) {
					setDialogDayNightMode($(this));
					$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('completeButton');
					$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
					$('.ui-dialog-buttonpane').find('button:contains("Add/Modify Another Admin")').addClass('addAnotherButton');
				},
			modal: true,
			position: { my: "top", at: "top" },
			resizable: false,
			closeOnEscape: false,
			buttons: {
				"Complete": function()
				{
					pendingProcess.push("Add Administrator");
					var formData = $("form#newAdminForm").serialize();
					$.ajax({
						type: "POST",
						url: "adminUsersEnterprise/addAdmin.php",
						data: formData,
						success: function(result)
						{
							if(foundServerConErrorOnProcess(result, "Add Administrator")) {
								return false;
			                }
							//$(window).scrollTop(0);
							$("#newAdminDialog").dialog("close");
							$("#dialog").dialog("open");
							$(".ui-dialog-titlebar-close", this.parentNode).hide();
							$(".ui-dialog-buttonpane", this.parentNode).show();
							$(":button:contains('Complete')").hide();
							$(":button:contains('Cancel')").hide();
							$(":button:contains('Add/Modify Another Admin')").show();
							if($("#superUser").is(":checked")){
								$("html, body").animate({ scrollTop: 0 }, 600);
							}else{
								$("html, body").animate({ scrollTop: "400px" }, 600);
							}
							$("#dialog").html(result);
							$("#dialog").append(returnLinkToEntMainPage);
						}
					});
				},
				"Cancel": function()
				{
					$(this).dialog("close");
				},
				"Add/Modify Another Admin": function()
				{
					$(this).dialog("close");
					$("html, body").animate({ scrollTop: 0 }, 600);
					$("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
					$("#loading").show();
					$(".navMenu").removeClass("active");
					$(this).addClass("active");
					$.ajax({
						type: "POST",
						url: "navigate.php",
						data: { module: 'adminUserEnt' },
						success: function(result)
						{
							$("#loading").hide();
							$("#mainBody").html(result);
						}
					});
				}
			}
		}));

		/* $("#sortable_1, #sortable_2").sortable({
			placeholder: "ui-state-highlight",
                        connectWith: "#sortable_1, #sortable_2",
			cursor: "crosshair",
			update: function(event, ui)
			{
				var monUsers = "";
				var order = $("#sortable_2").sortable("toArray");
				for (i = 0; i < order.length; i++)
				{
					monUsers += order[i] + ";";
				}
				$("#groups1").val(monUsers);
			}
		}).disableSelection(); */

$("#sortable_1, #sortable_2").multisortableList({
			placeholder: "ui-state-highlight",
                        connectWith: "#sortable_1, #sortable_2",
			cursor: "crosshair",
			update: function(event, ui)
			{
				var monUsers = "";
				var order = $("#sortable_2").sortable("toArray");
				for (i = 0; i < order.length; i++)
				{
					monUsers += order[i] + ";";
				}
				$("#groups1").val(monUsers);
			},
                        stop : function(){
                                            
						getArrayFromLi('#sortable_1');
						getArrayFromLi('#sortable_2');
                                                
					}
		}).disableSelection();


                
                
                $("#enterprise_available_user, #enterprise_assign_user").multisortableList({
			placeholder: "ui-state-highlight",
                        connectWith: "#enterprise_available_user, #enterprise_assign_user",
			cursor: "crosshair",
			update: function(event, ui)
			{
				var enterList = "";
				var orderEnt = $("#enterprise_assign_user").sortable("toArray");
				for (i = 0; i < orderEnt.length; i++)
				{
					enterList += orderEnt[i] + ";";
				}
				$("#enterpriseAssignUser").val(enterList);
			},
                        stop : function(){
                                            
                            getArrayFromLi('#enterprise_available_user');
                            getArrayFromLi('#enterprise_assign_user');

                        }
		}).disableSelection();


var insensitive = function(s1, s2) {
					 var s1lower = s1.toLowerCase();
					 var s2lower = s2.toLowerCase();
					 return s1lower > s2lower? 1 : (s1lower < s2lower? -1 : 0);
				}

				//make new html for the ul tag for ordering the li data
				var getArrayFromLi = function(ulId){
                                   
					var liArray = [];
					var liNewArrayUsers = [];
					var liStr = "";
					
					$(ulId+" li").each(function(){
						var el = $(this);
						liArray.push(el.text());
					});
					
					var liNewArray = liArray.sort(insensitive);
					$(ulId).html('');
					for (i = 0; i < liNewArray.length; i++)
					{
						liStr += '<li class="ui-state-default" id="'+liNewArray[i]+'">'+liNewArray[i]+'</li>';
					}
					$(ulId).html(liStr);
                                        /*alert('liStr - '+liStr);
                                        alert('liNewArray - '+liNewArray);
                                        alert('liNewArrayUsers - '+liNewArrayUsers);
					console.log(liNewArrayUsers);*/
				}
		//select service provider for group permissions (non-superusers only)
                
                //Code added @ 18 June 2019
                $("#entAdministratorClusterId").change(function()
		{
			$("#enterprise_available_user").empty();
			$("#enterprise_assign_user").empty();
                        $("#entsecurityDomain").empty();
                        $("#entsecurityDomain").val('');
                        
                        var selectCluster = $("#entAdministratorClusterId").val();	
                        //alert('selectClusterEnterprise - ' + selectCluster);
			$.ajax({
				type: "POST",
				url: "adminUsersEnterprise/getAllEnterprisesClusterBise.php",
				data: { action: 'getServiceProviderList', clusterName: selectCluster },
				success: function(result)
				{
                                        //console.log(result);
					$("#enterprise_available_user").append(result);
				}
			});
                        
                        var autoComplete = new Array();
                        $.ajax({
                                type: "POST",
                                url: "adminUsersEnterprise/getAllEnterprisesClusterBise.php",
                                data: { action: 'getSysSecurityDomainList', clusterName: selectCluster},
                                success: function(result)
                                {
                                        //console.log(result);
                                        var explode = result.split(":");
                                        for (a = 0; a < explode.length; a++)
                                        {
                                                autoComplete[a] = explode[a];
                                        }
                                        $(".entsecurityDomainAdd").autocomplete({
                                                source: autoComplete,
                                                select: function( event , ui ) {
                                                         $(this).val($.trim(ui.item.label));
                                                         return false;
                                         }
                                        });
                                }
                        });
			// getSpSecurityDomainList(sp);
		});
                //End code
                
                //Code added @ 13 June 2019
                $("#entGroupAdministratorClusterId").change(function () {
                   $("#sortable_1").empty();
                   $("#sortable_2").empty();
                   $("#securityDomain").val('');
                   
                   var selectedCluster = $('#entGroupAdministratorClusterId').val();
                   //alert('selectedClusterGroup - '+selectedCluster);            
                   if (selectedCluster != "") {

                       $.ajax({
                           type: "POST",
                           url: "changeConnectionForSelCluster.php",
                           data: {clusterName: selectedCluster, chooseGroup: "true"},
                           success: function (result) {
                               //console.log(result);
                               $('#Selsp').html(result);
                               //alert('Testing123');
                           }
                       });
                       
                       var autoComplete = new Array();
                        $.ajax({
                                type: "POST",
                                url: "adminUsersEnterprise/getAllGroupsClusterBise.php",
                                data: { action: 'getSysSecurityDomainList', clusterName: selectedCluster},
                                success: function(result)
                                {
                                        //console.log(result);
                                        var explode = result.split(":");
                                        for (a = 0; a < explode.length; a++)
                                        {
                                                autoComplete[a] = explode[a];
                                        }
                                        $(".securityDomainAdd").autocomplete({
                                                source: autoComplete,
                                                select: function( event , ui ) {
                                                         $(this).val($.trim(ui.item.label));
                                                         return false;
                                         }
                                        });
                                }
                        });
                       
                       
                       
                   }
               });
        //End code
                
                
		$("#Selsp").change(function()
		{
			$("#sortable_1").empty();
			$("#sortable_2").empty();
                        
                        var selectCluster = $("#entGroupAdministratorClusterId").val();		
			var sp = $(this).val();
			$.ajax({
				type: "POST",
				url: "adminUsersEnterprise/getAllGroupsClusterBise.php",
				data: { getSP: sp, clusterName: selectCluster },
				success: function(result)
				{
					$("#sortable_1").append(result);
				}
			});
			// getSpSecurityDomainList(sp);
		});

		
// 		$("#SelspWithSecurityDomain").change(function()
// 				{
// 					$("#securityDomain").empty();
// 					var sp = $(this).val();
// 					$.ajax({
// 						type: "POST",
// 						url: "adminUsersEnterprise/getAllGroups.php",
// 						data: { action: 'getSpSecurityDomainList', getSP: sp},
// 						success: function(result)
// 						{
// 							var result = JSON.parse(result);
// 							var options = "";
// 							options += "<option value='' selected=''>None</option>";
// 							if(result.Error == "") {
// 								result = result.Success.domain;
// 								$.each(result, function(index, value){
// 									options += "<option value='"+value+"'>" +value+ "</option>";
// 								});
// 							}
// 							$("#securityDomain").html(options);
// 						}
// 					});
					 
// 				});
		
		
		//select service provider for username (non-superusers only)
		$("#bsSp").change(function()
		{
			$("#bsGroupId").empty();
			$("#userNameDropdown").empty();

			var bsSp = $(this).val();
			$.ajax({
				type: "POST",
				url: "adminUsersEnterprise/getAllGroups.php",
				data: { getSP: bsSp, dropdown: "true" },
				success: function(result)
				{
					$("#bsGroupId").append("<option value=\"\"></option>");
					$("#bsGroupId").append(result);
				}
			});
		});

		//select group for username (non-superusers only)
		$("#bsGroupId").change(function()
		{
			$("#userNameDropdown").empty();

			var bsSp = $("#bsSp").val();
			var bsGroupId = $(this).val();
			$.ajax({
				type: "POST",
				url: "adminUsersEnterprise/getAllAdmins.php",
				data: { getSP: bsSp, getGroup: bsGroupId },
				success: function(result)
				{
					$("#userNameDropdown").append("<option value=\"\"></option>");
					$("#userNameDropdown").append(result);
				}
			});
		});

		//disable/enable fields based on whether user is a superuser

		$("#superUser").click(function()
		{
			superUser();
		});

		//check all permissions checkboxes
		$("#checkAll").click(function()
		{
			$("[id^=perms\\[]").prop("checked", true);
		});

		superUser();
		
	});

       //code added @ 27 Feb 2019 
       //Checked Select All if all group permission is checked
       $("#adminPermissions :checkbox").change(function() {
        var names = {};
        $('#adminPermissions :checkbox').each(function() {
            names[$(this).attr('name')] = true;
        });
        var count = 0;
        $.each(names, function() { 
            count++;
        });
        if ($('#adminPermissions :checkbox:checked').length === count) {
            $("#permissionRadioSelectAll").prop("checked",true);
        }
        }).change();
        
        
        //Checked Select All if all enterprise/group permission is checked
            $("#enterpriseGroupPermissions :checkbox").change(function() {
            var names = {};
            $('#enterpriseGroupPermissions :checkbox').each(function() {
                names[$(this).attr('name')] = true;
            });
            var count = 0;
            $.each(names, function() { 
                count++;
            });
            if ($('#enterpriseGroupPermissions :checkbox:checked').length === count) {
                $("#entpermissionRadioSelectAll").prop("checked",true);
            }
            }).change();

            $("[id^=entperms]").click(function() {
                    $("input[name='entpermissionRadio']").prop("checked", false);
            });

            function enableDefaultPerCheckBox() {
                $("#defaultPermissionAdd").prop("checked",false);
                $("#defaultPermissionAdd").prop("disabled", false);
                $(".defaultPermCheckLabel").removeClass("opacityDisabled");
                //alert('1');
                 //$("#defaultPermissionAdd").prop("checked",false).prop("disabled", false).removeClass("opacityDisabled");
            }
       //End code

	function defaultPerCheckBox() {
		 $("#defaultPermissionAdd").prop("checked",false);
		 $("input[name='permissionRadio']").prop("checked", false);
	}
	
	$("[id^=perms]").click(function() {
		$("input[name='permissionRadio']").prop("checked", false);
		//defaultPerCheckBox();
                enableDefaultPerCheckBox();
	});
	
	$("#use_ldap_authentication_new").click(function(){
		if($(this).prop('checked') == true) {
			$(".passRequired").hide();
			$(".verifyPassRequired").hide();
		} else {
			$(".passRequired").show();
			$(".verifyPassRequired").show();
		}
	});


	function showHideAccordian(result, permissionRadio) {
            //alert('Hello Hi');
		 var selectAll = false;
		 if(permissionRadio != "") {
			 selectAll = permissionRadio == "Select All" ? true : false;
		}
                
                //console.log(result);
		 
		 if($.inArray("Users", result) > -1 || selectAll) {
				$("#usersTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#userPerDivAdd").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         if($.inArray("Groups", result) > -1 || selectAll) {
         		$("#groupTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#groupPerDivAdd").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         if($.inArray("Group", result) > -1 || selectAll) {
         		$("#groupEntTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#groupEntPerDivAdd").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         if($.inArray("Devices", result) > -1 || selectAll) {
         		$("#devicesTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#devicesPerDivAdd").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         if($.inArray("Services", result) > -1 || selectAll) {
         		$("#servicesTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#servicesPerDivAdd").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         if($.inArray("Supervisory", result) > -1 || selectAll) {
         		$("#supervisoryTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#supervisoryPerDivAdd").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         if($.inArray("Inventory", result) > -1 || selectAll) {
         		$("#InventoryTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#InventoryPerDivAdd").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         
         if($.inArray("Administrators", result) > -1 || selectAll) {
         		$("#AdministratorsTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#AdministratorsPerDivAdd").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         
         if($.inArray("Enterprise", result) > -1 || selectAll) {
         		$("#entPermTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#entPermFeaturePerDiv").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         
	}
	
	var permissionRadio = "";
	$("input[name='permissionRadio']").click(function() {
		permissionRadio =  $(this).val();
		showHideAccordian(result = "", permissionRadio);
	});
        
        var entpermissionRadio = "";
	$("input[name='entpermissionRadio']").click(function() {
		entpermissionRadio =  $(this).val();
		showHideAccordian(result = "", entpermissionRadio);
	});
        

	
	$("#defaultPermissionAdd").click(function() {
		$("input[name='permissionRadio']").prop("checked", false);
		if($(this).prop('checked') == true){
			var adminType = $('#adminTypeAdd option:selected').val();
			var value = {'value': adminType}; 
			updatePermissions(value);                        
		} else {
			//$("[id^=permsUpdate]").prop("checked", false);
		}
	});

	// accordian
	function getAllPermissionscategories(defaultPermission) {
		$("input[name='permissionRadio']").prop("checked", false);
		$(".collapsideDivAdd").removeClass('in').attr("aria-expanded","false");
		$("[id^=filtersStatusTitle]").addClass("collapsed").attr("aria-expanded","false");
		
		$.ajax({
            type: "POST",
            url: "adminUsersEnterprise/getPermissions.php",
            data: { action: 'getAllPermissionscategories', defaultPermission: defaultPermission},
            success: function(result)
            {
                var result = JSON.parse(result);
                showHideAccordian(result, permissionRadio = "");
            }
        });
	}

	
    function updatePermissions(radio) {
        //alert('updatePermissions - '+radio.value);
        if (radio.value == "Select All") {
            $("[id^=perms]").prop("checked", true);
            $("#defaultPermissionAdd").prop("checked",false);
        }
        else if (radio.value == "Clear") {
            $("[id^=perms]").prop("checked", false);
            $("#defaultPermissionAdd").prop("checked",false);
        }
        else if(radio.value !=""){
        	onAdminCheckPermission(radio.value);
        }
//         else if(radio.value == "1" || radio.value == "2" || radio.value == "3" || radio.value == "3" || radio.value == "4") 
//         {
//        	  	onAdminCheckPermission(radio.value);
//        	}
        else if(radio.value == "") {
        	$("[id^=perms]").prop("checked", false);
       	 	$("#defaultPermissionAdd").prop("checked",false);
       		$("input[name='permissionRadio']").prop("checked", false);
			return false;
		}
         else if($("#defaultPermissionAdd").prop('checked') == false) {
        	$("input[name='permissionRadio']").prop("checked", false);
			return false;
		}else {
        	onAdminCheckPermission(radio.value);
        }
       
    }
    
    
     function updateEnterprisePermissions(radio) {
        //alert('updatePermissions - '+radio.value);
        if (radio.value == "Select All") {
            $("[id^=entperms]").prop("checked", true);
        }
        else if (radio.value == "Clear") {
            $("[id^=entperms]").prop("checked", false);
        }
        
        
        
        
        else if(radio.value == "") {
        	$("[id^=entperms]").prop("checked", false);
       		$("input[name='entpermissionRadio']").prop("checked", false);
			return false;
        }
    }
    
    
    /* 
    * method used for Permissions checked
    * Check permission according to Admin type change , selected
    * checkbox default checked disabled
    */
    
	var onAdminCheckPermission = function(adminTypeValue){
        //alert('adminTypeValue - '+adminTypeValue);
    	$.ajax({
             type: "POST",
             url: "adminUsersEnterprise/getPermissions.php",
             data: { permissionSet:adminTypeValue },
             success: function(result)
             {
                //alert('result - '+result);$("[id^=permsUpdate]").prop("checked", false);
                $("[id^=perms]").prop("checked", false);
             	getAllPermissionscategories(result);
                 var values = result.split(",");
                 for (var i = 0; i < values.length; i++) {
                     
                      var id = "perms[" + values[i] + "]";
                     document.getElementById(id).checked = true;
                     $("#defaultPermissionAdd").prop("checked",true).prop("disabled", true);
                     //$(".defaultPermCheckLabel").addClass("opacityDisabled");
//                     var params = values[i].split(":");
//                     var id = "perms[" + params[0] + "]";
//                     document.getElementById(id).checked = (params[1] == "1");                	
                 }
             }
         });
	};

	function  autoCompleteFunction() {
                
		var autoComplete = new Array();
                var selectedCluster = $('#entGroupAdministratorClusterId').val();
		$.ajax({
			type: "POST",
			url: "adminUsersEnterprise/getAllGroupsClusterBise.php",
			data: { action: 'getSysSecurityDomainList', clusterName: selectedCluster},
			success: function(result)
			{
				var explode = result.split(":");
				for (a = 0; a < explode.length; a++)
				{
					autoComplete[a] = explode[a];
				}
				$(".securityDomainAdd").autocomplete({
					source: autoComplete,
					select: function( event , ui ) {
						 $(this).val($.trim(ui.item.label));
						 return false;
			         }
				});
			}
		});
	}
    

        
        function  autoCompleteFunctionForEnterpriseSecurtyDomain() {
		var autoComplete = new Array();
                var selectCluster = $("#entAdministratorClusterId").val();
		$.ajax({
			type: "POST",
			url: "adminUsersEnterprise/getAllGroupsClusterBise.php",
			data: { action: 'getSysSecurityDomainList', clusterName: selectCluster},
			success: function(result)
			{
				var explode = result.split(":");
				for (a = 0; a < explode.length; a++)
				{
					autoComplete[a] = explode[a];
				}
				$(".entsecurityDomainAdd").autocomplete({
					source: autoComplete,
					select: function( event , ui ) {
						 $(this).val($.trim(ui.item.label));
						 return false;
			         }
				});
			}
		});
	}
    


    //Code added @ 13 Feb 2019   
    function selectAdminUserDiv(selecter) {
       
		
		if(selecter === 'superUserDiv')
                {

                        $("#superUser").val('1');
                        
                        $("#changImg1").css('background-image','url(images/NewIcon/superadd_over.png)');
                        $('#changImg1').css('color','#ffb200');
                        
                        $('#changImg2').css('background-image','url(images/NewIcon/adminadd_rest.png)');
                        $('#changImg2').css('color','#6ea0dc');
                        
                        $('#changImg3').css('background-image','url(images/NewIcon/groupadd_rest.png)');
                        $('#changImg3').css('color','#6ea0dc');
                        
                        $(".enterprisePermissionBlock").hide();
                        $(".groupPermissionBlock").hide();
                        
                        
                        
		}
		else if(selecter === 'enterpriseAdminDiv')
                {

                        $("#superUser").val('3');                        
			$("#changImg2").css('background-image','url(images/NewIcon/adminadd_over.png)');
                        $('#changImg2').css('color','#ffb200');
                        
                        $('#changImg1').css('background-image','url(images/NewIcon/superadd_rest.png)');
                        $('#changImg1').css('color','#6ea0dc');
                        
                        $('#changImg3').css('background-image','url(images/NewIcon/groupadd_rest.png)');
                        $('#changImg3').css('color','#6ea0dc'); 
                        //alert('Hi');
                        $(".enterprisePermissionBlock").show();
                        //alert('Hello');
                        $(".groupPermissionBlock").hide();
                        
                        
			$("#enterprise_available_user").empty();
			$("#enterprise_assign_user").empty();
			
                        
                        var selectCluster = $("#entAdministratorClusterId").val();
			$.ajax({
				type: "POST",
				url: "adminUsersEnterprise/getAllEnterprisesClusterBise.php",
				data: { action: 'getServiceProviderList', clusterName: selectCluster },
				success: function(result)
				{
                                        //console.log(result);
					$("#enterprise_available_user").append(result);
				}
			});
			//getSpSecurityDomainList(sp);
		
                        //////////////////////////////////
                        
                        
                        
		}

		else if(selecter === 'groupAdminDiv') 
                {
                        $("#superUser").val('0');
                        
			$("#changImg3").css('background-image','url(images/NewIcon/groupadd_over.png)');
                        $('#changImg3').css('color','#ffb200');
                        
                        $('#changImg1').css('background-image','url(images/NewIcon/superadd_rest.png)');
                        $('#changImg1').css('color','#6ea0dc');
                        
                        $('#changImg2').css('background-image','url(images/NewIcon/adminadd_rest.png)');
                        $('#changImg2').css('color','#6ea0dc'); 
                        
                        $(".groupPermissionBlock").show();
                        $(".enterprisePermissionBlock").hide();
                        
		}

		superUser();
    }
    //End code
	
</script>
<?php
	require_once("/var/www/lib/broadsoft/adminPortal/getServiceProviders.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getSystemDomains.php");

	$licenseArray = array();
	if(isset($license["vdmLite"]) && $license["vdmLite"] == "false"){
		$licenseArray[] = "vdmLight";
		$licenseArray[] = "vdmAdvanced";
	}
	
	$systemConfig = new DBSimpleReader($db, "systemConfig");
	$ldapAuthentication = $systemConfig->get("ldapAuthentication");

	$fields = "";
	$exp = "EXPLAIN permissions";
	$qpo = $db->query($exp);
	while ($r = $qpo->fetch(MYSQL_ASSOC))
	{
	    //if($r["Field"] != 'vdmLight') {
	    if($r["Field"] != 'vdmLight') {
	    	if(!in_array($r["Field"], $licenseArray)){
	    		$fields .= $r["Field"] . ",";
	    	}
	    } else {
	        //if(isset($license["vdmLite"]) && $license["vdmLite"] =="true"){
	    	if(!in_array($r["Field"], $licenseArray)){
	            $fields .= $r["Field"] . ",";
	        }
	    }
	}
	//echo "<pre>"; print_r($fields); die;
	$fields = substr($fields, 3, -1);
	
?>
<style>
	.uiModalAccordian {
   /* width: 77%;
    float: left;
    padding: 0 8px; */
}
.borderUp {
    border-bottom: 1px solid #ccc;
    height: 0 !important;
    margin: 25px 0 !important;
}


.hideShow{display:none;}

.averistar-bs3 .panel-default {
    margin-bottom: 35px;
}

/*modify tooltip */

.ui-dialog .ui-dialog-content{
	overflow: hidden !important;
 }
 
 
span#serviceTooltip, span#deviceTooltip, span#SupervisoryTooltip {
    bottom: 0% !important;
    top: inherit;
    margin-left: -386px;
    /* margin-left: -386px; */
}

#usersTitleAdd #usertool1 span.tooltiptextUsers.tooltiptext, #groupTitleAdd #usertool2 span.tooltiptextGroups.tooltiptext {
    margin-left: -1080px !important;
    margin-top: -43px;
}
  
</style>
<form action="#" method="POST" name="newAdminForm" id="newAdminForm" class="fcorn-registerTwo">
	<h2 class="adminUserText">Add Administrators</h2>
	
        <!-- 
        <div class="row">
		<div class="">
			<div class="col-md-12">
				<div class="form-group">
					<input type="hidden" name="superUser" value="0">
					<input type="checkbox" name="superUser" id="superUser" value="1"><label for="superUser"><span></span></label>
					<label class="labelText" for="superUser">Super User</label>
				</div>
			</div>
		</div>
	</div>
        -->
        <div class="row">
		<div class="">
			<div class="col-md-12">
				<div class="icons-div userAdd-icons-div">
                                     <ul style="" class="feature-list">
                                        <li class="changeOnMouseHover">
                                            <div class="adminUserMhover" id="superUserDiv" onclick="selectAdminUserDiv('superUserDiv')">
                                                <div id="changImg1">Super User</div>                
                                            </div></li>
                                        <li>
                                            <div class="adminUserMhover" id="enterpriseAdminDiv" onclick="selectAdminUserDiv('enterpriseAdminDiv')">
                                                <div id="changImg2">Enterprise Admin</div>
                                            </div></li>
                                        <li>
                                            <div class="adminUserMhover" id="groupAdminDiv" onclick="selectAdminUserDiv('groupAdminDiv')">
                                                <div id="changImg3">Group Admin</div>                
                                            </div>
                                                </li>
                                    </ul>
                                    
                                    <input type="hidden" name="superUser" id="superUser" value="" />
                                </div>
			</div>
		</div>
	</div>
        
        
        <?php
    	if ($ldapAuthentication == "true") {
    		?>
    		<div class="row">
        		<div class="col-md-6">
        			<div class="form-group">
    				<div class="marginTopSe">&nbsp;</div>	
            			<input type="hidden" name="use_ldap_authentication" id="use_ldap_authentication_newAdmin1" value="0" />
            			<input type="checkbox" name="use_ldap_authentication" id="use_ldap_authentication_new" style="width: auto;" value="1" CHECKED /><label for="use_ldap_authentication_new"><span></span></label>
        				<label class="labelText" for="use_ldap_authentication_new">LDAP Authentication</label>
        			</div>
        		</div>
    		</div>	
    		<?php
    	} else {
    		?>
    		<input type="hidden" name="use_ldap_authentication" id="use_ldap_authentication" value="0" />
    		<?php
    	}
    	?>

	<?php
	if ($permissionsBwAuthentication == "true") {
	    ?>
                    <div class="row">
                        <div class="col-md-6 initialShow">
                            <div class="form-group">
                                <div class="form-group">
                                    <label class="labelText" for="bsSp">Service Provider</label>
                                    <div class="dropdown-wrap">
                                        <select name="bsSp" id="bsSp">
                                            <option value=""></option>
                                            <?php
                                            foreach ($sps as $v)
                                            {
                                                    echo "<option value=\"" . $v . "\">" . $v . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div><!--service provider div cloased -->

                        <div class="col-md-6 initialShow">
                            <div class="form-group">
                                <label class="labelText" for="bsGroupId">Group</label>
                                <div class="dropdown-wrap">
                                    <select name="bsGroupId" id="bsGroupId"></select>
                                </div>
                            </div>
                        </div>

                    </div>
			 
                    <div class="row">
                        <div class="col-md-6">
                            <div class="initialShow">
                                <div class="form-group">
                                    <label class="labelText" for="userNameDropdown">Username</label> <span class="required">*</span>
                                    <div class="dropdown-wrap">
                                        <select name="userName" id="userNameDropdown">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="initialHide">
                                <div class="form-group">
                                    <label class="labelText" for="userNameText">Username</label> <span class="required">*</span>
                                    <input type="text" name="userName" id="userNameText" size="35" maxlength="32">
                                </div>
                            </div>
                        </div>
                     
<!--                         <div class="col-md-6 initialHide"> -->
<!--                             <label class="labelText" for="passwordText">Password</label> <span class="required">*</span> -->
<!--                             <div class="form-group"> -->
<!--                                 <input type="password" name="password" id="passwordText" size="35"> -->
<!--                             </div> -->
<!--                         </div> -->
                    
                    </div>
        
                   <!-- <div class=" initialShow ">
				<label class="labelText" for="bsSp">Service Provider</label>
			</div>
			<div class="inputText initialShow dropdown-wrap">   
				<select name="bsSp" id="bsSp">
					<option value=""></option>
					<?php
				/*	foreach ($sps as $v)
					{
						echo "<option value=\"" . $v . "\">" . $v . "</option>";
					} */
					?>
				</select>
			</div>
			<div class=" initialShow ">
				<label class="labelText" for="bsGroupId">Group</label>
			</div>
			
			<div class="inputText initialShow dropdown-wrap">   
				<select name="bsGroupId" id="bsGroupId"></select>
			</div>
			
			<div class="initialShow">
				<label class="labelText" for="userNameDropdown">Username</label> <span class="required">*</span>
			</div>
			<div class="inputText initialShow dropdown-wrap">
				<select name="userName" id="userNameDropdown">
				</select>
			</div>
			<div class="row initialHide">
				<label class="labelText" for="userNameText">Username</label><span class="required">*</span>
			</div>
			<div class="inputText initialHide">
				<input type="text" name="userName" id="userNameText" size="35" maxlength="32">
			</div>

			<div class="initialHide">
				<label class="labelText" for="passwordText">Password</label> <span class="required">*</span>
			</div>
			<div class="inputText initialHide">
				<input type="password" name="password" id="passwordText" size="35">
			</div> -->
			<?php
                          
		}
		else
		{
			?>		
	<div class="row">
		<div class="">
			<div class="col-md-6">
				<div class="form-group">
					<label class="labelText" for="userName">Username</label> <span class="required">*</span>
					<input type="text" name="userName" id="userName" size="35" maxlength="32">
				</div>
			</div>
			<!--<div class="col-md-6">
			<div class="form-group">
				<label class="labelText" for="password">Password</label> <span class="required">*</span>
				<input type="password" name="password" id="password" size="35">
			</div>
			</div>-->
			<div class="col-md-6">
    		<div class="form-group">
    	<label class="labelText" for="user_cell_phone_number">Cell Phone Number</label>
    	<input class="form-control" type="tel" name="user_cell_phone_number" id="user_cell_phone_number" size="35" maxlength="65">
    		</div>
    	</div>
		</div>
	</div>

			<?php
		}
	?>
	

	<div class="row">
	<div class="">
	 <?php if ($isAdminPasswordFieldVisible == "true") { ?>
    <div class="col-md-6">
		<div class="form-group">
		<?php if ($permissionsBwAuthentication == "true") { ?>
			<div class="initialHide"><label class="labelText" for="passwordText">Password</label>
				<span style="display:<?php echo $ldapAuthentication == 'true' ? 'none' : 'block';?>" class="required passRequired">*</span>
			</div>
			<div class="initialHide"><input type="password" name="password" id="passwordText" size="35"></div>

		<?php } else {
			?>
			<label class="labelText" for="password">Password</label>
				<span style="display:<?php echo $ldapAuthentication == 'true' ? 'none' : 'block';?>" class="required passRequired">*</span>
			<input type="password" name="password" id="password" size="35">
		<?php }?>
		</div>
	</div>
	
	<!-- 	verify password -->
	<div class="col-md-6">
		<div class="form-group">
		<?php if ($permissionsBwAuthentication == "true") { ?>
			<div class="initialHide"><label class="labelText" for="passwordText">Verify Password</label>
				<span style="display:<?php echo $ldapAuthentication == 'true' ? 'none' : 'block';?>" class="required verifyPassRequired">*</span>
			</div>
			<div class="initialHide"><input type="password" name="verifyPassword" id="verifyPasswordText" size="35"></div>

		<?php } else {
			?>
			<label class="labelText" for="password">Verify Password</label>
				<span style="display:<?php echo $ldapAuthentication == 'true' ? 'none' : 'block';?>" class="required verifyPassRequired">*</span>
			<input type="password" name="verifyPassword" id="verifyPassword" size="35">
		<?php }?>
		</div>
	</div>
	
	<?php } ?>
	
	
</div>
</div>
<!-- Merged -->
<div class="row">
	<div class="">
		<div class="col-md-6">
			<div class="form-group">
			<label class="labelText" for="name">Name</label> <span class="required">*</span>
			<input type="text" name="name" id="name" size="35" maxlength="65">

			</div>
		</div>
			
		<div class="col-md-6">
			<div class="form-group">
				<label class="labelText" for="emailAddress">Email Address</label> <span class="required">*</span>
				<input type="text" name="emailAddress" id="emailAddress" size="40" maxlength="64">
			</div>
		</div>
	</div>
</div>

<div class="row ">
    <div class="">
    	
		<div class="col-md-6">
			<div class="form-group">
			<input type="hidden" name="use_sms_every_login" value="0">
			<div class="marginTopSe">&nbsp;</div>
			<input style="" type="checkbox" name="use_sms_every_login" id="use_sms_every_login" value="1"><label for="use_sms_every_login"><span></span></label>
			<label class="labelText" for="use_sms_every_login">Use SMS every login</label>
			</div>
		</div>
    
    	<div class="col-md-6">
        	<div class="form-group">
			<div class="marginTopSe">&nbsp;</div>
            	<input type="hidden" name="use_sms_on_password_reset" value="0">
            	<input type="checkbox" name="use_sms_on_password_reset" id="use_sms_on_password_reset" value="1"><label for="use_sms_on_password_reset"><span></span></label>
            <!-- <div class="<?php //echo ($ldapAuthentication == "true") ? "quarterDesc" : "leftDesc2"; ?>">  --> 	
            	<label class="labelText" for="use_sms_on_password_reset">Require SMS to reset</label>
            <!--</div> -->
        	</div>
    	</div>
		
    </div>	
</div>

<div class="vertSpacer initialShow borderUp">&nbsp;</div>

    <div class="groupPermissionBlock">
<!--New code found Start-->
	<div class="row" id="permissionsBlock">
	   <div class="col-md-6">
           <div class="form-group">
				<label class="labelText">Admin Type: <span class="required">*</span></label>
				<div class="dropdown-wrap">
					<select id="adminTypeAdd" class="form-control" name="checkPermissions" onchange="updatePermissions(this)">
						 <?php echo createPermissionsRadioButtons(); ?>
					</select>
				</div>
			</div>
		</div>
    
		<div class="col-md-6 initialShow">
			<div class="form-group">
				<div class="marginTopSe">&nbsp;</div>	
				<input type="checkbox" id="defaultPermissionAdd" /><label class="defaultPermCheckLabel" for="defaultPermissionAdd"><span></span></label>
			   <label class="labelText" for="">Default Permissions</label>
			</div>
		</div>
   </div>
<!--New code found End-->
  
<div class="row">
    
<?php 
$clusterSupport  =  (isset($bwClusters) && $bwClusters == "true") && $enableClusters == "true" ? true : false;
if($clusterSupport){
?>    
<div class="col-md-12">
    <div class="form-group">
        <label class="labelText">Select Cluster</label>
        <div class="dropdown-wrap">   
            <?php 
                $clusterIdName  = "entGroupAdministratorClusterId";
                $clusterClsName = "form-control";
                $selectedCluster = $_SESSION['selectedCluster'];
                echo $clusterDropdownHtml = $configUtilObj->createClustersDropdownList($clusterIdName, $clusterClsName ,$clusterList, $selectedCluster);
            ?>                                                    
        </div>
    </div>
</div>
<?php 
}
?>
                                   
    
    
    
    
    
<div class="">
	<div class="col-md-6">
        <div class="form-group" id="securityDomainDivAdd" style="display: <?php echo (hasSecurityDomain() ? "block" : "none"); ?>">
                    <label class="labelText" for="securityDomain">Security Domain <span class="required">*</span></label>
                            <input type="text" class="form-control autoFill securityDomainAdd" name="securityDomain" id="securityDomain">
            </div>
        </div>
  </div>
  </div>

        <div class="row">
        <div class="">
                <div class="col-md-6" style="display: <?php echo (hasSecurityDomain() ? "none" : "block"); ?>">
                <div class="form-group" id="spDiv" style="display: <?php echo (hasSecurityDomain() ? "none" : "block"); ?>">
                    <div class="initialShow">
                        <label class="labelText" for="Selsp">Enterprise</label><span class="required">*</span>
                    </div>
                    <div class="initialShow dropdown-wrap">
                    <select name="Selsp" id="Selsp">
                        <option value=""></option>
                        <?php
                        foreach ($sps as $v)
                        {
                            echo "<option value=\"" . $v . "\">" . $v . "</option>";
                        }
                        ?>
                    </select>
                    </div>
                </div>
                </div>
        </div>
        </div>

    <div class="row" id="groupDiv" style="display: <?php echo (hasSecurityDomain() ? "none" : "block"); ?>">
    	<div class="">
		    <div class="col-md-12">
		    <div class="form-group">
		        <div class="initialShow"><label class="labelText">Groups <span class="required">*</span></label></div>
        		<input type="hidden" name="groups1" id="groups1" value="">
			</div>
			</div>
		</div>
		<div class="">
		    <div class="col-md-6 availNCurrentGroup">
			    <div class="form-group initialShow">Available Groups</div>
	        </div>
	        <div class="col-md-6 availNCurrentGroup">
		        <div class="form-group initialShow">Current Groups</div>
        	</div>
                </div>
       
		<div class=""> 
			<div class="col-md-6"> 
				<div class="form-group">
			        <div class="initialShow">
			            <div id="list1" class="scrollableList1">
			                <ul id="sortable_1" class="connectedSortable connectedSortableCustom">
                </ul>
            </div>
        </div>
		        </div>
	        </div>
	        <div class="col-md-6">
		        <div class="form-group">
			        <div class="initialShow">
			            <div id="list2" class="scrollableList2">
			                <ul id="sortable_2" class="connectedSortable connectedSortablecustom">
                </ul>
            </div>
        </div>
		        </div>
	        </div>
	    </div>
    </div>
    

<!--  

    <div id="permissionsBlock">
        <div class="diaPL diaP3">
	<label>Permissions: <span class="required">*</span></label></div>
        <?php //echo createPermissionsRadioButtons(); ?>
        <div class="diaPL diaP18"></div>
    </div> -->
    	<!-- Permissions -->
		
		
		
     <!-- New code found start-->  
	<div class="row initialShow">
		<div class="col-md-6" style="padding-left: 0;">
			<div class="col-md-6 voiceMsgTxt">
			 <div class="form-group">
			<label class="labelText">Permissions</label><span class="required">*</span>
			<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="At least one permission must be selected."><img class="infoIcon" src="images/NewIcon/info_icon.png"></a>
			</div>
			</div>
			<div class="col-md-6 voiceMsgRBtn">
			<div class="col-md-6 rBtnYes" style="padding-right: 0;">
				<div class="form-group">
				 <input type="radio" name="permissionRadio"	id="permissionRadioSelectAll" class="checkbox-lg" value="Select All" onclick="updatePermissions(this)">
				 <label class="labelText labelTextMargin" for="permissionRadioSelectAll"><span></span>Select All</label>
				</div>
			</div>

			 <div class="col-md-6 rBtnNo" style="padding-right: 0;">
				 <div class="form-group">
					<input type="radio" name="permissionRadio" id="permissionRadioClearAll" class="checkbox-lg" value="Clear" onclick="updatePermissions(this)">
					<label class="labelText" for="permissionRadioClearAll"><span></span>Clear All</label>
				</div>
			</div>
			</div>
		</div>
	</div>
                		
	<?php 
	$permissionCat = getPermissionsFromPermissionsCategories($db);
	//echo "<pre>";
        //print_r($permissionCat);
	// Users Permissions
	$toolTip = "<span class='tooltiptextUsers tooltiptext' id='userTooltip'>";
	$userCategoryPermissions = "<div>";
        
        //echo "<pre>";
        //print_r($permissionCat['Users']);
//        /die();
        
        
	foreach($permissionCat['Users'] as $perKey => $perVal) {
	    $toolTip .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $createPermission = createPermissionCheckboxes($perVal);
	    $userCategoryPermissions .= $createPermission["html"];
	}
	$userCategoryPermissions .= "</div>";
	$toolTip .= "</span>";
	?>
	
	<div id="adminPermissions" class="initialShow">
		
        <div class="row">
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="userPerRowAdd">
                		<div class="panel-heading" role="tab" id="usersTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#userPerDivAdd" aria-expanded="false"
                				   aria-controls="userPerDivAdd">User Permissions
                				   </a>
                			</h4>
        <!--         	tool tip -->
       					 	<div id="usertool1">
            					<?php echo $toolTip; ?>
                    		</div>
        				</div>
                		<div id="userPerDivAdd" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="usersTitleAdd">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $userCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
		
<!-- Groups Permissions -->
<?php 
	$toolTipGrpup = "<span class='tooltiptextGroups tooltiptext' id='groupToolTip'>";
	$groupCategoryPermissions = "<div>";
	foreach($permissionCat['Groups'] as $perKey => $perVal) {
	    $toolTipGrpup .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $createPermission = createPermissionCheckboxes($perVal);
	    $groupCategoryPermissions .= $createPermission["html"];
	}
	$groupCategoryPermissions .= "</div>";
	$toolTipGrpup .= "</span>";
	?>
	
	<div class="row">
	
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="groupPerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="groupTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#groupPerDivAdd" aria-expanded="false"
                				   aria-controls="groupPerDiv">Group Permissions
                				   </a>
                			</h4>
        <!--   tool tip -->
        					<div id="usertool2">
            					<?php echo $toolTipGrpup; ?>
                    		</div>
        				</div>
                		<div id="groupPerDivAdd" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="groupTitleAdd">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $groupCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        
<!-- 		Services Permissions -->
<?php 
	$toolTipServ = "<span class='tooltiptextServices tooltiptext' id='serviceTooltip'>";
	$servicesCategoryPermissions = "<table border='1' style='width:80%;'>";
	foreach($permissionCat['Services'] as $perKey => $perVal) {
	    $toolTipServ .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $servicesCategory = createPermissionCheckboxes($perVal);
	    
	    $servicesCategoryPermissions .= $servicesCategory["html"];
	}
	$servicesCategoryPermissions .= "</table>";
	$toolTipServ .= "</span>";
	?>
		<div class="row">
		
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="servicesPerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="servicesTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#servicesPerDivAdd" aria-expanded="false"
                				   aria-controls="servicesPerDivAdd">Services Permissions
                				   </a>
                			</h4>
                                                <!--   tool tip -->
        					<div id="usertool3">
            					<?php echo $toolTipServ; ?>
                                                </div>
        				</div>
                		<div id="servicesPerDivAdd" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="servicesTitleAdd">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $servicesCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        

<!-- Devices Permissions -->
<?php 
	$toolTipDevice = "<span class='tooltiptextDevices tooltiptext' id='deviceTooltip'>";
	$devicesCategoryPermissions = "<table border='1' style='width:80%;'>";
	foreach($permissionCat['Devices'] as $perKey => $perVal) {
	    $toolTipDevice .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $devicesCategory = createPermissionCheckboxes($perVal);
	    
	    $devicesCategoryPermissions .= $devicesCategory["html"];
	}
	$devicesCategoryPermissions .= "</table>";
	$toolTipDevice .= "</span>";
	?>
		
		<div class="row">
		
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="devicePerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="devicesTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#devicesPerDivAdd" aria-expanded="false"
                				   aria-controls="devicesPerDivAdd">Devices Permissions
                				   </a>
                			</h4>
        <!--   tool tip -->
        					<div id="usertool4">
            					<?php echo $toolTipDevice; ?>
                    		</div>
        				</div>
                		<div id="devicesPerDivAdd" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="devicesTitleAdd">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $devicesCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        
<!-- Supervisory Permissions -->
<?php
	$toolTipSupervisory = "<span class='tooltiptextSupervisory tooltiptext' id='SupervisoryTooltip'>";
	$supervisoryCategoryPermissions = "<table border='1' style='width:80%;'>";
	foreach($permissionCat['Supervisory'] as $perKey => $perVal) {
	    $toolTipSupervisory .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $supervisoryCategory = createPermissionCheckboxes($perVal);
	    
	    $supervisoryCategoryPermissions .= $supervisoryCategory["html"];
	}
	$supervisoryCategoryPermissions .= "</table>";
	$toolTipSupervisory .= "</span>";
	?>
		
		<div class="row">
		
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="supervisoryPerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="supervisoryTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#supervisoryPerDivAdd" aria-expanded="false"
                				   aria-controls="supervisoryPerDivAdd">Supervisory Permissions
                				   </a>
                			</h4>
        <!--   tool tip -->
            				<div id="usertool5">
            					<?php echo $toolTipSupervisory; ?>
                    		</div>
        				</div>
                		<div id="supervisoryPerDivAdd" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="supervisoryTitleAdd">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $supervisoryCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
        			</div>
        		</div>
        	</div>
        </div>
        
	</div>	
    </div>
	

    <!-- Code added @ 13 Feb 2019 -->
    <div class="enterprisePermissionBlock">
         
        <div class="row">
            
            <?php 
                $clusterSupport  =  (isset($bwClusters) && $bwClusters == "true") && $enableClusters == "true" ? true : false;
                if($clusterSupport){
            ?>  
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="labelText">Select Cluster</label>
                            <div class="dropdown-wrap">   
                                <?php 
                                    $clusterIdName  = "entAdministratorClusterId";
                                    $clusterClsName = "form-control";
                                    $selectedCluster = $_SESSION['selectedCluster'];
                                    echo $clusterDropdownHtml = $configUtilObj->createClustersDropdownList($clusterIdName, $clusterClsName ,$clusterList, $selectedCluster);
                                ?>                                                    
                            </div>
                        </div>
                    </div>
            <?php
                 }
            ?>
            
            <div class="">
                <div class="col-md-6">
                <div class="form-group" id="securityDomainDivAdd" style="display: <?php echo (hasSecurityDomain() ? "block" : "none"); ?>">
                            <label class="labelText" for="entsecurityDomain">Security Domain <span class="required">*</span></label>
                                    <input type="text" class="form-control autoFill entsecurityDomainAdd" name="entsecurityDomain" id="entsecurityDomain">
                    </div>
                </div>

                
               </div>
            </div>
        
            <div class="row">
            <div class="">
                <div class="row">     
                <div class="col-md-12" style="display: <?php echo (hasSecurityDomain() ? "none" : "block"); ?>">
                    <div class="form-group" id="spDiv" style="display: <?php echo (hasSecurityDomain() ? "none" : "block"); ?>">
                            <div class="initialShow">
                                <label class="labelText" for="Selsp" style="margin-left:25px">Enterprise</label><span class="required">*</span>
                            </div>
                        
                        
                        <div class="">
                            <div class="col-md-6 availNCurrentGroup">
                                    <div class="form-group initialShow">Available Enterprises</div>
                        </div>
                        <div class="col-md-6 availNCurrentGroup">
                                <div class="form-group initialShow">Current Enterprises</div>
                        </div>
                        </div>
                        
                        
                        <div class="col-md-6">
                            <div class="initialShow">
                                
                                <div id="list1" class="scrollableList1">
			                <ul id="enterprise_available_user" class="connectedSortable connectedSortableCustom">
                                    </ul>
                                </div>                            
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="initialShow">
                            <div id="list1" class="scrollableList2">
			                <ul id="enterprise_assign_user" class="connectedSortable connectedSortableCustom"></ul>
                                        <input type="hidden" name="enterpriseAssignUser" id="enterpriseAssignUser" value="" />
                            </div>
                            </div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
            </div>
        
        <div class="vertSpacer initialShow borderUp">&nbsp;</div>
        
        <div class="row initialShow">
		<div class="col-md-6" style="padding-left: 0;">
			<div class="col-md-6 voiceMsgTxt">
			 <div class="form-group">
			<label class="labelText">Permissions</label><span class="required">*</span>
			<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="At least one permission must be selected."><img class="infoIcon" src="images/NewIcon/info_icon.png"></a>
			</div>
			</div>
			<div class="col-md-6 voiceMsgRBtn">
			<div class="col-md-6 rBtnYes" style="padding-right: 0;">
				<div class="form-group">
				 <input type="radio" name="entpermissionRadio" id="entpermissionRadioSelectAll" class="checkbox-lg" value="Select All" onclick="updateEnterprisePermissions(this)">
				 <label class="labelText labelTextMargin" for="entpermissionRadioSelectAll"><span></span>Select All</label>
				</div>
			</div>

			 <div class="col-md-6 rBtnNo" style="padding-right: 0;">
				 <div class="form-group">
					<input type="radio" name="entpermissionRadio" id="entpermissionRadioClearAll" class="checkbox-lg" value="Clear" onclick="updateEnterprisePermissions(this)">
					<label class="labelText" for="entpermissionRadioClearAll"><span></span>Clear All</label>
				</div>
			</div>
			</div>
		</div>
	    </div>
        
        <div id="enterpriseGroupPermissions">
        <?php 
        $x =0;
        $entPermissionCat = getEnterprisePermissionsFromPermissionsCategories($db);
	$toolTipEntGrpup = "<span class='tooltiptextEntGroups tooltiptext' id='groupEntToolTip'>";
	$entGroupCategoryPermissions = "<div>";        
        foreach($entPermissionCat['Group'] as $entPerKey => $entPerVal) {
           // $entPerVal['description'];
	    $toolTipEntGrpup .= "<div>".$entPerVal['description']."</div>";
	    $createPermission = createEnterprisePermissionCheckboxes($entPerVal, $x);
	    $entGroupCategoryPermissions .= $createPermission["html"];
            $x++;
	}
	$entGroupCategoryPermissions .= "</div>";
	$toolTipEntGrpup .= "</span>";
	?>	
	<div class="row">	
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="groupEntPerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="groupEntTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#groupEntPerDivAdd" aria-expanded="false"
                				   aria-controls="groupEntPerDivAdd">Group Permissions
                				</a>
                			</h4>
        <!--   tool tip -->
                                <div id="usertool6">
            					<?php echo $toolTipEntGrpup; ?>
                    		</div>
        				</div>
                		<div id="groupEntPerDivAdd" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="groupEntToolTip">
                			<div class="panel-body" style="padding: 0px;">
                                <div>
                    				<div class="">
                    						<?php echo $entGroupCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        
            <!--enterPrise group permission -->
     
     <?php 
        $a =0;
        $entPermissionCat = getEnterprisePermissionsFromPermissionsCategories($db);
	 $tooltipEntPerm = "<span class='tooltiptextEntPerm tooltiptext' id='entPermToolTip'>";
	$entPermCategoryPermissions = "<div>";        
        foreach($entPermissionCat['Enterprise'] as $entPerKey => $entPerVal) {
           // $entPerVal['description'];
            $tooltipEntPerm .= "<div>".$entPerVal['description']."</div>";
	    $createPermission = createEnterprisePermissionCheckboxes($entPerVal, $a);
	    $entPermCategoryPermissions .= $createPermission["html"];
            $a++;
	}
	$entPermCategoryPermissions .= "</div>";
	  $tooltipEntPerm .= "</span>";
	?>	
	<div class="row">	
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="entPermRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="entPermTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#entPermFeaturePerDiv" aria-expanded="false"
                				   aria-controls="entPermFeaturePerDiv">Enterprise Permissions
                				</a>
                			</h4>
        <!--   tool tip -->
                                <div id="usertool7">
            					<?php echo $tooltipEntPerm; ?>
                    		</div>
        				</div>
                		<div id="entPermFeaturePerDiv" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="entPermToolTip">
                			<div class="panel-body" style="padding: 0px;">
                                <div>
                    				<div class="">
                    						<?php echo $entPermCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        
            <!--enterPrise group permission -->
            
        <?php 
        $y = 0;
	$toolTipInventory = "<span class='tooltiptextInventory tooltiptext' id='inventoryTooltip'>";
	$InventoryCategoryPermissions = "<table border='1' style='width:80%;'>";
	foreach($entPermissionCat['Inventory'] as $invtryPerKey => $invtryPerVal) {
	    $toolTipInventory .= "<div>".$invtryPerVal['description']."</div>";
	    $InventoryCategory = createEnterprisePermissionCheckboxes($invtryPerVal, $y);
	    
	    $InventoryCategoryPermissions .= $InventoryCategory["html"];
            $y++;
	}
	$InventoryCategoryPermissions .= "</table>";
	$toolTipInventory .= "</span>";
	?>
		
	<div class="row">
		
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="InventryPerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="InventoryTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#InventoryPerDivAdd" aria-expanded="false"
                				   aria-controls="InventoryPerDivAdd">Inventory Permissions
                				   </a>
                			</h4>
        <!--   tool tip -->
        					<div id="usertool8">
            					<?php echo $toolTipInventory; ?>
                    		</div>
        				</div>
                		<div id="InventoryPerDivAdd" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="InventoryPerDivAdd">
                			<div class="panel-body" style="padding: 0px;">
                                <div>
                    				<div class="">
                    						<?php echo $InventoryCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        
        
        <?php 
        $z = 0;
	$toolTipAdministrators = "<span class='tooltiptextAdministrators tooltiptext' id='AdministratorsTooltip'>";
	$AdministratorsCategoryPermissions = "<table border='1' style='width:80%;'>";
        
	foreach($entPermissionCat['Administrators'] as $admnsPerKey => $admnsPerVal) {
	    $toolTipAdministrators             .= "<div>".$admnsPerVal['description']."</div>";
	    $AdministratorsCategory             = createEnterprisePermissionCheckboxes($admnsPerVal, $z);	    
	    $AdministratorsCategoryPermissions .= $AdministratorsCategory["html"];
            $z++;
	}
        
	$AdministratorsCategoryPermissions .= "</table>";
	$toolTipAdministrators .= "</span>";
	?>
		
	<div class="row">
		
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="AministratorPerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="AdministratorsTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#AdministratorsPerDivAdd" aria-expanded="false"
                				   aria-controls="AdministratorsPerDivAdd">Administrators Permissions
                				   </a>
                			</h4>
        <!--   tool tip -->
        					<div id="usertool9">
            					<?php echo $toolTipAdministrators; ?>
                    		</div>
        				</div>
                		<div id="AdministratorsPerDivAdd" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="AdministratorsPerDivAdd">
                                    <div class="panel-body" style="padding: 0px;">
                                <div>
                    				<div class="">
                    						<?php echo $AdministratorsCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
     </div>   
    </div>
    <!-- End code -->
	<!-- New code found start-->   
</form>
 
 