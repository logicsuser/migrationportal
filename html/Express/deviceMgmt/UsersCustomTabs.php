<?php

/**
 * Created by Karl.
 * Date: 10/2/2016
 */
class UsersCustomTabs extends HTMLTable
{
    private $users = array();

    private static function Attr_UserID()   { return "userId"; }
    private static function Attr_TagName()  { return "tagName"; }
    private static function Attr_TagValue() { return "tagValue"; }


    /**
     * UsersCustomTabs constructor.
     * @param $users
     */
    public function __construct($users)
    {
        parent::__construct();

        $this->addAttribute(self::Attr_UserID(),   "User ID");
        $this->addAttribute(self::Attr_TagName(),  "Tag Name");
        $this->addAttribute(self::Attr_TagValue(), "Tag Value");

        $i = 0;
        foreach ($users as $index=>$user) {
            $this->users[$i++] = $user["id"];
        }
    }


    /**
     * mandatory implementation with abstract prototype in parent class
     */
    protected function executeAction()
    {
        for ($i = 0; $i < count($this->users); $i++) {
            // process each user - get user's device
            $deviceName = $this->getUserDevice($this->users[$i]);
            $this->getAccessDeviceCustomTagsList($this->users[$i], $deviceName);
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    // get the name of a device assigned to a user
    //
    private static function getUserDevice($userId)
    {
        global $ociVersion, $sessionid, $client;

        if ($ociVersion == "17") {
            $xmlinput = xmlHeader($sessionid, "UserGetRequest17sp4");
        }
        else if ($ociVersion == "20") {
            $xmlinput = xmlHeader($sessionid, "UserGetRequest20");
        }
        else {
            $xmlinput = xmlHeader($sessionid, "UserGetRequest19");
        }

        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $deviceName = "";
        if (isset($xml->command->accessDeviceEndpoint->accessDevice->deviceName)) {
            $deviceName = $xml->command->accessDeviceEndpoint->accessDevice->deviceName;
        }

        return $deviceName;
    }


    //-----------------------------------------------------------------------------------------------------------------
    // Get all custom tags in a group for a specified device profile
    //
    private function getAccessDeviceCustomTagsList($userId, $deviceName) {
        global $sessionid, $client;
        $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagGetListRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (isset($xml->command->deviceCustomTagsTable->row)) {

            foreach ($xml->command->deviceCustomTagsTable->row as $key => $value) {
                $this->data[$this->numRecords][self::Attr_UserID()] = $userId;
                $this->data[$this->numRecords][self::Attr_TagName()] = strval($value->col[0]);
                $this->data[$this->numRecords][self::Attr_TagValue()] = strval($value->col[1]);
                $this->numRecords++;
            }
        }
    }
}
