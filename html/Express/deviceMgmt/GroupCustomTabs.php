<?php

/**
 * Created by PhpStorm.
 * User: Karl
 * Date: 10/2/2016
 * Time: 4:19 PM
 */
class GroupCustomTabs extends HTMLTable
{
    private $deviceTypes = array();

    private static function Attr_DeviceType() { return "deviceType"; }
    private static function Attr_TagName()    { return "tagName"; }
    private static function Attr_TagValue()   { return "tagValue"; }


    /**
     * GroupCustomTabs constructor.
     * @param $devices - list of device types
     */
    public function __construct($devices)
    {
        parent::__construct();

        $this->deviceTypes = $devices;

        $this->addAttribute(self::Attr_DeviceType(), "Device Type");
        $this->addAttribute(self::Attr_TagName(),    "Tag Name");
        $this->addAttribute(self::Attr_TagValue(),   "Tag Value");
    }


    /**
     * mandatory implementation with abstract prototype in parent class
     */
    protected function executeAction()
    {
        for ($i = 0; $i < count($this->deviceTypes); $i++) {
            // get custom tags for each device type
            $this->getGroupCustomTagsList($this->deviceTypes[$i]);
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    // Get all custom tags in a group for a specified device type
    //
    private function getGroupCustomTagsList($deviceType) {
        global $sessionid, $client;

        $xmlinput  = xmlHeader($sessionid, "GroupDeviceTypeCustomTagGetListRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceType>" . $deviceType . "</deviceType>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $this->hasCustomTags = false;
        if (isset($xml->command->groupDeviceTypeCustomTagsTable->row)) {
            foreach ($xml->command->groupDeviceTypeCustomTagsTable->row as $key => $value) {
                $this->data[$this->numRecords][self::Attr_DeviceType()] = $deviceType;
                $this->data[$this->numRecords][self::Attr_TagName()] = strval($value->col[0]);
                $this->data[$this->numRecords][self::Attr_TagValue()] = strval($value->col[1]);
                $this->numRecords++;
            }
        }
    }
}
