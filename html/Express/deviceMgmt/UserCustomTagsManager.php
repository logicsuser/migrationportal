<?php

/**
 * Created by PhpStorm.
 * User: Karl
 * Date: 9/28/2016
 * Time: 11:01 PM
 */
class UserCustomTagsManager extends CustomTagsManager
{
    private $state = "";
    private $users=array();
    private $customTags;
    private $hasCustomTags = false;


    /**
     * UserCustomTagsManager constructor.
     * @param $allGroupsInSystem
     * @param $users    - list of all users in the group
     * @param $postName - array name used to retrieve data from a posted form
     */
    public function __construct($allGroupsInSystem, $users, $postName)
    {
        parent::__construct($allGroupsInSystem, $postName);

        $this->addAttribute("action",       "Action");
        $this->addAttribute("groupId",      "Group ID");
        $this->addAttribute("userId",       "User ID");
        $this->addAttribute("tagName",      "Tag Name");
        $this->addAttribute("tagValue",     "Tag Value");

        $i = 0;
        foreach ($users as $index=>$user) {
            $this->users[$i++] = $user["id"];
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    // Mandatory function defined in abstract parent for implementation in derived classes
    //
    protected function executeAction($value)
    {
        global $sessionid, $client;

        // Do not process back-end if group is not yet provisioned
        if (! in_array($value[self::groupId()], $this->allGroups)) {
            return self::State_NoGroup();
        }

        // Do not proces back-end if group in the bulk file is not a current group
        if ($value[self::groupId()] != $this->currentGroup) {
            return self::State_NotCurrentGroup();
        }

        // Do not process back-end if user is not yet provisioned
        if (! in_array($value[self::userId()],$this->users)) {
            return self::State_NoUser();
        }

        // Do not process back-end if user has no device
        $deviceName = self::getUserDevice($value[self::userId()]);
        if ($deviceName == "") {
            return self::State_NoDevice();
        }

        $ociRequest = $this->getOciRequestCommand($value, $deviceName);
        if ($ociRequest == "") {
            return $this->state;
        }

        $xmlinput  = xmlHeader($sessionid, $ociRequest);
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($value[self::groupId()]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= "<tagName>" . $value[self::tagName()] . "</tagName>";
        if ($this->state != self::State_Delete()) {
            $xmlinput .= "<tagValue>" . $value[self::tagValue()] . "</tagValue>";
        }
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (readError($xml) != "") {
            $this->backEndErrorMsg = "OCI Error";
        }

        return $this->state;
    }


    //-----------------------------------------------------------------------------------------------------------------
    // get the name of a device assigned to a user
    //
    private static function getUserDevice($userId)
    {
        global $ociVersion, $sessionid, $client;

        if ($ociVersion == "17") {
            $xmlinput = xmlHeader($sessionid, "UserGetRequest17sp4");
        }
        else if ($ociVersion == "20") {
            $xmlinput = xmlHeader($sessionid, "UserGetRequest20");
        }
        else {
            $xmlinput = xmlHeader($sessionid, "UserGetRequest19");
        }

        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $deviceName = "";
        if (isset($xml->command->accessDeviceEndpoint->accessDevice->deviceName)) {
            $deviceName = $xml->command->accessDeviceEndpoint->accessDevice->deviceName;
        }

        return $deviceName;
    }


    //-----------------------------------------------------------------------------------------------------------------
    // Determine OCI command for user's device custom tags management
    // Determination is based on tag values in tag provisioned in BroadWorks and tag in the bulk file
    //
    private function getOciRequestCommand($value, $deviceName) {
        $this->getAccessDeviceCustomTagsList($value[self::groupId()], $deviceName);
        $currentTagValue = $this->getUserCustomTagValue($value[self::tagName()]);

        // Do nothing if current BW tag value is equal to value in bulk file
        if ($currentTagValue == $value[self::tagValue()]) {
            $this->state = self::State_NoAction();
            return "";
        }

        switch ($currentTagValue) {
            case "":
                $this->state = self::State_Add();
                return "GroupAccessDeviceCustomTagAddRequest";
                break;
            default:
                $this->state = $value[self::tagValue()] == "" ? self::State_Delete() : self::State_Update();
                return $value[self::tagValue()] == "" ? "GroupAccessDeviceCustomTagDeleteListRequest" : "GroupAccessDeviceCustomTagModifyRequest";
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    // Get all custom tags in a group for a specified device profile
    //
    private function getAccessDeviceCustomTagsList($groupId, $deviceName) {
        global $sessionid, $client;
        
        $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagGetListRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (isset($xml->command->deviceCustomTagsTable->row)) {
            $i = 0;
            $this->hasCustomTags = false;

            foreach ($xml->command->deviceCustomTagsTable->row as $key => $value) {
                $this->customTags[$i]["name"] = strval($value->col[0]);
                $this->customTags[$i]["value"] = strval($value->col[1]);
                $i++;
            }

            $this->hasCustomTags = $i > 0;
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    // Get custom tag value from a list of custom tags in a device profile
    //
    private function getUserCustomTagValue($tagName) {
        if (! $this->hasCustomTags) {
            return "";
        }

        foreach ($this->customTags as $customTag) {
            if ($customTag["name"] == $tagName) {
                return $customTag["value"];
            }
        }

        return "";
    }
}
