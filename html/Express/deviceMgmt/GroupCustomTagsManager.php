<?php

/**
 * Created by Karl.
 * Date: 9/22/2016
 */
class GroupCustomTagsManager extends CustomTagsManager
{
    private $state = "";
    private $customTags;
    private $hasCustomTags = false;

    /**
     * GroupCustomTagsManager constructor.
     * @param $allGroupsInSystem
     * @param $postName - array name used to retrieve data from a posted form
     */
    public function __construct($allGroupsInSystem, $postName)
    {
        parent::__construct($allGroupsInSystem, $postName);

        $this->addAttribute("action",       "Action");
        $this->addAttribute("groupId",      "Group ID");
        $this->addAttribute("deviceType",   "Device Type");
        $this->addAttribute("tagName",      "Tag Name");
        $this->addAttribute("tagValue",     "Tag Value");
    }


    //-----------------------------------------------------------------------------------------------------------------
    // Mandatory function defined in abstract parent for implementation in derived classes
    //
    protected function executeAction($value) {
        global $sessionid, $client;

        // Do not process back-end if group is not yet provisioned
        if (! in_array($value[self::groupId()], $this->allGroups)) {
            return self::State_NoGroup();
        }

        $sp = $this->getSpId($value[self::groupId()]);
        $this->getGroupCustomTagsList($sp, $value[self::groupId()], $value[self::deviceType()]);

        $ociRequest = $this->getOciRequestCommand($value);
        if ($ociRequest == "") {
            return $this->state;
        }
//$this->msg .= $ociRequest . " : " . $sp . " : " . $value[self::groupId()] . " : " . $value[self::deviceType()] . " : " . $value[self::tagName()] . " : " . $value[self::tagValue()];

        $xmlinput  = xmlHeader($sessionid, $ociRequest);
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($sp) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($value[self::groupId()]) . "</groupId>";
        $xmlinput .= "<deviceType>" . $value[self::deviceType()] . "</deviceType>";
        $xmlinput .= "<tagName>" . $value[self::tagName()] . "</tagName>";
        if ($this->state != self::State_Delete()) {
            $xmlinput .= "<tagValue>" . $value[self::tagValue()] . "</tagValue>";
        }
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (readError($xml) != "") {
            $this->backEndErrorMsg = "OCI Error";
        }

        return $this->state;
    }


    //-----------------------------------------------------------------------------------------------------------------
    // Determine OCI command for group device type custom tags management
    // Determination is based on tag values in tag provisioned in BroadWorks and tag in the bulk file
    //
    private function getOciRequestCommand($value) {
        $currentTagValue = $this->getBWGroupTagValue($value[self::tagName()]);

        // Do nothing if current BW tag value is equal to value in bulk file
        if ($currentTagValue == $value[self::tagValue()]) {
            $this->state = self::State_NoAction();
            return "";
        }

        switch ($currentTagValue) {
            case "":
                $this->state = self::State_Add();
                return "GroupDeviceTypeCustomTagAddRequest";
            break;
            default:
                $this->state = $value[self::tagValue()] == "" ? self::State_Delete() : self::State_Update();
                return $value[self::tagValue()] == "" ? "GroupDeviceTypeCustomTagDeleteListRequest" : "GroupDeviceTypeCustomTagModifyRequest";
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    // Get all custom tags in a group for a specified device type
    //
    private function getGroupCustomTagsList($sp, $groupId, $deviceType) {
        global $sessionid, $client;

        $xmlinput  = xmlHeader($sessionid, "GroupDeviceTypeCustomTagGetListRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($sp) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
        $xmlinput .= "<deviceType>" . $deviceType . "</deviceType>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $i = 0;
        $this->hasCustomTags = false;
        if (isset($xml->command->groupDeviceTypeCustomTagsTable->row)) {
            foreach ($xml->command->groupDeviceTypeCustomTagsTable->row as $key => $value) {
                $this->customTags[$i]["name"] = strval($value->col[0]);
                $this->customTags[$i]["value"] = strval($value->col[1]);
                $i++;
            }

            $this->hasCustomTags = $i > 0;
        }

    }


    //-----------------------------------------------------------------------------------------------------------------
    // Get custom tag value from a list of custom tags in a group
    //
    private function getBWGroupTagValue($tagName) {
        if (! $this->hasCustomTags) {
            return "";
        }

        foreach ($this->customTags as $customTag) {
            if ($customTag["name"] == $tagName) {
                return $customTag["value"];
            }
        }

        return "";
    }
}
