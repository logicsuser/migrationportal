<?php
//error_reporting(E_ALL);
//error_reporting(E_ALL & E_WARNING & E_NOTICE);
error_reporting(0);
    //begin session if not already started
	if (session_id() == "")
	{
		session_start();
	}

	const appsServer = "bwAppServer";
	const surgeMailServer = "surgeMail";
	const counterPathServer = "counterPath";
	const primaryServer = "primary";
	const secondaryServer = "secondary";
	global $ociVersion;

	$uploads_directory = '/var/www/uploads';


        //get settings from .ini file
	$serversDetail = new stdClass;

	$settings = parse_ini_file("/var/www/adminPortal.ini", TRUE);

	//MySQL database connections
	require_once("/var/www/html/Express/db.php");

	if (array_key_exists('db', $GLOBALS))
	{
		$db = $GLOBALS['db'];
	}

        //if login failed, redirect to home page and display appropriate error message
	if (isset($_SESSION["loginFailure"]) and $_SESSION["loginFailure"] == "true")
	{
		if (isset($_SESSION["failedLoginLimit"]) and $_SESSION["failedLoginLimit"] == "true")
		{
			$_SESSION["loginMsg"] = "You've reached the limit for failed login attempts and your account has been disabled. Contact Support to reset your password.";
		}
		else if (isset($_SESSION["invalidCharacter"]) and $_SESSION["invalidCharacter"] == "true")
		{
			$_SESSION["loginMsg"] = "Invalid character entered.";
		}
		else
		{
			$_SESSION["loginMsg"] = "Username/Password not correct.";
		}
		header("Location: index.php");
		exit;
	}

	if (!isset($_SESSION["sp"]))
	{
		$_SESSION["sp"] = "None";
	}

	//retrieve BroadSoft credentials
	$bsQuery = "select c.ip, c.username, c.password, v.version, p1.passwordType as voicePortalPasswordType, c.voicePortalPassword, p2.passwordType as webPortalPasswordType, c.webPortalPassword, d.domainType, c.broadSoftDomain, u.userIdFormat as broadWorksUserId, dn.deviceNameFormat as deviceName, lp.userIdFormat as linePortName, c.requireAddress";
	$bsQuery .= " from config c left join versionLookup v on c.version=v.id left join passwordLookup p1 on c.voicePortalPasswordType=p1.id left join passwordLookup p2 on c.webPortalPasswordType=p2.id left join domainLookup d on c.domainType=d.id left join userIdLookup u on c.broadWorksUserId=u.id left join deviceNameLookup dn on c.deviceName=dn.id left join userIdLookup lp on c.linePortName=lp.id";
	$bsQuery .= " where c.type='BS' and c.active='1'";
	$bsResults = $db->query($bsQuery);
	$bsRow = $bsResults->fetch();
	$bwBroadWorksUserId = $bsRow["broadWorksUserId"];																	//TODO: REMOVE - it is not used anywhere

        require_once("/var/www/lib/broadsoft/adminPortal/DBSimpleReader.php");
	require_once("/var/www/lib/broadsoft/adminPortal/ServerConfiguration.php");


        // get Servers configuration
    
	/*$serverConfiguration = new ServerConfiguration($db,appsServer,primaryServer);
	$defaultBwAppServer = "primaryServer";
        */
        
        $systemConfig = new DBSimpleReader($db, "systemConfig");
        $enableClusters = $systemConfig->get("enableClusters");
        
        
        //express License Permission 
	$license =array();	
        $expressLicense = "SELECT * from expressLicense";
	$exLicense      = $db->query($expressLicense);
        while ($row = $exLicense->fetch())
        {
             if($row["licensePermissionName"]=="customTag"){
                 $license["customTags"]    =  $row["licensePermissionVal"];
             }else{
                 $license[$row['licensePermissionName']] = $row["licensePermissionVal"];
             }
        }
        
        //Code added @ 14 June 2019
        $selectedCluster = "";
        $bwClusters = $license["bwClusters"];
        $clusterSupport  =  (isset($bwClusters) && $bwClusters == "true") && $enableClusters == "true" ? true : false;
        if($clusterSupport){
              $selectedCluster = $_SESSION['selectedCluster'];
        }else{
              $selectedCluster = "";
        }
        $_SESSION['cluster_support'] = $clusterSupport;
        
        //End code
        
        // get Servers configuration
        
	$serverConfiguration = new ServerConfiguration($db,appsServer,primaryServer, $selectedCluster);
        if ($serverConfiguration->getActive() != "true") {
		$serverConfiguration = new ServerConfiguration($db,appsServer,secondaryServer, $selectedCluster);
            }
        // get Servers configuration

    /* XSP Server Detail */
    $xspServerDetails = new ServerConfiguration($db,xspServer,primaryServer, $selectedCluster);
    if ($xspServerDetails->getActive() != "true") {
        $xspServerDetails = new ServerConfiguration($db,xspServer,secondaryServer, $selectedCluster);
    }
    $xspPrivateIP = $xspServerDetails->getIP();
    //$xspSecondaryServerIP = $xspServerDetails->getIP();
    /* End Xsp Server */
    
    
	$bw_ews      = $serverConfiguration->getIP();																			//TODO: CHANGE NAME $bw_ews to $bwServerIP
	//print_r($bw_ews); exit;
        
	$bwuserid    = $serverConfiguration->getUserName();																	//TODO: CHANGE NAME $bwuserid to $bwUserName
	$bwpasswd    = $serverConfiguration->getPassword();																	//TODO: CHANGE NAME $bwpasswd to $bwPassword
	$ociVersion  = $serverConfiguration->getOCIRelease();																	//TODO: CHANGE NAME $ociVersion to $bwRelease
	$bwAuthlevel = "system";                              // alternative value expected= serviceProvider
        $bwProtocol  = $serverConfiguration->getProtocol();   //code added @ 10 May 2018
        $bwPort      = $serverConfiguration->getPort();       //Code added @ 10 May 2018
        $clusterName = $serverConfiguration->getclusterName();
        $bwAppServer = $serverConfiguration->getInstance();   //Code added @ 22 May 2019
        
        //Code added @08 May 2019
        $clusterNameForBWLicnese = $serverConfiguration->getclusterName();
        $ipAddressForBWLicnese   = $serverConfiguration->getIP();
        $bwAppServerInstance     = $serverConfiguration->getInstance();
        //End code
        
        
        //Code added @ 27 May 2019
        if (filter_var($bw_ews, FILTER_VALIDATE_IP)) {
            $bwfqdn = "None";
        } else {
            $bwfqdn = $bw_ews;
        }
        //End code
        
    // get System configuration
    
    $groupsDomainType = $systemConfig->get("groupsDomainType");
    $staticDomain = $systemConfig->get("staticDomain");
    $proxyDomainType = $systemConfig->get("proxyDomainType");
    $staticProxyDomain = $systemConfig->get("staticProxyDomain");
	$securityDomainPattern = $systemConfig->get("securityDomainPattern");
	$bwWebPortalPasswordType = $systemConfig->get("bwPortalPasswordType");												//TODO: CHANGE NAME $bwWebPortalPasswordType to $bwPortalPasswordType
	//Decrypt Password if Salt is present
	if($staticBwPortalPasswordSalt = $systemConfig->get("staticBwPortalPasswordSalt")) {
		$bwWebPortalPassword = decryptString($systemConfig->get("staticBwPortalPassword"), "StaticBwPortalPassword_" . $staticBwPortalPasswordSalt);
	} else {
		$bwWebPortalPassword = $systemConfig->get("staticBwPortalPassword");                                            //TODO: CHANGE NAME $bwWebPortalPassword to $staticBwPortalPassword
	}
    $policiesUsrAuthFirstLoginPasswordChange = $systemConfig->get("bwPortalForcePasswordChange");                       //TODO: CHANGE NAME $policiesUsrAuthFirstLoginPasswordChange to $bwPortalForcePasswordChange
    $permissionsViewCDR = $systemConfig->get("viewCDR");                                                                //TODO: CHANGE NAME $permissionsViewCDR to $viewCDR
	$useDepartments = $systemConfig->get("useDepartments");
    $permissionsBwAuthentication = $systemConfig->get("bwAuthentication");                                              //TODO: CHANGE NAME $permissionsBwAuthentication to $bwAuthentication
    $policiesSupportSurgeMailDebug = $systemConfig->get("surgeMailDebug");                                              //TODO: CHANGE NAME $policiesSupportSurgeMailDebug to $surgeMailDebug
    $surgeMailIdCriteria1 = $systemConfig->get("surgeMailIdCriteria1");
	$surgeMailIdCriteria2 = $systemConfig->get("surgeMailIdCriteria2");
    $surgemailDomain = $systemConfig->get("surgeMailDomain");                                                           //TODO: CHANGE NAME $surgemailDomain to $surgeMailDomain
    $smSurgeMailPasswordType = $systemConfig->get("surgeMailUserPasswordType");                                         //TODO: CHANGE NAME $smSurgeMailPasswordType to $surgeMailUserPasswordType
	//Decrypt Password if Salt is present
	if($surgeMailUserPasswordSalt = $systemConfig->get("surgeMailUserPasswordSalt")) {
		$smSurgeMailPassword = decryptString($systemConfig->get("surgeMailUserPassword"), "SurgeMailPassword_" . $surgeMailUserPasswordSalt);
	} else {
		$smSurgeMailPassword = $systemConfig->get("surgeMailUserPassword");                                             //TODO: CHANGE NAME $smSurgeMailPassword to $surgeMailUserPassword
	}
    $supportEmailSupport = $systemConfig->get("supportEmail");                                                          //TODO: CHANGE NAME $supportEmailSupport to $supportEmail
    $billingHost = $systemConfig->get("billingHost");
    $billingDB = $systemConfig->get("billingDB");
    $billingUser = $systemConfig->get("billingUser");
	//Decrypt Password if Salt is present
	if($billingPasswordSalt = $systemConfig->get("billingPasswordSalt")) {
		$billingPassword = decryptString($systemConfig->get("billingPassword"), "BillingPassword_" . $billingPasswordSalt);
	} else {
		$billingPassword = $systemConfig->get("billingPassword");
	}
	$billingForceSSL = $systemConfig->get("billingForceSSL");
	$billingCACert = $systemConfig->get("billingCACert");
	$policiesSupportDeleteVMOnUserDel = $systemConfig->get("deleteVMOnUserDel");                                        //TODO: CHANGE name $policiesSupportDeleteVMOnUserDel to $deleteVMOnUserDel
	$userLimitGroup = $systemConfig->get("userLimitGroup");                                        //TODO: CHANGE name $policiesSupportDeleteVMOnUserDel to $deleteVMOnUserDel
	$sessionTimeout = $systemConfig->get("inactivityTimeout");
    // $advancedUserFilters = $systemConfig->get("advancedUserFilters"); // comment regarding to ex-623
    $useExtensionLengthRange = $systemConfig->get("useExtensionLengthRange");
    
    /*$sshUsername = $systemConfig->get("sshUsername");
	//Decrypt Password if Salt is present
	if($sshPasswordSalt = $systemConfig->get("sshPasswordSalt")) {
		$sshPassword = decryptString($systemConfig->get("sshPassword"), "SSHPassword_" . $sshPasswordSalt);
	} else {
		$sshPassword = $systemConfig->get("sshPassword");
	}
    $sshPort = $systemConfig->get("sshPort");*/
	$isAdminPasswordFieldVisible = $systemConfig->get("isAdminPasswordFieldVisible");
	$maxFailedLoginAttempts = $systemConfig->get("maxFailedLoginAttempts");
/*password expiration super user */
	$passExpiration_Super = $systemConfig->get("passwordExpiration_superUser");
	$passExpiration_RegUser = $systemConfig->get("passwordExpiration_regularUser");
 /*end password expiration  */	
	$newUsrAdminReport = $systemConfig->get("newUsrAdminReport");
	$newUsrSupervisorReport = $systemConfig->get("newUsrSupervisorReport");
	$newUsrSupervisorEmail = $systemConfig->get("newUsrSupervisorEmail");
 
	$counterpathGroupName = $systemConfig->get("counterpathGroupName");

	$enableContinuePreviousSession = $systemConfig->get("enableContinuePreviousSession");
 
	/*xsp private ip */
/*	$xspPrimaryServerIP = $systemConfig->get("xspPrimaryServerIP");
	$xspSecondaryServerIP = $systemConfig->get("xspSecondaryServerIP");
	$xspActive = $systemConfig->get("xspActive");
*/        
        //Code added @ 10 May 2019
        $warningThreshold   = $systemConfig->get("warningsThreshold");
        $attentionThreshold = $systemConfig->get("alertsThreshold");
        //End code
	
	/*end xsp privat ip */
	require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/PasswordRulesConfiguration.php");
	$adminPasswordRules = new PasswordRulesConfiguration('adiminPassword');

    // get Group configuration
    $groupConfig = new DBSimpleReader($db, "groupConfig");
    $groupDomainRequired = $groupConfig->get("groupDomainRequired");


    // get Device configuration
    $deviceConfig = new DBSimpleReader($db, "deviceConfig");
	//$useCustomDeviceList = $deviceConfig->get("useCustomDeviceList");
	$deleteAnalogUserDevice = $deviceConfig->get("deleteAnalogUserDevice");
    $linePortCriteria1 = $deviceConfig->get("linePortCriteria1");
    $linePortCriteria2 = $deviceConfig->get("linePortCriteria2");
	$scaLinePortCriteria1 = $deviceConfig->get("scaLinePortCriteria1");
	$scaLinePortCriteria2 = $deviceConfig->get("scaLinePortCriteria2");
    $deviceNameDID1 = $deviceConfig->get("deviceNameDID1");
    $deviceNameDID2 = $deviceConfig->get("deviceNameDID2");
    $deviceNameAnalog = $deviceConfig->get("deviceNameAnalog");
    $deviceAccessUserName1 = $deviceConfig->get("deviceAccessUserName1");
    $deviceAccessUserName2 = $deviceConfig->get("deviceAccessUserName2");
    $deviceAccessPassword = $deviceConfig->get("deviceAccessPassword");
	$analogAccessAuthentication = $deviceConfig->get("analogAccessAuthentication");
	$scaOnlyCriteria = $deviceConfig->get("scaOnlyCriteria");
	$rebuildResetDevice = $deviceConfig->get("rebuildResetDevice");
	$softPhoneLinePortCriteria1 = $deviceConfig->get("softPhoneLinePortCriteria1");
	$softPhoneLinePortCriteria2 = $deviceConfig->get("softPhoneLinePortCriteria2");


    // get User configuration
    $userConfig = new DBSimpleReader($db, "userConfig");
    $userIdCriteria1 = $userConfig->get("userIdCriteria1");
    $userIdCriteria2 = $userConfig->get("userIdCriteria2");
    $policiesClidPolicyLevel = $userConfig->get("clidPolicyLevel");                                                     //TODO: CHANGE name $policiesClidPolicyLevel to $clidPolicyLevel
    $policiesClidPolicy = $userConfig->get("clidPolicy");                                                               //TODO: CHANGE name $policiesClidPolicy to $clidPolicy
    $clidNameFieldsVisible = $userConfig->get("clidNameFieldsVisible");
    $dialingNameFieldsVisible = $userConfig->get("dialingNameFieldsVisible"); //Code added @ 23 Oct 2018
    $bwRequireAddress = $userConfig->get("postalAddressRequired");														//TODO: CHANGE name $bwRequireAddress to $postalAddressRequired
    $portalRequiresClidNumber = $userConfig->get("clidNumberRequired");                                                 //TODO: CHANGE name $portalRequiresClidNumber to $clidNumberRequired
    $portalShowClidFields = $userConfig->get("clidPolicyFieldVisible");                                                 //TODO: CHANGE name $portalShowClidFields to $clidPolicyFieldVisible
    $useGroupAddress = $userConfig->get("useGroupAddress");
    $addressFieldsVisible = $userConfig->get("addressFieldsVisible");
    //Modifiable Address Fields
    $addressFieldsModifiable = $userConfig->get("addressFieldsModifiable");
    //end
    $useGroupTimeZone = $userConfig->get("useGroupTimeZone");
    $timeZoneFieldVisible = $userConfig->get("timeZoneFieldVisible");
    $emailFieldVisible = $userConfig->get("emailFieldVisible");
    //Code added for location visible fields
    $locationFieldVisible = $userConfig->get("locationFieldVisible");
    //End code
    $portalShowLanguageField = $userConfig->get("languageFieldVisible");                                                //TODO: CHANGE name $portalShowLanguageField to $languageFieldVisible
    $portalShowDeviceFields = $userConfig->get("deviceFieldsVisible");                                                  //TODO: CHANGE name $portalShowDeviceFields to $deviceFieldsVisible
    $departmentsFieldVisible = $userConfig->get("departmentsFieldVisible");
    //$ccdFieldVisible = $userConfig->get("ccdFieldVisible"); //Code commented @ 07 Sep 2018
    $cpgFieldVisible = $userConfig->get("cpgFieldVisible");
    $macAddressFieldVisible = $userConfig->get("macAddressFieldVisible");
    $voiceMessagingVisible = $userConfig->get("voiceMessagingVisible");
    $polycomServicesFieldVisible = $userConfig->get("polycomServicesFieldVisible");
    $numSIPGatewayInstances = $userConfig->get("numSIPGatewayInstances");
    $serviceAssignmentVisible = $userConfig->get("serviceAssignmentVisible");
    $sharedDeviceVisible = $userConfig->get("sharedDeviceVisible");
    $softPhoneVisible = $userConfig->get("softPhoneVisible");
    //$numberActivationCheck = $userConfig->get("numberActivation");

    // get Services Configuration
    // --------------------------
    $serviceConfig = new DBSimpleReader($db, "serviceConfig");
    $bwVoicePortalPasswordType = $serviceConfig->get("userVoicePortalPasscodeType");                                    //TODO: CHANGE name $bwVoicePortalPasswordType to $userVoicePortalPasscodeType
    $userVoicePortalPasscodeFormula = $serviceConfig->get("userVoicePortalPasscodeFormula");
    $bwVoicePortalPassword = $serviceConfig->get("userVoicePortalStaticPasscode");                                      //TODO: CHANGE name $bwVoicePortalPassword to $userVoicePortalStaticPasscode
	//$usrAllowInternationalCalls = $serviceConfig->get("usrAllowInternationalCalls");
	$aaCriteria1Value = $serviceConfig->get("aacriteria1");
	$aaCriteria2Value = $serviceConfig->get("aacriteria2");
	$aaClidFnameValue = $serviceConfig->get("aaClidFname");
	$aaClidLnameValue = $serviceConfig->get("aaClidLname");
	$aaNameCriteriaValue = $serviceConfig->get("aaNameCriteria");
	

	// get SCA Service Configuration
	// -----------------------------
	$scaConfig = new DBSimpleReader($db, "scaConfig");
	$scaIsActive = $scaConfig->get("scaIsActive");
	$scaAllowOrigination = $scaConfig->get("scaAllowOrigination");
	$scaAllowTermination = $scaConfig->get("scaAllowTermination");
	$scaAlertClickToDial = $scaConfig->get("scaAlertClickToDial");
	$scaAllowGroupPaging = $scaConfig->get("scaAllowGroupPaging");
	$scaAllowCallRetrieve = $scaConfig->get("scaAllowCallRetrieve");
	$scaAllowBridging = $scaConfig->get("scaAllowBridging");
	$scaCallParkNotification = $scaConfig->get("scaCallParkNotification");
	$scaMultipleCallArrangement = $scaConfig->get("scaMultipleCallArrangement");
	$scaBridgeWarningTone = $scaConfig->get("scaBridgeWarningTone");

    // Attempt connecting to Billing database
    if ($permissionsViewCDR == "true" && $billingHost != "" && $billingDB != "" && $billingUser != "" && $billingPassword != "") {
        try {

	        $options = array();

	        if($billingForceSSL == "true" && $billingCACert) {
		        $options[PDO::MYSQL_ATTR_SSL_CA] = $billingCACert;
		        $options[PDO::MYSQL_ATTR_SSL_CIPHER] = 'AES128-SHA';
	        }

	        $billDB = new PDO("mysql:host=" . $billingHost . ";dbname=" . $billingDB . ";charset=utf8;",
		        $billingUser,
		        $billingPassword,
		        $options
	        );

	        /*
	        $billDB = mysqli_init();

	        //Use SSL if turned on.
	        if($billingForceSSL == "true" && $billingCACert) {
		        $billDB->ssl_set("/var/www/uploads/server-key.pem", "/var/www/uploads/server-cert.pem", $billingCACert, NULL, "AES128-SHA");
	        }
	        $billDB->options(
		        MYSQLI_OPT_SSL_VERIFY_SERVER_CERT,
		        false
	        );

	        openssl_pkey_get_public("");
	        openssl_error_string();
	        $billDB->real_connect($billingHost, $billingUser, $billingPassword, $billingDB, 3306, NULL, MYSQLI_CLIENT_SSL);
	        $billDB->set_charset('utf8');
	        if ($billDB->connect_errno) {
		        $error_message = $billDB->connect_error;
		        $billDB = NULL;
	        }
	        */


        }
        catch (PDOException $e) {}
    }

	//retrieve SurgeMail credentials
	$smQuery = "select c.ip, c.username, c.password, c.domain, p.passwordType as surgeMailPasswordType, c.surgeMailPassword, u.userIdFormat as surgeMailUsername";
	$smQuery .= " from config c left join passwordLookup p on c.surgeMailPasswordType=p.id left join userIdLookup u on c.surgeMailUsername=u.id";
	$smQuery .= " where c.type='SM' and c.active='1'";
	$smResults = $db->query($smQuery);
	$smRow = $smResults->fetch();

        
	/*$surgemailUsername = $smRow["username"];
	$surgemailPassword = $smRow["password"];
	$surgemailIp       = $smRow["ip"];
        $surgemailURL      = $policiesSupportSurgeMailDebug == "true" ? "http://" :"https://";
        $surgemailURL     .= $smRow["ip"] . "/cgi/user.cgi";     */   
        
        
        //Code added by @ 03 April 2018 get Surgemail Configration details from serversConfig table
        // -------------------------          
        $surgMailServerConfig = new ServerConfiguration($db, surgeMailServer, primaryServer);
	if ($surgMailServerConfig->getActive() != "true") {
		$surgMailServerConfig = new ServerConfiguration($db, surgeMailServer, secondaryServer);
	}        
	$surgemailIp       = $surgMailServerConfig->getIP().":".$surgMailServerConfig->getPort();	
        $surgemailUsername = $surgMailServerConfig->getUserName();
	$surgemailPassword = $surgMailServerConfig->getPassword();
        $surgemailProtocol = $surgMailServerConfig->getProtocol();
        $surgemailPort     = $surgMailServerConfig->getPort();
                
        $surgemailURL      = $policiesSupportSurgeMailDebug == "true" ? "http://" :"$surgemailProtocol://";
        $surgemailURL     .= $surgemailIp . "/cgi/user.cgi";
        //End code
         
        
    /*counter Path streto config for database */
        
        $counterPathServerConfig = new ServerConfiguration($db,counterPathServer,primaryServer);
        if ($counterPathServerConfig->getActive() != "true") {
            $counterPathServerConfig = new ServerConfiguration($db,counterPathServer,secondaryServer);
        }
        $counterPath_ews = $counterPathServerConfig->getIP();																			//TODO: CHANGE NAME $counterPath_ews to $bwServerIP
        $counterPathUserid = $counterPathServerConfig->getUserName();																	//TODO: CHANGE NAME $counterPathuserid to $bwUserName
        $counterPathPasswd = $counterPathServerConfig->getPassword();																	//TODO: CHANGE NAME $counterPathpasswd to $counterPathpasswd
        $counterPathVersion = $counterPathServerConfig->getOCIRelease();																	//TODO: CHANGE NAME $counterPathVersion to $$counterPathRelease
        $counterPathAuthlevel = "system"; // alternative value expected= serviceProvider
        $counterPathProtocol  = $counterPathServerConfig->getProtocol();
        $counterPathPort      = $counterPathServerConfig->getPort();     
   /* end counter Path streto config for database */

	//set constants and common variables
	date_default_timezone_set("America/New_York");

	define("UNCHANGED", "#EEEEEE"); //color for unchanged values during error-checking
	define("CHANGED", "#72ac5d"); //color for changed values during error-checking
	define("INVALID", "#ac5f5d"); //color for invalid values during error-checking

	$errorTableHeader = "<table cellspacing=\"0\" cellpadding=\"5\" style=\"width:850px; margin: 0 auto;\" class=\"confSettingTable\">";
	$errorTableHeader .= "<tr><th colspan=\"2\" align=\"center\">Please confirm the settings below and click Complete to process your modifications.</th></tr>";
	$errorTableHeader .= "<tr><td colspan=\"2\"><table cellpadding=\"5\" width=\"100%\" class=\"legendRGTable\">";
	$errorTableHeader .= "<tr><td style=\"background:" . CHANGED . ";\" width=\"5%\">&nbsp;</td><td>Value Changed</td></tr>";
	$errorTableHeader .= "<tr><td style=\"background:" . INVALID . ";\" width=\"5%\">&nbsp;</td><td>Invalid Value</td></tr>";
	$errorTableHeader .= "</table></td></tr>";
	$errorTableHeader .= "<tr><td width=\"50%\">&nbsp;</td><td width=\"50%\">&nbsp;</td></tr>";

	$tollFree = array("800", "1800", "+800", "+1800", "1866", "866", "+866", "+1866", "+877", "+1877", "1877", "877", "855", "1855", "+855", "+1855");

	//set common functions
	function checkLogin() //check if user's session is set; if not, send user to login page
	{
		global $sessionTimeout;

		if (!(isset($_SESSION["loggedInUserName"]) and $_SESSION["loggedInUserName"] != ""))
		{
			header("Location: /Express/index.php");
			exit;
		}
		$session_id = get_user_session_id();
		if( $session_id != session_id()) {
			error_log("CHECK: " . session_id() . ", " . $session_id);
			header("Location: /Express/index.php");
			exit;
		}

		//CHECK Session Timeout
		if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > $sessionTimeout)) {

			error_log("Session Timed out: " . $_SESSION["loggedInUserName"]);
			$_SESSION["loginMsg"] = "You have been logged out due to inactivity.";
			header("Location: /Express/index.php");
			exit;
		}

		$_SESSION['LAST_ACTIVITY'] = time();
	}

	function get_user_session_id() {

		require_once "db.php";
		global $db;

		if(isset($_SESSION["loggedInUserName"])) {
			$usersQuery = "select session_id from users where userName='" . $_SESSION["loggedInUserName"] . "'";
			$usersResults = $db->query($usersQuery);
			if ($usersRow = $usersResults->fetch()) {
				return $usersRow['session_id'];
			}

		} else {
			return "";
		}
	}

	function readError($xml) //display BroadSoft errors
	{
		global $supportEmailSupport;
		if (count($xml->command->attributes()) > 0)
		{
			foreach ($xml->command->attributes() as $a => $b)
			{
				if ($b == "Error" || $b == "Warning")
				{
					//echo "<p style=\"color:red;\">The following Errors occurred. Please report this to " . $supportEmailSupport . " .<br>";
					//echo "Please include what you were attempting to do when this error occurred.";
					echo "<p style=\"color:red; text-align: center;\">The following Errors occurred. ";

					echo "<br><br>" . strval($xml->command->summaryEnglish);
					if ($xml->command->detail)
					{
						echo "<br>" . strval($xml->command->detail);
					}
					echo "</p>";
					return "Error";
				}
			}
		}
	}
        
        //Code added @ 04 Jan 2019 Show Broadsoft Error With Expired License 
        function readErrorDup($xml, $spName, $serviceName)
	{
		global $supportEmailSupport;
                //Code added @ 04 Jan 2019
                require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");   
                $serviceObj = new Services();
                //End code
		if (count($xml->command->attributes()) > 0)
		{
			foreach ($xml->command->attributes() as $a => $b)
			{
				if ($b == "Error" || $b == "Warning")
				{
					//echo "<p style=\"color:red;\">The following Errors occurred. Please report this to " . $supportEmailSupport . " .<br>";
					//echo "Please include what you were attempting to do when this error occurred.";
					echo "<p style=\"color:red;\">The following Errors occurred. ";

					echo "<br><br>" . strval($xml->command->summaryEnglish);
					if ($xml->command->detail)
					{
						echo "<br>" . strval($xml->command->detail);
                                                if(stripos(strval($xml->command->detail), "Service pack") !==false){
                                                    
                                                }else{  
                                                    if($spName!="None"){
                                                        $serviceList = $serviceObj->getSericesListOfServicePack($spName);
                                                        $serviceErrorList = $serviceObj->getServiceLicenseErrorOnAssignServicePackToUser($_SESSION["groupId"], $serviceList);
                                                        foreach($serviceErrorList as $errorServiceName => $serviceErrorMessage){
                                                            echo "<br /><span style='color:red;'>$errorServiceName : $serviceErrorMessage</span>";
                                                        }   
                                                    }
                                                    if($serviceName!="None"){
                                                        $serviceErrorMessage = $serviceObj->retriveLicenseErrorAgainstServiceName($serviceName);
                                                        echo "<br /><span style='color:red;'>$serviceName : $serviceErrorMessage</span>"; 
                                                    }
                                                    
                                                }
					}
					echo "</p>";
					return "Error";
				}
			}
		}
	}


	function searchArray($array, $column, $user) //array search
	{
		if (isset($array))
		{
			foreach ($array as $key => $value)
			{
				if ($value[$column] === $user)
				{
					return true;
				}
			}
		}
		return false;
	}

	function setAuthPassword() //set authentication configuration password
	{
		$password = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"), 0, 7);
		$passwordNumber = substr(str_shuffle("0123456789"), 0, 1);
		$passwordNonAlpha = substr(str_shuffle('$@$-!'), 0, 1);
		return $password.$passwordNumber.$passwordNonAlpha;
	}

	function setEmailPassword() //set SurgeMail password
	{
		global $smSurgeMailPasswordType, $smSurgeMailPassword;
		if ($smSurgeMailPasswordType == "Static")
		{
			$password = $smSurgeMailPassword;
		}
		else
		{
			$password = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"), 0, 16);
		}
		return $password;
	}

	function setVoicePassword() //set voice portal password
	{
		global $bwVoicePortalPasswordType, $bwVoicePortalPassword;
		if ($bwVoicePortalPasswordType == "Static")
		{
			$password = $bwVoicePortalPassword;
		}
		else
		{
			//$password = substr(str_shuffle("0123456789"), 0, 6);
                        //$password   = substr(str_shuffle("123456789098"), 0, 6);//Code added because in start 0 may be disappear
                        $password   = "476983";  //Code added @ 08 May 2019 regarcing Ex-1318 as discussed
                }
		return $password;
	}

	function setWebPassword($minLength = 3) //set web portal password
	{
		global $bwWebPortalPasswordType, $bwWebPortalPassword;
		if ($bwWebPortalPasswordType == "Static")
		{
			$password = $bwWebPortalPassword;
		}
		else
		{
			$password = "!";
            while(true) {
                $password .= substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1);
                if (strlen($password) == $minLength + 1) {break;}

                $password .= substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 1);
                if (strlen($password) == $minLength + 1) {break;}

                $password .= substr(str_shuffle("0123456789"), 0, 1);
                if (strlen($password) == $minLength + 1) {break;}
            }
		}
		return $password;
	}

	function subval_sort($a, $subkey = "") //case-insensitive array sort
	{
		if (isset($a))
		{
			foreach ($a as $k => $v)
			{
				if ($subkey != "")
				{
					$b[$k] = strtolower($v[$subkey]);
				}
				else
				{
					$b[$k] = strtolower($v);
				}
			}
			asort($b);
			foreach ($b as $key => $val)
			{
				$c[] = $a[$key];
			}
			return $c;
		}
	}
	
	/* Get Login Page and */
	$allLogos = getAllLogoDetails();
	function getAllLogoDetails()
	{
	    require_once "db.php";
	    global $db;
	    $selQuery = "select * from expressLogos where selected='true'";
	    $selResults = $db->query($selQuery);
	    if($selResults->rowCount() > 0) {
	        $logos = array();
	        while ($row = $selResults->fetch()) {
	            $logos[$row['type']] = $row;
	        }
	    }
	    return $logos;
	}
	
	// get Voice Portal configuration
	$vpConfig = new DBSimpleReader($db, "voiceportalconfig");
	$configVoicePortalID = $vpConfig->get("voicePortalID");
	$configVoicePortalName = $vpConfig->get("voicePortalName");
	$configClidFirstName = $vpConfig->get("clidFirstName");
	$configClidLastName = $vpConfig->get("clidLastName");


	// get Branding Configuration
	// -----------------------------
	$brandingConfig = new DBSimpleReader($db, "brandingConfig");
// 	$brandingLogo = $brandingConfig->get("logo");
	$brandingLogo = isset($allLogos['header']['filename']) ? $allLogos['header']['filename'] : "";
	$brandingLoginLogo = isset($allLogos['loginPage']['filename']) ? $allLogos['loginPage']['filename'] : "";
	$customerLogoOnLoginPage = $brandingConfig->get("customerLogoOnLoginPage");
	$brandingcolorSchema = $brandingConfig->get("colorSchema");
	$brandingLogoPath = $brandingLogo ? "/var/www/logos/$brandingLogo" : "";
	$brandingLogoWebPath = $brandingLogo ? ("/logos/$brandingLogo") : "";
	$brandingLoginLogoWebPath = $brandingLoginLogo ? ("/logos/loginPageLogo/$brandingLoginLogo") : "";
	
	// get Color Schemas
	// -----------------------------

	// $colorSchemaData = getColorSchemas();
	// $_SESSION["colorSchema"] = $colorSchema = getButtonColor($brandingcolorSchema);
	// $buttonBase = $colorSchema["Colors"]["buttonBase"];
	// $buttonHover = $colorSchema["Colors"]["buttonHover"];
	// $buttonPressed = $colorSchema["Colors"]["buttonPressed"];
	// $buttonTextBase = $colorSchema["Colors"]["buttonTextBase"];
	// $buttonTextActive = $colorSchema["Colors"]["buttonTextActive"];
	// $textLinkActive = $colorSchema["Colors"]["textLinkActive"];
	// $textLinkHover = $colorSchema["Colors"]["textLinkHover"];
	// $userTableHiglight = $colorSchema["Colors"]["userTableHiglight"];
	// $userTableHiglight2 = $colorSchema["Colors"]["userTableHiglight2"];
	//$tabsBackground = $colorSchema["Colors"]["tabsBackground"];
	// $navigationTextBase = $colorSchema["Colors"]["navigationTextBase"];
	
	// function getColorSchemas()
	// {
	    // require_once "db.php";
	    // global $db;
	    // $colorSchemaData = array();
	    
	    // $colorSchemaQuery = "select name from colorSchemas";
	    // $csResults = $db->query($colorSchemaQuery);
	    // while($csRow = $csResults->fetch() ) {
	        // $colorSchemaData["name"][] = $csRow["name"];
	    // }
	    // return $colorSchemaData;
	// }
	
	// function getButtonColor($brandingcolorSchema)
	// {
	    // require_once "db.php";
	    // global $db;
	    // $colorSchema = array();
	    
	    // $getColor = "select * from colorSchemas where name = '" . $brandingcolorSchema . "'";
	    // $csResults = $db->query($getColor);
	    // $colorSchema["Colors"] = $csResults->fetch();
	    // return $colorSchema;
	// }
 	 
        //Code March 2018 to User Table Column Width
        function getTableWidth($tableName)
	{
	    require_once "db.php";
	    global $db;
	    $colWithArr = array();	    
	    $getColWidthSql = "select * from manageTblColumnsWidth where tableName = '$tableName '";
	    $csResults = $db->query($getColWidthSql);
            $colWithArr = $csResults->fetch();
	    return $colWithArr;
	}
        //End Code
	
	//function for customProfiles data check
	function checkCustomProfiles(){
	    require_once "db.php";
	    global $db;
	    $output = array();
	    $query = "select * from customProfiles";
	    $queryResults = $db->query($query);
	    while($result = $queryResults->fetch()){
	        $output[] = $result;
	    }
	    if(count($output) > 0 ){
	        return true;
	    }else{
	        return false;
	    }
	}

	//Logos
	if(file_exists("/var/www/logos") && !file_exists("/var/www/html/logos")) {
		symlink("/var/www/logos" , "/var/www/html/logos");
	}
	
        //Code added @ 22 May 2019        
        $_SESSION['changeLogAddlnlParamArr'] = array();
        $columnVal = array();
        $changeLogAddlnlParamArr  = createArrayofClusterBWAppServerFQDNForChangeLog($license["bwClusters"], $enableClusters, $clusterName, $bwAppServer, $bwfqdn);
        $_SESSION['changeLogAddlnlParamArr'] = $changeLogAddlnlParamArr;
        function createArrayofClusterBWAppServerFQDNForChangeLog($bwClusters, $enableClusters, $clusterName, $bwAppServer, $bwfqdn){
            $clusterSupport        =  (isset($bwClusters) && $bwClusters == "true") && $enableClusters == "true" ? true : false;
            $columnNames           =  ", clusterName, bwAppServer, fqdn";
            $columnValForPPStmt    =  ", ?, ?, ?";
            
            if(!$clusterSupport){
                 $clusterName = "None";
            }  
            if($clusterName == ""){
                $clusterName = "None";
            }
            if($bwfqdn == ""){
                $bwfqdn = "None";
            }
            
            
            $columnVal              = array($clusterName, $bwAppServer, $bwfqdn);
            $changeLogAddnlParamArr = array("coulmnNames"=> $columnNames, "columnValForPPStmt" => $columnValForPPStmt, "columnVal" => $columnVal);
            return $changeLogAddnlParamArr;
        }
        //End Code


if(!function_exists("http_post_fields")) {

	function http_post_fields($url, $data, $headers=null) {
		$data = http_build_query($data);
		$opts = array('http' => array('method' => 'POST', 'content' => $data));

		if($headers) {
			$opts['http']['header'] = $headers;
		}
		$st = stream_context_create($opts);
		$fp = fopen($url, 'rb', false, $st);

		if(!$fp) {
			return false;
		}
		return stream_get_contents($fp);
	}

}

function checkServerIsActive($serversDetail) {
    require_once("/var/www/lib/broadsoft/adminPortal/util/ServerConnCheckUtil.php");
    $response = new stdClass;
    
    $testConn =  new ServerConnCheckUtil();
    
    if($serversDetail->defaultAppServer->ip != "") {
        $primaryServerResponse = $testConn->check_login_connection($serversDetail->defaultAppServer->bwProtocol, $serversDetail->defaultAppServer->ip, $serversDetail->defaultAppServer->port, $serversDetail->defaultAppServer->userId, $serversDetail->defaultAppServer->password);
        if($primaryServerResponse) {
            $response->defaultServerConnected = true;
        } else {
            $response->defaultServerConnected = false;
        }
    }

    if($serversDetail->alternateAppServer->ip != "") {
        $alternateServerResponse = $testConn->check_login_connection($serversDetail->alternateAppServer->bwProtocol, $serversDetail->alternateAppServer->ip, $serversDetail->alternateAppServer->port, $serversDetail->alternateAppServer->userId, $serversDetail->alternateAppServer->password);
        if($alternateServerResponse) {
            $response->alternateServerConnected = true;
        } else {
            $response->alternateServerConnected = false;
        }
    }
    
    return $response;
}

function server_fail_over_debuggin_testing()
{
    global $db;
    
    $debugOn = false;
    if($debugOn) {
        $queryUpdate = 'update serversConfig set userName = "express_adminn" where id="1"';
        $stmt = $db->prepare($queryUpdate);
        $stmt->execute();
    }
}
?>
