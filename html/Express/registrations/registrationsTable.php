<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$regChoice = $_POST["regChoice"];

	if (date("i") >= 0 and date("i") <= 29)
	{
		$minutes = "00";
	}
	else
	{
		$minutes = "30";
	}
	$dateNow = date("Y-m-d H:" . $minutes . ":00");
	$dateThirtyMinutesAgo = date("Y-m-d H:i:s", strtotime("-30 minutes", strtotime($dateNow)));
	$dateSixtyMinutesAgo = date("Y-m-d H:i:s", strtotime("-60 minutes", strtotime($dateNow)));
	$dateTwentyFourHoursAgo = date("Y-m-d H:i:s", strtotime("-24 hours", strtotime($dateNow)));
	$dateSeventyTwoHoursAgo = date("Y-m-d H:i:s", strtotime("-72 hours", strtotime($dateNow)));
	$dateArray = array($dateNow, $dateThirtyMinutesAgo, $dateSixtyMinutesAgo, $dateTwentyFourHoursAgo, $dateSeventyTwoHoursAgo);
?>
<script>
	$(function() {
		$("#endUserId").html("<?php echo $regChoice; ?>");
	});
</script>
<div style="width:40%;margin-left:auto;margin-right:auto;">
	<table align="center" style="border-collapse:collapse;" border="1">
		<?php
			foreach($dateArray as $key)
			{
				$query = "select * from registrations where sp='" . $_SESSION["sp"] . "' and groupId='" . $regChoice . "' and dateTime='" . $key . "'";
				$result = $db->query($query);

				if ($result->rowCount() == 0)
				{
					$registered[] = "N/A";
					$nonregistered[] = "N/A";
				}
				else
				{
					while ($row = $result->fetch())
					{
						$registered[] = $row["registered"];
						$nonregistered[] = $row["notRegistered"];
					}
				}
			}
		?>
		<tr>
			<th style="width:25%;">&nbsp;</th>
			<th style="width:15%;"><?php echo date("F j<\b\\r>g:i A", strtotime($dateNow)); ?></th>
			<th style="width:15%;"><?php echo date("F j<\b\\r>g:i A", strtotime($dateThirtyMinutesAgo)); ?></th>
			<th style="width:15%;"><?php echo date("F j<\b\\r>g:i A", strtotime($dateSixtyMinutesAgo)); ?></th>
			<th style="width:15%;"><?php echo date("F j<\b\\r>g:i A", strtotime($dateTwentyFourHoursAgo)); ?></th>
			<th style="width:15%;"><?php echo date("F j<\b\\r>g:i A", strtotime($dateSeventyTwoHoursAgo)); ?></th>
		</tr>
		<tr>
			<th style="width:25%;">Registered Users</th>
			<?php
				for ($i = 0; $i < count($registered); $i++)
				{
					echo "<td class=\"macTable\" style=\"width:15%;\">" . $registered[$i] . "</td>";
				}
			?>
		</tr>
		<tr>
			<th style="width:25%;">Non-Registered Users</th>
			<?php
				for ($i = 0; $i < count($nonregistered); $i++)
				{
					echo "<td class=\"macTable\" style=\"width:15%;\">" . $nonregistered[$i] . "</td>";
				}
			?>
		</tr>
	</table>
</div>
