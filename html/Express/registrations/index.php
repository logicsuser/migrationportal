<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$serP = $_SESSION["sp"];
	require_once("/var/www/lib/broadsoft/adminPortal/getAllGroups.php");
?>
<script>
	$(function() {
		$("#module").html("> View Registrations: ");
		$("#endUserId").html("");

		$("#regChoice").change(function()
		{
			$("#regData").html("");
			var regChoice = $("#regChoice").val();

			if (regChoice !== "")
			{
				$.ajax({
					type: "POST",
					url: "registrations/registrationsTable.php",
					data: { regChoice: regChoice },
					success: function(result) {
						$("#regData").html(result);
					}
				});
			}
			else
			{
				$("#endUserId").html("");
			}
		});
	});
</script>
<div class="subBanner">View Registrations</div>
<div class="vertSpacer">&nbsp;</div>

<form method="POST" name="regForm" id="regForm">
	<div class="centerDesc">
		<label for="regChoice">Group</label>
		<span class="spacer">&nbsp;</span>
		<select name="regChoice" id="regChoice">
			<option value=""></option>
			<?php
				if (isset($allGroups))
				{
					foreach ($allGroups as $v)
					{
						echo "<option value=\"" . $v . "\">" . $v . "</option>";
					}
				}
			?>
		</select>
	</div>
</form>
<div id="regData"></div>
