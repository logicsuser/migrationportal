<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();

	require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");
	require_once("/var/www/lib/broadsoft/adminPortal/util/PreviousChangeGroupUtil.php");

	if(isset($_POST["userId"])) {
		$oldUserData = get_user_info($_POST["userId"], true);
	}

	$deleteQuery = "DELETE from users where id='" . $_POST["userId"] . "'";
	$deleteResult = $db->query($deleteQuery);

	$deleteQuery = "DELETE from permissions where id='" . $_POST["userId"] . "'";
	$deleteResult = $db->query($deleteQuery);
        
    //Code added @ 16 July 2018 to delete permissions from Features Permissions table
    $delete_stmt = $db->prepare("DELETE from featuresPermissions where userId = ?");
    $delete_stmt->execute(array($_POST["userId"]));
    //End code
    $preCG = new PreviousChangeGroupUtil();
    $changeGroupDetail = $preCG->deleteChangeGroupOfAdmin($_POST["userId"]);
    
	//Add Admin Log Entry - Start
	if(isset($_POST["userId"])) {
		require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
		$adminLogs = new adminLogs();
                
                $superUser      = $_SESSION["superUser"];
                $adminEmail     = $_SESSION["emailAddress"];
                $adminTypeName  = $_SESSION["adminType"];
                $adminUserName  = $_SESSION["loggedInUserName"];
                $lastActivity   = date("m/d/Y", $_SESSION["LAST_ACTIVITY"]);
                $tmpArr         = explode(" ", $_SESSION["loggedInUser"]);
                $adminFirstName = $tmpArr[0];
                $adminLastName  = $tmpArr[1];
                
                
		$log = array(
			'adminUserID' => $oldUserData['id'],
			'eventType' => 'DELETE_ADMIN',
			'adminUserName' => $oldUserData["userName"],
			'adminName' => $oldUserData["firstName"] . ' ' . $oldUserData["lastName"],
			'updatedBy' => $_SESSION["adminId"],
			'details' => array(
				'Deleted By'    => $_SESSION["loggedInUser"],
				'User Name'     => $adminUserName,
				'First Name'    => $adminFirstName,
				'Last Name'     => $adminLastName,
				'Email Address' => $adminEmail,
				'Super User'    => (isset($superUser) and $superUser == "1") ? "Yes" : "No",
				'Disabled'      => (isset($oldUserData["disabled"]) and $oldUserData["disabled"] == "1") ? "Yes" : "No",
				'Admin Type'    => $adminTypeName,
				"Last Login"    => $lastActivity
			)
		);
		$adminLogs->addLogEntry($log);
	}
	//Add Admin Log Entry - End

	echo "Administrator has been deleted.";
?>
