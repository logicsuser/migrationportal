<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getSystemDomains.php");
	
	if(isset($_POST["action"]) && $_POST["action"] == "getSysSecurityDomainList") {
	        global $systemDomains, $permissions, $securityDomainPattern;
	        $systemDomainsList = array();
	        foreach ($systemDomains as $key => $value){
	            if(strpos($value, $securityDomainPattern) !== false){
	                $systemDomainsList[] = $value;
	            }
	        }
	        
	        foreach ($systemDomainsList as $domain) {
                    if($domain == $_SESSION["securityDomain"]){
                        echo $domain ." ". ":";
                    }
	        }
	        die;
	}
?>
