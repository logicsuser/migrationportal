<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();
	require_once("/var/www/html/Express/functions.php");

	if(isset($_POST["securityDomain"])) {
	    $_POST["securityDomain"] = trim($_POST["securityDomain"]);
	}
        if(isset($_POST["clusterName"])) {
	    $_POST["clusterName"] = trim($_POST["clusterName"]);
	}
	$exp = explode(" ", $_POST["name"], 2);
	$firstName = $exp[0];
	$lastName = $exp[1];
	if ($_POST["superUser"] == "1" or $permissionsBwAuthentication == "false") {
		$bsAuthentication = "0";
		$bsSp = "";
		$bsGroupId = "";
	}
	else
	{
		$bsAuthentication = "1";
		$bsSp = $_POST["bsSp"];
		$bsGroupId = $_POST["bsGroupId"];
	}

	if($isAdminPasswordFieldVisible == 'true') {
		$password = md5($_POST["password"]);
		$lastPasswordChange = "Now()";
	} else{
		$password = bin2hex(openssl_random_pseudo_bytes(10));
		$lastPasswordChange = "";
	}

	$adminType = "";
	$adminTypeName = "";
	if (isset($_POST["checkPermissions"]) && $_POST["checkPermissions"] != "" &&
        $_POST["checkPermissions"] != "Select All" && $_POST["checkPermissions"] != "Clear") {            
        $adminType = $_POST["checkPermissions"];
        $sqladminType = "select name from adminTypesLookup  where adminTypeIndex=".$_POST["checkPermissions"];
        $resultadminType = $db->query($sqladminType);
            if($row = $resultadminType->fetch()) {
                $adminTypeName = $row['name'];
            }        
	}
        //echo "<br />Admin Type Name - ".$adminTypeName;
	$stmt = $db->prepare('INSERT into users (userName, firstName, lastName, password, emailAddress, superUser, disabled, lastPasswordChange, bsAuthentication,clusterName, bsSp, bsGroupId,user_cell_phone_number,use_sms_every_login,use_sms_on_password_reset, adminTypeIndex, adminType, use_ldap_authentication, cluster_name_selected) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
	$stmt->execute(
		array(
			$_POST["userName"],
			$firstName,
			addslashes($lastName),
			$password,
			addslashes($_POST["emailAddress"]),
			0,
			0,
			$lastPasswordChange,
			$bsAuthentication,
                        $_POST["clusterName"],
			$bsSp,
			$bsGroupId,
			$_POST["user_cell_phone_number"],
			$_POST["use_sms_every_login"],
			$_POST["use_sms_on_password_reset"],
			$adminType,
			$adminTypeName,
			$_POST["use_ldap_authentication"] ? $_POST["use_ldap_authentication"] : 0,
                        $_POST["clusterName"]
		)
	);
	     
	$lastId = $db->lastInsertId();
        
        
        //Code added @ 13 July 2018
        $sqlUpdatePassHistory = $db->prepare('INSERT into adminPasswordHistory (userId, password, modifiedOn) VALUES (?,?,?)');
	$currDate = date("Y-m-d h:i:s");
        $sqlUpdatePassHistory->execute(
		array(
			$lastId,
			$password,
			$currDate
		)
	);
        //End code

	//Send an Email to the user to set his password
	if($isAdminPasswordFieldVisible == 'false' && $lastId && $_POST["use_ldap_authentication"] != 1) {

		$token = sha1(uniqid(bin2hex(openssl_random_pseudo_bytes(32)), true));

		$update_temporary_token = $db->prepare('UPDATE users SET temporary_token = ? WHERE id = ? LIMIT 1');
		$update_temporary_token->execute(array($token, $lastId));

		$link = "https://" . $_SERVER['HTTP_HOST'] . "/Express/reset_password.php?token=" . $token;
		$to = trim($_POST["emailAddress"]);

		$subject = 'Welcome - Please set your Password';

		$message = "<a href=\"$link\">Click here</a> to set your password. If the link does not work copy this url: <a href=\"$link\">$link</a>  and paste it in your browser.";

		send_email($to, $subject, $message);

	}

	//if user is not a super user, set group permissions
	if ($_POST["superUser"] != "1")
	{
            if ($securityDomainPattern != "") {
            // Set permissions based on security domain

            $uFields = array('id', 'securityDomain');
            $uValues = array($lastId, $_POST["securityDomain"]);
            
            $insert_sql = "INSERT INTO permissions (userId, securityDomain)" . "VALUES " . "(?, ?)";
            $insert_stmt = $db->prepare($insert_sql);
            $insert_stmt->execute($uValues);
             
            ///Newly Added
            if (isset($_POST["perms"]))
            {
                $insertValue = array();
                foreach ($_POST["perms"] as $k => $v)
                {
                        if($v == "1") {
                            $insertValue[] = array($lastId, $adminTypeIndex, $k);
                        }
                }
            }
            foreach($insertValue as $key => $value) {
                $params_tagFP = "(?, ?, ?)";
                $insert_sql = "INSERT into featuresPermissions (userId, adminTypeIndex, permissionId)" . "VALUES " . $params_tagFP;
                $insert_stmt = $db->prepare($insert_sql);
                $insert_stmt->execute($value);
            }
            //End Code

        }
        else 
        {
            $groups = explode(";", $_POST["groups1"]);
            unset($groups[count($groups) - 1]);
            foreach ($groups as $v)
            { 
                $uValues = array($lastId, $_POST["Selsp"], $v);
                $insert_sql = "INSERT INTO permissions (userId, sp, groupId)" . "VALUES " . "(?, ?, ?)";
                $insert_stmt = $db->prepare($insert_sql);
                $insert_stmt->execute($uValues);
            }
                
                //Adding permissions into featuresPermissions table
                if (isset($_POST["perms"]))
                {
                    $insertValue = array();
                    foreach ($_POST["perms"] as $k => $v)
                    {
                            if($v == "1") {
                                $insertValue[] = array($lastId, $adminTypeIndex, $k);
                            }
                    }
                }
                foreach($insertValue as $key => $value) 
                {
                    $params_tagFP = "(?, ?, ?)";
                    $insert_sql = "INSERT into featuresPermissions (userId, adminTypeIndex, permissionId)" . "VALUES " . $params_tagFP;
                    $insert_stmt = $db->prepare($insert_sql);
                    $insert_stmt->execute($value);
                }                
        }
        }
//}

	//Add Admin Log Entry - Start
	require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
	$adminLogs = new adminLogs();
        
        $superUser      = $_SESSION["superUser"];
        $adminEmail     = $_SESSION["emailAddress"];
        $adminTypeName  = $_SESSION["adminType"];    
        
        
	$log = array(
		'adminUserID' => $lastId,
		'eventType' => 'ADD_ADMIN',
		'adminUserName' => $_POST["userName"],
		'adminName' => $firstName . ' ' . $lastName,
		'updatedBy' => $_SESSION["adminId"],
		'details' => array(
			'Added By' => $_SESSION["loggedInUser"],
			'Email Address' => $adminEmail,
			'Super User' => (isset($superUser) and $superUser == "1") ? "Yes" : "No",
			'Disabled' => 'No',
			'Admin Type' => $adminTypeName,
			"BS Authentication" => (isset($bsAuthentication) and $bsAuthentication == "1") ? "Yes" : "No",
			"BS Sp" => $bsSp,
			"BS Group ID" => $bsGroupId,
			"Cell Phone Number" => $_POST["user_cell_phone_number"],
			"Use SMS On Login" => (isset($_POST["user_cell_phone_number"]) and $_POST["user_cell_phone_number"] == "1") ? "Yes" : "No",
			"Use LDAP Authentication" => (isset($_POST["use_sms_every_login"]) and $_POST["use_sms_every_login"] == "1") ? "Yes" : "No",
			"Use SMS On Password Reset" => (isset($_POST["use_sms_on_password_reset"]) and $_POST["use_sms_on_password_reset"] == "1") ? "Yes" : "No",
		)
	);
	$adminLogs->addLogEntry($log);
	//Add Admin Log Entry - End

	echo "Administrator created successfully.";
?>
