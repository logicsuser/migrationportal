<?php
/**
 * Created by Karl.
 * Date: 5/15/2017
 */
    require_once("/var/www/html/Express/config.php");
    checkLogin();
	
    if(isset($_POST["action"]) && $_POST["action"] == "getAllPermissionscategories") {
        $allPermissionCategories = getAllPermissionscategories($db, $_POST["defaultPermission"]);
        print_r(json_encode($allPermissionCategories));
        die;
    }
    
    $licenseArray = array();
    if(isset($license["vdmLite"]) && $license["vdmLite"] == "false"){
    	$licenseArray[] = "vdmLight";
    	$licenseArray[] = "vdmAdvanced";
    }
    
    if (isset($_POST["permissionSet"])) {
        
        $permissions = array();
        //Code added @ 06 March 2019
        $permissionsList        = $_SESSION['permissionsList']; 
        $prmnsNotExistWarnFlag  = "no";
        //End code
        
        $query = "select * from defaultAdminPermissions where adminTypeIndex=". $_POST["permissionSet"];
        $result = $db->query($query);
        
        while ($row = $result->fetch()) {
            
            if(in_array($row['permissionId'], $permissionsList)){
                $permissions[] = $row['permissionId'];
                $columns[]     = $row['permissionId']; 
            }  
            if(!in_array($row['permissionId'], $permissionsList)){
                $prmnsNotExistWarnFlag = "yes";
            }            
        }
        //Code added @ 06 March 2019
        if($prmnsNotExistWarnFlag == "yes"){
            $permissions[] = "Attention";
        }
        //End code
        echo join(",", $permissions);
    }
    
    
    function getAllPermissionscategories($db, $defaultPermission) {
        $defaultPermission = explode(",", $defaultPermission);
        foreach($defaultPermission as $key => $val){
            //$explodedData = explode(":", $val);
           // if($explodedData[1] == 1){
              $allData[] = $val;
            //}
        }
        
        $allData = join("','", $allData);
        $permissionsCategory = array();
        $query = "select * from permissionsCategories where permissionId in ('$allData') group By category";
        $result = $db->query($query);
        while ($row = $result->fetch()) {
            $permissionsCategory[] = $row["category"];
        }
        return $permissionsCategory;
    }
?>
