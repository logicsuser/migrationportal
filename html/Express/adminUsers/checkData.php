<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();
	require_once ("/var/www/lib/broadsoft/adminPortal/PreventPasswordOperation.php");
        $password = isset($_POST["passwordUpdate"]) ? $_POST["passwordUpdate"] : "";
        $objPPO = new PreventPasswordOperation($_SESSION["adminUsers"]["userNameUpdate"], md5($password));
	
	$hidden = array("userId", "superUserUpdate");
	if ($securityDomainPattern == "" || $_POST["superUserUpdate"] == "1") {
		$hidden[] = "securityDomain";
	}

	$isSuperUser = (isset($_POST["superUser"]) && $_POST["superUser"] == "1") || (isset($_POST["superUserUpdate"]) && $_POST["superUserUpdate"] == "1");

	$required = array("userName", "userNameUpdate", "name", "nameUpdate", "emailAddress", "emailAddressUpdate");
	if ($securityDomainPattern != "" && $_POST["superUserUpdate"] != "1") {
	    $required[] = "checkPermissions";
    }
    

    /*if($isSuperUser !== "1"){
		if(!array_key_exists("checkPermissions", $_POST)){
			$_POST['checkPermissions'] = "Clear";
		}
	}*/
    
    $assignedPermissions = isset($_POST["assignedPermissions"]) ? $_POST["assignedPermissions"] : array();
    unset($_POST["assignedPermissions"]);
    unset($_POST["permissionRadio"]);
    
    unset($_POST["clusterName"]);
    unset($_POST["clusterNameUpdate"]);
    
    $verifyPassword = isset($_POST ["verifyPassword"]) ? $_POST ["verifyPassword"] : "";
    if( isset($_POST ["verifyPassword"]) ) {
        unset($_POST ["verifyPassword"]);
    }
    
//     unset($_POST["permissionRadio"]);
    
    // Get Admin type
    $queryATI = "select name from adminTypesLookup where adminTypeIndex='" . $_POST["checkPermissions"] . "'";
    $resultATI = $db->query($queryATI);
    while ($row = $resultATI->fetch()) {
        $_POST["checkPermissions"] = $row["name"];
    }
    
    
	$data = $errorTableHeader;

	$changes = 0;
	$error = 0;
	    
	foreach ($_POST as $key => $value)
	{
	    
		if (!in_array($key, $hidden)) //hidden form fields that don't change don't need to be checked or displayed
		{
		    // for no security domain exist.
		    if($_POST["sp"] != "") {
		        if ($key !="permsUpdate" && (isset($_POST["userId"]) and isset($_SESSION["adminUsers"][$key]) and  $value !== $_SESSION["adminUsers"][$key]) or !isset($_POST["userId"]))
		        {
		            $changes++;
		            $bg = CHANGED;
		        }
		        else
		        {
		            $bg = UNCHANGED;
		        }
		    }
            
			// if security domain exist
		    if($_POST["sp"] == "") {
			    if ((isset($_POST["userId"]) and isset($_SESSION["adminUsers"][$key]) and  $value !== $_SESSION["adminUsers"][$key]) or !isset($_POST["userId"]))
			    {
			        $changes++;
			        $bg = CHANGED;
			    }
			    else
			    {
			        $bg = UNCHANGED;
			    }
			}
			
			
			
			if (in_array($key, $required) and $value == "")
			{
				$error = 1;
				$bg = INVALID;
				$value = $_SESSION["adminUsersNames"][$key] . " is a required field.";
			}
			else if ($key == "userName" or $key == "userNameUpdate")
			{
				//check if username already exists
				$select = "SELECT u.userName from users u where u.userName='" . $value . "'";
				if (isset($_POST["userId"]))
				{
					$select .= " and id <> " . $_POST["userId"];
				}
				$lookup = $db->query($select);
				if ($lookup->fetch(MYSQL_ASSOC))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["adminUsersNames"][$key] . " already exists.";
				}
				//else if (!preg_match("/^[A-Za-z0-9\.@]+$/", $value))
				else if (!preg_match("/^[A-Za-z0-9\-_.@]+$/", $value))
				{
					$error = 1;
					$bg = INVALID;
					//$value = $_SESSION["adminUsersNames"][$key] . " must include letters, numbers, \"@\", and \".\" only.";
					$value = $_SESSION["adminUsersNames"][$key] . " must include letters, numbers , \"@\" ,  \".\" , \"_\" , \"-\" only.";
				}
			}
			else if ($key == "name" or $key == "nameUpdate")
			{
				if (!preg_match("/^[A-Za-z-]+[ ][A-Za-z',\. -]+$/", $value))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["adminUsersNames"][$key] . " must include both a first and last name and be valid (characters such as numbers are not permitted).";
				}
			}
			else if ($key == "password" or $key == "passwordUpdate")
			{
			    if ($key == "password" and $value == "" && $_POST["use_ldap_authentication"] == "0")
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["adminUsersNames"][$key] . " is a required field.";
				}
				else if ($value != "" and !checkPasswordStrength($adminPasswordRules, $value, $errors))
				{
					$error = 1;
					$bg = INVALID;
					$value = implode('<br/>',$errors);
				}
				else if ($value != "" and preg_match("/[&]+/", $value))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["adminUsersNames"][$key] . " must not include an ampersand (\"&\").";
				}
                                else if($objPPO->passwordReuseValidationBasedOnPreventPassConfig())
                                {
                                        $error = 1;
                                        $bg = INVALID;
                                        $value = "Password has been used already. Try another.";
                                }
                else if ( ($value != "" || $verifyPassword != "") && $verifyPassword != $value)
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Password and Verify Password do not match";
                }
				else if ($value)
				{
					$value = str_repeat("*", strlen($value)); //don't display password
				}
			}
// 			else if ($key == "emailAddress")
// 			{
//                 // super users' email must be an active domain
//                 $domain = ($groupsDomainType == "Static" && $staticDomain != "") ? $staticDomain : $_SESSION["systemDefaultDomain"];
// 				if (!preg_match("/" . $domain . "/i", $value) and $_POST["superUser"] == "1")
// 				{
// 					$error = 1;
// 					$bg = INVALID;
// 					$value = $_SESSION["adminUsersNames"][$key] . " must include domain \"" . $domain . "\".";
// 				}
// 			}
            else if ($key == "emailAddress") {
                // super users' email must be an active domain
			    if ($groupsDomainType == "Static" && $staticDomain != "" and $_POST["superUser"] == "1") {
			        if (in_array($key, $required) and $value == "") {
			            $error = 1;
			            $bg = INVALID;
			            $value = $_SESSION["adminUsersNames"][$key] . " is a required field.";
			        }
			    }
			}
                        
             else if ($key == "clusterName" || $key == "clusterNameUpdate") {
           
                if (in_array($key, $required) and $value == "") {
                    $error = 1;
                    $bg = INVALID;
                    $value = $_SESSION["adminUsersNames"][$key] . " is a required field.";
                }
                        
                    }           
                        
                        
                        
			else if ($key == "sp")
			{ 
				if(empty($securityDomainPattern)){
					if($value == ""){
						$error = 1;
						$bg = INVALID;
						$value = $_SESSION["adminUsersNames"][$key] . " must not be null.";
					}
				}else{
					if(!empty($_POST["userId"])){
						$bg = UNCHANGED;
						$changes--;
					}
				}
				
			}
			else if ($key == "Selsp" )
			{ 
				if(empty($securityDomainPattern)){
					if($value == ""){
						$error = 1;
						$bg = INVALID;
						$value = $_SESSION["adminUsersNames"][$key] . " must not be null.";
					}
				}else{
					if(!empty($_POST["userId"])){
						$bg = UNCHANGED;
						$changes--;
					}
				}
				
			}
			else if ($key == "groups1" && $securityDomainPattern == "")
			{
				if ($value == "" and $_POST["superUser"] !== "1")
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["adminUsersNames"][$key] . " must be selected if user is not a Super User.";
				}
			}
			else if ($key == "groups_1" && $securityDomainPattern == "")
			{
				if($_POST["superUserUpdate"] !== "1"){
					if ($value == "" and $_POST["superUser"] !== "1")
					//if ($value == "" and (isset($_POST["superUser"]) && $_POST["superUser"] !== "1"))
					{
						$error = 1;
						$bg = INVALID;
						$value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
					}
				}
			}
			else if ($key == "securityDomain" && $securityDomainPattern != "") {
			    //if (($value == "None" || $value == "") && $_POST["superUserUpdate"] != "1") {
			    if (($value == "None" || $value == "") && ! $isSuperUser) {
			        $error = 1;
			        $bg = INVALID;
                    $value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
			    }else if (($value <> "" && $_SESSION["adminUsers"][$key] == $value) && ! $isSuperUser) {
			    	//$changes++;
			    	$bg = UNCHANGED;
                }
            }
            else if ($key == "user_cell_phone_numberUpdate") { 
				if (($value == "None" || $value == "") && $_SESSION["adminUsers"][$key] == $value) {
			        //$error = 1;
			        $bg = UNCHANGED;
                   // $value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
                }
            }
            /*else if ($key == "checkPermissions") { 
            	if($isSuperUser <> "1"){
				    if ($value == "Select All" || $value == "Clear" || $value == "None") {
	                    $error = 1;
	                    $bg = INVALID;
	                    $value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
	                }
            	}else{
            		$bg = UNCHANGED;
            		//$error = 1;
            		$changes--;
            	}
            }*/
            else if($key == "checkPermissions"){
            	if($isSuperUser <> "1"){
            		if ($value == "Select All" || $value == "Clear" || $value == "None") {
            			$error = 1;
            			$bg = INVALID;
            			$value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
            		}
            	}else{
            		$bg = UNCHANGED;
            		//$error = 1;
            		$changes--;
            	}
            }
			//else if (($key == "perms" and $_POST["superUser"] !== "1") or ($key == "permsUpdate" and $_POST["superUserUpdate"] !== "1" and $_POST["superUserUpdate"] !== "2"))
			else if (($key == "perms" and $_POST["superUser"] !== "1") or ($key == "permsUpdate" and ! $isSuperUser))
			{
			    $perHasChanged = false;
				$setPerms = "false";
				foreach ($value as $k => $v)
				{
					if ($v == "1")
					{
						$setPerms = "true";
					}
				}
				
				// check permission has changed in modify
				if($key == "permsUpdate") {
				    foreach ($value as $k => $v)
				    {
				        if(isset($assignedPermissions[$k]) && $assignedPermissions[$k] <> $v || ( !isset($assignedPermissions[$k]) && $v == "1") ) {
				            $changes++;
				            $bg = CHANGED;
				            $perHasChanged = true;
				        }
				    }
				    
				}
				if ($setPerms !== "true")
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["adminUsersNames"][$key] . " must be selected.";
				}
			}

//			if ($error == 1)
//			{
//				$bg = INVALID;
//			}
//			else
//			{
//				$bg = UNCHANGED;
//			}
			if ($bg != UNCHANGED)
			{
			    if (! is_array($value)) //for permissions array
			    {
			        $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["adminUsersNames"][$key] . "</td><td class=\"errorTableRows\">";
			    }				
				
				if (is_array($value)) //for permissions array
				{
				    if($perHasChanged || $key == "perms") {
				        $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["adminUsersNames"][$key] . "</td><td class=\"errorTableRows\">";
				    }
				    // for add
				    if($key == "perms") {
				        foreach ($value as $k => $v)
				        {
				            if ($v == "1")
				            {
				                $data .= $_SESSION["fieldNamesList"][$k] . "<br />"; //Code added @ 07 May 2018 for remove br tags in permissions on administratpor check data page
				                //$data .= $_SESSION["fieldNames"][$k] . "<br />";
				            }
				        }
				    }
					
					// For modify
					if($key == "permsUpdate") {                                            
					    foreach ($value as $k => $v)
					    {
					        if (isset($assignedPermissions[$k]) && $v <> $assignedPermissions[$k] || (!isset($assignedPermissions[$k]) && $v == "1") )
					        {   
                                                    //Code commented @ 25 July 2018
					            //$data .= $_SESSION["fieldNamesList"][$k] . "<br />"; //Code added @ 07 May 2018 for remove br tags in permissions on administratpor check data page
					            //$data .= $_SESSION["fieldNames"][$k] . "<br />";
                                                    
                                                    //Code added @ 25 July 2018
                                                    if($v=="1"){
                                                        $data .= "<strong>Added:</strong> ".$_SESSION["fieldNamesList"][$k] . "<br />";
                                                    }else{
                                                        $data .= "<strong>Removed:</strong> ".$_SESSION["fieldNamesList"][$k] . "<br />";
                                                    }
                                                    //End code
                                                    //$data .= $_SESSION["fieldNames"][$k] . "<br />";
					        }
					    }
                                            //echo "<br />Tmp Array - ";
                                            //print_r($tmpArr);
					}
					
				}
				else if ($key == "superUser" or $key == "disabled" or $key == "use_sms_every_login" or $key == "use_sms_on_password_reset" or $key == "use_sms_every_loginUpdate" or $key == "use_sms_on_password_resetUpdate" or $key == "use_ldap_authentication")
				{
					if ($value == "1")
					{
						$data .= "True";
					}
					else
					{
						$data .= "False";
					}
				}
				else if ($value !== "")
				{
					$data .= $value;
				}
				else
				{
					$data .= "None";
				}
				$data .= "</td></tr>";
			}
		}
	}
	$data .= "</table>";
	//echo "Change ".$changes;echo "Error ".$error;
	if ($changes == 0 && $error  == 0)
	{
		$error = 1;
		$data = "<label class=\"labelTextGrey\">You have made no changes.</label>";
	}
	echo $error . $data;
?>
