<?php

/**
 * Created by Karl.
 * Date: 1/19/2017
 */
class DBAccessPrivileges
{

    private $db;
    private $txt = "";
    private $newRecord = true;
    private $accessPrivilegeKeys = array();

    //private $id;
    private $adminID;
    private $rdLevelID;
    private $rdLevelName;
    private $wrLevelID = 0;
    private $wrLevelName = "";
    private $rdAccessPrivileges = array();
    private $wrAccessPrivileges = array();

    private static function tableName() { return "accessPrivileges"; }

    //private static function attrID()                   { return "id"; }
    private static function attrAdminID()              { return "adminID"; }
    private static function attrlevelID()              { return "levelID"; }
    private static function attrLevelName()            { return "levelName"; }
    private static function attrGroupWideUserDelete()  { return "groupWideUserDelete"; }
    private static function attrBulkUserAdd()          { return "bulkUserAdd"; }
    private static function attrViewCDRs()             { return "viewCDRs"; }
    private static function attrModifyUserDN()         { return "modifyUserDN"; }
    private static function attrGroupWideUserModify()  { return "groupWideUserModify"; }
    private static function attrDeleteUser()           { return "deleteUser"; }
    private static function attrAddUsers()             { return "addUsers"; }
    private static function attrModifyDevice()         { return "modifyDevice"; }
    private static function attrDeviceCustomTags()     { return "deviceCustomTags"; }
    private static function attrSharedCallAppearance() { return "sharedCallAppearance"; }
    private static function attrCustomContactDir()     { return "customContactDir"; }
    private static function attrSchedules()            { return "schedules"; }
    private static function attrAutoAttendant()        { return "autoAttendant"; }
    private static function attrCallCenter()           { return "callCenter"; }
    private static function attrHuntGroup()            { return "huntGroup"; }
    private static function attrBLF()                  { return "blf"; }
    private static function attrHoteling()             { return "hoteling"; }
    private static function attrCallControl()          { return "callControl"; }
    private static function attrModifyVM()             { return "modifyVM"; }
    private static function attrModifyBasicInfo()      { return "modifyBasicInfo"; }
    private static function attrViewChangeLog()        { return "viewChangeLog"; }
    private static function attrViewNumbers()          { return "viewNumbers"; }
    private static function attrChangeUserPassword()   { return "changeUserPassword"; }
    private static function attrModifyUsers()          { return "modifyUsers"; }

    /**
     * DBAccessPrivileges constructor.
     * @param $db
     * @param string $adminID
     */
    public function __construct($db, $adminID) {
        $this->db = $db;
        $this->adminID = $adminID;

        $this->registerAccessPrivileges();
        $this->get();
    }

    public function getTxt() {
        return $this->txt;
    }

    /*************************************************
     * @return mixed
     */
    public function getLevelID() {
        return $this->rdLevelID;
    }

    /*************************************************
     * @return mixed
     */
    public function getLevelName() {
        return $this->rdLevelName;
    }

    /*************************************************
     * @param $levelID
     * @param $levelName
     */
    public function setLevel($levelID, $levelName) {
        if ($levelID < 1 || $levelID > 6) { return; }
        if ($levelName == "") { return; }

        $this->wrLevelID = $levelID;
        $this->wrLevelName = $levelName;
    }

    /*************************************************
     * @param $attr
     * @return mixed|string
     */
    public function getAccessPrivilege($attr) {
        foreach ($this->rdAccessPrivileges as $key => $accessPrivilegeValue) {
            $this->txt .= "key:" . $key . ":" . $accessPrivilegeValue . " ";
            if ($attr == $key) {
                return $accessPrivilegeValue;
            }
        }

        return "";
    }


    /*************************************************
     * @param $attr
     * @param $value
     */
    public function setAccessPrivilege($attr, $value) {
        if (! $this->isAccessPrivilege($attr)) {return; }
        if ($value != "false" && $value != "true") { return; }

//        $this->wrAccessPrivileges[] = array($attr => $value);
        $this->wrAccessPrivileges[$attr] = $value;
    }


    /*************************************************
     * @return string
     */
    public function getAdminID() {
        return $this->adminID;
    }


    /*************************************************
     * @param $adminID
     */
    public function setAdminID($adminID) {
        $this->adminID = $adminID;
    }


    /*************************************************
     * @param $sourceAdminID
     */
    public function cloneAccessPrivileges($sourceAdminID) {
        $this->clonePrivileges($sourceAdminID);
    }


    /*************************************************
     *
     */
    public function cloneSuperUserAccessPrivileges() {
        $this->clonePrivileges();
    }


    /*************************************************
     *
     */
    public function write() {
        if ($this->wrLevelName == "" && ! isset($this->wrAccessPrivileges)) { return; }

        $query = $this->newRecord ? $this->formatForInsert() : $this->formatForUpdate();
        $this->db->query($query);
    }

    /*************************************************
     *
     */
    public function delete() {
        //delete from accessPrivileges where adminID='test4';

        $query = "delete from " . self::tableName() . " where " . self::attrAdminID() . "='" . $this->adminID . "'";
        $this->db->query($query);
    }


    //-----------------------------------------------------------------------------------------------------------------
    private  function registerAccessPrivileges() {
        $this->accessPrivilegeKeys[] = self::attrGroupWideUserDelete();
        $this->accessPrivilegeKeys[] = self::attrBulkUserAdd();
        $this->accessPrivilegeKeys[] = self::attrViewCDRs();
        $this->accessPrivilegeKeys[] = self::attrModifyUserDN();
        $this->accessPrivilegeKeys[] = self::attrGroupWideUserModify();
        $this->accessPrivilegeKeys[] = self::attrDeleteUser();
        $this->accessPrivilegeKeys[] = self::attrAddUsers();
        $this->accessPrivilegeKeys[] = self::attrModifyDevice();
        $this->accessPrivilegeKeys[] = self::attrDeviceCustomTags();
        $this->accessPrivilegeKeys[] = self::attrSharedCallAppearance();
        $this->accessPrivilegeKeys[] = self::attrCustomContactDir();
        $this->accessPrivilegeKeys[] = self::attrSchedules();
        $this->accessPrivilegeKeys[] = self::attrAutoAttendant();
        $this->accessPrivilegeKeys[] = self::attrCallCenter();
        $this->accessPrivilegeKeys[] = self::attrHuntGroup();
        $this->accessPrivilegeKeys[] = self::attrBLF();
        $this->accessPrivilegeKeys[] = self::attrHoteling();
        $this->accessPrivilegeKeys[] = self::attrCallControl();
        $this->accessPrivilegeKeys[] = self::attrModifyVM();
        $this->accessPrivilegeKeys[] = self::attrModifyBasicInfo();
        $this->accessPrivilegeKeys[] = self::attrViewChangeLog();
        $this->accessPrivilegeKeys[] = self::attrViewNumbers();
        $this->accessPrivilegeKeys[] = self::attrChangeUserPassword();
        $this->accessPrivilegeKeys[] = self::attrModifyUsers();
    }

    //-----------------------------------------------------------------------------------------------------------------
    private function get() {
//      select * from accessPrivileges where adminID='__defaultLevel6'

        $query = "select * from " . self::tableName() . " where " . self::attrAdminID() . "='" . $this->adminID . "'";
        $result = $this->db->query($query);

        while ($row = $result->fetch()) {
            //$this->id = $row[self::attrID()];
            //$this->adminID = $row[self::attrAdminID()];
            $this->rdLevelID = $row[self::attrlevelID()];
            $this->rdLevelName = $row[self::attrLevelName()];

            $this->newRecord = false;

            for ($i = 0; $i < count($this->accessPrivilegeKeys); $i++) {
                $key = $this->accessPrivilegeKeys[$i];
                $this->rdAccessPrivileges[$key] = $row[$key];
            }
        }
    }

    //-----------------------------------------------------------------------------------------------------------------
    private function isAccessPrivilege($attr) {
        for ($i = 0; $i < count($this->accessPrivilegeKeys); $i++) {
            if ($attr == $this->accessPrivilegeKeys[$i]) {
                return true;
            }
        }

        return false;
    }

    //-----------------------------------------------------------------------------------------------------------------
    // Note: Empty $sourceAdminID is when cloning is done for super users, which is always Level 0 and all AP = "true"
    //
    private function clonePrivileges($sourceAdminID="") {
        $clonedAP = $sourceAdminID != "" ? new DBAccessPrivileges($this->db, $sourceAdminID) : null;
        $this->wrLevelID   = $sourceAdminID != "" ? $clonedAP->getLevelID()   : 0;
        $this->wrLevelName = $sourceAdminID != "" ? $clonedAP->getLevelName() : "";

        for ($i = 0; $i < count($this->accessPrivilegeKeys); $i++) {
            $key = $this->accessPrivilegeKeys[$i];
            $val = $sourceAdminID != "" ? $clonedAP->getAccessPrivilege($key) : "true";
            //$this->wrAccessPrivileges[] = array($key, $val);
            $this->wrAccessPrivileges[$key] = $val;
        }
    }

    //-----------------------------------------------------------------------------------------------------------------
    private function formatForUpdate() {
        // build the query
        // update accessPrivileges set modifyUsers='true' where id='6';
        $hasFirstParam = false;
        $query = "update " . self::tableName() . " set";

        if ($this->wrLevelName != "") {
            $query .= " "  . self::attrlevelID()   . "='" . $this->wrLevelID   . "'";
            $query .= ", " . self::attrLevelName() . "='" . $this->wrLevelName . "'";
            $hasFirstParam = true;
        }

        foreach ($this->wrAccessPrivileges as $attr => $value) {
            if ($hasFirstParam) {
                $query .= ",";
            } else {
                $hasFirstParam = true;
            }
            $query .= " " . $attr . "='" . $value . "'";
        }

        $query .= " where " . self::attrAdminID() . "='" . $this->adminID . "'";
$this->txt .= "UPDATE:" . $query . " ";
        return $query;
    }

    //-----------------------------------------------------------------------------------------------------------------
    private function formatForInsert() {
        //INSERT INTO `accessPrivileges` (`adminID`,`levelID`,`levelName`) VALUES ('__defaultLevel1','1','Level I');
        //INSERT INTO `accessPrivileges` (`adminID`,`levelID`,`levelName`,`groupWideUserDelete`,`bulkUserAdd`,`viewCDRs`,`modifyUserDN`) VALUES ('__defaultLevel2','2','Level II','false','false','false','false');

        $query = "insert into " . self::tableName() . " (`" . self::attrAdminID() . "`";
        $valQuery = "values ('" . $this->getAdminID() . "'";

        if ($this->wrLevelName != "" ) {
            $query    .= ",`" . self::attrlevelID() . "`,`" . self::attrLevelName() . "`";
            $valQuery .= ",'" . $this->wrLevelID    . "','" . $this->wrLevelName    . "'";
        }

        foreach ($this->wrAccessPrivileges as $attr => $value) {
            $query    .= ",`" . $attr  . "`";
            $valQuery .= ",'" . $value . "'";
        }

        $query    .= ")";
        $valQuery .= ")";

        $query .= " " . $valQuery;
$this->txt .= "INSERT:" . $query . " ";
        return $query;
    }

}
