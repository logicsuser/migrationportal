<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();

    require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");
	if(isset($_POST["userId"])) {
		$oldUserData = get_user_info($_POST["userId"], true);
	}

	$exp = explode(" ", $_POST["nameUpdate"], 2);

	//update personal data
    $update_sql = 'UPDATE users SET '
        . 'userName = ?,'
        . 'disabled = ?,'
        . 'emailAddress = ?,'
        . 'user_cell_phone_number = ?,'
        . 'use_sms_every_login = ?,'
        . 'use_sms_on_password_reset = ?,'
        . 'use_ldap_authentication = ?,'
        . 'firstName = ?,'
        . 'lastName = ?,'
        . 'clusterName = ?';

    $params = array(
        $_POST["userNameUpdate"],
        $_POST["disabled"],
        addslashes($_POST["emailAddressUpdate"]),
        $_POST["user_cell_phone_numberUpdate"],
        $_POST["use_sms_every_loginUpdate"],
        $_POST["use_sms_on_password_resetUpdate"],
        $_POST["use_ldap_authentication"],
        $exp[0],
        addslashes($exp[1]),
        $_POST["clusterNameUpdate"]
        
    );


	$adminType = "";
	$adminTypeIndex = "";
    if (isset($_POST["checkPermissions"]) && $_POST["checkPermissions"] != "" &&
        $_POST["checkPermissions"] != "Select All" && $_POST["checkPermissions"] != "Clear") {
            $adminTypeIndex = $_POST["checkPermissions"];
    }
    
    $update_sql .= ", adminTypeIndex = ? ";
    $update_sql .= ", adminType = ? ";
    $params[] = $adminTypeIndex;
    
    $queryATI = "select name from adminTypesLookup where adminTypeIndex='" . $adminTypeIndex . "'";
    $resultATI = $db->query($queryATI);
    while ($row = $resultATI->fetch()) {
        $adminType = $row["name"];
    }
    $params[] = $adminType;

    if ($isAdminPasswordFieldVisible == "true") {
        if (isset($_POST["passwordUpdate"]) and strlen($_POST["passwordUpdate"]) > 0) {
            $password = md5($_POST["passwordUpdate"]);
            $update_sql .= ", password= ?, lastPasswordChange = Now()";
            //reset number of failed login attempts
            $update_sql .= ", failedLogins = 0";

            $params[] = $password;
        }
    }

	if (!isset($_POST["disabled"]) || $_POST["disabled"] != '1')
	{
		//reset number of failed login attempts
		$update_sql .= ", failedLogins = 0";
	}

	$update_sql .= " where id = ?";
    $params[] = $_POST["userId"];

    $update_stmt = $db->prepare($update_sql);
    $update_stmt->execute($params);
    
    
    //Code added @ 13 July 2018
    $sqlUpdatePassHistory = $db->prepare('INSERT into adminPasswordHistory (userId, password, modifiedOn) VALUES (?,?,?)');
    $currDate = date("Y-m-d h:i:s");
    $sqlUpdatePassHistory->execute(
            array(
                    $_POST["userId"],
                    $password,
                    $currDate
            )
    );
    //End code
    
    
    
    //clear out all featuresPermissions
    $delete_stmt = $db->prepare("DELETE from featuresPermissions where userId = ?");
    $delete_stmt->execute(array($_POST["userId"]));

    //clear out all permissions
    $delete_stmt = $db->prepare("DELETE from permissions where userId = ?");
    $delete_stmt->execute(array($_POST["userId"]));
    
    $exp = "EXPLAIN permissions";
    $qpo = $db->query($exp);
    $permissions_fields = array();
    while ($r = $qpo->fetch(MYSQL_ASSOC))
    {
        $permissions_fields[] = $r["Field"];
    }


	if ($securityDomainPattern != "") {
	    // Set permissions based on security domain

//         $uFields = array('userId', 'securityDomain');
        //$params_tag = array('?', '?');
        // $params_tag = array();

        // add security domain
	    $uValues = array($_POST["userId"], $_POST["securityDomain"]);
        $insert_sql = "INSERT INTO permissions (userId, securityDomain)" . "VALUES " . "(?, ?)";
        $insert_stmt = $db->prepare($insert_sql);
        $insert_stmt->execute($uValues);
        
        if (isset($_POST["permsUpdate"]))
        {
            $insertValue = array();
            foreach ($_POST["permsUpdate"] as $k => $v)
            {
                   /* 
                     if(in_array($k, $permissions_fields)) {
                        $uFields[] = $k;
                        $uValues[] = $v;
                        $params_tag[] = '?';
                    }
                    */
                    if($v == "1") {
                        $insertValue[] = array($_POST['userId'], $adminTypeIndex, $k);
                    }
                    
            }
        }
        foreach($insertValue as $key => $value) {
            $params_tagFP = "(?, ?, ?)";
            $insert_sql = "INSERT into featuresPermissions (userId, adminTypeIndex, permissionId)" . "VALUES " . $params_tagFP;
            $insert_stmt = $db->prepare($insert_sql);
            $insert_stmt->execute($value);
        }
       
        #$insert = "INSERT into permissions (id, securityDomain" . $uFields . ")";
        #$values = " VALUES ('" . $_POST["userId"] . "','" . $_POST["securityDomain"] . "'" . $uValues . ")";
        #$qry = $insert . $values;
        #$yip = $db->query($qry);

    }
    else {
        // Set permissions based on groups

        $groups = explode(";", $_POST["groups_1"]);
        unset($groups[count($groups) - 1]);
        
        $groups = array_unique($groups); //Code added @ 21 Sep 2018
        
        foreach ($groups as $gs)
        {
            $uFields = array('userId', 'groupId', 'sp');
            $uValues = array($_POST["userId"], $gs, $_POST["sp"]);
            $params_tag = array('?', '?', '?');

            $insert_sql = "INSERT into permissions (" . implode(',', $uFields) . ")" . "VALUES (" . implode(',', $params_tag) . ")";

            error_log($insert_sql);
            $insert_stmt = $db->prepare($insert_sql);
            $insert_stmt->execute(array_merge($uValues));
            //$insert_stmt->debugDumpParams();

            //$insert = "INSERT into permissions (id, groupId, sp, " . $uFields . ")";
            //$values = " VALUES ('" . $_POST["userId"] . "', '" . $gs . "', '" . $_POST["sp"] . "', " . $uValues . ")";
            //$qry = $insert . $values;
            //$yip = $db->query($qry);
        }
        
        if (isset($_POST["permsUpdate"]))
        {
            $insertValue = array();
            foreach ($_POST["permsUpdate"] as $k => $v)
            {
                if($v == "1") {
                    $insertValue[] = array($_POST['userId'], $adminTypeIndex, $k);
                }
            }
        }
        
        // Add to featuresPermissions table
        foreach($insertValue as $key => $value) {
            $params_tagFP = "(?, ?, ?)";
            $insert_sql = "INSERT into featuresPermissions (userId, adminTypeIndex, permissionId)" . "VALUES " . $params_tagFP;
            $insert_stmt = $db->prepare($insert_sql);
            $insert_stmt->execute($value);
        }
    }

//Add Admin Log Entry - Start
if (isset($_POST["userId"])) {
	$newUserData = get_user_info($_POST["userId"], true);
	require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
	$adminLogs = new adminLogs();
	$details = array('Modified By' => $_SESSION["loggedInUser"]);
	foreach ($newUserData as $key => $newValue) {
	    $oldValue = $oldUserData[$key];
        switch ($key) {

            case "userName" :
                if($newValue != $oldUserData[$key]) {
	                $details['User Name'] = array($newValue, $oldValue);
                }
                break;
	        case "firstName" :
		        if($newValue != $oldUserData[$key]) {
			        $details['First Name'] = array($newValue, $oldValue);
		        }
		        break;
	        case "lastName" :
		        if($newValue != $oldUserData[$key]) {
			        $details['Last Name'] = array($newValue, $oldValue);
		        }
		        break;
	        case "superUser" :
		        if($newValue != $oldUserData[$key]) {
			        $details['Super User'] = array(
				        (isset($newValue) and $newValue == "1") ? "Yes" : "No",
				        (isset($oldValue) and $oldValue == "1") ? "Yes" : "No"
                    );
		        }
		        break;
	        case "disabled" :
		        if($newValue != $oldUserData[$key]) {
			        $details['Disabled'] = array(
				        (isset($newValue) and $newValue == "1") ? "Yes" : "No",
				        (isset($oldValue) and $oldValue == "1") ? "Yes" : "No"
			        );
		        }
		        break;
	        case "adminType" :
		        if($newValue != $oldUserData[$key]) {
			        $details['Admin Type'] = array($newValue, $oldValue);
		        }
		        break;
	        case "bsAuthentication" :
		        if($newValue != $oldUserData[$key]) {
			        $details['BS Authentication'] = array(
				        (isset($newValue) and $newValue == "1") ? "Yes" : "No",
				        (isset($oldValue) and $oldValue == "1") ? "Yes" : "No"
			        );
		        }
		        break;
	        case "bsSp" :
		        if($newValue != $oldUserData[$key]) {
			        $details['BS Sp'] = array($newValue, $oldValue);
		        }
		        break;
	        case "bsGroupId" :
		        if($newValue != $oldUserData[$key]) {
			        $details['BS Group ID'] = array($newValue, $oldValue);
		        }
		        break;
	        case "user_cell_phone_number" :
		        if($newValue != $oldUserData[$key]) {
			        $details['Cell Phone Number'] = array($newValue, $oldValue);
		        }
		        break;
	        case "use_sms_every_login" :
		        if($newValue != $oldUserData[$key]) {
			        $details['Use SMS On Login'] = array(
				        (isset($newValue) and $newValue == "1") ? "Yes" : "No",
				        (isset($oldValue) and $oldValue == "1") ? "Yes" : "No"
			        );
		        }
		        break;
	        case "use_sms_on_password_reset" :
		        if($newValue != $oldUserData[$key]) {
			        $details['Use SMS On Password Reset'] = array(
				        (isset($newValue) and $newValue == "1") ? "Yes" : "No",
				        (isset($oldValue) and $oldValue == "1") ? "Yes" : "No"
			        );
		        }
		        break;
	        case "use_ldap_authentication" :
		        if($newValue != $oldUserData[$key]) {
			        $details['Use LDAP Authentication'] = array(
				        (isset($newValue) and $newValue == "1") ? "Yes" : "No",
				        (isset($oldValue) and $oldValue == "1") ? "Yes" : "No"
			        );
		        }
		        break;
            default:
                break;
        }
    }
	$log = array(
		'adminUserID' => $newUserData["id"],
		'eventType' => 'MODIFY_ADMIN',
		'adminUserName' => $newUserData["userName"],
		'adminName' => $newUserData["firstName"] . ' ' . $newUserData["lastName"],
		'updatedBy' => $_SESSION["adminId"],
		'details' => $details
	);
	$adminLogs->addLogEntry($log);
}
//Add Admin Log Entry - End

	echo "Administrator has been updated.";
?>
