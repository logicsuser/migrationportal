<!-- User Filters -->
<link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v11">
<!--<script src="/Express/js/bootstrap.min.js"></script>-->
<script src="/Express/js/bootstrap/bootstrap.min.js"></script>
<script src="/Express/js/jquery-1.17.0.validate.js"></script>
<script src="/Express/js/jquery.loadTemplate.min.js"></script>
<script src="userMod/multiDragAndDrop.js"></script>
<script src="userMod/multiDragAndDropList.js"></script>
<!-- admin permission tooltip open  js -->
 <script src="/Express/js/popper.js"></script>
  <!-- end admin -->
	
<style>
.borderUp {
    border-bottom: 1px solid #ccc;
    height: 0 !important;
    margin: 25px 0 !important;
}
.centerDescBtn{text-align: center;
padding-bottom: 30px !important;}
.hideShow{display:none;}
.panel-forms .tooltiptext {
    left: 0px !important;
    margin-left: -255px !important;
} 
 
  
#usersTitle #usertool1 span.tooltiptextUsers.tooltiptext,#groupTitle #usertool2 span.tooltiptextGroups.tooltiptext {
    /*top: calc(50% - 63.5px) !important;*/
    left: calc(-51% - -8.5px) !important;
    margin-top: -40px;
    
 }
 
  #devicesTitle #usertool4 span.tooltiptextDevices.tooltiptext,#supervisoryTitle #usertool5 span.tooltiptextSupervisory.tooltiptext,#servicesTitle #usertool3 span.tooltiptextServices.tooltiptext{
     bottom: 0% !important;
    top: inherit !important;
 
 }
  
 
 #sortable1, #sortable2, #sortable_1, #sortable_2 {
    padding-top: 25px;
}
  #enterprise_available_user li.selected, #enterprise_assign_user li.selected, #availableUserServiceToAssign li.selected, #assignedUserService li.selected,#sortable_3 li.selected, #sortable_4 li.selected,#sortable1 li.selected, #sortable2 li.selected{
    background:#ffb200;
}
#enterprise_available_user li:hover, #enterprise_assign_user li:hover , #availableUserServiceToAssign li:hover, #assignedUserService li:hover,#sortable_3 li:hover, #sortable_4 li:hover,#sortable1 li:hover, #sortable2 li:hover{
    background:#ffb200;
    color:white;
}

 #enterprise_available_user , #enterprise_assign_user,#availableUserServiceToAssign, #assignedUserService,#sortable1, #sortable2{background: #fff;}

    

#enterprise_available_user, #enterprise_assign_user, #availableUserServiceToAssign li, #assignedUserService li,#sortable_3 li, #sortable_4 li,#sortable1 li, #sortable2 li{
	width: 100%;
	cursor:pointer;
	font-size:12px;
	font-weight:bold;
	text-align:center;
	padding:4px 4px 4px 3px;
	margin:0 0 7px -1px;
}
 
#usersTitle #usertool1 span.tooltiptextDevices.tooltiptext, #usertool4 span.tooltiptextSupervisory.tooltiptext { 
    bottom: 0% !important;
    top: inherit !important;
}
 input#delBut:hover{background:#ac5f5d !important;}
</style>
<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/domains/domains.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getServiceProviders.php");

	function hasSecurityDomain() {
	    global $securityDomainPattern;

        if ($securityDomainPattern == "") {
            return false;
        }
        return $_SESSION["superUser"] != "0" && $_SESSION["adminUsers"]["superUserUpdate"] == "0";
    }

	function getSecurityDomains() {
	    global $systemDomains, $permissions, $securityDomainPattern;
        
	    $securityDomain = $permissions["securityDomain"];
	    $str = "<option value=\"\"";
	    $str .= $securityDomain == "" ? " selected" : "";
	    $str .= ">None</option>";

	    foreach ($systemDomains as $domain) {
            //if (strpos($domain, $securityDomainPattern) !== false ) {
                $str .= "<option value=\"" . $domain . "\"";
                $str .= $domain == $securityDomain ? " selected" : "";
                $str .= ">" . $domain . "</option>";
            //}
        }
        return $str;
    }
    
    function getServiceProvidersList($selectedSp)
    {
    	global $sps;
    	
    	if (isset($_SESSION["spList"]) && count($_SESSION["spList"] > 1)) {
    		$serviceProviders = $_SESSION["spList"];
    	}else if(isset($_SESSION["spListNameId"]) && count($_SESSION["spListNameId"] > 1)){
               $serviceProviders = $_SESSION["spListNameId"];
        }else {
    		$serviceProviders = $sps;
    	}
    	
    	//$str = "<option value=\"\"></option>";
    	foreach ($serviceProviders as $v) {
    		$str .= "<option value=\"" . $v . "\"";
    		if($selectedSp == $v){
    			$str .= " selected";
    		}
    		$str .= ">" . $v . "</option>";
    	}
    	return $str;
    }
    

    function createPermissionsRadioButtons() {
	    global $db;
        
	    $query = "select adminTypeIndex from users where userName='" . $_SESSION["adminUsers"]["userNameUpdate"] . "'";
	    $result = $db->query($query);
	    while ($row = $result->fetch()) {
	        $adminType = $row["adminTypeIndex"];
	    }
	    
	    $permissionTypes = array();
	    $query = "select adminTypeIndex, name from adminTypesLookup";
        $result = $db->query($query);
        $i = 0;
        while ($row = $result->fetch()) {
            $permissionTypes[$i]['adminTypeIndex'] = $row["adminTypeIndex"];
            $permissionTypes[$i]['name'] = $row["name"];
            $i++;
        }

        //<div class="diaPN diaP3" style="width: 2%"><input type="radio" name="checkPermissions" id="checkPermissionsDistrictIT" value="districtIT"
        //onclick="updatePermissions(this)"></div>
        //<div class="diaPN diaP3"><label style="margin-left: 4px; margin-right: 16px; margin-top: 4px">District IT</label></div>

        $html = "";
        $options = "";
        if(count($permissionTypes) > 0) {
            for ($i = 0; $i < count($permissionTypes); $i++) {
                $adminTypeIndex = $permissionTypes[$i]['adminTypeIndex'];
                $type = $permissionTypes[$i]['name'];
                
                $selected = $adminType == $adminTypeIndex ? "selected" : "";
                $options .= "<option value='$adminTypeIndex' $selected>$type</option>";
            }

        } else {
            $options .= "<option value='Admin'>Admin</option>";

        }
        
        return $options;
    }

    require_once("/var/www/lib/broadsoft/adminPortal/getSystemDomains.php");

	$systemConfig = new DBSimpleReader($db, "systemConfig");
	$ldapAuthentication = $systemConfig->get("ldapAuthentication");

// 	$fields = "";
// 	$exp = "EXPLAIN permissions";
// 	$qpo = $db->query($exp);
// 	while ($r = $qpo->fetch(MYSQL_ASSOC))
// 	{
// 		$fields .= $r["Field"] . ",";
// 	}
// 	$fields = substr($fields, 3, -1);
	
	$sel = "SELECT p.*,fp.permissionId, u.id, u.userName, u.firstName, u.lastName, u.emailAddress, u.superUser, u.disabled, u.adminType, u.bsAuthentication,u.clusterName, u.user_cell_phone_number, u.use_sms_every_login, u.use_sms_on_password_reset, u.use_ldap_authentication";
	$sel .= " from users u left join permissions p on u.id=p.userId  LEFT JOIN featuresPermissions fp on fp.userId=u.id where u.id='" . $_POST["userId"] . "'";
	
	$qwr = $db->query($sel);
	$a = 0;
	while ($r = $qwr->fetch(MYSQL_ASSOC))
	{
		$_SESSION["adminUsers"]["sp"] = $r["sp"];
		$_SESSION["adminUsers"]["userNameUpdate"] = $r["userName"];
		$_SESSION["adminUsers"]["passwordUpdate"] = "";
		$_SESSION["adminUsers"]["nameUpdate"] = $r["firstName"] . " " . $r["lastName"];
		$_SESSION["adminUsers"]["emailAddressUpdate"] = $r["emailAddress"];
		$_SESSION["adminUsers"]["user_cell_phone_numberUpdate"] = $r["user_cell_phone_number"];
		$_SESSION["adminUsers"]["use_sms_every_loginUpdate"] = $r["use_sms_every_login"];
		$_SESSION["adminUsers"]["use_sms_on_password_resetUpdate"] = $r["use_sms_on_password_reset"];
		$_SESSION["adminUsers"]["superUserUpdate"] = $r["superUser"];
		$_SESSION["adminUsers"]["disabled"] = $r["disabled"];
		$_SESSION["adminUsers"]["bsAuthentication"] = $r["bsAuthentication"];
		$_SESSION["adminUsers"]["securityDomain"] = $r["securityDomain"];
		$_SESSION["adminUsers"]["use_ldap_authentication"] = $r["use_ldap_authentication"];
		$_SESSION["adminUsers"]["checkPermissions"] = $r["adminType"];
                $_SESSION["adminUsers"]["clusterNameUpdate"] = $r["clusterName"];
		//$permissions["groups"][$a] = $r["groupId"];
		if(!empty($r["groupId"])){
			if($r["groupId"] == "NULL"){
				$permissions["groups"][$a] = "";
			}else{
				$permissions["groups"][$a] = $r["groupId"];
			}
		}else{
			$permissions["groups"][$a] = "";
		}
		$a++;
		if(isset($r["securityDomain"])) {
		    $permissions['securityDomain'] = $r["securityDomain"];
		}
		$permissions[$r['permissionId']] = 1;
// 		foreach ($r['permissionId'] as $key => $value)
// 		{
// 		    $permissions[$value] = 1;
// 		}
	}
	
	function checkPermissionExist($db, $userId){
		
		$num_rows = 0;
		//check the user row exist in permissions table
		$checkPermissionsQuery = "SELECT * from permissions p where p.userId='" . $userId. "'";
		$checkPermissionsQueryResult = $db->query($checkPermissionsQuery);
		$rows = $checkPermissionsQueryResult->fetchAll();
		if(count($rows) > 0){
			$num_rows = count($rows);
		}
		return $num_rows;
	}
	
	/*
        function getPermissionsFromPermissionsCategories($db) {
	  
	    $fetchPermissions = "SELECT * from permissionsCategories";
	    $fetchPermissionsQueryResult = $db->query($fetchPermissions);
	     
	    $permissionsData = array();
	    while ($fetchPermissionsQueryRow = $fetchPermissionsQueryResult->fetch(MYSQL_ASSOC)) {
	        if($fetchPermissionsQueryRow['category'] == 'Users') {
	            $permissionsData['Users'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'], 
	                                               'description' => $fetchPermissionsQueryRow['description']);	            
	        } else if($fetchPermissionsQueryRow['category'] == 'Supervisory') {
	            $permissionsData['Supervisory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
	                                                       'description' => $fetchPermissionsQueryRow['description']);
	        } else if($fetchPermissionsQueryRow['category'] == 'Services') {
	            $permissionsData['Services'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
	                                                   'description' => $fetchPermissionsQueryRow['description']);
	        } else if($fetchPermissionsQueryRow['category'] == 'Groups') {
	            $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
	                                                 'description' => $fetchPermissionsQueryRow['description']);
	        } else if($fetchPermissionsQueryRow['category'] == 'Devices') {
	            $permissionsData['Devices'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
	                                                  'description' => $fetchPermissionsQueryRow['description']);
	        }
	    }
	    
	    foreach ($permissionsData as $key => $row) {
	        $order = array();
	        foreach ($row as $key1 => $row1) {
	            $order[$key1] = $row1['description'];
	        }
	        
	        array_multisort($order, SORT_ASC, $row);
	        $permissionsData[$key] = $row;
	    }
	    
	    return $permissionsData;
	}
	*/
        
        function getPermissionsFromPermissionsCategories($db) {
    
            $fetchPermissions = "SELECT * from permissionsCategories";
            $fetchPermissionsQueryResult = $db->query($fetchPermissions);

            $permissionsData = array();
            $permissionsList = array();
            $_SESSION['permissionsList'] = array();

            while ($fetchPermissionsQueryRow = $fetchPermissionsQueryResult->fetch(MYSQL_ASSOC)) {
                if($fetchPermissionsQueryRow['category'] == 'Users')
                {
                    $tempUserPermissionsArr    = explode("_", $_SESSION["permissions"]["groupAdminUserPermission"]);
                    $groupAdminUserPermission  = $tempUserPermissionsArr[0];
                    $groupAdminUserMod         = $tempUserPermissionsArr[1];
                    $groupAdminUserAddDel      = $tempUserPermissionsArr[2];

                    if($groupAdminUserPermission == "1" && $groupAdminUserMod == "yes" && in_array($fetchPermissionsQueryRow['permissionId'], $_SESSION["GroupAdminPermissionDependencies"]["userModPrmsonArr"])){
                        $permissionsData['Users'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                        'description' => $fetchPermissionsQueryRow['description']);
                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    }
                    if($groupAdminUserPermission == "1" && $groupAdminUserAddDel == "yes" && in_array($fetchPermissionsQueryRow['permissionId'], $_SESSION["GroupAdminPermissionDependencies"]["userAddDelPrmsonArr"])){
                        $permissionsData['Users'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                        'description' => $fetchPermissionsQueryRow['description']);
                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];

                    }
                    if($groupAdminUserPermission == "1" && in_array($fetchPermissionsQueryRow['permissionId'], $_SESSION["GroupAdminPermissionDependencies"]["userViewPrmsonArr"])){
                        $permissionsData['Users'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                        'description' => $fetchPermissionsQueryRow['description']);
                        $permissionsList[]            = $fetchPermissionsQueryRow['permissionId'];
                    }
                }
                else if($fetchPermissionsQueryRow['category'] == 'Supervisory') 
                {

                    $tempSupervisoryPermissionsArr     = explode("_", $_SESSION["permissions"]["groupAdminSupervisoryPermission"]);
                    $groupAdminSupervisoryPrmson       = $tempSupervisoryPermissionsArr[0];

                    if($groupAdminSupervisoryPrmson == "1"){

                        $announcementPermnsArr          = explode("_", $_SESSION["permissions"]["entAnnouncements"]);
                        $announcementPermission         = $announcementPermnsArr[0];
                        $announcementModPermission      = $announcementPermnsArr[1];
                        $announcementAddDelPermission   = $announcementPermnsArr[2];

                        $changeLogPermnsArr             = explode("_", $_SESSION["permissions"]["changeLog"]);
                        $changeLogPermission            = $changeLogPermnsArr[0];

                        $cdrPermnsArr                   = explode("_", $_SESSION["permissions"]["cdr"]);
                        $cdrPermission                  = $cdrPermnsArr[0];
                        $supervisoryPrmnsArr            = array("viewChangeLog", "viewcdrs", "announcements"); 

                        if($fetchPermissionsQueryRow['permissionId'] == "viewChangeLog" && $changeLogPermission == "1"){
                            $permissionsData['Supervisory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                                  'description'  => $fetchPermissionsQueryRow['description']);
                            $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];

                        }else if($fetchPermissionsQueryRow['permissionId'] == "viewcdrs" && $cdrPermission == "1"){
                            $permissionsData['Supervisory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                                  'description'  => $fetchPermissionsQueryRow['description']);
                            $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];

                        }else if($fetchPermissionsQueryRow['permissionId'] == "announcements" && $announcementPermission == "1" && $announcementAddDelPermission == "yes"){
                            $permissionsData['Supervisory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                                  'description'  => $fetchPermissionsQueryRow['description']);
                            $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];

                        }else {
                            if(!in_array($fetchPermissionsQueryRow['permissionId'], $supervisoryPrmnsArr)){
                            $permissionsData['Supervisory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                                  'description'  => $fetchPermissionsQueryRow['description']);
                            $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];

                            }
                        }
                    }



                } 
                else if($fetchPermissionsQueryRow['category'] == 'Services') 
                {
                    $tempServicesPermissionsArr  = explode("_", $_SESSION["permissions"]["groupAdminServicesPermission"]);
                    $groupAdminServicesPrmson    = $tempServicesPermissionsArr[0];

                    if($groupAdminServicesPrmson == "1"){
                        $permissionsData['Services'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                               'description' => $fetchPermissionsQueryRow['description']);
                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    }
                }         
                else if($fetchPermissionsQueryRow['category'] == 'Devices') 
                {            
                    $tempDevicePermissionsArr  = explode("_", $_SESSION["permissions"]["groupAdminDevicesPermission"]);
                    $groupAdminDevicesPrmson   = $tempDevicePermissionsArr[0];
                    $groupAdminDevicesMod      = $tempDevicePermissionsArr[1];
                    $groupAdminDevicesAddDel   = $tempDevicePermissionsArr[2];

                    if($groupAdminDevicesPrmson == "1" && $groupAdminDevicesMod == "yes" && in_array($fetchPermissionsQueryRow['permissionId'], $_SESSION["GroupAdminPermissionDependencies"]["DevicesModPrmsonArr"])){
                        $permissionsData['Devices'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                        'description' => $fetchPermissionsQueryRow['description']);
                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    }
                    if($groupAdminDevicesPrmson == "1" && $groupAdminDevicesAddDel == "yes" && in_array($fetchPermissionsQueryRow['permissionId'], $_SESSION["GroupAdminPermissionDependencies"]["DevicesAddDelPrmsonArr"])){
                        $permissionsData['Devices'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                        'description' => $fetchPermissionsQueryRow['description']);
                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    }
                    if($groupAdminDevicesPrmson == "1" && in_array($fetchPermissionsQueryRow['permissionId'], $_SESSION["GroupAdminPermissionDependencies"]["DevicesViewPrmsonArr"])){
                        $permissionsData['Devices'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                        'description' => $fetchPermissionsQueryRow['description']);
                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    }            
                }

                else if($fetchPermissionsQueryRow['category'] == 'Groups')
                {
                    $basicInformationLogsArr              = explode("_", $_SESSION["permissions"]["basicInformation"]);
                    $basicInformationLogPermission        = $basicInformationLogsArr[0];
                    $basicInformationModPermission        = $basicInformationLogsArr[1];

                    $domainsLogsArr                       = explode("_", $_SESSION["permissions"]["domains"]);
                    $domainsLogPermission                 = $domainsLogsArr[0]; 
                    $domainsModPermission                 = $domainsLogsArr[1]; 

                    $servicesLogsArr                      = explode("_", $_SESSION["permissions"]["services"]);
                    $servicesLogPermission                = $servicesLogsArr[0];
                    $servicesModPermission                = $servicesLogsArr[1];

                    $dnsLogsArr                           = explode("_", $_SESSION["permissions"]["DNs"]);
                    $dnsLogPermission                     = $dnsLogsArr[0];
                    $dnsModPermission                     = $dnsLogsArr[1];

                    $callProcessingPoliciesLogsArr        = explode("_", $_SESSION["permissions"]["callProcessingPolicies"]);
                    $callProcessingPoliciesLogPermission  = $callProcessingPoliciesLogsArr[0];
                    $callProcessingPoliciesModPermission  = $callProcessingPoliciesLogsArr[1];

                    $networkClassesofServiceLogsArr       = explode("_", $_SESSION["permissions"]["networkClassesofService"]);
                    $networkClassesofServiceLogPermission = $networkClassesofServiceLogsArr[0];
                    $networkClassesofServiceModPermission = $networkClassesofServiceLogsArr[1];

                    $voicePortalServiceLogsArr            = explode("_", $_SESSION["permissions"]["voicePortalService"]);
                    $voicePortalServiceLogPermission      = $voicePortalServiceLogsArr[0];
                    $voicePortalServiceModPermission      = $voicePortalServiceLogsArr[1];

                    $outgoingCallingPlanLogsArr           = explode("_", $_SESSION["permissions"]["outgoingCallingPlan"]);
                    $outgoingCallingPlanLogPermission     = $outgoingCallingPlanLogsArr[0];
                    $outgoingCallingPlanModPermission     = $outgoingCallingPlanLogsArr[1];

                    $securityDomainsLogsArr               = explode("_", $_SESSION["permissions"]["securityDomains"]);
                    $securityDomainsLogPermission         = $securityDomainsLogsArr[0];
                    $securityDomainsModPermission         = $securityDomainsLogsArr[1];


                    if($fetchPermissionsQueryRow['permissionId'] == "modifyGroup" && ($basicInformationLogPermission == "1" || $outgoingCallingPlanLogPermission == "1" 
                        || $domainsLogPermission == "1" || $securityDomainsLogPermission == "1" 
                        || $servicesLogPermission == "1"  || $dnsLogPermission == "1" || $callProcessingPoliciesLogPermission == "1" 
                        || $networkClassesofServiceLogPermission == "1" || $voicePortalServiceLogPermission == "1"))
                    { 
                        $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                        'description' => $fetchPermissionsQueryRow['description']);

                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    }

                    if($fetchPermissionsQueryRow['permissionId'] == "groupBasicInfo" && $basicInformationLogPermission == "1" && $basicInformationModPermission == "yes")
                    { 
                        $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                        'description' => $fetchPermissionsQueryRow['description']);

                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    }

                    if($fetchPermissionsQueryRow['permissionId'] == "groupDomains" && $domainsLogPermission == "1" && $domainsModPermission == "yes")
                    { 
                        $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                        'description' => $fetchPermissionsQueryRow['description']);

                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    }

                    if($fetchPermissionsQueryRow['permissionId'] == "groupServices" && $servicesLogPermission == "1" && $servicesModPermission == "yes")
                    { 
                        $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                        'description' => $fetchPermissionsQueryRow['description']);

                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    }

                    if($fetchPermissionsQueryRow['permissionId'] == "groupDN" && $dnsLogPermission == "1" && $dnsModPermission == "yes")
                    { 
                        $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                        'description' => $fetchPermissionsQueryRow['description']);

                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    }

                    if($fetchPermissionsQueryRow['permissionId'] == "groupPolicies" && $callProcessingPoliciesLogPermission == "1" && $callProcessingPoliciesModPermission == "yes")
                    { 
                        $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                        'description' => $fetchPermissionsQueryRow['description']);

                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    }

                    if($fetchPermissionsQueryRow['permissionId'] == "groupNCOS" && $networkClassesofServiceLogPermission == "1" && $networkClassesofServiceModPermission == "yes")
                    { 
                        $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                        'description' => $fetchPermissionsQueryRow['description']);

                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    }

                    if($fetchPermissionsQueryRow['permissionId'] == "groupVoicePortal" && $voicePortalServiceLogPermission == "1" && $voicePortalServiceModPermission == "yes")
                    { 
                        $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                        'description' => $fetchPermissionsQueryRow['description']);

                        $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    }
                }

            }

            foreach ($permissionsData as $key => $row) {
                $order = array();
                foreach ($row as $key1 => $row1) {
                    $order[$key1] = $row1['description'];
                }

                array_multisort($order, SORT_ASC, $row);
                $permissionsData[$key] = $row;
            }

            $_SESSION['permissionsList'] = $permissionsList;
            return $permissionsData;
        }

        
	function getDefaultPermissionAdmiNType($db, $adminType){
		$permissionsNew = array();
		

// 		$fetchPermissionDefault = "SELECT `basicUser`, `resetPasswords`, `deleteUsers`, `changeDevice`, `changeHotel`, `changeForward`, `changeblf`, `changeSCA`, `modhuntgroup`, `addusers`, `modifyUsers`, `modcallCenter`, `groupWideUserModify`, `aamod`, `announcements`, `modCcd`, `modschedule`, `viewcdrs`, `viewusers`, `viewnums`, `viewChangeLog`, `adminUsers`, `vdmLight`, `modifyGroup`, `callPickupGroup`, `vdmAdvanced`, `scaOnlyDevice`, `siteReports`, `sasTest`, `expressSheets`, `groupBasicInfo`, `groupDomains`, `groupServices`, `groupDN`, `groupPolicies`, `groupNCOS`, `groupVoicePortal`, `voiceManagement`, `clearMACAddress` FROM `permissionsSets` where adminType = '" . $adminType . "'";
        
		$fetchPermissionDefault = "select dap.* from defaultAdminPermissions dap INNER JOIN adminTypesLookup atl on atl.`adminTypeIndex` = dap.`adminTypeIndex` where atl.name='". $adminType."'";
		$fetchPermissionDefaultQueryResult = $db->query($fetchPermissionDefault);
		
		while ($fetchPermissionDefaultQueryRow = $fetchPermissionDefaultQueryResult->fetch()){
		    $permissionsNew[$fetchPermissionDefaultQueryRow['permissionId']] = 1;
		    
// 			$permissionsNew['basicUser'] = $fetchPermissionDefaultQueryRow['basicUser'];
// 			$permissionsNew['resetPasswords'] = $fetchPermissionDefaultQueryRow['resetPasswords'];
// 			$permissionsNew['deleteUsers'] = $fetchPermissionDefaultQueryRow['deleteUsers'];
// 			$permissionsNew['changeDevice'] = $fetchPermissionDefaultQueryRow['changeDevice'];
// 			$permissionsNew['clearMACAddress'] = $fetchPermissionDefaultQueryRow['clearMACAddress'];
// 			$permissionsNew['voiceManagement'] = $fetchPermissionDefaultQueryRow['voiceManagement'];
// 			$permissionsNew['changeHotel'] = $fetchPermissionDefaultQueryRow['changeHotel'];
// 			$permissionsNew['changeForward'] = $fetchPermissionDefaultQueryRow['changeForward'];
// 			$permissionsNew['changeblf'] = $fetchPermissionDefaultQueryRow['changeblf'];
// 			$permissionsNew['changeSCA'] = $fetchPermissionDefaultQueryRow['changeSCA'];
// 			$permissionsNew['modhuntgroup'] = $fetchPermissionDefaultQueryRow['modhuntgroup'];
// 			$permissionsNew['addusers'] = $fetchPermissionDefaultQueryRow['addusers'];
// 			$permissionsNew['modifyUsers'] = $fetchPermissionDefaultQueryRow['modifyUsers']; 
// 			$permissionsNew['modcallCenter'] = $fetchPermissionDefaultQueryRow['addumodcallCentersers'];
// 			$permissionsNew['groupWideUserModify'] = $fetchPermissionDefaultQueryRow['groupWideUserModify'];
// 			$permissionsNew['aamod'] = $fetchPermissionDefaultQueryRow['aamod'];
// 			$permissionsNew['announcements'] = $fetchPermissionDefaultQueryRow['announcements'];
// 			$permissionsNew['modCcd'] = $fetchPermissionDefaultQueryRow['modCcd'];
// 			$permissionsNew['modschedule'] = $fetchPermissionDefaultQueryRow['modschedule'];
// 			$permissionsNew['viewcdrs'] = $fetchPermissionDefaultQueryRow['viewcdrs'];
// 			$permissionsNew['viewusers'] = $fetchPermissionDefaultQueryRow['viewusers'];
// 			$permissionsNew['viewnums'] = $fetchPermissionDefaultQueryRow['viewnums'];
// 			$permissionsNew['viewChangeLog'] = $fetchPermissionDefaultQueryRow['viewChangeLog'];
// 			$permissionsNew['adminUsers'] = $fetchPermissionDefaultQueryRow['adminUsers'];
// 			$permissionsNew['vdmLight'] = $fetchPermissionDefaultQueryRow['vdmLight'];
// 			$permissionsNew['modifyGroup'] = $fetchPermissionDefaultQueryRow['modifyGroup'];
// 			$permissionsNew['callPickupGroup'] = $fetchPermissionDefaultQueryRow['callPickupGroup'];
// 			$permissionsNew['vdmAdvanced'] = $fetchPermissionDefaultQueryRow['vdmAdvanced'];
// 			$permissionsNew['scaOnlyDevice'] = $fetchPermissionDefaultQueryRow['scaOnlyDevice'];
// 			$permissionsNew['siteReports'] = $fetchPermissionDefaultQueryRow['siteReports'];
// 			$permissionsNew['sasTest'] = $fetchPermissionDefaultQueryRow['sasTest'];
// 			$permissionsNew['expressSheets'] = $fetchPermissionDefaultQueryRow['expressSheets'];
// 			$permissionsNew['groupBasicInfo'] = $fetchPermissionDefaultQueryRow['groupBasicInfo'];
// 			$permissionsNew['groupDomains'] = $fetchPermissionDefaultQueryRow['groupDomains'];
// 			$permissionsNew['groupServices'] = $fetchPermissionDefaultQueryRow['groupServices'];
// 			$permissionsNew['groupDN'] = $fetchPermissionDefaultQueryRow['groupDN'];
// 			$permissionsNew['groupPolicies'] = $fetchPermissionDefaultQueryRow['groupPolicies'];
// 			$permissionsNew['groupNCOS'] = $fetchPermissionDefaultQueryRow['groupNCOS'];
// 			$permissionsNew['groupVoicePortal'] = $fetchPermissionDefaultQueryRow['groupVoicePortal'];
		}
		return $permissionsNew;
	}
	
	function createPermissionCheckboxes($perVal, $permissions, $i) {
	    $html = "";
	    $openAccordian = 0;
	    if($i % 2 == 0) {
	        //$html .= "<tr>";
	    }
	    
	    if (count($permissions) > 0 && isset($permissions[$perVal["permissionId"]]) && $permissions[$perVal["permissionId"]] == "1")
	    {
	        $_SESSION["adminUsers"]["permsUpdate"][$perVal["permissionId"]] = $permissions[$perVal["permissionId"]];
	        $chk = "checked=\"CHECKED\"";
	        $openAccordian = 1;
	    }
	    else
	    {
	        $_SESSION["adminUsers"]["permsUpdate"][$perVal["permissionId"]] = "0";
	        $chk = "";
	    }
	    
	    $html .= "<div class=\"col-md-6\">";
	    $html .= "<input type=\"hidden\" name=\"permsUpdate[" . $perVal["permissionId"] . "]\" value=\"0\">";
	    $html .= "<input style=\"\" type=\"checkbox\" id=\"permsUpdate[" . $perVal["permissionId"] . "]\" " . $chk . " name=\"permsUpdate[" . $perVal["permissionId"] . "]\" value=\"1\"><label for=\"permsUpdate[" . $perVal["permissionId"] . "]\" " . $chk . "><span></span></label>";
	  
	    $html .= "<label class=\"labelText\" for=\"permsUpdate[" . $perVal["permissionId"] . "]\">" . $perVal["description"] . "</label>";
	    $html .= "</div>";
	    
	    if($i % 2 != 0) {
	        //$html .= "</tr>";
	    }
	    
	    $returnData = array("html" => $html, "openAccordian" => $openAccordian);
	    return $returnData;
	}
	
	
	?>
<script>
	$(function()
	{		
	 
        var bootstrapButton = $.fn.button.noConflict()
		$.fn.bootstrapBtn = bootstrapButton;
		
		$("#endUserId").html("<?php echo $_SESSION["adminUsers"]["userNameUpdate"]; ?>");

		//$("#dialogUpdate").dialog($.extend({}, defaults, {
		$("#dialogUpdate").dialog({
		autoOpen: false,
		width: 800,
		open: function(event) {
				setDialogDayNightMode($(this));
				$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('completeButton');
				$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
				$('.ui-dialog-buttonpane').find('button:contains("Add/Modify Another Admin")').addClass('subButton');
			},
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
			buttons: {
				"Complete": function()
				{
					var formData = $("form#userAdmin").serialize();
					$.ajax({
						type: "POST",
						url: "adminUsers/updateAdmin.php",
						data: formData,
						success: function(result)
						{
							$("#dialogUpdate").dialog("open");
							$(".ui-dialog-titlebar-close", this.parentNode).hide();
							$(".ui-dialog-buttonpane", this.parentNode).show();
							$(":button:contains('Complete')").hide();
							$(":button:contains('Cancel')").hide();
							$(":button:contains('Add/Modify Another Admin')").show();
							$("#dialogUpdate").html(result);
							$("#dialogUpdate").append(returnLink);
						}
					});
				},
				"Cancel": function()
				{
					$(this).dialog("close");
				},
				"Add/Modify Another Admin": function()
				{
					$(this).dialog("close");
					$("html, body").animate({ scrollTop: 0 }, 600);
					$("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
					$("#loading").show();
					$(".navMenu").removeClass("active");
					$(this).addClass("active");
					$.ajax({
						type: "POST",
						url: "navigate.php",
						data: { module: 'adminUser' },
						success: function(result)
						{
							$("#loading").hide();
							$("#mainBody").html(result);
						}
					});
				}
				
			}
		});
                
                
                var insensitive = function(s1, s2) {
					 var s1lower = s1.toLowerCase();
					 var s2lower = s2.toLowerCase();
					 return s1lower > s2lower? 1 : (s1lower < s2lower? -1 : 0);
				}

				//make new html for the ul tag for ordering the li data
				var getArrayFromLi = function(ulId){
                                   
					var liArray = [];
					var liNewArrayUsers = [];
					var liStr = "";
					
					$(ulId+" li").each(function(){
						var el = $(this);
						liArray.push(el.text());
					});
					
					var liNewArray = liArray.sort(insensitive);
					$(ulId).html('');
					for (i = 0; i < liNewArray.length; i++)
					{
						liStr += '<li class="ui-state-default" id="'+liNewArray[i]+'">'+liNewArray[i]+'</li>';
					}
					$(ulId).html(liStr);
                                        /*alert('liStr - '+liStr);
                                        alert('liNewArray - '+liNewArray);
                                        alert('liNewArrayUsers - '+liNewArrayUsers);
					console.log(liNewArrayUsers);*/
				}
                

		$("#subBut").click(function()
		{


			if (($('#use_sms_on_password_resetUpdate').is(':checked') || $('#use_sms_every_loginUpdate').is(':checked')) && $("#user_cell_phone_numberUpdate_field").val() === "") {
				alert("Cell Phone Number is a required field.");
			}else{

			var formData = $("form#userAdmin").serialize();
			$.ajax({
				type: "POST",
				url: "adminUsers/checkData.php",
				data: formData,
				success: function(result)
				{
					$("#dialogUpdate").dialog("open");
					if (result.slice(0, 1) == "1")
					{
						$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
					}
					else
					{
						$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
					}
					result = result.slice(1);
					$(":button:contains('Add/Modify Another Admin')").hide();
					$(":button:contains('Complete')").show();
					$(":button:contains('Cancel')").show();
					$("#dialogUpdate").html(result);
				}

			});
		}
		});
		$("#use_sms_every_loginUpdate").click(function () {
            if ($('#use_sms_every_loginUpdate').is(':checked')) {
             $("#user_cell_phone_numberUpdate").text("*");
             $("#user_cell_phone_numberUpdate").attr('class', 'required');
               
            } else if (!$('#use_sms_every_loginUpdate').is(':checked')) {
             $("#user_cell_phone_numberUpdate").attr('class', 'not_required');
             $("#user_cell_phone_numberUpdate").text("");
            }

        });
        $("#use_sms_on_password_resetUpdate").click(function () {
            if ($('#use_sms_on_password_resetUpdate').is(':checked')) {
             $("#user_cell_phone_numberUpdate").text("*");
             $("#user_cell_phone_numberUpdate").attr('class', 'required');
               
            } else if (!$('#use_sms_on_password_resetUpdate').is(':checked')) {
             $("#user_cell_phone_numberUpdate").attr('class', 'not_required');
             $("#user_cell_phone_numberUpdate").text("");
            }

        });
		$("#delBut").click(function()
		{
		$("#dialogDelete").dialog($.extend({}, defaults, {
				width: 800,
				open: function(event) {
					setDialogDayNightMode($(this));
					$('.ui-dialog-buttonpane').find('button:contains("Delete")').addClass('deleteButton');
					$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
					$('.ui-dialog-buttonpane').find('button:contains("Add/Modify Another Admin")').addClass('subButton');
				},
				buttons: {
					"Delete": function()
					{
						var formData = $("form#userAdmin").serialize();
						$.ajax({
							type: "POST",
							url: "adminUsers/deleteAdmin.php",
							data: formData,
							success: function(result)
							{
								$("#dialogDelete").dialog("open");
								$(".ui-dialog-titlebar-close", this.parentNode).hide();
								$(".ui-dialog-buttonpane", this.parentNode).show();
								$(":button:contains('Complete')").hide();
								$(":button:contains('Cancel')").hide();
								$(":button:contains('Delete')").hide();
								$(":button:contains('Add/Modify Another Admin')").show();
								$("#dialogDelete").html(result);
								$("#dialogDelete").append(returnLink);
							}
						});
					},
					"Cancel": function()
					{
						$(this).dialog("close");
					},
					"Add/Modify Another Admin": function()
					{
						$(this).dialog("close");
						$("html, body").animate({ scrollTop: 0 }, 600);
						$("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
						$("#loading").show();
						$(".navMenu").removeClass("active");
						$(this).addClass("active");
						$.ajax({
							type: "POST",
							url: "navigate.php",
							data: { module: 'adminUserEnt' },
							success: function(result)
							{
								$("#loading").hide();
								$("#mainBody").html(result);
							}
						});
					}
				}
			}));

			$("#dialogDelete").dialog("open");
			$("#dialogDelete").html("<label class=\"labelTextGrey\">Are you sure you would like to delete administrator \"" + $("#nameUpdate").val() + "</label>"+"\"?");
			$(":button:contains('Add/Modify Another Admin')").hide();
			$(":button:contains('Cancel')").show();
			$(":button:contains('Delete')").show();
		});

		/*$("#sortable1, #sortable2").sortable({
			placeholder: "ui-state-highlight",
			connectWith: ".connectedSortable",
			cursor: "crosshair",
			update: function(event, ui)
			{
				var monUsers = "";
				var order = $("#sortable2").sortable("toArray");
				for (i = 0; i < order.length; i++)
				{
					if(order.length > 0){
						monUsers += order[i] + ";";
					}
				}
				$("#groups_1").val(monUsers);
			}
		}).disableSelection(); */
                $("#sortable1, #sortable2").multisortableList({
			placeholder: "ui-state-highlight",
			connectWith: "#sortable1,#sortable2",
			cursor: "crosshair",
			update: function(event, ui)
			{
				var monUsers = "";
				var order = $("#sortable2").sortable("toArray");
				for (i = 0; i < order.length; i++)
				{
					if(order.length > 0){
						monUsers += order[i] + ";";
					}
				}
				$("#groups_1").val(monUsers);
			},
                         stop : function(){
                            getArrayFromLi('#sortable1');
                            getArrayFromLi('#sortable2');

                        }
		}).disableSelection();

		//select service provider for group permissions (non-superusers only)
		$("#sp").change(function()
		{
			$("#sortable1").empty();
			$("#sortable2").empty();

			var sp = $(this).val();
			$.ajax({
				type: "POST",
				url: "adminUsers/getAllGroups.php",
				data: { getSP: sp },
				success: function(result)
				{
					$("#sortable1").append(result);
					$("#sortable2").empty();
					$("#groups_1").val('');
				}
			});
		});
	});
	
	function defaultPerCheckBox() {
		 $("#defaultPermission").prop("disabled",false);
		 $("#defaultPermission").prop("checked",false);
                 $('#attentionDivMod').html("");
		 $(".defaultPermCheckLabel").css("opacity","1");
	}

	function showHideAccordian(result, permissionRadio) {
		 var selectAll = false;
		 if(permissionRadio != "") {
			 selectAll = permissionRadio == "Select All" ? true : false;
		}
		 
         if($.inArray("Users", result) > -1 || selectAll) {
             
				$("#usersTitle #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#userPerDiv").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         if($.inArray("Groups", result) > -1 || selectAll) {
         	$("#groupTitle #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#groupPerDiv").addClass("in").attr("aria-expanded","true").css("height", "auto");;
         }
         if($.inArray("Devices", result) > -1 || selectAll) {
         	$("#devicesTitle #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#devicesPerDiv").addClass("in").attr("aria-expanded","true").css("height", "auto");;
         }
         if($.inArray("Services", result) > -1 || selectAll) {
         	$("#servicesTitle #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#servicesPerDiv").addClass("in").attr("aria-expanded","true").css("height", "auto");;
         }
         if($.inArray("Supervisory", result) > -1 || selectAll) {
         	$("#supervisoryTitle #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#supervisoryPerDiv").addClass("in").attr("aria-expanded","true").css("height", "auto");;
         }
	}
	
	var permissionRadio = "";
	$("input[name='permissionRadio']").click(function() {
		permissionRadio =  $(this).val();
		showHideAccordian(result = "", permissionRadio);
	});
        
        
        //Checked Select All if all group permission is checked
        $("#adminPermissions :checkbox").change(function() {
        var names = {};
        $('#adminPermissions :checkbox').each(function() {
            names[$(this).attr('name')] = true;
        });
        var count = 0;
        $.each(names, function() { 
            count++;
        });
        if ($('#adminPermissions :checkbox:checked').length === count) {
            $("#permissionRadioSelectAll").prop("checked",true);
        }
        }).change();  
        //End code
	
	$("[id^=permsUpdate]").click(function() {
		$("input[name='permissionRadio']").prop("checked", false);
		defaultPerCheckBox();
	});
	
	
	$("#defaultPermission").click(function() {
                $('#attentionDivMod').html("");
		$("input[name='permissionRadio']").prop("checked", false);
		if($(this).prop('checked') == true){
			var adminType = $('#adminType option:selected').val();
			var value = {'value': adminType}; 
			updatePermissions(value);
		} else {
// 			$("[id^=permsUpdate]").prop("checked", false);
		}
	});

	// accordian
	function getAllPermissionscategories(defaultPermission) {
	 	$("input[name='permissionRadio']").prop("checked", false);
		$(".collapsideDiv").removeClass('in').attr("aria-expanded","false");
		$("[id^=filtersStatusTitle]").addClass("collapsed").attr("aria-expanded","false");
		
		$.ajax({
            type: "POST",
            url: "adminUsers/getPermissions.php",
            data: { action: 'getAllPermissionscategories', defaultPermission: defaultPermission},
            success: function(result)
            {
            	var result = JSON.parse(result);
            	showHideAccordian(result, permissionRadio = "");
            }
        });
	}
	
	
	function updatePermissions(radio) {
		if (radio.value == "Select All") {
            $("[id^=permsUpdate]").prop("checked", true);
            defaultPerCheckBox();
        } else if (radio.value == "Clear") {
            $("[id^=permsUpdate]").prop("checked", false);
            defaultPerCheckBox();
        } else if($("#defaultPermission").prop('checked') == false){
                        $('#attentionDivMod').html("");
			$("input[name='permissionRadio']").prop("checked", false);
			return false;
		}
        else {
            $.ajax({
                type: "POST",
                url: "adminUsers/getPermissions.php",
                data: { permissionSet: radio.value },
                success: function(result)
                {
                	$("[id^=permsUpdate]").prop("checked", false);
                	 getAllPermissionscategories(result);
                     var values = result.split(",");
                     
                     //Code added @ 06 March 2019
                     //console.log(values);
                     if(values.indexOf("Attention") >= 0){
                        $('#attentionDivMod').html("<a title='Due to permissions restrictions of this Administrator, not all Default Permissions will be enabled for the created Group Administrator'><img src='images/NewIcon/attention.png' style='width: 21px; cursor: pointer;' /></a>");
                        values.pop();
                     }else{
                        $('#attentionDivMod').html("");
                     }
                     //End code
                     
                     for (var i = 0; i < values.length; i++) {

                    	 
                    	 var id = "permsUpdate[" + values[i] + "]";
                    	 
                        // var params = values[i].split(":");
                       // var id = "permsUpdate[" + params[0] + "]";
                     document.getElementById(id).checked = true;
                     }
                }
            });
        }
    }

/* search  security domain search level 
 * ex-525 */

 autoCompleteFunction();
	function  autoCompleteFunction() {
		var autoComplete = new Array();
		$.ajax({
			type: "POST",
			url: "adminUsers/getAllSecurityDomains.php",
			data: { action: 'getSysSecurityDomainList'},
			success: function(result)
			{
				var explode = result.split(":");
				for (a = 0; a < explode.length; a++)
				{
					autoComplete[a] = explode[a];
				}
				$("#securityDomain").autocomplete({
					source: autoComplete,
 				 select: function( event , ui ) {
 					 $(this).val($.trim(ui.item.label));
 					 return false;
 		         }
				});
			}
		});
	}

 $(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
	});
 
 /*end*/
</script>
<form name="userAdmin" id="userAdmin" method="POST" action="#">
	
		<input type="hidden" name="userId" id="userId" value="<?php echo $_POST["userId"]; ?>">
		<!-- input type="hidden" name="sp" id="sp" value="<?php //echo $_SESSION["adminUsers"]["sp"]; ?>"-->
		<input type="hidden" name="superUserUpdate" id="superUserUpdate" value="<?php echo $_SESSION["adminUsers"]["superUserUpdate"]; ?>">


<div class="row">&nbsp;</div>
	
<div class="row">
	<div class="">	
		<div class="">
    		<div class="col-md-6">
    			<?php
    			if ($ldapAuthentication == "true") {
    			?>
    			<div class="form-group">
    				<input type="hidden" name="use_ldap_authentication" id="use_ldap_authentication1" value="0" />
    				<input type="checkbox" name="use_ldap_authentication" id="use_ldap_authentication" style="width: auto;" value="1" <?php echo $_SESSION["adminUsers"]["use_ldap_authentication"] ? "checked" : ""; ?>>
    				<label for="use_ldap_authentication"><span></span></label>
    				<label class="labelText" for="use_ldap_authentication">LDAP Authentication</label>
    			</div>
    
    				<?php
    			} else {
    				?>
    				<input type="hidden" name="use_ldap_authentication" id="use_ldap_authentication" value="0" />
    			<?php
    			}
    			?>
    		</div>
        	<div class="col-md-6">	
        		<div class="form-group">
        			<input type="hidden" name="disabled" value="0">
        			<input type="checkbox" name="disabled" id="disabled" value="1" <?php echo $_SESSION["adminUsers"]["disabled"] ? "checked" : ""; ?>><label for="disabled"><span></span></label>
        			<label for="disabled" class="labelText labelTextZero">Disable User</label>
        		</div>
        	</div>
		</div>

	</div>
</div>
<div class="row">
		<div class="">	
    		<div class="col-md-6">
    			<div class="form-group">
    					
    				<label class="labelText" for="userNameUpdate">Username</label> <span class="required">*</span>
    					<?php //if BroadSoft user, prevent modification of username ?>
    					<?php
    					if($_SESSION["adminUsers"]["bsAuthentication"] == "1") 
    					{
    						?>
    						<input type="hidden" name="userNameUpdate" id="userNameUpdate" value="<?php echo $_SESSION["adminUsers"]["userNameUpdate"]; ?>">
    						<?php
    					}
    					?>
					<input type="text" class="form-control" <?php echo ($_SESSION["adminUsers"]["bsAuthentication"] == "1") ? " disabled style='background: #F6F6F6;' " : " name='userNameUpdate' id='userNameUpdate' " ?> value="<?php echo $_SESSION["adminUsers"]["userNameUpdate"]; ?>" size="35" maxlength="32">
    			</div>
    		</div>
			<div class="col-md-6">
    		<div class="form-group">
    		<label class="labelText" for="user_cell_phone_numberUpdate">Cell Phone Number</label><span class="not_required" id="user_cell_phone_numberUpdate"></span>
    		<input class="form-control" type="tel" name="user_cell_phone_numberUpdate" id="user_cell_phone_numberUpdate_field" value="<?php echo $_SESSION["adminUsers"]["user_cell_phone_numberUpdate"]; ?>" size="35" maxlength="65" required>
    		</div>
    	</div>
    		<!--<div class="col-md-6">
    			<div class="form-group">
    				<label class="labelText" for="passwordUpdate">Password</label>
    			<?php //if BroadSoft or LDAP user, prevent modification of password ?>
    			<input class="form-control" <?php //echo ($_SESSION["adminUsers"]["use_ldap_authentication"] || $_SESSION["adminUsers"]["bsAuthentication"] == "1" ) ? "disabled style='background: #F6F6F6;' value='N/A - LDAP Enabled'" : "type='password'" ?> name="passwordUpdate" id="passwordUpdate" size="35">
    			</div>
    		</div>-->

		</div>
</div>
<div class="row">
	<div class="">
		<?php if ($isAdminPasswordFieldVisible == "true") { ?>
				<div class="col-sm-6">
				<div class="form-group">
					<label class="labelText" for="passwordUpdate">Password</label>
						<?php //if BroadSoft or LDAP user, prevent modification of password ?>
						<input class="form-control" <?php echo ($_SESSION["adminUsers"]["use_ldap_authentication"] || $_SESSION["adminUsers"]["bsAuthentication"] == "1" ) ? "disabled style='background: #F6F6F6;' value='N/A - LDAP Enabled'" : "type='password'" ?>
						       name="passwordUpdate"
						       id="passwordUpdate"
						       size="35">					
				</div>
				</div>
				
				<!-- 		verify password -->
        		<div class="col-sm-6">
        			<div class="form-group">
            			<label class="labelText" for="verifyPassword">Verify Password</label>
            				<?php //if BroadSoft or LDAP user, prevent modification of password ?>
            				<input <?php echo ($_SESSION["adminUsers"]["use_ldap_authentication"] || $_SESSION["adminUsers"]["bsAuthentication"] == "1" ) ? "disabled placeholder='N/A - LDAP Enabled' class='form-control disabled'" : "class='form-control'" ?>
            					   type="password"
            					   name="verifyPassword"
            					   id="verifyPassword"
            					   size="35">					
            		</div>
        		</div>
		<?php } ?>
	
</div>
</div>
<div class="row">
	<div class="">
    
    	<div class="col-md-6">
    		<div class="form-group">
    		<label class="labelText" for="nameUpdate">Name</label> <span class="required">*</span>
    		<input class="form-control" type="text" name="nameUpdate" id="nameUpdate" value="<?php echo $_SESSION["adminUsers"]["nameUpdate"]; ?>" size="35" maxlength="65">
    		</div>
    	</div>
        <div class="col-md-6">
    		<div class="form-group">
    			<label class="labelText" for="emailAddressUpdate">Email Address</label> <span class="required">*</span>
    			<input class="form-control" type="text" name="emailAddressUpdate" id="emailAddressUpdate" value="<?php echo $_SESSION["adminUsers"]["emailAddressUpdate"]; ?>" size="40" maxlength="64">
    		</div>
    	</div>
	</div>
</div>
		
<div class="row ">
	<div class="">
	<div class="col-md-6">
    		<div class="form-group">
    		<input type="hidden" name="use_sms_every_loginUpdate" value="0">
    		<div class="marginTopSe">&nbsp;</div>
    		<input type="checkbox" class="form-control" name="use_sms_every_loginUpdate" id="use_sms_every_loginUpdate" value="1" <?php echo $_SESSION["adminUsers"]["use_sms_every_loginUpdate"] ? "checked" : ""; ?>><label for="use_sms_every_loginUpdate"><span></span></label>
    		<label class="labelText" for="use_sms_every_loginUpdate">Use SMS every login</label>
    		</div>
	</div>
	
	<div class="col-md-6">
			<?php
			if ($_SESSION["adminUsers"]["use_ldap_authentication"]) {
				?>
				<input type="hidden" name="use_sms_on_password_resetUpdate" id="use_sms_on_password_resetUpdate" value="<?php echo $_SESSION["adminUsers"]["use_sms_on_password_resetUpdate"] ? "1" : "0"; ?>" />
				<?php
			} else {
				?>
				<div class="form-group">
				<div class="marginTopSe">&nbsp;</div>
				<input type="hidden" name="use_sms_on_password_resetUpdate" value="0">
				<input type="checkbox" class="checkbox-lg" name="use_sms_on_password_resetUpdate" id="use_sms_on_password_resetUpdate" value="1" <?php echo $_SESSION["adminUsers"]["use_sms_on_password_resetUpdate"] ? "checked" : ""; ?>><label for="use_sms_on_password_resetUpdate"><span></span></label>
				<label class="labelText" for="use_sms_on_password_resetUpdate">Require SMS to reset</label>
				</div>
	
				<?php
			}
			?>
                                </div>
	</div>
</div>

<div class="vertSpacer initialShow ">&nbsp;</div>
		 <?php echo $_SESSION["adminUsers"]["superUserUpdate"] == "0" ? '<div class="vertSpacer initialShow borderUp">&nbsp;</div>':"";?>
<div class="vertSpacer initialShow">&nbsp;</div>
		 
<!-- New code found start-->
	<div class="row">
            <div>
            <?php 
            if ($_SESSION["adminUsers"]["superUserUpdate"] == "0")
            {
            ?>
	            <div class="col-md-6 form-group">
		            <label class="labelText">Admin Type</label><span class="required">*</span>
		            <div class="dropdown-wrap">
			            <select id="adminType" class="form-control" name="checkPermissions" onchange="updatePermissions(this)">
			            	 <?php echo createPermissionsRadioButtons(); ?>
			            </select>
		            </div>
	            </div>
	        <?php } ?>
	        
	        <?php 
	        unset($permissions["queryString"]);
	        unset($permissions["groupId"]);
	        unset($permissions["userName"]);
	        unset($permissions["firstName"]);
	        unset($permissions["lastName"]);
	        unset($permissions["emailAddress"]);
	        unset($permissions["user_cell_phone_number"]);
	        unset($permissions["use_sms_every_login"]);
	        unset($permissions["use_sms_on_password_reset"]);
	        unset($permissions["superUser"]);
	        unset($permissions["disabled"]);
	        unset($permissions["bsAuthentication"]);
	        unset($permissions["id"]);
	        $serP = $permissions["sp"];
	        if(empty($serP)){
	            $serP = $_SESSION["adminUsers"]["sp"];
	        }
	        require_once("/var/www/lib/broadsoft/adminPortal/getAllGroups.php");
	        unset($permissions["sp"]);
	        
	        $getAdminDefaultPermissions = getDefaultPermissionAdmiNType($db, $_SESSION["adminUsers"]["checkPermissions"]);
	        $defaultPermissions = "true";
	        $defaultPermissionsDisabled = "disabled";
	        
	        $tempPer = $permissions;
	        unset($tempPer['securityDomain']);
	        unset($tempPer['groups']);
	        $isBorPerEqual = count($tempPer) == count($getAdminDefaultPermissions);
	        if($isBorPerEqual) {
	            foreach($getAdminDefaultPermissions as $perKey => $perVal) {
	                if(!isset($tempPer[$perKey])) {
	                    $defaultPermissions = "false";
	                    $defaultPermissionsDisabled = "";
	                    break;
	                }
	            }
	        } else {
	            $defaultPermissions = "false";
	            $defaultPermissionsDisabled = "";
	        }
	        ?>
	        
	        <?php 
            if ($_SESSION["adminUsers"]["superUserUpdate"] == "0")
            {
                $opacity = "";
                if($defaultPermissionsDisabled == "disabled") {
                    $opacity = "opacity: 0.6;";
                }
            ?>
	            <div class="col-md-6">                            
                            <div class="form-group" style="float: left;">
                                <div class="marginTopSe">&nbsp;</div>
                                    <input type="checkbox"
                                           class="checkbox-lg"
                                           id="defaultPermission"
                                           style="width: auto;" <?php echo $defaultPermissionsDisabled; ?>
                                           value="1" <?php echo $defaultPermissions == "true" ? "checked" : ""; ?>>
                                           <label class="defaultPermCheckLabel" style="<?php echo $opacity;?>" for="defaultPermission"><span></span></label>
                                           <label class="labelText">Default Permissions</label>
                            </div>
                            <div id="attentionDivMod" style="float: left; margin: 23px;">&nbsp;</div>
                                        
		   </div>
				
			<?php } ?>	
				
            </div>
	</div>
		

<!--enterprise group Admin -->
<!--end code enterprise group admin -->

<input type="hidden" id="clusterNameUpdate" name="clusterNameUpdate" value="<?php echo $_SESSION["adminUsers"]["clusterNameUpdate"]; ?>" />

<div class="row" id="securityDomainDiv" style="display: <?php echo (hasSecurityDomain() ? "block" : "none"); ?>">
    <div class="">
        <div class="col-md-6">
            <div class="form-group">
				<label for="securityDomain" class="labelText">Security Domain</label><span class="required">*</span>   
				<div class="">   
				 <input type="text" class="autoFill" name="securityDomain" id="securityDomain" value="<?php echo trim($permissions['securityDomain']); ?>">
				</div>
            </div>
        </div>
    </div>

</div>

			<?php

				//if not logged in as superuser, or editing a superuser, don't display group selection
				if ($_SESSION["superUser"] == "0" or $_SESSION["adminUsers"]["superUserUpdate"] != "0")
				{
					echo "<div style=\"display:none;\">";
				}

				$groupList = "";
				if(!empty($permissions["groups"][0])){
					foreach ($permissions["groups"] as $groupArray)
					{
						$groupList .= $groupArray . ";";
					}
				}
				$_SESSION["adminUsers"]["groups_1"] = $groupList;

				//if not editing a superuser, display service provider
				
				if ($_SESSION["adminUsers"]["superUserUpdate"] == "0")
				{
					?>
					
                <div class="row" id="spDiv" style="display: <?php echo (hasSecurityDomain() ? "none" : "block"); ?>">
                     	<div class="">
                            <div class="col-md-6">
                            <div class="form-group">
                                    	<div class="labelText">Enterprise <span class="required">*</span></div>
                                    	<div class="dropdown-wrap"><select name="sp" id="sp"><?php echo getServiceProvidersList($_SESSION["adminUsers"]["sp"]); ?></select>
                			<?php //echo $_SESSION["adminUsers"]["sp"]; ?>
                						</div>
                    	    </div>
                            </div>
                       	</div>
                 </div>
                	<?php
                	}
                ?>
             
           <?php if ($_SESSION["adminUsers"]["superUserUpdate"] == "0")
			{
	       ?>
			<div id="groupDiv" class="row" style="display:<?php echo (hasSecurityDomain() ? "none" : "block"); ?>">
                 <div class="col-md-12">
        		    <div class="form-group">
                        <label class="labelText">Groups <span class="required">*</span></label>
                        <input type="hidden" name="groups_1" id="groups_1" value="<?php echo $groupList; ?>">
                    </div>
            	 </div>
            <div class="">
    	        <div class="col-md-6 availNCurrentGroup">
                        	<div class="form-group">Available Groups</div>
        		</div>
    			<div class="col-md-6 availNCurrentGroup">
                		<div class="form-group">Current Groups</div>
        		</div>
	    	</div>

    
			<div class="row form-group">
				<div class="col-md-12">
					<div class="col-md-6" style="">
					
							<?php
							if (isset($allGroups))
							{
								?>
								<div class="form-group scrollableListCustom1 sdasdasd">
									<ul id="sortable1" class="connectedSortablesadsdsd connectedSortableCustom">
								<?php
								foreach ($allGroups as $v)
								{
									//display groups user does not have permissions for
									if (!in_array($v, $permissions["groups"]))
									{
										?>
										<li class="ui-state-default" id="<?php echo $v; ?>"><?php echo $v; ?></li>

										<?php
									}
								}
								?>
									</ul>
								</div>
							<?php
							}
							?>
						
					
				</div>
					<div class="col-md-6" style="">
					
							<?php
							if (isset($allGroups))
							{
							?>
								<div class="form-group scrollableListCustom2">
									<ul id="sortable2" class="connectedSortable connectedSortableCustom">
							<?php
								foreach ($allGroups as $v)
								{
									//display groups user has permissions for
									if (in_array($v, $permissions["groups"]))
									{
										?>
										<li class="ui-state-highlight" id="<?php echo $v; ?>"><?php echo $v; ?></li>
										<?php
									}
								}
								?>
									</ul>
								</div>
							<?php
							}
							?>
							
					</div>
				</div>
			</div>
	</div>
	<?php 
	}
	?>
			<?php
				//if not logged in as superuser, or editing a superuser, don't display group selection
				if ($_SESSION["superUser"] == "0" or $_SESSION["adminUsers"]["superUserUpdate"] != "0")
				{
					echo "</div>";
				}

				//if editing a superuser, don't display permissions
				if ($_SESSION["adminUsers"]["superUserUpdate"] == "0")
				{
					?>

				<div class="row">
					<div class="col-md-6" style="padding-left: 0;">
						<div class="col-md-6 voiceMsgTxt">
						 <div class="form-group">
						<label class="labelText">Permissions</label><span class="required">*</span>
							<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="At least one permission must be selected."><img src="images/NewIcon/info_icon.png" /></a>
							<!-- span class="required">*</span-->
								
						</div>
						</div>
						<div class="col-md-6 voiceMsgRBtn">
						<div class="col-md-6 rBtnYes" style="padding-right: 0;">
							<div class="form-group">
							 <input type="radio" name="permissionRadio"	id="permissionRadioSelectAll" class="checkbox-lg" value="Select All" onclick="updatePermissions(this)">
							 <label class="labelText labelTextMargin" for="permissionRadioSelectAll"><span></span>Select All</label>
							</div>
						</div>
			
						 <div class="col-md-6 rBtnNo" style="padding-right: 0;">
							 <div class="form-group">
								<input type="radio" name="permissionRadio" id="permissionRadioClearAll" class="checkbox-lg" value="Clear" onclick="updatePermissions(this)">
								<label class="labelText" for="permissionRadioClearAll"><span></span>Clear All</label>
							</div>
						</div>
						</div>
					</div>
				</div>

					
					
					
					<!-- Permissions start -->

<?php
// print_r($permissions); exit;
$countCheckPermExist = checkPermissionExist($db, $_POST["userId"]);
if($countCheckPermExist == 0){
    //if not than select the default permission sets
    $permissions = getDefaultPermissionAdmiNType($db, $_SESSION["adminUsers"]["checkPermissions"]);    
}
if(isset($license["vdmLite"]) && $license["vdmLite"] =="false"){
    unset($permissions["vdmLight"]);
    unset( $_SESSION["fieldNames"]["vdmLight"]);
    unset($permissions["vdmAdvanced"]);
    unset( $_SESSION["fieldNames"]["vdmAdvanced"]);
    
    unset($permissions["securityDomain"]);
}
$permissionCategories = getPermissionsFromPermissionsCategories($db);

?>
    <div id="adminPermissions">                                 
    <!-- Users Permissions -->
	<?php
	
	$i = 0;
	$toolTip = "<span class='tooltiptextUsers tooltiptext'>";
	$userCategoryPermissions = "<div>";
	$openAccordian = 0;
        if(count($permissionCategories['Users']) > 0){
	foreach($permissionCategories['Users'] as $perKey => $perVal) {
	    $toolTip .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $createPermission = createPermissionCheckboxes($perVal, $permissions, $i);
	    $userCategoryPermissions .= $createPermission["html"];
	    
	    if($openAccordian == 0) {
	        $openAccordian = $createPermission['openAccordian'];
	    }  
	    $i++;
	}
	$userCategoryPermissions .= "</div>";
	$toolTip .= "</span>";
	?>
	
		<div class="">&nbsp;</div>
        <div class="row">
           <div class="col-md-12" style="">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="userPerRow">
                		<div class="panel-heading" role="tab" id="usersTitle">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width userPerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#userPerDiv" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="userPerDiv">User Permissions
                				   </a>
                			</h4>
        <!--         	tool tip -->
        					<div id="usertool1">
        					<?php echo $toolTip; ?>
        					</div>
                		</div>
                		<div id="userPerDiv" class="collapsideDiv panel-collapse collapse <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="usersTitle">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $userCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
	<?php } ?>	
<!-- Groups Permissions -->
<?php 
	//array_multisort( array_column($permissionCategories['Groups'], "description"), SORT_ASC, $permissionCategories['Groups']);
	$i = 0;
	$toolTipGrpup = "<span class='tooltiptextGroups tooltiptext'>";
	$groupCategoryPermissions = "<div>";
	$openAccordian = 0;
        if(count($permissionCategories['Groups']) > 0){
	foreach($permissionCategories['Groups'] as $perKey => $perVal) {
	    $toolTipGrpup .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $createPermission = createPermissionCheckboxes($perVal, $permissions, $i);
	    $groupCategoryPermissions .= $createPermission["html"];
	    if($openAccordian == 0) {
	        $openAccordian = $createPermission['openAccordian'];
	    }
	    $i++;
	}
	$groupCategoryPermissions .= "</div>";
	$toolTipGrpup .= "</span>";
	?>
	
	<div class="row">
           <div class="col-md-12" style="">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="groupPerRow">
                		<div class="panel-heading" role="tab" id="groupTitle">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width groupPerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#groupPerDiv" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="groupPerDiv">Group Permissions
                				   </a>
                			</h4>
        <!--   tool tip --><div id="usertool2">
        					<?php echo $toolTipGrpup; ?>
        					</div>
                		</div>
                		<div id="groupPerDiv" class="collapsideDiv panel-collapse collapse <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="groupTitle">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $groupCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        <?php } ?>
<!-- 		Services Permissions -->
<?php 
	//array_multisort( array_column($permissionCategories['Services'], "description"), SORT_ASC, $permissionCategories['Services']);
	$i = 0;
	$toolTipServ = "<span class='tooltiptextServices tooltiptext'>";
	$servicesCategoryPermissions = "<table border='1' style='width:80%;'>";
	$openAccordian = 0;
        if(count($permissionCategories['Services']) > 0){
	foreach($permissionCategories['Services'] as $perKey => $perVal) {
	    $toolTipServ .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $servicesCategory = createPermissionCheckboxes($perVal, $permissions, $i);
	    
	    $servicesCategoryPermissions .= $servicesCategory["html"];
	    if($openAccordian == 0) {
	        $openAccordian = $servicesCategory['openAccordian'];
	    }
	    $i++;
	}
	$servicesCategoryPermissions .= "</table>";
	$toolTipServ .= "</span>";
	?>
		<div class="row">
           <div class="col-md-12" style="">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="servicesPerRow">
                		<div class="panel-heading" role="tab" id="servicesTitle">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width servicePerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#servicesPerDiv" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="servicesPerDiv">Services Permissions
                				   </a>
                			</h4>
        <!--   tool tip --><div id="usertool3">
        					<?php echo $toolTipServ; ?>
        					</div>
                		</div>
                		<div id="servicesPerDiv" class="collapsideDiv panel-collapse collapse <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="servicesTitle">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $servicesCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        
        <?php } ?>
<!-- Devices Permissions -->
<?php 
	//array_multisort( array_column($permissionCategories['Devices'], "description"), SORT_ASC, $permissionCategories['Devices']);
	$i = 0;
	$toolTipDevice = "<span class='tooltiptextDevices tooltiptext'>";
	$devicesCategoryPermissions = "<table border='1' style='width:80%;'>";
	$openAccordian = 0;
        if(count($permissionCategories['Devices']) > 0){
	foreach($permissionCategories['Devices'] as $perKey => $perVal) {
	    $toolTipDevice .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $devicesCategory = createPermissionCheckboxes($perVal, $permissions, $i);
	    
	    $devicesCategoryPermissions .= $devicesCategory["html"];
	    if($openAccordian == 0) {
	        $openAccordian = $devicesCategory['openAccordian'];
	    }
	    $i++;
	}
	$devicesCategoryPermissions .= "</table>";
	$toolTipDevice .= "</span>";
	?>
		
		<div class="row">
           <div class="col-md-12" style="">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="devicePerRow">
                		<div class="panel-heading" role="tab" id="devicesTitle">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width devicePerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#devicesPerDiv" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="devicesPerDiv">Devices Permissions
                				   </a>
                			</h4>
        <!--   tool tip --><div id="usertool4">
        					<?php echo $toolTipDevice; ?>
        					</div>
                		</div>
                		<div id="devicesPerDiv" class="collapsideDiv panel-collapse collapse <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="devicesTitle">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $devicesCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        <?php } ?>
<!-- Supervisory Permissions -->
<?php 
	//array_multisort( array_column($permissionCategories['Supervisory'], "description"), SORT_ASC, $permissionCategories['Supervisory']);
	$i = 0;
	$toolTipSupervisory = "<span class='tooltiptextSupervisory tooltiptext'>";
	$supervisoryCategoryPermissions = "<table border='1' style='width:80%;'>";
	$openAccordian = 0;
        if(count($permissionCategories['Supervisory']) > 0){
	foreach($permissionCategories['Supervisory'] as $perKey => $perVal) {
	    $toolTipSupervisory .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $supervisoryCategory = createPermissionCheckboxes($perVal, $permissions, $i);
	    
	    $supervisoryCategoryPermissions .= $supervisoryCategory["html"];
	    if($openAccordian == 0) {
	        $openAccordian = $supervisoryCategory['openAccordian'];
	    }
	    $i++;
	}
	$supervisoryCategoryPermissions .= "</table>";
	$toolTipSupervisory .= "</span>";
	?>
		
		<div class="row">
           <div class="col-md-12" style="">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="supervisoryPerRow">
                		<div class="panel-heading" role="tab" id="supervisoryTitle">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width superPerCls <?php echo $openAccordian == 1 ? '':'collapsed';?>" role="button" data-toggle="collapse"
                				   href="#supervisoryPerDiv" aria-expanded="<?php echo $openAccordian == 1 ? "true" : "false";?>"
                				   aria-controls="supervisoryPerDiv">Supervisory Permissions
							    </a>
                			</h4>
        <!--   tool tip -->
							<div id="usertool5">
								<?php echo $toolTipSupervisory; ?>
							</div>
						</div>
                		<div id="supervisoryPerDiv" class="collapsideDiv panel-collapse collapse <?php echo $openAccordian == 1 ? 'in':'';?>" role="tabpanel" aria-labelledby="supervisoryTitle">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $supervisoryCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>     	
	
        <?php } ?>
<!-- Permissions end -->
									

					<?php
				}
			?>
					
                   
			
	<div class="row">
		<div class="form-group alignBtn">
        			<!-- <p class="register-submit"> -->
                            <?php 
                               $administratorsServiceLogsArr         = explode("_", $_SESSION["permissions"]["administrators"]);
                                $administratorsLogPermission          = $administratorsServiceLogsArr[0];
                                $modAdminPersmission                  = $administratorsServiceLogsArr[1];
                                $addDelAdminPersmission               = $administratorsServiceLogsArr[2];
                                if ($administratorsLogPermission == "1" && $modAdminPersmission == "yes")
        			{
                            ?>
        			<input type="button" name="subBut" id="subBut" class="subButton marginRightButton" value="Submit">
                           <?php } ?>
                       <!-- </p> -->
        	
        		<?php
        			//if logged in as superuser, delete admins (except for system admin)
                                
                                
                                if ($administratorsLogPermission == "1" && $addDelAdminPersmission == "yes")
        			{
        			
        				// echo "<p class=\"register-submit\">";
        				echo "<input type=\"button\" class=\"deleteBtn marginRightButton\" id=\"delBut\" name=\"delBut\" value=\"Delete Administrator\">";
        				// echo "</p>";
        			}
        		?>
    	</div>
	</div>

</div>
<!-- 	assigned permission -->
<?php 

foreach($permissions as $key => $value)
{
    if($key !="groups" && $key !="securityDomain") {
        echo '<input type="hidden" name="assignedPermissions['.$key.']" value="'. $value. '">';
    }
}
?>

</form>
<script>

/* Tooltip show/hide on hover & click */

function tooltipShowHideHover(element){
	var dataId = element.children('div').attr('id');
     var isCollapsed= element.find('h4:first').find('a:first').hasClass("collapsed");
     if(isCollapsed) {
    	$('#'+dataId).removeClass("hideShow");
     } else  {
    	 $('#'+dataId).addClass("hideShow");
     }
 };
 function tooltipShowHideClick(element){
	 var dataId = element.children('div').attr('id');
     var isCollapsed= element.find('h4:first').find('a:first').hasClass("collapsed");
     if(isCollapsed) {
    	 $('#'+dataId).addClass("hideShow");
     } else  {
    	 
    	 $('#'+dataId).removeClass("hideShow");
     	}
	 };
	 
 $(".panel-heading").hover(function(){
	 var element = $(this);
	 tooltipShowHideHover(element);
 });
 $(".panel-heading").click(function(){
	 var element = $(this);
	 tooltipShowHideClick(element);
 });

 /* tooltip hover event */
  new Popper(
		  document.querySelector('#usersTitle'),
		  document.querySelector('.tooltiptextUsers')
		  
		);

new Popper(
		  document.querySelector('#groupTitle'),
		  document.querySelector('.tooltiptextGroups')
		);
 /*end tooltip hover event */
 

</script>
<div id="dialogUpdate" class="dialogClass"></div>
<div id="dialogDelete" class="dialogClass"></div>
