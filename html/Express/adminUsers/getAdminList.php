<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	require_once("/var/www/html/Express/functions.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/domains/domains.php");
	//$level = $_POST['level'];
	$selectAdminN = $_POST['selectAdminN'];
        
	$spListArray = array();
	$users = array();
	$adminList = array();
	
	//$dnsList = getServiceProviderDomainListRes(); 
        $clusterSupport  =  (isset($bwClusters) && $bwClusters == "true") && $enableClusters == "true" ? true : false;
        if($clusterSupport){
                $clusterName  = $_SESSION['selectedCluster'];
                $whereCndtn   = " u.superUser = '0' and u.clusterName='$clusterName' ";
        }else{
                $whereCndtn   = " u.superUser = '0' ";
        }
        
        
        if(isset($_SESSION["securityDomain"]) && $_SESSION["securityDomain"]!="" && $securityDomainPattern!="" ){
            $whereCndtn .= " and p.securityDomain LIKE '%".$_SESSION["securityDomain"]."%' ";   
        }else{
            $spListArr = $_SESSION['spListNameId'];
            $spList = implode("','", $spListArr);

            if(!empty($spListArr)){
                $whereCndtn .= " and p.sp IN ('$spList')";
            }
        }
        
	$sel = "SELECT distinct u.userName, u.id, u.emailAddress, u.failedLogins, u.lastName,u.adminType, u.superUser, u.disabled, u.lastLogin, u.user_cell_phone_number, u.use_sms_every_login, u.use_sms_on_password_reset, u.use_ldap_authentication, u.failedLogins, p.securityDomain , p.sp from users u";
	$sel .= " left join permissions p on u.id = p.userId where $whereCndtn ";
        $sel .= " order by u.lastName, u.firstName, u.userName";
                 
        $rqw = $db->query($sel);
	while ($r = $rqw->fetch(MYSQL_ASSOC))
	{
	    
	    /*
	     if ($level == 'enterprise')
	    {
	        if(! isAdminExistInSP($level, $r['securityDomain'], $r, $_SESSION["sp"], $spListArray)) {
	            continue;
	        }
	    } 
	     */
	    
	    /* Filter Admin Data */
	    if(!applyFilterForAdmin($selectAdminN, $r)) {
            continue;
        }
	    
		$users[$r["id"]]["name"] = $r["firstName"] . " " . $r["lastName"];
		$users[$r["id"]]["firstName"] = $r["firstName"];
		$users[$r["id"]]["lastName"] = $r["lastName"];
		$users[$r["id"]]["UserName"] = $r["userName"];
		$users[$r["id"]]["SuperUser"] = $r["superUser"];
		$users[$r["id"]]["Disabled"] = $r["disabled"] == "0" ? "No" : "Yes";
		$users[$r["id"]]["adminType"] = $r["adminType"];
		$users[$r["id"]]["emailAddress"] = $r["emailAddress"];
		$users[$r["id"]]["user_cell_phone_number"] = str_replace("-","",$r["user_cell_phone_number"]);
		$users[$r["id"]]["adminTypeIndex"] = $r["adminTypeIndex"];
		$users[$r["id"]]["securityDomain"] = $r["securityDomain"] == null ? "" : $r["securityDomain"];
		$users[$r["id"]]["sp"] = $r["sp"];
		$users[$r["id"]]["groupId"][] = $r["groupId"];
		$users[$r["id"]]["use_sms_every_login"] = $r["use_sms_every_login"] == "0" ? "No" : "Yes";
		$users[$r["id"]]["use_ldap_authentication"] = $r["use_ldap_authentication"] == "0" ? "No" : "Yes";
		$users[$r["id"]]["failedLogins"] = $r["failedLogins"];
// 		$users[$r["id"]]["lastLogin"] = $r["lastLogin"];
		$users[$r["id"]]["require_sms_to_reset"] = $r["use_sms_on_password_reset"] == "0" ? "No" : "Yes";
		$users[$r["id"]]["lastLogin"] = $r["lastLogin"] == null ? "" : $r["lastLogin"];
		
	}
	
	foreach ($users as $key => $value)
	{
		if ($value["SuperUser"] == "2") {
			continue;
		}
		
		if ($value["SuperUser"] == "1")
		{
			$value['adminType'] = "Super User";
		} 
		
		if(! hasSecurityDomain()) {
		    $value["securityDomain"] = "False";
		}
		 
// 		if ($value["Disabled"] == "1")
// 		{
// 			$disabled = "<span></span>";
// 		}else{
// 			$disabled = "";
// 		}
		
		$value['groupList'] = implode(",", $value['groupId']);
		//$value["modAdminHref"] = $value["UserName"] . " - " . $value["name"];
		$value["modAdminHref"] = $value["UserName"] . "<span>-</span>" . $value["name"];
// 		$value["modAdminHref"] = $value["UserName"] . " - " . $value["name"] . $su . $disabled;
		$adminList[] = $value;
	}
	 
	usort($adminList, 'querySort');
	
	print_r(json_encode($adminList));

	
/*-------------------------------------------------------------------------------------------------------------*/
	function querySort ($x, $y) {
	    return strcasecmp($x['name'], $y['name']);
	}
	
	/*
	 function isAdminExistInSP($level, $securityDomain, $row, $sessionSP, $spListArray) {
	    global $client, $sessionid;
    	    if($row["superUser"] == "1") {
    	        return true;
    	    } else {
    	        $spList = array();
    	        if($securityDomain != "") {
    	            if(isset($spListArray[$securityDomain])) {
    	                $spList = $spListArray[$securityDomain];
    	            } else {
    	                $spList = getSecurityDomainSp($securityDomain, $client, $sessionid);
    	                $spListArray[$securityDomain] = $spList;
    	            }
    	            
    	        } else if($row['sp'] != "") {
    	            $spList[] = $row['sp'];
    	        }
    	        if(in_array($sessionSP, $spList)) {
    	            return true;
    	        }
    	        return false;
    	    }
	}
	 */
	
	function applyFilterForAdmin($selectAdminN, $r) {
	    $adminExist = true;
	    if($selectAdminN != "") {
	        $name = $r["firstName"] . " " . $r["lastName"];
	        $adminType = $r["superUser"] == "1" ? "Super User" : $r['adminType'];
	        
    	     if(
    	         stripos($r["userName"], $selectAdminN) !== false ||
    	         stripos($adminType, $selectAdminN) !== false ||
    	         stripos($r["user_cell_phone_number"], $selectAdminN) !== false ||
    	         stripos($name, $selectAdminN) !== false
    	         
	          ) {
	            $adminExist = true;
	        } else {
	            $adminExist = false;
	        } 
	    }
	    
	    return $adminExist;
	}
	
	function getServiceProviderDomainListRes() {
	    $dns = new domains();
	    $dnslist = $dns->getServiceProviderDomainListRes($_SESSION['sp']);
	    return $dnslist;
	}
	
	function hasSecurityDomain() {
	    global $securityDomainPattern;
	    
	    return $securityDomainPattern != "";
	}
?>
