<!-- User Filters -->
<link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v11">
<!--<script src="/Express/js/bootstrap.min.js"></script>-->
<script src="/Express/js/bootstrap/bootstrap.min.js"></script>
<script src="/Express/js/jquery-1.17.0.validate.js"></script>
<script src="/Express/js/jquery.loadTemplate.min.js"></script>
<script src="userMod/multiDragAndDrop.js"></script>
<script src="userMod/multiDragAndDropList.js"></script>
<!-- admin permission tooltip open  js -->
 <script src="/Express/js/popper.js"></script>
  <!-- end admin -->
<?php
    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();

function hasSecurityDomain() {
    global $securityDomainPattern;

    return $securityDomainPattern != "";
}

function getSecurityDomains() {
    global $systemDomains, $permissions, $securityDomainPattern;

    $securityDomain = $permissions["securityDomain"];
    $str = "<option value=\"\"";
    $str .= $securityDomain == "" ? " selected" : "";
    $str .= ">None</option>";

    foreach ($systemDomains as $domain) {
        if (strpos($domain, $securityDomainPattern) !== false ) {
            $str .= "<option value=\"" . $domain . "\"";
            $str .= $domain == $securityDomain ? " selected" : "";
            $str .= ">" . $domain . "</option>";
        }
    }
    return $str;
}
//Function changed @ 01 May 2018
function createPermissionsRadioButtons() {
    global $db;
    /*
    $permissionTypes = array();
    $query = "select adminType from permissionsSets";
    $result = $db->query($query);
    while ($row = $result->fetch()) {
        $permissionTypes[] = $row["adminType"];
    }
    */
        $permissionTypes = array();
	$query = "select adminTypeIndex, name from adminTypesLookup";
        $result = $db->query($query);
        $i = 0;
        while ($row = $result->fetch()) {
            $permissionTypes[$i]['adminTypeIndex'] = $row["adminTypeIndex"];
            $permissionTypes[$i]['name'] = $row["name"];
            $i++;
        }
    //<div class="diaPN diaP3" style="width: 2%"><input type="radio" name="checkPermissions" id="checkPermissionsDistrictIT" value="districtIT"
    //onclick="updatePermissions(this)"></div>
    //<div class="diaPN diaP3"><label style="margin-left: 4px; margin-right: 16px; margin-top: 4px">District IT</label></div>

    $html = "";
    $options = "";
    if(count($permissionTypes) > 0) {
        $options = "<option value=''>None</option>";
        /*
        for ($i = 0; $i < count($permissionTypes); $i++) {
            $type = $permissionTypes[$i];
            $options .= "<option value='$type'>$type</option>";
        }*/
        for ($i = 0; $i < count($permissionTypes); $i++) {
                $adminTypeIndex = $permissionTypes[$i]['adminTypeIndex'];
                $type = $permissionTypes[$i]['name']; 
                $options .= "<option value='$adminTypeIndex'>$type</option>";
            }
    } else {
        $options .= "<option value='Admin'>Admin</option>";
    }    
    return $options;
}

//getPermissionsFromPermissionsCategories //Change function name due to some changes in old function regarding EX-1089 (Group Permission Impact)
function getGroupPermissionsFromPermissionsCategories($db) {
    
    $fetchPermissions = "SELECT * from permissionsCategories";
    $fetchPermissionsQueryResult = $db->query($fetchPermissions);
    
    $permissionsData = array();
    while ($fetchPermissionsQueryRow = $fetchPermissionsQueryResult->fetch(MYSQL_ASSOC)) {
        if($fetchPermissionsQueryRow['category'] == 'Users') {
            $permissionsData['Users'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        } else if($fetchPermissionsQueryRow['category'] == 'Supervisory') {
            $permissionsData['Supervisory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        } else if($fetchPermissionsQueryRow['category'] == 'Services') {
            $permissionsData['Services'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        } else if($fetchPermissionsQueryRow['category'] == 'Groups') {
            $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        } else if($fetchPermissionsQueryRow['category'] == 'Devices') {
            $permissionsData['Devices'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
        }
    }
    
    foreach ($permissionsData as $key => $row) {
        $order = array();
        foreach ($row as $key1 => $row1) {
            $order[$key1] = $row1['description'];
        }
        
        array_multisort($order, SORT_ASC, $row);
        $permissionsData[$key] = $row;
    }
    
    return $permissionsData;
}

//getGroupPermissionsFromPermissionsCategories
function getPermissionsFromPermissionsCategories($db) {
    
    $fetchPermissions = "SELECT * from permissionsCategories";
    $fetchPermissionsQueryResult = $db->query($fetchPermissions);
    
    $permissionsData = array();
    $permissionsList = array();
    $_SESSION['permissionsList'] = array();
    
    while ($fetchPermissionsQueryRow = $fetchPermissionsQueryResult->fetch(MYSQL_ASSOC)) {
        if($fetchPermissionsQueryRow['category'] == 'Users')
        {
            $tempUserPermissionsArr    = explode("_", $_SESSION["permissions"]["groupAdminUserPermission"]);
            $groupAdminUserPermission  = $tempUserPermissionsArr[0];
            $groupAdminUserMod         = $tempUserPermissionsArr[1];
            $groupAdminUserAddDel      = $tempUserPermissionsArr[2];
            
            if($groupAdminUserPermission == "1" && $groupAdminUserMod == "yes" && in_array($fetchPermissionsQueryRow['permissionId'], $_SESSION["GroupAdminPermissionDependencies"]["userModPrmsonArr"])){
                $permissionsData['Users'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                'description' => $fetchPermissionsQueryRow['description']);
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
            }
            if($groupAdminUserPermission == "1" && $groupAdminUserAddDel == "yes" && in_array($fetchPermissionsQueryRow['permissionId'], $_SESSION["GroupAdminPermissionDependencies"]["userAddDelPrmsonArr"])){
                $permissionsData['Users'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                'description' => $fetchPermissionsQueryRow['description']);
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                
            }
            if($groupAdminUserPermission == "1" && in_array($fetchPermissionsQueryRow['permissionId'], $_SESSION["GroupAdminPermissionDependencies"]["userViewPrmsonArr"])){
                $permissionsData['Users'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                'description' => $fetchPermissionsQueryRow['description']);
                $permissionsList[]            = $fetchPermissionsQueryRow['permissionId'];
            }
        }
        else if($fetchPermissionsQueryRow['category'] == 'Supervisory') 
        {
            
            $tempSupervisoryPermissionsArr     = explode("_", $_SESSION["permissions"]["groupAdminSupervisoryPermission"]);
            $groupAdminSupervisoryPrmson       = $tempSupervisoryPermissionsArr[0];
            
            if($groupAdminSupervisoryPrmson == "1"){
                
                $announcementPermnsArr          = explode("_", $_SESSION["permissions"]["entAnnouncements"]);
                $announcementPermission         = $announcementPermnsArr[0];
                $announcementModPermission      = $announcementPermnsArr[1];
                $announcementAddDelPermission   = $announcementPermnsArr[2];
                
                $changeLogPermnsArr             = explode("_", $_SESSION["permissions"]["changeLog"]);
                $changeLogPermission            = $changeLogPermnsArr[0];
                
                $cdrPermnsArr                   = explode("_", $_SESSION["permissions"]["cdr"]);
                $cdrPermission                  = $cdrPermnsArr[0];
                $supervisoryPrmnsArr            = array("viewChangeLog", "viewcdrs", "announcements"); 
                
                if($fetchPermissionsQueryRow['permissionId'] == "viewChangeLog" && $changeLogPermission == "1"){
                    $permissionsData['Supervisory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                          'description'  => $fetchPermissionsQueryRow['description']);
                    $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    
                }else if($fetchPermissionsQueryRow['permissionId'] == "viewcdrs" && $cdrPermission == "1"){
                    $permissionsData['Supervisory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                          'description'  => $fetchPermissionsQueryRow['description']);
                    $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    
                }else if($fetchPermissionsQueryRow['permissionId'] == "announcements" && $announcementPermission == "1" && $announcementAddDelPermission == "yes"){
                    $permissionsData['Supervisory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                          'description'  => $fetchPermissionsQueryRow['description']);
                    $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    
                }else {
                    if(!in_array($fetchPermissionsQueryRow['permissionId'], $supervisoryPrmnsArr)){
                    $permissionsData['Supervisory'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                          'description'  => $fetchPermissionsQueryRow['description']);
                    $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
                    
                    }
                }
            }
            
            
            
        } 
        else if($fetchPermissionsQueryRow['category'] == 'Services') 
        {
            $tempServicesPermissionsArr  = explode("_", $_SESSION["permissions"]["groupAdminServicesPermission"]);
            $groupAdminServicesPrmson    = $tempServicesPermissionsArr[0];
            
            if($groupAdminServicesPrmson == "1"){
                $permissionsData['Services'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                       'description' => $fetchPermissionsQueryRow['description']);
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
            }
        }         
        else if($fetchPermissionsQueryRow['category'] == 'Devices') 
        {            
            $tempDevicePermissionsArr  = explode("_", $_SESSION["permissions"]["groupAdminDevicesPermission"]);
            $groupAdminDevicesPrmson   = $tempDevicePermissionsArr[0];
            $groupAdminDevicesMod      = $tempDevicePermissionsArr[1];
            $groupAdminDevicesAddDel   = $tempDevicePermissionsArr[2];
            
            if($groupAdminDevicesPrmson == "1" && $groupAdminDevicesMod == "yes" && in_array($fetchPermissionsQueryRow['permissionId'], $_SESSION["GroupAdminPermissionDependencies"]["DevicesModPrmsonArr"])){
                $permissionsData['Devices'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                'description' => $fetchPermissionsQueryRow['description']);
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
            }
            if($groupAdminDevicesPrmson == "1" && $groupAdminDevicesAddDel == "yes" && in_array($fetchPermissionsQueryRow['permissionId'], $_SESSION["GroupAdminPermissionDependencies"]["DevicesAddDelPrmsonArr"])){
                $permissionsData['Devices'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                'description' => $fetchPermissionsQueryRow['description']);
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
            }
            if($groupAdminDevicesPrmson == "1" && in_array($fetchPermissionsQueryRow['permissionId'], $_SESSION["GroupAdminPermissionDependencies"]["DevicesViewPrmsonArr"])){
                $permissionsData['Devices'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                                                'description' => $fetchPermissionsQueryRow['description']);
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
            }            
        }
        
        else if($fetchPermissionsQueryRow['category'] == 'Groups')
        {
            $basicInformationLogsArr              = explode("_", $_SESSION["permissions"]["basicInformation"]);
            $basicInformationLogPermission        = $basicInformationLogsArr[0];
            $basicInformationModPermission        = $basicInformationLogsArr[1];
            
            $domainsLogsArr                       = explode("_", $_SESSION["permissions"]["domains"]);
            $domainsLogPermission                 = $domainsLogsArr[0]; 
            $domainsModPermission                 = $domainsLogsArr[1]; 
            
            $servicesLogsArr                      = explode("_", $_SESSION["permissions"]["services"]);
            $servicesLogPermission                = $servicesLogsArr[0];
            $servicesModPermission                = $servicesLogsArr[1];
            
            $dnsLogsArr                           = explode("_", $_SESSION["permissions"]["DNs"]);
            $dnsLogPermission                     = $dnsLogsArr[0];
            $dnsModPermission                     = $dnsLogsArr[1];
            
            $callProcessingPoliciesLogsArr        = explode("_", $_SESSION["permissions"]["callProcessingPolicies"]);
            $callProcessingPoliciesLogPermission  = $callProcessingPoliciesLogsArr[0];
            $callProcessingPoliciesModPermission  = $callProcessingPoliciesLogsArr[1];
            
            $networkClassesofServiceLogsArr       = explode("_", $_SESSION["permissions"]["networkClassesofService"]);
            $networkClassesofServiceLogPermission = $networkClassesofServiceLogsArr[0];
            $networkClassesofServiceModPermission = $networkClassesofServiceLogsArr[1];
            
            $voicePortalServiceLogsArr            = explode("_", $_SESSION["permissions"]["voicePortalService"]);
            $voicePortalServiceLogPermission      = $voicePortalServiceLogsArr[0];
            $voicePortalServiceModPermission      = $voicePortalServiceLogsArr[1];
            
            $outgoingCallingPlanLogsArr           = explode("_", $_SESSION["permissions"]["outgoingCallingPlan"]);
            $outgoingCallingPlanLogPermission     = $outgoingCallingPlanLogsArr[0];
            $outgoingCallingPlanModPermission     = $outgoingCallingPlanLogsArr[1];
            
            $securityDomainsLogsArr               = explode("_", $_SESSION["permissions"]["securityDomains"]);
            $securityDomainsLogPermission         = $securityDomainsLogsArr[0];
            $securityDomainsModPermission         = $securityDomainsLogsArr[1];
            
            
            if($fetchPermissionsQueryRow['permissionId'] == "modifyGroup" && ($basicInformationLogPermission == "1" || $outgoingCallingPlanLogPermission == "1" 
                || $domainsLogPermission == "1" || $securityDomainsLogPermission == "1" 
                || $servicesLogPermission == "1"  || $dnsLogPermission == "1" || $callProcessingPoliciesLogPermission == "1" 
                || $networkClassesofServiceLogPermission == "1" || $voicePortalServiceLogPermission == "1"))
            { 
                $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
                
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
            }
            
            if($fetchPermissionsQueryRow['permissionId'] == "groupBasicInfo" && $basicInformationLogPermission == "1" && $basicInformationModPermission == "yes")
            { 
                $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
                
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
            }
            
            if($fetchPermissionsQueryRow['permissionId'] == "groupDomains" && $domainsLogPermission == "1" && $domainsModPermission == "yes")
            { 
                $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
                
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
            }
            
            if($fetchPermissionsQueryRow['permissionId'] == "groupServices" && $servicesLogPermission == "1" && $servicesModPermission == "yes")
            { 
                $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
                
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
            }
            
            if($fetchPermissionsQueryRow['permissionId'] == "groupDN" && $dnsLogPermission == "1" && $dnsModPermission == "yes")
            { 
                $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
                
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
            }
            
            if($fetchPermissionsQueryRow['permissionId'] == "groupPolicies" && $callProcessingPoliciesLogPermission == "1" && $callProcessingPoliciesModPermission == "yes")
            { 
                $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
                
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
            }
            
            if($fetchPermissionsQueryRow['permissionId'] == "groupNCOS" && $networkClassesofServiceLogPermission == "1" && $networkClassesofServiceModPermission == "yes")
            { 
                $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
                
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
            }
            
            if($fetchPermissionsQueryRow['permissionId'] == "groupVoicePortal" && $voicePortalServiceLogPermission == "1" && $voicePortalServiceModPermission == "yes")
            { 
                $permissionsData['Groups'][] = array('permissionId' => $fetchPermissionsQueryRow['permissionId'],
                'description' => $fetchPermissionsQueryRow['description']);
                
                $permissionsList[]          = $fetchPermissionsQueryRow['permissionId'];
            }
        }
        
    }
    
    foreach ($permissionsData as $key => $row) {
        $order = array();
        foreach ($row as $key1 => $row1) {
            $order[$key1] = $row1['description'];
        }
        
        array_multisort($order, SORT_ASC, $row);
        $permissionsData[$key] = $row;
    }
    
    $_SESSION['permissionsList'] = $permissionsList;
    return $permissionsData;
}


function createPermissionCheckboxes($perVal) {
    $html = "";
    $chk = "";
    $html .= "<div class=\"col-md-6\">";
    $html .= "<input type=\"hidden\" name=\"perms[" . $perVal['permissionId'] . "]\" value=\"0\">";
    $html .= "<input style=\"\" type=\"checkbox\" id=\"perms[" . $perVal['permissionId'] . "]\" " . $chk . " name=\"perms[" . $perVal['permissionId'] . "]\" value=\"1\"><label for=\"perms[" . $perVal['permissionId'] . "]\" " . $chk . "><span></span></label>";
	
    $html .= "<label class=\"labelText\" for=\"perms[" . $perVal['permissionId'] . "]\">" . $perVal["description"] . "</label>";
    $html .= "</div>";
    
    $returnData = array("html" => $html);
    return $returnData;
}

require_once("/var/www/lib/broadsoft/adminPortal/getSystemDomains.php");

?>
<style>
#sortable_1 li.selected, #sortable_2 li.selected{
    background:#ffb200;
}

#sortable1 li:first-child, #sortable2 li:first-child, #sortable_1 li:first-child, #sortable_2 li:first-child{margin-top: 0px;}
 
#sortable_1 li:hover, #sortable_2 li:hover{
    background:#ffb200;
    color:white;
}
 
#sortable_1 li, #sortable_2 li{
	width: 100%;
	cursor:pointer;
	font-size:12px;
	font-weight:bold;
	text-align:center;
	padding:4px 4px 4px 3px;
	margin:0 0 7px -1px;
}

#sortable1, #sortable2, #sortable_1, #sortable_2 {
    
    padding-top: 25px !important;
}
</style>
<script>
    var securityDomainPattern = "<?php echo $securityDomainPattern; ?>";
    autoCompleteFunction();
	$(function()
	{
		var bootstrapButton = $.fn.button.noConflict()
		$.fn.bootstrapBtn = bootstrapButton;

        $("#superUser").click(function(){
            if($("#superUser").is(':checked')) {
                $("#permissionsBlock").hide();
                $("#adminTypeAdd").attr("disabled", true);
            } else {
                $("#permissionsBlock").show();
                $("#adminTypeAdd").attr("disabled", false);
            }
        });

        $("#dialog").dialog($.extend({}, defaults, {
			width: 800,
			open: function(event) {
				setDialogDayNightMode($(this));
				$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('completeButton');
				$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
				$('.ui-dialog-buttonpane').find('button:contains("Add/Modify Another Admin")').addClass('addAnotherButton');
			},
			buttons: {
				"Complete": function()
				{
					var formData = $("form#newAdminForm").serialize();
					$.ajax({
						type: "POST",
						url: "adminUsers/addAdmin.php",
						data: formData,
						success: function(result)
						{
							$("#newAdminDialog").dialog("close");
							$("#dialog").dialog("open");
							$(".ui-dialog-titlebar-close", this.parentNode).hide();
							$(".ui-dialog-buttonpane", this.parentNode).show();
							$(":button:contains('Complete')").hide();
							$(":button:contains('Cancel')").hide();
							$(":button:contains('Add/Modify Another Admin')").show();
							if($("#superUser").is(":checked")){
								$("html, body").animate({ scrollTop: 0 }, 600);
							}else{
								$("html, body").animate({ scrollTop: "400px" }, 600);
							}
							$("#dialog").html(result);
							$("#dialog").append(returnLink);
						}
					});
				},
				"Cancel": function()
				{
					$(this).dialog("close");
				},
				"Add/Modify Another Admin": function()
				{
					$(this).dialog("close");
					$("html, body").animate({ scrollTop: 0 }, 600);
					$("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
					$("#loading").show();
					$(".navMenu").removeClass("active");
					$(this).addClass("active");
					$.ajax({
						type: "POST",
						url: "navigate.php",
						data: { module: 'groupAdminUser' },
						success: function(result)
						{
							$("#loading").hide();
							$("#mainBody").html(result);
						}
					});
				}
			}
		}));
                
                 $("#sortable_1, #sortable_2").multisortableList({
			placeholder: "ui-state-highlight",
			connectWith: "#sortable_1,#sortable_2",
			cursor: "crosshair",
			update: function(event, ui)
			{
				var monUsers = "";
				var order = $("#sortable_2").sortable("toArray");
				for (i = 0; i < order.length; i++)
				{
					if(order.length > 0){
						monUsers += order[i] + ";";
					}
				}
				$("#groups1").val(monUsers);
			},
                         stop : function(){
                            getArrayFromLi('#sortable_1');
                            getArrayFromLi('#sortable_2');

                        }
		}).disableSelection();
                
                
                 var insensitive = function(s1, s2) {
					 var s1lower = s1.toLowerCase();
					 var s2lower = s2.toLowerCase();
					 return s1lower > s2lower? 1 : (s1lower < s2lower? -1 : 0);
				}

				//make new html for the ul tag for ordering the li data
				var getArrayFromLi = function(ulId){
                                   
					var liArray = [];
					var liNewArrayUsers = [];
					var liStr = "";
					
					$(ulId+" li").each(function(){
						var el = $(this);
						liArray.push(el.text());
					});
					
					var liNewArray = liArray.sort(insensitive);
					$(ulId).html('');
					for (i = 0; i < liNewArray.length; i++)
					{
						liStr += '<li class="ui-state-default" id="'+liNewArray[i]+'">'+liNewArray[i]+'</li>';
					}
					$(ulId).html(liStr);
                                        /*alert('liStr - '+liStr);
                                        alert('liNewArray - '+liNewArray);
                                        alert('liNewArrayUsers - '+liNewArrayUsers);
					console.log(liNewArrayUsers);*/
				}
                

		/* $("#sortable_1, #sortable_2").sortable({
			placeholder: "ui-state-highlight",
			connectWith: ".connectedSortable",
			cursor: "crosshair",
			update: function(event, ui)
			{
				var monUsers = "";
				var order = $("#sortable_2").sortable("toArray");
				for (i = 0; i < order.length; i++)
				{
					monUsers += order[i] + ";";
				}
				$("#groups1").val(monUsers);
			}
		}).disableSelection(); */

		//select service provider for group permissions (non-superusers only)
		$("#Selsp").change(function()
		{
			$("#sortable_1").empty();
			$("#sortable_2").empty();

			var sp = $(this).val();
			$.ajax({
				type: "POST",
				url: "adminUsers/getAllGroups.php",
				data: { getSP: sp },
				success: function(result)
				{
					$("#sortable_1").append(result);
				}
			});
			//getSpSecurityDomainList(sp);
		});

		//select service provider for username (non-superusers only)
		$("#bsSp").change(function()
		{
			$("#bsGroupId").empty();
			$("#userNameDropdown").empty();

			var bsSp = $(this).val();
			$.ajax({
				type: "POST",
				url: "adminUsers/getAllGroups.php",
				data: { getSP: bsSp, dropdown: "true" },
				success: function(result)
				{
					$("#bsGroupId").append("<option value=\"\"></option>");
					$("#bsGroupId").append(result);
				}
			});
		});

		//select group for username (non-superusers only)
		$("#bsGroupId").change(function()
		{
			$("#userNameDropdown").empty();

			var bsSp = $("#bsSp").val();
			var bsGroupId = $(this).val();
			$.ajax({
				type: "POST",
				url: "adminUsers/getAllAdmins.php",
				data: { getSP: bsSp, getGroup: bsGroupId },
				success: function(result)
				{
					$("#userNameDropdown").append("<option value=\"\"></option>");
					$("#userNameDropdown").append(result);
				}
			});
		});

		//disable/enable fields based on whether user is a superuser
		function superUser()
		{
			if ($("#superUser").is(":checked"))
			{
				$("#bsSp").attr("disabled", "disabled");
				$("#bsGroupId").attr("disabled", "disabled");
				$("#userNameDropdown").attr("disabled", "disabled");
				$("#userNameText").removeAttr("disabled");
				$("#passwordText").removeAttr("disabled");
				$("#Selsp").attr("disabled", "disabled");
				$("#list1").attr("disabled", "disabled");
				$("#list2").attr("disabled", "disabled");
				$("#sortable_2").attr("disabled", "disabled");
				$("[id^=perms\\[]").attr("disabled", "disabled");
				$(".initialHide").show();
				$(".initialShow").hide();
                $("#securityDomainDiv").hide();
			}
			else
			{
				$("#bsSp").removeAttr("disabled");
				$("#bsGroupId").removeAttr("disabled");
				$("#userNameDropdown").removeAttr("disabled");
				$("#userNameText").attr("disabled", "disabled");
				$("#passwordText").attr("disabled", "disabled");
				$("#Selsp").removeAttr("disabled");
				$("#list1").removeAttr("disabled");
				$("#list2").removeAttr("disabled");
				$("#sortable_2").removeAttr("disabled");
				$("[id^=perms\\[]").removeAttr("disabled");
				$(".initialHide").hide();
				$(".initialShow").show();
				if (securityDomainPattern != "") {
                    $("#securityDomainDiv").show();
                }
			}
		}

		$("#superUser").click(function()
		{
			superUser();
		});

		//check all permissions checkboxes
		$("#checkAll").click(function()
		{
			$("[id^=perms\\[]").prop("checked", true);
		});

		superUser();
	});
        
        //Checked Select All if all group permission is checked
        $("#adminPermissions :checkbox").change(function() {
        var names = {};
        $('#adminPermissions :checkbox').each(function() {
            names[$(this).attr('name')] = true;
        });
        var count = 0;
        $.each(names, function() { 
            count++;
        });
        if ($('#adminPermissions :checkbox:checked').length === count) {
            $("#permissionRadioSelectAll").prop("checked",true);
        }
        }).change();      
        
        
        function enableDefaultPerCheckBox() {
                 $("#defaultPermissionAdd").prop("checked",false).prop("disabled", false).removeClass("opacityDisabled");
        }
        //End code


	function defaultPerCheckBox() {
		 $("#defaultPermissionAdd").prop("checked",false);
		 $("input[name='permissionRadio']").prop("checked", false);
	}
	
	$("[id^=perms]").click(function() {
		$("input[name='permissionRadio']").prop("checked", false);
		//defaultPerCheckBox();
                enableDefaultPerCheckBox();
	});
	
	$("#use_ldap_authentication_newAdmin").click(function(){
		if($(this).prop('checked') == true) {
			$(".passRequired").hide();
			$(".verifyPassRequired").hide();
		} else {
			$(".passRequired").show();
			$(".verifyPassRequired").show();
		}
	});


	function showHideAccordian(result, permissionRadio) {
		 var selectAll = false;
		 if(permissionRadio != "") {
			 selectAll = permissionRadio == "Select All" ? true : false;
		}
		 
		 if($.inArray("Users", result) > -1 || selectAll) {
				$("#usersTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#userPerDivAdd").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         if($.inArray("Groups", result) > -1 || selectAll) {
         		$("#groupTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#groupPerDivAdd").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         if($.inArray("Devices", result) > -1 || selectAll) {
         		$("#devicesTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#devicesPerDivAdd").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         if($.inArray("Services", result) > -1 || selectAll) {
         		$("#servicesTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#servicesPerDivAdd").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
         if($.inArray("Supervisory", result) > -1 || selectAll) {
         		$("#supervisoryTitleAdd #filtersStatusTitle").removeClass("collapsed").attr("aria-expanded","true");
				$("#supervisoryPerDivAdd").addClass("in").attr("aria-expanded","true").css("height", "auto");
         }
	}
	
	var permissionRadio = "";
	$("input[name='permissionRadio']").click(function() {
		permissionRadio =  $(this).val();
		showHideAccordian(result = "", permissionRadio);
	});

	
	$("#defaultPermissionAdd").click(function() {
                $('#attentionDivAdd').html("");
		$("input[name='permissionRadio']").prop("checked", false);
		if($(this).prop('checked') == true){
			var adminType = $('#adminTypeAdd option:selected').val();
			var value = {'value': adminType}; 
			updatePermissions(value);
		} else {
			//$("[id^=permsUpdate]").prop("checked", false);
		}
	});

	// accordian
	function getAllPermissionscategories(defaultPermission) {
		$("input[name='permissionRadio']").prop("checked", false);
		$(".collapsideDivAdd").removeClass('in').attr("aria-expanded","false");
		$("[id^=filtersStatusTitle]").addClass("collapsed").attr("aria-expanded","false");
		
		$.ajax({
            type: "POST",
            url: "adminUsers/getPermissions.php",
            data: { action: 'getAllPermissionscategories', defaultPermission: defaultPermission},
            success: function(result)
            {
                var result = JSON.parse(result);          
                showHideAccordian(result, permissionRadio = "");
            }
        });
	}

	
    function updatePermissions(radio) {
        //alert('updatePermissions - '+radio.value);
        if (radio.value == "Select All") {
            $("[id^=perms]").prop("checked", true);
            $("#defaultPermissionAdd").prop("checked",false);
        }
        else if (radio.value == "Clear") {
            $("[id^=perms]").prop("checked", false);
            $("#defaultPermissionAdd").prop("checked",false);
        }
        else if(radio.value !=""){
        	onAdminCheckPermission(radio.value);
        }
//         else if(radio.value == "1" || radio.value == "2" || radio.value == "3" || radio.value == "3" || radio.value == "4") 
//         {
//        	  	onAdminCheckPermission(radio.value);
//        	}
        else if(radio.value == "") {
        	$("[id^=perms]").prop("checked", false);
       	 	$("#defaultPermissionAdd").prop("checked",false);
       		$("input[name='permissionRadio']").prop("checked", false);
			return false;
		}
         else if($("#defaultPermissionAdd").prop('checked') == false) {
        	$("input[name='permissionRadio']").prop("checked", false);
			return false;
		}else {
        	onAdminCheckPermission(radio.value);
        }
       
    }
    
    /* 
    * method used for Permissions checked
    * Check permission according to Admin type change , selected
    * checkbox default checked disabled
    */
    
	var onAdminCheckPermission = function(adminTypeValue){
        //alert('adminTypeValue - '+adminTypeValue);
    	$.ajax({
             type: "POST",
             url: "adminUsers/getPermissions.php",
             data: { permissionSet:adminTypeValue },
             success: function(result)
             {
                //alert('result - '+result);$("[id^=permsUpdate]").prop("checked", false);
                console.log(result);
                $("[id^=perms]").prop("checked", false);
             	getAllPermissionscategories(result);
                 var values = result.split(",");
                 //Code added @ 06 March 2019
                 if(values.indexOf("Attention") >= 0){
                     $('#attentionDivAdd').html("<a title='Due to permissions restrictions of this Administrator, not all Default Permissions will be enabled for the created Group Administrator'><img src='images/NewIcon/attention.png' style='width: 21px; cursor: pointer; ' /></a>");
                     values.pop();
                 }
                 else{
                     $('#attentionDivAdd').html("");
                 }
                 //End code
                 for (var i = 0; i < values.length; i++) {
                      //console.log(values[i]);
                      var id = "perms[" + values[i] + "]";
                      document.getElementById(id).checked = true;
                      $("#defaultPermissionAdd").prop("checked",true).prop("disabled", true);                                   	
                 }
             }
         });
	};

	function  autoCompleteFunction() {
		var autoComplete = new Array();
		$.ajax({
			type: "POST",
			url: "adminUsers/getAllSecurityDomains.php",
			data: { action: 'getSysSecurityDomainList'},
			success: function(result)
			{
				var explode = result.split(":");
				for (a = 0; a < explode.length; a++)
				{
					autoComplete[a] = explode[a];
				}
				$(".securityDomainAdd").autocomplete({
					source: autoComplete,
					select: function( event , ui ) {
						 $(this).val($.trim(ui.item.label));
						 return false;
			         }
				});
			}
		});
	}
	
</script>
<style>.createButton{float:left;}</style>
<?php
	require_once("/var/www/lib/broadsoft/adminPortal/getServiceProviders.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getSystemDomains.php");

	$licenseArray = array();
	if(isset($license["vdmLite"]) && $license["vdmLite"] == "false"){
		$licenseArray[] = "vdmLight";
		$licenseArray[] = "vdmAdvanced";
	}
	
	$systemConfig = new DBSimpleReader($db, "systemConfig");
	$ldapAuthentication = $systemConfig->get("ldapAuthentication");

	$fields = "";
	$exp = "EXPLAIN permissions";
	$qpo = $db->query($exp);
	while ($r = $qpo->fetch(MYSQL_ASSOC))
	{
	    //if($r["Field"] != 'vdmLight') {
	    if($r["Field"] != 'vdmLight') {
	    	if(!in_array($r["Field"], $licenseArray)){
	    		$fields .= $r["Field"] . ",";
	    	}
	    } else {
	        //if(isset($license["vdmLite"]) && $license["vdmLite"] =="true"){
	    	if(!in_array($r["Field"], $licenseArray)){
	            $fields .= $r["Field"] . ",";
	        }
	    }
	}
	//echo "<pre>"; print_r($fields); die;
	$fields = substr($fields, 3, -1);
	
?>

<!--New code found Start-->
<style>

.borderUp {
    border-bottom: 1px solid #ccc;
    height: 0 !important;
    margin: 25px 0 !important;
}
/* .panel-forms .tooltiptext { */
/*   margin-left: -996px !important; */
/*   margin-top: -62px; */
    
/* } */
.hideShow{display:none;}

.averistar-bs3 .panel-default {
    margin-bottom: 35px;
}

/*modify tooltip */

.ui-dialog .ui-dialog-content{
	overflow: hidden !important;
 }
 
#usersTitleAdd #usertool1 span.tooltiptextUsers.tooltiptext,#groupTitleAdd #usertool2 span.tooltiptextGroups.tooltiptext {
   margin-left: -1080px !important;
   margin-top: -43px;
}
 
 .tooltiptext > div {
    line-height: 22px;
}
 
span#serviceTooltip, span#deviceTooltip, span#SupervisoryTooltip {
    bottom: 0% !important;
    top: inherit;
     margin-left: -386px;
}
  
</style>
<!--New code found End-->

<form action="#" method="POST" name="newAdminForm" id="newAdminForm" class="fcorn-registerTwo">
	<h2 class="adminUserText">Add Administrators</h2>
	<div class="row">
            <input type="hidden" name="superUser" value="0">
		<!--<div class="">
			<div class="col-md-12">
				<div class="form-group">
					<input type="hidden" name="superUser" value="0">
					<input type="checkbox" name="superUser" id="superUser" value="1"><label for="superUser"><span></span></label>
					<label class="labelText" for="superUser">Super User</label>
				</div>
			</div>
		</div> -->
	</div>

<?php
    	if ($ldapAuthentication == "true") {
    		?>
    		<div class="row">
    		<div class="col-md-6">
    			<div class="form-group">
    			<div class="marginTopSe">&nbsp;</div>
        			<input type="hidden" name="use_ldap_authentication" id="use_ldap_authentication_newAdmin1" value="0" />
        			<input type="checkbox" name="use_ldap_authentication" id="use_ldap_authentication_newAdmin" style="width: auto;" value="1" CHECKED /><label for="use_ldap_authentication_newAdmin"><span></span></label>
    				<label class="labelText" for="use_ldap_authentication_newAdmin">LDAP Authentication</label>
    			</div>
    		</div>	
    		</div>
    		<?php
    	} else {
    		?>
    		<input type="hidden" name="use_ldap_authentication" id="use_ldap_authentication_newAdmin" value="0" />
    		<?php
    	}
   ?>
    	
	<?php
		if ($permissionsBwAuthentication == "true") {
			?>
			<div class=" initialShow ">
				<label class="labelText" for="bsSp">Enterprise</label>
			</div>
			<div class="inputText initialShow dropdown-wrap">   
				<select name="bsSp" id="bsSp">
					<option value=""></option>
					<?php
					foreach ($sps as $v)
					{
						echo "<option value=\"" . $v . "\">" . $v . "</option>";
					}
					?>
				</select>
			</div>
			<div class=" initialShow ">
				<label class="labelText" for="bsGroupId">Group</label>
			</div>
			
			<div class="inputText initialShow dropdown-wrap">   
				<select name="bsGroupId" id="bsGroupId"></select>
			</div>
			
			<div class="initialShow">
				<label class="labelText" for="userNameDropdown">Username</label> <span class="required">*</span>
			</div>
			<div class="inputText initialShow dropdown-wrap">
				<select name="userName" id="userNameDropdown">
				</select>
			</div>
			<div class="row initialHide">
				<label class="labelText" for="userNameText">Username</label><span class="required">*</span>
			</div>
			<div class="inputText initialHide">
				<input type="text" name="userName" id="userNameText" size="35" maxlength="32">
			</div>

			<div class="initialHide">
				<label class="labelText" for="passwordText">Password</label> <span class="required">*</span>
			</div>
			<div class="inputText initialHide">
				<input type="password" name="password" id="passwordText" size="35">
			</div>
			<?php
		}
		else
		{
			?>		
	<div class="row">
		<div class="">
			<div class="col-md-6">
				<div class="form-group">
					<label class="labelText" for="userName">Username</label> <span class="required">*</span>
					<input type="text" name="userName" id="userName" size="35" maxlength="32">
				</div>
			</div>
			<!--<div class="col-md-6">
			<div class="form-group">
				<label class="labelText" for="password">Password</label> <span class="required">*</span>
				<input type="password" name="password" id="password" size="35">
			</div>
			</div>-->
			<div class="col-md-6">
    		<div class="form-group">
    	<label class="labelText" for="user_cell_phone_number">Cell Phone Number</label>
    	<input class="form-control" type="tel" name="user_cell_phone_number" id="user_cell_phone_number" size="35" maxlength="65">
    		</div>
    	</div>
		</div>
	</div>

			<?php
		}
	?>
	

	<div class="row">
	<div class="">
	  <?php if ($isAdminPasswordFieldVisible == "true") { ?>
    <div class="col-md-6">
		<div class="form-group">
		<?php if ($permissionsBwAuthentication == "true") { ?>
			<div class="initialHide"><label class="labelText" for="passwordText">Password</label>
				<span style="display:<?php echo $ldapAuthentication == 'true' ? 'none' : 'block';?>" class="required passRequired">*</span>
			</div>
			<div class="initialHide"><input type="password" name="password" id="passwordText" size="35"></div>

		<?php } else {
			?>
			<label class="labelText" for="password">Password</label>
				<span style="display:<?php echo $ldapAuthentication == 'true' ? 'none' : 'block';?>" class="required passRequired">*</span>
			<input type="password" name="password" id="password" size="35">
		<?php }?>
		</div>
	</div>
	
	<!-- 	verify password -->
	<div class="col-md-6">
		<div class="form-group">
		<?php if ($permissionsBwAuthentication == "true") { ?>
			<div class="initialHide"><label class="labelText" for="passwordText">Verify Password</label>
				<span style="display:<?php echo $ldapAuthentication == 'true' ? 'none' : 'block';?>" class="required verifyPassRequired">*</span>
			</div>
			<div class="initialHide"><input type="password" name="verifyPassword" id="verifyPasswordText" size="35"></div>

		<?php } else {
			?>
			<label class="labelText" for="password">Verify Password</label>
				<span style="display:<?php echo $ldapAuthentication == 'true' ? 'none' : 'block';?>" class="required verifyPassRequired">*</span>
			<input type="password" name="verifyPassword" id="verifyPassword" size="35">
		<?php }?>
		</div>
	</div>
	
	<?php }?>
	
</div>
</div>
<!-- Merged -->
<div class="row">
	<div class="">
      	<div class="col-md-6">
        	<div class="form-group">
            	<label class="labelText" for="name">Name</label> <span class="required">*</span>
            	<input type="text" name="name" id="name" size="35" maxlength="65">
            
            </div>
    	</div>
    			
    	<div class="col-md-6">
        	<div class="form-group">
        		<label class="labelText" for="emailAddress">Email Address</label> <span class="required">*</span>
        		<input type="text" name="emailAddress" id="emailAddress" size="40" maxlength="64">
        	</div>
    	</div>
	</div>
</div>

<div class="row ">
    <div class="">
		
    <div class="col-md-6">
			<div class="form-group">
			<input type="hidden" name="use_sms_every_login" value="0">
			<div class="marginTopSe">&nbsp;</div>
			<input style="" type="checkbox" name="use_sms_every_login" id="use_sms_every_login" value="1"><label for="use_sms_every_login"><span></span></label>
			<label class="labelText" for="use_sms_every_login">Use SMS every login</label>
			</div>
		</div>
		
    	<div class="col-md-6">
        	<div class="form-group">
			<div class="marginTopSe">&nbsp;</div>
            	<input type="hidden" name="use_sms_on_password_reset" value="0">
            	<input type="checkbox" name="use_sms_on_password_reset" id="use_sms_on_password_reset" value="1"><label for="use_sms_on_password_reset"><span></span></label>
            <!-- <div class="<?php //echo ($ldapAuthentication == "true") ? "quarterDesc" : "leftDesc2"; ?>">  --> 	
            	<label class="labelText" for="use_sms_on_password_reset">Require SMS to reset</label>
            <!--</div> -->
        	</div>
    	</div>
		
		
    </div>	
</div>

<div class="vertSpacer initialShow borderUp">&nbsp;</div>

<!--New code found Start-->
	<div class="row" id="permissionsBlock">
	   <div class="col-md-6">
           <div class="form-group">
				<label class="labelText">Admin Type: <span class="required">*</span></label>
				<div class="dropdown-wrap">
					<select id="adminTypeAdd" class="form-control" name="checkPermissions" onchange="updatePermissions(this)">
						 <?php echo createPermissionsRadioButtons(); ?>
					</select>
				</div>
			</div>
		</div>
    
		<div class="col-md-6 initialShow">                        
			<div class="form-group" style="float: left;">
			<div class="marginTopSe">&nbsp;</div>				
                                <input type="checkbox" id="defaultPermissionAdd" /><label class="defaultPermCheckLabel" for="defaultPermissionAdd"><span></span></label>
                                <label class="labelText" for="">Default Permissions</label>
                        </div>
                        <div id="attentionDivAdd" style="float: left; margin: 23px;">&nbsp;</div>
		</div>
   </div>
<!--New code found End-->

<input type="hidden" name="clusterName" value="<?php echo $_SESSION['selectedCluster']; ?>" />


<div class="row">
<div class="">
	<div class="col-md-6">
        <div class="form-group" id="securityDomainDiv" style="display: <?php echo (hasSecurityDomain() ? "block" : "none"); ?>">
	        <label class="labelText" for="securityDomain">Security Domain <span class="required">*</span></label>
			<input type="text" class="form-control autoFill securityDomainAdd" name="securityDomain" id="securityDomain">
	</div>
        </div>
</div>
</div>

<div class="row">
    <div class="">
    <div class="col-md-6" style="display: <?php echo (hasSecurityDomain() ? "none" : "block"); ?>">
	<div class="form-group" id="spDiv" style="display: <?php echo (hasSecurityDomain() ? "none" : "block"); ?>">
	    <div class="initialShow">
	    	<label class="labelText" for="Selsp">Enterprise</label><span class="required">*</span>
	    </div>
	    <div class="initialShow dropdown-wrap">            
            <select name="Selsp" id="Selsp">
                <option value=""></option>
                <?php
                $sp_List  = $_SESSION['spListNameId'];
                foreach ($sp_List as $v)
                {
                    echo "<option value=\"" . $v . "\">" . $v . "</option>";
                }
                ?>
            </select>
        </div>
        </div>
	</div>
       </div>
</div>

    <div class="row" id="groupDiv" style="display: <?php echo (hasSecurityDomain() ? "none" : "block"); ?>">
    	<div class="">
		    <div class="col-md-12">
		    <div class="form-group">
		        <div class="initialShow"><label class="labelText">Groups <span class="required">*</span></label></div>
        		<input type="hidden" name="groups1" id="groups1" value="">
			</div>
			</div>
		</div>
		<div class="">
		    <div class="col-md-6 availNCurrentGroup">
			    <div class="form-group initialShow">Available Groups</div>
	        </div>
	        <div class="col-md-6 availNCurrentGroup">
		        <div class="form-group initialShow">Current Groups</div>
        	</div>
        </div>
       
		<div class=""> 
			<div class="col-md-6"> 
				<div class="form-group">
			        <div class="initialShow">
			            <div id="list1" class="scrollableList1">
			                <ul id="sortable_1" class="connectedSortable connectedSortableCustom">
                </ul>
            </div>
        </div>
		        </div>
	        </div>
	        <div class="col-md-6">
		        <div class="form-group">
			        <div class="initialShow">
			            <div id="list2" class="scrollableList2">
			                <ul id="sortable_2" class="connectedSortable connectedSortablecustom">
                </ul>
            </div>
        </div>
		        </div>
	        </div>
	    </div>
    </div>
    

<!--  

    <div id="permissionsBlock">
        <div class="diaPL diaP3">
	<label>Permissions: <span class="required">*</span></label></div>
        <?php //echo createPermissionsRadioButtons(); ?>
        <div class="diaPL diaP18"></div>
    </div> -->
    	<!-- Permissions -->
		
		
		
       
	<div class="row initialShow">
		<div class="col-md-6" style="padding-left: 0;">
			<div class="col-md-6 voiceMsgTxt">
			 <div class="form-group">
			<label class="labelText">Permissions</label><span class="required">*</span>
			<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="At least one permission must be selected."><img src="images/NewIcon/info_icon.png" /></a>
			</div>
			</div>
			<div class="col-md-6 voiceMsgRBtn">
			<div class="col-md-6 rBtnYes" style="padding-right: 0;">
				<div class="form-group">
				 <input type="radio" name="permissionRadio"	id="permissionRadioSelectAll" class="checkbox-lg" value="Select All" onclick="updatePermissions(this)">
				 <label class="labelText labelTextMargin" for="permissionRadioSelectAll"><span></span>Select All</label>
				</div>
			</div>

			 <div class="col-md-6 rBtnNo" style="padding-right: 0;">
				 <div class="form-group">
					<input type="radio" name="permissionRadio" id="permissionRadioClearAll" class="checkbox-lg" value="Clear" onclick="updatePermissions(this)">
					<label class="labelText" for="permissionRadioClearAll"><span></span>Clear All</label>
				</div>
			</div>
			</div>
		</div>
	</div>
              
        <div id="adminPermissions" class="initialShow">
	<?php 
	$permissionCat = getPermissionsFromPermissionsCategories($db);
	//echo "<pre>SESSION DATA - ";
        //print_r($_SESSION["permissions"]);
        //print_r($permissionCat);
	//Users Permissions
	$toolTip = "<span class='tooltiptextUsers tooltiptext' id='userTooltip'>";
	$userCategoryPermissions = "<div>";
        
        //echo "<pre>";
        //print_r($permissionCat['Users']);
//        /die();
        
        if(count($permissionCat['Users']) > 0){
	foreach($permissionCat['Users'] as $perKey => $perVal) {
	    $toolTip .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $createPermission = createPermissionCheckboxes($perVal);
	    $userCategoryPermissions .= $createPermission["html"];
	}
	$userCategoryPermissions .= "</div>";
	$toolTip .= "</span>";
	?>
	
	
		
        <div class="row">
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="userPerRowAdd">
                		<div class="panel-heading" role="tab" id="usersTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#userPerDivAdd" aria-expanded="false"
                				   aria-controls="userPerDivAdd">User Permissions
                				   </a>
                			</h4>
        <!--         	tool tip -->
       					 	<div id="usertool1">
            					<?php echo $toolTip; ?>
                    		</div>
        				</div>
                		<div id="userPerDivAdd" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="usersTitleAdd">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $userCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        <?php } ?>	
<!-- Groups Permissions -->
<?php 
        if(count($permissionCat['Groups']) > 0){
	$toolTipGrpup = "<span class='tooltiptextGroups tooltiptext' id='groupToolTip'>";
	$groupCategoryPermissions = "<div>";
	foreach($permissionCat['Groups'] as $perKey => $perVal) {
	    $toolTipGrpup .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $createPermission = createPermissionCheckboxes($perVal);
	    $groupCategoryPermissions .= $createPermission["html"];
	}
	$groupCategoryPermissions .= "</div>";
	$toolTipGrpup .= "</span>";
	?>
	
	<div class="row">
	
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="groupPerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="groupTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#groupPerDivAdd" aria-expanded="false"
                				   aria-controls="groupPerDiv">Group Permissions
                				   </a>
                			</h4>
        <!--   tool tip -->
        					<div id="usertool2">
            					<?php echo $toolTipGrpup; ?>
                    		</div>
        				</div>
                		<div id="groupPerDivAdd" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="groupTitleAdd">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $groupCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        <?php } ?>
<!-- 		Services Permissions -->
<?php 
        if(count($permissionCat['Services']) > 0){
	$toolTipServ = "<span class='tooltiptextServices tooltiptext' id='serviceTooltip'>";
	$servicesCategoryPermissions = "<table border='1' style='width:80%;'>";
	foreach($permissionCat['Services'] as $perKey => $perVal) {
	    $toolTipServ .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $servicesCategory = createPermissionCheckboxes($perVal);
	    
	    $servicesCategoryPermissions .= $servicesCategory["html"];
	}
	$servicesCategoryPermissions .= "</table>";
	$toolTipServ .= "</span>";
	?>
		<div class="row">
		
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="servicesPerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="servicesTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#servicesPerDivAdd" aria-expanded="false"
                				   aria-controls="servicesPerDivAdd">Services Permissions
                				   </a>
                			</h4>
        <!--   tool tip -->
        					<div id="usertool3">
            					<?php echo $toolTipServ; ?>
                    		</div>
        				</div>
                		<div id="servicesPerDivAdd" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="servicesTitleAdd">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $servicesCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        
         <?php } ?>
<!-- Devices Permissions -->
<?php 
        if(count($permissionCat['Devices']) > 0){
	$toolTipDevice = "<span class='tooltiptextDevices tooltiptext' id='deviceTooltip'>";
	$devicesCategoryPermissions = "<table border='1' style='width:80%;'>";
	foreach($permissionCat['Devices'] as $perKey => $perVal) {
	    $toolTipDevice .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $devicesCategory = createPermissionCheckboxes($perVal);
	    
	    $devicesCategoryPermissions .= $devicesCategory["html"];
	}
	$devicesCategoryPermissions .= "</table>";
	$toolTipDevice .= "</span>";
	?>
		
		<div class="row">
		
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="devicePerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="devicesTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#devicesPerDivAdd" aria-expanded="false"
                				   aria-controls="devicesPerDivAdd">Devices Permissions
                				   </a>
                			</h4>
        <!--   tool tip -->
        					<div id="usertool4">
            					<?php echo $toolTipDevice; ?>
                    		</div>
        				</div>
                		<div id="devicesPerDivAdd" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="devicesTitleAdd">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $devicesCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
                        <!--    Tool Tip For users list   -->
        			</div>
        		</div>
        	</div>
        </div>
        <?php } ?>
<!-- Supervisory Permissions -->
<?php
        if(count($permissionCat['Supervisory']) > 0){
	$toolTipSupervisory = "<span class='tooltiptextSupervisory tooltiptext' id='SupervisoryTooltip'>";
	$supervisoryCategoryPermissions = "<table border='1' style='width:80%;'>";
	foreach($permissionCat['Supervisory'] as $perKey => $perVal) {
	    $toolTipSupervisory .= "<div style='margin-left: 15px;'>".$perVal['description']."</div>";
	    $supervisoryCategory = createPermissionCheckboxes($perVal);
	    
	    $supervisoryCategoryPermissions .= $supervisoryCategory["html"];
	}
	$supervisoryCategoryPermissions .= "</table>";
	$toolTipSupervisory .= "</span>";
	?>
		
		<div class="row">
		
           <div class="col-md-12 uiModalAccordian">
        		<div class="averistar-bs3">
        			<div class="panel panel-default panel-forms" id="supervisoryPerRowAdd" style="width: 100%;">
                		<div class="panel-heading" role="tab" id="supervisoryTitleAdd">
                			<h4 class="panel-title">
                				<a id="filtersStatusTitle"
                				   class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                				   href="#supervisoryPerDivAdd" aria-expanded="false"
                				   aria-controls="supervisoryPerDivAdd">Supervisory Permissions
                				   </a>
                			</h4>
        <!--   tool tip -->
            				<div id="usertool5">
            					<?php echo $toolTipSupervisory; ?>
                    		</div>
        				</div>
                		<div id="supervisoryPerDivAdd" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="supervisoryTitleAdd">
                			<div class="panel-body">
                                <div>
                    				<div class="">
                    						<?php echo $supervisoryCategoryPermissions; ?>
                    				</div>
                				</div>
                			</div>
                		</div>
        			</div>
        		</div>
        	</div>
        </div>
        
	</div>	
	<?php } ?>
	
	<!-- New code found start-->   
</form>
<script>

/* Tooltip show/hide on hover & click */

function tooltipShowHideHover(element){
	var dataId = element.children('div').attr('id');
     var isCollapsed= element.find('h4:first').find('a:first').hasClass("collapsed");
     if(isCollapsed) {
    	$('#'+dataId).removeClass("hideShow");
     } else  {
    	 $('#'+dataId).addClass("hideShow");
     }
 };
 function tooltipShowHideClick(element){
	 var dataId = element.children('div').attr('id');
     var isCollapsed= element.find('h4:first').find('a:first').hasClass("collapsed");
     if(isCollapsed) {
    	 $('#'+dataId).addClass("hideShow");
     } else  {
    	 
    	 $('#'+dataId).removeClass("hideShow");
     	}
	 };
	 
 $(".panel-heading").hover(function(){
	 var element = $(this);
	 tooltipShowHideHover(element);
 });
 $(".panel-heading").click(function(){
	 var element = $(this);
	 tooltipShowHideClick(element);
 });

</script>
<script>
$(document).ready(function (){
  new Popper(
		  document.getElementById('usersTitleAdd'),
		  document.getElementById('userTooltip')
			  
			);

  new Popper(
		  document.getElementById('groupTitleAdd'),
		  document.getElementById('groupToolTip')
		 );
    // tooltip
	$('[data-toggle="tooltip"]').tooltip();
	
});
</script>