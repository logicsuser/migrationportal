<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();

	if (strlen($_POST["macAddress"]) > 0)
	{
		require_once("/var/www/lib/broadsoft/login.php");
		$xmlinput = xmlHeader($sessionid, "SystemAccessDeviceGetAllRequest");
		$xmlinput .= "<searchCriteriaDeviceMACAddress>
					<mode>Equal To</mode>
					<value>" . $_POST["macAddress"] . "</value>
					<isCaseInsensitive>true</isCaseInsensitive>
				</searchCriteriaDeviceMACAddress>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		$count = count($xml->command->accessDeviceTable->row);
		if ($count > 0)
		{
			$errorMsg = "MAC Address is already in use.";
		}
		else if (strlen($_POST["macAddress"]) !== 12)
		{
			$errorMsg = "MAC Address must be 12 characters (A-F, 0-9).";
		}
		else if (!preg_match("/^[A-Fa-f0-9]+$/", $value))
		{
			$errorMsg = "MAC Address must only include the characters A-F and 0-9.";
		}
	}
?>
