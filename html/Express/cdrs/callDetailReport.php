<script>

$(function(){
	var sourceSwapImage = function() {
        var thisId = this.id;
        var image = $("#"+thisId).children('img');
        var newSource = image.data('alt-src');
        image.data('alt-src', image.attr('src'));
        image.attr('src', newSource);
    }
    
	$("#downloadCSV").hover(sourceSwapImage, sourceSwapImage);
});
 
    $("#allUsers2").tablesorter();
    $("#downloadCSV").click(function()
    {
        $("#departmentDropdown1").submit();
    });
</script>
<?php
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once("/var/www/lib/broadsoft/adminPortal/getUserRegisterationCdrReport.php");
	if(isset($_POST["from"])){
			$fromDate = $_POST["from"];
		}else{
			$fromDate="";
		}
		if(isset($_POST["to"])){
			$toDate = $_POST["to"];
		}else{
			$toDate="";
	    }
	$obj = new GetUserRegistrationCdrReport();
	$data = $obj->getCDRSummary($fromDate, $toDate);
	$rowHeaders = $obj->getReportHeaders();
	$table="";
	foreach($data as $key => $value){
		$tablerow = "<tr><td class='thsmall'>" . $value[$rowHeaders[0]] . "</td><td class='thsmall'>". $value[$rowHeaders[1]] ."</td><td class='thsmall'>". $value[$rowHeaders[2]] ."</td><td class='thsmall'>". $value[$rowHeaders[3]] ."</td><td class='thsmall'>". $value[$rowHeaders[4]] ."</td><td class='thsmall'>". $value[$rowHeaders[5]] ."</td><td class='thsmall'>".$value[$rowHeaders[6]]."</td></tr>";
	    $table .= $tablerow;
	}
?>

<form action="cdrs/printCSVCallDetail.php" method="POST" name="departmentDropdown1" id="departmentDropdown1">
    <div class="centerDesc">
        <input type='hidden' name='fDate' value='<?php echo $fromDate;?>'/>
        <input type='hidden' name='tDate' value='<?php echo $toDate;?>'/>
    </div>
</form>

<div class="row"style="margin-bottom:8px">
	<div class="col-md-6"></div>
	
	<div class="col-md-6 usersTableButton" style="float:right">
		<div class="col-md-10"></div>
		<!-- <div class="col-md-2 downloadExcel" id=""><img src="images/icons/download_excel.png" id="" /><br><span>Download<br>Excel</span></div> -->
	 	<div class="col-md-1" name="downloadCSV" id="downloadCSV" value="">
	 		<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" onclick="location.href='cdrs/printCSVCallDetail.php';">
	 		<br><span>Download<br>CSV</span>
	 	</div>
	  </div>
</div>

<div class="row">
<div class="viewDetail autoHeight viewDetailNew">
<div style="zoom: 1;">
<table style="width:100%;margin:0;" id="allUsers2" class="scroll tablesorter dataTable">
	<thead>
		  <tr>
			<th class="thsmall header">Phone Number</th>
			<th class="thsmall header"><?php echo $rowHeaders[1]; ?></th>
			<th class="thsmall header"><?php echo $rowHeaders[2]; ?></th>
			<th class="thsmall header"><?php echo $rowHeaders[3]; ?></th>
			<th class="thsmall header"><?php echo $rowHeaders[4]; ?></th>
			<th class="thsmall header"><?php echo $rowHeaders[5]; ?></th>
			<th class="thsmall header"><?php echo $rowHeaders[6]; ?></th>
		  </tr>
	</thead>
	<tbody>
		<?php echo $table; ?>
	</tbody>
</table>
</div>
</div>
</div>
