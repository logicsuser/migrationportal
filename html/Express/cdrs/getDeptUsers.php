<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$xmlinput = xmlHeader($sessionid, "UserGetListInGroupRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<GroupId>" . htmlspecialchars($_SESSION["groupId"]) . "</GroupId>";
	$xmlinput .= "<searchCriteriaExactUserDepartment>
				<departmentKey xsi:type=\"GroupDepartmentKey\">
					<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>
					<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>
					<name>" . $department . "</name>
				</departmentKey>
			</searchCriteriaExactUserDepartment>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$deptUsers = "";
	foreach ($xml->command->userTable->row as $key => $value)
	{
		if (strval($value->col[4]))
		{
			$exp = explode("-", strval($value->col[4]));
			$number = "+1" . $exp[1];
		}
		else
		{
			$exp = explode("x", strval($value->col[0]));
			$number = "+1" . $exp[0];
		}
		$deptUsers .= "'" . $number . "',";
	}
?>
