<?php
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once("/var/www/lib/broadsoft/adminPortal/getUserRegisterationCdrReport.php");
if(isset($_POST["fDate"])){
			$fromDate = $_POST["fDate"];
		}else{
			$fromDate="";
		}
		if(isset($_POST["tDate"])){
			$toDate = $_POST["tDate"];
		}else{
			$toDate="";
	    }

$fileName = $_SESSION["groupId"] ."-CallDetail-". rand(1092918, 9281716);
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment;filename=\"" . $fileName . ".csv\"");
header("Content-Transfer-Encoding: binary");

//require_once("getCdrsSummary.php");
	$message = "Responsible Party, 411, 911, LD, Local, Intl, Toll Free\n";
	$obj = new GetUserRegistrationCdrReport();
	$data = $obj->getCDRSummary($fromDate, $toDate);
	$rowHeaders = $obj->getReportHeaders();
	foreach($data as $key => $value){
		$responsibleParty = $value[$rowHeaders[0]];
		$fOneOne = $value[$rowHeaders[1]];
		$NineOneOne = $value[$rowHeaders[2]];
		$LD = $value[$rowHeaders[3]];
		$Local = $value[$rowHeaders[4]];
		$intl = $value[$rowHeaders[5]];
		$tollFree = $value[$rowHeaders[6]];
		$message .=
        "=\"" . $responsibleParty . "\"," .
        "=\"" . $fOneOne . "\"," .
        "=\"" . $NineOneOne . "\"," .
        "=\"" . $LD . "\"," .
        "=\"" . $Local . "\"," .
        "=\"" . $intl . "\"," .
        "=\"" . $tollFree . "\"\n";
	}


$fp = fopen("php://output", "a");
fwrite($fp, $message);
fclose($fp);
?>
