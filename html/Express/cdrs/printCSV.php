<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$fileName = $_GET["userName"] . " - " . $_SESSION["groupId"] . rand(1092918, 9281716);
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment;filename=\"" . $fileName . ".csv\"");
	header("Content-Transfer-Encoding: binary");

	$query = $_SESSION["query"];
	openssl_error_string();
	$sth = $billDB->query($query);
	$message = "Customer Id,Start Date,Start Time,End Date,End Time,Responsible Party,User,Direction,Answer Indicator,Calling Number,Called Number,Country,Duration (secs)\n";

	//retrieve user names from BroadSoft
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getAllNumberAssignments.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");

	while ($row = $sth->fetch(PDO::FETCH_ASSOC))
	{
		$message .=
			"=\"" . $row["customerId"] . "\"," .
			"=\"" . $row["startDate"] . "\"," .
			"=\"" . $row["startTime"] . "\"," .
			"=\"" . $row["endDate"] . "\"," .
			"=\"" . $row["endTime"] . "\"," .
			"=\"" . $row["responsibleParty"] . "\"," .
			"\"=\"\"" . (isset($userPh[$row["responsibleParty"]]["name"]) ? $userPh[$row["responsibleParty"]]["name"] : "") . "\"\"\"," .
			"=\"" . $row["direction"] . "\"," .
			"=\"" . $row["answerIndicator"] . "\"," .
			"=\"" . $row["callingNumber"] . "\"," .
			"=\"" . $row["calledNumber"] . "\"," .
			"=\"";

		//display country for international calls
		if (strpos($row["calledNumber"], "011") === 0)
		{
			$internationalNumber = substr($row["calledNumber"], 3); //remove "011" from beginning of number
			$intQuery = "select destinationName from internationalCodes where '" . $internationalNumber . "' like concat(countryCode, digits, '%') order by length(digits) desc limit 1";
			$intResults = $db->query($intQuery);
			if ($intRow = $intResults->fetch(MYSQL_ASSOC))
			{
				$message .= $intRow["destinationName"];
			}
			else
			{
				$message .= "N/A";
			}
		}

		$message .= "\"," .
			"=\"" . $row["duration"] . "\"\n";
	}
	$fp = fopen("php://output", "a");
	fwrite($fp, $message);
	fclose($fp);
?>
