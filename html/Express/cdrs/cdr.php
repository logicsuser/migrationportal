<?php
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
?>

<style>
.cdrsFormDiv{width:860px; margin:0 auto;}
div#ui-datepicker-div {
    top: 52px !important;
    left: 15px !important;
}
</style>

<script>
$(document).ready(function() {
    $("input[name='cdrs']").on('change', function() {
        var val = $(this).val();
        $("div.desc").hide();
        $("#CDR" + val).show();
        
        switch(val)
         {
            case '1':
                  //$('#showCDRName').html("CDR Summary");
                  $('#showCDRName').html("Call Detail Report");
                  $('#hideFields').show();
                  break;
            case '2':
                  //$('#showCDRName').html("Call Detail Report");
                  $('#showCDRName').html("CDR Summary");
                  $('#hideFields').hide();
                  break;
        }
    });

	$("#from").datepicker({
		defaultDate: "-4w",
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function(selectedDate) {
			$("#to").datepicker("option", "minDate", selectedDate);
		},
                beforeShow: function(input, obj) {
                    $(input).after($(input).datepicker('widget'));
                }
	});
	$("#to").datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function(selectedDate) {
			$("#from").datepicker("option", "maxDate", selectedDate);
		},
                beforeShow: function(input, obj) {
                    $(input).after($(input).datepicker('widget'));
                }
	});
	
});
$("#allUsers2").tablesorter();
	$(function() {
		$("#module").html("> View CDRs");
		$("#endUserId").html("");

		$("#submit").click(function()
		{
			var cdr1Checked = $('#cdrs1').prop('checked');
		    var cdr2Checked = $('#cdrs2').prop('checked');
		    var url = cdr1Checked ? "cdrs/getCdrs.php":"cdrs/callDetailReport.php";
			$("#cdrResults").html("");
			$("#loading2").show();
			var dataToSend = $("#cdrFilter").serialize();
			$.ajax({
				type: "POST",
				url: url,
				data: dataToSend,
				success: function(result)
				{
					$("#loading2").hide();
					if(cdr1Checked) {
						$("#cdrResultsNew").hide();
						$("#cdrResults").show();
						$("#cdrResults").html(result);
					} else {
						$("#cdrResults").hide();
						$("#cdrResultsNew").show();
						$("#cdrResultsNew").html(result);
					}
				}
			});
		});

		$("#applyDate").click(function()
		{
			if ($("#dateFilter").is(":checked"))
			{
				$("#dateApplied").hide();
				$("#dateFilter").prop("checked", false);
			}
			else
			{
				$("#dateApplied").show();
				$("#dateFilter").prop("checked", true);
			}
			return false;
		});

		$("#applyUser").click(function()
		{
			if ($("#userFilter").is(":checked"))
			{
				$("#userApplied").hide();
				$("#userFilter").prop("checked", false);
			}
			else
			{
				$("#userApplied").show();
				$("#userFilter").prop("checked", true);
			}
			return false;
		});

		$("#applyCalled").click(function()
		{
			if ($("#calledFilter").is(":checked"))
			{
				$("#calledApplied").hide();
				$("#calledFilter").prop("checked", false);
			}
			else
			{
				$("#calledApplied").show();
				$("#calledFilter").prop("checked", true);
			}
			return false;
		});

		$("#applyCalling").click(function()
		{
			if ($("#callingFilter").is(":checked"))
			{
				$("#callingApplied").hide();
				$("#callingFilter").prop("checked", false);
			}
			else
			{
				$("#callingApplied").show();
				$("#callingFilter").prop("checked", true);
			}
			return false;
		});


	});
</script>

<?php
require_once("cdrs/getDnInfo.php");
require_once("/var/www/lib/broadsoft/adminPortal/getDepartments.php");
?>
<div class="selectContainer">
<div class="row cdrsFormDiv">
	 
	<div class="col-md-6" style="padding:0px">	
		<div class="form-group">
			<input type="radio" name="cdrs" id="cdrs1" checked="checked" value="1" />
			<label class="labelText" for="cdrs1"><span></span>Call Detail Report</label>
			<input type="radio" name="cdrs" id="cdrs2" value="2" />
			<label class="labelText" for="cdrs2"><span></span>CDR Summary</label>
		</div>
	</div>
	 
</div>


<div class="row cdrsFormDiv">
	<div class="col-md-12">	
    		<div class="form-group">
				<h2 class="adminUserText" id="showCDRName"> Call Detail Report </h2>
    		</div>
	</div>
</div>

<div class="row cdrsFormDiv">
	<div class="col-md-12">	
		<div class="form-group alignCenter">
			<label class="labelTextGrey"> Filters </label>
		</div>
	</div>
</div>

<form action="#" method="POST" id="cdrFilter" class="cdrsForm fcorn-registerTwo cdrsFormDiv">
	
    <div class="row cdrsFormDiv">
    	<div class="col-md-6">	
    		<div class="form-group">
				<label class="labelText" for="from">From Date</label>
				<input type="text" name="from" id="from" size="12">
    		</div>
		</div>
		
		<div class="col-md-6">	
    		<div class="form-group">
				<label class="labelText" for="to">To Date</label>
				<input type="text" name="to" id="to" size="12">
    		</div>
		</div>
     </div>
	
	
	<div id="hideFields">
	
    	<div class="row cdrsFormDiv">
        	<div class="col-md-6">
            	<div class="form-group">
					<label class="labelText" for="user">User</label>
					<div class="dropdown-wrap">   
            			<select name="user" id="user">
        					<option value=""></option>
                				<?php
                					if (isset($users))
                					{
                						foreach ($users as $key => $value)
                						{
                							echo "<option value=\"" . $value["id"] . "\">" . $value["name"] . "</option>";
                						}
                					}
                				?>
						</select>
					</div>
            	</div>
        	</div>
        	<div class="col-md-6"></div>
             
       	</div>
       	
       	
       	<div class="row cdrsFormDiv">
        	<div class="col-md-6">	
            	<div class="form-group">
            		<label class="labelText" for="calledNum">Called Number</label>
					<input value="" type="text" name="calledNum" id="calledNum" class="form-control date_field">
            	</div>
            </div>
            <div class="col-md-6">	
        		<div class="form-group">
        			<label class="labelText" for="callingNum">Calling Number</label>
					<input value="" type="text" name="callingNum" id="callingNum" class="form-control date_field">
        		</div>
        	</div>
        </div>
       	
       	
       	<div class="row cdrsFormDiv" <?php echo "style= \"display: " . ($useDepartments == "true" ? "block" : "none") . "\""; ?>>
        	<div class="col-md-6">	
        		<div class="form-group">
					<label class="department labelText" for="calledNum">Department</label>
					<div class="dropdown-wrap">   
                		<select name="department" id="department">
                        	<option value=""></option>
                            <?php
                            if (isset($departments))
                            {
                                foreach ($departments as $key => $value)
                                {
                                    echo "<option value=\"" . $value . "\">" . $value . "</option>";
                                }
                            }
                            ?>
    					</select>
					</div>
            	</div>
            </div>
            <div class="col-md-6"></div>
       	</div>
       	
    </div>
        		
    <div class="alignBtn">
    	<input class="subButton" type="button" id="submit" value="Submit">
    </div>
</form>
			<!-- Div Should be end here for hide fields -->

			<!-- <div class="block col-sm-2 col-md-2 col-md-offset-6 col-sm-offset-5"><br>
					<div class="loading" id="loading2">
					<img src="/Express/images/ajax-loader.gif" style="margin-left: -120px;"></div>
				</div> -->


<div class ="desc" id="CDR1">
<div id="cdrResults"></div>
</div>
<div class ="desc" id="CDR2" style="display:none;">
	<div id="cdrResultsNew"></div>
</div>
</div>

