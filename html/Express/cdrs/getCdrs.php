<?php
    require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once("/var/www/lib/broadsoft/login.php");
	 checkLogin();

	if (strlen($_POST["from"]) > 0)
	{
		$from = trim($_POST["from"]);
		$expFrom = explode("/", $from);
		$y = $expFrom[2];
		$m = $expFrom[0];
		$d = $expFrom[1];
		$from = $y . "-" . $m . "-" . $d;
	}
	else
	{
		$from = date("Y-m-d", strtotime("-30 days")); //limit CDRs to the past 30 days
//		$from = "0";
	}
	if (strlen($_POST["to"]) > 0)
	{
		$to = trim($_POST["to"]);
		$expTo = explode("/", $to);
		$y = $expTo[2];
		$m = $expTo[0];
		$d = $expTo[1];
		$to = $y . "-" . $m . "-" . $d;
	}
	else
	{
		$to = date("Y-m-d", strtotime("+1 day"));
//		$to = "0";
	}

	if (strlen($_POST["user"]) > 0)
	{
		$user = trim($_POST["user"]);
	}
	else
	{
		$user = "0";
	}
	if (strlen($_POST["calledNum"]) > 0)
	{
		$called = trim($_POST["calledNum"]);
	}
	else
	{
		$called = "0";
	}
	if (strlen($_POST["callingNum"]) > 0)
	{
		$calling = trim($_POST["callingNum"]);
	}
	else
	{
		$calling = "0";
	}
	if (strlen($_POST["department"]) > 0)
	{
		$department = trim($_POST["department"]);
	}
	else
	{
		$department = "0";
	}
?>
<script>
	$(function()
	{
		$("#callDetail").dialog(
		{
			autoOpen: false,
			width: 1400,
			modal: true,
			resizable: false,
			position: { my: "top", at: "top" },
			closeOnEscape: true,
			resizable: false,
			open: function(event) {
				setDialogDayNightMode($(this));
        		$('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('closeButton');
        	},
        	buttons: {
        		"Close": function()
        		{
        			$(this).dialog("close");
        		}

        	}
		});

		$("#summary tr, #summary2 td").hover(function() {
//			$(this).addClass("sumOn");
		}, function() {
//			$(this).removeClass("sumOn");
		});

		$("#summary a, #summary2 a").click(function() {
			$.ui.dialog.prototype._focusTabbable = function(){};		/*revmove autofocus from dialog buttons.*/
			$("#callDetail").dialog("open");
			$("#callDetail").html("<div class=\"loading\" id=\"loading3\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
			$("#loading3").show();
			var from = "<?php echo $from; ?>";
			var to = "<?php echo $to; ?>";
			var called = "<?php echo $called; ?>";
			var calling = "<?php echo $calling; ?>";
			var department = "<?php echo $department; ?>";
			var user = "<?php echo $user; ?>";
			var userId = $(this).attr("id");
			var filter = $(this).attr("id");

			$.ajax(
			{
				type: "POST",
				url: "cdrs/callDetails.php",
				data: { userId: userId, from: from, to: to, called: called, calling: calling, department: department, user: user, filter: filter },
				success: function(result)
				{
					$("#loading3").hide();
			 		$("#callDetail").html(result);
				}
			});
		});

		$("#summary").tablesorter();
	});
</script>
<?php
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getAllNumberAssignments.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");

	$query = "SELECT * from broadsoft_cdrs where BINARY customerId='" . $_SESSION["groupId"] . "'";
	if ($department !== "0")
	{
		require_once("getDeptUsers.php");
		$query .= " AND responsibleParty in (" . ($deptUsers != "" ? rtrim($deptUsers, ",") : "NULL") . ")";
	}
	if ($user !== "0")
	{
		$query .= " AND responsibleParty='" . $user . "'";
	}
	if ($from !== "0")
	{
		$query .= " AND startDate>='" . $from . "'";
	}
	if ($to !== "0")
	{
		$query .= " AND startDate<='" . $to . "'";
	}
	if ($called !== "0")
	{
		$query .= " AND calledNumber like '%" . $called . "%'";
	}
	if ($calling !== "0")
	{
		$query .= " AND callingNumber like '%" . $calling . "%'";
	}
	$query .= " order by startDate desc";
	$_SESSION["query"] = $query;
//echo $query;
	openssl_error_string();
	$sth = $billDB->query($query);

	$totalInbound = 0;
	$ibCount = 0;
	$totalOutbound = 0;
	$obCount = 0;
	$totalOutboundExternal = 0;
	$obeCount = 0;
	$totalInboundOutbound = 0;
	$ibobCount = 0;
	$totalIntl = 0;
	$intCount = 0;
	$total411 = 0;
	$total411Count = 0;
	$totalTollFree = 0;
	$tfCount = 0;
	$total911 = 0;
	$total911Count = 0;
	$a = 0;
	while ($row = $sth->fetch(PDO::FETCH_ASSOC))
	{
		//All inbound
		if ($row["direction"] == "Terminating")
		{
			$totalInbound += $row["duration"];
			$ibCount++;

			$totalInboundOutbound += $row["duration"];
			$ibobCount++;
		}

		//All outbound
		if ($row["direction"] == "Originating")
		{
			$totalOutbound += $row["duration"];
			$obCount++;

			$totalInboundOutbound += $row["duration"];
			$ibobCount++;

			//run separate count of external outbound calls for Rhema Bible Church
			if ($_SESSION["groupId"] == "Rhema Bible Church")
			{
				//check if any calls are toll free
				$tollFreeFlag = 0;
				foreach ($tollFree as $key => $value)
				{
					if (substr($row["calledNumber"], 0, strlen($value)) === $value)
					{
						$tollFreeFlag = 1;
					}
				}

				//check if any calls are to assigned numbers
				$numberFlag = 0;
				foreach ($numberAssignments as $key => $value)
				{
					$key = explode("-", $key);
					$key = $key[1];
					if (substr($row["calledNumber"], -strlen($key)) === $key)
					{
						$numberFlag = 1;
					}
				}

				if ($row["calledNumber"] !== "pingtone.com" and strlen($row["calledNumber"]) >= 10 and substr($row["calledNumber"], 0, 3) !== "011" and $row["answerIndicator"] !== "No" and $tollFreeFlag == 0 and $numberFlag == 0)
				{
					$totalOutboundExternal += $row["duration"];
					$obeCount++;
				}
			}
		}

		//All outbound International
		if (substr($row["calledNumber"], 0, 3) === "011" and $row["direction"] == "Originating")
		{
			$totalIntl += $row["duration"];
			$intCount++;
		}

		//All 411 calls
		if ($row["calledNumber"] === "411")
		{
			$total411 += $row["duration"];
			$total411Count++;
		}

		//substring for checking 800's
		$first3 = substr($row["calledNumber"], 0, 3);
		$first4 = substr($row["calledNumber"], 0, 4);
		$first5 = substr($row["calledNumber"], 0, 5);

		if (in_array($first3, $tollFree) and $row["direction"] == "Terminating")
		{
			$totalTollFree += $row["duration"];
			$tfCount++;
		}
		if (in_array($first4, $tollFree) and $row["direction"] == "Terminating")
		{
			$totalTollFree += $row["duration"];
			$tfCount++;
		}
		if (in_array($first5, $tollFree) and $row["direction"] == "Terminating")
		{
			$totalTollFree += $row["duration"];
			$tfCount++;
		}

		//All emergency calls
		if ($row["calledNumber"] === "911")
		{
			$total911 += $row["duration"];
			$total911Count++;
		}

		$resp = $row["responsibleParty"];
		$resp = $resp . ":" . (isset($userPh[$resp]["name"]) ? $userPh[$resp]["name"] : "");

		if ($row["direction"] == "Terminating")
		{
			if (!isset($calls[$resp]["totalInboundCount"]))
			{
				$calls[$resp]["totalInboundCount"] = 0;
			}
			$calls[$resp]["totalInboundCount"]++;
			if (!isset($calls[$resp]["totalInboundMinutes"]))
			{
				$calls[$resp]["totalInboundMinutes"] = 0;
			}
			$calls[$resp]["totalInboundMinutes"] += $row["duration"];
		}
		if ($row["direction"] == "Originating")
		{
			if (!isset($calls[$resp]["totalOutboundCount"]))
			{
				$calls[$resp]["totalOutboundCount"] = 0;
			}
			$calls[$resp]["totalOutboundCount"]++;
			if (!isset($calls[$resp]["totalOutboundMinutes"]))
			{
				$calls[$resp]["totalOutboundMinutes"] = 0;
			}
			$calls[$resp]["totalOutboundMinutes"] += $row["duration"];
		}
	}
?>

<div class="row">
	<div class="col-md-4">
	<div class="viewDetail autoHeight viewDetailNew" style="width:100%!important;">
	<div style="zoom: 1;">
	
			<!--<table id="summary2" style="width:100%;margin:0;" class="scroll tablesorter dataTable">-->
			<table id="summary2" style="width:100%;margin:0;" class="scroll dataTable display table table-striped table-bordered">
			
				<thead><th class="thsmall">Totals for Filters Applied</th></thead>
				
				<tr>
					<th class="thClassCDR">Total Outbound</th>
					<td class="thClassCDR"><a href="#" id="outbound"><?php echo $obCount; ?> calls, <?php echo floor($totalOutbound/60) . ":" . str_pad($totalOutbound%60, 2, "0", STR_PAD_LEFT); ?> Minutes</a></td>
				</tr>
				<?php
					if ($_SESSION["groupId"] == "Rhema Bible Church")
					{
						?>
						<tr>
							<th class="thClassCDR">Total Outbound (External Only)</th>
							<td class="thClassCDR"><a href="#" id="outboundExternal"><?php echo $obeCount; ?> calls, <?php echo floor($totalOutboundExternal/60) . ":" . str_pad($totalOutboundExternal%60, 2, "0", STR_PAD_LEFT); ?> Minutes</a></td>
						</tr>
						<?php
					}
				?>
				<tr>
					<th class="thClassCDR">Total Inbound</th>
					<td class="thClassCDR"><a href="#" id="inbound"><?php echo $ibCount; ?> calls, <?php echo floor($totalInbound/60) . ":" . str_pad($totalInbound%60, 2, "0", STR_PAD_LEFT); ?> Minutes</a></td>
				</tr>
				<tr>
					<th class="thClassCDR">Total Outbound + Inbound</th>
					<td class="thClassCDR"><a href="#" id="inboundoutbound"><?php echo $ibobCount; ?> calls, <?php echo floor($totalInboundOutbound/60) . ":" . str_pad($totalInboundOutbound%60, 2, "0", STR_PAD_LEFT); ?> Minutes</a></td>
				</tr>
				<tr>
					<th class="thClassCDR">Total International</th>
					<td class="thClassCDR"><a href="#" id="international"><?php echo $intCount; ?> calls, <?php echo floor($totalIntl/60) . ":" . str_pad($totalIntl%60, 2, "0", STR_PAD_LEFT); ?> Minutes</a></td>
				</tr>
				<tr>
					<th class="thClassCDR">Total Toll Free</th>
					<td class="thClassCDR"><a href="#" id="tollFree"><?php echo $tfCount; ?> calls, <?php echo floor($totalTollFree/60) . ":" . str_pad($totalTollFree%60, 2, "0", STR_PAD_LEFT); ?> Minutes</a></td>
				</tr>
				<tr>
					<th class="thClassCDR">Total Dir Asst</th>
					<td class="thClassCDR"><a href="#" id="dirAsst"><?php echo $total411Count; ?> calls, <?php echo floor($total411/60) . ":" . str_pad($total411%60, 2, "0", STR_PAD_LEFT); ?> Minutes</a></td>
				</tr>
				<tr>
					<th class="thClassCDR">Total Emergency Asst</th>
					<td class="thClassCDR"><a href="#" id="emergencyAsst"><?php echo $total911Count; ?> calls, <?php echo floor($total911/60) . ":" . str_pad($total911%60, 2, "0", STR_PAD_LEFT); ?> Minutes</a></td>
				</tr>
			</table>
			</div>
			</div>
		</div>
		<div class="col-md-8">
		<div class="viewDetail autoHeight viewDetailNew" style="width:100%!important;">
		<div style="zoom: 1;">
				<table name="summary" id="summary" class="tablesorter scroll tablesorter dataTable" style="width:100%;margin:0;">
					<thead>
						<tr>
						<th class="thsmall">User</th>
						<th class="thsmall">Inbound Count</th>
						<th class="thsmall">Inbound Minutes</th>
						<th class="thsmall">Outbound Count</th>
						<th class="thsmall">Outbound Minutes</th>
						</tr>
					</thead>
					<tbody>
						
						<?php
							if (isset($calls))
							{
								foreach ($calls as $key => $value)
								{
									if (!isset($value["totalInboundCount"]))
									{
										$value["totalInboundCount"] = 0;
									}
									if (!isset($value["totalInboundMinutes"]))
									{
										$value["totalInboundMinutes"] = 0;
									}
									if (!isset($value["totalOutboundCount"]))
									{
										$value["totalOutboundCount"] = 0;
									}
									if (!isset($value["totalOutboundMinutes"]))
									{
										$value["totalOutboundMinutes"] = 0;
									}
									$exp = explode(":", $key);
									$id = $exp[0];
									$key = $exp[1];
									echo "<tr>";
									if (strlen($key) > 1)
									{
										echo "<td id=\"" . $key . "\" class=\"callSums thsmall\"><a href=\"#\" id=\"" . $id . ":" . $key . "\">" . $key . "</a></td>";
									}
									else
									{
										echo "<td id=\"" . $id . "\" class=\"callSums thsmall\"><a href=\"#\" id=\"" . $id . "\">" . $id . "</a></td>";
									}
									echo "<td class=\"callSums thsmall\">" . $value["totalInboundCount"] . "</td>";
									echo "<td class=\"callSums thsmall\">" . floor($value["totalInboundMinutes"]/60) . ":" . str_pad($value["totalInboundMinutes"]%60, 2, "0", STR_PAD_LEFT) . "</td>";
									echo "<td class=\"callSums thsmall\">" . $value["totalOutboundCount"] . "</td>";
									echo "<td class=\"callSums thsmall\">" . floor($value["totalOutboundMinutes"]/60) . ":" . str_pad($value["totalOutboundMinutes"]%60, 2, "0", STR_PAD_LEFT) . "</td>";
									echo "</tr>";
								}
							}
						?>
					</tbody>
				</table>
		</div>
		</div>
		</div>
</div>
<div id="callDetail"></div>
