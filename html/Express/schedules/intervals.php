<?php
    //error_reporting(0);
	require_once("/var/www/html/Express/config.php");
	checkLogin();

	$dayOfWeekInMonthArray = array("First", "Second", "Third", "Fourth", "Last");
	$dayOfWeekArray = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
	$monthArray = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
?>
<script src="/Express/js/timePicker.js"></script>
<script src="/Express/js/timepickerRange.js"></script>
<script>
	$(function() {
		function changeStartDate()
		{
			if ($("#dateStart").val())
			{
				$("#startDate").html("<label class=\"labelTextGrey\">" + $("#dateStart").val() + " (Note: Start Date is always equal to Event Time Start Date value)</label>");
			}
			else
			{
				$("#startDate").html("");
			}
		}

		$("#dateStart").change(function()
		{
			changeStartDate();
		});

		changeStartDate();

		<?php
			if ($_POST["intervalValue"] == "Monthly")
			{
				?>
				function changeRecurMonthly()
				{
					if (!$("#recurMonthlyDay").is(":checked"))
					{
						$("#dayOfMonth").val("");
						$("#dayOfMonth").attr("disabled", "disabled");
					}
					else
					{
						$("#dayOfMonth").removeAttr("disabled");
					}
					if (!$("#recurMonthlyWeek").is(":checked"))
					{
						$("#dayOfWeekInMonth").get(0).selectedIndex = 0;
						$("#dayOfWeekInMonth").attr("disabled", "disabled");
						$("#dayOfWeek").get(0).selectedIndex = 0;
						$("#dayOfWeek").attr("disabled", "disabled");
					}
					else
					{
						$("#dayOfWeekInMonth").removeAttr("disabled");
						$("#dayOfWeek").removeAttr("disabled");
					}
				}

				$("#recurMonthlyDay").click(function()
				{
					changeRecurMonthly();
				});

				$("#recurMonthlyWeek").click(function()
				{
					changeRecurMonthly();
				});

				changeRecurMonthly();
				<?php
			}

			if ($_POST["intervalValue"] == "Yearly")
			{
				?>
				function changeRecurYearly()
				{
					if (!$("#recurYearlyDay").is(":checked"))
					{
						$("#dayOfMonthInYear").val("");
						$("#dayOfMonthInYear").attr("disabled", "disabled");
						$("#byDayMonth").get(0).selectedIndex = 0;
						$("#byDayMonth").attr("disabled", "disabled");
					}
					else
					{
						$("#dayOfMonthInYear").removeAttr("disabled");
						$("#byDayMonth").removeAttr("disabled");
					}
					if (!$("#recurYearlyWeek").is(":checked"))
					{
						$("#dayOfWeekInMonthInYear").get(0).selectedIndex = 0;
						$("#dayOfWeekInMonthInYear").attr("disabled", "disabled");
						$("#dayOfWeekInYear").get(0).selectedIndex = 0;
						$("#dayOfWeekInYear").attr("disabled", "disabled");
						$("#byWeekMonth").get(0).selectedIndex = 0;
						$("#byWeekMonth").attr("disabled", "disabled");
					}
					else
					{
						$("#dayOfWeekInMonthInYear").removeAttr("disabled");
						$("#dayOfWeekInYear").removeAttr("disabled");
						$("#byWeekMonth").removeAttr("disabled");
					}
				}

				$("#recurYearlyDay").click(function()
				{
					changeRecurYearly();
				});

				$("#recurYearlyWeek").click(function()
				{
					changeRecurYearly();
				});

				changeRecurYearly();
				<?php
			}
		?>

		function changeRecurEnd()
		{
			if (!$("#recurEndA").is(":checked"))
			{
				$("#recurEndOccurrence").val("");
				$("#recurEndOccurrence").attr("disabled", "disabled");
			}
			else
			{
				$("#recurEndOccurrence").removeAttr("disabled");
			}
			if (!$("#recurEndD").is(":checked"))
			{
				$("#recurEndDate").val("");
				$("#recurEndDate").attr("disabled", "disabled");
			}
			else
			{
				$("#recurEndDate").removeAttr("disabled");
			}
		}

		$("#recurEndN").click(function()
		{
			changeRecurEnd();
		});

		$("#recurEndA").click(function()
		{
			changeRecurEnd();
		});

		$("#recurEndD").click(function()
		{
			changeRecurEnd();
		});

		changeRecurEnd();
	});
</script>
 <div class="row">
        <div class="col-md-12">
        	<div class="col-md-6">
        		<div class="form-group"> 
        			<label for="recurrenceInterval" class="labelText">Every :</label>
        			<input type="text" name="recurrenceInterval" id="recurrenceInterval" value="<?php echo $_SESSION["event"]["recurInterval"] ? $_SESSION["event"]["recurInterval"] : "1"; ?>" size="3" maxlength="<?php echo ($_POST["intervalValue"] == "Monthly" or $_POST["intervalValue"] == "Yearly") ? "2" : "3"; ?>" />
        		</div>
        	</div>
        </div>
    </div>

    <div class="row">
    	<div class="col-md-12">
    		<div class="col-md-6"> 
        		<div class="form-group">
                    <?php
                    if ($_POST["intervalValue"] == "Daily")
                    {
                    	?>
                    <label class='labelTextGrey'>day(s)</label>
                    <?php
                    }
                    if ($_POST["intervalValue"] == "Weekly")
                    {
                    	?>
                    <label class="labelText" >Week(s) on :</label>
                    <style>
                    .weekscheck li { display: inline-block; text-align: center; margin: 0 auto; padding: 10px 30px; }
                    </style>
                    <?php } ?>
        		</div>
        	</div>
        </div>
    </div>    
    <div class="row">
     <div class="col-md-12"> 
        <div class="form-group">
                  
    		<ul class="weekscheck">
    			<li>
    				<input type="hidden" name="weeklyIntervalSunday" value="False" />
				<input type="checkbox" name="weeklyIntervalSunday" id="weeklyIntervalSunday" value="True"<?php echo $_SESSION["event"]["sunday"] == "true" ? " checked" : ""; ?> />
    				<label for="weeklyIntervalSunday"><span></span></label>
    				<label class="labelText" for="weeklyIntervalSunday">Sunday</label>
    			</li>
    			<li>
    				<input type="hidden" name="weeklyIntervalMonday" value="False" />
				<input type="checkbox" name="weeklyIntervalMonday" id="weeklyIntervalMonday" value="True"<?php echo $_SESSION["event"]["monday"] == "true" ? " checked" : ""; ?> />
    				<label for="weeklyIntervalMonday"><span></span></label>
    				<label class="labelText" for="weeklyIntervalMonday">Monday</label>
    			</li>
    			<li>
    				<input type="hidden" name="weeklyIntervalTuesday" value="False" />
				<input type="checkbox" name="weeklyIntervalTuesday" id="weeklyIntervalTuesday" value="True"<?php echo $_SESSION["event"]["tuesday"] == "true" ? " checked" : ""; ?> />
    				<label for="weeklyIntervalTuesday"><span></span></label>
    				<label class="labelText" for="weeklyIntervalTuesday">Tuesday</label>
    			</li>
    			<li>
    				<input type="hidden" name="weeklyIntervalWednesday" value="False" />
				<input type="checkbox" name="weeklyIntervalWednesday" id="weeklyIntervalWednesday" value="True"<?php echo $_SESSION["event"]["wednesday"] == "true" ? " checked" : ""; ?> />
    				<label for="weeklyIntervalWednesday"><span></span></label>
    				<label class="labelText" for="weeklyIntervalWednesday">Wednesday</label>
    			</li>
    			<li>
    				<input type="hidden" name="weeklyIntervalThursday" value="False" />
				<input type="checkbox" name="weeklyIntervalThursday" id="weeklyIntervalThursday" value="True"<?php echo $_SESSION["event"]["thursday"] == "true" ? " checked" : ""; ?> />
    				<label for="weeklyIntervalThursday"><span></span></label>
    				<label class="labelText" for="weeklyIntervalThursday">Thursday</label>
    			</li>
    			<li>
    				<input type="hidden" name="weeklyIntervalFriday" value="False" />
				<input type="checkbox" name="weeklyIntervalFriday" id="weeklyIntervalFriday" value="True"<?php echo $_SESSION["event"]["friday"] == "true" ? " checked" : ""; ?> />
    				<label for="weeklyIntervalFriday"><span></span></label>
    				<label class="labelText" for="weeklyIntervalFriday">Friday</label>
    			</li>
    			<li>
    				<input type="hidden" name="weeklyIntervalSaturday" value="False" />
				<input type="checkbox" name="weeklyIntervalSaturday" id="weeklyIntervalSaturday" value="True"<?php echo $_SESSION["event"]["saturday"] == "true" ? " checked" : ""; ?> />
    				<label for="weeklyIntervalSaturday"><span></span></label>
    				<label class="labelText" for="weeklyIntervalSaturday">Saturday</label>
    			</li>
    		</ul>
    	
    	</div>
     </div>
    </div>
		<?php
	
	if ($_POST["intervalValue"] == "Monthly")
	{
		?>
		
	<div class="row">
     	<div class="col-md-12">
     		<div class="col-md-6"> 
            	<div class="form-group">
            		<label class="labelText">Month(s) on:</label>
            		<input type="radio" name="recurMonthly" id="recurMonthlyDay" value="ByDay" checked />
            		<label for="recurMonthlyDay"><span></span></label>
            		<label class="labelText">Day</label>
					<input type="text" name="dayOfMonth" id="dayOfMonth" size="2" maxlength="2" value="<?php echo $_SESSION["event"]["dayOfMonth"]; ?>" />
					<label class="labelText"> Of the month</label>
            	</div>
            </div>
            
            <div class="col-md-6"> 
            	<div class="form-group">
            		<input  type="radio" name="recurMonthly" id="recurMonthlyWeek" value="ByWeek"<?php echo $_SESSION["event"]["recurMonthly"] == "ByWeek" ? " checked" : ""; ?> />
            			<label for="recurMonthlyWeek"><span></span></label>
						<label class="labelText"> The </label>
						<div class="dropdown-wrap">
            			<select name="dayOfWeekInMonth" id="dayOfWeekInMonth">
    					<?php
    						foreach ($dayOfWeekInMonthArray as $key => $value)
    						{
    							echo "<option value=\"" . $value . "\"";
    							if ($value == $_SESSION["event"]["dayOfWeekInMonth"])
    							{
    								echo " selected";
    							}
    							echo ">" . $value . "</option>";
    						}
    					?>
						</select>
						</div>
            	</div>
            </div>
            
        </div>
     </div>
		
	
	<div class="row">
     	<div class="col-md-12">
     		<div class="col-md-6"> 
            	<div class="form-group">
            		&nbsp
            	</div>
            </div>
            
           <div class="col-md-6"> 
            	<div class="form-group">
					<label class=" labelText">Of the month :</label>
					<div class="dropdown-wrap">
            		<select name="dayOfWeek" id="dayOfWeek">
    					<?php
    						foreach ($dayOfWeekArray as $key => $value)
    						{
    							echo "<option value=\"" . $value . "\"";
    							if ($value == $_SESSION["event"]["dayOfWeek"])
    							{
    								echo " selected";
    							}
    							echo ">" . $value . "</option>";
    						}
    					?>
					</select>
						</div>
            	</div>
            </div>
            
        </div>
     </div>
     	 
		<?php
	}
	
	if ($_POST["intervalValue"] == "Yearly")
	{
	?>
	
		<div class="row">
     	  <div class="col-md-12">
     		<div class="col-md-6"> 
              <div class="form-group">
            		<label class="labelText">Year(s) On:</label>
            		<input type="radio" name="recurYearly" id="recurYearlyDay" value="ByDay" checked />
        			<label for="recurYearlyDay"><span></span></label>
        			<label class="labelText"> Day </label>
        			<input type="text" name="dayOfMonthInYear" id="dayOfMonthInYear" size="2" maxlength="2" value="<?php echo $_SESSION["event"]["dayOfMonthInYear"]; ?>" />
               </div>
            </div>
            
            <div class="col-md-6"> 
              <div class="form-group">
				<label class=" labelText">Of :</label>
				<div class="dropdown-wrap">
            	<select name="byDayMonth" id="byDayMonth">
    				<?php
    					foreach ($monthArray as $key => $value)
    					{
    						echo "<option value=\"" . $value . "\"";
    						if ($value == $_SESSION["event"]["byDayMonth"])
    						{
    							echo " selected";
    						}
    						echo ">" . $value . "</option>";
    					}
    				?>
				</select>
					</div>
              </div>
            </div>
          </div>
        </div>

		<div class="row">
     	  <div class="col-md-12">
     		<div class="col-md-6"> 
              <div class="form-group">
            		<label class=" labelText">Or The :</label>
               		<input type="radio" name="recurYearly" id="recurYearlyWeek" value="ByWeek"<?php echo $_SESSION["event"]["recurYearly"] == "ByWeek" ? " checked" : ""; ?> />
					<label for="recurYearlyWeek"><span></span></label>
					<div class="dropdown-wrap">
        			<select name="dayOfWeekInMonthInYear" id="dayOfWeekInMonthInYear">
        				<?php
        					foreach ($dayOfWeekInMonthArray as $key => $value)
        					{
        						echo "<option value=\"" . $value . "\"";
        						if ($value == $_SESSION["event"]["dayOfWeekInMonthInYear"])
        						{
        							echo " selected";
        						}
        						echo ">" . $value . "</option>";
        					}
        				?>
					</select>
						</div>
               </div>
            </div>
            
            <div class="col-md-6"> 
              <div class="form-group">
			  <div class="dropdown-wrap">
            	<select name="dayOfWeekInYear" id="dayOfWeekInYear">
				<?php
					foreach ($dayOfWeekArray as $key => $value)
					{
						echo "<option value=\"" . $value . "\"";
						if ($value == $_SESSION["event"]["dayOfWeekInYear"])
						{
							echo " selected";
						}
						echo ">" . $value . "</option>";
					}
				?>
    			</select></div> of
    			<div class="dropdown-wrap"><select name="byWeekMonth" id="byWeekMonth">
    				<?php
    					foreach ($monthArray as $key => $value)
    					{
    						echo "<option value=\"" . $value . "\"";
    						if ($value == $_SESSION["event"]["byWeekMonth"])
    						{
    							echo " selected";
    						}
    						echo ">" . $value . "</option>";
    					}
    				?>
    			</select></div>
    			<?php
    		}
    	?>
              </div>
            </div>
          </div>
        </div>
                
<div class="row">
  <div class="col-md-12">
	 <div class="col-md-6"> 
       <div class="form-group">
      		<label class="labelText">Start Date :</label>
      		<div class="" id="startDate"></div>
  	   </div>
  	 </div>
  	 
  	  <div class="col-md-6"> 
       <div class="form-group">
      		<label class=" labelText">End :</label>
  	   </div>
  	 </div>
  </div>
</div>
        
<div class="row">
  <div class="col-md-12">
	 <div class="col-md-6"> 
       <div class="form-group">
      		<input type="radio" name="recurEnd" id="recurEndN" value="Never" checked />
      		<label for="recurEndN"><span></span></label>
      		<label for="recurEndN" class="labelText">Never :</label>
  	   </div>
  	 </div>
  	 
  	  <div class="col-md-6"> 
       <div class="form-group">
      		<input type="radio" name="recurEnd" id="recurEndA" value="After"<?php echo $_SESSION["event"]["recurrenceEnd"] == "After" ? " checked" : ""; ?> />
  	   		<label for="recurEndA"><span></span></label>
  	   		<label for="recurEndA" class=" labelText">After :</label>
  	   </div>
  	 </div>
  	 
  </div>
</div>

<div class="row">
    <div class="col-md-12">
    	<div class="col-md-6">
           <div class="form-group">
           		&nbsp
           </div>
        </div>
        
        <div class="col-md-6">
           <div class="form-group">
           		<input type="text" name="recurEndOccurrence" id="recurEndOccurrence" size="3" maxlength="3" value="<?php echo $_SESSION["event"]["recurEndOccurrence"]; ?>" />
           		<label for="recurEndOccurrence"><span></span></label>
           		<label class=" labelText" for="recurEndOccurrence">occurrences</label>
           </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
           <div class="form-group">
          		<input type="radio" name="recurEnd" id="recurEndD" value="Date"<?php echo $_SESSION["event"]["recurrenceEnd"] == "Date" ? " checked" : ""; ?> />
           		<label for="recurEndD"><span></span></label>
           		<label class="labelText" for="recurEndD">Date</label>
           </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
           <div class="form-group">
           		<div class="datepair">
					<input type="text" class="date" name="recurEndDate" id="recurEndDate" size="10" maxlength="10" value="<?php echo $_SESSION["event"]["recurEndDate"]; ?>" />           		
           		</div>
           </div>
        </div>
    </div>
</div>