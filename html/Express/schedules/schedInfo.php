<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$exp = explode(":", $_POST["schedValue"]);
	$schedName = $exp[1];
	$schedType = $exp[0];

	require_once("/var/www/html/Express/schedules/editSchedule.php");
?>
<script>
	$("#eventChoice").change(function()
	{
		$("#eventData").html("");
		var eventName = $("#eventChoice").val();
		var schedName = "<?php echo $schedName; ?>";
		var schedType = "<?php echo $schedType; ?>";
		if (eventName !== "")
		{
			$.ajax({
				type: "POST",
				url: "schedules/editEvent.php",
				data: { eventName: eventName, schedName: schedName, schedType: schedType },
				success: function(result) {
					$("#eventData").html(result);
				}
			});
		}
	});
</script>
<div class="fcorn-registerTwo">
<h2 class="adminUserText" style="margin:50px 0 0px !important;">Choose Event to Modify</h2>

	<div class="row">
		<div class="col-md-12"> 
			<div class="form-group"> 
				<!-- row space  -->
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12"> 
			<div class="col-md-6"> 
                <div class="form-group">
					<label for="eventChoice" class="newSchedType labelText">Event :</label><br>
					<div class="dropdown-wrap">
                    	<select name="eventChoice" id="eventChoice" class="widthHP" style="width:100% !important;">
                		<option value=""></option>
                		<?php
                			$xmlinput = xmlHeader($sessionid, "GroupScheduleGetEventListRequest");
                			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
                			$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
                			$xmlinput .= "<scheduleKey>
                						<scheduleName>" . htmlspecialchars($schedName) . "</scheduleName>
                						<scheduleType>" . $schedType . "</scheduleType>
                					</scheduleKey>";
                			$xmlinput .= xmlFooter();
                			$response = $client->processOCIMessage(array("in0" => $xmlinput));
                			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                
                			$a = 0;
                			foreach ($xml->command->eventName as $key => $value)
                			{
                				$events[$a] = strval($value);
                				$a++;
                			}
                			if (isset($events))
                			{
                				$events = subval_sort($events);
                
                				foreach ($events as $key => $value)
                				{
                					echo "<option value=\"" . $schedName . ":" . $schedType . ":" . $value . "\">" . $value . "</option>";
                				}
                			}
                		?>
                		<option value="newEvent">--Create New Event--</option>
						</select>
					</div>
                </div>
			</div>
		</div>
	</div>
</div>