<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();
?>
<script>
	var buttonClick = "";

	$(function()
	{
		var schedName = "<?php echo isset($schedName) ? $schedName : ""; ?>";

		if (schedName == "")
		{
			$("#endUserId").html("New Schedule");
		}
		else
		{
			$("#endUserId").html(schedName);
		}

		$("#dialogSC").dialog({
			autoOpen: false,
			width: 800,
			modal: true,
			position: { my: "top", at: "top" },
			resizable: false,
			closeOnEscape: false,
			buttons: {
				"Complete": function() {
					var processAction = (buttonClick == "subButtonEdit") ? "Build Schedule" : "Delete Schedule";
					pendingProcess.push(processAction);
					var dataToSend = $("form#schedEdit").serializeArray();
					$('#newScheduleName').val('');
					$.ajax({
						type: "POST",
						url: (buttonClick == "subButtonEdit" ? "schedules/buildSchedule.php" : "schedules/deleteSchedule.php"),
						data: dataToSend,
                                                beforeSend: function(){
                                                    $("html, body").animate({ scrollTop: 0 }, 0); //Code added @11 Feb 2019
                                                },
						success: function(result) {
							if(foundServerConErrorOnProcess(result, processAction)) {
		    					return false;
		                  	}
							var afterColon = result.substr(result.indexOf("-") + 1);
							myStr = afterColon.split("<")[0];
							var deletedVar = result.search("has been deleted");
							//$('#chag_sort').val(sort2);
							$('#newScheduleName').val(myStr);
							$("#dialogSC").dialog("option", "title", "Request Complete");
							//$(".ui-dialog-titlebar-close", this.parentNode).hide();
							//$(".ui-dialog-buttonpane", this.parentNode).hide();
							$("#dialogSC").html(result);
							//$("#dialogSC").append(returnLink);
							if(buttonClick == 'subButtonEdit'){

								$(".ui-dialog-buttonpane button:contains('More Changes')").button().show().addClass('moreChangesButton');
							}
							else if(buttonClick == 'subButtonDel'){
								$(".ui-dialog-buttonpane button:contains('More Changes')").button().show().addClass('moreChangesButton');
							}
							else{
								$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
							}
							
							$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
							$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
							$(".ui-dialog-buttonpane button:contains('Return to Main')").button().show().addClass('returnToMainButton');
						}
					});
				},
				"More Changes": function() {
					$(this).dialog("close");
					scheduleChangeFun();
				},
				"Return to Main": function() {
					location.href="main.php";
				},
				"Cancel": function() {
					$(this).dialog("close");
				}
			},
			open: function() {
				setDialogDayNightMode($(this));
				$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
				$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('completeButton');
				$('.ui-dialog-buttonpane').find('button:contains("Return to Main")').addClass('returnToMainButton');			 
				$('.ui-dialog-buttonpane').find('button:contains("More Changes")').addClass('moreChangesButton');			 
            }
		});

		$("#subButtonEdit").click(function()
		{
			buttonClick = this.id;
			var dataToSend = $("form#schedEdit").serializeArray();
			$.ajax({
				type: "POST",
				url: "schedules/checkData.php",
				data: dataToSend,
                                beforeSend: function(){
                                    $("html, body").animate({ scrollTop: 0 }, 0); //Code added @11 Feb 2019
                                },
				success: function(result) {
					$("#dialogSC").dialog("open");
					$("#dialogSC").dialog("option", "title", "Confirm Setting");
					if (result.slice(0, 1) == "1")
					{
						$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
					}
					else
					{
						$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
					}
					$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
					result = result.slice(1);
					$("#dialogSC").html(result);
				}
			});
		});

		$("#subButtonDel").click(function()
		{
			buttonClick = this.id;
			$("#dialogSC").dialog("open");
			$("#dialogSC").html("Are you sure you want to delete schedule \"" + schedName + "\"?");
			$(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide();
			$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
			$(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');
			$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
		});
		
		var scheduleChangeFun = function(){	
			  var module = 'modSched';
       			var schedNameToSend = $('#newScheduleName').val();

       			$.ajax({
       				type: "POST",
       				url: "navigate.php",
       				data: { module : module, schedNameToSend : schedNameToSend},
       				success: function(result)
       				{
           				$('#scheduleDetailsId').html('');
       					$('#schedData').html('');
       					$("#ScheduleData").html("");
       					$("#scheduleDetailsId").html(result);
       				}
       			});
		}
	});
</script>

<h2 class="adminUserText"><?php echo isset($schedName) ? "Edit Schedule" : "Create New Schedule"; ?></h2>


<form class="fcorn-registerTwo" name="schedEdit" id="schedEdit">
        
        <div class="row">
        	<div class="col-md-12"> 
            	<div class="col-md-6">
            		<div class="form-group"> 
            			<label for="newSchedName" class="labelText">Schedule Name :</label><span class="required">*</span>
            			<input class="form-control" type="text" name="newSchedName" id="newSchedName" size="40" maxlength="40" style="width:100% !important;" value="<?php echo isset($schedName) ? $schedName : ""; ?>">
            		</div>
            	</div>
        		
        		<div class="col-md-6">
            		<div class="form-group"> 
            			<label for="newSchedType" class="labelText" style="margin-left:0;">Schedule Type  :</label><span class="required">*</span>
                        <br/>
                        <?php
                        if (isset($schedName))
                        {
                        	echo "<label class='labelTextGrey labelText' style='margin-left:0;'>$schedType</label>";
                        }
                        else
                        {
							?>
							<div class="dropdown-wrap">
                        <select name="newSchedType" id="newSchedType">
                        	<option value=""></option>
                        	<option value="Holiday">Holiday</option>
                        	<option value="Time">Time</option>
						</select>
						</div>
                        <?php
                        }
                        ?>
            		</div>
        		</div>
        	
        	</div>
        </div>
		
		
		<!-- <div class="row">
        	<div class="col-md-12"> 
        		<div class="col-md-6" style="text-align:right;">
        			<input class="subButton marginRightButton" type="button" id="subButtonEdit" value="Confirm Settings">
        		</div>
        		<div class="col-md-6">
        			<?php
                       // if (isset($schedName))
                       // {
                        ?>
                        <input type="button" id="subButtonDel" class="deleteBtn marginRightButton" value="Delete This Schedule">
                        <input type="hidden" name="editSched" id="editSched" value="<?php //echo $schedName; ?>">
                        <input type="hidden" name="editSchedType" id="editSchedType" value="<?php //echo $schedType; ?>">
                        <?php
                      //  }
                        ?>
        		</div>     
					<input type="hidden" name="" id="newScheduleName" value="">
            </div>
         </div> --->
         
         <div class="row">
        	<div class="col-md-12" style="text-align:center;"> 
        	 	<input class="subButton" type="button" id="subButtonEdit" value="Confirm Settings">
        	 	<?php
                  if (isset($schedName)) {
                    ?>
                        <input type="button" id="subButtonDel" class="deleteBtn confirmDeleteButtonCenterAlignment" value="Delete This Schedule">
                        <input type="hidden" name="editSched" id="editSched" value="<?php echo $schedName; ?>">
                        <input type="hidden" name="editSchedType" id="editSchedType" value="<?php echo $schedType; ?>">
                     <?php
                   }
               ?>
        		<input type="hidden" name="" id="newScheduleName" value="">
            </div>
         </div>

</form>
<div id="dialogSC" class="dialogClass"></div>
