<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$_SESSION["scheduleNames"] = array(
		"newSchedName" => "Schedule Name",
		"newSchedType" => "Schedule Type",
		"eventName" => "Event Name",
		"dateStart" => "Start Date",
		"startTime" => "Start Time",
		"dateEnd" => "End Date",
		"endTime" => "End Time",
		"allDayEvent" => "All Day Event",
		"recurrence" => "Recurs",
		"recurrenceInterval" => "Every",
		"weeklyIntervalSunday" => "Sunday",
		"weeklyIntervalMonday" => "Monday",
		"weeklyIntervalTuesday" => "Tuesday",
		"weeklyIntervalWednesday" => "Wednesday",
		"weeklyIntervalThursday" => "Thursday",
		"weeklyIntervalFriday" => "Friday",
		"weeklyIntervalSaturday" => "Saturday",
		"recurMonthly" => "Monthly Interval",
		"dayOfMonth" => "Day of the Month",
		"dayOfWeekInMonth" => "Day of the Week in the Month",
		"dayOfWeek" => "Day of the Week",
		"recurYearly" => "Yearly Interval",
		"dayOfMonthInYear" => "Day of the Month",
		"byDayMonth" => "Month by Day",
		"dayOfWeekInMonthInYear" => "Day of the Week in the Month",
		"dayOfWeekInYear" => "Day of the Week",
		"byWeekMonth" => "Month by Week",
		"recurEnd" => "Recurrence End",
		"recurEndOccurrence" => "Recurrence End Occurrences",
		"recurEndDate" => "Recurrence End Date"
	);
?>
<script>
	$(function()
	{
		$("#module").html("> Modify Schedules: ");
		$("#endUserId").html("");

		$("#schedChoice").change(function()
		{
			scheduleChange();
		});

		var scheduleChange = function(){
			
			$("#eventData").html("");
			$("#schedData").html("");
			var id = $("#schedChoice").val();

			if (id == "newSchedule")
			{
				$.ajax({
					type: "POST",
					url: "schedules/editSchedule.php",
					success: function(result) {
						$("#eventData").html("");
						$("#schedData").html(result);
					}
				});
			}
			else if (id !== "")
			{
				pendingProcess.push("Schedule");
				$.ajax({
					type: "POST",
					url: "schedules/schedInfo.php",
					data: { schedValue: id },
					success: function(result) {
						if(foundServerConErrorOnProcess(result, "Schedule")) {
	    					return false;
	                  	}
						$("#eventData").html("");
						$("#dialogEV").html("");
						$("#schedData").html(result);
					}
				});
			}
			else
			{
				$("#endUserId").html("");
			}
		}
		var selectChoiceId = "<?php if(isset($_POST['schedNameToSend'])){echo $_POST['schedNameToSend']; }else{echo "";}?>";
		if(selectChoiceId){
		var trimmedVal = selectChoiceId.trim();
			$("#schedChoice option:contains("+trimmedVal+")").attr('selected', 'selected');
			//$('#schedChoice').val(selectChoiceId);
			$('#schedChoice').trigger("change");
		}
	});

	 if ($.fn.button.noConflict) {
	    	var bootstrapButton = $.fn.button.noConflict();
	    	$.fn.bootstrapBtn = bootstrapButton;
	    	}	
 </script>

<div id="scheduleDetailsId">
<!--<h2 class="adminUserText"> Choose Schedule to Modify </h2>-->
<h3 class="adminUserText"> Choose Schedule to Modify </h3>
<div class="adminUserForm">
	<form action="schedConfirm.php" method="POST" id="hgMod" class="fcorn-registerTwo">
		<div class="row fcorn-registerTwo">
			<div class="col-md-12 adminSelectDiv"> 
				<div class="form-group"> 
					<label class="labelText">Schedule :</label>
					<div class="dropdown-wrap">
                        <select name="schedChoice" id="schedChoice" class="form-control selectBlue">
                        <option value=""></option>
                        <?php
                        	require_once("/var/www/lib/broadsoft/adminPortal/getAllSchedules.php");
                        	if (isset($schedules))
                        	{
                        		foreach ($schedules as $key => $value)
                        		{
                        		    if ($value["level"] == "Group") {
                                        echo "<option value=\"" . $value["type"] . ":" . $value["name"] . "\">" . $value["name"] . "</option>";
                                    }
                        		}
                        	}
                        ?>
                        	<option value="newSchedule">--Create New Schedule--</option>
                        </select>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
	
<div id="schedData"></div>

<div id="ScheduleData"></div>
<div id="eventData"></div>
</div>

