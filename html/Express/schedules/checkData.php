<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$hidden = array("schedName", "schedType", "editSched", "editSchedType", "delEventName");
	$required = array("newSchedName", "newSchedType", "eventName", "dateStart", "dateEnd", "recurrenceInterval");

	if (isset($_POST["schedName"]))
	{
		if (isset($_POST["allDayEvent"]) and $_POST["allDayEvent"] == "True")
		{
			$startStamp = $_POST["dateStart"] . " 12:00am";
			$endStamp = $_POST["dateEnd"] . " 11:59pm";
		}
		else
		{
			$startStamp = $_POST["dateStart"] . " " . $_POST["startTime"];
			$endStamp = $_POST["dateEnd"] . " " . $_POST["endTime"];
		}
		$startUnix = strtotime($startStamp);
		$endUnix = strtotime($endStamp);
		if (isset($_POST["allDayEvent"]) and $_POST["allDayEvent"] == "True")
		{
			$endUnix += 60; //add 60 seconds
		}

		//divide by 24 hours to get length of duration in days
		$duration = ($endUnix - $startUnix)/(60 * 60 * 24);

		$unixNow = time();
	}

	$data = $errorTableHeader;

	$error = 0;
	foreach ($_POST as $key => $value)
	{
		if (!in_array($key, $hidden)) //hidden form fields that don't change don't need to be checked or displayed
		{
			$bg = CHANGED;

			if (in_array($key, $required))
			{
				if ($value == "")
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " is a required field.";
				}
			}
			if ($key == "newSchedName")
			{
				if (preg_match("[\"]", $value))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " must not include double quotes.";
				}
			}
			if ($key == "eventName")
			{
				if (preg_match("[\"]", $value))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " must not include double quotes.";
				}
				else
				{
					//check if event already exists
					$xmlinput = xmlHeader($sessionid, "GroupScheduleGetEventRequest");
					$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
					$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
					$xmlinput .= "<scheduleKey>
								<scheduleName>" . htmlspecialchars($_POST["schedName"]) . "</scheduleName>
								<scheduleType>" . $_POST["schedType"] . "</scheduleType>
							</scheduleKey>";
					$xmlinput .= "<eventName>" . htmlspecialchars($value) . "</eventName>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

					if (strval($xml->command->startDate) and ((isset($_POST["delEventName"]) and $value !== $_POST["delEventName"]) or !isset($_POST["delEventName"])))
					{
						$error = 1;
						$bg = INVALID;
						$value = $_SESSION["scheduleNames"][$key] . " already exists.";
					}
				}
			}
			if ($key == "dateStart")
			{
				if (!preg_match("/^\d{2}\/\d{2}\/\d{4}$/", $value))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " doesn't seem to be in the correct format (i.e. MM/DD/YYYY).";
				}
			/*	else if ($startUnix <= $unixNow)
				{
					$error = 1;
					$bg = INVALID;
					$value = "Start Date and Time cannot be in the past.";
				}
			*/	
			}
			if ($key == "dateEnd")
			{
				if (!preg_match("/^\d{2}\/\d{2}\/\d{4}$/", $value))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " doesn't seem to be in the correct format (i.e. MM/DD/YYYY).";
				}
				
			/*	else if ($endUnix <= $unixNow)
				{
					$error = 1;
					$bg = INVALID;
					$value = "End Date and Time cannot be in the past.";
				}
			*/	
				else if ($endUnix - $startUnix <= 0)
				{
					$error = 1;
					$bg = INVALID;
					$value = "End Date and Time must be after Start Date and Time.";
				}
			}
			if ($key == "startTime")
			{
				if ($value == "" and $_POST["allDayEvent"] !== "True")
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " is a required field.";
				}
			}
			if ($key == "endTime")
			{
				if ($value == "" and $_POST["allDayEvent"] !== "True")
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " is a required field.";
				}
			}
			if ($key == "recurrenceInterval")
			{
				//multiply recurrence interval by type of recurrence to get length of recurrence in days
				if ($_POST["recurrence"] == "Daily")
				{
					$recurrence = $_POST["recurrenceInterval"];
				}
				else if ($_POST["recurrence"] == "Weekly")
				{
					$recurrence = $_POST["recurrenceInterval"] * 7;
				}
				else if ($_POST["recurrence"] == "Monthly")
				{
					$recurrence = $_POST["recurrenceInterval"] * 28;
				}
				else if ($_POST["recurrence"] == "Yearly")
				{
					$recurrence = $_POST["recurrenceInterval"] * 365;
				}

				if (!preg_match("/^[0-9]+$/", $value))
				{
					$error = 1;
					$bg = INVALID;
					$value = "Recurrence interval must be numeric.";
				}
				else if ($value < 1)
				{
					$error = 1;
					$bg = INVALID;
					$value = "Recurrence interval must be >= 1.";
				}
				else if ($_POST["recurrence"] == "Weekly" and $_POST["weeklyIntervalSunday"] !== "True" and $_POST["weeklyIntervalMonday"] !== "True" and $_POST["weeklyIntervalTuesday"] !== "True" and $_POST["weeklyIntervalWednesday"] !== "True" and $_POST["weeklyIntervalThursday"] !== "True" and $_POST["weeklyIntervalFriday"] !== "True" and $_POST["weeklyIntervalSaturday"] !== "True")
				{
					$error = 1;
					$bg = INVALID;
					$value = "At least one day should be chosen for weekly recurrence.";
				}
				else if (($recurrence <= $duration and $_POST["allDayEvent"] !== "True") or ($recurrence < $duration and $_POST["allDayEvent"] == "True"))
				{
					//if duration of event is longer than recurrence, generate an error
					$error = 1;
					$bg = INVALID;
					$value = "The recurrence is invalid. The duration must be shorter than how frequently it occurs. Shorten the duration or change the recurrence.";
				}
				else
				{
					if ($_POST["recurrence"] == "Daily")
						$value .= " day(s)";
					else if ($_POST["recurrence"] == "Weekly")
						$value .= " week(s)";
					else if ($_POST["recurrence"] == "Monthly")
						$value .= " month(s)";
					else if ($_POST["recurrence"] == "Yearly")
						$value .= " year(s)";
				}
			}
			if ($key == "recurEndOccurrence" and $_POST["recurEnd"] == "After")
			{
				if ($value == "")
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " is a required field.";
				}
				else if (!preg_match("/^[0-9]+$/", $value))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " must be numeric.";
				}
			}
			if ($key == "recurEndDate" and $_POST["recurEnd"] == "Date")
			{
				if ($value == "")
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " is a required field.";
				}
				else if (!preg_match("/^\d{2}\/\d{2}\/\d{4}$/", $value))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " doesn't seem to be in the correct format (i.e. MM/DD/YYYY).";
				}
				else if (strtotime($value) <= strtotime($_POST["dateStart"]))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " must be after Start Date.";
				}
			}
			if ($key == "dayOfMonth" and $_POST["recurMonthly"] == "ByDay")
			{
				if ($value == "")
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " is a required field.";
				}
				else if (!preg_match("/^[0-9]+$/", $value))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " must be numeric.";
				}
				else if ($value < 1 or $value > 31)
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " must be >= 1 and <= 31.";
				}
			}
			if ($key == "dayOfMonthInYear" and $_POST["recurYearly"] == "ByDay")
			{
				if ($value == "")
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " is a required field.";
				}
				else if (!preg_match("/^[0-9]+$/", $value))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " must be numeric.";
				}
				else if (($value < 1 or $value > 29) and $_POST["byDayMonth"] == "February")
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " must be >= 1 and <= 29.";
				}
				else if (($value < 1 or $value > 30) and ($_POST["byDayMonth"] == "April" or $_POST["byDayMonth"] == "June" or $_POST["byDayMonth"] == "September" or $_POST["byDayMonth"] == "November"))
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " must be >= 1 and <= 30.";
				}
				else if ($value < 1 or $value > 31)
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["scheduleNames"][$key] . " must be >= 1 and <= 31.";
				}
			}

			$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["scheduleNames"][$key] . "</td><td class=\"errorTableRows\">";
			$data .= $value;
			$data .= "</td></tr>";
		}
	}
	echo $error . $data;
?>
