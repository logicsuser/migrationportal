<?php
    //error_reporting(0);
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	require_once("/var/www/lib/broadsoft/adminPortal/getEventInfo.php");

	$recurrenceArray = array("Never", "Daily", "Weekly", "Monthly", "Yearly");
        
?>
<style>
.ui-timepicker-list li {
    padding: 2px 0px 0px 10px;
}
</style>
<script src="/Express/js/timePicker.js"></script>
<script src="/Express/js/timepickerRange.js"></script>
<script>
	var buttonClickEvent = "";

	$(function()
	{
		var delEventName = "<?php echo isset($eventName) ? $eventName : ""; ?>";

		$("#dialogEV").dialog({
			autoOpen: false,
			width: 800,
			modal: true,
			position: { my: "top", at: "top" },
			resizable: false,
			closeOnEscape: false,
			buttons: {
				"Complete": {
					text: "Complete",
					id: "buildNewEvent",
					click: function() {
						var processAction = (buttonClickEvent == "EVsubButton") ? "Modify Schedule" : "Delete Schedule";
						pendingProcess.push(processAction);
						var dataToSend = $("form#eventEdit").serializeArray();
						$.ajax({
							type: "POST",
							url: (buttonClickEvent == "EVsubButton" ? "schedules/buildEvent.php" : "schedules/deleteEvent.php"),
							data: dataToSend,
                                                        beforeSend: function(){
                                                            $("html, body").animate({ scrollTop: 0 }, 0); //Code added @07 Feb 2019
                                                        },
							success: function(result) { 
								if(foundServerConErrorOnProcess(result, processAction)) {
			    					return false;
			                  	}
								if(buttonClickEvent == "EVsubButton"){
									var filterUsingLi = result.split("</li>");
									var filterUsingColon = filterUsingLi[0].split(":");
									$("#eventNameChanged").val(filterUsingColon[2].trim());
								}
								$("#dialogEV").dialog("option", "title", "Request Complete");
								//$(".ui-dialog-titlebar-close", this.parentNode).hide();
								//$(".ui-dialog-buttonpane", this.parentNode).hide();
								$("#dialogEV").html(result);
								//$("#dialogEV").append(returnLink);
								if(buttonClickEvent == "EVsubButton"){

									$(".ui-dialog-buttonpane button:contains('More Changes')").button().show().addClass('moreChangesButton');
								}									
								else if(buttonClickEvent == 'EVDELsubButton'){
									$(".ui-dialog-buttonpane button:contains('More Changes')").button().show().addClass('moreChangesButton');
								}
								else{
									$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
								}
								$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
								$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
								$(".ui-dialog-buttonpane button:contains('Return to Main')").button().show().addClass('returnToMainButton');
							}
						});
					}
				},
				"Cancel": function() {
					$(this).dialog("close");
				},
				"More Changes": function() {
					$(this).dialog("close");
					eventChangeFun();
				},
				"Return to Main": function() {
					location.href="main.php";
				}
			},
			open: function() {
				setDialogDayNightMode($(this));
				$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
				$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('completeButton');
				$('.ui-dialog-buttonpane').find('button:contains("Return to Main")').addClass('returnToMainButton');			 
				$('.ui-dialog-buttonpane').find('button:contains("More Changes")').addClass('moreChangesButton');			 
            }
		});

		$("#EVsubButton").click(function()
		{
			buttonClickEvent = this.id;
			var dataToSend = $("form#eventEdit").serializeArray();
			$.ajax({
				type: "POST",
				url: "schedules/checkData.php",
				data: dataToSend,
                                beforeSend: function(){
                                    $("html, body").animate({ scrollTop: 0 }, 0); //Code added @07 Feb 2019
                                },
				success: function(result) {
					$("#dialogEV").dialog("open");
					if (result.slice(0, 1) == "1")
					{
						$("#buildNewEvent").attr("disabled", "disabled").addClass("ui-state-disabled");
					}
					else
					{
						$("#buildNewEvent").removeAttr("disabled").removeClass("ui-state-disabled");
					}
					$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
					result = result.slice(1);
					$("#dialogEV").html(result);
				}
			});
	 	});

		$("#EVDELsubButton").click(function()
		{
			buttonClickEvent = this.id;
			$("#dialogEV").dialog("open");
			$("#dialogEV").html("Are you sure you want to delete event \"" + delEventName + "\"?");
			$(".ui-dialog-buttonpane button:contains('Complete')").removeClass("ui-state-disabled");
			$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
			$(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide();
			$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
			$(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');
		});

		function enableAllDayEvent()
		{
			if ($("#allDayEvent").is(":checked"))
			{
				$("#startTime").val("");
				$("#startTime").attr("disabled", "disabled");
				$("#endTime").val("");
				$("#endTime").attr("disabled", "disabled");
			}
			else
			{
				$("#startTime").removeAttr("disabled");
				$("#endTime").removeAttr("disabled");
			}
		}

		function enableIntervals()
		{
			var interval = $("#recurrence").val();
			if (interval == "Never")
			{
				$("#intervalDiv").html("");
			}
			else
			{
				$.ajax({
					type: "POST",
					url: "schedules/intervals.php",
					data: { intervalValue: interval },
					success: function(result) {                                                
						$("#intervalDiv").html(result);
					}
				});
			}
		}

		$("#allDayEvent").click(function()
		{
			enableAllDayEvent();
		});

		$("#recurrence").change(function()
		{
			enableIntervals();
		});

		enableAllDayEvent();
		enableIntervals();
		
		var eventChangeFun = function(){	
		
       			var module = 'modSched';
       			var selectedSchedule = $("#schedChoice").val();
       			var eventVal = selectedSchedule.split(":");
				var eventValReverse = eventVal[1]+":"+eventVal[0];
       			
       			var eventChoice = eventValReverse + ":" + $("#eventNameChanged").val();
       			
       			$('html, body').animate({scrollTop: '0px'}, 300);
       			
       			$("#schedData").html('');
       			$("#eventData").html('');
       			$("#schedChoice").val('');
       			$.ajax({
       				type: "POST",
       				url: "navigate.php",
       				data: { module : module, selectedSchedule : selectedSchedule, eventChoice : eventChoice},
       				success: function(result)
       				{ 	
       					$("#schedChoice").val(selectedSchedule);
       					$("#schedChoice").trigger("change");

       					setTimeout(function(){ 
       						$("#eventChoice").val(eventChoice);
           					$("#eventChoice").trigger("change");
           				}, 3000);
       				}
       			});
		}
		
	//Datepicker, Timepicker zooming	
	$(window).resize(function() {
		$('.startTime').timepicker();
	    var field = $(document.activeElement);
		if (field.is('.hasDatepicker')) {
            field.datepicker('hide').datepicker('show');
        }	 	
        if (field.is('.startTime')) {
            field.timepicker('hide').timepicker('show');
        }
	    if (field.is('.endTime')) {
            field.timepicker('hide').timepicker('show');
       }
    });
});

</script>
<h2 class="adminUserText" style="margin:16px 0 0px !important;"><?php echo isset($eventName) ? "Edit Event" : "Create New Event"; ?></h2>
<div class="row">
		<div class="col-md-12"> 
			<div class="form-group"> 
				<!-- row space  -->
			</div>
		</div>
	</div>

<form name="eventEdit" id="eventEdit" class="fcorn-registerTwo">
    <input type="hidden" name="schedName" id="schedName" value="<?php echo $_POST["schedName"]; ?>">
    <input type="hidden" name="schedType" id="schedType" value="<?php echo $_POST["schedType"]; ?>">
	
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
    			<div class="form-group">
        			<label class="labelText" for="eventName">Event Name:</label><span class="required">*</span>
        			<input class="form-control widthHP" type="text" name="eventName" id="eventName" size="40" maxlength="40" value="<?php echo $eventName; ?>">
    			</div>			
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
    			<div class="form-group">
    				<label class="labelText" for="dateStart">Start Date:</label><span class="required">*</span>
    				<div class="datepair">
						<input type="text" class="date start form-control widthHP" name="dateStart" id="dateStart" value="<?php echo $startDate; ?>" />    				
    				</div>
    			</div>
			</div>
			<div class="col-md-6">
    			<div class="form-group">
    				<label class="labelText" for="startTime">Start Time:</label><span class="required">*</span>
    				<div class="datepair">
    					<input type="text" class="time start form-control widthHP startTime" name="startTime" id="startTime" value="<?php echo $allDayEvent ? "" : $startTime; ?>" />
    				</div>
    			</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
    			<div class="form-group">
        			<label class="labelText" for="dateEnd">End Date:</label><span class="required">*</span>
        			<div class="datepair">
        				<input class="date end form-control widthHP" type="text" name="dateEnd" id="dateEnd" value="<?php echo $endDate; ?>" />
        			</div>
    			</div>
			</div>
			
			<div class="col-md-6">
    			<div class="form-group">
        			<label class="labelText" for="endTime">End Time:</label><span class="required">*</span>
        			<div class="datepair">
        				<input class="time end form-control widthHP endTime" type="text" name="endTime" id="endTime" value="<?php echo $allDayEvent ? "" : $endTime; ?>" />
        			</div>
    			</div>
			</div>
		</div>

	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6">
    			<div class="form-group">
    				<label class="labelText" for="allDayEvent">All Day Event:</label><span class="required">*</span>
    				<input type="hidden" name="allDayEvent" value="False" />
					<input type="checkbox" name="allDayEvent" id="allDayEvent" value="True"<?php echo isset($allDayEvent) && $allDayEvent ? " checked" : ""; ?> />
					<label for="allDayEvent"><span></span></label>
    			</div>			
			</div> 
		</div>
	</div>
	
	
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-12">
    			<div class="form-group">
					<label class="labelText" for="recurrence">Recurs:</label><span class="required">*</span>
					<div class="dropdown-wrap">
        			<select name="recurrence" id="recurrence" style="width:100% !important;">
					<?php
						foreach ($recurrenceArray as $key => $value)
						{
							echo "<option value=\"" . $value . "\"";
							if (isset($recurrence) and $value == $recurrence)
							{
								echo " selected";
							}
							echo ">" . $value . "</option>";
						}
					?>
				</select>
				<div>
    			</div>
			</div>
		</div>
	</div>	
	</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
    			<div id="intervalDiv"></div>
			</div>
		</div>
	</div>
	
	<div> 
		<div class="col-md-12">
    			<div class="form-group alignBtn">
    				<input class="subButton marginRightButton" type="button" id="EVsubButton" value="Confirm Settings">
    				<?php
            			if (isset($eventName))
            			{
            				?>
            					<input type="button" id="EVDELsubButton" class="deleteBtn marginRightButton" value="Delete This Event">
            				<input class="deleteBtn marginRightButton" type="hidden" name="delEventName" id="delEventName" value="<?php echo $eventName; ?>">
            				<?php
            			}
            		?>
    			</div>			
			</div> 
	</div>
		 
	</div>
</form>

<input type="hidden" id="eventNameChanged" name="eventNameChanged" value="" />
<div id="dialogEV" class="dialogClass"></div>
