<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
	$expProvDB = new ExpressProvLogsDB();
	
	$schedName = $_POST["schedName"];
	$schedType = $_POST["schedType"];
	require_once("/var/www/lib/broadsoft/adminPortal/deleteEvent.php");
	echo "Event " . $_POST["delEventName"] . " has been deleted.";

        require_once("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php"); //Added 30 Jan 2019
        
        $objChngLogUtil  = new ChangeLogUtility($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
        $objChngLogUtil->module      = "Delete Event";
        $objChngLogUtil->entityName  = addslashes($_POST["delEventName"]);        
        $lastId                      = $objChngLogUtil->changeLog("Delete Event", $schedName, "scheduleModChanges");
        
	/*$date = date("Y-m-d H:i:s");
	$query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
	$query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', 'Delete Event', '" . $_SESSION["groupId"] . "', '" . addslashes($_POST["delEventName"]) . "')";
	$sth = $expProvDB->expressProvLogsDb->query($query);
	$lastId = $expProvDB->expressProvLogsDb->lastInsertId();
	*/
        
	$insert = "INSERT into scheduleModChanges (id, serviceId, eventName) VALUES ('" . $lastId . "', '" . addslashes($schedName) . "', '" . addslashes($_POST["delEventName"]) . "')";
	$sth = $expProvDB->expressProvLogsDb->query($insert);
?>
