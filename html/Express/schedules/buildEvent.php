<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	require_once("/var/www/lib/broadsoft/adminPortal/buildEvent.php");
	require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
	$expProvDB = new ExpressProvLogsDB();
        
        //Code added @ 23 Jan 2019
        require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
        $cLUObj = new ChangeLogUtility(addslashes($_POST["eventName"]), $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
        //End code

	echo "The following event has been " . (isset($_POST["delEventName"]) ? "modified" : "created") . ":<br><br>";

        /*
	$date = date("Y-m-d H:i:s");
	$query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
	$query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', '" . (isset($_POST["delEventName"]) ? "Edit" : "New") . " Event', '" . $_SESSION["groupId"] . "', '" . addslashes($_POST["eventName"]) . "')";
	
	$sth = $expProvDB->expressProvLogsDb->query($query);
	$lastId = $expProvDB->expressProvLogsDb->lastInsertId();
	*/
        
        $cLUObj->module       = (isset($_POST["delEventName"]) ? "Edit" : "New") . " Event";
        $cLUObj->entityName   = addslashes($_POST["eventName"]);
        $lastId               = $cLUObj->changeLog();
        
        
	if ($_POST["allDayEvent"] == "True")
	{
		$startTime = "All Day Event";
		$endTime = "All Day Event";
	}
	else
	{
		$startTime = $_POST["startTime"];
		$endTime = $_POST["endTime"];
	}
	$insert = "INSERT into scheduleModChanges (id, serviceId, eventName, startTime, endTime, startDate, endDate, recurrence)";
	$insert .= " VALUES ('" . $lastId . "', '" . addslashes($_POST["schedName"]) . "', '" . addslashes($_POST["eventName"]) . "', '" . $startTime . "', '" . $endTime . "', '" . $_POST["dateStart"] . "', '" . $_POST["dateEnd"] . "', '" . $_POST["recurrence"] . "')";
	$sth = $expProvDB->expressProvLogsDb->query($insert);

	echo "<ul>";
	foreach ($_POST as $key => $value)
	{
		if ($key !== "schedName" and $key !== "schedType" and $key !== "delEventName")
		{
			echo "<li>" . $_SESSION["scheduleNames"][$key] . ": " . $value;
			if ($key == "recurrenceInterval")
			{
				if ($_POST["recurrence"] == "Daily")
					echo " day(s)";
				else if ($_POST["recurrence"] == "Weekly")
					echo " week(s)";
				else if ($_POST["recurrence"] == "Monthly")
					echo " month(s)";
				else if ($_POST["recurrence"] == "Yearly")
					echo " year(s)";
			}
			echo "</li>";
		}
	}
	echo "</ul>";
?>
