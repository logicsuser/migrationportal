<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/Announcement/Announcement.php");
$announcement = new Announcement();

$changeString = "";
$_SESSION['announcementUpdate'] = false;

$find = "C:\\fakepath\\";
$replace = "";
$_POST['announcementUploadVal'] = str_replace($find, $replace, $_POST['announcementUploadVal']);

$postArray = array();
$postArray['announcementId'] = $_POST['announcementId'];
$postArray['announcementName'] = $_POST['announcementName'];
$postArray['announcementTypeVal'] = $_POST['announcementTypeVal'];
$postArray['announcementType'] = $_POST['announcementType'];
$postArray['announcementUploadVal'] = $_POST['announcementUploadVal'];

//echo "<pre>"; print_r($_SESSION['announcement']); echo "<pre>"; print_r($_POST);
$where = array();
$where["sp"] = $_SESSION["sp"];
if(isset($_SESSION["groupId"]) && !empty($_SESSION["groupId"])){
	$where["groupId"] = $_SESSION["groupId"];
}else{
	$where["groupId"] = $_POST["grpName"];
}
 
$formDataArray = array(
		"announcementLabel" => "Announcement Label",
		"announcementName" => "Announcement Name",
		"announcementType" => "Announcement Type",
		//"announcementTypeVal" => "Announcement Type ID",
		"announcementUploadVal" => "Announcement Upload",
		);

//newcode starts
$error = "";
$backgroundColor = 'background:72ac5d;'; $errorMessage = '';

if(isset($postArray['announcementId']) && !empty($postArray['announcementId'])){ 
	
	foreach($postArray as $key=>$val){
		foreach($_SESSION['announcement'] as $key1=>$val1){
			$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
			if($key == $key1){
				$val = trim($val);
				if($val <> $val1){
					if($key == "announcementName"){
						$announcementNameExist = $announcement->checkAnnoucementExist($db, $val, $where);
						//echo "<pre>"; print_r($announcementNameExist); die;
						if($announcementNameExist == "Exist"){
							$backgroundColor = 'background:#ac5f5d;'; $error = "Error"; $errorMessage = " (Announcement Already Exist)";
						}
					}
					if($key == "announcementUploadVal"){
						$ext = pathinfo($val, PATHINFO_EXTENSION);
						if($ext <> "wav"){
							$backgroundColor = 'background:#ac5f5d;'; $error = "Error"; $errorMessage = " (Please upload only wav file.)";
						}
					}
					if(trim($val) == "" ){
						$backgroundColor = 'background:#ac5f5d;'; $error = "Error"; $errorMessage = $formDataArray[$key]." is mandatory fields";
					}
					if($val == "-Select-"){
						$backgroundColor = 'background:#ac5f5d;'; $error = "Error"; $errorMessage = $formDataArray[$key]." is mandatory fields"; $val = "";
					}
					
					if($key <> "announcementId" && $key <> "announcementTypeVal"){				
						$_SESSION['announcementUpdate'] = true;
						$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows labelTextGrey">'.$val.' '. $errorMessage.' </td></tr>';
					}
				}
			}
		}
	}
	if($_SESSION['announcementUpdate'] == false){
		$changeString = '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'">Announcement</td><td class="errorTableRows labelTextGrey">No Changes </td></tr>';
	}
	
}else{
	$changeString = "";
	foreach($postArray as $key=>$val){
		$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
		if (array_key_exists($key, $formDataArray)) {
			if($key == "announcementName"){
				$val = trim($val);
				$announcementNameExist = $announcement->checkAnnoucementExist($db, $val, $where);
				//echo "<pre>"; print_r($announcementNameExist); die;
				if($announcementNameExist == "Exist"){
					$backgroundColor = 'background:#ac5f5d;'; $error = "Error"; $errorMessage = " (Announcement Already Exist)";
				}
			}
			if($key == "announcementUploadVal"){
				$ext = pathinfo($val, PATHINFO_EXTENSION);
				if($ext <> "wav"){
					$backgroundColor = 'background:#ac5f5d;'; $error = "Error"; $errorMessage = " (Please upload only wav file.)";
				}
			}
			if(trim($val) == "" ){
				$backgroundColor = 'background:#ac5f5d;'; $error = "Error"; $errorMessage = $formDataArray[$key]." is mandatory fields";
			}
			if($val == "-Select-"){
				$backgroundColor = 'background:#ac5f5d;'; $error = "Error"; $errorMessage = $formDataArray[$key]." is mandatory fields"; $val = "";
			}
			$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$formDataArray[$key].'</td><td class="errorTableRows labelTextGrey">'.$val.' '. $errorMessage.' </td></tr>';
		}
	}
}

//newcode ends
echo $error.$changeString;
?>