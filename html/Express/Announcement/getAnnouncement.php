<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/Announcement/Announcement.php");

$announcement = new Announcement();
$where = array();
if(isset($_POST['announcement_id']) && !empty($_POST['announcement_id'])){
	$where['announcement_id'] = $_POST['announcement_id'];
}

if(isset($_POST['announcement_type']) && !empty($_POST['announcement_type'])){
	$where['announcement_type'] = $_POST['type'];
}
if(isset($_SESSION['sp']) && !empty($_SESSION['sp']) && isset($_SESSION['groupId']) && !empty($_SESSION['groupId'])){
    $where['sp'] = $_SESSION['sp'];
    $where['group'] = $_SESSION['groupId'];
}
if(isset($_POST['announcementGroupName']) && !empty($_POST['announcementGroupName'])){
    $where['sp'] = $_SESSION['sp'];
    $where['group'] = $_POST['announcementGroupName'];
}
//print_r($where);
$getAnnouncement = $announcement->getAnnouncement($db, $where);

if(isset($_POST['announcement_id']) && !empty($_POST['announcement_id'])){
	$_SESSION['announcement']['announcementId'] = $getAnnouncement[0]['announcement_id'];
	$_SESSION['announcement']['announcementName'] = $getAnnouncement[0]['announcement_name'];
	$_SESSION['announcement']['announcementTypeVal'] = $getAnnouncement[0]['announcement_type_id'];
	$_SESSION['announcement']['announcementUploadVal'] = $getAnnouncement[0]['announcement_file'];
	
	$announcementTypeText = $announcement->getAnnouncementTypeInfo($db, $getAnnouncement[0]['announcement_type_id']);
	$val = $announcementTypeText['name'];
	$_SESSION['announcement']['announcementType'] = $val;
	
}else{

	$_SESSION['announcement']['announcementName'] = "";
	$_SESSION['announcement']['announcementTypeVal'] = "";
	$_SESSION['announcement']['announcementUploadVal'] = "";
	$_SESSION['announcement']['announcementType'] = "";
	
}

//echo "<pre>"; print_r($getAnnouncement); die;
echo json_encode($getAnnouncement);



?>