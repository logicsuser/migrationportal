<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/Announcement/Announcement.php");

$announcement = new Announcement();

$where = array();
$deleteAnnouncement = "";

$announcementType = array(
	"1" => "Business Hours",	
	"2" => "After Hours",	
	"3" => "Holiday Hours",	
	"4" => "Voice Portal",	
	"5" => "Announcements",	
	"7" => "Submenu",	
);

if(isset($_POST["announcementGroupName"]) && !empty($_POST["announcementGroupName"])){
	$uploaddir = $_SERVER['DOCUMENT_ROOT']."/announcementUpload/".$_SESSION["sp"]."/".$_POST["announcementGroupName"]."/";
}else{
	$uploaddir = $_SERVER['DOCUMENT_ROOT']."/announcementUpload/".$_SESSION["sp"]."/".$_SESSION["groupId"]."/";
}

if(isset($_POST['announcement_id']) && !empty($_POST['announcement_id'])){
	$where['announcement_id'] = $_POST['announcement_id'];
	if($announcementType[$_POST["announcementType"]] <> "Submenu"){ 
		if($announcementType[$_POST["announcementType"]] <> "Announcements"){
			$deleteAnnouncement = $announcement->deleteAnnouncement($db, $where, $uploaddir, $ociVersion);
		}else{
			$deleteAnnouncement = $announcement->deleteAnnouncementTypeAnnouncement($db, $where, $uploaddir, $ociVersion);
		}
		
	}else{ 
		$deleteAnnouncement = $announcement->deleteAnnouncementSubmenuType($db, $where, $uploaddir, $ociVersion);
	}
	
	//echo "<pre>"; print_r($deleteAnnouncement); die;
	
	if(empty($deleteAnnouncement['Error'])){
		
		$changeLogObj = new ChangeLogUtility($_POST['announcement_id'], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
		$module = "Delete Announcement";
		$tableName = "announcementModChanges";
		$entity = $_POST["announcementName"];
		$changeResponse = $changeLogObj->changeLogDeleteUtility($module, $entity, $tableName);
	}
	/*
	 * admin Log show delete announcement details 
	 * ex- 765 
	 */
	if(isset($_SESSION["adminId"])) {
	    require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
	    $adminLogs = new adminLogs();
	    $resultAnnTypeName = $announcement->getAnnouncementTypeInfo($db,$_POST["announcementType"]);
	    $log = array(
	        'adminUserID' =>$_SESSION["adminId"],
	        'eventType' => 'DELETE_ANNOUNCEMENT',
	        'adminUserName' => $_SESSION["loggedInUserName"],
	        'adminName' => $_SESSION["loggedInUser"],
	        'updatedBy' => $_SESSION["adminId"],
	        'details' => array(
	            'Group Name' => $_POST["announcementGroupName"],
	            'Announcement Name' => $_POST["announcementName"],
	        )
	    );
	    $adminLogs->addLogEntry($log);
	}
}
echo json_encode($deleteAnnouncement);



?>