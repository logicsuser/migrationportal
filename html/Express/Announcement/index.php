<?php 
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();
$serP = $_SESSION["sp"];
require_once("/var/www/lib/broadsoft/adminPortal/getAllGroups.php");
?>
<script type="text/javascript" src="Announcement/js/announcement.js"></script>
<?php if($_SESSION["sp"] && !isset($_SESSION["groupId"])){ ?>
<script type="text/javascript">
    var selectedSP = "<?php echo $serP; ?>";
    getGroupList(selectedSP);
</script>
<?php  } ?>
<!-- script type="text/javascript" src="Announcement/js/jquery.min.js"></script-->
<h2 class="adminUserText">Announcements</h2>

<style>
input[type='file'] {
  opacity:0    
}

</style> 
 
	<div class="selectContainer padding-top-zero">
	    <form name="announcementList" id="announcementList" method="POST" action="#" class="announcementView" action="#" style="display:none">
		<?php if($_SESSION["sp"] && !isset($_SESSION["groupId"])){ ?>
			<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="labelText">Select Group</label>
			
					<div class="oneColWidth">
						<!--
                                                    <select name="announcementGroup" id="announcementGroup">
							<option value ="">None</option>
							<?php 
							/*if(count($allGroups) > 0){
								foreach($allGroups as $key => $value){
									echo "<option value = ".$value.">".$value."</option>";
								}
							}*/
							?>
						</select>
                                                -->
                                                <!-- Code added @ 11 Jan 2019 Regarding EX-1032 -->
                                                <input class="form-control blue-dropdown magnify" style="overflow:scroll;" type="text" class="autoFill" name="announcementGroups" id="announcementGroups">
                                                <input class="form-control" type="hidden" name="announcementGroup" id="announcementGroup">
                                                <!-- End code -->
					</div>
				</div>
			</div>
			</div>
			<?php } ?>
			
	        <div class="row">
	            <div class="col-md-11 adminSelectDiv">
	             	<div class="form-group existingAnnouncement">
					   <label class="labelText"> Select Announcement to view or modify</label>
					    <div class="dropdown-wrap">   
						   <select name="selectAnnouncement" id="selectAnnouncement" class="form-control selectBlue"></select>
						</div>
	        		</div>
	            </div>
	        	<div class="col-md-1 adminAddBtnDiv">
	            	<div class="form-groups">
	        			<?php
	        				if ($_SESSION["superUser"] == "1")
	        				{
	        						echo "<p class=\"register-submit addAnnounceBtnIcon\">";
	        						echo "<img src=\"images\icons\add_admin_rest.png\" name=\"newAnnouncement\" id=\"newAnnouncement\" class=\"center-block\">";
	        					echo "</p>";
	        				}
	        			?>
	        			<label class="labelText labelTextOpp" style="margin-left: -25px !important;"><span>Add</span><span>Announcement</span></label>
	        		</div>
	        	</div>
	        </div>	
	    </form>
	    <div class="loading" id="loading2">
			<img src="/Express/images/ajax-loader.gif">
		</div>
		<div id="announcementDetails">
	    	<form name="announcementForm" id="announcementForm" method="post" enctype="multipart/form-data" class="announcementView">
			<input type = "hidden" name = "annGroupName" id = "annGroupName" value="">
	        	<div class="row">
	        	 	<div class="col-md-6">
	    				<div class="form-group">
	    					<label class="labelText">Name:<span class="required">*</span></label>
	    					<input type="text" name="announcementName" id="announcementName" value="" autocomplete="off" class="form-controll"/>
	                        <input type="hidden" name="announcementId" id="announcementId" value="" />
	    				</div>
	        		</div>
	    			<div class="col-md-6">
	    				<div class="form-group">
							<label class="labelText">Type:<span class="required">*</span></label>
							<div class="dropdown-wrap oneColWidth">   
							<select name="announcementType" id="announcementType"></select>
							</div>
	    				</div>
	    			</div>
	        	</div>
	    		<div class="row">
	             	<div class="col-md-3">
	                 	<div class="form-group">
	            			<label class="labelText">Announcement:<span class="required">*</span></label>
	            			<input type="button" id="uploadFile" value="UploadFile" class="addAnnouncement">
	            			<input type="file" name="announcementUpload" id="announcementUpload" />
	            		 	<input type="hidden" name="announcementUploadVal" id="announcementUploadVal" value="" />
	                    
						</div>
	                </div>
	                <div class="col-md-2" style="padding-left:0">
            			<div class="form-group">	
                			<div id="chkFileUpload"></div>
                			<div id="showFileUpload"></div>
            			</div>
            		</div>
	        		<div class="col-md-7">
	            		<div class="col-md-12" style="padding-right:0">
	            			<div class="form-group"><br/>
	                		 	<a href="javascript:void(0);" id="download" class="download"><img src="images/icons/wav.png" class="annDownload" /></a>
	                		 	<audio controls id="audio" src="" controlsList="nodownload" style="width:75%; float:right; margin-right:16px;">
	                				<source id="audioOgg" src="" type="audio/ogg">
	                				<source id="audioMpeg" src="" type="audio/mpeg">
	                			</audio>

	                		</div>
	                	</div>
	            	</div>
	    		</div> 
	        	<div class="row" id="selectOptAnnButtons">
					<div class="form-group alignBtn">
						 <input type="hidden" name="checkAnnouncementWIthAutoattendant" id="checkAnnouncementWIthAutoattendant" value=""/>
						 <input id="announcementButtonDelete" class="deleteButton marginRightButton" value="Delete" type="button">
			
						<input type="button" id="announcementButtonCancel" value="Cancel" class="cancelButton marginRightButton">
				
						<input type="submit" id="saveButtonAnnouncement" value="Confirm Settings" class="subButton marginRightButton"> 
	        			
					</div>
	    		 
	    		</div>
	    		<!-- add announcement button -->
	    	 
	    		
	    		<!-- end add announcement buttons -->
	    		
	        </form>
	        <div class="row">
	        		 <div class="form-group alignBtn">	 
	        			<input type="hidden" name="completeButton" id="completeButton" value=""/>
	        		</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-12">
	        		 <div class="form-group">	 
						<div id="dialogAN" class="dialogClass"></div>
	        		</div>
	        	</div>
	        </div>
	    </div><!-- end announcement -->
</div><!-- main-body -->
<input type="hidden" name="spName" id="spName" value="<?php if(isset($_SESSION["sp"])){echo $_SESSION["sp"];}else{ echo "";}?>">
<input type="hidden" name="grpName" id="grpName" value="<?php if(isset($_SESSION["groupId"])){echo $_SESSION["groupId"];}else{ echo "";}?>">