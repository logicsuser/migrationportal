<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/Announcement/Announcement.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
$adminLogs = new adminLogs();
$announcement = new Announcement();
//$uploaddir = "../../announcementUpload/";
if($_SESSION["sp"] && isset($_SESSION["groupId"])){
    $sp = $_SESSION["sp"];
    $groupId = $_SESSION["groupId"];
    if (!file_exists('/var/www/html/announcementUpload/'.$sp."/")) {
        mkdir('/var/www/html/announcementUpload/'.$sp."/");
    }
    if (!file_exists('/var/www/html/announcementUpload/'.$sp."/".$groupId."/")) {
        mkdir('/var/www/html/announcementUpload/'.$sp."/".$groupId."/");
    }
    $uploaddir = "/var/www/html/announcementUpload/".$sp."/".$groupId."/";
}
if(!empty($_POST["annGroupName"]) && isset($_POST["annGroupName"])){
    $sp = $_SESSION["sp"];
    $groupId = $_POST["annGroupName"];
    if (!file_exists('/var/www/html/announcementUpload/'.$sp."/")) {
        mkdir('/var/www/html/announcementUpload/'.$sp."/");
    }
    if (!file_exists('/var/www/html/announcementUpload/'.$sp."/".$groupId."/")) {
        mkdir('/var/www/html/announcementUpload/'.$sp."/".$groupId."/");
    }
    $uploaddir = "/var/www/html/announcementUpload/".$sp."/".$groupId."/";
}

if(isset($_GET['files'])) { 
	if(!empty($_FILES[0]['name'])){
		
		$uploadAnnouncement = $announcement->uploadAnnouncement($_FILES[0], $uploaddir);
		echo $uploadAnnouncement;
	}else{ 
		$error = true;
		$data = ($error) ? array('error' => 'File is not available') : array('files' => $filenames);
		echo json_encode($data);
	}
}else{
    if(isset($_SESSION["groupId"]) && !empty($_SESSION["groupId"])){
        $sp = $_SESSION["sp"];
        $groupId = $_SESSION["groupId"];
    }else if(!empty($_POST["annGroupName"]) && isset($_POST["annGroupName"])){
        $sp = $_SESSION["sp"];
        $groupId = $_POST["annGroupName"];
    }else{
        $sp = "";
        $groupId = "";
    }
    
	
	$changeLog = array();
	$logArray = array();

    if(empty($_POST["announcementId"])){
    	$logArray = array();
    	$logArray = $announcement->makeAddArray($_POST);

	    $logSessionArray = array(
	    		"sp" => $sp,
	    		"annGroupName" => $groupId
	    );
	    $changeLog = array_merge($logArray, $logSessionArray);
	    $serviceId = "";
    }else{
    	$serviceId = $_POST["announcementId"];
    }
    
	$saveAnnouncement = $announcement->saveAnnouncement($db,$_POST, $uploaddir, $sp, $groupId);
	$saveAnnouncementResponse = json_decode($saveAnnouncement);
	//get announcement type name by announcement id
	$resultAnnTypeName = $announcement->getAnnouncementTypeInfo($db,$_POST["announcementType"]);
	if(!empty($saveAnnouncementResponse->success)){
	
		//code for change log
		
		$changeLogObj = new ChangeLogUtility($serviceId, $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
		
		if(empty($_POST["announcementId"])){
			
			$module = "Add Announcement";
			$tableName = "announcementAddChanges";
			$entity = $_POST["announcementName"];
			$changeResponse = $changeLogObj->changeLogAddUtility($module, $entity, $tableName, $changeLog);
			/*
			 * EX-765 updated date 16-08-2018
			 * Show admin Log details in add/save announcement
			 * get announcement Type using getAnnouncementTypeInfo();
			 * find user Id into user table accourding to userName which name is store in session
			 */
			$log = array(
			    'adminUserID' => $_SESSION["adminId"],
			    'eventType' => 'ADD_ANNOUNCEMENT',
			    'adminUserName' => $_SESSION["loggedInUserName"],
			    'adminName' => $_SESSION["loggedInUser"],
			    'updatedBy' => $_SESSION["adminId"],
			    'details' => array(
			        'Group Name' => $_POST["annGroupName"],
			        'Announcement Name' => $_POST["announcementName"],
			        'Announcement Type' => $resultAnnTypeName["name"],
			        'Announcement File' => $_POST["filenames"][0],
			    )
			);
			
		}else{
			$_POST["announcementUploadVal"] = $_POST["filenames"][0];
			$formDataArray = array(
					"announcementName" => "Announcement Name",
					"announcementType" => "Announcement Type",
					"announcementUploadVal" => "Announcement File",
			);
			$changeLogObj->createChangesArrayFromArray($_POST, $_SESSION['announcement'], $formDataArray);
			$module = "Modify Announcement";
			$tableName = "announcementModChanges";
			$entity = $_POST["announcementName"];
			$changeResponse = $changeLogObj->changeLogModifyUtility($module, $entity, $tableName);
        	/*
        	 * Admin log show announcement Modify details 
        	 * Details are show according to old value and new value
        	 */	
			$details = array('Modified By' => $_SESSION["loggedInUser"],'Group Name' =>$_POST['annGroupName']);
    		if($_POST[announcementName] != $_SESSION['announcement']['announcementName']) {
    	        $details['Announcement Name'] = array($_POST[announcementName],$_SESSION['announcement']['announcementName']);
    		}
    		if($resultAnnTypeName["name"] != $_SESSION['announcement']['announcementType']) {
    		    $details['Announcement Type'] = array($resultAnnTypeName["name"],$_SESSION['announcement']['announcementType']);
    		}
    		if(isset($_POST[announcementUploadVal]) && !empty($_POST[announcementUploadVal])){
        		if($_POST[announcementUploadVal] != $_SESSION['announcement']['announcementUploadVal']) {
        		    $details['Announcement File'] = array($_POST[announcementUploadVal],$_SESSION['announcement']['announcementUploadVal']);
        		}
    		}
		    $log = array(
    	      'adminUserID' =>$_SESSION["adminId"],
    		    'eventType' => 'MODIFY_ANNOUNCEMENT',
    		    'adminUserName' => $_SESSION["loggedInUserName"],
    		    'adminName' => $_SESSION["loggedInUser"],
    		    'updatedBy' => $_SESSION["adminId"],
    	      'details' => $details
    	     );
		 
	   }
		//print_r($changeResponse); die;
	   $adminLogs->addLogEntry($log);
	}
	//end code for change log
	
	echo $saveAnnouncement;
}



?>