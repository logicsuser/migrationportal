$(function()
{
	//hide form for announcement
	$("#announcementDetails").hide();
	$("#announcementList").hide();
	$("#loading2").hide();
	 $("#showFileUpload").hide();
	// $("#selectOptAnnButtons").hide();
    //$("#addNewAnnButtons").show();
	$('#selectGroup').on('keyup keypress', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) {
			e.preventDefault();
			return false;
		  }
		});
	
	//function to get the list of announcement
	var getAnnouncementList = function(announcementId){
		$("#loading2").show();
		$("#announcementDetails").hide();
                
                //code added @ 27 Nov 2018
                var announcementGroupName = "";
                if($("#grpName").val()!=""){
                    announcementGroupName = $("#grpName").val();
                }
                //End code
                
		var autoComplete = new Array();
                
                //code added @ 27 Nov 2018
                var announcementGroupName = "";
                if($("#grpName").val()!=""){
                    announcementGroupName = $("#grpName").val();
                }
                //End code
                
		var html = '<option value="">Select Announcement</option>';
		
		$.ajax({
			url: 'Announcement/getAnnouncement.php',
            type: 'POST',
            data: {announcementGroupName : announcementGroupName}, //code added @ 27 Nov 2018
            success: function(data)
            { 
            	var obj = jQuery.parseJSON(data);
            	if(obj.length > 0){
					for (var i = 0; i < obj.length; i++) {
						html += '<option value="'+ obj[i].announcement_id +'">' + obj[i].announcement_name + '</option>';
						//autoComplete[i] = obj[i].announcement_name;
					}
            	}else{
            		html = '<option value="">Announcement not available</option>';
            	}
				$("#selectAnnouncement").html(html);
				if(announcementId){
					$("#selectAnnouncement").val(announcementId);
					getAnnouncementInfo(announcementId);
		           	$("#announcementDetails").show();
		           	$("#selectOptAnnButtons").show();
		           	//$("#addNewAnnButtons").hide();
				}
				$("#loading2").hide();
				$("#announcementList").show();
				var groupVal = $("#announcementGroup").val();
				//if (typeof val === "undefined") {
				if (groupVal != "") {
					$("#selectAnnouncement").prop("disabled", false);
					$("#newAnnouncement").prop("disabled", false);
				}else{
					$("#selectAnnouncement").prop("disabled", true);
					$("#newAnnouncement").prop("disabled", true);
				}
				
				
            	/*$("#selectAnnouncement").autocomplete({
	                source: autoComplete,
	            }).autocomplete( "widget" ).addClass( "announcementAutoClass" );
            	*/
            }
            
		});
	};
	getAnnouncementList(0);
	
	$("#announcementGroup").change(function(){ 
		//$("#loading2").show();
		$("#announcementDetails").hide();
		var autoComplete = new Array();
		var announcementGroupName = $("#announcementGroup").val();
		if(announcementGroupName){
			$("#grpName").val(announcementGroupName);
		}
		
		//alert(announcementGroupName);
		var html = '<option value="">Select Announcement</option>';
		$("#selectAnnouncement").html("");
		$.ajax({
			url: 'Announcement/getAnnouncement.php',
            type: 'POST',
            data: {announcementGroupName : announcementGroupName},
            success: function(data)
            {
            	var obj = jQuery.parseJSON(data);
            	if(obj.length > 0){
					for (var i = 0; i < obj.length; i++) {
						html += '<option value="'+ obj[i].announcement_id +'">' + obj[i].announcement_name + '</option>';
						//autoComplete[i] = obj[i].announcement_name;
					}
            	}else{
            		html = '<option value="">Announcement not available</option>';
            	}
				$("#selectAnnouncement").html(html);
				/*if(announcementId){
					$("#selectAnnouncement").val(announcementId);
					getAnnouncementInfo(announcementId);
		           	$("#announcementDetails").show();
				}else{
					$("#selectAnnouncement").val("");
				}*/
				$("#loading2").hide();
				$("#announcementList").show();
				if(!$("#announcementGroup").val()){
					$("#selectAnnouncement").prop("disabled", true);
					$("#newAnnouncement").prop("disabled", true);
				}else{
					$("#selectAnnouncement").prop("disabled", false);
					$("#newAnnouncement").prop("disabled", false);
				}
            	/*$("#selectAnnouncement").autocomplete({
	                source: autoComplete,
	            }).autocomplete( "widget" ).addClass( "announcementAutoClass" );
            	*/
            }
            
		});
	});

	//function to get the list of announcement
	var getAnnouncementTypeList = function(){
		
		var html = '<option value="">-Select-</option>';
		
		$.ajax({
			url: 'Announcement/getAnnouncementType.php',
            type: 'POST',
            success: function(data)
            { 
            	var obj = jQuery.parseJSON(data);
            	if(obj.length > 0){
					for (var i = 0; i < obj.length; i++) {
						html += '<option value="'+ obj[i].announcement_type_id +'">' + obj[i].name + '</option>';
					}
					
            	}else{
            		html = '<option value=""></option>';
            	}
            	$("#announcementType").html(html);
            }            
		});
	};
	getAnnouncementTypeList();
	
	//function to get announcement info
	var getAnnouncementInfo = function(announcementId){
		
		$("#showFileUpload").html('');
        $("#showFileUpload").hide();
        
		$("#announcementButtonDelete").show();
		$.ajax({
			url: 'Announcement/getAnnouncement.php',
            type: 'POST',
            data : {announcement_id : announcementId},
            success: function(data)
            {	
            	var obj = jQuery.parseJSON(data);
        		$("#announcementType").val(obj[0].announcement_type_id);
        		$("#announcementName").val(obj[0].announcement_name);
        		$("#announcementUploadVal").val(obj[0].announcement_file);
        		$("#announcementId").val(obj[0].announcement_id);
        		$("#announcementDetails").show();
        		 $("#selectOptAnnButtons").show();
        		   // $("#addNewAnnButtons").hide();
        		$("#completeButton").val('');
        		$("#loading2").hide();  
        		$("#chkFileUpload").html('<label class="labelpadding labelText"><br/>Uploaded:</label>');
        		playAnnouncement(obj[0].announcement_file);
        		$('#announcementUpload').val('');
        		
            }
            
		});
	};
	
	//function to run when create new announcement
	var createAnnouncement = function(){
		$("#announcementButtonDelete").hide();
		$("input[type=text]").val('');
		$("#announcementType").val('');
		$("input[type=hidden]").val('');
		$("input[type=file]").val('');
		$("#audio").hide();
		$("#download").hide();
		$("#audio").trigger('pause');
		//$("#audio").trigger('reset');
		$("#uploadFile").val('Choose File');
		$("#chkFileUpload").html('');
		$('#announcementUpload').val('');
		$("#attachedFileName").html();
		/*if($("#announcementGroup").val()){
			$("#annGroupName").val($("#announcementGroup").val());
		}else{
			$("#annGroupName").val();
		}*/
                //Code commented @ 11 Jan 2019 regarding EX-1032
		/*if($("#announcementGroup").val()){
			$("#annGroupName").val($("#announcementGroup").val());
		}else{
			$("#annGroupName").val();
		}*/             
                //End code
                //Code added @ 11 Jan 2019 regarding EX-1032               
                if($('#announcementGroups').length > 0){
                    $('#announcementGroups').trigger("blur");
                    var groupNameNId = $('#announcementGroups').val();
                    var tmpStr       = groupNameNId.split("-");
                    var groupId      = tmpStr[0].trim();
                    $('#announcementGroup').val(groupId);
                    if($("#announcementGroup").val()){
                            $("#annGroupName").val($("#announcementGroup").val());
                    }else{
                            $("#annGroupName").val();
                    }
                }else{
                    $("#annGroupName").val();
                }
                //End code
		
	}
	
	//function to run when click on cancel button in create or modify
	var closeCreateAnnouncement = function(){
		$("#announcementDetails").hide();
	}
	
	//function to run when modify announcement
	var modifyAnnouncement = function(announcementId){
		var spName = $("#spName").val();
		var grpName = $("#grpName").val();
		createAnnouncement();
		$("#spName").val(spName);
		$("#grpName").val(grpName);
		getAnnouncementInfo(announcementId);
	}
	
	//button click event for create announcement
	$("#newAnnouncement").click(function(){
		$("#loading2").hide();
		$("#selectAnnouncement").val('');
		var spName = $("#spName").val();
		var grpName = $("#grpName").val();
		createAnnouncement();
		$("#announcementDetails").show();
		$('#announcementUpload').val('');
		$("#attachedFileName").html('');
		$("#showFileUpload").hide();
		$("#selectOptAnnButtons").show();
		//$("#addNewAnnButtons").show();
		$("#spName").val(spName);
		$("#grpName").val(grpName);

	});	
	
	//button click event for modify announcement
	$("#selectAnnouncement").change(function(){ 
		var el = $(this);
		var announcementVal = el.val();
		if(announcementVal == ""){
			$("#announcementDetails").hide();
			return false;
		}
		
		$("#announcementDetails").hide();
		$("#loading2").show();
		$(".audio").remove();
		
		modifyAnnouncement(announcementVal);
		checkAnnouncementWithAutoattendant(announcementVal);
	});	
	
	//button click event for close create or modify announcement
	$("#announcementButtonCancel").click(function(){
		closeCreateAnnouncement();
		$("#audio").trigger('pause');
		//$("#audio").trigger('reset');
		$("#audio").hide();
		$("#download").hide();
		$("#selectAnnouncement").val('');
	});	
	
	//delete announcement function
	var deleteAnnouncement = function(announcementId){
		var announcementGroupName = $("#announcementGroup").val();
		var announcementName  = $("#announcementName").val();
		if(announcementGroupName){
			var announcementGroupNamedel = announcementGroupName;
		}else{
			var announcementGroupNamedel = "";
		}
		
		var announcementType = $("#announcementType").val();
		
		$.ajax({
			url: 'Announcement/deleteAnnouncement.php',
            type: 'POST',
            data:{announcement_id : announcementId, announcementGroupName : announcementGroupNamedel, announcementType : announcementType, announcementName : announcementName},
            success: function(data)
            { 
            	var obj = jQuery.parseJSON(data);            	
            	if(obj.Error){
	           		$("#dialogAN").html(obj.Error);	
					buttonsShowHide('Ok', 'hide');
					buttonsShowHide('Cancel', 'show');
					$(":button:contains('Cancel')").addClass("cancelButton");
	          	}else{
	             		$("#dialogAN").html("<label class=\'labelTextGrey\'>Announcement deleted successfully</label>");	
	             		$("#announcementDetails").hide();    
						buttonsShowHide('Ok', 'show');
						$(":button:contains('Ok')").addClass("subButton");
						buttonsShowHide('Cancel', 'hide');
	          	}
            	$(":button:contains('Delete')").removeAttr("disabled", "disabled").removeClass("ui-state-disabled");
        		getAnnouncementList(0);
				buttonsShowHide('Delete', 'hide');
				buttonsShowHide('Complete', 'hide');
				buttonsShowHide('More changes', 'hide');
				$('#selectAnnouncement').prop('disabled', false);
				$('#newAnnouncement').prop('disabled', false);
				//$("#announcementGroup").trigger("change");
                                showSavedAnnouncement(); //Code added @ 11 Jan 2019
           }            
		});
		
	}
	
	//check for announcement attached with auto attendant function
	var checkAnnouncementWithAutoattendant = function(announcementId){
		
		$.ajax({
			url: 'Announcement/checkAnnouncementWithAutoattendant.php',
			type: 'POST',
			data:{announcement_id : announcementId},
			success: function(data)
			{ 
				if(data > 0){
					$("#checkAnnouncementWIthAutoattendant").val('yes');
				}else{
					$("#checkAnnouncementWIthAutoattendant").val('no');
				}
			}            
		});
		
	}
	
	
	//delete button click event
	$("#announcementButtonDelete").click(function(){
		
		if(announcementId == ""){
			return false;
		}
		var checkAttached = $("#checkAnnouncementWIthAutoattendant").val();
		
		$("#dialogAN").dialog("open");
		$("#dialogAN").dialog("option", "title", "Request Complete");
		if(checkAttached == "yes"){
			$("#dialogAN").html("<label class=\'labelTextGrey\'>Given announcement is attached with one or more Auto attendant services. Are you sure you want to delete this announcement?</label>");
		}else{
			$("#dialogAN").html("<label class=\'labelTextGrey\'>Are you sure you want to delete this announcement?</label>");
		}
		$(":button:contains('Delete')").removeAttr("disabled", "disabled").removeClass("ui-state-disabled deleteButton");
		buttonsShowHide('Delete', 'show');
		$(":button:contains('Delete')").addClass("deleteButton");
		buttonsShowHide('Cancel', 'show');
		$(":button:contains('Cancel')").addClass("cancelButton");
		buttonsShowHide('Complete', 'hide');
		buttonsShowHide('More changes', 'hide');
		buttonsShowHide('Ok', 'hide');
		
	});
	
	var showSaveDialogue = function(){
		
		var announcementName = $("#announcementName").val();
		var announcementType = $("#announcementType option:selected").text();
		var announcementTypeVal = $("#announcementType").val();
		var announcementId = $("#announcementId").val();
		var announcementFileHiddenVal = $("#announcementUploadVal").val();
		var announcementFileUpload = $("#announcementUpload").val();
		
		if(announcementFileUpload != "" && announcementFileHiddenVal != ""){	
			announcementUploadVal =  announcementFileUpload;
		}else if(announcementFileUpload == "" && announcementFileHiddenVal != ""){
			announcementUploadVal =  announcementFileHiddenVal
		}else if(announcementFileUpload != "" && announcementFileHiddenVal == ""){
			announcementUploadVal =  announcementFileUpload;
		}else{
			announcementUploadVal = "";
		}
		var grpName = $("#grpName").val();
		
		$.ajax({
			url: 'Announcement/validate.php',
            type: 'POST',
            data:{announcementId : announcementId, announcementName : announcementName, announcementTypeVal : announcementTypeVal, announcementType : announcementType, announcementUploadVal: announcementUploadVal, grpName : grpName},
            success: function(data)
            { 
            	var checkAttached = $("#checkAnnouncementWIthAutoattendant").val();
            	$("#loading2").hide();
            	
            	var noChangesExist = data.search("No Changes");
            	var errorExist = data.search("Error");
            	
            	
        		$("#dialogAN").dialog("open");
				$("#dialogAN").dialog("option", "title", "Request Create Announcement");
				if(checkAttached == "no" || checkAttached == ""){
					$("#dialogAN").html('<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelist" class="confSettingTable"><tbody><tr><th colspan="2" align="center" class="labelTextGreyHeadingRc">Please confirm the settings below and click Complete to process your modifications.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable labelTextGrey"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');
				}
				if(checkAttached == "yes"){
					$("#dialogAN").html('<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelist" class="confSettingTable"><tbody><tr><th colspan="2" align="center" class="labelTextGreyHeadingRc">Please confirm the settings below and click Complete to process your modifications.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable labelTextGrey"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td colspan="2">&nbsp;</td></tr><tr><td colspan="2"><b style="color:#ac5f5d;""><label class=\'labelTextGrey\'>Announcement is already attached with Auto Attendant Service, modification of announcement make you to change in Auto Attendant service also. Please confirm to complete.</label></b></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');
				}
				$("#changelist").append(data);
				$("#dialogAN").append('</tbody></table>');  
				
				if(errorExist == 0){
					$("#dialogAN").html('<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelist" class="confSettingTable"><tbody><tr><th colspan="2" align="center" class="labelTextGreyHeadingRc"><label class=\'labelTextGrey\'>Please confirm the settings below and click Complete to process your modifications.</label></th></tr><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable labelTextGrey"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td colspan="2">&nbsp;</td></tr>');
					$("#changelist").append(data);
					$("#dialogAN").append('</tbody></table>');  buttonsShowHide('Delete', 'hide');
					buttonsShowHide('Cancel', 'show');
					$(":button:contains('Cancel')").addClass("cancelButton");
					buttonsShowHide('Complete', 'hide');
					buttonsShowHide('More changes', 'hide');
					buttonsShowHide('Ok', 'hide');
				}else if(noChangesExist > 0){
					$("#dialogAN").html('<table style="width:100%;" cellspacing="0" cellpadding="5" id="changelist"><tbody><tr><td colspan="2" class="labelTextGreyHeadingRc">You have made no changes</td></tr></tbody></table>');
					buttonsShowHide('Delete', 'hide');
					buttonsShowHide('Cancel', 'show');
					$(":button:contains('Cancel')").addClass("cancelButton");
					buttonsShowHide('Complete', 'hide');
					buttonsShowHide('More changes', 'hide');
					buttonsShowHide('Ok', 'hide');
				}else{            	
	            	
					buttonsShowHide('Delete', 'hide');
					buttonsShowHide('Cancel', 'show');
					$(":button:contains('Cancel')").addClass("cancelButton");
					buttonsShowHide('Complete', 'show');
					$(":button:contains('Complete')").addClass("subButton");
					buttonsShowHide('More changes', 'hide');
					buttonsShowHide('Ok', 'hide'); 
            	}
            }            
		});
	}
	
	var buttonsShowHide = function(b,e){
		if(e == "show"){
			$(".ui-dialog-buttonpane button:contains("+b+")").button().show();
		}else{
		 	$(".ui-dialog-buttonpane button:contains("+b+")").button().hide();
		}
	};

	$("#dialogAN").dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		buttons: {
			"Complete": function() {
				$("#completeButton").val('1');
				$("#announcementForm").submit();
			},
			"Cancel": function() {
				$(this).dialog("close");
				$("#loading2").hide();
				$("#announcementDetails").show();
			},
			"Delete": function() {				
				$("#audio").trigger('pause');
				//$("#audio").trigger('reset');
				$("#audio").hide();
				$("#download").hide();
				$(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
				$("#dialogAN").html("<label class=\'labelTextGrey\'>Deleting the announcement....</label><img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">");
				var announcementId = $("#announcementId").val();
				deleteAnnouncement(announcementId);	
				$('#selectAnnouncement').prop('disabled', false);
				$('#newAnnouncement').prop('disabled', false);
			},
            "More changes": function() {
            	$('#selectAnnouncement').prop('disabled', 'disabled');
            	var announcementId = $("#announcementId").val();
            	var val = $("#announcementGroup").val();
            	
            	if (typeof val === "undefined") {
            		getAnnouncementList(announcementId);
            	}else{
            		//$("#announcementGroup").trigger("change");
                        showSavedAnnouncement(); //Code added @ 11 Jan 2019
                	setTimeout(function(){ 
                		$("#selectAnnouncement").val(announcementId); 
                		$("#selectAnnouncement").trigger("change");
                		$("#selectAnnouncement").prop("disabled", false);
                	}, 4000);
            	}
            	
            	
            	
            	$(this).dialog("close");
            },
            "Done": function() {
            	createAnnouncement();    
            	$(this).dialog("close");
            	$("#announcementDetails").hide();
            	$("#selectAnnouncement").val('');
			},
            "Ok": function() {
            	$(this).dialog("close");
			},
			"Return to Main": function() {
				/*$(this).dialog("close");
				$("#announcementDetails").hide();
            	$("#selectAnnouncement").val('');*/
				$(location).attr('href','main.php');
			},
			"Add New Announcement": function() {
				$(this).dialog("close");
				getAnnouncementList(0);
				$("#newAnnouncement").trigger('click');
			}
		},
        open: function() {
                $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019	
                setDialogDayNightMode($(this));
        	buttonsShowHide('Complete', 'show');
        	$(":button:contains('Complete')").addClass("subButton");
        	buttonsShowHide('Cancel', 'show');
        	$(":button:contains('Cancel')").addClass("cancelButton");
        	buttonsShowHide('More Changes', 'hide');
        	buttonsShowHide('Done', 'hide');
        	buttonsShowHide('Delete', 'hide');
        	buttonsShowHide('Return to Main', 'hide');
        	$(":button:contains('Return to Main')").addClass("returnToMainButton");
        	buttonsShowHide('Add New Announcement', 'hide');
        }
	});		

	// Variable to store your files
	var files;

	// Add events
	$('input[type=file]').on('change', prepareUpload);
	$('form').on('submit', uploadFiles);

	// Grab the files and set them to our variable
	function prepareUpload(event)
	{	
		files = event.target.files;
	}

	// Catch the form submit and upload the files
	function uploadFiles(event)
	{	
		$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled ui-state-hover").addClass("subButton");
		
		var announcementName = $("#announcementName").val();
		var announcementName = announcementName.trim();
		var announcementType = $("#announcementType").val();
		var announcementUpload = $("#announcementUpload").val();
		var announcementUploadVal = $("#announcementUploadVal").val();
		var file_ext = announcementUpload.split('.').pop();
		var file_ext_uploaded = announcementUploadVal.split('.').pop();
		
		/*if(announcementName == "" || announcementType == "" || (announcementUpload == "" && announcementUploadVal == "")){
			$("#dialogAN").dialog("open");
			$("#dialogAN").dialog("option", "title", "Request Parameters");
			$("#dialogAN").html("Please fill the mandatory fields for announcement..");
			buttonsShowHide('Delete', 'hide');
			buttonsShowHide('Cancel', 'hide');
			buttonsShowHide('Complete', 'hide');
			buttonsShowHide('More changes', 'hide');
			buttonsShowHide('Ok', 'show');
			return false;
		}
		
		if((announcementUpload != "" && file_ext != "wav") || (announcementUploadVal != "" && file_ext_uploaded != "wav" )){
			$("#dialogAN").dialog("open");
			$("#dialogAN").dialog("option", "title", "Request Parameters");
			$("#dialogAN").html("Please upload only wav file.");
			buttonsShowHide('Delete', 'hide');
			buttonsShowHide('Cancel', 'hide');
			buttonsShowHide('Complete', 'hide');
			buttonsShowHide('More changes', 'hide');
			buttonsShowHide('Ok', 'show');
			return false;			
		}*/
		
		$("#loading2").show();
		
		//check the value of complete button hidden field to switch to complete button click when form submitted insted of submitting the form on confirm setting button
		var completeButton = $("#completeButton").val();
		if(completeButton == ""){
			$("#announcementDetails").hide();
			showSaveDialogue();
			return false;
		}
		
		//check the announcement id is null or not to change the message
		var announcementIdVal = $("#announcementId").val();
		if(announcementIdVal == ""){
			$("#dialogAN").html('<label class=\'labelTextGrey\'>Creating the announcement....</label><img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
		}else{
			$("#dialogAN").html('<label class=\'labelTextGrey\'>Modifying the announcement....</label><img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
			
		}
		$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton").removeClass("ui-state-hover");
		$("#loading2").hide();
		$("#announcementDetails").hide();
		
		event.stopPropagation(); // Stop stuff happening
        event.preventDefault(); // Totally stop stuff happening
        
        
        // START A LOADING SPINNER HERE
		//$("#loading2").show();
		
		var fileVal = $("input[type=file]").val();
		
        // Create a formdata object and add the files
		var data = new FormData();
		var announcementGroupName = $("#announcementGroup").val();
		if(fileVal != ""){
			$.each(files, function(key, value)
			{
				data.append(key, value);
			});
			if(announcementGroupName){
				data.append("annGroupName", announcementGroupName);
			}
		}
        console.log(data);
        $.ajax({
            url: 'Announcement/saveAnnouncement.php?files',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function(data, textStatus, jqXHR)
            { 
            	if(typeof data.error === 'undefined')
            	{
            		// Success so call function to process the form
            		submitForm(event, data);
            	}
            	else
            	{
            		// Handle errors here
            		console.log('ERRORS: ' + data.error);
            		submitForm(event, data);
            	}
            },
            error: function(jqXHR, textStatus, errorThrown)
            { 
            	var errorResponseText = jqXHR.responseText;
            	var errorText = errorResponseText.split('*');
            	
            	if(errorText[0] == "File is not available"){
            		submitForm(event, data);
            	}
            	// Handle errors here
            	console.log('ERRORS: uuuu ' + textStatus);
            	// STOP LOADING SPINNER
            }
        });
    }

    function submitForm(event, data)
	{
		// Create a jQuery object from the form
		$form = $(event.target);
		
		var fileVal = $("input[type=file]").val();
		
		// Serialize the form data
		var formData = $form.serialize();
		
		// You should sterilise the file names
		/*$.each(data.files, function(key, value)
		{
			formData = formData + '&filenames[]=' + value;
		});*/
		
		if(fileVal != ""){
			$.each(data.files, function(key, value)
			{
				formData = formData + '&filenames[]=' + value;
			});
		}
		var announcementGroupName = $("#announcementGroup").val();

		$.ajax({
			url: 'Announcement/saveAnnouncement.php',
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            success: function(data, textStatus, jqXHR)
            { 
            	if(typeof data.error === 'undefined')
            	{	 
            		var announcementId = $("#announcementId").val();
            		
            		$("#dialogAN").html(data.success);
            		buttonsShowHide('Complete', 'hide');
                	buttonsShowHide('Cancel', 'hide');
                	buttonsShowHide('More changes', 'show');
                	$(":button:contains('More changes')").addClass("moreChangesButton");
                	buttonsShowHide('Done', 'hide');
                	buttonsShowHide('Delete', 'hide');
                	buttonsShowHide('Ok', 'hide');
                	if(announcementId == ""){
                		buttonsShowHide('Add New Announcement', 'show');
                		$(":button:contains('Add New Announcement')").addClass("subButton");
                	}else{
                		$(":button:contains('Add New Announcement')").addClass("subButton");
                		buttonsShowHide('Return to Main', 'show');
                		$(":button:contains('Return to Main')").addClass("subButton");
                	}
                	
            		// Success so call function to process the form
                	$("#announcementId").val(data.announcementId);
                	$("#announcementButtonDelete").show();            		
                	$("#loading2").hide();
            	}
            	else
            	{ 
            		// Handle errors here
            		console.log('ERRORS: ' + data.error);
            	}
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
            	// Handle errors here
            	console.log('ERRORS: ' + textStatus);
            	//$("#announcementDetails").show();
            	$("#loading2").hide();
            },
            complete: function()
            {	
            	$("#announcementDetails").hide();
            	//$("#loading2").hide();
            	
            	// STOP LOADING SPINNER
            }
		});
	}
	$("#audio").hide();
	$("#download").hide();

	var playAnnouncement = function(fileName){		
		
		if(fileName != ""){
			var spName = $("#spName").val();
			var grpName = $("#grpName").val();
			if(spName && grpName){
				var filePath = window.location.origin+'/announcementUpload/'+spName+"/"+grpName+"/"+fileName;
			}
			
			$.get(filePath, function(data, textStatus) {
		        if (textStatus == "success") {	
					var downloadPath = window.location.origin+'/broadsoft/download.php?file=' + fileName + '&spName=' + spName + '&grpName=' + grpName;
					
					$("#audio").attr("src", filePath);		
					//$("#audioOgg").attr("src", filePath);		
					//$("#audioMpeg").attr("src", filePath);		
					$("#download").attr("href", downloadPath);
					$("#audio").show();
					$("#download").show();
		        }
			});
		}
	};
	
	$('#uploadFile').click(function(){
	   $("input[type='file']").trigger('click');
	   
		   $('#announcementUpload').change(function() {
		        var filename = $('#announcementUpload').val();
		        filename = filename.split('\\');
		        console.log(filename);
		        $("#chkFileUpload").html('');
		        $("#showFileUpload").show();
		        $("#showFileUpload").html('<label class="labelTextGrey"><br/><span id="attachedFileName"></span></label>');
		        $("#audio").hide();
		        $("#download").hide();
		        $("#attachedFileName").html(filename[2]);
		    });
	});
	
	/*var chkAnnouncementNameExist = function(){
		
	};
	
	$("#announcementName").blur(function(){
		
	});*/
	
	if(!$("#announcementGroup").val()){
		$("#selectAnnouncement").prop("disabled", true);
		$("#newAnnouncement").prop("disabled", true);
	}else{
		$("#selectAnnouncement").prop("disabled", false);
		$("#newAnnouncement").prop("disabled", false);
	}
        
        
        
        //Code added @ 10 Jan 2019 regarding EX-1032
        //var groupSearchString = "";
        function decodString(encodedStr) {
            var parser = new DOMParser;
            var dom = parser.parseFromString(
                '<!doctype html><body>' + encodedStr,
                'text/html');
            var decodedString = dom.body.textContent;
            return decodedString;
        }
         $('#announcementGroups').on('keyup keypress keydown', function(e) {
              var keyCode = e.keyCode || e.which;
              //Code added @28 March 2018 to fix the issue EX-458
              if (keyCode === 38)
              {
            	  var tmpStrSearch = $("#announcementGroups").val();
                  var tmpStr = $.trim(tmpStrSearch);
                  $('#announcementGroups').val(decodString(tmpStr.replace('<span class="search_separator">-</span>', '-')));
              }
              
              if (keyCode === 40)
              { 
                    var tmpStrSearch = $("#announcementGroups").val();
                    var tmpStr = $.trim(tmpStrSearch);
                    $('#announcementGroups').val(decodString(tmpStr.replace('<span class="search_separator">-</span>', '-')));
              }
              if (keyCode === 9)
              { 
                        if($(".ui-state-focus").length){
                              var tmpStrs = $(".ui-state-focus").html();
                              var tmpStr =  $.trim(tmpStrs);
                        }else{
                                              var tmpStr = $.trim(groupSearchString);
                        }
                        groupSearchString = $.trim(tmpStr);
                        $(document).find(".ui-state-focus").addClass("tabGroup");
                        $('#announcementGroup').val(tmpStr);
                        $('#announcementGroups').val(decodString(tmpStr.replace('<span class="search_separator">-</span>', '-')));
                        //$('#announcementGroups').trigger("blur");
              }
              //End code
	      if (keyCode === 13) {
                        e.preventDefault();
                        //var searchVal = $("#announcementGroup").val(); //Commented on 28 March 2018 due to fix the issue EX-458
                        //Code added @28 March 2018 to fix the issue EX-458                                
                        var searchTmpNewStr = $("#announcementGroups").val();
                        var tmpNewStr = $.trim(searchTmpNewStr);
                        if($(".ui-state-focus").length) {
                               var tmpNewStrs = $(".ui-state-focus").html();
                               var tmpNewStr = $.trim(tmpNewStrs);
                         }else{
                               var tmpNewStr = $.trim(groupSearchString);

                         }
                        groupSearchString = tmpNewStr;
                        $('#announcementGroup').val(tmpNewStr);
                        var searchVal = tmpNewStr.replace('<span class="search_separator">-</span>', '-'); 
                        $('#announcementGroups').trigger('blur');
                        //End Code
                        if(searchVal != "") {            
                                var searchGroupName = $.trim(searchVal);                          
                                $('#announcementGroups').val(decodString(searchGroupName));
                                if(tmpNewStr.search('span') != -1) {
                                        var searchtmpNewStr = $.trim(tmpNewStr); 
                                        $('#announcementGroup').val(searchtmpNewStr);
                                        showSavedAnnouncement();
                                }
                                //$('#search').trigger('click');
                        }
                }
	    });
		
		$("#announcementGroups").blur(function(e){
        	var el = $(this).val(); 
        	if(el != "") {
        		var seachIdValue = el;
        		groupSearchString = $.trim(seachIdValue);
                }
        	if($(".ui-state-focus").length){
                        var searchValRes = $(".ui-state-focus").html();
                        var searchVal = $.trim(searchValRes);
                }else{
                        var searchVal = $.trim(groupSearchString);
                }
            
                if(searchVal.search('span') != -1) {
                        $('#announcementGroup').val(searchVal);
                }
                var groudVal = groupSearchString.replace('<span class="search_separator">-</span>', '-');
                if(groudVal != "") {
                        var groudVals = $.trim(groudVal);                          
                        $('#announcementGroups').val(decodString(groudVals));

                } else {
                         var groudValgroupSearchString = $.trim(groupSearchString);                          
                         $('#announcementGroups').val(decodString(groudValgroupSearchString));
                }   
					
        });
        $(document).on("click", ".groupAutoClass > li > a", function(){ 
                
    		var el = $(this);
    		var groupSearchStrings = el[0].innerHTML;
                var groupNameNId = decodString(el[0].innerHTML.replace('<span class="search_separator">-</span>', '-'));
                var tmpStr       = groupNameNId.split("-");
                var groupId      = tmpStr[0].trim();
    		$('#announcementGroups').val(decodString(el[0].innerHTML.replace('<span class="search_separator">-</span>', '-')));
    		var groupSearchString = $.trim(groupSearchStrings);
                $('#announcementGroup').val(groupId);
                showSavedAnnouncement();
	 });
        //End code
	
	
});



//Code added @ 10 Jan 2019 regarding EX-1032
       function showSavedAnnouncement()
       {
           $("#announcementDetails").hide();
		var autoComplete = new Array();
		var announcementGroupName = $("#announcementGroup").val();
		if(announcementGroupName){
			$("#grpName").val(announcementGroupName);
		}
		
		//alert(announcementGroupName);
		var html = '<option value="">Select Announcement</option>';
		$("#selectAnnouncement").html("");
		$.ajax({
			url: 'Announcement/getAnnouncement.php',
            type: 'POST',
            data: {announcementGroupName : announcementGroupName},
            success: function(data)
            {
            	var obj = jQuery.parseJSON(data);
            	if(obj.length > 0){
					for (var i = 0; i < obj.length; i++) {
						html += '<option value="'+ obj[i].announcement_id +'">' + obj[i].announcement_name + '</option>';
						//autoComplete[i] = obj[i].announcement_name;
					}
            	}else{
            		html = '<option value="">Announcement not available</option>';
            	}
				$("#selectAnnouncement").html(html);
				/*if(announcementId){
					$("#selectAnnouncement").val(announcementId);
					getAnnouncementInfo(announcementId);
		           	$("#announcementDetails").show();
				}else{
					$("#selectAnnouncement").val("");
				}*/
				$("#loading2").hide();
				$("#announcementList").show();
				if(!$("#announcementGroup").val()){
					$("#selectAnnouncement").prop("disabled", true);
					$("#newAnnouncement").prop("disabled", true);
				}else{
					$("#selectAnnouncement").prop("disabled", false);
					$("#newAnnouncement").prop("disabled", false);
				}
            }
            
		});
       }


        function getGroupList(selectedSP) {
        var autoComplete = new Array();
        $.ajax({
                    type: "POST",
                    url: "getGroupsSearchData.php",
                    data: {serviceProvider: selectedSP},
                    success: function (result) 
                    {
                        var explode = result.split(":");
                        for (var a = 0; a < explode.length; a++) {
                            autoComplete[a] = explode[a];
                        }
                        $("#announcementGroups").autocomplete({
                            source: autoComplete
                        }).data("ui-autocomplete")._renderItem = function (ul, item){
                                ul.addClass('groupAutoClass'); //Ul custom class here
                                if(item.value.search('<span class="search_separator">-</span>') > 0){
                                        return $("<li></li>")
                                        .append("<a href='#'>" + item.value + "</a>")
                                        .data("ui-autocomplete-item", item)
                                        .appendTo(ul);
                                }else{
                                        return $("<li class='ui-menu-item'></li>")
                                        .append("<a href='#'>" + item.label + "</a>")
                                        .data("ui-autocomplete-item", item)
                                        .appendTo(ul);						
                                 }
                        };
                    }
                });
        }
        //End code