<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();

	require_once("/var/www/lib/broadsoft/adminPortal/arrays.php");
	

	if(!array_key_exists("huntActivateNumber", $_POST)){
		$_POST['huntActivateNumber'] = "No";
	}

	$hidden = array("id", "huntPhoneActivateNumber");
	$required = array("hgName", "policy", "fwdSecs");

	$data = $errorTableHeader;

	$changes = 0;
	$error = 0;
	foreach ($_POST as $key => $value)
	{
		if (!in_array($key, $hidden)) //hidden form fields that don't change don't need to be checked or displayed
		{
			if($key == "huntActivateNumber" && $value !== $_SESSION["hg"][$key]){
				$changes++;
				$bg = CHANGED;
				if($value == "Yes"){
				    $var = $_SESSION["hg"]["phoneNumber"]." have been Activated";
				}else{
				    $var = $_SESSION["hg"]["phoneNumber"]." have been Deactivated";
				}
			}
			if ($key !== "hgAgents_1" and $value !== $_SESSION["hg"][$key])
			{
				$changes++;
				$bg = CHANGED;
			}
			else
			{
				$bg = UNCHANGED;
			}

			if ($key == "hgAgents_1")
			{
				if ($value != "NIL")
				{
					$changes++;
					$bg = CHANGED;
					if (strlen($value) > 0)
					{
						$exp = explode(";", $value);
						$v = "";
						unset($exp[count($exp) - 1]);

						$var = "";
						foreach ($exp as $k => $v)
						{
							$explode = explode(":", $v);
							$var .= $explode[1] . "<br>";
						}
					}
					else
					{
						$var = "None";
					}
				}
			}
			else
			{
				if (in_array($key, $required) and $value == "")
				{
					$error = 1;
					$bg = INVALID;
					$value = $_SESSION["hgNames"][$key] . " is a required field.";
				}
				else if ($key == "fwdTimeout")
				{
					if ($value == "true" and $_POST["fwdNum"] == "")
					{
						$error = 1;
						$bg = INVALID;
						$value = "If " . $_SESSION["hgNames"][$key] . " is true, forward number must be valid.";
					}
//					if ($value == "true" and $_POST["fwdSecs"] == "")
//					{
//						$error = 1;
//						$bg = INVALID;
//						$value = "If " . $_SESSION["hgNames"][$key] . " is true, forward seconds must be valid.";
//					}
				}
				else if ($key == "fwdNum")
				{
					if (strlen($value) > 11)
					{
						$error = 1;
						$bg = INVALID;
						$value = $_SESSION["hgNames"][$key] . " must be Extension or US Domestic only.";
					}
					foreach ($restrictedNumbers as $k => $v)
					{
						if (substr($value, 0, strlen($k)) == $k and strlen($value) > strlen($k) + 5)
						{
							$error = 1;
							$bg = INVALID;
							$value = $v . " is restricted.";
						}
					}
				}
				else if ($key == "hgName")
				{
					if (preg_match("[\"]", $value))
					{
						$error = 1;
						$bg = INVALID;
						$value = $_SESSION["hgNames"][$key] . " must not include double quotes.";
					}
				}
			}

			if ($bg != UNCHANGED and $value != "NIL")
			{
				$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["hgNames"][$key] . "</td><td class=\"errorTableRows\">";
				if (isset($var))
				{
					$data .= $var;
				}
				else if ($value)
				{
					$data .= $value;
				}
				else
				{
					$data .= "None";
				}
				$data .= "</td></tr>";
			}
		}
	}
	$data .= "</table>";

	if ($changes == 0)
	{
		$error = 1;
		$data = "<label class = \"labelTextGrey\">You have made no changes.</label>";
	}
	echo $error . $data;
?>
