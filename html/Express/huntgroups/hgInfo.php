<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$hgId = $_POST["hgId"];
	 $_SESSION["hgNames"] = array(
		"hgName" => "Name",
		"policy" => "Group Policy",
		"fwdTimeout" => "Forward No Answer",
		"fwdSecs" => "Forward Seconds",
		"fwdNum" => "Forward to Number",
		"allowCallWaitingForAgents" => "Allow Call Waiting on agents",
		"allowMembersToControlGroupBusy" => "Allow members to control Group Busy",
		"enableGroupBusy" => "Enable Group Busy",
		"hgAgents_1" => "Agents",
		"huntActivateNumber" => "Activate Number",
		"huntPhoneActivateNumber" => "Activate Phone Number"
	);
	 
 ?>
<script>
	$(function() {
		$("#hgTabs").tabs();
		$("#endUserId").html("<?php echo $hgId; ?>");

		$("#dialogHG").dialog({
			autoOpen: false,
			width: 800,
			modal: true,
			position: { my: "top", at: "top" },
			resizable: false,
			closeOnEscape: false,
			buttons: {
				"Complete": function() {
					pendingProcess.push("Modify Hunt Group");
					var dataToSend = $("#hgInfo").serialize();
					 $.ajax({
						type: "POST",
						url: "huntgroups/hgDo.php",
						data: dataToSend,
                                                beforeSend: function(){
                                                    //alert('Hello');
                                                    $("html, body").animate({ scrollTop: 0 }, 0); //Code added @07 Feb 2019
                                                },
						success: function(result) {
							if(foundServerConErrorOnProcess(result, "Modify Hunt Group")) {
		    					return false;
		                  	}
							$("#dialogHG").dialog("option", "title", "Request Complete");
						//	$(".ui-dialog-titlebar-close", this.parentNode).hide();
						//	$(".ui-dialog-buttonpane", this.parentNode).hide();
							$("#dialogHG").html(result);
						//	$("#dialogHG").append(returnLink);
							$(".ui-dialog-buttonpane button:contains('More Changes')").button().show().addClass('subButton');
							$(".ui-dialog-buttonpane button:contains('Return to Main')").button().show().addClass('subButton');
							$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
							$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
						}
					});
				},
				"Cancel": function() {
					$(this).dialog("close");
				},
				"More Changes": function() {					 
					  $("#loading2").show();   
					  huntGroupChangeMoreChange();
		              $(this).dialog("close"); 
		            },
	            "Return to Main": function() {
					location.href="main.php";
				}
			},
			open: function() {
				setDialogDayNightMode($(this));
				$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('completeButton');
				$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');			 
				$('.ui-dialog-buttonpane').find('button:contains("More Changes")').addClass('moreChangesButton');			 
				$('.ui-dialog-buttonpane').find('button:contains("Return to Main")').addClass('returnToMainButton');			 
			}
		});

		$("#subButtonHG").click(function() {
			var dataToSend = $("#hgInfo").serialize();
			$.ajax({
				type: "POST",
				url: "huntgroups/checkData.php",
				data: dataToSend,
                                beforeSend: function(){
                                    $("html, body").animate({ scrollTop: 0 }, 0); //Code added @07 Feb 2019
                                },
				success: function(result) {
					$("#dialogHG").dialog("open");
					if (result.slice(0, 1) == "1")
					{
						$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
						$(":button:contains('Cancel')").addClass("cancelButton");
					}
					else
					{
						$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled").addClass('subButton');
						$(":button:contains('Cancel')").addClass('cancelButton');
					}
					$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide();
					$(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');;
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');;
					result = result.slice(1);
					$("#dialogHG").html(result);
					 
				}
			});
		});

		var huntGroupChangeMoreChange = function(){
			var module = 'modHunt';
   			var hgNameToSend = $('#hgName').val();
			if(hgNameToSend !== "") {
			  	$.ajax({
       				type: "POST",
       				url: "navigate.php",
       				data: { module : module, hgNameToSend : hgNameToSend},
       				success: function(result) {
       					$("#hgData").html('');
       					$("#hgDetailsId").html(result); 
					}
				});
			}
			else {
				$("#endUserId").html("");
			}
		}
 
		
		$("#sortable1, #sortable2").sortable({
			placeholder: "ui-state-highlight",
			connectWith: ".connectedSortable",
			cursor: "crosshair",
			update: function(event, ui)
			{
				var monUsers = "";
				var order = $("#sortable2").sortable("toArray");
				for (i = 0; i < order.length; i++)
				{
					monUsers += order[i] + ";";
				}
				$("#hgAgents_1").val(monUsers);
			}
		}).disableSelection();
	});
	
		// tooltip
			$(document).ready(function(){
				$('[data-toggle="tooltip"]').tooltip();   
			});
</script>
<?php
	require_once("/var/www/lib/broadsoft/adminPortal/getHuntgroupInfo.php");
	$policies = array(
		"Circular" => "Circular",
		"Regular" => "Regular",
		"Simultaneous" => "Simultaneous",
		"Uniform" => "Uniform",
		"Weighted" => "Weighted"
	);
	//echo "<pre>"; print_r($_SESSION["hg"]);
	 
?>
<form name="hgInfo" id="hgInfo" method="POST" action="#" class="fcorn-registerTwo">
	<input class="form-control" type="hidden" name="id" id="id" value="<?php echo $hgId; ?>">
	<div id="hgTabs">
	
	<div class="row">
		
    			<div class="divBeforeUl">
            		<ul class="TabWidthHuntGroup">
            			<li><a href="#hgSettings_1" class="tabs" id="hgSettings">Settings</a></li>
            			<li><a href="#hgAgents_10" class="tabs" id="hgAgents">Agents</a></li>
            		</ul>
    			</div>
	</div>
	
	<div id="hgSettings_1" class="">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
			<h2 class="subBannerCustom"> Settings </h2>
			</div>
		</div>
	</div>
		<div class="row">
			<div class="">
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="hgName">Name</label><span class="required">*</span> 
						<input class="form-control" type="text" name="hgName" id="hgName" value="<?php echo $_SESSION["hg"]["hgName"]; ?>" size="35" maxlength="30">
					</div>
				</div>
				<?php if(!empty($_SESSION["hg"]["phoneNumber"])) {
				    ?>
    				<div class="col-md-6">
    					<div class="form-group">
    				 		<label class="labelText">Phone Number:</label><br/>
    						<span class="labelTextGrey phoneNumberMargin"> <?php echo $_SESSION["hg"]["phoneNumber"]; ?> </span>
    					</div>
    				</div>
				<?php }?>
			</div>
		</div>
					
					
		<div class="row">
			<div class="">
				<div class="col-md-6">
					<div class="form-group">
					
						<label  class="labelText" for="policy">Group Policy</label><span class="required">*</span> 
						<div class="dropdown-wrap">
						<select name="policy" id="policy">
							<option value=""></option>
							<?php
								foreach ($policies as $value)
								{
									if ($_SESSION["hg"]["policy"] == $value)
									{
										$sel = "SELECTED";
									}
									else
									{
										$sel = "";
									}
									echo "<option value=\"" . $value . "\"" . $sel . ">" . $value . "</option>";
								}
							?>
						</select>
							</div>						
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group">
						<div class="marginTopText">&nbsp;</div>
						<input type="hidden" name="fwdTimeout" value="false" />
						<input class="form-control" type="checkbox" name="fwdTimeout" id="fwdTimeout" value="true"<?php echo $_SESSION["hg"]["fwdTimeout"] == "true" ? " checked" : ""; ?>>
						<label for="fwdTimeout"><span></span></label>
						<label class="labelText" for="fwdTimeout">Forward No Answer:</label>
					</div>
				</div>
				
			</div>
		</div>
				
	
		
		
		<div class="row">
			<div class="">
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="fwdSecs">Forward Seconds</label> <span class="required">*</span>
						<div class="dropdown-wrap">
							<select name="fwdSecs" id="fwdSecs">
							<option value=""></option>
							<?php
								for ($a = 0; $a <= 7200; $a += 1)
								{
									if ($a == $_SESSION["hg"]["fwdSecs"])
									{
										$sel = "SELECTED";
									}
									else
									{
										$sel = "";
									}
									echo "<option value=\"" . $a . "\"" . $sel . ">" . $a . "</option>";
								}
							?>
						</select>	
							</div>					
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group">
					<label class="labelText" for="fwdNum">Forward to Number:</label>
					<input class="form-control" type="text" name="fwdNum" id="fwdNum" size="35" maxlength="30" value="<?php echo $_SESSION["hg"]["fwdNum"]; ?>" placeholder="non-011 only" />
					
					</div>
				</div>
			</div>
		</div>
		
		
		<?php if(!empty($_SESSION["hg"]["phoneNumber"]))
		{
	     ?>
			<div class="row">
				<div class="">
					
					<div class="col-md-6">
						<div class="form-group">
							
							<input type="checkbox" name="huntActivateNumber" id="huntActivateNumber" value="Yes" <?php if($_SESSION["hg"]["huntActivateNumber"] == "Yes"){ echo "checked";} ?> />
							<label for="huntActivateNumber"><span></span></label>
							<label class="labelText" for="fwdNum">Activate Number:</label>
							<input type="hidden" name="huntPhoneActivateNumber" id="huntPhoneActivateNumber" value="<?php echo $_SESSION["hg"]["phoneNumber"]; ?>"/>
							
						</div>
					</div>
				</div>	
			</div>
		<?php } ?>	
					
			<div class="row">
				<div class="">	
					<div class="col-md-6">
						<div class="form-group">
							
							<input type="hidden" name="allowCallWaitingForAgents" value="false" />
						<input type="checkbox" name="allowCallWaitingForAgents" id="allowCallWaitingForAgents" value="true"<?php echo $_SESSION["hg"]["allowCallWaitingForAgents"] == "true" ? " checked" : ""; ?> ><label for="allowCallWaitingForAgents"><span></span></label>
						<label class="labelText" for="fwdNum">Allow Call Waiting on agents:</label>
						</div>
					</div>
				</div>
			</div>
		
		
		<?php
		if ($ociVersion !== "17")
		{
		?>
		<div class="row">
			<div class="">
				<div class="col-md-6">
					<div class="form-group">
						
						<input type="hidden" name="allowMembersToControlGroupBusy" value="false" />
						<input type="checkbox" name="allowMembersToControlGroupBusy" id="allowMembersToControlGroupBusy" value="true"<?php echo $_SESSION["hg"]["allowMembersToControlGroupBusy"] == "true" ? " checked" : ""; ?> ><label for="allowMembersToControlGroupBusy"><span></span></label>
						<label class="labelText" for="fwdNum">Allow members to control Group Busy:</label>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="">
				<div class="col-md-6">
					<div class="form-group">
						
						<input type="hidden" name="enableGroupBusy" value="false" />
						<input type="checkbox" name="enableGroupBusy" id="enableGroupBusy" value="true"<?php echo $_SESSION["hg"]["enableGroupBusy"] == "true" ? " checked" : ""; ?> style="width:24px">
						<label for="enableGroupBusy"><span></span></label>
						<label class="labelText" for="fwdNum">Enable Group Busy:</label>
					</div>
				</div>
			</div>
		</div>
		<?php
		}
		?>
				
				
				
	</div>
	
		<!--end hgSettings_1 div-->
		<div id="hgAgents_10" class="">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
					<h2 class="adminUserText"> Agents </h2>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="">
					<div class="col-md-6" style="padding-left:0;">
						<div class="alignCenter">
							<label class="labelTextGrey">Available Agents <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="You can drag things back and forth"><img src="images/NewIcon/info_icon.png" /></a></label>
						</div>
								<div class="form-group scrollableListCustom1">
									<ul id="sortable1" class="connectedSortable connectedSortableCustom">
										<?php
										if (isset($_SESSION["hg"]["availableUsers"]))
										{
											for ($a = 0; $a < count($_SESSION["hg"]["availableUsers"]); $a++)
											{
													$id = $_SESSION["hg"]["availableUsers"][$a]["id"] . ":" . $_SESSION["hg"]["availableUsers"][$a]["name"];
												?>
												<li class="ui-state-default" id="<?php echo $id; ?>"><?php echo $_SESSION["hg"]["availableUsers"][$a]["name"]; ?></li>
										<?php
											}
										}
										?>
									</ul>
								</div>
								
							
						
					</div>
					<div class="col-md-6" style="padding-right:0;">
						<div class="alignCenter">
							<label class="labelTextGrey">Current Agents <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="You can drag things back and forth"><img src="images/NewIcon/info_icon.png" /></a></label>
						</div>	
								<div class="form-group scrollableListCustom2">
									<ul id="sortable2" class="connectedSortable connectedSortableCustom">
										<?php
										if (isset($_SESSION["hg"]["users"]))
										{
											for ($a = 0; $a < count($_SESSION["hg"]["users"]); $a++)
											{
												$id = $_SESSION["hg"]["users"][$a]["id"] . ":" . $_SESSION["hg"]["users"][$a]["name"];
											?>
												<li class="ui-state-highlight" id="<?php echo $id; ?>"><?php echo $_SESSION["hg"]["users"][$a]["name"]; ?></li>
										<?php
											}
										}
										?>
									</ul>
								</div>								
						
					</div>
					
				</div>
			</div>
			
			
		</div><!--end hgAgents_10 div-->
		<input type="hidden" name="hgAgents_1" id="hgAgents_1" value="NIL">
	</div>
	
	<div class="alignBtn"><input class="subButton" type="button" id="subButtonHG" value="Confirm Settings" onClick="window.location='#top'"></div>
</form>
<div id="dialogHG" class="dialogClass labelTextGrey"></div>