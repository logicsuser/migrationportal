<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
?>
<script>
$(function() {
	$("#module").html("> Modify Hunt Groups: ");
	$("#endUserId").html("");

	$("#hgChoice").change(function() {
		pendingProcess.push("Hunt Group");
		$("#hgData").html("");
		var id = $("#hgChoice").val();

		if (id !== "") {
			$.ajax({
				type: "POST",
				url: "huntgroups/hgInfo.php",
				data: { hgId: id },
				success: function(result) {
					if(foundServerConErrorOnProcess(result, "Hunt Group")) {
    					return false;
                  	}
					$("#hgData").html(result);
				}
			});
		}else {
			$("#endUserId").html("");
		}
	});
});

    var selectChoiceId = "<?php if(isset($_POST['hgNameToSend'])){echo $_POST['hgNameToSend']; }else{echo "";}?>";
    if(selectChoiceId){
    var trimmedVal = selectChoiceId.trim();
    	$("#hgChoice option:contains("+trimmedVal+")").attr('selected', 'selected');
    	$('#hgChoice').trigger("change");
	}
 
</script>

<div id="hgDetailsId">
<h2 class="adminUserText"> Choose Hunt Group to Modify </h2> 

<div class="adminUserForm">
	
		<form method="POST" name="hgMod" id="hgMod" class="fcorn-registerTwo">
			<div class="row">
				<div class="col-md-12 adminSelectDiv">
						<div class="form-group"> 
        					<label class="labelText">Hunt Group</label>
        					<div class="dropdown-wrap">
        						<select name="hgChoice" id="hgChoice" class="form-control selectBlue adminSelectBox">
        							<option value=""></option>
        							<?php
        								require_once("/var/www/lib/broadsoft/adminPortal/getAllHuntgroups.php");
        								if (isset($huntgroups))
        								{
        									foreach ($huntgroups as $key => $value)
        									{
        										echo "<option value=\"" . $value["id"] . "\">" . $value["name"] . "</option>";
        									}
        								}
        							?>
        						</select>
        					</div>
						</div>
				</div>
				
			</div>
		</form>
	<div class="row">
	
		<div class="">
			<div class="form-group">
				<div id="hgData"></div>
			</div>
		</div>
	</div>
	
</div>
</div>