<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
	require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php"); //Code added @ 23 Jan 2019
	$expProvDB = new ExpressProvLogsDB();
        
        

	if(!array_key_exists("huntActivateNumber", $_POST)){
		$_POST['huntActivateNumber'] = "No";
	}
	
	$hgId = $_POST["id"];
	$userId = "";
        
        //Code added @ 23 Jan 2019        
        $cLUObj = new ChangeLogUtility($hgId, $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
        //End code

	$xmlinput = xmlHeader($sessionid, "GroupHuntGroupModifyInstanceRequest");
	$xmlinput .= "<serviceUserId>" . $hgId . "</serviceUserId>";

	server_fail_over_debuggin_testing(); /* for fail Over testing. */
	
	foreach ($_POST as $key => $value)
	{
		if (($key !== "id" && $key !== "huntPhoneActivateNumber") and ((isset($_SESSION["hg"][$key]) and $value !== $_SESSION["hg"][$key]) or !isset($_SESSION["hg"][$key])))
		{
			if ($key == "hgName")
			{
				$xmlinput .= "<serviceInstanceProfile>
							<name>" . htmlspecialchars($value) . "</name>
						</serviceInstanceProfile>";
			}
			if ($key == "policy")
			{
				$xmlinput .= "<policy>" . $value . "</policy>";
			}
			if ($key == "fwdTimeout")
			{
				$xmlinput .= "<forwardAfterTimeout>" . $value . "</forwardAfterTimeout>";
			}
			if ($key == "fwdSecs" and strlen($value) > 0)
			{
				$xmlinput .= "<forwardTimeoutSeconds>" . $value . "</forwardTimeoutSeconds>";
			}
			if ($key == "fwdNum")
			{
				if ($value == "")
				{
					$xmlinput .= "<forwardToPhoneNumber xsi:nil=\"true\" />";
				}
				else
				{
					$xmlinput .= "<forwardToPhoneNumber>" . $value . "</forwardToPhoneNumber>";
				}
			}
			if ($key == "hgAgents_1")
			{
				if (isset($_SESSION["hg"]["users"]))
				{
					$oldList = "";
					for ($a = 0; $a < count($_SESSION["hg"]["users"]); $a++)
					{
						$oldList .= $_SESSION["hg"]["users"][$a]["name"] . "; ";
					}
				}

	 			if ($value !== "" and $value !== "NIL")
				{
					$exp = explode(";", $value);
					$newList = "";
					foreach ($exp as $v)
					{
						if ($v !== "hgAgents_1" and $v !== "")
						{
							$exp2 = explode(":", $v);
							$newList .= $exp2[1] . "; ";
							$userId .= "<userId>" . $exp2[0] . "</userId>";
						}
					}
					$xmlinput .= "<agentUserIdList>";
					$xmlinput .= $userId;
					$xmlinput .= "</agentUserIdList>";
				}
				if ($value == "")
				{
					$AGxmlinput = xmlHeader($sessionid, "GroupHuntGroupModifyInstanceRequest");
					$AGxmlinput .= "<serviceUserId>" . $hgId . "</serviceUserId>";
					$AGxmlinput .= "<agentUserIdList xsi:nil=\"true\"></agentUserIdList>";
					$AGxmlinput .= xmlFooter();
					$AGresponse = $client->processOCIMessage(array("in0" => $AGxmlinput));
					$AGxml = new SimpleXMLElement($AGresponse->processOCIMessageReturn, LIBXML_NOWARNING);
				}
			}

			if ($value != "NIL")
			{
				if (isset($oldList))
				{
					$changes[$key]["oldValue"] = substr($oldList, 0, -2);
				}
				else if (isset($_SESSION["hg"][$key]))
				{
					$changes[$key]["oldValue"] = $_SESSION["hg"][$key];
				}
				else
				{
					$changes[$key]["oldValue"] = "None";
				}

				if (isset($newList))
				{
					$changes[$key]["newValue"] = substr($newList, 0, -2);
				}
				else if ($value)
				{
					$changes[$key]["newValue"] = $value;
				}
				else
				{
					$changes[$key]["newValue"] = "None";
				}
			}
		}
		//code to activate number request
		if($key == "huntActivateNumber"){
			
			if (isset($_SESSION["hg"][$key]) && $value !== $_SESSION["hg"][$key]){
				
				if($_SESSION["hg"][$key] == "No"){
					$oldValue = "Deactivated";
				}else{
					$oldValue = "Activated";
				}
				
				if($value == "No"){
					$newValue = "Deactivated";
				}else{
					$newValue = "Activated";
				}
				
				$changes[$key]["oldValue"] = $oldValue;
				$changes[$key]["newValue"] = $newValue;
				$huntDn = new Dns ();
				$dnPostArray = array(
						"sp" => $_SESSION['sp'],
						"groupId" => $_SESSION['groupId'],
						"phoneNumber" => array($_POST['huntPhoneActivateNumber'])
				);
				if(!empty($_POST[$key])){
					$dnsActiveResponse = $huntDn->activateDNRequest($dnPostArray);
				}else{
					$dnsActiveResponse = $huntDn->deActivateDNRequest($dnPostArray);
				}
			}
		}
		
	}

	$xmlinput .= "<allowCallWaitingForAgents>" . $_POST["allowCallWaitingForAgents"] . "</allowCallWaitingForAgents>";
	if ($ociVersion !== "17")
	{
		$xmlinput .= "<allowMembersToControlGroupBusy>" . $_POST["allowMembersToControlGroupBusy"] . "</allowMembersToControlGroupBusy>";
		$xmlinput .= "<enableGroupBusy>" . $_POST["enableGroupBusy"] . "</enableGroupBusy>";
	}
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	readError($xml);

	echo "<div class=\"labelTextGrey textAlignCenter\" style=\"font-size:14px;\">The following changes are complete:</div><br/><br/>";

	$date = date("Y-m-d H:i:s");
        
        if(isset($changes))
        {            
            $module             = "Hunt Group Modify";
            $tableName          = "huntgroupModChanges";
            $entityName         = addslashes($_SESSION["hg"]["hgName"]);
            $cLUObj->changesArr = $changes;     
            $changeResponse     = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName); 
        }
        

        /*
	$query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
	$query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', 'Hunt Group Modify', '" . $_SESSION["groupId"] . "', '" . addslashes($_SESSION["hg"]["hgName"]) . "')";

	$sth = $expProvDB->expressProvLogsDb->query($query);
	$lastId = $expProvDB->expressProvLogsDb->lastInsertId();
	
	echo "<ul class=\"uiDialogLabel labelTextGrey\">";
	foreach ($changes as $key => $value)
	{
		$insert = "INSERT into huntgroupModChanges (id, serviceId, entityName, field, oldValue, newValue)";
		$values = " VALUES ('" . $lastId . "', '" . $hgId . "', '" . addslashes($_SESSION["hg"]["hgName"]) . "', '" . $_SESSION["hgNames"][$key] . "', '" . addslashes($value["oldValue"]) . "', '" . addslashes($value["newValue"]) . "')";
		$query = $insert . $values;
		$sth = $expProvDB->expressProvLogsDb->query($query);
		if($key !== "huntActivateNumber"){
			echo "<li>" . $_SESSION["hgNames"][$key] . " has changed from " . $value["oldValue"] . " to " . $value["newValue"] . ".</li>";
		}else{
			echo "<li>" . $_SESSION["hg"]["phoneNumber"]. " have been changed from " . $value["oldValue"] . " to " . $value["newValue"] . ".</li>";
		}
	}
	echo "</ul>";
        */
        echo "<ul class=\"uiDialogLabel labelTextGrey\">";
	foreach ($changes as $key => $value)
	{
		if($key !== "huntActivateNumber"){
			echo "<li>" . $_SESSION["hgNames"][$key] . " has changed from " . $value["oldValue"] . " to " . $value["newValue"] . ".</li>";
		}else{
			echo "<li>" . $_SESSION["hg"]["phoneNumber"]. " have been changed from " . $value["oldValue"] . " to " . $value["newValue"] . ".</li>";
		}
	}
	echo "</ul>";
?>
