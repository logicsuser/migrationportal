<?php
$settings = parse_ini_file("/var/www/adminPortal.ini", TRUE);
$db = new PDO("mysql:host=" . $settings["adminPortalDatabase"]["host"] . ";dbname=" . $settings["adminPortalDatabase"]["database"] . ";charset=utf8",
    $settings["adminPortalDatabase"]["username"],
    $settings["adminPortalDatabase"]["password"]
);

//	$sugarDB = new PDO("mysql:host=" . $settings["sugarDatabase"]["host"] . ";dbname=" . $settings["sugarDatabase"]["database"] . ";charset=utf8", $settings["sugarDatabase"]["username"], $settings["sugarDatabase"]["password"]);
?>
