<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/help.php");

//echo "<pre>"; print_r($_POST);

$helpUrlArray = array(
	"main" => "main",	
	"users" => "users",	
	"userAdd" => "addUser",	
	"bulk" => "addBulk",	
	"adminUser" => "admins",	
	"groupModify" => "groups",	
	"userMod" => "modUser",	
	"modHunt" => "modHuntGrp",	
	"modAA" => "modAA",	
	"modCallCenter" => "modCC",	
	"modSched" => "schedules",	
	"findMac" => "findMAC",	
	"dids" => "viewNumb",	
	"changeLog" => "viewChangeLog",	
	"cdrs" => "viewCDR",	
	"siteReport" => "siteReport",
	"modGrpCallPickup" => "callPickupGroup"
);

if(isset($_POST['adminType']) && !empty($_POST['adminType'])){
	$adminType = $_POST['adminType']; 
}

if(isset($_POST['module']) && !empty($_POST['module'])){
	if($_POST['module'] == "userAdd"){
		$modId = "addUser";
	}else if($_POST['module'] == "groupModify"){
		$modId = "groups";
	}else if($_POST['module'] == "modGrpCallPickup"){
		$modId = "callPickupGroup";
	}else if($_POST['module'] == "cdrs"){
		$modId = "viewCDR";
	}else if($_POST['module'] == "adminUser"){
		$modId = "admins";
	}else if($_POST['module'] == "modSched"){
		$modId = "schedules";
	}else if($_POST['module'] == "bulk"){
		$modId = "addBulk";
	}else if($_POST['module'] == "userMod"){
		$modId = "modUser";
	}else if($_POST['module'] == "dids"){
		$modId = "viewNumb";
	}else if($_POST['module'] == "changeLog"){
		$modId = "viewChangeLog";
	}else if($_POST['module'] == "siteReport"){
		$modId = "siteManagement";
	}else if($_POST['module'] == "modDevice"){
	    $modId = "deviceInventory";
	}else{
		$modId = $_POST['module'];
	}
	 
}

if(isset($_POST['tabId']) && !empty($_POST['tabId'])){
	$tabId = ucfirst($_POST['tabId']);
	if($tabId == "Device_search_button"){
		$tabId = "DeviceSearch";
	}else if($tabId == "ScaOnly_devices"){
		$tabId = "scaOnlydevices";
	}else if($tabId == "Algo_8180" || $tabId == "Algo_8301"){
		$tabId = "algoAlerter";
	}else if($tabId == "Algo_8028"){
		$tabId = "algoAudiodoor";
	}else if($tabId == "Algo_8128"){
		$tabId = "algoStrobe";
	}else if($tabId == "AUDIO"){
		$tabId = "audioCodes";
	}else if($tabId == "Polycom_VVX310" || $tabId == "Polycom_VVX410" || $tabId == "Polycom_VVX500" || $tabId == "Polycom_VVX600"){
		$tabId = "polycom";
	}else if($tabId == "ServiceLicenses"){
		$tabId = "broadSoftLicenseReport";
	}
}else{
	$tabId = "";
}
if($tabId == "Device_search_button"){
    $tabId = "deviceSearch";
}
if($tabId == "ScaOnly_devices"){
    $tabId = "scaOnly_devices";
}

if(!empty($modId) && !empty($adminType)){
	
	$help = new Help();
	$helpUrl = $help->getHelpFile($adminType, $modId, $tabId);
	
	$filePath = "/var/www/html/helpUrl/".$helpUrl.".htm";
	if(file_exists($filePath)){
		$filePath = $filePath;
	}else{
		$helpUrl = $help->getHelpFile("superUser", $modId, $tabId);
		$filePath = "/var/www/html/helpUrl/".$helpUrl.".htm";
	}
	$helpResponse = array();
	$helpUrl1 = $help->getHelpFile($adminType, $modId, $tabId);
	$videoPath = "/var/www/html/help-video/".$helpUrl1.".mp4";
	
	if(file_exists($videoPath)){
		
		$matchesVideo= $helpUrl1;
		$helpResponse["Video"] = $helpUrl1;
	}else{
		$videoPath= $help->getHelpFile("superUser", $modId, $tabId);
		$matchesVideo= "/var/www/html/help-video/".$videoPath.".mp4";
		if(file_exists($matchesVideo)){
			$helpResponse["Video"] = $videoPath;
		}else{
			$helpResponse["Video"] = "false";
		}
		
	}
	//$videoPath = "/var/www/html/helpUrl/video/".$helpUrl.".mp4";
	//$matchesVideo = glob("/var/www/html/help-video/".$helpUrl."*.*");
	//print_r($matchesVideo);
	
	$helpResponse["Success"] ="true";
	$helpResponse["FileName"] =$helpUrl;
	
	if(file_exists($filePath)){		
		echo json_encode($helpResponse);
	}else{
		$helpResponse["Success"] ="false";
		echo json_encode($helpResponse);
	}
	
	//echo "<pre>"; print_r($helpUrl);
	
}else{
	echo json_decode("error");
}
?>