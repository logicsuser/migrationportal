<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
$_SESSION ["dialPlanPolicyDialogContent"] = array (
    "accessCode"                  => "Access Code",
    "description"             => "Description",
    "includeCodeForNetworkTranslationsAndRouting"     => "Include Code for Network Translations and Routing",
    "includeCodeForScreeningServices"      => "Include Code for Screening Services",
    "enableSecondaryDialTone"      => "Enable Secondary Dial Tone"
);
$data = $errorTableHeader;
$error = 0;
//print_r($_POST);
//print_r($_SESSION["allResp"]["dialPlanAccessCodeList"]);
$changeDialAccessCodeArray = array();
foreach ($_SESSION["allResp"]["dialPlanAccessCodeList"] as $key => $value){
    if($value["accessCode"] == $_POST["accessCode"]){
        if($value["description"] != $_POST["description"]){
            $changeDialAccessCodeArray[$_POST["accessCode"]]["description"] = $_POST["description"];
        }
        if($value["includeCodeForNetworkTranslationsAndRouting"] != $_POST["includeCodeForNetworkTranslationsAndRouting"]){
            $changeDialAccessCodeArray[$_POST["accessCode"]]["includeCodeForNetworkTranslationsAndRouting"] = $_POST["includeCodeForNetworkTranslationsAndRouting"];
        }
        if($value["includeCodeForScreeningServices"] != $_POST["includeCodeForScreeningServices"]){
            $changeDialAccessCodeArray[$_POST["accessCode"]]["includeCodeForScreeningServices"] = $_POST["includeCodeForScreeningServices"];
        }
        if($value["enableSecondaryDialTone"] != $_POST["enableSecondaryDialTone"]){
            $changeDialAccessCodeArray[$_POST["accessCode"]]["enableSecondaryDialTone"] = $_POST["enableSecondaryDialTone"];
        }
    }
}
if(count($changeDialAccessCodeArray) > 0){
    foreach ($changeDialAccessCodeArray as $key1 => $value1)
    {
        foreach ($value1 as $key => $value){
            $bg = CHANGED;
            
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:" . $bg . ";\">" . $_SESSION ["dialPlanPolicyDialogContent"][$key] . "</td><td class=\"errorTableRows\">";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $data .= $v . "<br>";
                }
            }
            else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            $data .= "</td></tr>";
        }
        
    }
}else{
    $error = 1;
    $bg = INVALID;
    $data .= "<tr style=\"border:1px solid #fff;color:#fff;vertical-align:middle;\"><td class='errorTableRows' style='background:#72ac5d;'>Enterprise Changes</td><td style='width:50%;' class='errorTableRows'>No changes.</td></tr>";
    //$data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:10px;'>Enterprised Changes</td><td style='width:50%;'>No changes.</td></tr>";
    
}

$data .= "</table>";
echo $error . $data;
?>