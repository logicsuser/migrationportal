<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/enterprise/enterpriseOperations.php");
require_once ("/var/www/html/Express/enterprise/services/ServicesOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
require_once("/var/www/html/Express/enterprise/administrator/AdministratorOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
$expProvDB = new ExpressProvLogsDB();
$obj = new EnterpriseOperations();
$finalChangeLogArray = array();
$modifyResponse = "";

    $entPermSecurityDomainsLogArr              = explode("_", $_SESSION["permissions"]["entPermSecurityDomains"]);
    $entPermSecurityDomainsLogPermission       = $entPermSecurityDomainsLogArr[0];
    $entPermSecurityDomainsLogModPermission    = $entPermSecurityDomainsLogArr[1];
    
    $entPermDomainsLogArr                      = explode("_", $_SESSION["permissions"]["entPermDomains"]);
    $entPermDomainsLogPermission               = $entPermDomainsLogArr[0];
    $entPermDomainsLogModPermission            = $entPermDomainsLogArr[1];

//$_SESSION["allResp"]["basicInfo"] = $obj->getEnterpriseInfo($_POST["basicInfo"]["enterpriseId"], $ociVersion);

if($_POST["cpPolicy"]["conferenceURI"] == ""){
    $_POST["cpPolicy"]["conferenceURI"] = "";
}else{
    $_POST["cpPolicy"]["conferenceURI"] = $_POST["cpPolicy"]["conferenceURI"]."@".$_POST["cpPolicy"]["conferenceURIDomain"];
}
unset($_POST["cpPolicy"]["conferenceURIDomain"]);
//Domain code start
$domainChangesAssign = array();
$stringDomainAssigned = explode(";", $_POST["domainAssigned"]);
$stringSecurityDomainAssigned = explode(";", $_POST["securityDomainAssigned"]);

$_POST["domains"]["domainAssigned"] = $stringDomainAssigned;
$_POST["domains"]["securityDomainAssigned"] = $stringSecurityDomainAssigned;

/* Administrator Start */
$modifiedAdmins = isset($_SESSION['modifiedAdmins']) ? $_SESSION['modifiedAdmins'] : array();
$addedAdmins = isset($_SESSION['addedAdmins']) ? $_SESSION['addedAdmins'] : array();
$deleteAdmins = array();

server_fail_over_debuggin_testing(); /* for fail Over testing. */

if( isset($_POST['deleteAdmin']) || (isset($_SESSION['deleteAdmins']) && count($_SESSION['deleteAdmins']) > 0) ) {
    if(isset($_POST['deleteAdmin'])) {
        foreach ($_POST['deleteAdmin'] as $adminKey => $adminValue) {
            $deleteAdmins[] = $adminKey;
        }
    }
    if(isset($_SESSION['deleteAdmins']) && count($_SESSION['deleteAdmins']) > 0) {
        $deleteAdmins = array_merge($deleteAdmins, $_SESSION['deleteAdmins']);
    }
    foreach($deleteAdmins as $adminKey => $adminValue) {
        if( isset($modifiedAdmins[$adminValue]) ) {
            unset($modifiedAdmins[$adminValue]);
        }
    }
    foreach($addedAdmins as $addKey => $addValue) {
        if( in_array($addValue['administratorId'], $deleteAdmins) ) {
            unset($addedAdmins[$addKey]);
            unset($deleteAdmins[array_search($addValue['administratorId'], $deleteAdmins)]);
        }
    }
}
/* Administrator End*/

/* DNs Start*/
$unassignedDNs = array();
$unassignedDNs["numbers"] = array();
//$unassignedDNs["numbersRanges"] = "";

if( isset($_POST['unassignNumber']) && $_POST['unassignNumber'] != "" ) {
    $unassignNumber = explode(";;", rtrim($_POST['unassignNumber'], ";;"));
    $unassignedDNs['numbers'] = array_merge($unassignedDNs["numbers"], $unassignNumber);
}

/*  if( isset($_SESSION['enterpriseDns']['removedDnNumber']) && count(isset($_SESSION['enterpriseDns']['removedDnNumber'])) > 0 ) {
        $unassignedDNs['numbers'] = array_merge($unassignedDNs["numbers"], $_SESSION['enterpriseDns']['removedDnNumber']);
}
    if( isset($_SESSION['enterpriseDns']['removedDnsRanges']) && count($_SESSION['enterpriseDns']['removedDnsRanges']) > 0) {
        $unassignedDNs['numbersRanges'] = $_SESSION['enterpriseDns']['removedDnsRanges'];
} */

/* DNs End*/


$assignedDomains = array();
if($entPermDomainsLogModPermission == "yes"){
    $arrayDiff1 =  flip_isset_diff($_POST["domains"]["domainAssigned"], $_SESSION["allResp"]["domains"]["domainAssigned"]);

    $arrayDiff2 = flip_isset_diff($_SESSION["allResp"]["domains"]["domainAssigned"], $_POST["domains"]["domainAssigned"]);
    if(count($arrayDiff1) > 0 && !empty($arrayDiff1[0])){
        $assignedDomains["domains"]["newAssignedDomains"] = $arrayDiff1;
    }
    if(count($arrayDiff2) > 0 && !empty($arrayDiff2[0])){
        $assignedDomains["domains"]["unAssignedDomains"] = $arrayDiff2;
    }
}



if($entPermSecurityDomainsLogModPermission == "yes"){
    $arrayDiff3 =  flip_isset_diff($_POST["domains"]["securityDomainAssigned"], $_SESSION["allResp"]["domains"]["securityDomainAssigned"]);
    $arrayDiff4 = flip_isset_diff($_SESSION["allResp"]["domains"]["securityDomainAssigned"], $_POST["domains"]["securityDomainAssigned"]);
    if(count($arrayDiff3) > 0 && !empty($arrayDiff3[0])){
        $assignedDomains["domains"]["newAssignedSecurityDomains"] = $arrayDiff3;
    }
    if(count($arrayDiff4) > 0 && !empty($arrayDiff4[0])){
        $assignedDomains["domains"]["unAssignedSecurityDomains"] = $arrayDiff4;
    }
}
//Domain code end

//cps code
$cpsChangeArray = array();
foreach ($_POST["cpPolicy"] as $key => $value){
    if($value != $_SESSION["allResp"]["callProcessingPolicy"]["Success"][$key]){
        $cpsChangeArray[$key] = $value;
        $cpsChangeLogArray[$key]["oldValue"] = $_SESSION["allResp"]["callProcessingPolicy"]["Success"][$key];
        $cpsChangeLogArray[$key]["newValue"] = $value;
    }
}
//cps code

//voice messaging
$voiceMessegingChangeArray = array();
$voiceMessegingChangeLogArray = array();
foreach ($_POST["voiceMessaging"] as $key => $value){
    if($value != $_SESSION["allResp"]["voiceMessaging"]["Success"][$key]){
        $voiceMessegingChangeArray[$key] = $value;
        $voiceMessegingChangeLogArray[$key]["oldValue"] = $_SESSION["allResp"]["voiceMessaging"]["Success"][$key];
        $voiceMessegingChangeLogArray[$key]["newValue"] = $value;
    }
}
//voice messaging

$ncsDataValidation = array();
$ncsChangeLogArray = array();
foreach ($_POST["assignedNCS"] as $key => $value){
    if(!in_array($value, $_SESSION["allResp"]["ncsData"]["entData"])){
        $ncsDataValidation["assigned"][] = $value;
        $ncsChangeLogArray["networkClassOfServices"]["oldValue"] = implode(", ",$_SESSION["allResp"]["ncsData"]["entData"]);
        $ncsChangeLogArray["networkClassOfServices"]["newValue"] = implode(", ",$_POST["assignedNCS"]);
    }
}

foreach ($_SESSION["allResp"]["ncsData"]["entData"] as $key => $value){
    if(!in_array($value, $_POST["assignedNCS"])){
        $ncsDataValidation["unassigned"][] = $value;
        $ncsChangeLogArray["networkClassOfServices"]["oldValue"] = implode(", ",$_SESSION["allResp"]["ncsData"]["entData"]);
        $ncsChangeLogArray["networkClassOfServices"]["newValue"] = implode(", ",$_POST["assignedNCS"]);
    }
}

foreach ($_POST["assignedDefault"] as $key => $value){
    if(!in_array($value, $_SESSION["allResp"]["ncsData"]["defaultData"])){
        $ncsDataValidation["default"][] = $value;
        $ncsChangeLogArray["defaultNetworkClassOfServices"]["oldValue"] = $_SESSION["allResp"]["ncsData"]["defaultData"][0];
        $ncsChangeLogArray["defaultNetworkClassOfServices"]["newValue"] = $_POST["assignedDefault"][0];
    }
}

//routing Profile
$routingProfileChanges = array();
$routingChangeLogs = array();
if(isset($_POST["routingProfile"]["routingProfile"])){
    if($_POST["routingProfile"]["routingProfile"] != $_SESSION["allResp"]["selectedRoutingProfile"]["Success"]){
        $routingProfileChanges["routingProfile"] =  $_POST["routingProfile"]["routingProfile"];
        $routingChangeLogs["routingProfile"]["oldValue"] = $_SESSION["allResp"]["selectedRoutingProfile"]["Success"];
        $routingChangeLogs["routingProfile"]["newValue"] = $_POST["routingProfile"]["routingProfile"];
    }
}
if($_POST["sipPassword"]["endpointAuthenticationLockoutType"] == "Temporary"){
    if($_POST["sipPassword"]["endpointAuthenticationLockoutTypeCheckbox"] == "true"){
        $_POST["sipPassword"]["endpointAuthenticationLockoutType"] = "Temporary Then Permanent";
        unset($_POST["sipPassword"]["endpointAuthenticationLockoutTypeCheckbox"]);
    }else{
        $_POST["sipPassword"]["endpointAuthenticationLockoutType"] = "Temporary";
        unset($_POST["sipPassword"]["endpointAuthenticationLockoutTypeCheckbox"]);
    }
}
unset($_POST["sipPassword"]["endpointAuthenticationLockoutTypeCheckbox"]);

if($_POST["sipPassword"]["trunkGroupAuthenticationLockoutType"] == "Temporary"){
    if($_POST["sipPassword"]["trunkGroupAuthenticationLockoutTypeCheckbox"] == "true"){
        $_POST["sipPassword"]["trunkGroupAuthenticationLockoutType"] = "Temporary Then Permanent";
        unset($_POST["sipPassword"]["trunkGroupAuthenticationLockoutTypeCheckbox"]);
    }else{
        $_POST["sipPassword"]["trunkGroupAuthenticationLockoutType"] = "Temporary";
        unset($_POST["sipPassword"]["trunkGroupAuthenticationLockoutTypeCheckbox"]);
    }
}
unset($_POST["sipPassword"]["trunkGroupAuthenticationLockoutTypeCheckbox"]);

$sipPasswordRulesChange = array();
$sipPasswordRulesChangeLog = array();
foreach ($_POST["sipPassword"] as $sipKey => $sipPass){
    if($sipPass != $_SESSION["allResp"]["sipPasswordRules"]["Success"][$sipKey]){
        $sipPasswordRulesChange[$sipKey] = $sipPass;
        $sipPasswordRulesChangeLog[$sipKey]["oldValue"] = $_SESSION["allResp"]["sipPasswordRules"]["Success"][$sipKey];
        $sipPasswordRulesChangeLog[$sipKey]["newValue"] = $sipPass;
    }
}


//device password
if($_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutType"] == "Temporary"){
    if($_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutTypeChk"] == "true"){
        $_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutType"] = "Temporary Then Permanent";
        unset($_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutTypeChk"]);
    }else{
        $_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutType"] = "Temporary";
        unset($_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutTypeChk"]);
    }
}
unset($_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutTypeChk"]);

$devicePasswordRulesChange = array();
$devicePasswordRulesChangeLog = array();
foreach ($_POST["devicePasswordRules"] as $devpKey => $devpPass){
    if($devpPass != $_SESSION["allResp"]["devicePasswordRules"]["Success"][$devpKey]){
        $devicePasswordRulesChange[$devpKey] = $devpPass;
        $devicePasswordRulesChangeLog[$devpKey]["oldValue"] = $_SESSION["allResp"]["sipPasswordRules"]["Success"][$devpKey];
        $devicePasswordRulesChangeLog[$devpKey]["newValue"] = $devpPass;
    }
}
//device password

//Basic Info Modify
$changes = array();
$changesVoicePortal = array() ;
$basicInfoChanges = array();
if(empty($_SESSION["allResp"]["basicInfo"]["Error"])){
    foreach ($_POST["basicInfo"] as $key => $value){
        if($value != $_SESSION["allResp"]["basicInfo"]["Success"][$key]){
            $basicInfoChanges[$key] = $value;
            $changes[$key]["oldValue"] = $_SESSION["allResp"]["basicInfo"]["Success"][$key];
            $changes[$key]["newValue"] = $value;
        }
    }
}
//echo "case1-";print_r($basicInfoChanges);

//Code added @06 June 2019
if(empty($_SESSION["allResp"]["voicePortal"]["Error"])){
    foreach ($_POST["voicePortal"] as $key => $value){
        if($value =="Enterprise"){
                $value = "Service Provider";
            }
        if($value != $_SESSION["allResp"]["voicePortal"]["Success"][$key]){
            $voicePortalChanges[$key] = $value;
            $changesVoicePortal[$key]["oldValue"] = $_SESSION["allResp"]["voicePortal"]["Success"][$key];
            $changesVoicePortal[$key]["newValue"] = $value;
            
            
            
        }
    }
}
//End code

$passwordRulesChange = array();
$passwordRulesChangeLog = array();
foreach($_POST["password"] as $passKey => $passVal){
    if($passVal != $_SESSION["allResp"]["passwordRules"]["Success"][$passKey]){
        $passwordRulesChange[$passKey] = $passVal;
        $passwordRulesChangeLog[$passKey]["oldValue"] = $_SESSION["allResp"]["passwordRules"]["Success"][$passKey];
        $passwordRulesChangeLog[$passKey]["newValue"] = $passVal;
    }
}

$passcodeRulesChange = array();
$passcodeRulesChangeLog = array();
foreach($_POST["passcode"] as $passKey => $passVal){
    if($passVal != $_SESSION["allResp"]["passcodeRules"]["Success"][$passKey]){
        $passcodeRulesChange[$passKey] = $passVal;
        $passcodeRulesChangeLog[$passKey]["oldValue"] = $_SESSION["allResp"]["passcodeRules"]["Success"][$passKey];
        $passcodeRulesChangeLog[$passKey]["newValue"] = $passVal;
    }
}

$dialPlanDataChange = array();
$dialPlanDataChangeLog = array();
foreach ($_POST["dialPolicy"] as $dKey => $dValue){
    if($dValue != $_SESSION["allResp"]["dialPlanData"]["Success"][$dKey]){
        $dialPlanDataChange[$dKey] = $dValue;
        $dialPlanDataChangeLog[$dKey]["oldValue"] = $_SESSION["allResp"]["dialPlanData"]["Success"][$dKey];
        $dialPlanDataChangeLog[$dKey]["newValue"] = $dValue;
    }
}

if(count($basicInfoChanges) > 0){
    $basicInfoChanges["serviceProviderId"] = $_POST["basicInfo"]["enterpriseId"];
    $modResp = $obj->modifyEnterprise($basicInfoChanges);
    if(!empty($modResp["Error"])){
        echo "<p style='text-align:center;color:red;'>".$modResp["Error"]."</p>";
    }else{
        echo "<p style='text-align:center;'>Basic information updated succesfully.</p>";
        $module         = "Enterprise Modify";
        $tableName      = "enterpriseModifyChanges";
        $entityName     = $_POST["basicInfo"]["enterpriseId"];
        $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
        $cLUObj->changesArr = $changes;
        $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
    }
}


if(count($voicePortalChanges) > 0){
    $voicePortalChanges["serviceProviderId"] = $_POST["basicInfo"]["enterpriseId"];
    $modResp = $obj->serviceProviderVoicePortalModifyRequest($voicePortalChanges);
    if(!empty($modResp["Error"])){
        echo "<p style='text-align:center;color:red;'>".$modResp["Error"]."</p>";
    }else{
        echo "<p style='text-align:center;'>Voice Portal updated succesfully.</p>";
        $module         = "Enterprise Modify";
        $tableName      = "enterpriseModifyChanges";
        $entityName     = $_POST["basicInfo"]["enterpriseId"];
        $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
        $cLUObj->changesArr = $changesVoicePortal;
        $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
    }
}

if(count($_SESSION["domainsAdd"]) > 0){
    foreach ($_SESSION["domainsAdd"] as $key => $value){
        $domainAddResp = $obj->addDomainsToSystem($value);
        $changesAddDomain[$key]["oldValue"] = "";
        $changesAddDomain[$key]["newValue"] = $value;
        if(!empty($domainAddResp["Error"])){
            echo "<p style='text-align:center;color:red;'>".$domainAddResp["Error"]."</p>";
        }else{
            echo "<p style='text-align:center;'>Domain ".$value." added succesfully.</p>";
            $module         = "Enterprise Modify";
            $tableName      = "enterpriseModifyChanges";
            $entityName     = $_POST["basicInfo"]["enterpriseId"];
            $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
            $cLUObj->changesArr = $changesAddDomain;
            $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
        }
    }
}

if(count($_SESSION["securityDomainsAdd"]) > 0){
    foreach ($_SESSION["securityDomainsAdd"] as $key => $value){
        $domainAddResp = $obj->addDomainsToSystem($value);
        $changesAddSecurityDomain[$key]["oldValue"] = "";
        $changesAddSecurityDomain[$key]["newValue"] = $value;
        if(!empty($domainAddResp["Error"])){
            echo "<p style='text-align:center;color:red;'>".$domainAddResp["Error"]."</p>";
        }else{
            echo "<p style='text-align:center;'>Security domain ".$value." added succesfully.</p>";
            $module         = "Enterprise Modify";
            $tableName      = "enterpriseModifyChanges";
            $entityName     = $_POST["basicInfo"]["enterpriseId"];
            $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
            $cLUObj->changesArr = $changesAddSecurityDomain;
            $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
        }
    }
}

if(count($assignedDomains) > 0){
    $domaintoassigned = array();
    if(isset($assignedDomains["domains"]["newAssignedDomains"])){
        foreach ($assignedDomains["domains"]["newAssignedDomains"] as $key => $value){
            $domaintoassigned[] = $value;
        }
        $domainChangesAssign["domains"]["oldValue"] = implode(", ",$_SESSION["allResp"]["domains"]["domainAssigned"]);
        $domainChangesAssign["domains"]["newValue"] = implode(", ",$_POST["domains"]["domainAssigned"]);
    }
    if(isset($assignedDomains["domains"]["newAssignedSecurityDomains"])){
        foreach ($assignedDomains["domains"]["newAssignedSecurityDomains"] as $key => $value){
            $domaintoassigned[] = $value;
        }
        $domainChangesAssign["securityDomains"]["oldValue"] = implode(", ",$_SESSION["allResp"]["domains"]["securityDomainAssigned"]);
        $domainChangesAssign["securityDomains"]["newValue"] = implode(", ",$_POST["domains"]["securityDomainAssigned"]);
    }
    
    $domaintounassigned = array();
    if(isset($assignedDomains["domains"]["unAssignedDomains"])){
        foreach ($assignedDomains["domains"]["unAssignedDomains"] as $key => $value){
            $domaintounassigned[] = $value;
        }
        $domainChangesAssign["domains"]["oldValue"] = implode(", ",$_SESSION["allResp"]["domains"]["domainAssigned"]);
        $domainChangesAssign["domains"]["newValue"] = implode(", ",$_POST["domains"]["domainAssigned"]);
    }
    if(isset($assignedDomains["domains"]["unAssignedSecurityDomains"])){
        foreach ($assignedDomains["domains"]["unAssignedSecurityDomains"] as $key => $value){
            $domaintounassigned[] = $value;
        }
        $domainChangesAssign["securityDomains"]["oldValue"] = implode(", ",$_SESSION["allResp"]["domains"]["securityDomainAssigned"]);
        $domainChangesAssign["securityDomains"]["newValue"] = implode(", ",$_POST["domains"]["securityDomainAssigned"]);
    }
    
    if(count($domaintoassigned) > 0){
        $domainAssignResp = $obj->enterpriseDomainAssignRequest($_POST["basicInfo"]["enterpriseId"], $domaintoassigned);
        if(!empty($domainAssignResp["Error"])){
            echo "<p style='text-align:center;color:red;'>".$domainAssignResp["Error"]."</p>";
        }else{
            echo "<p style='text-align:center;'>Domains assigned succesfully.</p>";
            
            /*$module         = "Enterprise Modify";
            $tableName      = "enterpriseModifyChanges";
            $entityName     = $_POST["basicInfo"]["enterpriseId"];
            $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
            $cLUObj->changesArr = $changes;
            $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);*/
        }
    }
    if(count($domaintounassigned) > 0){
        $domainUnAssignResp = $obj->enterpriseDomainUnAssignRequest($_POST["basicInfo"]["enterpriseId"], $domaintounassigned);
        if(!empty($domainUnAssignResp["Error"])){
            echo "<p style='text-align:center;color:red;'>".$domainUnAssignResp["Error"]."</p>";
        }else{
            echo "<p style='text-align:center;'>Domains unassigned succesfully.</p>";
            /*$module         = "Enterprise Modify";
             $tableName      = "enterpriseModifyChanges";
             $entityName     = $_POST["basicInfo"]["enterpriseId"];
             $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
             $cLUObj->changesArr = $changes;
             $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);*/
        }
    }
    //print_r($domainChangesAssign);
    if(count($domaintounassigned) > 0 || count($domaintoassigned) > 0){
        $module         = "Enterprise Modify";
         $tableName      = "enterpriseModifyChanges";
         $entityName     = $_POST["basicInfo"]["enterpriseId"];
         $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
         $cLUObj->changesArr = $domainChangesAssign;
         $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
    }
    
}

if(count($cpsChangeArray) > 0){
    $cpsChangeArray["serviceProviderId"] = $_POST["basicInfo"]["enterpriseId"];
    $cpsResp = $obj->modifyEnterpriseCallProcessingPolicy($cpsChangeArray);
    if(!empty($cpsResp["Error"])){
        echo "<p style='text-align:center;color:red;'>".$cpsResp["Error"]."</p>";
    }else{
        echo "<p style='text-align:center;'>Call processing policy updated succesfully.</p>";
        $module         = "Enterprise Modify";
        $tableName      = "enterpriseModifyChanges";
        $entityName     = $_POST["basicInfo"]["enterpriseId"];
        $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
        $cLUObj->changesArr = $cpsChangeLogArray;
        $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
    }
    
    
}


if(count($_SESSION['modifiedServicesToBeUpdate']) > 0) {
    $servOP = new ServicesOperations($_POST["basicInfo"]["enterpriseId"]);
    $serviceChangeLogArray = $servOP->createServiceChangeLogArray();
    $serviceResponse = $obj->serviceProviderServiceModifyAuthorizationListRequest($_POST["basicInfo"]["enterpriseId"], $_SESSION['modifiedServicesToBeUpdate']);
    if( empty($serviceResponse["Error"]) ) {
        echo "<p style='text-align:center;'>Services updated succesfully.</p>";
        $module         = "Enterprise Modify";
        $tableName      = "enterpriseModifyChanges";
        $entityName     = $_POST["basicInfo"]["enterpriseId"];
        $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
        $cLUObj->changesArr = $serviceChangeLogArray;
        $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
        
        unset($_SESSION['modifiedServicesToBeUpdate']);
        unset($_SESSION['groupServices']);
        unset($_SESSION['userServices']);
    } else {
        echo "<p style='text-align:center;color:red;'>".$serviceResponse["Error"]."</p>";
    }
}

if(count($voiceMessegingChangeArray) > 0){
    $voiceMessegingChangeArray["serviceProviderId"] = $_POST["basicInfo"]["enterpriseId"];
    $modVMResp = $obj->enterpriseVoiceMessagingModifyRequest($voiceMessegingChangeArray);
  
    if(!empty($modVMResp["Error"])){
        echo "<p style='text-align:center;color:red;'>".$modVMResp["Error"]."</p>";
    }else{
        echo "<p style='text-align:center;'>Voice messaging updated succesfully.</p>";
        $module         = "Enterprise Modify";
        $tableName      = "enterpriseModifyChanges";
        $entityName     = $_POST["basicInfo"]["enterpriseId"];
        $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
        $cLUObj->changesArr = $voiceMessegingChangeLogArray;
        $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
    }
}
//print_r($ncsChangeLogArray);
if(count($ncsDataValidation) > 0){
    //print_r($ncsDataValidation);
    if(count($ncsDataValidation["assigned"]) >0 || count($ncsDataValidation["default"])){
        $modNCSAssignResp = $obj->enterpriseNCSAssignRequest($_POST["basicInfo"]["enterpriseId"], $ncsDataValidation);
    }
    if(count($ncsDataValidation["unassigned"]) >0){
        $modNCSUnAssignResp = $obj->enterpriseNCSUnAssignRequest($_POST["basicInfo"]["enterpriseId"], $ncsDataValidation);
    }
    
    
    if(!empty($modNCSAssignResp["Error"]) || !empty($modNCSUnAssignResp["Error"])){
        echo "<p style='text-align:center;color:red;'>".$modNCSAssignResp["Error"]."</p>";
        echo "<p style='text-align:center;color:red;'>".$modNCSUnAssignResp["Error"]."</p>";
    }else{
        echo "<p style='text-align:center;'>Network class of services updated succesfully.</p>";
        $module         = "Enterprise Modify";
        $tableName      = "enterpriseModifyChanges";
        $entityName     = $_POST["basicInfo"]["enterpriseId"];
        $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
        $cLUObj->changesArr = $ncsChangeLogArray;
        $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
    }
}

if(count($routingProfileChanges) > 0){
    $routesResp = $obj->enterpriseRoutingProfileModifyRequest($_POST["basicInfo"]["enterpriseId"], $routingProfileChanges);
    
    if(!empty($routesResp["Error"])){
        echo "<p style='text-align:center;color:red;'>".$routesResp["Error"]."</p>";
    }else{
        echo "<p style='text-align:center;'>Routing profile updated succesfully.</p>";
        $module         = "Enterprise Modify";
        $tableName      = "enterpriseModifyChanges";
        $entityName     = $_POST["basicInfo"]["enterpriseId"];
        $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
        $cLUObj->changesArr = $routingChangeLogs;
        $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
    }
    
}

$finalDnChangeLogArray = array();
if( count($unassignedDNs['numbers']) > 0 ) {
        $info = new Dns();
        $delDnResponse = $info->serviceProviderDnDeleteListRequest($unassignedDNs, $_POST["basicInfo"]["enterpriseId"]);
        if(!empty($delDnResponse["Error"])) {
            echo "<p style='text-align:center;color:red;'>".$delDnResponse["Error"]."</p>";
        } else {
            echo "<p style='text-align:center;'>Dn\'s deleted succesfully to the service provider.</p>";
            $dnChangeLogData = createDeleteDnChangeLogData($unassignedDNs);
            $finalDnChangeLogArray = array_merge($finalDnChangeLogArray, $dnChangeLogData);
            
//             $module         = "Enterprise Modify";
//             $tableName      = "enterpriseModifyChanges";
//             $entityName     = $_POST["basicInfo"]["enterpriseId"];
//             $cLUObj         = new ChangeLogUtility($_POST["basicInfo"]["enterpriseId"], "None", $_SESSION["loggedInUserName"]);
//             $cLUObj->changesArr = $dnChangeLogData;
//             $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
        }
}

if( (isset($_SESSION['enterpriseDns']['newlyAddedDns']) && count($_SESSION['enterpriseDns']['newlyAddedDns']) > 0) ||
    (isset($_SESSION['enterpriseDns']['newlyAddedDnsRanges']) && count($_SESSION['enterpriseDns']['newlyAddedDnsRanges']) > 0)
 ){
    $enterpriseDns = $_SESSION['enterpriseDns'];
    $info = new Dns();
    $responseAddDn = $info->serviceProviderDnAddListRequest($_POST["basicInfo"]["enterpriseId"], $enterpriseDns);
    if(!empty($responseAddDn["Error"])){
        echo "<p style='text-align:center;color:red;'>".$responseAddDn["Error"]."</p>";
    } else {
        echo "<p style='text-align:center;'>Dn\'s added succesfully to the service provider.</p>";
        $dnChangeLogData = createDnChangeLogData($enterpriseDns);
        $finalDnChangeLogArray = array_merge($finalDnChangeLogArray, $dnChangeLogData);
//         $module         = "Enterprise Modify";
//         $tableName      = "enterpriseModifyChanges";
//         $entityName     = $_POST["basicInfo"]["enterpriseId"];
//         $cLUObj         = new ChangeLogUtility($_POST["basicInfo"]["enterpriseId"], "None", $_SESSION["loggedInUserName"]);
//         $cLUObj->changesArr = $dnChangeLogData;
//         $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
    }
}

if( isset($modifiedAdmins) && count($modifiedAdmins) > 0) {
    $objAdmin = new AdministratorOperations($_POST["basicInfo"]["enterpriseId"]);
    foreach($modifiedAdmins as $modAdminKey => $modAdminValue) {
        $respose = $objAdmin->modifyAdministrator($modAdminValue);
        if( empty($respose['Error']) ) {
            $changeLogData = $objAdmin->createChangeLogData($modAdminValue);
            $modifyResponse .= "<p style=''> " . "Administrator " .$modAdminValue['administratorId'] . " Modified successfully." . "</p>";
            $finalChangeLogArray = array_merge($finalChangeLogArray, $changeLogData);
        } else {
            $modifyResponse .= "<p style='color:red'> " . $respose['Error'] . "</p>";
        }
    }
    unset($_SESSION['modifiedAdministratorOldData']);
    unset($_SESSION['modifiedAdmins']);
}

if(count($passwordRulesChange) > 0){
    $passwordRulesResp = $obj->enterprisePasswordRulesModifyRequest($_POST["basicInfo"]["enterpriseId"], $passwordRulesChange);
    
    if(!empty($passwordRulesResp["Error"])){
        echo "<p style='text-align:center;color:red;'>".$passwordRulesResp["Error"]."</p>";
    }else{
        echo "<p style='text-align:center;'>Password rules updated succesfully.</p>";
        $module         = "Enterprise Modify";
        $tableName      = "enterpriseModifyChanges";
        $entityName     = $_POST["basicInfo"]["enterpriseId"];
        $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
        $cLUObj->changesArr = $passwordRulesChangeLog;
        $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
    }
    
}

if(count($passcodeRulesChange) > 0){
    $passcodeRulesResp = $obj->enterprisePasscodeRulesModifyRequest($_POST["basicInfo"]["enterpriseId"], $passcodeRulesChange);
    
    if(!empty($passcodeRulesResp["Error"])){
        echo "<p style='text-align:center;color:red;'>".$passcodeRulesResp["Error"]."</p>";
    }else{
        echo "<p style='text-align:center;'>Passcode rules updated succesfully.</p>";
        $module         = "Enterprise Modify";
        $tableName      = "enterpriseModifyChanges";
        $entityName     = $_POST["basicInfo"]["enterpriseId"];
        $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
        $cLUObj->changesArr = $passcodeRulesChangeLog;
        $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
    }
    
}

if(count($sipPasswordRulesChange) > 0){
    $sipPasswordRulesResp = $obj->enterpriseSIPPasswordRulesModifyRequest($_POST["basicInfo"]["enterpriseId"], $sipPasswordRulesChange);
    
    if(!empty($sipPasswordRulesResp["Error"])){
        echo "<p style='text-align:center;color:red;'>".$sipPasswordRulesResp["Error"]."</p>";
     }else{
     echo "<p style='text-align:center;'>SIP authentication password rules updated succesfully.</p>";
     $module         = "Enterprise Modify";
     $tableName      = "enterpriseModifyChanges";
     $entityName     = $_POST["basicInfo"]["enterpriseId"];
     $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
     $cLUObj->changesArr = $sipPasswordRulesChangeLog;
     $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
     }
    
}

if(count($devicePasswordRulesChange) > 0){
    //to be done function
    $devicePasswordRulesResp = $obj->enterpriseDevicePasswordRulesModifyRequest($_POST["basicInfo"]["enterpriseId"], $devicePasswordRulesChange);
    
    if(!empty($devicePasswordRulesChangeLog["Error"])){
        echo "<p style='text-align:center;color:red;'>".$devicePasswordRulesChangeLog["Error"]."</p>";
     }else{
     echo "<p style='text-align:center;'>Device profile authentication password rules updated succesfully.</p>";
     $module         = "Enterprise Modify";
     $tableName      = "enterpriseModifyChanges";
     $entityName     = $_POST["basicInfo"]["enterpriseId"];
     $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
     $cLUObj->changesArr = $devicePasswordRulesChangeLog;
     $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
     }
    
}

if( isset($addedAdmins) && count($addedAdmins) > 0) {
    $objAdmin = new AdministratorOperations($_POST["basicInfo"]["enterpriseId"]);
    foreach($addedAdmins as $adminKey => $adminValue) {
        $respose = $objAdmin->addAdministrator($adminValue);
        if( empty($respose['Error']) ) {
            $changeLogAdd = $objAdmin->createChangeLogDataAddAdmin($adminValue);
            $modifyResponse .= "<p style=''> Administrator " . $adminValue['administratorId'] . " added successfully. </p>";
            $finalChangeLogArray = array_merge($finalChangeLogArray, $changeLogAdd);
        } else {
            $modifyResponse .= "<p style='color:red'> " . $respose['Error'] . "</p>";
        }
    }
    unset($_SESSION['addedAdmins']);
}

if( isset($deleteAdmins) && count($deleteAdmins) > 0) {
    $objAdmin = new AdministratorOperations($_POST["basicInfo"]["enterpriseId"]);
    foreach($deleteAdmins as $adminKey => $adminId) {
        $respose = $obj->serviceProviderAdminDeleteRequest($adminId);
        if( empty($respose['Error']) ) {
            $changeLogDelete = $objAdmin->createChangeLogDataDeleteAdmin($adminId);
            $finalChangeLogArray = array_merge($finalChangeLogArray, $changeLogDelete);
            $modifyResponse .= "<p style=''> Administrator " . $adminId . " deleted successfully. </p>";
        } else {
            $modifyResponse .= "<p style='color:red'> " . $respose['Error'] . "</p>";
        }
    }
    unset($_SESSION['deleteAdmins']);
}

if(isset($_SESSION["dialPlanPolicyToAdd"]) && count($_SESSION["dialPlanPolicyToAdd"]) > 0){
    foreach ($_SESSION["dialPlanPolicyToAdd"] as $key => $value){
        $dialPlanPolicyAddResp = $obj->ServiceProviderDialPlanPolicyAddAccessCodeRequest($_POST["basicInfo"]["enterpriseId"], $value);
        
        if(!empty($dialPlanPolicyAddResp["Error"])){
            echo "<p style='text-align:center;color:red;'>".$dialPlanPolicyAddResp["Error"]."</p>";
        }else{
            $dpChanges = array();
            foreach ($value as $dkey => $dvalue){
                $dpChanges[$dkey]["oldValue"] = "";
                $dpChanges[$dkey]["newValue"] = $dvalue;
            }
            echo "<p style='text-align:center;'>".$value["accessCode"].": Dial plan access code added succesfully.</p>";
            $module         = "Enterprise Modify";
            $tableName      = "enterpriseModifyChanges";
            $entityName     = $_POST["basicInfo"]["enterpriseId"];
            $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
            $cLUObj->changesArr = $dpChanges;
            $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
        }
    }
}

if(isset($_SESSION["dialPlanAccessCodeModifyData"]) && count($_SESSION["dialPlanAccessCodeModifyData"]) > 0){
    foreach ($_SESSION["dialPlanAccessCodeModifyData"] as $key => $value){
        $dialPlanPolicyModResp = $obj->serviceProviderDialPlanPolicyModifyAccessCodeRequest($_POST["basicInfo"]["enterpriseId"], $key, $value);
        
        if(!empty($dialPlanPolicyModResp["Error"])){
            echo "<p style='text-align:center;color:red;'>".$dialPlanPolicyModResp["Error"]."</p>";
        }else{
            $dpModChanges = array();
            //print_r($_SESSION["allResp"]["dialPlanAccessCodeList"]);
            //print_r($value);
            $tempArray = array();
            foreach($_SESSION["allResp"]["dialPlanAccessCodeList"] as $dataKey => $dataValue){
                $tempArray[$dataValue["accessCode"]] = $dataValue;
            }
            if(isset($value["description"])){
                $dpModChanges["description"]["oldValue"] = $tempArray[$key]["description"];
                $dpModChanges["description"]["newValue"] = $value["description"];
            }
            if(isset($value["includeCodeForNetworkTranslationsAndRouting"])){
                $dpModChanges["includeCodeForNetworkTranslationsAndRouting"]["oldValue"] = $tempArray[$key]["includeCodeForNetworkTranslationsAndRouting"];
                $dpModChanges["includeCodeForNetworkTranslationsAndRouting"]["newValue"] = $value["includeCodeForNetworkTranslationsAndRouting"];
            }
            if(isset($value["includeCodeForScreeningServices"])){
                $dpModChanges["includeCodeForScreeningServices"]["oldValue"] = $tempArray[$key]["includeCodeForScreeningServices"];
                $dpModChanges["includeCodeForScreeningServices"]["newValue"] = $value["includeCodeForScreeningServices"];
            }
            if(isset($value["enableSecondaryDialTone"])){
                $dpModChanges["enableSecondaryDialTone"]["oldValue"] = $tempArray[$key]["enableSecondaryDialTone"];
                $dpModChanges["enableSecondaryDialTone"]["newValue"] = $value["enableSecondaryDialTone"];
            }
           // print_r($dpModChanges);
            echo "<p style='text-align:center;'>".$key.": Dial plan access code modified succesfully.</p>";
            $module         = "Enterprise Modify";
            $tableName      = "enterpriseModifyChanges";
            $entityName     = $_POST["basicInfo"]["enterpriseId"];
            $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
            $cLUObj->changesArr = $dpModChanges;
            $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
        }
    }
}
//$_SESSION["dialPlanAccessCodeModifyData"]

if(count($dialPlanDataChange) > 0){
    //print_r($dialPlanDataChange);
    $dialPDataRulesResp = $obj->serviceProviderDialPlanPolicyModifyRequest($_POST["basicInfo"]["enterpriseId"], $dialPlanDataChange);
    
    if(!empty($dialPDataRulesResp["Error"])){
        echo "<p style='text-align:center;color:red;'>".$dialPDataRulesResp["Error"]."</p>";
    }else{
        echo "<p style='text-align:center;'>Dial plan policy updated succesfully.</p>";
        $module         = "Enterprise Modify";
        $tableName      = "enterpriseModifyChanges";
        $entityName     = $_POST["basicInfo"]["enterpriseId"];
        $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
        $cLUObj->changesArr = $dialPlanDataChangeLog;
        $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
    }
    
}

if(count($_POST["dialPlanAccessCode"]) > 0){
    foreach ($_POST["dialPlanAccessCode"] as $key => $value){
        $dialPAccessCodeDeleteResp = $obj->serviceProviderDialPlanPolicyDeleteAccessCodeRequest($_POST["basicInfo"]["enterpriseId"], $value);
        
        if(!empty($dialPAccessCodeDeleteResp["Error"])){
            echo "<p style='text-align:center;color:red;'>".$dialPAccessCodeDeleteResp["Error"]."</p>";
        }else{
            $dpDeleteChanges = array();
            $dpDeleteChanges["Access Code"]["oldValue"] = $value;
            $dpDeleteChanges["Access Code"]["newValue"] = "Deleted successfully";
            echo "<p style='text-align:center;'>".$value.": Dial plan policy access code deleted succesfully.</p>";
            $module         = "Enterprise Modify";
            $tableName      = "enterpriseModifyChanges";
            $entityName     = $_POST["basicInfo"]["enterpriseId"];
            $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
            $cLUObj->changesArr = $dpDeleteChanges;
            $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
        }
    }
}

unset($_SESSION['enterpriseDns']);

if( count($finalChangeLogArray) > 0) {
    createChangeLog($_POST, $finalChangeLogArray);
}

if( count($finalDnChangeLogArray) > 0) {
    createDnChangeLogEntry($_POST, $finalDnChangeLogArray);
}

echo $modifyResponse;

function flip_isset_diff($b, $a) {
    $at = array_flip($a);
    $d = array();
    foreach ($b as $i)
        if (!isset($at[$i]))
            $d[] = $i;
            
            return $d;
}

function createChangeLog($POST, $finalChangeLogArray) {
     
    $module         = "Enterprise Modify";
    $tableName      = "enterpriseModifyChanges";
    $entityName     = $POST["basicInfo"]["enterpriseId"];
    $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
    $cLUObj->changesArr = $finalChangeLogArray;
    $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
}

function createDnChangeLogEntry($POST, $dnChangeLogData) {
    $module         = "Enterprise Modify";
    $tableName      = "enterpriseModifyChanges";
    $entityName     = $POST["basicInfo"]["enterpriseId"];
    $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
    $cLUObj->changesArr = $dnChangeLogData;
    $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
}

function createDnChangeLogData($enterpriseDns) {
    $data = "";
    if(isset($enterpriseDns['newlyAddedDns']) && count($enterpriseDns['newlyAddedDns']) > 0){
        foreach($enterpriseDns['newlyAddedDns'] as $dnKey => $dnValue) {
            $data .= $dnValue .", \n";
        }
    }
    if(!empty($enterpriseDns['newlyAddedDnsRanges']) ) {
        foreach($enterpriseDns['newlyAddedDnsRanges'] as $rangeKey => $rangeValue) {
            $data .= $rangeValue['minPhoneNumber'] . " - " . $rangeValue['maxPhoneNumber'] .", \n";
        }
    }
    return array("Added DN\'s" => array("oldValue" => "", "newValue" => $data));
}

function createDeleteDnChangeLogData($unassignNumber) {
    $data = "";
    if( isset($unassignNumber['numbers']) ) {
        foreach($unassignNumber['numbers'] as $unDnKey => $unDnValue) {
            $data .= $unDnValue .", \n";
        }
    }
  /*  if( isset($unassignNumber['numbersRanges']) ) {
        foreach($unassignNumber['numbersRanges'] as $unDnKey => $unDnValue) {
            $rangeDn = $unDnValue['minPhoneNumber'] ." - ". $unDnValue['maxPhoneNumber'];
            $data .= $rangeDn .", \n";
        }
    }  */
    
    return array("Deleted DN\'s" => array("oldValue" => "", "newValue" => $data));
}

?>