<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/enterprise/enterpriseOperations.php");

$obj = new EnterpriseOperations();
$systemDoamins = $obj->getAllSystemDomain();
//print_r($systemDoamins);
$statList = $obj->getAllStateProvince();
$entList = $obj->getAllEnterprise();

//die;
?>
<script src="enterprise/js/enterprise.js"></script>
<script>
$(document).ready(function(){
	$("#enterpriseCheckBoxes").hide();
    $('[data-toggle="tooltip"]').tooltip(); 
    $("#enterpriseCloneID").change(function() {
        if(this.value != ""){
        	$.ajax({
                type: "POST",
                url: "enterprise/securityDomainCheck.php",
                data: {"selectedEnterprise" : this.value},
                success: function(result)
                {
                	var res = $.parseJSON(result);
                	if(res.available.length > 0 || res.assigned.length > 0){
                    	$("#securityDomainId").show();
                	}else{
                		$("#securityDomainId").hide();
                    }
                	$("#enterpriseCheckBoxes").show();
                   	$('#enterpriseCheckBoxes').find('input[type=checkbox]:checked').removeAttr('checked');
                }
        	});
           	
        }else{
        	$("#enterpriseCheckBoxes").hide();
        	$('#enterpriseCheckBoxes').find('input[type=checkbox]:checked').removeAttr('checked');
        }
    });
});
</script>
<div id="enterpriseData" style="padding-top:40px;">
        	<div id="tabs">
        		<div class="divBeforeUl">
        			<ul>
        				<li><a href="#basicInfoData_1" id="basicInfo" class="tabs">Basic Information</a></li>
        				<li class="disabled"><a href="#domainData_1" id="domains" class="tabs">Domains</a></li>
        				
        				<li class="disabled"><a href="#servicesData_1" id="services" class="tabs">Services</a></li>
        				<li class="disabled"><a href="#dnsData_1" id="dns" class="tabs">DNs</a></li>
        				<li class="disabled"><a href="#administratorsData_1" id="administrators" class="tabs">Administrators</a></li>
        				<li class="disabled"><a href="#callProcessingPolicyData_1" id="callProcessingPolicy" class="tabs">Call Processing Policies</a></li>
        				<li class="disabled"><a href="#dialPlanPolicyData_1" id="dialPlanPolicy" class="tabs">Dial Plan Policy</a></li>
        				<li class="disabled"><a href="#voiceMessagingData_1" id="voiceMessaging" class="tabs">Voice Messaging</a></li>
        				<li class="disabled"><a href="#voicePortalData_1" id="voicePortal" class="tabs">Voice Portal</a></li>
        				<li class="disabled"><a href="#ncsData_1" id="ncs" class="tabs">Network Classes of Service</a></li>
        				<li class="disabled"><a href="#routingData_1" id="routing" class="tabs">Routing Profile</a></li>
        				<li class="disabled"><a href="#passwordsRulesData_1" id="passwordsRules" class="tabs">Passwords Rules</a></li>
        			</ul>
        		</div>
        		<form name ="enterpriseAdd" id="enterpriseAdd" class="groupModifyCls" method="POST">
        		<div id="basicInfoData_1" class="userDataClass" style="display:none;"><!--begin basicInfo_1 div-->
				<h2 class="subBannerCustom">Basic Info</h2>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="labelText">Enterprise ID</label><span class="required">*</span><br/>
								<input type="text" name="basicInfo[serviceProviderId]" id="serviceProviderId" size="30" maxlength="30" value="">
								
							</div>
							
							<div class="form-group">	
								<label class="labelText">Name</label><br>
								<input type="text" name="basicInfo[serviceProviderName]" id="serviceProviderName" size="30" maxlength="30" value="">
							</div>
							<div class="form-group">	
								<label class="labelText">Contact Phone</label><br>
								<input type="text" name="basicInfo[contactNumber]" id="contactNumber" size="30" maxlength="30" value="" >
							</div>
							<div class="form-group">	
								<label class="labelText">Support Email</label><br>
								<input type="text" name="basicInfo[supportEmail]" id="supportEmail" size="30" maxlength="30" value="" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="labelText" for="defaultDomain">Default Domain:</label><br>
								<div class="dropdown-wrap">   
									<select name="basicInfo[defaultDomain]" id="defaultDomain">
									<?php 
									if(count($systemDoamins["Success"]) > 0){
									    $domainVal = "";
									    foreach ($systemDoamins["Success"]["domain"] as $key => $value){
									        $sel = "";
									        if($value == $systemDoamins["Success"]["systemDefaultDomain"]){
									            $sel="selected";
									        }
									        $domainVal .= "<option value='".$value."' $sel>".$value."</option>"; 
									    }
									    echo $domainVal;
									}
									?>
									</select>
								</div>
							</div>
							<div class="form-group">	
								<label class="labelText">Contact Name</label><br>
								<input type="text" name="basicInfo[contactName]" id="contactName" size="30" maxlength="30" value="">
							</div>
							
							<div class="form-group">	
								<label class="labelText">Contact Email</label><br>
								<input type="text" name="basicInfo[contactEmail]" id="contactEmail" size="30" maxlength="30" value="" >
							</div>
							
						</div>
					</div> 
					<h2 class="subBannerCustom">Address Information</h2>
					<div class="row">
						<div class="col-md-6">
							
							<div class="form-group">	
								<label class="labelText">Address Line 1</label><br>
								<input type="text" name="basicInfo[addressLine1]" id="addressLine1" size="30" maxlength="30" value="">
							</div>
							
							<div class="form-group">	
								<label class="labelText">City</label><br>
								<input type="text" name="basicInfo[city]" id="city" size="30" maxlength="30" value="" >
							</div>
							
							<div class="form-group">	
								<label class="labelText">Zip/Postal Code</label><br>
								<input type="text" name="basicInfo[zipOrPostalCode]" id="zipOrPostalCode" size="30" maxlength="30" value="">
							</div>
							
						</div>
						
						<div class="col-md-6">
							
							<div class="form-group">	
								<label class="labelText">Address Line 2</label><br>
								<input type="text" name="basicInfo[addressLine2]" id="addressLine2" size="30" maxlength="30" value="">
							</div>
							
							<div class="form-group">
								<label class="labelText" for="defaultDomain">State/Province:</label><br>
								<div class="dropdown-wrap">   
									<select name="basicInfo[stateOrProvince]" id="stateOrProvince">
									<?php 
									if(count($statList["Success"]) > 0){
									    
									    
									    $stateVal = "<option value=''>-- Select --</option>";
									    foreach ($statList["Success"] as $key => $value ){
									        
									        $stateVal .= "<option value='".$value."'>".$value."</option>";
									    }
									    echo $stateVal;
									}
									?>
									</select>
								</div>
							</div>
							<div class="form-group">	
								<label class="labelText">Country</label><br>
								<input type="text" name="basicInfo[country]" id="country" size="30" maxlength="30" value="">
							</div>
							<input type="hidden" value="" id="hiddenEntId">
						</div>
					</div>  
					<h2 class="subBannerCustom">Clone Enterprise</h2>
					<div class="row" style="">
                        <div class="col-md-12" style="padding-right:0;">
                        	<div class="form-group">
                        	<label class="labelText">Select Enterprise:</label><a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Enterprise cloning will be allowed only when creating new Enterprise."><img src="images/NewIcon/info_icon.png"></a>
                            	<div class="dropdown-wrap">
                            		<select name="enterpriseCloneID" id="enterpriseCloneID" style="width:100% !important;">
                            		<option value="">None</option>
                            			<?php 
                            			foreach ($entList["Success"] as $key => $value){
                            			    if($value["enterpriseName"] != ""){
                            			        $entName = $value["enterpriseId"]."-".$value["enterpriseName"];
                            			    }else{
                            			        $entName = $value["enterpriseId"];
                            			    }
                            			    echo "<option value='".$value["enterpriseId"]."'>".$entName."</option>";
                            			}
                            			?>						
                            		</select>
                            	</div>
                        	</div>
                        </div>
                	</div>
                	 
                	<div id="enterpriseCheckBoxes"> 
                	 <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[domainsClone]" id="domainsClone" type="checkbox" value="true">
                    				<label for="domainsClone"><span></span></label>
                    				<label class="labelText">Domains</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row" id="securityDomainId">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[securityDomainsClone]" id="securityDomainsClone" type="checkbox" value="true">
                    				<label for="securityDomainsClone"><span></span></label>
                    				<label class="labelText">Security Domains</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[groupServiceClone]" id="groupServiceClone" type="checkbox" value="true">
                    				<label for="groupServiceClone"><span></span></label>
                    				<label class="labelText">Group Services</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[userServiceClone]" id="userServiceClone" type="checkbox" value="true">
                    				<label for="userServiceClone"><span></span></label>
                    				<label class="labelText">User Services</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[clidPolicyClone]" id="clidPolicyClone" type="checkbox" value="true">
                    				<label for="clidPolicyClone"><span></span></label>
                    				<label class="labelText">Calling Line ID Policies</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[mediaPolicyClone]" id="mediaPolicyClone" type="checkbox" value="true">
                    				<label for="mediaPolicyClone"><span></span></label>
                    				<label class="labelText">Media Policiy</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[callLimitsClone]" id="callLimitsClone" type="checkbox" value="true">
                    				<label for="callLimitsClone"><span></span></label>
                    				<label class="labelText">Call Limits</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[conferencingPolicyClone]" id="conferencingPolicyClone" type="checkbox" value="true">
                    				<label for="conferencingPolicyClone"><span></span></label>
                    				<label class="labelText">Conferencing Policy</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[tranlatingAndRoutingPoliciesClone]" id="tranlatingAndRoutingPoliciesClone" type="checkbox" value="true">
                    				<label for="tranlatingAndRoutingPoliciesClone"><span></span></label>
                    				<label class="labelText">Translation and Routing Policies</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[disableCallerIDPolicyClone]" id="disableCallerIDPolicyClone" type="checkbox" value="true">
                    				<label for="disableCallerIDPolicyClone"><span></span></label>
                    				<label class="labelText">Disable Caller ID Policy</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[dialPlanPolicyClone]" id="dialPlanPolicyClone" type="checkbox" value="true">
                    				<label for="dialPlanPolicyClone"><span></span></label>
                    				<label class="labelText">Dial Plan Policy</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[voiceMessagingPolicyClone]" id="voiceMessagingPolicyClone" type="checkbox" value="true">
                    				<label for="voiceMessagingPolicyClone"><span></span></label>
                    				<label class="labelText">Voice Messaging Policies</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[networkClassOfServicesClone]" id="networkClassOfServicesClone" type="checkbox" value="true">
                    				<label for="networkClassOfServicesClone"><span></span></label>
                    				<label class="labelText">Network Classes of Services</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[routingProfileClone]" id="routingProfileClone" type="checkbox" value="true">
                    				<label for="routingProfileClone"><span></span></label>
                    				<label class="labelText">Routing Profile</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[passwordRulesClone]" id="passwordRulesClone" type="checkbox" value="true">
                    				<label for="passwordRulesClone"><span></span></label>
                    				<label class="labelText">Password Rules</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[sipPasswordAuthClone]" id="sipPasswordAuthClone" type="checkbox" value="true">
                    				<label for="sipPasswordAuthClone"><span></span></label>
                    				<label class="labelText">SIP Authentication Rules</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    
                    <div class="row">
                    	<div class="">
                    		<div class="col-md-8">
                    			<div class="form-group alignChkBox">
                    				<input name="clone[deviceAuthPasswordClone]" id="deviceAuthPasswordClone" type="checkbox" value="true">
                    				<label for="deviceAuthPasswordClone"><span></span></label>
                    				<label class="labelText">Device Profile Authentication Password Rules</label>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    </div>
                	                   
        	</div>
        	<div id="domainData_1" class="userDataClass" style="display:none;"><!--begin basicInfo_1 div-->
        	dfdffd
        	</div>
                
                <!--administrator module -->
              <div id="administratorsData_1" class="userDataClass" style="display:none;">
                    <div class="row">
                        <?php 
                         //   require_once("/var/www/html/Express/enterprise/administrator/index.php");
                        ?>
                    </div>
        	</div>            
                            
                 <!--end admininstrator module -->           
        	</form>
        </div>
        <div class="row alignBtn">
			<div class="col-md-2"></div>
            <div class="col-md-8" style="text-align: center;">
				<input type="button" id="subButtonEntAdd" class="marginRightButton" value="Confirm Settings">
				<input type="button" id="EntButtonCancel" value="Cancel" class="cancelButton marginRightButton">
			</div>
            <div class="col-md-2" style="text-align: right;">
				
			</div>
        </div>
        <div class="clr"></div>
        <div class="clr"></div>	
        	
        	
		</div>