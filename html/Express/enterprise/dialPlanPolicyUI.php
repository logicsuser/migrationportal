<h2 class="adminUserText">Dial Plan Policy</h2>

<div style="clear: right;">&nbsp;</div>
<input type='hidden' id='dialPlanModPermission' value='<?php echo $entPermDialPlanPolicyLogModPermission ?>' />

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="dialPolicy[requiresAccessCodeForPublicCalls]" type="hidden" value="false">
				<input name="dialPolicy[requiresAccessCodeForPublicCalls]" id="requiresAccessCodeForPublicCalls" type="checkbox" value="true" <?php if($allResp["dialPlanData"]["Success"]["requiresAccessCodeForPublicCalls"] == "true"){echo "checked";}?>>
				<label for="requiresAccessCodeForPublicCalls"><span></span></label>
				<label class="labelText">Requires Access Code for Public Calls</label>
			</div>
			
		</div>
		<div class="col-md-8" style="margin-left:45px;">
			<div class="form-group alignChkBox">
				<input name="dialPolicy[allowE164PublicCalls]" type="hidden" value="false">
				<input name="dialPolicy[allowE164PublicCalls]" id="allowE164PublicCalls" type="checkbox" value="true" <?php if($allResp["dialPlanData"]["Success"]["allowE164PublicCalls"] == "true"){echo "checked";}?>>
				<label for="allowE164PublicCalls"><span></span></label>
				<label class="labelText">Allow E.164 Public Calls</label>
			</div>
		</div>
</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="dialPolicy[preferE164NumberFormatForCallbackServices]" type="hidden" value="false">
				<input name="dialPolicy[preferE164NumberFormatForCallbackServices]" id="preferE164NumberFormatForCallbackServices" type="checkbox" value="true" <?php if($allResp["dialPlanData"]["Success"]["preferE164NumberFormatForCallbackServices"] == "true"){echo "checked";}?>>
				<label for="preferE164NumberFormatForCallbackServices"><span></span></label>
				<label class="labelText">Prefer E.164 Number Format for Callback Services</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox">
				<label class="labelText" style="width:110px;margin:0;">Public Digit Map:</label>&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" maxlength="2048" name="dialPolicy[publicDigitMap]" id="publicDigitMap" style="width:400px !important;" value="<?php echo $allResp["dialPlanData"]["Success"]["publicDigitMap"];?>">
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox">
				<label class="labelText" style="width:110px;margin:0;">Private Digit Map:</label>&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" maxlength="2048" name="dialPolicy[privateDigitMap]" id="privateDigitMap" style="width:400px !important;" value="<?php echo $allResp["dialPlanData"]["Success"]["privateDigitMap"];?>">
			</div>
		</div>
	</div>
</div>

<div class="row">
		<!-- <div class="col-md-6"></div> -->
                <div class="col-md-6" style="padding-right:0;float:right;">
			 
				<!-- <div class="col-md-10"></div> -->
				<div class="col-md-2" style="padding-right:0;float:right;">
                                <?php                                             
                                    if($entPermDialPlanPolicyLogPermission == "1" && $entPermDialPlanPolicyLogModPermission == "yes"){
                                ?>
                                            <div id="downloadCSVBtn" class="csvFileDownloadAlign" style="display: block;float:left;">
                                            <div id="downloadDeviceCSV" class="csvFileDownloadAlign">
                                                    <div id="dialPlanPolicyAdd" style="display: block;text-align:center;cursor:pointer;">
                                                    <img src="images/icons/service-pack-add.png" style="width:40px;" data-alt-src="images/icons/download_csv_over.png">
                                                    <br><span class='addServicePack' style='color:#6ea0dc;'>Add<br>Dial Plan<br>Policy</span>
                                                    </div>
                                            </div>
                                            </div>
                                <?php
                                    }
                                ?>
				</div>
               </div>
              
        </div>
<div class="row" id="dialPlanPolicyTableDiv">
    <!-- <table border="1" class="servicePackTableDiv tableAlignMentDesign scroll tablesorter tagTableHeight" id="servicePacksTable">
    	<thead>
    		<tr>  
    			<th id="disableTh" class="header" style="width:10%;">Delete</th>
    			<th class="header thsmall" style="height: auto !important; width:20% !important;">Access Code</th>
    			<th class="header thsmall" style="height: auto !important; width:30% !important;">Enable Secondry Dial Tone</th>
    			<th class="header thsmall" style="height: auto !important; width:40% !important;">Description</th>
    		</tr>
    	</thead>
    	<tbody id="customTagTable" style="overflow-y:scroll !important;">
    		<tr class="tableTrAlignment" style="" id="servicePacksTable">
    			<td class="deleteCheckboxtd thSno header" style="width:10% !important"><input name="servicePackName[]" disabled="" data-servicepackuserservicelist="undefined" data-isadded="undefined" data-servicepackquantity="[object Object]" data-isavailableforuse="true" data-inuse="Yes" data-servicepackdescription="Premium Service Pack" data-spname="Averi_Premium" value="Averi_Premium" type="checkbox" style="width:17px" class="servicePackDeleteCheckBox" id="Averi_Premium"><label for="Averi_Premium"><span onmouseover="checkBoxDelBtnShowNavo(true)" onmouseout="checkBoxDelBtnShowNavo(false)"></span></label></td>
    			
    			<td class="header thsmall" style="width:20% !important">Averi_Premium</td>
    			<td class="header thsmall" style="width:30% !important">Premium Service Pack</td>
    			<td class="header thsmall" style="width:40% !important">Yes</td>
    		</tr>
    		<tr class="tableTrAlignment" style="" id="servicePacksTable">
    			<td class="deleteCheckboxtd thSno header" style="width:10% !important"><input name="servicePackName[]" disabled="" data-servicepackuserservicelist="undefined" data-isadded="undefined" data-servicepackquantity="[object Object]" data-isavailableforuse="true" data-inuse="Yes" data-servicepackdescription="Premium Service Pack" data-spname="Averi_Premium" value="Averi_Premium" type="checkbox" style="width:17px" class="servicePackDeleteCheckBox" id="Averi_Premium"><label for="Averi_Premium"><span onmouseover="checkBoxDelBtnShowNavo(true)" onmouseout="checkBoxDelBtnShowNavo(false)"></span></label></td>
    			
    			<td class="header thsmall" style="width:20% !important">Averi_Premium</td>
    			<td class="header thsmall" style="width:30% !important">Premium Service Pack</td>
    			<td class="header thsmall" style="width:40% !important">Yes</td>
    		</tr>
    	</tbody>
    </table> -->
</div>

<div id="dialPlanPolicyDialog" class="dialogClass"></div>
<div id="updateDialPlanPolicyDialog" class="dialogClass"></div>
