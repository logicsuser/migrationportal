<?php
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
//print_r($_POST);
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/enterprise/enterpriseOperations.php");
$obj = new EnterpriseOperations();
$resp = $obj->ServiceProviderDialPlanPolicyGetAccessCodeListRequest($_POST["entId"]);

if(count($resp["Success"]) > 0){
    foreach ($resp["Success"] as $key => $value){
        $responseDetails = $obj->serviceProviderDialPlanPolicyGetAccessCodeRequest($_POST["entId"], $value["accessCode"]);
        $respDet[] = $responseDetails["Success"];
    }
}
$resp["Success"] = $respDet;

if($_POST["funcAction"] == "showDialPlanPolicyList"){
    
    echo  json_encode($resp["Success"]);
}
if($_POST["funcAction"] == "dialPlanPolicyAdd"){
    $postData = array();
    foreach ($_POST["datatosend"] as $key => $value){
        $postData[$value["name"]] = $value["value"];
    }
    //print_r($postData);
    $postArrayAdd= array(
        "action"=> "added",
        "accessCode" => $postData["accessCode"],
        "enableSecondaryDialTone" => $postData["enableSecondaryDialTone"],
        "description" => $postData["description"],
        "includeCodeForNetworkTranslationsAndRouting" => $postData["includeCodeForNetworkTranslationsAndRouting"],
        "includeCodeForScreeningServices" => $postData["includeCodeForScreeningServices"]
    );
    
    if(isset($_SESSION["allDialPlanListToAdd"]) && count($_SESSION["allDialPlanListToAdd"]) >0){
        $resp = $_SESSION["allDialPlanListToAdd"];
    }else{
        $resp = $resp;
    }
    foreach ($resp["Success"] as $key => $value){
        $tempResp[$value["accessCode"]] = $value;
    }
    $resp["Success"] = $tempResp;
    //print_r($postArrayAdd);
    $resp["Success"][$postData["accessCode"]] = $postArrayAdd;
    //print_r($resp["Success"]);
    $_SESSION["dialPlanPolicyToAdd"][$postData["accessCode"]] = $postData;
    $_SESSION["allDialPlanListToAdd"] = $resp;
    echo  json_encode($resp["Success"]);
}

if($_POST["funcAction"] == "dialPlanPolicyModify"){
    //print_r($_POST);
    //print_r($_SESSION["allResp"]["dialPlanAccessCodeList"]);
    $postData = array();
    foreach ($_POST["datatosend"] as $key => $value){
        $postData[$value["name"]] = $value["value"];
    }
    //print_r($postData);
    $changeDialAccessCodeArray = array();
    foreach ($_SESSION["allResp"]["dialPlanAccessCodeList"] as $key => $value){
        if($value["accessCode"] == $postData["accessCode"]){
            if($value["description"] != $postData["description"]){
                $changeDialAccessCodeArray[$postData["accessCode"]]["description"] = $postData["description"];
            }
            if($value["includeCodeForNetworkTranslationsAndRouting"] != $postData["includeCodeForNetworkTranslationsAndRouting"]){
                $changeDialAccessCodeArray[$postData["accessCode"]]["includeCodeForNetworkTranslationsAndRouting"] = $postData["includeCodeForNetworkTranslationsAndRouting"];
            }
            if($value["includeCodeForScreeningServices"] != $postData["includeCodeForScreeningServices"]){
                $changeDialAccessCodeArray[$postData["accessCode"]]["includeCodeForScreeningServices"] = $postData["includeCodeForScreeningServices"];
            }
            if($value["enableSecondaryDialTone"] != $postData["enableSecondaryDialTone"]){
                $changeDialAccessCodeArray[$postData["accessCode"]]["enableSecondaryDialTone"] = $postData["enableSecondaryDialTone"];
            }
        }
    }
    
    if(isset($_SESSION["dialPlanAccessCodeModifyData"]) && !empty($_SESSION["dialPlanAccessCodeModifyData"])){
        if(array_key_exists($postData["accessCode"], $_SESSION["dialPlanAccessCodeModifyData"])){
            $_SESSION["dialPlanAccessCodeModifyData"][$postData["accessCode"]]=$changeDialAccessCodeArray[$postData["accessCode"]];
        }else{
            $_SESSION["dialPlanAccessCodeModifyData"][$postData["accessCode"]] = $changeDialAccessCodeArray[$postData["accessCode"]];
        }
    }else{
        $_SESSION["dialPlanAccessCodeModifyData"][$postData["accessCode"]] = $changeDialAccessCodeArray[$postData["accessCode"]];
    }
    
    foreach ($resp["Success"] as $key => $value){
        $tempResp[$value["accessCode"]] = $value;
    }
    $resp["Success"] = $tempResp;
    
    foreach ($_SESSION["dialPlanAccessCodeModifyData"] as $key => $value){
        if(isset($value["description"])){
            $resp["Success"][$key]["description"] = $value["description"];
        }
        if(isset($value["includeCodeForNetworkTranslationsAndRouting"])){
            $resp["Success"][$key]["includeCodeForNetworkTranslationsAndRouting"] = $value["includeCodeForNetworkTranslationsAndRouting"];
        }
        if(isset($value["includeCodeForScreeningServices"])){
            $resp["Success"][$key]["includeCodeForScreeningServices"] = $value["includeCodeForScreeningServices"];
        }
        if(isset($value["enableSecondaryDialTone"])){
            $resp["Success"][$key]["enableSecondaryDialTone"] = $value["enableSecondaryDialTone"];
        }
    }
    if(isset($_SESSION["dialPlanPolicyToAdd"]) && !empty($_SESSION["dialPlanPolicyToAdd"])){
        foreach ($_SESSION["dialPlanPolicyToAdd"] as $key => $value){
            $resp["Success"][$key] = $value;
        }
        //print_r($resp["Success"]);
        //print_r($_SESSION["dialPlanPolicyToAdd"]);
    }
    echo  json_encode($resp["Success"]);
    
    
}
?>