<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
unset($_SESSION["autoFillEnt"]);

$_SESSION ["enterpriseDialogArr"] = array(
    "serviceProviderId" => "Enterprise ID",
    "serviceProviderName" => "Enterprise Name",
    "contactNumber" => "Contact Number",
    "supportEmail" => "Support Email",
    "defaultDomain" => "Default Domain",
    "contactName" => "Contact Name",
    "contactEmail" => "Contact Email",
    "addressLine1" => "Address Line 1",
    "addressLine2" => "Address Line 2",
    "city" => "City",
    "stateOrProvince" => "State/Province",
    "zipOrPostalCode" => "Zip/Postal Code",
    "country" => "Country",
    "newAssignedDomains" => "Domain Assigned",
    "unAssignedDomains" => "Domain Unassigned",
    "newAssignedSecurityDomains" => "Security Domain Assigned",
    "unAssignedSecurityDomains" => "Security Domain Unassigned",
    "clidPolicy" => "External Calls",
    "enterpriseCallsCLIDPolicy" => "Enterprise Calls",
    "groupCallsCLIDPolicy" => "Group Calls",
    "emergencyClidPolicy" => "Emergency Calls",
    "allowAlternateNumbersForRedirectingIdentity" => "Allow Alternates Numbers for Redirecting Identity",
    "allowConfigurableCLIDForRedirectingIdentity" => "Allow Configurable CLID for Redirecting Identity",
    "blockCallingNameForExternalCalls" => "Block Calling Name for External Calls",
    "useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable" => "Use User Phone Number for Enterprise Calls when Internal CLID Unavailable",
    "useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable" => "Use User Phone Number for Group Calls when Internal CLID Unavailable",
    "mediaPolicySelection" => "External Calls",
    "useMaxSimultaneousCalls" => "Enable Maximum Number of Concurrent Calls",
    "maxSimultaneousCalls" => "Max Simultaneous Calls",
    "useMaxSimultaneousVideoCalls" => "Enable Maximum Number of Concurrent Video Calls",
    "maxSimultaneousVideoCalls" => "Max Simultaneous Video Calls",
    "useMaxCallTimeForAnsweredCalls" => "Enable Maximum Duration for Answered Calls",
    "maxCallTimeForAnsweredCallsMinutes" => "Max Call Time For Answered Calls Minutes",
    "useMaxCallTimeForUnansweredCalls" => "Enable Maximum Duration for Unanswered Calls",
    "maxCallTimeForUnansweredCallsMinutes" => "Max Call Time For Unanswered Calls Minutes",
    "useMaxConcurrentRedirectedCalls" => "Enable Maximum Number of Concurrent Redirected Calls",
    "maxConcurrentRedirectedCalls" => "Max Concurrent Redirected Calls",
    "useMaxConcurrentFindMeFollowMeInvocations" => "Enable Maximum Number of Concurrent Find Me/Follow Me Invocations",
    "maxConcurrentFindMeFollowMeInvocations" => "Max Concurrent Find Me Follow Me Invocations",
    "useMaxFindMeFollowMeDepth" => "Enable Maximum Find Me/Follow Me Depth",
    "maxFindMeFollowMeDepth" => "Max Find Me Follow Me Depth",
    "maxRedirectionDepth" => "Max Redirection Depth",
    "useSettingLevel" => "Use Conference URI Settings",
    "conferenceURI" => "Conference-URI sip",
    "networkUsageSelection" => "Network Usage",
    "enableEnterpriseExtensionDialing" => "Enable Enterprise Extension Dialing",
    "enforceGroupCallingLineIdentityRestriction" => "Enforce Group Calling Line Identity Restriction",
    "enforceEnterpriseCallingLineIdentityRestriction" => "Enforce Enterprise Calling Line Identity Restriction",
    "allowEnterpriseGroupCallTypingForPrivateDialingPlan" => "Allow Enterprise/Group Call Typing For Private Dialing Plan",
    "allowEnterpriseGroupCallTypingForPublicDialingPlan" => "Allow Enterprise/Group Call Typing For Public Dialing Plan",
    "overrideCLIDRestrictionForPrivateCallCategory" => "Override CLID Restriction For Private Call Category",
    "useEnterpriseCLIDForPrivateCallCategory" => "Use Enterprise CLID For Private Call Category",
    "useServiceProviderDCLIDSetting" => "Dialable Caller ID Policy",
    "enableDialableCallerID" => "Disable Caller ID",
    "enablePhoneListLookup" => "Enable Phone List Lookup",
    "useSystemDefaultDeliveryFromAddress" => "Use System Default Delivery From Address",
    "deliveryFromAddress" => "Delivery From Address",
    "useSystemDefaultNotificationFromAddress" => "Use System Default Notification From Address",
    "notificationFromAddress" => "Notification From Address",
    "useSystemDefaultVoicePortalLockoutFromAddress" => "Use System Default Voice Portal Lockout From Address",
    "voicePortalLockoutFromAddress" => "Voice Portal Lockout From Address",
    "assigned" => "Network class of services assigned",
    "unassigned" => "Network class of services unassigned",
    "default" => "Default network class of service",
    "routingProfile" => "Routing Profile",
    "rulesApplyTo" => "Rules apply to",
    "allowWebAddExternalAuthenticationUsers" => "Allow users to be created on web portal",
    "disallowUserId" => "Cannot contain the login ID",
    "disallowOldPassword" => "Cannot contain the old password",
    "disallowReversedOldPassword" => "Cannot be the reverse of the old password",
    "disallowPreviousPasswords" => "Cannot be any of the last",
    "numberOfPreviousPasswords" => "Number of previous passwords",
    "restrictMinDigits" => "Must contain at least",
    "minDigits" => "Password min digits",
    "restrictMinUpperCaseLetters" => "Must contain upper case letters",
    "minUpperCaseLetters" => "Minimum upper case letters",
    "restrictMinLowerCaseLetters" => "Must contain lower case letters",
    "minLowerCaseLetters" => "Minimum lower case letters",
    "restrictMinNonAlphanumericCharacters" => "Must contains non alpha numeric characters",
    "minNonAlphanumericCharacters" => "Minimum non alpha numeric charecters",
    "minLength" => "Passwords min length",
    "passwordExpiresDays" => "Password expiration days",
    "forcePasswordChangeAfterReset" => "Force Password Change After Reset",
    "maxFailedLoginAttempts" => "Max Failed Login Attempts",
    "sendLoginDisabledNotifyEmail" => "Send Login Disabled Notify Email",
    "loginDisabledNotifyEmailAddress" => "Login Disabled Notify Email Address",
    "useServiceProviderSettings" => "Use Service Provider Settings",
    "disallowAuthenticationName" => "Cannot contain the authentication user name",
    "endpointAuthenticationLockoutType" => "Endpoint Lockout Settings",
    "endpointTemporaryLockoutThreshold" => "Endpoint Temporary Lockout Threshold",
    "endpointWaitAlgorithm" => "Endpoint Wait Algorithm",
    "endpointLockoutFixedMinutes" => "End Point Lockout Fixed Minutes",
    "endpointAuthenticationLockoutTypeCheckbox" => "Permanently lock out after",
    "endpointPermanentLockoutThreshold" => "Endpoint Permanent Lockout Threshold",
    "trunkGroupAuthenticationLockoutType" => "Trunk Group Authentication Lockout Type",
    "trunkGroupWaitAlgorithm" => "Trunk Group Wait Algorithm",
    "trunkGroupLockoutFixedMinutes" => "Trunk Group Lockout Fixed Minutes",
    "trunkGroupAuthenticationLockoutTypeCheckbox" => "Permanently lock out after",
    "trunkGroupPermanentLockoutThreshold" => "Trunk Group Permanent Lockout Threshold",
    "sendPermanentLockoutNotification" => "Send Permanent Lockout Notification",
    "permanentLockoutNotifyEmailAddress" => "Permanent Lockout Notify EmailAddress",
    "trunkGroupTemporaryLockoutThreshold" => "Trunk Group Temporary Lockout Threshold",
    "deviceProfileAuthenticationLockoutType" => "Authentication Lockout",
    "accessCode" => "Access Code",
    "description" => "Description",
    "includeCodeForNetworkTranslationsAndRouting" => "Include Code for Network Translations and Routing",
    "includeCodeForScreeningServices" => "Include Code for Screening Services",
    "enableSecondaryDialTone" => "Enable Secondary Dial Tone",
    "requiresAccessCodeForPublicCalls" => "Requires Access Code for Public Calls",
    "allowE164PublicCalls" => "Allow E.164 Public Calls",
    "preferE164NumberFormatForCallbackServices" => "Prefer E.164 Number Format for Callback Services",
    "publicDigitMap" => "Public Digit Map",
    "privateDigitMap" => "Private Digit Map",
    "deviceProfileWaitAlgorithm" => "Device Profile Wait Algorithm",
    "deviceProfileLockoutFixedMinutes" => "Lockout Fixed Minutes",
    "deviceProfilePermanentLockoutThreshold" => "Device Profile Permanent Lockout Threshold",
    "deviceProfileTemporaryLockoutThreshold" => "Device Profile Temporary Lockout Threshold",
    "voicePortalScope"=> "Voice Portal Scope",
    "enterpriseCloneID" => "Clone Enterprise Name",
    "securityDomainsClone" => "Security Domains Clone",
    "domainsClone" => "Domains Clone",
    "groupServiceClone" => "Group Services Clone",
    "userServiceClone" => "User Service Clone",
    "clidPolicyClone" => "Calling Line ID Policy Clone",
    "mediaPolicyClone" => "Media Policy Clone",
    "callLimitsClone" => "Call Limits Clone",
    "conferencingPolicyClone" => "Conferencing Policy Clone",
    "tranlatingAndRoutingPoliciesClone" => "Tranlating and Routing Policies Clone",
    "disableCallerIDPolicyClone" => "Disable Caller ID Policy Clone",
    "dialPlanPolicyClone" => "Dial Plan Policy Clone",
    "voiceMessagingPolicyClone" => "Voice Messaging Policy Clone",
    "networkClassOfServicesClone" => "Network Class of Services Clone",
    "routingProfileClone" => "Routing Profile Clone",
    "passwordRulesClone" => "Password Rules Clone",
    "sipPasswordAuthClone" => "SIP Password Authentication Rules Clone",
    "deviceAuthPasswordClone" => "Device Profile Authentication Password Rules Clone",
    "disallowUserNumber" => "Cannot be the user's own extension or phone number",
    "disallowReversedUserNumber" => "Cannot be the user's own extension or phone number reversed",
    "disallowRepeatedDigits" => "Disallow Repeated Digits",
    "numberOfRepeatedDigits" => "Number of Repeated Digits",
    "disallowContiguousSequences" => "Disallow Contiguous Sequences",
    "numberOfAscendingDigits" => "Number of Ascending Digits",
    "numberOfDescendingDigits" => "Number of Descending Digits",
    "disallowRepeatedPatterns" => "Disallow Repeated Patterns",
    "disallowOldPasscode" => "Disallow Old Passcode PassCode",
    "numberOfPreviousPasscodes" => "Number of Previous Passcodes",
    "disallowReversedOldPasscode" => "Disallow Reversed Old Passcode",
    "minCodeLength" => "Min Code Length",
    "maxCodeLength" => "Max Code Length",
    "passcodeExpiresDays" => "Passcode Expires Days",
    "expirePassword" => "Passcode Expire",
    "disableLoginAfterMaxFailedLoginAttempts" => "Disable Login"
);

$basicInfoEntLogsArr                       = explode("_", $_SESSION["permissions"]["entPermBasicInformation"]);
$basicInfoEntLogPermission                 = $basicInfoEntLogsArr[0];
$basicInfoEntLogModPermission              = $basicInfoEntLogsArr[1];
$basicInfoEntLogAddDelPermission           = $basicInfoEntLogsArr[2];  //Done

 // 
?>

<script src="enterprise/js/getEnterpriseListAutoFill.js"></script>
<script type="text/javascript">

$(function() {

	var callbackFunctionToGetEnt = function(){
		$.ajax({
            url: "enterprise/loadEntByDefault.php",
            type:'POST',
            //data:{"selectedEnterprise" : entId},
            success: function(result)
            {
            	var obj = jQuery.parseJSON(result);
            	var dataP = obj.Success.enterpriseId;
            	var dataPQ = obj.Success.serviceProviderName;
            	var totalData = dataP+"   -   "+dataPQ;
            	$("#searchEnterpriseByAutoFill").val(totalData);
            	
            	$("#getEnterpriseInfo").trigger("click");
            }
        });
	}
	getEnterpriseList(callbackFunctionToGetEnt);
		
		$("#getEnterpriseInfo").click(function(){
			var selectedEnterprise = $("#searchEnterpriseByAutoFill").val();
			var tmpStr       = selectedEnterprise.split("  -  ");
            var entId      = tmpStr[0].trim();
			
			$("#enterpriseModifyData").html("");
			
			if(entId !== null && entId !== '') {
				$("#loadingEnt").show();
				$.ajax({
		            url: "enterprise/getEnterpriseInfo.php",
		            type:'POST',
		            data:{"selectedEnterprise" : entId},
		            success: function(result)
		            {
		            	$("#loadingEnt").hide();
		            	$("#enterpriseModifyData").html("");
		            	$("#enterpriseModifyData").html(result);
		            	getPreviousModuleId("enterpriseMod");
		            }
		        });
			}else{
				alert("Enterprise ID/Name is blank");
			}
			
		});

		$("#newEnterprise").click(function(){
			$("#searchEnterpriseByAutoFill").val("");
			$("#loadingEnt").show();
			$("#enterpriseModifyData").html("");
			$.ajax({
	            url: "enterprise/addEnterpriseUI.php",
	            type:'POST',
	            //data:{"selectedEnterprise" : selectedEnterprise},
	            success: function(result)
	            {
	            	$("#loadingEnt").hide();
	            	$("#enterpriseModifyData").html("");
	            	$("#enterpriseModifyData").html(result);
	            }
	        });
		});
		
});
</script>
<div id="modUserBody">

        <h2 class="h2Heading mainBannerModify">Enterprises</h2>
<!-- <div class="vertSpacer">&nbsp;</div> -->


<div style="">
        <div class="searchBodyForm">
        	<form name="searchEnterprise" id="searchEnterprise" method="POST" class="fcorn-register container">
			<div class="row">
				<div class="col-md-11 adminSelectDiv" style="padding-left:0;">
						<div class="form-group">
						<label class="labelText" for="searchVal">Search By Enterprise Id or Name</label><br>
						<input style="" type="text" class="autoFill magnify" name="searchEnterprise" id="searchEnterpriseByAutoFill" size="55">
						<div id="hidden-stuffEnt" style="height: 0px;"></div>
						</div>
				</div>
                            
                            <?php
                            if($basicInfoEntLogPermission == "1" && $basicInfoEntLogAddDelPermission == "yes"){
                            ?>
				<div class="col-md-1 adminAddBtnDiv">
                                        <div class="form-group">
                                                        <p class="register-submit addAdminBtnIcon" style="text-align:center;">
                                                        <img src="images\icons\add_admin_rest.png" data-alt-src="images\icons\add_admin_over.png" name="newEnterprise" id="newEnterprise">
                                                        <label class="labelText labelTextOpp"><span>Add</span><span style="margin-left: 8px;">Enterprise</span></label>
                                                                <!-- select style="width: 120px; display:none" name="groupName" id="groupName"></select-->
                                                        </p>
                                        </div>
                                </div>
                            <?php } ?>
                            
				<div class="col-md-12" style="text-align: center;display:none;">
					<div class="col-md-4"></div>
					<div class="col-md-4" style="text-align: center;"><input type="button" name="go" class="go" id="getEnterpriseInfo" value="Get"></div>
					<div class="col-md-4"></div>
				</div>
    		</div>
                   
        	</form>
        </div>
        <div class="loading" id="loadingEnt"><img src="/Express/images/ajax-loader.gif"></div>
        
        <div id="enterpriseModifyData">
        </div>
        
     </div>
</div> 
<div id="enterpriseDialog" class="dialogClass"></div>