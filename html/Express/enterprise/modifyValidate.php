<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
//print_r($_POST);
 // Security Domains
    $entPermSecurityDomainsLogArr              = explode("_", $_SESSION["permissions"]["entPermSecurityDomains"]);
    $entPermSecurityDomainsLogPermission       = $entPermSecurityDomainsLogArr[0];
    $entPermSecurityDomainsLogModPermission    = $entPermSecurityDomainsLogArr[1];
    
    $entPermDomainsLogArr                      = explode("_", $_SESSION["permissions"]["entPermDomains"]);
    $entPermDomainsLogPermission               = $entPermDomainsLogArr[0];
    $entPermDomainsLogModPermission            = $entPermDomainsLogArr[1];
//print_r($_SESSION["dialPlanAccessCodeModifyData"]);
//echo "post data"; print_r($_POST);
//echo "session data";print_r($_SESSION["allResp"]);
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/html/Express/enterprise/services/ServicesOperations.php");
require_once ("/var/www/html/Express/enterprise/administrator/AdministratorOperations.php");
$adminOP = new AdministratorOperations($_POST["basicInfo"]["enterpriseId"]);



$stringDomainAssigned = explode(";", $_POST["domainAssigned"]);
$stringSecurityDomainAssigned = explode(";", $_POST["securityDomainAssigned"]);

$_POST["domains"]["domainAssigned"] = $stringDomainAssigned;
$_POST["domains"]["securityDomainAssigned"] = $stringSecurityDomainAssigned;
$basicInfoChanges = array();
$assignedDomains = array();

/* Administrator Start */
$modifiedAdmins = isset($_SESSION['modifiedAdmins']) ? $_SESSION['modifiedAdmins'] : array();
$addedAdmins = isset($_SESSION['addedAdmins']) ? $_SESSION['addedAdmins'] : array();
$deleteAdmins = array();

if( isset($_POST['deleteAdmin']) || (isset($_SESSION['deleteAdmins']) && count($_SESSION['deleteAdmins']) > 0) ) {
    if(isset($_POST['deleteAdmin'])) {
        foreach ($_POST['deleteAdmin'] as $adminKey => $adminValue) {
            $deleteAdmins[] = $adminKey;
        }
    }
    if(isset($_SESSION['deleteAdmins']) && count($_SESSION['deleteAdmins']) > 0) {
        $deleteAdmins = array_merge($deleteAdmins, $_SESSION['deleteAdmins']);
    }
    foreach($deleteAdmins as $adminKey => $adminValue) {
        if( isset($modifiedAdmins[$adminValue]) ) {
            unset($modifiedAdmins[$adminValue]);
        }
    }
    foreach($addedAdmins as $addKey => $addValue) {
        if( in_array($addValue['administratorId'], $deleteAdmins) ) {
            unset($addedAdmins[$addKey]);
            unset($deleteAdmins[array_search($addValue['administratorId'], $deleteAdmins)]);
        }
    }
}
/* Administrator End*/

/* DNs Start*/

print_r($_SESSION['enterpriseDns']['removedDnNumber']);




$unassignedDNs = array();
$unassignedDNs["numbers"] = array();
$unassignedDNs["numbersRanges"] = "";
if( isset($_SESSION['enterpriseDns']['removedDnNumber']) && count(isset($_SESSION['enterpriseDns']['removedDnNumber'])) > 0 ) {
    $unassignedDNs['numbers'] = array_merge($unassignedDNs["numbers"], $_SESSION['enterpriseDns']['removedDnNumber']);
}

if( isset($_POST['unassignNumber']) && $_POST['unassignNumber'] != "" ) {
    $unassignNumber = explode(";;", rtrim($_POST['unassignNumber'], ";;"));
//     print_r($unassignNumber); exit;
    $unassignedDNs['numbers'] = array_merge($unassignedDNs["numbers"], $unassignNumber);
}
if( isset($_SESSION['enterpriseDns']['removedDnsRanges']) && count($_SESSION['enterpriseDns']['removedDnsRanges']) > 0) {
    $unassignedDNs['numbersRanges'] = $_SESSION['enterpriseDns']['removedDnsRanges'];
}
//print_r($unassignedDNs); exit;
/* DNs End*/

if($_POST["cpPolicy"]["conferenceURI"] == ""){
    $_POST["cpPolicy"]["conferenceURI"] = "";
}else{
    $_POST["cpPolicy"]["conferenceURI"] = $_POST["cpPolicy"]["conferenceURI"]."@".$_POST["cpPolicy"]["conferenceURIDomain"];
}

unset($_POST["cpPolicy"]["conferenceURIDomain"]);

if(empty($_SESSION["allResp"]["basicInfo"]["Error"])){
    foreach ($_POST["basicInfo"] as $key => $value){
        if($value != $_SESSION["allResp"]["basicInfo"]["Success"][$key]){
            $basicInfoChanges[$key] = $value;
        }
    }
}
 
//Code added @06 June 2019
if(empty($_SESSION["allResp"]["voicePortal"]["Error"])){
   
    foreach ($_POST["voicePortal"] as $key => $value){
         if($value =="Enterprise"){
                $value = "Service Provider";
            }
        if($value != $_SESSION["allResp"]["voicePortal"]["Success"][$key]){
            $voicePortalChanges[$key] = $value;
        }
    }
}
//End code
if($entPermDomainsLogModPermission == "yes"){
    $arrayDiff1 =  flip_isset_diff($_POST["domains"]["domainAssigned"], $_SESSION["allResp"]["domains"]["domainAssigned"]);

    $arrayDiff2 = flip_isset_diff($_SESSION["allResp"]["domains"]["domainAssigned"], $_POST["domains"]["domainAssigned"]);
    if(count($arrayDiff1) > 0 && !empty($arrayDiff1[0])){
        $assignedDomains["domains"]["newAssignedDomains"] = $arrayDiff1;
    }
    if(count($arrayDiff2) > 0 && !empty($arrayDiff2[0])){
        $assignedDomains["domains"]["unAssignedDomains"] = $arrayDiff2;
    }
}

if($entPermSecurityDomainsLogModPermission == "yes"){
    $arrayDiff3 =  flip_isset_diff($_POST["domains"]["securityDomainAssigned"], $_SESSION["allResp"]["domains"]["securityDomainAssigned"]);
    $arrayDiff4 = flip_isset_diff($_SESSION["allResp"]["domains"]["securityDomainAssigned"], $_POST["domains"]["securityDomainAssigned"]);
    if(count($arrayDiff3) > 0 && !empty($arrayDiff3[0])){
        $assignedDomains["domains"]["newAssignedSecurityDomains"] = $arrayDiff3;
    }
    if(count($arrayDiff4) > 0 && !empty($arrayDiff4[0])){
        $assignedDomains["domains"]["unAssignedSecurityDomains"] = $arrayDiff4;
    }
}


$cpsChangeArray = array();
foreach ($_POST["cpPolicy"] as $key => $value){
    if($value != $_SESSION["allResp"]["callProcessingPolicy"]["Success"][$key]){
        $cpsChangeArray[$key] = $value;
    }
}


$voiceMessegingChangeArray = array();
foreach ($_POST["voiceMessaging"] as $key => $value){
    if($value != $_SESSION["allResp"]["voiceMessaging"]["Success"][$key]){
        $voiceMessegingChangeArray[$key] = $value;
    }
}

$serviceOP = new ServicesOperations($_POST["basicInfo"]["enterpriseId"]);
$_POST['allServiceData'] = $_SESSION['modifiedServicesToBeUpdate'] = $serviceOP->createServiceData($_POST);

$ncsDataValidation = array();
foreach ($_POST["assignedNCS"] as $key => $value){
    if(!in_array($value, $_SESSION["allResp"]["ncsData"]["entData"])){
        $ncsDataValidation["assigned"][] = $value;
    }
}

foreach ($_SESSION["allResp"]["ncsData"]["entData"] as $key => $value){
    if(!in_array($value, $_POST["assignedNCS"])){
        $ncsDataValidation["unassigned"][] = $value;
    }
}

foreach ($_POST["assignedDefault"] as $key => $value){
    if(!in_array($value, $_SESSION["allResp"]["ncsData"]["defaultData"])){
        $ncsDataValidation["default"][] = $value;
    }
}

//routing Profile
$routingProfileChanges = array();
//print_r($_POST["routingProfile"]["routingProfile"]);
//print_r($_SESSION["allResp"]["selectedRoutingProfile"]);
if(isset($_POST["routingProfile"]["routingProfile"])){
     
if($_POST["routingProfile"]["routingProfile"] != $_SESSION["allResp"]["selectedRoutingProfile"]["Success"]){
    $routingProfileChanges["routingProfile"] =  $_POST["routingProfile"]["routingProfile"];
}
}
$passwordRulesChange = array();
foreach($_POST["password"] as $passKey => $passVal){
    if($passVal != $_SESSION["allResp"]["passwordRules"]["Success"][$passKey]){
        $passwordRulesChange[$passKey] = $passVal;
    }
}

$passcodeRulesChange = array();
foreach($_POST["passcode"] as $passcodeKey => $passcodeVal){
    if($passcodeVal != $_SESSION["allResp"]["passcodeRules"]["Success"][$passcodeKey]){
        $passcodeRulesChange[$passcodeKey] = $passcodeVal;
    }
}
//print_r($_POST["passcode"]);
//print_r($_SESSION["allResp"]["passcodeRules"]);
//print_r($passcodeRulesChange);//die;
if($_POST["sipPassword"]["endpointAuthenticationLockoutType"] == "Temporary"){
    if($_POST["sipPassword"]["endpointAuthenticationLockoutTypeCheckbox"] == "true"){
        $_POST["sipPassword"]["endpointAuthenticationLockoutType"] = "Temporary Then Permanent";
        unset($_POST["sipPassword"]["endpointAuthenticationLockoutTypeCheckbox"]);
    }else{
        $_POST["sipPassword"]["endpointAuthenticationLockoutType"] = "Temporary";
        unset($_POST["sipPassword"]["endpointAuthenticationLockoutTypeCheckbox"]);
    }
}
unset($_POST["sipPassword"]["endpointAuthenticationLockoutTypeCheckbox"]);

if($_POST["sipPassword"]["trunkGroupAuthenticationLockoutType"] == "Temporary"){
    if($_POST["sipPassword"]["trunkGroupAuthenticationLockoutTypeCheckbox"] == "true"){
        $_POST["sipPassword"]["trunkGroupAuthenticationLockoutType"] = "Temporary Then Permanent";
        unset($_POST["sipPassword"]["trunkGroupAuthenticationLockoutTypeCheckbox"]);
    }else{
        $_POST["sipPassword"]["trunkGroupAuthenticationLockoutType"] = "Temporary";
        unset($_POST["sipPassword"]["trunkGroupAuthenticationLockoutTypeCheckbox"]);
    }
}
unset($_POST["sipPassword"]["trunkGroupAuthenticationLockoutTypeCheckbox"]);

$sipPasswordRulesChange = array();
foreach ($_POST["sipPassword"] as $sipKey => $sipPass){
    if($sipPass != $_SESSION["allResp"]["sipPasswordRules"]["Success"][$sipKey]){
        $sipPasswordRulesChange[$sipKey] = $sipPass;
    }
}
if($_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutType"] == "Temporary"){
    if($_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutTypeChk"] == "true"){
        $_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutType"] = "Temporary Then Permanent";
        unset($_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutTypeChk"]);
    }else{
        $_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutType"] = "Temporary";
        unset($_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutTypeChk"]);
    }
}
unset($_POST["devicePasswordRules"]["deviceProfileAuthenticationLockoutTypeChk"]);

$devicePasswordRulesChange = array();
foreach ($_POST["devicePasswordRules"] as $devpKey => $devpPass){
    if($devpPass != $_SESSION["allResp"]["devicePasswordRules"]["Success"][$devpKey]){
        $devicePasswordRulesChange[$devpKey] = $devpPass;
    }
}
//print_r($passwordRulesChange);
//print_r($sipPasswordRulesChange);
//print_r($devicePasswordRulesChange);
$dialPlanDataChange = array();
//print_r($_SESSION["allResp"]["dialPlanData"]);
//print_r($_POST["dialPolicy"]);
foreach ($_POST["dialPolicy"] as $dKey => $dValue){
    if($dValue != $_SESSION["allResp"]["dialPlanData"]["Success"][$dKey]){
        $dialPlanDataChange[$dKey] = $dValue;
    }
}
//print_r($dialPlanDataChange);
$error = 0;
$data = $errorTableHeader;

//Code added @ 06 June 2019
if(count($voicePortalChanges) > 0){
    
    foreach ($voicePortalChanges as $key => $value)
    {
        $bg = CHANGED;
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k3 => $v3)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k3."</td><td style='width:50%;'>".$v3."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k3."</td><td style='width:50%;'>".$v3."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}
//End code

if(count($basicInfoChanges) > 0){
    
    foreach ($basicInfoChanges as $key => $value)
    {
        $bg = CHANGED;
        
        if($key == "contactEmail" && $value != ""){
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $error = 1;
                $bg = INVALID;
                $value = "Invalid email format.";
            }
        }
        
        if($key == "supportEmail" && $value != ""){
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $error = 1;
                $bg = INVALID;
                $value = "Invalid email format.";
            }
        }
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}

if(count($_SESSION["domainsAdd"]) > 0){
    foreach ($_SESSION["domainsAdd"] as $key => $value)
    {
        $bg = CHANGED;
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">Domain added</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}

if(count($_SESSION["securityDomainsAdd"]) > 0){
    foreach ($_SESSION["securityDomainsAdd"] as $key => $value)
    {
        $bg = CHANGED;
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">Security domain added</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}

if(count($assignedDomains) > 0){
    foreach ($assignedDomains["domains"] as $key => $value)
    {
        $bg = CHANGED;
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}


if(count($cpsChangeArray) > 0){
    foreach ($cpsChangeArray as $key => $value)
    {
        $bg = CHANGED;
        
        if($key == "maxSimultaneousCalls"){
            if($value == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of simultaneous calls can not be empty.";
            }else if(!is_numeric ($value)){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of simultaneous calls should be numeric.";
            }else if($value < 1 || $value > 999999){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of simultaneous calls: Value must be >=1 and <= 999999.";
            }
            
        }
        
        if($key == "maxSimultaneousVideoCalls"){
            if($value == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of simultaneous video calls can not be empty.";
            }else if(!is_numeric ($value)){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of simultaneous video calls should be numeric.";
            }else if($value < 1 || $value > 999999){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of simultaneous video calls: Value must be >=1 and <= 999999.";
            }
            
        }
        
        if($key == "maxCallTimeForAnsweredCallsMinutes"){
            if($value == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum call time for answered calls can not be empty.";
            }else if(!is_numeric ($value)){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum call time for answered calls should be numeric.";
            }else if(($value < 3 || $value > 2880)) {
                $error = 1;
                $bg = INVALID;
                $value = "Maximum call time for answered calls: Value must be >=3 and <= 2880.";
            }
            
        }
        
        if($key == "maxCallTimeForUnansweredCallsMinutes"){
            if($value == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum call time for unanswered calls can not be empty.";
            }else if(!is_numeric ($value)){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum call time for unanswered calls should be numeric.";
            }else if($value < 1 || $value > 2880){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum call time for unanswered calls: Value must be >=1 and <= 2880.";
            }
            
        }
        
        
        if($key == "maxConcurrentRedirectedCalls"){
            if($value == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of concurrent redirect calls can not be empty.";
            }else if(!is_numeric ($value)){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of concurrent redirect calls should be numeric.";
            }else if($value < 1 || $value > 999999){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of concurrent redirect calls: Value must be >=1 and <= 999999.";
            }
            
        }
        
        if($key == "maxConcurrentFindMeFollowMeInvocations"){
            if($value == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of find me/follow me invocations can not be empty.";
            }else if(!is_numeric ($value)){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of find me/follow me invocations should be numeric.";
            }else if($value < 1 || $value > 999999){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of find me/follow me invocations: Value must be >=1 and <= 999999.";
            }
            
        }
        
        
        if($key == "maxFindMeFollowMeDepth"){
            if($value == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of find me/follow me depth can not be empty.";
            }else if(!is_numeric ($value)){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of find me/follow me depth should be numeric.";
            }else if($value < 1 || $value > 100){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of find me/follow me depth: Value must be >=1 and <= 100.";
            }
            
        }
        
        if($key == "maxRedirectionDepth"){
            if($value == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of redirection depth can not be empty.";
            }else if(!is_numeric ($value)){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of redirection depth should be numeric.";
            }else if($value < 1 || $value > 100){
                $error = 1;
                $bg = INVALID;
                $value = "Maximum number of redirection depth: Value must be >=1 and <= 100.";
            }
            
        }
        
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}

if( (isset($_SESSION['enterpriseDns']['newlyAddedDns']) && count($_SESSION['enterpriseDns']['newlyAddedDns']) > 0) ||
    (isset($_SESSION['enterpriseDns']['newlyAddedDnsRanges']) && count($_SESSION['enterpriseDns']['newlyAddedDnsRanges']) > 0)
) {
    $bg = CHANGED;
    if ($bg != UNCHANGED)
    {
        $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . "Add DNs" . "</td><td class=\"errorTableRows\">";
        if($_SESSION['enterpriseDns']['newlyAddedDns']) {
            $newlyAddedDns = $_SESSION['enterpriseDns']['newlyAddedDns'];
            foreach($newlyAddedDns as $dnKey => $dnValue) {
                $data .= "<p>". $dnValue ."</p>";
            }
        }
        if($_SESSION['enterpriseDns']['newlyAddedDnsRanges']) {
            $newlyAddedDnsRanges = $_SESSION['enterpriseDns']['newlyAddedDnsRanges'];
            foreach($newlyAddedDnsRanges as $dnRKey => $dnRValue) {
                $data .= "<p>". $dnRValue['minPhoneNumber'] . " - " . $dnRValue['maxPhoneNumber'] ."</p>";
            }
        }
        $data .= "</td></tr>";
    }
}

if( count($unassignedDNs['numbers']) > 0 || $unassignedDNs['numbersRanges'] != "") {
    //$unassignNumber = explode(";;", rtrim($_POST['unassignNumber'], ";;"));
    if( count($unassignedDNs) > 0) {
        $bg = CHANGED;
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . "Delete DNs" . "</td><td class=\"errorTableRows\">";
            if( isset($unassignedDNs['numbers']) ) {
                foreach($unassignedDNs['numbers'] as $unDnKey => $unDnValue) {
                    $data .= "<p>". $unDnValue ."</p>";
                }
            }
            if( isset($unassignedDNs['numbersRanges']) ) {
                foreach($unassignedDNs['numbersRanges'] as $unDnKey => $unDnValue) {
                    $rangeDn = $unDnValue['minPhoneNumber'] ." - ". $unDnValue['maxPhoneNumber'];
                    $data .= "<p>". $rangeDn ."</p>";
                }
            }
            
            $data .= "</td></tr>";
        }
    }
}

if( isset($deleteAdmins) && count($deleteAdmins) > 0 ) {
    $bg = CHANGED;
    if ($bg != UNCHANGED)
    {
        $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . "Delete Administrators" . "</td><td class=\"errorTableRows\">";
        $data .= "<table style='width:100%'>";
        foreach ($deleteAdmins as $delKey => $delValue) {
            $data .= "<tr style=\"border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;background:#72ac5d;'> ". "Administrator Id" ." </td><td style='width:50%;color:#9c9c9c;'>".$delValue."</td></tr>";
            }
        $data .= "</table></br>";
        $data .= "</td></tr>";
    }
}

// print_r($_SESSION['modifiedAdministratorOldData']); exit;
if(count($modifiedAdmins) > 0) {
    $bg = CHANGED;
    
    if ($bg != UNCHANGED)
    {
        $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . "Modify Administrators" . "</td><td class=\"errorTableRows\">";
        foreach ($modifiedAdmins as $keyMod => $keyValue) {
            $data .= "<table style='width:100%'>";
          //  $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'> Field Name </td><td style='width:50%;border-right:1px solid #fff;padding:5px;'> Old Value </td><td style='width:50%;'>New Value</td></tr>";
              $data .= "<tr style=\"border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;background:#72ac5d;'> Field Name </td><td style='width:50%;border-right:1px solid #fff;padding:5px;background:#72ac5d;'> Old Value </td><td style='width:50%;background:#72ac5d;'>New Value</td></tr>";
            foreach ($keyValue as $adKey => $adValue) {
                if( $adKey == "initialPassword" || $adKey == "initialConformPassword") {
                    continue;
                }
                if($adKey == "password") {
                    $adValue = "******";
                }
                //$data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'> ". $adminOP->formField[$adKey] ." </td><td style='width:50%;border-right:1px solid #fff;padding:5px;'> ". $_SESSION['modifiedAdministratorOldData'][$keyValue['administratorId']][$adKey] ." </td><td style='width:50%;'>".$adValue."</td></tr>";
                $data .= "<tr style=\"border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;background:#72ac5d;'> ". $adminOP->formField[$adKey] ." </td><td style='width:50%;border-right:1px solid #fff;padding:5px;color:#9c9c9c;'> ". $_SESSION['modifiedAdministratorOldData'][$keyValue['administratorId']][$adKey] ." </td><td style='width:50%;border-right:1px solid #fff;padding:5px;color:#9c9c9c;'>".$adValue."</td></tr>";
            }
            $data .= "</table></br>";
        }
        $data .= "</td></tr>";
    }
}

if(count($addedAdmins) > 0) {
    $bg = CHANGED;
    if ($bg != UNCHANGED)
    {
        $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . "Add Administrators" . "</td><td class=\"errorTableRows\">";
        foreach ($addedAdmins as $keyMod => $keyValue) {
            $data .= "<table style='width:100%'>";
            foreach ($keyValue as $modKey => $modValue) {
                if($modKey == "password") {
                    $modValue = "******";
                }
                if($modKey == "administratorId" || $modKey == "firstName" || $modKey == "lastName" || $modKey == "password" || $modKey == "language" || $modKey == "administratorType") {
                    $data .= "<tr style=\"border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='background:" . $bg . "; width:50%;border-right:1px solid #fff;padding:5px;'> ". $adminOP->formField[$modKey] ." </td><td style='width:50%;color:#9c9c9c;'>".$modValue."</td></tr>";
                }
            }
            $data .= "</table></br>";
        }
        $data .= "</td></tr>";
    }
}

if( count($_POST['allServiceData']) > 0) {
    foreach($_POST['allServiceData'] as $sKey => $sValue) {
        $bg = CHANGED;
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $serviceOP->serviceInputName[$sKey] . "</td><td class=\"errorTableRows\">";
            if (is_array($sValue))
            {
                foreach ($sValue as $k => $v)
                {

                    $limitedValue = "";
                    $errorMsg = "";
                    $bg = CHANGED;
                    $data .= "<table style='width:100%'><tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'> Service Name </td><td style='width:50%;'>".$k."</td></tr>";
                    if( (isset($v['quantity']) || isset($v['limited'])) && 
                            ( ( isset($v['authorized']) && $v['authorized'] == "true") ||
                                ( !isset($v['authorized']) && $_SESSION[$sKey][$k]['authorized'] == "true") 
                            )
                    ) {
                                if( $v['limited'] == "true" && isset($v['quantity']) ) {
                                    if( (isset($v['quantity']) && $v['quantity'] < $_SESSION[$sKey][$k]['allocated']) ||
                                        isset($v['quantity']) && $_SESSION[$sKey][$k]['allocated'] == "Unlimited"
                                     ) {
                                            $errorMsg = ServicesOperations::EXCEEDS_LIMIT;
                                            $bg = INVALID;
                                            $error = 1;
                                       } else if( $v['quantity'] == "" || !ctype_digit($v['quantity'])) {
                                            $errorMsg = ServicesOperations::INVALID_VALUE;
                                            $bg = INVALID;
                                            $error = 1;
                                     } else {
                                        $limitedValue = $v['quantity'];
                                    }
                                } else if( isset($v['limited']) && $v['limited'] == "true" && !isset($v['quantity']) ||
                                           !isset($v['limited']) && isset($v['quantity']) && $v['quantity'] == "" ||
                                           !ctype_digit($v['quantity'])
                                    ) {
                                        $errorMsg = ServicesOperations::INVALID_VALUE;
                                        $bg = INVALID;
                                        $error = 1;
                                } else if( (isset($v['quantity']) && $v['quantity'] < $_SESSION[$sKey][$k]['allocated']) ||
                                           (isset($v['quantity']) && $_SESSION[$sKey][$k]['allocated'] == "Unlimited") 
                                 ) {
                                     $errorMsg = ServicesOperations::EXCEEDS_LIMIT;
                                     $bg = INVALID;
                                     $error = 1;
                                } else if( isset($v['limited']) && $v['limited'] == "false" ) {
                                    $limitedValue = "Unlimited";
                                } else if( !isset($v['limited']) && isset($v['quantity']) ) {
                                    $limitedValue = $v['quantity'];
                                }
                                
                                if($bg==INVALID){
                                    $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$serviceOP->serviceInputName['limited']."</td><td style='width:50%;'>".$errorMsg."</td></tr>";
                                } else {
                                    $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$serviceOP->serviceInputName['limited']."</td><td style='width:50%;'>". $limitedValue ."</td></tr>";
                                }
                                
                            }
                            if ( isset($v['authorized']) ) {
                                $bg = CHANGED;
                                $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$serviceOP->serviceInputName['authorized']."</td><td style='width:50%;'>".$v['authorized']."</td></tr>";
                            }
                     
                    $data .= "</table></br>";
                }
            }else if ($sValue !== "")
            {
                $data .= $sValue;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</td></tr>";
        }
    }
}
//print_r($voiceMessegingChangeArray);
if(count($voiceMessegingChangeArray) > 0){
    
    foreach ($voiceMessegingChangeArray as $key => $value)
    {
        $bg = CHANGED;
        
        if($key == "useSystemDefaultDeliveryFromAddress" && $value == "false"){
            if($_POST["voiceMessaging"]["deliveryFromAddress"] == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Voice message delivery : Default from email address can not be blank.";
            }else{
                if (!filter_var($_POST["voiceMessaging"]["deliveryFromAddress"], FILTER_VALIDATE_EMAIL)) {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Invalid email format.";
                }
            }
            
        }
        
        if($key == "useSystemDefaultNotificationFromAddress" && $value == "false"){
            if($_POST["voiceMessaging"]["notificationFromAddress"] == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Voice message notification : Default from email address can not be blank.";
            }else{
                if (!filter_var($_POST["voiceMessaging"]["notificationFromAddress"], FILTER_VALIDATE_EMAIL)) {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Invalid email format.";
                }
            }
            
        }
        
        if($key == "useSystemDefaultVoicePortalLockoutFromAddress" && $value == "false"){
            if($_POST["voiceMessaging"]["voicePortalLockoutFromAddress"] == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Voice portal passcode lookout : Default from email address can not be blank.";
            }else{
                if (!filter_var($_POST["voiceMessaging"]["voicePortalLockoutFromAddress"], FILTER_VALIDATE_EMAIL)) {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Invalid email format.";
                }
            }
            
        }
        
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}

if(count($ncsDataValidation) > 0){
    
    
    foreach ($ncsDataValidation as $key => $value)
    {
        $bg = CHANGED;
        
        if(count($_POST["assignedNCS"]) > 0 && empty($_POST["assignedDefault"])){
            $error = 1;
            $bg = INVALID;
            $value = "Default network class of services can not be empty.";
        }
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}

if(count($routingProfileChanges) > 0){
    
    
    foreach ($routingProfileChanges as $key => $value)
    {
        $bg = CHANGED;
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}

if(count($passwordRulesChange) > 0){
    
    
    foreach ($passwordRulesChange as $key => $value)
    {
        $bg = CHANGED;
        
        if($key == "passwordExpiresDays"){
            if($value == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Password expiration days can not be empty.";
            }else if(!is_numeric ($value)){
                $error = 1;
                $bg = INVALID;
                $value = "Password expiration days should be numeric.";
            }else if($value < 0 || $value > 199){
                $error = 1;
                $bg = INVALID;
                $value = "Password expiration days: Value must be >=1 and <= 199.";
            }
            
        }
        
        if($key == "sendLoginDisabledNotifyEmail"){
            if($value == "true" && $_POST["password"]["loginDisabledNotifyEmailAddress"] == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Email address is required field.";
            }
            
        }
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}

if(count($passcodeRulesChange) > 0){
    foreach ($passcodeRulesChange as $key => $value)
    {
        $bg = CHANGED;
        
        if($key == "expirePassword" && $value == "true"){
            if($_POST["passcode"]["passcodeExpiresDays"] == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Password expiration days can not be empty.";
            }else if(!is_numeric ($_POST["passcode"]["passcodeExpiresDays"])){
                $error = 1;
                $bg = INVALID;
                $value = "Password expiration days should be numeric.";
            }else if($_POST["passcode"]["passcodeExpiresDays"] < 15 || $_POST["passcode"]["passcodeExpiresDays"] > 180){
                $error = 1;
                $bg = INVALID;
                $value = "Password expiration days: Value must be >=15 and <= 180.";
            }
        }
        
        
        if($key == "sendLoginDisabledNotifyEmail"){
            if($value == "true" && $_POST["passcode"]["loginDisabledNotifyEmailAddress"] == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Email address is required field.";
            }
            
        }
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}

if(count($sipPasswordRulesChange) > 0){
    
    
    foreach ($sipPasswordRulesChange as $key => $value)
    {
        $bg = CHANGED;
        
        if($key == "sendPermanentLockoutNotification"){
            if($value == "true" && $_POST["sipPassword"]["permanentLockoutNotifyEmailAddress"] == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Email address is required field.";
            }
            
        }
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}

if(count($devicePasswordRulesChange) > 0){
    foreach ($devicePasswordRulesChange as $key => $value)
    {
        $bg = CHANGED;
        
        if($key == "sendPermanentLockoutNotification"){
            if($value == "true" && $_POST["devicePasswordRules"]["permanentLockoutNotifyEmailAddress"] == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Email address is required field.";
            }
            
        }
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}

if(count($_SESSION["dialPlanPolicyToAdd"]) > 0){
    foreach ($_SESSION["dialPlanPolicyToAdd"] as $key1 => $value1)
    {
        foreach ($value1 as $key => $value){
            $bg = CHANGED;
            
            if ($bg != UNCHANGED)
            {
                $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
                if (is_array($value))
                {
                    foreach ($value as $k => $v)
                    {
                        $bg = CHANGED;
                        
                        if($bg==INVALID){
                            $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                        }else{
                            $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                        }
                        
                    }
                }else if ($value !== "")
                {
                    $data .= $value;
                }
                else
                {
                    $data .= "None";
                }
                
                $data .= "</table></td></tr>";
            }
        }
        
    }
}

//$_SESSION["dialPlanAccessCodeModifyData"]
if(count($_SESSION["dialPlanAccessCodeModifyData"]) > 0){
    foreach ($_SESSION["dialPlanAccessCodeModifyData"] as $key1 => $value1)
    {
        foreach ($value1 as $key => $value){
            $bg = CHANGED;
            
            if ($bg != UNCHANGED)
            {
                $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\"><table style='width:100%'>";
                if (is_array($value))
                {
                    foreach ($value as $k => $v)
                    {
                        $bg = CHANGED;
                        
                        if($bg==INVALID){
                            $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                        }else{
                            $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                        }
                        
                    }
                }else if ($value !== "")
                {
                    $data .= $value;
                }
                else
                {
                    $data .= "None";
                }
                
                $data .= "</table></td></tr>";
            }
        }
        
    }
}

if(count($dialPlanDataChange) > 0){
    foreach ($dialPlanDataChange as $key => $value){
            $bg = CHANGED;
            
            if($key == "publicDigitMap"){
                if($value != ""){
                    if(!is_numeric($value)){
                        $error = 1;
                        $bg = INVALID;
                        $value = "Public digit map should be numeric.";
                    }
                }
                
                
            }
            if($key == "privateDigitMap"){
                if($value != ""){
                    if(!is_numeric($value)){
                        $error = 1;
                        $bg = INVALID;
                        $value = "Private digit map should be numeric.";
                    }
                }else if($value == ""){
                    $dialCheck = false;
                    foreach ($_SESSION["allResp"]["dialPlanAccessCodeList"] as $dtKey => $dtValue){
                        if($dtValue["enableSecondaryDialTone"] == "true"){
                            $dialCheck = true;
                        }
                    }
                    if($dialCheck){
                        $error = 1;
                        $bg = INVALID;
                        $value = "Private digit map is required because one of more policy access codes have 'Secondary Dial Tone' flag enabled.";
                    }
                }
            }
            
            if ($bg != UNCHANGED)
            {
                $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\" style='word-break:break-all'><table style='width:100%'>";
                if (is_array($value))
                {
                    foreach ($value as $k => $v)
                    {
                        $bg = CHANGED;
                        
                        if($bg==INVALID){
                            $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                        }else{
                            $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                        }
                        
                    }
                }else if ($value !== "")
                {
                    $data .= $value;
                }
                else
                {
                    $data .= "None";
                }
                
                $data .= "</table></td></tr>";
            }
        }
}

if(count($_POST["dialPlanAccessCode"]) > 0){
    foreach ($_POST["dialPlanAccessCode"] as $key => $value){
        $bg = CHANGED;
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";\">Dial plan access code to be deleted</td><td class=\"errorTableRows\" style='word-break:break-all'><table style='width:100%'>";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $bg = CHANGED;
                    
                    if($bg==INVALID){
                        $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }else{
                        $data .= "<tr style=\"background:transparent;border:1px solid #fff;\"><td style='width:50%;border-right:1px solid #fff;padding:5px;'>".$k."</td><td style='width:50%;'>".$v."</td></tr>";
                    }
                    
                }
            }else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            
            $data .= "</table></td></tr>";
        }
    }
}

if(empty($basicInfoChanges) && empty($assignedDomains) && empty($cpsChangeArray) &&  empty($_POST['allServiceData']) && empty($voiceMessegingChangeArray) && empty($ncsDataValidation) && empty($routingProfileChanges) && empty($passwordRulesChange) && empty($passcodeRulesChange) && empty($sipPasswordRulesChange) && empty($devicePasswordRulesChange) && empty($voicePortalChanges) &&
    count($modifiedAdmins) == 0 && count($addedAdmins) == 0 && count($deleteAdmins) == 0 && ( !isset($_SESSION['enterpriseDns']) || count($_SESSION['enterpriseDns']) == 0) && $_POST['unassignNumber'] == "" && empty($_SESSION["dialPlanPolicyToAdd"]) && empty($dialPlanDataChange) && empty($_POST["dialPlanAccessCode"]) && empty($_SESSION["dialPlanAccessCodeModifyData"]) && empty($_SESSION["domainsAdd"]) && empty($_SESSION["securityDomainsAdd"])){
    $error = 1;
    $bg = INVALID;
    $data .= "<tr style=\"border:1px solid #fff;color:#fff;vertical-align:middle;\"><td class='errorTableRows' style='background:#72ac5d;'>Enterprise Changes</td><td style='width:50%;' class='errorTableRows'>No changes.</td></tr>";
}

$data .= "</table>";
echo $error . $data;

function flip_isset_diff($b, $a) {
    $at = array_flip($a);
    $d = array();
    foreach ($b as $i)
        if (!isset($at[$i]))
            $d[] = $i;
            
            return $d;
}
?>