<!--add new administrator ui -->
    
    <form action="#" method="POST" name="newAdministratorForm" id="newAdministratorForm" class="fcorn-registerTwo">
	<h2 class="adminUserText"> <?php echo $module == "Add Administrator" ? "Add Administrators" : "Modify Administrators"; ?></h2>
        <div class="row"> 
            <div class="col-md-6">
                <div class="form-group">
                            <label class="labelText" for="AdministratorID">Administrator ID :
                            <?php if($module == "Add Administrator") { ?>
                                    </label> <span class="required">*</span>
                            <input type="text" name="administratorId" id="administratorId" size="35" maxlength="32" value="<?php echo $administratorId; ?>">    
                            <?php } else {?>
                                    <input type="hidden" name="administratorId" id="administratorId" value="<?php echo $administratorId; ?>">
                                    <label class="labelTextGrey"> <?php echo $administratorId; ?> </label>
                            <?php } ?>

                </div>
            </div>
        
        </div>
            
        <div class="row">
        	<div class="col-md-6">
               <div class="form-group">
                       <label class="labelText" for="AdministratorFName">First Name :</label><span class="required">*</span>
                       <input type="text" name="firstName" id="firstName" size="35" maxlength="32" value="<?php echo $firstName; ?>">
               </div>
           </div>
            
            <div class="col-md-6">
                <div class="form-group">
                        <label class="labelText" for="AdministratorLName">Last Name :</label><span class="required">*</span>
                        <input type="text" name="lastName" id="lastName" size="35" maxlength="32" value="<?php echo $lastName; ?>">
                </div>
            </div>
        
        </div>
        
        <div class="row">
            
            <div class="col-md-6">
                <div class="form-group">
                        <label class="labelText" for="initialPassword"> Initial Password :</label> 
                        <?php if($module == "Add Administrator") { ?> <span class="required">*</span> <?php } ?>
                        <input type="password" name="initialPassword" id="initialPassword" size="35" maxlength="32" value="">
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="form-group">
                        <label class="labelText" for="initialConformPassword">Re-type Initial Password :</label> 
                        <?php if($module == "Add Administrator") { ?> <span class="required">*</span> <?php } ?>
                        <input type="password" name="initialConformPassword" id="initialConformPassword" size="35" maxlength="32" value="">
                </div>
            </div>
        
           
             
        </div>
        
        <div class="row">
         <div class="col-md-6">
                <div class="form-group">
                    <label class="labelText" for="language">Language :</label>
                    <div class="dropdown-wrap">
                        <select name="language" id="language">
                            <option value="English" <?php $language == "English" ? "selected" : ""?>>English</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        
      <div class="row">
		<div class="">
			<div class="col-md-3" style="margin-top: 11px;">
				<div class="form-group">
					<label class="labelText">Administrator Type :</label>
				</div>
			</div>
		<?php if ($module == "Add Administrator") {  ?>
			<div class="boundaryArea">
				<div class="col-md-3 labelTextMarginAdminTypeSP">
					<div class="form-group">
						 <input type="radio" name="administratorType" id="administratorTypeSP" class="checkbox-lg" value="Normal" checked>
						 <label class="labelText labelTextMargin" for="administratorTypeSP"><span></span>Service Provider</label>
					</div>
				</div>
     
				<div class="col-md-3 labelTextMarginAdminCustomer">
					<div class="form-group">
						<input type="radio" name="administratorType" id="administratorTypeCu" class="checkbox-lg" value="Customer">
						<label class="labelText" for="administratorTypeCu"><span></span>Customer</label>
					</div>
				</div>
			
				<div class="col-md-3">
					<div class="form-group">
						<input type="radio" name="administratorType" id="administratorTypePRO" class="checkbox-lg" value="Password Reset Only">
						<label class="labelText" for="administratorTypePRO"><span></span>Password Reset Only</label>
					</div>
				</div>
			</div>
			<?php } else {  ?>
                                <div class="col-md-3" style="margin-top:11px;">
                                    <div class="form-group">
                                      <?php $administratorType =  $administratorType == "Normal" ? "Service Provider" : $administratorType; ?>
                                        <label class="labelTextGrey" for=""><?php echo $administratorType ; ?></label>
                                    </div>
				</div>
			    
			<?php }   ?>
		</div><!-- end col-md-12 -->
	</div><!--end Administrator Type div-->
        
        
    </form>
    

<!--end new administrator ui -->




<div id="entAdministratorDialog" class="dialogClass"></div>