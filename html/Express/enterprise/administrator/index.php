<script src="enterprise/administrator/js/administrator.js"></script>
<script src="enterprise/administrator/js/downloadAsCsv.js"></script>
<div id="EntAdministratorBody">
    <h2 class="h2Heading mainBannerModify">Administrator</h2>
     
        <div class="searchBodyForm">
<!--             <form name="searchAdministratorForm" id="searchAdministratorForm" method="POST" class="fcorn-register container"> -->
                <div class="row">
                    <div class="col-md-11 adminSelectDiv">
                        <div class="form-group">
                            <label class="labelText" for="searchVal">Search By Administrator</label><br>
                            <input placeholder="Type here for Search Administrator by Administrator ID, First Name, Last Name, Administrator Type" style="width: 100% !important" type="text" class="autoFill magnify" name="searchAdministrator" id="searchAdministrator" size="55" value="">
                        </div>
                    </div>
                    
                    <div class="col-md-1 adminAddBtnDiv">
                        <?php
                            if($entPermBWAdminsLogPermission == "1" && $entPermBWAdminsLogAddDelPermission == "yes"){
                            ?>
                        <div class="form-group">
                            <p class="register-submit addAdminBtnIcon" style="text-align:center;">
                            <img src="images\icons\add_admin_rest.png" data-alt-src="images\icons\add_admin_over.png" name="newAdministrator" id="newEnterpriseAdministrator">
                            <label class="labelText labelTextOpp"><span>Add</span><span style="margin-left: 8px;">Administrator</span></label>
                                    <!-- select style="width: 120px; display:none" name="groupName" id="groupName"></select-->
                            </p>
                        </div>
                        <?php 
                            }
                        ?>
                    </div>

                    <div class="col-md-12 form-group" style="text-align: center;">
                        <div class="col-md-4"></div>
                        <div class="col-md-4" style="text-align: center;"><input type="button" name="getAdministratorBtn" class="cancelButton" id="getAdministratorBtn" value="Get"></div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
<!--             </form> -->
        </div>
        <div class="loading" id="loadingAdmins"><img src="/Express/images/ajax-loader.gif"></div>
        
       
        <!--get enterprise Administrator modify data -->
</div>
<div class="entAdministratorsDiv form-group"  id="entAdministratorsDiv">
  <!--  <h2 class="adminUserText">Administrator </h2> -->
    <div class="row">
        <div class="col-md-6 checkAllAdmin">
            <?php
                  if($entPermBWAdminsLogPermission == "1" && $entPermBWAdminsLogAddDelPermission == "yes"){
            ?>
            <input type="checkbox" class="inputCheckbox" name="checkAllAdministrator" id="checkAllAdministrator">
            <label for="checkAllAdministrator" style="margin-left:15px;"><span></span></label>
            <label class="labelText" style="margin-top: 53px;"> Select All</label>
            <?php 
                  }
            ?>
        </div>

        <div class="col-md-6">
            <div class="fcorn-register" style="">
                <div class="dataTables_length limitSearch" id="" style="float:right;margin-top: 41px;margin-left: 25px;display:none;">
                    
                        <select name="administrator_length" class="administrator_length">
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="200">200</option>
                            <option value="All Records" selected>All Records</option>
                        </select> 
                        
                </div>
                <div class="" id="downloadAdministratorCSV" style="float:right;margin-top:8px;">
                    <img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" id="downloadAdminCSV">
                    <br><span>Download<br>CSV</span>
                </div>
            </div>
        </div>
    </div>

    <div class="" style="margin-bottom: 25px;">
        <table id="enterpriseAdministratorTable" class="tablesorter scroll tagTableHeight dataTable" style="margin: 0">
            <thead>
                <tr>
                    <th class="thsmall header" style="max-width:7% !important;">Delete</th>
                    <th class="thsmall header" style=""> Administrator ID</th>
                    <th class="thsmall header">Administrator Type</th>
                    <th class="thsmall header">Last Name</th>
                    <th class="thsmall header">First Name</th>
                    <th class="thsmall header">Language</th>                    
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        <p id="noDataFoundDiv" style="display: none;text-align:center">No Administrator Found.</p>
    </div>
 </div>
 
<!--add new administrator ui -->

<div id="newAdministratorDiv" style="display:none">    
    <div id="addAdministratorFormDiv" class="fcorn-register container" style="width:830px;margin:0 auto;">
       
    </div>
</div>
<div id="newAdministratorDialog" class="dialogClass">
<div id="inputFormControl"></div>
<div id="validationDiv"></div>
</div>
