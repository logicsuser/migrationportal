var module = "";
var deletedAdmin = "";
var entPermBWAdminsLogAddDelPermission  = $("#entPermBWAdminsLogAddDelPermission").val();
var entPermBWAdminsLogPermission        = $("#entPermBWAdminsLogPermission").val();
var entPermBWAdminsLogModPermission     = $("#entPermBWAdminsLogModPermission").val();
 $(function(){
	
	//$("#getAdministratorBtn").trigger("click");
	loadAdmins();
	
	$(document).on("keyup","#searchAdministrator",function(e){
		var key = e.which;
		 if(key == 13)
		  {
		    $('#getAdministratorBtn').click();
		    return false;  
		  }
    });
	
    $(document).on("click","#getAdministratorBtn",function(){
    	//loadAdmins();
    	var searchString = $("#searchAdministrator").val();
    	$("#enterpriseAdministratorTable tbody tr").filter(function() {
    		$(this).toggle($(this).text().toLowerCase().indexOf(searchString) > -1);
        });
    	
    	if($("#enterpriseAdministratorTable tbody tr:visible").length > 0 ) {
    		$("#noDataFoundDiv").hide();
    	} else {
    		$("#noDataFoundDiv").show();
    	}
    });	
    
    /*add new administrator */
    $("#newEnterpriseAdministrator").click(function(){
    //$(document).on("click","#newEnterpriseAdministrator",function(){
        $("#loading2").show();
         $.ajax({
           type: "POST",
           url: "enterprise/administrator/administratorActions.php",
           data: { action: 'addAdministrator'},
           success: function(result)
           {
               $("#newAdministratorDialog").dialog("open");
               $("#newAdministratorDiv").show();
               $("#newAdministratorDialog").find("#inputFormControl").html(result).show();
               $("#newAdministratorDialog").find("#validationDiv").html("").hide();
               
               if(entPermBWAdminsLogAddDelPermission =='yes' && entPermBWAdminsLogPermission =='1'){
                    $(".ui-dialog-buttonpane button:contains('Add Administrator')").button().show().addClass("subButton");
                }else if(entPermBWAdminsLogAddDelPermission =='no' && entPermBWAdminsLogPermission =="1"){
                    $(":button:contains('Add Administrator')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
                }
                else{
                    $(".ui-dialog-buttonpane button:contains('Add Administrator')").button().hide();
                }
               //$(".ui-dialog-buttonpane button:contains('Add Administrator')").button().show().addClass("subButton");
      	   	$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass("cancelButton");; 
           }
       });
       
     });
    
    /*end add new administrator */
    
    /*adiministrator add dialog */
    $("#newAdministratorDialog").dialog({
        autoOpen: false,
        width: 800,
        modal: true,
        title: "",
        position: { my: "top", at: "top" },
        resizable: false,
        closeOnEscape: false,
        buttons: {
            
        	'Delete Administrator' : function(){
        	module = "deletedAdministrator";
            	deletedAdmin = $("form#newAdministratorForm").serializeArray();
                if(entPermBWAdminsLogAddDelPermission =='yes' && entPermBWAdminsLogPermission =='1'){
                    $(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass("deleteButton");
                }else if(entPermBWAdminsLogAddDelPermission =='no' && entPermBWAdminsLogPermission =="1"){
                    $(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
                }
                else{
                    $(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
                }   
                
            	$(".ui-dialog-buttonpane button:contains('Delete Administrator')").button().hide();
            	$(".ui-dialog-buttonpane button:contains('Modify Administrator')").button().hide();
            	$("#newAdministratorDialog").find("#validationDiv").html("Are you sure want to delete administrator ? ").show();
            	$("#newAdministratorDialog").find("#inputFormControl").hide();
            },
            'Delete' : function(){
            	module = "deletedAdministrator";
            	$(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
            	deleteAdministrator(deletedAdmin);
            },
            'Add Administrator' : function(){
            	 module = "addAdmin";
            	 var addAdminFormData = $("form#newAdministratorForm").serializeArray();
            	 $(".ui-dialog-buttonpane button:contains('Add Administrator')").button().hide();
                 $(".ui-dialog-buttonpane button:contains('Modify Administrator')").button().hide();
            	 addAdminFormDataValidation(addAdminFormData);
            },
            'Modify Administrator' : function(){
            	module = "modifyAdmin";
            	var modifyAdminFormData = $("form#newAdministratorForm").serializeArray();
            	$(".ui-dialog-buttonpane button:contains('Modify Administrator')").button().hide();
            	$(".ui-dialog-buttonpane button:contains('Delete Administrator')").button().hide();
            	modifyAdminFormDataValidation(modifyAdminFormData);
            },
            'Return To Main' : function(){
        	 location.href="main_enterprise.php";
            },
            'Complete': function() {
            	if(module == "addAdmin") {
            		completeAddAdmin();
            	} else if(module == "modifyAdmin") {
            		completeModifyAdmin();
            	}
            	module = "";
            },
            'Done': function() {
            	loadAdmins();
            	$(this).dialog("close");
            },
           'Close': function() {
                $(this).dialog("close");
            },              
            'Cancel': function() {
            	resumeFormOnCancel($(this));
            },
       },
       open: function() {
       	setDialogDayNightMode($(this));
       		module = "";
 	  		$(".ui-dialog-buttonpane button:contains('Close')").button().hide();
 	   		$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide(); 
 	   		$(".ui-dialog-buttonpane button:contains('Close')").button().hide();
 	   		$(".ui-dialog-buttonpane button:contains('Return To Main')").button().hide();
 	   		$(".ui-dialog-buttonpane button:contains('Add Administrator')").button().hide();
 	   		$(".ui-dialog-buttonpane button:contains('Modify Administrator')").button().hide();
 	   		$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
 	   		$(".ui-dialog-buttonpane button:contains('Delete Administrator')").button().hide();
 	   		$(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
 	   		$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
       }
    });
	

    
   //
   $(".administrator_length").change(function() {
	var thisClass = $(this).attr("class");
        var currentTable;
        var showTotalRow = $(this).val();
        if( thisClass == "administrator_length") {
            currentTable = "enterpriseAdministratorTable";
        } 
        if(showTotalRow == "All Records") {
            $("#" + currentTable + " tbody tr").toggle(true);
        }else {
            var count = 0;
            $("#" + currentTable + " tbody tr").filter(function() {
                $(this).toggle( count < showTotalRow );
                count++;
            });	
        }
    });
    
    //check all administrator
    $("#checkAllAdministrator").change(function() { 
        var isChecked = $(this).prop("checked");
        $(".deleteCheckboxAdmin").prop("checked", isChecked);
            $("#enterpriseAdministratorTable").filter(function(){
            if( isChecked == false) {
                $(this).find(".deleteCheckboxAdmin").prop("checked", false);
            } else {
                $(this).find(".deleteCheckboxAdmin").prop("checked", true);
            }
        });  
				
    });
   
   $(document).on("change", ".deleteCheckboxAdmin", function() {
        var totalAdminCheckBox = $(".deleteCheckboxAdmin").length;
        var totalCheckedTableCheckBox = $("input:checkbox.deleteCheckboxAdmin:checkbox:checked").length;
        if(totalAdminCheckBox == totalCheckedTableCheckBox) {
            $("#checkAllAdministrator").prop("checked", true);
        } else {
            $("#checkAllAdministrator").prop("checked", false);
        }
	
    });
        
});
  
var adminDataForAdd = "";
function addAdminFormDataValidation(addAdminFormData) {
	var selectedSp = $("#hiddenEnterpriseName").val();
	adminDataForAdd = addAdminFormData;
	$.ajax({
        type: "POST",
        url: "enterprise/administrator/administratorActions.php",
        data: { action: 'validateAddAdminData', 'addAdminFormData': addAdminFormData, selectedSp: selectedSp},
        success: function(result)
        {
        	result = result.trim();                      	
            if (result.slice(0, 1) == 1){
                $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
            }else{
                $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
            }
            $("#newAdministratorDialog").dialog("open");
            $("#newAdministratorDiv").show();
            $("#newAdministratorDialog").find("#validationDiv").html(result).show();
            $("#newAdministratorDialog").find("#inputFormControl").hide();
            $(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass("subButton");
   	   		$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass("cancelButton"); 
        }
    });
}

var adminDataForModify = "";

function deleteAdministrator(deletedAdmin) {
	adminDataForModify = deletedAdmin;
	$.ajax({
        type: "POST",
        url: "enterprise/administrator/administratorActions.php",
        data: { action: 'deletedAdministrator', 'deletedAdminData': adminDataForModify},
        success: function(result)
        {
        	result = result.trim();                      	
            $("#newAdministratorDialog").dialog("open");
            $("#newAdministratorDiv").show();
            $("#newAdministratorDialog").find("#validationDiv").html(result).show();
   	   		$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
   	   		$(".ui-dialog-buttonpane button:contains('Done')").button().addClass("subButton").show();
        }
    });
}

function modifyAdminFormDataValidation(modifyAdminFormData) {
	adminDataForModify = modifyAdminFormData;
	$.ajax({
        type: "POST",
        url: "enterprise/administrator/administratorActions.php",
        data: { action: 'validateModifyAdminData', 'modifyAdminFormData': modifyAdminFormData},
        success: function(result)
        {
        	result = result.trim();                      	
            if (result.slice(0, 1) == 1){
                $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
            }else{
                $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
            }
            $("#newAdministratorDialog").dialog("open");
            $("#newAdministratorDiv").show();
            $("#newAdministratorDialog").find("#validationDiv").html(result).show();
            $("#newAdministratorDialog").find("#inputFormControl").hide();
            $(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass("subButton");
   	   		$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass("cancelButton"); 
        }
    });
}

function completeAddAdmin() {
		var selectedSp = $("#hiddenEnterpriseName").val();
		$.ajax({
	        type: "POST",
	        url: "enterprise/administrator/administratorActions.php",
	        data: { action: 'completeAddAdmin', 'addAdminFormData': adminDataForAdd, selectedSp: selectedSp},
	        success: function(result)
	        {
	            $("#newAdministratorDialog").dialog("open");
	            $("#newAdministratorDiv").show();
	            $("#newAdministratorDialog").find("#validationDiv").html(result).show();
	            $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
	   	   		$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
	   	   		$(".ui-dialog-buttonpane button:contains('Done')").button().addClass("subButton").show();
	        }
	    });
}


function completeModifyAdmin() {
	var selectedSp = $("#hiddenEnterpriseName").val();
	$.ajax({
        type: "POST",
        url: "enterprise/administrator/administratorActions.php",
        data: { action: 'completeModifyAdmin', 'modifyAdminFormData': adminDataForModify, selectedSp: selectedSp},
        success: function(result)
        {
            $("#newAdministratorDialog").dialog("open");
            $("#newAdministratorDiv").show();
            $("#newAdministratorDialog").find("#validationDiv").html(result).show();
            $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
   	   		$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
   	   		$(".ui-dialog-buttonpane button:contains('Done')").button().addClass("subButton").show();
        }
    });
}

function modifyAdministrator(adminData, event) {
	var target = event.target.tagName;
	
	if(target == "INPUT" || target == "SPAN") {
			return;
	}
	$.ajax({
        type: "POST",
        url: "enterprise/administrator/administratorActions.php",
        data: { action: 'modifyAdministrator', adminData: adminData},
        success: function(result)
        {
            $("#newAdministratorDialog").dialog("open");
            $("#newAdministratorDiv").show();
            $("#newAdministratorDialog").find("#inputFormControl").html(result).show();
            $("#newAdministratorDialog").find("#validationDiv").html("").hide();
               
               if(entPermBWAdminsLogAddDelPermission =='yes' && entPermBWAdminsLogPermission =='1'){
                    $(".ui-dialog-buttonpane button:contains('Delete Administrator')").button().show().addClass("deleteButton");
                }else if(entPermBWAdminsLogAddDelPermission =='no' && entPermBWAdminsLogPermission =="1"){
                    $(":button:contains('Delete Administrator')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
                }
                else{
                    $(".ui-dialog-buttonpane button:contains('Delete Administrator')").button().hide();
                }
                 
                if(entPermBWAdminsLogPermission =="1" && entPermBWAdminsLogModPermission =="yes"){
                   $(".ui-dialog-buttonpane button:contains('Modify Administrator')").button().show().addClass("subButton");
                }else if(entPermBWAdminsLogModPermission =='no' && entPermBWAdminsLogPermission =="1"){
                     $(":button:contains('Modify Administrator')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
                }else{
                    $(".ui-dialog-buttonpane button:contains('Modify Administrator')").button().hide(); 
                }
                
                $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass("cancelButton");;
             
        }
    });
}

function loadAdmins() {
	$("#loadingAdmins").show();
	$("#entAdministratorsDiv").hide();
	var selectedSp = $("#hiddenEnterpriseName").val();
    $.ajax({
       type: "POST",
       url: "enterprise/administrator/administratorActions.php",
       data: { action: 'getAdminList', selectedSp: selectedSp },
       success: function(result)
       {
    	   var result = JSON.parse(result);
           $("#loadingAdmins").hide();
           $("#entAdministratorsDiv").show();
           if(result.status == "success") {
        	   $("#enterpriseAdministratorTable tbody").html(result.administratorTable);
        	   $("#enterpriseAdministratorTable").tablesorter();
           } else if(result.status == "failure") {
        	   alert(result.errorMsg);
           }
       }
   });
}

function resumeFormOnCancel(thisEl) {	
	if(module == "addAdmin") {
            $("#newAdministratorDialog").find("#validationDiv").html("").hide();
            $("#newAdministratorDialog").find("#inputFormControl").show();
            
            
            if(entPermBWAdminsLogAddDelPermission =='yes' && entPermBWAdminsLogPermission =='1'){
                $(".ui-dialog-buttonpane button:contains('Add Administrator')").button().show().addClass("subButton");
            }else if(entPermBWAdminsLogAddDelPermission =='no' && entPermBWAdminsLogPermission =="1"){
                $(":button:contains('Add Administrator')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
            }
            else{
                $(".ui-dialog-buttonpane button:contains('Add Administrator')").button().hide();
            }
            
                //$(".ui-dialog-buttonpane button:contains('Add Administrator')").button().show();
		$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
	} else if(module == "modifyAdmin") {
		$("#newAdministratorDialog").find("#validationDiv").html("").hide();
                $("#newAdministratorDialog").find("#inputFormControl").show();
                
                
                
            if(entPermBWAdminsLogModPermission =='yes' && entPermBWAdminsLogPermission =='1'){
                $(".ui-dialog-buttonpane button:contains('Modify Administrator')").button().show().addClass("subButton");
            }else if(entPermBWAdminsLogModPermission =='no' && entPermBWAdminsLogPermission =="1"){
                $(":button:contains('Modify Administrator')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
            }
            else{
                $(".ui-dialog-buttonpane button:contains('Modify Administrator')").button().hide();
            }
                
             if(entPermBWAdminsLogAddDelPermission =='yes' && entPermBWAdminsLogPermission =='1'){
                $(".ui-dialog-buttonpane button:contains('Delete Administrator')").button().show().addClass("deleteButton");
            }else if(entPermBWAdminsLogAddDelPermission =='no' && entPermBWAdminsLogPermission =="1"){
                $(":button:contains('Delete Administrator')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
            }
            else{
                $(".ui-dialog-buttonpane button:contains('Delete Administrator')").button().hide();
            }   
                
                
		// $(".ui-dialog-buttonpane button:contains('Modify Administrator')").button().show();
    	// $(".ui-dialog-buttonpane button:contains('Delete Administrator')").button().show().addClass("deleteButton");
    	$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
	} else {
		thisEl.dialog("close");            		
	}
	module = "";
}