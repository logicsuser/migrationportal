 <?php
 require_once ("/var/www/lib/broadsoft/adminPortal/enterprise/enterpriseOperations.php");
 
 class AdministratorOperations {
    public $entId;
    public $formField = array(
                            "administratorId" => "Aministrator ID",
                            "initialPassword" => "Initial Password",
                            "firstName" => "First Name",
                            "lastName" => "Last Name",
                            "initialConformPassword" => "Initial Conform Password",
                            "language" => "Language",
                            "administratorType" => "Administrator Type",
                            "password" => "Password"
                        );
    
    public function __construct($entId) {
        $this->entId = $entId;
     }
     
    public function createAdminTableRow($adminList) {
        $tableRow = "";
        //Code added @ 29 May 2019
        $entPermBWAdminsLogDupArr                     = explode("_", $_SESSION["permissions"]["entPermBWAdministrators"]);
        $entPermBWAdminsLogDupPermission              = $entPermBWAdminsLogDupArr[0];
        $entPermBWAdminsLogDupModPermission           = $entPermBWAdminsLogDupArr[1];
        $entPermBWAdminsLogDupAddDelPermission        = $entPermBWAdminsLogDupArr[2];
        //End code
        
        if( isset($_SESSION['addedAdmins']) && count($_SESSION['addedAdmins']) > 0 ) {
            $adminList = array_merge($adminList, $_SESSION['addedAdmins']);
        }
        if($entPermBWAdminsLogDupAddDelPermission=="no"){
            $hideCheckbox = "display:none";
        }
        
        if( count($adminList) > 0) {
            foreach( $adminList as $adminKey => $adminValue) {
                
                if( isset($_SESSION['deleteAdmins']) && count($_SESSION['deleteAdmins']) > 0 ) {
                    if( in_array($adminValue['administratorId'], $_SESSION['deleteAdmins']) ) {
                        continue;   
                    }
                }
                
                $thisAdmin = $_SESSION['modifiedAdmins'][$adminValue['administratorId']];
                if( isset($_SESSION['modifiedAdmins']) && count($_SESSION['modifiedAdmins']) > 0 ) {
                    $adminValue['firstName'] = isset($thisAdmin['firstName']) ? $thisAdmin['firstName'] : $adminValue['firstName'];
                    $adminValue['lastName'] = isset($thisAdmin['lastName']) ? $thisAdmin['lastName'] : $adminValue['lastName'];
                    $adminValue['language'] = isset($thisAdmin['language']) ? $thisAdmin['language'] : $adminValue['language'];
                }
                
                $adminData = json_encode($adminValue);
                 if ($entPermBWAdminsLogDupPermission == "1" && $entPermBWAdminsLogDupModPermission == "yes") {
                        $tableRow .= "<tr onclick='modifyAdministrator($adminData, event)'>";
                }else{
                        $tableRow .= "<tr>";
                }
                $checkboxId = $adminValue['administratorId'];
                $tableRow .= "<td class='serWidthClass' style='max-width:7% !important;'> <input class='inputCheckbox deleteCheckboxAdmin' style='$hideCheckbox' type='checkbox' name='deleteAdmin[$checkboxId]' id='$checkboxId' value='false'> <label class='' for='$checkboxId' style='$hideCheckbox'><span></span></label> </td>";
                $tableRow .= "<td> " . $adminValue['administratorId'] . " </td>";
                $tableRow .= "<td> " . $adminValue['administratorType'] . " </td>";
                $tableRow .= "<td> " . $adminValue['lastName'] . " </td>";
                $tableRow .= "<td> " . $adminValue['firstName'] . " </td>";
                $tableRow .= "<td> " . $adminValue['language'] . " </td>";
                $tableRow .= "</tr>";
            }
        } else {
            $tableRow .= "<tr><td colspan=100%>No Administrator Found.</td></tr>";
        }
               
        return $tableRow;
    }
    
    public function isAdministratorIdAlreadyExist($adminId) {
        $entOp = new enterpriseOperations();
        $response = "";
        $adminList = $entOp->serviceProviderAdminGetListRequest($this->entId);
        if( empty($adminList['Error']) ) {
            $adminList = $adminList['Success'];
            if( isset($_SESSION['addedAdmins']) && count($_SESSION['addedAdmins']) > 0 ) {
                $adminList = array_merge($adminList, $_SESSION['addedAdmins']);
            }
        }
        
        $isAdminExist = false;
        foreach ($adminList as $admiKey => $adminValue) {
            if($adminValue['administratorId'] == $adminId) {
                $isAdminExist = true;
                break;
            }
        }
        
        return $isAdminExist;
    }
    
    public function getEnterpriseAdministrator() {
        $entOp = new enterpriseOperations();
        $response = "";
        $adminList = $entOp->serviceProviderAdminGetListRequest($this->entId); 
        
        if( empty($adminList['Error']) ) {
            $response = array("status" => "success", "administratorTable" => $this->createAdminTableRow($adminList['Success']));
        } else {
            $response = array("status" => "failure", "errorMsg" => $adminList['Error']);
        }
        
        return $response;
    }
     
    public function addAdministrator($addAdminFormData) {
        $entOp = new enterpriseOperations();
        return $entOp->serviceProviderAdminAddRequest($this->entId, $addAdminFormData);
    }
    
    public function modifyAdministrator($modifyAdminFormData) {
        $entOp = new enterpriseOperations();
        return $entOp->serviceProviderAdminModifyRequest($this->entId, $modifyAdminFormData);
    }
    
    public function createChangeLogData($changedAdminData) {
        $logArray = array();
        foreach($changedAdminData as $logKey => $logValue) {
            $adminId = $changedAdminData['administratorId'];
            $fieldName = $changedAdminData['administratorId'] ."(Modify Administrator):". $this->formField[$logKey];
            if($logKey == "password") {
                $logArray[$fieldName]['oldValue'] = "********";
                $logArray[$fieldName]['newValue'] = "********";
            } else {
                $logArray[$fieldName]['oldValue'] = $_SESSION['modifiedAdministratorOldData'][$adminId][$logKey];
                $logArray[$fieldName]['newValue'] = $logValue;
            }
        }
        
        return $logArray;
    }
    
    public function createChangeLogDataAddAdmin($changedAdminData) {
        $logArray = array();
        foreach($changedAdminData as $logKey => $logValue) {
            if ($logKey == "initialPassword" || $logKey == "initialConformPassword") {
                continue;
            }
            $fieldName = $changedAdminData['administratorId'] ."(Add Administrator):". $this->formField[$logKey];
            $logArray[$fieldName]['oldValue'] = "";
            $logArray[$fieldName]['newValue'] = $logValue;
        }
        
        return $logArray;
    }
    
    public function createChangeLogDataDeleteAdmin($adminId) {
        $logArray = array();
        $fieldName = $adminId ."(Delete Administrator):". $this->formField['administratorId'];
        $logArray[$fieldName]['oldValue'] = "";
        $logArray[$fieldName]['newValue'] = $adminId;
        
        return $logArray;
    }
        
 }
 
 ?>
