<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/html/Express/enterprise/administrator/AdministratorOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");

if( isset($_POST['action']) && $_POST['action'] == "getAdminList" ) {
    $objAdmin = new AdministratorOperations($_POST['selectedSp']);   
    $adminResponse = $objAdmin->getEnterpriseAdministrator();
    
    if($adminResponse['status'] == "failure") {
        $response = array("status" => "failure", "errorMsg" => $adminResponse['errorMsg']);
    } else {
        $response = array("status" => "success", "administratorTable" => $adminResponse['administratorTable']);
    }
    
    echo json_encode($response);
    die;
}

else if( isset($_POST['action']) && $_POST['action'] == "addAdministrator" ) {
    $module = "Add Administrator";
    $administratorId = "";
    $firstName = "";
    $lastName = "";
    $language = "";
    $administratorType = "";
    require_once ("/var/www/html/Express/enterprise/administrator/addAdmin.php");
}

else if( isset($_POST['action']) && $_POST['action'] == "modifyAdministrator" ) {
    $module = "Modify Administrator";
    $administratorId            = isset($_POST['adminData']['administratorId']) ? $_POST['adminData']['administratorId'] : "";
    $firstName         = isset($_POST['adminData']['firstName']) ? $_POST['adminData']['firstName'] : "";
    $lastName         = isset($_POST['adminData']['lastName']) ? $_POST['adminData']['lastName'] : "";
//  $initialPassword            = isset($_POST['adminData']['initialPassword']) ? $_POST['adminData']['initialPassword'] : "";
//  $initialConformPassword     = isset($_POST['adminData']['initialConformPassword']) ? $_POST['adminData']['initialConformPassword'] : "";
    $language                   = isset($_POST['adminData']['language']) ? $_POST['adminData']['language'] : "";
    $administratorType      = isset($_POST['adminData']['administratorType']) ? $_POST['adminData']['administratorType'] : "";
    
    $_SESSION['modifiedAdministratorOldData'][$administratorId] = array("administratorId" => $administratorId, "firstName" => $firstName, "lastName" => $lastName, "language" => $language);
    require_once ("/var/www/html/Express/enterprise/administrator/addAdmin.php");
    
}

else if( isset($_POST['action']) && $_POST['action'] == "validateAddAdminData" ) {
    
    $addAdminFormData = $_POST['addAdminFormData'];
    $data .= '<table style="width:850px; margin: 0 auto; width="100%" cellpadding="5" class="legendRGTable"><tr><th colspan="2" align="center">Please confirm the settings below and click Complete to process your modifications.</th></tr><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></br>';
    $data .= "<table style='width:850px; margin: 0 auto;'>";
    
    $password = "";
    $cfPassword = "";
    foreach( $addAdminFormData as $adminKey => $adminValue) {
        
        if($adminValue["name"] == "initialPassword") {
            $password =  $adminValue["value"];
            $adminValue["name"] = "password";
        } else if($adminValue["name"] == "initialConformPassword") {
            $cfPassword = $adminValue["value"];
            continue;
        }
        
        $objAdmin = new AdministratorOperations($_POST['selectedSp']);
        $bg = CHANGED;
        
        if($adminValue["name"] == "administratorId" || $adminValue["name"] == "password" || $adminValue["name"] == "firstName" || $adminValue["name"] == "lastName"){
            if( $adminValue["value"] == "" ) {
                $error = 1;
                $bg = INVALID;
                $errorMsg = $objAdmin->formField[$adminValue['name']] . " is required field";
            }
            else if( $adminValue["name"] == "password" && $adminValue["value"] != "" && (strlen($adminValue["value"]) < 6 || strlen($adminValue["value"]) > 60)) {
                $error = 1;
                $bg = INVALID;
                $errorMsg = $objAdmin->formField[$adminValue['name']] . " Invalid password. The password must be at least 6 characters and not more than 60 characters";
            }
            else if($adminValue["name"] == "administratorId" && (strlen($adminValue["value"]) < 6 || strlen($adminValue["value"]) > 80) ) {
                $error = 1;
                $bg = INVALID;
                $errorMsg = "Invalid Administrator ID. The ID must be at least 6 characters long and not greater than 80 characters long.";
            }else if($adminValue["name"] == "administratorId" &&  strpos($adminValue["value"], " ") !== false) {
                $error = 1;
                $bg = INVALID;
                $errorMsg = "Invalid Administrator ID. Each character in the Administrator ID must be either a letter, a digit, a single quote, or one of the following characters: ! # $ % & + - / = ? ^ _ ` { | } . ~.";
            }else if($adminValue["name"] == "administratorId" &&  $adminValue["value"] != "") {
                if( $objAdmin->isAdministratorIdAlreadyExist($adminValue["value"]) ) {
                    $error = 1;
                    $bg = INVALID;
                    $errorMsg = $adminValue["value"] . " Administrator ID is already exist.";
                }
            }else if($adminValue["name"] == "firstName" &&  $adminValue["value"] == "") {
                    $error = 1;
                    $bg = INVALID;
                    $errorMsg = $objAdmin->formField[$adminValue['name']] . " is required field";
            }else if($adminValue["name"] == "lastName" &&  $adminValue["value"] == "") {
                $error = 1;
                $bg = INVALID;
                $errorMsg = $objAdmin->formField[$adminValue['name']] . " is required field";
            }
        }
        
        if ($bg != UNCHANGED)
        {
            if($bg==INVALID){
                $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";width:50%\">" . $objAdmin->formField[$adminValue['name']] . "</td><td class=\"errorTableRows\" style=\"width:50%\">".$errorMsg."</td></tr>";
            }else{
                if($adminValue["name"] == "password") {
                    $adminValue["value"] = "********";
                }
                $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";width:50%\">" . $objAdmin->formField[$adminValue['name']] . "</td><td class=\"errorTableRows\" style=\"width:50%\">".$adminValue['value']."</td></tr>";
            }
        }
        
    }
    if( ($password != "" || $cfPassword != "") && ($password != $cfPassword) ) {
            $error = 1;
            $bg = INVALID;
            $errorMsg = "Password and Confirm Password must be similar.";
            
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";width:50%\"> Password Error</td><td class=\"errorTableRows\" style=\"width:50%\">".$errorMsg."</td></tr>";
    }
    $data .= "</table>";
    
    echo $error . $data;;
    die;
    
    
}

else if( isset($_POST['action']) && $_POST['action'] == "validateModifyAdminData" ) {
    
    $modifyAdminFormData = $_POST['modifyAdminFormData'];
    $data .= '<table style="width:850px; margin: 0 auto; width="100%" cellpadding="5" class="legendRGTable"><tr><th colspan="2" align="center">Please confirm the settings below and click Complete to process your modifications.</th></tr><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></br>';
    $data .= "<table style='width:850px; margin: 0 auto;'>";
    $password = "";
    $cfPassword = "";
    $changed = 0;

    $adminId = $modifyAdminFormData[0]['value'];
    foreach( $modifyAdminFormData as $adminKey => $adminValue) {
        if($adminValue["name"] == "administratorId") {
            continue;
        }
        
        if($adminValue["name"] == "initialPassword") {
            $password =  $adminValue["value"];
            $adminValue["name"] = "password";
        } else if($adminValue["name"] == "initialConformPassword") {
            $cfPassword = $adminValue["value"];
            continue;
        }
        
        $objAdmin = new AdministratorOperations($_POST['selectedSp']);
        $bg = CHANGED;
        
        if($_SESSION['modifiedAdministratorOldData'][$adminId][$adminValue["name"]] <> $adminValue["value"]) {
            $changed = 1;
            if( ($adminValue["name"] == "lastName" &&  $adminValue["value"] == "") ||
                ($adminValue["name"] == "firstName" &&  $adminValue["value"] == "")
            ) {
                $error = 1;
                $bg = INVALID;
                $errorMsg = $objAdmin->formField[$adminValue['name']] . " is required field";
            }
            if ($bg != UNCHANGED)
            {
                if($bg==INVALID){
                    $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";width:50%\">" . $objAdmin->formField[$adminValue['name']] . "</td><td class=\"errorTableRows\" style=\"width:50%\">".$errorMsg."</td></tr>";
                }else{
                    if($adminValue["name"] == "password") {
                        $adminValue["value"] = "********";
                    }
                    $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";width:50%\">" . $objAdmin->formField[$adminValue['name']] . "</td><td class=\"errorTableRows\" style=\"width:50%\">".$adminValue['value']."</td></tr>";
                }
            }
        }
    }
    if( ($password != "" || $cfPassword != "") && ($password != $cfPassword) ) {
        $error = 1;
        $bg = INVALID;
        $errorMsg = "Password and Confirm Password must be similar.";
        
        $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:".$bg.";width:50%\"> Password Error</td><td class=\"errorTableRows\" style=\"width:50%\">".$errorMsg."</td></tr>";
    }
    
    if( !$changed ){
        $error = 1;
        $bg = INVALID;
       // $data .= "<tr style=\"background:" . $bg . ";border:1px solid #fff;color:#fff;vertical-align:middle;\"><td style='width:50%;border-right:1px solid #fff;padding:10px;'>Enterprise Changes</td><td style='width:50%;'>No changes.</td></tr>";
         $data .= "<tr style=\"border:1px solid #fff;color:#fff;vertical-align:middle;\"><td class='errorTableRows' style='background:#72ac5d;'>Enterprise Changes</td><td style='width:50%;' class='errorTableRows'>No changes.</td></tr>";
    }
    $data .= "</table>";
    
    echo $error . $data;;
    die;
    
}

else if( isset($_POST['action']) && $_POST['action'] == "completeAddAdmin" ) 
{
    $addAdminFormData = $_POST['addAdminFormData'];
    $adminData = array();
    foreach( $addAdminFormData as $adminKey => $adminValue) {
        if( $adminValue['name'] == "initialPassword" ) {
            $adminData["password"] = $adminValue['value'];
        }
        $adminData[$adminValue['name']] = $adminValue['value'];
        
    }
    $_SESSION['addedAdmins'][] = $adminData;
    echo "<p style=''> " . "Administrator added in the list." . "</p>";
    die;
}

else if( isset($_POST['action']) && $_POST['action'] == "completeModifyAdmin" )
{
    
    $modifyAdminFormData = $_POST['modifyAdminFormData'];
    $adminData = array();
    foreach( $modifyAdminFormData as $adminKey => $adminValue) {
        if( $adminValue['name'] == "initialPassword" ) {
            $adminData["password"] = $adminValue['value'];
        }
        $adminData[$adminValue['name']] = $adminValue['value'];
        
    }
    $changedAdminData = array();
    //     print_r($changedAdminData); exit;
    foreach( $adminData as $key1 => $value1) {
        if($key1 == "password" && $value1 != "") {
            $changedAdminData["password"] = $value1;
        } else if($key1 == "administratorId") {
            $changedAdminData["administratorId"] = $value1;
        } else if($_SESSION['modifiedAdministratorOldData'][$adminData['administratorId']][$key1] <> $value1) {
            $changedAdminData[$key1] = $value1;
        }
    }
    
    $_SESSION['modifiedAdmins'][$changedAdminData['administratorId']] = $changedAdminData;
    //unset($_SESSION['modifiedAdministratorOldData']);
    echo "<p style=''> " . "Administrator modified in the list." . "</p>";
    die;
}

else if( isset($_POST['action']) && $_POST['action'] == "deletedAdministrator" ) {
    $deletedAdminData = $_POST['deletedAdminData'];
    $adminId = "";
    foreach( $deletedAdminData as $adminKey => $adminValue) {
        if( $adminValue['name'] == "administratorId" ) {
            $adminId = $adminValue['value'];
        }
    }
    $_SESSION['deleteAdmins'][] = $adminId;
    echo "<p style=''> " . "Administrator deleted in the list." . "</p>";
    die;
}

?>