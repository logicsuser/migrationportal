<?php 
session_start();
$dnsAvailable = $_POST['getEntDnListData'];


function getDnsAvaliableList($dnsAvailable){
    
    $str = "";
		foreach ($dnsAvailable as $v) {
		  $range = explode(' - ',$v); //ex-774
		  $range1 = str_replace("+1-", "", $range[0]);
                  if($range1){
                      $range2 = str_replace("+1-", "", $range[1]);
                  }else{
                      $range2 = "";
                  }
                  if($range1 != "" && $range2 == ""){
                  
                   $str .= "<option value=\"+1-".$range1."\">+1-" . $range1 . "</option>";   
                    //  html += '<option value="+1-'+range1+'">+1-' + range1 + '</option>';
                  }else if($range1 != "" && $range2 != ""){
                    if($range1 < $range2){                            
                        for($j=$range1; $j <= $range2; $j++){
                            $str .= "<option value=\"+1-".$j."\">+1-" . $j . "</option>"; 
                        }
                    }
                       
                }
		 
		}
                
                return $str;
}

$assignedNumbers = getDnsAvaliableList($dnsAvailable); 
?>

<script> 
 function reArrangeSelectSorting(sortingId) {
	$("#"+sortingId).html($("#" + sortingId + ' option').sort(function(a, b) {
	 	return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
    }));
}
    $("#addAssign, #addAssignAll").click(function(){ 
			var BtnId = $(this).attr('id');
			if(BtnId == "addAssignAll"){
				$('#availableRanges option').prop('selected', true);
			}
			addAssignNumber();
			var sortingId = "availableRanges";
			reArrangeSelectSorting(sortingId);
	});
        
        var addAssignNumber = function(){
		html = "";
		range1 = "";

		var availableRanges = $("#availableRanges").val();		
		if(availableRanges){
			for (var i = 0; i < availableRanges.length; i++) {
				
				range1 = parseInt(availableRanges[i].replace("+1-", "")); 
				if(range1 != ""){
					html += '<option value="+1-'+range1+'">+1-' + range1 + '</option>';
				}
				$("#availableRanges option[value='"+availableRanges[i]+"']").remove();			
			}
		}
		$("#assignedNumbers").append(html);
	};
        
        
    $("#removeAssign, #removeAssignAll").click(function(){
			var BtnId = $(this).attr('id');
			if(BtnId == "removeAssignAll"){
				$('#assignedNumbers option').prop('selected', true);
			}
			
			removeAssignNumber();
			var sortingId = "availableRanges";
			reArrangeSelectSorting(sortingId);
	}); 
        
     var removeAssignNumber = function(){
		html = "";
		range1 = "";
		//debugger ;
		var assignedNumbers = $("#assignedNumbers").val();	
		if(assignedNumbers){
			for (var i = 0; i < assignedNumbers.length; i++) {
				
				range1 = parseInt(assignedNumbers[i].replace("+1-", "")); 
				if(range1 != ""){
					html += '<option value="+1-'+range1+'">+1-' + range1 + '</option>';
				}
				$("#assignedNumbers option[value='"+assignedNumbers[i]+"']").remove();
				
			}
		}
		$("#availableRanges").append(html);
	};
    
    </script>
<!--dns dialog close -->
<div class="row">
	<div class="">
		<div class="form-group">
			<h2 class="adminUserText">MODIFY / DELETE DN</h2>
		</div>
	</div>
</div>
    
    
<div class="row form-group alignCenter dnsButton" id="" style="width: 830px !important;margin: 0 auto;">
	<div class="col-md-12" style="margin-left: 136px;">
		<div class="" style="float:left; padding-right:15px;">
			<div class="form-group alignCenter" style="width:235px;">
				<label class="labelTextGrey">UnAssigned Phone Number</label>
			</div>
			<div class="form-group alignCenter dnranges" style="width:235px;">
                            
				<select id="availableRanges" name="availableRanges" multiple="" style="height: 250px;width: 230px !important;height: 400px !important;"></select>
				<input type="hidden" name="" id="" value="">
			</div>
		</div>
		
		<div class="" style="float:left; padding-right:10px;">
										<div class="form-group alignCenter dnsButton">
										<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
											    <button type="button" id="addAssign" name="addAvailableNumbers" class="addBlueCircle">
                                                    <img src="/Express/images/icons/arrow_Add.png"></button>
                                                <br/> 
                                                <button type="button" id="removeAssign" name="removeAvailableNumbers" class="addRedCircle">
                                                    <img src="/Express/images/icons/arrow_remove.png">
                                                </button>
                                                    <br /><br /> 
                                                  <button type="button" id="addAssignAll" name="addAvailableNumbers" class="addBlueCircle">
                                                      <img src="/Express/images/icons/arrow_add_all.png">
                                                  </button>
                                                      <br/>
                                                <button type="button" id="removeAssignAll" name="removeAvailableNumbers" class="addRedCircle" style="margin-left:-4px;">
                                                    <img src="/Express/images/icons/arrow_remove_all.png">
                                                </button>
										</div>
									</div>
		
		<div class="" style="float:left; ">
			<div class="form-group alignCenter">
				<label class="labelTextGrey" style="text-align: center;">Assigned Phone Number</label>
			</div>
			<div class="form-group alignCenter dnranges" style="width:235px;">
                            <select id="assignedNumbers" name="assignedNumbers" multiple style="height: 220px; padding:4px !important;width:230px !important;height: 400px !important;">
                                 <?php  echo $assignedNumbers ;?>
                            </select><input type="hidden" name="assignedNumberss"
												id="assignedNumberss" value="" />
			</div>
		</div>
	</div>
    
    <input type="hidden" value="" id="hiddenDnRange">
</div>


<!--end dialog code-->
 