<?php 

require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
$dns = new Dns();
$responses = "";

if( isset($_POST['action']) && $_POST['action'] == "getEntDnList" ) {
    
    if(isset($_SESSION['modifyDNsRangeArr']) && !empty($_SESSION['modifyDNsRangeArr'])){
        $_SESSION['modifyDNsRangeArr'] = array();
        $_SESSION['oldAssignDNsRange'] = array(); 
        unset($_SESSION['modifyDNsRangeArr']);
        unset($_SESSION['oldAssignDNsRange']);
        
    }
    
    $dnList = $dns->getDNServiceProviderRequest($_POST['selectedSp']);
    if( empty($dnList['Error']) ) {
        $dnOptions = createEntDnOptions($dnList['Success']);
        $responses = array("status" => "success", "dnOption" => $dnOptions);
    } else {
        $responses = array("status" => "failure", "errorMsg" => $dnList['Error']);
    }
    
    echo json_encode($responses);
    die;
    
} else if( isset($_POST['action']) && $_POST['action'] == "addDnsToEntTempList" ) {
    $responsesError = "";
    $responses = "";
    $dnListArray = array();
    $dnOptions = "";
    
    $dnList = $dns->getDNServiceProviderRequest($_POST['selectedSp']);
    if( empty($dnList['Error']) ) {
        $dnListArray = $dnList['Success'];
    }
    
    /* List All Ranges to be assigned to Service Provider Temp List. */
    $dnRanges = listPostedRanges($_POST);
    
    /* Filter Range to be assigned to Service Provider Temp List. */
    $dnRangeFilteredRes = filterDnRanges($dnRanges, $dnListArray);
    $responsesError .= $dnRangeFilteredRes['responsesError'];
    $responses .= $dnRangeFilteredRes['responses'];
    
    /* Filter DNs to be assigned to Service Provider Temp List. */
    $dnNumbersFilteredRes = filterDnNumbers($_POST['phoneNumber'], $dnListArray);
    $responsesError .= $dnNumbersFilteredRes['responsesError'];
    $responses .= $dnNumbersFilteredRes['responses'];
    
    if(isset($_SESSION['enterpriseDns'])) {  
        $dnOptions .= createTempOptionsListRange($_SESSION['enterpriseDns']);
        $dnOptions .= createTempOptionsListDn($_SESSION['enterpriseDns']);
    }
    
    
    ///////////////////////////////
    if(!empty($_SESSION['modifyDNsRangeArr'])){
    foreach($_SESSION['modifyDNsRangeArr'] as $tmPKey01 => $tmpCompleteDNsRangeArr1){
            foreach($tmpCompleteDNsRangeArr1 as $ky => $arrVal){
                if(!empty($arrVal)){            
                    if(count($arrVal) > 1){                
                        $minPhoneNumber = $arrVal[0];
                        $maxPhoneNumber = end($arrVal);
                        $phoneNumber    = $minPhoneNumber . " - " . $maxPhoneNumber;
                        $groupId        = "";
                        $canDelete      = "Yes";
                        $disabled       = "";
                        $dnOptions .= "<tr data-canDelete='$canDelete' data-groupId='$groupId' data-value='$phoneNumber' $disabled class='DNsTableClass $disableClass'><td class=\" \" style=\"width:10% !important;max-width: 70px;\"><input class=\"$disableClass dnsServiceLimitCheckBox\" data-canDelete='$canDelete' $disabled  data-groupId='$groupId' data-value='$phoneNumber'type=\"checkbox\" name='$checkboxName' id='$checkboxId' value='$canDelete'><label class='$disableClass' for='$checkboxId' style='margin-left: 10px;'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label></td><td style=\"max-width: 212px !important;border-right:none !important\">$phoneNumber</td><td style=\"max-width: 100% !important;\">$groupId </td></tr>";
                    }else{
                        $phoneNumber = $arrVal[0];
                        $groupId     = "";
                        $canDelete   = "Yes";
                        $disabled    = "";
                        $dnOptions .= "<tr data-canDelete='$canDelete' data-groupId='$groupId' data-value='$phoneNumber' $disabled class='DNsTableClass $disableClass'><td class=\" \" style=\"width:10% !important;max-width: 70px;\"><input class=\"$disableClass dnsServiceLimitCheckBox\" data-canDelete='$canDelete' $disabled  data-groupId='$groupId' data-value='$phoneNumber'type=\"checkbox\" name='$checkboxName' id='$checkboxId' value='$canDelete'><label class='$disableClass' for='$checkboxId' style='margin-left: 10px;'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label></td><td style=\"max-width: 212px !important;border-right:none !important\">$phoneNumber</td><td style=\"max-width: 100% !important;\">$groupId </td></tr>";
                    }
                }
            }
        }
    }
    ///////////////////////////////
    $dnOptions .= createEntDnOptions($dnListArray);
        
   echo json_encode(array("status" => "success", "responses" => $responses.$responsesError, "dnOption" => $dnOptions));
   die;
}


else if( isset($_POST['action']) && $_POST['action'] == "modifyDnsToEntTempList" ) {
    $getAllRangeArr                 =   array();
    $getExplodeAllRangeArr          =   array();
    $assingListNumberRange          =   array();
    $assignArray                    =   array();
    $unassingListNumber             =   array();
    $assingListNumberRange[]        =   $_POST['assignNumberRange'];
    $unassignArray                  =   $_POST['unassignNumber'];
    $assignArray                    =   $_POST['assignNumber'];
    
    $modifyPhoneRange               =  $_POST['assignNumberRange'];
        
    $dnList = $dns->getDNServiceProviderRequest($_POST['selectedSp']);
    if( empty($dnList['Error']) ) {
        $dnListArray = $dnList['Success'];
    }
    
    $unassignArr = explode("+1-", $unassignArray);
    $unassignFinalArr = array();
    foreach($unassignArr as $tk => $tPVal){
        if(!empty($tPVal)){
            $unassignFinalArr[] = $tPVal;
        }
    }
    $unAssignTmpArr  = implode(", ", $unassignFinalArr);

    
    if(checkRemoveDNsExistsINAddDNsList($dnListArray, $_POST['assignNumberRange']))
    {
            $assignedAllNumberRang  = getDnsAvaliableListRange($assingListNumberRange);
            
            $tmpCompleteDNsRangeArr = array();
            $tmpDNsRangeArr         = array();

            foreach($assignedAllNumberRang as $DNsKey => $DNsVal)
            {
                if(in_array($DNsVal, $unassignFinalArr))
                {
                        $delElementArr = array_keys($unassignFinalArr, $DNsVal);
                        unset($delElementArr);
                        $tmpCompleteDNsRangeArr[] = $tmpDNsRangeArr;
                        $tmpDNsRangeArr = array();
                }
                else
                {
                    $tmpDNsRangeArr[] = $DNsVal;
                }         
            }

            if(!empty($tmpDNsRangeArr)){
                $tmpCompleteDNsRangeArr[] = $tmpDNsRangeArr;
            }

            $dnOptions = "";

            if(isset($_SESSION['modifyDNsRangeArr']) && !empty($_SESSION['modifyDNsRangeArr']))
            {
                $recordExists = count($_SESSION['modifyDNsRangeArr']);                
                $_SESSION['modifyDNsRangeArr'][$recordExists]  = $tmpCompleteDNsRangeArr; 
            }
            if(empty($_SESSION['modifyDNsRangeArr']))
            {
                $_SESSION['modifyDNsRangeArr'][0] = $tmpCompleteDNsRangeArr;
            }
            
            
            
            if(isset($_SESSION['oldAssignDNsRange']) && !empty($_SESSION['oldAssignDNsRange']))
            {
                $noOfRecords  = count($_SESSION['oldAssignDNsRange']);
                $_SESSION['oldAssignDNsRange'][$noOfRecords]   = $_POST['assignNumberRange'];
            }
            if(empty($_SESSION['oldAssignDNsRange']))
            {
                $_SESSION['oldAssignDNsRange'][0] = $_POST['assignNumberRange'];
            }


            foreach($_SESSION['modifyDNsRangeArr'] as $tmPKey01 => $tmpCompleteDNsRangeArr1){
                foreach($tmpCompleteDNsRangeArr1 as $ky => $arrVal){
                    if(!empty($arrVal)){            
                        if(count($arrVal) > 1){                
                            $minPhoneNumber = $arrVal[0];
                            $maxPhoneNumber = end($arrVal);
                            $phoneNumber    = $minPhoneNumber . " - " . $maxPhoneNumber;
                            $groupId        = "";
                            $canDelete      = "Yes";
                            $disabled       = "";
                            $dnOptions .= "<tr data-canDelete='$canDelete' data-groupId='$groupId' data-value='$phoneNumber' $disabled class='DNsTableClass $disableClass'><td class=\" \" style=\"width:10% !important;max-width: 70px;\"><input class=\"$disableClass dnsServiceLimitCheckBox\" data-canDelete='$canDelete' $disabled  data-groupId='$groupId' data-value='$phoneNumber'type=\"checkbox\" name='$checkboxName' id='$checkboxId' value='$canDelete'><label class='$disableClass' for='$checkboxId' style='margin-left: 10px;'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label></td><td style=\"max-width: 212px !important;border-right:none !important\">$phoneNumber</td><td style=\"max-width: 100% !important;\">$groupId </td></tr>";
                        }else{
                            $phoneNumber = $arrVal[0];
                            $groupId     = "";
                            $canDelete   = "Yes";
                            $disabled    = "";
                            $dnOptions .= "<tr data-canDelete='$canDelete' data-groupId='$groupId' data-value='$phoneNumber' $disabled class='DNsTableClass $disableClass'><td class=\" \" style=\"width:10% !important;max-width: 70px;\"><input class=\"$disableClass dnsServiceLimitCheckBox\" data-canDelete='$canDelete' $disabled  data-groupId='$groupId' data-value='$phoneNumber'type=\"checkbox\" name='$checkboxName' id='$checkboxId' value='$canDelete'><label class='$disableClass' for='$checkboxId' style='margin-left: 10px;'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label></td><td style=\"max-width: 212px !important;border-right:none !important\">$phoneNumber</td><td style=\"max-width: 100% !important;\">$groupId </td></tr>";
                        }
                    }
                }
            }

            $responsesErrorModify = "<p style='text-align:center;'>phone number $unAssignTmpArr will be unassigned to enterprise phone number list. </p>";

           if(isset($_SESSION['enterpriseDns'])) {  
                $dnOptions .= createTempOptionsListRange($_SESSION['enterpriseDns']);
                $dnOptions .= createTempOptionsListDn($_SESSION['enterpriseDns']);
            } 
            
            
           $dnOptions .= modifyEntDnOptions($dnListArray, $_SESSION['oldAssignDNsRange']);    
           echo json_encode(array("status" => "success", "responses" => $responsesErrorModify, "dnOption" => $dnOptions, 'oldAssignDNsRangeMod'=>$_SESSION['oldAssignDNsRange'])); // 
    }
   else
   {
        $responses = "";
        $assignedRange = $_POST['assignNumberRange'];
        $responsesErrorDelDN .= "<p style='color:#ac5f5d; text-align:center;'>". $modifyPhoneRange . " phone number can't be removed because this phone number is not assigned.</p>";
        echo json_encode(array("status" => "error", "responsesError" => $responsesErrorDelDN, "responses" => $responses));
   }
   die;
}




else if( isset($_POST['action']) && $_POST['action'] == "removeDnsToEntTempList" ) {
    
    $assingListNumberRange[]        =   $_POST['assignNumberRange'];    
        
    $dnList = $dns->getDNServiceProviderRequest($_POST['selectedSp']);
    if( empty($dnList['Error']) ) {
        $dnListArray = $dnList['Success'];
    }
    
    
    if(checkRemoveDNsExistsINAddDNsList($dnListArray, $_POST['assignNumberRange']))
    {
        $assignedAllNumberRang  = getDnsAvaliableListRange($assingListNumberRange);    
        if(isset($_SESSION['oldAssignDNsRange']) && !empty($_SESSION['oldAssignDNsRange']))
        {
            $recordExists = count($_SESSION['oldAssignDNsRange']);    
            $_SESSION['oldAssignDNsRange'][$recordExists]  = $_POST['assignNumberRange'];
        }
        if(empty($_SESSION['oldAssignDNsRange']))
        {        
            $_SESSION['oldAssignDNsRange'][0] = $_POST['oldAssignDNsRange'];
        }

       //$dnOptions = "";
       if(isset($_SESSION['enterpriseDns'])) {  
            $dnOptions .= createTempOptionsListRange($_SESSION['enterpriseDns']);
            $dnOptions .= createTempOptionsListDn($_SESSION['enterpriseDns']);
       } 
       
       
       if(!empty($_SESSION['modifyDNsRangeArr'])){
        foreach($_SESSION['modifyDNsRangeArr'] as $tmPKey01 => $tmpCompleteDNsRangeArr1){
                foreach($tmpCompleteDNsRangeArr1 as $ky => $arrVal){
                    if(!empty($arrVal)){            
                        if(count($arrVal) > 1){                
                            $minPhoneNumber = $arrVal[0];
                            $maxPhoneNumber = end($arrVal);
                            $phoneNumber    = $minPhoneNumber . " - " . $maxPhoneNumber;
                            $groupId        = "";
                            $canDelete      = "Yes";
                            $disabled       = "";
                            $dnOptions .= "<tr data-canDelete='$canDelete' data-groupId='$groupId' data-value='$phoneNumber' $disabled class='DNsTableClass $disableClass'><td class=\" \" style=\"width:10% !important;max-width: 70px;\"><input class=\"$disableClass dnsServiceLimitCheckBox\" data-canDelete='$canDelete' $disabled  data-groupId='$groupId' data-value='$phoneNumber'type=\"checkbox\" name='$checkboxName' id='$checkboxId' value='$canDelete'><label class='$disableClass' for='$checkboxId' style='margin-left: 10px;'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label></td><td style=\"max-width: 212px !important;border-right:none !important\">$phoneNumber</td><td style=\"max-width: 100% !important;\">$groupId </td></tr>";
                        }else{
                            $phoneNumber = $arrVal[0];
                            $groupId     = "";
                            $canDelete   = "Yes";
                            $disabled    = "";
                            $dnOptions .= "<tr data-canDelete='$canDelete' data-groupId='$groupId' data-value='$phoneNumber' $disabled class='DNsTableClass $disableClass'><td class=\" \" style=\"width:10% !important;max-width: 70px;\"><input class=\"$disableClass dnsServiceLimitCheckBox\" data-canDelete='$canDelete' $disabled  data-groupId='$groupId' data-value='$phoneNumber'type=\"checkbox\" name='$checkboxName' id='$checkboxId' value='$canDelete'><label class='$disableClass' for='$checkboxId' style='margin-left: 10px;'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label></td><td style=\"max-width: 212px !important;border-right:none !important\">$phoneNumber</td><td style=\"max-width: 100% !important;\">$groupId </td></tr>";
                        }
                    }
                }
            }
        }
       
       
       $deletedDnList = $_POST['assignNumberRange'];
       $responsesErrorModify = "<p style='text-align:center;'>$deletedDnList phone number range will be unassigned to enterprise phone number list. </p>";
       $dnOptions .= modifyEntDnOptions($dnListArray, $_SESSION['oldAssignDNsRange']);    
       echo json_encode(array("status" => "success", "responses" => $responsesErrorModify, "dnOption" => $dnOptions, 'oldAssignDNsRangeDel' => $_SESSION['oldAssignDNsRange']));
   }
   else{
        $responses = "";
        $assignedRange = $_POST['assignNumberRange'];
        $responsesErrorDelDN .= "<p style='color:#ac5f5d; text-align:center;'>". $assignedRange . " phone number can't be removed because this phone number is not assigned.</p>";
        echo json_encode(array("status" => "error", "responsesError" => $responsesErrorDelDN, "responses" => $responses));
   }  
   die;
}

    function getDnsAvaliableListRange($assingListNumberRange){
            $str = "";
            foreach ($assingListNumberRange as $v) {
                $range = explode(' - ',$v);
                $range1 = str_replace("+1-", "", $range[0]);
                if($range1){
                    $range2 = str_replace("+1-", "", $range[1]);
                }else{
                    $range2 = "";
                }
                if($range1 != "" && $range2 == ""){
                   $getAllRangeArr[] =$range1;
                    $x++;
                }else if($range1 != "" && $range2 != ""){
                    if($range1 < $range2){  
                        $x =0;
                        for($j=$range1; $j <= $range2; $j++){
                          $getAllRangeArr[] =$j;
                          $x++;
                        }
                    }
                       
                }
	}
        $getExplodeAllRangeArr = $getAllRangeArr;
        return $getExplodeAllRangeArr;
    }
 





/*=============================================================================*/

function createEntDnOptions($dnList) {
    $dnOptionString = "";
    foreach ($dnList as $dnKey => $dnValue) {
        if(!in_array($dnValue['phoneNumber'], $_SESSION['oldAssignDNsRange']))
        {
            $phoneNumber = isset($dnValue['phoneNumber']) ? $dnValue['phoneNumber'] : "";
            $checkboxName = "dnsCheckAuth[" . $dnValue['phoneNumber'] . "]";
            $checkboxId = $dnValue['phoneNumber'];
            $groupId = isset($dnValue['groupId']) ? $dnValue['groupId'] : "";
            $canDelete = isset($dnValue['canDelete']) ? $dnValue['canDelete'] : $dnValue['canDelete'];
            $disabled = $canDelete == "false" ? "disabled" : "";
            $disableClass = $disabled == "disabled" ? "disableService" : "";
            //$dnOptionString .= "<option data-canDelete='$canDelete' data-groupId='$groupId' value='$phoneNumber' $disabled>$phoneNumber</option>";
            $dnOptionString .= "<tr data-canDelete='$canDelete' data-groupId='$groupId' data-value='$phoneNumber' $disabled class='DNsTableClass $disableClass'><td class=\" \" style=\"width:10% !important;max-width: 70px;\"><input class=\"$disableClass dnsServiceLimitCheckBox\" data-canDelete='$canDelete' $disabled  data-groupId='$groupId' data-value='$phoneNumber'type=\"checkbox\" name='$checkboxName' id='$checkboxId' value='$canDelete'><label class='$disableClass' for='$checkboxId' style='margin-left: 10px;'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label></td><td style=\"max-width: 212px !important;border-right:none !important\">$phoneNumber</td><td style=\"max-width: 100% !important;\">$groupId </td></tr>";

        }
        
    }
    
    return $dnOptionString;
}


function modifyEntDnOptions($dnList, $deletedDNArr) 
{
    $dnOptionString = "";
    foreach ($dnList as $dnKey => $dnValue) 
    {       
       if(!in_array($dnValue['phoneNumber'], $deletedDNArr))
       {
            $phoneNumber = isset($dnValue['phoneNumber']) ? $dnValue['phoneNumber'] : "";
            $checkboxName = "dnsCheckAuth[" . $dnValue['phoneNumber'] . "]";
            $checkboxId = $dnValue['phoneNumber'];
            $groupId = isset($dnValue['groupId']) ? $dnValue['groupId'] : "";
            $canDelete = isset($dnValue['canDelete']) ? $dnValue['canDelete'] : $dnValue['canDelete'];
            $disabled = $canDelete == "false" ? "disabled" : "";
            $disableClass = $disabled == "disabled" ? "disableService" : "";
            $dnOptionString .= "<tr data-canDelete='$canDelete' data-groupId='$groupId' data-value='$phoneNumber' $disabled class='DNsTableClass $disableClass'><td class=\" \" style=\"width:10% !important;max-width: 70px;\"><input class=\"$disableClass dnsServiceLimitCheckBox\" data-canDelete='$canDelete' $disabled  data-groupId='$groupId' data-value='$phoneNumber'type=\"checkbox\" name='$checkboxName' id='$checkboxId' value='$canDelete'><label class='$disableClass' for='$checkboxId' style='margin-left: 10px;'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label></td><td style=\"max-width: 212px !important;border-right:none !important\">$phoneNumber</td><td style=\"max-width: 100% !important;\">$groupId </td></tr>";
       }
   }    
   return $dnOptionString;
}






function createTempOptionsListRange($TempDnList) {
    $dnOptionString = "";
    foreach ($TempDnList['newlyAddedDnsRanges'] as $dnRKey => $dnRValue) {
        $rangeFirstEl = $dnRValue['minPhoneNumber'];
        $rangeSecondEl = $dnRValue['maxPhoneNumber'];
        $phoneNumber = $rangeFirstEl . " - " . $rangeSecondEl;
        $groupId = "";
        $canDelete = "Yes";
        $disabled == "";        
        //$dnOptionString .= "<option data-canDelete='$canDelete' data-groupId='$groupId' value='$phoneNumber' $disabled>$phoneNumber</option>";
        $dnOptionString .= "<tr data-canDelete='$canDelete' data-groupId='$groupId' data-value='$phoneNumber' $disabled class='DNsTableClass $disableClass'><td class=\" \" style=\"width:10% !important;max-width: 70px;\"><input class=\"$disableClass dnsServiceLimitCheckBox\" data-canDelete='$canDelete' $disabled  data-groupId='$groupId' data-value='$phoneNumber'type=\"checkbox\" name='$checkboxName' id='$checkboxId' value='$canDelete'><label class='$disableClass' for='$checkboxId' style='margin-left: 10px;'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label></td><td style=\"max-width: 212px !important;border-right:none !important\">$phoneNumber</td><td style=\"max-width: 100% !important;\">$groupId </td></tr>";
        
    }
    return $dnOptionString;
}

function createTempOptionsListDn($TempDnList) {
    $dnOptionString = "";
    
    foreach ($TempDnList['newlyAddedDns'] as $dnRKey => $dnRValue) {
        $phoneNumber = $dnRValue;
        $groupId = "";
        $canDelete = "Yes";
        $disabled == "";
        
        //$dnOptionString .= "<option data-canDelete='$canDelete' data-groupId='$groupId' value='$phoneNumber' $disabled>$phoneNumber</option>";
        $dnOptionString .= "<tr data-canDelete='$canDelete' data-groupId='$groupId' data-value='$phoneNumber' $disabled class='DNsTableClass $disableClass'><td class=\" \" style=\"width:10% !important;max-width: 70px;\"><input class=\"$disableClass dnsServiceLimitCheckBox\" data-canDelete='$canDelete' $disabled  data-groupId='$groupId' data-value='$phoneNumber'type=\"checkbox\" name='$checkboxName' id='$checkboxId' value='$canDelete'><label class='$disableClass' for='$checkboxId' style='margin-left: 10px;'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label></td><td style=\"max-width: 212px !important;border-right:none !important\">$phoneNumber</td><td style=\"max-width: 100% !important;\">$groupId </td></tr>";
    }
    
    return $dnOptionString;
}

function listPostedRanges($POST) {
    $dnRanges= array();
    
    foreach($POST['minPhoneNumber'] as $minKey => $minValue) {
        $dnRanges[$minKey]['minPhoneNumber'] = $minValue;
    }
    foreach($POST['maxPhoneNumber'] as $maxKey => $maxValue) {
        $dnRanges[$maxKey]['maxPhoneNumber'] = $maxValue;
    }
    
    return $dnRanges;
}

function filterDnRanges($dnRanges, $dnListArray) {
    $responsesErrorDN = "";
    $responses = "";
    foreach($dnRanges as $dnKey1 => $dnValue1) {
        $assignedRange = $dnValue1['minPhoneNumber'] . " - ". $dnValue1['maxPhoneNumber'];
        
        $minRes = validateNumber( trim($dnValue1['minPhoneNumber']) );
        $maxRes = validateNumber( trim($dnValue1['maxPhoneNumber']) );
        
        $rangeIsAssigned = false;
        if( $minRes["status"] == "Invalid" || $maxRes["status"] == "Invalid" ) {
            $responsesErrorDN .= "<p style='color:red;text-align:center;'>". $assignedRange . " Invalide phone number range number.</p>";
        } else {
                $dnValue1['minPhoneNumber'] = $minRes["number"];
                $dnValue1['maxPhoneNumber'] = $maxRes["number"];
            
                if(isset($_SESSION['enterpriseDns']['newlyAddedDnsRanges'])) {
                    foreach($_SESSION['enterpriseDns']['newlyAddedDnsRanges'] as $dnRKey2 => $dnRValue2) {
                        $validateRes = validateNumber( $dnRValue2 );
                        $dnRValue2 = $validateRes["number"];
                        if( $dnRValue2 >= $dnValue1['minPhoneNumber'] && $dnRValue2 <= $dnValue1['maxPhoneNumber'] ) {
                            $responsesErrorDN .= "<p style='color:red;text-align:center;'>". $assignedRange . " phone number Range is already assinged to Enterprise range List.</p>";
                            $rangeIsAssigned = true;
                            break;
                        }
                    }
                    
                }
                
                if( !$rangeIsAssigned ) {
                    foreach($dnListArray as $dnKey => $dnValue) {
                        if( strpos($dnValue['phoneNumber'], " - ") !== false) {
                            /* This Range. */
                            $numRange = explode(" - ", $dnValue['phoneNumber']);
                            
                            $validMin = validateNumber($numRange[0]);
                            $validMax = validateNumber($numRange[1]);
                            $rangeFirst = $validMin["number"];
                            $rangeSecond = $validMax["number"];
                            
                            if( $dnValue1["minPhoneNumber"] >= $rangeFirst && $dnValue1["maxPhoneNumber"] <= $rangeSecond ) {
                                $responsesErrorDN .= "<p style='color:red;text-align:center;'>". $assignedRange . " phone number is already assinged to Enterprise List.</p>";
                                $rangeIsAssigned = true;
                                break;
                            }                         
                        } else {
                             if($dnValue1 >= $dnValue1["minPhoneNumber"] && $dnValue1 <= $dnValue1["maxPhoneNumber"]) {
                                $responsesErrorDN .= "<p style='color:red;text-align:center;'>".$assignedRange . " phone number is already assinged to Enterprise List.</p>";
                                $rangeIsAssigned = true;
                                break;
                            }
                        }
                    }
                }
                
                if(!$rangeIsAssigned) {
                    if( $dnValue1['minPhoneNumber'] > $dnValue1['maxPhoneNumber']) {
                        $responsesErrorDN .= "<p style='color:red'>Invalid phone number range.  The starting phone number cannot be larger than the ending phone number.</p>";
                        $rangeIsAssigned = true;
                    }
                    else {
                        $_SESSION['enterpriseDns']['newlyAddedDnsRanges'][] = $dnValue1;
                        $responses .= "<p style='text-align:center;'>" . $assignedRange . " phone number range is assigned to enterprise phone number list. </p>";
                    }
                }
                
        }
        
        
    }
    
    return array("responsesError" => $responsesErrorDN, "responses" => $responses);
}

function filterDnNumbers($phoneNumber, $dnListArray) {
    $responsesErrorDn = "";
    $responses = "";
    foreach($phoneNumber as $numKey => $numValue) {
        $numberVal = trim($numValue);
        $numberIsAssigned = false;
        
        $validRes = validateNumber( $numValue );
        
        if( $validRes["status"] == "Invalid") {
            $responsesErrorDn .= "<p style='color:red;text-align:center;'>".$numberVal . " Invalide phone number.</p>";
        } else {
                    $numValue = $validRes["number"];
                    //print_r($_SESSION['enterpriseDns']['newlyAddedDns']); 
                    if( isset($_SESSION['enterpriseDns']['newlyAddedDns']) && in_array($numValue, $_SESSION['enterpriseDns']['newlyAddedDns'])) {
                        $responsesErrorDn .= "<p style='color:red;text-align:center;'>".$numberVal . " phone number is already assinged to Enterprise Dn List.</p>";
                        $numberIsAssigned = true;
//                      break;
                    }
                    
                    if( !$numberIsAssigned ) {
                        foreach($dnListArray as $dnKey => $dnValue) {
                            
                            if( strpos($dnValue['phoneNumber'], " - ") !== false) {
                                $numRange = explode(" - ", $dnValue['phoneNumber']);
                                $rangeFirst = explode("-", $numRange[0])[1];
                                $rangeSecond = explode("-", $numRange[1])[1];
                                if($numValue >= $rangeFirst && $numValue <= $rangeSecond) {
                                    //echo"hhhhhhhhhh";
                                    $numberIsAssigned = true;
                                    break;
                                }
                            } else {
                                $validateAssingedRes = validateNumber($dnValue['phoneNumber']);
                                $assignedNumber = $validateAssingedRes["number"];
                                if( $assignedNumber == $numValue) {
                                    $numberIsAssigned = true;
                                    break;
                                }
                            }
                        }
                    }
                    
                    if( !$numberIsAssigned ) {
                        $_SESSION['enterpriseDns']['newlyAddedDns'][] = $numValue;
                        $responses .= "<p style='text-align:center;'> ". $numberVal . " phone number is assigned to Enterprise Dn List </p>";
                    } else {
                        $responsesErrorDn .= "<p style='color:red;text-align:center;'>".$numberVal . " phone number is not assinged to Enterprise Dn List.</p>";
                    }
        }
        
    }
    
    return array("responsesError" => $responsesErrorDn, "responses" => $responses);
}

function validateNumber($number) {
    $response = "";
    $number = substr_count($number, "-") == 1 ? explode("-", $number) :  $number;
    if( is_array($number) ) {
        $country_code = substr($number[0], 1);
        $number = $number[1];
    } else {
        $country_code = "";
    }
    
    if( ! ctype_digit($number) || ($country_code != "" && !ctype_digit($country_code)) ) {
        $response = "Invalid";
    } else {
        $response = "Valid";
    }
    
    return array("status" => $response, "number" => $number);
}

function checkRemoveDNsExistsINAddDNsList($dnList, $deletedDNArr){
    $dnOptionString = "";
    $flag = false;
    foreach ($dnList as $dnKey => $dnValue) 
    {       
       if($dnValue['phoneNumber'] == $deletedDNArr)
       {
           $flag = true;
       }
   }  
   return $flag;
}
?>