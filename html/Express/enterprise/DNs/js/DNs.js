var dnLevel = "";
var unassignNumber = "";

$("#dnTable").tablesorter();
$(".eventTableShorter").trigger("click");
var sourceSwapImage = function() {
            var thisId = this.id;
            var image = $("#"+thisId).children('img');
            var newSource = image.data('alt-src');
            image.data('alt-src', image.attr('src'));
            image.attr('src', newSource);
        }
        
		$("#downloadDnsCSV").hover(sourceSwapImage, sourceSwapImage);
              
$(function(){
   $("#loadingDns").show();
$(".inVisibleSelectAll").addClass("dnsShowSelectAll");
 
	$("#showAddDnEnterprise").hide();

	$("#enterpriseAddDns").click(function(){
		$("#errorDnsGroup").html('');
		$("#showAddDnEnterprise").slideDown();
	//	$("#enterpriseAddDns").hide();
		$("#closeRemoveDnsButton").trigger("click");
	});
	
	$("#closeEnterpriseDNsBtn").click(function(){
		$("#showAddDnEnterprise").slideUp();
		//$("#enterpriseAddDns").show();
		$("input[name=minPhoneNumber]").val('');
		$("input[name=maxPhoneNumber]").val('');
		$("input[name=phoneNumber]").val('');
		$(".extraAddRow").remove();
	});
	
	
	
$("#showRemoveDnEnterprise").hide();
	
/*	$("#enterpriseRemoveDns").click(function(){
		$("#errorDnsRemoveGroup").html('');
		$("#showRemoveDnEnterprise").slideDown();
		$("#enterpriseRemoveDns").hide();
		$("#closeEnterpriseDNsBtn").trigger("click");
	});
*/
	
	$("#closeRemoveDnsButton").click(function(){
		$("#showRemoveDnEnterprise").slideUp();
		$("#enterpriseRemoveDns").show();
		$("input[name=minPhoneNumberRemove]").val('');
		$("input[name=maxPhoneNumberRemove]").val('');
		$("input[name=removePhoneNumber]").val('');
		$(".extraRemoveRow").remove();
	});
	
	
	$("#addNumberRowEnterprise").click(function(){
		var el = $(this);
		html = '<div class="form-group extraAddRow"><div class="col-md-10 extraAddRow extraDivRow" style="padding-left:0px;"><input class="extraInputRow" type="text" name="phoneNumber" value="" style="width:400px !important;" /></div><div class="col-md-1" style="width:13% !important"><img class="" id="deleteNumberRow" src="images/minus.png" style="width:18px; float:right; margin-top:21px"></div></div>'; 
		el.parent('div').parent('div').parent('div').append(html);
	});
	
	
	$("#addNumberRangeRowRemoveEnt").click(function(){
		var el = $(this);
		html = '<div class="extraRemoveRow form-group"><div class="col-md-5" style="padding-left:0px;"><input class="extraInputRow" type="text" name="minPhoneNumberRemove" value="" style="width:125px !important"/></div><div class="col-md-1" style="padding:0 !important; width:20px; margin:0"><br/><span style="width:20px; margin-top:17px;">-</span></div><div class="col-md-5" style="padding-left:0px;"><input class="extraInputRow" type="text" name="maxPhoneNumberRemove" value="" style="width:125px !important;"/></div>';
		html +=	'<div class="col-md-1"><img class="" id="deleteNumberRangeRowRemoveEnt" src="images/minus.png" style="width:18px; margin-top:17px;"></div>'; 
		el.parent('div').parent('div').parent('div').append(html);
	});
	
	$("#addNumberRowRemoveEnterprise").click(function(){
		var el = $(this);
		html = '<div class="extraRemoveRow form-group"><div class="col-md-10" style="padding-left:0"><input class="extraInputRow" type="text" name="removePhoneNumber" value="" style="width:400px !important; float:left" /></div><div class="col-md-1" style="width:13% !important"><br/><img class="" id="deleteNumberRowRemove" src="images/minus.png" style="width:18px; float:right;"></div></div>'; 
		el.parent('div').parent("div").parent('div').append(html);
	});
	
	$("#addNumberRangeRow").click(function(){
		var el = $(this);
		html = '<div class="extraRemoveRow form-group"><div class="col-md-5" style="padding-left:0px;"><input class="extraInputRow" type="text" name="minPhoneNumber" value="" style="width:145px !important"/></div><div class="col-md-1" style="padding:0 !important; width:20px; margin:0"><br/><span style="width:20px; margin-top:17px;color:#555 !important;">-</span></div><div class="col-md-5" style="padding-left:0px;"><input class="extraInputRow" type="text" name="maxPhoneNumber" value="" style="width:145px !important;"/></div>';
		html +=	'<div class="col-md-1"><img class="" id="deleteNumberRangeRowRemove" src="images/minus.png" style="width:18px; margin-top:17px;"></div>'; 
		el.parent('div').parent('div').parent('div').append(html);
	});
	
	$(document).on("click", "#deleteNumberRangeRowRemove", function(){
		var el = $(this);
		el.parent('div').parent('div').remove();
	});
        
        $(document).on("click", "#deleteNumberRow", function(){
		var el = $(this);
		el.parent('div').parent('div').remove();
	});
	
	$(document).on("click", "#deleteNumberRangeRowRemoveEnt", function(){
		var el = $(this);
		el.parent('div').parent('div').remove();
	});
	
	
	var getEnterpriseDnList = function() {
		var selectedSp = $("#hiddenEnterpriseName").val();
		$.ajax({
	           type: "POST",
	           url: "enterprise/DNs/dnsOperations.php",
	           data: { action: 'getEntDnList', selectedSp: selectedSp},
	           success: function(result)
	           {                       
                       $("#loadingDns").hide();
	        	   result = JSON.parse(result);
	        	   if(result.status == "success") 
                           {                              
                                if(result.dnOption.length >0) 
                                 { 
                                     $("#dnsTbody").html(result.dnOption);                                     
                                     $(".inVisibleSelectAll").removeClass("dnsShowSelectAll");
                                 }
                            }if(result.dnOption =="" || result.dnOption.indexOf("tr")=='0')
                            {
                                $(".inVisibleSelectAll").addClass("dnsShowSelectAll");
                                $("#dnsTbody").html("<tr class=''><td class=\" \" colspan='4'>No Record Found</td></tr>"); 
                            }
                            $("#dnTable").tablesorter();
                            $(".eventTableShorter").trigger("click"); 
	           }
                   
	       });
	}
	getEnterpriseDnList();
	
	var addDnsInEnterpriseTempList = function(entDnsData) {
		var selectedSp = $("#hiddenEnterpriseName").val();
		entDnsData['action'] = 'addDnsToEntTempList';
		entDnsData['selectedSp'] = selectedSp;
		
		$.ajax({
		type: "POST",
		url: "enterprise/DNs/dnsOperations.php",
		data: entDnsData,
		success: function(result) { 
				   result = JSON.parse(result);                                   
                                   //console.log(result.responses);
				   if(result.status == "success") {
					   $("#dnsOperationDialog").html(result.responses);	
					  // $("#assignedRanges").html(result.dnOption);
                                           //$("#dnsTbody").html(result.dnOption);
                                          
                                            if(result.dnOption.length >0) { 
                                                $("#dnsTbody").html(result.dnOption);
                                                $(".inVisibleSelectAll").removeClass("dnsShowSelectAll");
                                                $(".ui-dialog-buttonpane button:contains('Continue')").button().show().addClass('subButton');
						$("input[name=phoneNumber]").val('');
						$("input[name=minPhoneNumber]").val('');
						$("input[name=maxPhoneNumber]").val('');
                                            }if(result.dnOption =="" || result.dnOption.indexOf("tr")=='0'){
                                                $("#dnsTbody").html("<tr class=''><td class=\" \" colspan='4'>No Record Found</td></tr>"); 
                                            }  	
				   } else {
					   alert(result.errorMsg);
				   }
                                    $("#dnTable tbody").find('tr#resultNodFound').remove();
                                 //  $("#noDNsFoundDiv").hide();
                                   $("#searchDns").val("");                                 
		}
                });

	}
	
	
var removeDnsInEnterpriseTempList = function(entDnsDataRemove) {
	var selectedSp = $("#hiddenEnterpriseName").val();
	entDnsDataRemove['action'] = 'removeDnsToEntTempList';
	entDnsDataRemove['selectedSp'] = selectedSp;
	
	$.ajax({
		type: "POST",
		url: "enterprise/DNs/dnsOperations.php",
		data: entDnsDataRemove,
		success: function(result) { 
                    //debugger ;
			result = JSON.parse(result);
			   if(result.status == "success") {
				   $("#dnsOperationDialog").html(result.responses);	
				   //$("#assignedRanges").html(result.dnOption);
                                   $("#dnsTbody").html(result.dnOption);
				   	$(".ui-dialog-buttonpane button:contains('Continue')").button().show().addClass('subButton');
					$("input[name=phoneNumber]").val('');
					$("input[name=minPhoneNumber]").val('');
					$("input[name=maxPhoneNumber]").val('');
			   } else {
				   alert(result.errorMsg);
			   }
		}
	});	
}
/*
$("#removeDnsButton").click(function(){ 
		var phoneNumberValues = [];	
		//var inputs = $("input[name=removePhoneNumber]");
                var inputs = $("input[name=availableRanges]");
		if(inputs.length > 0){
			for(var i = 0; i < inputs.length; i++){
				if($(inputs[i]).val() != ""){
					phoneNumberValues.push($(inputs[i]).val());
				}
			}
		}
		
		var phoneRangeNumberValues = [];
		phoneRangeNumberValues['min'] = [];	 
		phoneRangeNumberValues['max'] = [];	 
		var inputRmin = $("input[name=minPhoneNumberRemove]");	
		var inputRmax = $("input[name=maxPhoneNumberRemove]");	
		if(inputRmin.length > 0 && inputRmax.length > 0){
			for(var i = 0; i < inputRmin.length; i++){
				if($(inputRmin[i]).val() != "" && $(inputRmax[i]).val() != ""){
					phoneRangeNumberValues['min'].push($(inputRmin[i]).val());
					phoneRangeNumberValues['max'].push($(inputRmax[i]).val());
					$("#removePhoneRangeDiv").find('div').eq(i).find('input[name=minPhoneNumberRemove]').css('border', '1px solid #000');
					$("#removePhoneRangeDiv").find('div').eq(i).find('input[name=maxPhoneNumberRemove]').css('border', '1px solid #000');
				}else if($(inputRmin[i]).val() == "" && $(inputRmax[i]).val() != ""){
					$("#removePhoneRangeDiv").find('div').eq(i).find('input[name=minPhoneNumberRemove]').css('border', '1px solid red');
					$("#removePhoneRangeDiv").find('div').eq(i).find('input[name=maxPhoneNumberRemove]').css('border', '1px solid #000');
					return false;
				}else if($(inputRmin[i]).val() != "" && $(inputRmax[i]).val() == ""){
					$("#removePhoneRangeDiv").find('div').eq(i).find('input[name=maxPhoneNumberRemove]').css('border', '1px solid red');
					$("#removePhoneRangeDiv").find('div').eq(i).find('input[name=minPhoneNumberRemove]').css('border', '1px solid #000');
					return false;
				}
			}
		}
		//console.log(phoneNumberValues.length);
		//console.log(phoneRangeNumberValues);
		if(phoneNumberValues.length > 0 || phoneRangeNumberValues['min'].length > 0){
			$("#dnsOperationDialog").dialog("open");
			$("#dnsOperationDialog").dialog("option", "title", "DN's request");
			$("#dnsOperationDialog").html('Removing DN\'s to your Service Provider....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
			
			$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
			$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();

		}else{
			return false;
		}
		var entDnsDataRemove =  {phoneNumber : phoneNumberValues, minPhoneNumber : phoneRangeNumberValues['min'], maxPhoneNumber : phoneRangeNumberValues['max']};
		removeDnsInEnterpriseTempList(entDnsDataRemove);
		
	});  */


	$("#addEnterpriseDNsBtn").click(function(){ 
		var phoneNumberValues = [];	
		var inputs = $("input[name=phoneNumber]");	
		if(inputs.length > 0){
			for(var i = 0; i < inputs.length; i++){
				if($(inputs[i]).val() != ""){
					phoneNumberValues.push($(inputs[i]).val());
				}
			}
		}
		
		var phoneRangeNumberValues = [];
		phoneRangeNumberValues['min'] = [];	 
		phoneRangeNumberValues['max'] = [];
		var inputRmin = $("input[name=minPhoneNumber]");	
		var inputRmax = $("input[name=maxPhoneNumber]");	
		if(inputRmin.length > 0 && inputRmax.length > 0){
			for(var i = 0; i < inputRmin.length; i++){
				if($(inputRmin[i]).val() != "" && $(inputRmax[i]).val() != ""){
					phoneRangeNumberValues['min'].push($(inputRmin[i]).val());
					phoneRangeNumberValues['max'].push($(inputRmax[i]).val());
					$("#phoneRangeDiv").find('div').eq(i).find('input[name=minPhoneNumber]').css('border', '1px solid #000');
					$("#phoneRangeDiv").find('div').eq(i).find('input[name=maxPhoneNumber]').css('border', '1px solid #000');
				}else if($(inputRmin[i]).val() == "" && $(inputRmax[i]).val() != ""){
					$("#phoneRangeDiv").find('div').eq(i).find('input[name=minPhoneNumber]').css('border', '1px solid red');
					$("#phoneRangeDiv").find('div').eq(i).find('input[name=maxPhoneNumber]').css('border', '1px solid #000');
					return false;
				}else if($(inputRmin[i]).val() != "" && $(inputRmax[i]).val() == ""){
					$("#phoneRangeDiv").find('div').eq(i).find('input[name=maxPhoneNumber]').css('border', '1px solid red');
					$("#phoneRangeDiv").find('div').eq(i).find('input[name=minPhoneNumber]').css('border', '1px solid #000');
					return false;
				}
			}
		}

		if(phoneNumberValues.length > 0 || phoneRangeNumberValues['min'].length > 0){
			$("#dnsOperationDialog").dialog("open");
			$("#dnsOperationDialog").dialog("option", "title", "DN's request");
			$("#dnsOperationDialog").html('Adding DN\'s to your Service Provider....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
			
			$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
			$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
		}else{
			return false;
		}
		
		var entDnsData = {phoneNumber : phoneNumberValues, minPhoneNumber : phoneRangeNumberValues['min'], maxPhoneNumber : phoneRangeNumberValues['max']};	
		
		addDnsInEnterpriseTempList(entDnsData);
                
	});

	
	$("#dnsOperationDialog").dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		buttons: {
			"Complete": function() {
				 				
			},
			"Cancel": function() {
				 $(this).dialog("close");
			},
            "Done": function() {                
            	$(this).dialog("close");
            	$('html, body').animate({scrollTop: '0px'}, 300);
            },
            
            "Continue": function() {
            	$(this).dialog("close");
			},
			"Ok" : function(){
				$(this).dialog("close"); 
			},
		 
             
		},
        open: function() {
            $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019	
            setDialogDayNightMode($(this));
             
            $(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');
            $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
             
           
            $(".ui-dialog-buttonpane button:contains('More changes')").button().hide();
            $(".ui-dialog-buttonpane button:contains('Done')").button().hide();
            $(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
            $(".ui-dialog-buttonpane button:contains('Continue')").button().hide();
            $(".ui-dialog-buttonpane button:contains('Next')").button().hide();
            $(".ui-dialog-buttonpane button:contains('Ok')").button().hide();
            $(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide();
        }
});


$("#dnsOperationModDialog").dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		buttons: {
                "Delete DN": function() {
                    
                    var assignNumberRange = $("#hiddenSpecificRangeValue").val();
                    var selectedSp = $("#hiddenEnterpriseName").val();
                    console.log(unassignNumber);
                    var entDnsData = {selectedSp: selectedSp, assignNumberRange: assignNumberRange, action: 'removeDnsToEntTempList'};
                    $.ajax({
                            type: "POST",
                            url: "enterprise/DNs/dnsOperations.php",
                            data: entDnsData,
                            success: function(result) { 
				   result = JSON.parse(result);
                                   console.log(result.oldAssignDNsRangeDel);
				   if(result.status == "success") {
                                            //Start old Code
                                            var DNsListTmp  = "";                     
                                            var tmpVarDN    = "";          
                                            $("#availableRanges option").each(function(){
                                                  DNsListTmp += $(this).val()+ ";;";
                                            });                                            
                                            $("#assignedNumbers option").each(function(){
                                                  DNsListTmp += $(this).val()+ ";;";
                                            });                      
                                            unassignNumber += DNsListTmp;
                                            //End old code                 
					   $("#dnsOperationDialog").html(result.responses);
                                           $("#dnsOperationDialog").dialog("open");
                                            if(result.dnOption.length >0) { 
                                                $("#dnsTbody").html(result.dnOption);
                                                $(".inVisibleSelectAll").removeClass("dnsShowSelectAll");
                                                $(".ui-dialog-buttonpane button:contains('Continue')").button().show().addClass('subButton');
                                                $(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
                                                $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
						$("input[name=phoneNumber]").val('');
						$("input[name=minPhoneNumber]").val('');
						$("input[name=maxPhoneNumber]").val('');
                                            }if(result.dnOption == "" || result.dnOption.indexOf("tr")=='0'){
                                                $("#dnsTbody").html("<tr class=''><td class=\" \" colspan='4'>No Record Found</td></tr>"); 
                                            }  	
				   }else if(result.status == "error"){
                                       $("#dnsOperationDialog").html(result.responsesError);
                                       $("#dnsOperationDialog").dialog("open");
                                       $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
                                       $(".ui-dialog-buttonpane button:contains('Continue')").button().hide();
                                       $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
                                       
                                   }else {
					   alert(result.responsesError);
				   }                                   
                        }
                    });
                    //End code                    
                    $(this).dialog("close");
                    $('html, body').animate({scrollTop: '0px'}, 300);
                      
                     
                },
                "Modify DN": function() {  
                    
                    var  assignNumbers   = "";
                    var  unassignNumbers = "";
                      
                    //Start logic to create Temprary list of DNs to show on table
                    $("#assignedNumbers option").each(function(){
                          assignNumbers   += $(this).val();
                    });

                    $("#availableRanges option").each(function(){
                          unassignNumbers += $(this).val();
                    });

                    var assignNumberRange = $("#hiddenSpecificRangeValue").val();
                    var selectedSp = $("#hiddenEnterpriseName").val();
                    
                    var entDnsData = {selectedSp: selectedSp, action : 'unassignNumber', unassignNumber : unassignNumbers, assignNumber : assignNumbers ,assignNumberRange:assignNumberRange,action:'modifyDnsToEntTempList'};
                    $.ajax({
                            type: "POST",
                            url: "enterprise/DNs/dnsOperations.php",
                            data: entDnsData,
                            success: function(result) { 
				   result = JSON.parse(result);                                   
                                   console.log(result.oldAssignDNsRangeMod);
				   if(result.status == "success") {
                                       
                                           $("#availableRanges option").each(function(){
                                                unassignNumber += $(this).val() + ";;";
                                           });
                                            
					   $("#dnsOperationDialog").html(result.responses);
                                           $("#dnsOperationDialog").dialog("open");
                                            if(result.dnOption.length >0) { 
                                                $("#dnsTbody").html(result.dnOption);
                                                $(".inVisibleSelectAll").removeClass("dnsShowSelectAll");
                                                $(".ui-dialog-buttonpane button:contains('Continue')").button().show().addClass('subButton');
                                                $(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
                                                $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
						$("input[name=phoneNumber]").val('');
						$("input[name=minPhoneNumber]").val('');
						$("input[name=maxPhoneNumber]").val('');
                                            }if(result.dnOption == "" || result.dnOption.indexOf("tr")=='0'){
                                                $("#dnsTbody").html("<tr class=''><td class=\" \" colspan='4'>No Record Found</td></tr>"); 
                                            }  	
				   }else if(result.status == "error"){
                                       $("#dnsOperationDialog").html(result.responsesError);
                                       $("#dnsOperationDialog").dialog("open");
                                       $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
                                       $(".ui-dialog-buttonpane button:contains('Continue')").button().hide();
                                       $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
                                   }else{
					   alert(result.errorMsg);
				   }
                        }
                        
                    });
                    //End code                    
                    $(this).dialog("close");
                    $('html, body').animate({scrollTop: '0px'}, 300);
                },            
                "Cancel": function() {
                        $(this).dialog("close");
               },

           },
        open: function() {
            $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019	
            setDialogDayNightMode($(this));
            $(".ui-dialog-buttonpane button:contains('Delete DN')").button().show().addClass("deleteButton");
            $(".ui-dialog-buttonpane button:contains('Modify DN')").button().show().addClass("subButton");
            $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass("cancelButton");
                
	}
});
 
$(document).on("click","tr.DNsTableClass" ,function (event) {
    var disabledRowEvent = $(this).hasClass('disableService');
    if(disabledRowEvent){return false;}
    var target = event.target.tagName;
   if(target == "INPUT" || target == "SPAN") {
			return;
    }else{
        var dnsValue  =  $(this).attr("data-value") ;
        $("#hiddenDnRange").val(dnsValue);
        $("#hiddenSpecificRangeValue").val(dnsValue);
       var dnsUnAssignArray = [dnsValue];

        $.ajax({
            type: "POST",
            url: "enterprise/DNs/getAssignDns.php",
            data: { getEntDnListData: dnsUnAssignArray},
            success: function(result)
            {
                
                $("#dnsOperationDialog").dialog("close");
                $("#dnsOperationModDialog").html(result);
                $("#dnsOperationModDialog").dialog("open");
                result = JSON.toString(result);
                if(result.status == "success") {
                    if(result.dnOption.length >0) { 
                        $("#dnsTbody").html(result.dnOption);
                        $(".inVisibleSelectAll").removeClass("dnsShowSelectAll");
                    }if(result.dnOption =="" || result.dnOption.indexOf("tr")=='0'){
                        $("#dnsTbody").html("<tr class=''><td class=\" \" colspan='4'>No Record Found</td></tr>"); 
                    } 
                }
                
            }
        });
    }
        
});
	 
	
	 var removeAssignedDns = function(){
		var html = "";
		var range = "";
		var range1 = "";
		var range2 = "";
		var assignedRanges = $("#assignedRanges").val();		
		for (var i = 0; i < assignedRanges.length; i++) {
			$("#assignedRanges option[value='"+assignedRanges[i]+"']").remove();			
			range = assignedRanges[i].split(" - ");
			range1 = parseInt(range[0].replace("+1-", "")); 
			if(range[1]){
				range2 = parseInt(range[1].replace("+1-", ""));
			}else{
				range2 = "";
			}
			if(range1 != "" && range2 == ""){
				html += '<option value="+1-'+range1+'">+1-' + range1 + '</option>';
			}else if(range1 != "" && range2 != ""){
				for(j = parseInt(range1); j < parseInt(range2) + 1; j++){
					html += '<option value="+1-'+j+'">+1-' + j + '</option>';
				}
			}
		}
		$("#assignedNumbers").append(html);		
	}  
	
        //
        
        var removeTableRowAssignedDns = function(specificRowsValue){
		//debugger ;
            var html = "";
		var range = "";
		var range1 = "";
		var range2 = "";
		var assignedRanges = specificRowsValue;		
		for (var i = 0; i < assignedRanges.length; i++) {
			//$("#assignedRanges option[value='"+assignedRanges[i]+"']").remove();			
			range = assignedRanges[i].split(" - ");
			range1 = parseInt(range[0].replace("+1-", "")); 
			if(range[1]){
				range2 = parseInt(range[1].replace("+1-", ""));
			}else{
				range2 = "";
			}
			if(range1 != "" && range2 == ""){
				html += '<option value="+1-'+range1+'">+1-' + range1 + '</option>';
			}else if(range1 != "" && range2 != ""){
				for(j = parseInt(range1); j < parseInt(range2) + 1; j++){
					html += '<option value="+1-'+j+'">+1-' + j + '</option>';
				}
			}
		}
		$("#assignedNumbers").append(html);		
	}
        
        //
        
        
        
        
        
	$("#removeAssignedRanges").click(function(){
		removeAssignedDns();
		var sortingId = "assignedNumbers";
		reArrangeSelectSorting(sortingId);
	});
	
	
	/* var addAssignNumber = function(){
		html = "";
		range1 = "";

		var availableRanges = $("#availableRanges").val();		
		if(availableRanges){
			for (var i = 0; i < availableRanges.length; i++) {
				
				range1 = parseInt(availableRanges[i].replace("+1-", "")); 
				if(range1 != ""){
					html += '<option value="+1-'+range1+'">+1-' + range1 + '</option>';
				}
				$("#availableRanges option[value='"+availableRanges[i]+"']").remove();			
			}
		}
		$("#assignedNumbers").append(html);
	};
 */

        var removeAssignNumber = function(){
		html = "";
		range1 = ""; 
		var assignNumbers = $("#assignedNumbers").val();	
		if(assignNumbers){
			for (var i = 0; i < assignNumbers.length; i++) {
				
				range1 = parseInt(assignNumbers[i].replace("+1-", "")); 
				if(range1 != ""){
					html += '<option value="+1-'+range1+'">+1-' + range1 + '</option>';
				}
				$("#assignNumbers option[value='"+assignNumbers[i]+"']").remove();
				
			}
		}
		$("#availableRanges").append(html);
	};
/*	var removeAssignNumber = function(){
		html = "";
		range1 = "";
		debugger ;
		var assignNumbers = $("#assignNumbers").val();	
		if(assignNumbers){
			for (var i = 0; i < assignNumbers.length; i++) {
				
				range1 = parseInt(assignNumbers[i].replace("+1-", "")); 
				if(range1 != ""){
					html += '<option value="+1-'+range1+'">+1-' + range1 + '</option>';
				}
				$("#assignNumbers option[value='"+assignNumbers[i]+"']").remove();
				
			}
		}
		$("#dnTable").append(html);
	};  */
	
        $(document).on("click","#removeAssign, #removeAssignAll" , function(){
           var BtnId = $(this).attr('id');
		if(BtnId == "removeAssignAll"){
			$('#availableRanges option').prop('selected', true);
		}
		
		removeAssignNumber();
		var sortingId = "availableNumbers";
		reArrangeSelectSorting(sortingId); 
        });
	/*$("#removeAssign, #removeAssignAll").click(function(){ 
		var BtnId = $(this).attr('id');
		if(BtnId == "removeAssignAll"){
			$('#availableRanges option').prop('selected', true);
		}
		
		removeAssignNumber();
		var sortingId = "availableNumbers";
		reArrangeSelectSorting(sortingId);
	});*;

/*
$(document).on("click","#removeAssign, #removeAssignAll" ,function(){
    
			var BtnId = $(this).attr('id');
			if(BtnId == "removeAssignAll"){
				$('#assignedNumbers option').prop('selected', true);
			}
			
			removeAssignNumber();
			var sortingId = "availableRanges";
			reArrangeSelectSorting(sortingId);
	 
}); */

	/* $("#addAssign, #addAssignAll").click(function(){ 
			var BtnId = $(this).attr('id');
			if(BtnId == "addAssignAll"){
				$('#availableRanges option').prop('selected', true);
			}
			addAssignNumber();
			var sortingId = "availableRanges";
			reArrangeSelectSorting(sortingId);
	}); */
    
    $(document).on("change", "#checkAllDNs", function() {
    var isChecked = $(this).prop("checked");
    $(".dnsServiceLimitCheckBox:not([disabled])").prop("checked", isChecked);
    if(isChecked === false){	    
     //   unassignNumber = ""; 
    }  
     
});
				
	 
$(document).on("change", ".dnsServiceLimitCheckBox", function() {
    var totalUserAuthCheckBox = $(".dnsServiceLimitCheckBox:not([disabled])").length;
    var totalCheckedUserAuthCheckBox = $(".dnsServiceLimitCheckBox:checkbox:checked").length;
    if(totalUserAuthCheckBox == totalCheckedUserAuthCheckBox) {
            $("#checkAllDNs").prop("checked", true);
    } else {
            $("#checkAllDNs").prop("checked", false);
    }
});
	
});

function reArrangeSelectSorting(sortingId) {
	$("#"+sortingId).html($("#" + sortingId + ' option').sort(function(a, b) {
	 	return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }));
}
 
 
function getUnassignNumbers(dataToSend) {
            /*    var unassignNumber = "";
                
                    $("#availableRanges option").each(function(){
                        unassignNumber += $(this).val() + ";;";                
                    });
            */    
           
            var selUnassignDns = "";
            var selUnassignDnsChk = "";
            if($('input.dnsServiceLimitCheckBox:checked').length >0){
                $(".dnsServiceLimitCheckBox:checked").each(function() {
                var dnsValue  =  $(this).attr("data-value") ;
                var dnsUnAssignArray = [dnsValue];

                var range = "";
		var range1 = "";
		var range2 = "";
		var assignedRanges = dnsUnAssignArray;		
		for (var i = 0; i < assignedRanges.length; i++) {
			//$("#assignedRanges option[value='"+assignedRanges[i]+"']").remove();			
			range = assignedRanges[i].split(" - ");
			range1 = parseInt(range[0].replace("+1-", "")); 
			if(range[1]){
				range2 = parseInt(range[1].replace("+1-", ""));
			}else{
				range2 = "";
			}
			if(range1 != "" && range2 == ""){
				selUnassignDns += "+1-"+range1 + ";;";
                                // html += '<option value="+1-'+range1+'">+1-' + range1 + '</option>';
			}else if(range1 != "" && range2 != ""){
				for(j = parseInt(range1); j < parseInt(range2) + 1; j++){
					selUnassignDns += "+1-"+ j + ";;";
                                     //html += '<option value="+1-'+j+'">+1-' + j + '</option>';
				}
			}
		}
           });
           
       }
       if(selUnassignDns !=""){
            var idx = $.inArray(selUnassignDns, dataToSend);
                 if (idx == -1) {
             selUnassignDnsChk =  selUnassignDns;
             } else {
                 dataToSend.splice(idx, 1);
             }
        }
    dataToSend.push({name:'unassignNumber', value: unassignNumber+selUnassignDnsChk});
    return dataToSend;
  
}



    