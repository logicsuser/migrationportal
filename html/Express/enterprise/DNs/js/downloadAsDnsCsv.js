$(function()
{
	
	$('#downloadDnsCSV').click(function() {
		
            var clickedId = $(this).attr('id');
            var table = "";
            var serviceFileName = "";
            if(clickedId == "downloadDnsCSV") {
                table = "dnTable";
                serviceFileName = "EnterpriseDNs.csv";
            }  
            var titles = [];
            var data = [];
            var i = 0;
            $('#' + table + ' th').each(function() {    //table id here
                if (i > 0) {
                    titles.push($(this).text());
                }
                i++;
            });
         
            $('#' + table + ' td').each(function() {    //table id here
            	var text = "";            	
            	if($(this).find(".dnsServiceLimitCheckBox").length <= 0) {
                    data.push('="'+$(this).text()+'"');           		
            	}
            });
            
            var CSVString = prepCSVRow(titles, titles.length, '');
            CSVString = prepCSVRow(data, titles.length, CSVString);

            var blob = new Blob(["\ufeff", CSVString]);
                if (navigator.msSaveBlob) { // IE 10+
                    navigator.msSaveBlob(blob, serviceFileName);
            }else{
                var downloadLink = document.createElement("a");
                var url = URL.createObjectURL(blob);
                downloadLink.href = url;
                downloadLink.download = serviceFileName;
                document.body.appendChild(downloadLink);
                downloadLink.click();
                document.body.removeChild(downloadLink);
	    }
        });

      
});

function prepCSVRow(arr, columnCount, initial) {
    var row = '';
    var delimeter = ',';
    var newLine = '\r\n';

    function splitArray(_arr, _count) {
      var splitted = [];
      var result = [];
      _arr.forEach(function(item, idx) {
        if ((idx + 1) % _count === 0) {
          splitted.push(item);
          result.push(splitted);
          splitted = [];
        } else {
          splitted.push(item);
        }
      });
      return result;
    }
    var plainArr = splitArray(arr, columnCount);
   
    plainArr.forEach(function(arrItem) {
      arrItem.forEach(function(item, idx) {
        row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
      });
      row += newLine;
    });
    return initial + row;
  }