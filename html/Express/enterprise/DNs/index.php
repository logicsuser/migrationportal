<script>
    $("#dnTable").tablesorter();
    $(document).on("keyup","#searchDns",function(e){
		var key = e.which;
		 if(key == 13)
		  {
		    $('#getDnBtn').click();
		    return false;  
		  }
    });
        $(document).on("click","#getDnBtn",function(){
        var searchString = $("#searchDns").val();
            $("#dnTable tbody tr").filter(function() {
                var dnNumber = $(this).children(':eq(1)').text();
                    $(this).toggle(filterDns(dnNumber, searchString));
            });
             
            if($("#dnTable tbody tr td:visible").length > 0 ) {
              //  $("#noDNsFoundDiv").hide();
                $("#dnTable tbody").find('tr#resultNodFound').remove();
             // $("#dnTable tbody").append("<tr><td colspan='4' class='lableTextGrey'>No DNs Found. </td>").remove();  
                $(".inVisibleSelectAll").removeClass("dnsShowSelectAll");
            }else {
    		//$("#noDNsFoundDiv").show();
                $("#dnTable tbody").append("<tr id='resultNodFound'><td colspan='4' class='lableTextGrey'>No DNs Found. </td>");
                $(".inVisibleSelectAll").addClass("dnsShowSelectAll");
            }
        });
         
        
       function filterDns(numberString, searchString) {
           debugger;
               var numbers = createDnsFromRanges(numberString);
               if( numbers.indexOf(searchString) > -1 ) {
                   return true;
               } else {
                   return false;
               }
       } 
       
       function createDnsFromRanges(numberRanges){
		var html = "";
		var range = "";
		var range1 = "";
		var range2 = "";
		var availableRanges = numberRanges;		
			range = availableRanges.split(" - ");
			range1 = parseInt(range[0].replace("+1-", "")); 
			if(range[1]){
				range2 = parseInt(range[1].replace("+1-", ""));
			}else{
				range2 = "";
			}
			if(range1 != "" && range2 == ""){
				html += '+1-' + range1;
			}else if(range1 != "" && range2 != ""){
				for(j = parseInt(range1); j < parseInt(range2) + 1; j++){
					html += '+1-' + j;
				}
			}
                        
                return html;
	}
       
</script>
<script src="enterprise/DNs/js/DNs.js"></script>
  <script src="enterprise/DNs/js/downloadAsDnsCsv.js"></script>  

							<br/><br/>
							
				
         <div class="searchBodyForm">
                    <h2 class="h2Heading mainBannerModify">DNs</h2>
<!--             <form name="searchAdministratorForm" id="searchAdministratorForm" method="POST" class="fcorn-register container"> -->
                <div class="row">
                    <div class="col-md-11 adminSelectDiv">
                        <div class="form-group">
                            <label class="labelText" for="searchVal">Search By Dns</label><br>
                            <input placeholder="" style="width: 100% !important" type="text" class="autoFill magnify" name="searchDns" id="searchDns" size="55" value="">
                        </div>
                    </div>
                    
                     <div class="col-md-1 adminAddBtnDiv">
                        <?php
                          //  if($entPermBWAdminsLogPermission == "1" && $entPermBWAdminsLogAddDelPermission == "yes"){
                            ?>
                        <div class="form-group">
                            <p class="register-submit addAdminBtnIcon" style="text-align:center;">
                            <img src="images\icons\add_admin_rest.png" data-alt-src="images\icons\add_admin_over.png" name="addEnterpriseDNsBtn" id="enterpriseAddDns" class="">
                            <label class="labelText labelTextOpp"><span style="margin-left: 8px !important;">Add DN</span></label>
                                    <!-- select style="width: 120px; display:none" name="groupName" id="groupName"></select-->
                            </p>
                        </div>
                        <?php 
                         //   }
                        ?>
                    </div>
                    
                    
                    
                      <div class="col-md-12 form-group" style="text-align: center;">
                        <div class="col-md-4"></div>
                        <div class="col-md-4" style="text-align: center;"><input type="button" name="getDnBtn" class="cancelButton" id="getDnBtn" value="Get"></div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
<!--             </form> -->
        </div>
        <div class="loading" id="loadingDns"><img src="/Express/images/ajax-loader.gif"></div>				
							
							
							
							
							
							
							
							
                            <div class=" ">
                                <?php 
                                if($entPermDNsLogPermission == "1" && $entPermDNsLogAddDelPermission == "yes"){
                                ?>
							<div class="row">
								<div class="col-md-12">
								<div class="col-md-4"></div>
								<!--<div class="col-md-4">
									<div class="form-group">
										<input type="button" id="enterpriseAddDns" value="Add DNs to Enterprise >" style="width: 180px; font-size: 12px; margin-right: 30px; float:right" class="createNewButton" />
									</div>
								</div>-->
								<div class="col-md-4"></div>
								</div>
<!-- 								<div class="col-md-6"> -->
<!-- 									<div class="form-group"> -->
<!-- 								  		<input type="button" id="enterpriseRemoveDns" value="Remove Enterprise DNs <" style="width: 210px; font-size: 12px;" class="createNewButton" /> -->
<!-- 									</div> -->
<!-- 								</div> -->
							</div>
                                <?php 
                                    }
                                ?>

                                                    <!-- End code -->
							
								<div id="showAddDnEnterprise" class="row">
									<div class="">
										<div class="form-group">
											<h2 class="adminUserText">ADD DN</h2>
										</div>
									</div>
									<div class="col-md-7" id="phoneNumberDiv">
										<div class="form-group">
											<div class="col-md-10" style="padding-left:0px;"><label class="labelText">Phone Numbers</label>
												<input class="extraInputRow" type="text" name="phoneNumber"
													value="" style="width:400px !important;float: left" />
											</div>
                                                                                    <div class="col-md-1" style="width:13% !important;"><label class="labelText">&nbsp;</label><br/>
												<img class="" id="addNumberRowEnterprise" src="images/add.png"
													style="width: 18px; float: right; margin-top: 17px">
											</div>
										</div>
									</div>
									<div class="col-md-5" id="phoneRangeDiv">
										<div class="row" style="padding-left:0;">
											<label class="labelText">Phone Numbers Ranges</label>
											<div class="form-group">
												<div class="col-md-5" style="padding-left:0px">
													<input class="extraInputRow" type="text" name="minPhoneNumber" value="" style="width:145px !important;" />
												</div>
												<div class="col-md-1" style="padding:0 !important; width:20px; margin:0"><br/><span style="margin-top:17px;color:#555 !important;">-</span></div>
												<div class="col-md-5" style="padding-left:0">
													<input class="extraInputRow" type="text" name="maxPhoneNumber" value="" style="width:145px !important;" />
												</div>
												<div class="col-md-1">
													<img class="" id="addNumberRangeRow" src="images/add.png" style="width: 18px; margin-top: 17px">
												</div>
											</div>
										</div>
									</div>
									<div class="clr"></div>
									<div class="col-md-12 dnBtnMargin">
										<div class="col-md-7">
											<div class="form-group">
												<input type="button" id="addEnterpriseDNsBtn" class="resetAll addDNButtonColor" value="Add DN" style="margin-right: 29px;">
											</div>
										</div>
										<div class="col-md-5">
											<div class="form-group">
												<!--<input type="button" class ="cancelButton" id="closeServiceProviderDns" value="Close" style="width: 100px;">-->
												<input type="button" class ="resetAll closeDNButton" id="closeEnterpriseDNsBtn" value="Close" style="margin-left: -12px;"/>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label class="labelpadding" id="errorDnsGroup"
											style="font-weight: bold; text-align: center; color: red; width: 100%;"></label>
										</div>
									</div>
								</div> <!-- end showAddDnServiceProvider div -->
								<div id="showRemoveDnEnterprise">
									<div class="">
										<div class="form-group">
											<h2 class="adminUserText">Remove DN</h2>
										</div>
									</div>
									<div id="removePhoneNumberDiv" class="col-md-6">
										<div class="form-group">
											<div class="col-md-10" style="padding-left:0px;">
												<label class="labelText">Phone Numbers</label>
													<input class="extraInputRow" type="text" name="removePhoneNumber" value="" style="width:400px !important; float: left;" />
											</div>
                                                                                    <div class="col-md-1" style="width:13% !important;">
												<br/>
												<img class="" id="addNumberRowRemoveEnterprise" src="images/add.png" style="width: 18px; float: right; margin-top: 19px" />
											</div>
													
										</div>
									</div>
									<div id="removePhoneRangeDiv" class="col-md-6">
										<div class="col-md-12" style="padding-left:0px;margin-left: -15px;">
											<label class="labelText">Phone Numbers Ranges</label>
											<div class="form-group">
												<div class="col-md-5" style="padding-left:0px">
													<input class="extraInputRow" type="text" name="minPhoneNumberRemove" value="" style="width: 145px !important;" />
												</div>
												<div class="col-md-1" style="padding:0 !important; width:20px; margin:0">
													<br/>
													<span style="width: 20px; margin-top:17px;color:#555 !important;">-</span>
												</div>
												<div class="col-md-5" style="padding-left:0px">
													<input class="extraInputRow" type="text" name="maxPhoneNumberRemove" value="" style="width: 145px !important;" />
												</div>
												<div class="col-md-1">
													<img id="addNumberRangeRowRemoveEnt" src="images/add.png" style="width: 18px; margin-top: 17px" />
												</div>
											</div>
										</div>
									</div>
									<div class="clr"></div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<!--<input type="button" id="removeServiceProviderDns" value="Remove DN" class="deleteButton" style="width: 100px; float:right"/>-->
												<input type="button" id="removeDnsButton" value="Remove DN" class="resetAll removeDNButtonColor" style="margin-right: 29px;"/>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<!-- <input type="button" id="closeServiceProviderDnsRemove" class="cancelButton" value="Close" style="width: 100px;"/>-->
												<input type="button" id="closeRemoveDnsButton" class="resetAll closeDNButton" value="Close" style="margin-left:-12px;"/>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="form-group">
											<label class="labelText" id="errorDnsRemoveGroup" style="font-weight: bold; text-align: center; color: red; width: 100%;"></label>
										</div>
									</div>
								</div>
								
								
							</div>	



			
				<!--new DNs code -->
				
				<div class="row inVisibleSelectAll">
	<div class="col-md-6" style="padding-left: 28px;">
		<input type="checkbox" class="inputCheckbox" name="checkAllDNs" id="checkAllDNs">
		<label for="checkAllDNs">
			<span style="text-align: center;margin-left: 15px;"></span>
		</label>
		<label class="labelText" style="margin-top: 53px;"> Select All</label>
	</div>
			
	<div class="col-md-6">
		<div class="fcorn-register" style="">
			<div class="" id="downloadDnsCSV" style="float:right;margin-bottom: 8px;">
				<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" id="downloadAdminCSV" style="cursor:pointer;">
				<br><span>Download<br>CSV</span>
			</div>
		</div>
	</div>
</div>


<div class="" style="margin-bottom: 25px;">
	<table class="tablesorter scroll dataTable" style="margin: 0" id="dnTable">
		<thead>
			<tr>
				<th class="thsmall header" style="width:10% !important;max-width: 70px; background-image: none !important; pointer-events: none;">Delete</th>
				<th class="thsmall header eventTableShorter" style="max-width: 212px !important;">DNs</th>
				<th class="thsmall header" style="max-width: 212px !important;">Group Id</th>
			 </tr>
		</thead>
				
		<tbody id="dnsTbody" style=" max-height:240px; transition:all 1s ease 0s;">
			 
		</tbody>
	</table>
    
   <!-- <p id="noDNsFoundDiv" class="labelTextGrey " style="text-align: center;display:none">No DNs Found.</p>-->
</div>
				
				<!--end DNs code -->
							
							
							<!--<div class="row">
								<div class="">
									<div class="form-group">
										<h2 class="adminUserText">Assign Phone Numbers</h2>
									</div>
								</div>
							</div>
							<div class="row" id="dnsModify"> -->
								  
							 
							

<!--
<div id="dnAssignDiv" style="display:none">

 
<div class="row">
	<div class="">
		<div class="form-group">
			<h2 class="adminUserText">ADD DN</h2>
		</div>
	</div>
</div>
    
    
<div class="row form-group alignCenter dnsButton" id="" style="width: 830px !important;margin: 0 auto;">
	<div class="col-md-12" style="margin-left: 136px;">
		<div class="" style="float:left; padding-right:15px;">
			<div class="form-group alignCenter" style="width:235px;">
				<label class="labelTextGrey">UnAssigned Phone Number</label>
			</div>
			<div class="form-group alignCenter dnranges" style="width:235px;">
                            
				<select id="availableRanges" name="availableRanges" multiple="" style="height: 250px;width: 230px !important;"></select>
				<input type="hidden" name="" id="" value="">
			</div>
		</div>
		
		<div class="" style="float:left; padding-right:10px;">
			<div class="form-group alignCenter dnsButton">
			<br><br><br><br><br><br><br><br><br><br>
					<button type="button" id="" name="" class="addBlueCircle">
						<img src="/Express/images/icons/arrow_Add.png"></button>
					<br> 
					<button type="button" id="removeAssign" name="removeAvailableNumbers" class="addRedCircle">
						<img src="/Express/images/icons/arrow_remove.png">
					</button>
						<br><br> 
					  <button type="button" id="addAssignAll" name="addAvailableNumbers" class="addBlueCircle">
						  <img src="/Express/images/icons/arrow_add_all.png">
					  </button>
						  <br>
					<button type="button" id="removeAssignAll" name="removeAvailableNumbers" class="addRedCircle" style="margin-left:-4px;">
						<img src="/Express/images/icons/arrow_remove_all.png">
					</button>
			</div>
		</div>
		
		<div class="" style="float:left; ">
			<div class="form-group alignCenter">
				<label class="labelTextGrey" style="text-align: center;">Assigned Phone Number</label>
			</div>
			<div class="form-group alignCenter dnranges" style="width:235px;">
						<select id="assignedNumbers" name="assignedNumbers" multiple style="height: 220px; padding:4px !important;width:230px !important"></select><input type="hidden" name="assignedNumberss"
												id="assignedNumberss" value="" />
			</div>
		</div>
	</div>
</div>

</div> -->
<!--end dialog code-->



<input type="hidden" value="" id="hiddenSpecificRangeValue"> 




							
                        <div id="dnsOperationDialog" class="dialogClass">
<!--                             <div id="inputFormControl"></div> -->
                        </div>
<div id="dnsOperationModDialog" class="dialogClass"></div>