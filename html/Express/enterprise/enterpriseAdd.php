<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/enterprise/enterpriseOperations.php");

require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
$expProvDB = new ExpressProvLogsDB();
$changeLogArr = array();

$addObj = new EnterpriseOperations();
$addResp = $addObj->addEnterprise($_POST["basicInfo"]);
//print_r($addResp);
$resp["entId"] = array();
$resp["Success"]= array();
$resp["Error"] = array();
$resp["clone"] = array();

server_fail_over_debuggin_testing(); /* for fail Over testing. */

//print_r($_POST);

if(!empty($addResp["Error"])){
    $resp["Error"]=$addResp["Error"];
    //echo "<p style='text-align:center;color:red;'>".$addResp["Error"]."</p>";
}else{
    if($_POST["enterpriseCloneID"] != ""){
        $serviceProviderId = $_POST["basicInfo"]["serviceProviderId"];
        
        $cloneResp = array();
        $_POST["selectedEnterprise"] = $_POST["enterpriseCloneID"];
        require_once("/var/www/html/Express/enterprise/getEnterpriseAllInformations.php");
        
        if(isset($_POST["clone"]["domainsClone"]) && $_POST["clone"]["domainsClone"] == "true"){
            $domainsData = $allResp["domains"]["domainAssigned"];
            foreach($domainsData as $domainKey => $domainValue){
                if($domainValue == $_POST["basicInfo"]["defaultDomain"]){
                    unset($domainsData[$domainKey]);
                }
            }
            if(count($domainsData) > 0){
                $cloneResp["domainAssigned"] = $addObj->enterpriseDomainAssignRequest($serviceProviderId, $domainsData);
            }
            
        }
        
        if(isset($_POST["clone"]["securityDomainsClone"]) && $_POST["clone"]["securityDomainsClone"] == "true"){
            $securityDomainsData = $allResp["domains"]["securityDomainAssigned"];
            foreach($securityDomainsData as $domainKey => $domainValue){
                if($domainValue == $_POST["basicInfo"]["defaultDomain"]){
                    unset($securityDomainsData[$domainKey]);
                }
            }
            if(count($securityDomainsData) > 0){
                $cloneResp["securityDomainAssigned"] = $addObj->enterpriseDomainAssignRequest($serviceProviderId, $securityDomainsData);
            }
            
        }
        
        if(isset($_POST["clone"]["groupServiceClone"]) && $_POST["clone"]["groupServiceClone"] == "true"){
            if(count($entGroupServiceArray) > 0){
                $cloneResp["groupService"] = $addObj->serviceProviderServiceModifyAuthorizationListRequestClone($serviceProviderId, "groupServiceAuthorization", $entGroupServiceArray);
            }
        }
        
        if(isset($_POST["clone"]["userServiceClone"]) && $_POST["clone"]["userServiceClone"] == "true"){
            if(count($entUserServiceArray) > 0){
                $cloneResp["userService"] = $addObj->serviceProviderServiceModifyAuthorizationListRequestClone($serviceProviderId, "userServiceAuthorization", $entUserServiceArray);
            }
        }
        
        if(isset($_POST["clone"]["clidPolicyClone"]) && $_POST["clone"]["clidPolicyClone"] == "true"){
            $callingLineIdPolicy = array();
            
            $callingLineIdPolicy["serviceProviderId"] = $serviceProviderId;
            
            $callingLineIdPolicy["clidPolicy"] = $allResp["callProcessingPolicy"]["Success"]["clidPolicy"];
            $callingLineIdPolicy["enterpriseCallsCLIDPolicy"] = $allResp["callProcessingPolicy"]["Success"]["enterpriseCallsCLIDPolicy"];
            
            $callingLineIdPolicy["groupCallsCLIDPolicy"] = $allResp["callProcessingPolicy"]["Success"]["groupCallsCLIDPolicy"];
            $callingLineIdPolicy["emergencyClidPolicy"] = $allResp["callProcessingPolicy"]["Success"]["emergencyClidPolicy"];
            
            $callingLineIdPolicy["allowAlternateNumbersForRedirectingIdentity"] = $allResp["callProcessingPolicy"]["Success"]["allowAlternateNumbersForRedirectingIdentity"];
            
            $callingLineIdPolicy["allowConfigurableCLIDForRedirectingIdentity"] = $allResp["callProcessingPolicy"]["Success"]["allowConfigurableCLIDForRedirectingIdentity"];
            
            $callingLineIdPolicy["blockCallingNameForExternalCalls"] = $allResp["callProcessingPolicy"]["Success"]["blockCallingNameForExternalCalls"];
            
            $callingLineIdPolicy["useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable"] = $allResp["callProcessingPolicy"]["Success"]["useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable"];
            
            $callingLineIdPolicy["useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable"] = $allResp["callProcessingPolicy"]["Success"]["useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable"];
            
            $cloneResp["callingLineIDPolicy"] = $addObj->modifyEnterpriseCallProcessingPolicy($callingLineIdPolicy);
        }
        
        if(isset($_POST["clone"]["mediaPolicyClone"]) && $_POST["clone"]["mediaPolicyClone"] == "true"){
            $mediaPolicyArray = array();
            $mediaPolicyArray["mediaPolicySelection"] = $allResp["callProcessingPolicy"]["Success"]["mediaPolicySelection"];
            $mediaPolicyArray["serviceProviderId"] = $serviceProviderId;
            $cloneResp["mediaPolicy"] = $addObj->modifyEnterpriseCallProcessingPolicy($mediaPolicyArray);
        }
        
        if(isset($_POST["clone"]["callLimitsClone"]) && $_POST["clone"]["callLimitsClone"] == "true"){
            $callLimits = array();
            $callLimits["useMaxSimultaneousCalls"] = $allResp["callProcessingPolicy"]["Success"]["useMaxSimultaneousCalls"];
            $callLimits["maxSimultaneousCalls"] = $allResp["callProcessingPolicy"]["Success"]["maxSimultaneousCalls"];
            $callLimits["useMaxSimultaneousVideoCalls"] = $allResp["callProcessingPolicy"]["Success"]["useMaxSimultaneousVideoCalls"];
            $callLimits["maxSimultaneousVideoCalls"] = $allResp["callProcessingPolicy"]["Success"]["maxSimultaneousVideoCalls"];
            $callLimits["useMaxCallTimeForAnsweredCalls"] = $allResp["callProcessingPolicy"]["Success"]["useMaxCallTimeForAnsweredCalls"];
            $callLimits["maxCallTimeForAnsweredCallsMinutes"] = $allResp["callProcessingPolicy"]["Success"]["maxCallTimeForAnsweredCallsMinutes"];
            
            $callLimits["useMaxCallTimeForUnansweredCalls"] = $allResp["callProcessingPolicy"]["Success"]["useMaxCallTimeForUnansweredCalls"];
            $callLimits["maxCallTimeForUnansweredCallsMinutes"] = $allResp["callProcessingPolicy"]["Success"]["maxCallTimeForUnansweredCallsMinutes"];
            $callLimits["useMaxConcurrentRedirectedCalls"] = $allResp["callProcessingPolicy"]["Success"]["useMaxConcurrentRedirectedCalls"];
            $callLimits["maxConcurrentRedirectedCalls"] = $allResp["callProcessingPolicy"]["Success"]["maxConcurrentRedirectedCalls"];
            
            $callLimits["useMaxConcurrentFindMeFollowMeInvocations"] = $allResp["callProcessingPolicy"]["Success"]["useMaxConcurrentFindMeFollowMeInvocations"];
            $callLimits["maxConcurrentFindMeFollowMeInvocations"] = $allResp["callProcessingPolicy"]["Success"]["maxConcurrentFindMeFollowMeInvocations"];
            
            $callLimits["useMaxFindMeFollowMeDepth"] = $allResp["callProcessingPolicy"]["Success"]["useMaxFindMeFollowMeDepth"];
            $callLimits["maxFindMeFollowMeDepth"] = $allResp["callProcessingPolicy"]["Success"]["maxFindMeFollowMeDepth"];
            
            $callLimits["maxRedirectionDepth"] = $allResp["callProcessingPolicy"]["Success"]["maxRedirectionDepth"];
            $callLimits["serviceProviderId"] = $serviceProviderId;
            $cloneResp["callLimits"] = $addObj->modifyEnterpriseCallProcessingPolicy($callLimits);
        }
        
        if(isset($_POST["clone"]["conferencingPolicyClone"]) && $_POST["clone"]["conferencingPolicyClone"] == "true"){
            $conferencingPolicy = array();
            $conferencingPolicy["useSettingLevel"] = $allResp["callProcessingPolicy"]["Success"]["useSettingLevel"];
            $conferencingPolicy["conferenceURI"] = $allResp["callProcessingPolicy"]["Success"]["conferenceURI"];
            $conferencingPolicy["useSettingLevel"] = $allResp["callProcessingPolicy"]["Success"]["useSettingLevel"];
            $conferencingPolicy["useSettingLevel"] = $allResp["callProcessingPolicy"]["Success"]["useSettingLevel"];
            $conferencingPolicy["serviceProviderId"] = $serviceProviderId;
            $cloneResp["conferencingPolicy"] = $addObj->modifyEnterpriseCallProcessingPolicy($conferencingPolicy);
        }
        
        if(isset($_POST["clone"]["tranlatingAndRoutingPoliciesClone"]) && $_POST["clone"]["tranlatingAndRoutingPoliciesClone"] == "true"){
            $translationData = array();
            $translationData["networkUsageSelection"] = $allResp["callProcessingPolicy"]["Success"]["networkUsageSelection"];
            $translationData["enableEnterpriseExtensionDialing"] = $allResp["callProcessingPolicy"]["Success"]["enableEnterpriseExtensionDialing"];
            $translationData["enforceGroupCallingLineIdentityRestriction"] = $allResp["callProcessingPolicy"]["Success"]["enforceGroupCallingLineIdentityRestriction"];
            $translationData["enforceEnterpriseCallingLineIdentityRestriction"] = $allResp["callProcessingPolicy"]["Success"]["enforceEnterpriseCallingLineIdentityRestriction"];
            $translationData["allowEnterpriseGroupCallTypingForPrivateDialingPlan"] = $allResp["callProcessingPolicy"]["Success"]["allowEnterpriseGroupCallTypingForPrivateDialingPlan"];
            $translationData["allowEnterpriseGroupCallTypingForPublicDialingPlan"] = $allResp["callProcessingPolicy"]["Success"]["allowEnterpriseGroupCallTypingForPublicDialingPlan"];
            $translationData["overrideCLIDRestrictionForPrivateCallCategory"] = $allResp["callProcessingPolicy"]["Success"]["overrideCLIDRestrictionForPrivateCallCategory"];
            $translationData["useEnterpriseCLIDForPrivateCallCategory"] = $allResp["callProcessingPolicy"]["Success"]["useEnterpriseCLIDForPrivateCallCategory"];
            $translationData["serviceProviderId"] = $serviceProviderId;
            $cloneResp["translationRouting"] = $addObj->modifyEnterpriseCallProcessingPolicy($translationData);
        }
        
        if(isset($_POST["clone"]["disableCallerIDPolicyClone"]) && $_POST["clone"]["disableCallerIDPolicyClone"] == "true"){
            $disableCallerID = array();
            $disableCallerID["serviceProviderId"] = $serviceProviderId;
            $disableCallerID["enableDialableCallerID"] = $allResp["callProcessingPolicy"]["Success"]["enableDialableCallerID"];
            $cloneResp["disableCallerId"] = $addObj->modifyEnterpriseCallProcessingPolicy($disableCallerID);
        }
        
        if(isset($_POST["clone"]["dialPlanPolicyClone"]) && $_POST["clone"]["dialPlanPolicyClone"] == "true"){
            $dialPlanData = $allResp["dialPlanData"]["Success"];
            $cloneResp["dialPlanData"] = $addObj->serviceProviderDialPlanPolicyModifyRequest($serviceProviderId, $dialPlanData);
            if(count($allResp["dialPlanAccessCodeList"]) > 0){
                foreach ($allResp["dialPlanAccessCodeList"] as $key => $value){
                    $cloneResp["dialPlanAccessCodeList"][] = $addObj->ServiceProviderDialPlanPolicyAddAccessCodeRequest($serviceProviderId, $value);
                }
                
            }
        }
        
        if(isset($_POST["clone"]["voiceMessagingPolicyClone"]) && $_POST["clone"]["voiceMessagingPolicyClone"] == "true"){
            if(count($allResp["voiceMessaging"]["Success"]) > 0){
                $voiceMessagingData = $allResp["voiceMessaging"]["Success"];
                $voiceMessagingData["serviceProviderId"] = $serviceProviderId;
                $cloneResp["voiceMessaging"] = $addObj->enterpriseVoiceMessagingModifyRequest($voiceMessagingData);
            }
            
        }
        
        if(isset($_POST["clone"]["networkClassOfServicesClone"]) && $_POST["clone"]["networkClassOfServicesClone"] == "true"){
            if($allResp["ncsData"]["entData"] != ""){
                $entNCS["assigned"] = $allResp["ncsData"]["entData"];
                $entNCS["default"] = $allResp["ncsData"]["defaultData"];
                $cloneResp["ncs"] = $addObj->enterpriseNCSAssignRequest($serviceProviderId, $entNCS);
            }
        }
        
        if(isset($_POST["clone"]["routingProfileClone"]) && $_POST["clone"]["routingProfileClone"] == "true"){
            if($allResp["selectedRoutingProfile"]["Success"] != ""){
                $routingProfileData["routingProfile"] = $allResp["selectedRoutingProfile"]["Success"];
                $cloneResp["routingProfile"] = $addObj->enterpriseRoutingProfileModifyRequest($serviceProviderId, $routingProfileData);
            }
        }
        
        if(isset($_POST["clone"]["passwordRulesClone"]) && $_POST["clone"]["passwordRulesClone"] == "true"){
            if($allResp["passwordRules"]["Success"] != ""){
                $cloneResp["passwordRules"] = $addObj->enterprisePasswordRulesModifyRequest($serviceProviderId, $allResp["passwordRules"]["Success"]);
            }
        }
        
        if(isset($_POST["clone"]["sipPasswordAuthClone"]) && $_POST["clone"]["sipPasswordAuthClone"] == "true"){
            if($allResp["sipPasswordRules"]["Success"] != ""){
                $cloneResp["sipPasswordRules"] = $addObj->enterpriseSIPPasswordRulesModifyRequest($serviceProviderId, $allResp["sipPasswordRules"]["Success"]);
            }
        }
        
        if(isset($_POST["clone"]["deviceAuthPasswordClone"]) && $_POST["clone"]["deviceAuthPasswordClone"] == "true"){
            if($allResp["devicePasswordRules"]["Success"] != ""){
                $cloneResp["devicePasswordRules"] = $addObj->enterpriseSIPPasswordRulesModifyRequest($serviceProviderId, $allResp["devicePasswordRules"]["Success"]);
            }
        }
        $resp["clone"] = $cloneResp;
    }
    
    $resp["Success"] = $_POST["basicInfo"]["serviceProviderId"];
    $dummyArray = $_POST["basicInfo"];
    $changeLogArr  = array(
        'enterpriseId'                     => $dummyArray["serviceProviderId"],
        'enterpriseName'              => ($dummyArray["serviceProviderName"]!="") ? $dummyArray["serviceProviderName"]: "None",
        'defaultDomain'            => ($dummyArray["defaultDomain"]!="") ? $dummyArray["defaultDomain"]: "None",
        'contactName'        => ($dummyArray["contactName"]!="") ? $dummyArray["contactName"]: "None",
        'contactPhone' => ($dummyArray["contactNumber"]!="") ? $dummyArray["contactNumber"]: "None",
        'contactEmail' => ($dummyArray["contactEmail"]!="") ? $dummyArray["contactEmail"]: "None",
        'supportEmail' => ($dummyArray["supportEmail"]!="") ? $dummyArray["supportEmail"]: "None",
        'addressLine1' => ($dummyArray["addressLine1"]!="") ? $dummyArray["addressLine1"]: "None",
        'addressLine2' => ($dummyArray["addressLine2"]!="") ? $dummyArray["addressLine2"]: "None",
        'city' => ($dummyArray["city"]!="") ? $dummyArray["city"]: "None",
        'stateProvince' => ($dummyArray["stateOrProvince"]!="") ? $dummyArray["stateOrProvince"]: "None",
        'zipPostalCode' => ($dummyArray["zipOrPostalCode"]!="") ? $dummyArray["zipOrPostalCode"]: "None",
        'country' => ($dummyArray["country"]!="") ? $dummyArray["country"]: "None",
    );
    
    $changes        = array();
    $changes        = $changeLogArr;
    $module         = "Enterprise Add";
    $tableName      = "enterpriseAddChanges";
    $entityName     = $dummyArray["serviceProviderId"];
    //echo "<pre>Changes - ";
    //print_r($changes);
    $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
    $changeResponse = $cLUObj->changeLogAddUtility($module, $entityName, $tableName, $changes);
    //echo "<p style='text-align:center;'>".$_POST["basicInfo"]["serviceProviderId"]." : Enterprise created succesfully.</p>";
    //$resp["entId"] =  $_POST["basicInfo"]["serviceProviderId"];
}
echo json_encode($resp);
?>