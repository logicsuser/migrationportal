<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/enterprise/enterpriseOperations.php");
$obj = new EnterpriseOperations();

$systemDomainsList = $obj->systemDomainGetListRequest();
$resp["available"] = array();
$resp["assigned"] = array();
$enterpriseDomainsList = $obj->serviceProviderDomainGetAssignedListRequest($_POST["selectedEnterprise"]);
$availableDomains = $obj->getAvailableDomainsforEnterprise($systemDomainsList, $enterpriseDomainsList);
$resp["available"] = $obj->getSecurityDomain($availableDomains['available'], $securityDomainPattern);
$resp["assigned"] = $obj->getSecurityDomain($enterpriseDomainsList['Success']["domains"], $securityDomainPattern);
echo json_encode($resp);
?>