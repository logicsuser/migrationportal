<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/enterprise/enterpriseOperations.php");
$obj = new EnterpriseOperations();
$allResp["basicInfo"] = $obj->getEnterpriseInfo($_POST["selectedEnterprise"], $ociVersion);

$systemDomainsList = $obj->systemDomainGetListRequest();

$enterpriseDomainsList = $obj->serviceProviderDomainGetAssignedListRequest($_POST["selectedEnterprise"]);

$availableDomains = $obj->getAvailableDomainsforEnterprise($systemDomainsList, $enterpriseDomainsList);

$allResp["domains"]["availableDoamins"] = $domainAvailable = $obj->getAvailableDomainsBasedOnSPattern($availableDomains["available"], $securityDomainPattern);
$allResp["domains"]["domainAssigned"] = $domainAssigned = $obj->getAvailableDomainsBasedOnSPattern($enterpriseDomainsList['Success']["domains"], $securityDomainPattern);

$allResp["domains"]["availableSecurityDoamins"] = $securityDomainAvailable = $obj->getSecurityDomain($availableDomains['available'], $securityDomainPattern);
$allResp["domains"]["securityDomainAssigned"] = $securityDomainAssigned = $obj->getSecurityDomain($enterpriseDomainsList['Success']["domains"], $securityDomainPattern);

$allResp["callProcessingPolicy"] = $obj->enterpriseCallProcessingGetRequest($_POST["selectedEnterprise"], $ociVersion);

$allResp["voiceMessaging"] = $obj->getEnterpriseVoiceMessagingRequest($_POST["selectedEnterprise"]);

$allResp["voicePortal"] = $obj->getEnterpriseVoicePortalRequest($_POST["selectedEnterprise"]); //Code added @06 June 2019
$voicePortalTabShow     = $obj->getContextForVoicePortalRequest($ociVersion); //Code added @06 June 2019
$voicePortalContext     = $voicePortalTabShow["Success"]["voicePortalScope"];

$enterpriseNCS = $obj->enterprieNCSRequest($_POST["selectedEnterprise"]);
$systemNCS = $obj->systemNCSGetRequest();
$allResp["ncsData"] = $obj->compareSystemAndEnterpriseNCSData($systemNCS["Success"]["systemAvailArr"], $enterpriseNCS["Success"]["ncsAvailArr"]);

$allResp["selectedRoutingProfile"] = $obj->enterpriseRoutingProfileGetRequest($_POST["selectedEnterprise"]);
$allResp["routingProfile"] = $obj->systemRoutingProfileGetRequest();

$allResp["passwordRules"] = $obj->enterprisePasswordRulesGetRequest($_POST["selectedEnterprise"], $ociVersion);
$allResp["sipPasswordRules"] = $obj->enterpriseSIPAuthenticationPasswordGetRequest($_POST["selectedEnterprise"]);
$allResp["devicePasswordRules"] = $obj->enterpriseDeviceAuthenticationPasswordRulesGetRequest($_POST["selectedEnterprise"]);
$allResp["passcodeRules"] = $obj->enterprisePasscodeRulesGetRequest($_POST["selectedEnterprise"]);
//print_r($allResp["passcodeRules"]);
//print_r($enterpriseDomainsList);
$resultMergedArray = array_merge($securityDomainAssigned, $securityDomainAvailable);
$enterpriseDomainsListResp = array();
if(count($enterpriseDomainsList["Success"]["domains"])>0){
    foreach ($enterpriseDomainsList["Success"]["domains"] as $ktkey => $ktval){
        if(!in_array($ktval, $resultMergedArray)){
            $enterpriseDomainsListResp[] = $ktval;
        }
    }
}
if(!in_array($enterpriseDomainsList["Success"]["defaultDomain"], $enterpriseDomainsListResp)){
    $enterpriseDomainsListResp[] = $enterpriseDomainsList["Success"]["defaultDomain"];
}
ksort($enterpriseDomainsListResp);
$allResp["dialPlanData"] = $obj->serviceProviderDialPlanPolicyGetRequest($_POST["selectedEnterprise"]);
//print_r($allResp["dialPlanData"]);

$resp = $obj->ServiceProviderDialPlanPolicyGetAccessCodeListRequest($_POST["selectedEnterprise"]);

if(count($resp["Success"]) > 0){
    foreach ($resp["Success"] as $key => $value){
        $responseDetails = $obj->serviceProviderDialPlanPolicyGetAccessCodeRequest($_POST["selectedEnterprise"], $value["accessCode"]);
        $respDet[] = $responseDetails["Success"];
    }
}
$allResp["dialPlanAccessCodeList"] = $respDet;
$_SESSION["allResp"] = $allResp;
$serviceResp = $obj->serviceProviderServiceGetAuthorizationListRequest($_POST["selectedEnterprise"]);
$entUserServiceArray = array();
$entGroupServiceArray = array();
foreach ($serviceResp["Success"]["userServices"] as $entSKey => $entSValue){
    if($entSValue["authorized"] == "true"){
        $entUserServiceArray[$entSKey] = $entSValue;
    }
}
foreach ($serviceResp["Success"]["groupServices"] as $entGKey => $entGValue){
    if($entGValue["authorized"] == "true"){
        $entGroupServiceArray[$entGKey] = $entGValue;
    }
}
//print_r($_SESSION["allResp"]);
?>