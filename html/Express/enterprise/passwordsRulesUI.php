<h2 class="adminUserText">Password Rules</h2>

<div style="clear: right;">&nbsp;</div>

<div class="row">
	<div class="col-md-7">
    	<div class="col-md-3">
    	
    	</div>
    	<div class="col-md-9">
			<label class="labelText fontHead cpMarginBottom">Rules apply to:</label>
    	</div>
	</div>
	<div class="col-md-5">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="password[rulesApplyTo]" type="radio" value="Administrator" id="rulesApplyTo1" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["rulesApplyTo"] == "Administrator"){echo "checked";}?>>
					<label for="rulesApplyTo1"><span></span></label>
					<label class="labelText">Group Administrators Only</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="password[rulesApplyTo]" type="radio" value="Administrator and User" id="rulesApplyTo2" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["rulesApplyTo"] == "Administrator and User"){echo "checked";}?>>
					<label for="rulesApplyTo2"><span></span></label> 
					<label class="labelText">Group Administrators and Users</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="password[rulesApplyTo]" type="radio" value="Group Administrator and User External Authentication" id="rulesApplyTo3" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["rulesApplyTo"] == "Group Administrator and User External Authentication"){echo "checked";}?>>
					<label for="rulesApplyTo3"><span></span></label> 
					<label class="labelText">Group Administrators and<br />Users use external authentication</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="password[allowWebAddExternalAuthenticationUsers]" type="hidden" value="false">
					<input name="password[allowWebAddExternalAuthenticationUsers]" type="checkbox" value="true" id="allowWebAddExternalAuthenticationUsers" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["allowWebAddExternalAuthenticationUsers"] == "true"){echo "checked";}?>>
					<label for="allowWebAddExternalAuthenticationUsers" style="margin-top:5px;padding-left:3px;"><span></span></label> 
					<label class="labelText">Allow users to be created on web portal</label>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="password[disallowUserId]" type="hidden" value="false">
				<input name="password[disallowUserId]" id="disallowUserId" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["disallowUserId"] == "true"){echo "checked";}?>>
				<label for="disallowUserId"><span></span></label>
				<label class="labelText">cannot contain the login ID</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="password[disallowOldPassword]" type="hidden" value="false">
				<input name="password[disallowOldPassword]" id="disallowOldPassword" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["disallowOldPassword"] == "true"){echo "checked";}?>>
				<label for="disallowOldPassword"><span></span></label>
				<label class="labelText">cannot contain the old password</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="password[disallowReversedOldPassword]" type="hidden" value="false">
				<input name="password[disallowReversedOldPassword]" id="disallowReversedOldPassword" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["disallowReversedOldPassword"] == "true"){echo "checked";}?>>
				<label for="disallowReversedOldPassword"><span></span></label>
				<label class="labelText">cannot be the reverse of the old password</label>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="password[disallowPreviousPasswords]" type="hidden" value="false">
				<input name="password[disallowPreviousPasswords]" id="disallowPreviousPasswords" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["disallowPreviousPasswords"] == "true"){echo "checked";}?>>
				<label for="disallowPreviousPasswords" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">cannot be any of the last</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="password[numberOfPreviousPasswords]" id="numberOfPreviousPasswords">
				<?php 
				$optTag = "";
				for ($i=1; $i<11; $i++){
				    $selectedVal = "";
				    if($_SESSION["allResp"]["passwordRules"]["Success"]["numberOfPreviousPasswords"] == $i){
				        $selectedVal = "selected";
				    }
				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
				}
				echo $optTag;
				?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">passwords</label>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="password[restrictMinDigits]" type="hidden" value="false">
				<input onchange="calculatePasswordRules()" name="password[restrictMinDigits]" id="restrictMinDigits" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["restrictMinDigits"] == "true"){echo "checked";}?>>
				<label for="restrictMinDigits" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must contain at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="password[minDigits]" id="minDigits" onchange="calculatePasswordRules()">
				<?php 
				$optTag = "";
				for ($i=1; $i<11; $i++){
				    $selectedVal = "";
				    if($_SESSION["allResp"]["passwordRules"]["Success"]["minDigits"] == $i){
				        $selectedVal = "selected";
				    }
				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
				}
				echo $optTag;
				?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">number(s)</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="password[restrictMinUpperCaseLetters]" type="hidden" value="false">
				<input onchange="calculatePasswordRules()" name="password[restrictMinUpperCaseLetters]" id="restrictMinUpperCaseLetters" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["restrictMinUpperCaseLetters"] == "true"){echo "checked";}?>>
				<label for="restrictMinUpperCaseLetters" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must contain at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="password[minUpperCaseLetters]" id="minUpperCaseLetters" onchange="calculatePasswordRules()">
				<?php 
				$optTag = "";
				for ($i=1; $i<11; $i++){
				    $selectedVal = "";
				    if($_SESSION["allResp"]["passwordRules"]["Success"]["minUpperCaseLetters"] == $i){
				        $selectedVal = "selected";
				    }
				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
				}
				echo $optTag;
				?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">uppercase alpha character(s)</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="password[restrictMinLowerCaseLetters]" type="hidden" value="false">
				<input onchange="calculatePasswordRules()" name="password[restrictMinLowerCaseLetters]" id="restrictMinLowerCaseLetters" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["restrictMinLowerCaseLetters"] == "true"){echo "checked";}?>>
				<label for="restrictMinLowerCaseLetters" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must contain at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="password[minLowerCaseLetters]" id="minLowerCaseLetters" onchange="calculatePasswordRules()">
				<?php 
				$optTag = "";
				for ($i=1; $i<11; $i++){
				    $selectedVal = "";
				    if($_SESSION["allResp"]["passwordRules"]["Success"]["minLowerCaseLetters"] == $i){
				        $selectedVal = "selected";
				    }
				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
				}
				echo $optTag;
				?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">lowercase alpha character(s)</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="password[restrictMinNonAlphanumericCharacters]" type="hidden" value="false">
				<input onchange="calculatePasswordRules()" name="password[restrictMinNonAlphanumericCharacters]" id="restrictMinNonAlphanumericCharacters" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["restrictMinNonAlphanumericCharacters"] == "true"){echo "checked";}?>>
				<label for="restrictMinNonAlphanumericCharacters" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must contain at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="password[minNonAlphanumericCharacters]" id="minNonAlphanumericCharacters" onchange="calculatePasswordRules()">
				<?php 
				$optTag = "";
				for ($i=1; $i<11; $i++){
				    $selectedVal = "";
				    if($_SESSION["allResp"]["passwordRules"]["Success"]["minNonAlphanumericCharacters"] == $i){
				        $selectedVal = "selected";
				    }
				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
				}
				echo $optTag;
				?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">non-alphanumeric character(s)</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				
				<input name="cpPolicy11[allowAlternateNumbersForRedirectingIdentity]" id="allowAlternateNumbersForRedirectingIdentityPW" type="checkbox" value="true" disabled checked>
				<label for="allowAlternateNumbersForRedirectingIdentityPW" style="float:left;opacity:0.6;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must be at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="password[minLength]" id="minLength">
				<?php 
				$optTag = "";
				for ($i=3; $i<41; $i++){
				    $selectedVal = "";
				    if($_SESSION["allResp"]["passwordRules"]["Success"]["minLength"] == $i){
				        $selectedVal = "selected";
				    }
				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
				}
				echo $optTag;
				?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">characters</label>
			</div>
		</div>
	</div>
</div>

<div style="clear: right;">&nbsp;</div>
<div style="clear: right;">&nbsp;</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
				<label style="margin-left:0; margin-bottom:20px;" class="labelText">Passwords expire:</label>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			
			<div class="form-group alignChkBox ">
						<input name="password[passwordExpiresDays]" id="expiration1" class="passwordExpiresDaysChk" type="radio" value="0" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["passwordExpiresDays"] == 0){echo "checked";}?>>
						<label for="expiration1"><span></span></label> <label class="labelText" style="margin-bottom:0;">Never</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<input name="password[passwordExpiresDays]" id="expiration2" class="passwordExpiresDaysChk" type="radio" value="" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["passwordExpiresDays"] > 0){echo "checked";}?>>
						<label style="padding-left: 35px;" for="expiration2"><span></span></label> <label class="labelText" style="margin-bottom:0;">After</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<input type="text" name="password[passwordExpiresDays]" id="expirationDay" style="width:100px !important;" size="3" maxlength="3" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["passwordExpiresDays"] == 0){echo "disabled";}?> value="<?php echo $_SESSION["allResp"]["passwordRules"]["Success"]["passwordExpiresDays"]; ?>"> 
						<label class="labelText" style="margin-bottom:0;">Days</label>
				</div>
		</div>
		
	</div>
</div>
<?php if($ociVersion == "22"){?>
<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="password[forcePasswordChangeAfterReset]" type="hidden" value="false">
				<input name="password[forcePasswordChangeAfterReset]" id="forcePasswordChangeAfterReset" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["forcePasswordChangeAfterReset"] == "true"){echo "checked";}?>>
				<label for="forcePasswordChangeAfterReset"><span></span></label>
				<label class="labelText">Force password change after reset</label>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<div style="clear: right;">&nbsp;</div>
<div style="clear: right;">&nbsp;</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
				<label style="margin-left:0; margin-bottom:20px;" class="labelText">Disable login:</label>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			
			<div class="form-group alignChkBox ">
						<input name="password[maxFailedLoginAttempts]" class="maxFailedLoginAttemptsChk" id="disableLogin" type="radio" value="0" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["maxFailedLoginAttempts"] == 0){echo "checked";}?>>
						<label for="disableLogin" class="passwordLabelClass"><span></span></label> 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">Never</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<input name="password[maxFailedLoginAttempts]" class="maxFailedLoginAttemptsChk" id="disableLogin1" type="radio" value="" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["maxFailedLoginAttempts"] > 0){echo "checked";}?>>
						<label style="padding-left: 35px;" for="disableLogin1" class="passwordLabelClass"><span></span></label> 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">After</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<div class="dropdown-wrap passwordLabelClassDD">
						<select name="password[maxFailedLoginAttempts]" id="disableLoginDays" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["maxFailedLoginAttempts"]==0){echo "disabled"; }?>>
						<?php 
            				$optTag = "";
            				for ($i=1; $i<11; $i++){
            				    $selectedVal = "";
            				    if($_SESSION["allResp"]["passwordRules"]["Success"]["maxFailedLoginAttempts"] == $i){
            				        $selectedVal = "selected";
            				    }
            				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
            				}
            				echo $optTag;
            			?>
						</select>
						</div>
						 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">failed login attempts</label>
				</div>
		</div>
		
	</div>
</div>
<div style="clear: right; margin:4px;">&nbsp;</div>
<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="password[sendLoginDisabledNotifyEmail]" type="hidden" value="false">
				<input name="password[sendLoginDisabledNotifyEmail]" id="sendLoginDisabledNotifyEmail" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passwordRules"]["Success"]["sendLoginDisabledNotifyEmail"] == "true"){echo "checked";}?>>
				<label for="sendLoginDisabledNotifyEmail"><span></span></label>
				<label class="labelText">When login is disabled, send e-mail to:</label>&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type="text" name="password[loginDisabledNotifyEmailAddress]" id="loginDisabledNotifyEmailAddress" style="width:210px !important;" value="<?php echo $_SESSION["allResp"]["passwordRules"]["Success"]["loginDisabledNotifyEmailAddress"]; ?>">
			</div>
		</div>
	</div>
</div>

<!-- Passcode Start -->
<?php if (array_key_exists("Voice Messaging Group", $entGroupServiceArray)) { ?>
<div style="clear: right;">&nbsp;</div>
<h2 class="adminUserText">Passcode Rules</h2>

<div style="clear: right;">&nbsp;</div>
<div class="row">
	<div class="">
		<div class="col-md-12">
			<label style="margin-left:0; margin-bottom:20px;" class="labelText">Passcode format:</label>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="passcode[disallowUserNumber]" type="hidden" value="false">
				<input name="passcode[disallowUserNumber]" id="disallowUserNumber" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["disallowUserNumber"] == "true"){echo "checked";}?>>
				<label for="disallowUserNumber"><span></span></label>
				<label class="labelText">cannot be the user's own extension or phone number</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="passcode[disallowReversedUserNumber]" type="hidden" value="false">
				<input name="passcode[disallowReversedUserNumber]" id="disallowReversedUserNumber" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["disallowReversedUserNumber"] == "true"){echo "checked";}?>>
				<label for="disallowReversedUserNumber"><span></span></label>
				<label class="labelText">cannot be the user's own extension or phone number reversed</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="passcode[disallowRepeatedDigits]" type="hidden" value="false">
				<input onchange="calculateSIPPasswordRules()" name="passcode[disallowRepeatedDigits]" id="disallowRepeatedDigits" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["disallowRepeatedDigits"] == "true"){echo "checked";}?>>
				<label for="disallowRepeatedDigits" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">cannot contain the same digit more than</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="passcode[numberOfRepeatedDigits]" id="numberOfRepeatedDigits">
					<?php 
            			$optTag = "";
            			for ($i=1; $i<7; $i++){
            			    $selectedVal = "";
            			    if($_SESSION["allResp"]["passcodeRules"]["Success"]["numberOfRepeatedDigits"] == $i){
            			        $selectedVal = "selected";
            			    }
            			    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
            			}
            			echo $optTag;
				    ?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">times in a row</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="passcode[disallowContiguousSequences]" type="hidden" value="false">
				<input name="passcode[disallowContiguousSequences]" id="disallowContiguousSequences" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["disallowContiguousSequences"] == "true"){echo "checked";}?>>
				<label for="disallowContiguousSequences" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">cannot contain more than</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="passcode[numberOfAscendingDigits]" id="numberOfAscendingDigits">
					<?php 
            			$optTag = "";
            			for ($i=2; $i<6; $i++){
            			    $selectedVal = "";
            			    if($_SESSION["allResp"]["passcodeRules"]["Success"]["numberOfAscendingDigits"] == $i){
            			        $selectedVal = "selected";
            			    }
            			    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
            			}
            			echo $optTag;
				    ?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;margin-right:20px;">sequentially ascending digits or</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="passcode[numberOfDescendingDigits]" id="numberOfDescendingDigits">
					<?php 
            			$optTag = "";
            			for ($i=2; $i<6; $i++){
            			    $selectedVal = "";
            			    if($_SESSION["allResp"]["passcodeRules"]["Success"]["numberOfDescendingDigits"] == $i){
            			        $selectedVal = "selected";
            			    }
            			    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
            			}
            			echo $optTag;
				    ?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">sequentially descending digits</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="passcode[disallowRepeatedPatterns]" type="hidden" value="false">
				<input name="passcode[disallowRepeatedPatterns]" id="disallowRepeatedPatterns" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["disallowRepeatedPatterns"] == "true"){echo "checked";}?>>
				<label for="disallowRepeatedPatterns"><span></span></label>
				<label class="labelText">cannot be repeating patterns</label>
			</div>
		</div>
	</div>
</div>

<!-- to be implemented -->
<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="passcode[disallowOldPasscode]" type="hidden" value="false">
				<input onchange="calculateSIPPasswordRules()" name="passcode[disallowOldPasscode]" id="disallowOldPasscodePassCode" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["disallowOldPasscode"] == "true"){echo "checked";}?>>
				<label for="disallowOldPasscodePassCode" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">cannot be any of the last</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="passcode[numberOfPreviousPasscodes]" id="numberOfPreviousPasscodes">
					<?php 
            			$optTag = "";
            			for ($i=1; $i<11; $i++){
            			    $selectedVal = "";
            			    if($_SESSION["allResp"]["passcodeRules"]["Success"]["numberOfPreviousPasscodes"] == $i){
            			        $selectedVal = "selected";
            			    }
            			    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
            			}
            			echo $optTag;
				    ?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">passcode(s)</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="passcode[disallowReversedOldPasscode]" type="hidden" value="false">
				<input name="passcode[disallowReversedOldPasscode]" id="disallowReversedOldPasscode" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["disallowReversedOldPasscode"] == "true"){echo "checked";}?>>
				<label for="disallowReversedOldPasscode"><span></span></label>
				<label class="labelText">cannot be the reversed old passcode</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				
				<input name="" id="minAndMaxRules" type="checkbox" value="true" disabled checked>
				<label for="allowAlternateNumbersForRedirectingIdentitySIP" style="float:left;opacity:0.6;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must be at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="passcode[minCodeLength]" id="minCodeLength">
					<?php 
        				$optTag = "";
        				for ($i=2; $i<16; $i++){
        				    $selectedVal = "";
        				    if($_SESSION["allResp"]["passcodeRules"]["Success"]["minCodeLength"] == $i){
        				        $selectedVal = "selected";
        				    }
        				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
        				}
        				echo $optTag; 
        			?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;margin-right:55px;">characters, no more than</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="passcode[maxCodeLength]" id="maxCodeLength">
					<?php
        				$optTag = "";
        				for ($i=3; $i<31; $i++){
        				    $selectedVal = "";
        				    if($_SESSION["allResp"]["passcodeRules"]["Success"]["maxCodeLength"] == $i){
        				        $selectedVal = "selected";
        				    }
        				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
        				}
        				echo $optTag; 
        			?>
				</select>
				</div>
			</div>
		</div>
	</div>
</div>

<div style="clear: right;">&nbsp;</div>
<div style="clear: right;">&nbsp;</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
				<label style="margin-left:0; margin-bottom:20px;" class="labelText">Passcodes expire:</label>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			
			<div class="form-group alignChkBox ">
						<input name="passcode[expirePassword]" id="passcodeExpiresDays1" class="passwordExpiresDaysChk1" type="radio" value="false" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["expirePassword"] == "false"){echo "checked";}?>>
						<label for="passcodeExpiresDays1"><span></span></label> <label class="labelText" style="margin-bottom:0;">Never</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<input name="passcode[expirePassword]" id="passcodeExpiresDays2" class="passwordExpiresDaysChk1" type="radio" value="true" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["expirePassword"] == "true"){echo "checked";}?>>
						<label style="padding-left: 35px;" for="passcodeExpiresDays2"><span></span></label> <label class="labelText" style="margin-bottom:0;">After</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<input type="hidden" name="passcode[passcodeExpiresDays]" value="<?php echo $_SESSION["allResp"]["passcodeRules"]["Success"]["passcodeExpiresDays"]; ?>">
						<input type="text" name="passcode[passcodeExpiresDays]" id="expirationDay1" style="width:100px !important;" size="3" maxlength="3" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["expirePassword"] == "false"){echo "disabled";}?> value="<?php echo $_SESSION["allResp"]["passcodeRules"]["Success"]["passcodeExpiresDays"]; ?>"> 
						<label class="labelText" style="margin-bottom:0;">Days</label>
				</div>
		</div>
		
	</div>
</div>

<div style="clear: right;">&nbsp;</div>
<div style="clear: right;">&nbsp;</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
				<label style="margin-left:0; margin-bottom:20px;" class="labelText">Disable login:</label>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			
			<div class="form-group alignChkBox ">
						<input name="passcode[disableLoginAfterMaxFailedLoginAttempts]" class="maxFailedLoginAttemptsChk1" id="disableLoginPasscode" type="radio" value="false" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["disableLoginAfterMaxFailedLoginAttempts"] == "false"){echo "checked";}?>>
						<label for="disableLoginPasscode" class="passwordLabelClass"><span></span></label> 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">Never</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<input name="passcode[disableLoginAfterMaxFailedLoginAttempts]" class="maxFailedLoginAttemptsChk1" id="disableLoginPasscode1" type="radio" value="true" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["disableLoginAfterMaxFailedLoginAttempts"] == "true"){echo "checked";}?>>
						<label style="padding-left: 45px;" for="disableLoginPasscode1" class="passwordLabelClass"><span></span></label> 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;margin-right:22px;">After</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<div class="dropdown-wrap passwordLabelClassDD">
						<input type="hidden" name="passcode[maxFailedLoginAttempts]" value="<?php echo $_SESSION["allResp"]["passcodeRules"]["Success"]["maxFailedLoginAttempts"]; ?>">
						<select name="passcode[maxFailedLoginAttempts]" id="disableLoginDays1" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["disableLoginAfterMaxFailedLoginAttempts"] == "false"){echo "disabled"; }?>>
						<?php 
            				$optTag = "";
            				for ($i=2; $i<11; $i++){
            				    $selectedVal = "";
            				    if($_SESSION["allResp"]["passcodeRules"]["Success"]["maxFailedLoginAttempts"] == $i){
            				        $selectedVal = "selected";
            				    }
            				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
            				}
            				echo $optTag;
            			?>
						</select>
						</div>
						 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">failed login attempts</label>
				</div>
		</div>
		
	</div>
</div>
<div style="clear: right; margin:4px;">&nbsp;</div>
<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="passcode[sendLoginDisabledNotifyEmail]" type="hidden" value="false">
				<input name="passcode[sendLoginDisabledNotifyEmail]" id="sendLoginDisabledNotifyEmailPasscode" type="checkbox" value="true" <?php if($_SESSION["allResp"]["passcodeRules"]["Success"]["sendLoginDisabledNotifyEmail"] == "true"){echo "checked";}?>>
				<label for="sendLoginDisabledNotifyEmailPasscode"><span></span></label>
				<label class="labelText">When login is disabled, send e-mail to:</label>&nbsp;&nbsp;&nbsp;&nbsp; 
				<input type="text" name="passcode[loginDisabledNotifyEmailAddress]" id="loginDisabledNotifyEmailAddress" style="width:210px !important;" value="<?php echo $_SESSION["allResp"]["passcodeRules"]["Success"]["loginDisabledNotifyEmailAddress"]; ?>">
			</div>
		</div>
	</div>
</div>

<?php } ?>
<!-- Passcode End -->


<h2 class="adminUserText">SIP Authentication Password Rules</h2>

<div style="clear: right;">&nbsp;</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
				<label style="margin-left:0; margin-bottom:20px;" class="labelText">Configuration:</label>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			
			<div class="form-group alignChkBox ">
						<input name="sipPassword[useServiceProviderSettings]" id="useServiceProviderSettings1" type="radio" value="true" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["useServiceProviderSettings"] == "true"){echo "checked"; }?>>
						<label for="useServiceProviderSettings1"><span></span></label> <label class="labelText" style="margin-bottom:0;">Use Service Provider/Enterprise Settings</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<input name="sipPassword[useServiceProviderSettings]" id="useServiceProviderSettings2" type="radio" value="false" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["useServiceProviderSettings"] == "false"){echo "checked"; }?>>
						<label style="padding-left: 35px;" for="useServiceProviderSettings2"><span></span></label> 
						<label class="labelText" style="margin-bottom:0;">Use System Settings</label> &nbsp;&nbsp;&nbsp;&nbsp;
						
				</div>
		</div>
		
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<label style="margin-left:0; margin-bottom:20px;" class="labelText">Password format:</label>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="sipPassword[disallowAuthenticationName]" type="hidden" value="false">
				<input name="sipPassword[disallowAuthenticationName]" id="disallowAuthenticationName" type="checkbox" value="true" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["disallowAuthenticationName"] == "true"){echo "checked"; }?>>
				<label for="disallowAuthenticationName"><span></span></label>
				<label class="labelText">cannot contain the authentication user name</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="sipPassword[disallowOldPassword]" type="hidden" value="false">
				<input name="sipPassword[disallowOldPassword]" id="disallowOldPasswordSIP" type="checkbox" value="true" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["disallowOldPassword"] == "true"){echo "checked"; }?>>
				<label for="disallowOldPasswordSIP"><span></span></label>
				<label class="labelText">cannot contain the old password</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="sipPassword[disallowReversedOldPassword]" type="hidden" value="false">
				<input name="sipPassword[disallowReversedOldPassword]" id="disallowReversedOldPassword1" type="checkbox" value="true" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["disallowReversedOldPassword"] == "true"){echo "checked"; }?>>
				<label for="disallowReversedOldPassword1"><span></span></label>
				<label class="labelText">cannot be the reverse of the old password</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="sipPassword[restrictMinDigits]" type="hidden" value="false">
				<input onchange="calculateSIPPasswordRules()" name="sipPassword[restrictMinDigits]" id="restrictMinDigits1" type="checkbox" value="true" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["restrictMinDigits"] == "true"){echo "checked"; }?>>
				<label for="restrictMinDigits1" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must contain at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="sipPassword[minDigits]" id="minDigits1" onchange="calculateSIPPasswordRules()">
					<?php 
            			$optTag = "";
            			for ($i=1; $i<11; $i++){
            			    $selectedVal = "";
            			    if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["minDigits"] == $i){
            			        $selectedVal = "selected";
            			    }
            			    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
            			}
            			echo $optTag;
				    ?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">number(s)</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="sipPassword[restrictMinUpperCaseLetters]" type="hidden" value="false">
				<input onchange="calculateSIPPasswordRules()" name="sipPassword[restrictMinUpperCaseLetters]" id="restrictMinUpperCaseLetters1" type="checkbox" value="true" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["restrictMinUpperCaseLetters"] == "true"){echo "checked"; }?>>
				<label for="restrictMinUpperCaseLetters1" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must contain at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="sipPassword[minUpperCaseLetters]" id="minUpperCaseLetters1" onchange="calculateSIPPasswordRules()">
					<?php 
            			$optTag = "";
            			for ($i=1; $i<11; $i++){
            			    $selectedVal = "";
            			    if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["minUpperCaseLetters"] == $i){
            			        $selectedVal = "selected";
            			    }
            			    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
            			}
            			echo $optTag;
				    ?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">uppercase alpha character(s)</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="sipPassword[restrictMinLowerCaseLetters]" type="hidden" value="false">
				<input onchange="calculateSIPPasswordRules()" name="sipPassword[restrictMinLowerCaseLetters]" id="restrictMinLowerCaseLetters1" type="checkbox" value="true" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["restrictMinLowerCaseLetters"] == "true"){echo "checked"; }?>>
				<label for="restrictMinLowerCaseLetters1" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must contain at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="sipPassword[minLowerCaseLetters]" id="minLowerCaseLetters1" onchange="calculateSIPPasswordRules()">
					<?php 
            			$optTag = "";
            			for ($i=1; $i<11; $i++){
            			    $selectedVal = "";
            			    if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["minLowerCaseLetters"] == $i){
            			        $selectedVal = "selected";
            			    }
            			    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
            			}
            			echo $optTag;
				    ?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">lowercase alpha character(s)</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="sipPassword[restrictMinNonAlphanumericCharacters]" type="hidden" value="false">
				<input onchange="calculateSIPPasswordRules()" name="sipPassword[restrictMinNonAlphanumericCharacters]" id="restrictMinNonAlphanumericCharacters1" type="checkbox" value="true" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["restrictMinNonAlphanumericCharacters"] == "true"){echo "checked"; }?>>
				<label for="restrictMinNonAlphanumericCharacters1" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must contain at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="sipPassword[minNonAlphanumericCharacters]" id="minNonAlphanumericCharacters1" onchange="calculateSIPPasswordRules()">
					<?php 
            			$optTag = "";
            			for ($i=1; $i<11; $i++){
            			    $selectedVal = "";
            			    if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["minNonAlphanumericCharacters"] == $i){
            			        $selectedVal = "selected";
            			    }
            			    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
            			}
            			echo $optTag;
				    ?>				
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">non-alphanumeric character(s)</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				
				<input name="cpPolicy12[allowAlternateNumbersForRedirectingIdentity]" id="allowAlternateNumbersForRedirectingIdentitySIP" type="checkbox" value="true" disabled checked>
				<label for="allowAlternateNumbersForRedirectingIdentitySIP" style="float:left;opacity:0.6;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must be at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="sipPassword[minLength]" id="minLength1">
					<?php 
        				$optTag = "";
        				for ($i=3; $i<41; $i++){
        				    $selectedVal = "";
        				    if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["minLength"] == $i){
        				        $selectedVal = "selected";
        				    }
        				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
        				}
        				echo $optTag;
        			?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">characters</label>
			</div>
		</div>
	</div>
</div>

<div style="clear: right;">&nbsp;</div>
<div style="clear: right;">&nbsp;</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
				<label style="margin-left:0; margin-bottom:20px;" class="labelText">Endpoint Lockout Settings:</label>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			
			<div class="form-group alignChkBox " style="margin:0;">
						<input class="endpointAuthenticationLockoutTypeRadio" name="sipPassword[endpointAuthenticationLockoutType]" id="endpointAuthenticationLockoutType1" type="radio" value="None" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointAuthenticationLockoutType"] == "None"){echo "checked"; }?>>
						<label for="endpointAuthenticationLockoutType1" style="margin-bottom:15px;"><span></span></label> <label class="labelText" style="margin-bottom:0;">Never lock out</label><br />
						<input class="endpointAuthenticationLockoutTypeRadio" name="sipPassword[endpointAuthenticationLockoutType]" id="endpointAuthenticationLockoutType2" type="radio" value="Temporary" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointAuthenticationLockoutType"] == "Temporary" || $_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointAuthenticationLockoutType"] == "Temporary Then Permanent"){echo "checked"; }?>>
						<label for="endpointAuthenticationLockoutType2" style="margin-bottom:15px;float:left;"><span></span></label> 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">Temporarily lock out after</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<div class="dropdown-wrap passwordLabelClassDD">
							<select name="sipPassword[endpointTemporaryLockoutThreshold]" id="endpointTemporaryLockoutThreshold" style="width:100px !important;">
								<?php 
                    				$optTag = "";
                    				for ($i=3; $i<11; $i++){
                    				    $selectedVal = "";
                    				    if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointTemporaryLockoutThreshold"] == $i){
                    				        $selectedVal = "selected";
                    				    }
                    				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
                    				}
                    				echo $optTag;
                    			?>						
							</select>
						</div>
						
						 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">authentication failures</label>
				</div>
		</div>
		
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox " style="margin-left:50px;">
						<input name="sipPassword[endpointWaitAlgorithm]" id="endpointWaitAlgorithm1" type="radio" value="Fixed" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointWaitAlgorithm"] == "Fixed"){echo "checked"; }?> <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointAuthenticationLockoutType"] == "None"){echo "disabled"; }?>>
						<label for="endpointWaitAlgorithm1" class="passwordLabelClass sipAuthOpacity" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointAuthenticationLockoutType"] == "None"){echo "style='opacity:0.6'"; }?>><span></span></label> 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">Wait</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<div class="dropdown-wrap passwordLabelClassDD">
						<select name="sipPassword[endpointLockoutFixedMinutes]" id="endpointLockoutFixedMinutes" style="width:100px !important;" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointAuthenticationLockoutType"] == "None"){echo "disabled"; }?>>
								<?php 
                    				$optTag = "";
                    				for ($i=1; $i<11; $i++){
                    				    $selectedVal = "";
                    				    if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointLockoutFixedMinutes"] == $i){
                    				        $selectedVal = "selected";
                    				    }
                    				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
                    				}
                    				echo $optTag;
                    			?>						
							</select>
						</div>
						
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">minutes before processing new authentication attempts</label> <br />
						
						<div style="clear: right; margin:10px;">&nbsp;</div>
						<input name="sipPassword[endpointWaitAlgorithm]" id="endpointWaitAlgorithm2" type="radio" value="Double" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointWaitAlgorithm"] == "Double"){echo "checked"; }?> <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointAuthenticationLockoutType"] == "None"){echo "disabled"; }?>>
						<label for="endpointWaitAlgorithm2" class="passwordLabelClass sipAuthOpacity" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointAuthenticationLockoutType"] == "None"){echo "style='opacity:0.6'"; }?>><span></span></label> 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">Double wait time (starting with 5 minutes) before processing new authentication attempts</label>
						
						<div style="clear: right;margin: 24px;">&nbsp;</div>
						<input name="sipPassword[endpointAuthenticationLockoutTypeCheckbox]" id="" type="hidden" value="false">
						<input name="sipPassword[endpointAuthenticationLockoutTypeCheckbox]" id="endpointAuthenticationLockoutTypeCheckbox" type="checkbox" value="true" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointAuthenticationLockoutType"] == "Temporary Then Permanent"){echo "checked";}else{echo "disabled";}?>>
						<label for="endpointAuthenticationLockoutTypeCheckbox" class="passwordLabelClass sipAuthOpacity" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointAuthenticationLockoutType"] == "None"){echo "style='opacity:0.6'"; }?>><span></span></label> 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">Permanently lock out after</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<div class="dropdown-wrap passwordLabelClassDD">
						<select name="sipPassword[endpointPermanentLockoutThreshold]" id="endpointPermanentLockoutThreshold" style="width:100px !important;" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointAuthenticationLockoutType"] == "None"){echo "disabled"; }?>>
							<?php 
                				$optTag = "";
                				for ($i=1; $i<11; $i++){
                				    $selectedVal = "";
                				    if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["endpointPermanentLockoutThreshold"] == $i){
                				        $selectedVal = "selected";
                				    }
                				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
                				}
                				echo $optTag;
                    		?>					
						</select>
						</div>
						
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">temporary lockouts</label>
				</div>
		</div>
	</div>
</div>

<div style="clear: right;">&nbsp;</div>
<div style="clear: right;">&nbsp;</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
				<label style="margin-left:0; margin-bottom:20px;" class="labelText">Trunk Group Lockout Settings:</label>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			
			<div class="form-group alignChkBox " style="margin:0;">
						<input class="trunkGroupAuthenticationLockoutTypeRadio" name="sipPassword[trunkGroupAuthenticationLockoutType]" id="trunkGroupAuthenticationLockoutType1" type="radio" value="None" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupAuthenticationLockoutType"] == "None"){echo "checked"; }?>>
						<label for="trunkGroupAuthenticationLockoutType1" style="margin-bottom:15px;"><span></span></label> <label class="labelText" style="margin-bottom:0;">Never lock out</label><br />
						<input class="trunkGroupAuthenticationLockoutTypeRadio" name="sipPassword[trunkGroupAuthenticationLockoutType]" id="trunkGroupAuthenticationLockoutType2" type="radio" value="Temporary" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupAuthenticationLockoutType"]=="Temporary" || $_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupAuthenticationLockoutType"]=="Temporary Then Permanent"){echo "checked"; }?>>
						<label for="trunkGroupAuthenticationLockoutType2" style="margin-bottom:15px;float:left;"><span></span></label> 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">Temporarily lock out after</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<div class="dropdown-wrap passwordLabelClassDD">
						<select name="sipPassword[trunkGroupTemporaryLockoutThreshold]" id="trunkGroupTemporaryLockoutThreshold" style="width:100px !important;">
							<?php 
                				$optTag = "";
                				for ($i=1; $i<11; $i++){
                				    $selectedVal = "";
                				    if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupTemporaryLockoutThreshold"] == $i){
                				        $selectedVal = "selected";
                				    }
                				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
                				}
                				echo $optTag;
                    		?>						
						</select>
						</div>
						 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">authentication failures</label>
				</div>
		</div>
		
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			
			<div class="form-group alignChkBox " style="margin-left:50px;">
						
						<input name="sipPassword[trunkGroupWaitAlgorithm]" id="trunkGroupWaitAlgorithm1" type="radio" value="Fixed" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupWaitAlgorithm"] == "Fixed"){echo "checked";} ?> <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupAuthenticationLockoutType"] == "None"){echo "disabled"; }?>>
						<label for="trunkGroupWaitAlgorithm1" class="passwordLabelClass trunkAuthAlgorithmRadio" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupAuthenticationLockoutType"] == "None"){echo "style='opacity: 0.6'"; }?>><span></span></label>
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">Wait</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<div class="dropdown-wrap passwordLabelClassDD">
						<select name="sipPassword[trunkGroupLockoutFixedMinutes]" id="trunkGroupLockoutFixedMinutes" style="width:100px !important;" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupAuthenticationLockoutType"] == "None"){echo "disabled"; }?>>
							<?php 
                				$optTag = "";
                				for ($i=1; $i<11; $i++){
                				    $selectedVal = "";
                				    if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupLockoutFixedMinutes"] == $i){
                				        $selectedVal = "selected";
                				    }
                				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
                				}
                				echo $optTag;
                    		?>					
						</select>
						</div>
						
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">minutes before processing new authentication attempts</label>
						<div style="clear: right; margin:10px;">&nbsp;</div>
						
						<input name="sipPassword[trunkGroupWaitAlgorithm]" id="trunkGroupWaitAlgorithm2" type="radio" value="Double" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupWaitAlgorithm"] == "Double"){echo "checked";} ?> <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupAuthenticationLockoutType"] == "None"){echo "disabled"; }?>>
						<label for="trunkGroupWaitAlgorithm2" class="passwordLabelClass trunkAuthAlgorithmRadio" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupAuthenticationLockoutType"] == "None"){echo "style='opacity: 0.6'"; }?>><span></span></label> 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">Double wait time (starting with 5 minutes) before processing new authentication attempts</label>
						<div style="clear: right;margin: 24px;">&nbsp;</div>
						
						<input name="sipPassword[trunkGroupAuthenticationLockoutTypeCheckbox]" id="trunkGroupAuthenticationLockoutTypeCheckbox" type="checkbox" value="true" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupAuthenticationLockoutType"] == "Temporary Then Permanent"){echo "checked";}else{echo "disabled";}?>>
						<label for="trunkGroupAuthenticationLockoutTypeCheckbox" class="passwordLabelClass trunkAuthAlgorithmRadio" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupAuthenticationLockoutType"] == "None"){echo "style='opacity: 0.6'"; }?>><span></span></label> 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">Permanently lock out after</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<div class="dropdown-wrap passwordLabelClassDD">
						<select name="sipPassword[trunkGroupPermanentLockoutThreshold]" id="trunkGroupPermanentLockoutThreshold" style="width:100px !important;" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupAuthenticationLockoutType"] == "None"){echo "disabled"; }?>>
							<?php 
                				$optTag = "";
                				for ($i=1; $i<11; $i++){
                				    $selectedVal = "";
                				    if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["trunkGroupPermanentLockoutThreshold"] == $i){
                				        $selectedVal = "selected";
                				    }
                				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
                				}
                				echo $optTag;
                    		?>						
						</select>
						</div>
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">temporary lockouts</label>
				</div>
		</div>
		
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox">
				<input name="sipPassword[sendPermanentLockoutNotification]" type="hidden" value="false">
				<input name="sipPassword[sendPermanentLockoutNotification]" id="sendPermanentLockoutNotification" type="checkbox" value="true" <?php if($_SESSION["allResp"]["sipPasswordRules"]["Success"]["sendPermanentLockoutNotification"] == "true"){echo "checked";} ?>>
				<label for="sendPermanentLockoutNotification"><span></span></label>
				<label class="labelText">When authentication is permanently locked out, send email to:</label>&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="sipPassword[permanentLockoutNotifyEmailAddress]" id="permanentLockoutNotifyEmailAddress" style="width:210px !important;" value="<?php echo $_SESSION["allResp"]["sipPasswordRules"]["Success"]["permanentLockoutNotifyEmailAddress"]; ?>">
			</div>
		</div>
	</div>
</div>

<h2 class="adminUserText">Device Profile Authentication Password Rules</h2>

<div style="clear: right;">&nbsp;</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
				<label style="margin-left:0; margin-bottom:20px;" class="labelText">Configuration:</label>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox ">
				<input name="devicePasswordRules[useServiceProviderSettings]" id="useServiceProviderSettingsDevice1" type="radio" value="true" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["useServiceProviderSettings"] == "true"){echo "checked"; }?>>
				<label for="useServiceProviderSettingsDevice1"><span></span></label> <label class="labelText" style="margin-bottom:0;">Use Service Provider/Enterprise Settings</label> &nbsp;&nbsp;&nbsp;&nbsp;
				<input name="devicePasswordRules[useServiceProviderSettings]" id="useServiceProviderSettingsDevice2" type="radio" value="false" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["useServiceProviderSettings"] == "false"){echo "checked"; }?>>
				<label style="padding-left: 35px;" for="useServiceProviderSettingsDevice2"><span></span></label> <label class="labelText" style="margin-bottom:0;">Use System Settings</label> &nbsp;&nbsp;&nbsp;&nbsp;
			</div>
		</div>
		
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
				<label style="margin-left:0; margin-bottom:20px;" class="labelText">Password format:</label>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="devicePasswordRules[disallowAuthenticationName]" type="hidden" value="false">
				<input name="devicePasswordRules[disallowAuthenticationName]" id="disallowAuthenticationNameDevice" type="checkbox" value="true" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["disallowAuthenticationName"] == "true"){echo "checked"; }?>>
				<label for="disallowAuthenticationNameDevice"><span></span></label>
				<label class="labelText">cannot contain the authentication user name</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="devicePasswordRules[disallowOldPassword]" type="hidden" value="false">
				<input name="devicePasswordRules[disallowOldPassword]" id="disallowOldPasswordDevice" type="checkbox" value="true" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["disallowOldPassword"] == "true"){echo "checked"; }?>>
				<label for="disallowOldPasswordDevice"><span></span></label>
				<label class="labelText">cannot contain the old password</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
				<input name="devicePasswordRules[disallowReversedOldPassword]" type="hidden" value="false">
				<input name="devicePasswordRules[disallowReversedOldPassword]" id="disallowReversedOldPasswordDevice" type="checkbox" value="true" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["disallowReversedOldPassword"] == "true"){echo "checked"; }?>>
				<label for="disallowReversedOldPasswordDevice"><span></span></label>
				<label class="labelText">cannot be the reverse of the old password</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="devicePasswordRules[restrictMinDigits]" type="hidden" value="false">
				<input onchange="calculateDevicePasswordRules()" name="devicePasswordRules[restrictMinDigits]" id="allowAlternateNumbersForRedirectingIdentityD" type="checkbox" value="true" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["restrictMinDigits"] == "true"){echo "checked"; }?>>
				<label for="allowAlternateNumbersForRedirectingIdentityD" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must contain at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="devicePasswordRules[minDigits]" id="minDigitsDevice" onchange="calculateDevicePasswordRules()">
					<?php 
        				$optTag = "";
        				for ($i=1; $i<11; $i++){
        				    $selectedVal = "";
        				    if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["minDigits"] == $i){
        				        $selectedVal = "selected";
        				    }
        				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
        				}
        				echo $optTag;
            		?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">number(s)</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="devicePasswordRules[restrictMinUpperCaseLetters]" type="hidden" value="false">
				<input onchange="calculateDevicePasswordRules()" name="devicePasswordRules[restrictMinUpperCaseLetters]" id="restrictMinUpperCaseLettersDevice" type="checkbox" value="true" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["restrictMinUpperCaseLetters"] == "true"){echo "checked"; }?>>
				<label for="restrictMinUpperCaseLettersDevice" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must contain at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="devicePasswordRules[minUpperCaseLetters]" id="minUpperCaseLettersDevice" onchange="calculateDevicePasswordRules()">
					<?php 
        				$optTag = "";
        				for ($i=1; $i<11; $i++){
        				    $selectedVal = "";
        				    if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["minUpperCaseLetters"] == $i){
        				        $selectedVal = "selected";
        				    }
        				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
        				}
        				echo $optTag;
            		?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">uppercase alpha character(s)</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="devicePasswordRules[restrictMinLowerCaseLetters]" type="hidden" value="false">
				<input onchange="calculateDevicePasswordRules()" name="devicePasswordRules[restrictMinLowerCaseLetters]" id="restrictMinLowerCaseLettersDevice" type="checkbox" value="true" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["restrictMinLowerCaseLetters"] == "true"){echo "checked"; }?>>
				<label for="restrictMinLowerCaseLettersDevice" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must contain at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="devicePasswordRules[minLowerCaseLetters]" id="minLowerCaseLettersDevice" onchange="calculateDevicePasswordRules()">
					<?php 
        				$optTag = "";
        				for ($i=1; $i<11; $i++){
        				    $selectedVal = "";
        				    if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["minLowerCaseLetters"] == $i){
        				        $selectedVal = "selected";
        				    }
        				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
        				}
        				echo $optTag;
            		?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">lowercase alpha character(s)</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				<input name="devicePasswordRules[restrictMinNonAlphanumericCharacters]" type="hidden" value="false">
				<input onchange="calculateDevicePasswordRules()" name="devicePasswordRules[restrictMinNonAlphanumericCharacters]" id="restrictMinNonAlphanumericCharactersDevice" type="checkbox" value="true" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["restrictMinNonAlphanumericCharacters"] == "true"){echo "checked"; }?>>
				<label for="restrictMinNonAlphanumericCharactersDevice" style="float:left;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must contain at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="devicePasswordRules[minNonAlphanumericCharacters]" id="minNonAlphanumericCharactersDevice" onchange="calculateDevicePasswordRules()">
					<?php 
        				$optTag = "";
        				for ($i=1; $i<11; $i++){
        				    $selectedVal = "";
        				    if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["minNonAlphanumericCharacters"] == $i){
        				        $selectedVal = "selected";
        				    }
        				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
        				}
        				echo $optTag;
            		?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">non-alphanumeric character(s)</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:32px;">
				
				<input name="devicePasswordRules[deviceMinLength]" id="deviceMinLength1" type="checkbox" value="true" checked disabled>
				<label for="deviceMinLength1" style="float:left;opacity:0.6;"><span></span></label>
				<label class="labelText" style="width:250px;float:left;">must be at least</label>&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:50px !important;float:left;margin-top:-4px;">
				<select name="devicePasswordRules[minLength]" id="minLengthDevice">
					<?php 
        				$optTag = "";
        				for ($i=3; $i<41; $i++){
        				    $selectedVal = "";
        				    if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["minLength"] == $i){
        				        $selectedVal = "selected";
        				    }
        				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
        				}
        				echo $optTag;
            		?>
				</select>
				</div>&nbsp;&nbsp;
				<label class="labelText" style="float:left;">characters</label>
			</div>
		</div>
	</div>
</div>

<div style="clear: right;">&nbsp;</div>
<div style="clear: right;">&nbsp;</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
				<label style="margin-left:0; margin-bottom:20px;" class="labelText">Device Profile Lockout Settings:</label>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
            <div class="col-md-4" style="width:32.7% !important;">
                <label style="margin-left:0; margin-bottom:20px;" class="labelText">Authentication Lockout:</label>
            </div>
		<div class="col-md-8">
			<div class="form-group alignChkBox " style="margin:0;">
				<input class="deviceTypeAuthLockoutTypeRadio" name="devicePasswordRules[deviceProfileAuthenticationLockoutType]" id="deviceProfileAuthenticationLockoutTypedevice1" type="radio" value="None" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileAuthenticationLockoutType"] == "None"){echo "checked"; }?>>
				<label for="deviceProfileAuthenticationLockoutTypedevice1" style="margin-bottom:15px;"><span></span></label> <label class="labelText" style="margin-bottom:0;">Never lock out</label><br />
				<input class="deviceTypeAuthLockoutTypeRadio" name="devicePasswordRules[deviceProfileAuthenticationLockoutType]" id="deviceProfileAuthenticationLockoutTypedevice2" type="radio" value="Temporary" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileAuthenticationLockoutType"] == "Temporary" || $_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileAuthenticationLockoutType"] == "Temporary Then Permanent"){echo "checked"; }?>>
				<label for="deviceProfileAuthenticationLockoutTypedevice2" style="margin-bottom:15px;float:left;"><span></span></label> <label class="labelText" style="margin:10px 7px 0px 14px;float:left;">Temporarily lock out after</label> &nbsp;&nbsp;&nbsp;&nbsp;
				<div class="dropdown-wrap" style="width:100px;float:left;margin-top:5px;">
				<select name="devicePasswordRules[deviceProfileTemporaryLockoutThreshold]" id="devicePasswordRulesDevice" style="width:100px !important;">
					<?php 
        				$optTag = "";
        				for ($i=1; $i<11; $i++){
        				    $selectedVal = "";
        				    if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileTemporaryLockoutThreshold"] == $i){
        				        $selectedVal = "selected";
        				    }
        				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
        				}
        				echo $optTag;
            		?>				
				</select>
				</div>
				 
				<label class="labelText" style="margin:10px 7px 0px 14px;float:left;">authentication failures</label>
			</div>
		</div>
		
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12" style="margin-left: 280px;">
			
			<div class="form-group alignChkBox " style="margin-left:50px;">
						
						<input name="devicePasswordRules[deviceProfileWaitAlgorithm]" id="deviceProfileWaitAlgorithmDevice1" type="radio" value="Fixed" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileWaitAlgorithm"] == "Fixed"){echo "checked"; }?> <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileAuthenticationLockoutType"] == "None"){echo "disabled"; }?>>
						<label for="deviceProfileWaitAlgorithmDevice1" class="passwordLabelClass deviceAlgorithmRadio" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileAuthenticationLockoutType"] == "None"){echo "style='opacity: 0.6'"; }?>><span></span></label> 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">Wait</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<div class="dropdown-wrap passwordLabelClassDD">
							<select name="devicePasswordRules[deviceProfileLockoutFixedMinutes]" id="deviceProfileLockoutFixedMinutesDevice" style="width:100px !important;" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileAuthenticationLockoutType"] == "None"){echo "disabled"; }?>>
								<?php 
								    $optionArray = array("5", "10", "20", "40", "60");
                    				$optTag = "";
                    				foreach ($optionArray as $optKey => $optVal){
                    				    $selectedVal = "";
                    				    if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileLockoutFixedMinutes"] == $optVal){
                    				        $selectedVal = "selected";
                    				    }
                    				    $optTag .= "<option value='".$optVal."' ".$selectedVal.">".$optVal."</option>";
                    				}
                    				//for ($i=3; $i<11; $i++){
                    				    
                    				//}
                    				echo $optTag;
                        		?>							
							</select>
						</div>
						
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">minutes before processing new authentication attempts</label> 
						<div style="clear: right; margin:10px;">&nbsp;</div>
						
						<input name="devicePasswordRules[deviceProfileWaitAlgorithm]" id="deviceProfileWaitAlgorithmDevice2" type="radio" value="Double" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileWaitAlgorithm"] == "Double"){echo "checked"; }?> <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileAuthenticationLockoutType"] == "None"){echo "disabled"; }?>>
						<label for="deviceProfileWaitAlgorithmDevice2" class="passwordLabelClass deviceAlgorithmRadio" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileAuthenticationLockoutType"] == "None"){echo "style='opacity: 0.6'"; }?>><span></span></label>
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">Double wait time (starting with 5 minutes) before processing new authentication attempts</label>
						<div style="clear: right; margin:24px;">&nbsp;</div>
						<input name="devicePasswordRules[deviceProfileAuthenticationLockoutTypeChk]" id="" type="hidden" value="false">
						<input name="devicePasswordRules[deviceProfileAuthenticationLockoutTypeChk]" id="deviceProfileAuthenticationLockoutTypeChk" type="checkbox" value="true" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileAuthenticationLockoutType"] == "Temporary Then Permanent"){echo "checked";}else{echo "disabled"; }?>>
						<label for="deviceProfileAuthenticationLockoutTypeChk" class="passwordLabelClass deviceAlgorithmRadio" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileAuthenticationLockoutType"] == "None"){echo "style='opacity: 0.6'"; }?>><span></span></label> 
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">Permanently lock out after</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<div class="passwordLabelClassDD dropdown-wrap">
							<select name="devicePasswordRules[deviceProfilePermanentLockoutThreshold]" id="deviceProfilePermanentLockoutThresholdDevice" style="width:100px !important;" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfileAuthenticationLockoutType"] == "None"){echo "disabled"; }?>>
								<?php 
                    				$optTag = "";
                    				for ($i=2; $i<11; $i++){
                    				    $selectedVal = "";
                    				    if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["deviceProfilePermanentLockoutThreshold"] == $i){
                    				        $selectedVal = "selected";
                    				    }
                    				    $optTag .= "<option value='".$i."' ".$selectedVal.">".$i."</option>";
                    				}
                    				echo $optTag;
                        		?>						
							</select>
						</div>
						
						<label class="labelText passwordLabelClassText" style="margin-bottom:0;">temporary lockouts</label>
				</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12" style="margin-left: 280px;">
			<div class="form-group alignChkBox">
				<input name="devicePasswordRules[sendPermanentLockoutNotification]" type="hidden" value="false">
				<input name="devicePasswordRules[sendPermanentLockoutNotification]" id="sendPermanentLockoutNotificationDevice" type="checkbox" value="true" <?php if($_SESSION["allResp"]["devicePasswordRules"]["Success"]["sendPermanentLockoutNotification"] == "true"){echo "checked";} ?>>
				<label for="sendPermanentLockoutNotificationDevice"><span></span></label>
				<label class="labelText">When authentication is permanently locked out, send email to:</label>&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="devicePasswordRules[permanentLockoutNotifyEmailAddress]" id="permanentLockoutNotifyEmailAddress" style="width:210px !important;" value="<?php echo $_SESSION["allResp"]["devicePasswordRules"]["Success"]["permanentLockoutNotifyEmailAddress"]; ?>">
			</div>
		</div>
	</div>
</div>

<div style="clear: right;">&nbsp;</div>
<div style="clear: right;">&nbsp;</div>
<div style="clear: right;">&nbsp;</div>
<div style="clear: right;">&nbsp;</div>