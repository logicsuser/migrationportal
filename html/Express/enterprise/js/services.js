$(function() {
	$(".allServiceTable").hide();
	var init = function() {
		getServices();
	}
	init();
	
	$("#expCollapse").click(function(){
		var extVal = $(this).val();
		
		if(extVal == "Expand"){
			$(this).val("Collapse");
			//$("#groupServiceTableBody").css({"height": "auto !important", "max-height": "inherit !important"});
			$("#serviceFilterTitleName").text("Collapse");
			$("#enterpriseServicesFiltersStatusTitleIcon").removeClass("glyphicon-chevron-up");
			$("#enterpriseServicesFiltersStatusTitleIcon").addClass("glyphicon-chevron-down");
			$("#groupServiceTableBody").css("max-height", "5550px");
		}else{
			$(this).val("Expand");
			$("#serviceFilterTitleName").text("Expand");
			$("#enterpriseServicesFiltersStatusTitleIcon").removeClass("glyphicon-chevron-down");
			$("#enterpriseServicesFiltersStatusTitleIcon").addClass("glyphicon-chevron-up");
			$("#groupServiceTableBody").css("max-height", "220px");
		}
	});
	
	$("#expCollapse1").click(function(){
		var extVal = $(this).val();
		
		if(extVal == "Expand"){
			$(this).val("Collapse");
			//$("#groupServiceTableBody").css({"height": "auto !important", "max-height": "inherit !important"});
			$("#serviceFilterTitleName1").text("Collapse");
			$("#enterpriseServicesFiltersStatusTitleIcon1").removeClass("glyphicon-chevron-up");
			$("#enterpriseServicesFiltersStatusTitleIcon1").addClass("glyphicon-chevron-down");
			$("#groupServiceTableBody1").css("max-height", "20000px");
		}else{
			$(this).val("Expand");
			$("#serviceFilterTitleName1").text("Expand");
			$("#enterpriseServicesFiltersStatusTitleIcon1").removeClass("glyphicon-chevron-down");
			$("#enterpriseServicesFiltersStatusTitleIcon1").addClass("glyphicon-chevron-up");
			$("#groupServiceTableBody1").css("max-height", "220px");
		}
	});
	
	$(document).on("click", ".groupServiceLimitCheckBox, .userServiceLimitCheckBox", function() {
		var nameValue = $(this).attr('name');
		var mySubString = nameValue.substring(
				nameValue.lastIndexOf("[") + 1, 
				nameValue.lastIndexOf("]")
		);
		var isChecked = $(this).prop("checked");
		if(startsWith(nameValue,"groupServicePackLimit")) {
			var isAuthChecked = $("input[name = 'groupServicePackAuth[" + mySubString+ "]']").prop("checked");
			var groupServicePackQnt = $("input[name = 'groupServicePackQnt[" + mySubString+ "]']");
			
			groupServicePackQnt.prop("disabled", !(isChecked && isAuthChecked) ).val("");
			if( ! (isChecked && isAuthChecked)) {
				groupServicePackQnt.addClass("disableService");
			} else {
				groupServicePackQnt.removeClass("disableService");
			}
		} else if(startsWith(nameValue,"userServicePackLimit")) {
			var isAuthChecked = $("input[name = 'userServicePackAuth[" + mySubString+ "]']").prop("checked");
			var userServicePackQnt = $("input[name = 'userServicePackQnt[" + mySubString+ "]']");
			
			userServicePackQnt.prop("disabled", !(isChecked && isAuthChecked) ).val("");
			if( ! (isChecked && isAuthChecked)) {
				userServicePackQnt.addClass("disableService");
			} else {
				userServicePackQnt.removeClass("disableService");
			}
		}
		
	});
	
	$(document).on("change", "#checkAllGroupService", function() {
		var isChecked = $(this).prop("checked");
		$("input[name^='groupServicePackAuth']:not([disabled])").prop("checked", isChecked);
		
		$("#enterpriseGroupServiceTable").filter(function(){
			if( ! $(this).find("input[name^='groupServicePackAuth']").prop("disabled")) {
				if( isChecked == false) {
					$(this).find(".groupServiceLimitCheckBox").prop("checked", false).prop("disabled", true);
					$(this).find(".groupServiceLimitCheckBox").next("label").addClass("disableService");
					$(this).find("input[name^='groupServicePackQnt']").prop("disabled", true).val("").addClass("disableService");
				} else {
					$(this).find(".groupServiceLimitCheckBox").prop("disabled", false);
					$(this).find(".groupServiceLimitCheckBox").next("label").removeClass("disableService");
				}
			}
		});
				
	});
	
	$(document).on("change", "input[name^='groupServicePackAuth'], input[name^='userServicePackAuth']", function() {
			var isChecked = $(this).prop("checked");
			var nameValue = $(this).attr('name');
			var mySubString = nameValue.substring(
					nameValue.lastIndexOf("[") + 1, 
					nameValue.lastIndexOf("]")
			);
		
			if(startsWith(nameValue, "groupServicePackAuth")) {
				var groupServicePackLimit = $("input[name = 'groupServicePackLimit[" + mySubString+ "]']");
				
				if( isChecked == false) {
					groupServicePackLimit.prop("checked", false).prop("disabled", true);
					groupServicePackLimit.next("label").addClass("disableService");
					$("input[name = 'groupServicePackQnt[" + mySubString+ "]']").prop("disabled", true).val("").addClass("disableService");
				} else {
					groupServicePackLimit.prop("disabled", false);
					groupServicePackLimit.next("label").removeClass("disableService");
				}

			} else if(startsWith(nameValue, "userServicePackAuth")) {
				var userServicePackLimit = $("input[name = 'userServicePackLimit[" + mySubString+ "]']");
				
				if( isChecked == false) {
					userServicePackLimit.prop("checked", false).prop("disabled", true);
					userServicePackLimit.next("label").addClass("disableService");
					$("input[name = 'userServicePackQnt[" + mySubString+ "]']").prop("disabled", true).val("").addClass("disableService");
				} else {
					userServicePackLimit.prop("disabled", false);
					userServicePackLimit.next("label").removeClass("disableService");
				}
				
			}
		
		var totalGrpAuthCheckBox = $("input[name^='groupServicePackAuth']").length;
		var totalCheckedGrpAuthCheckBox = $("input[name^='groupServicePackAuth']:checkbox:checked").length;
		if(totalGrpAuthCheckBox == totalCheckedGrpAuthCheckBox) {
			$("#checkAllGroupService").prop("checked", true);
		} else {
			$("#checkAllGroupService").prop("checked", false);
		}
	
	});
	
	$(document).on("change", "#checkAllUserService", function() {
		var isChecked = $(this).prop("checked");
		$("input[name^='userServicePackAuth']:not([disabled])").prop("checked", isChecked);
		
		$("#enterpriseUserServiceTable").filter(function(){
			if( ! $(this).find("input[name^='userServicePackAuth']").prop("disabled")) {
				if( isChecked == false) {
					$(this).find(".userServiceLimitCheckBox").prop("checked", false).prop("disabled", true);
					$(this).find(".userServiceLimitCheckBox").next("label").addClass("disableService");
					$(this).find("input[name^='userServicePackQnt']").prop("disabled", true).val("").addClass("disableService");
				} else {
					$(this).find(".userServiceLimitCheckBox").prop("disabled", false);
					$(this).find(".userServiceLimitCheckBox").next("label").removeClass("disableService");
				}
			}
			
		});
		
	});
	
	$(document).on("change", "input[name^='userServicePackAuth']", function() {
		var totalUserAuthCheckBox = $("input[name^='userServicePackAuth']").length;
		var totalCheckedUserAuthCheckBox = $("input[name^='userServicePackAuth']:checkbox:checked").length;
		if(totalUserAuthCheckBox == totalCheckedUserAuthCheckBox) {
			$("#checkAllUserService").prop("checked", true);
		} else {
			$("#checkAllUserService").prop("checked", false);
		}
	
	});
	
	$(".groupService_length, .userService_length").change(function() {
		
		var thisClass = $(this).attr("class");
		var currentTable;
		var showTotalRow = $(this).val();
		if( thisClass == "groupService_length") {
			currentTable = "enterpriseGroupServiceTable";
		} else if( thisClass == "userService_length") {
			currentTable = "enterpriseUserServiceTable";
		}
		
		if(showTotalRow == "All Services") {
			$("#" + currentTable + " tbody tr").toggle(true);
		} else {
			var count = 0;
			$("#" + currentTable + " tbody tr").filter(function() {
				$(this).toggle( count < showTotalRow );
				count++;
			});	
		}

	});
	
	
	$(document).on("click","#enterpriseUserServiceTable tbody tr", function(event){
		var target = event.target.tagName;
		
		if(target == "INPUT" || target == "SPAN") {
				return;
		}
		
		$("#servicePackTableDiv").html("");
		$("#servicePackGetServiceUsage").dialog("open");
		$("#servicePackGetServiceLoading").show();
		var selectedSp = $("#hiddenEnterpriseName").val();
		var serviceName = $(this).find(".userServiceLink").attr("data-servicename");
		$.ajax({
			type: "POST",
			url: "enterprise/services/services.php",
			data: {action : "getServicePackGetServiceUsage", selectedSp: selectedSp, serviceName: serviceName},
			success: function(result) {
				var obj = jQuery.parseJSON(result);
				$("#servicePackTableDiv").html(obj);
				$("#servicePackGetServiceLoading").hide();
				$("#servicePackGetServiceUsageTable").tablesorter();				
			}
			
		});
		
	});
	

	$("#servicePackGetServiceUsage").dialog($.extend({}, defaults, {
		width: 800,
		open: function(event) {
				setDialogDayNightMode($(this));
				$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
			},
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		buttons: {
			"Cancel": function()
			{
				$(this).dialog("close");
//				$("#servicePackGetServiceUsage").dialog("open");
			},
		}
	}));
	
});


function createInputFieldDataString(thisId) {
	var tempData = "";
	thisId.each(function() {
			var nameValue = $(this).attr('name');
			var mySubString = nameValue.substring(
					nameValue.lastIndexOf("[") + 1, 
					nameValue.lastIndexOf("]")
			);
			var value = $(this).val();			
			tempData += mySubString + "::" + value + ";;";
	});
	//console.log(tempData);
	return tempData;
}


function createCheckBoxDataString(thisId) {
	var tempData = "";
	thisId.each(function() {
			var nameValue = $(this).attr('name');
			var mySubString = nameValue.substring(
					nameValue.lastIndexOf("[") + 1, 
					nameValue.lastIndexOf("]")
			);
			var isChecked = $(this).prop("checked") ? "true" : "false";
			tempData += mySubString + "::" + isChecked + ";;";
	});
	//console.log(tempData);
	return tempData;
}


function makeServicePostData(dataToSend) {
		/* Group Service from data */
		var response = {};
		response.groupServicePackAuth = createCheckBoxDataString($("input[name^='groupServicePackAuth']"));
		response.groupServicePackLimit = createCheckBoxDataString($("input[name^='groupServicePackLimit']"));
		response.groupServicePackQnt = createInputFieldDataString($("input[name^='groupServicePackQnt']"));

		/* User Service from data */
		response.userServicePackAuth = createCheckBoxDataString($("input[name^='userServicePackAuth']"));
		response.userServicePackLimit = createCheckBoxDataString($("input[name^='userServicePackLimit']"));
		response.userServicePackQnt = createInputFieldDataString($("input[name^='userServicePackQnt']"));
		
		dataToSend.push({"name": "groupServicePackAuth", "value" : response.groupServicePackAuth},
				{"name": "groupServicePackLimit", "value" : response.groupServicePackLimit},
				{"name": "groupServicePackQnt", "value" : response.groupServicePackQnt},
				{"name": "userServicePackAuth", "value" : response.userServicePackAuth},
				{"name": "userServicePackLimit", "value" : response.userServicePackLimit},
				{"name": "userServicePackQnt", "value" : response.userServicePackQnt}
		);
		
		return dataToSend;
}


function getServices() {
		var selectedSp = $("#hiddenEnterpriseName").val();
		$.ajax({
			type: "POST",
			url: "enterprise/services/services.php",
			data: {action : "getServices", selectedSp: selectedSp},
			success: function(result) {
				
				var obj = jQuery.parseJSON(result);
				$("#enterpriseGroupServiceTable tbody").html(obj.groupServiceTable);
				$("#enterpriseUserServiceTable tbody").html(obj.userServiceTable);
				$("#loadingServices").hide();
				$(".allServiceTable").show();
				$("#enterpriseGroupServiceTable").tablesorter();
				$("#enterpriseUserServiceTable").tablesorter();
				
			}
			
		});
	}

function startsWith(str, word) {
    return str.lastIndexOf(word, 0) === 0;
}