$(function()
{
	
	$('#downloadGroupServiceCSV, #downloadUserServiceCSV').click(function() {
		
		var clickedId = $(this).attr('id');
		var table = "";
		var serviceFileName = "";
		if(clickedId == "downloadGroupServiceCSV") {
			table = "enterpriseGroupServiceTable";
			serviceFileName = "EnterpriseGroupServices.csv"
		} else if(clickedId == "downloadUserServiceCSV") {
			table = "enterpriseUserServiceTable";
			serviceFileName = "EnterpriseUserServices.csv"
		}
        var titles = [];
        var data = [];
        $('#' + table + ' th').each(function() {    //table id here
          titles.push($(this).text());
        });

     
        $('#' + table + ' td:visible').each(function() {    //table id here
        	var text = "";
        	if( $(this).find(".groupServicePack").length > 0 || $(this).find(".userServicePack").length > 0) {
        		text = $(this).find("input[type='checkbox']").prop("checked") ? "True" : "False";
        	}
        	else if( $(this).find(".quantity").length > 0 ) {
        		var quantity = $(this).find(".quantity").val();
        		if( quantity != "" ) {
        			text = quantity;
        		} else {
        			var limitClass = (table == "enterpriseGroupServiceTable") ? "groupServiceLimitCheckBox" : "userServiceLimitCheckBox";
        			text = $(this).find(limitClass).prop("checked") ? "Limited" : "Unlimited";
        		}
        	} else {
        		text = $(this).text();        		
        	}
        	
        	data.push(text);
        	
        });
       
        var CSVString = prepCSVRow(titles, titles.length, '');
        CSVString = prepCSVRow(data, titles.length, CSVString);

        var blob = new Blob(["\ufeff", CSVString]);
		  if (navigator.msSaveBlob) { // IE 10+
	        	navigator.msSaveBlob(blob, serviceFileName);
	      }else{
	    	  var downloadLink = document.createElement("a");
			  
			  var url = URL.createObjectURL(blob);
			  downloadLink.href = url;
			  downloadLink.download = serviceFileName;

			  
			  document.body.appendChild(downloadLink);
			  downloadLink.click();
			  document.body.removeChild(downloadLink);
	      }
        
      });
 
      
});
function prepCSVRow(arr, columnCount, initial) {
    var row = '';
    var delimeter = ',';
    var newLine = '\r\n';

    function splitArray(_arr, _count) {
      var splitted = [];
      var result = [];
      _arr.forEach(function(item, idx) {
        if ((idx + 1) % _count === 0) {
          splitted.push(item);
          result.push(splitted);
          splitted = [];
        } else {
          splitted.push(item);
        }
      });
      return result;
    }
    var plainArr = splitArray(arr, columnCount);
   
    plainArr.forEach(function(arrItem) {
      arrItem.forEach(function(item, idx) {
        row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
      });
      row += newLine;
    });
    return initial + row;
  }