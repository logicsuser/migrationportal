var dialPlanPolicyPos = "";
	var isModify = "";
	var domainPos = "";
	var securtyDomainFlag = "";
var isCheckBoxIsClicked = false;
function checkBoxDelBtnShowNavo(status) {
	isCheckBoxIsClicked = status;
}
$(document).on("click", "tr#dialPlanPolicyTableRow", function(event) {
		dialPlanPolicyPos = "Update";
		$("#dialPlanPolicyDialog").html("");
		//$("#selectSoftPhoneDeviceType").prop("disabled", false);
		//$("#softPhoneDeviceTypeLinePort").prop("disabled", false);
		if (!isCheckBoxIsClicked) {	
		//if (event.target.type !== 'checkbox') {
			var accessCode  				= $(this).closest('tr#dialPlanPolicyTableRow').find('.dialPlanPolicyDelCheckBox').data("accesscode");
			var enableSecondaryDialTone  			= $(this).closest('tr#dialPlanPolicyTableRow').find('.dialPlanPolicyDelCheckBox').data("enableSecondaryDialTone");
			var description  				= $(this).closest('tr#dialPlanPolicyTableRow').find('.dialPlanPolicyDelCheckBox').data("description");
			var networkrouting  			= $(this).closest('tr#dialPlanPolicyTableRow').find('.dialPlanPolicyDelCheckBox').data("networkrouting");
			var screenservice  				= $(this).closest('tr#dialPlanPolicyTableRow').find('.dialPlanPolicyDelCheckBox').data("screenservice");
			var isAdded  				= $(this).closest('tr#dialPlanPolicyTableRow').find('.dialPlanPolicyDelCheckBox').data("action");
			
			$.ajax({
				url: "enterprise/dialPlanPolicyDialog.php",
	            type:'GET',
	            success: function(result)
	            {
	           
	        	isModify = "modify";
	            	$("#dialPlanPolicyDialog").html(result);
	        	    $("#dialPlanPolicyDialog").dialog("open");
	        	    
	        	    $("#accessCode").val(accessCode);
	        	    $("#description").val(description);
	        	    if(networkrouting){
	        	    	$('#includeCodeForNetworkTranslationsAndRouting').prop('checked', true);
	        	    }else{
	        	    	$('#includeCodeForNetworkTranslationsAndRouting').prop('checked', false);
	        	    }
	        	    
	        	    if(screenservice){
	        	    	$('#includeCodeForScreeningServices').prop('checked', true);
	        	    }else{
	        	    	$('#includeCodeForScreeningServices').prop('checked', false);
	        	    }
	        	    if(enableSecondaryDialTone){
	        	    	$('#enableSecondaryDialTone').prop('checked', true);
	        	    }else{
	        	    	$('#enableSecondaryDialTone').prop('checked', false);
	        	    }
	        	    
	        	    $("#accessCode").prop("readonly", true);
	        	    var enableSDT = $("#privateDigitMap").val();
	        	    if(enableSDT != ""){
	        	    	$("#enableSecondaryDialTone").prop("disabled", false);
	        	    }else{
	        	    	$("#enableSecondaryDialTone").prop("disabled", true);
	        	    }
	        	    if(isAdded == "added"){
	        	    	$("#actionName").val("added");
	        	    }else{
	        	    	$("#actionName").val("");
	        	    }
	        	    
	        	    $(":button:contains('Update')").show().addClass('subButton');
	        	    $(":button:contains('Cancel')").show().addClass('cancelButton');
	        	    $(":button:contains('Create')").hide();
	        	    
	        	    //$("#softPhoneDeviceTypeLinePort").prop("disabled", true);
	            }
	        });
			
			/*var port  				= $(this).closest('tr#softPhoneTable').find('.softPhonedelCheckbox').data("port");
			var updateDeviceName   = $(this).closest('tr#softPhoneTable').find('.softPhonedelCheckbox').data("name");
			var updateDeviceType   = $(this).closest('tr#softPhoneTable').find('.softPhonedelCheckbox').data("device");
			$("#softPhonePort").attr('value',port);
			$("#softPhoneDeviceType").attr('value',updateDeviceType);
			$("#softPhoneDeviceName").attr('value',updateDeviceName);
			
			$("#hiddenSoftPhonePort").attr('value',port);
			$("#hiddenSoftPhoneDeviceType").attr('value',updateDeviceType);
			$("#hiddenSoftPhoneDeviceName").attr('value',updateDeviceName);
			
			var updateData 	=	$("#updateSoftPhoneDialog").html() ;
			$("#softPhoneDialog").html(updateData) ;
			$("#softPhoneDialog").dialog("open");*/
		 }
	});

var successResonseGetDialPlanListData = function (data){
	//alert("success"+data) ;
//	var tableCls   = "";
            var tableData  = "";
            //var deviceType = "";
            var result     = JSON.parse(data);
            var dialPlanPolicylist = result;
            //var deviceTypeClass = "SortingClass";
            //var newClassForSorting = "";            
            //delete result.deviceList; //Removing last element mean deviceType from array 
            tableCls = "dialPlanPolicyTableDiv";
            //Code added @ 30 May 2019
            var dialPlanModPermission = $("#dialPlanModPermission").val();
            var dialRowId             = "id='dialPlanPolicyTableRow'";
            var hideCheckBox          = "style='width:17px;'";
            if(dialPlanModPermission === "no"){
                dialRowId      = "id='dialPlanPolicyTableRowDup'";
                hideCheckBox   = "style='width:17px; opacity:0.4;' disabled";
            }
            //End code
            if(dialPlanPolicylist!=undefined){ 
                    tableData         += "<table border='1' style='border-right:1px solid #ccc;' class='"+tableCls+" tableAlignMentDesign scroll tablesorter tagTableHeight' id='dialPlanPolicyTable'>";
                    tableData         += "<thead> ";
                    tableData         += "<tr>  ";

                    tableData         += "<th id='disableTh' class='header thsmall' style='width:10% !important;height:auto;'>Delete</th>";
                    tableData         += "<th class='header thsmall' style='height: auto !important;width:12% !important;'>Access Code</th>";
                    tableData         += "<th class='header thsmall' style='height: auto !important;width:17% !important;'>Enable Secondry Dial Tone</th>";
                    tableData         += "<th class='header thsmall' style='height: auto !important;width:33% !important;'>Include Code for</th>";
                    tableData         += "<th class='header thsmall' style='height: auto !important;width:28% !important;'>Description</th>";
                    tableData         += "</tr>";

                    tableData         += "</thead>";
                    tableData         += "<tbody id='customTagTable' style='overflow-y:scroll !important;'>"; 
                    //var pos = 1;
                    $.each(dialPlanPolicylist, function(key, value){
                         var accessCode = value.accessCode;
                         var enableSecondaryDialTone = value.enableSecondaryDialTone;  
                         var description = value.description;  
                         var networkrouting = value.includeCodeForNetworkTranslationsAndRouting;
                         var screenservice = value.includeCodeForScreeningServices;
                         var action = value.action;
                         if(enableSecondaryDialTone == "true"){
                        	 var enableSecondaryDialToneUpdated = "Yes";
                         }else{
                        	 var enableSecondaryDialToneUpdated = "No";
                         }
                         
                         tableData += "<tr class='tableTrAlignment' style='' "+dialRowId+ ">";
                         tableData += "<td style='width:10% !important;padding:15px !important;' class='deleteCheckboxtd thSno header'><input name='dialPlanAccessCode[]' value='"+accessCode+"' type='checkbox' "+hideCheckBox+" class='dialPlanPolicyDelCheckBox' id='"+accessCode+"' data-accesscode='"+accessCode+"' data-enableSecondaryDialTone='"+enableSecondaryDialTone+"' data-description='"+description+"' data-networkrouting='"+networkrouting+"' data-screenservice='"+screenservice+"' data-action='"+action+"'><label for='"+accessCode+"'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label></td>";
                         tableData += "<td style='width:12% !important;' class='header thsmall'>"+ accessCode + "</td>";
                         tableData += "<td style='width:17% !important;' class='header thsmall'>"+ enableSecondaryDialToneUpdated + "</td>";
                         tableData += "<td style='width:33% !important;' class='header thsmall'>Network Translation and Routing: "+networkrouting+"<br />Screening Services: "+screenservice+"</td>";
                         tableData += "<td style='width:28% !important;border-right:none;' class='header thsmall'>"+ description + "</td>";

                         tableData += "</tr>"; 
                         //pos++;
                    })
                    tableData         += "</tbody>";
                    tableData         += "</table>";
                    
                    $("#dialPlanPolicyTableDiv").html(tableData);
                    $(".dialPlanPolicyTableDiv").tablesorter();
                    
                   // $("."+tableCls).tablesorter();
                    
            }else{  
            	
            	
            	
                    $("#dialPlanPolicyTableDiv").html("<div class='col-md-12'><div class='form-group' style='text-align: center;'><b style='font-weight:bold;'>--- No Dial plan access codes found ---</b></div></div>");
            }
            
            $("#customConfigTagDeviceNameDiv").show();
            $("#deviceCustomTagCongLoader").hide() ;
};

var getDialPlanPolicyList = function (){
	var entId = $("#hiddenEnterpriseName").val();
	//$("#customConfigTagDeviceNameDiv").hide();
	//$("#deviceCustomTagCongLoader").show() ;
        $.ajax({
                method:"POST",
                  url:"enterprise/dialPlanPolicyLogic.php",
                  data:{"funcAction":"showDialPlanPolicyList", "entId" : entId},
        }).then(successResonseGetDialPlanListData);
};

var dialPlanPolicyAdd = function (){
	var entId = $("#hiddenEnterpriseName").val();
	//var softPhoneType = $("#selectSoftPhoneDeviceType").find(':selected').data('type');
	var dataToSend = $("form#dialPlanPolicyFormData").serializeArray();
	$.ajax({
		type: "POST",
		url:"enterprise/dialPlanPolicyLogic.php",
		data:{"datatosend":dataToSend, "funcAction":"dialPlanPolicyAdd", "entId" : entId},
		success: function(result) {
			$("#updateDialPlanPolicyDialog").dialog("close");
			successResonseGetDialPlanListData(result);
			
		}
	});
        /*$.ajax({
                type:"POST",
                url:"userMod/softPhone/softPhoneLogic.php",
                data:{"funcAction":"softphoneAdd", "data":dataToSend},
         }).then(successResonseGetSoftPhoneList,errorSoftPhoneResponseMsg);*/
};

var domainAdd = function (){
	var domainValues = [];	
	var inputs = $("input[name=domainName]");	
	if(inputs.length > 0){
		for(var i = 0; i < inputs.length; i++){
			if($(inputs[i]).val() != ""){
				domainValues.push($(inputs[i]).val());
			}
		}
	}
	console.log(domainValues);
	
	var html = "";
	var html1 = "";
	$.ajax({
		type: "POST",
		url:"enterprise/domainsLogic.php",
		data:{"datatosend":domainValues, "funcAction":"domainAdd", "entId" : entId},
		success: function(result) {
			$("#updateDomainDialog").dialog("close");
			var obj = jQuery.parseJSON(result);
			var resultData = [];
			for (var k = 0; k < obj.length; k++) {
				resultData.push(obj[k]);
			}
			    
			var order1 = $("#sortable_1").sortable("toArray");

			$.each(resultData, function(i, v) {
				if(  $.inArray(v, order1) == -1) {
					order1.push(v);
				}
			});
			
			for (i = 0; i < order1.length; i++)
			{
				html += '<li class="ui-state-default" id="'+order1[i]+'">' + order1[i] + '</li>';
			}
			$("#sortable_1").html("");
			$("#sortable_1").html(html);
			$(".extraInputRowDomain").val("");
			$("#closeEnterpriseDomainsBtn").trigger("click");
		}
	});
	
	
	var entId = $("#hiddenEnterpriseName").val();
	var securityDomainPatternMatch = $("#securityDomainPatternMatch").val();
	var securityDoaminShow = $("#securityDoaminShow").val();
	
	/*var dataToSend = $("form#domainFormData").serializeArray();
	var html = "";
	var html1 = "";
	$.ajax({
		type: "POST",
		url:"enterprise/domainsLogic.php",
		data:{"datatosend":dataToSend, "funcAction":"domainAdd", "entId" : entId},
		success: function(result) {debugger;
			$("#updateDomainDialog").dialog("close");
			if(securityDoaminShow == "show"){
				var obj = jQuery.parseJSON(result);
				var resultData = [];
				var resultDataSecMatch = [];
				for (var k = 0; k < obj.length; k++) {
					//resultData.push(obj[k]);
					var objName = obj[k];
					if ( objName.indexOf(securityDomainPatternMatch) === -1 ){
						resultData.push(obj[k]);
					}else{
						resultDataSecMatch.push(obj[k]);
					}
				}
				    
				var order1 = $("#sortable_1").sortable("toArray");
				$.each(resultData, function(i, v) {
					if(  $.inArray(v, order1) == -1) {
						order1.push(v);
					}
				});
				for (i = 0; i < order1.length; i++)
				{
					html += '<li class="ui-state-default" id="'+order1[i]+'">' + order1[i] + '</li>';
				}
				$("#sortable_1").html("");
				$("#sortable_1").html(html);
				
				var order2 = $("#sortable1").sortable("toArray");
				$.each(resultDataSecMatch, function(i, v) {
					if(  $.inArray(v, order2) == -1) {
						order2.push(v);
					}
				});
				for (i = 0; i < order2.length; i++)
				{
					html1 += '<li class="ui-state-default" id="'+order2[i]+'">' + order2[i] + '</li>';
				}
				$("#sortable1").html("");
				$("#sortable1").html(html1);
			}else{
				var obj = jQuery.parseJSON(result);
				var resultData = [];
				for (var k = 0; k < obj.length; k++) {
					resultData.push(obj[k]);
				}
				    
				var order1 = $("#sortable_1").sortable("toArray");

				$.each(resultData, function(i, v) {
					if(  $.inArray(v, order1) == -1) {
						order1.push(v);
					}
				});
				
				for (i = 0; i < order1.length; i++)
				{
					html += '<li class="ui-state-default" id="'+order1[i]+'">' + order1[i] + '</li>';
				}
				$("#sortable_1").html("");
				$("#sortable_1").html(html);
			}
			
		}
	});*/
};

var securityDomainAdd = function (){
	var domainValues = [];	
	var inputs = $("input[name=securityDomainName]");	
	if(inputs.length > 0){
		for(var i = 0; i < inputs.length; i++){
			if($(inputs[i]).val() != ""){
				domainValues.push($(inputs[i]).val());
			}
		}
	}
	console.log(domainValues);
	
	var html = "";
	var html1 = "";
	$.ajax({
		type: "POST",
		url:"enterprise/securityDomainsLogic.php",
		data:{"datatosend":domainValues, "funcAction":"domainAdd", "entId" : entId},
		success: function(result) {
			$("#updateDomainDialog").dialog("close");
			var obj = jQuery.parseJSON(result);
			var resultData = [];
			for (var k = 0; k < obj.length; k++) {
				resultData.push(obj[k]);
			}
			    
			var order1 = $("#sortable1").sortable("toArray");

			$.each(resultData, function(i, v) {
				if(  $.inArray(v, order1) == -1) {
					order1.push(v);
				}
			});
			
			for (i = 0; i < order1.length; i++)
			{
				html += '<li class="ui-state-default" id="'+order1[i]+'">' + order1[i] + '</li>';
			}
			$("#sortable1").html("");
			$("#sortable1").html(html);
			$(".extraInputRowDomain").val("");
			$("#closeEnterpriseSecurityDomainsBtn").trigger("click");
		}
	});
	
	
	var entId = $("#hiddenEnterpriseName").val();
	var securityDomainPatternMatch = $("#securityDomainPatternMatch").val();
	var securityDoaminShow = $("#securityDoaminShow").val();
	
	/*var dataToSend = $("form#domainFormData").serializeArray();
	var html = "";
	var html1 = "";
	$.ajax({
		type: "POST",
		url:"enterprise/domainsLogic.php",
		data:{"datatosend":dataToSend, "funcAction":"domainAdd", "entId" : entId},
		success: function(result) {debugger;
			$("#updateDomainDialog").dialog("close");
			if(securityDoaminShow == "show"){
				var obj = jQuery.parseJSON(result);
				var resultData = [];
				var resultDataSecMatch = [];
				for (var k = 0; k < obj.length; k++) {
					//resultData.push(obj[k]);
					var objName = obj[k];
					if ( objName.indexOf(securityDomainPatternMatch) === -1 ){
						resultData.push(obj[k]);
					}else{
						resultDataSecMatch.push(obj[k]);
					}
				}
				    
				var order1 = $("#sortable_1").sortable("toArray");
				$.each(resultData, function(i, v) {
					if(  $.inArray(v, order1) == -1) {
						order1.push(v);
					}
				});
				for (i = 0; i < order1.length; i++)
				{
					html += '<li class="ui-state-default" id="'+order1[i]+'">' + order1[i] + '</li>';
				}
				$("#sortable_1").html("");
				$("#sortable_1").html(html);
				
				var order2 = $("#sortable1").sortable("toArray");
				$.each(resultDataSecMatch, function(i, v) {
					if(  $.inArray(v, order2) == -1) {
						order2.push(v);
					}
				});
				for (i = 0; i < order2.length; i++)
				{
					html1 += '<li class="ui-state-default" id="'+order2[i]+'">' + order2[i] + '</li>';
				}
				$("#sortable1").html("");
				$("#sortable1").html(html1);
			}else{
				var obj = jQuery.parseJSON(result);
				var resultData = [];
				for (var k = 0; k < obj.length; k++) {
					resultData.push(obj[k]);
				}
				    
				var order1 = $("#sortable_1").sortable("toArray");

				$.each(resultData, function(i, v) {
					if(  $.inArray(v, order1) == -1) {
						order1.push(v);
					}
				});
				
				for (i = 0; i < order1.length; i++)
				{
					html += '<li class="ui-state-default" id="'+order1[i]+'">' + order1[i] + '</li>';
				}
				$("#sortable_1").html("");
				$("#sortable_1").html(html);
			}
			
		}
	});*/
};



var dialPlanPolicyModify = function (){
	var entId = $("#hiddenEnterpriseName").val();
	//var softPhoneType = $("#selectSoftPhoneDeviceType").find(':selected').data('type');
	var dataToSend = $("form#dialPlanPolicyFormData").serializeArray();
	$.ajax({
		type: "POST",
		url:"enterprise/dialPlanPolicyLogic.php",
		data:{"datatosend":dataToSend, "funcAction":"dialPlanPolicyModify", "entId" : entId},
		success: function(result) {
			$("#updateDialPlanPolicyDialog").dialog("close");
			successResonseGetDialPlanListData(result);
			
		}
	});
};

$(function() {
	$("#showAddDomainEnterprise").hide();
	$("#showAddSecurityDomainEnterprise").hide();
	
	$("#tabs").tabs();
	var action = "";
	$("#addDomainButton").click(function(){
		//$("#errorDnsGroup").html('');
		$("#showAddDomainEnterprise").slideDown();
		//$("#enterpriseAddDns").hide();
		//$("#closeRemoveDnsButton").trigger("click");
	});
	$("#addSecurityDomainButton").click(function(){
		//$("#errorDnsGroup").html('');
		$("#showAddSecurityDomainEnterprise").slideDown();
		//$("#enterpriseAddDns").hide();
		//$("#closeRemoveDnsButton").trigger("click");
	});
	
	$("#closeEnterpriseDomainsBtn").click(function(){
		$("#showAddDomainEnterprise").slideUp();
		//$("#enterpriseAddDns").show();
		//$("input[name=minPhoneNumber]").val('');
		//$("input[name=maxPhoneNumber]").val('');
		//$("input[name=phoneNumber]").val('');
		//$(".extraAddRow").remove();
	});
	
	$("#closeEnterpriseSecurityDomainsBtn").click(function(){
		$("#showAddSecurityDomainEnterprise").slideUp();
		//$("#enterpriseAddDns").show();
		//$("input[name=minPhoneNumber]").val('');
		//$("input[name=maxPhoneNumber]").val('');
		//$("input[name=phoneNumber]").val('');
		//$(".extraAddRow").remove();
	});
	
	$("#addDomainRowEnterprise").click(function(){
		var el = $(this);
		html = '<div class="form-group extraAddRow"><div class="col-md-10 extraAddRow extraDivRow" style="padding-left:0px;"><input class="extraInputRowDomain" type="text" name="domainName" value="" style="width:380px !important;" /></div><div class="col-md-1" style="width:16% !important"><img class="" id="deleteDomainRow" src="images/minus.png" style="width:18px; float:right; margin-top:21px"></div></div>'; 
		el.parent('div').parent('div').parent('div').append(html);
	});
	
	$("#addSecurityDomainRowEnterprise").click(function(){
		var el = $(this);
		html = '<div class="form-group extraAddRow"><div class="col-md-10 extraAddRow extraDivRow" style=""><input class="extraInputRowDomain" type="text" name="securityDomainName" value="" style="width:380px !important;" /></div><div class="col-md-1" style="width:16% !important"><img class="" id="deleteSecurityDomainRow" src="images/minus.png" style="width:18px; float:right; margin-top:21px"></div></div>'; 
		el.parent('div').parent('div').parent('div').append(html);
	});
	
	$(document).on("click", "#deleteSecurityDomainRow", function(){
		var el = $(this);
		el.parent('div').parent('div').remove();
	});
	
	$(document).on("click", "#deleteDomainRow", function(){
		var el = $(this);
		el.parent('div').parent('div').remove();
	});
	
	
	$("#subButtonEnt").click(function(){
		action = "Modify";
		if($("#assignedNCS option").length > 0){
			$('#assignedNCS option').prop('selected', true);
		}
		if($("#assignedDefault option").length > 0 && $('#assignedDefault option:selected').length == 0){
			$('#assignedDefault option').prop('selected', true);
		}
		var dataToSend = $("form#enterpriseModify").not("#enterpriseGroupServiceTable, #enterpriseUserServiceTable").serializeArray();
		dataToSend = makeServicePostData(dataToSend);
		dataToSend = getUnassignNumbers(dataToSend);
               // dataToSend = getDelUnassignNumbers(dataToSend);
                 
                
		$.ajax({
            type: "POST",
            url: "enterprise/modifyValidate.php",
            data: dataToSend,
            success: function(result)
            {   
            	$("#enterpriseDialog").dialog("option","title", "Enterprise Modify Dialog");
                result = result.trim();                      	
                if (result.slice(0, 1) == 1){
                    $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                }else{
                    $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                }
                //isModify = "modify";
                $("#enterpriseDialog").html(result.slice(1));
                //$("#enterpriseDialog").dialog("close");
                $("html, body").animate({ scrollTop: 0 }, 600);
                $("#enterpriseDialog").dialog("open");
                
                $(":button:contains('Complete')").show().addClass('subButton');
                $(":button:contains('Cancel')").show().addClass('cancelButton');
                $(":button:contains('Create')").hide();
                $(":button:contains('Close')").hide();
                $(":button:contains('Update')").hide();
                $(":button:contains('Confirm')").hide();
                $(":button:contains('More Changes')").hide();
                $(":button:contains('Return To Main')").hide();
                /*var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                getCustomTagList(deviceTypeNameValue);*/
            }
      });  
	});
	
	$("#EntButtonCancel").click(function(){
		$("#enterpriseModifyData").html("");
		$("#searchEnterpriseByAutoFill").val("");
	});
	
	$("#subButtonEntAdd").click(function(){
		action = "Add";
		var dataToSend = $("form#enterpriseAdd").serializeArray();
        $.ajax({
            type: "POST",
            url: "enterprise/addValidate.php",
            data: dataToSend,
            success: function(result)
            {        
            	$("#enterpriseDialog").dialog("option","title", "Enterprise Add Dialog");
                result = result.trim();                      	
                if (result.slice(0, 1) == 1){
                    $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                }else{
                    $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                }
                //isModify = "modify";
                $("#enterpriseDialog").html(result.slice(1));
                //$("#enterpriseDialog").dialog("close");
                $("html, body").animate({ scrollTop: 0 }, 600);
                $("#enterpriseDialog").dialog("open");
                
                $(":button:contains('Complete')").show().addClass('subButton');
                $(":button:contains('Cancel')").show().addClass('cancelButton');
                $(":button:contains('Create')").hide();
                $(":button:contains('Close')").hide();
                $(":button:contains('Update')").hide();
                $(":button:contains('Confirm')").hide();
                $(":button:contains('More Changes')").hide();
                $(":button:contains('Return To Main')").hide();
                /*var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                getCustomTagList(deviceTypeNameValue);*/
            }
      });  
	});
	
	$("#subButtonDelEnt").click(function(){
		/*var enterpriseId = $("#hiddenEnterpriseName").val();
        $.ajax({
            type: "POST",
            url: "enterprise/enterpriseDelete.php",
            data: {"enterpriseId" : enterpriseId},
            success: function(result)
            {
            	
            }
        });*/
		$("#enterpriseDialog").dialog("option","title", "Enterprise Delete Dialog");
		$("#enterpriseDialog").html("<p style='text-align:center;'>Are you sure you want to delete this enterprise?</p>");
        //$("#enterpriseDialog").dialog("close");
        $("html, body").animate({ scrollTop: 0 }, 600);
        $("#enterpriseDialog").dialog("open");
        
        $(":button:contains('Complete')").hide().addClass('subButton');
        $(":button:contains('Cancel')").show().addClass('cancelButton');
        $(":button:contains('Create')").hide();
        $(":button:contains('Close')").hide();
        $(":button:contains('Update')").hide();
        $(":button:contains('Confirm')").hide();
        $(":button:contains('More Changes')").hide();
        $(":button:contains('Return To Main')").hide();
        $(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass('deleteButton');
		
		/*var enterpriseId = $("#hiddenEnterpriseName").val();
        $.ajax({
            type: "POST",
            url: "enterprise/enterpriseDelete.php",
            data: {"enterpriseId" : enterpriseId},
            success: function(result)
            {                 
                result = result.trim();                      	
                if (result.slice(0, 1) == 1){
                    $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                }else{
                    $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                }
                //isModify = "modify";
                $("#enterpriseDialog").html(result.slice(1));
                //$("#enterpriseDialog").dialog("close");
                $("html, body").animate({ scrollTop: 0 }, 600);
                $("#enterpriseDialog").dialog("open");
                
                $(":button:contains('Complete')").show().addClass('subButton');
                $(":button:contains('Cancel')").show().addClass('cancelButton');
                $(":button:contains('Create')").hide();
                $(":button:contains('Close')").hide();
                $(":button:contains('Update')").hide();
                $(":button:contains('Confirm')").hide();
                $(":button:contains('More Changes')").hide();
                $(":button:contains('Return To Main')").hide();
                /*var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                getCustomTagList(deviceTypeNameValue);*/
           /* }
      });*/
	});
	
	
	$("#enterpriseDialog").dialog({
        autoOpen: false,
        width: 800,
        modal: true,
        title: "Enterprise Modify Dialog",
        position: { my: "top", at: "top" },
        resizable: false,
        closeOnEscape: false,
        buttons: {
      	  Update: function() {
      		  $("html, body").animate({ scrollTop: 0 }, 600);
      		  //$("#selectSoftPhoneDeviceType").prop("disabled", false);
      		  //$("#softPhoneDeviceTypeLinePort").prop("disabled", false);
      		  
          	  var dataToSend = $("form#servicePackModDialogForm").serializeArray();
                $.ajax({
                    type: "POST",
                    url: "servicePacks/modValidate.php",
                    data: dataToSend,
                    success: function(result)
                    {                 
                        result = result.trim();                      	
                        if (result.slice(0, 1) == 1){
                            $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                        }else{
                            $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                        }
                        isModify = "modify";
                        $("#updateServicePackDialog").html(result.slice(1));
                        $("#enterpriseDialog").dialog("close");
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        $("#updateServicePackDialog").dialog("open");
                        
                        $(":button:contains('Complete')").show().addClass('subButton');
                        $(":button:contains('Cancel')").show().addClass('cancelButton');
                        $(":button:contains('Create')").hide();
                        $(":button:contains('Close')").hide();
                        $(":button:contains('Update')").hide();
                        $(":button:contains('Confirm')").hide();
                        $(":button:contains('More Changes')").hide();
                        $(":button:contains('Return To Main')").hide();
                        /*var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                        getCustomTagList(deviceTypeNameValue);*/
                    }
              });  
          	   
           },
          
          	Create: function() {
          		$("html, body").animate({ scrollTop: 0 }, 600);
                  var dataToSend = $("form#servicePackDialogForm").serializeArray();
                  var pageUrl = "addCheckData.php"; 
                  $.ajax({
                    type: "POST",
                    url: "servicePacks/"+pageUrl,
                    data: dataToSend,
                    success: function(result)
                    {

                        result = result.trim();
                        if (result.slice(0, 1) == 1)
                        {
                            $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                        }
                        else
                        {
                            $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                        }
                        $("#updateServicePackDialog").html(result.slice(1));
                        $("#enterpriseDialog").dialog("close");
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        $("#updateServicePackDialog").dialog("open");

                        $(":button:contains('Complete')").show().addClass('subButton');;
                        $(":button:contains('Cancel')").show().addClass('cancelButton');;
                        $(":button:contains('More Changes')").hide();
                        $(":button:contains('Return To Main')").hide();
                        $(":button:contains('Create')").hide();
                        isModify = "add";
                        // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                    }
                  });  
	 	 
           },
           Complete: function(){
        	   var actionOperation = "";
        	   $("html, body").animate({ scrollTop: 0 }, 600);
        	   if(action == "Add"){
        		   //alert("Add");
        		   actionOperation = "Add Enterprise";
        		   var dataToSend = $("form#enterpriseAdd").serializeArray();
                   var pageUrl = "enterpriseAdd.php"; 
        	   }else{
        		   //alert("Modify");
        		   actionOperation = "Modify Enterprise";
        		   var dataToSend = $("form#enterpriseModify").serializeArray();
        		   dataToSend = getUnassignNumbers(dataToSend);
                   var pageUrl = "enterpriseModify.php"; 
        	   }
        	   pendingProcess.push(actionOperation);
               $.ajax({
                 type: "POST",
                 url: "enterprise/"+pageUrl,
                 data: dataToSend,
                 success: function(result)
                 { //debugger;
                	 if(foundServerConErrorOnProcess(result, "User Basic List")) {
     					return false;
                     }
                	 if(action == "Add"){
                		 getEnterpriseList();
                		 var obj = jQuery.parseJSON(result);
                		 if(obj.Error == ""){
                			 $("#hiddenEntId").val(obj.Success);
                			 checkResult(false); 
                             
                             $("#enterpriseDialog").html("<p style='text-align:center;'>Enterprise created succesfully</p>");
                             
                             if (typeof obj.clone.domainAssigned != 'undefined'){
                            	 if(obj.clone.domainAssigned.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Domains cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.securityDomainAssigned != 'undefined'){
                            	 if(obj.clone.securityDomainAssigned.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Security Domains cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.groupService != 'undefined'){
                            	 if(obj.clone.groupService.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Group Services cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.userService != 'undefined'){
                            	 if(obj.clone.userService.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>User Services cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.callingLineIDPolicy != 'undefined'){
                            	 if(obj.clone.callingLineIDPolicy.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Calling Line ID Policies cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.mediaPolicy != 'undefined'){
                            	 if(obj.clone.mediaPolicy.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Media Policiy cloned succesfully</p>");
                                 }
                             }
                           
                             if (typeof obj.clone.callLimits != 'undefined'){
                            	 if(obj.clone.callLimits.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Call limits cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.conferencingPolicy != 'undefined'){
                            	 if(obj.clone.conferencingPolicy.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Conferencing Policy cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.translationRouting != 'undefined'){
                            	 if(obj.clone.translationRouting.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Translation and Routing Policies cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.disableCallerId != 'undefined'){
                            	 if(obj.clone.disableCallerId.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Disable Caller ID Policy cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.dialPlanData != 'undefined'){
                            	 if(obj.clone.dialPlanData.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Dial Plan Policy cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.voiceMessaging != 'undefined'){
                            	 if(obj.clone.voiceMessaging.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Voice Messaging Policies cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.ncs != 'undefined'){
                            	 if(obj.clone.ncs.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Network Classes of Services cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.routingProfile != 'undefined'){
                            	 if(obj.clone.routingProfile.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Routing Profile cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.passwordRules != 'undefined'){
                            	 if(obj.clone.passwordRules.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Password Rules cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.sipPasswordRules != 'undefined'){
                            	 if(obj.clone.sipPasswordRules.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>SIP Authentication Rules cloned succesfully</p>");
                                 }
                             }
                             
                             if (typeof obj.clone.devicePasswordRules != 'undefined'){
                            	 if(obj.clone.devicePasswordRules.Success == "Success"){
                                	 $("#enterpriseDialog").append("<p style='text-align:center;'>Device Profile Authentication Password Rules cloned succesfully</p>");
                                 }
                             }
                             
                             $("html, body").animate({ scrollTop: 0 }, 600);
                             $("#enterpriseDialog").dialog("open");

                             $(":button:contains('Complete')").hide();
                             $(":button:contains('More Changes')").show().addClass('moreChangesButton');
                             $(":button:contains('Return To Main')").show().addClass('moreChangesButton');
                             $(":button:contains('Create')").hide();
                             $(":button:contains('Cancel')").hide();
                		 }else{
                			 checkResult(false); 
                             
                             $("#enterpriseDialog").html("<p style='text-align:center;color:red'>"+obj.Error+"</p>");
                             $("html, body").animate({ scrollTop: 0 }, 600);
                             $("#enterpriseDialog").dialog("open");

                             $(":button:contains('Complete')").hide();
                             $(":button:contains('More Changes')").hide().addClass('moreChangesButton');
                             $(":button:contains('Return To Main')").show().addClass('moreChangesButton');
                             $(":button:contains('Create')").hide();
                             $(":button:contains('Cancel')").show().addClass('cancelButton');
                		 }
                		 
                		
                	 }else{
                		 checkResult(false); 
                         
                         $("#enterpriseDialog").html(result);
                         $("html, body").animate({ scrollTop: 0 }, 600);
                         $("#enterpriseDialog").dialog("open");

                         $(":button:contains('Complete')").hide();
                         $(":button:contains('More Changes')").show().addClass('moreChangesButton');
                         $(":button:contains('Return To Main')").show().addClass('moreChangesButton');
                         $(":button:contains('Create')").hide();
                         $(":button:contains('Cancel')").hide();
                	 }

                	 
                 }
               });
           },
           'More Changes': function(){
        	   if(action == "Add"){
        		   var hiddenEntId = $("#hiddenEntId").val();
        		   $("#searchEnterpriseByAutoFill").val(hiddenEntId);
        		   action ="";
        	   }
        	   $("html, body").animate({ scrollTop: 0 }, 600);
        	   //var selectedEnterprise = $("#searchEnterpriseByAutoFill").val();
        	   $(this).dialog("close");
        	   $("#getEnterpriseInfo").trigger("click");
        	   
           },
           "Return To Main" : function(){
        	  checkResult(false);
               location.href="main_enterprise.php";
           },
           Delete : function(){
        	   pendingProcess.push("Delete Enterprise");
        	   var enterpriseId = $("#hiddenEnterpriseName").val();
               $.ajax({
                   type: "POST",
                   url: "enterprise/enterpriseDelete.php",
                   data: {"enterpriseId" : enterpriseId},
                   success: function(result)
                   {                 
                	   if(foundServerConErrorOnProcess(result, "Delete Enterprise")) {
       						return false;
                       }
                       
                       //isModify = "modify";
                       $("#enterpriseDialog").html(result);
                       //$("#enterpriseDialog").dialog("close");
                       $("html, body").animate({ scrollTop: 0 }, 600);
                       $("#enterpriseDialog").dialog("open");
                       
                       $(":button:contains('Complete')").hide().addClass('subButton');
                       $(":button:contains('Cancel')").hide().addClass('cancelButton');
                       $(":button:contains('Create')").hide();
                       $(":button:contains('Close')").hide();
                       $(":button:contains('Update')").hide();
                       $(":button:contains('Confirm')").hide();
                       $(":button:contains('More Changes')").hide();
                       $(":button:contains('Return To Main')").show().addClass('cancelButton');
                       $(":button:contains('Done')").show().addClass('subButton');
                       $(":button:contains('Delete')").hide();
                       /*var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                       getCustomTagList(deviceTypeNameValue);*/
                   }
             });
           },
            Close: function() {
                $(this).dialog("close");
            },              
            Cancel: function() {
                $(this).dialog("close");
            },
            Done: function(){
            	$(this).dialog("close");
				$("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
				$("#loading").show();
				$.ajax({
					type: "POST",
					url: "navigate.php",
					data: { module: 'Enterprises' },
					success: function(result)
					{
						$("#loading").hide();
						$("#mainBody").html(result);
						//$(":button:contains('Complete')").show().addClass('subButton');
                        //$(":button:contains('Cancel')").show().addClass('cancelButton');
					}
				});
				//$(".ui-dialog-titlebar-close", this.parentNode).show();
				//$(".ui-dialog-buttonpane", this.parentNode).show();
            }

        },
        open: function() {
        	setDialogDayNightMode($(this));
      	  	$(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
 		    $(".ui-dialog-buttonpane button:contains('Create')").button().hide();
 		    $(".ui-dialog-buttonpane button:contains('Close')").button().hide();
      	   	$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
      	    $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
      	    $(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
      	    $(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
      	    $(".ui-dialog-buttonpane button:contains('Return To Main')").button().hide();
      	    $(".ui-dialog-buttonpane button:contains('Done')").button().hide();
      	   
      	    /*if(entAction == "Create"){
      		   $(".ui-dialog-buttonpane button:contains('Create')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      		}
      	   
      	   if(entAction == "Update"){
      		   $(".ui-dialog-buttonpane button:contains('Update')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      	   }
      	   
      	   if(entAction == "Delete"){
      		   $(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      	   }*/
      	   
       }
    });
	
	$("#sortable_1, #sortable_2").sortable({
		placeholder: "ui-state-highlight",
		connectWith: "#sortable_2, #sortable_1",
		cursor: "crosshair",
		update: function(event, ui)
		{
			var domainAssignList = "";
			var order = $("#sortable_2").sortable("toArray");
			var orderLength = (order.length)-1;
			for (i = 0; i < order.length; i++)
			{
				if(orderLength > i){
					domainAssignList += order[i] + ";";
				}else{
					domainAssignList += order[i];
				}
				
			}
			$("#domainAssigned").val(domainAssignList);
		},
		receive: function(event, ui){
			var defaultDomainValue = $("#defaultDomain").val();
	        if(ui.item.attr('id') == defaultDomainValue){   
	            $(ui.placeholder).addClass('ui-state-error');                    
	            $(ui.sender).sortable('cancel');
	        }
	    }
	}).disableSelection();
	
	$("#sortable1, #sortable2").sortable({
		placeholder: "ui-state-highlight",
		connectWith: "#sortable1, #sortable2",
		cursor: "crosshair",
		update: function(event, ui)
		{
			var securityDomainAssignList = "";
			var order = $("#sortable2").sortable("toArray");
			var orderLength = (order.length)-1;
			for (i = 0; i < order.length; i++)
			{
				if(orderLength > i){
					securityDomainAssignList += order[i] + ";";
				}else{
					securityDomainAssignList += order[i];
				}
				//securityDomainAssignList += order[i] + ";";
			}
			$("#securityDomainAssigned").val(securityDomainAssignList);
		},
		receive: function(event, ui){
			var defaultDomainValue = $("#defaultDomain").val();
	        if(ui.item.attr('id') == defaultDomainValue){   
	            $(ui.placeholder).addClass('ui-state-error');                    
	            $(ui.sender).sortable('cancel');
	        }
	    }
	}).disableSelection();
	
	$(".useSystemDefaultDeliveryFromAddress").change(function() {
	    if (this.value == 'true') {
	        //$("#deliveryFromAddress").prop('disabled', true);
	        $("#deliveryFromAddress").prop("readonly",true);
	    }
	    else if (this.value == 'false') {
	    	//$("#deliveryFromAddress").prop('disabled', false);
	    	$("#deliveryFromAddress").prop("readonly",false);
	    }
	});
	
	$(".maxFailedLoginAttemptsChk").change(function() {
	    if (this.value == "") {
	        $("#disableLoginDays").prop('disabled', false);
	    }
	    else{
	    	$("#disableLoginDays").prop('disabled', true);
	    }
	});
	
	$(".maxFailedLoginAttemptsChk1").change(function() {
	    if (this.value == "true") {
	        $("#disableLoginDays1").prop('disabled', false);
	    }
	    else{
	    	$("#disableLoginDays1").prop('disabled', true);
	    }
	});
	
	$(".passwordExpiresDaysChk").change(function() {
	    if (this.value == "") {
	        $("#expirationDay").prop('disabled', false);
	    }
	    else{
	    	$("#expirationDay").prop('disabled', true);
	    }
	});
	
	$(".passwordExpiresDaysChk1").change(function() {
	    if (this.value == "true") {
	        $("#expirationDay1").prop('disabled', false);
	    }
	    else{
	    	$("#expirationDay1").prop('disabled', true);
	    }
	});
	
	$(".endpointAuthenticationLockoutTypeRadio").change(function() {
	    if (this.value == "None") {
	        $("#endpointWaitAlgorithm1").prop('disabled', true);
	        $("#endpointWaitAlgorithm2").prop('disabled', true);
	        $("#endpointLockoutFixedMinutes").prop('disabled', true);
	        $("#endpointAuthenticationLockoutTypeCheckbox").prop('disabled', true);
	        $("#endpointPermanentLockoutThreshold").prop('disabled', true);
	        
	        $('.sipAuthOpacity').css('opacity', '0.6');
	    }
	    else{
	    	$("#endpointWaitAlgorithm1").prop('disabled', false);
	        $("#endpointWaitAlgorithm2").prop('disabled', false);
	        $("#endpointLockoutFixedMinutes").prop('disabled', false);
	        $("#endpointAuthenticationLockoutTypeCheckbox").prop('disabled', false);
	        $("#endpointPermanentLockoutThreshold").prop('disabled', false);
	        $('.sipAuthOpacity').css('opacity', '1.0');
	    }
	});
	
	$(".trunkGroupAuthenticationLockoutTypeRadio").change(function() {
	    if (this.value == "None") {
	        $("#trunkGroupWaitAlgorithm1").prop('disabled', true);
	        $("#trunkGroupWaitAlgorithm2").prop('disabled', true);
	        $("#trunkGroupLockoutFixedMinutes").prop('disabled', true);
	        $("#trunkGroupAuthenticationLockoutTypeCheckbox").prop('disabled', true);
	        $("#trunkGroupPermanentLockoutThreshold").prop('disabled', true);
	        $('.trunkAuthAlgorithmRadio').css('opacity', '0.6');
	    }
	    else{
	    	$("#trunkGroupWaitAlgorithm1").prop('disabled', false);
	        $("#trunkGroupWaitAlgorithm2").prop('disabled', false);
	        $("#trunkGroupLockoutFixedMinutes").prop('disabled', false);
	        $("#trunkGroupAuthenticationLockoutTypeCheckbox").prop('disabled', false);
	        $("#trunkGroupPermanentLockoutThreshold").prop('disabled', false);
	        $('.trunkAuthAlgorithmRadio').css('opacity', '1.0');
	    }
	});
	
	$(".deviceTypeAuthLockoutTypeRadio").change(function() {
	    if (this.value == "None") {
	        $("#deviceProfileWaitAlgorithmDevice1").prop('disabled', true);
	        $("#deviceProfileWaitAlgorithmDevice2").prop('disabled', true);
	        $("#deviceProfileLockoutFixedMinutesDevice").prop('disabled', true);
	        $("#deviceProfileAuthenticationLockoutTypeChk").prop('disabled', true);
	        $("#deviceProfilePermanentLockoutThresholdDevice").prop('disabled', true);
	        $('.deviceAlgorithmRadio').css('opacity', '0.6');
	        
	    }
	    else{
	    	$("#deviceProfileWaitAlgorithmDevice1").prop('disabled', false);
	        $("#deviceProfileWaitAlgorithmDevice2").prop('disabled', false);
	        $("#deviceProfileLockoutFixedMinutesDevice").prop('disabled', false);
	        $("#deviceProfileAuthenticationLockoutTypeChk").prop('disabled', false);
	        $("#deviceProfilePermanentLockoutThresholdDevice").prop('disabled', false);
	        $('.deviceAlgorithmRadio').css('opacity', '1.0');
	    }
	});
	
	
	
	$(".useSystemDefaultNotificationFromAddress").change(function() {
	    if (this.value == 'true') {
	        $("#notificationFromAddress").prop('readonly', true);
	    }
	    else if (this.value == 'false') {
	    	$("#notificationFromAddress").prop('readonly', false);
	    }
	});
	
	$(".useSystemDefaultVoicePortalLockoutFromAddress").change(function() {
	    if (this.value == 'true') {
	        $("#voicePortalLockoutFromAddress").prop('readonly', true);
	    }
	    else if (this.value == 'false') {
	    	$("#voicePortalLockoutFromAddress").prop('readonly', false);
	    }
	});
	
	$("#addNCS, #addNCSAll").click(function(){ 
		var BtnId = $(this).attr('id');
		if(BtnId == "addNCSAll"){
			$('#availableNCSToAssign option').prop('selected', true);
		}
		addNcsToAssignList();
	});
	
	var addNcsToAssignList = function(){
		var html = "";
		var data = "";

		var availableNcs = $("#availableNCSToAssign").val();	
		if(availableNcs){
			for (var i = 0; i < availableNcs.length; i++) {
				html += '<option value="'+availableNcs[i]+'">' +availableNcs[i]+ '</option>';
				
				$("#availableNCSToAssign option[value='"+availableNcs[i]+"']").remove();			
			}
		}
		$("#assignedNCS").append(html);
	}
	
	$("#removeFromAssigned, #removeAllFromAssigned").click(function(){ 
		var BtnId = $(this).attr('id');
		if(BtnId == "removeAllFromAssigned"){
			$('#assignedNCS option').prop('selected', true);
		}
		removeNcsFromAssigned();
	});
	
	var removeNcsFromAssigned = function(){
		var html = "";
		var data = "";

		var assignedNcs = $("#assignedNCS").val();
		//var defVal = $("#assignedDefault").find("option:first-child").val();

		for (var i = 0; i < assignedNcs.length; i++) {
			var val = assignedNcs[i];
			//if(defVal != val){
				html += '<option value="'+assignedNcs[i]+'">' +assignedNcs[i]+ '</option>';
				$("#assignedNCS option[value='"+assignedNcs[i]+"']").remove();
				$("#assignedDefault option[value='"+assignedNcs[i]+"']").remove();
			//}
		}
		$("#availableNCSToAssign").append(html);
	}
	
	$("#selectToDefault").click(function(){
		var asgnData="";
		var assignedNcsSelected = $("#assignedNCS").val();
		for (var i = 0; i < 1; i++) {
			asgnData += '<option value="'+assignedNcsSelected[i]+'">' +assignedNcsSelected[i]+ '</option>';
		}
		$('#assignedDefault').find('option').remove();
		$("#assignedDefault").append(asgnData);
	});
	
	

	
	
	getDialPlanPolicyList();
	
	
	
	$("#dialPlanPolicyAdd").click(function(){
		dialPlanPolicyPos ="Create" ;
    	isModify = "add";
	    
	    $.ajax({
            url: "enterprise/dialPlanPolicyDialog.php",
            type:'GET',
            //data:{"userId":userId},
            success: function(result)
            {
            	//alert(dialPlanPolicyPos);
            	$("#dialPlanPolicyDialog").html(result);
        	    $("#dialPlanPolicyDialog").dialog("open");
        	    $(":button:contains('Create')").show().addClass('subButton');
        	    $(":button:contains('Cancel')").show().addClass('cancelButton');
        	    $(":button:contains('Update')").hide();
        	    var enableSDT = $("#privateDigitMap").val();
        	    if(enableSDT != ""){
        	    	$("#enableSecondaryDialTone").prop("disabled", false);
        	    }else{
        	    	$("#enableSecondaryDialTone").prop("disabled", true);
        	    }
        	   // var email = $("#emailAddress").val();
    	    	//$("#softPhoneAccountEmailAddress").val(email);
            }
        });
	 });
	
	$("#addEnterpriseDomainsBtn").click(function(){
		domainPos ="Create" ;
    	//isModify = "add";
		var domainValues = [];	
		var inputs = $("input[name=domainName]");	
		if(inputs.length > 0){
			for(var i = 0; i < inputs.length; i++){
				if($(inputs[i]).val() != ""){
					domainValues.push($(inputs[i]).val());
				}
			}
		}
		
		$("html, body").animate({ scrollTop: 0 }, 600);
        var dataToSend = domainValues;
        var pageUrl = "domainsAddValidate.php"; 
        $.ajax({
          type: "POST",
          url: "enterprise/"+pageUrl,
          data: {dataToSend : dataToSend},
          success: function(result)
          {

              result = result.trim();
              if (result.slice(0, 1) == 1)
              {
                  $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
              }
              else
              {
                  $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
              }
              $("#updateDomainDialog").html(result.slice(1));
              $("#domainDialog").dialog("close");
              //$("html, body").animate({ scrollTop: 0 }, 600);
              $("#updateDomainDialog").dialog("open");

              $(":button:contains('Complete')").show().addClass('subButton');;
              $(":button:contains('Cancel')").show().addClass('cancelButton');;
              $(":button:contains('More Changes')").hide();
              $(":button:contains('Return To Main')").hide();
              $(":button:contains('Create')").hide();
              $("#updateDomainDialog").dialog("option","title", "Add Domain");
              // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
          }
});
	    /*$.ajax({
            url: "enterprise/domainsDialog.php",
            type:'GET',
            //data:{"userId":userId},
            success: function(result)
            {
            	//alert(dialPlanPolicyPos);
            	$("#domainDialog").html(result);
        	    $("#domainDialog").dialog("open");
        	    $(":button:contains('Create')").show().addClass('subButton');
        	    $(":button:contains('Cancel')").show().addClass('cancelButton');
        	    $(":button:contains('Update')").hide();
            }
        });*/
	 });
	
	$("#addEnterpriseSecurityDomainsBtn").click(function(){
		domainPos ="Create" ;
		securtyDomainFlag = "Add";
    	//isModify = "add";
		var domainValues = [];	
		var inputs = $("input[name=securityDomainName]");	
		if(inputs.length > 0){
			for(var i = 0; i < inputs.length; i++){
				if($(inputs[i]).val() != ""){
					domainValues.push($(inputs[i]).val());
				}
			}
		}
		
		$("html, body").animate({ scrollTop: 0 }, 600);
        var dataToSend = domainValues;
        var pageUrl = "securityDomainsAddValidate.php"; 
        $.ajax({
          type: "POST",
          url: "enterprise/"+pageUrl,
          data: {dataToSend : dataToSend},
          success: function(result)
          {

              result = result.trim();
              if (result.slice(0, 1) == 1)
              {
                  $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
              }
              else
              {
                  $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
              }
              $("#updateDomainDialog").html(result.slice(1));
              $("#domainDialog").dialog("close");
              //$("html, body").animate({ scrollTop: 0 }, 600);
              $("#updateDomainDialog").dialog("open");

              $(":button:contains('Complete')").show().addClass('subButton');;
              $(":button:contains('Cancel')").show().addClass('cancelButton');;
              $(":button:contains('More Changes')").hide();
              $(":button:contains('Return To Main')").hide();
              $(":button:contains('Create')").hide();
              $("#updateDomainDialog").dialog("option","title", "Add Security Domain");
              // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
          }
});
	    /*$.ajax({
            url: "enterprise/domainsDialog.php",
            type:'GET',
            //data:{"userId":userId},
            success: function(result)
            {
            	//alert(dialPlanPolicyPos);
            	$("#domainDialog").html(result);
        	    $("#domainDialog").dialog("open");
        	    $(":button:contains('Create')").show().addClass('subButton');
        	    $(":button:contains('Cancel')").show().addClass('cancelButton');
        	    $(":button:contains('Update')").hide();
            }
        });*/
	 });
	
	
	$("#dialPlanPolicyDialog").dialog({
        autoOpen: false,
        width: 800,
        modal: true,
        title: "Dial Plan Policy Dialog",
        position: { my: "top", at: "top" },
        resizable: false,
        closeOnEscape: false,
        buttons: {
      	  Update: function() {
      		$("html, body").animate({ scrollTop: 0 }, 600);
            var dataToSend = $("form#dialPlanPolicyFormData").serializeArray();
            var actionData = $("#actionName").val();
            if(actionData == "added"){
            	var pageUrl = "dialPlanPolicyAddValidate.php";
            	isModify = "added";
            }else{
            	var pageUrl = "dialPlanPolicyModValidate.php";
            	isModify = "modify";
            }
             
            $.ajax({
              type: "POST",
              url: "enterprise/"+pageUrl,
              data: dataToSend,
              success: function(result)
              {

                  result = result.trim();
                  if (result.slice(0, 1) == 1)
                  {
                      $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                  }
                  else
                  {
                      $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                  }
                  $("#updateDialPlanPolicyDialog").html(result.slice(1));
                  $("#dialPlanPolicyDialog").dialog("close");
                  //$("html, body").animate({ scrollTop: 0 }, 600);
                  $("#updateDialPlanPolicyDialog").dialog("open");

                  $(":button:contains('Complete')").show().addClass('subButton');;
                  $(":button:contains('Cancel')").show().addClass('cancelButton');;
                  $(":button:contains('More Changes')").hide();
                  $(":button:contains('Return To Main')").hide();
                  $(":button:contains('Create')").hide();
                  $(":button:contains('Update')").hide();
                  // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
              }
});  
          	   
           },
          
          	Create: function() {
          		$("html, body").animate({ scrollTop: 0 }, 600);
                  var dataToSend = $("form#dialPlanPolicyFormData").serializeArray();
                  var pageUrl = "dialPlanPolicyAddValidate.php"; 
                  $.ajax({
                    type: "POST",
                    url: "enterprise/"+pageUrl,
                    data: dataToSend,
                    success: function(result)
                    {

                        result = result.trim();
                        if (result.slice(0, 1) == 1)
                        {
                            $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                        }
                        else
                        {
                            $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                        }
                        $("#updateDialPlanPolicyDialog").html(result.slice(1));
                        $("#dialPlanPolicyDialog").dialog("close");
                        //$("html, body").animate({ scrollTop: 0 }, 600);
                        $("#updateDialPlanPolicyDialog").dialog("open");

                        $(":button:contains('Complete')").show().addClass('subButton');;
                        $(":button:contains('Cancel')").show().addClass('cancelButton');;
                        $(":button:contains('More Changes')").hide();
                        $(":button:contains('Return To Main')").hide();
                        $(":button:contains('Create')").hide();
                        // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                    }
      });  
	 	 
           },
            Close: function() {
                $(this).dialog("close");
            },              
            Cancel: function() {
                $(this).dialog("close");
            }
            

        },
        open: function() {
				setDialogDayNightMode($(this));
      	  	$(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
 		     	$(".ui-dialog-buttonpane button:contains('Create')").button().hide();
 		     	$(".ui-dialog-buttonpane button:contains('Close')").button().hide();
      	   	$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
      	    $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
      	   
      	    if(dialPlanPolicyPos == "Create"){
      		   $(".ui-dialog-buttonpane button:contains('Create')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      		}
      	   
      	   if(dialPlanPolicyPos == "Update"){
      		   $(".ui-dialog-buttonpane button:contains('Update')").button().show().addClass('subButton');;
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      	   }
      	   
      	   if(dialPlanPolicyPos == "Delete"){
      		   $(".ui-dialog-buttonpane button:contains('Confirm')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      	   }
       }
    });
	
	$("#domainDialog").dialog({
        autoOpen: false,
        width: 800,
        modal: true,
        title: "Domain Dialog",
        position: { my: "top", at: "top" },
        resizable: false,
        closeOnEscape: false,
        buttons: {
      	  Update: function() {
      		$("html, body").animate({ scrollTop: 0 }, 600);
            var dataToSend = $("form#dialPlanPolicyFormData").serializeArray();
            var actionData = $("#actionName").val();
            if(actionData == "added"){
            	var pageUrl = "dialPlanPolicyAddValidate.php";
            	isModify = "added";
            }else{
            	var pageUrl = "dialPlanPolicyModValidate.php";
            	isModify = "modify";
            }
             
            $.ajax({
              type: "POST",
              url: "enterprise/"+pageUrl,
              data: dataToSend,
              success: function(result)
              {

                  result = result.trim();
                  if (result.slice(0, 1) == 1)
                  {
                      $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                  }
                  else
                  {
                      $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                  }
                  $("#updateDialPlanPolicyDialog").html(result.slice(1));
                  $("#dialPlanPolicyDialog").dialog("close");
                  //$("html, body").animate({ scrollTop: 0 }, 600);
                  $("#updateDialPlanPolicyDialog").dialog("open");

                  $(":button:contains('Complete')").show().addClass('subButton');;
                  $(":button:contains('Cancel')").show().addClass('cancelButton');;
                  $(":button:contains('More Changes')").hide();
                  $(":button:contains('Return To Main')").hide();
                  $(":button:contains('Create')").hide();
                  $(":button:contains('Update')").hide();
                  // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
              }
});  
          	   
           },
          
          	Create: function() {
          		$("html, body").animate({ scrollTop: 0 }, 600);
                  var dataToSend = $("form#domainFormData").serializeArray();
                  var pageUrl = "domainsAddValidate.php"; 
                  $.ajax({
                    type: "POST",
                    url: "enterprise/"+pageUrl,
                    data: dataToSend,
                    success: function(result)
                    {

                        result = result.trim();
                        if (result.slice(0, 1) == 1)
                        {
                            $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                        }
                        else
                        {
                            $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                        }
                        $("#updateDomainDialog").html(result.slice(1));
                        $("#domainDialog").dialog("close");
                        //$("html, body").animate({ scrollTop: 0 }, 600);
                        $("#updateDomainDialog").dialog("open");

                        $(":button:contains('Complete')").show().addClass('subButton');;
                        $(":button:contains('Cancel')").show().addClass('cancelButton');;
                        $(":button:contains('More Changes')").hide();
                        $(":button:contains('Return To Main')").hide();
                        $(":button:contains('Create')").hide();
                        // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                    }
      });  
	 	 
           },
            Close: function() {
                $(this).dialog("close");
            },              
            Cancel: function() {
                $(this).dialog("close");
            }
            

        },
        open: function() {
				setDialogDayNightMode($(this));
      	  	$(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
 		     	$(".ui-dialog-buttonpane button:contains('Create')").button().hide();
 		     	$(".ui-dialog-buttonpane button:contains('Close')").button().hide();
      	   	$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
      	    $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
      	   
      	    if(domainPos == "Create"){
      		   $(".ui-dialog-buttonpane button:contains('Create')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      		}
      	   
      	   if(domainPos == "Update"){
      		   $(".ui-dialog-buttonpane button:contains('Update')").button().show().addClass('subButton');;
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      	   }
      	   
      	   if(domainPos == "Delete"){
      		   $(".ui-dialog-buttonpane button:contains('Confirm')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      	   }
       }
    });
	
	
	$("#updateDialPlanPolicyDialog").dialog({
        autoOpen: false,
        width: 800,
        modal: true,
        title: "Dial Plan Policy Dialog",
        position: { my: "top", at: "top" },
        resizable: false,
        closeOnEscape: false,
        buttons: {
      	  	
      	Create: function() {
                          
      	},
           Update: function() {
               
          	   
           },
          Confirm: function() {   
          	 
           },
           Complete: function(){
          	 if(isModify == "modify"){
          		dialPlanPolicyModify();
          	 }else{
          		 dialPlanPolicyAdd();
          	 }
          	 
           },
           Close: function() {
               $(this).dialog("close");
           },              
           Cancel: function() {
            $("#dialPlanPolicyDialog").dialog("open");
            $(this).dialog("close");
           }
           

        },
       
  	  open: function() {
				setDialogDayNightMode($(this));
      	  	$(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
 		     	$(".ui-dialog-buttonpane button:contains('Create')").button().hide();
 		     	$(".ui-dialog-buttonpane button:contains('Close')").button().hide();
      	   	$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
      	    $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
      	   
      	    if(dialPlanPolicyPos == "Create"){
      		   $(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      		}
      	   
      	   if(dialPlanPolicyPos == "Update"){
      		   $(".ui-dialog-buttonpane button:contains('Update')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      	   }
      	   
      	   if(dialPlanPolicyPos == "Delete"){
      		   $(".ui-dialog-buttonpane button:contains('Confirm')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      	   }
       }
     
    });
	
	$("#updateDomainDialog").dialog({
        autoOpen: false,
        width: 800,
        modal: true,
        title: "Domain Dialog",
        position: { my: "top", at: "top" },
        resizable: false,
        closeOnEscape: false,
        buttons: {
      	  	
      	Create: function() {
                          
      	},
           Update: function() {
               
          	   
           },
          Confirm: function() {   
          	 
           },
           Complete: function(){
        	   if(securtyDomainFlag == "Add"){
        		   securityDomainAdd();
        	   }else{
        		   domainAdd();
        	   }
        	   
          	 /*if(isModify == "modify"){
          		dialPlanPolicyModify();
          	 }else{
          		 dialPlanPolicyAdd();
          	 }*/
          	 
           },
           Close: function() {
               $(this).dialog("close");
           },              
           Cancel: function() {
            //$("#domainDialog").dialog("open");
            $(this).dialog("close");
           }
           

        },
       
  	  open: function() {
				setDialogDayNightMode($(this));
      	  	$(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
 		     	$(".ui-dialog-buttonpane button:contains('Create')").button().hide();
 		     	$(".ui-dialog-buttonpane button:contains('Close')").button().hide();
      	   	$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
      	    $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
      	   
      	    if(domainPos == "Create"){
      		   $(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      		}
      	   
      	   if(domainPos == "Update"){
      		   $(".ui-dialog-buttonpane button:contains('Update')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      	   }
      	   
      	   if(domainPos == "Delete"){
      		   $(".ui-dialog-buttonpane button:contains('Confirm')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      	   }
       }
     
    });
	
});


			
function calculatePasswordRules(){
	var finalValue = 0; 
	if ($('#restrictMinDigits').is(':checked')) {
		var num = $("#minDigits").val();
		finalValue = Number(finalValue) + Number(num);
	}
	if ($('#restrictMinUpperCaseLetters').is(':checked')) {
		var num1 = $("#minUpperCaseLetters").val();
		finalValue = Number(finalValue) + Number(num1);
	}
	if ($('#restrictMinLowerCaseLetters').is(':checked')) {
		var num2 = $("#minLowerCaseLetters").val();
		finalValue = Number(finalValue) + Number(num2);
	}
	if ($('#restrictMinNonAlphanumericCharacters').is(':checked')) {
		var num3 = $("#minNonAlphanumericCharacters").val();
		finalValue = Number(finalValue) + Number(num3);
	}
	var atleastVal = Number($("#minLength").val());
	if(finalValue > atleastVal){
		$("#minLength").val(finalValue);
	}
}

function calculateSIPPasswordRules(){
	var finalValue = 0; 
	if ($('#restrictMinDigits1').is(':checked')) {
		var num = $("#minDigits1").val();
		finalValue = Number(finalValue) + Number(num);
	}
	if ($('#restrictMinUpperCaseLetters1').is(':checked')) {
		var num1 = $("#minUpperCaseLetters1").val();
		finalValue = Number(finalValue) + Number(num1);
	}
	if ($('#restrictMinLowerCaseLetters1').is(':checked')) {
		var num2 = $("#minLowerCaseLetters1").val();
		finalValue = Number(finalValue) + Number(num2);
	}
	if ($('#restrictMinNonAlphanumericCharacters1').is(':checked')) {
		var num3 = $("#minNonAlphanumericCharacters1").val();
		finalValue = Number(finalValue) + Number(num3);
	}
	var atleastVal = Number($("#minLength1").val());
	if(finalValue > atleastVal){
		$("#minLength1").val(finalValue);
	}
}

function calculateDevicePasswordRules(){
	var finalValue = 0; 
	if ($('#allowAlternateNumbersForRedirectingIdentityD').is(':checked')) {
		var num = $("#minDigitsDevice").val();
		finalValue = Number(finalValue) + Number(num);
	}
	if ($('#restrictMinUpperCaseLettersDevice').is(':checked')) {
		var num1 = $("#minUpperCaseLettersDevice").val();
		finalValue = Number(finalValue) + Number(num1);
	}
	if ($('#restrictMinLowerCaseLettersDevice').is(':checked')) {
		var num2 = $("#minLowerCaseLettersDevice").val();
		finalValue = Number(finalValue) + Number(num2);
	}
	if ($('#restrictMinNonAlphanumericCharactersDevice').is(':checked')) {
		var num3 = $("#minNonAlphanumericCharactersDevice").val();
		finalValue = Number(finalValue) + Number(num3);
	}
	var atleastVal = Number($("#minLengthDevice").val());
	if(finalValue > atleastVal){
		$("#minLengthDevice").val(finalValue);
	}
}
