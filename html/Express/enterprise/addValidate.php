<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
$error = 0;
$data = $errorTableHeader;
//print_r($_POST);
if(count($_POST["basicInfo"]) > 0){
    foreach ($_POST["basicInfo"] as $key => $value)
    {
        $bg = CHANGED;
        
        if($key == "serviceProviderId" && $value == ""){
            $error = 1;
            $bg = INVALID;
            $value = "Enterprise ID can not ne blank.";
        }
        
        if($key == "contactEmail" && $value != ""){
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $error = 1;
                $bg = INVALID;
                $value = "Invalid email format.";
            }
        }
        
        if($key == "supportEmail" && $value != ""){
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                $error = 1;
                $bg = INVALID;
                $value = "Invalid email format.";
            }
        }
        
        
        if ($bg != UNCHANGED)
        {
            $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:" . $bg . ";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\">";
            if (is_array($value))
            {
                foreach ($value as $k => $v)
                {
                    $data .= $v . "<br>";
                }
            }
            else if ($value !== "")
            {
                $data .= $value;
            }
            else
            {
                $data .= "None";
            }
            $data .= "</td></tr>";
        }
    }
    
    if(count($_POST["clone"]) > 0 && $_POST["enterpriseCloneID"] != ""){
        //$_POST["clone"]["enterpriseCloneID"] = $_POST["enterpriseCloneID"];
        $_POST["clone"] = array('enterpriseCloneID' => $_POST["enterpriseCloneID"]) + $_POST["clone"];
        foreach ($_POST["clone"] as $key => $value)
        {
            $bg = CHANGED;
            
            if ($bg != UNCHANGED)
            {
                $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:" . $bg . ";\">" . $_SESSION ["enterpriseDialogArr"][$key] . "</td><td class=\"errorTableRows\">";
                if (is_array($value))
                {
                    foreach ($value as $k => $v)
                    {
                        $data .= $v . "<br>";
                    }
                }
                else if ($value !== "")
                {
                    $data .= $value;
                }
                else
                {
                    $data .= "None";
                }
                $data .= "</td></tr>";
            }
        }
    }else if(count($_POST["clone"]) == 0 && $_POST["enterpriseCloneID"] != ""){
        $error = 1;
        $bg = INVALID;
        //$value = "Invalid email format.";
        $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:" . $bg . ";\">Enterprise Cloning</td><td class=\"errorTableRows\">Enterprise cloning is required field</td></tr>";
    }
    
    $data .= "</table>";
    echo $error . $data;
}
?>