<?php
	require_once("/var/www/html/Express/config.php");
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/html/Express/enterprise/services/ServicesOperations.php");
	
	
	if( isset($_POST['action']) && $_POST['action'] == "getServices" ) {
			$serviceOP = new ServicesOperations($_POST['selectedSp']);
			$serviceTableData = $serviceOP->createServiceTableRows();
			if($serviceTableData->status == "failure") {
					$response = array("status" => "failure", "ErrorMsg" => $serviceTableData->ErrorMsg);
			} else {
				$response = array("groupServiceTable" => $serviceTableData->groupServiceTable, "userServiceTable" => $serviceTableData->userServiceTable);
			}
			
			echo json_encode($response);
			die;
	}

	if( isset($_POST['action']) && $_POST['action'] == "getServicePackGetServiceUsage" ) {
	    $tabelData = "";
	    $tableRow = "";
	    $serviceName = $_POST['serviceName'];
	    $serviceOP = new ServicesOperations($_POST['selectedSp']);
	    $serviceTableData = $serviceOP->getServicePackGetServiceUsageList($serviceName);
	    
	    if( empty($serviceTableData["Error"]) ) {
	        $serviceTableData = $serviceTableData["Success"];
	        if( count($serviceTableData) <= 0 ) {
	            $tableRow .= "<tr><td colspan=100%> No Data Found. </td></tr>";
	        } else {
	            foreach($serviceTableData as $key => $value) {
	                $tableRow .= "<tr>";
	                $tableRow .=   "<td>" . $value["Service Pack Name"] . " </td>";
	                $tableRow .=   "<td>" . $value["Total Packs"] . " </td>";
	                $tableRow .=   "<td>" . $value["Allocated to Groups"] . " .</td>";
	                $tableRow .= "</tr>";
	            }
	            
	        }
	        
	        $tabelData .= "<table id='servicePackGetServiceUsageTable' class='tablesorter scroll tagTableHeight dataTable' style='margin:0 auto;width:60% !important'>";
	        $tabelData .= "<thead><tr><th class='thsmall header'>Service Pack Name</th>";
	        $tabelData .= "<th class='thsmall header'>Total Packs</th>";
	        $tabelData .= "<th class='thsmall header'>Allocated to Groups</th></tr></thead>";
	        $tabelData .= "<tbody>" . $tableRow . "</tbody>";
	        $tabelData .= "</table>"; 
	        $response = $tabelData;
	    }
	    else {
	        $response = $serviceTableData["Error"];
	    }
	    
	    
	    echo json_encode($response);
	}
?>