<script type="text/javascript" src="enterprise/js/downloadAsCsv.js"></script>
<script src="enterprise/js/services.js"></script>
<html>
	<div style="display: block;" class="loading" id="loadingServices"><img src="/Express/images/ajax-loader.gif"></div>
	<div class="allServiceTable">
		<h2 class="adminUserText">Group Services</h2>
		<div class="row">
			<div class="col-md-6" style="padding-left: 28px;">
				<input type="checkbox" class="inputCheckbox" name="checkAllGroupService" id="checkAllGroupService">
				<label for="checkAllGroupService"><span></span></label>
				<label class="labelText" style="margin-top: 53px;"> Select All</label>
			</div>
			<div class="col-md-6">
				<div class="fcorn-register" style="">
				     <div class="dataTables_length limitSearch" id="" style="float:right;margin-top: 22px;margin-left: 25px;">
            			<!-- <label>Show<br> 
                		<select name="groupService_length" class="groupService_length">
                			<option value="20">20</option>
                			<option value="50">50</option>
                			<option value="100">100</option>
                			<option value="200">200</option>
                			<option value="All Services" selected>All Services</option>
                		</select> <br>Services</label> -->
                		<button type="button" id="expCollapse" value="Expand" class="cancelButton" style="width:100px;">
                		<span id="serviceFilterTitleName" style="color: inherit;">Expand</span>
						<span id="enterpriseServicesFiltersStatusTitleIcon" class="glyphicon glyphicon-chevron-up" style="color: inherit;"></span>
                		</button>
            		</div>
            		<div class="" id="downloadGroupServiceCSV" style="float:right;">
            			<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" id="downloadAdminCSV" style="cursor:pointer;">
            	 		<br><span>Download<br>CSV</span>
            		</div>
        		</div>
			</div>
		</div>
		<div class="enterpriseGroupServiceTableDiv" style="margin-bottom: 25px;">
			<table id="enterpriseGroupServiceTable"	class="tablesorter scroll dataTable" style="margin: 0">
				<thead>
					<tr>
    					<th class="thsmall header" style="width:10% !important;">Authorized</th>
    					<th class="thsmall header" style="width: 270px !important;">Group Services</th>
    					<th class="thsmall header">Limits</th>
    					<th class="thsmall header">Allocated</th>
					</tr>
				</thead>
				<tbody id="groupServiceTableBody" style="max-height:220px;transition:all 1s ease 0s;"></tbody>
			</table>
		</div>
		<h2 class="adminUserText">User Services</h2>
		<div class="row formspace" style="font-size: 12px;">
		</div>
		<div class="row">
			<div class="col-md-6" style="padding-left: 28px;">
				<input type="checkbox" class="inputCheckbox" name="checkAllUserService" id="checkAllUserService">
				<label for="checkAllUserService"><span></span></label>
				<label class="labelText" style="margin-top: 53px;"> Select All</label>
			</div>
			<div class="col-md-6">
				<div class="fcorn-register" style="">
					<div class="dataTables_length limitSearch" id="" style="float:right;margin-top: 22px;margin-left: 25px;">
            			<!-- <label>Show<br> 
                		<select name="userService_length" class="userService_length">
                			<option value="20">20</option>
                			<option value="50">50</option>
                			<option value="100">100</option>
                			<option value="200">200</option>
                			<option value="All Services" selected>All Services</option>
                		</select> <br>Services</label>-->
                		<button type="button" id="expCollapse1" value="Expand" class="cancelButton" style="width:100px;">
                		<span id="serviceFilterTitleName1" style="color: inherit;">Expand</span>
						<span id="enterpriseServicesFiltersStatusTitleIcon1" class="glyphicon glyphicon-chevron-up" style="color: inherit;"></span>
                		</button>
            		</div>
            		<div class="" id="downloadUserServiceCSV" style="float:right;">
            			<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" id="downloadAdminCSV" style="cursor:pointer;">
            	 		<br><span>Download<br>CSV</span>
            		</div>
        		</div>
			</div>
		</div>
		<div class="enterpriseUserServiceTableDiv" style="margin-bottom: 25px;">
			<table id="enterpriseUserServiceTable"
				class="tablesorter scroll dataTable" style="margin: 0;">
				<thead>
					<tr>
    					<th class="thsmall header" style = "width:10% !important;">Authorized</th>
    					<th class="thsmall header" style="width: 270px !important;">User Services</th>
    					<th class="thsmall header">Limits</th>
    					<th class="thsmall header">Allocated (Packs) </th>
					</tr>
				</thead>
				<tbody id="groupServiceTableBody1" style="max-height: 220px; transition: all 1s ease 0s;"></tbody>
			</table>
		</div>
	</div>
	
	<div id="servicePackGetServiceUsage" style="margin:0 auto;">
		<div style="display: block;" class="loading" id="servicePackGetServiceLoading"><img src="/Express/images/ajax-loader.gif"></div>
		<div id="servicePackTableDiv"></div>
	</div>
</html>