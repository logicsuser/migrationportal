<?php
	require_once ("/var/www/lib/broadsoft/adminPortal/enterprise/enterpriseOperations.php");
	
	class ServicesOperations {
		public $entId;
		public $serviceInputName = array("groupServices" => "Group Services",
                                		  "userServices" => "User Services",
                                	      "limited" => "Limit",
                                	      "quantity" => "Quantity",
                                	      "authorized" => "Authorized"
                                		);
		
		const EXCEEDS_LIMIT  = "The number of Account/Authorization Codes services in use exceeds the new limit.";
		const INVALID_VALUE = "Invalid Limit! Limit value should be 6 digit numeric value.";
		
		public function __construct($entId) {
			$this->entId = $entId;	
		}

		public function createServiceTableRows() {
				$response = new stdClass();
				$serviceData = $this->getEnterpriseServices();
				if($serviceData->status == "success") {
					$response->groupServiceTable = $this->createGroupServiceTableRow($serviceData->allServices["groupServices"]); 
					$response->userServiceTable = $this->createUserServiceTableRow($serviceData->allServices["userServices"]);
					$_SESSION['groupServices'] = $serviceData->allServices["groupServices"];
					$_SESSION['userServices'] = $serviceData->allServices["userServices"];
					//print_r($_SESSION['userServices']); exit;
					return $response;
				} else {
					return $serviceData;
				}
		}
		
		
		public function createGroupServiceTableRow($rowData) {
			$tableRow = "";
                        
                        $entPermServicesLogArr                     = explode("_", $_SESSION["permissions"]["entPermServices"]);
                        $entPermServicesLogPermission              = $entPermServicesLogArr[0];
                        $entPermServicesLogModPermission           = $entPermServicesLogArr[1];
                        
                        $cssStyle = "";
                        $disableField = "";
                        if($entPermServicesLogModPermission == "no"){
                            $cssStyle = "style='cursor:none !important; opacity:0.6;'";
                            $disableField = "disabled";
                        }
                        
                        
			foreach( $rowData as $key => $value) {
			    if( $value['licensed'] == "false") {
			         continue;    
			    }
			    
				$tableRow .= "<tr>";
					$checkboxName = "groupServicePackAuth[" . $value['serviceName'] . "]";
					$checkboxId = $value['serviceName'];
					$checkAuth = $value['authorized'] == "true" ? "checked" : "";
					$disabledAuth = ($value['allocated'] > 0 || $value['allocated'] == "Unlimited") ? "disabled" : "";
					$disableClass = $disabledAuth == "disabled" ? "disableService" : "";
					$tableRow .= "<td class='serWidthClass'> <input $cssStyle $disableField class='inputCheckbox groupServicePack' type='checkbox' name='$checkboxName' id='$checkboxId' value='false' $checkAuth $disabledAuth> <label class='$disableClass' for='$checkboxId'><span></span></label> </td>";
					$tableRow .= "<td style='width: 133px;'>" . $value['serviceName'] . "</td>";
					
					$checkboxLimitedName = "groupServicePackLimit[" . $value['serviceName'] . "]";
					$checkboxLimitedId =  "groupService_limited_". $value['serviceName'];
					$checkboxLimitedVal = $value['limited'];
					$selectedLimited = $value['limited'] == "Limited" ? "checked" : "";
					$disabledLimit = ($value['authorized'] == "false" || $value['allocated'] == "Unlimited") ? "disabled" : "";
					$disableClass = $disabledLimit == "disabled" ? "disableService" : "";
					$tableRow .= "<td> <input $cssStyle $disableField class='inputCheckbox groupServiceLimitCheckBox' type='checkbox' name='$checkboxLimitedName' id='$checkboxLimitedId' value='$checkboxLimitedVal' $selectedLimited $disabledLimit> <label class='$disableClass serCheckSpace $disableField' for='$checkboxLimitedId'><span></span></label>";
					
					$checkboxQntName = "groupServicePackQnt[" . $value['serviceName'] . "]";
					$checkboxQntId =  "groupService_Qnt_". $value['serviceName'];
					$disabledQnt = $value['limited'] == "Limited" ? "" : "disabled";
					$checkboxQntVal = $value['quantity'];
					$disabledClass = $disabledQnt == "disabled" ? "disableService" : "";
					$tableRow .= "<input $cssStyle $disableField class='$disabledClass serInputWidth quantity' type='number' name='$checkboxQntName' id='$checkboxQntId' value='$checkboxQntVal' $disabledQnt pattern='/^-?\d+\.?\d*$/' onKeyPress='if(this.value.length==6) return false;' > </td>";
					
					$tableRow .= "<td>" . $value['allocated'] . "</td>";
				$tableRow .= "</tr>";
			}
			return $tableRow;
		}
		
		public function createUserServiceTableRow($rowData) {
			$tableRow = "";
                        
                        $entPermServicesLogArr                     = explode("_", $_SESSION["permissions"]["entPermServices"]);
                        $entPermServicesLogPermission              = $entPermServicesLogArr[0];
                        $entPermServicesLogModPermission           = $entPermServicesLogArr[1];
                        
                        $cssStyle = "";
                        $disableField = "";
                        if($entPermServicesLogModPermission == "no"){
                            $cssStyle = "style='cursor:none !important; opacity: 0.6;'";
                            $disableField = "disabled";
                        }
                        
			foreach( $rowData as $key => $value) {
			    if( $value['licensed'] == "false") {
			        continue;
			    }
			    
				$tableRow .= "<tr>";
					$checkboxName = "userServicePackAuth[" . $value['serviceName'] . "]";
					$checkboxId = $value['serviceName'];
					$checkAuth = $value['authorized'] == "true" ? "checked" : "";
					$disabledAuth = ($value['allocated'] > 0 || $value['allocated'] == "Unlimited") ? "disabled" : "";
					$disabledClass = $disabledAuth == "disabled" ? "disableService" : "";
					$tableRow .= "<td class='serWidthClass'> <input $cssStyle $disableField class='inputCheckbox userServicePack' type='checkbox' name='$checkboxName' id='$checkboxId' value='false' $checkAuth $disabledAuth > <label class='$disabledClass' for='$checkboxId'><span></span></label> </td>";
					$tableRow .= "<td style='width: 133px;'><a class='userServiceLink' data-serviceName='" . $value['serviceName'] . "'>" . $value['serviceName'] . "</a></td>";
					
					$checkboxLimitedName = "userServicePackLimit[" . $value['serviceName'] . "]";
					$checkboxLimitedId =  "userService_limited_". $value['serviceName'];
					$checkboxLimitedVal = $value['limited'];
					$selectedLimited = $value['limited'] == "Limited" ? "checked" : "";
					$disabledLimit = ($value['authorized'] == "false" || $value['allocated'] == "Unlimited") ? "disabled" : "";
					$disabledClass = $disabledLimit == "disabled" ? "disableService" : "";
					$tableRow .= "<td> <input $cssStyle $disableField class='inputCheckbox userServiceLimitCheckBox' type='checkbox' name='$checkboxLimitedName' id='$checkboxLimitedId' value='$checkboxLimitedVal' $selectedLimited $disabledLimit> <label class='$disabledClass serCheckSpace $disableField' for='$checkboxLimitedId'><span></span></label>";
					
					$checkboxQntName = "userServicePackQnt[" . $value['serviceName'] . "]";
					$checkboxQntId =  "userService_Qnt_". $value['serviceName'];
					$disabledQnt = $value['limited'] == "Limited" ? "" : "disabled";
					$disabledClass = $disabledQnt == "disabled" ? "disableService" : "";
					$checkboxQntVal = $value['quantity'];
					$tableRow .= "<input $cssStyle $disableField class='$disabledClass serInputWidth quantity' type='number' name='$checkboxQntName' id='$checkboxQntId' value='$checkboxQntVal' $disabledQnt pattern='/^-?\d+\.?\d*$/' onKeyPress='if(this.value.length==6) return false;'' > </td>";
					
					$tableRow .= "<td>" . $value['allocated'] . " (" . $value['servicePackAllocation'] . ")" . "</td>";
				$tableRow .= "</tr>";
			}
			return $tableRow;
		}
		
		public function getEnterpriseServices() {
				$response = new stdClass();
				$entOp = new enterpriseOperations();
				$servicesRes = $entOp->serviceProviderServiceGetAuthorizationListRequest($this->entId);
				if( empty($servicesRes["Error"]) ) {
					$response->status = "success";
					$response->allServices = $servicesRes["Success"];
				} else {
					$response->status = "failure";
					$response->ErrorMsg = $servicesRes["Error"];
				}
				
				return $response;
		}
		
		public function explodeStringToArray($serviceData) {
		    $arrayData = explode(";;", rtrim($serviceData, ";;") );
		    $tempArray = array();
		    foreach($arrayData as $key => $value) {
		        $tempVar = explode("::", $value);
		        $tempArray[$tempVar[0]] = $tempVar[1];
		    }
		    return $tempArray;
		}
		
		public function createServiceData($POST) {
		    $tempServiceArray = array();
		    $POST['groupServicePackAuth'] = $this->explodeStringToArray($POST['groupServicePackAuth']);
		    $POST['groupServicePackLimit'] = $this->explodeStringToArray($POST['groupServicePackLimit']);
		    $POST['groupServicePackQnt'] = $this->explodeStringToArray($POST['groupServicePackQnt']);
		    $POST['userServicePackAuth'] = $this->explodeStringToArray($POST['userServicePackAuth']);
		    $POST['userServicePackLimit'] = $this->explodeStringToArray($POST['userServicePackLimit']);
		    $POST['userServicePackQnt'] = $this->explodeStringToArray($POST['userServicePackQnt']);
		    
		    foreach($POST['groupServicePackQnt'] as $key1 => $value1) {
		        $tempServiceArray['groupServices'][$key1]['authorized'] = $POST['groupServicePackAuth'][$key1];
		        $tempServiceArray['groupServices'][$key1]['limited'] = $POST['groupServicePackLimit'][$key1];
		        $tempServiceArray['groupServices'][$key1]['quantity'] = $value1;
		    }
		    
		    foreach($POST['userServicePackQnt'] as $key2 => $value2) {
		        $tempServiceArray['userServices'][$key2]['authorized'] = $POST['userServicePackAuth'][$key2];
		        $tempServiceArray['userServices'][$key2]['limited'] = $POST['userServicePackLimit'][$key2];
		        $tempServiceArray['userServices'][$key2]['quantity'] = $value2;
		    }
		    //$serviceOP = new ServicesOperations($_SESSION['sp']);
		    return $this->createModifiedServiceData($tempServiceArray);
		}
		
		public function createModifiedServiceData($allServices) {
		    $modifiedServicesToBeUpdate = array();
		    foreach($allServices['groupServices'] as $key3 => $value3) {
		        if($_SESSION['groupServices'][$key3]['authorized'] <> $value3['authorized']) {
		            $modifiedServicesToBeUpdate['groupServices'][$key3]['authorized'] = $value3['authorized'];
		        }
		        $oldLimitedValue = $_SESSION['groupServices'][$key3]['limited'] == "Limited" ? "true" : "false";
		        if($oldLimitedValue <> $value3['limited']) {
		            $modifiedServicesToBeUpdate['groupServices'][$key3]['limited'] = $value3['limited'];
		        }
		        if($_SESSION['groupServices'][$key3]['quantity'] <> $value3['quantity']) {
		            $modifiedServicesToBeUpdate['groupServices'][$key3]['quantity'] = $value3['quantity'];
		        }
		    }
		    
		    foreach($allServices['userServices'] as $key4 => $value4) {
		        if($_SESSION['userServices'][$key4]['authorized'] <> $value4['authorized']) {
		            $modifiedServicesToBeUpdate['userServices'][$key4]['authorized'] = $value4['authorized'];
		        }
		        $oldLimitedValue = $_SESSION['userServices'][$key4]['limited'] == "Limited" ? "true" : "false";
		        if($oldLimitedValue <> $value4['limited']) {
		            $modifiedServicesToBeUpdate['userServices'][$key4]['limited'] = $value4['limited'];
		        }
		        if($_SESSION['userServices'][$key4]['quantity'] <> $value4['quantity']) {
		            $modifiedServicesToBeUpdate['userServices'][$key4]['quantity'] = $value4['quantity'];
		        }
		    }
		    
		    return $modifiedServicesToBeUpdate;
		}
		
		public function createServiceChangeLogArray() {
		    $tempArray = array();
		    foreach($_SESSION['modifiedServicesToBeUpdate'] as $modSerKey => $modSerVal) {
		        foreach($modSerVal as $modSerK => $modSerV) {
		            $i = 0;
		            foreach($modSerV as $modSerK2 => $modSerV2) {
		                
		                if( $_SESSION[$modSerKey][$modSerK][$modSerK2] <> $modSerV2 && ($modSerK2 == "authorized" || $modSerK2 == "quantity") ) {
		                    $serviceType = $this->serviceInputName[$modSerKey];
		                    $fieldName = $modSerK . " (".$serviceType.") " . " : " . $this->serviceInputName[$modSerK2];
		                    
		                    if($modSerK2 == "authorized") {
		                        $modSerV2 = $modSerV2 == "true" ? "Authorized" : "Unauthorized";
		                        $oldAuthDetail = $_SESSION[$modSerKey][$modSerK][$modSerK2] == "true" ? "Authorized" : "Unauthorized";
		                    }
		                    if($modSerK2 == "quantity") {
		                        $modSerV2 = $modSerV2 != "" ? $modSerV2 : "Unlimited";
		                        $oldAuthDetail = $_SESSION[$modSerKey][$modSerK][$modSerK2] != "" ? $_SESSION[$modSerKey][$modSerK][$modSerK2] : "Unlimited";
		                    }
		                    $tempArray[$fieldName]['oldValue'] = $oldAuthDetail;
		                    $tempArray[$fieldName]['newValue'] = $modSerV2;
		                }
		             $i++;
		            }
		        }
		        
		    }
		    
		    return $tempArray;
		}
		
		
		public function getServicePackGetServiceUsageList($serviceName) {
		    $entOpResponse = "";
		    $entOp = new enterpriseOperations();
		    $entOpResponse = $entOp->ServiceProviderServicePackGetServiceUsageListRequest($this->entId, $serviceName);
		    return $entOpResponse;
		}
	}

?>