<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/enterprise/enterpriseOperations.php");
server_fail_over_debuggin_testing(); /* for fail Over testing. */

$obj = new EnterpriseOperations();
$delResp = $obj->deleteEnterprise($_POST["enterpriseId"]);
if(!empty($delResp["Error"])){
    echo "<p style='text-align:center;color:red;'>".$delResp["Error"]."</p>";
}else{
    echo "<p style='text-align:center;'>".$_POST["enterpriseId"].": Enterprise deleted succesfully.</p>";
}
?>