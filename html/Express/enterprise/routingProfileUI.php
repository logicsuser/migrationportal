<h2 class="adminUserText">Routing Profile</h2>

<div style="clear: right;">&nbsp;</div>

<div class="row" style="margin-bottom:200px;">
    <div class="col-md-12">
    	<div class="form-group">
    	<label class="labelText">Select Routing Profile:</label>
        	<div class="dropdown-wrap">
        		<select name="routingProfile[routingProfile]" id="routingProfile" style="">
        			<?php 
        			if (count($allResp["routingProfile"]["Success"]) > 0){
        			    $routes = "<option value=''>None</option>";
        			    foreach ($allResp["routingProfile"]["Success"] as $key => $value){
        			        if($allResp["selectedRoutingProfile"]["Success"] == $value){
        			            $selectedRoutes = "selected";
        			        }else{
        			            $selectedRoutes = "";
        			        }
        			        $routes .= "<option value='".$value."' ".$selectedRoutes.">".$value."</option>";
        			    }
        			    echo $routes;
        			}
        			?>								
        		</select>
        	</div>
    	</div>
    </div>

</div>