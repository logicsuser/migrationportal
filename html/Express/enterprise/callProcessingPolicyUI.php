<?php 
$callProcessResp = $allResp["callProcessingPolicy"]["Success"];
?>
<h2 class="adminUserText">Call Processing Policies</h2>
<h2 class="adminUserText" style="padding-top:40px;">Calling Line ID</h2>
<div style="clear: right;">&nbsp;</div>

<div class="row">
	<div class="col-md-7">
		<div class="form-group callProTextAlign">
			<label class="labelText fontHead labelMarginLeft">External Calls:</label>
		</div>
	</div>
	<div class="col-md-5" style="padding-right: 0px;">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[clidPolicy]" type="radio" <?php if($callProcessResp["clidPolicy"] == "Use DN"){echo "checked"; }?> value="Use DN" id="clidPolicy1"> <label for="clidPolicy1"><span></span></label><label class="labelText">Use user phone number for Calling Line
						Identity</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[clidPolicy]" type="radio" <?php if($callProcessResp["clidPolicy"] == "Use Configurable CLID"){echo "checked"; }?> value="Use Configurable CLID" id="clidPolicy2"><label for="clidPolicy2"><span></span></label> <label class="labelText">Use configurable CLID for Calling Line
						Identity</label>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-7">
		<div class="form-group callProTextAlign">
			<label class="labelText fontHead labelMarginLeft">Enterprise Calls:</label>
		</div>
	</div>
	<div class="col-md-5">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[enterpriseCallsCLIDPolicy]" type="radio" <?php if($callProcessResp["enterpriseCallsCLIDPolicy"] == "Use Extension"){echo "checked"; }?> value="Use Extension" id="enterpriseCallsCLIDPolicy1"> <label for="enterpriseCallsCLIDPolicy1"><span></span></label><label class="labelText">Use extension</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[enterpriseCallsCLIDPolicy]" type="radio" <?php if($callProcessResp["enterpriseCallsCLIDPolicy"] == "Use Location Code plus Extension"){echo "checked"; }?> value="Use Location Code plus Extension" id="enterpriseCallsCLIDPolicy2"><label for="enterpriseCallsCLIDPolicy2"><span></span></label> <label class="labelText">Use location code plus extension</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[enterpriseCallsCLIDPolicy]" type="radio" <?php if($callProcessResp["enterpriseCallsCLIDPolicy"] == "Use External Calls Policy"){echo "checked"; }?> value="Use External Calls Policy" id="enterpriseCallsCLIDPolicy3"><label for="enterpriseCallsCLIDPolicy3"><span></span></label> <label class="labelText">Use External Policy</label>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-7">
		<div class="form-group callProTextAlign">
			<label class="labelText fontHead labelMarginLeft">Group Calls:</label>
		</div>
	</div>
	<div class="col-md-5">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[groupCallsCLIDPolicy]" type="radio" <?php if($callProcessResp["groupCallsCLIDPolicy"] == "Use Extension"){echo "checked"; }?> value="Use Extension" id="groupCallsCLIDPolicy1"> <label for="groupCallsCLIDPolicy1"><span></span></label><label class="labelText">Use extension</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[groupCallsCLIDPolicy]" type="radio" <?php if($callProcessResp["groupCallsCLIDPolicy"] == "Use Location Code plus Extension"){echo "checked"; }?> value="Use Location Code plus Extension" id="groupCallsCLIDPolicy2"><label for="groupCallsCLIDPolicy2"><span></span></label> <label class="labelText">Use location code plus extension</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[groupCallsCLIDPolicy]" type="radio" <?php if($callProcessResp["groupCallsCLIDPolicy"] == "Use External Calls Policy"){echo "checked"; }?> value="Use External Calls Policy" id="groupCallsCLIDPolicy3"><label for="groupCallsCLIDPolicy3"><span></span></label> <label class="labelText">Use External Policy</label>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-7">
		<div class="form-group callProTextAlign">
			<label class="labelText fontHead labelMarginLeft">Emergency Calls:</label>
		</div>
	</div>
	<div class="col-md-5">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[emergencyClidPolicy]" type="radio" <?php if($callProcessResp["emergencyClidPolicy"] == "Use DN"){echo "checked"; }?> value="Use DN" id="emergencyClidPolicy1"> <label for="emergencyClidPolicy1"><span></span></label><label class="labelText">Use user phone number for Calling Line
						Identity</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[emergencyClidPolicy]" type="radio" <?php if($callProcessResp["emergencyClidPolicy"] == "Use Configurable CLID"){echo "checked"; }?> value="Use Configurable CLID" id="emergencyClidPolicy2"><label for="emergencyClidPolicy2"><span></span></label> <label class="labelText">Use configurable CLID for Calling Line
						Identity</label>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
			<?php 
			if($callProcessResp["allowAlternateNumbersForRedirectingIdentity"] == "true"){
			    $redirectIdentity = "checked";
			}else{
			    $redirectIdentity = "";
			}
			?>
			<input name="cpPolicy[allowAlternateNumbersForRedirectingIdentity]"  type="hidden" value="false">
				<input name="cpPolicy[allowAlternateNumbersForRedirectingIdentity]" id="allowAlternateNumbersForRedirectingIdentity" type="checkbox" value="true" <?php echo $redirectIdentity; ?>><label for="allowAlternateNumbersForRedirectingIdentity"><span></span></label>
				<label class="labelText">Allow Alternates Numbers for Redirecting Identity</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
			<?php 
			if($callProcessResp["allowConfigurableCLIDForRedirectingIdentity"] == "true"){
			    $redirectConfigIdentity = "checked";
			}else{
			    $redirectConfigIdentity = "";
			}
			?>
			<input name="cpPolicy[allowConfigurableCLIDForRedirectingIdentity]"  type="hidden" value="false">
				<input name="cpPolicy[allowConfigurableCLIDForRedirectingIdentity]" id="allowConfigurableCLIDForRedirectingIdentity" type="checkbox" value="true" <?php echo $redirectConfigIdentity; ?>><label for="allowConfigurableCLIDForRedirectingIdentity"><span></span></label>
				<label class="labelText">Allow Configurable CLID for Redirecting Identity</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
			<?php 
			if($callProcessResp["blockCallingNameForExternalCalls"] == "true"){
			    $blockCallingName = "checked";
			}else{
			    $blockCallingName = "";
			}
			?>
			<input name="cpPolicy[blockCallingNameForExternalCalls]"  type="hidden" value="false">
				<input name="cpPolicy[blockCallingNameForExternalCalls]" id="blockCallingNameForExternalCalls" type="checkbox" value="true" <?php echo $blockCallingName; ?>><label for="blockCallingNameForExternalCalls"><span></span></label>
				<label class="labelText">Block Calling Name for External Calls</label>
			</div>
		</div>
	</div>
</div>
<!-- 22222 -->
<?php 
if($ociVersion == "22"){ ?>
   <div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
			<input name="cpPolicy[useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable]"  type="hidden" value="false">
				<input name="cpPolicy[useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable]" id="useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable" type="checkbox" value="true" <?php if($callProcessResp["useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable"] == "true"){echo "checked"; }?>><label for="useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable"><span></span></label>
				<label class="labelText">Use User Phone Number for Enterprise Calls when Internal CLID Unavailable</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
			<input name="cpPolicy[useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable]"  type="hidden" value="false">
				<input name="cpPolicy[useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable]" id="useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable" type="checkbox" value="true" <?php if($callProcessResp["useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable"] == "true"){echo "checked"; }?>><label for="useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable"><span></span></label>
				<label class="labelText">Use User Phone Number for Group Calls when Internal CLID Unavailable</label>
			</div>
		</div>
	</div>
</div> 
<?php } ?>

<!-- 22222 -->
<h2 class="adminUserText" style="padding-top:40px;">Media</h2>
<div class="row">
	 
    	 <div class="col-md-7">
    	 
            <div class="form-group callProTextAlign">
                            <label class="labelText fontHead labelMarginLeft">Media Policy:</label>


                            <!--<div class="dropdown-wrap">
                                <select class="" id="" disabled>
                                <option>None</option>
                                </select>
                            </div>-->

            </div>
	</div>
	<div class="col-md-5">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[mediaPolicySelection]" type="radio" <?php if($callProcessResp["mediaPolicySelection"] == "Use Uncompressed Codec"){echo "checked"; }?> value="Use Uncompressed Codec" id="mediaPolicySelection1">
					<label for="mediaPolicySelection1"><span></span></label><label class="labelText">Force Use of Uncompressed Codec</label>
				</div>
			</div>
			<div class="row">
                            <div class="alignChkBox">    
                                <div class="col-md-7">
                                    <input name="cpPolicy[mediaPolicySelection]" disabled type="radio" <?php if($callProcessResp["mediaPolicySelection"] == "Use Supported Media Set"){echo "checked"; }?> value="Use Supported Media Set" id="mediaPolicySelection2">
                                    <label for="mediaPolicySelection2" style="opacity: 0.6"><span></span></label> <label class="labelText">Use Supported Media</label>
                                </div>
                            
                                <div class="col-md-5" sytle="margin-top:4px;">
                                    <div class="dropdown-wrap">
                                        <select class="" id="" disabled="">
                                            <option>None</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[mediaPolicySelection]" type="radio" <?php if($callProcessResp["mediaPolicySelection"] == "No Restrictions"){echo "checked"; }?> value="No Restrictions" id="mediaPolicySelection3">
					<label for="mediaPolicySelection3"><span></span></label> <label class="labelText">None</label>
				</div>
			</div>
		</div>
	</div>
</div>

<h2 class="adminUserText" style="padding-top:40px;">Call Limits</h2>

<div class="row">
	<div class="">
		<div class="col-md-7">
			<div class="form-group alignChkBox">
			<?php 
			if($callProcessResp["useMaxSimultaneousCalls"] == "true"){
			    $useSimCall = "checked";
			}else{
			    $useSimCall = "";
			}
			?>
			<input name="cpPolicy[useMaxSimultaneousCalls]"  type="hidden" value="false">
				<input name="cpPolicy[useMaxSimultaneousCalls]" id="useMaxSimultaneousCalls" type="checkbox" value="true" <?php echo $useSimCall; ?>><label for="useMaxSimultaneousCalls"><span></span></label> <label class="labelText">Enable Maximum Number of Concurrent Calls</label>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<input style="width:60% !important;" name="cpPolicy[maxSimultaneousCalls]" id="maxSimultaneousCalls" type="text" size="6" maxlength="6" value="<?php echo $callProcessResp["maxSimultaneousCalls"]; ?>">&nbsp;&nbsp;<label class="labelText">Calls</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-7">
			<div class="form-group alignChkBox">
			<?php 
			if($callProcessResp["useMaxSimultaneousVideoCalls"] == "true"){
			    $useSimVidCall = "checked";
			}else{
			    $useSimVidCall = "";
			}
			?>
			<input name="cpPolicy[useMaxSimultaneousVideoCalls]"  type="hidden" value="false">
				<input name="cpPolicy[useMaxSimultaneousVideoCalls]" id="useMaxSimultaneousVideoCalls" type="checkbox" value="true" <?php echo $useSimVidCall; ?>><label for="useMaxSimultaneousVideoCalls"><span></span></label>
				<label class="labelText">Enable Maximum Number of Concurrent
					Video Calls</label>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<input style="width:60% !important;" name="cpPolicy[maxSimultaneousVideoCalls]" id="maxSimultaneousVideoCalls" type="text" size="6" maxlength="6" value="<?php echo $callProcessResp["maxSimultaneousVideoCalls"]; ?>">&nbsp;&nbsp;<label class="labelText">Video
					Calls</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-7">
			<div class="form-group alignChkBox">
			<?php 
			if($callProcessResp["useMaxCallTimeForAnsweredCalls"] == "true"){
			    $usemaxtimecheck = "checked";
			}else{
			    $usemaxtimecheck = "";
			}
			?>
			<input name="cpPolicy[useMaxCallTimeForAnsweredCalls]"  type="hidden" value="false">
				<input name="cpPolicy[useMaxCallTimeForAnsweredCalls]" id="useMaxCallTimeForAnsweredCalls" type="checkbox" value="true" <?php echo $usemaxtimecheck; ?>><label for="useMaxCallTimeForAnsweredCalls"><span></span></label>
				<label class="labelText">Enable Maximum Duration for Answered
					Calls</label>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<input style="width:60% !important;" name="cpPolicy[maxCallTimeForAnsweredCallsMinutes]" id="maxCallTimeForAnsweredCallsMinutes" type="text" size="4" maxlength="4" value="<?php echo $callProcessResp["maxCallTimeForAnsweredCallsMinutes"]; ?>">&nbsp;&nbsp;<label class="labelText">Minutes</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-7">
			<div class="form-group alignChkBox">
			<?php 
			if($callProcessResp["useMaxCallTimeForUnansweredCalls"] == "true"){
			    $usemaxCallunans = "checked";
			}else{
			    $usemaxCallunans = "";
			}
			?>
			<input name="cpPolicy[useMaxCallTimeForUnansweredCalls]"  type="hidden" value="false">
				<input name="cpPolicy[useMaxCallTimeForUnansweredCalls]" id="useMaxCallTimeForUnansweredCalls" type="checkbox" value="true" <?php echo $usemaxCallunans; ?>><label for="useMaxCallTimeForUnansweredCalls"><span></span></label>
				<label class="labelText">Enable Maximum Duration for
					Unanswered Calls</label>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<input style="width:60% !important;" name="cpPolicy[maxCallTimeForUnansweredCallsMinutes]" id="maxCallTimeForUnansweredCallsMinutes" type="text" size="4" maxlength="4" value="<?php echo $callProcessResp["maxCallTimeForUnansweredCallsMinutes"]; ?>">&nbsp;&nbsp;<label class="labelText">Minutes</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-7">
			<div class="form-group alignChkBox">
			<?php 
			if($callProcessResp["useMaxConcurrentRedirectedCalls"] == "true"){
			    $usemaxCallRedirect = "checked";
			}else{
			    $usemaxCallRedirect = "";
			}
			?>
			<input name="cpPolicy[useMaxConcurrentRedirectedCalls]"  type="hidden" value="false">
				<input name="cpPolicy[useMaxConcurrentRedirectedCalls]" id="useMaxConcurrentRedirectedCalls" type="checkbox" value="true" <?php echo $usemaxCallRedirect; ?>><label for="useMaxConcurrentRedirectedCalls"><span></span></label>
				<label class="labelText">Enable Maximum Number of Concurrent
					Redirected Calls</label>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<input style="width:60% !important;" name="cpPolicy[maxConcurrentRedirectedCalls]" id="maxConcurrentRedirectedCalls" type="text" size="6" maxlength="6" value="<?php echo $callProcessResp["maxConcurrentRedirectedCalls"]; ?>">&nbsp;&nbsp;<label class="labelText">Calls</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-7">
			<div class="form-group alignChkBox">
			<?php 
			if($callProcessResp["useMaxConcurrentFindMeFollowMeInvocations"] == "true"){
			    $usemaxCallInvocations = "checked";
			}else{
			    $usemaxCallInvocations = "";
			}
			?>
			<input name="cpPolicy[useMaxConcurrentFindMeFollowMeInvocations]"  type="hidden" value="false">
				<input name="cpPolicy[useMaxConcurrentFindMeFollowMeInvocations]" id="useMaxConcurrentFindMeFollowMeInvocations" type="checkbox" value="true" <?php echo $usemaxCallInvocations; ?>><label for="useMaxConcurrentFindMeFollowMeInvocations" style="vertical-align: top;"><span></span></label>
				<label class="labelText">Enable Maximum Number of Concurrent <br>
					Find Me/Follow Me Invocations</label>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<input style="width:60% !important;" name="cpPolicy[maxConcurrentFindMeFollowMeInvocations]" id="maxConcurrentFindMeFollowMeInvocations" type="text" size="3" maxlength="3" value="<?php echo $callProcessResp["maxConcurrentFindMeFollowMeInvocations"]; ?>">&nbsp;&nbsp;<label class="labelText">Invocations</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-7">
			<div class="form-group alignChkBox">
			<?php 
			if($callProcessResp["useMaxFindMeFollowMeDepth"] == "true"){
			    $usefollowmedepth = "checked";
			}else{
			    $usefollowmedepth = "";
			}
			?>
			<input name="cpPolicy[useMaxFindMeFollowMeDepth]"  type="hidden" value="false">
				<input name="cpPolicy[useMaxFindMeFollowMeDepth]" id="useMaxFindMeFollowMeDepth" type="checkbox" value="true" <?php echo $usefollowmedepth; ?>><label for="useMaxFindMeFollowMeDepth"><span></span></label> <label class="labelText">Enable Maximum Find Me/Follow Me Depth</label>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<input style="width:60% !important;margin-top:0;" name="cpPolicy[maxFindMeFollowMeDepth]" id="maxFindMeFollowMeDepth" type="text" size="3" maxlength="3" value="<?php echo $callProcessResp["maxFindMeFollowMeDepth"]?>">
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-7">
				<label style="margin-left:0;" class="labelText">Maximum Redirection Depth:</label>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<input style="width:60% !important;" name="cpPolicy[maxRedirectionDepth]" id="maxRedirectionDepth" type="text" size="3" maxlength="3" value="<?php echo $callProcessResp["maxRedirectionDepth"]; ?>">
			</div>
		</div>
	</div>
</div>

<h2 class="adminUserText" style="padding-top:40px;">Conferencing</h2>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox marginuriclass">
				<label style="padding-left: 32px;" class="labelText">Use Conference URI Settings</label>
			</div>
			<div class="form-group alignChkBox ">
					<input name="cpPolicy[useSettingLevel]" id="useSettingLevel1" type="radio" value="System" <?php if($callProcessResp["useSettingLevel"] == "System"){echo "checked"; }?>>
					<label for="useSettingLevel1"><span></span></label> <label class="labelText" style="margin-bottom:0;">System</label> &nbsp;&nbsp;&nbsp;&nbsp;
					<input name="cpPolicy[useSettingLevel]" id="useSettingLevel2" type="radio" value="Service Provider" <?php if($callProcessResp["useSettingLevel"] == "Service Provider"){echo "checked"; }?>>
					<label for="useSettingLevel2"><span></span></label> <label class="labelText" style="margin-bottom:0;">Service Provider</label>
				</div>
		</div>
		
	</div>
</div>

<div class="row">
	<div class="">
	<?php 
	$explodedVal = explode("@",$callProcessResp["conferenceURI"]);
	
	?>
		<div class="col-md-7">
			<div class="form-group alignChkBox marginuriclass">
				<label style="padding-left: 32px;" class="labelText">Conference-URI sip</label>
			</div>
			<div class="form-group">
				<input style="width:180px !important;" name="cpPolicy[conferenceURI]" id="conferenceURI" type="text" value="<?php echo $explodedVal[0]; ?>">
				<span style="float:right;color:#6ea0dc;">@</span>
			</div>
			
		</div>
		<div class="col-md-5">
		<div class="form-group">
			<div class="dropdown-wrap">   
				<select name="cpPolicy[conferenceURIDomain]" id="conferenceURIDomain">
				<?php 
				if(count($enterpriseDomainsList["Success"]["domains"]) > 0){
				    $domainVal = "";
				    foreach ($enterpriseDomainsList["Success"]["domains"] as $key => $value){
				        $sel = "";
				        if($value == $explodedVal[1]){
				            $sel="selected";
				        }
				        $domainVal .= "<option value='".$value."' $sel>".$value."</option>"; 
				    }
				    echo $domainVal;
				}
				?>
				</select>
			</div>
			
		</div>
		</div>
		
	</div>
</div>

<h2 class="adminUserText" style="padding-top:40px;">Translation and Routing</h2>
<div class="row">
	<div class="col-md-7">
    	<div class="col-md-3">
    	
    	</div>
    	<div class="col-md-9">
			<label class="labelText fontHead cpMarginBottom">Network Usage:</label>
			
    	</div>
		
	</div>
	<div class="col-md-5">
		<div class="form-group">
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[networkUsageSelection]" type="radio" <?php if($callProcessResp["networkUsageSelection"] == "Force All Calls"){echo "checked"; }?> value="Force All Calls" id="networkUsageSelection1"> <label for="networkUsageSelection1"><span></span></label><label class="labelText">Force All Calls to use the Network</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[networkUsageSelection]" type="radio" <?php if($callProcessResp["networkUsageSelection"] == "Force All Except Extension and Location Calls"){echo "checked"; }?> value="Force All Except Extension and Location Calls" id="networkUsageSelection2"><label for="networkUsageSelection2"><span></span></label> <label class="labelText">Force All Calls to the Network except<br />extensions/location</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 alignChkBox">
					<input name="cpPolicy[networkUsageSelection]" type="radio" <?php if($callProcessResp["networkUsageSelection"] == "Do Not Force Enterprise and Group Calls"){echo "checked"; }?> value="Do Not Force Enterprise and Group Calls" id="networkUsageSelection3"><label for="networkUsageSelection3"><span></span></label> <label class="labelText">Dont Force Enterprise/Group<br />Calls to the Network</label>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-6">
			<div class="form-group alignChkBox">
			<input name="cpPolicy[enableEnterpriseExtensionDialing]"  type="hidden" value="false">
				<input name="cpPolicy[enableEnterpriseExtensionDialing]" id="enableEnterpriseExtensionDialing" type="checkbox" value="true" <?php if($callProcessResp["enableEnterpriseExtensionDialing"] == "true"){echo "checked"; }?>><label for="enableEnterpriseExtensionDialing"><span></span></label>
				<label class="labelText">Enable Enterprise Extension Dialing</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-6">
			<div class="form-group alignChkBox">
			<input name="cpPolicy[enforceGroupCallingLineIdentityRestriction]"  type="hidden" value="false">
				<input name="cpPolicy[enforceGroupCallingLineIdentityRestriction]" id="enforceGroupCallingLineIdentityRestriction" type="checkbox" value="true" <?php if($callProcessResp["enforceGroupCallingLineIdentityRestriction"] == "true"){echo "checked"; }?>><label for="enforceGroupCallingLineIdentityRestriction"><span></span></label>
				<label class="labelText">Enforce Group Calling Line Identity Restriction</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-6">
			<div class="form-group alignChkBox">
			<input name="cpPolicy[enforceEnterpriseCallingLineIdentityRestriction]"  type="hidden" value="false">
				<input name="cpPolicy[enforceEnterpriseCallingLineIdentityRestriction]" id="enforceEnterpriseCallingLineIdentityRestriction" type="checkbox" value="true" <?php if($callProcessResp["enforceEnterpriseCallingLineIdentityRestriction"] == "true"){echo "checked"; }?>><label for="enforceEnterpriseCallingLineIdentityRestriction"><span></span></label>
				<label class="labelText">Enforce Enterprise Calling Line Identity Restriction</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-6">
			<div class="form-group alignChkBox">
			<input name="cpPolicy[allowEnterpriseGroupCallTypingForPrivateDialingPlan]"  type="hidden" value="false">
				<input name="cpPolicy[allowEnterpriseGroupCallTypingForPrivateDialingPlan]" id="allowEnterpriseGroupCallTypingForPrivateDialingPlan" type="checkbox" value="true" <?php if($callProcessResp["allowEnterpriseGroupCallTypingForPrivateDialingPlan"] == "true"){echo "checked"; }?>><label for="allowEnterpriseGroupCallTypingForPrivateDialingPlan"><span></span></label>
				<label class="labelText">Allow Enterprise/Group Call Typing For Private Dialing Plan</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-6">
			<div class="form-group alignChkBox">
			<input name="cpPolicy[allowEnterpriseGroupCallTypingForPublicDialingPlan]"  type="hidden" value="false">
				<input name="cpPolicy[allowEnterpriseGroupCallTypingForPublicDialingPlan]" id="allowEnterpriseGroupCallTypingForPublicDialingPlan" type="checkbox" value="true" <?php if($callProcessResp["allowEnterpriseGroupCallTypingForPublicDialingPlan"] == "true"){echo "checked"; }?>><label for="allowEnterpriseGroupCallTypingForPublicDialingPlan"><span></span></label>
				<label class="labelText">Allow Enterprise/Group Call Typing For Public Dialing Plan</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-6">
			<div class="form-group alignChkBox">
			<input name="cpPolicy[overrideCLIDRestrictionForPrivateCallCategory]"  type="hidden" value="false">
				<input name="cpPolicy[overrideCLIDRestrictionForPrivateCallCategory]" id="overrideCLIDRestrictionForPrivateCallCategory" type="checkbox" value="true" <?php if($callProcessResp["overrideCLIDRestrictionForPrivateCallCategory"] == "true"){echo "checked"; }?>><label for="overrideCLIDRestrictionForPrivateCallCategory"><span></span></label>
				<label class="labelText">Override CLID Restriction For Private Call Category</label>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-6">
			<div class="form-group alignChkBox">
			<input name="cpPolicy[useEnterpriseCLIDForPrivateCallCategory]"  type="hidden" value="false">
				<input name="cpPolicy[useEnterpriseCLIDForPrivateCallCategory]" id="useEnterpriseCLIDForPrivateCallCategory" type="checkbox" value="true" <?php if($callProcessResp["useEnterpriseCLIDForPrivateCallCategory"] == "true"){echo "checked"; }?>><label for="useEnterpriseCLIDForPrivateCallCategory"><span></span></label>
				<label class="labelText">Use Enterprise CLID For Private Call Category</label>
			</div>
		</div>
	</div>
</div>

<h2 class="adminUserText" style="padding-top:40px;">Disable Caller ID</h2>

<div class="row">
	<div class="">
		<div class="col-md-12">
			
			<div class="form-group alignChkBox " style="padding-left:44px;">
						<input name="cpPolicy[useServiceProviderDCLIDSetting]" id="useServiceProviderDCLIDSetting1" type="radio" value="true" <?php if($callProcessResp["useServiceProviderDCLIDSetting"] == "true"){echo "checked"; }?>>
						<label for="useServiceProviderDCLIDSetting1"><span></span></label> <label class="labelText" style="margin-bottom:0;">Use Enterprise Dialable Caller ID Policy</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<input name="cpPolicy[useServiceProviderDCLIDSetting]" id="useServiceProviderDCLIDSetting2" type="radio" value="false" <?php if($callProcessResp["useServiceProviderDCLIDSetting"] == "false"){echo "checked"; }?>>
						<label style="padding-left: 35px;" for="useServiceProviderDCLIDSetting2"><span></span></label> <label class="labelText" style="margin-bottom:0;">Use System Dialable Caller ID Policy</label>
				</div>
		</div>
		
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox marginuriclass" style="width:185px;">
				<label style="padding-left: 32px;" class="labelText">Disable Caller ID</label>
			</div>
			<div class="form-group alignChkBox ">
					<input name="cpPolicy[enableDialableCallerID]" id="enableDialableCallerID1" type="radio" value="true" <?php if($callProcessResp["enableDialableCallerID"] == "true"){echo "checked"; }?>>
						<label for="enableDialableCallerID1"><span></span></label> <label class="labelText" style="margin-bottom:0;">On</label> &nbsp;&nbsp;&nbsp;&nbsp;
						<input name="cpPolicy[enableDialableCallerID]" id="enableDialableCallerID2" type="radio" value="false" <?php if($callProcessResp["enableDialableCallerID"] == "false"){echo "checked"; }?>>
						<label style="padding-left:50px;" for="enableDialableCallerID2"><span></span></label> <label class="labelText" style="margin-bottom:0;">Off</label>
				</div>
		</div>
		
	</div>
</div>
<?php if($ociVersion == "22"){ ?>
    <h2 class="adminUserText" style="padding-top:40px;">Phone List Lookup</h2>

<div class="row">
	<div class="">
		<div class="col-md-8">
			<div class="form-group alignChkBox">
			<input name="cpPolicy[enablePhoneListLookup]"  type="hidden" value="false">
				<input name="cpPolicy[enablePhoneListLookup]" id="enablePhoneListLookup" type="checkbox" value="true" <?php if($callProcessResp["enablePhoneListLookup"] == "true"){echo "checked"; }?>><label for="enablePhoneListLookup"><span></span></label>
				<label class="labelText">Enable Phone List Lookup</label>
			</div>
		</div>
	</div>
</div>
<?php }?>














	
	

	

		



