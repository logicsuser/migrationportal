<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/enterprise/enterpriseOperations.php");
$obj = new EnterpriseOperations();
$domainList = $obj->getAllSystemDomain();

$_SESSION ["domainsDialogContent"] = array (
    "domain" => "Domain"
);
$data = $errorTableHeader;
$error = 0;
//print_r($_POST["dataToSend"]);
if(count($_POST["dataToSend"]) > 0){
    foreach ($_POST["dataToSend"] as $key => $value)
    {
        $bg = CHANGED;
        
        if($value == ""){
            $error = 1;
            $bg = INVALID;
            $value = "Domain can not be empty.";
        }else if($value != ""){
            //print_r($securityDomainPattern);
            $domainList = $obj->getAllSystemDomain();
            if(in_array($value, $domainList["Success"]["domain"]) || in_array($value, $domainList["Success"]["systemDefaultDomain"])){
                $error = 1;
                $bg = INVALID;
                $value = $value." : Domain already exist.";
            }
            else if(strpos($value, $securityDomainPattern) === false){
                //print_r($securityDomainPattern);
                //print_r($value);
                $error = 1;
                $bg = INVALID;
                $value = $value." : Security Domain pattern not matching.";
            }
        }
        
        $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:" . $bg . ";\">Security Domain Name</td><td class=\"errorTableRows\">";
        if (is_array($value))
        {
            foreach ($value as $k => $v)
            {
                $data .= $v . "<br>";
            }
        }
        else if ($value !== "")
        {
            $data .= $value;
        }
        else
        {
            $data .= "None";
        }
        $data .= "</td></tr>";
    }
}else{
    $error = 1;
    $bg = INVALID;
    $value = "Domain can not be empty.";
    
    $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:" . $bg . ";\">Domain Name</td><td class=\"errorTableRows\">Domain name can not be empty.</td>";
}

$data .= "</table>";
echo $error . $data;
?>