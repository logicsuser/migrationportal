<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
$_SESSION ["dialPlanPolicyDialogContent"] = array (
    "accessCode"                  => "Access Code",
    "description"             => "Description",
    "includeCodeForNetworkTranslationsAndRouting"     => "Include Code for Network Translations and Routing",
    "includeCodeForScreeningServices"      => "Include Code for Screening Services",
    "enableSecondaryDialTone"      => "Enable Secondary Dial Tone"
);
$data = $errorTableHeader;
$error = 0;
foreach ($_POST as $key => $value)
{
    $bg = CHANGED;
    if($key == "accessCode"){
        if($value == ""){
            $error = 1;
            $bg = INVALID;
            $value = "Access code can not be empty.";
        }else if(!preg_match("/^[0-9*#+]+$/", $value)){
            $error = 1;
            $bg = INVALID;
            $value = "The access code can only contain one of the following charactors: 0 through 9, * or #.";
        }
    }
    $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:" . $bg . ";\">" . $_SESSION ["dialPlanPolicyDialogContent"][$key] . "</td><td class=\"errorTableRows\">";
    if (is_array($value))
    {
        foreach ($value as $k => $v)
        {
            $data .= $v . "<br>";
        }
    }
    else if ($value !== "")
    {
        $data .= $value;
    }
    else
    {
        $data .= "None";
    }
    $data .= "</td></tr>";
}
$data .= "</table>";
echo $error . $data;
?>