<?php 
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/enterprise/enterpriseOperations.php");

unset($_SESSION['modifiedAdmins']);
unset($_SESSION['modifiedAdministratorOldData']);
unset($_SESSION['addedAdmins']);
unset($_SESSION['deleteAdmins']);
unset($_SESSION['enterpriseDns']);
unset($_SESSION["dialPlanPolicyToAdd"]);
unset($_SESSION["allDialPlanListToAdd"]);
unset($_SESSION["dialPlanAccessCodeModifyData"]);
unset($_SESSION["domainsAdd"]);
unset($_SESSION["securityDomainsAdd"]);

//uset dns
/*
if(isset($_SESSION['oldAssignDNsRange']) && !empty($_SESSION['oldAssignDNsRange'])){
    $_SESSION['oldAssignDNsRange'] = array();
    unset($_SESSION['oldAssignDNsRange']);
    
    
}


if(isset($_SESSION['modifyDNsRangeArr']) && !empty($_SESSION['modifyDNsRangeArr'])){
    $_SESSION['modifyDNsRangeArr'] = array();
    unset($_SESSION['modifyDNsRangeArr']);
} */
//end 
$obj = new EnterpriseOperations();
$systemDoamins = $obj->getAllSystemDomain();
$statList = $obj->getAllStateProvince();

require_once("/var/www/html/Express/enterprise/getEnterpriseAllInformations.php");
if(!empty($allResp["basicInfo"]["Error"])){
    echo "<p style='text-align:center;color:red;padding-top:20px;'>".$allResp["basicInfo"]["Error"]."</p>";
    die;
}
//pemission 

//echo "<br />Voice Portal Context - ".$voicePortalContext; 

// Express Administrator  //Currently not using here
    $entPermExpressAdminsLogArr                = explode("_", $_SESSION["permissions"]["entPermExpressAdministrators"]);
    $entPermExpressAdminsLogPermission         = $entPermExpressAdminsLogArr[0];
    $entPermExpressAdminsLogModPermission      = $entPermExpressAdminsLogArr[1];
    $entPermExpressAdminsLogAddDelPermission   = $entPermExpressAdminsLogArr[2];

    /* Basic Info Permission */

    $basicInfoEntLogsArr                       = explode("_", $_SESSION["permissions"]["entPermBasicInformation"]);
    $basicInfoEntLogPermission                 = $basicInfoEntLogsArr[0];
    $basicInfoEntLogModPermission              = $basicInfoEntLogsArr[1];
    $basicInfoEntLogAddDelPermission           = $basicInfoEntLogsArr[2];  //Done

    // Domain Permisson 
    $entPermDomainsLogArr                      = explode("_", $_SESSION["permissions"]["entPermDomains"]);
    $entPermDomainsLogPermission               = $entPermDomainsLogArr[0];
    $entPermDomainsLogModPermission            = $entPermDomainsLogArr[1];
    $entPermDomainsLogAddDelPermission         = $entPermDomainsLogArr[2];  //Need to implement Another place 

    // DNs Permission 
    $entPermDNsLogArr                          = explode("_", $_SESSION["permissions"]["entPermDNs"]);
    $entPermDNsLogPermission                   = $entPermDNsLogArr[0];
    $entPermDNsLogModPermission                = $entPermDNsLogArr[1];
    $entPermDNsLogAddDelPermission             = $entPermDNsLogArr[2];   //Done

    // BW Administrator
    $entPermBWAdminsLogArr                     = explode("_", $_SESSION["permissions"]["entPermBWAdministrators"]);
    $entPermBWAdminsLogPermission              = $entPermBWAdminsLogArr[0];
    $entPermBWAdminsLogModPermission           = $entPermBWAdminsLogArr[1];
    $entPermBWAdminsLogAddDelPermission        = $entPermBWAdminsLogArr[2];

    // Security Domains
    $entPermSecurityDomainsLogArr              = explode("_", $_SESSION["permissions"]["entPermSecurityDomains"]);
    $entPermSecurityDomainsLogPermission       = $entPermSecurityDomainsLogArr[0];
    $entPermSecurityDomainsLogModPermission    = $entPermSecurityDomainsLogArr[1];

    // Services Permission
    $entPermServicesLogArr                     = explode("_", $_SESSION["permissions"]["entPermServices"]);
    $entPermServicesLogPermission              = $entPermServicesLogArr[0];
    $entPermServicesLogModPermission           = $entPermServicesLogArr[1];

    // Call Processing Policies 
    $entPermCallPrcsPoliciesLogArr             = explode("_", $_SESSION["permissions"]["entPermCallProcessingPolicies"]);
    $entPermCallPrcsPoliciesLogPermission      = $entPermCallPrcsPoliciesLogArr[0];
    $entPermCallPrcsPoliciesLogModPermission   = $entPermCallPrcsPoliciesLogArr[1];

    // Dial Plan Policy
    $entPermDialPlanPolicyLogArr               = explode("_", $_SESSION["permissions"]["entPermDialPlanPolicy"]);
    $entPermDialPlanPolicyLogPermission        = $entPermDialPlanPolicyLogArr[0];
    $entPermDialPlanPolicyLogModPermission     = $entPermDialPlanPolicyLogArr[1];

    // Voice Messaging
    $entPermVoiceMessagingLogArr               = explode("_", $_SESSION["permissions"]["entPermVoiceMessaging"]);
    $entPermVoiceMessagingLogPermission        = $entPermVoiceMessagingLogArr[0];
    $entPermVoiceMessagingLogModPermission     = $entPermVoiceMessagingLogArr[1];

    // Voice Portal
    $entPermVoicePortalLogArr                  = explode("_", $_SESSION["permissions"]["entPermVoicePortal"]);
    $entPermVoicePortalLogPermission           = $entPermVoicePortalLogArr[0];
    $entPermVoicePortalLogModPermission        = $entPermVoicePortalLogArr[1];

    // Network Class of Services Permission
    $entPermNCOSLogArr                         = explode("_", $_SESSION["permissions"]["entPermNetworkClassesofService"]);
    $entPermNCOSLogPermission                  = $entPermNCOSLogArr[0];
    $entPermNCOSLogModPermission               = $entPermNCOSLogArr[1];


    // Routing Profile
    $entPermRoutingProfileLogArr               = explode("_", $_SESSION["permissions"]["entPermRoutingProfile"]);
    $entPermRoutingProfileLogPermission        = $entPermRoutingProfileLogArr[0];
    $entPermRoutingProfileLogModPermission     = $entPermRoutingProfileLogArr[1];

    // Passwords Rules 
    $entPermPasswordRulesLogArr                = explode("_", $_SESSION["permissions"]["entPermPasswordRules"]);
    $entPermPasswordRulesLogPermission         = $entPermPasswordRulesLogArr[0];
    $entPermPasswordRulesLogModPermission      = $entPermPasswordRulesLogArr[1];
    //End code
    
    
    $enterprisePermissions      = (($basicInfoEntLogPermission == "1" || $entPermDomainsLogPermission == "1"
                                            || $entPermDNsLogPermission == "1" || $entPermBWAdminsLogPermission == "1"
                                            || $entPermSecurityDomainsLogPermission == "1" || $entPermServicesLogPermission == "1"
                                            || $entPermCallPrcsPoliciesLogPermission == "1" || $entPermDialPlanPolicyLogPermission == "1"
                                            || $entPermVoiceMessagingLogPermission == "1" || $entPermVoicePortalLogPermission == "1"
                                            || $entPermNCOSLogPermission == "1" || $entPermRoutingProfileLogPermission == "1"
                                            || $entPermPasswordRulesLogPermission == "1")
                                          );
    
    $enterpriseModPermissions   = (($basicInfoEntLogModPermission == "yes" || $entPermDomainsLogModPermission == "yes"
                                                || $entPermDNsLogModPermission == "yes" || $entPermBWAdminsLogModPermission == "yes"
                                                || $entPermSecurityDomainsLogPermission == "yes" || $entPermServicesLogModPermission == "yes"
                                                || $entPermCallPrcsPoliciesLogModPermission == "yes" || $entPermDialPlanPolicyLogModPermission == "yes"
                                                || $entPermVoiceMessagingLogModPermission == "yes" || $entPermVoicePortalLogModPermission == "yes"
                                                || $entPermNCOSLogModPermission == "yes" || $entPermRoutingProfileLogModPermission == "yes"
                                                || $entPermPasswordRulesLogModPermission == "yes")
                                              );

/*
$entBasicInfoPermission =   ((isset($_SESSION["permissions"]["groupBasicInfo"]) || $basicInformationLogPermission == "1"));
$entDomainPermission    =   ((isset($_SESSION["permissions"]["groupDomains"]) && $_SESSION["permissions"]["groupDomains"] == "1") || ($domainsLogPermission == "1" || $domainsModPermission == "yes")) ;
$entServicesPermission  =   ((isset($_SESSION["permissions"]["groupServices"]) && $_SESSION["permissions"]["groupServices"] == "1") ||($servicesLogPermission == "1"));
$entDnsPermission       =   ((isset($_SESSION["permissions"]["groupDN"]) && $_SESSION["permissions"]["groupDN"] == "1") || ($dnsLogPermission == "1"));
$entNCOSPermission      =   ((isset($_SESSION["permissions"]["groupNCOS"]) && $_SESSION["permissions"]["groupNCOS"] == "1") || ($networkClassesofServiceLogPermission == "1"));
$entCallProPoliciesPermission       =   ((isset($_SESSION["permissions"]["groupPolicies"]) && $_SESSION["permissions"]["groupPolicies"] == "1") || ($callProcessingPoliciesLogPermission == "1"));
$entVoicePortalServicePermission =  ((isset($_SESSION["permissions"]["groupVoicePortal"]) && $_SESSION["permissions"]["groupVoicePortal"] == "1") || ($voicePortalServiceLogPermission == "1"));
*/

 
?>

<style>   
.disabledulli{
cursor:none !important;
opacity:0.4;
}
.enabledulli{
opacity:1 !important;
}
</style>
<script src="enterprise/js/enterprise.js"></script>
<!--if modify permission access condition not apply then fields are disable -->
<script type="text/javascript">
	$(document).ready(function(){
                $('[data-toggle="tooltip"]').tooltip();         
                //Code added @ 18 March 2019
                if ("<?php echo $basicInfoEntLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#basicInfoData_1 :input").css("background", "#e3e3e3");
                            $("#basicInfoData_1 :input[type=checkbox]").addClass('disabledulli');
                            $("#basicInfoData_1 :input[type=radio]").addClass('disabledulli');
                            $("#basicInfoData_1 span").addClass('disabledulli');
                            $("#basicInfoData_1 :input").prop("disabled", true);
                            $("#basicInfoData_1 option").prop("disabled", true);
                            $("#basicInfoData_1 span").prop("disabled", true);
                            
                }
                if ("<?php echo $entPermDomainsLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#domainData_1 :input").prop("disabled", true);                                                
                            $("#domainData_1 span").prop("disabled", true);
                            $("#domainData_1 :input").css("background", "#e3e3e3"); 
                            
                            $("#domainData_1 #sortable_1").addClass('disabledulli');
                            $("#domainData_1 #sortable_2").addClass('disabledulli');
                            $("#domainData_1 #sortable_1").addClass('disabled');
                            $("#domainData_1 #sortable_2").addClass('disabled');
                            
                            /*$("#domainData_1 #sortable1").addClass('disabledulli');
                            $("#domainData_1 #sortable2").addClass('disabledulli');
                            $("#domainData_1 #sortable1").addClass('disabled');
                            $("#domainData_1 #sortable2").addClass('disabled');*/
        
                            $('#securityDoaminShow').removeAttr("disabled");
                            $('#securityDomainAssigned').removeAttr("disabled");
                            
                }
                
                if ("<?php echo $entPermSecurityDomainsLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#securityDiv :input").prop("disabled", true);                                                
                            $("#securityDiv span").prop("disabled", true);
                            $("#securityDiv :input").css("background", "#e3e3e3"); 
                                                        
                            $("#securityDiv #sortable1").addClass('disabledulli');
                            $("#securityDiv #sortable2").addClass('disabledulli');
                            $("#securityDiv #sortable1").addClass('disabled');
                            $("#securityDiv #sortable2").addClass('disabled');
                            
                }
                
                
                
                
                
                if ("<?php echo $entPermDNsLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#dnsData_1 :input").prop("disabled", true);
                            $("#dnsData_1 option").prop("disabled", true);
                            $("#dnsData_1 span").prop("disabled", true);
                            //$("#dnsData_1 :input").css("background", "#e3e3e3"); 
                            //Code added @ 08 June regarding EX-1168 Fixes
                            $('#enterpriseAddDns').removeAttr("disabled");
                            $('.extraInputRow').removeAttr("disabled");
                            $('#addNumberRowEnterprise').removeAttr("disabled");
                            $('#addNumberRangeRow').removeAttr("disabled");
                            $('#deleteNumberRow').removeAttr("disabled");
                            $('#addEnterpriseDNsBtn').removeAttr("disabled");
                            $('#closeEnterpriseDNsBtn').removeAttr("disabled");
                            //End code
                }
                if ("<?php echo $entPermBWAdminsLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#administratorsData_1 :input").prop("disabled", true);
                            $("#administratorsData_1 span").prop("disabled", true);
                            $("#administratorsData_1 :input").css("background", "#e3e3e3");
                            $("#administratorsData_1 li").addClass('disabledulli');
                            $("#administratorsData_1 option").prop("disabled", true);
                            $("#administratorsData_1 :input").css("background", "#e3e3e3");
                            $("#administratorsData_1 :input[type=checkbox]").prop('disabled',true);
                }
                if ("<?php echo $entPermServicesLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            //alert('In Services Permission');
                            $("#servicesData_1 :input").css("background", "#e3e3e3");
                            $("#servicesData_1 :input[type=checkbox]").addClass('disabledulli');
                            $("#servicesData_1 :input[type=radio]").addClass('disabledulli');
                            $("#servicesData_1 span").addClass('disabledulli');
                            
                            $("#servicesData_1 :input").prop("disabled", true);
                            $("#servicesData_1 option").prop("disabled", true);
                            $("#servicesData_1 span").prop("disabled", true);
                            $("#servicesData_1 :input").css("background", "#e3e3e3");
                            
                            $('#expCollapse').removeAttr("disabled");
                            $('#expCollapse1').removeAttr("disabled");
                            $('#expCollapse').removeClass("disabledulli");
                            $('#expCollapse1').removeClass("disabledulli");
                            $('#expCollapse').addClass("enabledulli");
                            $('#expCollapse1').addClass("enabledulli");
                            $('#serviceFilterTitleName').removeClass("disabledulli");
                            $('#enterpriseServicesFiltersStatusTitleIcon').removeClass("disabledulli")                            
                            $('#serviceFilterTitleName').addClass("enabledulli");
                            $('#enterpriseServicesFiltersStatusTitleIcon').addClass("enabledulli");
                            
                            $('#serviceFilterTitleName1').removeClass("disabledulli");
                            $('#enterpriseServicesFiltersStatusTitleIcon1').removeClass("disabledulli")                            
                            $('#serviceFilterTitleName1').addClass("enabledulli");
                            $('#enterpriseServicesFiltersStatusTitleIcon1').addClass("enabledulli");
                } 
                if ("<?php echo $entPermCallPrcsPoliciesLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#callProcessingPolicyData_1 :input[type=checkbox]").addClass('disabledulli');
                            $("#callProcessingPolicyData_1 :input[type=radio]").addClass('disabledulli');
                            $("#callProcessingPolicyData_1 span").addClass('disabledulli');
                            
                            $("#callProcessingPolicyData_1 :input").prop("disabled", true);
                            $("#callProcessingPolicyData_1 option").prop("disabled", true);
                            $("#callProcessingPolicyData_1 span").prop("disabled", true);
                            $("#callProcessingPolicyData_1 :input").css("background", "#e3e3e3");
                }
                if ("<?php echo $entPermDialPlanPolicyLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#dialPlanPolicyData_1 :input").prop("disabled", true);                                            
                            $("#dialPlanPolicyData_1 #sortable_1").addClass('disabledulli');
                            $("#dialPlanPolicyData_1 #sortable_2").addClass('disabledulli');
                            $("#dialPlanPolicyData_1 #sortable_1").addClass('disabled');
                            $("#dialPlanPolicyData_1 #sortable_2").addClass('disabled');
                            $("#dialPlanPolicyData_1 li").addClass('disabledulli');
                            $("#dialPlanPolicyData_1 option").prop("disabled", true);
                            $("#dialPlanPolicyData_1 span").prop("disabled", true);
                            $("#dialPlanPolicyData_1 :input").css("background", "#e3e3e3");
                }
                if ("<?php echo $entPermVoiceMessagingLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#voiceMessagingData_1 button").prop("disabled", true);
                            $("#voiceMessagingData_1 :input").css("background", "#e3e3e3");
                            $("#voiceMessagingData_1 :input[type=checkbox]").addClass('disabledulli');
                            $("#voiceMessagingData_1 :input[type=radio]").addClass('disabledulli');
                            $("#voiceMessagingData_1 span").addClass('disabledulli');
                            $("#voiceMessagingData_1 li").addClass('disabled');
                            $("#voiceMessagingData_1 option").prop("disabled", true);
                            $("#voiceMessagingData_1 span").prop("disabled", true);
                            
                }


                if ("<?php echo $entPermVoicePortalLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#voicePortalData_1 button").prop("disabled", true);
                            $("#voicePortalData_1 :input").css("background", "#e3e3e3");
                            $("#voicePortalData_1 :input[type=checkbox]").addClass('disabledulli');
                            $("#voicePortalData_1 :input[type=radio]").addClass('disabledulli');
                            $("#voiceMessagingData_1 :input[type=radio]").prop("disabled","true");
                            $("#voicePortalData_1 span").addClass('disabledulli');
                            $("#voicePortalData_1 li").addClass('disabled');
                            $("#voicePortalData_1 option").prop("disabled", true);
                            $("#voicePortalData_1 span").prop("disabled", true);
                            
                }
                
                
                if ("<?php echo $entPermNCOSLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#ncsData_1 button").prop("disabled", true);
                            $("#ncsData_1 :input").css("background", "#e3e3e3");
                            $("#ncsData_1 :input[type=checkbox]").addClass('disabledulli');
                            $("#ncsData_1 :input[type=radio]").addClass('disabledulli');
                            $("#ncsData_1 span").addClass('disabledulli');
                            $("#ncsData_1 li").addClass('disabled');
                            $("#ncsData_1 option").prop("disabled", true);
                            $("#ncsData_1 span").prop("disabled", true);
                            
                }
                
                if ("<?php echo $entPermRoutingProfileLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#routingData_1 button").prop("disabled", true);
                            $("#routingData_1 :input").css("background", "#e3e3e3");
                            $("#routingData_1 :input[type=checkbox]").addClass('disabledulli');
                            $("#routingData_1 :input[type=radio]").addClass('disabledulli');
                            $("#routingData_1 span").addClass('disabledulli');
                            $("#routingData_1 li").addClass('disabled');
                           // $("#routingProfile select").prop("disabled", true);
                            $("#routingProfile option").prop("disabled", true);
                            $("#routingProfile").prop("disabled",true);
                            $("#routingData_1 span").prop("disabled", true);
                            
                }
                
                if ("<?php echo $entPermPasswordRulesLogModPermission ?>" == "no" && "<?php echo $_SESSION['superUser'] ?>" == "3") {
                            $("#passwordsRulesData_1 button").prop("disabled", true);
                            $("#passwordsRulesData_1 :input").css("background", "#e3e3e3");
                            $("#passwordsRulesData_1 :input[type=checkbox]").addClass('disabledulli');
                            $("#passwordsRulesData_1 :input[type=radio]").addClass('disabledulli');
                           // $("#passwordsRulesData_1 :input[type=radio]").prop("disabled","true");
                            $("#passwordsRulesData_1 span").addClass('disabledulli');
                            $("#passwordsRulesData_1 li").addClass('disabled');
                            $("#passwordsRulesData_1 option").prop("disabled", true);
                            $("#passwordsRulesData_1 span").prop("disabled", true);
                            $("#passwordsRulesData_1 :input[type=checkbox]").prop('disabled',true);
                            $("#passwordsRulesData_1 input").prop("disabled", true);
                             
                }
                //End code
                        
                        
	});
	 

</script>

<?php if( ($_SESSION["superUser"] == "1") 
            || ($basicInfoEntLogPermission == "1") 
            || ($entPermDomainsLogPermission == "1")
            || ($entPermDNsLogPermission == "1") 
            || ($entPermBWAdminsLogPermission == "1") 
            || ($entPermSecurityDomainsLogPermission == "1") 
            || ($entPermServicesLogPermission == "1") 
            || ($entPermCallPrcsPoliciesLogPermission == "1")
            || ($entPermDialPlanPolicyLogPermission == "1") 
            || ($entPermVoiceMessagingLogPermission == "1")
            || ($entPermVoicePortalLogPermission == "1")
            || ($entPermNCOSLogPermission == "1")
            || ($entPermRoutingProfileLogPermission == "1")
            || ($entPermPasswordRulesLogPermission == "1") ) { ?>

<div class="labelTextGrey" style="padding-top: 15px; text-align: center; display: block;" id="modifyGrouptitle">
<?php 
if(isset($allResp["basicInfo"]["Success"]["serviceProviderName"]) && $allResp["basicInfo"]["Success"]["serviceProviderName"] != ""){
    $entNameDisplay = $allResp["basicInfo"]["Success"]["enterpriseId"]." - ".$allResp["basicInfo"]["Success"]["serviceProviderName"];
    
}else{
    $entNameDisplay = $allResp["basicInfo"]["Success"]["enterpriseId"];
}
?>
	<p class="subBannerCustom register-submit selectedGroup"><?php echo $entNameDisplay; ?></p><p>
			
	</p>
</div>
<div id="enterpriseData" style="">
        	<div id="tabs">
        		<div class="divBeforeUl">
        			<ul>
                                <?php 
                                if($_SESSION["superUser"] == "1" || $basicInfoEntLogPermission){
                                ?> 
                                    <li><a href="#basicInfoData_1" id="basicInfo" class="tabs">Basic Information</a></li>
                                <?php
                                 }
                                if($_SESSION["superUser"] == "1" || $entPermDomainsLogPermission){
                                ?>
        				<li><a href="#domainData_1" id="domains" class="tabs">Domains</a></li>
                                <?php
                                }                        
                        	if($_SESSION["superUser"] == "1" || $entPermServicesLogPermission){
                                ?>
                                        <li><a href="#servicesData_1" id="services" class="tabs">Services</a></li>
                               <?php			    
                                }
                                    if($_SESSION["superUser"] == "1" || $entPermDNsLogPermission){
                                ?>        
        				<li><a href="#dnsData_1" id="dns" class="tabs">DNs</a></li>
                                <?php			    
                                }
                                if($_SESSION["superUser"] == "1" || ($entPermBWAdminsLogPermission == "1")){                                        
                                ?>        
        				<li><a href="#administratorsData_1" id="administrators" class="tabs">Administrators</a></li>
                                <?php			    
                                }                                
                                if($_SESSION["superUser"] == "1" || $entPermCallPrcsPoliciesLogPermission){
                                ?>        
        				<li><a href="#callProcessingPolicyData_1" id="callProcessingPolicy" class="tabs">Call Processing Policies</a></li>
                                <?php			    
                                }  
                                if($_SESSION["superUser"] == "1" || $entPermDialPlanPolicyLogPermission){
                                ?>
                                    <li><a href="#dialPlanPolicyData_1" id="dialPlanPolicy" class="tabs">Dial Plan Policy</a></li>
                                <?php 
                                    }                                 
                                    if(($_SESSION["superUser"] == "1" || $entPermVoiceMessagingLogPermission) && (array_key_exists("Voice Messaging User", $entUserServiceArray))){
                                ?> 
                                <li><a href="#voiceMessagingData_1" id="voiceMessaging" class="tabs">Voice Messaging</a></li>
                                <?php 
                                    }                                       
                                    if(($_SESSION["superUser"] == "1" || $entPermVoicePortalLogPermission) && ($voicePortalContext == "Service Provider" && (array_key_exists("Voice Messaging Group", $entGroupServiceArray)))){                                  
                                ?>        
                                    <li><a href="#voicePortalData_1" id="voicePortal" class="tabs">Voice Portal</a></li>
                                <?php
                                    }                                       
                                if($_SESSION["superUser"] == "1" || $entPermNCOSLogPermission){
                                  ?>        
                                      <li><a href="#ncsData_1" id="ncs" class="tabs">Network Classes of Service</a></li>
                                <?php			    
                                }                                       
                                if($_SESSION["superUser"] == "1" || $entPermRoutingProfileLogPermission){
                                  ?>        
                                      <li><a href="#routingData_1" id="routing" class="tabs">Routing Profile</a></li>
                                <?php
                                }                                      
                                if($_SESSION["superUser"] == "1" || $entPermPasswordRulesLogModPermission){
                                  ?>        
                                      <li><a href="#passwordsRulesData_1" id="passwordsRules" class="tabs">Passwords Rules</a></li>
                                  <?php
                                  }
                                ?> 
        			</ul>
        		</div>
        		<form name ="enterpriseModify" id="enterpriseModify" class="groupModifyCls" method="POST">
        		<div id="basicInfoData_1" class="userDataClass" style="display:none;"><!--begin basicInfo_1 div-->
				<h2 class="subBannerCustom">Basic Info</h2>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="labelText">Enterprise ID</label><span class="required">*</span><br/>
								<input type="text" name="enterpriseId" id="serviceProviderId" size="30" maxlength="30" value="<?php echo $allResp["basicInfo"]["Success"]["enterpriseId"]; ?>" disabled>
								<input type="hidden" id="hiddenEnterpriseName" name="basicInfo[enterpriseId]" value="<?php echo $allResp["basicInfo"]["Success"]["enterpriseId"]; ?>">
							</div>
							
							<div class="form-group">	
								<label class="labelText">Name</label><br>
								<input type="text" name="basicInfo[serviceProviderName]" id="serviceProviderName" size="30" maxlength="30" value="<?php echo $allResp["basicInfo"]["Success"]["serviceProviderName"]; ?>">
							</div>
							<div class="form-group">	
								<label class="labelText">Contact Phone</label><br>
								<input type="text" name="basicInfo[contactNumber]" id="contactNumber" size="30" maxlength="30" value="<?php echo $allResp["basicInfo"]["Success"]["contactNumber"]; ?>" >
							</div>
							<div class="form-group">	
								<label class="labelText">Support Email</label><br>
								<input type="text" name="basicInfo[supportEmail]" id="supportEmail" size="30" maxlength="30" value="<?php echo $allResp["basicInfo"]["Success"]["supportEmail"]; ?>" >
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="labelText" for="defaultDomain">Default Domain:</label><br>
								<div class="dropdown-wrap">   
									<select name="basicInfo[defaultDomain]" id="defaultDomain">
									<?php 
									if(count($enterpriseDomainsListResp) > 0){
									    $domainVal = "";
									    foreach ($enterpriseDomainsListResp as $key => $value){
									        $sel = "";
									        if($value == $allResp["basicInfo"]["Success"]["defaultDomain"]){
									            $sel="selected";
									        }
									        $domainVal .= "<option value='".$value."' $sel>".$value."</option>"; 
									    }
									    echo $domainVal;
									}
									?>
									</select>
								</div>
							</div>
							<div class="form-group">	
								<label class="labelText">Contact Name</label><br>
								<input type="text" name="basicInfo[contactName]" id="contactName" size="30" maxlength="30" value="<?php echo $allResp["basicInfo"]["Success"]["contactName"]; ?>">
							</div>
							
							<div class="form-group">	
								<label class="labelText">Contact Email</label><br>
								<input type="text" name="basicInfo[contactEmail]" id="contactEmail" size="30" maxlength="30" value="<?php echo $allResp["basicInfo"]["Success"]["contactEmail"]; ?>" >
							</div>
							
						</div>
					</div> 
					<h2 class="subBannerCustom">Address Information</h2>
					<div class="row">
						<div class="col-md-6">
							
							<div class="form-group">	
								<label class="labelText">Address Line 1</label><br>
								<input type="text" name="basicInfo[addressLine1]" id="addressLine1" size="30" maxlength="30" value="<?php echo $allResp["basicInfo"]["Success"]["addressLine1"]; ?>">
							</div>
							
							<div class="form-group">	
								<label class="labelText">City</label><br>
								<input type="text" name="basicInfo[city]" id="city" size="30" maxlength="30" value="<?php echo $allResp["basicInfo"]["Success"]["city"]; ?>" >
							</div>
							
							<div class="form-group">	
								<label class="labelText">Zip/Postal Code</label><br>
								<input type="text" name="basicInfo[zipOrPostalCode]" id="zipOrPostalCode" size="30" maxlength="30" value="<?php echo $allResp["basicInfo"]["Success"]["zipOrPostalCode"]; ?>">
							</div>
							
						</div>
						
						<div class="col-md-6">
							
							<div class="form-group">	
								<label class="labelText">Address Line 2</label><br>
								<input type="text" name="basicInfo[addressLine2]" id="addressLine2" size="30" maxlength="30" value="<?php echo $allResp["basicInfo"]["Success"]["addressLine2"]; ?>">
							</div>
							
							<div class="form-group">
								<label class="labelText" for="defaultDomain">State/Province:</label><br>
								<div class="dropdown-wrap">   
									<select name="basicInfo[stateOrProvince]" id="stateOrProvince">
									<?php 
									if(count($statList["Success"]) > 0){
									    
									    
									    $stateVal = "<option value=''>-- Select --</option>";
									    foreach ($statList["Success"] as $key => $value ){
									        $stateSel = "";
									        if($allResp["basicInfo"]["Success"]["stateOrProvince"] == $value){
									            $stateSel = "selected";
									        }
									        $stateVal .= "<option value='".$value."' $stateSel>".$value."</option>";
									    }
									    echo $stateVal;
									}
									?>
									</select>
								</div>
							</div>
							<div class="form-group">	
								<label class="labelText">Country</label><br>
								<input type="text" name="basicInfo[country]" id="country" size="30" maxlength="30" value="<?php echo $allResp["basicInfo"]["Success"]["country"]; ?>">
							</div>
						</div>
					</div>                      
        	</div>
        	<div id="domainData_1" class="userDataClass" style="display:none;"><!--begin basicInfo_1 div-->
        		<h2 class="adminUserText">Domains
                	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Drag and drop items back and forth"><img src="images/NewIcon/info_icon.png"></a>
                	<?php 
                	if($_SESSION["superUser"] == "1"){ ?>
                	   <!-- <span class="addDomainClass"><input type="button" value = "Add Domain" id = "addDomainButton" class="subButton" style="width:100px !important;font-weight:normal;height:26px;"/></span> -->
                        <span class="addDomainClass">  
                            <img src="/Express/images/NewIcon/add_domain_button.png" data-alt-src="/Express/images/NewIcon/add_domain_button.png" id="addDomainButton" style="">
                                <label class="labelText labelTextOpp" style=""><span style="float: initial;margin: 0 auto;">Add</span><br><span style="margin-left: 2px;">Domain</span>
                                </label>
                        </span>
                	<?php } ?>
                	
				</h2>
				<!-- add domain start -->
				<div id="showAddDomainEnterprise" class="row">
					<div class="">
						<div class="form-group">
							<h2 class="adminUserText">ADD Domain </h2>
						</div>
					</div>
					<div class="col-md-7" id="phoneNumberDiv">
						<div class="form-group">
							<div class="col-md-10" style="padding-left:0px;"><label class="labelText">Domain Name</label>
								<input class="extraInputRowDomain" type="text" name="domainName" value="" style="width:380px !important;float: left">
							</div>
                                                                    <div class="col-md-1" style="width:16% !important;"><label class="labelText">&nbsp;</label><br>
								<img class="" id="addDomainRowEnterprise" src="images/add.png" style="width: 18px; float: right; margin-top: 17px">
							</div>
						</div>
					</div>
					
					<div class="clr"></div>
					<div class="col-md-12 dnBtnMargin">
						<div class="col-md-6">
							<div class="form-group">
								<input type="button" id="addEnterpriseDomainsBtn" class="resetAll addDNButtonColor" value="Add Domain" style="margin-right: 0px;width:100px !important;">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<!--<input type="button" class ="cancelButton" id="closeServiceProviderDns" value="Close" style="width: 100px;">-->
								<input type="button" class="resetAll closeDNButton" id="closeEnterpriseDomainsBtn" value="Close" style="width:80px !important;">
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="labelpadding" id="errorDomainsGroup" style="font-weight: bold; text-align: center; color: red; width: 100%;"></label>
						</div>
					</div>
				</div>
				<!-- add domain end -->
								
				
				<div class="row">
					<div class="">
						<div class="col-md-6">
							<div class="form-group alignCenter">
								<label class="labelTextGrey" style="text-align: center;">
                                	Available Domains
                                <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="You can drag things forth to assign Domains"><img src="images/NewIcon/info_icon.png" /></a>
                                </label><br>
								<div class="form-group scrollableListCustom1">
									<ul id="sortable_1" class="connectedSortable ui-sortable">
									<?php 
									if(count($domainAvailable) > 0){
									    foreach ($domainAvailable as $key => $value){
									        echo "<li class='ui-state-default' id = '".$value."'>".$value."</li>";
									    }
									}
									?>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group alignCenter">
								<label class="labelTextGrey" style="text-align: center;">
                                	Assigned Domains
                                    <a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="You can drag things back to unassign Domains"><img src="images/NewIcon/info_icon.png" /></a>
                                </label><br>
								<div class="form-group scrollableListCustom2">
									<ul id="sortable_2" class="connectedSortable ui-sortable">
									<?php 
									foreach ($domainAssigned as $key => $value){
									    $defDomain = "";
									    if($value == $_SESSION["allResp"]["basicInfo"]["Success"]["defaultDomain"]){
									        $defDomain = "(Default Domain)";
									    }
									    echo "<li class='ui-state-default' id='".$value."'>".$value." ".$defDomain."</li>";
									}
									?>
									
									</ul>
									<input type="hidden" name="domainAssigned" id="domainAssigned" value="<?php echo implode(";",$domainAssigned); ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<?php 
				if(($entPermSecurityDomainsLogPermission == "1") && (count($securityDomainAvailable) > 0 || count($securityDomainAssigned) > 0)){ ?>
				<input type="hidden" value="show" id="securityDoaminShow">
                                
                                <input type="hidden" name="securityDomainAssigned" id="securityDomainAssigned" value="<?php echo implode(";",$securityDomainAssigned); ?>">
                                
				<div class="row" id="securityDiv">
					<h2 class="adminUserText">Security Domains
					<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Drag and drop items back and forth"><img src="images/NewIcon/info_icon.png"></a>
					<?php 
                	if($_SESSION["superUser"] == "1"){ ?>
				<!--	<span class="addDomainClass"><input type="button" value="Add Security Domain" id="addSecurityDomainButton" class="subButton" style="width:140px !important;font-weight:normal;height:26px;"></span>-->
                                        <span class="addDomainClass">  
                                            <img src="/Express/images/NewIcon/add_domain_button.png" data-alt-src="/Express/images/NewIcon/add_domain_button.png" id="addSecurityDomainButton" style="margin-right: -2px !important">
                                            <label class="labelText labelTextOpp" style=""><span style="float: initial;margin: 0 auto;">Add</span><br><span style="float: initial;margin: 0 auto;">Security</span><br>
                                            <span style="margin-left: 2px;">Domain</span></label>
                                        </span>
					<?php } ?>
					</h2>
					
					<!-- add domain start -->
				<div id="showAddSecurityDomainEnterprise" class="row">
					<div class="">
						<div class="form-group">
							<h2 class="adminUserText">ADD Security Domain </h2>
						</div>
					</div>
					<div class="col-md-7" id="phoneNumberDiv">
						<div class="form-group">
							<div class="col-md-10" style=""><label class="labelText">Domain Name</label>
								<input class="extraInputRowDomain" type="text" name="securityDomainName" value="" style="width:380px !important;float: left">
							</div>
                                                                    <div class="col-md-1" style="width:16% !important;"><label class="labelText">&nbsp;</label><br>
								<img class="" id="addSecurityDomainRowEnterprise" src="images/add.png" style="width: 18px; float: right; margin-top: 17px">
							</div>
						</div>
					</div>
					
					<div class="clr"></div>
					<div class="col-md-12 dnBtnMargin">
						<div class="col-md-6">
							<div class="form-group">
								<input type="button" id="addEnterpriseSecurityDomainsBtn" class="resetAll addDNButtonColor" value="Add Security Domain" style="margin-right: 0px;width:140px !important;">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<!--<input type="button" class ="cancelButton" id="closeServiceProviderDns" value="Close" style="width: 100px;">-->
								<input type="button" class="resetAll closeDNButton" id="closeEnterpriseSecurityDomainsBtn" value="Close" style="width:80px !important;">
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="labelpadding" id="errorSecurityDomainsGroup" style="font-weight: bold; text-align: center; color: red; width: 100%;"></label>
						</div>
					</div>
				</div>
				<!-- add domain end -->
					
					<div class="">
						<div class="col-md-6">
							<div class="form-group alignCenter">
								<label class="labelTextGrey" style="text-align: center;">Available
									Domains
									<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="You can drag things forth to assign Security Domains"><img src="images/NewIcon/info_icon.png" /></a>
									</label><br>
								<div class="form-group scrollableListCustom1">
									<ul id="sortable1" class="connectedSortable ui-sortable">
									<?php 
									if(count($securityDomainAvailable) > 0){
									    foreach ($securityDomainAvailable as $key => $value){
									        echo "<li class='ui-state-default' id = '".$value."'>".$value."</li>";
									    }
									}
									?>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group alignCenter">
								<label class="labelTextGrey" style="text-align: center;">Assigned
									Domains
									<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="You can drag things back to unassign Security Domains"><img src="images/NewIcon/info_icon.png"></a>
									</label><br>
								<div class="form-group scrollableListCustom2">
									<ul id="sortable2" class="connectedSortable ui-sortable">
									<?php 
									if(count($securityDomainAssigned) > 0){
									    
									    foreach ($securityDomainAssigned as $key => $value){
									        $defSecDomain = "";
									        if($value == $_SESSION["allResp"]["basicInfo"]["Success"]["defaultDomain"]){
									            $defSecDomain = "(Default Domain)";
									        }
									        echo "<li class='ui-state-default' id = '".$value."'>".$value." ".$defSecDomain."</li>";
									    }
									}
									?>
									</ul>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				    
				<?php }else{
				    echo '<input type="hidden" value="hide" id="securityDoaminShow">';
				}
				?>
				
				
        	</div> 
        	<!-- Domain end -->
        	
            <!-- Services -->
        	<div id="servicesData_1" class="userDataClass" style="display:none;">
            	<div class="row">
            	<?php 
            		require_once("/var/www/html/Express/enterprise/services/index.php");
            	?>
            	</div>
        	</div>
        	<!-- Services End-->
            
            <!-- DNs Start -->   
            
            <div id="dnsData_1" class="userDataClass" style="display:none;">
            	<div class="row">
            	<?php 
            		require_once("/var/www/html/Express/enterprise/DNs/index.php");
            	?>
            	</div>
        	</div> 
            <!-- DNs End --> 
               
             <!--administrator module -->
            <div id="administratorsData_1" class="userDataClass" style="display:none;">
                <div class="row">
                    <?php 
                        require_once("/var/www/html/Express/enterprise/administrator/index.php");
                    ?>
                </div>
        	</div>
                <!--end admininstrator module -->   

        	<div id="callProcessingPolicyData_1" class="userDataClass" style="display:none;">
        	<?php 
        	include('callProcessingPolicyUI.php');
        	?>
        	</div>
        	
        	<div id="dialPlanPolicyData_1" class="userDataClass" style="display:none;">
        	<?php 
        	include('dialPlanPolicyUI.php');
        	?>
        	</div>
        	
        	<div id="voiceMessagingData_1" class="userDataClass" style="display:none;">
        	<?php 
        	include('voiceMessagingUI.php');
        	?>
        	</div>
        	
        	<div id="voicePortalData_1" class="userDataClass" style="display:none;">
                        <?php 
                            include('voicePortalUI.php');
                        ?>
        	</div>
        	
        	<div id="ncsData_1" class="userDataClass" style="display:none;padding:0;">
        		<?php 
        	       include('networkClassofServiceUI.php');
        	    ?>
        	</div>
        	
        	<div id="routingData_1" class="userDataClass" style="display:none;padding:0;">
        		<?php 
        	       include('routingProfileUI.php');
        	    ?>
        	</div>
        	
        	<div id="passwordsRulesData_1" class="userDataClass" style="display:none;padding:0;">
        		<?php 
        	       include('passwordsRulesUI.php');
        	    ?>
        	</div>
        	
        	
        	
        	</form>
        </div>
    
   <!-- permission for modify and delete enterprise --> 
    
        <div class="row alignBtn">
            <div class="col-md-2"></div>
            <?php
                if($enterprisePermissions && $enterpriseModPermissions){
                ?>
                <div class="col-md-8" style="text-align: center;">
                    <input type="button" id="subButtonEnt" class="marginRightButton" value="Confirm Settings">
                    <input type="button" id="EntButtonCancel" value="Cancel" class="cancelButton marginRightButton">
                </div>
              <?php 
                }
            ?>

            <?php
                if($basicInfoEntLogPermission == "1" && $basicInfoEntLogAddDelPermission == "yes"){
                ?>
                <div class="col-md-2" style="text-align: right;">
                    <input id="subButtonDelEnt" value="Delete Enterprise" type="button" class="deleteButton marginRightButton deleteSmallButton" style="display: inline-block;">
                </div>
                <?php 
                }
            ?>
        </div>
        <div class="clr"></div>
        <div class="clr"></div>	
        	
        	
		</div>

                                 <?php } ?>
<input type = "hidden" id="securityDomainPatternMatch" value="<?php echo $securityDomainPattern; ?>">
 <input type="hidden" value="<?php echo $entPermBWAdminsLogPermission ; ?>" id="entPermBWAdminsLogPermission" />
<input type="hidden" value="<?php echo $entPermBWAdminsLogModPermission; ?>"    id="entPermBWAdminsLogModPermission" />
<input type="hidden" value="<?php echo $entPermBWAdminsLogAddDelPermission; ?>" id="entPermBWAdminsLogAddDelPermission" />


<div id="domainDialog" class="dialogClass"></div>
<div id="updateDomainDialog" class="dialogClass"></div>
