<h2 class="adminUserText">Voice Messaging</h2>
<p <?php if(!empty($allResp["voiceMessaging"]["Error"])){echo "style='color:red;display:block;text-align:center;'"; }else{echo "style='display:none;'"; }?>><?php echo $allResp["voiceMessaging"]["Error"]; ?></p>
<div style="clear: right;">&nbsp;</div>
<div class="" <?php if(!empty($allResp["voiceMessaging"]["Error"])){echo "style='pointer-events:none;opacity:0.6;'"; }?>>
<div class="row">
	<div class="">
		<div class="col-md-7">
				<label style="margin-left:0;" class="labelText">Voice Message Delivery:</label>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:0;">
				<input name="voiceMessaging[useSystemDefaultDeliveryFromAddress]" class="useSystemDefaultDeliveryFromAddress" id="useSystemDefaultDeliveryFromAddress1" type="radio" value="true" <?php if($allResp["voiceMessaging"]["Success"]["useSystemDefaultDeliveryFromAddress"] == "true"){echo "checked"; }?>>
				<label for="useSystemDefaultDeliveryFromAddress1"><span></span></label> <label class="labelText" style="margin-bottom:0;">Use System Default From Email Address</label> &nbsp;&nbsp;&nbsp;&nbsp;
			</div>
		</div>
		
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox">
				<input name="voiceMessaging[useSystemDefaultDeliveryFromAddress]" class="useSystemDefaultDeliveryFromAddress" id="useSystemDefaultDeliveryFromAddress2" type="radio" value="false" <?php if($allResp["voiceMessaging"]["Success"]["useSystemDefaultDeliveryFromAddress"] == "false"){echo "checked"; }?>>
				<label for="useSystemDefaultDeliveryFromAddress2"><span></span></label> <label class="labelText" style="margin-bottom:0;">Default From Email Address</label> &nbsp;&nbsp;&nbsp;&nbsp;
				<input type="text" id="deliveryFromAddress" name="voiceMessaging[deliveryFromAddress]" value="<?php echo $allResp["voiceMessaging"]["Success"]["deliveryFromAddress"]; ?>" style="width:200px !important;vertical-align:middle;" <?php if($allResp["voiceMessaging"]["Success"]["useSystemDefaultDeliveryFromAddress"] == "true"){echo "readonly"; }?>> &nbsp;&nbsp;&nbsp;&nbsp;
				<label class="labelText" style="margin-bottom:0;">Default is used as the 'From' field</label>
			</div>
		</div>
		
	</div>
</div>

<div style="clear: right;">&nbsp;</div>

<div class="row">
	<div class="">
		<div class="col-md-7">
				<label style="margin-left:0;" class="labelText">Voice Message Notification:</label>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom: 0;">
				<input name="voiceMessaging[useSystemDefaultNotificationFromAddress]" class="useSystemDefaultNotificationFromAddress" id="useSystemDefaultNotificationFromAddress1" type="radio" value="true" <?php if($allResp["voiceMessaging"]["Success"]["useSystemDefaultNotificationFromAddress"] == "true"){echo "checked"; }?>>
				<label for="useSystemDefaultNotificationFromAddress1"><span></span></label> <label class="labelText" style="margin-bottom:0;">Use System Default From Email Address</label> &nbsp;&nbsp;&nbsp;&nbsp;
			</div>
		</div>
		
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox">
				<input name="voiceMessaging[useSystemDefaultNotificationFromAddress]" class="useSystemDefaultNotificationFromAddress" id="useSystemDefaultNotificationFromAddress2" type="radio" value="false" <?php if($allResp["voiceMessaging"]["Success"]["useSystemDefaultNotificationFromAddress"] == "false"){echo "checked"; }?>>
				<label for="useSystemDefaultNotificationFromAddress2"><span></span></label> <label class="labelText" style="margin-bottom:0;">Default From Email Address</label> &nbsp;&nbsp;&nbsp;&nbsp;
				<input type="text" name="voiceMessaging[notificationFromAddress]" id="notificationFromAddress" value="<?php echo $allResp["voiceMessaging"]["Success"]["notificationFromAddress"]; ?>" style="width:200px !important; vertical-align:middle;" <?php if($allResp["voiceMessaging"]["Success"]["useSystemDefaultNotificationFromAddress"] == "true"){echo "readonly"; }?>> &nbsp;&nbsp;&nbsp;&nbsp;
				<label class="labelText" style="margin-bottom:0;">Default is used as the 'From' field</label>
			</div>
		</div>
		
	</div>
</div>

<div style="clear: right;">&nbsp;</div>

<div class="row">
	<div class="">
		<div class="col-md-7">
				<label style="margin-left:0;" class="labelText">Voice Portal Passcode Lookout:</label>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox" style="margin-bottom:0;">
				<input name="voiceMessaging[useSystemDefaultVoicePortalLockoutFromAddress]" class="useSystemDefaultVoicePortalLockoutFromAddress" id="useSystemDefaultVoicePortalLockoutFromAddress1" type="radio" value="true" <?php if($allResp["voiceMessaging"]["Success"]["useSystemDefaultVoicePortalLockoutFromAddress"] == "true"){echo "checked"; }?>>
				<label for="useSystemDefaultVoicePortalLockoutFromAddress1"><span></span></label> <label class="labelText" style="margin-bottom:0;">Use System Default From Email Address</label> &nbsp;&nbsp;&nbsp;&nbsp;
			</div>
		</div>
		
	</div>
</div>

<div class="row">
	<div class="">
		<div class="col-md-12">
			<div class="form-group alignChkBox">
				<input name="voiceMessaging[useSystemDefaultVoicePortalLockoutFromAddress]" class="useSystemDefaultVoicePortalLockoutFromAddress" id="useSystemDefaultVoicePortalLockoutFromAddress2" type="radio" value="false" <?php if($allResp["voiceMessaging"]["Success"]["useSystemDefaultVoicePortalLockoutFromAddress"] == "false"){echo "checked"; }?>>
				<label for="useSystemDefaultVoicePortalLockoutFromAddress2"><span></span></label> <label class="labelText" style="margin-bottom:0;">Default From Email Address</label> &nbsp;&nbsp;&nbsp;&nbsp;
				<input type="text" name="voiceMessaging[voicePortalLockoutFromAddress]" id="voicePortalLockoutFromAddress" value="<?php echo $allResp["voiceMessaging"]["Success"]["voicePortalLockoutFromAddress"]; ?>" style="width:200px !important;vertical-align:middle;" <?php if($allResp["voiceMessaging"]["Success"]["useSystemDefaultVoicePortalLockoutFromAddress"] == "true"){echo "readonly"; }?>> &nbsp;&nbsp;&nbsp;&nbsp;
				<label class="labelText" style="margin-bottom:0;">Default is used as the 'From' field</label>
			</div>
		</div>
		
	</div>
</div>
</div>