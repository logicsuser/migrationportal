
<!--begin networkServices_1 div-->
<h2 class="adminUserText">Network Class of Service</h2>
<div class="row">
	<div class="col-md-12">
		<div class="" style="float:left;padding-right: 12px;">
<div class="form-group alignCenter" style="width:235px;">
	<label class="labelTextGrey">Available NC Service</label><br>
</div>

<div class="form-group alignCenter" style="width:235px;border:1px solid #002c60 !important;">
    <select style="height: 250px !important;width: 230px !important;border:none !important;margin:1px;" id="availableNCSToAssign" multiple="">
        <?php 
        if(count($_SESSION["allResp"]["ncsData"]["systemData"]) > 0){
            foreach ($_SESSION["allResp"]["ncsData"]["systemData"] as $key => $value){
                $opttag .= "<option value='".$value."'>".$value."</option>";
            }
            echo $opttag;
        }
        ?>
	</select>
</div>
</div>
<!-- button arrow  -->
<div class="" style="float:left; padding-right:12px;">
	<div class="form-group alignCenter servicesButton " style="padding-top:100px;">
		<button id="addNCS" type="button" class="addBlueCircle"> 
			<img src="/Express/images/icons/arrow_Add.png">
		</button>
		
		<br> 
		
		<button id="removeFromAssigned" type="button" class="addRedCircle"> 
			<img src="/Express/images/icons/arrow_remove.png">
		</button>
		
        <br><br> 
	   
	   <button id="addNCSAll" type="button" class="addBlueCircle"> 
			<img src="/Express/images/icons/arrow_add_all.png">
		</button>
		
      <br>
      
	  <button id="removeAllFromAssigned" type="button" class="addRedCircle" style="margin-left:-4px;"> 
			<img src="/Express/images/icons/arrow_remove_all.png">
	  </button>
	</div>
</div>

<!-- Assign N/W service -->
		
<div class="" style="float:left;padding-right: 12px;">
    <div class="form-group alignCenter" style="width: 230px;">
    	<label class="labelTextGrey ">Assigned NC Service</label>
    </div>
    
    <div class="form-group alignCenter" style="width: 235px;border:1px solid #002c60 !important;">
    	<select id="assignedNCS" name="assignedNCS[]" multiple="" style="height: 250px !important;width: 230px !important;border:none !important;margin:1px;">
    	<?php 
        if(count($_SESSION["allResp"]["ncsData"]["entData"]) > 0){
            foreach ($_SESSION["allResp"]["ncsData"]["entData"] as $key1 => $value1){
                $opttag1 .= "<option value='".$value1."'>".$value1."</option>";
            }
            echo $opttag1;
        }
        ?>
    	</select>
   	</div>
</div>

<div class="" style="float:left; padding-right:15px;">
	<div class="form-group alignCenter servicesButton" style="padding-top:100px;">
		 <button id="selectToDefault" type="button" class="addBlueCircle"> 
			<img src="/Express/images/icons/arrow_Add.png">
		</button>										
     </div>
</div>
		
		
<div class="" style="float:left; ">
    <div class="form-group alignCenter">
    	<label class="labelTextGrey" style="text-align: center;">Default</label>
    </div>
    		
    <div class="form-group alignCenter" style="border:1px solid #002c60 !important">
    	<select name="assignedDefault[]" id="assignedDefault" multiple="" style="height: 250px !important;width: 230px !important;border:none !important;margin:1px;">
    	<?php 
        if(count($_SESSION["allResp"]["ncsData"]["defaultData"]) > 0){
            foreach ($_SESSION["allResp"]["ncsData"]["defaultData"] as $key2 => $value2){
                $opttag2 .= "<option value='".$value2."'>".$value2."</option>";
            }
            echo $opttag2;
        }
        ?>
    	</select>
    </div>
</div>
</div>
		
</div>	
