<?php

/**
 * Created by Dipin Krishna.
 * Date: 10/18/2017
 */
class ExpressSheetsSCAManager
{
    private $keys = array();
    private $headTitles = array();
    private $numColumns = 0;
    private $postName = "";
    private $backEndErrorMsg = "";
    private $staticLineOrderingFlag = "";
    private $portValueForTableUI = "";

    private $lastUserId = "";
    private $lastAlertAllAppearancesForClickToDialCalls = "";
    private $lastAlertAllAppearancesForGroupPagingCalls = "";
    private $lastAllowSCACallRetrieve = "";
    private $lastMultipleCallArrangementIsActive = "";
    private $lastAllowBridgingBetweenLocations = "";
    private $lastBridgeWarningTone = "";
    private $lastEnableCallParkNotification = "";
    private $sharedCallAppearanceUsers = array();
	private $deviceNames = array();
	private $sysOperations;

	private $oldSCAUsers = array();

	private static function userId()                                    { return "userId"; }
	private static function scaUserId()                                 { return "scaUserId"; }
	private static function action()                                    { return "action"; }
	private static function primaryUser()                               { return "primaryUser"; }
    private static function scaPort()                                   { return "scaPort"; }
	private static function scaUser()                                   { return "scaUser"; }
    private static function isActive()                                  { return "isActive"; }
    private static function allowOrigination()                          { return "allowOrigination"; }
    private static function allowTermination()                          { return "allowTermination"; }
    private static function alertAllAppearancesForClickToDialCalls()    { return "alertAllAppearancesForClickToDialCalls"; }
    private static function alertAllAppearancesForGroupPagingCalls()    { return "alertAllAppearancesForGroupPagingCalls"; }
    private static function allowSCACallRetrieve()                      { return "allowSCACallRetrieve"; }
    private static function multipleCallArrangementIsActive()           { return "multipleCallArrangementIsActive"; }
    private static function enableCallParkNotification()                { return "enableCallParkNotification"; }
    private static function allowBridgingBetweenLocations()             { return "allowBridgingBetweenLocations"; }
    private static function bridgeWarningTone()                         { return "bridgeWarningTone"; }
	private static function phoneProfile()                              { return "phoneProfile"; }
	private static function tagBundles()                                { return "tagBundles"; }
	private static function numOfLineApperances()                       { return "numOfLineApperances"; }
	private static function callsPerLineKey()                           { return "callsPerLineKey"; }
	private static function polycomLineRegNumber()                      { return "polycomLineRegNumber"; }

    public function __construct($postName) {

        $this->postName = $postName;

	    $this->addAttribute(self::action(),                                 "Action");
        $this->addAttribute(self::primaryUser(),                            "Primary User");
        $this->addAttribute(self::scaPort(),                                "SCA Port");
        $this->addAttribute(self::scaUser(),                                "SCA User");
        $this->addAttribute(self::isActive(),                               "Active");
        $this->addAttribute(self::allowOrigination(),                       "Origination");
        $this->addAttribute(self::allowTermination(),                       "Termination");
        $this->addAttribute(self::alertAllAppearancesForClickToDialCalls(), "Alert Click to Dial");
        $this->addAttribute(self::alertAllAppearancesForGroupPagingCalls(), "Alert Group Paging");
        $this->addAttribute(self::allowSCACallRetrieve(),                   "Allow Call Retrieve");
        $this->addAttribute(self::multipleCallArrangementIsActive(),        "MCA");
        $this->addAttribute(self::enableCallParkNotification(),             "Call Park");
        $this->addAttribute(self::allowBridgingBetweenLocations(),          "Bridging");
        $this->addAttribute(self::bridgeWarningTone(),                      "Bridge Warning Tone");
	    $this->addAttribute(self::phoneProfile(),                           "Phone Profile");
	    $this->addAttribute(self::tagBundles(),                             "Tag Bundles");
	    $this->addAttribute(self::numOfLineApperances(),                    "Num of Line Appearances");
	    $this->addAttribute(self::callsPerLineKey(),                        "Calls Per LineKey");
	    $this->addAttribute(self::polycomLineRegNumber(),                   "Polycom Line Reg. Number");

	    require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
	    $this->sysOperations = new sysLevelDeviceOperations();
    }


	public function createTableHeader()
	{
		$header = "";
		$margin = $this->numColumns == 0 ? 100 : (int)(100 / $this->numColumns);

		for ($i = 0; $i < $this->numColumns; $i++) {
			$header .= "<th style=\"width:" . $margin . "%\">" . $this->headTitles[$i] . "</th>";
		}

		return $header;
	}


	public function processBulk()
	{
		global $sessionid, $client, $db, $ociVersion;

		$row = "";
		foreach ($_POST[$this->postName] as $key => $value) {
			$row .= "<tr style=\"font-size: 12px\">";
			$this->executeAction($value);

			for ($i = 0; $i < $this->numColumns; $i++) {
				if($i == 2 && empty($value[$this->keys[$i]])){
					$row .= "<td>" . $this->portValueForTableUI. "</td>";
				}else{
					$row .= "<td>" . $value[$this->keys[$i]] . "</td>";
				}
			}

			$row .= "</tr>";

			if ($this->backEndErrorMsg != "") {
				break;
			}
		}

		require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/expressSheetFunctions.php");

		foreach ($this->oldSCAUsers as $primaryUserID => $primaryUser) {

			if (isset($primaryUser['scaUsers']))
			{
				$oldScaUSERS = array();
				foreach ($primaryUser['scaUsers'] as $scaUser)
				{
					if ($scaUser["endpointType"] == "Shared Call Appearance")
					{
						$oldScaUSERS[] = $scaUser["name"];
					}
				}
				if(count($oldScaUSERS) <= 0) {
					unset($oldScaUSERS);
				}
			}

			$userInfo = array();
			$userId = $primaryUserID;
			require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getUserInfo.php");

			if (isset($userInfo['sharedCallAppearanceUsers']))
			{
				$newScaUSERS = array();
				foreach ($userInfo['sharedCallAppearanceUsers'] as $scaUser)
				{
					if ($scaUser["endpointType"] == "Shared Call Appearance")
					{
						$newScaUSERS[] = $scaUser["name"];
					}
				}
				if(count($newScaUSERS) <= 0) {
					unset($newScaUSERS);
				}
			}

			$changeLogs = array(
				array(
					"field" => "Shared Call Appearances",
					"old" => isset($oldScaUSERS) ? implode("; ", $oldScaUSERS) : "None",
					"new" => isset($newScaUSERS) ? implode("; ", $newScaUSERS) : "None",
				)
			);

			$changes = array(
				'logs' => $changeLogs,
				'userId' => $primaryUserID,
				'entityName' => $primaryUser["lastName"] . ", " . $primaryUser["firstName"],
				"action" => "Modify",
			);
			insertUserChangeLogs($changes);
		}

		return $row;
	}


	//-----------------------------------------------------------------------------------------------------------------
	private function executeAction($row_value)
	{
		global $sessionid, $client, $db, $ociVersion;

		$action = $row_value[self::action()];
		$userId = $row_value[self::userId()];

		$userInfo = array();
		require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getUserInfo.php");
		require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/expressSheetFunctions.php");
		
		//get the system level configuration for the device type...EX-782...Jeetesh 
		require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/util/scaUtility.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/expressSheets/ExpressSheetsExcelManager.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
		
		$vdmOperationObj = new VdmOperations();
		$scaUtilityObj = new SCAUtility();
		$this->sharedCallAppearanceUsers[$userId] = array();
		$sheetExcelMngr = new ExpressSheetsExcelManager();

		$this->deviceNames[$userId] = $userInfo["deviceName"];

		$deviceType = $userInfo["deviceType"];
		$deviceName = $userInfo["deviceName"];
		
		//get the system level configuration for the device type...EX-782...Jeetesh 
		$systemOutputLevelConfig = new sysLevelDeviceOperations();
		$systemOutputLevelConfigArray = $systemOutputLevelConfig->getSysytemConfigDeviceTypeRequest($deviceType, $ociVersion);
		
		if(empty($systemOutputLevelConfigArray["Error"])){
			$this->staticLineOrderingFlag = $systemOutputLevelConfigArray["Success"]->staticLineOrdering;
		}
		
		if (isset($userInfo["sharedCallAppearanceUsers"])) {
			$this->sharedCallAppearanceUsers[$userId] = $userInfo['sharedCallAppearanceUsers'];
		}
		if(!isset($this->oldSCAUsers[$userId])) {
			$this->oldSCAUsers[$userId] = array(
				"scaUsers" => $userInfo['sharedCallAppearanceUsers'],
				'firstName' => $userInfo["firstName"],
				'lastName' => $userInfo["lastName"]
			);
		}
		$deviceDetails = $vdmOperationObj->getUserDeviceDetail($_SESSION["sp"], $_SESSION["groupId"], $deviceName);
		$sharedCallAppearanceUsers = $this->sharedCallAppearanceUsers[$userId];
		//SCA Commands array
		$sca = array();

		for ($a = 0; $a < count($sharedCallAppearanceUsers); $a++) {
			if ($sharedCallAppearanceUsers[$a]["endpointType"] == "Shared Call Appearance") {
			    
			    $scaDelete = xmlHeader($sessionid, "UserSharedCallAppearanceDeleteEndpointListRequest14");
				$scaDelete .= "<userId>" . $sharedCallAppearanceUsers[$a]["id"] . "</userId>";
				$scaDelete .= "<accessDeviceEndpoint>";
				$scaDelete .= "<accessDevice>";
				$scaDelete .= "<deviceLevel>Group</deviceLevel>";
				$scaDelete .= "<deviceName>" . $this->deviceNames[$userId] . "</deviceName>";
				$scaDelete .= "</accessDevice>";
				$scaDelete .= "<linePort>" . $sharedCallAppearanceUsers[$a]["linePort"] . "</linePort>";
				$scaDelete .= "</accessDeviceEndpoint>";
				$scaDelete .= xmlFooter();
				$sca[] = $scaDelete;
				//echo "SCA Delete: " . $sharedCallAppearanceUsers[$a]["linePort"] . "<br/><br/>";
			}
		}

		// Rebuild Device
		$scaRebuild = xmlHeader($sessionid, "GroupCPEConfigRebuildDeviceConfigFileRequest");
		$scaRebuild .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$scaRebuild .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$scaRebuild .= "<deviceName>" . $this->deviceNames[$userId] . "</deviceName>";
		$scaRebuild .= xmlFooter();
		$sca[] = $scaRebuild;

		// Reset Device
		$scaReset = xmlHeader($sessionid, "GroupCPEConfigResetDeviceRequest");
		$scaReset .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$scaReset .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$scaReset .= "<deviceName>" . $this->deviceNames[$userId] . "</deviceName>";
		$scaReset .= xmlFooter();
		$sca[] = $scaReset;

		//retrieve default domain
		$domain = $_SESSION["defaultDomain"];

		$scaUserId = trim($row_value[self::scaUserId()]);
		$scaPort = trim($row_value[self::scaPort()]);
		$primaryPhoneNumber = $this->getPrimaryUserAttribute($scaUserId, "phoneNumber");
		$primaryExtension = $this->getPrimaryUserAttribute($scaUserId, "extension");
		$linePort = $this->getSCALinePortName($primaryPhoneNumber, $primaryExtension, $userInfo);
		switch (strtolower($action)) {
			case 'add':
			case 'modify':
				$excelSHeetManagerArray = $sheetExcelMngr->assignScaUserExpressSheet($userId);
				$this->sharedCallAppearanceUsers[$userId] = $this->removeSCAItem($scaUserId, $this->sharedCallAppearanceUsers[$userId]);
				if ($scaPort) {
					$this->sharedCallAppearanceUsers[$userId] = $this->removeSCAPort($scaUserId, $this->sharedCallAppearanceUsers[$userId]);
				} else {
				    $vdmOperationsArr = $vdmOperationObj->getAllUserOfDevice($_SESSION["sp"], $_SESSION["groupId"], $deviceName);
				    $scaUtilityData="";
					if(empty($vdmOperationsArr["Error"])){
					    $scaUtilityData = $scaUtilityObj->scaPortCalculation($vdmOperationsArr["Success"],(int)$deviceDetails["Success"]["numberOfPorts"]);
						if($scaUtilityData != ""){
							$scaPort = $scaUtilityData;
						}else{
							$scaPort = "";
						}
						$this->portValueForTableUI = $scaPort;
					}
				}
				$this->sharedCallAppearanceUsers[$userId][] = array('id' => $scaUserId, 'port' => $scaPort, 'endpointType' => "Shared Call Appearance", 'linePort' => $linePort);
				break;
			case 'delete':
				$this->sharedCallAppearanceUsers[$userId] = $this->removeSCAItem($scaUserId, $this->sharedCallAppearanceUsers[$userId]);
				/* Start Delete Sca service pack from Sca User */
				$servicePackToBeUnAssigned = $this->getUserServicePackToBeUnAssign($scaUserId);
				if($servicePackToBeUnAssigned) {
				    $grpServiceList = new Services();
				    $grpServiceList->unAssignServicePackUser($scaUserId, $servicePackToBeUnAssigned);
				}
				break;
		}

		$this->sharedCallAppearanceUsers[$userId] = array_msort($this->sharedCallAppearanceUsers[$userId], array('port' => SORT_ASC));
		if (isset($this->sharedCallAppearanceUsers[$userId])) {
		foreach ($this->sharedCallAppearanceUsers[$userId] as $k => $v) {
		if ($v["endpointType"] == "Shared Call Appearance") {
					$scaAddUserId = $v['id'];
					$scaPort = $v['port'];
					$linePort = $v['linePort'];
					/* Start Service Pack Operation */
					$response = $this->userServicePackAssignment($scaAddUserId);
					if($response != "SCA service pack has been assigned" || $response != "") {
					    $this->backEndErrorMsg = $response["Error"];
					}
					/* End Service Pack Operation */
					
					$scaInsert = xmlHeader($sessionid, "UserSharedCallAppearanceAddEndpointRequest14sp2");
					$scaInsert .= "<userId>" . $scaAddUserId . "</userId>";
					$scaInsert .= "<accessDeviceEndpoint>";
					$scaInsert .= "<accessDevice>";
					$scaInsert .= "<deviceLevel>Group</deviceLevel>";
					$scaInsert .= "<deviceName>" . $this->deviceNames[$userId] . "</deviceName>";
					$scaInsert .= "</accessDevice>";
					$scaInsert .= "<linePort>" . $linePort . "</linePort>";
					if($this->staticLineOrderingFlag == "true"){
						$scaInsert .= "<portNumber>" . $scaPort . "</portNumber>";
					}
					$scaInsert .= "</accessDeviceEndpoint>";
					$scaInsert .= "<isActive>" . self::normalize(self::isActive(), $row_value[self::isActive()]) . "</isActive>";
					$scaInsert .= "<allowOrigination>" . self::normalize(self::allowOrigination(), $row_value[self::allowOrigination()]) . "</allowOrigination>";
					$scaInsert .= "<allowTermination>" . self::normalize(self::allowTermination(), $row_value[self::allowTermination()]) . "</allowTermination>";
					$scaInsert .= xmlFooter();
					$sca[] = $scaInsert;

					// Modify Shared Call Appearance
					$scaModify = xmlHeader($sessionid, "UserSharedCallAppearanceModifyRequest");
					$scaModify .= "<userId>" . $scaAddUserId . "</userId>";
					$scaModify .= "<alertAllAppearancesForClickToDialCalls>" . self::normalize(self::alertAllAppearancesForClickToDialCalls(), $row_value[self::alertAllAppearancesForClickToDialCalls()]) . "</alertAllAppearancesForClickToDialCalls>";
					$scaModify .= "<alertAllAppearancesForGroupPagingCalls>" . self::normalize(self::alertAllAppearancesForGroupPagingCalls(), $row_value[self::alertAllAppearancesForGroupPagingCalls()]) . "</alertAllAppearancesForGroupPagingCalls>";
					$scaModify .= "<allowSCACallRetrieve>" . self::normalize(self::allowSCACallRetrieve(), $row_value[self::allowSCACallRetrieve()]) . "</allowSCACallRetrieve>";
					$scaModify .= "<multipleCallArrangementIsActive>" . self::normalize(self::multipleCallArrangementIsActive(), $row_value[self::multipleCallArrangementIsActive()]) . "</multipleCallArrangementIsActive>";
					$scaModify .= "<allowBridgingBetweenLocations>" . self::normalize(self::allowBridgingBetweenLocations(), $row_value[self::allowBridgingBetweenLocations()]) . "</allowBridgingBetweenLocations>";
					$scaModify .= "<bridgeWarningTone>" . self::normalize(self::bridgeWarningTone(), $row_value[self::bridgeWarningTone()]) . "</bridgeWarningTone>";
					$scaModify .= "<enableCallParkNotification>" . self::normalize(self::enableCallParkNotification(), $row_value[self::enableCallParkNotification()]) . "</enableCallParkNotification>";
					$scaModify .= xmlFooter();
					$sca[] = $scaModify;

				}
			}

			// Rebuild Device
			$scaRebuild = xmlHeader($sessionid, "GroupCPEConfigRebuildDeviceConfigFileRequest");
			$scaRebuild .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
			$scaRebuild .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
			$scaRebuild .= "<deviceName>" . $this->deviceNames[$userId] . "</deviceName>";
			$scaRebuild .= xmlFooter();
			$sca[] = $scaRebuild;

			// Reset Device
			$scaReset = xmlHeader($sessionid, "GroupCPEConfigResetDeviceRequest");
			$scaReset .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
			$scaReset .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
			$scaReset .= "<deviceName>" . $this->deviceNames[$userId] . "</deviceName>";
			$scaReset .= xmlFooter();
			$sca[] = $scaReset;
		}

		if (isset($sca)) {
			foreach ($sca as $scaValue) {
				$response = $client->processOCIMessage(array("in0" => $scaValue));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
				if (readError($xml) != "") {
					$this->backEndErrorMsg = "OCI Error";
				}
			}
		}

		if ($deviceType) {

			$tagsetName = $this->sysOperations->getTagSetNameByDeviceType($deviceType, $ociVersion);

			$systemTagList = array();
			if(count($tagsetName["Success"]) > 0){
				$systemTagListResponse = $this->sysOperations->getDefaultTagListByTagSet($tagsetName["Success"]);
				$systemTagList = $systemTagListResponse["Success"];
			}


			/*
			//Update Phone Profile
			if(trim($row_value[self::phoneProfile()]) != "") {

				//error_log("Update phoneProfile : " . $row_value[self::phoneProfile()]);
				$phoneProfileValue = getPhoneProfileValue($row_value[self::phoneProfile()]);

				if($phoneProfileValue != null) {

					//error_log("phoneProfileValue $deviceName: $phoneProfileValue");

					//Update %phone-template% & %Express_Custom_Profile_Name%
					removeDeviceTag($_SESSION["sp"], $_SESSION["groupId"], $deviceName, '%phone-template%');
					removeDeviceTag($_SESSION["sp"], $_SESSION["groupId"], $deviceName, '%Express_Custom_Profile_Name%');
					if(!isset($systemTagList['%phone-template%']) || $phoneProfileValue != $systemTagList['%phone-template%'][0]){
						error_log("phoneProfileValue: $phoneProfileValue");
						addDeviceTag($_SESSION["sp"], $_SESSION["groupId"], $deviceName, '%phone-template%', $phoneProfileValue);
						addDeviceTag($_SESSION["sp"], $_SESSION["groupId"], $deviceName, '%Express_Custom_Profile_Name%', $phoneProfileValue);
					}

				}
			}

			//Tag Bundles
			if(trim($row_value[self::tagBundles()]) != "") {

				//error_log("Update tagBundles");

				//Update %Express_Tag_Bundle% custom tag
				removeDeviceTag($_SESSION["sp"], $_SESSION["groupId"], $deviceName, '%Express_Tag_Bundle%');
				if(!isset($systemTagList['%Express_Tag_Bundle%']) || $row_value[self::tagBundles()] != $systemTagList['%Express_Tag_Bundle%'][0]) {
					addDeviceTag($_SESSION["sp"], $_SESSION["groupId"], $deviceName, '%Express_Tag_Bundle%', $row_value[self::tagBundles()]);
				}

				$all_tags = getAllUniqueTagsForBundles();
				foreach ($all_tags as $tagName) {

					removeDeviceTag($_SESSION["sp"], $_SESSION["groupId"], $deviceName, $tagName);
					//error_log("Delete Tag: $tagName");

				}

				//Update all individual tags in the bundle
				$tagBundles = explode(";", $row_value[self::tagBundles()]);

				foreach ($tagBundles as $tagBundle) {
					$tags = getAllTagsforBundle($tagBundle);
					foreach ($tags as $tag) {

						$tagValue = $tag['tagValue'];

						if(!isset($systemTagList[$tag['tag']]) || $tagValue != $systemTagList[$tag['tag']][0]) {
							addDeviceTag($_SESSION["sp"], $_SESSION["groupId"], $deviceName, $tag['tag'], $tagValue);
						}

					}
				}
			}
			*/

			//Num of Line Apperances && Calls Per LineKey
			if(trim($row_value[self::polycomLineRegNumber()]) != "" && $row_value[self::polycomLineRegNumber()] > 0) {
				if($row_value[self::numOfLineApperances()] != "" && $row_value[self::numOfLineApperances()] > 0) {

					//error_log("Update numOfLineAppearances");

					//Delete Existing Tag
					removeDeviceTag($_SESSION["sp"], $_SESSION["groupId"], $deviceName, '%reg' . $row_value[self::polycomLineRegNumber()] . 'lineKeys%');

					$numOfLineAppearances = $row_value[self::numOfLineApperances()];
					// update custom tags from back-end
					if(!isset($systemTagList['%reg' . $row_value[self::polycomLineRegNumber()] . 'lineKeys%']) || $numOfLineAppearances != $systemTagList['%reg' . $row_value[self::polycomLineRegNumber()] . 'lineKeys%'][0]) {
						addDeviceTag($_SESSION["sp"], $_SESSION["groupId"], $deviceName, '%reg' . $row_value[self::polycomLineRegNumber()] . 'lineKeys%', $numOfLineAppearances);
					}
				}

				if($row_value[self::callsPerLineKey()] != "" && $row_value[self::callsPerLineKey()] > 0) {

					//error_log("Update callsPerLineKey");

					//Delete Existing Tag
					removeDeviceTag($_SESSION["sp"], $_SESSION["groupId"], $deviceName, '%CallsPerLine-' . $row_value[self::polycomLineRegNumber()] . '%');

					$callsPerLineKey = $row_value[self::callsPerLineKey()];
					// update custom tags from back-end
					if(!isset($systemTagList['%CallsPerLine-' . $row_value[self::polycomLineRegNumber()] . '%']) || $callsPerLineKey != $systemTagList['%CallsPerLine-' . $row_value[self::polycomLineRegNumber()] . '%'][0]) {
						addDeviceTag($_SESSION["sp"], $_SESSION["groupId"], $deviceName, '%CallsPerLine-' . $row_value[self::polycomLineRegNumber()] . '%', $callsPerLineKey);
					}

				}
			}

			require_once $_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig.php";
			rebuildAndResetDeviceConfig($deviceName, "rebuildAndReset");
		}


	}

	private function removeSCAItem($scaUserId, $sharedCallAppearanceUsers)
	{
		foreach ($sharedCallAppearanceUsers as $key => $sharedCallAppearanceUser) {
			if ($sharedCallAppearanceUser['id'] == $scaUserId) {
				unset($sharedCallAppearanceUsers[$key]);
				break;
			}
		}
		return $sharedCallAppearanceUsers;
	}

	private function removeSCAPort($scaPort, $sharedCallAppearanceUsers)
	{
		foreach ($sharedCallAppearanceUsers as $key => $sharedCallAppearanceUser) {
			if ($sharedCallAppearanceUser['port'] == $scaPort) {
				unset($sharedCallAppearanceUsers[$key]);
				break;
			}
		}
		return $sharedCallAppearanceUsers;
	}

	private function nextPortNumber($sharedCallAppearanceUsers)
	{

		$port = 0;
		foreach ($sharedCallAppearanceUsers as $key => $sharedCallAppearanceUser) {
			if ($sharedCallAppearanceUser['port'] > $port) {
				$port = $sharedCallAppearanceUser['port'];
			}
		}
		return $port + 1;
	}

	private function getPrimaryUserAttribute($userId, $attribute)
	{
		global $users;

		for ($i = 0; $i < count($users); $i++) {
			if ($users[$i]["id"] == $userId) {
				return $users[$i][$attribute];
			}
		}
		return "";
	}

	private function getSCALinePortName($scaPhoneNumber, $scaExtension, $userInfo)
	{

		global $scaLinePortCriteria1, $scaLinePortCriteria2;

		if ($scaPhoneNumber != "") {
			if (substr($scaPhoneNumber, 0, 3) == "+1-") {
				$scaPhoneNumber = substr($scaPhoneNumber, 3);
			}
		}

		$linePort = "";
		$linePortBuilder1 = new LineportNameBuilder($scaLinePortCriteria1,
			$scaPhoneNumber,
			$scaExtension,
			$this->getResourceForLineportCreation(ResourceNameBuilder::USR_CLID, $userInfo),
			$this->getResourceForLineportCreation(ResourceNameBuilder::GRP_DN, $userInfo),
			$this->getResourceForLineportCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $userInfo),
			$scaLinePortCriteria2 == "");  // force fallback only if second criteria does not exist

		if (!$linePortBuilder1->validate()) {
			// TODO: Implement resolution when lineport input formula has errors
		}

		do {
			$attr = $linePortBuilder1->getRequiredAttributeKey();
			$linePortBuilder1->setRequiredAttribute($attr, $this->getResourceForLineportCreation($attr, $userInfo));
		} while ($attr != "");

		$linePort = $linePortBuilder1->getName();

		if ($linePort == "") {
			// Create lineport name builder from the second formula. At this point we know that there is second
			// formula, because without second formula name would be forcefully resolved in the first formula.

			$linePortBuilder2 = new LineportNameBuilder($scaLinePortCriteria2,
				$scaPhoneNumber,
				$scaExtension,
				$this->getResourceForLineportCreation(ResourceNameBuilder::USR_CLID, $userInfo),
				$this->getResourceForLineportCreation(ResourceNameBuilder::GRP_DN, $userInfo),
				$this->getResourceForLineportCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $userInfo),
				true);

			if (!$linePortBuilder2->validate()) {
				// TODO: Implement resolution when User ID input formula has errors
			}

			do {
				$attr = $linePortBuilder2->getRequiredAttributeKey();
				$linePortBuilder2->setRequiredAttribute($attr, $this->getResourceForLineportCreation($attr, $userInfo));
			} while ($attr != "");

			$linePort = $linePortBuilder2->getName();
		}

		return str_replace(" ", "_", $linePort);
	}



	private function getResourceForLineportCreation($attr, $userInfo) {
		switch ($attr) {
			//case ResourceNameBuilder::DN:               return isset($_SESSION["userInfo"]["phoneNumber"]) ? $_SESSION["userInfo"]["phoneNumber"] : "";
			//case ResourceNameBuilder::EXT:              return isset($_SESSION["userInfo"]["extension"]) ? $_SESSION["userInfo"]["extension"] : "";
			case ResourceNameBuilder::USR_CLID:         return isset($userInfo["callingLineIdPhoneNumber"]) ? $userInfo["callingLineIdPhoneNumber"] : "";
			case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
			case ResourceNameBuilder::GRP_ID:           return $_SESSION["groupId"];
			case ResourceNameBuilder::SYS_DFLT_DOMAIN:  return $_SESSION["systemDefaultDomain"];
			case ResourceNameBuilder::DEFAULT_DOMAIN:   return $_SESSION["defaultDomain"];
			case ResourceNameBuilder::GRP_PROXY_DOMAIN: return $_SESSION["proxyDomain"];
			case ResourceNameBuilder::FIRST_NAME:       return isset($userInfo["firstName"])  ? $userInfo["firstName"] : "";
			case ResourceNameBuilder::LAST_NAME:        return isset($userInfo["lastName"]) ? $userInfo["lastName"] : "";
			case ResourceNameBuilder::DEV_NAME:         return isset($userInfo["lastName"]) && $userInfo["deviceName"] != "" ? $userInfo["deviceName"] : "";
			case ResourceNameBuilder::MAC_ADDR:         return isset($userInfo["macAddress"]) && $userInfo["macAddress"] != "" ? $userInfo["macAddress"] : "";
			default:                                    return "";
		}
	}


    //-----------------------------------------------------------------------------------------------------------------
    private function addAttribute($key, $headTitle) {
        $this->keys[] = $key;
        $this->headTitles[] = $headTitle;

        $this->numColumns++;
    }


    //-----------------------------------------------------------------------------------------------------------------
    private static function normalize($key, $value) {
        switch ($key) {
            case self::isActive(): return strtolower($value);
            case self::allowOrigination(): return strtolower($value);
            case self::allowTermination(): return strtolower($value);
            case self::alertAllAppearancesForClickToDialCalls(): return strtolower($value);
            case self::alertAllAppearancesForGroupPagingCalls(): return strtolower($value);
            case self::allowSCACallRetrieve(): return strtolower($value);
            case self::multipleCallArrangementIsActive(): return strtolower($value);
            case self::enableCallParkNotification(): return strtolower($value);
            case self::allowBridgingBetweenLocations(): return strtolower($value);
            case self::bridgeWarningTone():
                if (strtolower($value) == "none") {
                    return "None";
                }
                elseif (strtolower($value) == "barge-in") {
                    return "Barge-In";
                }
                elseif (strtolower($value) == "barge-in and repeat") {
                    return "Barge-In and Repeat";
                }
            break;
        }
    }
    
    
    public function userServicePackAssignment($scaId) {
        require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");   //Code added @ 10 July 2018
        require_once ("/var/www/lib/broadsoft/adminPortal/services/userServices.php");
        
        $message = "";
        $grpServiceList = new Services();
        $changeSCAAssignLog = array();
        $grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
        $servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
        $servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray);
        $scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);
        
        foreach($scaServiceArray as $ssa=>$ssv){
            $pos = strpos($ssv->col[0], "Shared Call Appearance");
            if($pos === false){
            }else{
                if($ssv->count == "1"){
                    $servicePackToBeAssigned = $servicePackArray[$ssa][0];
                }
            }
        }
        
        $userServices = new userServices();
        $userSessionservicePackArray = $userServices->getUserServicesAll($scaId, $_SESSION["sp"]);
        $servicesSessionArray = $grpServiceList->getServicePackServicesListUsers($userSessionservicePackArray["Success"]["ServicePack"]["assigned"]);
        $scaSessionServiceArray = $grpServiceList->checkScaUserServiceInServicepack($servicesSessionArray);
        
        //code ends
        if(count($scaSessionServiceArray) == 0){
            $responseServicePackUser = $grpServiceList->assignServicePackUser($scaId, $servicePackToBeAssigned);
            if(empty($responseServicePackUser["Error"])){
                $message = "SCA service pack has been assigned";
            } else {
                $message = $responseServicePackUser["Error"];
            }
        }
        
        return $message;
    }
    
    
    public function getUserServicePackToBeUnAssign($scaUserId) {
        require_once ("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");   //Code added @ 10 July 2018
        require_once ("/var/www/lib/broadsoft/adminPortal/services/userServices.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/UserGetRequest.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/getDataByMac.php");
        $scaArray = array(
            "Shared Call Appearance",
            "Shared Call Appearance 10",
            "Shared Call Appearance 15",
            "Shared Call Appearance 20",
            "Shared Call Appearance 25",
            "Shared Call Appearance 30",
            "Shared Call Appearance 35",
            "Shared Call Appearance 5",
        );
        $scaServiceToBeUnassign = "";
        $userservicePackArray = array();
        $userServices = new userServices();
        $userSessionservicePackArray = $userServices->getUserServicesAll($scaUserId, $_SESSION["sp"]);
        $userServicePackArr      = $userSessionservicePackArray["Success"]["ServicePack"]["assigned"];
        $scaServicePackArr       = $userServices->getServicePacksListHaveScaServices($userServicePackArr);
        $scaServicePackCustomArr = $userServices->getServicePacksHasOneScaServiceOnly($scaServicePackArr);
        $servicePackSCACounter = 0;
        foreach($scaServicePackCustomArr as $keyTmpN => $valTmpN)
        {
            if($valTmpN['hasOneSCAServiceOnly']=="true")
            {
                $scaServiceToBeUnassign = $keyTmpN;
                $servicePackSCACounter = $servicePackSCACounter + 1;
            }
        }
        if($servicePackSCACounter > 1) {
            /* unambiguity Case. */
            return false;
        }
        return $scaServiceToBeUnassign;
    }
}
