<?php
/**
 * Created by Karl.
 * Date: 10/12/2016
 */
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

if ($_SESSION["superUser"] != "1" && $_SESSION["permissions"]["expressSheets"] != "1")
{
	echo "<div style='color: red;text-align: center;'>You don't have permission to access Express Sheets.</div>";
	exit;
}

if($_SESSION["superUser"] != "1" && $_SESSION["permissions"]["changeSCA"] != "1") {
	echo "<div style='color: red;text-align: center;'>You don't have permission to provision SCA.</div>";
	exit;
}

require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");

require_once("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");
require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
require_once("/var/www/lib/broadsoft/adminPortal/LineportNameBuilder.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/ExpressSheetsSCAManager.php");

$scaManager = new ExpressSheetsSCAManager("excelData");
?>

<div class="formPadding">
	The following Shared Call Appearances have been provisioned:<br><br>
	<table border="1" align="center" class="bulkSCATable" style="width:96%;">
		<tr>
			<?php echo $scaManager->createTableHeader(); ?>
		</tr>
		<?php echo $scaManager->processBulk(); ?>
	</table>
	<div class='mainPageButton' id='mainPageButton'><a href='javascript:void(0)' id='downloadSCASuccessTable'>Download CSV</a></div>
</div>

<script>

$(document).ready(function(){
	$('#downloadSCASuccessTable').click(function() {
        var titles = [];
        var data = [];
        $('.bulkSCATable th').each(function() {    //table id here
          titles.push($(this).text());
        });

      
        $('.bulkSCATable td').each(function() {    //table id here
          data.push($(this).text());
        });
        
        
        var CSVString = prepCSVRowForSCASuccessTable(titles, titles.length, '');
        CSVString = prepCSVRowForSCASuccessTable(data, titles.length, CSVString);

        var blob = new Blob(["\ufeff", CSVString]);
        var fileName = "SuccessSCATable.csv";
        if (navigator.msSaveBlob) { // IE 10+
        	navigator.msSaveBlob(blob, fileName);
        }else{
        	var downloadLink = document.createElement("a");
            
            var url = URL.createObjectURL(blob);
            downloadLink.href = url;
            downloadLink.download = fileName;

            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
       
        
      });
	
});
function prepCSVRowForSCASuccessTable(arr, columnCount, initial) {
    var row = '';
    var delimeter = ',';
    var newLine = '\r\n';

    function splitArray(_arr, _count) {
      var splitted = [];
      var result = [];
      _arr.forEach(function(item, idx) {
        if ((idx + 1) % _count === 0) {
          splitted.push(item);
          result.push(splitted);
          splitted = [];
        } else {
          splitted.push(item);
        }
      });
      return result;
    }
    var plainArr = splitArray(arr, columnCount);
   
    plainArr.forEach(function(arrItem) {
      arrItem.forEach(function(item, idx) {
    	  item = "\""+item+"\"";
        row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
      });
      row += newLine;
    });
    return initial + row;
  }


</script>
