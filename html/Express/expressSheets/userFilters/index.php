<?php
/**
 * Created by Dipin Krishna.
 * Date: 12/15/17
 * Time: 11:00 AM
 */
?>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
checkLogin();

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/config.php");
//require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/DBLookup.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/expressSheetFunctions.php");

$sp = $_SESSION['sp'];
$groupId = $_SESSION['groupId'];

if(!isset($groupInfoData)) {
	unset($groupInfoData);
	require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/expressSheets/group_info.php");
}

//Get Device Types
$nonObsoleteDevices = array();
require_once($_SERVER['DOCUMENT_ROOT'] . '/../lib/broadsoft/adminPortal/getDevices.php');

// Build devices lists:  customDevices, deviceTypesDigitalVoIP, analogGatewayDevices
/*$deviceTypesAnalogGateways = array();
$sipGatewayLookup = new DBLookup ($db, "devices", "deviceName", "sipGateway");

// Get Custom Devices List from DB.
// If the list exists, only devices from the list will be available in drop-down for assignment.
$customDevices = array();
if ($useCustomDeviceList == "true") {
	$query = "select deviceName from customDeviceList";
	$result = $db->query($query);

	while ($row = $result->fetch()) {
		$customDevices [] = $row ["deviceName"];
	}
}

foreach ($nonObsoleteDevices as $key => $value) {
	// shortcut custom device list for Audio Codes until they are resolved
	$deviceIsAudioCodes = substr($value, 0, strlen("AudioCodes-")) == "AudioCodes-";

	if ($useCustomDeviceList == "true" && !in_array($value, $customDevices)) {
		if (!$deviceIsAudioCodes) {
			continue;
		}
	}
	if ($sipGatewayLookup->get($value) == "true" || $deviceIsAudioCodes) {
		$deviceTypesAnalogGateways [] = $value;
	} else {
		$deviceTypesDigitalVoIP [] = $value;
	}
}*/
require_once ("/var/www/html/Express/util/getAuthorizeDeviceTypes.php");

//Get Custom Tags
require_once ("/var/www/lib/broadsoft/adminPortal/customTagManagement.php");

$availableTagsArray = getAllCustomTags();

$preset_filters = getUserPresetFilters($_SESSION["adminId"]);
$preset_filters_checked = isset($preset_filters->Filters) ? $preset_filters->Filters : array();
$preset_filtersd_checked = isset($preset_filters->FiltersD) ? $preset_filters->FiltersD : array();
$preset_filters_options = isset($preset_filters->Filter) ? $preset_filters->Filter : array();

// Code added  by sollogics @ 14 March 2018 to manage column width of Users Table within Express Sheet
$colWidth = getTableWidth("userTable");
        $col1  = $colWidth['col1'];
        $col2  = $colWidth['col2'];
        $col3  = $colWidth['col3'];
        $col4  = $colWidth['col4'];
        $col5  = $colWidth['col5'];
        $col6  = $colWidth['col6'];
        $col7  = $colWidth['col7'];
        $col8  = $colWidth['col8'];
        $col9  = $colWidth['col9']; 
        $col10 = $colWidth['col10'];
        $col11 = $colWidth['col11'];
        $col12 = $colWidth['col12']; 
        $col13 = $colWidth['col13']; 
        $col14 = $colWidth['col14'];
        $col15 = $colWidth['col15'];
        $col16 = $colWidth['col16'];
        $col17 = $colWidth['col17'];
        $col18 = $colWidth['col18'];
        $col19 = $colWidth['col19'];
        $col20 = $colWidth['col20'];
        $col21 = $colWidth['col21'];
//Code End
?>
<link rel="stylesheet" type="text/css" href="/Express/js/queryBuilder/jquery.jui_filter_rules.css">
<script type="text/javascript" src="/Express/js/queryBuilder/localization/en.js"></script>
<script type="text/javascript" src="/Express/js/queryBuilder/jquery.jui_filter_rules.js"></script>


<form name="userFiltersForm" id="userFiltersForm" method="POST" class="form-horizontal">

	<input type="hidden" class="filtersAreEnabled" id="filtersAreEnabled" name="filtersAreEnabled" value="<?php echo isset($applyFiltersByDefault) && $applyFiltersByDefault ? 1 : 0; ?>"/>

	<div class="panel panel-default panel-forms" id="filterUsersValuesRow" <?php echo $showFiltersByDefault ? "" : 'style="display: none; background:#eef0f3;"'; ?>>

            <div id="filtersTitle" class="twoColWidth">
			<!--<h4 class="panel-title">
<a id="filtersStatusTitle" class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse" 
href="#filtersDiv" aria-expanded="false" aria-controls="filtersDiv">Filters - Not Applied
</a>
			</h4>-->

			<a class="btn filterTitleBtn" id="filtersStatusTitle">Filters - Not Applied <span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>

<!--New code found Start-->
	<div class="">
		<div id="filtersDiv" class="col-md-12 viewRadioUserDetail">
			
			<div class="panel-body">
				<div class="row twoColWidth">
					<div class="col-md-12">
						<div class="col-md-7">
						<h2  class="userListTxt filterh1title">User Filters </h2>
					</div>
					<div class="col-md-5">
						<input id="showActiveFilterBtn" class="" value="Switch View" type="button"/>
   					 	<input id="showAllFilterBtn" class="" value="Switch View" type="button"/>
					</div>
					</div>
					
				</div>
				
			<div class="row twoColWidth divCFAforStyle">

					<div class="col-md-12">
					<div class="col-md-12">

					<div class="form-group">
						<input type="checkbox" name="clearAllFiltersBtn" id="clearAllFiltersBtn" /><label class="labelText" for="clearAllFiltersBtn"><span></span></label><label class="labelText">Enable All Filters</label>
						<button type="button" id="resetAllFilters" class="resetAll">Reset All</button>
					</div>
					</div>
					</div>
					 
					<!--div style="margin:28px 18px;"><label style="margin-left:6px;"><input type="checkbox" name="clearAllFiltersBtn" id="clearAllFiltersBtn" /></label><label style="margin-left:5px;">Enable All Filters</label></div>
					<div class="filterDisableOverlay" style="display:none;position: absolute;height: 100%;width: 100%;top: 0;left: 0;z-index: 1000;background: #faebd7b0;">&nbsp;</div>
					
					<!--span style="margin: 0 24px; width:30%; float:left " class="filterh1title">User Filters </span>

					<div style="float:right; width:60%; xmargin-top:30px; margin-right:21px">
						<!-- input id="showActiveFilterBtn" name="" class="btn btn-primary center-block btn-averistar" value="Show Active Filters" style="padding:9px 15px; float:left" type="button"/>
   					 	<input id="showAllFilterBtn" name="" class="btn btn-primary center-block btn-averistar" value="Show All Filters" style="float:left; padding:9px 15px; float:left" type="button"/> 
   					 	<button type="button" id="resetAllFilters" class="pull-right btn btn-averistar">Reset All</button>
				 	</div>

				 	<br/-->
					
<!--New code found End-->
	
	<div class="user_filter_set" id="defaultUserFiltersDiv">

						<table class="table table-condensed">
							<tr <?php echo in_array('RegisteredUser', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td style="width: 30px;">
									<!-- <input <?php // echo in_array('RegisteredUser', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="RegisteredUser"
											class="presetToggleCheckbox"
											data-filter="RegisteredUser">-->
											
											<input <?php //echo in_array('RegisteredUser', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('RegisteredUser', $preset_filters_checked) ? "RegisteredUser" : ""; ?>"
											data-filter="RegisteredUser" class="filterH">
											
											<input type="hidden"
											name="FiltersDD[]"
											value="">
											<!--New code found Start-->																				
											<label class="switch">
												<input <?php echo in_array('RegisteredUser', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="RegisteredUser"
												class="filterCheckStatus switch-input"
												data-filterd="RegisteredUser">
												<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
											</label>
										<!--New code found End-->		
								</td>
								<td>
									<span class='expandFilterClass'>Registration Status</span>
									<div class="FilterOptions" id="RegisteredUser_FilterOptions" <?php echo in_array('RegisteredUser', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
									
										<div class="col-xs-8 paddingZero">
											<div class="radio-inline">
											 			<label>
        											 		<input <?php echo (isset($preset_filters_options->RegisteredUser) && $preset_filters_options->RegisteredUser == 'YES') ? "checked" : ""; ?>
        															type="radio"
        															name="Filter[RegisteredUser]"
        															id="Registered_UserFilterYES"
        															class="filterOptionRadio"
        															value="YES">
															<label for="Registered_UserFilterYES" class="labelText labelTextUFilter"><span></span>Registered</label>
														</label>
													</div>
											<div class="radio-inline">
														<input <?php echo (isset($preset_filters_options->RegisteredUser) && $preset_filters_options->RegisteredUser == 'NO') ? "checked" : ""; ?>
													type="radio"
													name="Filter[RegisteredUser]"
													id="Registered_UserFilterNO"
													class="filterOptionRadio"
													value="NO">
													
													<label for="Registered_UserFilterNO" class="labelText labelTextUFilter"><span></span>Not Registered</label>
								 			</div>
										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('ActivatedUser', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php //echo in_array('ActivatedUser', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="ActivatedUser"
											class="presetToggleCheckbox"
											data-filter="ActivatedUser">-->
											
										<input 
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('ActivatedUser', $preset_filters_checked) ? "ActivatedUser" : ""; ?>"
											data-filter="ActivatedUser" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
						
										<label class="switch">
											<input <?php echo in_array('ActivatedUser', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="ActivatedUser"
											class="filterCheckStatus switch-input"
											data-filterd="ActivatedUser">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
										
								</td>
								<td>
									<span class='expandFilterClass'>Activation Status</span>
									<div class="FilterOptions" id="ActivatedUser_FilterOptions" <?php echo in_array('ActivatedUser', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->ActivatedUser) && $preset_filters_options->ActivatedUser == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[ActivatedUser]"
															id="Activated_UserFilterYES"
															class="filterOptionRadio"
															value="YES">
													<label class="labelText labelTextUFilter" for="Activated_UserFilterYES"><span></span>Activated</label>
											</label>
											</div>
											
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->ActivatedUser) && $preset_filters_options->ActivatedUser == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[ActivatedUser]"
															id="Activated_UserFilterNO"
															class="filterOptionRadio"
															value="NO">
															<label for="Activated_UserFilterNO" class="labelText labelTextUFilter"><span></span>Not Activated</label>
												</label>
											
											</div>	
												</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
									
							</td>
						</tr>
						
							<tr <?php echo in_array('ExtensionOnly', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php // echo in_array('ExtensionOnly', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="ExtensionOnly"
											class="presetToggleCheckbox"
											data-filter="ExtensionOnly">-->
											
										<input <?php //echo in_array('ExtensionOnly', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('ExtensionOnly', $preset_filters_checked) ? "ExtensionOnly" : ""; ?>"
											data-filter="ExtensionOnly" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<label class="switch">
											<input <?php echo in_array('ExtensionOnly', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="ExtensionOnly"
												class="filterCheckStatus switch-input"
												data-filterd="ExtensionOnly">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>

								</td>
								<td>
									<span class='expandFilterClass'>Phone Number/Extension</span>
									<div class="FilterOptions" id="ExtensionOnly_FilterOptions" <?php echo in_array('ExtensionOnly', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->ExtensionOnly) && $preset_filters_options->ExtensionOnly == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[ExtensionOnly]"
															id="ExtensionOnly_UserFilterNO"
															class="filterOptionRadio"
															value="NO">
													<label class="labelText labelTextUFilter" for="ExtensionOnly_UserFilterNO"><span></span>Phone Number and Extension</label>
												</label>
										 	</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->ExtensionOnly) && $preset_filters_options->ExtensionOnly == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[ExtensionOnly]"
															id="ExtensionOnly_UserFilterYES"
															class="filterOptionRadio"
															value="YES">
													<label class="labelText labelTextUFilter" for="ExtensionOnly_UserFilterYES"><span></span>Extension Only</label>
												</label>
												</div>
											</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
							</td>
						</tr>
						<tr <?php echo in_array('DeviceLessUser', $preset_filters_checked) ? "class='active'" : ""; ?>>
							<td>										
								<!-- <input <?php // echo in_array('DeviceLessUser', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="DeviceLessUser"
											class="presetToggleCheckbox"
											data-filter="DeviceLessUser">-->
											
											<input <?php //echo in_array('DeviceLessUser', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('DeviceLessUser', $preset_filters_checked) ? "DeviceLessUser" : ""; ?>"
											data-filter="DeviceLessUser" class="filterH">
											
											<input type="hidden"
											name="FiltersDD[]"
											value="">
											
											<!-- input <?php //echo in_array('DeviceLessUser', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="DeviceLessUser"
											class="filterCheckStatus"
											id="checkboxDeviceLessUser"
											data-filterd="DeviceLessUser"><label for="checkboxDeviceLessUser"><span></span></label>
											data-filterd="DeviceLessUser"-->
											
											<label class="switch">
												<input <?php echo in_array('DeviceLessUser', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="DeviceLessUser"
												class="filterCheckStatus switch-input"
												data-filterd="DeviceLessUser">
												<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
											</label>

								</td>
								<td>
									<span class='expandFilterClass'>Device Assignment</span>
									<div class="FilterOptions" id="DeviceLessUser_FilterOptions" <?php echo in_array('DeviceLessUser', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->DeviceLessUser) && $preset_filters_options->DeviceLessUser == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[DeviceLessUser]"
															id="DeviceLess_UserUserFilterNO"
															class="filterOptionRadio"
															value="NO">
													<label class="labelText labelTextUFilter" for="DeviceLess_UserUserFilterNO"><span></span>Device Assigned</label>
												</label>
											 </div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->DeviceLessUser) && $preset_filters_options->DeviceLessUser == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[DeviceLessUser]"
															id="DeviceLess_UserUserFilterYES"
															class="filterOptionRadio"
															value="YES">
													<label class="labelText labelTextUFilter" for="DeviceLess_UserUserFilterYES"><span></span>Device Not Assigned</label>
												</label>
											 	</div>
											</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
							</td>
						</tr>
							
						<tr <?php echo in_array('DnD', $preset_filters_checked) ? "class='active'" : ""; ?>>
							<td>
									<!-- <input <?php // echo in_array('DnD', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="DnD"
											class="presetToggleCheckbox"
											data-filter="DnD">-->
											
										<input <?php //echo in_array('DnD', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('DnD', $preset_filters_checked) ? "DnD" : ""; ?>"
											data-filter="DnD" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!--input <?php echo in_array('DnD', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="DnD"
											class="filterCheckStatus"
											data-filterd="DnD"-->
											
										<label class="switch">
											<input <?php echo in_array('DnD', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="DnD"
												class="filterCheckStatus switch-input"
												data-filterd="DnD">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Do Not Disturb</span>
									<div class="FilterOptions" id="DnD_FilterOptions" <?php echo in_array('DnD', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->DnD) && $preset_filters_options->DnD == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[DnD]"
															id="DnD_UserFilterYES"
															class="filterOptionRadio"
															value="YES">
													<label class="labelText labelTextUFilter" for="DnD_UserFilterYES"><span></span>Enabled</label>
												</label>
											 </div>	
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->DnD) && $preset_filters_options->DnD == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[DnD]"
															id="DnD_UserFilterNO"
															class="filterOptionRadio"
															value="NO">
													<label class="labelText labelTextUFilter" for="DnD_UserFilterNO"><span></span>Disabled</label>
												</label>
												</div>	 
											</div>	
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
							</td>
						</tr>
						
							<tr <?php echo in_array('SIP_ANALOG', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!--<input <?php // echo in_array('SIP_ANALOG', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="SIP_ANALOG"
											class="presetToggleCheckbox"
											data-filter="SIP_ANALOG">-->
											
										<input <?php //echo in_array('SIP_ANALOG', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('SIP_ANALOG', $preset_filters_checked) ? "SIP_ANALOG" : ""; ?>"
											data-filter="SIP_ANALOG" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php //echo in_array('SIP_ANALOG', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="SIP_ANALOG"
											class="filterCheckStatus"
											id="checkboxSIP_ANALOG"
											data-filterd="SIP_ANALOG"><label for="checkboxSIP_ANALOG"><span></span></label>
											data-filterd="SIP_ANALOG"-->
										
										<label class="switch">
											<input <?php echo in_array('SIP_ANALOG', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="SIP_ANALOG"
												class="filterCheckStatus switch-input"
												data-filterd="SIP_ANALOG">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>SIP(digital)/Analog Users</span>
									<div class="FilterOptions" id="SIP_ANALOG_FilterOptions" <?php echo in_array('SIP_ANALOG', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->SIP_ANALOG) && $preset_filters_options->SIP_ANALOG == 'DIGITAL') ? "checked" : ""; ?>
															type="radio"
															name="Filter[SIP_ANALOG]"
															id="SIP_ANALOG_UserFilterYES"
															class="filterOptionRadio"
															value="DIGITAL">
													<label class="labelText labelTextUFilter" for="SIP_ANALOG_UserFilterYES"><span></span>SIP(digital)</label>
												</label>
											 </div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->SIP_ANALOG) && $preset_filters_options->SIP_ANALOG == 'ANALOG') ? "checked" : ""; ?>
															type="radio"
															name="Filter[SIP_ANALOG]"
															id="SIP_ANALOG_UserFilterNO"
															class="filterOptionRadio"
															value="ANALOG">
													<label class="labelText labelTextUFilter" for="SIP_ANALOG_UserFilterNO"><span></span>Analog</label>
												</label>
												</div> 
											</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
<!--New code found Start-->
							<tr <?php echo in_array('Voice_Messaging', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php // echo in_array('Voice_Messaging', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="Voice_Messaging"
											class="presetToggleCheckbox"
											data-filter="Voice_Messaging">-->
											
										<input <?php //echo in_array('Voice_Messaging', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('Voice_Messaging', $preset_filters_checked) ? "Voice_Messaging" : ""; ?>"
											data-filter="Voice_Messaging" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php //echo in_array('Voice_Messaging', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="Voice_Messaging"
											class="filterCheckStatus"
											id="checkboxVoice_Messaging"
											data-filterd="Voice_Messaging"><label for="checkboxVoice_Messaging"><span></span></label>
											data-filterd="Voice_Messaging"-->
										
										<label class="switch">
											<input <?php echo in_array('Voice_Messaging', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="Voice_Messaging"
												class="filterCheckStatus switch-input"
												data-filterd="Voice_Messaging">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>

								</td>
								<td>
									<span class='expandFilterClass'>Voice Messaging Service</span>
									<div class="FilterOptions" id="Voice_Messaging_FilterOptions" <?php echo in_array('Voice_Messaging', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->Voice_Messaging_Assigned) && $preset_filters_options->Voice_Messaging_Assigned == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[Voice_Messaging_Assigned]"
															id="Voice_Messaging_UserFilterAssigned"
															class="filterOptionRadio"
															value="YES">
															<label for="Voice_Messaging_UserFilterAssigned" class="labelText labelTextUFilter"><span></span>Assigned</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->Voice_Messaging_Assigned) && $preset_filters_options->Voice_Messaging_Assigned == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[Voice_Messaging_Assigned]"
															id="Voice_Messaging_UserFilterNotAssigned"
															class="filterOptionRadio"
															value="NO"><label for="Voice_Messaging_UserFilterNotAssigned" class="labelText labelTextUFilter"><span></span>Not Assigned</label>
												</label>
											</div>
											<br/>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->Voice_Messaging) && $preset_filters_options->Voice_Messaging == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[Voice_Messaging]"
															id="Voice_Messaging_UserFilterYES"
															class="filterOptionRadio"
															value="YES"><label for="Voice_Messaging_UserFilterYES" class="labelText labelTextUFilter"><span></span>Enabled</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->Voice_Messaging) && $preset_filters_options->Voice_Messaging == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[Voice_Messaging]"
															id="Voice_Messaging_UserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="Voice_Messaging_UserFilterNO" class="labelText labelTextUFilter"><span></span>Disabled</label>
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
<!--New code found End-->
							<tr <?php echo in_array('CFA_CFB_CFN', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php // echo in_array('CFA_CFB_CFN', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="CFA_CFB_CFN"
											class="presetToggleCheckbox"
											data-filter="CFA_CFB_CFN">-->
											
										<input <?php //echo in_array('CFA_CFB_CFN', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('CFA_CFB_CFN', $preset_filters_checked) ? "CFA_CFB_CFN" : ""; ?>"
											data-filter="CFA_CFB_CFN" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php //echo in_array('CFA_CFB_CFN', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="CFA_CFB_CFN"
											class="filterCheckStatus"
											id="checkBox_CFA_CFB_CFN"
											data-filterd="CFA_CFB_CFN"><label for="checkBox_CFA_CFB_CFN"><span></span></label>
											data-filterd="CFA_CFB_CFN"-->
										
										<label class="switch">
											<input <?php echo in_array('CFA_CFB_CFN', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="CFA_CFB_CFN"
												class="filterCheckStatus switch-input"
												data-filterd="CFA_CFB_CFN">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>

								</td>
								<td>
									<span class='expandFilterClass'>CFA/CFB/CFNA/CFNR</span>
									<div class="FilterOptions" id="CFA_CFB_CFN_FilterOptions" <?php echo in_array('CFA_CFB_CFN', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class=" control-label">

												<div class="row">
													<div class="radio-inline labelText">CFA:</div>

													<div class="radio-inline">
														<label>
														<input <?php echo (isset($preset_filters_options->CFA) && $preset_filters_options->CFA == 'YES') ? "checked" : ""; ?>
														type="radio"
														name="Filter[CFA]"
														id="CFA_UserFilterYES"
														class="filterOptionRadio"
														value="YES">
														<label class="labelText labelTextUFilter" for="CFA_UserFilterYES"><span></span>Enabled</label>
														</label>
												</div>
													<div class="radio-inline">
														<label>
														<input <?php echo (isset($preset_filters_options->CFA) && $preset_filters_options->CFA == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFA]"
																	id="CFA_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO">
														<label class="labelText labelTextUFilter" for="CFA_UserFilterNO"><span></span>Disabled</label>
														</label>
												</div>
											</div>

										<div class="row">
													<div class="radio-inline labelText">CFB:</div>

													<div class="radio-inline">
														<label>
														<input <?php echo (isset($preset_filters_options->CFB) && $preset_filters_options->CFB == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFB]"
																	id="CFB_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES">
														<label class="labelText labelTextUFilter" for="CFB_UserFilterYES"><span></span>Enabled</label>
														</label>
													</div>
													<div class="radio-inline">
														<label>
														<input <?php echo (isset($preset_filters_options->CFB) && $preset_filters_options->CFB == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFB]"
																	id="CFB_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO">
														<label class="labelText labelTextUFilter" for="CFB_UserFilterNO"><span></span>Disabled</label>
														</label>
													</div>
												</div>

										
										<div class="row">		 
													<div class="radio-inline labelText">CFNA:</div>

													<div class="radio-inline">
														<label>
														<input <?php echo (isset($preset_filters_options->CFN) && $preset_filters_options->CFN == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFN]"
																	id="CFN_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="CFN_UserFilterYES" class="labelText labelTextUFilter"><span></span>Enabled</label>
														</label>
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CFN) && $preset_filters_options->CFN == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFN]"
																	id="CFN_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="CFN_UserFilterNO" class="labelText labelTextUFilter"><span></span>Disabled</label>
														</label>
													</div>
												</div>

												<div class="row">
													<div class="radio-inline labelText">CFNR:</div>

													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CFR) && $preset_filters_options->CFR == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFR]"
																	id="CFR_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="CFR_UserFilterYES" class="labelText labelTextUFilter"><span></span>Enabled</label>
														</label>
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CFR) && $preset_filters_options->CFR == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFR]"
																	id="CFR_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="CFR_UserFilterNO" class="labelText labelTextUFilter"><span></span>Disabled</label>
														</label>
													</div>
												</div>

											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('BLF', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!--<input <?php //echo in_array('BLF', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="BLF"
											class="presetToggleCheckbox"
											data-filter="BLF">-->
											
										<input <?php //echo in_array('BLF', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('BLF', $preset_filters_checked) ? "BLF" : ""; ?>"
											data-filter="BLF" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php //echo in_array('BLF', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="BLF"
											class="filterCheckStatus"
											id="checkboxBLF"
											data-filterd="BLF"><label for="checkboxBLF"><span></span></label>
											data-filterd="BLF"-->
										
										<label class="switch">
											<input <?php echo in_array('BLF', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="BLF"
												class="filterCheckStatus switch-input"
												data-filterd="BLF">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>BLF (Monitoring)</span>
									<div class="FilterOptions" id="BLF_FilterOptions" <?php echo in_array('BLF', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->BLF) && $preset_filters_options->BLF == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[BLF]"
															id="BLF_UserFilterYES"
															class="filterOptionRadio"
															value="YES"><label class="labelText" for="BLF_UserFilterYES"><span></span>Yes</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->BLF) && $preset_filters_options->BLF == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[BLF]"
															id="BLF_UserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="BLF_UserFilterNO" class="labelText labelTextUFilter"><span></span>No</label>
															
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('SCA_PRIMARY', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!--<input <?php // echo in_array('SCA_PRIMARY', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="SCA_PRIMARY"
											class="presetToggleCheckbox"
											data-filter="SCA_PRIMARY">-->
											
										<input <?php //echo in_array('SCA_PRIMARY', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('SCA_PRIMARY', $preset_filters_checked) ? "SCA_PRIMARY" : ""; ?>"
											data-filter="SCA_PRIMARY" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php //echo in_array('SCA_PRIMARY', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="SCA_PRIMARY"
											class="filterCheckStatus"
											id="checkboxSCA_PRIMARY"
											data-filterd="SCA_PRIMARY"><label for="checkboxSCA_PRIMARY"><span></span></label>
											data-filterd="SCA_PRIMARY"-->
										
										<label class="switch">
											<input <?php echo in_array('SCA_PRIMARY', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="SCA_PRIMARY"
												class="filterCheckStatus switch-input"
												data-filterd="SCA_PRIMARY">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>

								</td>
								<td>
									<span class='expandFilterClass'>SCA Primary Users (users having shared call appearances on their devices)</span>
									<div class="FilterOptions" id="SCA_PRIMARY_FilterOptions" <?php echo in_array('SCA_PRIMARY', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->SCA_PRIMARY) && $preset_filters_options->SCA_PRIMARY == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[SCA_PRIMARY]"
															id="SCA_PRIMARY_UserFilterYES"
															class="filterOptionRadio"
															value="YES"><label for="SCA_PRIMARY_UserFilterYES" class="labelText labelTextUFilter"><span></span>Has SCA Users</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->SCA_PRIMARY) && $preset_filters_options->SCA_PRIMARY == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[SCA_PRIMARY]"
															id="SCA_PRIMARY_UserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="SCA_PRIMARY_UserFilterNO" class="labelText labelTextUFilter"><span></span>Has No SCA Users</label>
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('SCA_USER', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php // echo in_array('SCA_USER', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="SCA_USER"
											class="presetToggleCheckbox"
											data-filter="SCA_USER">-->
											
										<input <?php //echo in_array('SCA_USER', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('SCA_USER', $preset_filters_checked) ? "SCA_USER" : ""; ?>"
											data-filter="SCA_USER" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!--input <?php //echo in_array('SCA_USER', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="SCA_USER"
											class="filterCheckStatus"
											id="checkboxSCA_USER"
											data-filterd="SCA_USER"><label for="checkboxSCA_USER"><span></span></label>
											data-filterd="SCA_USER"-->
										
										<label class="switch">
											<input <?php echo in_array('SCA_USER', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="SCA_USER"
												class="filterCheckStatus switch-input"
												data-filterd="SCA_USER">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>

								</td>
								<td>
									<span class='expandFilterClass'>SCA Users (SCA Appearances - users having appearances on other devices)</span>
									<div class="FilterOptions" id="SCA_USER_FilterOptions" <?php echo in_array('SCA_USER', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->SCA_USER) && $preset_filters_options->SCA_USER == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[SCA_USER]"
															id="SCA_USER_UserFilterYES"
															class="filterOptionRadio"
															value="YES"><label for="SCA_USER_UserFilterYES" class="labelText labelTextUFilter"><span></span>Has Appearances</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->SCA_USER) && $preset_filters_options->SCA_USER == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[SCA_USER]"
															id="SCA_USER_UserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="SCA_USER_UserFilterNO" class="labelText labelTextUFilter"><span></span>Has No Appearances</label>
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('DEVICE_TYPE', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php // echo in_array('DEVICE_TYPE', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="DEVICE_TYPE"
											class="presetToggleCheckbox"
											data-filter="DEVICE_TYPE">-->
											
										<input <?php //echo in_array('DEVICE_TYPE', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('DEVICE_TYPE', $preset_filters_checked) ? "DEVICE_TYPE" : ""; ?>"
											data-filter="DEVICE_TYPE" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php //echo in_array('DEVICE_TYPE', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="DEVICE_TYPE"
											class="filterCheckStatus"
											id="checkboxDEVICE_TYPE"
											data-filterd="DEVICE_TYPE"><label for="checkboxDEVICE_TYPE"><span></span></label>
											data-filterd="DEVICE_TYPE"-->
										
										<label class="switch">
											<input <?php echo in_array('DEVICE_TYPE', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="DEVICE_TYPE"
												class="filterCheckStatus switch-input"
												data-filterd="DEVICE_TYPE">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Device Type</span>
									<div class="FilterOptions" id="DEVICE_TYPE_FilterOptions" <?php echo in_array('DEVICE_TYPE', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="control-label">

												<div class="">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->DEVICE_TYPE) && $preset_filters_options->DEVICE_TYPE == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[DEVICE_TYPE]"
																	id="DEVICE_TYPE_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="DEVICE_TYPE_UserFilterYES" class="labelText labelTextUFilter"><span></span>With</label>
														</label>
														<label class="labelText"> &nbsp; OR &nbsp; </label>
														
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->DEVICE_TYPE) && $preset_filters_options->DEVICE_TYPE == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[DEVICE_TYPE]"
																	id="DEVICE_TYPE_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="DEVICE_TYPE_UserFilterNO" class="labelText labelTextUFilter"><span></span>Without</label>
														</label>
													</div>
												</div>
    												<div class="dropdown-wrap oneColWidth userFilterSelectType">
    										 			<select name="Filter[DEVICE_TYPE_SELECTED]" id="DEVICE_TYPE_SELECTED_UserFilter" class="form-control">
        													<option value="">Please Select</option>
        													<?php
        													asort($deviceTypesDigitalVoIP);
        													foreach ($deviceTypesDigitalVoIP as $deviceType) {
        														?>
        														<option <?php echo (isset($preset_filters_options->DEVICE_TYPE_SELECTED) && $preset_filters_options->DEVICE_TYPE_SELECTED == $deviceType) ? "SELECTED" :
        															"";
        														?>><?php echo $deviceType ?></option>
        														<?php
        													}
        													?>
        												</select>
    												</div>
    											</div>
											</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('SERVICE_PACK', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!--<input <?php // echo in_array('SERVICE_PACK', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="SERVICE_PACK"
											class="presetToggleCheckbox"
											data-filter="SERVICE_PACK">-->
											
										<input <?php //echo in_array('SERVICE_PACK', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('SERVICE_PACK', $preset_filters_checked) ? "SERVICE_PACK" : ""; ?>"
											data-filter="SERVICE_PACK" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php //echo in_array('SERVICE_PACK', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="SERVICE_PACK"
											class="filterCheckStatus"
											id="checkboxSERVICE_PACK"
											data-filterd="SERVICE_PACK"><label for="checkboxSERVICE_PACK"><span></span></label>
											data-filterd="SERVICE_PACK"-->
										
										<label class="switch">	
											<input <?php echo in_array('SERVICE_PACK', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="SERVICE_PACK"
												class="filterCheckStatus switch-input"
												data-filterd="SERVICE_PACK">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
											
								</td>
								<td>
									<span class='expandFilterClass'>Service Pack</span>
									<div class="FilterOptions" id="SERVICE_PACK_FilterOptions" <?php echo in_array('SERVICE_PACK', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="control-label">

												<div class="">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->SERVICE_PACK) && $preset_filters_options->SERVICE_PACK == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[SERVICE_PACK]"
																	id="SERVICE_PACK_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="SERVICE_PACK_UserFilterYES" class="labelText labelTextUFilter"><span></span>With</label>
														</label>
														<label class="labelText"> &nbsp; OR &nbsp; </label>
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->SERVICE_PACK) && $preset_filters_options->SERVICE_PACK == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[SERVICE_PACK]"
																	id="SERVICE_PACK_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="SERVICE_PACK_UserFilterNO" class="labelText labelTextUFilter"><span></span>Without</label>
														</label>
													</div>
												</div>
    												<div class="dropdown-wrap oneColWidth userFilterSelectType">
														<select name="Filter[SERVICE_PACK_SELECTED]" id="SERVICE_PACK_SELECTED_UserFilter" class="form-control">
        													<option value="">Please Select</option>
        													<?php
        													asort($servicePacks);
        													foreach ($servicePacks as $servicePack) {
        														?>
        														<option <?php echo (isset($preset_filters_options->SERVICE_PACK_SELECTED) && $preset_filters_options->SERVICE_PACK_SELECTED == $servicePack) ? "SELECTED" :
        															"";
        														?>><?php echo $servicePack ?></option>
        														<?php
        													}
        													?>
														</select>
													</div>
												</div>
											</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('USER_SERVICE', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!--<input <?php // echo in_array('USER_SERVICE', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="USER_SERVICE"
											class="presetToggleCheckbox"
											data-filter="USER_SERVICE">-->
											
										<input <?php //echo in_array('USER_SERVICE', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('USER_SERVICE', $preset_filters_checked) ? "USER_SERVICE" : ""; ?>"
											data-filter="USER_SERVICE" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php //echo in_array('USER_SERVICE', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="USER_SERVICE"
											class="filterCheckStatus"
											data-filterd="USER_SERVICE"
											id="checkBoxUSER_SERVICE"><label for="checkBoxUSER_SERVICE"><span></span></label>
											data-filterd="USER_SERVICE"-->
										
										<label class="switch">
											<input <?php echo in_array('USER_SERVICE', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="USER_SERVICE"
												class="filterCheckStatus switch-input"
												data-filterd="USER_SERVICE">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>							
								</td>
								<td>
									<span class='expandFilterClass'>User Service</span>
									<div class="FilterOptions" id="USER_SERVICE_FilterOptions" <?php echo in_array('USER_SERVICE', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="control-label">

												<div class="">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->USER_SERVICE) && $preset_filters_options->USER_SERVICE == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[USER_SERVICE]"
																	id="USER_SERVICE_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="USER_SERVICE_UserFilterYES" class="labelText labelTextUFilter"><span></span>With</label>
														</label>
														<label class="labelText"> &nbsp; OR &nbsp; </label>
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->USER_SERVICE) && $preset_filters_options->USER_SERVICE == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[USER_SERVICE]"
																	id="USER_SERVICE_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="USER_SERVICE_UserFilterNO" class="labelText labelTextUFilter"><span></span>Without</label>
														</label>
													</div>
												</div>
    												<div class="dropdown-wrap oneColWidth userFilterSelectType">
        												<select name="Filter[USER_SERVICE_SELECTED]" id="USER_SERVICE_SELECTED_UserFilter" class="form-control">
        													<option value="">Please Select</option>
        													<?php
        													asort($userServices);
        													foreach ($userServices as $userService) {
        														?>
        														<option <?php echo (isset($preset_filters_options->USER_SERVICE_SELECTED) && $preset_filters_options->USER_SERVICE_SELECTED == $userService) ? "SELECTED" :
        															"";
        														?>><?php echo $userService ?></option>
        														<?php
        													}
        													?>
        												</select>
													</div>
												</div>
											</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('CUSTOM_PROFILE', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php // echo in_array('CUSTOM_PROFILE', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="CUSTOM_PROFILE"
											class="presetToggleCheckbox"
											data-filter="CUSTOM_PROFILE">-->
											
										<input <?php //echo in_array('CUSTOM_PROFILE', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('CUSTOM_PROFILE', $preset_filters_checked) ? "CUSTOM_PROFILE" : ""; ?>"
											data-filter="CUSTOM_PROFILE" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('CUSTOM_PROFILE', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="CUSTOM_PROFILE"
											class="filterCheckStatus"
											id="checkboxCUSTOM_PROFILE"
											data-filterd="CUSTOM_PROFILE"><label for="checkboxCUSTOM_PROFILE"><span></span></label>
											data-filterd="CUSTOM_PROFILE"-->
										
										<label class="switch">
											<input <?php echo in_array('CUSTOM_PROFILE', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="CUSTOM_PROFILE"
												class="filterCheckStatus switch-input"
												data-filterd="CUSTOM_PROFILE">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Custom Profile</span>
									<div class="FilterOptions" id="CUSTOM_PROFILE_FilterOptions" <?php echo in_array('CUSTOM_PROFILE', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="control-label">

												<div class="">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CUSTOM_PROFILE) && $preset_filters_options->CUSTOM_PROFILE == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CUSTOM_PROFILE]"
																	id="CUSTOM_PROFILE_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="CUSTOM_PROFILE_UserFilterYES" class="labelText labelTextUFilter"><span></span>With</label>
														</label>
														<label class="labelText labelTextUFilter">OR</label>
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CUSTOM_PROFILE) && $preset_filters_options->CUSTOM_PROFILE == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CUSTOM_PROFILE]"
																	id="CUSTOM_PROFILE_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="CUSTOM_PROFILE_UserFilterNO" class="labelText labelTextUFilter"><span></span>Without</label>
														</label>
													</div>
												</div>
    												<div class="dropdown-wrap oneColWidth userFilterSelectType">
        												<select name="Filter[CUSTOM_PROFILE_SELECTED]" id="CUSTOM_PROFILE_SELECTED_UserFilter" class="form-control">
        													<option>None</option>
        													<?php
        													$custom_profiles = getDistictCustomProfiles();
        													asort($custom_profiles);
        													foreach ($custom_profiles as $profile) {
        														?>
        														<option <?php echo (isset($preset_filters_options->CUSTOM_PROFILE_SELECTED) && $preset_filters_options->CUSTOM_PROFILE_SELECTED == $profile[expressSheetCustomProfileName]) ? "SELECTED" :
        															"";
        														?>><?php echo $profile[expressSheetCustomProfileName] ?></option>
        														<?php
        													}
        													?>
        												</select>
													</div>
												</div>
											</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('CUSTOM_TAG', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php // echo in_array('CUSTOM_TAG', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="CUSTOM_TAG"
											class="presetToggleCheckbox"
											data-filter="CUSTOM_TAG">-->
											
										<input <?php //echo in_array('CUSTOM_TAG', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('CUSTOM_TAG', $preset_filters_checked) ? "CUSTOM_TAG" : ""; ?>"
											data-filter="CUSTOM_TAG" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php //echo in_array('CUSTOM_TAG', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="CUSTOM_TAG"
											class="filterCheckStatus"
											id="checkboxCUSTOM_TAG"
											data-filterd="CUSTOM_TAG"><label for="checkboxCUSTOM_TAG"><span></span></label>
											data-filterd="CUSTOM_TAG"-->
										
										<label class="switch">
											<input <?php echo in_array('CUSTOM_TAG', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="CUSTOM_TAG"
												class="filterCheckStatus switch-input"
												data-filterd="CUSTOM_TAG">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Custom Tag</span>
									<div class="FilterOptions" id="CUSTOM_TAG_FilterOptions" <?php echo in_array('CUSTOM_TAG', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="control-label">

												<div class="">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CUSTOM_TAG) && $preset_filters_options->CUSTOM_TAG == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CUSTOM_TAG]"
																	id="CUSTOM_TAG_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="CUSTOM_TAG_UserFilterYES" class="labelText labelTextUFilter"><span></span>With</label>
														</label>
														<label class="labelText labelTextUFilter">OR</label>
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CUSTOM_TAG) && $preset_filters_options->CUSTOM_TAG == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CUSTOM_TAG]"
																	id="CUSTOM_TAG_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="CUSTOM_TAG_UserFilterNO" class="labelText labelTextUFilter"><span></span>Without</label>
														</label>
													</div>
												</div>
    												<div class="dropdown-wrap oneColWidth userFilterSelectType">
        												<select name="Filter[CUSTOM_TAG_SELECTED]" id="CUSTOM_TAG_SELECTED_UserFilter" class="form-control">
        													<option>None</option>
        													<?php
        													//asort($custom_tags);
        													foreach ($availableTagsArray as $custom_tag) {
        														?>
        														<option <?php echo (isset($preset_filters_options->CUSTOM_TAG_SELECTED) && $preset_filters_options->CUSTOM_TAG_SELECTED == $custom_tag['shortDescription']) ? "SELECTED" :
        															"";
        														?>><?php echo $custom_tag['shortDescription'] ?></option>
        														<?php
        													}
        													?>
        												</select>
													</div>
												</div>
											</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('PHONE_NUMBER', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php // echo in_array('PHONE_NUMBER', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="PHONE_NUMBER"
											class="presetToggleCheckbox"
											data-filter="PHONE_NUMBER">-->
											
										<input <?php //echo in_array('PHONE_NUMBER', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('PHONE_NUMBER', $preset_filters_checked) ? "PHONE_NUMBER" : ""; ?>"
											data-filter="PHONE_NUMBER" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php //echo in_array('PHONE_NUMBER', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="PHONE_NUMBER"
											class="filterCheckStatus"
											id="checkboxPHONE_NUMBER"
											data-filterd="PHONE_NUMBER"><label for="checkboxPHONE_NUMBER"><span></span></label>
											data-filterd="PHONE_NUMBER"-->
											
											<label class="switch">
												<input <?php echo in_array('PHONE_NUMBER', $preset_filtersd_checked) ? "checked" : ""; ?>
													type="checkbox"
													name="FiltersD[]"
													value="PHONE_NUMBER"
													class="filterCheckStatus switch-input"
													data-filterd="PHONE_NUMBER">
												<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
											</label>
								</td>
								<td>
									<span class='expandFilterClass'>Phone Numbers</span>
									<div class="FilterOptions" id="PHONE_NUMBER_FilterOptions" <?php echo in_array('PHONE_NUMBER', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-10">

											<div class="control-label">

												<div class="row" id="uFilterNumber">
													<div class="radio-inline">
														<label>
    													<input <?php echo (isset($preset_filters_options->PHONE_NUMBER) && $preset_filters_options->PHONE_NUMBER == 'StartsWith') ? "checked" : ""; ?>
    																type="radio"
    																name="Filter[PHONE_NUMBER]"
    																id="PHONE_NUMBER_UserFilter_StartsWith"
    																class="filterOptionRadio"
    																value="StartsWith">
    													<label class="labelText" for="PHONE_NUMBER_UserFilter_StartsWith"><span></span>Starts With:</label>
														</label>
    												</div>
    											
        												<input type="number"
        													       name="Filter[PHONE_NUMBER_STARTS_WITH]"
        													       id="PHONE_NUMBER_STARTS_WITH"
        													       class="filterOptionText noTextColor"
        													       placeholder="Eg: 301, 301916"
													       	       min="0"
        													       value="<?php echo isset($preset_filters_options->PHONE_NUMBER_STARTS_WITH) ? $preset_filters_options->PHONE_NUMBER_STARTS_WITH : ""; ?>">
												</div>

												<div class="row" id="uFilterNumber">
													<div class="radio-inline">
														<label>
													<input <?php echo (isset($preset_filters_options->PHONE_NUMBER) && $preset_filters_options->PHONE_NUMBER == 'Range') ? "checked" : ""; ?>
    																	type="radio"
    																	name="Filter[PHONE_NUMBER]"
    																	id="PHONE_NUMBER_UserFilter_Range"
    																	class="filterOptionRadio"
    																	value="Range">
    												<label class="labelText" for="PHONE_NUMBER_UserFilter_Range"><span></span>Range From:</label>
														</label>
											</div>
    												<input type="number"
    													       name="Filter[PHONE_NUMBER_RANGE_FROM]"
    													       id="PHONE_NUMBER_RANGE_FROM"
													       class="filterOptionText"
    													       placeholder="Eg: 3019162000"
													       min="0"
													       value="<?php echo isset($preset_filters_options->PHONE_NUMBER_RANGE_FROM) ? $preset_filters_options->PHONE_NUMBER_RANGE_FROM : ""; ?>">
													<strong class="labelText labelTextUFilter"> To </strong>
														<input type="number"
													       name="Filter[PHONE_NUMBER_RANGE_TO]"
													       id="PHONE_NUMBER_RANGE_TO"
													       class="filterOptionText noTextColor"
													       placeholder="Eg: 3019162050"
														min="0"
												       value="<?php echo isset($preset_filters_options->PHONE_NUMBER_RANGE_TO) ? $preset_filters_options->PHONE_NUMBER_RANGE_TO : ""; ?>" style="">

												</div>
											</div>
										</div>
										<div class="col-xs-2 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('EXTENSION', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php //echo in_array('EXTENSION', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="EXTENSION"
											class="presetToggleCheckbox"
											data-filter="EXTENSION">-->
											
										<input <?php //echo in_array('EXTENSION', $preset_filters_checked) ? "checked" : ""; ?>
											type="hidden"
											name="Filters[]"
											value="<?php echo in_array('EXTENSION', $preset_filters_checked) ? "EXTENSION" : ""; ?>"
											data-filter="EXTENSION" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!--input <?php //echo in_array('EXTENSION', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="EXTENSION"
											class="filterCheckStatus"
											data-filterd="EXTENSION"
											id="checkboxEXTENSION"><label for="checkboxEXTENSION"><span></span></label>
											data-filterd="EXTENSION"-->
										
										<label class="switch">
											<input <?php echo in_array('EXTENSION', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="EXTENSION"
												class="filterCheckStatus switch-input"
												data-filterd="EXTENSION">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Extensions</span>
									<div class="FilterOptions" id="EXTENSION_FilterOptions" <?php echo in_array('EXTENSION', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-10">

											<div class=" control-label">

												<div class="row" id="uFilterNumber">
													<div class="radio-inline">
														<label>
														<input <?php echo (isset($preset_filters_options->EXTENSION) && $preset_filters_options->EXTENSION == 'StartsWith') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[EXTENSION]"
																	id="EXTENSION_UserFilter_StartsWith"
																	class="filterOptionRadio"
																	value="StartsWith"><label for="EXTENSION_UserFilter_StartsWith" class="labelText labelTextUFilter"><span></span>Starts With:</label>
														</label>
													</div>
													<input type="number"
													       name="Filter[EXTENSION_STARTS_WITH]"
													       id="EXTENSION_STARTS_WITH"
													       class="filterOptionText noTextColor"
													       placeholder="Eg: 200, 210"
													       min="0"
													       value="<?php echo isset($preset_filters_options->EXTENSION_STARTS_WITH) ? $preset_filters_options->EXTENSION_STARTS_WITH : ""; ?>">
												</div>

												<div class="row" id="uFilterNumber">
													<div class="radio-inline">
														<label>
													<input <?php echo (isset($preset_filters_options->EXTENSION) && $preset_filters_options->EXTENSION == 'Range') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[EXTENSION]"
																	id="EXTENSION_UserFilter_Range"
																	class="filterOptionRadio"
																	value="Range"><label for="EXTENSION_UserFilter_Range" class="labelText labelTextUFilter"><span></span>Range From:</label>
														</label>
													</div>
													<input type="number"
													       name="Filter[EXTENSION_RANGE_FROM]"
													       id="EXTENSION_RANGE_FROM"
													       class="filterOptionText noTextColor"
													       placeholder="Eg: 2000"											
														   min="0"
													       value="<?php echo isset($preset_filters_options->EXTENSION_RANGE_FROM) ? $preset_filters_options->EXTENSION_RANGE_FROM : ""; ?>" style ="">
													<strong class="labelText labelTextUFilter"> To </strong>
													<input type="number"
												       name="Filter[EXTENSION_RANGE_TO]"
												       id="EXTENSION_RANGE_TO"
												       class="filterOptionText noTextColor"
												       placeholder="Eg: 2050"
													   min="0"
												       value="<?php echo isset($preset_filters_options->EXTENSION_RANGE_TO) ? $preset_filters_options->EXTENSION_RANGE_TO : ""; ?>"  style ="">
												</div>
											</div>
										</div>
										<div class="col-xs-2 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>

									</div>
								</td>
							</tr>
						</table>
			</div>
		</div>


				<?php
			//	if ($advancedUserFilters == "true") { //ex-623 regarding hide
					?>


					<!--   <div class="row formspace well" style="position: relative;margin:10px">

						<div class="filterDisableOverlay" style="display:none;position: absolute;height: 100%;width: 100%;top: 0;left: 0;z-index: 1000;background: #faebd7b0;">&nbsp;</div>

						<h2 class="userListTxt" style="margin-top: 35px;">Advanced Filters</h2>
 
						<div class="col-md-12" id="addNewFilterSetButtonDiv">
							<button type="button" id="addNewFilterSet" class="">Add New Advanced Filter</button>
						</div>
					</div>   
                    --> 
					<?php
				//}
				?>

				<div class="row twoColWidth">
					<div class="col-md-12">
					<div class="col-md-12">
						<div class="form-group" style="text-align: center;">
							<button type="button" class="subButton applyFilterBtn" style="">Apply Filters</button>
					<!-- <button type="button" id="clearAllFiltersBtn" class="deleteBtn marginRightButton">Disable Filters</button>
					<button type="button" id="enableAllFiltersBtn" class="btn btn-averistar subButton" style="display: none;">Enable Filters</button>-->
						</div>
						</div>
					</div>
				</div>

				<!-- User List-->
				<?php

				if ($need_user_table) {
					?>
<!--New code found Start-->

					<div class="selectClassHide dataTables_wrapper">
    					<div class="row">
            					<div class="">
            						<input type="checkbox" id="selectAllButtonExpSheet" <?php if(isset($selectAllButtonExpSheetChk) && $selectAllButtonExpSheetChk == "true"){echo "checked";}?> style="margin-left:18px;">
                                                        <label class="labelText" for="selectAllButtonExpSheet" style="float:left">
                                                            <span class="selectAllSpan"></span>Select All</label>
            						<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="You may also select Users by clicking checkboxes in the table, then selecting Group-Wide Modify actions from pull-down menu"><img src="images/NewIcon/info_icon.png"></a>
            					
<!--             					<div class="row-fluid"> -->
<!-- 									<button id="infoIconES" href="#selectAllInfo" class="btn btn-link no-focus" title="Instructions" data-toggle="collapse"> -->
<!-- 									<i class="glyphicon glyphicon-info-sign"></i></button> -->
<!-- 									<div id="selectAllInfo" class="collapse"> -->
<!-- 										<div class="alert alert-info" role="alert"> -->
<!-- 										you may also select Users by clicking checkboxes in the table, then selecting Group-Wide Modify actions from pull-down menu -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</div> -->
								
            					
            					</div>
    					</div>
                    </div>

					<div class="row userFilterHtml" id="usersTableDiv"></div>
                                       <!-- <div class="row formspace" id="usersTableDiv" style="display: none;">-->
<!--                      new code start to remove
					<br style="clear:both"/><br style="clear:both"/>
                    <div class="row formspace selectClassHide">
                    	<input type="checkbox" id ="selectAllButtonExpSheet" <?php if(isset($selectAllButtonExpSheetChk) && $selectAllButtonExpSheetChk == "true"){echo "checked";}?> style="margin-left:18px;"><b style="padding-left:5px;">Select All</b>
					new code -->
                                         
					<?php
				}
				?>

		</div>
		</div>

	</div>
<!-- </div> -->

</form>
	<input type="hidden" name="loadCount" id="loadCount" value="" />
	<input type="hidden" name="checkAllActiveStatus" id="checkAllActiveStatus" value="" />
	<input type="hidden" name="resetAllStatus" id="resetAllStatus" value="" />
	<input type="hidden" name="applyActiveClicked" id="applyActiveClicked" value="0" />
<?php if(isset($_POST["switchToExpress"]) && !empty($_POST["switchToExpress"])){$switchToExpress = $_POST["switchToExpress"];}else{$switchToExpress = 0;}?>
	<input type="hidden" name="switchToExpress" id="switchToExpress" value="<?php echo $switchToExpress;?>" />
<div style="clear:both"></div>
<div id="filterUsersLoading" class="loading"><img src="/Express/images/ajax-loader.gif" style="margin:15px 0"></div>
<div style="clear:both"></div>
<!-- User Filters Start -->
<script>
    /* Preset Filters */
    /*var selectedUsersExp = {};
    $("#selectAllButtonExpSheet").click(function () {
    	var selectAllCheck = $(this).is(":checked");
		var allCheckboxs = $(document).find(".DTFC_LeftBodyWrapper table td input[type=checkbox]");
		
		allCheckboxs.each(function() {debugger;
			
			var el = $(this);
			var elChked = el.is(":checked");
			var cName = el.attr("name");
			var cId = el.attr("id");
			if(elChked && !selectAllCheck) {
				
		        if (! el.hasOwnProperty(cName)) {
		            selectedUsersExp[cName] = false;
		        }
		        
		        if($('#'+cId+':visible').length != 0)
	            {
		        	selectedUsersExp[cName] = selectAllCheck;
		        	$(".DTFC_LeftBodyWrapper").find("#"+cId).prop("checked", false);
	            }  
			}else{
				
		            if($('#'+cId+':visible').length != 0)
		            {
		            	selectedUsersExp[cName] = selectAllCheck;
		            	// cId.checked = selectAllCheck;
		            	 $(".DTFC_LeftBodyWrapper").find("#"+cId).prop("checked", true);
		            }
		        //selectedUsers[cName] = selectAllCheck;
		       
			}
			
		});
    });
    function onUserInvidualSelect(checkbox) {
            var selectAllButtonExpSheet = document.getElementById("selectAllButtonExpSheet");
            selectAllButtonExpSheet.checked = false;

            // if User is clicked, create array element with userID as the key
            var userId = checkbox.name;
            if (! selectedUsersExp.hasOwnProperty(userId)) {
            	selectedUsersExp[userId] = false;
            }
            selectedUsersExp[userId] = checkbox.checked;

            // determine if there are any row check boxes checked
            var userChecked = false;
            for (var key in selectedUsersExp) {debugger;
                if (selectedUsersExp.hasOwnProperty(key) && selectedUsersExp[key]) {
                    userChecked = true;
                    break;
                }
            }

            // Make sure that Modification Forms are hidden if no checkbox is selected
            // And dropdown Modification Forms selection is not selecting any form
           /* if (! userChecked) {
                $("#gwMods option")[0].selected = true;
                disableAllModificationForms();
            }*/

            // Show 'Delete Selected Users' button as long as there is one row checkbox checked
           /* if (deleteUsersAllowed) {
                deleteButton.style.display = userChecked ? deleteButtonStyle : 'none';
            }
            if (expressSheetUsersAllowed) {
                expressSheetButton.style.display = userChecked ? expressSheetButtonStyle : 'none';
            }

            if (groupWideOperationsAllowed == "1") {
                // Show/hide group-wide drop-down operations menu
                modificationMenu.style.display = userChecked ? modificationMenuStyle : 'none';
            }*/
            //activeButton.style.display = userChecked ? activeButtonStyle : 'none';
            //deactiveButton.style.display = userChecked ? deactiveButtonStyle : 'none';        
        /*}*/
    $(function () {   
    	var selectedCheckBoxCount = "";
    	var selectAllIsChecked = "";

    	$("#selectAllButtonExpSheet").change(function(){  //"select all" change 

        	//console.log($(".DTFC_LeftBodyWrapper").find(".users_selected:visible").is(':checked'));
        	var selectedCount = selectedCheckBoxCount= $(".DTFC_LeftBodyWrapper").find(".user_selected_chk").length;
//         	var selectedCount = $(".DTFC_LeftBodyWrapper").find(".user_selected_chk:visible").length;
        	var selectIsChecked = selectAllIsChecked = $(this).is(':checked');
//         	 $(".DTFC_LeftBodyWrapper").find(".user_selected_chk:visible").each(function(){
        	 $(".DTFC_LeftBodyWrapper").find(".user_selected_chk").each(function(){
        		 var el = $(this);
    	    	 var cIds = el.attr("id");
    	    	 var cId = cIds.replace(/[\-\[\]\/\{\}\(\)\*\@\?\.\\\^\$\|]/g, "\\$&");
    	    	if(selectIsChecked){
            		$("#users_selected_count").html(selectedCount+" Users selected.");
            		$('.DTFC_LeftBodyWrapper').find("#"+cId).prop("checked",true);
            		$('#allUsersTableEx').find("#"+cId).prop("checked",true);
                }else{ 
                	$('.DTFC_LeftBodyWrapper').find("#"+cId).prop("checked",false);
                	$('#allUsersTableEx').find("#"+cId).prop("checked",false);
                	$("#users_selected_count").html("0 Users selected.");
                }
             
        	});
//         	 var selectedCount = selectedCheckBoxCount= $(".DTFC_LeftBodyWrapper").find(".user_selected_chk").length;
//     		$(".someDiv:visible").each(....);
//     	    $(".users_selected").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
    	});

    	//".checkbox" change 
    	//$('.user_selected_chk').change(function(){
    	//$(document).on("click", ".user_selected_chk", function() {
    	$(document).on('click', '.user_selected_chk', function(){

    		var el = $(this);
    		var elChked = el.is(":checked");
    		var cName = el.attr("name");
    		var cIds = el.attr("id");
    		var cId = cIds.replace(/[\-\[\]\/\{\}\(\)\*\@\?\.\\\^\$\|]/g, "\\$&");
    		
    		$(".DTFC_LeftBodyWrapper #"+cId).prop("checked", $(this).is(":checked"));
		
    		// added for count selected user
    		var selectedUseCount = "";
    		if(selectAllIsChecked) {
        		if($(this).is(":checked")) {
        			selectedCheckBoxCount++;
                } else {
                	selectedCheckBoxCount--;
                }
    			
        	} else {
        		if($(this).is(":checked")) {
        			$(".DTFC_LeftBodyWrapper #"+cId).addClass("countableClass");
            		} else {
            			$(".DTFC_LeftBodyWrapper #"+cId).removeClass("countableClass");
                	}
        		selectedCheckBoxCount = $('.DTFC_LeftBodyWrapper').find('.countableClass').length;
            }
        	
    		$("#users_selected_count").html(selectedCheckBoxCount + " Users selected.");
    		
    		// end added for count selected user
    		
 	   	 	//uncheck "select all", if one of the listed checkbox item is unchecked
    	    if(false == $(this).prop("checked")){ //if this item is unchecked
    	        $("#selectAllButtonExpSheet").prop('checked', false); //change "select all" checked status to false
    	    }
    	    //check "select all" if all checkbox items are checked
    	    //if ($('.DTFC_LeftBodyWrapper .user_selected_chk:checked').length == $('.DTFC_LeftBodyWrapper').find('.user_selected_chk:visible').length ){
    	   // if ($('.DTFC_LeftBodyWrapper .user_selected_chk:checked').length == $('.DTFC_LeftBodyWrapper').find('.user_selected_chk').length ){
    	    if ($('.DTFC_LeftBodyWrapper').find('.user_selected_chk:checked').length == $('.DTFC_LeftBodyWrapper').find('.user_selected_chk').length ){
    	        $("#selectAllButtonExpSheet").prop('checked', true);
    	    }else{
    	    	 $("#selectAllButtonExpSheet").prop('checked', false);
    	    }

    	   
// 			var userCount = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").length;
// 			var userCountChecked = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").filter(":checked").length;
// 			var checkVal = userCount == userCountChecked ? true: false;
// 			$("#selectAllButtonExpSheet").prop("checked", checkVal);
    		
    	});
        
       /*
        $(".users_selected").click(function() {         
            if($(this).is(":checked"))
            {
                $("#"+this.id).prop("checked", true);
	 	$("#"+this.id).prop("disabled", false);                
            }
            if($(this).is(":not(:checked)"))
            {
                $("#"+this.id).prop("checked", false);
            }
  	});  
        */
        
        $(".presetToggleCheckbox").click(function () {
            var filter = $(this).data("filter");
            if ($(this).is(":checked")) {
                $("#" + filter + "_FilterOptions").show();
                $(this).closest('tr').addClass('active');
            } else {
                $("#" + filter + "_FilterOptions").hide();
                $(this).closest('tr').removeClass('active');
            }

            $("#filtersStatusTitle").html("Filters - Not Applied  <span class='glyphicon glyphicon-chevron-down'></span>");

        });

        $("#filtersStatusTitle").click(function(){
			//$("#filtersDiv").slideToggle();
			$("#filtersDiv").toggle();
			$(".applyFilterBtn").show();
        });

        $(".filterOptionRadio").change(function () {

	        $("#filtersStatusTitle").html("Filters - Not Applied <span class='glyphicon glyphicon-chevron-down'></span>");

        });

        $(".filterOptionText").change(function () {

            $("#filtersStatusTitle").html("Filters - Not Applied <span class='glyphicon glyphicon-chevron-down'></span>");

        });
    });

</script>


<script>

    var usersFromUserModule = <?php echo isset($usersFromUserModule) ? json_encode($usersFromUserModule) : json_encode(array()); ?>;
    var filtersAreDisabled = false;
    var resetFilters = false;

    $(function () {

        var filter_set_id = 0;
        $(".triggerColumn").trigger("click");
        //Add New Filter
        $("#addNewFilterSet").click(function () {

            filter_set_id++;
            $('<div id="filter_set_' + filter_set_id + '" class="user_filter_set col-md-12 divCFAforStyle" style="padding-top: 15px;"></div>').insertBefore("#addNewFilterSetButtonDiv");
            $("#filter_set_" + filter_set_id).loadTemplate($("#filterSetTemplate"),
                {
                    filter_set_id_id: 'filter_set_id_' + filter_set_id,
                    filter_set_id_value: 0,
                    filter_set_name_id: 'filter_set_name_' + filter_set_id,
                    filter_set_name_value: 'Filter ' + filter_set_id,
                    filterUsersValuesDiv_id: 'filterUsersValuesDiv_' + filter_set_id,
                    applyFilterBtn_id: 'applyFilterBtn_' + filter_set_id,
                    css_class: "user_filters active",
                    hide_enable: "display: none;",
                    hide_disable: "",
                    filterUsersSetContainer_id: 'filterUsersSetContainer_' + filter_set_id,
                    filter_delete_id: "deleteUserFilterBtn_" + filter_set_id,
                    filter_enable_id: "enableUserFilterBtn_" + filter_set_id,
                    filter_disable_id: "disableUserFilterBtn_" + filter_set_id,
                    data_id: filter_set_id
                }
            );

            apply_jui_filter(filter_set_id);

        });

        //Apply Filters
        $(".applyFilterBtn").click(function () {
            if (!filtersAreDisabled) {
                $("#filtersAreEnabled").val("1");
                if (typeof globalFiltersAreEnabled !== 'undefined') {
                    globalFiltersAreEnabled = 1;
                }
            }
            apply_active_filters();
        });

        //$("#clearAllFiltersBtn").click(function () {
        var disableAllFilter = function() {
        	$(".filterCheckStatus:checked").parent("label").parent("td").siblings("td").find(".expandFilterClass").trigger("click");
        	$(".filterCheckStatus").prop("checked", false);
        	$("#defaultUserFiltersDiv").find(".filterH").val('');
        	$("#defaultUserFiltersDiv tr").addClass("disableRow");
        	$("#defaultUserFiltersDiv tr").removeClass("enableRow");
        	$("#defaultUserFiltersDiv tr").removeClass("active");
            $("#filtersAreEnabled").val("0");
            
            if (typeof globalFiltersAreEnabled !== 'undefined') {
                globalFiltersAreEnabled = 0;
            }

            filtersAreDisabled = true;
            if (typeof globalfiltersAreDisabled !== 'undefined') {
                globalfiltersAreDisabled = true;
            }

            $(this).hide();
            $("#enableAllFiltersBtn").show();
            //$(".filterDisableOverlay").show();
            //$(".applyFilterBtn").prop("disabled", true);
            //$(".filterOptionRadio").prop("disabled", true);
            $(".filterOptionRadio").css("pointer-events", "none");
            $(".filterOptionRadio").parent("label").css("pointer-events", "none");
            //apply_active_filters();
            //$(".filterCheckStatus").prop("disabled", true);
            $(".filterCheckStatus").css("pointer-events", "none");
            $(".filterCheckStatus").addClass("penone");
            $(".filterCheckStatus").removeClass("peauto");
          //$(".FilterOptions").find("select").prop("disabled", true);
            $(".FilterOptions").find("select").css("pointer-events", "none");
          //$(".FilterOptions").find(".filterOptionText").prop("disabled", true);
            $(".FilterOptions").find(".filterOptionText").css("pointer-events", "none");
            $(".FilterOptions").hide();
            //$(".resetF").prop("disabled", true);
            $(".filterOptionText").val('');
            $("#filtersStatusTitle").html("Filters - Not Applied <span class='glyphicon glyphicon-chevron-down'></span>");
            $("#usersBanner").html("Users List / Group-wide Users Modify");
            $("#usersBanner").append('<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Click User Id link to switch to Modify User page"><img class="infoIcon" src="images/NewIcon/info_icon.png"></a>');
			//$("#usersBanner").append("<sup class='filtersApplied'>(Filters Applied)</sup>");
            var checkAllActiveStatusVal = $("#checkAllActiveStatus").val();
        	if(checkAllActiveStatusVal == "all"){
        		 $("#showAllFilterBtn").trigger("click");
        	}else{
        		 $("#showActiveFilterBtn").trigger("click");
        	}
        	
        	$(".filterCheckStatus").each(function(){ 
        		var el = $(this);
        		//commented for EX-679
//         		if(el.closest("tr").find("td").find(".filterOptionRadio").is(":checked")){
// 					el.prop("checked", true);
// 				}
        		
        		el.prop("checked", false);
				var dataVal = el.attr("data-filterd");
				el.parent("label").prev("input[type=hidden]").val(dataVal);
        	});
        	$(".filterCheckStatus:checked").trigger("click");
        	$("#applyActiveClicked").val('1');
           	
			if($(".active .enableRow").length == 0){
				if(checkAllActiveStatusVal == "all"){
					$("#clearAllFiltersBtn").parent("div").show();
	        	}else{
	        		$("#clearAllFiltersBtn").parent("div").hide();
	        	}
	    			
        	}else{
        		$("#clearAllFiltersBtn").parent("div").show();
        	}  
        };

        //$("#enableAllFiltersBtn").click(function () {
		var enableAllFilter = function() {
			//$(".filterCheckStatus").prop("checked", true);
			$("#filtersAreEnabled").val("1");
            if (typeof globalFiltersAreEnabled !== 'undefined') {
                globalFiltersAreEnabled = 1;
            }

            filtersAreDisabled = false;
            if (typeof globalfiltersAreDisabled !== 'undefined') {
                globalfiltersAreDisabled = false;
            }

            $(this).hide();
			//$("#clearAllFiltersBtn").show();
            $(".filterDisableOverlay").hide();
            $(".applyFilterBtn").prop("disabled", false);
            $(".filterOptionRadio").prop("disabled", false);
            $(".filterOptionRadio").css("pointer-events", "auto");
            //apply_active_filters();
            $(".FilterOptions").find(".resetF").css("pointer-events", "auto");
            $(".FilterOptions").find("select").css("pointer-events", "auto");
            $(".FilterOptions").find(".filterOptionText").prop("disabled", false);
            $(".FilterOptions").find(".filterOptionText").css("pointer-events", "auto");
            $(".resetF").prop("disabled", false);
            $(".filterOptionText").val('');

            $("#filtersStatusTitle").html("Filters - Not Applied <span class='glyphicon glyphicon-chevron-down fIcon'></span>");

            //$("#checkAllActiveStatus").val('active');

			$(".filterCheckStatus").each(function(){ 
				var el = $(this);
				if(el.closest("tr").find("td").find(".filterOptionRadio").is(":checked")){
					el.closest("tr").find("td").find(".filterCheckStatus").css("pointer-events", "auto");
					el.closest("tr").find("td").find(".filterCheckStatus").prop("disabled", false);
					el.closest("tr").addClass("enableRow");
					el.closest("tr").addClass("active");
					el.closest("tr").removeClass("disableRow");
					var filterHVal = el.parent("td").find(".filterH").attr("data-filter");
			    	el.parent("td").find(".filterH").val(filterHVal);
			    	el.prop("checked", true);
				}else{
					el.prop("checked", false);
					el.closest("tr").addClass("disableRow");
					el.closest("tr").removeClass("enableRow");
				}
				el.closest("tr").find("td").find(".filterCheckStatus").addClass("peauto");
				el.closest("tr").find("td").find(".filterCheckStatus").removeClass("penone");
				el.parent("label").prev("input[type=hidden]").val('');
				
            });
			var checkAllActiveStatusVal = $("#checkAllActiveStatus").val();
        	if(checkAllActiveStatusVal == "all"){
        		//$("#showAllFilterBtn").trigger("click");
        		if($(".active.enableRow").length > 0){
        		 	//$(".disableRow").hide();
        		 	$("#clearAllFiltersBtn").parent("div").show();
        		}
        	}else{
        		 $("#showActiveFilterBtn").trigger("click");
        		 if($(".filterOptionRadio:checked").length > 0){ 
	    			$("#clearAllFiltersBtn").parent("div").show();
	        	}else{
	        		$("#clearAllFiltersBtn").parent("div").hide();
	        	} 
        	}
        	$("#applyActiveClicked").val('1');
        };

		var checkedAllFilterStatus = function(){
			if($('#clearAllFiltersBtn').is(':checked')){
				enableAllFilter();
			}else{
				disableAllFilter();
			}
		};

		$('#clearAllFiltersBtn').click(function(){
			checkedAllFilterStatus();
		});
			
        

        $("#resetAllFilters").click(function() {
            if(confirm("All user filters will be reset. Proceed?")) {
                $(".user_filter_set input[type=checkbox]").prop("checked", false);
                $(".user_filter_set input[type=radio]").prop("checked", false);
                $(".user_filter_set input[type=text]").val('');
                $(".user_filter_set input[type=number]").val('');
                $(".user_filter_set option").prop("selected", false);
                $(".user_filter_set").find(".filterH").val('');
                $(".user_filter_set").find("tr").addClass('disableRow');
                $(".user_filter_set").find("tr").removeClass('active');
                $(".user_filter_set").find("tr").removeClass('enableRow');
                //$('form#userFiltersForm').reset();
                //
                $(".user_filter_set tr.active").removeClass('active');
                $(".FilterOptions").hide();

                $("#filtersAreEnabled").val('1');
                $(".filterOptionRadio").css("pointer-events", "auto");
                //$(".filterCheckStatus").prop("disabled", true);
                $(".filterCheckStatus").addClass("penone");
                $(".filterCheckStatus").prop("checked", false);

                resetFilters = true;
                $("#showActiveFilterBtn").css("float", "right");
                $("#loadCount").val('1');
                $("#resetAllStatus").val('1');

                $("#filtersStatusTitle").html("Filters - Not Applied <span class='glyphicon glyphicon-chevron-down'></span>");
                apply_active_filters();

             	//$("#usersBanner").html("Users List / Group-wide Users Modify");
                
                //checkedAllFilterStatus();
                if($('#clearAllFiltersBtn').is(':checked')){
                	$('#clearAllFiltersBtn').trigger("click");
                }
                var checkAllActiveStatusVal = $("#checkAllActiveStatus").val();
            	if(checkAllActiveStatusVal == "all"){
            		 $("#showAllFilterBtn").trigger("click");
            	}else{
            		 $("#showActiveFilterBtn").trigger("click");
            	}
                //$("#showActiveFilterBtn").trigger("click");
                $("#applyActiveClicked").val('1');
                $("#selectAllChk").prop("checked", false);
                $(".checkUserListBox").prop("checked", false);
                $(".users_selected").prop("checked", false);
                $("#selectAllButtonExpSheet").prop("checked", false);//Code added @ 08 May 2018 to uncheck if filter is reset
                $("#users_selected_count").html("0 Users selected.");
//        			$("#clearAllFiltersBtn").parent("label").hide();
//      			$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
            };
            

            
        });
                   



		<?php
		if(isset($advancedUserFilters) && $advancedUserFilters == "true") {

		$saved_user_filters = getAllUserFilters($_SESSION["adminId"]);
		foreach ($saved_user_filters as $saved_user_filter) {
		?>
        filter_set_id++;
        $('<div id="filter_set_' + filter_set_id + '" class="user_filter_set <?php echo $saved_user_filter['inactive'] ? 'inactive' : 'active'; ?>"></div>').insertBefore("#addNewFilterSetButtonDiv");
        $("#filter_set_" + filter_set_id).loadTemplate($("#filterSetTemplate"),
            {
                filter_set_id_id: 'filter_set_id_' + filter_set_id,
                filter_set_id_value: '<?php echo $saved_user_filter['id'] ?>',
                filter_set_name_id: 'filter_set_name_' + filter_set_id,
                filter_set_name_value: '<?php echo $saved_user_filter['filter_name'] ?>',
                filterUsersValuesDiv_id: 'filterUsersValuesDiv_' + filter_set_id,
                applyFilterBtn_id: 'applyFilterBtn_' + filter_set_id,
                css_class: "user_filters <?php echo $saved_user_filter['inactive'] ? 'inactive' : 'active'; ?>",
                hide_enable: "<?php echo $saved_user_filter['inactive'] ? '' : 'display: none;'; ?>",
                hide_disable: "<?php echo $saved_user_filter['inactive'] ? 'display: none;' : ''; ?>",
                filterUsersSetContainer_id: 'filterUsersSetContainer_' + filter_set_id,
                filter_delete_id: "deleteUserFilterBtn_" + filter_set_id,
                filter_enable_id: "enableUserFilterBtn_" + filter_set_id,
                filter_disable_id: "disableUserFilterBtn_" + filter_set_id,
                data_id: filter_set_id
            }
        );

        apply_jui_filter(filter_set_id);

        $("#filterUsersValuesDiv_" + filter_set_id).jui_filter_rules("setRules", jQuery.parseJSON('<?php echo $saved_user_filter['filter']; ?>'));
		<?php
		}
		}
		?>
                        
                        
                        
                        
        
        
       
    });

    function apply_jui_filter(filter_id) {

        $("#filterUsersValuesDiv_" + filter_id).jui_filter_rules({

            bootstrap_version: "3",

            filters: [
                {
                    filterName: "Registred", "filterType": "text", field: "registred", filterLabel: "Registred",
                    excluded_operators: ["in", "not_in",
                        "less", "less_or_equal",
                        "greater", "greater_or_equal",
                        "is_null", "is_not_null",
                        "begins_with", "not_begins_with", "contains", "not_contains", "ends_with", "not_ends_with"
                    ],
                    filter_interface: [
                        {
                            filter_element: "select"
                        }
                    ],
                    lookup_values: [
                        {lk_option: "No", lk_value: "No"},
                        {lk_option: "Yes", lk_value: "Yes", lk_selected: "yes"}
                    ]
                },
                {
                    filterName: "Activated", "filterType": "text", field: "activated", filterLabel: "Activated",
                    excluded_operators: ["in", "not_in",
                        "less", "less_or_equal",
                        "greater", "greater_or_equal",
                        "is_null", "is_not_null",
                        "begins_with", "not_begins_with", "contains", "not_contains", "ends_with", "not_ends_with"
                    ],
                    filter_interface: [
                        {
                            filter_element: "select"
                        }
                    ],
                    lookup_values: [
                        {lk_option: "No", lk_value: "No"},
                        {lk_option: "Yes", lk_value: "Yes", lk_selected: "yes"}
                    ]
                },
                {
                    filterName: "DND", "filterType": "text", field: "dnd", filterLabel: "DND",
                    excluded_operators: ["in", "not_in",
                        "less", "less_or_equal",
                        "greater", "greater_or_equal",
                        "is_null", "is_not_null",
                        "begins_with", "not_begins_with", "contains", "not_contains", "ends_with", "not_ends_with"
                    ],
                    filter_interface: [
                        {
                            filter_element: "select"
                        }
                    ],
                    lookup_values: [
                        {lk_option: "Off", lk_value: "Off"},
                        {lk_option: "On", lk_value: "On", lk_selected: "yes"}
                    ]
                }
            ],

            onValidationError: function (event, data) {
                alert(data["err_description"] + ' (' + data["err_code"] + ')');
                if (data.hasOwnProperty("elem_filter")) {
                    data.elem_filter.focus();
                }
            }

        });

        //Delete Filter
        $("#deleteUserFilterBtn_" + filter_id).click(function () {
            deleteFilter(this);
        });

        //Enable/Disable Filter
        $("#enableUserFilterBtn_" + filter_id).click(function () {
            toggleFilter(this, 0);
        });
        $("#disableUserFilterBtn_" + filter_id).click(function () {
            toggleFilter(this, 1);
        });

    }

    //Toggle apply filter button
    function toggle_apply_filter_btn() {

        if ($(".user_filters.active").length > 0) {
            $("#applyFilterBtn").show();
        } else {
            if ($(".user_filters").length > 0) {
                $("#applyFilterBtn").prop('disabled', true);
            } else {
                $("#applyFilterBtn").hide();
            }
        }

    }

    function checkUserLength(){
    	$('[data-toggle="tooltip"]').tooltip();
		var userCount = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").length;
		var userCountChecked = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").filter(":checked").length;
		userCount ? $("#selectAllDiv").show() : $("#selectAllDiv").hide();
		userCountChecked ? $("#gwModsLabel").show() : $("#gwModsLabel").hide();
		var checkVal = userCount == userCountChecked ? true: false;
		$("#selectAllButtonExpSheet").prop("checked", checkVal);
		selectAllPermission = checkVal;
	}
    //Apply Active Filters
    function apply_active_filters() { 
        var all_filters = [];
        var filter_ids = [];
        $(document).find("#selectAllDiv").hide();
        $(document).find("#downloadCSV").hide();
        $(".selectClassHide").hide();
		
        var filtersAreEnabled = $("#filtersAreEnabled").val();
        $("#usersBanner").html("Users List / Group-wide Users Modify");
        $("#usersBanner").append('<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Click User Id link to switch to Modify User page"><img class="infoIcon" src="images/NewIcon/info_icon.png"></a>');
        var loadCount =  $("#loadCount").val();
        if(loadCount == 0){
                $("#userFiltersForm").hide();
                $("#filterUsersLoading").show();
        }

      //code for the apply active button when nothing has been changed.... it will return false, if we do not get anything changed

//         var applyActiveClickedVal = $("#applyActiveClicked").val();
//         //if(applyActiveClickedVal == 0 && loadCount == 0){return false;}
//         //if($(".enableRow").length == 0){return false;}

        
//         var switchToExpressVal =  $("#switchToExpress").val();
//         if((switchToExpressVal == 0 && $(".enableRow").length == 0) && applyActiveClickedVal == 0){
// 			return false;
//         }

        
        var dataToSend = {};
        
        if (filtersAreEnabled == "1") {
            $(".user_filters.active").each(function () {
                var filter_id = $(this).data('id');
                var a_rules = $("#filterUsersValuesDiv_" + filter_id).jui_filter_rules("getRules", 0, []);

                if (a_rules.length === 0) {
                    //alert("No rules defined...");
                    $("#filter_set_" + filter_id).addClass("error");
                    all_filters = null;
                    return false;
                }
                $("#filter_set_" + filter_id).removeClass("error");
                filter_ids.push(filter_id);

                var filter = {};
                filter['rules'] = a_rules;
                filter['id'] = $("#filter_set_id_" + filter_id).val();
                filter['name'] = $("#filter_set_name_" + filter_id).val();
                filter['filter_id'] = filter_id;

                all_filters.push(filter);
            });

            var preset_filters = $("#userFiltersForm").serialize();

            dataToSend = {
                all_filters: all_filters,
                preset_filters: preset_filters,
                fileType : "express",
                dataType : "JSON",
            }

        } else {

        	var preset_filters = $("#userFiltersForm").serialize();
       	 	dataToSend = {
                    preset_filters: preset_filters,
                    fileType : "express",
                    dataType : "JSON",
                }

        }

	    $("#filterUsersLoading").show();

        $("#filtersDiv").show();
        $("#usersTableDiv").hide();

        $(".users_selected").prop("disabled", true);
        $(".users_selected").prop("disabled", false);
        $(".users_selected").prop('checked', false);
        $(document).find(".selectClassHide").hide();

        $.ajax({
            type: 'POST',
            url: "expressSheets/userFilters/filter_users.php",
            data: dataToSend,
            //dataType: "JSON",
            success: function (data) {
				var checkFilterApplied = "0";
				if (data.slice(0, 1) == "1")
				{
					checkFilterApplied = 1;
				}
				else
				{
					checkFilterApplied = 0;
				}
				data = data.slice(1);
				$(".userFilterHtml").html(data);
				debugger;
                $("#filterUsersLoading").hide();
                $("#usersTableDiv").show();
                setTableSorter();
                checkUserLength();
                eventAfterLoad();
                
                $(".triggerColumn").trigger("click");
                $("#usersTableDiv1").show();
                $(".applyFilterBtn").show();
                
                $("#usersBanner").html("Users List / Group-wide Users Modify");
                $("#usersBanner").append('<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Click User Id link to switch to Modify User page"><img class="infoIcon" src="images/NewIcon/info_icon.png"></a>');
				$("#usersBanner").append("<span class='filtersApplied'>(Filters Applied)</span>");

				$("#filtersStatusTitle").html("Filters - Applied <span class='glyphicon glyphicon-chevron-down fIcon'></span>");

				//users count when come from user table
                if (usersFromUserModule.length > 0) {                            
                    
                    if(usersFromUserModule.length == 0)
                    {
                    	$(".selectClassHide").hide();
                    }
                    else{
                    	$(".selectClassHide").show();
                    }
                	  
                    
                    $("table.DTFC_Cloned tbody td input.users_selected").prop('checked', false);
                    $("table.customDataTable tbody td input.users_selected").prop('checked', false);
                    
                    
                    $.each(usersFromUserModule, function (i, userId) {
                        
                            var clsName    = "broadsoft_usersRow_"+userId;
                            var uTitle       = "usr_selected_"+userId;
                            
                            $('[name="'+clsName+'"]').show();                                   
                            $('table.DTFC_Cloned tbody td [title="'+uTitle+'"]').prop("checked", true);
                            $('table#allUsersTableEx tbody td [title="'+uTitle+'"]').prop("checked", true);
                        });
                    usersFromUserModule = [];               //Code Added @08 May 2018 
                }

                //code ends
 
                if (typeof updateSelectedUsersCount !== 'undefined' && $.isFunction(updateSelectedUsersCount)) {
                    updateSelectedUsersCount();
                }
                //setTimeout to make the code run when user table has been load....
                setTimeout(function(){
                	updateSelectedUsersCount();
                	$(".triggerColumn").trigger("click");
                	
                	if($(".filterCheckStatus:checked").length == $(".penone").length){                	
                		$("#clearAllFiltersBtn").prop("checked", true);
                	}else{
                		$("#clearAllFiltersBtn").prop("checked", false);
                	}
                	
                	if($(".filterOptionRadio:checked").length == 0 ||  $("#clearAllFiltersBtn").prop("checked") == true){
           				$("#filtersStatusTitle").html("Filters - Not Applied <span class='glyphicon glyphicon-chevron-down'></span>");
           			}

                    var resetAllStatusVal =  $("#resetAllStatus").val();
                    if(resetAllStatusVal == "1"){
                                $(".users_selected").prop("checked", false);
                                $("#resetAllStatus").val('0');
                                $("#users_selected_count").html("0 Users selected.");
                                $("#selectAllButtonExpSheet").prop("checked", false); //Code added @ 08 May 2018
                    }
                    var switchToExpressVal = $("#switchToExpress").val();
                                        
                    if(switchToExpressVal == "0")
                    {
                         $(".users_selected").prop("checked", false);
                         $("#users_selected_count").html("0 Users selected."); 
                    }
                    $("#switchToExpress").val('0');
                    if($("#filterUsers").val() == "FilterUsers"){
                    $("#filtersTitle").show();
                	}else{
                		$("#filtersTitle").hide();
                	}
                }, 3000);
				//code ends
                
                $("#loadCount").val('1');
                var loadCount =  $("#loadCount").val();
                var resetAllStatusVal =  $("#resetAllStatus").val();
                if(resetAllStatusVal == "1"){
                	$("#usersBanner").html("Users List / Group-wide Users Modify");
                	$("#usersBanner").append('<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Click User Id link to switch to Modify User page"><img class="infoIcon" src="images/NewIcon/info_icon.png"></a>');
                	$("#filtersStatusTitle").html("Filters - Not Applied <span class='glyphicon glyphicon-chevron-down'></span>");
                }
                $("#userFiltersForm").show();
                $("#filterUsersLoading").hide();
                $("#applyActiveClicked").val('0');
                $("#selectAllChk").prop("checked", false);
                //$("#selectAllButtonExpSheet").prop("checked", false); // Code added @ 29 June 2018 to unselect Select All Button
                $(".checkUserListBox").prop("checked", false);

                if($(".users_selected").length > 0){
                	$(".selectClassHide").show();   
                }
				
                if($(".checkUserListBox:checked").length > 0){
                	$(document).find("#selectAllDiv").show();
                }
                if($(".user_selected_chk:checked").length > 0){
                	$(document).find(".selectClassHide").show();
                }
                $(document).find("#downloadCSV").show();
                checkUserLength();
                
                
            }
        });

    }
    //apply_active_filters();
    
    function deleteFilter(deleteBtnObj) {

        var filter_id = $(deleteBtnObj).data('id');
        if (confirm("Are you sure?")) {
            var save_id = $(deleteBtnObj).data('save-id');
            if (save_id > 0) {

                $.ajax({
                    type: 'POST',
                    url: "expressSheets/userFilters/filter_users.php",
                    data: {
                        delete_filter: save_id
                    },
                    dataType: "JSON",
                    success: function (data) {

                        if (data.success) {
                            $("#filterUsersValuesDiv_" + filter_id).removeClass('active');
                            $("#filter_set_" + filter_id).slideUp(500, function () {
                                $(this).remove();
                                toggle_apply_filter_btn();
                                apply_active_filters();
                            });
                        } else {
                            alert("Filter was not deleted.");
                        }

                    }
                });

            } else {
                $("#filter_set_" + filter_id).slideUp(500, function () {
                    $(this).remove();
                });
                toggle_apply_filter_btn();
                apply_active_filters();
            }
        }

    }

    function toggleFilter(toggleBtnObj, inactive) {

        var filter_id = $(toggleBtnObj).data('id');
        //if(confirm("Are you sure?")) {
        var save_id = $(toggleBtnObj).data('save-id');
        if (save_id > 0) {

            $.ajax({
                type: 'POST',
                url: "expressSheets/userFilters/filter_users.php",
                data: {
                    toggle_filter: save_id,
                    inactive: inactive
                },
                dataType: "JSON",
                success: function (data) {

                    if (data.success) {

                        $(toggleBtnObj).hide();

                        if (inactive) {
                            $("#filter_set_" + filter_id).removeClass('success');
                            $("#filter_set_" + filter_id).addClass('inactive');
                            $("#filterUsersValuesDiv_" + filter_id).removeClass('active');
                            $("#filterUsersValuesDiv_" + filter_id).addClass('inactive');
                            $("#filterUsersValuesDiv_" + filter_id).slideUp(500, function () {
                                $(this).hide();
                                $("#enableUserFilterBtn_" + filter_id).show();
                            });
                        } else {
                            $("#filter_set_" + filter_id).removeClass('inactive');
                            $("#filterUsersValuesDiv_" + filter_id).addClass('active');
                            $("#filterUsersValuesDiv_" + filter_id).removeClass('inactive');
                            $("#filterUsersValuesDiv_" + filter_id).slideDown(500, function () {
                                $(this).show();
                                $("#disableUserFilterBtn_" + filter_id).show();
                            });
                        }

                        toggle_apply_filter_btn();
                        apply_active_filters();

                    } else {
                        alert("Filter was not disbaled.");
                    }

                }
            });

        } else {

            $(toggleBtnObj).hide();

            if (inactive) {
                $("#filter_set_" + filter_id).removeClass('success');
                $("#filter_set_" + filter_id).addClass('inactive');
                $("#filterUsersValuesDiv_" + filter_id).removeClass('active');
                $("#filterUsersValuesDiv_" + filter_id).addClass('inactive');
                $("#filterUsersValuesDiv_" + filter_id).slideUp(500, function () {
                    $(this).hide();
                    $("#enableUserFilterBtn_" + filter_id).show();
                });
            } else {
                $("#filter_set_" + filter_id).removeClass('inactive');
                $("#filterUsersValuesDiv_" + filter_id).addClass('active');
                $("#filterUsersValuesDiv_" + filter_id).removeClass('inactive');
                $("#filterUsersValuesDiv_" + filter_id).slideDown(500, function () {
                    $(this).show();
                    $("#disableUserFilterBtn_" + filter_id).show();
                });
            }

            toggle_apply_filter_btn();
            apply_active_filters();

        }
        //}

    }

    // tooltip
    $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
     });
</script>
<!-- Code for redesign userFilter .filterCheckStatus-->   
 <script>
 
 $(document).ready(function(){

	 $("#defaultUserFiltersDiv").hide();
	 $("#filtersDiv").hide();
	 $("#resetAllFilters").show();
	 $("#showActiveFilterBtn").css("float", "right");
	 $("#loadCount").val(0);
	 $("#clearAllFiltersBtn").parent("div").hide();
	 
	 $(document).on("click", "#filtersStatusTitle", function(){
		if($("#filtersDiv").is(":visible")){
			$("#filtersStatusTitle span").removeClass("glyphicon-chevron-right");
			$("#filtersStatusTitle span").addClass("glyphicon-chevron-down");
			$("#userFiltersForm").addClass('divCFAforStyle'); //ex-792
		}else{
			$("#filtersStatusTitle span").removeClass("glyphicon-chevron-down");
			$("#filtersStatusTitle span").addClass("glyphicon-chevron-right");
			$("#userFiltersForm").removeClass('divCFAforStyle');
		}
		setTimeout(function(){
			$(".triggerColumn").trigger("click");
			$(".users_selected").prop("checked", false);
			$("#users_selected_count").html("0 Users selected.");
    	}, 3000);
	 });

	 var changeTitleStatusNotApplied = function(){
		 
		 $("#usersBanner").html("Users List / Group-wide Users Modify");
		 
		 $("#usersBanner").append('<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Click User Id link to switch to Modify User page"><img class="infoIcon" src="images/NewIcon/info_icon.png"></a>');
		 //$("#usersBanner").append("<span class='filtersApplied'>(Filters Not Applied)</span>");
		 $("#filtersStatusTitle").html("Filters - Not Applied <span class='glyphicon glyphicon-chevron-down'></span>");
	 }

	 var changeTitleStatusApplied = function(){
		 
		 $("#usersBanner").html("Users List / Group-wide Users Modify");
		 $("#usersBanner").append('<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Click User Id link to switch to Modify User page"><img class="infoIcon" src="images/NewIcon/info_icon.png"></a>');
		// $("#usersBanner").append("<span class='filtersApplied'>(Filters Applied)</span>");
		 $("#filtersStatusTitle").html("Filters - Applied <span class='glyphicon glyphicon-chevron-down'></span>");
	 }
	 
	//Active/ Inactive and expand data from user Filter particular record
    var filterExpand = function(e){
    	e.closest("tr").toggleClass("active");
    	e.parent("td").find("div.FilterOptions").toggle();
    };

    // function for active and expand record
    $(document).on("click", '.expandFilterClass', function(){
    	var el = $(this);
    	el.css("cursor", "pointer");
    	el.closest("tr").addClass("active");
    	if(el.siblings('.FilterOptions').is(":visible")){
    		el.siblings('.FilterOptions').hide();
    	}else{
    		el.siblings('.FilterOptions').show();
    	} 
    });

    var showActiveFilters = function(){ 
		$(".disableRow").hide();
		$(".active.enableRow").show();		
		$("#showActiveFilterBtn").hide();
		$("#showAllFilterBtn").show();
		$(".enableRow").addClass("active");
		$(".filterh1title").text("Active Filters");		
		$("#checkAllActiveStatus").val("active");
		//$(".enableRow").find("td").find(".FilterOptions").show();
		if($(".active.enableRow").length > 0){
			$("#defaultUserFiltersDiv").show();
			
		}else{
			$("#defaultUserFiltersDiv").hide();
		}
		$(".applyFilterBtn").show();

		if($(".enableRow").length > 0){
			$("#clearAllFiltersBtn").parent("div").parent("div").parent("div").parent("div").show();
  	 	}else{
   			$("#clearAllFiltersBtn").parent("div").parent("div").parent("div").parent("div").hide();
   		} 
		
    };
    
    var showAllFilters = function(){ 
    	$(".enableRow.active").show();
		$(".disableRow").show();
		$("#showActiveFilterBtn").show();
		//$(".FilterOptions").hide();
		$("#showAllFilterBtn").hide();
		$(".filterh1title").text("All Filters");
		$("#checkAllActiveStatus").val("all");
		//$(".disableRow").find("td").find(".FilterOptions").hide();
		//$(".enableRow").find("td").find(".FilterOptions").hide();
		//$(".active").find("td").find(".FilterOptions").hide();
		$("#defaultUserFiltersDiv").show();
		$("#clearAllFiltersBtn").prop("checked", false);
// 		if($(".filterOptionRadio:checked").length > 0){
// 			$("#clearAllFiltersBtn").parent("label").show();
// 			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
//   	 	}else{
//    		$("#clearAllFiltersBtn").parent("label").hide();
// 			$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
//    		} 
		$("#clearAllFiltersBtn").parent("div").parent("div").parent("div").parent("div").show();
		$("#clearAllFiltersBtn").parent("div").show();
    };

    // function for radio button enable then particular checkbox is enable 
     var chkboxEanable = function(e){
         	e.closest("tr").addClass("enableRow");
         	e.closest("tr").removeClass("disableRow");
         	e.closest("tr").addClass("active");
         	e.closest("tr").find("td").find(".filterCheckStatus").prop("checked", true);
         	/*if(e.closest("tr").find(".filterCheckStatus").is(":checked")){	
         		e.closest("tr").find("td").find(".filterCheckStatus").css("pointer-events", "none");
         		e.closest("tr").find("td").find(".filterCheckStatus").addClass("penone");
         		e.closest("tr").find("td").find(".FilterOptions").find(".resetF").css("pointer-events", "auto");
         	}else{
         		e.closest("tr").find("td").find(".filterCheckStatus").css("pointer-events", "auto");
         		e.closest("tr").find("td").find(".filterCheckStatus").prop("disabled", false);
         		e.closest("tr").find("td").find(".filterCheckStatus").addClass("peauto");
         		e.closest("tr").find("td").find(".FilterOptions").find(".resetF").css("pointer-events", "auto");
         	}*/
         	e.closest("tr").find("td").find(".filterCheckStatus").css("pointer-events", "auto");
         	e.closest("tr").find("td").find(".filterCheckStatus").addClass("peauto");
         	e.closest("tr").find("td").find(".filterCheckStatus").removeClass("penone");
         	
         	var activeFilters = e.closest("tr").find(".filterH").attr("data-filter");
 			e.closest("tr").find(".filterH").val(activeFilters);
 			e.closest("tr").find(".filterCheckStatus").parent("label").prev("input[type=hidden]").val("");
 			if($(".filterOptionRadio:checked").length > 0){
				$(".applyFilterBtn").prop("disabled", false);
			}else{
				$(".applyFilterBtn").prop("disabled", true);
			}
 			changeTitleStatusNotApplied();
 			$("#applyActiveClicked").val('1');
 			if($(".filterOptionRadio:checked").length > 0){
 				$("#clearAllFiltersBtn").parent("div").show();
 	    	}else{
 	    		$("#clearAllFiltersBtn").parent("div").hide();
 	    	} 

 			if($(".filterCheckStatus").length == $(".peauto").length && $(".disableRow.active").length == 0){
				$("#clearAllFiltersBtn").prop("checked", true);
    		}else{
    			$("#clearAllFiltersBtn").prop("checked", false);
    		}
 	    	
     };

     // function for radio button enable then particular checkbox is enable 
      var chkboxDisable = function(e){
    	  //e.css("pointer-events", "none");
    	  e.removeClass("peauto");
    	  e.addClass("penone");
    	  e.closest("tr").addClass("disableCheckbox");
    	  e.closest("tr").addClass("disableRow");
    	  e.closest("tr").removeClass("active");
    	  e.closest("tr").removeClass("enableRow");
    	  e.closest("tr").find("td").find(".filterH").val('');
    	  e.closest("tr").find("td").find(".filterOptionRadio").css("pointer-events", "none");
    	  e.closest("tr").find("td").find(".radio-inline").find("label").css("pointer-events", "none");
    	  e.closest("tr").find("td").find(".FilterOptions").hide();
    	 // e.closest("tr").find("td").find(".FilterOptions").find("select").prop("disabled", true);
    	  e.closest("tr").find("td").find(".FilterOptions").find("select").css("pointer-events", "none");
    	  //e.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").prop("disabled", true);
    	  e.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").css("pointer-events", "none");
    	  e.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").val('');
    	  
    	  var dataVal = e.attr("data-filterd");
		  e.parent("label").prev("input[type=hidden]").val(dataVal);
    	  changeTitleStatusNotApplied();
    	  var checkAllActiveStatusVal = $("#checkAllActiveStatus").val();
        	if(checkAllActiveStatusVal == "all"){
        		showAllFilters();
        	}else{
        		showActiveFilters();
        	}
        	$("#applyActiveClicked").val('1');
//         	if($(".filterOptionRadio:checked").length > 0){
            	
//     			$("#clearAllFiltersBtn").parent("label").show();
//      			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
//      			if($(".filterCheckStatus:checked").length == 0){
//             		showAllFilters();
//             	}
//         	}else{
//         		$("#clearAllFiltersBtn").parent("label").hide();
//      			$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
//         	}      
      };

      // function for radio button enable then particular checkbox is enable 
      var chkboxClickEnable = function(e){
    	  //e.css("pointer-events", "none");
    	  e.addClass("penone");
    	  //e.prop("checked", false);
    	  e.closest("tr").removeClass("disableCheckbox");
    	  //e.closest("tr").removeClass("disableRow");
    	  e.closest("tr").addClass("active");
    	  //e.closest("tr").addClass("enableRow");
    	  e.closest("tr").find("td").find(".filterOptionRadio").css("pointer-events", "auto");
    	  e.closest("tr").find("td").find(".radio-inline").find("label").css("pointer-events", "auto");
    	  e.closest("tr").find("td").find(".FilterOptions").find("select").css("pointer-events", "auto");
    	  e.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").css("pointer-events", "auto");
    	  e.closest("tr").find("td").find(".FilterOptions").show();
    	  if(e.closest("tr").find("td").find(".filterOptionRadio").is(":checked")){
    	      var filterHVal = e.parent("label").parent("td").find(".filterH").attr("data-filter");
    	      e.parent("label").parent("td").find(".filterH").val(filterHVal);
    	      e.closest("tr").addClass("enableRow active");
          }else{
        	  e.parent("label").parent("td").find(".filterH").val('');
        	  e.closest("tr").addClass("disableRow");
          }
    	  
 	  	  e.parent("label").prev("input[type=hidden]").val("");
 	  	  e.addClass("peauto");
 		  e.removeClass("penone");
 	  
    	  changeTitleStatusNotApplied();
    	  var checkAllActiveStatusVal = $("#checkAllActiveStatus").val();
        	if(checkAllActiveStatusVal == "all"){
        		showAllFilters();
        	}else{
        		showActiveFilters();
        	}
        	//$(".filterCheckStatus:checked").parent("label").parent("td").siblings("td").find(".expandFilterClass").trigger("click");
        $("#applyActiveClicked").val('1');
        
//    		if($(".filterOptionRadio:checked").length > 0){
// 			$("#clearAllFiltersBtn").parent("label").show();
// 			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
// 	   	}else{
// 	   		$("#clearAllFiltersBtn").parent("label").hide();
// 				$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
// 	   	}    
      };
       

    //click event to make the filter disbable when clicked on checkbox
    $(document).on("click", ".filterCheckStatus", function(){
    	var el = $(this);
    	if(el.is(":checked")){
    		chkboxClickEnable(el);
    	}else{
			chkboxDisable(el);
    	}

    	if($(".filterCheckStatus").length == $(".peauto").length && $(".disableRow.active").length == 0){
			$("#clearAllFiltersBtn").prop("checked", true);
		}else{
			$("#clearAllFiltersBtn").prop("checked", false);
		}

    });

    //click event for checkbox enable or disable
     $(document).on("click", '.filterOptionRadio', function(){
     	var el = $(this);
     	 chkboxEanable(el);
     });
     
 	// event for check enable or disable all radio button in table or form
	var filterStatus = function(){
    	$(".filterCheckStatus").each(function(){ 
    		var el = $(this);
    		//get radio button check 
    		var chkRadio = el.parent("label").parent("td").siblings('td').find("input[type=radio]");
     		var elChked = chkRadio.is(":checked");
			//check enable radio-button particular checkbox
    		if(elChked){ 
        		if(el.is(":checked")){		
     				//el.css("pointer-events", "none");
     				chkRadio.css("pointer-events", "none");
     				//el.closest("tr").removeClass("enableRow");
					//el.closest("tr").removeClass("active");
					//el.closest("tr").addClass("disableRow");
	     			el.closest("tr").find(".radio-inline").find("label").css("pointer-events", "none");
	     			//el.closest("tr").find("td").find(".FilterOptions").find("select").prop("disabled", true);
	     			el.closest("tr").find("td").find(".FilterOptions").find("select").css("pointer-events", "none");
	     	    	//el.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").prop("disabled", true);
	     	    	el.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").css("pointer-events", "none");
	     	    	el.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").val('');
	     	    	el.closest("tr").find("td").find(".FilterOptions").find(".resetF").css("pointer-events", "none");
	     	    	var dataVal = el.attr("data-filterd");
	     			el.parent("label").prev("input[type=hidden]").val(dataVal);
        		}else{ 
        			el.css("pointer-events", "auto");

        			el.closest("tr").addClass("enableRow");
	     			el.closest("tr").addClass("active");
	     			el.parent("label").prev("input[type=hidden]").val('');
        		}
        		el.addClass("peauto");
        		el.closest("tr").addClass("enableRow");
        		el.closest("tr").removeClass("disableRow");
     			var activeFilters = el.closest("tr").find(".filterH").attr("data-filter");
     			el.closest("tr").find("i.filterH").val(activeFilters);
     			el.prop("checked", true);
     		}else{ 		
     			el.addClass("penone");
     	 		el.closest("tr").addClass("disableRow");
     	 		el.closest("tr").find(".filterH").val('');
     	 		el.parent("label").prev("input[type=hidden]").val('');
     	 		el.prop("checked", false);
     		}
    		el.closest("tr").find("td").find(".FilterOptions").hide();

    		$("#filterUsersValuesRow").show();
    		$(".triggerColumn").trigger("click");
    	});
    	$(".applyFilterBtn").show();


    	if($(".filterCheckStatus:checked").length == $(".penone").length){
    		$("#clearAllFiltersBtn").prop("checked", true);
    		//$(".FilterOptions").find("select").prop("disabled", true);
            //$(".FilterOptions").find(".filterOptionText").prop("disabled", true);
            //$(".FilterOptions").hide();
            //$(".resetF").prop("disabled", true);
			//$(".filterOptionRadio").prop("disabled", true);
			//$(".radio-inline").find("label").prop("disabled", true);
    	}else{
    		$("#clearAllFiltersBtn").prop("checked", false);
    	}

    	$("#loadCount").val('1');
    	//$("#switchToExpress").val(0);
    	if($("#filterUsers").val() == "FilterUsers"){ 
			$("#filtersTitle").show();
		}else{
			$("#filtersTitle").hide();
		}
    	if($(".filterOptionRadio:checked").length > 0){
			$("#applyActiveClicked").val('1');
    	}
    	if($(".filterOptionRadio:checked").length > 0){
			$("#clearAllFiltersBtn").parent("div").show();
    	}else{
    		$("#clearAllFiltersBtn").parent("div").hide();
    	}  
	}; 
	filterStatus();

	var resetFilter = function(el){
		el.parent("div").parent("div").parent("td").siblings("td").find(".filterCheckStatus").css("pointer-events", "none");
		el.parent("div").parent("div").parent("td").siblings("td").find(".filterCheckStatus").addClass("penone");
		el.parent("div").parent("div").parent("td").siblings("td").find(".filterCheckStatus").removeClass("peauto");
		//el.parent("div").parent("div").parent("td").siblings("td").find(".filterCheckStatus").prop("disabled", true);
		el.parent("div").parent("div").parent("td").siblings("td").find(".filterCheckStatus").prop("checked", false);
		el.parent("div").parent("div").parent("td").siblings("td").find(".filterCheckStatus").parent("label").prev("input[type=hidden]").val('');
		el.parent("div").prev().find(".radio-inline").find(".filterOptionRadio").prop("checked", false);
		el.parent("div").prev().find(".radio-inline").find(".filterOptionRadio").prop("disabled", false);
		el.parent("div").prev().find(".radio-inline").find(".filterOptionRadio").css("pointer-events", "auto");
		el.closest(".FilterOptions").find("select").prop("disabled", false);
		el.closest(".FilterOptions").find("select").val(""); 
		el.closest(".FilterOptions").hide();
		el.closest(".FilterOptions").find(".filterOptionText").val("");
		el.closest(".FilterOptions").find(".filterOptionText").prop("disabled", false);
		el.parent("div").parent("div").hide();
		el.closest("tr").addClass("disableRow");
		el.closest("tr").removeClass("enableRow");
		el.closest("tr").removeClass("disableCheckbox");
		el.closest("tr").removeClass("active");
		el.closest("tr").find(".filterH").val('');
		var checkAllActiveStatusVal = $("#checkAllActiveStatus").val();
		
      	if(checkAllActiveStatusVal == "all"){
      		showAllFilters();
      	}else{
      		showActiveFilters();
      	}
      	changeTitleStatusNotApplied();
      	if($('#clearAllFiltersBtn').prop('checked')){
      		$(".applyFilterBtn").prop("disabled", true);
            $(".filterOptionRadio").prop("disabled", true);
            //apply_active_filters();
            $(".filterCheckStatus").prop("disabled", true);
            //$(".filterCheckStatus").addClass("penone");
            $(".FilterOptions").find("select").prop("disabled", true);
      	}else{
      		$(".applyFilterBtn").prop("disabled", false);
            $(".filterOptionRadio").prop("disabled", false);
            //apply_active_filters();
            $(".filterCheckStatus").prop("disabled", false);
            $(".FilterOptions").find("select").prop("disabled", false);
      	}


      	if($(".filterCheckStatus").length == $(".peauto").length){
			$("#clearAllFiltersBtn").prop("checked", true); 
		}else{ 
			$("#clearAllFiltersBtn").prop("checked", false);
		}
//       	if($(".filterOptionRadio:checked").length > 0){
// 			$("#clearAllFiltersBtn").parent("label").show();
//  			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
//     	}else{
//     		$("#clearAllFiltersBtn").parent("label").hide();
//  			$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
//     	}    
		
      	$("#applyActiveClicked").val('1');
	}

	$(document).on("click", ".resetF", function(){
		var el = $(this);
		resetFilter(el);
	});
	
	showActiveFilters();
	
    $(document).on("click", "#showActiveFilterBtn", function(){
    	showActiveFilters();
    });

    $(document).on("click", "#showAllFilterBtn", function(){
    	showAllFilters();
    });

    var submitApply = function(){
		if($(".enableRow").length > 0){
			$(".applyFilterBtn").trigger("click");
		}
    };
    //submitApply();
    $(".triggerColumn").trigger("click");

    $("#filterUsers").change(function(){
        var el = $(this);
        var elVal = el.val();
        if(elVal == "FilterUsers"){
			$("#filtersTitle").show();
    	}else{
    		$("#filtersTitle").hide();
    	}
    });

    $("input[type=number]").bind('keyup keypress input change', function(){
        // handle event
    	var el = $(this);
    	if(el.val() < 0){el.val('0');}
    });
});

 
</script>

<!-- end code for userFilte redesign -->   

<script type="text/html" id="filterSetTemplate">
	<input type="hidden"
	       class="form-control"
	       data-template-bind='[{"attribute": "data-id", "value": "data_id"}]'
	       data-id="filter_set_id_id"
	       data-value="filter_set_id_value"/>
 
	<div class="">
		<div class="col-md-12">
			<div class="form-group" id="inputTextAFilter">
				<input type="text"
			       class="form-control"
			       data-template-bind='[{"attribute": "data-id", "value": "data_id"}]'
			       data-id="filter_set_name_id"
			       data-value="filter_set_name_value"/>
		</div>
		<div class="form-group">
			<button type="button"
			        data-template-bind='[{"attribute": "data-id", "value": "data_id"}, {"attribute": "data-save-id", "value": "filter_set_id_value"}, {"attribute": "style", "value": "hide_disable"}]'
			        data-id="filter_disable_id"
			        class="disableUserFilterBtn">Disable
			</button>
			<button type="button"
			        data-template-bind='[{"attribute": "data-id", "value": "data_id"}, {"attribute": "data-save-id", "value": "filter_set_id_value"}, {"attribute": "style", "value": "hide_enable"}]'
			        data-id="filter_enable_id"
			        class="btn-averistar enableUserFilterBtn">Enable
			</button>
			<button type="button"
			        data-template-bind='[{"attribute": "data-id", "value": "data_id"}, {"attribute": "data-save-id", "value": "filter_set_id_value"}]'
			        data-id="filter_delete_id"
			        class="deleteBtn deleteUserFilterBtn">Delete
			</button>
		</div>
	</div>
	<div data-id="filterUsersValuesDiv_id"
	     data-template-bind='[{"attribute": "data-id", "value": "data_id"}]'
	     data-class="css_class"
	     style="width: -moz-fit-content;width: fit-content;" class="col-md-12"></div>
</script>
<style type="text/css">

		.selectClassHide{float:left;margin-top: 5px;}
		#filtersStatusTitle {margin-right: 37px;}		

        .disableFilter
        {
            background: none repeat scroll 0 0 burlyWood;
            cursor: default !important;
        }
        
        .enableFilter{
            color: #2b2;
        }
        .disableRow td, .enableRow td{
        	background-color:#fff;
        	border:none;
        }
        .filterh1title {
	       /* font-size: 2em;
		   	xwidth: 100%;
		    color: #242424;
		    background: #FFF; */
        }
        #defaultUserFiltersDiv table tr.active, #defaultUserFiltersDiv table {
        	 background-color: #FFF;
        	 border:none;
        }
        .expandFilterClass{
        	cursor:pointer;
        	font-weight: bold;
            color: #6ea0dc;
        }
        .fIcon{
        	color:#fff !important;
        }
  </style>
<style>
	.user_filter_set {
		border: none;
		padding: 10px;
		margin: 20px 10px
	}

	.user_filter_set.error {
		background: #ce0d0d;
	}

	.user_filter_set.success {
		background: #9cc99c;
	}

	.user_filter_set.inactive {
		background: #fff4b0;
	}

	.user_filters.inactive {
		display: none;
	}

	.user_filters.active {
		display: block;
	}

	.resetF{
		
	}
	
	#resetAllFilters{
		
	}
	#showActiveFilterBtn{
		
	}

</style>
<!-- User Filters End -->

<link rel="stylesheet" type="text/css" href="/Express/js/jquery.dataTables.min.css" media="screen">
<link rel="stylesheet" type="text/css" href="/Express/js/fixedColumns.dataTables.min.css" media="screen">

<!--
<script type="text/javascript" src="/Express/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/Express/js/dataTables.fixedColumns.min.js"></script>
-->

<style type="text/css">
/* Ensure that the demo table scrolls */
    th, td { /* white-space: nowrap; */ }
    div.dataTables_wrapper {
        width: 90%;
        margin: 0 auto;
		float:none;
    }
/*   common css */
    table.dataTable tbody th, table.dataTable tbody td {padding: 14px 10px !important;text-align: left;}
    table.dataTable thead th, table.dataTable thead td{text-align:left !important;}
 
    table.dataTable.no-footer thead tr .sorting_asc{/* background-image: url(/Express/images/blue/desc.gif) !important; background-position: 98% 48%; */}
    table.dataTable.no-footer thead tr .sorting_desc {
 
    background-image: url(/Express/images/blue/asc.gif);
    background-position: 98% 48%;
}
    table.dataTable thead .sorting {
    background-image: url(/Express/images/blue/bg.gif);
    background-position: 98% 48%;
}
    #allUsersTableEx_wrapper table thead tr th {border-top:2px solid #d8dada;border-bottom: 2px solid #d8dada;border-right: 2px solid #d8dada;}
    #allUsersTableEx_wrapper table thead tr th:first-child {border-left: 2px solid #d8dada; background-image: none !important;}
    .dataTables_wrapper.no-footer .dataTables_scrollBody{border-bottom: none;}
   .DTFC_LeftBodyWrapper,.dataTables_scrollBody{/* margin-top: -20px; */}
   .col1{min-width:   <?php echo $col1."px"; ?> !important;} 
   .col2{min-width:   <?php echo $col2."px"; ?> !important;} 
   .col3{min-width:   <?php echo $col3."px"; ?> !important;} 
   .col4{min-width:   <?php echo $col4."px"; ?> !important;} 
   .col5{min-width:   <?php echo $col5."px"; ?> !important;} 
   .col6{min-width:   <?php echo $col6."px"; ?> !important;} 
   .col7{min-width:   <?php echo $col7."px"; ?> !important;} 
   .col8{min-width:   <?php echo $col8."px"; ?> !important;} 
   .col9{min-width:   <?php echo $col9."px"; ?> !important;} 
   .col10{min-width:  <?php echo $col10."px"; ?> !important;} 
   .col11{min-width:  <?php echo $col11."px"; ?> !important;} 
   .col12{min-width:  <?php echo $col12."px"; ?> !important;} 
   .col13{min-width:  <?php echo $col13."px"; ?> !important;} 
   .col14{min-width:  <?php echo $col14."px"; ?> !important;} 
   .col15{min-width:  <?php echo $col15."px"; ?> !important;} 
   .col16{min-width:  <?php echo $col16."px"; ?> !important;} 
   .col17{min-width:  <?php echo $col17."px"; ?> !important;} 
   .col18{min-width:  <?php echo $col18."px"; ?> !important;} 
   .col19{min-width:  <?php echo $col19."px"; ?> !important;} 
   .col20{min-width:  <?php echo $col20."px"; ?> !important;} 
   
    table#allUsersTableEx{/* table-layout:fixed; */margin: 0;}

   .DTFC_LeftHeadWrapper table thead tr th:first-child,  .DTFC_LeftHeadWrapper table thead tr .sorting_asc{background-image: none !important;cursor: default;}
   .DTFC_LeftBodyWrapper table tbody tr td:last-child {/*border-right: 2px solid #d8dada !important*/}
   .DTFC_LeftBodyWrapper table tbody tr{border-right: 2px solid #d8dada;}
    #allUsersTableEx tbody tr td {/* border-right: 2px solid #d8dada;*/}
    .DTFC_LeftBodyWrapper table tbody tr td{border-right: 2px solid #d8dada;}
  
        .registerUsr td:first-child {
            border-left: 1px solid #cccccc !important;
        }

  /*Registration Table*/
  .usersTableColor td{background-color:#fff !important;} 
  table.customDataTable{font-size:11px;}
 table.customDataTable thead {background-color: #e6eeee;}
 .allUsersRegistration thead tr th {
    border-top: 2px solid #d8dada;
    border-bottom: 2px solid #d8dada;
    border-right: 2px solid #d8dada;
}
.allUsersRegistration thead tr th:first-child {
    border-left: 2px solid #d8dada;
}
table#allUsersRegistration tbody tr td{background-color:#A8BEE333;border-right: 2px solid #d8dada;border-top: 2px solid #d8dada !important;border-bottom: 2px solid #d8dada !important;}
table#allUsersRegistration tbody tr td:first-child{border-left: 2px solid #d8dada;}
.dataTables_scrollHead, .DTFC_LeftHeadWrapper{z-index: 1;}

tr.unRegisterUsr td {
    border-right: none !important;
}
.DTFC_LeftBodyLiner{overflow: hidden !important;width:auto !important; }
</style>

<script type="text/javascript">
function setTableSorter() {

    // $('#allUsers').DataTable().destroy();
    var table3 = $("#allUsersTableEx").dataTable({        
         scrollY:        500,
         scrollX:        true,
         scrollCollapse: true,
         paging:         false,
         //aaData:         response.data,
         destroy: true,
         retrieve: true, 
         info:     false,               
         searching: false,                
     }); 
     //table3.destroy();
    	new $.fn.dataTable.FixedColumns( table3 , {
    	 leftColumns: 2
 	});

}
//     $(function()
// 	{ 
//            // $('#allUsers').DataTable().destroy();
//            var table3 = $("#allUsersTableEx").dataTable({        
//                 scrollY:        500,
//                 scrollX:        true,
//                 scrollCollapse: true,
//                 paging:         false,
//                 //aaData:         response.data,
//                 destroy: true,
//                 retrieve: true, 
//                 info:     false,               
//                 searching: false,                
//             }); 
//             //table3.destroy();
//            new $.fn.dataTable.FixedColumns( table3 , {
//             leftColumns: 2
//         });
        
//    });
</script>
<!-- Code for redesign userFilter .filterCheckStatus-->   
 <script>
 $(document).ready(function(){
	//Active/ Inactive and expand data from user Filter particular record
    var filterRedesign = function(e){
    	e.closest("tr").toggleClass("active");
    	e.parent("td").find("div.FilterOptions").toggle();
    };

    // function for active and expand record
    $('.expandFilterClass').click(function(){
    	var el =$(this);
    		//filterRedesign(el);
    
    });

   // function for radio button enable then particular checkbox is enable 
    /*var chkboxEanable = function(e){
    	e.closest("tr").find(".filterCheckStatus").addClass('enableFilter');
    };

    //click event for checkbox enable or disable
     $('input[type=radio]').click(function(){
     	var el =$(this);
     	 chkboxEanable(el);
    	// console.log(chkboxEanable);
     });
 // event for check enable or disable all radio button in table or form
	var filterStatus = function(){
    	$(".filterCheckStatus").each(function(){ 
    		var el = $(this);
    		//get radio button check 
    		var chkRadio = el.parent("td").siblings('td').find("input[type=radio]");
     		var elChked = chkRadio.is(":checked");
			//check enable radio-button particular checkbox
    		if(elChked){ 			
     			el.parent("td").find(".filterCheckStatus").addClass('enableFilter');
     		}else{ 		
     	 			el.parent("td").find(".filterCheckStatus").addClass('disableFilter');
     		}
    	});
	}; 
	filterStatus();*/
});


 //button for Active filter and Show all Filter
	$(".showActiveFilterBtn").click(function(){
		$(".showActiveFilterBtn").hide();
		$(".showAllActivefilterBtn").show();
	});

	 $(".showAllActivefilterBtn").click(function(){
		$(".showAllActivefilterBtn").hide();
		$(".showActiveFilterBtn").show();
	 });
 
</script>

<script>
$(function() {
	$(document).on("click", ".userIdVal", function() {
		
		var userIdValue1 = "";
		$("#filterUsersLoading").show();
		var userId = $(this).attr("id");
		var fname = $(this).attr("data-firstname");
		var lname = $(this).attr("data-lastname");

		var extension = $(this).attr("data-extension");
		var phone = $(this).attr("data-phone");
		if($.trim(phone) != ""){
			userIdValue1 = phone+"x"+extension;
		}else{
			userIdValue1 = userId;
		}
		var userIdValue = userIdValue1+" - "+lname+", "+fname;
		
		$.ajax({
				type: "POST",
				url: "userMod/userMod.php",
				data: { searchVal: userId, userFname : fname, userLname : lname },
				success: function(result)
				{
					$("#mainBody").html(result);
					$("#searchVal").val(userIdValue);
					
					$("#go").trigger("click");
					activeImageSwapModify();
				}

			});
	});
	

	var activeImageSwapModify = function(){	
		previousActiveMenu	= "expressSheets";
	 	currentClickedDiv = "userMod";       
	  
			//active
	  		if(previousActiveMenu != "")
			{ 
	  			$("#expressSheets").removeClass("activeNav");
	  		 	$(".navMenu").removeClass("activeNav");
				var $thisPrev = $("#expressSheets").find('.ImgHoverIcon');
		        var newSource = $thisPrev.data('alt-src');
		        $thisPrev.data('alt-src', $thisPrev.attr('src'));
		        $thisPrev.attr('src', newSource);
		       
			}
			// inactive tab
			if(currentClickedDiv != ""){
				$("#userMod").addClass("activeNav");
				var $thisPrev = $("#userMod").find('.ImgHoverIcon');
		        var newSource = $thisPrev.data('alt-src');
		        $thisPrev.data('alt-src', $thisPrev.attr('src'));
		        $thisPrev.attr('src', newSource);
				 previousActiveMenu = currentClickedDiv;
				
	    	}
	   
	}
});
</script>
<!-- end code for userFilte redesign -->  

<style type="text/css">
        .disableRow td, .enableRow td{
        	background-color:#fff;
        	border:none;
        }
        /*.filterh1title {
	        font-size: 2em;
		   	xwidth: 100%;		    
        }*/
        #defaultUserFiltersDiv table tr.active, #defaultUserFiltersDiv table {
        	 background-color: #FFF;
        	 border:none;
        }

        /*.expandFilterClass{
        	cursor:pointer;
        }*/
        .fIcon{
        	color: #6ea0dc !important;
            margin-right: 10px;
        }
         #defaultUserFiltersDiv table tr td:first-child{
        	 width:30px !important;
        }

        /*.filterTitleBtn{
        	float:right; 
        	margin-right: 10px; 
			color: #fff !important;
			font-weight: bold;
			background-color: #5d9aac !important;
			padding: 8px;
        }
         td, td > .userIdVal {  white-space: normal; word-break: break-all; }
         .applyFilterBtn{
         	margin-top:21px;
         }*/
  </style>

<div id="filterUsersLoading" class="loading"><img src="/Express/images/ajax-loader.gif" style="margin:15px 0"></div>
<script type="text/javascript">

// This will execute whenever the window is resized
	$(window).resize(function() {  
        $(".triggerColumn").trigger("click"); 
		$("table.dataTable tbody td").css("padding", "14px 10px");		
    }); 
	function eventAfterLoad(){
		//alert('eventAfterLoad Called');
		$("table.dataTable tbody td").css("padding", "14px 10px");
	};

</script>
