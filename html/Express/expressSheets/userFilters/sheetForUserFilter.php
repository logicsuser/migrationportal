<?php
/**
 * Created by Dipin Krishna.
 * Date: 12/15/17
 * Time: 11:00 AM
 */
?>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
checkLogin();

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/config.php");
//require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/DBLookup.php");
include_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/expressSheetFunctions.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
$serviceInfo = new Services();

$sp = $_SESSION['sp'];
$groupId = $_SESSION['groupId'];

$department = "";
if(isset($_POST["department"])){
	$department = $_POST["department"];
}
if(!isset($groupInfoData)) {
	unset($groupInfoData);
	require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/expressSheets/group_info.php");
}

//Get Device Types
$nonObsoleteDevices = array();
require_once($_SERVER['DOCUMENT_ROOT'] . '/../lib/broadsoft/adminPortal/getDevices.php');

// Build devices lists:  customDevices, deviceTypesDigitalVoIP, analogGatewayDevices
/*$deviceTypesAnalogGateways = array();
$sipGatewayLookup = new DBLookup ($db, "devices", "deviceName", "sipGateway");

// Get Custom Devices List from DB.
// If the list exists, only devices from the list will be available in drop-down for assignment.
$customDevices = array();
if ($useCustomDeviceList == "true") {
	$query = "select deviceName from customDeviceList";
	$result = $db->query($query);

	while ($row = $result->fetch()) {
		$customDevices [] = $row ["deviceName"];
	}
}

foreach ($nonObsoleteDevices as $key => $value) {
	// shortcut custom device list for Audio Codes until they are resolved
	$deviceIsAudioCodes = substr($value, 0, strlen("AudioCodes-")) == "AudioCodes-";

	if ($useCustomDeviceList == "true" && !in_array($value, $customDevices)) {
		if (!$deviceIsAudioCodes) {
			continue;
		}
	}
	if ($sipGatewayLookup->get($value) == "true" || $deviceIsAudioCodes) {
		$deviceTypesAnalogGateways [] = $value;
	} else {
		$deviceTypesDigitalVoIP [] = $value;
	}
}*/
require_once ("/var/www/html/Express/util/getAuthorizeDeviceTypes.php");

//Get Custom Tags
require_once ("/var/www/lib/broadsoft/adminPortal/customTagManagement.php");

$availableTagsArray = getAllCustomTags();

$preset_filters = getUserPresetFilters($_SESSION["adminId"]);
//echo "<pre>"; print_r($preset_filters);die;
$preset_filters_checked = isset($preset_filters->Filters) ? $preset_filters->Filters : array();
$preset_filtersd_checked = isset($preset_filters->FiltersD) ? $preset_filters->FiltersD : array();
$preset_filters_options = isset($preset_filters->Filter) ? $preset_filters->Filter : array();
// $fValue = "";
// if(isset($preset_filters_checked)){
// 	foreach($preset_filters["Filters"] as $fKey=>$fVal){
// 		if(!empty($fVal)){
// 			$fValue = "yes";
// 			break;
// 		}
// 	}
// }
?>
<link rel="stylesheet" type="text/css" href="/Express/js/queryBuilder/jquery.jui_filter_rules.css">
<script type="text/javascript" src="/Express/js/queryBuilder/localization/en.js"></script>
<script type="text/javascript" src="/Express/js/queryBuilder/jquery.jui_filter_rules.js"></script>

<div id="filterUsersValuesRow" <?php echo $showFiltersByDefault ? "" : 'style="display: none;"'; ?> >
<div class="row" style="margin: 0 auto; width: 90%;" id="filtersTitle">
        <button type="button" class="btn filterTitleBtn" id="filtersStatusTitle">
                Advanced Filters <span class="glyphicon glyphicon-chevron-right fIcon"></span>
        </button>
</div>
</div>

<div class="row twoColWidth">
<div class=" col-md-12 ">

<div class="hideInitialDetailInfo" style="width:90%;margin:0 auto;">
<form name="userFiltersForm" id="userFiltersForm" method="POST" > <!-- class="divCFAforStyle" -->

	<input type="hidden" class="filtersAreEnabled" id="filtersAreEnabled" name="filtersAreEnabled" value="<?php echo isset($applyFiltersByDefault) && $applyFiltersByDefault ? 1 : 0; ?>"/>

        <div id="filterUsersValuesRow" <?php echo $showFiltersByDefault ? "" : 'style="display: none;"'; ?> >
		<!-- <div class="row" style="margin: 0 auto;" id="filtersTitle">
			<button type="button" class="btn filterTitleBtn" id="filtersStatusTitle">
				Advanced Filters <span class="glyphicon glyphicon-chevron-right fIcon"></span>
			</button>
		</div>
                -->
		<div id="filtersDiv" style="clear: both; padding: 12px 0px 20px;" class="divCFAforStyle">
			
			<div class="panel-bodya" style="">
				<div class="row" style="">
                                    <div class="col-md-12">
					<div class="col-md-7">
						<!-- <span style="margin: 0 24px; float:right " class="filterh1title">User Filters </span> -->
                                                <h2 class="userListTxt filterh1title" class="">User Filters </h2>
					</div>	
					<div class="col-md-5">
						<input id="showActiveFilterBtn" name="" value="Switch View" style="padding:5px 15px; float:right" type="button"/>
   					 	<input id="showAllFilterBtn" name="" value="Switch View" style="padding:5px 15px; float:right" type="button"/>
					</div>	
                                    </div>
				</div>
				
				<div class="row" style="">
					<div class="col-md-12">		
                                                    <input type="checkbox" name="clearAllFiltersBtn" id="clearAllFiltersBtn" />
                                                    <label class="labelText" for="clearAllFiltersBtn"><span></span></label>                                                    
                                                    <label class="labelText">Enable All Filters</label>
                                                    <button type="button" id="resetAllFilters" class="resetAll">Reset All</button>
					</div>
					
					<!--div style="margin:28px 18px;"><label style="margin-left:6px;"><input type="checkbox" name="clearAllFiltersBtn" id="clearAllFiltersBtn" /></label><label style="margin-left:5px;">Enable All Filters</label></div>
					<div class="filterDisableOverlay" style="display:none;position: absolute;height: 100%;width: 100%;top: 0;left: 0;z-index: 1000;background: #faebd7b0;">&nbsp;</div>
					
					<!--span style="margin: 0 24px; width:30%; float:left " class="filterh1title">User Filters </span>
					<div style="float:right; width:60%; xmargin-top:30px; margin-right:21px">
						<!-- input id="showActiveFilterBtn" name="" class="btn btn-primary center-block btn-averistar" value="Show Active Filters" style="padding:9px 15px; float:left" type="button"/>
   					 	<input id="showAllFilterBtn" name="" class="btn btn-primary center-block btn-averistar" value="Show All Filters" style="float:left; padding:9px 15px; float:left" type="button"/> 
   					 	<button type="button" id="resetAllFilters" class="pull-right btn btn-averistar">Reset All</button>
				 	</div>
				 	<br/-->
					
					<div class="user_filter_set" id="defaultUserFiltersDiv" style="clear:both">

						<table class="table table-condensed">
							<tr <?php echo in_array('RegisteredUser', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td style="width: 30px;">
									
									<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('RegisteredUser', $preset_filters_checked) ? "RegisteredUser" : ""; ?>"
											data-filter="RegisteredUser" class="filterH">
											
									<input type="hidden"
											name="FiltersDD[]"
											value="">
									<!-- input <?php echo in_array('RegisteredUser', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="RegisteredUser"
											class="filterCheckStatus" data-status=""
											data-filterd="RegisteredUser"-->
									
									<label class="switch">
										<input <?php echo in_array('RegisteredUser', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="RegisteredUser"
											id="FiltersDCheck"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="RegisteredUser">
											
										<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
									</label>				
								</td>
								<td>
									<span class='expandFilterClass'>Registration Status</span>	
									<div class="FilterOptions" id="RegisteredUser_FilterOptions" <?php echo in_array('RegisteredUser', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-xs-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->RegisteredUser) && $preset_filters_options->RegisteredUser == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[RegisteredUser]"
															id="Registered_UserFilterYES"
															class="filterOptionRadio"
															value="YES"><label for="Registered_UserFilterYES" class="labelText labelTextUFilter"><span></span>Registered</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->RegisteredUser) && $preset_filters_options->RegisteredUser == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[RegisteredUser]"
															id="Registered_UserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="Registered_UserFilterNO" class="labelText labelTextUFilter"><span></span>Not Registered</label>
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('ActivatedUser', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php //echo in_array('ActivatedUser', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="ActivatedUser"
											class="presetToggleCheckbox"
											data-filter="ActivatedUser">-->
											
											<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('ActivatedUser', $preset_filters_checked) ? "ActivatedUser" : ""; ?>"
											data-filter="ActivatedUser" class="filterH">
											
											<input type="hidden"
											name="FiltersDD[]"
											value="">
											
											<!-- input <?php echo in_array('ActivatedUser', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="ActivatedUser"
											class="filterCheckStatus" data-status=""
											data-filterd="ActivatedUser"-->
											<label class="switch">
											<input <?php echo in_array('ActivatedUser', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="ActivatedUser"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="ActivatedUser">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Activation Status</span>
									<div class="FilterOptions" id="ActivatedUser_FilterOptions" <?php echo in_array('ActivatedUser', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->ActivatedUser) && $preset_filters_options->ActivatedUser == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[ActivatedUser]"
															id="Activated_UserFilterYES"
															class="filterOptionRadio"
															value="YES"><label for="Activated_UserFilterYES" class="labelText labelTextUFilter"><span></span>Activated</label>
												</label>
											</div>

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->ActivatedUser) && $preset_filters_options->ActivatedUser == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[ActivatedUser]"
															id="Activated_UserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="Activated_UserFilterNO" class="labelText labelTextUFilter"><span></span>Not Activated</label>
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('ExtensionOnly', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php //echo in_array('ExtensionOnly', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="ExtensionOnly"
											class="presetToggleCheckbox"
											data-filter="ExtensionOnly">-->
											
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('ExtensionOnly', $preset_filters_checked) ? "ExtensionOnly" : ""; ?>"
											data-filter="ExtensionOnly" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
										
										<!-- input <?php echo in_array('ExtensionOnly', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="ExtensionOnly"
											class="filterCheckStatus" data-status=""
											data-filterd="ExtensionOnly"-->
											
										<label class="switch">
											<input <?php echo in_array('ExtensionOnly', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="ExtensionOnly"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="ExtensionOnly">
										
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
										
								</td>
								<td>
								<span class='expandFilterClass'>Phone Number/Extension </span>
									<div class="FilterOptions" id="ExtensionOnly_FilterOptions" <?php echo in_array('ExtensionOnly', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-9 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->ExtensionOnly) && $preset_filters_options->ExtensionOnly == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[ExtensionOnly]"
															id="ExtensionOnly_UserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="ExtensionOnly_UserFilterNO" class="labelText labelTextUFilter"><span></span>Phone Number and Extension</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->ExtensionOnly) && $preset_filters_options->ExtensionOnly == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[ExtensionOnly]"
															id="ExtensionOnly_UserFilterYES"
															class="filterOptionRadio"
															value="YES"><label for="ExtensionOnly_UserFilterYES" class="labelText labelTextUFilter"><span></span>Extension Only</label>
												</label>
											</div>

										</div>
										<div class="col-xs-3 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('DeviceLessUser', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!--<input <?php //echo in_array('DeviceLessUser', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="DeviceLessUser"
											class="presetToggleCheckbox"
											data-filter="DeviceLessUser">-->
											
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('DeviceLessUser', $preset_filters_checked) ? "DeviceLessUser" : ""; ?>"
											data-filter="DeviceLessUser" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('DeviceLessUser', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="DeviceLessUser"
											class="filterCheckStatus" data-status=""
											data-filterd="DeviceLessUser"-->
										
										<label class="switch">
											<input <?php echo in_array('DeviceLessUser', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="DeviceLessUser"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="DeviceLessUser">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Device Assignment</span>
									<div class="FilterOptions" id="DeviceLessUser_FilterOptions" <?php echo in_array('DeviceLessUser', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->DeviceLessUser) && $preset_filters_options->DeviceLessUser == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[DeviceLessUser]"
															id="DeviceLess_UserUserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="DeviceLess_UserUserFilterNO" class="labelText labelTextUFilter"><span></span>Device Assigned</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->DeviceLessUser) && $preset_filters_options->DeviceLessUser == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[DeviceLessUser]"
															id="DeviceLess_UserUserFilterYES"
															class="filterOptionRadio"
															value="YES"><label for="DeviceLess_UserUserFilterYES" class="labelText labelTextUFilter"><span></span>Device Not Assigned</label>
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('DnD', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php //echo in_array('DnD', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="DnD"
											class="presetToggleCheckbox"
											data-filter="DnD">-->
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('DnD', $preset_filters_checked) ? "DnD" : ""; ?>"
											data-filter="DnD" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('DnD', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="DnD"
											class="filterCheckStatus" data-status=""
											data-filterd="DnD"-->
										
										<label class="switch">
											<input <?php echo in_array('DnD', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="DnD"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="DnD">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Do Not Disturb</span>
									<div class="FilterOptions" id="DnD_FilterOptions" <?php echo in_array('DnD', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->DnD) && $preset_filters_options->DnD == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[DnD]"
															id="DnD_UserFilterYES"
															class="filterOptionRadio"
															value="YES"><label for="DnD_UserFilterYES" class="labelText labelTextUFilter"><span></span>Enabled</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->DnD) && $preset_filters_options->DnD == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[DnD]"
															id="DnD_UserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="DnD_UserFilterNO" class="labelText labelTextUFilter"><span></span>Disabled</label>
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							
							<!-- Calling Line ID Blocking -->
							<?php if($serviceInfo->hasCLIDBlocKingService($_SESSION['groupId'])) { ?>
							<tr <?php echo in_array('CLID_BLOCKING', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('CLID_BLOCKING', $preset_filters_checked) ? "CLID_BLOCKING" : ""; ?>"
											data-filter="CLID_BLOCKING" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
										
										<label class="switch">
											<input <?php echo in_array('CLID_BLOCKING', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="CLID_BLOCKING"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="CLID_BLOCKING">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Calling Line ID Blocking</span>
									<div class="FilterOptions" id="CLID_BLOCKING_FilterOptions" <?php echo in_array('CLID_BLOCKING', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->CLID_BLOCKING) && $preset_filters_options->CLID_BLOCKING == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[CLID_BLOCKING]"
															id="CLID_BLOCKING_UserFilterYES"
															class="filterOptionRadio"
															value="YES"><label for="CLID_BLOCKING_UserFilterYES" class="labelText labelTextUFilter"><span></span>Enabled</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->CLID_BLOCKING) && $preset_filters_options->CLID_BLOCKING == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[CLID_BLOCKING]"
															id="CLID_BLOCKING_UserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="CLID_BLOCKING_UserFilterNO" class="labelText labelTextUFilter"><span></span>Disabled</label>
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<?php } ?>
							
							<tr <?php echo in_array('SIP_ANALOG', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php echo in_array('SIP_ANALOG', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="SIP_ANALOG"
											class="presetToggleCheckbox"
											data-filter="SIP_ANALOG">-->
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('SIP_ANALOG', $preset_filters_checked) ? "SIP_ANALOGDnD" : ""; ?>"
											data-filter="SIP_ANALOG" class="filterH">
											
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('SIP_ANALOG', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="SIP_ANALOG"
											class="filterCheckStatus" data-status=""
											data-filterd="SIP_ANALOG"-->
										
										<label class="switch">
											<input <?php echo in_array('SIP_ANALOG', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="SIP_ANALOG"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="SIP_ANALOG">										
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>

										
								</td>
								<td>
									<span class='expandFilterClass'>SIP(digital)/Analog Users</span>
									<div class="FilterOptions" id="SIP_ANALOG_FilterOptions" <?php echo in_array('SIP_ANALOG', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->SIP_ANALOG) && $preset_filters_options->SIP_ANALOG == 'DIGITAL') ? "checked" : ""; ?>
															type="radio"
															name="Filter[SIP_ANALOG]"
															id="SIP_ANALOG_UserFilterYES"
															class="filterOptionRadio"
															value="DIGITAL"><label for="SIP_ANALOG_UserFilterYES" class="labelText labelTextUFilter"><span></span>SIP(digital)</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->SIP_ANALOG) && $preset_filters_options->SIP_ANALOG == 'ANALOG') ? "checked" : ""; ?>
															type="radio"
															name="Filter[SIP_ANALOG]"
															id="SIP_ANALOG_UserFilterNO"
															class="filterOptionRadio"
															value="ANALOG"><label for="SIP_ANALOG_UserFilterNO" class="labelText labelTextUFilter"><span></span>Analog</label>
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('Voice_Messaging', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php //echo in_array('Voice_Messaging', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="Voice_Messaging"
											class="presetToggleCheckbox"
											data-filter="Voice_Messaging">-->
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('Voice_Messaging', $preset_filters_checked) ? "Voice_Messaging" : ""; ?>"
											data-filter="Voice_Messaging" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('Voice_Messaging', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="Voice_Messaging"
											class="filterCheckStatus" data-status=""
											data-filterd="Voice_Messaging"-->
										
										<label class="switch">
											<input <?php echo in_array('Voice_Messaging', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="Voice_Messaging"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="Voice_Messaging">
										
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>

								</td>
								<td>
									<span class='expandFilterClass'>Voice Messaging Service</span>
									<div class="FilterOptions" id="Voice_Messaging_FilterOptions" <?php echo in_array('Voice_Messaging', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->Voice_Messaging_Assigned) && $preset_filters_options->Voice_Messaging_Assigned == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[Voice_Messaging_Assigned]"
															id="Voice_Messaging_UserFilterAssigned"
															class="filterOptionRadio"
															value="YES"><label for="Voice_Messaging_UserFilterAssigned" class="labelText labelTextUFilter"><span></span>Assigned</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->Voice_Messaging_Assigned) && $preset_filters_options->Voice_Messaging_Assigned == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[Voice_Messaging_Assigned]"
															id="Voice_Messaging_UserFilterNotAssigned"
															class="filterOptionRadio"
															value="NO"><label for="Voice_Messaging_UserFilterNotAssigned" class="labelText labelTextUFilter"><span></span>Not Assigned</label>
												</label>
											</div>
											<br/>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->Voice_Messaging) && $preset_filters_options->Voice_Messaging == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[Voice_Messaging]"
															id="Voice_Messaging_UserFilterYES"
															class="filterOptionRadio"
															value="YES"><label for="Voice_Messaging_UserFilterYES" class="labelText labelTextUFilter"><span></span>Enabled</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->Voice_Messaging) && $preset_filters_options->Voice_Messaging == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[Voice_Messaging]"
															id="Voice_Messaging_UserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="Voice_Messaging_UserFilterNO" class="labelText labelTextUFilter"><span></span>Disabled</label>
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('CFA_CFB_CFN', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!--<input <?php //echo in_array('CFA_CFB_CFN', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="CFA_CFB_CFN"
											class="presetToggleCheckbox"
											data-filter="CFA_CFB_CFN">-->
											
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('CFA_CFB_CFN', $preset_filters_checked) ? "CFA_CFB_CFN" : ""; ?>"
											data-filter="CFA_CFB_CFN" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('CFA_CFB_CFN', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="CFA_CFB_CFN"
											class="filterCheckStatus" data-status=""
											data-filterd="CFA_CFB_CFN"-->
										
										<label class="switch">
											<input <?php echo in_array('CFA_CFB_CFN', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="CFA_CFB_CFN"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="CFA_CFB_CFN">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>

										
								</td>
								<td>
									<span class='expandFilterClass'>CFA/CFB/CFNA/CFNR</span>
									<div class="FilterOptions" id="CFA_CFB_CFN_FilterOptions" <?php echo in_array('CFA_CFB_CFN', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class=" control-label">

												<div class="row">
													<div class="radio-inline"><label class="labelText labelTextUFilter">CFA:</label></div>

													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CFA) && $preset_filters_options->CFA == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFA]"
																	id="CFA_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="CFA_UserFilterYES" class="labelText labelTextUFilter"><span></span>Enabled</label>
														</label>
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CFA) && $preset_filters_options->CFA == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFA]"
																	id="CFA_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="CFA_UserFilterNO" class="labelText labelTextUFilter"><span></span>Disabled</label>
														</label>
													</div>
												</div>

												<div class="row">
													<div class="radio-inline"><label class="labelText labelTextUFilter">CFB:</label></div>

													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CFB) && $preset_filters_options->CFB == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFB]"
																	id="CFB_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="CFB_UserFilterYES" class="labelText labelTextUFilter"><span></span>Enabled</label>
														</label>
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CFB) && $preset_filters_options->CFB == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFB]"
																	id="CFB_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="CFB_UserFilterNO" class="labelText labelTextUFilter"><span></span>Disabled</label>
														</label>
													</div>
												</div>

												<div class="row">
													<div class="radio-inline"><label class="labelText labelTextUFilter">CFNA:</label></div>

													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CFN) && $preset_filters_options->CFN == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFN]"
																	id="CFN_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="CFN_UserFilterYES" class="labelText labelTextUFilter"><span></span>Enabled</label>
														</label>
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CFN) && $preset_filters_options->CFN == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFN]"
																	id="CFN_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="CFN_UserFilterNO" class="labelText labelTextUFilter"><span></span>Disabled</label>
														</label>
													</div>
												</div>

												<div class="row">
													<div class="radio-inline"><label class="labelText labelTextUFilter">CFNR:</label></div>

													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CFR) && $preset_filters_options->CFR == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFR]"
																	id="CFR_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="CFR_UserFilterYES" class="labelText labelTextUFilter"><span></span>Enabled</label>
														</label>
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CFR) && $preset_filters_options->CFR == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CFR]"
																	id="CFR_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="CFR_UserFilterNO" class="labelText labelTextUFilter"><span></span>Disabled</label>
														</label>
													</div>
												</div>

											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('BLF', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!--<input <?php //echo in_array('BLF', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="BLF"
											class="presetToggleCheckbox"
											data-filter="BLF">-->
										
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('BLF', $preset_filters_checked) ? "BLF" : ""; ?>"
											data-filter="BLF" class="filterH">	
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('BLF', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="BLF"
											class="filterCheckStatus" data-status=""
											data-filterd="BLF"-->
										
										<label class="switch">
											<input <?php echo in_array('BLF', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="BLF"
												class="filterCheckStatus switch-input" data-status=""
												data-filterd="BLF">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>

										
								</td>
								<td>
									<span class='expandFilterClass'>BLF (Monitoring)</span>
									<div class="FilterOptions" id="BLF_FilterOptions" <?php echo in_array('BLF', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->BLF) && $preset_filters_options->BLF == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[BLF]"
															id="BLF_UserFilterYES"
															class="filterOptionRadio"
															value="YES"><label for="BLF_UserFilterYES" class="labelText labelTextUFilter"><span></span>Yes</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->BLF) && $preset_filters_options->BLF == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[BLF]"
															id="BLF_UserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="BLF_UserFilterNO" class="labelText labelTextUFilter"><span></span>No</label>
															
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('SCA_PRIMARY', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php //echo in_array('SCA_PRIMARY', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="SCA_PRIMARY"
											class="presetToggleCheckbox"
											data-filter="SCA_PRIMARY">-->
										
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('SCA_PRIMARY', $preset_filters_checked) ? "SCA_PRIMARY" : ""; ?>"
											data-filter="SCA_PRIMARY" class="filterH">
												
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('SCA_PRIMARY', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="SCA_PRIMARY"
											class="filterCheckStatus" data-status=""
											data-filterd="SCA_PRIMARY"-->
										
										<label class="switch">												
											<input <?php echo in_array('SCA_PRIMARY', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="SCA_PRIMARY"
												class="filterCheckStatus switch-input" data-status=""
												data-filterd="SCA_PRIMARY">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
											
								</td>
								<td>
									<span class='expandFilterClass'>SCA Primary Users (users having shared call appearances on their devices)</span>
									<div class="FilterOptions" id="SCA_PRIMARY_FilterOptions" <?php echo in_array('SCA_PRIMARY', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->SCA_PRIMARY) && $preset_filters_options->SCA_PRIMARY == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[SCA_PRIMARY]"
															id="SCA_PRIMARY_UserFilterYES"
															class="filterOptionRadio"
															value="YES"><label for="SCA_PRIMARY_UserFilterYES" class="labelText labelTextUFilter"><span></span>Has SCA Users</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->SCA_PRIMARY) && $preset_filters_options->SCA_PRIMARY == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[SCA_PRIMARY]"
															id="SCA_PRIMARY_UserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="SCA_PRIMARY_UserFilterNO" class="labelText labelTextUFilter"><span></span>Has No SCA Users</label>
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('SCA_USER', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php //echo in_array('SCA_USER', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="SCA_USER"
											class="presetToggleCheckbox"
											data-filter="SCA_USER">-->
											
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('SCA_USER', $preset_filters_checked) ? "SCA_USER" : ""; ?>"
											data-filter="SCA_USER" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('SCA_USER', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="SCA_USER"
											class="filterCheckStatus" data-status=""
											data-filterd="SCA_USER"-->
										
										<label class="switch">	
											<input <?php echo in_array('SCA_USER', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="SCA_USER"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="SCA_USER">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
										
								</td>
								<td>
									<span class='expandFilterClass'>SCA Users (SCA Appearances - users having appearances on other devices)</span>
									<div class="FilterOptions" id="SCA_USER_FilterOptions" <?php echo in_array('SCA_USER', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->SCA_USER) && $preset_filters_options->SCA_USER == 'YES') ? "checked" : ""; ?>
															type="radio"
															name="Filter[SCA_USER]"
															id="SCA_USER_UserFilterYES"
															class="filterOptionRadio"
															value="YES"><label for="SCA_USER_UserFilterYES" class="labelText labelTextUFilter"><span></span>Has Appearances</label>
												</label>
											</div>
											<div class="radio-inline">
												<label>
													<input <?php echo (isset($preset_filters_options->SCA_USER) && $preset_filters_options->SCA_USER == 'NO') ? "checked" : ""; ?>
															type="radio"
															name="Filter[SCA_USER]"
															id="SCA_USER_UserFilterNO"
															class="filterOptionRadio"
															value="NO"><label for="SCA_USER_UserFilterNO" class="labelText labelTextUFilter"><span></span>Has No Appearances</label>
												</label>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('DEVICE_TYPE', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php //echo in_array('DEVICE_TYPE', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="DEVICE_TYPE"
											class="presetToggleCheckbox"
											data-filter="DEVICE_TYPE">-->
											
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('DEVICE_TYPE', $preset_filters_checked) ? "DEVICE_TYPE" : ""; ?>"
											data-filter="DEVICE_TYPE" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('DEVICE_TYPE', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="DEVICE_TYPE"
											class="filterCheckStatus" data-status=""
											data-filterd="DEVICE_TYPE"-->
											
										<label class="switch">
											<input <?php echo in_array('DEVICE_TYPE', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="DEVICE_TYPE"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="DEVICE_TYPE">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
											
								</td>
								<td>
									<span class='expandFilterClass'>Device Type</span>
									<div class="FilterOptions" id="DEVICE_TYPE_FilterOptions" <?php echo in_array('DEVICE_TYPE', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="control-label">

												<div class="">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->DEVICE_TYPE) && $preset_filters_options->DEVICE_TYPE == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[DEVICE_TYPE]"
																	id="DEVICE_TYPE_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="DEVICE_TYPE_UserFilterYES" class="labelText labelTextUFilter"><span></span>With</label>
														</label>
														&nbsp; <label class="labelText labelTextUFilter">OR </label>&nbsp;
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->DEVICE_TYPE) && $preset_filters_options->DEVICE_TYPE == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[DEVICE_TYPE]"
																	id="DEVICE_TYPE_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="DEVICE_TYPE_UserFilterNO" class="labelText labelTextUFilter"><span></span>Without</label>
														</label>
													</div>
												</div>
												<div class="dropdown-wrap oneColWidth">
												<select name="Filter[DEVICE_TYPE_SELECTED]" id="DEVICE_TYPE_SELECTED_UserFilter" class="form-control">
													<option value="">Please Select</option>
													<?php
													asort($deviceTypesDigitalVoIP);
													foreach ($deviceTypesDigitalVoIP as $deviceType) {
														?>
														<option <?php echo (isset($preset_filters_options->DEVICE_TYPE_SELECTED) && $preset_filters_options->DEVICE_TYPE_SELECTED == $deviceType) ? "SELECTED" :
															"";
														?>><?php echo $deviceType ?></option>
														<?php
													}
													?>
												</select>
												</div>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('SERVICE_PACK', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php //echo in_array('SERVICE_PACK', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="SERVICE_PACK"
											class="presetToggleCheckbox"
											data-filter="SERVICE_PACK">-->
											
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('SERVICE_PACK', $preset_filters_checked) ? "SERVICE_PACK" : ""; ?>"
											data-filter="SERVICE_PACK" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('SERVICE_PACK', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="SERVICE_PACK"
											class="filterCheckStatus" data-status=""
											data-filterd="SERVICE_PACK"-->
										
										<label class="switch">
											<input <?php echo in_array('SERVICE_PACK', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="SERVICE_PACK"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="SERVICE_PACK">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Service Pack</span>
									<div class="FilterOptions" id="SERVICE_PACK_FilterOptions" <?php echo in_array('SERVICE_PACK', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="control-label">

												<div class="">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->SERVICE_PACK) && $preset_filters_options->SERVICE_PACK == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[SERVICE_PACK]"
																	id="SERVICE_PACK_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="SERVICE_PACK_UserFilterYES" class="labelText labelTextUFilter"><span></span>With</label>
														</label>
														&nbsp; <label class="labelText labelTextUFilter">OR</label> &nbsp;
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->SERVICE_PACK) && $preset_filters_options->SERVICE_PACK == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[SERVICE_PACK]"
																	id="SERVICE_PACK_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="SERVICE_PACK_UserFilterNO" class="labelText labelTextUFilter"><span></span>Without</label>
														</label>
													</div>
												</div>
												<div class="dropdown-wrap oneColWidth">
												<select name="Filter[SERVICE_PACK_SELECTED]" id="SERVICE_PACK_SELECTED_UserFilter" class="form-control">
													<option value="">Please Select</option>
													<?php
													asort($servicePacks);
													foreach ($servicePacks as $servicePack) {
														?>
														<option <?php echo (isset($preset_filters_options->SERVICE_PACK_SELECTED) && $preset_filters_options->SERVICE_PACK_SELECTED == $servicePack) ? "SELECTED" :
															"";
														?>><?php echo $servicePack ?></option>
														<?php
													}
													?>
												</select>
												</div>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('USER_SERVICE', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php //echo in_array('USER_SERVICE', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="USER_SERVICE"
											class="presetToggleCheckbox"
											data-filter="USER_SERVICE">-->
											
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('USER_SERVICE', $preset_filters_checked) ? "USER_SERVICE" : ""; ?>"
											data-filter="USER_SERVICE" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('USER_SERVICE', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="USER_SERVICE"
											class="filterCheckStatus" data-status=""
											data-filterd="USER_SERVICE"-->
										
										<label class="switch">
											<input <?php echo in_array('USER_SERVICE', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="USER_SERVICE"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="USER_SERVICE">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
										
								</td>
								<td>
									<span class='expandFilterClass'>User Service</span>
									<div class="FilterOptions" id="USER_SERVICE_FilterOptions" <?php echo in_array('USER_SERVICE', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">
											<div class="control-label">
												<div class="">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->USER_SERVICE) && $preset_filters_options->USER_SERVICE == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[USER_SERVICE]"
																	id="USER_SERVICE_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="USER_SERVICE_UserFilterYES" class="labelText labelTextUFilter"><span></span>With</label>
														</label>
														&nbsp; <label class="labelText labelTextUFilter">OR</label> &nbsp;
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->USER_SERVICE) && $preset_filters_options->USER_SERVICE == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[USER_SERVICE]"
																	id="USER_SERVICE_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="USER_SERVICE_UserFilterNO" class="labelText labelTextUFilter"><span></span>Without</label>
														</label>
													</div>
												</div>
												<div class="dropdown-wrap oneColWidth">
												<select name="Filter[USER_SERVICE_SELECTED]" id="USER_SERVICE_SELECTED_UserFilter" class="form-control">
													<option value="">Please Select</option>
													<?php
													asort($userServices);
													foreach ($userServices as $userService) {
														?>
														<option <?php echo (isset($preset_filters_options->USER_SERVICE_SELECTED) && $preset_filters_options->USER_SERVICE_SELECTED == $userService) ? "SELECTED" :
															"";
														?>><?php echo $userService ?></option>
														<?php
													}
													?>
												</select>
												</div>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('CUSTOM_PROFILE', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!-- <input <?php //echo in_array('CUSTOM_PROFILE', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="CUSTOM_PROFILE"
											class="presetToggleCheckbox"
											data-filter="CUSTOM_PROFILE">-->
											
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('CUSTOM_PROFILE', $preset_filters_checked) ? "CUSTOM_PROFILE" : ""; ?>"
											data-filter="CUSTOM_PROFILE" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('CUSTOM_PROFILE', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="CUSTOM_PROFILE"
											class="filterCheckStatus" data-status=""
											data-filterd="CUSTOM_PROFILE"-->
										
										<label class="switch">
											<input <?php echo in_array('CUSTOM_PROFILE', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="CUSTOM_PROFILE"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="CUSTOM_PROFILE">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Custom Profile</span>
									<div class="FilterOptions" id="CUSTOM_PROFILE_FilterOptions" <?php echo in_array('CUSTOM_PROFILE', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="control-label">

												<div class="">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CUSTOM_PROFILE) && $preset_filters_options->CUSTOM_PROFILE == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CUSTOM_PROFILE]"
																	id="CUSTOM_PROFILE_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="CUSTOM_PROFILE_UserFilterYES" class="labelText labelTextUFilter"><span></span>With</label>
														</label>
														<label class="labelText labelTextUFilter">OR</label>
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CUSTOM_PROFILE) && $preset_filters_options->CUSTOM_PROFILE == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CUSTOM_PROFILE]"
																	id="CUSTOM_PROFILE_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="CUSTOM_PROFILE_UserFilterNO" class="labelText labelTextUFilter"><span></span>Without</label>
														</label>
													</div>
												</div>
												<div class="dropdown-wrap oneColWidth">
												<select name="Filter[CUSTOM_PROFILE_SELECTED]" id="CUSTOM_PROFILE_SELECTED_UserFilter" class="form-control">
													<option>None</option>
													<?php
													$custom_profiles = getDistictCustomProfiles();
													asort($custom_profiles);
													foreach ($custom_profiles as $profile) {
														?>
														<option <?php echo (isset($preset_filters_options->CUSTOM_PROFILE_SELECTED) && $preset_filters_options->CUSTOM_PROFILE_SELECTED == $profile[expressSheetCustomProfileName]) ? "SELECTED" :
															"";
														?>><?php echo $profile[expressSheetCustomProfileName] ?></option>
														<?php
													}
													?>
												</select>
												</div>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('CUSTOM_TAG', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!--<input <?php //echo in_array('CUSTOM_TAG', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="CUSTOM_TAG"
											class="presetToggleCheckbox"
											data-filter="CUSTOM_TAG">-->
										
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('CUSTOM_TAG', $preset_filters_checked) ? "CUSTOM_TAG" : ""; ?>"
											data-filter="CUSTOM_TAG" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('CUSTOM_TAG', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="CUSTOM_TAG"
											class="filterCheckStatus" data-status=""
											data-filterd="CUSTOM_TAG"-->
										
										<label class="switch">
											<input <?php echo in_array('CUSTOM_TAG', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="CUSTOM_TAG"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="CUSTOM_TAG">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Custom Tag</span>
									<div class="FilterOptions" id="CUSTOM_TAG_FilterOptions" <?php echo in_array('CUSTOM_TAG', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-8 paddingZero">

											<div class="control-label">

												<div class="">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CUSTOM_TAG) && $preset_filters_options->CUSTOM_TAG == 'YES') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CUSTOM_TAG]"
																	id="CUSTOM_TAG_UserFilterYES"
																	class="filterOptionRadio"
																	value="YES"><label for="CUSTOM_TAG_UserFilterYES" class="labelText labelTextUFilter"><span></span>With</label>
														</label>
														<label class="labelText labelTextUFilter">OR</label>
													</div>
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->CUSTOM_TAG) && $preset_filters_options->CUSTOM_TAG == 'NO') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[CUSTOM_TAG]"
																	id="CUSTOM_TAG_UserFilterNO"
																	class="filterOptionRadio"
																	value="NO"><label for="CUSTOM_TAG_UserFilterNO" class="labelText labelTextUFilter"><span></span>Without</label>
														</label>
													</div>
												</div>
												<div class="dropdown-wrap oneColWidth">
												<select name="Filter[CUSTOM_TAG_SELECTED]" id="CUSTOM_TAG_SELECTED_UserFilter" class="form-control">
													<option>None</option>
													<?php
													//asort($custom_tags);
													foreach ($availableTagsArray as $custom_tag) {
														?>
														<option <?php echo (isset($preset_filters_options->CUSTOM_TAG_SELECTED) && $preset_filters_options->CUSTOM_TAG_SELECTED == $custom_tag['shortDescription']) ? "SELECTED" :
															"";
														?>><?php echo $custom_tag['shortDescription'] ?></option>
														<?php
													}
													?>
												</select>
												</div>
											</div>

										</div>
										<div class="col-xs-4 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('PHONE_NUMBER', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!--<input <?php //echo in_array('PHONE_NUMBER', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="PHONE_NUMBER"
											class="presetToggleCheckbox"
											data-filter="PHONE_NUMBER">-->
										
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('PHONE_NUMBER', $preset_filters_checked) ? "PHONE_NUMBER" : ""; ?>"
											data-filter="PHONE_NUMBER" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('PHONE_NUMBER', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="PHONE_NUMBER"
											class="filterCheckStatus" data-status=""
											data-filterd="PHONE_NUMBER"-->
										
										<label class="switch">
											<input <?php echo in_array('PHONE_NUMBER', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="PHONE_NUMBER"
											class="filterCheckStatus switch-input" data-status=""
											data-filterd="PHONE_NUMBER">
											<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Phone Numbers</span>
									<div class="FilterOptions" id="PHONE_NUMBER_FilterOptions" <?php echo in_array('PHONE_NUMBER', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-10">

											<div class="control-label">

												<div class="row" id="uFilterNumber">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->PHONE_NUMBER) && $preset_filters_options->PHONE_NUMBER == 'StartsWith') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[PHONE_NUMBER]"
																	id="PHONE_NUMBER_UserFilter_StartsWith"
																	class="filterOptionRadio"
																	value="StartsWith"><label for="PHONE_NUMBER_UserFilter_StartsWith" class="labelText labelTextUFilter"><span></span>Starts With:</label>
														</label>
													</div>
													<input type="number"
													       name="Filter[PHONE_NUMBER_STARTS_WITH]"
													       id="PHONE_NUMBER_STARTS_WITH"
													       class="filterOptionText"
													       placeholder="Eg: 301, 301916"
													       min="0"
													       value="<?php echo isset($preset_filters_options->PHONE_NUMBER_STARTS_WITH) ? $preset_filters_options->PHONE_NUMBER_STARTS_WITH : ""; ?>">
												</div>

												<div class="row" id="uFilterNumber">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->PHONE_NUMBER) && $preset_filters_options->PHONE_NUMBER == 'Range') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[PHONE_NUMBER]"
																	id="PHONE_NUMBER_UserFilter_Range"
																	class="filterOptionRadio"
																	value="Range"><label for="PHONE_NUMBER_UserFilter_Range" class="labelText labelTextUFilter"><span></span>Range From:</label>
														</label>
													</div>
													<input type="number"
													       name="Filter[PHONE_NUMBER_RANGE_FROM]"
													       id="PHONE_NUMBER_RANGE_FROM"
													       class="filterOptionText"
													       placeholder="Eg: 3019162000"
													       min="0"
													       value="<?php echo isset($preset_filters_options->PHONE_NUMBER_RANGE_FROM) ? $preset_filters_options->PHONE_NUMBER_RANGE_FROM : ""; ?>">
													<strong class="labelText labelTextUFilter"> To </strong>
													<input type="number"
													       name="Filter[PHONE_NUMBER_RANGE_TO]"
													       id="PHONE_NUMBER_RANGE_TO"
													       class="filterOptionText"
													       placeholder="Eg: 3019162050"
													       min="0"
													       value="<?php echo isset($preset_filters_options->PHONE_NUMBER_RANGE_TO) ? $preset_filters_options->PHONE_NUMBER_RANGE_TO : ""; ?>">
												</div>

											</div>

										</div>
										<div class="col-xs-2 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
							<tr <?php echo in_array('EXTENSION', $preset_filters_checked) ? "class='active'" : ""; ?>>
								<td>
									<!--  <input <?php //echo in_array('EXTENSION', $preset_filters_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="Filters[]"
											value="EXTENSION"
											class="presetToggleCheckbox"
											data-filter="EXTENSION"> -->
											
										<input type="hidden"
											name="Filters[]"
											value="<?php echo in_array('EXTENSION', $preset_filters_checked) ? "EXTENSION" : ""; ?>"
											data-filter="EXTENSION" class="filterH">
										
										<input type="hidden"
											name="FiltersDD[]"
											value="">
											
										<!-- input <?php echo in_array('EXTENSION', $preset_filtersd_checked) ? "checked" : ""; ?>
											type="checkbox"
											name="FiltersD[]"
											value="EXTENSION"
											class="filterCheckStatus" data-status=""
											data-filterd="EXTENSION"--> 
										
										<label class="switch">
											<input <?php echo in_array('EXTENSION', $preset_filtersd_checked) ? "checked" : ""; ?>
												type="checkbox"
												name="FiltersD[]"
												value="EXTENSION"
												class="filterCheckStatus switch-input" data-status=""
												data-filterd="EXTENSION"> 
												<span class="switch-label" data-on="On" data-off="Off"></span> <span class="switch-handle"></span>
										</label>
								</td>
								<td>
									<span class='expandFilterClass'>Extensions</span>
									<div class="FilterOptions" id="EXTENSION_FilterOptions" <?php echo in_array('EXTENSION', $preset_filters_checked) ? "" : 'style="display: none;"' ?>>
										<div class="col-sm-10">

											<div class=" control-label">

												<div class="row" id="uFilterNumber">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->EXTENSION) && $preset_filters_options->EXTENSION == 'StartsWith') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[EXTENSION]"
																	id="EXTENSION_UserFilter_StartsWith"
																	class="filterOptionRadio"
																	value="StartsWith"><label for="EXTENSION_UserFilter_StartsWith" class="labelText labelTextUFilter"><span></span>Starts With:</label>
														</label>
													</div>
													<input type="number"
													       name="Filter[EXTENSION_STARTS_WITH]"
													       id="EXTENSION_STARTS_WITH"
													       class="filterOptionText"
													       placeholder="Eg: 200, 210"
													       min="0"
													       value="<?php echo isset($preset_filters_options->EXTENSION_STARTS_WITH) ? $preset_filters_options->EXTENSION_STARTS_WITH : ""; ?>">
												</div>

												<div class="row" id="uFilterNumber">
													<div class="radio-inline">
														<label>
															<input <?php echo (isset($preset_filters_options->EXTENSION) && $preset_filters_options->EXTENSION == 'Range') ? "checked" : ""; ?>
																	type="radio"
																	name="Filter[EXTENSION]"
																	id="EXTENSION_UserFilter_Range"
																	class="filterOptionRadio"
																	value="Range"><label for="EXTENSION_UserFilter_Range" class="labelText labelTextUFilter"><span></span>Range From:</label>
														</label>
													</div>
													<input type="number"
													       name="Filter[EXTENSION_RANGE_FROM]"
													       id="EXTENSION_RANGE_FROM"
													       class="filterOptionText"
													       placeholder="Eg: 2000"
													       min="0"
													       value="<?php echo isset($preset_filters_options->EXTENSION_RANGE_FROM) ? $preset_filters_options->EXTENSION_RANGE_FROM : ""; ?>">
													<strong class="labelText labelTextUFilter"> To </strong>
													<input type="number"
													       name="Filter[EXTENSION_RANGE_TO]"
													       id="EXTENSION_RANGE_TO"
													       class="filterOptionText"
													       placeholder="Eg: 2050"
													       min="0"
													       value="<?php echo isset($preset_filters_options->EXTENSION_RANGE_TO) ? $preset_filters_options->EXTENSION_RANGE_TO : ""; ?>">
												</div>

											</div>

										</div>
										<div class="col-xs-2 resetBtnDiv" style="">
											<input type="button" name="resetFilter" class="resetF" value="Reset"/>
										</div>
									</div>
								</td>
							</tr>
						</table>
						
					</div>
				</div>

				<?php
				//if ($advancedUserFilters == "true") {
					?>

				<!--  <div class="row formspace well" style="position: relative;">

						<div class="filterDisableOverlay" style="display:none;position: absolute;height: 100%;width: 100%;top: 0;left: 0;z-index: 1000;background: #faebd7b0;">&nbsp;</div>

						<h1 style="padding-left: 10px;">Advanced Filters</h1>
						<div class="col-xs-24" id="addNewFilterSetButtonDiv">
							<button type="button" id="addNewFilterSet" class="btn btn-averistar pull-right">Add New Advanced Filter</button>
						</div>
					</div> -->

					<?php
			//	}
				?>

				<div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="text-align:center;">
                                            <button type="button" class="subButton applyFilterBtn" style="">Apply Filters</button>
                                        </div>
                                     </div>
                                        <!-- button type="button" id="clearAllFiltersBtn" class="btn btn-averistar pull-right">Disable Filters</button>
					<button type="button" id="enableAllFiltersBtn" class="btn btn-averistar pull-right" style="display: none;">Enable Filters</button-->
				</div>

				<!-- User List-->
				<!-- div style="clear:both; padding-bottom:15px;"></div>
				<div id="filterUsersLoading" class="loading"><img src="/Express/images/ajax-loader.gif"></div>
				<div style="clear:both; padding-bottom:15px;"></div-->
				<?php                                
				if ($need_user_table) {
					?>
					<div class="row formspace" id="usersTableDiv" style="display: none;">

						<table id="allUsers" class="scroll tablesorter" style="width:100%;margin:0;">
							<thead>
							<tr>
								<th style="width:15px;" class="user_column col_Selected">Select</th>
								<th style="width:15px;" class="user_column col_Activated">Activated</th>
								<th style="width:15px;" class="user_column col_Registered">Registered</th>
								<th style="width:15px;" class="user_column col_First_Name">First Name</th>
								<th style="width:15px;" class="user_column col_Last_Name">Last Name</th>
								<th style="width:15px;" class="user_column col_User_Id">User Id</th>
								<th style="width:15px;" class="user_column col_Extension">Extension</th>
								<th style="width:15px;" class="user_column col_Device_Name">Device Name</th>
								<th style="width:15px;" class="user_column col_Device_Type">Device Type</th>
								<th style="width:15px;" class="user_column col_Device_Type">Analog Port Assignment</th>
								<th style="width:15px;" class="user_column col_MAC_Address">MAC Address</th>
								<!--<th style="width:6%;" class="user_column col_Department">Department</th>-->
								<th style="width:15px;" class="user_column col_Service_Pack">Service Pack</th>
								<!--<th style="width:5%;" class="user_column col_Expiration">Expiration</th>-->
								<th style="width:15px;" class="user_column col_Type">Type</th>
								<th style="width:15px;" class="user_column col_DnD">DnD</th>
								<th style="width:15px;" class="user_column col_Fwd">Fwd</th>
								<th style="width:15px;" class="user_column col_Fwd_To">Fwd To</th>
								<!--<th style="width:5%;" class="user_column col_Remote_Office">Remote Office</th>-->
								<!--<th style="width:6%;" class="user_column col_Remote_Number">Remote Number</th>-->
								<th style="width:15px;" class="user_column col_Polycom_Phone_Services">Polycom Phone Services</th>
								<th style="width:15px;" class="user_column col_Custom_Contact_Directory">Custom Contact Directory</th>
							</tr>
							</thead>
							<tbody style="max-height: 450px;height: auto;">
							<?php

							//require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/users/getAllUsers.php");
							//require_once("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
							/*
							require_once("/var/www/lib/broadsoft/adminPortal/util/getAllUsersFilterOperation.php");
							$userOperation = new UserDetailsOperation();
							$users = $userOperation->getAllUserDetailsForExpress($sp, $groupId);
							
							if (isset($_SESSION["userDeviceList"])) {
								unset($_SESSION["userDeviceList"]);
							}

							$rowIndex = 0;
							//echo "rajesh"; print_r($users);
							if (isset($users)) {

								for ($a = 0; $a < count($users); $a++) {
									$userId = $users[$a]["userId"];
									$firstName = $users[$a]["firstName"];
									$lastName = $users[$a]["LastName"];
									$userName = $users[$a]["firstName"] . " " . $users[$a]["LastName"];
									$extension = $users[$a]["extension"];
									$DnD = $users[$a]["DnD"];
									$Fwd = $users[$a]["Fwd"]["active"];
									$remote = $users[$a]["remote"]["active"];
									$department = $users[$a]["department"];
									$servicePack = isset($users[$a]["servicePack"]) ? implode(", ", $users[$a]["servicePack"]) : "";

									if ($DnD == "false") {
										$DnD = "Off";
									} else {
										$DnD = "On";
									}
									if ($Fwd == "true") {
										$Fwd = "Active";
										$FwdTo = $users[$a]["Fwd"]["FwdTo"];
									} else {
										if (isset($users[$a]["Fwd"]["FwdTo"]) && $users[$a]["Fwd"]["FwdTo"] != "") {
											$Fwd = "Not Active";
											$FwdTo = $users[$a]["Fwd"]["FwdTo"];
										} else {
											$Fwd = "&nbsp;";
											$FwdTo = "&nbsp;";
										}
									}
									if ($remote == "true") {
										$remote = "*";
										$remote_number = $users[$a]["remote"]["number"];
									} else {
										$remote = "&nbsp;";
										$remote_number = "&nbsp;";
									}

									$fileName = "/var/www/SASTestingUser/SASTestUsers.csv";
									if (file_exists($fileName)) {
										$objcsv = new UserOperations();
										$searchInCsv = $objcsv->find_user_in_csv($fileName, $userId);
									} else {
										$searchInCsv = "";
									}

									// get user dn number activation response
									$userDn = new Dns ();
									$numberActivateResponse = $userDn->getUserDNActivateListRequest($userId);
									// echo "<pre>"; print_r($numberActivateResponse); die;
									$phoneNumberStatus = "";
									$userPhoneNumber = "";
									if (empty ($numberActivateResponse ['Error'])) {
										if (!empty ($numberActivateResponse ['Success'] [0] ['phoneNumber'])) {
											$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
										}
										if (!empty ($numberActivateResponse ['Success'] [0] ['status'])) {
											$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
										}
									}

									if (isset($users[$a]["registration"]) and count($users[$a]["registration"]) > 0) {
										for ($b = 0; $b < count($users[$a]["registration"]); $b++) {
											$deviceName = $users[$a]["registration"][$b]["deviceName"];
											$expiration = $users[$a]["registration"][$b]["expiration"];
											$epType = $users[$a]["registration"][$b]["epType"];

											$deviceType = $users[$a]["registration"][$b]["deviceType"];
											$macAddress = $users[$a]["registration"][$b]["macAddress"];

											$integration = $users[$a]["registration"][$b]["integration"];
											$ccd = $users[$a]["registration"][$b]["ccd"];

											if ($searchInCsv != "") {
												$isRegUserSasTester = "SAS";
											} else {
												$isRegUserSasTester = "Yes";
											}

											if (isset($_SESSION["permissions"]["modifyUsers"]) and $_SESSION["permissions"]["modifyUsers"] == "1") {
												$userIdValue = "<a href='#' class='userIdVal numberList' id='" . $userId . "' data-lastname='" . $users[$a]["LastName"] . "' data-firstname='" . $users[$a]["firstName"] . "' >" . $userId . "</a>";
											} else {
												$userIdValue = $userId;
											}
											echo "<tr id='broadsoft_user_$userId' title='broadsoft_usersRow_$userId' class='broadsoft_users'>"
												. "<td style='width:50px;' class=\"user_column col_Selected\">"
												. "<input type='checkbox' id='users_selected_$userId' class='users_selected' name='usersSelected[]' CHECKED value='" . $userId . "'></td>"
												. "<td style='width:70px;' class=\"user_column col_Registered\">" . $phoneNumberStatus . "</td>"
												. "<td style='width:70px;' class=\"user_column col_Registered\">" . $isRegUserSasTester . "</td>"
												. "<td style='width:70px;' class=\"user_column col_Fist_Name\">" . $firstName . "</td>"
												. "<td style='width:70px;' class=\"user_column col_Last_Name\">" . $lastName . "</td>"
												. "<td style='width:70px;' class=\"user_column col_User_Id\">" . $userIdValue . "</td>"
												. "<td style='width:70px;' class=\"user_column col_Extension\">" . $extension . "</td>"
												. "<td style='width:90px;' class=\"user_column col_Device_Name\">" . $deviceName . "</td>"
												. "<td style='width:90px;' class=\"user_column col_Device_Type\">" . $deviceType . "</td>"
												. "<td style='width:90px;' class=\"user_column col_MAC_Address\">" . $macAddress . "</td>"
												. "<!--<td style='width:90px;' class=\"user_column col_Department\">" . $department . "</td>-->"
												. "<td style='width:90px;' class=\"user_column col_Service_Pack\">" . $servicePack . "</td>"
												. "<!--<td style='width:70px;' class=\"user_column col_Expiration\">" . $expiration . "</td>-->"
												. "<td style='width:70px;' class=\"user_column col_Type\">" . $epType . "</td>"
												. "<td style='width:70px;' class=\"user_column col_DnD\">" . $DnD . "</td>"
												. "<td style='width:70px;' class=\"user_column col_Fwd\">" . $Fwd . "</td>"
												. "<td style='width:70px;' class=\"user_column col_Fwd_To\">" . $FwdTo . "</td>"
												. "<!--<td style='width:70px;' class=\"user_column col_Remote_Office\">" . $remote . "</td>-->"
												. "<!--<td style='width:90px;' class=\"user_column col_Remote_Number\">" . $remote_number . "</td>-->"
												. "<td style='width:70px;' class=\"user_column col_Polycom_Phone_Services\">" . $integration . "</td>"
												. "<td style='width:90px;' class=\"user_column col_Custom_Contact_Directory\">" . $ccd . "</td>"
												. "</tr>";

											++$rowIndex;
										}
									} else {
										$deviceName = $users[$a]["deviceName"];
										$deviceType = $users[$a]["deviceType"];
										$portNumber = $users [$a] ["portNumber"];
										$macAddress = $users[$a]["macAddress"];
										$integration = $users[$a]["integration"];
										$ccd = $users[$a]["ccd"];
										if ($searchInCsv != "") {
											$isUnRegUserSasTester = "SAS";
										} else {
											$isUnRegUserSasTester = "No";
										}

										if (isset($_SESSION["permissions"]["modifyUsers"]) and $_SESSION["permissions"]["modifyUsers"] == "1") {
											$userIdVal = "<a href='#' class='userIdVal numberList' id='" . $userId . "' data-lastname='" . $users[$a]["LastName"] . "' data-firstname='" . $users[$a]["firstName"] . "' >" . $userId . "</a>";
										} else {
											$userIdVal = $userId;
										}
										echo "<tr id='broadsoft_user_$userId' class='broadsoft_users usersTableColor'>"
											. "<td style='width:15px;' class=\"user_column col_Selected\">"
											. "<input type='checkbox' id='users_selected_$userId' class='users_selected' name='usersSelected[]' CHECKED value='" . $userId . "'></td>"
											. "<td style='width:15px;' class=\"user_column col_Registered\">" . $phoneNumberStatus . "</td>"
											. "<td style='width:15px;' class=\"user_column col_Registered\">" . $isUnRegUserSasTester . "</td>
												<td style='width:15px;' class=\"user_column col_Fist_Name\">" . $firstName . "</td>
												<td style='width:15px;' class=\"user_column col_Last_Name\">" . $lastName . "</td>
											<td style='width:15px;' class=\"user_column col_User_Id\">" . $userIdVal . "</td>
											<td style='width:15px;' class=\"user_column col_Extension\">" . $extension . "</td>
											<td style='width:15px;' class=\"user_column col_Device_Name\">" . $deviceName . "</td>
											<td style='width:15px;' class=\"user_column col_Device_Type\">" . $deviceType . "</td>
<td style='width:15px;' class=\"user_column col_Device_Name\">" . $portNumber . "</td>
											<td style='width:15px;' class=\"user_column col_MAC_Address\">" . $macAddress . "</td>
											<!--<td style='width:15px;' class=\"user_column col_Department\">" . $department . "</td>-->
											<td style='width:15px;' class=\"user_column col_Service_Pack\">" . $servicePack . "</td>
											<!--<td style='width:15px;' class=\"user_column col_Expiration\">&nbsp;</td>-->
											<td style='width:15px;' class=\"user_column col_Type\">&nbsp;</td>
											<td style='width:15px;' class=\"user_column col_DnD\">" . $DnD . "</td>
											<td style='width:15px;' class=\"user_column col_Fwd\">" . $Fwd . "</td>
											<td style='width:15px;' class=\"user_column col_Fwd_To\">" . $FwdTo . "</td>
											<!--<td style='width:15px;' class=\"user_column col_Remote_Office\">" . $remote . "</td>-->
											<!--<td style='width:15px;' class=\"user_column col_Remote_Number\">" . $remote_number . "</td>-->
											<td style='width:15px;' class=\"user_column col_Polycom_Phone_Services\">" . $integration . "</td>
											<td style='width:15px;' class=\"user_column col_Custom_Contact_Directory\">" . $ccd . "</td></tr>";

										++$rowIndex;
									}
								}
							}*/
							?>
							</tbody>
						</table>

						<div id="users_selected_count">
						</div>
					</div>
					<?php
				}
				?>

			</div>
			

		</div>

	</div>

</form>
    </div>
    
    </div>
</div>
    <input type="hidden" name="loadCount" id="loadCount" value="" />
	<input type="hidden" name="checkAllActiveStatus" id="checkAllActiveStatus" value="" />
	<input type="hidden" name="resetAllStatus" id="resetAllStatus" value="" />
	<input type="hidden" name="applyActiveClicked" id="applyActiveClicked" value="0" />
	
	<!-- div style="clear:both; padding-bottom:15px;"></div-->
		<div id="filterUsersLoading" class="loading"><img src="/Express/images/ajax-loader.gif" style="margin: 15px 0"></div>
	<!-- div style="clear:both; padding-bottom:15px;"></div-->
<div style="clear:both; padding-top:15px;"></div>
<!-- User Filters Start -->
<script>
    /* Preset Filters */
    $(function () {

//     	var checkUserLength = function() {
//     		var userCount = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").length;
//     		var userCountChecked = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").filter(":checked").length;
//     		userCount ? $("#selectAllDiv").show() : $("#selectAllDiv").hide();
//     		userCountChecked ? $("#gwModsLabel").show() : $("#gwModsLabel").hide();
//     		var checkVal = userCount == userCountChecked ? true: false;
//     		$("#selectAllChk").prop("checked", checkVal);
//     		selectAllPermission = checkVal;
//     	}

        $(".presetToggleCheckbox").click(function () {
            var filter = $(this).data("filter");
            if ($(this).is(":checked")) {
                $("#" + filter + "_FilterOptions").show();
                $(this).closest('tr').addClass('active');
            } else {
                $("#" + filter + "_FilterOptions").hide();
                $(this).closest('tr').removeClass('active');
            }

            $("#filtersStatusTitle").html("Advanced Filters - Not Applied <span class='glyphicon glyphicon-chevron-down fIcon'></span>");

        });

        $("#filtersStatusTitle").click(function(){
			//$("#filtersDiv").slideToggle();
			$("#filtersDiv").toggle();
			$(".applyFilterBtn").show();
        });

        $(".filterOptionRadio").change(function () {

	        $("#filtersStatusTitle").html("Advanced Filters - Not Applied <span class='glyphicon glyphicon-chevron-down fIcon'></span>");

        });

        $(".filterOptionText").change(function () {

            $("#filtersStatusTitle").html("Advanced Filters - Not Applied <span class='glyphicon glyphicon-chevron-down fIcon'></span>");

        });
    });
</script>


<script>

    var usersFromUserModule = <?php echo isset($usersFromUserModule) ? json_encode($usersFromUserModule) : json_encode(array()); ?>;
    var filtersAreDisabled = false;
    var resetFilters = false;

    var usersBannerDetailedDefaultTitle = $('#usersBanner').html();

    $(function () {

        var filter_set_id = 0;
        //$(".triggerColumn").trigger("click");

        //Add New Filter
        $("#addNewFilterSet").click(function () {

            filter_set_id++;
            $('<div id="filter_set_' + filter_set_id + '" class="user_filter_set"></div>').insertBefore("#addNewFilterSetButtonDiv");
            $("#filter_set_" + filter_set_id).loadTemplate($("#filterSetTemplate"),
                {
                    filter_set_id_id: 'filter_set_id_' + filter_set_id,
                    filter_set_id_value: 0,
                    filter_set_name_id: 'filter_set_name_' + filter_set_id,
                    filter_set_name_value: 'Filter ' + filter_set_id,
                    filterUsersValuesDiv_id: 'filterUsersValuesDiv_' + filter_set_id,
                    applyFilterBtn_id: 'applyFilterBtn_' + filter_set_id,
                    css_class: "user_filters active",
                    hide_enable: "display: none;",
                    hide_disable: "",
                    filterUsersSetContainer_id: 'filterUsersSetContainer_' + filter_set_id,
                    filter_delete_id: "deleteUserFilterBtn_" + filter_set_id,
                    filter_enable_id: "enableUserFilterBtn_" + filter_set_id,
                    filter_disable_id: "disableUserFilterBtn_" + filter_set_id,
                    data_id: filter_set_id
                }
            );

            apply_jui_filter(filter_set_id);

        });

        //Apply Filters
        $(".applyFilterBtn").click(function () {
            if (!filtersAreDisabled) {
                $("#filtersAreEnabled").val("1");
                if (typeof globalFiltersAreEnabled !== 'undefined') {
                    globalFiltersAreEnabled = 1;
                }
            }
            apply_active_filters();
        });

        //$("#clearAllFiltersBtn").click(function () {
        var disableAllFilter = function() {
            $(".filterCheckStatus:checked").parent("label").parent("td").siblings("td").find(".expandFilterClass").trigger("click");
        	$(".filterCheckStatus").prop("checked", false);
        	$("#defaultUserFiltersDiv").find(".filterH").val('');
        	$("#defaultUserFiltersDiv tr").addClass("disableRow");
        	$("#defaultUserFiltersDiv tr").removeClass("enableRow");
        	$("#defaultUserFiltersDiv tr").removeClass("active");
            $("#filtersAreEnabled").val("0");
            
            if (typeof globalFiltersAreEnabled !== 'undefined') {
                globalFiltersAreEnabled = 0;
            }

            filtersAreDisabled = true;
            if (typeof globalfiltersAreDisabled !== 'undefined') {
                globalfiltersAreDisabled = true;
            }

            $(this).hide();
            $("#enableAllFiltersBtn").show();
            //$(".filterDisableOverlay").show();
           // $(".applyFilterBtn").prop("disabled", true);
            //$(".filterOptionRadio").prop("disabled", true);
            $(".filterOptionRadio").css("pointer-events", "none");
            $(".filterOptionRadio").parent("label").css("pointer-events", "none");
            //apply_active_filters();
            //$(".filterCheckStatus").prop("disabled", true);
            $(".filterCheckStatus").css("pointer-events", "none");
            $(".filterCheckStatus").addClass("penone");
            $(".filterCheckStatus").removeClass("peauto");
            //$(".FilterOptions").find("select").prop("disabled", true);
            $(".FilterOptions").find("select").css("pointer-events", "none");
            //$(".FilterOptions").find(".filterOptionText").prop("disabled", true);
            $(".FilterOptions").find(".filterOptionText").css("pointer-events", "none");
            $(".FilterOptions").hide();
            //$(".resetF").prop("disabled", true);
            $(".filterOptionText").val('');
            $("#filtersStatusTitle").html("Advanced Filters - Not Applied <span class='glyphicon glyphicon-chevron-down fIcon'></span>");

            //$("#usersBanner").html("Users List / Group-wide Users Modify<sup class='filtersApplied'>(Advanced Filters Applied)</sup>");
            $("#usersBanner").html("<div class='filterMainHeadDiv'>Users List / Group-wide Users Modify </div><div class='filterApplyHead'><img src='/Express/images/icons/filter_icon_lrg.png' /><span>Advanced Filters Applied<span></div>");
            var checkAllActiveStatusVal = $("#checkAllActiveStatus").val();
        	if(checkAllActiveStatusVal == "all"){
        		 $("#showAllFilterBtn").trigger("click");
        	}else{
        		 $("#showActiveFilterBtn").trigger("click");
        	}
        	
        	
        	//commneted for EX-679
        	$(".filterCheckStatus").each(function(){ 
        		var el = $(this);


        		//commented for EX-679
        		/*if(el.closest("tr").find("td").find(".filterOptionRadio").is(":checked")){
					el.prop("checked", true);
				}*/
				
				el.prop("checked", false);
				var dataVal = el.attr("data-filterd");
				el.parent("label").prev("input[type=hidden]").val(dataVal);
        	});

        	  
        	
        	$("#usersBanner").html(usersBannerDetailedDefaultTitle);
        	$("#applyActiveClicked").val('1');
        	
        	var checkAllActiveStatusVal = $("#checkAllActiveStatus").val();
        	
			if($(".active .enableRow").length == 0){
				if(checkAllActiveStatusVal == "all"){
					$("#clearAllFiltersBtn").parent("label").show();
	     			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
	        	}else{
	        		$("#clearAllFiltersBtn").parent("label").hide();
	     			$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
	        	}
	    			
        	}else{
        		$("#clearAllFiltersBtn").parent("label").show();
     			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
        	}  
        };

        //$("#enableAllFiltersBtn").click(function () {
		var enableAllFilter = function() {
			//$(".filterCheckStatus").prop("checked", true);
            $("#filtersAreEnabled").val("1");
            if (typeof globalFiltersAreEnabled !== 'undefined') {
                globalFiltersAreEnabled = 1;
            }

            filtersAreDisabled = false;
            if (typeof globalfiltersAreDisabled !== 'undefined') {
                globalfiltersAreDisabled = false;
            }

            $(this).hide();
            $("#clearAllFiltersBtn").show();
            $(".filterDisableOverlay").hide();
            $(".applyFilterBtn").prop("disabled", false);
            $(".filterOptionRadio").prop("disabled", false);
            $(".filterOptionRadio").css("pointer-events", "auto");
            //apply_active_filters();
            $(".FilterOptions").find(".resetF").css("pointer-events", "auto");
            $(".FilterOptions").find("select").css("pointer-events", "auto");
            $(".FilterOptions").find(".filterOptionText").prop("disabled", false);
            $(".FilterOptions").find(".filterOptionText").css("pointer-events", "auto");
            $(".resetF").prop("disabled", false);
            $(".filterOptionText").val('');
            $("#filtersStatusTitle").html("Advanced Filters - Not Applied <span class='glyphicon glyphicon-chevron-down fIcon'></span>");
            $("#usersBanner").html(usersBannerDetailedDefaultTitle);
            //$("#checkAllActiveStatus").val('active');
            $(".filterCheckStatus").each(function(){ 
				var el = $(this);
				if(el.closest("tr").find("td").find(".filterOptionRadio").is(":checked")){
					el.closest("tr").find("td").find(".filterCheckStatus").css("pointer-events", "auto");
					el.closest("tr").find("td").find(".filterCheckStatus").prop("disabled", false);
					el.closest("tr").addClass("enableRow");
					el.closest("tr").addClass("active");
					el.closest("tr").removeClass("disableRow");
					var filterHVal = el.parent("td").find(".filterH").attr("data-filter");
				    el.parent("td").find(".filterH").val(filterHVal);
				    el.prop("checked", true);
				}else{
					el.prop("checked", false);
					el.closest("tr").addClass("disableRow");
					el.closest("tr").removeClass("enableRow");
				}
				el.closest("tr").find("td").find(".filterCheckStatus").addClass("peauto");
				el.closest("tr").find("td").find(".filterCheckStatus").removeClass("penone");
				el.parent("label").prev("input[type=hidden]").val('');
				
            });
            var checkAllActiveStatusVal = $("#checkAllActiveStatus").val();
        	if(checkAllActiveStatusVal == "all"){
        		 //$("#showAllFilterBtn").trigger("click");
        		 if($(".active.enableRow").length > 0){
        		 	//$(".disableRow").hide();
        		 	$("#clearAllFiltersBtn").parent("label").show();
	     			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
        		}
        	}else{
        		 $("#showActiveFilterBtn").trigger("click");
        		 if($(".filterOptionRadio:checked").length > 0){ 
	    			$("#clearAllFiltersBtn").parent("label").show();
	     			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
	        	}else{
	        		$("#clearAllFiltersBtn").parent("label").hide();
	     			$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
	        	}
        	}

        	  
        	
        	$("#applyActiveClicked").val('1');
        	
		};
		
		var checkedAllFilterStatus = function(){
			if($('#clearAllFiltersBtn').is(':checked')){
				enableAllFilter();
			}else{
				disableAllFilter();
			}
		};
		
		$('#clearAllFiltersBtn').click(function(){
			checkedAllFilterStatus();
		});
			
        

        $("#resetAllFilters").click(function() {
            if(confirm("All user filters will be reset. Proceed?")) {
                $(".user_filter_set input[type=checkbox]").prop("checked", false);
                $(".user_filter_set input[type=radio]").prop("checked", false);
                $(".user_filter_set input[type=text]").val('');
                $(".user_filter_set input[type=number]").val('');
                $(".user_filter_set option").prop("selected", false);
                $(".user_filter_set").find(".filterH").val('');
                $(".user_filter_set").find("tr").addClass('disableRow');
                $(".user_filter_set").find("tr").removeClass('active');
                $(".user_filter_set").find("tr").removeClass('enableRow');
                //$('form#userFiltersForm').reset();
                //
                $(".user_filter_set tr.active").removeClass('active');
                $(".FilterOptions").hide();

                $("#filtersAreEnabled").val('1');
                $(".filterOptionRadio").css("pointer-events", "auto");
                //$(".filterCheckStatus").prop("disabled", true);
                $(".filterCheckStatus").addClass("penone");
                $(".filterCheckStatus").prop("checked", false);

                resetFilters = true;
				$("#showActiveFilterBtn").css("float", "right");
				$("#loadCount").val('1');
				$("#resetAllStatus").val('1');

                $("#filtersStatusTitle").html("Advanced Filters - Not Applied <span class='glyphicon glyphicon-chevron-down fIcon'></span>");
                $("#applyActiveClicked").val('1');
                apply_active_filters();

                $("#usersBanner").html(usersBannerDetailedDefaultTitle);
               
                //checkedAllFilterStatus();
                if($('#clearAllFiltersBtn').is(':checked')){
                	$('#clearAllFiltersBtn').trigger("click");
                }
                var checkAllActiveStatusVal = $("#checkAllActiveStatus").val();
            	if(checkAllActiveStatusVal == "all"){
            		 $("#showAllFilterBtn").trigger("click");
            	}else{
            		 $("#showActiveFilterBtn").trigger("click");
            	}
                //$("#showActiveFilterBtn").trigger("click");
                $("#applyActiveClicked").val('1');
                $("#selectAllChk").prop("checked", false);
                $(".checkUserListBox").prop("checked", false);
                $("#deleteUsers").hide();
                $("#expressSheetUsers").hide();
//         			$("#clearAllFiltersBtn").parent("label").hide();
//          			$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
            };
            
        });


		<?php
		if($advancedUserFilters == "true") {

		$saved_user_filters = getAllUserFilters($_SESSION["adminId"]);
		foreach ($saved_user_filters as $saved_user_filter) {
		?>
        filter_set_id++;
        $('<div id="filter_set_' + filter_set_id + '" class="user_filter_set <?php echo $saved_user_filter['inactive'] ? 'inactive' : 'active'; ?>"></div>').insertBefore("#addNewFilterSetButtonDiv");
        $("#filter_set_" + filter_set_id).loadTemplate($("#filterSetTemplate"),
            {
                filter_set_id_id: 'filter_set_id_' + filter_set_id,
                filter_set_id_value: '<?php echo $saved_user_filter['id'] ?>',
                filter_set_name_id: 'filter_set_name_' + filter_set_id,
                filter_set_name_value: '<?php echo $saved_user_filter['filter_name'] ?>',
                filterUsersValuesDiv_id: 'filterUsersValuesDiv_' + filter_set_id,
                applyFilterBtn_id: 'applyFilterBtn_' + filter_set_id,
                css_class: "user_filters <?php echo $saved_user_filter['inactive'] ? 'inactive' : 'active'; ?>",
                hide_enable: "<?php echo $saved_user_filter['inactive'] ? '' : 'display: none;'; ?>",
                hide_disable: "<?php echo $saved_user_filter['inactive'] ? 'display: none;' : ''; ?>",
                filterUsersSetContainer_id: 'filterUsersSetContainer_' + filter_set_id,
                filter_delete_id: "deleteUserFilterBtn_" + filter_set_id,
                filter_enable_id: "enableUserFilterBtn_" + filter_set_id,
                filter_disable_id: "disableUserFilterBtn_" + filter_set_id,
                data_id: filter_set_id
            }
        );

        apply_jui_filter(filter_set_id);

        $("#filterUsersValuesDiv_" + filter_set_id).jui_filter_rules("setRules", jQuery.parseJSON('<?php echo $saved_user_filter['filter']; ?>'));
		<?php
		}
		}
		?>

        $("#allUsers").dataTable({
            "paging": false,
            "ordering": false,
            "info": false,
            "filter": false,
        });

    });

    function apply_jui_filter(filter_id) {

        $("#filterUsersValuesDiv_" + filter_id).jui_filter_rules({

            bootstrap_version: "3",

            filters: [
                {
                    filterName: "Registred", "filterType": "text", field: "registred", filterLabel: "Registred",
                    excluded_operators: ["in", "not_in",
                        "less", "less_or_equal",
                        "greater", "greater_or_equal",
                        "is_null", "is_not_null",
                        "begins_with", "not_begins_with", "contains", "not_contains", "ends_with", "not_ends_with"
                    ],
                    filter_interface: [
                        {
                            filter_element: "select"
                        }
                    ],
                    lookup_values: [
                        {lk_option: "No", lk_value: "No"},
                        {lk_option: "Yes", lk_value: "Yes", lk_selected: "yes"}
                    ]
                },
                {
                    filterName: "Activated", "filterType": "text", field: "activated", filterLabel: "Activated",
                    excluded_operators: ["in", "not_in",
                        "less", "less_or_equal",
                        "greater", "greater_or_equal",
                        "is_null", "is_not_null",
                        "begins_with", "not_begins_with", "contains", "not_contains", "ends_with", "not_ends_with"
                    ],
                    filter_interface: [
                        {
                            filter_element: "select"
                        }
                    ],
                    lookup_values: [
                        {lk_option: "No", lk_value: "No"},
                        {lk_option: "Yes", lk_value: "Yes", lk_selected: "yes"}
                    ]
                },
                {
                    filterName: "DND", "filterType": "text", field: "dnd", filterLabel: "DND",
                    excluded_operators: ["in", "not_in",
                        "less", "less_or_equal",
                        "greater", "greater_or_equal",
                        "is_null", "is_not_null",
                        "begins_with", "not_begins_with", "contains", "not_contains", "ends_with", "not_ends_with"
                    ],
                    filter_interface: [
                        {
                            filter_element: "select"
                        }
                    ],
                    lookup_values: [
                        {lk_option: "Off", lk_value: "Off"},
                        {lk_option: "On", lk_value: "On", lk_selected: "yes"}
                    ]
                }
            ],

            onValidationError: function (event, data) {
                alert(data["err_description"] + ' (' + data["err_code"] + ')');
                if (data.hasOwnProperty("elem_filter")) {
                    data.elem_filter.focus();
                }
            }

        });

        //Delete Filter
        $("#deleteUserFilterBtn_" + filter_id).click(function () {
            deleteFilter(this);
        });

        //Enable/Disable Filter
        $("#enableUserFilterBtn_" + filter_id).click(function () {
            toggleFilter(this, 0);
        });
        $("#disableUserFilterBtn_" + filter_id).click(function () {
            toggleFilter(this, 1);
        });

    }

    //Toggle apply filter button
    function toggle_apply_filter_btn() {

        if ($(".user_filters.active").length > 0) {
            $("#applyFilterBtn").show();
        } else {
            if ($(".user_filters").length > 0) {
                $("#applyFilterBtn").prop('disabled', true);
            } else {
                $("#applyFilterBtn").hide();
            }
        }

    }

    function checkUserLength(){
		var userCount = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").length;
		var userCountChecked = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").filter(":checked").length;
		userCount ? $("#selectAllDiv").show() : $("#selectAllDiv").hide();
		userCountChecked ? $("#gwModsLabel").show() : $("#gwModsLabel").hide();
		if(userCount > 0){
			var checkVal = userCount == userCountChecked ? true: false;
			$("#selectAllDiv").show();
		}else{
			$("#selectAllDiv").hide();
		}
		$("#selectAllChk").prop("checked", checkVal);
		selectAllPermission = checkVal;
	}
	
    //Apply Active Filters
    function apply_active_filters() {
        var all_filters = [];
        var filter_ids = [];

        $(document).find("#selectAllDiv").hide();
        $(document).find("#downloadCSV").hide();
        $("#deleteUsers").hide();
        $("#expressSheetUsers").hide(); //
        $("#gwModsLabel").hide();

        var filtersAreEnabled = $("#filtersAreEnabled").val();
        $("#usersBanner").html(usersBannerDetailedDefaultTitle);
        var loadCount =  $("#loadCount").val();
        if(loadCount == 0){
                $("#userFiltersForm").hide();
                $("#filterUsersLoading").show();
        }

        //code for the apply active button when nothing has been changed.... it will return false, if we do not get anything changed
        //var applyActiveClickedVal = $("#applyActiveClicked").val();
        //if(applyActiveClickedVal == 0){return false;}
		
        var dataToSend = {};
        
        if (filtersAreEnabled == "1") { 
            $(".user_filters.active").each(function () {
                var filter_id = $(this).data('id');
                var a_rules = $("#filterUsersValuesDiv_" + filter_id).jui_filter_rules("getRules", 0, []);

                if (a_rules.length === 0) {
                    //alert("No rules defined...");
                    $("#filter_set_" + filter_id).addClass("error");
                    all_filters = null;
                    return false;
                }
                $("#filter_set_" + filter_id).removeClass("error");
                filter_ids.push(filter_id);

                var filter = {};
                filter['rules'] = a_rules;
                filter['id'] = $("#filter_set_id_" + filter_id).val();
                filter['name'] = $("#filter_set_name_" + filter_id).val();
                filter['filter_id'] = filter_id;

                all_filters.push(filter);
            });

            var preset_filters = $("#userFiltersForm").serialize();

            dataToSend = {
                all_filters: all_filters,
                preset_filters: preset_filters,
                dataType : "JSON",
                fileType : "user",
                department : "<?php echo $department;?>",
            }

        } else {
        	var preset_filters = $("#userFiltersForm").serialize();
        	 dataToSend = {
                     preset_filters: preset_filters,
                    	dataType : "JSON",
                    	fileType : "user",
                    	department : "<?php echo $department;?>",
                 }
        }

        console.log(dataToSend);//

        $("#filterUsersLoading").show();
        $("#usersTableDiv").hide();
        $("#usersTableDiv1").hide();
        
        

	$(".users_selected").prop("disabled", true);

        $(".users_selected").prop("disabled", false);

        $(".users_selected").prop('checked', false);

        $('.userFilterHtml').hide();
        $('.userFilterHtml').html('');

        $.ajax({
            type: 'POST',
            url: "expressSheets/userFilters/filter_users.php",
            data: dataToSend,
           // dataType: "JSON",
            success: function (data) {
				var checkFilterApplied = "0";
				if (data.slice(0, 1) == "1")
				{
					checkFilterApplied = 1;
				}
				else
				{
					checkFilterApplied = 0;
				}
				data = data.slice(1);
				data = data.split("RegistrationDetails");
                $(".userFilterHtml").html(data[0]);
                //$(".viewDetailRegistration").html(data[1]);
                $("#filterUsersLoading").hide();
                //$(".triggerColumn").trigger("click");
                $("#usersTableDiv").show();
                $("#usersTableDiv1").show();
                $(".applyFilterBtn").show();
                $('#usersDetailedListTable_wrapper').show();

				//changeTitleStatusApplied();
                $("#usersBanner").html(usersBannerDetailedDefaultTitle);
				if(checkFilterApplied == "1"){
					 //$("#usersBanner").html("Users List / Group-wide Users Modify<sup class='filtersApplied'>(Advanced Filters Applied)</sup>");
					 $("#usersBanner").html("<div class='filterMainHeadDiv'>Users List / Group-wide Users Modify </div><div class='filterApplyHead'><img src='/Express/images/icons/filter_icon_lrg.png' /><span>Advanced Filters Applied<span></div>");
                                         $("#filtersStatusTitle").html("Advanced Filters - Applied <span class='glyphicon glyphicon-chevron-down fIcon'></span>");
				}
				
                if (typeof updateSelectedUsersCount !== 'undefined' && $.isFunction(updateSelectedUsersCount)) {
                    updateSelectedUsersCount();
                }
                //$(".triggerColumn").trigger("click");

                /*if($(".active").length == 0){
                	$("#filtersStatusTitle").html("Filters - Not Applied <span class='glyphicon glyphicon-chevron-down fIcon'></span>");
                	$("#usersBanner").html(usersBannerDetailedDefaultTitle);
                }*/

                $("#loadCount").val('1');
                var loadCount =  $("#loadCount").val();
                var resetAllStatusVal =  $("#resetAllStatus").val();
                if(resetAllStatusVal == "1"){
                    $("#usersBanner").html(usersBannerDetailedDefaultTitle);
                	$("#filtersStatusTitle").html("Advanced Filters - Not Applied <span class='glyphicon glyphicon-chevron-down fIcon'></span>");
                }
                if($(".filterCheckStatus:checked").length == $(".penone").length){
                    $("#usersBanner").html(usersBannerDetailedDefaultTitle);
                	$("#filtersStatusTitle").html("Advanced Filters - Not Applied <span class='glyphicon glyphicon-chevron-down fIcon'></span>");
                }
                $("#resetAllStatus").val('0');
               // console.log(loadCount);
                $("#userFiltersForm").show();
                $("#filterUsersLoading").hide();
               // alert(loadCount);
                $("#applyActiveClicked").val('0');
                $("#selectAllChk").prop("checked", false);
                $(".checkUserListBox").prop("checked", false);
                $("#deleteUsers").hide();
                $("#expressSheetUsers").hide(); //
                if($(".checkUserListBox:checked").length > 0){
	                $(document).find("#selectAllDiv").show();
                }
				
                //setRegisterTableSorter();

				//checkUserLength();
                eventAfterLoad();

                showSelectedView();
                $("#loading2").hide();
                console.log('completed apply');


                if(usersDetailedListTable_DT) {
                    $('.usersDetailedListTable_bottom_bar .retrieving_info').html('');
                    //toggleAllDetailedUsers(false);
                    //console.log("usersDetailedListTable_DT");
                    usersDetailedListTable_DT.clear().draw();
                    $("#usersTable1 tbody tr").each(function () {
                        $(this).remove();
                        usersDetailedListTable_DT.row.add($(this)).draw();
                    });
                    if(usersDetailedListTable_DT.page.info().recordsTotal) {
                        $(document).find("#downloadCSV").show();
                    } else {
						$('#usersDetailedListTable_wrapper').hide();
						$('.userFilterHtml').html('<h4 class="text-center">No Users Found.</h4>');
                        $('.userFilterHtml').show();
                    }
                }

                /*
                if(allUsersRegistration_DT) {
                    $('.usersDetailedListTable_bottom_bar .retrieving_info').html('');
                    //console.log("allUsersRegistration_DT");
                    allUsersRegistration_DT.clear().draw();
                    $("#allUsersRegistration tbody tr").each(function () {
                        $(this).remove();
                        allUsersRegistration_DT.row.add($(this)).draw();
                    });
                    console.log(allUsersRegistration_DT.rows().count());
                    if(allUsersRegistration_DT.rows().count()) {
                    } else {
                        $('#allUsersRegistration_wrapper').hide();
                    }
                }
                */

            }
        });

    }
    
    //apply_active_filters();
	
    function deleteFilter(deleteBtnObj) {

        var filter_id = $(deleteBtnObj).data('id');
        if (confirm("Are you sure?")) {
            var save_id = $(deleteBtnObj).data('save-id');
            if (save_id > 0) {

                $.ajax({
                    type: 'POST',
                    url: "expressSheets/userFilters/filter_users.php",
                    data: {
                        delete_filter: save_id,
                        fileType : "user"
                    },
                    dataType: "JSON",
                    success: function (data) {

                        if (data.success) {
                            $("#filterUsersValuesDiv_" + filter_id).removeClass('active');
                            $("#filter_set_" + filter_id).slideUp(500, function () {
                                $(this).remove();
                                toggle_apply_filter_btn();
                                apply_active_filters();
                            });
                        } else {
                            alert("Filter was not deleted.");
                        }

                    }
                });

            } else {
                $("#filter_set_" + filter_id).slideUp(500, function () {
                    $(this).remove();
                });
                toggle_apply_filter_btn();
                apply_active_filters();
            }
        }

    }

    function toggleFilter(toggleBtnObj, inactive) {

        var filter_id = $(toggleBtnObj).data('id');
        //if(confirm("Are you sure?")) {
        var save_id = $(toggleBtnObj).data('save-id');
        if (save_id > 0) {

            $.ajax({
                type: 'POST',
                url: "expressSheets/userFilters/filter_users.php",
                data: {
                    toggle_filter: save_id,
                    inactive: inactive,
                    fileType : "user"
                },
                dataType: "JSON",
                success: function (data) {

                    if (data.success) {

                        $(toggleBtnObj).hide();

                        if (inactive) {
                            $("#filter_set_" + filter_id).removeClass('success');
                            $("#filter_set_" + filter_id).addClass('inactive');
                            $("#filterUsersValuesDiv_" + filter_id).removeClass('active');
                            $("#filterUsersValuesDiv_" + filter_id).addClass('inactive');
                            $("#filterUsersValuesDiv_" + filter_id).slideUp(500, function () {
                                $(this).hide();
                                $("#enableUserFilterBtn_" + filter_id).show();
                            });
                        } else {
                            $("#filter_set_" + filter_id).removeClass('inactive');
                            $("#filterUsersValuesDiv_" + filter_id).addClass('active');
                            $("#filterUsersValuesDiv_" + filter_id).removeClass('inactive');
                            $("#filterUsersValuesDiv_" + filter_id).slideDown(500, function () {
                                $(this).show();
                                $("#disableUserFilterBtn_" + filter_id).show();
                            });
                        }

                        toggle_apply_filter_btn();
                        apply_active_filters();

                    } else {
                        alert("Filter was not disbaled.");
                    }

                }
            });

        } else {

            $(toggleBtnObj).hide();

            if (inactive) {
                $("#filter_set_" + filter_id).removeClass('success');
                $("#filter_set_" + filter_id).addClass('inactive');
                $("#filterUsersValuesDiv_" + filter_id).removeClass('active');
                $("#filterUsersValuesDiv_" + filter_id).addClass('inactive');
                $("#filterUsersValuesDiv_" + filter_id).slideUp(500, function () {
                    $(this).hide();
                    $("#enableUserFilterBtn_" + filter_id).show();
                });
            } else {
                $("#filter_set_" + filter_id).removeClass('inactive');
                $("#filterUsersValuesDiv_" + filter_id).addClass('active');
                $("#filterUsersValuesDiv_" + filter_id).removeClass('inactive');
                $("#filterUsersValuesDiv_" + filter_id).slideDown(500, function () {
                    $(this).show();
                    $("#disableUserFilterBtn_" + filter_id).show();
                });
            }

            toggle_apply_filter_btn();
            apply_active_filters();

        }
        //}

    }

</script>

<!-- Code for redesign userFilter .filterCheckStatus-->   
 <script>
 $(document).ready(function(){

	 $("#defaultUserFiltersDiv").hide();
	 $("#filtersDiv").hide();
	 $("#resetAllFilters").show();
	 $("#showActiveFilterBtn").css("float", "right");
	 $("#loadCount").val(0);
	 $("#clearAllFiltersBtn").parent("label").hide();
	 $("#clearAllFiltersBtn").parent("label").siblings("label").hide();
	 
	 $("#filtersStatusTitle").click(function(){
		if($("#filtersDiv").is(":visible")){
			$("#filtersStatusTitle span").removeClass("glyphicon-chevron-right");
			$("#filtersStatusTitle span").addClass("glyphicon-chevron-down");
		}else{
			$("#filtersStatusTitle span").removeClass("glyphicon-chevron-down");
			$("#filtersStatusTitle span").addClass("glyphicon-chevron-right");
		}
	 });

	 var changeTitleStatusNotApplied = function(){

         $("#usersBanner").html(usersBannerDetailedDefaultTitle);
		 //$("#usersBanner").append("<span class='filtersApplied'>(Filters Not Applied)</span>");
		 $("#filtersStatusTitle").html("Advanced Filters - Not Applied <span class='glyphicon glyphicon-chevron-down fIcon'></span>");
	 }

	 var changeTitleStatusApplied = function(){

         $("#usersBanner").html(usersBannerDetailedDefaultTitle);
         $("#usersBanner").html("Users List / Group-wide Users Modify<sup class='filtersApplied'>(Advanced Filters Applied)</sup>");
		 $("#filtersStatusTitle").html("Advanced Filters - Applied");
	 }
	 
	//Active/ Inactive and expand data from user Filter particular record
    var filterExpand = function(e){
    	e.closest("tr").toggleClass("active");
    	e.parent("td").find("div.FilterOptions").toggle();
    };

    // function for active and expand record
    $('.expandFilterClass').click(function(){
    	var el = $(this);
    	el.css("cursor", "pointer");
    	el.closest("tr").addClass("active");
    	if(el.siblings('.FilterOptions').is(":visible")){
    		el.siblings('.FilterOptions').hide();
    	}else{
    		el.siblings('.FilterOptions').show();
    	} 
    });

    var showActiveFilters = function(){ 
		$(".disableRow").hide();
		$(".active.enableRow").show();		
		$("#showActiveFilterBtn").hide();
		$("#showAllFilterBtn").show();
		$(".enableRow").addClass("active");
		$(".filterh1title").text("Active Filters");		
		$("#checkAllActiveStatus").val("active");
		//$(".enableRow").find("td").find(".FilterOptions").show();
		if($(".active.enableRow").length > 0){
			$("#defaultUserFiltersDiv").show();
			
		}else{
			$("#defaultUserFiltersDiv").hide();
		}
		$(".applyFilterBtn").show();

		if($(".enableRow").length > 0){
			$("#clearAllFiltersBtn").parent("label").show();
			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
  	 	}else{
   			$("#clearAllFiltersBtn").parent("label").hide();
			$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
   		} 
		
    };
    
    var showAllFilters = function(){
		$(".enableRow.active").show();
		$(".disableRow").show();
		$("#showActiveFilterBtn").show();
		//$(".FilterOptions").hide();
		$("#showAllFilterBtn").hide();
		$(".filterh1title").text("All Filters");
		$("#checkAllActiveStatus").val("all");
		//$(".disableRow").find("td").find(".FilterOptions").hide();
		//$(".enableRow").find("td").find(".FilterOptions").hide();
		//$(".active").find("td").find(".FilterOptions").show();
		$("#defaultUserFiltersDiv").show();
		$("#clearAllFiltersBtn").prop("checked", false);
// 		if($(".filterOptionRadio:checked").length > 0){
// 			$("#clearAllFiltersBtn").parent("label").show();
// 			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
//   	 	}else{
//    		$("#clearAllFiltersBtn").parent("label").hide();
// 			$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
//    		} 
		$("#clearAllFiltersBtn").parent("label").show();
		$("#clearAllFiltersBtn").parent("label").siblings("label").show();
    };

     //function for radio button enable then particular checkbox is enable 
     var chkboxEanable = function(e){ 
         	e.closest("tr").addClass("enableRow");
         	e.closest("tr").removeClass("disableRow");
         	e.closest("tr").addClass("active");
         	e.closest("tr").find("td").find(".filterCheckStatus").prop("checked", true);
         	/*if(e.closest("tr").find(".filterCheckStatus").is(":checked")){	
         		e.closest("tr").find("td").find(".filterCheckStatus").css("pointer-events", "auto");
         		e.closest("tr").find("td").find(".filterCheckStatus").addClass("peauto");
         		e.closest("tr").find("td").find(".filterCheckStatus").prop("disabled", false);
         	}else{
         		e.closest("tr").find("td").find(".filterCheckStatus").css("pointer-events", "none");
         		e.closest("tr").find("td").find(".filterCheckStatus").addClass("penone");
         	}*/
         	e.closest("tr").find("td").find(".filterCheckStatus").css("pointer-events", "auto");
         	e.closest("tr").find("td").find(".filterCheckStatus").addClass("peauto");
         	e.closest("tr").find("td").find(".filterCheckStatus").removeClass("penone");
         	
         	var activeFilters = e.closest("tr").find(".filterH").attr("data-filter");
 			e.closest("tr").find(".filterH").val(activeFilters);
 			e.closest("tr").find(".filterCheckStatus").parent("label").prev("input[type=hidden]").val("");
 			if($(".filterOptionRadio:checked").length > 0){
				$(".applyFilterBtn").prop("disabled", false);
			}else{
				$(".applyFilterBtn").prop("disabled", true);
			}
 			changeTitleStatusNotApplied();
 			$("#applyActiveClicked").val('1');
 			if($(".filterOptionRadio:checked").length > 0){
 				$("#clearAllFiltersBtn").parent("label").show();
 	 			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
 	    	}else{
 	    		$("#clearAllFiltersBtn").parent("label").hide();
 	 			$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
 	    	}  

 			if($(".filterCheckStatus").length == $(".peauto").length && $(".disableRow.active").length == 0){
				$("#clearAllFiltersBtn").prop("checked", true);
    		}else{
    			$("#clearAllFiltersBtn").prop("checked", false);
    		}
     };

  // function for radio button enable then particular checkbox is enable 
     var chkboxClickEnable = function(e){
   	  //e.css("pointer-events", "none");
   	  e.addClass("penone");
   	  //e.prop("checked", false);
   	  e.closest("tr").removeClass("disableCheckbox");
   	  //e.closest("tr").removeClass("disableRow");
   	  e.closest("tr").addClass("active");
   	  //e.closest("tr").addClass("enableRow");
   	  e.closest("tr").find("td").find(".filterOptionRadio").css("pointer-events", "auto");
   	  e.closest("tr").find("td").find(".radio-inline").find("label").css("pointer-events", "auto");
   	  e.closest("tr").find("td").find(".FilterOptions").find("select").css("pointer-events", "auto");
   	  e.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").css("pointer-events", "auto");
   	  e.closest("tr").find("td").find(".FilterOptions").show();
          //e.closest("tr").find("td").find(".FilterOptions").find(".resetF").css("pointer-events", "auto"); // Code Added
      if(e.closest("tr").find("td").find(".filterOptionRadio").is(":checked")){
	      var filterHVal = e.parent("label").parent("td").find(".filterH").attr("data-filter");
	      e.parent("label").parent("td").find(".filterH").val(filterHVal);
	      e.closest("tr").addClass("enableRow active");
      }else{
    	  e.parent("label").parent("td").find(".filterH").val('');
    	  e.closest("tr").addClass("disableRow");
      }
      
	  e.parent("label").prev("input[type=hidden]").val("");
	  e.addClass("peauto");
	  e.removeClass("penone");
	  
   	  changeTitleStatusNotApplied();
   	  var checkAllActiveStatusVal = $("#checkAllActiveStatus").val();
       	if(checkAllActiveStatusVal == "all"){
       		showAllFilters();
       	}else{
       		showActiveFilters();
       	}
       	//$(".filterCheckStatus:checked").parent("label").parent("td").siblings("td").find(".expandFilterClass").trigger("click");
       $("#applyActiveClicked").val('1');
		 
//    		if($(".filterOptionRadio:checked").length > 0){
// 			$("#clearAllFiltersBtn").parent("label").show();
// 			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
// 	   	}else{
// 	   		$("#clearAllFiltersBtn").parent("label").hide();
// 				$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
// 	   	}  
      	

     };

     // function for radio button enable then particular checkbox is enable 
      var chkboxDisable = function(e){	
    	  //e.css("pointer-events", "none");
    	  e.removeClass("peauto");
    	  e.addClass("penone");
    	  e.closest("tr").addClass("disableCheckbox");
    	  e.closest("tr").addClass("disableRow");
    	  e.closest("tr").removeClass("active");
    	  e.closest("tr").removeClass("enableRow");
    	  e.closest("tr").find("td").find(".filterH").val('');
    	  e.closest("tr").find("td").find(".filterOptionRadio").css("pointer-events", "none");
    	  e.closest("tr").find("td").find(".radio-inline").find("label").css("pointer-events", "none");
    	  e.closest("tr").find("td").find(".FilterOptions").hide();
    	  //e.closest("tr").find("td").find(".FilterOptions").find("select").prop("disabled", true);
    	  e.closest("tr").find("td").find(".FilterOptions").find("select").css("pointer-events", "none");
    	  //e.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").prop("disabled", true);
    	  e.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").css("pointer-events", "none");
    	  e.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").val('');
    	  //e.closest("tr").find("td").find(".FilterOptions").find(".resetF").css("pointer-events", "none");

    	  var dataVal = e.attr("data-filterd");
		  e.parent("label").prev("input[type=hidden]").val(dataVal);
    	  changeTitleStatusNotApplied();
    	  var checkAllActiveStatusVal = $("#checkAllActiveStatus").val();
        	if(checkAllActiveStatusVal == "all"){
        		showAllFilters();
        	}else{
        		showActiveFilters();
        	}
        $("#applyActiveClicked").val('1');
//         if($(".filterOptionRadio:checked").length > 0){
        	
// 			$("#clearAllFiltersBtn").parent("label").show();
//  			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
//  			if($(".filterCheckStatus:checked").length == 0){
//         		showAllFilters();
//         	}
//     	}else{
//     		$("#clearAllFiltersBtn").parent("label").hide();
//  			$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
//     	}  
      };

    //click event to make the filter disbable when clicked on checkbox
    $(".filterCheckStatus").click(function(){
    	var el = $(this); //
    	if(el.is(":checked")){
    		chkboxClickEnable(el);
    	}else{
			chkboxDisable(el);
    	}

    	//if($("#clearAllFiltersBtn").is(":checked")){
			//alert("to be process");
    		if($(".filterCheckStatus").length == $(".peauto").length && $(".disableRow.active").length == 0){
				$("#clearAllFiltersBtn").prop("checked", true);
    		}else{
    			$("#clearAllFiltersBtn").prop("checked", false);
    		}
    	//}
    		  	
    });

    //click event for checkbox enable or disable
     $('.filterOptionRadio').click(function(){
     	var el = $(this);
     	 chkboxEanable(el);
     });
     
 	// event for check enable or disable all radio button in table or form
	var filterStatus = function(){
    	$(".filterCheckStatus").each(function(){ 
    		var el = $(this);
    		//get radio button check 
    		var chkRadio = el.parent("label").parent("td").siblings('td').find("input[type=radio]");
     		var elChked = chkRadio.is(":checked");
			//check enable radio-button particular checkbox
    		if(elChked){
        		if(el.is(":checked")){
     				//el.css("pointer-events", "none");
     				//el.addClass("penone");
     				chkRadio.css("pointer-events", "none");
	     			//el.closest("tr").removeClass("enableRow");
	     			//el.closest("tr").removeClass("active");
	     			//el.closest("tr").addClass("disableRow");
	     			el.closest("tr").find(".radio-inline").find("label").css("pointer-events", "none");
	     			//el.closest("tr").find("td").find(".FilterOptions").find("select").prop("disabled", true);
	     			el.closest("tr").find("td").find(".FilterOptions").find("select").css("pointer-events", "none");
	     	    	//el.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").prop("disabled", true);
	     	    	el.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").css("pointer-events", "none");
	     	    	el.closest("tr").find("td").find(".FilterOptions").find(".filterOptionText").val(''); //
	     	    	el.closest("tr").find("td").find(".FilterOptions").find(".resetF").css("pointer-events", "none");
	     	    	var dataVal = el.attr("data-filterd");
	     			el.parent("label").prev("input[type=hidden]").val(dataVal);
        		}else{ // 
            		
        			el.css("pointer-events", "auto");
        			
	     			el.closest("tr").addClass("enableRow");
	     			el.closest("tr").addClass("active");
	     			el.parent("label").prev("input[type=hidden]").val('');
        		}
        		el.addClass("peauto");
        		el.closest("tr").addClass("enableRow");
        		el.closest("tr").removeClass("disableRow");
     			var activeFilters = el.closest("tr").find(".filterH").attr("data-filter");
     			el.closest("tr").find(".filterH").val(activeFilters);
     			el.prop("checked", true);
     			
     		}else{
     	 		el.addClass("penone");
     	 		el.closest("tr").addClass("disableRow");
     	 		el.closest("tr").find(".filterH").val('');
     	 		el.parent("label").prev("input[type=hidden]").val('');
     	 		el.prop("checked", false);
     		}
    		el.closest("tr").find("td").find(".FilterOptions").hide();
    	});
    	$(".applyFilterBtn").show();

    	if($(".filterCheckStatus:checked").length == $(".penone").length){
    		$("#clearAllFiltersBtn").prop("checked", true);
    	}else{
    		$("#clearAllFiltersBtn").prop("checked", false);
    	}

    	$("#loadCount").val('1');
    	if($(".filterOptionRadio:checked").length > 0){
			$("#applyActiveClicked").val('1');
    	}
    	if($(".filterCheckStatus:checked").length > 0){
			$("#clearAllFiltersBtn").parent("label").show();
 			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
    	}else{
    		$("#clearAllFiltersBtn").parent("label").hide();
 			$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
    	}  
	}; 
	filterStatus();

	var resetFilter = function(el){ //
		el.parent("div").parent("div").parent("td").siblings("td").find(".filterCheckStatus").css("pointer-events", "none");
		el.parent("div").parent("div").parent("td").siblings("td").find(".filterCheckStatus").addClass("penone");
		el.parent("div").parent("div").parent("td").siblings("td").find(".filterCheckStatus").removeClass("peauto");
		//el.parent("div").parent("div").parent("td").siblings("td").find(".filterCheckStatus").prop("disabled", true);
		el.parent("div").parent("div").parent("td").siblings("td").find(".filterCheckStatus").prop("checked", false);
		el.parent("div").parent("div").parent("td").siblings("td").find(".filterCheckStatus").parent("label").prev("input[type=hidden]").val('');
		el.parent("div").prev().find(".radio-inline").find(".filterOptionRadio").prop("checked", false);
		el.parent("div").prev().find(".radio-inline").find(".filterOptionRadio").prop("disabled", false);
		el.parent("div").prev().find(".radio-inline").find(".filterOptionRadio").css("pointer-events", "auto");
		el.closest(".FilterOptions").find("select").prop("disabled", false);
		el.closest(".FilterOptions").find("select").val("");
		el.closest(".FilterOptions").hide();
		el.closest(".FilterOptions").find(".filterOptionText").val("");
		el.closest(".FilterOptions").find(".filterOptionText").prop("disabled", false);
		el.closest("tr").addClass("disableRow");
		el.closest("tr").removeClass("enableRow");
		el.closest("tr").removeClass("disableCheckbox");
		el.closest("tr").removeClass("active");
		el.closest("tr").find(".filterH").val('');
		var checkAllActiveStatusVal = $("#checkAllActiveStatus").val();
		
      	if(checkAllActiveStatusVal == "all"){
      		showAllFilters();
      	}else{
      		showActiveFilters();
      	}
      	changeTitleStatusNotApplied();
      	if($('#clearAllFiltersBtn').prop('checked')){
      		$(".applyFilterBtn").prop("disabled", true);
            $(".filterOptionRadio").prop("disabled", true);
            //apply_active_filters();
            $(".filterCheckStatus").prop("disabled", true);
            $(".FilterOptions").find("select").prop("disabled", true);
      	}else{
      		$(".applyFilterBtn").prop("disabled", false);
            $(".filterOptionRadio").prop("disabled", false);
            //apply_active_filters();
            $(".filterCheckStatus").prop("disabled", false);
            $(".FilterOptions").find("select").prop("disabled", false);
      	}

      	if($(".filterCheckStatus").length == $(".peauto").length){
			$("#clearAllFiltersBtn").prop("checked", true); 
		}else{ 
			$("#clearAllFiltersBtn").prop("checked", false);
		}
//       	if($(".filterOptionRadio:checked").length > 0){
// 			$("#clearAllFiltersBtn").parent("label").show();
//  			$("#clearAllFiltersBtn").parent("label").siblings("label").show();
//     	}else{
//     		$("#clearAllFiltersBtn").parent("label").hide();
//  			$("#clearAllFiltersBtn").parent("label").siblings("label").hide();
//     	}  
      	$("#applyActiveClicked").val('1');
	}

	$(".resetF").click(function(){
		var el = $(this);
		resetFilter(el);
	});
	
	showActiveFilters();
	
    $("#showActiveFilterBtn").click(function(){
    	showActiveFilters();
    });

    $("#showAllFilterBtn").click(function(){
    	showAllFilters();
    });

    var submitApply = function(){
		if($(".enableRow").length > 0){
			$(".applyFilterBtn").trigger("click");
		}
    };
    
    <?php if(!empty($fValChecked)){?>
    	//submitApply();
	<?php }?>
	
    $("input[type=number]").bind('keyup keypress input change', function(){
        // handle event
    	var el = $(this);
    	if(el.val() < 0){el.val('0');}
    });
});

 
</script>

<!-- end code for userFilte redesign -->     

<script type="text/html" id="filterSetTemplate">
	<input type="hidden"
	       class="form-control"
	       data-template-bind='[{"attribute": "data-id", "value": "data_id"}]'
	       data-id="filter_set_id_id"
	       data-value="filter_set_id_value"/>
	<div class="row">
		<div class="col-xs-8">
			<input type="text"
			       class="form-control"
			       data-template-bind='[{"attribute": "data-id", "value": "data_id"}]'
			       data-id="filter_set_name_id"
			       data-value="filter_set_name_value"/>
		</div>
		<div class="col-xs-16">
			<button type="button"
			        data-template-bind='[{"attribute": "data-id", "value": "data_id"}, {"attribute": "data-save-id", "value": "filter_set_id_value"}, {"attribute": "style", "value": "hide_disable"}]'
			        data-id="filter_disable_id"
			        class="btn-averistar disableUserFilterBtn">Disable
			</button>
			<button type="button"
			        data-template-bind='[{"attribute": "data-id", "value": "data_id"}, {"attribute": "data-save-id", "value": "filter_set_id_value"}, {"attribute": "style", "value": "hide_enable"}]'
			        data-id="filter_enable_id"
			        class="btn-averistar enableUserFilterBtn">Enable
			</button>
			<button type="button"
			        data-template-bind='[{"attribute": "data-id", "value": "data_id"}, {"attribute": "data-save-id", "value": "filter_set_id_value"}]'
			        data-id="filter_delete_id"
			        class="btn-averistar btn-danger deleteUserFilterBtn">Delete
			</button>
		</div>
	</div>
	<div data-id="filterUsersValuesDiv_id"
	     data-template-bind='[{"attribute": "data-id", "value": "data_id"}]'
	     data-class="css_class"
	     style="width: -moz-fit-content;width: fit-content;"></div>
</script>
<style type="text/css">
        .disableFilter
        {
            background: none repeat scroll 0 0 burlyWood;
            cursor: default !important;
        }
        
        .enableFilter{
            color: #2b2;
        }
        .disableRow td, .enableRow td{
        	background-color:#fff;
        	border:none;
        }
        .filterh1title {
	        font-size: 2em;
		xwidth: 100%;		
                color: #6ea0dc;
                /* color: #242424; */
		/* background: #FFF; */
        }
        #defaultUserFiltersDiv table tr.active, #defaultUserFiltersDiv table {
        	 background-color: #FFF;
        	 border:none;
        }
        .expandFilterClass{
        	cursor:pointer;
        }
        .filtersApplied{
        	font-weight:bold;
        }
        .fIcon{
        	color:#fff !important;
        }
        #defaultUserFiltersDiv table tr td:first-child{
        	 width:30px !important;
        }
        .filterTitleBtn{
        	float:right; 
        	margin-left:3px; 
        	margin-top:-150px;
                background-color: #5d9aac !important;
                color: #FFF;
        }
  </style>
<style>
	.user_filter_set {
		border: none;
		padding: 10px;
		margin: 20px 10px
	}

	.user_filter_set.error {
		background: #ce0d0d;
	}

	.user_filter_set.success {
		background: #9cc99c;
	}

	.user_filter_set.inactive {
		background: #fff4b0;
	}

	.user_filters.inactive {
		display: none;
	}

	.user_filters.active {
		display: block;
	}
</style>
<!-- User Filters End -->
<!--
<script type="text/javascript">

    function setTableSorter() {
        /*if(userDetailDataTable) {
			userDetailDataTable.clear();
			return;
		}*/
        var table1 = $('#usersTable1').DataTable({
            scrollY: 500,
            scrollX: true,
            scrollCollapse: true,
            paging: false,
            fixedColumns: {
                leftColumns: 2
            },
            searching: false,
            info: false,
            "fnDrawCallback": function () {
                console.log("Caught Exception.");
            },

            "initComplete": function () {
                console.log("initComplete");
            }
        });

        userDetailDataTable = table1;



    }
// This will execute whenever the window is resized
	$(window).resize(function() {  
        $(".triggerColumn").trigger("click"); 
		$("table.dataTable tbody td").css("padding", "2px 10px");		
    });
	function eventAfterLoad(){
		//alert('eventAfterLoad Called'););
		$("table.dataTable tbody td").css("padding", "3px 10px");
	};

</script>
-->