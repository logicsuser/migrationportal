<?php
/**
 * Created by Dipin Krishna.
 * Date: 12/12/17
 * Time: 4:51 PM
 */
?>
<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
$serviceInfo = new Services();
parse_str($_POST['preset_filters'], $preset_filters);

$filtersArr = array();
if(is_array($preset_filters["FiltersDD"])){
	$i = 0;
	foreach($preset_filters["FiltersDD"] as $key=>$val){
		if(!empty($val)){
			$filtersArr[$key] = "";
		}else{
			$filtersArr[$key] = $preset_filters["Filters"][$i];
		}
		$i++;
	}
	unset($preset_filters["Filters"]);
	$preset_filters["FiltersD"]	= $preset_filters["FiltersDD"];
	$preset_filters["Filters"] = $filtersArr;
	unset($preset_filters["FiltersDD"]);
}
$_POST['preset_filters'] = http_build_query($preset_filters); 

$fValue = "0";
if(isset($preset_filters["Filters"])){
	foreach($preset_filters["Filters"] as $fKey=>$fVal){
		if(!empty($fVal)){
			$fValue = "1"; 
			break;
		}
	}
}
//echo $fValue;
//require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/DBLookup.php");
require($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/expressSheetFunctions.php");

if (isset($_POST['delete_filter'])) {
	if ($_POST['delete_filter'] > 0 && deleteUserFilter($_SESSION["adminId"], $_POST['delete_filter'])) {
		echo '{"success": true}';
		return;
	}

	echo '{"success": false}';
	return;

} else if (isset($_POST['toggle_filter'])) {
	if ($_POST['toggle_filter'] > 0 && toggleUserFilter($_SESSION["adminId"], $_POST['toggle_filter'], $_POST['inactive'])) {
		echo '{"success": true}';
		return;
	}

	echo '{"success": false}';
	return;
}

require_once("FilterRulesSql.php");

$sp = $_SESSION['sp'];
$groupId = $_SESSION['groupId'];

unset($groupInfoData);
require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/expressSheets/group_info.php");

//Get Device Types
$nonObsoleteDevices = array();
require_once($_SERVER['DOCUMENT_ROOT'] . '/../lib/broadsoft/adminPortal/getDevices.php');
require_once ("/var/www/html/Express/util/getAuthorizeDeviceTypes.php");

// Build devices lists:  customDevices, deviceTypesDigitalVoIP, analogGatewayDevices
//$deviceTypesAnalogGateways = array();
//$sipGatewayLookup = new DBLookup ($db, "devices", "deviceName", "sipGateway");

// Get Custom Devices List from DB.
// If the list exists, only devices from the list will be available in drop-down for assignment.
/*$customDevices = array();
if ($useCustomDeviceList == "true") {
	$query = "select deviceName from customDeviceList";
	$result = $db->query($query);

	while ($row = $result->fetch()) {
		$customDevices [] = $row ["deviceName"];
	}
}

foreach ($nonObsoleteDevices as $key => $value) {
	// shortcut custom device list for Audio Codes until they are resolved
	$deviceIsAudioCodes = substr($value, 0, strlen("AudioCodes-")) == "AudioCodes-";

	if ($useCustomDeviceList == "true" && !in_array($value, $customDevices)) {
		if (!$deviceIsAudioCodes) {
			continue;
		}
	}
	if ($sipGatewayLookup->get($value) == "true" || $deviceIsAudioCodes) {
		$deviceTypesAnalogGateways [] = $value;
	} else {
		$deviceTypesDigitalVoIP [] = $value;
	}
}*/

?>
<?php
// Get params
$all_filters = isset($_POST['all_filters']) ? $_POST['all_filters'] : array();
$all_errors = array();
$filter_sql = "";
$filter_params = array();
$result = array();
$output = array();
$users = array();

$jfr = new FilterRulesSql();
$jfr->set_allowed_functions(array('date_encode'));

foreach ($all_filters as $filter) {

	$filter_id = $filter['filter_id'];
	$a_rules = $filter['rules'];

	if (count($a_rules) == 0) {
		exit;
	}
	$result = $jfr->parse_rules($a_rules);

	$last_error = $jfr->get_last_error();
	if (!is_null($last_error['error_message'])) {
		$all_errors[] = array('filter_id' => $filter_id, 'error' => $last_error);
	}

	$jfr->append_rule_set = true;

}

if (count($all_errors)) {
	$output['errors'] = $all_errors;
} else {

	//Get Group Info
	$sp = $_SESSION['sp'];
	$groupId = $_SESSION['groupId'];
	unset($groupInfoData);
	$extraUserInfo = 1;
	//require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/expressSheets/group_info.php");
// 	require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/users/getAllUsers.php");
// 	require_once("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");

	$getCurrentDetailedUsersList = false;
	if($_POST["fileType"] == "user" && isset($_SESSION['detailedUsersList'])){
		$useCurrentDetailedUsersList = $_SESSION['detailedUsersList'];
	}

	require_once("/var/www/lib/broadsoft/adminPortal/util/getAllUsersFilterOperation.php");

        //Code @ 19 Sep 2018 EX-789
        function checkPolyComPhoneServices($servicePackArr, $deviceType, $ociVersion)
        {
            require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
            require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
            require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");            
            
            $serviceObj  = new Services();
            $syslevelObj = new sysLevelDeviceOperations();
            
            $userAssignedServicePackArr = $servicePackArr;
            
            $plycmServicePacksArr = $serviceObj->servicePacksHavePolycomPhoneServices($servicePackArr);
            $userHasPolycomSPFlag = "No";
            foreach($userAssignedServicePackArr as $k2 => $v2)
            {
                if(in_array($v2, $plycmServicePacksArr))
                {
                    $userHasPolycomSPFlag = "Yes";
                }    
            }        
            $returnResp = $syslevelObj->getSysytemDeviceTypeServiceRequest($deviceType, $ociVersion);
                        
            if($returnResp["Error"] == "" && empty($returnResp["Error"])){
            $respTmp = strval($returnResp["Success"]->supportsPolycomPhoneServices);
            }else{
                    $respTmp = "false";
            }
            //End code
            if($userHasPolycomSPFlag=="Yes" && $respTmp!="false"){
                $polycomPhoneService = "Yes";
            }else{
                $polycomPhoneService = "No";
            }
            
            return $polycomPhoneService;
        }
        //End code
        
	
	if (isset($_SESSION["userDeviceList"])) {
		unset($_SESSION["userDeviceList"]);
	}
		
	if (isset($users)) {

		$stmt = $db->prepare("DROP TABLE IF EXISTS temp_filtered_users");
		$stmt->execute();

		$stmt = $db->prepare("CREATE TEMPORARY TABLE temp_filtered_users (
      				  userId varchar(50),
				      first_name varchar(50),
				      last_name varchar(50),
				      registred varchar(10),
				      activated varchar(10),
				      phoneNumber varchar(10),
				      extension varchar(10),
				      device_name varchar(50),
				      device_type varchar(50),
				      custom_profile  varchar(50),
				      mac_address varchar(50),
				      service_packs text,
				      user_services text,
				      custom_tags text,
				      ep_type varchar(50),
				      fwd varchar(50),
				      fwd_to varchar(50),
				      dnd varchar(10),
                      clidBlock varchar(10),
				      is_analog tinyint(1) DEFAULT 0,
				      has_vm tinyint(1) DEFAULT 0,
				      has_BLF tinyint(1) DEFAULT 0,
				      has_CFA tinyint(1) DEFAULT 0,
				      has_CFB tinyint(1) DEFAULT 0,
				      has_CFN tinyint(1) DEFAULT 0,
				      has_CFR tinyint(1) DEFAULT 0,
				      is_sca_primary tinyint(1) DEFAULT 0,
				      is_sca_appearances tinyint(1) DEFAULT 0,
				      is_blf_monitored tinyint(1) DEFAULT 0,
				      vm_is_active tinyint(1) DEFAULT 0
    			)");
		$stmt->execute();

		//error_log(print_r($users, true));

		for ($a = 0; $a < count($users); $a++) {



			$userId = $users[$a]["userId"];
			$firstName = $users[$a]["firstName"];
			$lastName = $users[$a]["lastName"];
			$userName = $users[$a]["firstName"] . " " . $users[$a]["lastName"];
			$extension = $users[$a]["extension"];
			$DnD = $users[$a]["DnD"];
			$Fwd = $users[$a]["Fwd"]["active"];
			$remote = $users[$a]["remote"]["active"];
			$department = $users[$a]["department"];
			$servicePack = isset($users[$a]["servicePack"]) ? implode(", ", $users[$a]["servicePack"]) : "";
			$user_services = isset($users[$a]["userServices"]) ? implode(", ", $users[$a]["userServices"]) : "";
			$customProfile = $users[$a]["customProfile"];
			$customTags = isset($users[$a]["customTagsShortDescriptions"]) ? implode(", ", $users[$a]["customTagsShortDescriptions"]) : "";

			$has_vm = $users[$a]["voiceMessagingUser"] == "true" ? 1 : 0;
			$vm_is_active = $users[$a]['vm']["isActive"] == "true" ? 1 : 0;
			$has_BLF = $users[$a]["hasBLFUsers"] == "true" ? 1 : 0;
			$is_sca_primary = $users[$a]["sharedCallAppearances"] == "true" ? 1 : 0;

			$has_CFA = $users[$a]["cfaActive"] == "true" ? 1 : 0;
			$has_CFB = $users[$a]["cfbActive"] == "true" ? 1 : 0;
			$has_CFN = $users[$a]["cfnActive"] == "true" ? 1 : 0;
			$has_CFR = $users[$a]["cfrActive"] == "true" ? 1 : 0;

			$is_sca_appearances = in_array($userId, $userOperation->users_SCA_Appearances) ? 1 : 0;
			$is_blf_monitored = in_array($userId, $userOperation->users_BLF_Monitored) ? 1 : 0;
			$clidBlock = ($serviceInfo->hasCLIDBlocKingService($_SESSION['groupId'])) ? $users [$a] ["clidBlock"] : "";
			
			if ($DnD == "false") {
				$DnD = "Off";
			} else {
				$DnD = "On";
			}
			if ($Fwd == "true") {
				$Fwd = "Active";
				$FwdTo = $users[$a]["Fwd"]["FwdTo"];
			} else {
				if (isset($users[$a]["Fwd"]["FwdTo"]) && $users[$a]["Fwd"]["FwdTo"] != "") {
					$Fwd = "Not Active";
					$FwdTo = $users[$a]["Fwd"]["FwdTo"];
				} else {
					$Fwd = "&nbsp;";
					$FwdTo = "&nbsp;";
				}
			}
			if ($remote == "true") {
				$remote = "*";
				$remote_number = $users[$a]["remote"]["number"];
			} else {
				$remote = "&nbsp;";
				$remote_number = "&nbsp;";
			}

			$fileName = "/var/www/SASTestingUser/SASTestUsers.csv";
			if (file_exists($fileName)) {
				$objcsv = new UserOperations();
				$searchInCsv = $objcsv->find_user_in_csv($fileName, $userId);
			} else {
				$searchInCsv = "";
			}

			// get user dn number activation response
			$userDn = new Dns ();
			$numberActivateResponse = $userDn->getUserDNActivateListRequest($userId);
			$phoneNumberStatus = "";
			$userPhoneNumber = "";
			if (empty ($numberActivateResponse ['Error'])) {
				if (!empty ($numberActivateResponse ['Success'] [0] ['phoneNumber'])) {
					$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
				}
				if (!empty ($numberActivateResponse ['Success'] [0] ['status'])) {
					$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
				}
			}

			if (isset($users[$a]["registration"]) and count($users[$a]["registration"]) > 0) {
				for ($b = 0; $b < count($users[$a]["registration"]); $b++) {
					$deviceName = $users[$a]["registration"][$b]["deviceName"];
					$expiration = $users[$a]["registration"][$b]["expiration"];
					$epType = $users[$a]["registration"][$b]["epType"];

					$deviceType = $users[$a]["registration"][$b]["deviceType"];
					$macAddress = $users[$a]["registration"][$b]["macAddress"];

					$integration = $users[$a]["registration"][$b]["integration"];
					$ccd = $users[$a]["registration"][$b]["ccd"];

					if ($searchInCsv != "") {
						$isRegUserSasTester = "SAS";
					} else {
						$isRegUserSasTester = "Yes";
					}

					$is_analog = 0;
					if ($deviceType && in_array($deviceType, $deviceTypesAnalogGateways)) {
						$is_analog = 1;
					}

					

					$stmt = $db->prepare("INSERT INTO temp_filtered_users (
      				  userId,
      				  first_name,
      				  last_name,
				      registred,
				      activated,
				      phoneNumber,
				      extension,
				      device_name,
				      device_type,
				      custom_profile,
				      dnd,
                      clidBlock,
				      is_analog,
				      has_vm,
				      has_BLF,
				      has_CFA,
				      has_CFB,
				      has_CFN,
				      has_CFR,
				      is_sca_primary,
				      is_sca_appearances,
				      is_blf_monitored,
				      service_packs,
				      user_services,
				      custom_tags,
				      vm_is_active
    				) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
					$stmt->execute(
						array(
							$userId,
							$firstName,
							$lastName,
							$isRegUserSasTester,
							$phoneNumberStatus,
							$userPhoneNumber,
							$extension,
							$deviceName,
							$deviceType,
							$customProfile,
							$DnD,
						    $clidBlock,
							$is_analog,
							$has_vm,
							$has_BLF,
							$has_CFA, $has_CFB, $has_CFN, $has_CFR,
							$is_sca_primary,
							$is_sca_appearances,
							$is_blf_monitored,
							$servicePack . ',',
							$user_services . ',',
							$customTags . ',',
							$vm_is_active
						)
					);

				}

			} else {
				$deviceName = $users[$a]["deviceName"];
				$deviceType = $users[$a]["deviceType"];
				$macAddress = $users[$a]["macAddress"];
				$integration = $users[$a]["integration"];
				$ccd = $users[$a]["ccd"];
				if ($searchInCsv != "") {
					$isUnRegUserSasTester = "SAS";
				} else {
					$isUnRegUserSasTester = "No";
				}

				$is_analog = 0;
				if ($deviceType && in_array($deviceType, $deviceTypesAnalogGateways)) {
					$is_analog = 1;
				}

				
				$stmt = $db->prepare("INSERT INTO temp_filtered_users (
      				  userId,
      				  first_name,
      				  last_name,
				      registred,
				      activated,
				      phoneNumber,
				      extension,
				      device_name,				      
				      device_type,
				      custom_profile,
				      dnd,
                      clidBlock,
				      is_analog,
				      has_vm,
				      has_BLF,
				      has_CFA,
				      has_CFB,
				      has_CFN,
				      has_CFR,
				      is_sca_primary,
					  is_sca_appearances,
					  is_blf_monitored,
					  service_packs,
					  user_services,
					  custom_tags,
					  vm_is_active
    				) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
				$stmt->execute(
					array(
						$userId,
						$firstName,
						$lastName,
						$isUnRegUserSasTester,
						$phoneNumberStatus,
						$userPhoneNumber,
						$extension,
						$deviceName,
						$deviceType,
						$customProfile,
						$DnD,
					    $clidBlock,
						$is_analog,
						$has_vm,
						$has_BLF,
						$has_CFA, $has_CFB, $has_CFN, $has_CFR,
						$is_sca_primary,
						$is_sca_appearances,
						$is_blf_monitored,
						$servicePack . ',',
						$user_services . ',',
						$customTags . ',',
						$vm_is_active
					)
				);

			}
		}

	}
	//error_log("SELECT * FROM temp_filtered_users " . $result['sql']);
	//error_log(print_r($result['bind_params'], true));

	//$stmt = $db->prepare("SELECT * FROM temp_filtered_users $filter_sql");
	//$stmt->execute();
	//error_log( print_r($stmt->fetchAll(PDO::FETCH_ASSOC), true));

	//Save filters to database
	$filters_saved = array();
	foreach ($all_filters as $filter) {

		$filter_id = $filter['filter_id'];
		$a_rules = $filter['rules'];
		$id = $filter['id'];
		$name = $filter['name'];
		$id = addUpdateUserFilter($_SESSION["adminId"], json_encode($a_rules), $name, $id);
		$filters_saved[] = array('filter_id' => $filter_id, 'id' => $id);
	}
	$output['filters_saved'] = $filters_saved;

	//Preset Filters
	$preset_filters = null;
	if(isset($_POST['preset_filters'])) {
		parse_str($_POST['preset_filters'], $preset_filters);
	}

	//print_r($preset_filters);

	$preset_filters_sql_array = array();

	if (isset($preset_filters)) {

		addUpdatePresetFilter($_SESSION["adminId"], json_encode($preset_filters));

		if (isset($preset_filters['Filters'])) {

			if(!isset($result['bind_params'])) {
				$result['bind_params'] = array();
			}

			foreach ($preset_filters['Filters'] as $preset_filter) {

				switch ($preset_filter) {
					case 'RegisteredUser' :

						if ($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( registred = "YES" ) ';
						} else if ($preset_filters['Filter'][$preset_filter] == 'NO') {
							$preset_filters_sql_array[] = ' ( registred != "YES" ) ';
						}
						break;
					case 'ActivatedUser' :

						if ($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( activated = "YES" ) ';
						} else if ($preset_filters['Filter'][$preset_filter] == 'NO') {
							$preset_filters_sql_array[] = ' ( activated != "YES" ) ';
						}
						break;
					case 'ExtensionOnly' :

						if ($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( phoneNumber = "" and extension != "" ) ';
						} else if ($preset_filters['Filter'][$preset_filter] == 'NO') {
							$preset_filters_sql_array[] = ' ( !(phoneNumber = "" and extension != "") ) ';
						}
						break;
					case 'DeviceLessUser' :

						if ($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( device_name = "" ) ';
						} else if ($preset_filters['Filter'][$preset_filter] == 'NO') {
							$preset_filters_sql_array[] = ' ( device_name != "" ) ';
						}
						break;
					case 'DnD' :

						if ($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( dnd = "On" ) ';
						} else if ($preset_filters['Filter'][$preset_filter] == 'NO') {
							$preset_filters_sql_array[] = ' ( dnd != "On" ) ';
						}
						break;
					case 'SIP_ANALOG' :

						if ($preset_filters['Filter'][$preset_filter] == 'ANALOG') {
							$preset_filters_sql_array[] = ' ( is_analog = 1 ) ';
						} else if ($preset_filters['Filter'][$preset_filter] == 'DIGITAL') {
							$preset_filters_sql_array[] = ' ( is_analog = 0 ) ';
						}
						break;
					case 'Voice_Messaging' :

						if ($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( vm_is_active = 1 ) ';
						} else if ($preset_filters['Filter'][$preset_filter] == 'NO') {
							$preset_filters_sql_array[] = ' ( vm_is_active = 0 ) ';
						}

						if ($preset_filters['Filter']['Voice_Messaging_Assigned'] == 'YES') {
							$preset_filters_sql_array[] = ' ( has_vm = 1 ) ';
						} else if ($preset_filters['Filter']['Voice_Messaging_Assigned'] == 'NO') {
							$preset_filters_sql_array[] = ' ( has_vm = 0 ) ';
						}
						break;
					case 'BLF' :

						if ($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( has_BLF = 1 ) ';
						} else if ($preset_filters['Filter'][$preset_filter] == 'NO') {
							$preset_filters_sql_array[] = ' ( has_BLF = 0 ) ';
						}
						break;
					case 'BLF_USER' :

						if ($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( is_blf_monitored = 1 ) ';
						} else if ($preset_filters['Filter'][$preset_filter] == 'NO') {
							$preset_filters_sql_array[] = ' ( is_blf_monitored = 0 ) ';
						}
						break;
					case 'SCA_PRIMARY' :

						if ($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( is_sca_primary = 1 ) ';
						} else if ($preset_filters['Filter'][$preset_filter] == 'NO') {
							$preset_filters_sql_array[] = ' ( is_sca_primary = 0 ) ';
						}
						break;
					case 'SCA_USER' :

						if ($preset_filters['Filter'][$preset_filter] == 'YES') {
							$preset_filters_sql_array[] = ' ( is_sca_appearances = 1 ) ';
						} else if ($preset_filters['Filter'][$preset_filter] == 'NO') {
							$preset_filters_sql_array[] = ' ( is_sca_appearances = 0 ) ';
						}
						break;
					case 'DEVICE_TYPE' :

						if (isset($preset_filters['Filter']['DEVICE_TYPE_SELECTED'])) {
							/*
							$selected_device_types = $preset_filters['Filter'][$preset_filter];
							$selected_device_types_filler = array();
							foreach ($selected_device_types as $selected_device_type) {
								$result['bind_params'][] = $selected_device_type;
								$selected_device_types_filler[] = '?';
							}
							*/
							$preset_filters_sql_array[] = ' ( device_type ' . ($preset_filters['Filter']['DEVICE_TYPE'] == 'YES' ? ' = ' : ' != ') . ' ? ) ';
							$result['bind_params'][] = $preset_filters['Filter']['DEVICE_TYPE_SELECTED'];
						}
						break;
					case 'SERVICE_PACK' :

						if (isset($preset_filters['Filter']['SERVICE_PACK_SELECTED'])) {
							$preset_filters_sql_array[] = ' ( service_packs ' . ($preset_filters['Filter']['SERVICE_PACK'] == 'YES' ? ' LIKE ' : ' NOT LIKE ') . ' ? ) ';
							$result['bind_params'][] = '%' . $preset_filters['Filter']['SERVICE_PACK_SELECTED'] . ',%';
						}
						break;
					case 'USER_SERVICE' :

						if (isset($preset_filters['Filter']['USER_SERVICE_SELECTED'])) {
							$preset_filters_sql_array[] = ' ( user_services ' . ($preset_filters['Filter']['USER_SERVICE'] == 'YES' ? ' LIKE ' : ' NOT LIKE ') . ' ? ) ';
							$result['bind_params'][] = '%' . $preset_filters['Filter']['USER_SERVICE_SELECTED'] . ',%';
						}
						break;
					case 'CUSTOM_PROFILE' :

						if (isset($preset_filters['Filter']['CUSTOM_PROFILE_SELECTED'])) {
							$preset_filters_sql_array[] = ' ( custom_profile ' . ($preset_filters['Filter']['CUSTOM_PROFILE'] == 'YES' ? ' = ' : ' != ') . ' ? ) ';
							$result['bind_params'][] = $preset_filters['Filter']['CUSTOM_PROFILE_SELECTED'];
						}
						break;
					case 'CUSTOM_TAG' :

						if (isset($preset_filters['Filter']['CUSTOM_TAG_SELECTED'])) {
							$preset_filters_sql_array[] = ' ( custom_tags ' . ($preset_filters['Filter']['CUSTOM_TAG'] == 'YES' ? ' LIKE ' : ' NOT LIKE ') . ' ? ) ';
							$result['bind_params'][] = '%' . $preset_filters['Filter']['CUSTOM_TAG_SELECTED'] . ',%';
						}
						break;
					case 'PHONE_NUMBER' :

						if (isset($preset_filters['Filter']['PHONE_NUMBER'])) {

							switch ($preset_filters['Filter']['PHONE_NUMBER']) {

								case 'StartsWith' :

									if (isset($preset_filters['Filter']['PHONE_NUMBER_STARTS_WITH'])) {
										$preset_filters_sql_array[] = ' ( phoneNumber LIKE ? ) ';
										$result['bind_params'][] = $preset_filters['Filter']['PHONE_NUMBER_STARTS_WITH'] . '%';
									}
									break;

								case 'Range' :

									if (isset($preset_filters['Filter']['PHONE_NUMBER_RANGE_FROM']) && $preset_filters['Filter']['PHONE_NUMBER_RANGE_FROM'] != "") {
										$preset_filters_sql_array[] = ' ( phoneNumber >= ? ) ';
										$result['bind_params'][] = $preset_filters['Filter']['PHONE_NUMBER_RANGE_FROM'];
									}
									if (isset($preset_filters['Filter']['PHONE_NUMBER_RANGE_TO']) && $preset_filters['Filter']['PHONE_NUMBER_RANGE_TO'] != "") {
										$preset_filters_sql_array[] = ' ( phoneNumber <= ? ) ';
										$result['bind_params'][] = $preset_filters['Filter']['PHONE_NUMBER_RANGE_TO'];
									}
									break;


							}
						}
						break;
					case 'EXTENSION' :

						if (isset($preset_filters['Filter']['EXTENSION'])) {

							switch ($preset_filters['Filter']['EXTENSION']) {

								case 'StartsWith' :

									if (isset($preset_filters['Filter']['EXTENSION_STARTS_WITH'])) {
										$preset_filters_sql_array[] = ' ( extension LIKE ? ) ';
										$result['bind_params'][] = $preset_filters['Filter']['EXTENSION_STARTS_WITH'] . '%';
									}
									break;

								case 'Range' :

									if (isset($preset_filters['Filter']['EXTENSION_RANGE_FROM']) && $preset_filters['Filter']['EXTENSION_RANGE_FROM'] != "") {
										$preset_filters_sql_array[] = ' ( extension >= ? ) ';
										$result['bind_params'][] = $preset_filters['Filter']['EXTENSION_RANGE_FROM'];
									}
									if (isset($preset_filters['Filter']['EXTENSION_RANGE_TO']) && $preset_filters['Filter']['EXTENSION_RANGE_TO'] != "") {
										$preset_filters_sql_array[] = ' ( extension <= ? ) ';
										$result['bind_params'][] = $preset_filters['Filter']['EXTENSION_RANGE_TO'];
									}
									break;


							}
						}
						break;
					case 'CFA_CFB_CFN' :

						if (isset($preset_filters['Filter']['CFA'])) {

							if ($preset_filters['Filter']['CFA'] == 'YES') {
								$preset_filters_sql_array[] = ' ( has_CFA = 1 ) ';
							} else if ($preset_filters['Filter']['CFA'] == 'NO') {
								$preset_filters_sql_array[] = ' ( has_CFA = 0 ) ';
							}
						}

						if (isset($preset_filters['Filter']['CFB'])) {

							if ($preset_filters['Filter']['CFB'] == 'YES') {
								$preset_filters_sql_array[] = ' ( has_CFB = 1 ) ';
							} else if ($preset_filters['Filter']['CFB'] == 'NO') {
								$preset_filters_sql_array[] = ' ( has_CFB = 0 ) ';
							}
						}

						if (isset($preset_filters['Filter']['CFN'])) {

							if ($preset_filters['Filter']['CFN'] == 'YES') {
								$preset_filters_sql_array[] = ' ( has_CFN = 1 ) ';
							} else if ($preset_filters['Filter']['CFN'] == 'NO') {
								$preset_filters_sql_array[] = ' ( has_CFN = 0 ) ';
							}
						}

						if (isset($preset_filters['Filter']['CFR'])) {

							if ($preset_filters['Filter']['CFR'] == 'YES') {
								$preset_filters_sql_array[] = ' ( has_CFR = 1 ) ';
							} else if ($preset_filters['Filter']['CFR'] == 'NO') {
								$preset_filters_sql_array[] = ' ( has_CFR = 0 ) ';
							}
						}

						break;
						
					case 'CLID_BLOCKING' :
					    
					    if($serviceInfo->hasCLIDBlocKingService($_SESSION['groupId'])) {
					        if($preset_filters['Filter'][$preset_filter] == 'YES') {
					            $preset_filters_sql_array[] = ' ( clidBlock = "Yes" ) ';
					        } else {
					            $preset_filters_sql_array[] = ' ( clidBlock != "Yes" ) ';
					        }
					    }
					    break;
				}

			}
		}
	}

	//print_r($preset_filters_sql_array);

	$sql_query = "";

	if (isset($result['sql'])) {
		$sql_query = $result['sql'] . ' AND ' . implode(" AND ", $preset_filters_sql_array);
	} else if (count($preset_filters_sql_array)) {
		$sql_query = ' WHERE ' . implode(" AND ", $preset_filters_sql_array);
	}

	$output['query'] = $sql_query;
	
// 	$db->query('set profiling=1'); //optional if profiling is already enabled
// 	$runQuery = "SELECT userId FROM temp_filtered_users " . $sql_query;
// 	$db->query($runQuery);
// 	$res = $db->query('show profiles');
// 	$records = $res->fetchAll(PDO::FETCH_ASSOC);
// 	$duration = $records[0]['Duration'];
// 	print_r($records); print_r($duration);
	

	$stmt = $db->prepare("SELECT userId FROM temp_filtered_users " . $sql_query);
	if (isset($result['bind_params'])) {
		$stmt->execute($result['bind_params']);
	} else {
		$stmt->execute(array());
	}


	$filtered_users = array();
	while ($filtered_user = $stmt->fetch(PDO::FETCH_ASSOC)) {
		$filtered_users[] = $filtered_user['userId'];
	}
	$output['results'] = $filtered_users;


	$stmt = $db->prepare("SELECT * FROM temp_filtered_users ");
	$stmt->execute(array());

	$output['all_results'] = $stmt->fetchAll(PDO::FETCH_ASSOC);
}

if($fValue == "1"){ 

	$usersFilter = array();
	foreach($users as $key=>$val){
		if(in_array($val["userId"], $filtered_users)){
			$usersFilter[$key] = $users[$key];
		}
	}
}else{
	$usersFilter = $users;
 }
 $users = array_values($usersFilter);
 //print_r($users);die;

echo $fValue;

if($_POST["fileType"] == "user"){
	echo creatHtmlUser($users);
	echo "RegistrationDetails";
	echo createRegistrationDetails($registrationUsers);
}else{
	echo createHtmlExpress($users);
}

//function to create html for users
function creatHtmlUser($users){
    $serviceInfo = new Services();
    
    $usersHtml = '<table id="usersTable1" class="stripe row-border order-column customDataTable" cellspacing="0" width="100%">
        <thead>
			<tr>
				<th class="col1 triggerColumn" >&nbsp;</th>
				<th class="col2" >User Id</th>
				<th class="col3" >Activated</th>
				<th class="col4" >Registered</th>
				<th class="col5" >User Name</th>
                <th class="col5">Phone Number</th>
				<th class="col6" >Extension</th>
				<th class="col7" >Device Name</th>
				<th class="col8" >Device Type</th>
				<th class="col9" >Analog Port Assignment</th>
				<th class="col10" >MAC Address</th>
				<th class="col11" >Department</th>
				<th class="col12" >Service Pack</th>
				<th class="col13" >Expiration</th>
				<th class="col14" >Type</th>
				<th class="col15" >DnD</th>
				<th class="col16" >Fwd</th>
				<th class="col17" >Fwd To</th>';
    
    if($serviceInfo->hasCLIDBlocKingService($_SESSION['groupId'])) {
        $usersHtml .= '<th class="col18" >Calling Line ID Blocking</th>';
    }
    
    $usersHtml .= '<th class="col18" >Sim Ring</th>
					<th class="col19" >Remote Office</th>
					<th class="col20" >Remote Number</th>
					<th class="col21" >Polycom Phone Services</th>
					<th class="col22" >Custom Contact Directory</th>
				</tr>
			</thead><tbody>';
	$rowIndex = 0;
	if (isset ( $users )) {
		
		for($a = 0; $a < count ( $users ); $a ++) {
			$userId = $users [$a] ["userId"];
			$userName = $users [$a] ["firstName"] . " " . $users [$a] ["lastName"];
			$extension = $users [$a] ["extension"];
			$DnD = $users [$a] ["DnD"];
			$Fwd = $users [$a] ["Fwd"] ["active"];
			$remote = $users [$a] ["remote"] ["active"];
			$department = $users [$a] ["department"];
			$servicePack = isset ( $users [$a] ["servicePack"] ) ? implode ( ", ", $users [$a] ["servicePack"] ) : "";
			$servicePackArr = $users [$a] ["servicePack"];
			$customContactDirectory = $users [$a] ["customContactDirectory"];
			$simRing = $users [$a] ["simRing"];
			$clidBlock = ($serviceInfo->hasCLIDBlocKingService($_SESSION['groupId'])) ? $users [$a] ["clidBlock"] : "";
			$phoneNumber1 = $users [$a] ["phoneNumber"];
                        
                        
			if ($DnD == "false") {
				$DnD = "Off";
			} else {
				$DnD = "On";
			}
			if ($Fwd == "true") {
				$Fwd = "Active";
				$FwdTo = $users [$a] ["Fwd"] ["FwdTo"];
			} else {
				if (isset ( $users [$a] ["Fwd"] ["FwdTo"] ) && $users [$a] ["Fwd"] ["FwdTo"] != "") {
					$Fwd = "Not Active";
					$FwdTo = $users [$a] ["Fwd"] ["FwdTo"];
				} else {
					$Fwd = "&nbsp;";
					$FwdTo = "&nbsp;";
				}
			}
			if ($remote == "true") {
				$remote = "*";
				$remote_number = $users [$a] ["remote"] ["number"];
			} else {
				$remote = "&nbsp;";
				$remote_number = "&nbsp;";
			}
			
			$fileName = "/var/www/SASTestingUser/SASTestUsers.csv";
			if (file_exists ( $fileName )) {
				$objcsv = new UserOperations ();
				$searchInCsv = $objcsv->find_user_in_csv ( $fileName, $userId );
			} else {
				$searchInCsv = "";
			}
			
			// get user dn number activation response
			$userDn = new Dns ();
			$numberActivateResponse = $userDn->getUserDNActivateListRequest ( $userId );
			$phoneNumberStatus = "";
			$userPhoneNumber = "";
			if (empty ( $numberActivateResponse ['Error'] )) {
				if (! empty ( $numberActivateResponse ['Success'] [0] ['phoneNumber'] )) {
					$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
				}
				if (! empty ( $numberActivateResponse ['Success'] [0] ['status'] )) {
					$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
				}
			}
			// code ends
			
			if (isset ( $users [$a] ["registration"] ) and count ( $users [$a] ["registration"] ) > 0)
			{
				for($b = 0; $b < count ( $users [$a] ["registration"] ); $b ++)
				{
					$deviceName = $users [$a] ["registration"] [$b] ["deviceName"];
					$expiration = $users [$a] ["registration"] [$b] ["expiration"];
					$epType = $users [$a] ["registration"] [$b] ["epType"];
					
					$deviceType = $users [$a] ["registration"] [$b] ["deviceType"];
					$macAddress = $users [$a] ["registration"] [$b] ["macAddress"];
					$portNumber = $users [$a] ["registration"] [$b] ["portNumber"];
					
					$integration = $users [$a] ["registration"] [$b] ["integration"];
					$ccd = $users [$a] ["registration"] [$b] ["ccd"];
					$cssAddClassReg = "";
					$label = "<label for='users_selected_$userId'><span></span></label>";
					
					if ($searchInCsv != "") {
						$isRegUserSasTester = "SAS";
					} else {
						$cssAddClassReg = "registerUsr";
						$isRegUserSasTester = "Yes";
					}
					
					$userIdUI = str_replace("@", "\\\	@", $userId); 
					$checkBoxStr = "<td><input type=\"checkbox\" class=\"checkUserListBox\" data-id=\"userCheckBox" . $rowIndex . "\" onclick=\"onUserSelect(this)\" name=\"" . $userId . "*-*-*" . $deviceName . "*-*-*" . $deviceType . "\" id='users_selected_$userIdUI' ";
					
					if ($_SESSION ["permissions"] ["groupWideUserModify"] == "0") {
						$checkBoxStr .= " disabled";
					}
	//new code found				
					$checkBoxStr .= "><label for='users_selected_$userIdUI'><span onmouseover='checkBoxEventAct_Detail(true)' onmouseout='checkBoxEventAct_Detail(false)'></span></label></td>";

					
					
				//	if (isset ( $_SESSION ["permissions"] ["modifyUsers"] ) and $_SESSION ["permissions"] ["modifyUsers"] == "1") {
				//		$userIdValue = "<a href='#' class='userIdVal numberList' id='" . $userId . "' data-lastname='" . $users //[$a] ["lastName"] . "' data-firstname='" . $users [$a] ["firstName"] . "'data-phone='" . $userPhoneNumber . "' //data-extension='" . $users [$a] ["extension"] . "' >" . $userId . "</a>";

					//$checkBoxStr .= "></td>";
					
// 					if (isset ( $_SESSION ["permissions"]["modifyUsers"] ) and $_SESSION ["permissions"] ["modifyUsers"] == "1") {
// 						$userIdValue = "<a href='#' class='userIdVal' id='" . $userId . "' data-lastname='" . $users [$a] ["lastName"] . "' data-firstname='" . $users [$a] ["firstName"] . "'data-phone='" . $userPhoneNumber . "' data-extension='" . $users [$a] ["extension"] . "' >" . $userId . "</a>";
// 					} else {
// 						$userIdValue = $userId;
// 					}
                                        
					
					if (isset ( $_SESSION ["permissions"] ["modifyUsers"] ) and $_SESSION ["permissions"] ["modifyUsers"] == "1") {
					    $usersHtml .= "<tr id='broadsoft_user_$userId' name='broadsoft_usersRow_$userId'
                                    class='userIdVal broadsoft_users $cssAddClassReg broadsoft_usersRow_$userId rowHoverEffect$rowIndex' data-class='rowHoverEffect$rowIndex' 
                                    data-lastname='" . $users [$a] ["lastName"] . "'
                                    data-firstname='" . $users [$a] ["firstName"] . "'
                                    data-phone='" . $userPhoneNumber . "' 
                                    data-id='" . $userId . "' 
                                    data-extension='" . $users [$a] ["extension"] . "'>" . $checkBoxStr ;
					} else {
					    //$userIdVal = "<tr>";
					    $usersHtml .= "<tr id='broadsoft_user_$userId' name='broadsoft_usersRow_$userId' class='broadsoft_users $cssAddClassReg broadsoft_usersRow_$userId rowHoverEffect$rowIndex' data-class='rowHoverEffect$rowIndex'>" . $checkBoxStr . "";
					}
					
					//$usersHtml .= "<tr id='broadsoft_user_$userId' name='broadsoft_usersRow_$userId' class='broadsoft_users //broadsoft_usersRow_$userId $cssAddClassReg'>" . $checkBoxStr;

					$usersHtml .= "<td>" . $userId . "</td>";
					$usersHtml .= "<td>" . $phoneNumberStatus . "</td>";
					$usersHtml .= "<td>" . $isRegUserSasTester . "</td>";
					$usersHtml .= "<td>" . $userName . "</td>";
					$usersHtml .= "<td>" . $phoneNumber1 . "</td>";
					$polycomPhoneService =  checkPolyComPhoneServices($servicePackArr, $deviceType, $ociVersion);

					$usersHtml .= "<td>" . $extension . "</td>";
					$usersHtml .= "<td>" . $deviceName . "</td>";
					$usersHtml .= "<td>" . $deviceType . "</td>";
					$usersHtml .= "<td>" . $portNumber . "</td>";
					$usersHtml .= "<td>" . $macAddress . "</td>";
					$usersHtml .= "<td>" . $department . "</td>";
					$usersHtml .= "<td>" . $servicePack . "</td>";
					$usersHtml .= "<td>" . $expiration . "</td>";
					$usersHtml .= "<td>" . $epType . "</td>";
					$usersHtml .= "<td>" . $DnD . "</td>";
					$usersHtml .= "<td>" . $Fwd . "</td>";
					$usersHtml .= "<td>" . $FwdTo . "</td>";
					$usersHtml .= "<td>" . $clidBlock . "</td>";
					$usersHtml .= "<td>" . $simRing . "</td>";
					$usersHtml .= "<td>" . $remote . "</td>";
					$usersHtml .= "<td>" . $remote_number . "</td>";
					$usersHtml .= "<td>" . $polycomPhoneService . "</td>";
					$usersHtml .= "<td>" . $customContactDirectory . "</td></tr>";
					
					++ $rowIndex;
				}
			} else {
				
				$deviceName = $users [$a] ["deviceName"];
				$deviceType = $users [$a] ["deviceType"];
				$portNumber = $users [$a] ["portNumber"];
				$macAddress = $users [$a] ["macAddress"];
				$integration = $users [$a] ["integration"];
				$epType = $users[$a]["epType"];
				$ccd = $users [$a] ["ccd"];
				$cssAddClass = "usersTableColor";
				
				if ($searchInCsv != "") {
					$isUnRegUserSasTester = "SAS";
					$cssAddClass = "unRegisterUsr";
				} else {
					$cssAddClass = "unRegisterUsr";
					$isUnRegUserSasTester = "No";
				}
				$userIdUI = str_replace("@", "\\\@", $userId);
				$checkBoxStr = "<td><input type=\"checkbox\" class=\"checkUserListBox\" data-id=\"userCheckBox" . $rowIndex . "\" onclick=\"onUserSelect(this)\" name=\"" . $userId . "*-*-*" . $deviceName . "*-*-*" . $deviceType . "\" id='users_selected_$userIdUI' ";
				
				if ($_SESSION ["permissions"] ["groupWideUserModify"] == "0") {
					$checkBoxStr .= " disabled";
				}
				$checkBoxStr .= "><label for='users_selected_$userIdUI'><span onmouseover='checkBoxEventAct_Detail(true)' onmouseout='checkBoxEventAct_Detail(false)'></span></label></td>";
				
				//$checkBoxStr .= "></td>";
				
// 				if (isset ( $_SESSION ["permissions"] ["modifyUsers"] ) and $_SESSION ["permissions"] ["modifyUsers"] == "1") {
// 					$userIdVal = "<a href='#' class='userIdVal' id='" . $userId . "' data-lastname='" . $users [$a] ["lastName"] . "' data-firstname='" . $users [$a] ["firstName"] . "' data-phone='" . $userPhoneNumber . "' data-extension='" . $users [$a] ["extension"] . "'>" . $userId . "</a>";
// 				} else {
// 					$userIdVal = $userId;
// 				}
				
				if (isset ( $_SESSION ["permissions"] ["modifyUsers"] ) and $_SESSION ["permissions"] ["modifyUsers"] == "1") {
				    $usersHtml .= "<tr id='broadsoft_user_$userId' name='broadsoft_usersRow_$userId' 
                                    class='userIdVal broadsoft_users $cssAddClass broadsoft_usersRow_$userId rowHoverEffect$rowIndex' data-class='rowHoverEffect$rowIndex' 
                                    data-lastname='" . $users [$a] ["lastName"] . "' 
                                    data-firstname='" . $users [$a] ["firstName"] . "' 
                                    data-phone='" . $userPhoneNumber . "' 
                                    data-id='" . $userId . "' 
                                    data-extension='" . $users [$a] ["extension"] . "'>" . $checkBoxStr ;
				} else {
				    //$userIdVal = "<tr>";
				    $usersHtml .= "<tr id='broadsoft_user_$userId' name='broadsoft_usersRow_$userId' class='broadsoft_users $cssAddClass broadsoft_usersRow_$userId rowHoverEffect$rowIndex' data-class='rowHoverEffect$rowIndex'>" . $checkBoxStr . "";
				}
				
				$usersHtml .= "<td>" . $userId . "</td>";
				$usersHtml .= "<td>" . $phoneNumberStatus . "</td>";
                                
                $polycomPhoneService =  checkPolyComPhoneServices($servicePackArr, $deviceType, $ociVersion);									
                /* Navigate to Modify User functionality applied on <tr>               
				$usersHtml .= "<tr id='broadsoft_user_$userId' name='broadsoft_usersRow_$userId' class='broadsoft_users $cssAddClass broadsoft_usersRow_$userId'>" . $checkBoxStr . "<td>" . $userIdVal . "</td><td>" . $phoneNumberStatus . "</td>";
				*/
				
				$usersHtml .= "<td>" . $isUnRegUserSasTester . "</td>";
				$usersHtml .= "<td>" . $userName . "</td>";
				$usersHtml .= "<td>" . $phoneNumber1 . "</td>";
				$usersHtml .= "<td>" . $extension . "</td>";
				$usersHtml .= "<td>" . $deviceName . "</td>";
				$usersHtml .= "<td>" . $deviceType . "</td>";
				$usersHtml .= "<td>" . $portNumber . "</td>";
				$usersHtml .= "<td>" . $macAddress . "</td>";
				$usersHtml .= "<td>" . $department . "</td>";
				$usersHtml .= "<td>" . $servicePack . "</td>";
				$usersHtml .= "<td>&nbsp;</td>";
				$usersHtml .= "<td>" . $epType . "</td>";
				$usersHtml .= "<td>" . $DnD . "</td>";
				$usersHtml .= "<td>" . $Fwd . "</td>";
				$usersHtml .= "<td>" . $FwdTo . "</td>";
				$usersHtml .= "<td>" . $clidBlock . "</td>";
				$usersHtml .= "<td>" . $simRing . "</td>";
				$usersHtml .= "<td>" . $remote . "</td>";
				$usersHtml .= "<td>" . $remote_number . "</td>";
				$usersHtml .= "<td>" . $polycomPhoneService . "</td>";
				$usersHtml .= "<td>" . $customContactDirectory . "</td></tr>";
				++ $rowIndex;
			}
		}
	}
	$usersHtml .= "</tbody></table>";
	return $usersHtml;
}

function createHtmlExpress($users){
	
	$usersHtml = '<table id="allUsersTableEx" class="stripe row-border order-column customDataTable" cellspacing="0" width="100%">
							<thead>
							<tr>
								<th class="col1 triggerColumn">&nbsp;</th>
								<th class="col2">User Id</th>
                                                                <th class="col3">Activated</th>                                                                
								<th class="col4">Registered</th>
								<th class="col5">User Name</th>
								<th class="col6">Extension</th>
								<th class="col7">Device Name</th>
								<th class="col8">Device Type</th>
								<th class="col9" >Analog Port Assignment</th>
								<th class="col10">MAC Address</th>
								<th class="col11">Service Pack</th>
								<th class="col13">Type</th>
								<th class="col14">DnD</th>
								<th class="col15">Fwd</th>
								<th class="col16">Fwd To</th>
								<th class="col19">Polycom Phone Services</th>
								<th class="col20">Custom Contact Directory</th>
							</tr>
							</thead>
							<tbody>';
	
	$rowIndex = 0;
	if (isset($users)) {
		
		for ($a = 0; $a < count($users); $a++) {
			$userId = $users[$a]["userId"];
			$firstName = $users[$a]["firstName"];
			$lastName = $users[$a]["lastName"];
			$userName = $users[$a]["firstName"] . " " . $users[$a]["lastName"];
			$extension = $users[$a]["extension"];
			$DnD = $users[$a]["DnD"];
			$Fwd = $users[$a]["Fwd"]["active"];
			$remote = $users[$a]["remote"]["active"];
			$department = $users[$a]["department"];
			$servicePack = isset($users[$a]["servicePack"]) ? implode(", ", $users[$a]["servicePack"]) : "";
			$servicePackArr = $users [$a] ["servicePack"];
                        $customContactDirectory = $users [$a] ["customContactDirectory"]; 
                        
                        
			if ($DnD == "false") {
				$DnD = "Off";
			} else {
				$DnD = "On";
			}
			if ($Fwd == "true") {
				$Fwd = "Active";
				$FwdTo = $users[$a]["Fwd"]["FwdTo"];
			} else {
				if (isset($users[$a]["Fwd"]["FwdTo"]) && $users[$a]["Fwd"]["FwdTo"] != "") {
					$Fwd = "Not Active";
					$FwdTo = $users[$a]["Fwd"]["FwdTo"];
				} else {
					$Fwd = "&nbsp;";
					$FwdTo = "&nbsp;";
				}
			}
			if ($remote == "true") {
				$remote = "*";
				$remote_number = $users[$a]["remote"]["number"];
			} else {
				$remote = "&nbsp;";
				$remote_number = "&nbsp;";
			}
			
			$fileName = "/var/www/SASTestingUser/SASTestUsers.csv";
			if (file_exists($fileName)) {
				$objcsv = new UserOperations();
				$searchInCsv = $objcsv->find_user_in_csv($fileName, $userId);
			} else {
				$searchInCsv = "";
			}
			
			// get user dn number activation response
			$userDn = new Dns ();
			$numberActivateResponse = $userDn->getUserDNActivateListRequest($userId);
			// echo "<pre>"; print_r($numberActivateResponse); die;
			$phoneNumberStatus = "";
			$userPhoneNumber = "";
			if (empty ($numberActivateResponse ['Error'])) {
				if (!empty ($numberActivateResponse ['Success'] [0] ['phoneNumber'])) {
					$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
				}
				if (!empty ($numberActivateResponse ['Success'] [0] ['status'])) {
					$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];

				}
			}
			
			if (isset($users[$a]["registration"]) and count($users[$a]["registration"]) > 0) {
				for ($b = 0; $b < count($users[$a]["registration"]); $b++) {
					$deviceName = $users[$a]["registration"][$b]["deviceName"];
					$expiration = $users[$a]["registration"][$b]["expiration"];
					$epType = $users[$a]["registration"][$b]["epType"];
					
					$deviceType = $users[$a]["registration"][$b]["deviceType"];
					$macAddress = $users[$a]["registration"][$b]["macAddress"];
					$portNumber = $users[$a]["registration"] [$b] ["portNumber"];
					
					$integration = $users[$a]["registration"][$b]["integration"];
					$ccd = $users[$a]["registration"][$b]["ccd"];
					
					$cssAddClassReg = "";
					if ($searchInCsv != "") {
						$isRegUserSasTester = "SAS";
					} else {
						$cssAddClassReg = "registerUsr";
						$isRegUserSasTester = "Yes";
					}
					
					if (isset($_SESSION["permissions"]["modifyUsers"]) and $_SESSION["permissions"]["modifyUsers"] == "1") {
						$userIdValue = "<a href='#' class='userIdVal numberList' id='" . $userId . "' data-phone='".$userPhoneNumber."' data-extension='" . $users [$a] ["extension"] . "' data-lastname='" . $users[$a]["lastName"] . "' data-firstname='" . $users[$a]["firstName"] . "'   data-extension='" .$users[$a]["extension"] . "' >" . $userId . "</a>";
					} else {
						$userIdValue = $userId;
					}
										                                        
					$polycomPhoneService =  checkPolyComPhoneServices($servicePackArr, $deviceType, $ociVersion);
					
					$usersHtml .= "<tr id='broadsoft_user_$userId' name='broadsoft_usersRow_$userId' class='broadsoft_users broadsoft_usersRow_$userId $cssAddClassReg'>";
					$usersHtml .= "<td class='user_column col_Selected col1'>";
					$usersHtml .= "<input type='checkbox' id='users_selected_$userId' title='usr_selected_$userId' class='usr_selected_$userId users_selected user_selected_chk' name='usersSelected[]' CHECKED value='" . $userId . "'><label for='users_selected_$userId'><span></span></label></td>";
					$usersHtml .= "<td>" . $userIdValue . "</td>";
					$usersHtml .= "<td>" . $phoneNumberStatus . "</td>";
					$usersHtml .= "<td>" . $isRegUserSasTester . "</td>";
					$usersHtml .= "<td>" . $userName . "</td>";
					$usersHtml .= "<td>" . $extension . "</td>";
					$usersHtml .= "<td>" . $deviceName . "</td>";
					$usersHtml .= "<td>" . $deviceType . "</td>";
					$usersHtml .= "<td>" . $portNumber . "</td>";
					$usersHtml .= "<td>" . $macAddress . "</td>";
					$usersHtml .= "<td>" . $servicePack . "</td>";
					$usersHtml .= "<td>" . $epType . "</td>";
					$usersHtml .= "<td>" . $DnD . "</td>";
					$usersHtml .= "<td>" . $Fwd . "</td>";
					$usersHtml .= "<td  class=\"_To\">" . $FwdTo . "</td>";
					$usersHtml .= "<td>" . $polycomPhoneService . "</td>";
					$usersHtml .= "<td>" . $customContactDirectory . "</td>";
					$usersHtml .= "</tr>";
					++$rowIndex;
				}
			} else {
				$deviceName = $users[$a]["deviceName"];
				$deviceType = $users[$a]["deviceType"];
				$portNumber = $users [$a] ["portNumber"];
				$macAddress = $users[$a]["macAddress"];
				$integration = $users[$a]["integration"];
				$ccd = $users[$a]["ccd"];
				$epType = $users[$a]["epType"];
				
				$cssAddClass = "usersTableColor";
				if ($searchInCsv != "")
				{
					$isUnRegUserSasTester = "SAS";
					$cssAddClass = "unRegisterUsr";
				} else
				{
					$cssAddClass = "unRegisterUsr";
					$isUnRegUserSasTester = "No";
				}
				
				
				if (isset($_SESSION["permissions"]["modifyUsers"]) and $_SESSION["permissions"]["modifyUsers"] == "1") {
					$userIdVal = "<a href='#' class='userIdVal' id='" . $userId . "' data-lastname='" . $users[$a]["lastName"] . "' data-phone='".$userPhoneNumber."' data-firstname='" . $users[$a]["firstName"] . "' data-extension='" .$users[$a]["extension"] . "' >" . $userId . "</a>";
				} else {
					$userIdVal = $userId;
				}
				
                                $polycomPhoneService =  checkPolyComPhoneServices($servicePackArr, $deviceType, $ociVersion);
                                
				$usersHtml .= "<tr id='broadsoft_user_$userId' name='broadsoft_usersRow_$userId' class='broadsoft_users $cssAddClass broadsoft_usersRow_$userId'>";
				$usersHtml .= "<td><input type='checkbox' id='users_selected_$userId' title='usr_selected_$userId' class='usr_selected_$userId users_selected user_selected_chk' name='usersSelected[]' CHECKED value='" . $userId . "'><label for='users_selected_$userId'><span></span></label></td>";
				$usersHtml .= "<td>" . $userIdVal . "</td>";
				$usersHtml .= "<td>" . $phoneNumberStatus . "</td>";
				$usersHtml .= "<td>" . $isUnRegUserSasTester . "</td>";
				$usersHtml .= "<td>" . $userName . "</td>";
				$usersHtml .= "<td>" . $extension . "</td>";
				$usersHtml .= "<td>" . $deviceName . "</td>";
				$usersHtml .= "<td>" . $deviceType . "</td>";
				$usersHtml .= "<td>" . $portNumber . "</td>";
				$usersHtml .= "<td>" . $macAddress . "</td>";
				$usersHtml .= "<td>" . $servicePack . "</td>";
				$usersHtml .= "<td>" . $epType . "</td>";
				$usersHtml .= "<td>" . $DnD . "</td>";
				$usersHtml .= "<td>" . $Fwd . "</td>";
				$usersHtml .= "<td  class=\"_To\">" . $FwdTo . "</td>";
				$usersHtml .= "<td>" . $polycomPhoneService . "</td>";
				$usersHtml .= "<td>" . $customContactDirectory . "</td></tr>";
				++$rowIndex;
			}
		}
	}
		$usersHtml .= '</tbody></table><div id="users_selected_count" class="dataTables_wrapper"></div>';
         return $usersHtml;                  

}

function createRegistrationDetails($users){
	
	$html = '<div style="zoom: 1;">';
	$html .= '<!--  <table id="usersTable1" class="stripe row-border order-column customDataTable" cellspacing="0" width="100%"> -->';
	$html .= '<table id="allUsersRegistration" class="stripe row-border order-column customDataTable allUsersRegistration" cellspacing="0" style="width: 100%;">';
	$html .= '<thead><tr><th style="width: 10%; text-align: center">&nbsp;</th><th style="width: 15%; text-align: center">Device Name</th>';
	$html .= '<th style="width: 15%; text-align: center">Device Type</th>';
	$html .= '<th style="width: 15%; text-align: center">Registration Status</th>';
	$html .= '<th style="width: 15%; text-align: center">IP</th>';
	$html .= '<th style="width: 15%; text-align: center">Expiry</th>';
	$html .= '<th style="width: 15%; text-align: center">Agent Type</th>';
	$html .= '<th class = "hiddenHeadOnRegTable" style="display:none; width: 1%; text-align: center">Hiiden Type</th>';
	$html .= '</tr></thead><tbody>';
	
	if (isset ( $_SESSION ["userDeviceList"] )) {
		unset ( $_SESSION ["userDeviceList"] );
	}
	
	require_once ("/var/www/lib/broadsoft/adminPortal/getUserRegisterationCdrReport.php");
	$urObj = new GetUserRegistrationCdrReport ();
	$userArray = $urObj->getUserData ( $users );
	$sno = 1;
	
	$fileName = "/var/www/SASTestingUser/SASTestUsers.csv";
	
	foreach ( $userArray as $key => $val ) {
		
		if (file_exists ( $fileName )) {
			$objcsv = new UserOperations ();
			$isExistInCSV = $objcsv->find_user_in_csv ( $fileName, $val ['userId'] );
		} else {
			$isExistInCSV = "";
		}
		if ($isExistInCSV != "") {
			$registrationStatus = "SAS";
		} else {
			$registrationStatus = $val ['registerStatus'];
		}
		
		$html .= "<tr style=\"background-color:#A8BEE333;\">" . "<td>" . $sno . "</td>" . "<td>" . $val ['deviceName'] . "</td>
								<td>" . $val ['deviceType'] . "</td>
								<td>" . $registrationStatus . "</td>
								<td>" . $val ['publicIp'] . "</td>
								<td>" . $val ['expiration'] . "</td>
								<td>" . $val ['agentType'] . "</td>
		                          <td style='display:none;'>&nbsp;</td></tr>";
		$sno ++;
	}
		
	$html .= '</tbody></table></div>';
	return $html;
}
?>

<script>
/*$(function() {

	//if("<?php //echo $_SESSION['usersView_selected']; ?>" == "Registration") {
		setRegisterTableSorter();
		$(".triggerColumn1").trigger("click");
	}
});*/
</script>