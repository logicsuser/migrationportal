<?php
/**
 * Created by Dipin Krishna.
 * Date: 10/16/17
 * Time: 4:59 AM
 */
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
require_once("/var/www/html/Express/config.php");
checkLogin();

if ($_SESSION["superUser"] != "1" && $_SESSION["permissions"]["expressSheets"] != "1")
{
	echo "<div style='color: red;text-align: center;'>You don't have permission to access Express Sheets.</div>";
	exit;
}

if($_SESSION["superUser"] != "1" && $_SESSION["permissions"]["deleteUsers"] != "1" && $_SESSION["permissions"]["modifyUsers"] != "1" && $_SESSION["permissions"]["addusers"] != "1") {
	echo "<div style='color: red;text-align: center;'>You don't have permission to provision Users.</div>";
	exit;
}

require_once("/var/www/lib/broadsoft/adminPortal/getGroupClid.php");
require_once("/var/www/lib/broadsoft/adminPortal/getGroupDomains.php");
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/groupCallPickup.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/expressSheetFunctions.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/config.php");
require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
$systemLevelDevice = new sysLevelDeviceOperations();

const csvFirstName = 0;
const csvLastName = 1;
const csvcallingLineIdFirstName = 2;
const csvcallingLineIdLastName = 3;
const colNameDialingFirstName = 4;
const colNameDialingLastName = 5;
const csvPhoneNumber = 6;
const csvCallerId = 7;
const csvExtension = 8;
const csvDepartment = 9;
const csvaddressLine1 = 10;
const csvaddressLine2 = 11;
const csvCity = 12;
const csvState = 13;
const csvZip = 14;
const csvtimeZone = 15;
const csvLanguage = 16;
const csvEmailAddress = 17;
const csvServicePacks = "18";
const csvUserServices = "19";
const csvDeviceName = "20";
const csvDeviceType = "21";
const csvPhoneProfile = 22;
const csvLinePortDomain = "23";
const csvMACAddress = "24";
const csvDeviceIndex = "25";
const csvPortNumber = "26";
const assignVM = "27";
const enableVM = "28";
const polycomPhoneServices = "29";
const csvDeviceAccessUserName = 30;
const csvDeviceAccessPassword = 31;
const csvCallingLineIdPolicy = 32;
const csvNetworkClassOfService = 33;
const callPickupGroup = "34";
//const phoneProfile = "32";
const tagBundles = "35";
const numOfLineApperances = "36";
const callsPerLineKey = "37";
const polycomLineRegNumber = "38";
const colLocation = 39;
const colActivateNumber = 40;
const customContactDirectory = 41;
const userIDCol = 42;
const customTagsStartIndex = 43;
const customTagsEndIndex = 82;
const customTagsAction = 83;


const customProfileTable = "customProfiles";
const customProfileName = "customProfileName";
const customProfileType = "deviceType";

$columns = array('First Name' => csvFirstName,
	'Last Name' => csvLastName,
	'Calling Line ID First Name' => csvcallingLineIdFirstName,
	'Calling Line ID Last Name' => csvcallingLineIdLastName,
	'Name Dialing First Name' => colNameDialingFirstName,
	'Name Dialing ID Last Name' => colNameDialingLastName,
	'Phone Number' => csvPhoneNumber,
	'Calling Line ID Phone Number' => csvCallerId,
	'Extension' => csvExtension,
	'Department' => csvDepartment,
	'Address' => csvaddressLine1,
	'Suite' => csvaddressLine2,
	'City' => csvCity,
	'State/Province' => csvState,
	'ZIP/Postal Code' => csvZip,
	'Time Zone' => csvtimeZone,
	'Language' => csvLanguage,
	'Email Address' => csvEmailAddress,
    'Service Packs' => csvServicePacks,
    'User Services' => csvUserServices,
	'Device Name' => csvDeviceName,
	'Device Type' => csvDeviceType,
	'Custom Profile' => csvPhoneProfile,
	'Line Port Domain' => csvLinePortDomain,
	'MAC Address' => csvMACAddress,
	'Device Index' => csvDeviceIndex,
	'Port Number' => csvPortNumber,
	'Assign and Enable VM Service' => assignVM,
	'Enable VM' => enableVM,
	'Polycom Phone Services' => polycomPhoneServices,
	'Custom Contact Directory' => customContactDirectory,
	'Device Access User Name' => csvDeviceAccessUserName,
	'Device Access Password' => csvDeviceAccessPassword,
	'Calling Line ID Policy' => csvCallingLineIdPolicy,
	'Network Class of Service' => csvNetworkClassOfService,
	'Call Pickup Group' => callPickupGroup,
	'Device Mgnt. Tag Bundles' => tagBundles,
	'Num. of Line Apperances' => numOfLineApperances,
	'Calls per LineKey' => callsPerLineKey,
	'Polycom Line Reg. Number' => polycomLineRegNumber,
	'Address Location' => colLocation
);

//Set Non Visible Columns
$hiddenColumns = array(enableVM);
//Location
if($locationFieldVisible != "true") {
	$hiddenColumns[] = colLocation;
}
//Name Dialing
if(!($ociVersion=="20" || $ociVersion=="21" || $ociVersion=="22") || $dialingNameFieldsVisible != "true"){
	$hiddenColumns[] = colNameDialingFirstName;
	$hiddenColumns[] = colNameDialingLastName;
}

//Look for Hidden Fields
$isNameDialingVisibleOnSheet = false;
$isLocationVisibleOnSheet = false;

server_fail_over_debuggin_testing(); /* for fail Over testing. */

function hasCustomProfile($deviceType, $customProfileName)
{
	global $db;

	$query = "select count(*) from " . customProfileTable . " where " . customProfileType . "='" . $deviceType . "' and " . customProfileName . "='" . $customProfileName . "'";
	//error_log($query);
	$result = $db->query($query);
	return $result->fetchColumn() > 0;
}


function getResourceForDeviceNameCreation($attr, $e)
{
	switch ($attr) {
		case ResourceNameBuilder::DN:
			return isset($e[csvPhoneNumber]) && $e[csvPhoneNumber] != "" ? $e[csvPhoneNumber] : "";
		case ResourceNameBuilder::EXT:
			return isset($e[csvExtension]) && $e[csvExtension] != "" ? $e[csvExtension] : "";
		case ResourceNameBuilder::GRP_DN:
			return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
		case ResourceNameBuilder::GRP_ID:
			return $_SESSION["groupId"];
		case ResourceNameBuilder::FIRST_NAME:
			return $e[csvFirstName];
		case ResourceNameBuilder::LAST_NAME:
			return $e[csvLastName];
		case ResourceNameBuilder::DEV_TYPE:
			return $e[csvDeviceType];
		case ResourceNameBuilder::MAC_ADDR:
			return $e[csvMACAddress];
		case ResourceNameBuilder::INT_1:
		case ResourceNameBuilder::INT_2:
			return $e[csvDeviceIndex];
		case ResourceNameBuilder::USR_ID:
			$name = isset($_SESSION["userId"]) ? $_SESSION["userId"] : "";
			if ($name == "") {
				return "";
			}
			$nameSplit = explode("@", $name);
			return $nameSplit[0];
			break;
		default:
			return "";
	}
}

/**
 * Get resource value that can be used to build User ID
 * @param $attr - resource type
 * @param $e - user information from users bulk list
 * @param string $deviceName - device name if needed to build User ID
 * @return string               - requested resource value
 */
function getResourceForUserIdCreation($attr, $e, $deviceName = "")
{
	global $surgemailDomain;

	switch ($attr) {
		case ResourceNameBuilder::DN:
			return isset($e[csvPhoneNumber]) && $e[csvPhoneNumber] != "" ? $e[csvPhoneNumber] : "";
		case ResourceNameBuilder::EXT:
			return isset($e[csvExtension]) && $e[csvExtension] != "" ? $e[csvExtension] : "";
		case ResourceNameBuilder::USR_CLID:
			return isset($e[csvCallerId]) && $e[csvCallerId] != "" ? $e[csvCallerId] : "";
		case ResourceNameBuilder::GRP_DN:
			return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
		case ResourceNameBuilder::GRP_ID:
			return str_replace(" ", "_", trim($_SESSION["groupId"]));
		case ResourceNameBuilder::SYS_DFLT_DOMAIN:
			return $_SESSION["systemDefaultDomain"];
		case ResourceNameBuilder::DEFAULT_DOMAIN:
			return $_SESSION["defaultDomain"];
		case ResourceNameBuilder::GRP_PROXY_DOMAIN:
			return $e[csvLinePortDomain];
		case ResourceNameBuilder::SRG_DOMAIN:
			return $surgemailDomain;
		case ResourceNameBuilder::FIRST_NAME:
			return $e[csvFirstName];
		case ResourceNameBuilder::LAST_NAME:
			return $e[csvLastName];
		case ResourceNameBuilder::DEV_NAME:
			return $deviceName != "" ? $deviceName : "";                        // TODO: Make sure $deviceName is passed
		case ResourceNameBuilder::MAC_ADDR:
			return strlen($e[csvMACAddress]) == 12 ? $e[csvMACAddress] : "";
		case ResourceNameBuilder::ENT:
			return strtolower(trim($_SESSION["sp"]));
		case ResourceNameBuilder::USR_ID:
			$name = isset($_SESSION["userId"]) ? $_SESSION["userId"] : "";
			if ($name == "") {
				return "";
			}
			$nameSplit = explode("@", $name);
			return $nameSplit[0];
			break;
		default:
			return "";
	}
}

?>
<script>
    $(function () {
        $("#bulkDiv").tooltip();

        var datastring = $("form#bulkUsers").serializeArray();

        //console.log(datastring);

        $("#subBulk").click(function () {
        	pendingProcess.push("Express Sheet Provision Users");
            $("#loadingBulk").dialog("open");
            $("#loadingBulk").html("<div class=\"loading\" id=\"loading3\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
            $("#loading3").show();

            $.ajax({
                type: "POST",
                url: "expressSheets/processUsersData.php",
                data: datastring,
                success: function (result) {
                	if(foundServerConErrorOnProcess(result, "Express Sheet Provision Users")) {
    					return false;
                  	}
                    $("#loading3").hide();
                    $(".ui-dialog-buttonpane", this.parentNode).hide();
                    $("#loadingBulk").html(result);
                    $("#loadingBulk").append(returnLink);
                    // $("#loadingBulk").append("<p id='downloadUserSuccessTable'>Return to Main</p>");

                }
            });
        });

        $("#loadingBulk").dialog(
            {
                autoOpen: false,
                width: 800,
                modal: true,
                resizable: false,
                position: {my: "top", at: "top"},
                closeOnEscape: false,
                open: function (event, ui) {
                    setDialogDayNightMode($(this));
                    $(".ui-dialog-titlebar-close", ui.dialog).hide();
                    $(this).closest(".ui-dialog").find(".ui-dialog-titlebar-close").addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only").html("<span class='ui-button-icon-primary ui-icon ui-icon-closethick displayBlock'></span>");
                    $("#loadingBulk").dialog("option", "title", "Request Complete");
                }
            });

    });

</script>

<?php

$fields = array(
	"0" => array("firstName", "First Name"),
	"1" => array("lastName", "Last Name"),
	"2" => array("callingLineIdFirstName", "Calling Line ID First Name"),
	"3" => array("callingLineIdLastName", "Calling Line ID Last Name"),
	"4" => array("nameDialingFirstName", "Name Dialing First Name"),
	"5" => array("nameDialingLastName", "Name Dialing Last Name"),
	"6" => array("phoneNumber", "Phone Number"),
	"7" => array("callingLineIdPhoneNumber", "Calling Line ID Phone Number"),
	"8" => array("extension", "Extension"),
	"9" => array("department", "Department"),
	"10" => array("addressLine1", "Address"),
	"11" => array("addressLine2", "Suite"),
	"12" => array("city", "City"),
	"13" => array("state", "State/Province"),
	"14" => array("zip", "ZIP/Postal Code"),
	"15" => array("timeZone", "Time Zone"),
	"16" => array("language", "Language"),
	"17" => array("emailAddress", "Email Address"),
	"18" => array("servicePack", "Service Packs"),
    "19" => array("userServices", "User Services"),
	"20" => array("deviceName", "Device Name"),
	"21" => array("deviceType", "Device Type"),
	"22" => array("phoneProfile", "Custom Profile"),
	"23" => array("linePortDomain", "Line Port Domain"),
	"24" => array("macAddress", "MAC Address"),
	"25" => array("deviceIndex", "Device Index"),
	"26" => array("portNumber", "Port Number"),
	"27" => array("voiceMessaging", "Assign and Enable VM Service"),
	"28" => array("enableVM", "Enable VM"),
	"29" => array("polycomPhoneServices", "Polycom Phone Service"),
	"30" => array("deviceAccessUserName", "Device Access User Name"),
	"31" => array("deviceAccessPassword", "Device Access Password"),
	"32" => array("callingLineIdPolicy", "Calling Line ID Policy"),
	"33" => array("networkClassOfService", "Network Class of Service"),
	"34" => array("callPickupGroup", "Call Pickup Group"),
	"35" => array("tagBundles", "Device Mgnt. Tag Bundles"),
	"36" => array("numOfLineAppearances", "Num. of Line Appearances"),
	"37" => array("callsPerLineKey", "Calls per LineKey"),
	"38" => array("polycomLineRegNumber", "Polycom Line Reg. Number"),
	"39" => array("addressLocation", "Location"),
	"40" => array("uActivateNumber", "Activate Number"),
	"41" => array("customContactDirectory", "Custom Contact Directory"),
	"42" => array(".", "User ID"),
);



$exp = array();

//echo error_log(print_r($_FILES, true));

if (file_exists($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

	$tmp_file_name = tempnam(sys_get_temp_dir(), 'BULK');

	$file_type = $_FILES['file']['type'];

	$path = $_FILES['file']['name'];
	$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

	//print_r($_FILES['file']);

	move_uploaded_file(
		$_FILES['file']['tmp_name'], $tmp_file_name
	);

	if (file_exists($tmp_file_name) && ($file_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $ext == "xlsx" || $ext == "xlsm")) {

		try {
			/** PhpSpreadsheet */
			require_once $_SERVER['DOCUMENT_ROOT'] . "/../vendor/phpoffice/phpspreadsheet/src/Bootstrap.php";

			$objReader = PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
			//Load the file
			$objReader = $objReader->load($tmp_file_name);
			//Get number of Sheets
			$sheetCount = $objReader->getSheetCount();
			//Set the Last Sheet as Active
			
			$uploadedSheetVersion = getUploadedSheetVersion($objReader, $sheetNumber = 0);
			
			$objReader->setActiveSheetIndex($sheetCount - 1);
			//Read the data into an array
			$sheet = $objReader->getActiveSheet();
			$maxCell = $sheet->getHighestRowAndColumn();

			$max_col = $maxCell['column'];

			//Check For Visibility of columns
			if($sheet->getColumnDimension(getColumnNameFromNumber(colNameDialingFirstName + 1))->getVisible()) {
				$isNameDialingVisibleOnSheet = true;
			}
			if($sheet->getColumnDimension(getColumnNameFromNumber(colLocation + 1))->getVisible()) {
				$isLocationVisibleOnSheet = true;
			}

			/*
			//Check for the Compatibility of the Sheet and the Visible Columns
			if(
				($isLocationVisibleOnSheet == in_array(colLocation, $hiddenColumns)) ||
				($isNameDialingVisibleOnSheet == in_array(colNameDialingFirstName, $hiddenColumns))
			) {
				?>
				<div id="bulkDiv" class="well table-responsive">
					<div class="alert alert-danger">This version of this file is not compatible. Please generate a new sheet to provision.</div>
				</div>
				<?php
				exit;
			}
			*/
			//Check for the Compatibility of the Sheet and the Visible Columns
			if( $uploadedSheetVersion != "Rev." . $express_sheet_revision['Users']) {
			    ?>
				<div id="bulkDiv" class="well table-responsive">
					<div class="alert alert-danger">This version of this file is not compatible. Please generate a new sheet to provision.</div>
				</div>
				<?php
				exit;
			}
			
			$header_row = $sheet->rangeToArray("A1:$max_col" . 1, null, true, false);
			//print_r($header_row);
			$header_col_num = 1;
			$header_error = '';
			foreach ($fields as $field) {

				$column_header = $header_row[0][$header_col_num];
				$column_header = str_replace("\n", " ", $column_header);

				if($field[1] != $column_header) {
					//echo "'" . $field[1] . "'" . " != " . "'" . $header_row[0][$header_col_num] . "'" . "<br/>";
					$header_error .= "Column $header_col_num should be \"{$field[1]}\", but your sheet has \"" . $column_header . "\"<br/>";
				}
				$header_col_num++;
			}

			if($header_error) {
				?>
				<div class="alert alert-danger">
					<h2>Column Name Mismatch</h2>
					<?php echo $header_error ?>
				</div>
				<?php
				exit;
			}

			for ($row = 2; $row <= $maxCell['row']; $row++) {
				$sheet_row = $sheet->rangeToArray("A$row:$max_col$row", null, true, false);
				if ($sheet_row && count($sheet_row) > 0) {
					if (!array_filter($sheet_row[0])) {
						break;
					}

					//echo( print_r($sheet_row, true) );
					if(count($sheet_row[0]) > 0 && (strtolower($sheet_row[0][0]) == "add" || strtolower($sheet_row[0][0]) == "delete" || strtolower($sheet_row[0][0]) == "modify"
							|| strtolower($sheet_row[0][customTagsAction + 1]) != "")) {

						if ($_SESSION["superUser"] != "1"
							&& (
								(
									($_SESSION["permissions"]["deleteUsers"] != "1" && strtolower($sheet_row[0][0]) == "delete")
									|| ($_SESSION["permissions"]["modifyUsers"] != "1" && strtolower($sheet_row[0][0]) == "modify")
									|| ($_SESSION["permissions"]["addusers"] != "1" && strtolower($sheet_row[0][0]) == "add")
								)
								&&
								!(
									$_SESSION["permissions"]["modifyUsers"] == "1"
									&& (strtolower($sheet_row[0][0]) == "add" || strtolower($sheet_row[0][userIDCol + 1]) != "")
									&& strtolower($sheet_row[0][customTagsAction + 1]) != ""
								)
							)
						) {
							continue;
						}

						//array_shift($sheet_row[0]);
						if (!array_filter($sheet_row[0])) {
							break;
						}
						$exp[] = $sheet_row[0];
					}
				}
			}

		} catch (Exception $e) {
			error_log($e->getMessage());
		}

		//echo count($exp);
		//echo "<pre>";
		//print_r($exp);
		//echo "</pre>";
		//exit;

		//Remove the Header
		//if (count($exp) > 0) {
		//	array_shift($exp);
		//}

	}

}

$textFields = array(
	csvFirstName, csvLastName,
	csvcallingLineIdFirstName, csvcallingLineIdLastName,
	csvDepartment
);
$requiredFields = array(
	csvFirstName, csvLastName, csvcallingLineIdFirstName,
	csvcallingLineIdLastName, csvExtension, csvtimeZone,
	assignVM, polycomPhoneServices, csvCallingLineIdPolicy
);
if ($portalRequiresClidNumber == "true") {
	$requiredFields[] = csvCallerId;
}
if ($bwRequireAddress == "true") {
	$requiredFields[] = csvaddressLine1;
	$requiredFields[] = csvCity;
	$requiredFields[] = csvState;
	$requiredFields[] = csvZip;
}
//echo "<pre>"; print_r($exp);

$userJsonArray = array();
?>

<script>
    $(function(){
        var downloadExpressCsv = function(){
            var myTableArray = {};

            $("table#usersTableOutput tr:not('[class*=excelRow]')").each(function() {
                var thArrayOfThisRow = [];
                var tableThData = $(this).find('th');
                if (tableThData.length > 0) {
                    tableThData.each(function() {
                        var thData = $(this);
                        thArrayOfThisRow.push(thData.text());
                    });
                    myTableArray["columnArray"] = thArrayOfThisRow;
                    $("#columnArray").val(myTableArray["columnArray"]);
                    console.log(myTableArray["columnArray"]);
                }
            });

            var myTableValueArray = [];
            var myTableErrorArray = [];
            var i = 0;

            $("#bulkUsers table#usersTableOutput tr.errorBad").each(function(i) {
                var tdValueArray = [];
                var tdErrorArray = [];
                var s=0;
                var tableTdData = $(this).find('td');
                tableTdData.each(function(s) {
                    tdValueArray[s] = $(this).find("input").val();
                    tdErrorArray[s] = $(this).find("input").attr("title");
                    s++;
                });
                myTableValueArray[i] = tdValueArray;
                myTableErrorArray[i] = tdErrorArray;
                i++;
            });
            var jsonV = JSON.stringify(myTableValueArray);
            var jsonE = JSON.stringify(myTableErrorArray);
            $("#valueArray").val(jsonV);
            $("#errorArray").val(jsonE);

            $("#submitErrorForm").submit();

        }

        $("#showDialogueValidate").dialog({
            autoOpen: false,
            width: 800,
            modal: true,
            position: { my: "top", at: "top" },
            resizable: false,
            closeOnEscape: false,

            buttons: {
                "Download erroneous records as XLSX": function() {
                    downloadExpressCsv();
                },
                "Show All Records": function(){
                    tableHtml = $("#bulkUsers").html();
                    $("#showDialogueValidate").html(tableHtml);
                    $("#showDialogueValidate excelRow").hide();
                    $("#showDialogueValidate errorBad").show();
                    buttonsShowHide('Show All Records', 'hide');
                    buttonsShowHide('Provision', 'hide');

                    $(document).find("#showDialogueValidate").find("#usersTableOutput").find("#subBulk").removeClass("submitBulkClass").addClass("hideSubBulk");

                    var errorBadLength = $("#bulkUsers .errorBad").length;
                    //Added by Anshu @12-02-2018
                    if(errorBadLength > 0){
                        $(document).find("#showDialogueValidate").find("#usersTableOutput").find("#subBulk").hide();
                    }
                    //End Code

                    if(errorBadLength > 0){
                        buttonsShowHide('Show Records With Errors Only', 'show');
                    }else{
                        buttonsShowHide('Show Records With Errors Only', 'hide');
                    }
                },
                "Show Records With Errors Only": function(){
                    if ($(".errorBad")[0]){
                        tableHtml = $("#bulkUsers").html();
                        $("#showDialogueValidate").html(tableHtml);
                        $("#showDialogueValidate .excelRow").hide();
                        $("#showDialogueValidate .errorBad").show();
                        buttonsShowHide('Show Records With Errors Only', 'hide');
                        buttonsShowHide('Show All Records', 'show');
                        $(document).find("#showDialogueValidate").find("#usersTableOutput").find("#subBulk").removeClass("submitBulkClass").addClass("hideSubBulk");
                        $(document).find("#showDialogueValidate").find("#usersTableOutput").find("#subBulk").hide(); //Added by Anshu @12-02-2018
                    }
                },
                "Provision": function(){
                    //debugger;
                    $(document).find("#showDialogueValidate").find("#usersTableOutput").find("#subBulk").removeClass("submitBulkClass");
                    $(this).dialog("close");
                    $(".submitBulkClass").trigger("click");
                },
                 "Cancel": function() {
                    $(this).dialog("close");
                    //$("#loading2").hide();
                }
            },
            open: function() {
                setDialogDayNightMode($(this));
                $(":button:contains('Show All Records')").addClass("cancelButton").show();
                $(":button:contains('Show Records With Errors Only')").addClass("cancelButton").show();
                $(":button:contains('Download erroneous records as XLSX')").addClass("cancelButton").show();
                $(":button:contains('Provision')").addClass("cancelButton").show();
                $(":button:contains('Cancel')").addClass("cancelButton addNewLineCancelBtn").show();

                buttonsShowHide('Show All Records', 'show');
                buttonsShowHide('Show Records With Errors Only', 'hide');
                buttonsShowHide('Download erroneous records as XLSX', 'hide');
                $(this).closest(".ui-dialog").find(".ui-dialog-titlebar-close").addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only").html("<span class='ui-button-icon-primary ui-icon ui-icon-closethick'></span>");

            }
        });

        var buttonsShowHide = function(b,e){
            if(e == "show"){
                $(".ui-dialog-buttonpane button:contains("+b+")").button().show();
            }else{
                $(".ui-dialog-buttonpane button:contains("+b+")").button().hide();
            }
        };

        $("#showDialogueValidate").find("#usersTableOutput").find("#submitProvision").find("td").find("#subBulk").click(function(){
            $("#bulkUsers table#usersTableOutput tr#submitProvision").find("td").find("#subBulk").trigger("click");
        });

        $(document).on("click","#showDialogueValidate #usersTableOutput #submitProvision #subBulk",function(e){
            // do stuff
            //$("#submitProvision input").trigger("click");
            $(".submitBulkClass").trigger("click");
        });

        var loadDialogueValidate = function(){

            $("#bulkDiv").hide();
            var excelRowLength = $("#bulkUsers .excelRow").length;
            $("#showDialogueValidate").html('');
            var badLength;
            var goodLength;

            $(".excelRow").each(function () {
                var el = $(this);

                badLength = el.find("td.bad").length;
                if (badLength > 0){
                    el.addClass('errorBad');
                }else{
                    el.addClass('errorGood');
                }
            });

            var errorBadLength = $("#bulkUsers .errorBad").length;
            var errorGoodLength = $("#bulkUsers .errorGood").length;

            $("#showDialogueValidate").dialog("option", "title", "Request Complete");
            $("#showDialogueValidate").dialog("open");

            if(errorBadLength > 0){
                $("#showDialogueValidate").html('Express Sheets has attempted to provision '+ excelRowLength +' records, but detected '+ errorBadLength +' records with erroneous data. All errors must be corrected in the spreadsheet and spreadsheet resubmitted.');
                buttonsShowHide('Show Records With Errors Only', 'show');
                buttonsShowHide('Download erroneous records as XLSX', 'show');
                buttonsShowHide('Provision', 'hide');
            }else{
                $("#showDialogueValidate").html('Express Sheets has successfully validated '+ errorGoodLength +' records.');
                buttonsShowHide('Provision', 'show');
                buttonsShowHide('Show Records With Errors Only', 'hide');
                buttonsShowHide('Download erroneous records as XLSX', 'hide');
            }
            $("#showDialogueValidate table#usersTableOutput tr#submitProvision").hide();

        };
        loadDialogueValidate();
    });
</script>
<div id="bulkDiv" class="well table-responsive">
	<form name="bulkUsers" id="bulkUsers" method="POST" action="#">
		<table align="center" class="table tablesorter table-bordered table-striped dataTable" style="width:96%;" id="usersTableOutput">
			<thead>
			<th class="thsmallExSheet">Action</th>
			<?php
			foreach ($fields as $key => $headers) {
				if(in_array($key, $hiddenColumns)) {
					continue;
				}
				echo "<th class=\"thsmallExSheet\" style=\"width:5%;\">" . $headers[1] . "</th>";
			}
			?>
			</thead>
			<?php
			require_once("/var/www/lib/broadsoft/login.php");
			require_once("/var/www/lib/broadsoft/adminPortal/getAvailableNumbers.php");
			require_once("/var/www/lib/broadsoft/adminPortal/allNumbers.php");
			require_once("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");
			require_once("/var/www/lib/broadsoft/adminPortal/getAllDevices.php");
			require_once("/var/www/lib/broadsoft/adminPortal/getDepartments.php");
			require_once("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");
			require_once("/var/www/lib/broadsoft/adminPortal/getTimeZones.php");
			require_once("/var/www/lib/broadsoft/adminPortal/getNetworkClassesOfService.php");
			require_once("/var/www/lib/broadsoft/adminPortal/getLanguages.php");
			require_once("/var/www/lib/broadsoft/adminPortal/getDevices.php");
			require_once($_SERVER['DOCUMENT_ROOT'] . '/../lib/broadsoft/adminPortal/getDeviceList.php');
			require_once($_SERVER['DOCUMENT_ROOT'] . '/../lib/broadsoft/adminPortal/getDevicesR19.php');

			//BUILD VARIABLES for username, lineport, etc.
			require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
			require_once("/var/www/lib/broadsoft/adminPortal/UserIdNameBuilder.php");
			require_once("/var/www/lib/broadsoft/adminPortal/LineportNameBuilder.php");
			require_once("/var/www/lib/broadsoft/adminPortal/DeviceNameBuilder.php");
			require_once("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
			require_once $_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/UserServiceGetAssignmentList.php";
			require_once("/var/www/lib/broadsoft/adminPortal/services/Services.php");
			$sipGatewayLookup = new DBLookup($db, "systemDevices", "deviceType", "phoneType");
			// Build devices lists:  customDevices, deviceTypesDigitalVoIP, analogGatewayDevices
			//$deviceTypesAnalogGateways = array();
			//$deviceTypesDigitalVoIP = array();

			/*$sipGatewayLookup = new DBLookup($db, "devices", "deviceName", "sipGateway");

			// Get Custom Devices List from DB.
			$customDevices = array ();
			if ($useCustomDeviceList == "true") {
				$query = "select deviceName from customDeviceList";
				$result = $db->query ( $query );

				while ( $row = $result->fetch () ) {
					$customDevices [] = $row ["deviceName"];
				}
			}

			foreach ( $nonObsoleteDevices as $key => $value ) {
				// shortcut custom device list for Audio Codes until they are resolved
				$deviceIsAudioCodes = substr ( $value, 0, strlen ( "AudioCodes-" ) ) == "AudioCodes-";

				if ($useCustomDeviceList == "true" && ! in_array ( $value, $customDevices )) {
					if (! $deviceIsAudioCodes) {
						continue;
					}
				}
				if ($sipGatewayLookup->get ( $value ) == "true" || $deviceIsAudioCodes) {
					$deviceTypesAnalogGateways [] = $value;
				} else {
					$deviceTypesDigitalVoIP [] = $value;
				}
			}*/
			
			$userServicesAvailableOnGroup = array();
			$serviceObj = new Services();
			$groupServiceInfo = $serviceObj->getGroupServicePackAndUserServices($_SESSION['sp'], $_SESSION['groupId']);
			if( empty($groupServiceInfo["Error"])) {
			    $userServicesAvailableOnGroup = $groupServiceInfo["Success"]["userServices"];
			}
			
			require_once ("/var/www/html/Express/util/getAuthorizeDeviceTypes.php");
			asort($deviceTypesAnalogGateways);
			asort($deviceTypesDigitalVoIP);
			asort($nonObsoleteDevices);

			$a = 0;
			foreach ($userCheck->command->userTable->row as $key => $value) {
				$extensions[$a] = strval($value->col[10]);
				$a++;
			}

			$servicePackUserServices = array();

			foreach ($servicePacks as $ServicePackName) {
				$services = getServicePackUserServices($_SESSION["sp"], $ServicePackName);
				$servicePackUserServices[$ServicePackName] = $services;
			}
			//error_log('$servicePacks: ' . print_r($servicePacks, true));
			//error_log('$servicePackUserServices: ' . print_r($servicePackUserServices, true));

			$devicesPortSupports = array();

			echo "<input type=\"hidden\" name=\"groupId\" id=\"groupId\" value=\"" . $_SESSION["groupId"] . "\">
					<input type=\"hidden\" name=\"sp\" id=\"sp\" value=\"" . $_SESSION["sp"] . "\">";

			if (count($exp) == "0") {
				//if there are no entries in the .csv file, generate an error
				$error = "1";
			} else {
				//retrieve default domain
				$defaultDomain = $_SESSION["defaultDomain"];

				foreach ($exp as $e) {

					//Pop the action
					$action = $e[0];
					array_shift($e);

					echo "<tr class='excelRow'>";

					$sipGateway = "false";

					$hasDeviceType = $e[csvDeviceType] != "";
					$hasDeviceName = $e[csvDeviceName] != "";
					$hasDeviceIndex = $e[csvDeviceIndex] != "";

					$deviceType = $e[csvDeviceType];
					$deviceName = "";
					$deviceIndex = "";
					$derivedDeviceAccessUserName = "";
					$derivedDeviceAccessPassword = "";

					if($hasDeviceType && !isset($devicesPortSupports[$deviceType])) {
						$devicesPortSupports[$deviceType] = checkDeviceSupports($deviceType);
					}

					if ($hasDeviceType) {
						// Build Device Name.
						// ----------------------------------------------------------------------------------------
						// Choose formula based on device type: DID or analog.
						// For Analog type always enforce fallback
						// For DID type, enforce fallback if there is no second criteria/formula.

						$sipGateway = $sipGatewayLookup->get($e[csvDeviceType]);
						$forceResolve = $sipGateway == "Analog" ? true : $deviceNameDID2 == "";

						if($sipGateway == "true") {

							if($hasDeviceName) {

								for ($index = 1; $index <= $numSIPGatewayInstances; $index++) {

									// create device builder... since this is for analog devices, force resolve is always set to true.
									$deviceNameBuilder = new DeviceNameBuilder($deviceNameAnalog,
										"",
										"",
										getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN, $e),
										$forceResolve);

									if (! $deviceNameBuilder->validate()) {
										// TODO: Implement resolution when Device name input formula has errors
									}

									$deviceNameBuilder->setRequiredAttribute(ResourceNameBuilder::GRP_ID, $_SESSION["groupId"]);
									$deviceNameBuilder->setRequiredAttribute(ResourceNameBuilder::DEV_TYPE, $e[csvDeviceType]);
									$deviceNameBuilder->setRequiredAttribute(ResourceNameBuilder::INT_1, $index);
									$deviceNameBuilder->setRequiredAttribute(ResourceNameBuilder::INT_2, $index);

									$deviceNameTemp = $deviceNameBuilder->getName();

									if($deviceNameTemp == $e[csvDeviceName]) {
										$deviceIndex = $index;
										$deviceName = $deviceNameTemp;

										//error_log("DeviceIndex: $deviceIndex");
										//error_log("Device Name: $deviceName");
										break;
									}

								}

							} elseif ($hasDeviceIndex) {

								$deviceNameBuilder = new DeviceNameBuilder($deviceNameAnalog,
									getResourceForDeviceNameCreation(ResourceNameBuilder::DN, $e),
									getResourceForDeviceNameCreation(ResourceNameBuilder::EXT, $e),
									getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN, $e),
									$forceResolve);

								if (!$deviceNameBuilder->validate()) {
									// TODO: Implement resolution when Device name input formula has errors
								}

								do {
									$attr = $deviceNameBuilder->getRequiredAttributeKey();
									$deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr, $e));
								} while ($attr != "");

								$deviceName = $deviceNameBuilder->getName();

								if($deviceName != "") {
									$deviceIndex = $e[csvDeviceIndex];
									//error_log("DeviceIndex: $deviceIndex");
									//error_log("Device Name: $deviceName");
								}

							}


						} else {

							$deviceNameBuilder = new DeviceNameBuilder($deviceNameDID1,
								getResourceForDeviceNameCreation(ResourceNameBuilder::DN, $e),
								getResourceForDeviceNameCreation(ResourceNameBuilder::EXT, $e),
								getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN, $e),
								$forceResolve);

							if (!$deviceNameBuilder->validate()) {
								// TODO: Implement resolution when Device name input formula has errors
							}

							do {
								$attr = $deviceNameBuilder->getRequiredAttributeKey();
								$deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr, $e));
							} while ($attr != "");

							$deviceName = $deviceNameBuilder->getName();

							if ($deviceName == "") {
								// Create Device Name builder from the second formula (DID type only).
								// At this point we know that there is second formula, because without second formula
								// name would be forcefully resolved in the first formula.

								$deviceNameBuilder = new DeviceNameBuilder($deviceNameDID2,
									getResourceForDeviceNameCreation(ResourceNameBuilder::DN, $e),
									getResourceForDeviceNameCreation(ResourceNameBuilder::EXT, $e),
									getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN, $e),
									true);

								if (!$deviceNameBuilder->validate()) {
									// TODO: Implement resolution when Device name input formula has errors
								}

								do {
									$attr = $deviceNameBuilder->getRequiredAttributeKey();
									$deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr, $e));
								} while ($attr != "");

								$deviceName = $deviceNameBuilder->getName();
							}

						}


						if($deviceName != "") {

							// Build Device Access User Name - fallback definitions are based on Device Name fallback
							// ----------------------------------------------------------------------------------------
							// Enforce fallback if there is no second criteria/formula.

							$deviceAccessUserNameBuilder = new DeviceNameBuilder($deviceAccessUserName1,
								getResourceForDeviceNameCreation(ResourceNameBuilder::DN, $e),
								getResourceForDeviceNameCreation(ResourceNameBuilder::EXT, $e),
								getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN, $e),
								$deviceAccessUserName2 == "");    // force fallback only if second criteria does not exist

							if (!$deviceAccessUserNameBuilder->validate()) {
								// TODO: Implement resolution when Device name input formula has errors
							}

							do {
								$attr = $deviceAccessUserNameBuilder->getRequiredAttributeKey();
								$deviceAccessUserNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr, $e));
							} while ($attr != "");

							$derivedDeviceAccessUserName = $deviceAccessUserNameBuilder->getName();

							if ($derivedDeviceAccessUserName == "") {
								$deviceAccessUserNameBuilder = new DeviceNameBuilder($deviceAccessUserName2,
									getResourceForDeviceNameCreation(ResourceNameBuilder::DN, $e),
									getResourceForDeviceNameCreation(ResourceNameBuilder::EXT, $e),
									getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN, $e),
									true);

								if (!$deviceAccessUserNameBuilder->validate()) {
									// TODO: Implement resolution when Device name input formula has errors
								}

								do {
									$attr = $deviceAccessUserNameBuilder->getRequiredAttributeKey();
									$deviceAccessUserNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr, $e));
								} while ($attr != "");

								$derivedDeviceAccessUserName = $deviceAccessUserNameBuilder->getName();
							}

							// Build Device Access Password - fallback definitions are based on Device Name fallback
							// ----------------------------------------------------------------------------------------
							$deviceAccessPasswordBuilder = new DeviceNameBuilder($deviceAccessPassword,
								getResourceForDeviceNameCreation(ResourceNameBuilder::DN, $e),
								getResourceForDeviceNameCreation(ResourceNameBuilder::EXT, $e),
								getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN, $e),
								true);

							if (!$deviceAccessPasswordBuilder->validate()) {
								// TODO: Implement resolution when Device name input formula has errors
							}

							do {
								$attr = $deviceAccessPasswordBuilder->getRequiredAttributeKey();
								$deviceAccessPasswordBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr, $e));
							} while ($attr != "");

							$derivedDeviceAccessPassword = $deviceAccessPasswordBuilder->getName();

						}
					}


					if($action != "")
					{

						if(strtolower($action) == 'add') {

							// Build User ID.
							// --------------------------------------------------------------------------------------------

							// Create user ID builder from the first formula.
							// Enforce fallback if there is no second criteria/formula.
							// --------------------------------------------------------
							$userId = "";
							$userIdWithoutDomain = "";
							$userIdBuilder1 = new UserIdNameBuilder($userIdCriteria1,
								getResourceForUserIdCreation(ResourceNameBuilder::DN, $e),
								getResourceForUserIdCreation(ResourceNameBuilder::EXT, $e),
								getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID, $e),
								getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN, $e),
								getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $e),
								getResourceForUserIdCreation(ResourceNameBuilder::ENT, $e),
								$userIdCriteria2 == "");    // force fallback only if second criteria does not exist

							if (!$userIdBuilder1->validate()) {
								// TODO: Implement resolution when User ID input formula has errors
							}

							do {
								$attr = $userIdBuilder1->getRequiredAttributeKey();
								$userIdBuilder1->setRequiredAttribute($attr, getResourceForUserIdCreation($attr, $e, $deviceName));
							} while ($attr != "");

							$userId = $userIdBuilder1->getName();
							$userIdWithoutDomain = $userIdBuilder1->getNameWithoutDomain();

							if ($userId == "") {
								// Create user ID builder from the second formula. At this point we know
								// that there is second formula, because without second formula name
								// would be forcefully resolved in the first formula.

								$userIdBuilder2 = new UserIdNameBuilder($userIdCriteria2,
									getResourceForUserIdCreation(ResourceNameBuilder::DN, $e),
									getResourceForUserIdCreation(ResourceNameBuilder::EXT, $e),
									getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID, $e),
									getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN, $e),
									getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $e),
									getResourceForUserIdCreation(ResourceNameBuilder::ENT, $e),
									true);

								if (!$userIdBuilder2->validate()) {
									// TODO: Implement resolution when User ID input formula has errors
								}

								do {
									$attr = $userIdBuilder2->getRequiredAttributeKey();
									$userIdBuilder2->setRequiredAttribute($attr, getResourceForUserIdCreation($attr, $e, $deviceName));
								} while ($attr != "");

								$userId = $userIdBuilder2->getName();
								$userIdWithoutDomain = $userIdBuilder2->getNameWithoutDomain();
							}

							$_SESSION["userId"] = $userId;
							$blfUser = $userIdWithoutDomain . "-blf@" . $defaultDomain;


						} else {

							$userId = $e[userIDCol];
						}

						$linePort = "";

						if ($hasDeviceType) {
							// Build Line Port.
							// ----------------------------------------------------------------------------------------

							// Create lineport name from the first formula.
							// Enforce fallback if there is no second criteria/formula.
							// --------------------------------------------------------
							$linePortBuilder1 = new LineportNameBuilder($linePortCriteria1,
								getResourceForUserIdCreation(ResourceNameBuilder::DN, $e),
								getResourceForUserIdCreation(ResourceNameBuilder::EXT, $e),
								getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID, $e),
								getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN, $e),
								getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $e),
								$linePortCriteria2 == "");  // force fallback only if second criteria does not exist

							if (!$linePortBuilder1->validate()) {
								// TODO: Implement resolution when lineport input formula has errors
							}

							do {
								$attr = $linePortBuilder1->getRequiredAttributeKey();
								$linePortBuilder1->setRequiredAttribute($attr, getResourceForUserIdCreation($attr, $e, $deviceName));
							} while ($attr != "");

							$linePort = $linePortBuilder1->getName();

							if ($linePort == "") {
								// Create lineport name builder from the second formula. At this point we know that
								// there is second formula, because without second formula name would be forcefully
								// resolved in the first formula.

								$linePortBuilder2 = new LineportNameBuilder($linePortCriteria2,
									getResourceForUserIdCreation(ResourceNameBuilder::DN, $e),
									getResourceForUserIdCreation(ResourceNameBuilder::EXT, $e),
									getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID, $e),
									getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN, $e),
									getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $e),
									true);

								if (!$linePortBuilder2->validate()) {
									// TODO: Implement resolution when User ID input formula has errors
								}

								do {
									$attr = $linePortBuilder2->getRequiredAttributeKey();
									$linePortBuilder2->setRequiredAttribute($attr, getResourceForUserIdCreation($attr, $e, $deviceName));
								} while ($attr != "");

								$linePort = $linePortBuilder2->getName();
							}
							$linePort = str_replace(" ", "_", $linePort);
							$lineArr = explode("@",$linePort);
							$selectedDomain = $lineArr[1];
							if($selectedDomain != $e[csvLinePortDomain]){
							    $linePort = $lineArr[0]."@".$e[csvLinePortDomain];
							}
						}


						echo "<td class=\"good\">";
						echo " <input class=\"noBorder\" type=\"text\" readonly=\"readonly\" id=\"users[" . $userId . "][action]\" value=\"" . $action . "\">";
						echo "</td>";

						$userJsonArray[$userId]['action'] = $action;

						//Find the new set of available User Services
						$servicePackArray = explode(";", $e[csvServicePacks]);
						$servicePackArray = array_unique($servicePackArray);
						//error_log('$servicePackArray: '. print_r($servicePackArray, true));
						$newAvailableUserServices = array();
						foreach ($servicePackArray as $servicePack) {
							if(in_array($servicePack, $servicePacks)) {
								if($services = $servicePackUserServices[$servicePack]) {
									$newAvailableUserServices = array_merge($newAvailableUserServices, $services);
								}
							}
						}

						//error_log(print_r($newAvailableUserServices, true));
						
						$column = 0;
						foreach ($e as $key => $value) {
							$value .= "";
							$class = "good";
							$errorMsg = "";

							$deviceTypeServiceGetRequest = "";
							$systemDeviceGetRequest = $systemLevelDevice->getSysytemDeviceTypeServiceRequest($e["18"], $ociVersion);							
							if(empty($systemDeviceGetRequest["Error"])){
								$deviceTypeServiceGetRequest = strval($systemDeviceGetRequest["Success"]->supportsPolycomPhoneServices);
							}

							if($column >= count($fields)) { break; };
							$column++;

							if(strtolower($action) == 'add') {


								if ($key == csvPhoneNumber) //phone numbers
								{
									if ($value !== "" and !in_array($value, $availableNumbers)) {
										$class = "bad";
										$error = "1";
										$errorMsg = $value . " is not an available " . $fields[$key][1] . ".";
									}
									if ($hasDeviceType) {
										if ($sipGateway == "false" && in_array($deviceName, $devices)) {
											$class = "bad";
											$error = "1";
											$errorMsg = "This number creates a device name that is already in use.";
										}
									}
									//								if (in_array($phExt, $chkUserId))
									if (isset($chkUserId) && in_array($userId, $chkUserId)) {
										$class = "bad";
										$error = "1";
										$errorMsg = "This number creates a user ID that is already in use.";
									}
								}
								if ($key == csvCallerId) //calling line ID phone numbers
								{
									if ($value !== "" and !in_array($value, $allNumbers)) {
										$class = "bad";
										$error = "1";
										$errorMsg = $value . " is not an available " . $fields[$key][1] . ".";
									}
								}
								if ($key == csvExtension) //extensions
								{
									if (isset($extensions) && in_array($value, $extensions)) {
										$class = "bad";
										$error = "1";
										$errorMsg = $fields[$key][1] . " " . $value . " already in use.";
									} else {
										if (!is_numeric($value)) {
											$error = 1;
											$class = "bad";
											$value = "Extension must be numeric.";
										}
										//min length and max length
										$minLen = intval($_SESSION['groupExtensionLength']['min']);
										$maxLen = intval($_SESSION['groupExtensionLength']['max']);

										if (strlen($value) < $minLen or strlen($value) > $maxLen) {
											$error = 1;
											$class = "bad";
											if ($minLen == $maxLen) {
												$errorMsg = "Extension must be " . $minLen . " digits.";
											} else {
												$errorMsg = "Extension must be between " . $minLen . " and " . $maxLen . " digits.";
											}
										}
									}
								}
								/*
								if ($key == csvDepartment) //departments
								{
									if (isset($departments) && !in_array($value, $departments) and strlen($value) > 0) {
										$class = "bad";
										$error = "1";
										$errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
									}
								}
								*/
								if ($key == csvtimeZone) //time zones
								{
									if (!in_array($value, $timeZones) and strlen($value) > 0) {
										$class = "bad";
										$error = "1";
										$errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
									}
								}
								if ($key == csvLanguage) //languages
								{
									if (!in_array($value, $languages) and strlen($value) > 0) {
										$class = "bad";
										$error = "1";
										$errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
									}
								}
								if ($key == csvEmailAddress && $value != "") //email addresses
								{
									if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
										$class = "bad";
										$error = "1";
										$errorMsg = $value . " doesn&#39;t seem like a valid " . $fields[$key][1] . ".";
									}
								}
								if ($key == csvServicePacks) //service packs
								{
									$servicePackArray = explode(";", $value);
									$servicePackArray = array_unique($servicePackArray);
									// echo "ss<pre>"; print_r($servicePackArray);
									for ($i = 0; $i < count($servicePackArray); $i++) {
										if (!empty($servicePackArray[$i])) {
											if (!in_array($servicePackArray[$i], $servicePacks)) {
												$class = "bad";
												$error = "1";
												$errorMsg = $servicePackArray[$i] . " is not a valid " . $fields[$key][1] . ".";
												break;
											}
										} else {
											$class = "good";
											$error = "0";
										}
									}
									$value = implode(";", $servicePackArray);
									//echo "Jeet".$class."==".$error."++"; die;
								}

								if ($key == csvUserServices) // user services
								{
								    $userServicesArray = explode(";", $value);
								    $userServicesArray = array_unique($userServicesArray);
								    // echo "ss<pre>"; print_r($servicePackArray);
								    for ($i = 0; $i < count($userServicesArray); $i++) {
								        if (!empty($userServicesArray[$i])) {
								            if (!in_array($userServicesArray[$i], $userServicesAvailableOnGroup)) {
								                $class = "bad";
								                $error = "1";
								                $errorMsg = $userServicesArray[$i] . " is not a valid " . $fields[$key][1] . ".";
								                break;
								            }
								        } else {
								            $class = "good";
								            $error = "0";
								        }
								    }
								    $value = implode(";", $userServicesArray);
								    //echo "Jeet".$class."==".$error."++"; die;
								}
								
								if ($hasDeviceType) {
									if ($key == csvDeviceType) //device types
									{
										if (!in_array($value, $nonObsoleteDevices)) {
											$class = "bad";
											$error = "1";
											$errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
										}

									}
									if ($key == csvPhoneProfile) { //custom profiles
										$hasPhoneProfileValue = getPhoneProfileValue($value);
										if ($value != "" && $value != "None" && $e[csvDeviceType] != "" && !$hasPhoneProfileValue) {
											$class = "bad";
											$error = "1";
											$errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
										}
									}
									if ($key == csvMACAddress && $value != 0) //MAC addresses
									{
										if (!preg_match("/^[ A-Fa-f0-9]+$/", $value) and strlen($value) > 0) {
											$class = "bad";
											$error = "1";
											$errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
										}
										if (strlen($value) > 0 and strlen($value) != "12") {
											$class = "bad";
											$error = "1";
											$errorMsg = $value . " is not 12 characters.";
										}

										$xmlinput = xmlHeader($sessionid, "SystemAccessDeviceGetAllRequest");
										$xmlinput .= "<searchCriteriaDeviceMACAddress>
	                                        <mode>Equal To</mode>
	                                        <value>" . $value . "</value>
	                                        <isCaseInsensitive>true</isCaseInsensitive>
	                                    </searchCriteriaDeviceMACAddress>";
										$xmlinput .= xmlFooter();
										$response = $client->processOCIMessage(array("in0" => $xmlinput));
										$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

										$count = count($xml->command->accessDeviceTable->row);
										if ($count > 0) {
											$deviceName = strval($xml->command->accessDeviceTable->row->col[3]);
											$usedByGroup = strval($xml->command->accessDeviceTable->row->col[2]);
											$usedBySP = strval($xml->command->accessDeviceTable->row->col[0]);
											$class = "bad";
											$error = "1";
											$errorMsg = $value . " used by " . $deviceName . " in Group " . $usedByGroup . " in SP " . $usedBySP . ".";
										}
									}
									if($sipGateway == "true") {
										if ($key == csvDeviceName) { // Device Name
											if (!strlen($value)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Device Name is required for analog users.";
											} elseif(strlen($value) > 0 && $deviceName != $value) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Not a valid Device Name for Device Type '{$e[csvDeviceType]}''.";
											} elseif(!strlen($value)) {
												$value = $deviceName;
											}
										}
										if ($key == csvDeviceIndex) { // Device Index
											$value = $deviceIndex;
											/*
											if (!$hasDeviceName && !strlen($value)) {

												$class = "bad";
												$error = "1";
												$errorMsg = "Device Name or Device Index is required for analog users.";

											} elseif ($hasDeviceName && strlen($value) > 0 && $value != 0 && $deviceIndex != $value) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Device Index doesn't match the Device Name selected.";

											} elseif (!$hasDeviceName && (!ctype_digit($value . '') || (int)$value < 0)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Expected 0 or a non-negative number";
											} elseif( !strlen($value) || $value == 0 ) {
												$value = $deviceIndex;
											}
											*/
										}
										if ($key == csvPortNumber) { // Port Number
											if (strlen($value) > 0 && (!ctype_digit($value . '') || (int)$value < 0)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Expected 0 or a non-negative number";
											} elseif (strlen($value) > 0 && $value != 0) {
												// Check if it is a new device
												if(!in_array($value["deviceName"], $devices)) {

													$portDetails = getDevicePortUsage($deviceName);
													if($value > $portDetails["numberOfPorts"]) {

														$class = "bad";
														$error = "1";
														$errorMsg = "Not a valid Port for Device Name '$deviceName'";
													}
												} else {

													$availablePorts = getDeviceAvailablePorts($deviceName);
													if (!in_array($i, $availablePorts)) {
														$class = "bad";
														$error = "1";
														$errorMsg = "Port is already in use for Device Name '$deviceName'";
													}
												}
											} elseif (!strlen($value) > 0 || $value == 0) {
												$availablePorts = getDeviceAvailablePorts($deviceName);
												if(count($availablePorts)) {
													$value = array_shift($availablePorts);
												} else {
													//Check if it is a new device
													if(!in_array($value["deviceName"], $devices)) {
														$value = 1;
													} else {
														$class = "bad";
														$error = "1";
														$errorMsg = "No Ports are available for '$deviceName'";
													}
												}
											}

											//error_log("devicesPortSupports[$deviceType]: " . $devicesPortSupports[$deviceType] . " - $value");

											if($devicesPortSupports[$deviceType] == 'static' && (strlen($value) <= 0 || $value == 0)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Device type '$deviceType' is a static port device. Please choose a port.";
											}

											if($devicesPortSupports[$deviceType] != 'static' && (strlen($value) > 0 && $value > 0)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Device type '$deviceType' doesn't support static port.";
											}

										}
									} else {
										if ($key == csvPortNumber) { // Port Number

											//error_log("devicesPortSupports[$deviceType]: " . $devicesPortSupports[$deviceType] . " - $value");

											if($devicesPortSupports[$deviceType] == 'static' && (strlen($value) <= 0 || $value == 0)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Device type '$deviceType' is a static port device. Please choose a port.";
											}

											if($devicesPortSupports[$deviceType] != 'static' && (strlen($value) > 0 && $value > 0)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Device type '$deviceType' doesn't support static port.";
											}

										}
									}
								}
								if ($key == assignVM) //voice messaging/Polycom phone services
								{
								    if (strtoupper($value) == "YES") {
								        $isVMAvailable = isVMServicePackOrVMserviceAvailable($servicePackArray, $userServicesArray, $servicePacks, $userServicesAvailableOnGroup);
								    }
								    
									if (strtoupper($value) != "YES" and strtoupper($value) != "NO") {
										$class = "bad";
										$error = "1";
										$errorMsg = $fields[$key][1] . " must be either yes or no.";
									}
									else if (strtoupper($value) == "YES" && !$isVMAvailable) {
									    $class = "bad";
									    $error = "1";
									    $errorMsg = "VM services not assigned to User";
									}
								}
								if($key == polycomPhoneServices) {

									if (strtoupper($value) != "YES" and strtoupper($value) != "NO") {

										$class = "bad";
										$error = "1";
										$errorMsg = $fields[$key][1] . " must be either yes or no.";

									} else if(strtoupper($value) == "YES" && !in_array('Polycom Phone Services', $newAvailableUserServices)) {

										$class = "bad";
										$error = "1";
										$errorMsg = "None of the Service Packs selected have Phone Profile Services.";

									}else if($deviceTypeServiceGetRequest == "false" && strtoupper($value) == "YES"){

										$class = "bad";
										$error = "1";
										$errorMsg = "Polycom Phone Services is not enabled for Device Type : ". $e["18"];
									}
								}
								/*
								if($key == enableVM) {
									if (strtoupper($value) == "YES" and strtoupper($e[assignVM]) == "NO") {
										$class = "bad";
										$error = "1";
										$errorMsg = $fields[assignVM][1] . " must be set to yes to enable VM.";
									}
								}
								*/
								if ($key == csvDeviceAccessPassword) //device access password
								{
									if ($value) {
										//									$value = str_repeat("*", strlen($value)); //don't display password
									}
								}
								if ($key == csvCallingLineIdPolicy) //calling line ID policy
								{
									if (strtoupper($value) != "USER" and strtoupper($value) != "GROUP") {
										$class = "bad";
										$error = "1";
										$errorMsg = $fields[$key][1] . " must be either user or group.";
									}
								}
								if ($key == csvNetworkClassOfService) //Network Classes of Services
								{
									if($value == "None") {
										$value = "";
									}
									if (!in_array($value, $networkClassesOfService) and strlen($value) > 0) {
										$class = "bad";
										$error = "1";
										$errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
									}
								}
								if ($key == csvLinePortDomain) { // linePortDomain
									if ($value == "" && $hasDeviceType) {
										$class = "bad";
										$error = "1";
										$errorMsg = "Required field when device is specified.";
									}
									if ($value != "" && !in_array($value, $groupDomains)) {
										$class = "bad";
										$error = "1";
										$errorMsg = $fields[$key][1] . " does not match any group domain.";
									}
								}
								if ($key == callPickupGroup) {
									$groupCP = new GroupCallPickup();
									$groupCallPickupList = $groupCP->showGroupCallPickupInstanceCSV();
									if (isset($groupCallPickupList)) {
										if ($value != "" && !in_array($value, $groupCallPickupList)) {
											$class = "bad";
											$error = "1";
											$errorMsg = $fields[$key][1] . " does not exist.";
										}
									}
								}

								//Name Dialing
								if($key == colNameDialingFirstName || $key == colNameDialingLastName) {

									//Check Only if the system allows Name Dialing
									if(!in_array($key, $hiddenColumns)){

										//Make sure Name Dialing is visible on the Express Sheet
										if (!$isNameDialingVisibleOnSheet) {
											$class = "bad";
											$error = "1";
											$errorMsg = "Name Dialing is enabled on the system but not on the sheet.";
										}

									}
								}

								//Address Location
								if($key == colLocation) {

									//Check Only if the system allows Location
									if(!in_array($key, $hiddenColumns)){

										//Make sure Location is visible on the Express Sheet
										if (!$isLocationVisibleOnSheet) {
											$class = "bad";
											$error = "1";
											$errorMsg = "Location is enabled on the system but not on the sheet.";
										}

									}
								}


								if (in_array($key, $textFields) and strlen($value) > 0) {
									if (!preg_match("/^[ A-Za-z0-9\.\s-]+$/", $value)) {
										$class = "bad";
										$error = "1";
										$errorMsg = $fields[$key][1] . " has a bad character.";
									}
								}
								if (in_array($key, $requiredFields)) {
									if (strlen($value) < 1) {
										$class = "bad";
										$value = "&nbsp;";
										$error = "1";
										$errorMsg = $fields[$key][1] . " is a required field.";
									}
								}

							}
							else if (strtolower($action) == 'modify') {

								$row_value = $value;
								$row_key = $key;

								if (!isset($userInfo['userId']) || $userInfo['userId'] != $userId) {
									unset($userInfo);
									require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getUserInfo.php");
								}

								if ($row_key == csvPhoneNumber) //phone numbers
								{
									if ($row_value != $userInfo['phoneNumber']) {

										if ($row_value !== "" and !in_array($row_value, $availableNumbers)) {
											$class = "bad";
											$error = "1";
											$errorMsg = $row_value . " != " . $userInfo['phoneNumber'] . " is not an available " . $fields[$key][1] . ".";
										}
										/*
										if ($hasDeviceType) {
											if ($sipGateway == "false" && in_array($deviceName, $devices)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "This number creates a device name that is already in use.";
											}
										}
										if (isset($chkUserId) && in_array($userId, $chkUserId)) {
											$class = "bad";
											$error = "1";
											$errorMsg = "This number creates a user ID that is already in use.";
										}
										*/
									}
								}
								if ($row_key == csvCallerId) //calling line ID phone numbers
								{
									if ($row_value != $userInfo['callingLineIdPhoneNumber']) {
										if ($row_value !== "" and !in_array($row_value, $allNumbers)) {
											$class = "bad";
											$error = "1";
											$errorMsg = $row_value . " is not an available " . $fields[$row_key][1] . ".";
										}
									}
								}
								if ($row_key == csvExtension) //extensions
								{
									if ($row_value != $userInfo['extension']) {
										if (isset($extensions) && in_array($row_value, $extensions)) {
											$class = "bad";
											$error = "1";
											$errorMsg = $fields[$row_key][1] . " " . $row_value . " already in use.";
										} else {
											if (!is_numeric($row_value)) {
												$error = 1;
												$class = "bad";
												$row_value = "Extension must be numeric.";
											}
											//min length and max length
											$minLen = intval($_SESSION['groupExtensionLength']['min']);
											$maxLen = intval($_SESSION['groupExtensionLength']['max']);

											if (strlen($row_value) < $minLen or strlen($row_value) > $maxLen) {
												$error = 1;
												$class = "bad";
												if ($minLen == $maxLen) {
													$errorMsg = "Extension must be " . $minLen . " digits.";
												} else {
													$errorMsg = "Extension must be between " . $minLen . " and " . $maxLen . " digits.";
												}
											}
										}
									}
								}
								if ($key == csvServicePacks) //service packs
								{

									$servicePackArray = explode(";", $value);
									$servicePackArray = array_unique($servicePackArray);
									//echo "ss<pre>"; print_r($servicePackArray);
									for ($i = 0; $i < count($servicePackArray); $i++) {
										if (!empty($servicePackArray[$i])) {
											if (!in_array($servicePackArray[$i], $servicePacks)) {
												$class = "bad";
												$error = "1";
												$errorMsg = $servicePackArray[$i] . " is not a valid " . $fields[$key][1] . ".";
												break;
											}
										} else {
											$class = "good";
											$error = "0";
										}
									}
									$value = implode(";", $servicePackArray);
								}
								/*
								if ($row_key == csvDepartment) //departments
								{
									if($row_value != $userInfo['department']) {
										if (isset($departments) && !in_array($row_value, $departments) and strlen($row_value) > 0) {
											$class = "bad";
											$error = "1";
											$errorMsg = $row_value . " is not a valid " . $fields[$row_key][1] . ".";
										}
									}
								}
								if ($row_key == csvtimeZone) //time zones
								{
									if($row_value != $userInfo['timeZone']) {
										if (!in_array($row_value, $timeZones) and strlen($row_value) > 0) {
											$class = "bad";
											$error = "1";
											$errorMsg = $row_value . " is not a valid " . $fields[$row_key][1] . ".";
										}
									}
								}
								if ($row_key == csvLanguage) //languages
								{
									if($row_value != $userInfo['language']) {
										if (!in_array($row_value, $languages) and strlen($row_value) > 0) {
											$class = "bad";
											$error = "1";
											$errorMsg = $row_value . " is not a valid " . $fields[$row_key][1] . ".";
										}
									}
								}
								*/
								if ($row_key == csvEmailAddress && $row_value != "") //email addresses
								{
									if ($row_value != $userInfo['emailAddress']) {
										if (!filter_var($row_value, FILTER_VALIDATE_EMAIL)) {
											$class = "bad";
											$error = "1";
											$errorMsg = $row_value . " doesn&#39;t seem like a valid " . $fields[$row_key][1] . ".";
										}
									}
								}
								if ($hasDeviceType) {
									if ($key == csvDeviceType) //device types
									{
										if (!in_array($value, $nonObsoleteDevices)) {
											$class = "bad";
											$error = "1";
											$errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
										}

									}
									if ($key == csvPhoneProfile) { //custom profiles
										$hasPhoneProfileValue = getPhoneProfileValue($value);
										if ($value != "" && $value != "None" && $e[csvDeviceType] != "" && !$hasPhoneProfileValue) {
											$class = "bad";
											$error = "1";
											$errorMsg = $value . " is not a valid " . $fields[$key][1] . ".";
										}
									}
									if($sipGateway == "true") {
										if ($key == csvDeviceName) { // Device Name
											if (!strlen($row_value)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Device Name is required for analog users.";
											} elseif(strlen($row_value) > 0 && $deviceName != $row_value) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Not a valid Device Name for Device Type '{$e[csvDeviceType]}''.";
											} elseif(!strlen($value)) {
												$row_value = $deviceName;
											}
										}
										if ($key == csvDeviceIndex) { // Device Index
											$row_value = $deviceIndex;
											/*
											if (!$hasDeviceName && !strlen($row_value)) {

												$class = "bad";
												$error = "1";
												$errorMsg = "Device Name or Device Index is required for analog users.";

											} elseif ($hasDeviceName && strlen($row_value) > 0 && $row_value != 0 && $deviceIndex != $row_value) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Not a valid Device Index.";

											} elseif (!$hasDeviceName && (!ctype_digit($row_value . '') || (int)$row_value < 0)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Expected 0 or a non-negative number";
											} elseif(!strlen($value)) {
												$row_value = $deviceIndex;
											}
											*/
										}
										if ($key == csvPortNumber) { // Port Number
											if (!strlen($row_value) > 0 || !ctype_digit($row_value . '') || (int)$row_value < 0 || $row_value == 0) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Expected a non-negative number";
											} elseif (strlen($row_value) > 0 && $row_value != 0) {
												//$deviceNameTemp = $e[csvDeviceName] ? $e[csvDeviceName] : $deviceName;
												// Check if it is a new device
												if(!in_array($row_value["deviceName"], $devices)) {

													$portDetails = getDevicePortUsage($deviceName);
													if($row_value > $portDetails["numberOfPorts"]) {

														$class = "bad";
														$error = "1";
														$errorMsg = "Not a valid Port for Device Name '$deviceName'";
													}
												} else {

													$availablePorts = getDeviceAvailablePorts($deviceName);
													if (!in_array($i, $availablePorts)) {
														$class = "bad";
														$error = "1";
														$errorMsg = "Port is already in use for Device Name '$deviceName'";
													}
												}
											} elseif ($row_value == 0) {
												$availablePorts = getDeviceAvailablePorts($deviceName);
												if(count($availablePorts)) {
													$row_value = array_shift($availablePorts);
												} else {
													//Check if it is a new device
													if(!in_array($row_value["deviceName"], $devices)) {
														$row_value = 1;
													} else {
														$class = "bad";
														$error = "1";
														$errorMsg = "No Ports are available for '$deviceName'";
													}
												}
											}

											if($devicesPortSupports[$deviceType] == 'static' && (strlen($value) <= 0 || $value == 0)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Device type '$deviceType' is a static port device. Please choose a port.";
											}

											if($devicesPortSupports[$deviceType] != 'static' && (strlen($value) > 0 && $value > 0)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Device type '$deviceType' doesn't support static port.";
											}
										}
									} else {
										if ($key == csvPortNumber) { // Port Number

											if($devicesPortSupports[$deviceType] == 'static' && (strlen($value) <= 0 || $value == 0)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Device type '$deviceType' is a static port device. Please choose a port.";
											}

											if($devicesPortSupports[$deviceType] != 'static' && (strlen($value) > 0 && $value > 0)) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Device type '$deviceType' doesn't support static port.";
											}

										}
									}
								}
								/*
								if ($row_key == csvPortNumber) //service packs
								{
									$servicePackArray = explode(";", $row_value);
									for ($i = 0; $i < count($servicePackArray); $i++) {
										if (!in_array($servicePackArray[$i], $servicePacks)) {
											$class = "bad";
											$error = "1";
											$errorMsg = $servicePackArray[$i] . " is not a valid " . $fields[$row_key][1] . ".";
										}
									}
								}
								if ($hasDeviceType) {
									if ($row_key == csvDeviceType) //device types
									{
										if($row_value != $userInfo['deviceType']) {
											if (!in_array($row_value, $nonObsoleteDevices)) {
												$class = "bad";
												$error = "1";
												$errorMsg = $row_value . " is not a valid " . $fields[$row_key][1] . ".";
											}
										}
									}
									if ($row_key == csvPhoneProfile) { //custom profiles
										if($row_value != $userInfo['customProfile']) {
											if ($row_value != "" && $e[csvDeviceType] != "" && !hasCustomProfile($e[csvDeviceType], $row_value)) {
												$class = "bad";
												$error = "1";
												$errorMsg = $row_value . " is not a valid " . $fields[$row_key][1] . ".";
											}
										}
									}
									if ($row_key == csvMACAddress) //MAC addresses
									{
										if($row_value != $userInfo['macAddress']) {

											if (!preg_match("/^[ A-Fa-f0-9]+$/", $row_value) and strlen($row_value) > 0) {
												$class = "bad";
												$error = "1";
												$errorMsg = $row_value . " is not a valid " . $fields[$row_key][1] . ".";
											}
											if (strlen($row_value) > 0 and strlen($row_value) != "12") {
												$class = "bad";
												$error = "1";
												$errorMsg = $row_value . " is not 12 characters.";
											}

											$xmlinput = xmlHeader($sessionid, "SystemAccessDeviceGetAllRequest");
											$xmlinput .= "<searchCriteriaDeviceMACAddress>
												<mode>Equal To</mode>
												<value>" . $row_value . "</value>
												<isCaseInsensitive>true</isCaseInsensitive>
											</searchCriteriaDeviceMACAddress>";
											$xmlinput .= xmlFooter();
											$response = $client->processOCIMessage(array("in0" => $xmlinput));
											$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

											$count = count($xml->command->accessDeviceTable->row);
											if ($count > 0) {
												$deviceName = strval($xml->command->accessDeviceTable->row->col[3]);
												$usedByGroup = strval($xml->command->accessDeviceTable->row->col[2]);
												$usedBySP = strval($xml->command->accessDeviceTable->row->col[0]);
												$class = "bad";
												$error = "1";
												$errorMsg = $row_value . " used by " . $deviceName . " in Group " . $usedByGroup . " in SP " . $usedBySP . ".";
											}
										}
									}
									if ($row_key == csvDeviceIndex) { // Device Index
										if($row_value != $userInfo['deviceIndex']) {
											if (!ctype_digit($row_value . '') || (int)$row_value < 0) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Expected 0 or a non-negative number";
											}
										}
									}
									if ($row_key == csvPortNumber) { // Port Number
										if($row_value != $userInfo['portNumber']) {
											if (!ctype_digit($row_value . '') || (int)$row_value < 0) {
												$class = "bad";
												$error = "1";
												$errorMsg = "Expected 0 or a non-negative number";
											}
										}
									}
								}
								*/
								if ($key == assignVM) //voice messaging
								{
									if (strtoupper($value) != "YES" && strtoupper($value) != "NO") {

										$class = "bad";
										$error = "1";
										$errorMsg = $fields[$key][1] . " must be either yes or no.";

									} else {

										/*
										if($key == enableVM) {

											if (strtoupper($value) == "YES" && strtoupper($e[assignVM]) == "NO") {
												$class = "bad";
												$error = "1";
												$errorMsg = $fields[assignVM][1] . " must be set to yes to enable VM.";
											}
										}
										*/

										//Check if VM service can be removed
										if($key == assignVM
											&& (strtoupper($value) == "NO" && isset($_SESSION["userInfo"]["voiceMessagingUser"]) && $_SESSION["userInfo"]["voiceMessagingUser"] == "true") ) {

											$servicePackArray = explode(";", $e[csvServicePacks]);

											if(count($servicePackArray)) {
												
												$assignedServices = new UserServiceGetAssignmentList($userId);
												$currentServicePacks = $assignedServices->getservicePacks();
												foreach ($currentServicePacks as $servicePack => $userServices) {
													if (in_array($servicePack, $servicePackArray)) {
														if (in_array("Voice Messaging User", $userServices)) {
															if (count($userServices) > 1) {
															    $enableVM = false;
															/*	$class = "bad";
																$error = "1";
																$errorMsg = $fields[assignVM][1] . " cannot be un-assigned because it is assigned through '$servicePack' Service Pack, containing multiple services.";
																break;  */
															}
														}
													}
												}

											}

											} else if( strtoupper($value) == "YES" ) {
											    
											    $serviceObj = new Services();
											    $servicePackArray = explode(";", $e[csvServicePacks]);
											    $userServicesArray = explode(";", $e[csvUserServices]);
											    $getSevicePackWithVM = $serviceObj->getVMServicePack($servicePackArray, $_SESSION["sp"]);
											    $assignedVMInSheet = count($getSevicePackWithVM) > 0 ? true : false;
											    
											    $isVmServiceAssinged = false;
											    $assignedServices = new UserServiceGetAssignmentList($userId);
											    if ( $assignedServices->hasService("Voice Messaging User") ) {
											        $isVmServiceAssinged = true;
											    }
											    
											    $isVMAvailable = isVMServicePackOrVMserviceAvailable($servicePackArray, $userServicesArray, $servicePacks, $userServicesAvailableOnGroup);
											    
											    if( !$assignedVMInSheet && !$isVmServiceAssinged && !$isVMAvailable) {
											        $class = "bad";
											        $error = "1";
											        $errorMsg = "VM services not assigned to User";
											        //break;
											    }
											}

									}
								}
								if ($row_key == csvNetworkClassOfService) //Network Classes of Services
								{
									if($row_value == "None") {
										$row_value = "";
									}
									if (!in_array($row_value, $networkClassesOfService) and strlen($row_value) > 0) {
										$class = "bad";
										$error = "1";
										$errorMsg = $row_value . " is not a valid " . $fields[$row_key][1] . ".";
									}
								}
								/*
								if ($row_key == enableVM) //voice messaging
								{
									if($row_value != $userInfo['voiceMessaging']) {
										if (strtoupper($row_value) != "YES" and strtoupper($row_value) != "NO") {
											$class = "bad";
											$error = "1";
											$errorMsg = $fields[$row_key][1] . " must be either yes or no.";
										}
									}
								}*/

								if($row_key == polycomPhoneServices) {

									if (strtoupper($row_value) != "YES" and strtoupper($row_value) != "NO") {

										$class = "bad";
										$error = "1";
										$errorMsg = $fields[$key][1] . " must be either yes or no.";

									} else if(strtoupper($row_value) == "YES" && !in_array('Polycom Phone Services', $newAvailableUserServices)) {

										$class = "bad";
										$error = "1";
										$errorMsg = "None of the Service Packs selected have Phone Profile Services.";

									} else if($deviceTypeServiceGetRequest == "false" && strtoupper($value) == "YES"){

										$class = "bad";
										$error = "1";
										$errorMsg = "Polycom Phone Services is not enabled for Device Type : ". $e["18"];
									}
								}

								/*
								if ($row_key == csvDeviceAccessPassword) //device access password
								{
									if ($row_value) {
	//									$row_value = str_repeat("*", strlen($row_value)); //don't display password
									}
								}*/
								if ($key == callPickupGroup) {
									$groupCP = new GroupCallPickup();
									$groupCallPickupList = $groupCP->showGroupCallPickupInstanceCSV();
									if (isset($groupCallPickupList)) {
										if ($value != "" && !in_array($value, $groupCallPickupList)) {
											$class = "bad";
											$error = "1";
											$errorMsg = $fields[$key][1] . " does not exist.";
										}
									}
								}

								//Name Dialing
								if($key == colNameDialingFirstName || $key == colNameDialingLastName) {

									//Check Only if the system allows Name Dialing
									if(!in_array($key, $hiddenColumns)){

										//Make sure Name Dialing is visible on the Express Sheet
										if (!$isNameDialingVisibleOnSheet) {
											$class = "bad";
											$error = "1";
											$errorMsg = "Name Dialing is enabled on the system but not on the sheet.";
										}

									}
								}



								if (in_array($key, $textFields) and strlen($row_value) > 0) {
									if (!preg_match("/^[ A-Za-z0-9\.\s-]+$/", $row_value)) {
										$class = "bad";
										$error = "1";
										$errorMsg = $fields[$key][1] . " has a bad character.";
									}
								}
								if (in_array($key, $requiredFields)) {
									if (strlen($row_value) < 1) {
										$class = "bad";
										$row_value = "&nbsp;";
										$error = "1";
										$errorMsg = $fields[$key][1] . " is a required field.";
									}
								}

								$value = $row_value;
								$key = $row_key;

							}
							else if(strtolower($action) == 'delete') {

								if ($key == csvPhoneNumber) //phone numbers
								{
									if (isset($chkUserId) && !in_array($userId, $chkUserId)) {
										$class = "bad";
										$error = "1";
										$errorMsg = "This user doesn't exist.";
									}
								}

							}

							if(($key == numOfLineApperances || $key == callsPerLineKey) && (strtolower($action) == 'add' || strtolower($action) == 'modify')) {

								if(( (isset($value[numOfLineApperances]) && $value[numOfLineApperances] > 0) || (isset($value[callsPerLineKey]) && $value[callsPerLineKey]) > 0) &&
									( isset($value[polycomLineRegNumber]) && ($value[polycomLineRegNumber] == "" ||
											$value[polycomLineRegNumber] < 1))) {
									$class = "bad";
									$error = "1";
									$errorMsg = "Polycom Line Reg. Number cannot be empty.";
								}

							}

							if(in_array($key, $hiddenColumns)) {
								continue;
							}

							if($key == csvMACAddress && $value == 0) {
								$value = "";
							}

							echo "<td class=\"" . $class . "\">";
							echo "<input class=\"noBorder\" title=\"" . $errorMsg . "\" type=\"text\" id=\"users[" . $userId . "][" .
								$fields[$key][0] . "]\" value=\"" . $value . "\" style=\"width:98%;\" readonly=\"readonly\">";
							echo "</td>";

							$userJsonArray[$userId][$fields[$key][0]] = $value;
						}

					} else {

						$userId = $e[userIDCol];

						if($userId && $e[customTagsAction] != "") {

							$userJsonArray[$userId]['action'] = $e[customTagsAction] . ' Custom Tags';
							$userJsonArray[$userId]['firstName'] = $e[csvFirstName];
							$userJsonArray[$userId]['lastName'] = $e[csvLastName];
							$userJsonArray[$userId]['phoneNumber'] = $e[csvPhoneNumber];
							$userJsonArray[$userId]['callingLineIdPhoneNumber'] = $e[csvCallerId];
							$userJsonArray[$userId]['extension'] = $e[csvExtension];
							$userJsonArray[$userId]['deviceName'] = $e[csvDeviceName];
							$userJsonArray[$userId]['deviceType'] = $e[csvDeviceType];
							?>
							<td class="good">
								<input class="noBorder" type="text" readonly="readonly"
								       id="users[<?php echo $userId; ?>][action]"
								       value="<?php echo $e[customTagsAction]; ?> Custom Tags"/>
							</td>
							<td class="good">
								<input class="noBorder" type="text" readonly="readonly"
								       id="users[<?php echo $userId; ?>][firstName]"
								       value="<?php echo $e[csvFirstName]; ?>"/>
							</td>
							<td class="good">
								<input class="noBorder" type="text" readonly="readonly"
								       id="users[<?php echo $userId; ?>][lastName]"
								       value="<?php echo $e[csvLastName]; ?>"/>
							</td>
							<td class="good"></td>
							<td class="good"></td>
							<td class="good">
								<input class="noBorder" type="text" readonly="readonly"
								       id="users[<?php echo $userId; ?>][phoneNumber]"
								       value="<?php echo $e[csvPhoneNumber]; ?>"/>
							</td>
							<td class="good">
								<input class="noBorder" type="text" readonly="readonly"
								       id="users[<?php echo $userId; ?>][callingLineIdPhoneNumber]"
								       value="<?php echo $e[csvCallerId]; ?>"/>
							</td>
							<td class="good">
								<input class="noBorder" type="text" readonly="readonly"
								       id="users[<?php echo $userId; ?>][extension]"
								       value="<?php echo $e[csvExtension]; ?>"/>
							</td>
							<td class="good" colspan="10"></td>
							<td class="good">
								<input class="noBorder" type="text" readonly="readonly"
								       id="users[<?php echo $userId; ?>][deviceName]"
								       value="<?php echo $e[csvDeviceName]; ?>"/>
							</td>
							<td class="good">
								<input class="noBorder" type="text" readonly="readonly"
								       id="users[<?php echo $userId; ?>][deviceType]"
								       value="<?php echo $e[csvDeviceType]; ?>"/>
							</td>
							<?php

						}

					}

					if($userId && $e[customTagsAction] != "") {
						for($tagNum = customTagsStartIndex; $tagNum <= customTagsEndIndex; $tagNum++) {
							if($e[$tagNum]) {
								if(strtolower($e[customTagsAction]) == "delete") {
									echo " <input type=\"hidden\" name=\"customTags[" . $userId . "][delete][]\" value=\"" . $e[$tagNum] . "\">";
								} else {
									echo " <input type=\"hidden\" name=\"customTags[" . $userId . "][modify][". $e[$tagNum] ."]\" value=\"" . $e[++$tagNum] . "\">";
								}
							}
						}
					}

					echo " <input type=\"hidden\" id=\"users[" . $userId . "][userId]\" value=\"" . $userId . "\">";
					$userJsonArray[$userId]['userId'] = $userId;

					if(strtolower($action) != 'delete') {
						echo " <input type=\"hidden\" id=\"users[" . $userId . "][deviceName]\" value=\"" . $deviceName . "\">";
						echo " <input type=\"hidden\" id=\"users[" . $userId . "][derivedDeviceAccessUserName]\" value=\"" . $derivedDeviceAccessUserName . "\">";
						echo " <input type=\"hidden\" id=\"users[" . $userId . "][derivedDeviceAccessPassword]\" value=\"" . $derivedDeviceAccessPassword . "\">";
						echo " <input type=\"hidden\" id=\"users[" . $userId . "][linePort]\" value=\"" . $linePort . "\">";
						echo " <input type=\"hidden\" id=\"users[" . $userId . "][blfUser]\" value=\"" . $blfUser . "\">";

						$userJsonArray[$userId]['deviceName'] = $deviceName;
						$userJsonArray[$userId]['derivedDeviceAccessUserName'] = $derivedDeviceAccessUserName;
						$userJsonArray[$userId]['derivedDeviceAccessPassword'] = $derivedDeviceAccessPassword;
						$userJsonArray[$userId]['linePort'] = $linePort;
						$userJsonArray[$userId]['blfUser'] = $blfUser;
					}

					echo "</tr>";

					$userId = "";
				}
			}

			echo "<tr id=\"submitProvision\"><td colspan=\"100%\" align=\"center\">";
			if (isset($error) and $error == "1") {
				echo "<label class=\"labelText\">Please correct errors and resubmit.</label>";
			} else {
				echo "<input type=\"button\" name=\"subBulk\" id=\"subBulk\" class=\"submitBulkClass\" value=\"Provision Users\">";
			}
			echo "</td></tr>";
			?>
		</table>
		<input type="hidden" name="userData" value="<?php echo htmlentities(json_encode($userJsonArray))?>"/>
	</form>
</div>
<div id="loadingBulk"></div>
<div id="showDialogueValidate" class="dialogClass expressSrollTable"></div>
<form name="submitErrorForm" id="submitErrorForm" action="expressSheets/processUsersErrorData.php" method="post">
	<input type="hidden" name="columnArray" id="columnArray" value="">
	<input type="hidden" name="valueArray" id="valueArray" value="">
	<input type="hidden" name="errorArray" id="errorArray" value="">
</form>

<?php 

function isVMServicePackOrVMserviceAvailable($servicePackArray, $userServicesArray, $servicePacks, $userServices) {
    /* Start selected servicepacks have VM services */
    $assignedVMService = false;
    $serviceObj = new Services();
    $getSevicePackWithVM = $serviceObj->getVMServicePack($servicePackArray, $_SESSION["sp"]);
    $assignedVMService = count($getSevicePackWithVM) > 0 ? true : false;
    
    if( !$assignedVMService && in_array("Voice Messaging User", $userServicesArray) ) { /* Check in assigned services.*/
        $assignedVMService = true;
    }
    /* Assigned Service Pack and Services List*/
    $serviceResponse = getVMEnabledServicePack($servicePacks, $userServices);
    if( !$assignedVMService ) {
        $assignedVMService = ( isset($serviceResponse->VMOnlyServicePack) || isset($serviceResponse->VMService) ) ? true : false;
    }
    
    return $assignedVMService;
}


function getVMEnabledServicePack($servicePacks, $userServices) {
    global $sessionid, $client;
    
    $response = new stdClass;
    $serviceObj = new Services();
    /* Get VM service pack with VM user service only */
    foreach ($servicePacks as $servicePack) {
        $services = $serviceObj->getServicesListOfServicePackBasedOnEnterprise($servicePack, $_SESSION['sp']);
        if (in_array("Voice Messaging User", $services)) {
            if (count($services) == 1) {
                $response->VMOnlyServicePack = $servicePack;
                break;
            }
        }
    }
    
    /* If VM only service pack is not available then check VM user service */
    if( !(isset($response->VMOnlyServicePack)) ) {
        if (in_array("Voice Messaging User", $userServices)) {
            $response->VMService = "Voice Messaging User";
        }
    }
    
    return $response;
}

function getUploadedSheetVersion($objReader, $sheetNumber) {
    $objReader->setActiveSheetIndex($sheetNumber);
    $sheet = $objReader->getActiveSheet();
    $rowData = $sheet->rangeToArray("A12:H12" . 1, null, true, false);
    $sheetVersion = $rowData[0][2];
    return $sheetVersion;
}
?>