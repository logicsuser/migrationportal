<?php
/**
 * Created by Dipin Krishna.
 * Date: 01/04/18
 * Time: 9:47 AM
 */
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once("/var/www/html/Express/config.php");
/*require_once("/var/www/lib/broadsoft/adminPortal/surgeMail/SurgeMailCommunicator.php");
$explodeVar = explode(":", $surgemailIp);
$surgemailIp = $explodeVar[0];
$surgemailcomm = new SurgeMailCommunicator($surgemailIp, $sshUsername, $sshPassword, $sshPort);
if($_SERVER['HTTP_HOST'] != "localhost") {
    $surgeResponse = $surgemailcomm->checkConnection();
}

if(isset($surgeResponse) && $surgeResponse[0] == "false"){
    echo "<b style='color:red;text-align:center;'> $surgeResponse[1] </b>";
    exit;
}*/

if ($_SESSION["superUser"] != "1" && $_SESSION["permissions"]["expressSheets"] != "1")
{
	echo "<div style='color: red;text-align: center;'>You don't have permission to access Express Sheets.</div>";
	exit;
}

/*
if($_SESSION["superUser"] != "1" && $_SESSION["permissions"]["voicemail"] != "1") {
	echo "You don't have permission to provision Voicemail.";
	exit;
}
*/
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/html/Express/expressSheets/ExpressSheetsVoicemailManager.php");
require_once("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");
require_once("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
require_once("/var/www/lib/broadsoft/adminPortal/UserServiceGetAssignmentList.php");

$vmManager = new ExpressSheetsVoicemaillManager("excelData");
?>

<div class="formPadding">
	The following Voicemail Attributes have been provisioned:<br><br>
	<table border="1" align="center" class="bulkBLFTable bulkVMTable table" style="width:96%;">
		<tr>
			<?php echo $vmManager->createTableHeader(); ?>
		</tr>
		<?php echo $vmManager->processBulk(); ?>
	</table>
	<div class='mainPageButton' id='mainPageButton'><a href='javascript:void(0)' id='downloadVMSuccessTable'>Download CSV</a></div>
</div>


<script>

$(document).ready(function(){
	$('#downloadVMSuccessTable').click(function() {
        var titles = [];
        var data = [];
        $('.bulkVMTable th').each(function() {    //table id here
          titles.push($(this).text());
        });

      
        $('.bulkVMTable td').each(function() {    //table id here
          data.push($(this).text());
        });
        
        
        var CSVString = prepCSVRowForVMSuccessTable(titles, titles.length, '');
        CSVString = prepCSVRowForVMSuccessTable(data, titles.length, CSVString);

        var blob = new Blob(["\ufeff", CSVString]);
        var fileName = "SuccessVMTable.csv";
        if (navigator.msSaveBlob) { // IE 10+
        	navigator.msSaveBlob(blob, fileName);
        }else{
        	var downloadLink = document.createElement("a");
            
            var url = URL.createObjectURL(blob);
            downloadLink.href = url;
            downloadLink.download = fileName;

            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
       
        
      });
	
});
function prepCSVRowForVMSuccessTable(arr, columnCount, initial) {
    var row = '';
    var delimeter = ',';
    var newLine = '\r\n';

    function splitArray(_arr, _count) {
      var splitted = [];
      var result = [];
      _arr.forEach(function(item, idx) {
        if ((idx + 1) % _count === 0) {
          splitted.push(item);
          result.push(splitted);
          splitted = [];
        } else {
          splitted.push(item);
        }
      });
      return result;
    }
    var plainArr = splitArray(arr, columnCount);
   
    plainArr.forEach(function(arrItem) {
      arrItem.forEach(function(item, idx) {
    	  item = "\""+item+"\"";
        row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
      });
      row += newLine;
    });
    return initial + row;
  }


</script>