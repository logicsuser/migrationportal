<?php
/**
 * Created by Dipin Krishna.
 * Date: 10/11/17
 * Time: 9:23 AM
 */
?>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
checkLogin();

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/config.php");
//require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/DBLookup.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/expressSheetFunctions.php");
require_once("/var/www/lib/broadsoft/adminPortal/ServerConfiguration.php");
require_once("/var/www/lib/broadsoft/adminPortal/services/Services.php");
require_once $_SERVER['DOCUMENT_ROOT'] . "/../vendor/phpoffice/phpspreadsheet/src/Bootstrap.php";
require_once ("/var/www/lib/broadsoft/adminPortal/util/CommanUtil.php"); //Code added @ 16 Jan 2019 regarding EX-1005
$cmnUtilObj  = new CommanUtil();

$download_id = "";

register_shutdown_function('shutDownFunction');

const colAction = 'A';
const colFirstName = 'B';
const colLastName = 'C';
const colCallingLineIdFirstName = 'D';
const colCallingLineIdLastName = 'E';
const colNameDialingFirstName = 'F';
const colNameDialingLastName = 'G';
const colPhoneNumber = 'H';
const colPhoneNumberCheck = 'I';
const colActivateNumber = 'J';
const colCallerId = 'K';
const colCallerIdCheck = 'L';
const colExtension = 'M';
const colExtensionCheck = 'N';
const colLocation = 'O';
const colAssignVM = 'P';
//const colEnableVM = 'O';
const colEmail = 'Q';
const colDeviceType = 'R';
const colMACAddress = 'S';
const colPhoneProfile = 'U';
const colTagBundles = 'V';
const colNumLineAppearanes = 'W';
const colCallsPerLineKey = 'X';
const colLineRegNumber = 'Y';
const colNCS = 'AA';

const colServicePack = 'AB';
const colServicePacks = 'AC';

const colServices1 = 'AD';
const colServices2 = 'AE';

const colCallPickupGroup = "AF";
const colDeviceName = 'AG';
const colDeviceIndex = 'AH';
const colPortNumber = 'AI';
const colPortType = 'AJ';
const colPolycomPhoneServices = 'AK';
const colCustomContactDirectory = 'AL';
const colCombinedServices = 'AIO';
const colUserID = 'AP';

const expressSheetColLocation = 'AQ';
const expressSheetColNameDialingFirstName = 'F';
const expressSheetColNameDialingLastName = 'G';

const appsServer = "bwAppServer";
const primaryServer = "primary";
const secondaryServer = "secondary";

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_SESSION['sp']) && isset($_SESSION['groupId']) && $_SESSION['groupId'] != "") {

	$expressSheetType = $_POST['expressSheetType'];
	$process_id = posix_getpid();
	$download_status = array('d_progress' => 10, 'success' => true, 'pid' => $process_id);

	$download_id = time() . "_" . $expressSheetType . "_" . rand(10,1000);
	file_put_contents("/tmp/$download_id", json_encode($download_status));
	setServerErrorInTempFile();
	
	ignore_user_abort(true);
	set_time_limit(0);

	ob_start();
	// do initial processing here
	echo json_encode( array("success" => true, "download_id" => $download_id));
	header('Connection: close');
	header('Content-Length: '.ob_get_length());
	ob_end_flush();
	ob_flush();
	flush();


	$filterUsers = $_POST['filterUsers'];

	$sp = $_SESSION['sp'];
	$groupId = $_SESSION['groupId'];

	unset($groupInfoData);
	require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/expressSheets/group_info.php");

	$download_status['d_progress'] = 15;
	file_put_contents("/tmp/$download_id", json_encode($download_status));
	setServerErrorInTempFile();


	if ($expressSheetType == 'Users') {

		$revision = $express_sheet_revision['Users'];

		$file = $_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/expressSheets/templates/User-Rel-$revision.xlsm";

		$objReader = PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
		$objReader = $objReader->load($file);

		//Get Device Types
		$nonObsoleteDevices = array();
		require_once($_SERVER['DOCUMENT_ROOT'] . '/../lib/broadsoft/adminPortal/getDevices.php');
		require_once($_SERVER['DOCUMENT_ROOT'] . '/../lib/broadsoft/adminPortal/getDeviceList.php');
		require_once($_SERVER['DOCUMENT_ROOT'] . '/../lib/broadsoft/adminPortal/getDevicesR19.php');
		require_once($_SERVER['DOCUMENT_ROOT'] . '/../lib/broadsoft/adminPortal/ResourceNameBuilder.php');
		require_once($_SERVER['DOCUMENT_ROOT'] . '/../lib/broadsoft/adminPortal/DeviceNameBuilder.php');
		require_once ($_SERVER['DOCUMENT_ROOT'] . '/../lib/broadsoft/adminPortal/customTagManagement.php');

		// Build devices lists:  customDevices, deviceTypesDigitalVoIP, analogGatewayDevices
		/*$deviceTypesAnalogGateways = array ();
		$sipGatewayLookup = new DBLookup ( $db, "devices", "deviceName", "sipGateway" );

		// Get Custom Devices List from DB.
		// If the list exists, only devices from the list will be available in drop-down for assignment.
		$customDevices = array ();
		if ($useCustomDeviceList == "true") {
			$query = "select deviceName from customDeviceList";
			$result = $db->query ( $query );

			while ( $row = $result->fetch () ) {
				$customDevices [] = $row ["deviceName"];
			}
		}

		foreach ( $nonObsoleteDevices as $key => $value ) {
			// shortcut custom device list for Audio Codes until they are resolved
			$deviceIsAudioCodes = substr ( $value, 0, strlen ( "AudioCodes-" ) ) == "AudioCodes-";

			if ($useCustomDeviceList == "true" && ! in_array ( $value, $customDevices )) {
				if (! $deviceIsAudioCodes) {
					continue;
				}
			}
			if ($sipGatewayLookup->get ( $value ) == "true" || $deviceIsAudioCodes) {
				$deviceTypesAnalogGateways [] = $value;
			} else {
				$deviceTypesDigitalVoIP [] = $value;
			}
		}*/
		require_once ("/var/www/html/Express/util/getAuthorizeDeviceTypes.php");
		asort($deviceTypesAnalogGateways);
		asort($deviceTypesDigitalVoIP);
		asort($nonObsoleteDevices);

		//Get All Numbers and Availalle Numbers
		require_once("/var/www/lib/broadsoft/adminPortal/getAvailableNumbers.php");
		require_once("/var/www/lib/broadsoft/adminPortal/allNumbers.php");

		$download_status['d_progress'] = 20;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		server_fail_over_debuggin_testing(); /* for fail Over testing. */
		//Manipulate 'Group'
		$objReader->setActiveSheetIndex(0);

		//error_log(print_r($groupInfoData, true));
		//print_r($groupInfoData);
		//print_r($userServices);
		//exit;

		$callingLineIdDisplayPhoneNumber = str_replace("+1-","",$groupInfoData["callingLineIdDisplayPhoneNumber"]);

		$objReader->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
		$objReader->getActiveSheet()->getColumnDimension('C')->setWidth('20');


		$objReader->getActiveSheet()->setCellValue('C12', "Rev.$revision")
			->setCellValue('C13', "Requires Express $expressRevision or higher")
			->setCellValue('G12', date("m/d/Y"))
			->setCellValue('E16', $groupId)
			->setCellValue('E18', $groupInfoData['addressLine1'])
			->setCellValue('E19', $groupInfoData['addressLine2'])
			->setCellValue('E20', $groupInfoData['city'])
			->setCellValue('E21', $groupInfoData['stateOrProvince'])
			->setCellValue('E22', $groupInfoData['zipOrPostalCode'])
			->setCellValue('E24', $groupInfoData["timeZone"])
			->setCellValue('E28', $callingLineIdDisplayPhoneNumber)
			->setCellValue('E29', $groupInfoData["callingLineIdFirstName"])
			->setCellValue('E30', $groupInfoData["callingLineIdLastName"])
			->setCellValue('E31', $_POST['Users_linePortDomain'])
			->setCellValue('E33', $_POST['Users_extensionLength'])
			->setCellValue('E34', $_POST['Users_voiceMailServicePack'])
			->setCellValue('E35', $_POST['Users_callingLineIDPolicy'])
			->setCellValue('E36', $_POST['Users_networkClassofService'])
			->setCellValue('I37', $_POST['Users_polycomPhoneService'])
			->setCellValue('I38', $_POST['Users_defaultCustomContactDirectory']);

		insert_logo($objReader, 'C', 2, $brandingLogoPath);

		//Options Sheet - Start - Populate all named drop down options
		$objReader->setActiveSheetIndex(3);

		//Analog Device Names / Index
		asort($deviceTypesAnalogGateways);
		$objReader->getActiveSheet()->SetCellValue('A1', 'Analog Device Names');
		$objReader->getActiveSheet()->SetCellValue('B1', 'Custom Profiles');
		$objReader->getActiveSheet()->SetCellValue('C1', 'Device Ports');
		$objReader->getActiveSheet()->SetCellValue('D1', 'Device Reg. Line Numbers');

		$sheetRow = 1;
		$deviceTypeRow = 3;
		$customProfileRow = 1;
		$devicePortsRow = 1;
		$deviceRegLineRow = 1;
		$customTagsRow = 1;

		//Analog Devices
		foreach ($deviceTypesAnalogGateways as $deviceType) {

			//error_log($deviceNameAnalog);

			$objReader->getActiveSheet()->SetCellValue('A' . ++$sheetRow, "");
			$deviceNameListStartIndex = $sheetRow;
			for ($index = 1; $index <= $numSIPGatewayInstances; $index++) {

				// create device builder... since this is for analog devices, force resolve is always set to true.
				$deviceNameBuilder = new DeviceNameBuilder($deviceNameAnalog,
					"",
					"",
					isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "",
					true);

				if (! $deviceNameBuilder->validate()) {
					// TODO: Implement resolution when Device name input formula has errors
				}

				$deviceNameBuilder->setRequiredAttribute(ResourceNameBuilder::GRP_ID, $_SESSION["groupId"]);
				$deviceNameBuilder->setRequiredAttribute(ResourceNameBuilder::DEV_TYPE, $deviceType);
				$deviceNameBuilder->setRequiredAttribute(ResourceNameBuilder::INT_1, $index);
				$deviceNameBuilder->setRequiredAttribute(ResourceNameBuilder::INT_2, $index);

				$deviceName = $deviceNameBuilder->getName();

				//error_log($deviceName);
				//if (isset($deviceName) && deviceExists($deviceName)) {
				//getDevicePortUsage($deviceName);
				$objReader->getActiveSheet()->SetCellValue('A' . ++$sheetRow, $deviceName);
				//}


			}

			$deviceNameListEndIndex = $sheetRow;

			if($deviceNameListEndIndex >= $deviceNameListStartIndex) {
				$objReader->addNamedRange(
					new PhpOffice\PhpSpreadsheet\NamedRange(
						"DeviceNameRange_" . $deviceTypeRow,
						$objReader->getActiveSheet(),
						'$A$' . $deviceNameListStartIndex . ':$A$' . $deviceNameListEndIndex
					)
				);
			}


			//Custom Profiles for Device Type
			$objReader->getActiveSheet()->SetCellValue('B' . ++$customProfileRow, "None");
			$customProfileListStartIndex = $customProfileRow;
			$customProfilesForDeviceTypes = getCustomProfilesForDeviceType($deviceType);
			foreach ($customProfilesForDeviceTypes as $customProfile) {
				$objReader->getActiveSheet()->SetCellValue('B' . ++$customProfileRow, $customProfile[expressSheetCustomProfileName]);
			}
			$customProfileListEndIndex = $customProfileRow;
			if($customProfileListEndIndex >= $customProfileListStartIndex) {
				$objReader->addNamedRange(
					new PhpOffice\PhpSpreadsheet\NamedRange(
						"CustomProfileRange_" . $deviceTypeRow,
						$objReader->getActiveSheet(),
						'$B$' . $customProfileListStartIndex . ':$B$' . $customProfileListEndIndex
					)
				);
			}

			//Ports for Device Type
			//error_log("$deviceType : " . checkDeviceSupports($deviceType));
			if(checkDeviceSupports($deviceType) == 'static') {
				$objReader->getActiveSheet()->SetCellValue('C' . ++$devicePortsRow, "");
				$devicePortsListStartIndex = $devicePortsRow;
				$devicePortsForDeviceTypes = getDeviceTypeNumberOfPorts($deviceType);
				for($devicePort = 1; $devicePort <= $devicePortsForDeviceTypes; $devicePort++) {
					$objReader->getActiveSheet()->SetCellValue('C' . ++$devicePortsRow, $devicePort);
				}
				$devicePortsListEndIndex = $devicePortsRow;
				if ($devicePortsListEndIndex >= $devicePortsListStartIndex) {
					$objReader->addNamedRange(
						new PhpOffice\PhpSpreadsheet\NamedRange(
							"DevicePortsRange_" . $deviceTypeRow,
							$objReader->getActiveSheet(),
							'$C$' . $devicePortsListStartIndex . ':$C$' . $devicePortsListEndIndex
						)
					);
				}
			}

			//Reg. Line Numbers
			$objReader->getActiveSheet()->SetCellValue('D' . ++$deviceRegLineRow, "");
			$deviceRegLineListStartIndex = $deviceRegLineRow;
			if($ports = getPortsForDeviceType($deviceType)) {

				$deviceRegLines = $ports['ports'];
				for ($deviceRegLine = 1; $deviceRegLine <= $deviceRegLines; $deviceRegLine++) {
					$objReader->getActiveSheet()->SetCellValue('D' . ++$deviceRegLineRow, $deviceRegLine);
				}
				$deviceRegLineListEndIndex = $deviceRegLineRow;
				if ($deviceRegLineListEndIndex >= $deviceRegLineListStartIndex) {
					$objReader->addNamedRange(
						new PhpOffice\PhpSpreadsheet\NamedRange(
							"DeviceRegLineNumberRange_" . $deviceTypeRow,
							$objReader->getActiveSheet(),
							'$D$' . $deviceRegLineListStartIndex . ':$D$' . $deviceRegLineListEndIndex
						)
					);
				}
			}

			//Custom Tags for DeviceType
			$info = new CustomTagManagement();
			$objReader->getActiveSheet()->SetCellValue('E' . ++$customTagsRow, "");
			$customTagsListStartIndex = $customTagsRow;
			if($tagList = $info->getTagList($deviceType)) {
				uasort($tagList, function($a, $b) {
					return strnatcmp($a['tagName'], $b['tagName']);
				});
				foreach($tagList as $key=>$val){

					//error_log(print_r($val['tagName'], true));
					$objReader->getActiveSheet()->SetCellValue('E' . ++$customTagsRow, $val['tagName']);
				}
				$customTagsListEndIndex = $customTagsRow;
				if ($customTagsListEndIndex >= $customTagsListStartIndex) {
					$objReader->addNamedRange(
						new PhpOffice\PhpSpreadsheet\NamedRange(
							"DeviceTypeCustomTagsRange_" . $deviceTypeRow,
							$objReader->getActiveSheet(),
							'$E$' . $customTagsListStartIndex . ':$E$' . $customTagsListEndIndex
						)
					);
				}
			}

			$deviceTypeRow++;

		}

		//NON-Analog Devices
		foreach ($deviceTypesDigitalVoIP as $deviceType) {

			//Custom Profiles for Device Type
			$objReader->getActiveSheet()->SetCellValue('B' . ++$customProfileRow, "None");
			$customProfileListStartIndex = $customProfileRow;
			$customProfilesForDeviceTypes = getCustomProfilesForDeviceType($deviceType);
			foreach ($customProfilesForDeviceTypes as $customProfile) {
				$objReader->getActiveSheet()->SetCellValue('B' . ++$customProfileRow, $customProfile[expressSheetCustomProfileName]);
			}
			$customProfileListEndIndex = $customProfileRow;
			if($customProfileListEndIndex >= $customProfileListStartIndex) {
				$objReader->addNamedRange(
					new PhpOffice\PhpSpreadsheet\NamedRange(
						"CustomProfileRange_" . $deviceTypeRow,
						$objReader->getActiveSheet(),
						'$B$' . $customProfileListStartIndex . ':$B$' . $customProfileListEndIndex
					)
				);
			}


			// When Device is SIP type and supports static port assignment,
			// Port Number dropdown will have port Numbers.
			// Check if the device supports static port assignment.
			//error_log("$deviceType : " . checkDeviceSupports($deviceType));
			if(checkDeviceSupports($deviceType) == 'static') {

				$availablePorts = array();
				$devicePortsForDeviceTypes = getDeviceTypeNumberOfPorts($deviceType);
				//$deviceName = getDeviceNameForDigital(array('userType' => 'digital'), $deviceNameDID2, $deviceNameAnalog, $deviceNameDID1);

				//error_log("$deviceType : $devicePortsForDeviceTypes");

				//Ports for Device Type
				$objReader->getActiveSheet()->SetCellValue('C' . ++$devicePortsRow, "");
				$devicePortsListStartIndex = $devicePortsRow;
				for($devicePort = 1; $devicePort <= $devicePortsForDeviceTypes; $devicePort++) {
					error_log("$deviceType : $devicePort");
					$objReader->getActiveSheet()->SetCellValue('C' . ++$devicePortsRow, $devicePort);
				}
				$devicePortsListEndIndex = $devicePortsRow;
				//error_log("$deviceType : $devicePortsListStartIndex - $devicePortsListEndIndex");
				if($devicePortsListEndIndex >= $devicePortsListStartIndex) {
					$objReader->addNamedRange(
						new PhpOffice\PhpSpreadsheet\NamedRange(
							"DevicePortsRange_" . $deviceTypeRow,
							$objReader->getActiveSheet(),
							'$C$' . $devicePortsListStartIndex . ':$C$' . $devicePortsListEndIndex
						)
					);
				}

			}

			//Reg. Line Numbers
			$objReader->getActiveSheet()->SetCellValue('D' . ++$deviceRegLineRow, "");
			$deviceRegLineListStartIndex = $deviceRegLineRow;
			if($ports = getPortsForDeviceType($deviceType)) {

				$deviceRegLines = $ports['ports'];
				for ($deviceRegLine = 1; $deviceRegLine <= $deviceRegLines; $deviceRegLine++) {
					$objReader->getActiveSheet()->SetCellValue('D' . ++$deviceRegLineRow, $deviceRegLine);
				}
				$deviceRegLineListEndIndex = $deviceRegLineRow;
				if ($deviceRegLineListEndIndex >= $deviceRegLineListStartIndex) {
					$objReader->addNamedRange(
						new PhpOffice\PhpSpreadsheet\NamedRange(
							"DeviceRegLineNumberRange_" . $deviceTypeRow,
							$objReader->getActiveSheet(),
							'$D$' . $deviceRegLineListStartIndex . ':$D$' . $deviceRegLineListEndIndex
						)
					);
				}
			}

			//Custom Tags for DeviceType
			$info = new CustomTagManagement();
			$objReader->getActiveSheet()->SetCellValue('E' . ++$customTagsRow, "");
			$customTagsListStartIndex = $customTagsRow;
			if($tagList = $info->getTagList($deviceType)) {

				//error_log(print_r($tagList, true));
				uasort($tagList, function($a, $b) {
					return strnatcmp($a['shortDescription'], $b['shortDescription']);
				});
				foreach($tagList as $key=>$val){
					$objReader->getActiveSheet()->SetCellValue('E' . ++$customTagsRow, $val['tagName']);
				}
				$customTagsListEndIndex = $customTagsRow;
				if ($customTagsListEndIndex >= $customTagsListStartIndex) {
					$objReader->addNamedRange(
						new PhpOffice\PhpSpreadsheet\NamedRange(
							"DeviceTypeCustomTagsRange_" . $deviceTypeRow,
							$objReader->getActiveSheet(),
							'$E$' . $customTagsListStartIndex . ':$E$' . $customTagsListEndIndex
						)
					);
				}
			}

			$deviceTypeRow++;

		}

		//Options Sheet - End

		$objValidation_DeviceNames = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_DeviceNames->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_DeviceNames->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_DeviceNames->setAllowBlank(false);
		$objValidation_DeviceNames->setShowInputMessage(false);
		$objValidation_DeviceNames->setShowErrorMessage(true);
		$objValidation_DeviceNames->setShowDropDown(true);
		$objValidation_DeviceNames->setErrorTitle('Input error');
		$objValidation_DeviceNames->setError('Value is not in list.');
		$objValidation_DeviceNames->setFormula1('=INDIRECT("DeviceNameRange_"&MATCH('. colDeviceType . '3,$BI$1:$BI$2000,0))');

		$objValidation_CustomProfiles = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_CustomProfiles->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_CustomProfiles->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_CustomProfiles->setAllowBlank(false);
		$objValidation_CustomProfiles->setShowInputMessage(false);
		$objValidation_CustomProfiles->setShowErrorMessage(true);
		$objValidation_CustomProfiles->setShowDropDown(true);
		$objValidation_CustomProfiles->setErrorTitle('Input error');
		$objValidation_CustomProfiles->setError('Value is not in list.');
		$objValidation_CustomProfiles->setFormula1('=INDIRECT("CustomProfileRange_"&MATCH('. colDeviceType . '3,$BI$1:$BBI$2000,0))');

		$objValidation_DevicePorts = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_DevicePorts->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_DevicePorts->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_DevicePorts->setAllowBlank(false);
		$objValidation_DevicePorts->setShowInputMessage(false);
		$objValidation_DevicePorts->setShowErrorMessage(true);
		$objValidation_DevicePorts->setShowDropDown(true);
		$objValidation_DevicePorts->setErrorTitle('Input error');
		$objValidation_DevicePorts->setError('Value is not in list.');
		$objValidation_DevicePorts->setFormula1('=INDIRECT("DevicePortsRange_"&MATCH('. colDeviceType . '3,$BI$1:$BI$2000,0))');

		$objValidation_DeviceRegLines = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_DeviceRegLines->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_DeviceRegLines->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_DeviceRegLines->setAllowBlank(false);
		$objValidation_DeviceRegLines->setShowInputMessage(false);
		$objValidation_DeviceRegLines->setShowErrorMessage(true);
		$objValidation_DeviceRegLines->setShowDropDown(true);
		$objValidation_DeviceRegLines->setErrorTitle('Input error');
		$objValidation_DeviceRegLines->setError('Value is not in list.');
		$objValidation_DeviceRegLines->setFormula1('=INDIRECT("DeviceRegLineNumberRange_"&MATCH('. colDeviceType . '3,$BI$1:$BI$2000,0))');

		$objValidation_DeviceCustomTags = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_DeviceCustomTags->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_DeviceCustomTags->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_DeviceCustomTags->setAllowBlank(true);
		$objValidation_DeviceCustomTags->setShowDropDown(true);
		$objValidation_DeviceCustomTags->setFormula1('=INDIRECT("DeviceTypeCustomTagsRange_"&MATCH(Users!'. colDeviceType . '3,AllDeviceTypesRange,0))');


		$download_status['d_progress'] = 30;

		$objReader->setActiveSheetIndex(1);

		$objConditionalStyle_pass = new PhpOffice\PhpSpreadsheet\Style\Conditional();
		$objConditionalStyle_pass->setConditionType(PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CONTAINSTEXT)
			->setOperatorType(PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_CONTAINSTEXT)
			->setText("P");
		$objConditionalStyle_pass->getStyle()->getFont()->getColor()->setRGB('0f900f');

		$objConditionalStyle_error = new PhpOffice\PhpSpreadsheet\Style\Conditional();
		$objConditionalStyle_error->setConditionType(PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_CONTAINSTEXT)
			->setOperatorType(PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_CONTAINSTEXT)
			->setText("X");
		$objConditionalStyle_error->getStyle()->getFont()->getColor()->setRGB('d40808');

		$objConditionalStyle_DevicePort_error = new PhpOffice\PhpSpreadsheet\Style\Conditional();
		$objConditionalStyle_DevicePort_error->setConditionType(PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_EXPRESSION)
			->setOperatorType(PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_EQUAL)
			->addCondition('IF(' . colPortNumber . '3<>"",IF(COUNTIF(INDIRECT("DevicePortsRange_"&MATCH(' . colDeviceType . '3,$BI$1:$BI$2000,0)), ' . colPortNumber . '3), FALSE, TRUE), FALSE)');
		$objConditionalStyle_DevicePort_error->getStyle()->getFont()->getColor()->setRGB('d40808');
		$objConditionalStyle_DevicePort_error->getStyle()->getFont()->setBold(true);
		$objConditionalStyle_DevicePort_error->getStyle()->getBorders()->getBottom()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)->getColor()->setRGB('d40808');

		$objConditionalStyle_DeviceRegLine_error = new PhpOffice\PhpSpreadsheet\Style\Conditional();
		$objConditionalStyle_DeviceRegLine_error->setConditionType(PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_EXPRESSION)
			->setOperatorType(PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_EQUAL)
			->addCondition('IF(' . colLineRegNumber . '3<>"",IF(COUNTIF(INDIRECT("DeviceRegLineNumberRange_"&MATCH(' . colDeviceType. '3,$BI$1:$BI$2000,0)), ' . colLineRegNumber . '3), FALSE, TRUE), FALSE)');
		$objConditionalStyle_DeviceRegLine_error->getStyle()->getFont()->getColor()->setRGB('d40808');
		$objConditionalStyle_DeviceRegLine_error->getStyle()->getFont()->setBold(true);
		$objConditionalStyle_DeviceRegLine_error->getStyle()->getBorders()->getBottom()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)->getColor()->setRGB('d40808');

		$objConditionalStyle_DeviceName_error = new PhpOffice\PhpSpreadsheet\Style\Conditional();
		$objConditionalStyle_DeviceName_error->setConditionType(PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_EXPRESSION)
			->setOperatorType(PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_EQUAL)
			->addCondition('IF(' . colDeviceName . '3<>"",IF(COUNTIF(INDIRECT("DeviceNameRange_"&MATCH(' . colDeviceType . '3,$BI$1:$BI$2000,0)), ' . colDeviceName . '3), FALSE, TRUE), FALSE)');
		$objConditionalStyle_DeviceName_error->getStyle()->getFont()->getColor()->setRGB('d40808');
		$objConditionalStyle_DeviceName_error->getStyle()->getFont()->setBold(true);
		$objConditionalStyle_DeviceName_error->getStyle()->getBorders()->getBottom()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)->getColor()->setRGB('d40808');

		/*
		$objConditionalStyle_CustomProfile_error = new PhpOffice\PhpSpreadsheet\Style\Conditional();
		$objConditionalStyle_CustomProfile_error->setConditionType(PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_EXPRESSION)
			->setOperatorType(PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_EQUAL)
			->addCondition('IF(P3<>"",IF(COUNTIF(INDIRECT("CustomProfileRange_"&MATCH(O3,$BI$1:$BI$2000,0)), P3), FALSE, TRUE), FALSE)');
		$objConditionalStyle_CustomProfile_error->getStyle()->getFont()->getColor()->setRGB('d40808');
		$objConditionalStyle_CustomProfile_error->getStyle()->getFont()->setBold(true);
		$objConditionalStyle_CustomProfile_error->getStyle()->getBorders()->getBottom()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)->getColor()->setRGB('d40808');
		*/

		$objConditionalStyle_DeviceIndex_error = new PhpOffice\PhpSpreadsheet\Style\Conditional();
		$objConditionalStyle_DeviceIndex_error->setConditionType(PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_EXPRESSION)
			->setOperatorType(PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_EQUAL)
			->addCondition('IF(' . colDeviceIndex . '3<>"",IF(' . colDeviceIndex . '3<>(MATCH(' . colDeviceName . '2,INDIRECT("DeviceNameRange_"&MATCH(' . colDeviceType. '3,$BI$1:$BI$2000,0)),0)-1),TRUE,FALSE), FALSE)');
		$objConditionalStyle_DeviceIndex_error->getStyle()->getFont()->getColor()->setRGB('d40808');
		$objConditionalStyle_DeviceIndex_error->getStyle()->getFont()->setBold(true);
		$objConditionalStyle_DeviceIndex_error->getStyle()->getBorders()->getBottom()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)->getColor()->setRGB('d40808');


		$objConditionalStyle_ActivateNumber_error = new PhpOffice\PhpSpreadsheet\Style\Conditional();
		$objConditionalStyle_ActivateNumber_error->setConditionType(PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_EXPRESSION)
			->setOperatorType(PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_EQUAL)
			->addCondition('=IF(' . colPhoneNumber . '3<>"", IF(' . colActivateNumber . '3<>"", FALSE, TRUE), FALSE)');
		$objConditionalStyle_ActivateNumber_error->getStyle()->getFont()->getColor()->setRGB('d40808');
		$objConditionalStyle_ActivateNumber_error->getStyle()->getFont()->setBold(true);
		$objConditionalStyle_ActivateNumber_error->getStyle()->getBorders()->getBottom()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)->getColor()->setRGB('d40808');

		$conditionalStyles = $objReader->getActiveSheet()->getStyle(colPhoneNumberCheck . '3')
			->getConditionalStyles();
		array_push($conditionalStyles, $objConditionalStyle_pass, $objConditionalStyle_error);
		$objReader->getActiveSheet()->getStyle(colPhoneNumberCheck . '3:' . colPhoneNumberCheck. '2000')
			->setConditionalStyles($conditionalStyles);

		$conditionalStyles = $objReader->getActiveSheet()->getStyle(colCallerIdCheck . '3')
			->getConditionalStyles();
		array_push($conditionalStyles, $objConditionalStyle_pass, $objConditionalStyle_error);
		$objReader->getActiveSheet()->getStyle(colCallerIdCheck . '3:' . colCallerIdCheck. '2000')
			->setConditionalStyles($conditionalStyles);

		$conditionalStyles = $objReader->getActiveSheet()->getStyle(colExtensionCheck . '3')
			->getConditionalStyles();
		array_push($conditionalStyles, $objConditionalStyle_pass, $objConditionalStyle_error);
		$objReader->getActiveSheet()->getStyle(colExtensionCheck . '3:' . colExtensionCheck . '2000')
			->setConditionalStyles($conditionalStyles);

		/*
		$conditionalStyles = $objReader->getActiveSheet()->getStyle('Q3')
			->getConditionalStyles();
		array_push($conditionalStyles, $objConditionalStyle_CustomProfile_error);
		$objReader->getActiveSheet()->getStyle('R3:R2000')
			->setConditionalStyles($conditionalStyles);
		*/

		$conditionalStyles = $objReader->getActiveSheet()->getStyle(colLineRegNumber . '3')
			->getConditionalStyles();
		array_push($conditionalStyles, $objConditionalStyle_DeviceRegLine_error);
		$objReader->getActiveSheet()->getStyle(colLineRegNumber . '3:' . colLineRegNumber. '2000')
			->setConditionalStyles($conditionalStyles);

		$conditionalStyles = $objReader->getActiveSheet()->getStyle(colDeviceName . '3')
			->getConditionalStyles();
		array_push($conditionalStyles, $objConditionalStyle_DeviceName_error);
		$objReader->getActiveSheet()->getStyle(colDeviceName . '3:' . colDeviceName. '2000')
			->setConditionalStyles($conditionalStyles);

		$conditionalStyles = $objReader->getActiveSheet()->getStyle(colDeviceIndex . '3')
			->getConditionalStyles();
		array_push($conditionalStyles, $objConditionalStyle_DeviceIndex_error);
		$objReader->getActiveSheet()->getStyle(colDeviceIndex . '3:' . colDeviceIndex. '2000')
			->setConditionalStyles($conditionalStyles);

		$conditionalStyles = $objReader->getActiveSheet()->getStyle(colPortNumber . '3')
			->getConditionalStyles();
		array_push($conditionalStyles, $objConditionalStyle_DevicePort_error);
		$objReader->getActiveSheet()->getStyle(colPortNumber . '3:' . colPortNumber. '2000')
			->setConditionalStyles($conditionalStyles);

		$conditionalStyles = $objReader->getActiveSheet()->getStyle(colActivateNumber . '3')
			->getConditionalStyles();
		array_push($conditionalStyles, $objConditionalStyle_ActivateNumber_error);
		$objReader->getActiveSheet()->getStyle(colActivateNumber . '3:' . colActivateNumber. '2000')
			->setConditionalStyles($conditionalStyles);


		//All Action Dropdown
		$objValidation_all_actions = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_all_actions->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_all_actions->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_all_actions->setAllowBlank(false);
		$objValidation_all_actions->setShowDropDown(true);
		$objValidation_all_actions->setFormula1('"Add"');

		//Modify or Delete Action Dropdown
		$objValidation_action = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_action->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_action->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_action->setAllowBlank(false);
		$objValidation_action->setShowDropDown(true);
		$objValidation_action->setFormula1('"Modify,Delete"');

		$phoneProfiles = getAllPhoneProfiles();
		$phoneProfilesArray = array();
		foreach ($phoneProfiles as $phoneProfile) {
			$phoneProfilesArray[] = $phoneProfile['phoneProfile'];
		}
		//Phone Profiles Dropdown
		$objValidation_phoneProfile = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_phoneProfile->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_phoneProfile->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_phoneProfile->setAllowBlank(false);
		$objValidation_phoneProfile->setShowDropDown(true);
		$objValidation_phoneProfile->setFormula1('"' . implode(",", $phoneProfilesArray ) . '"');

		$tagBundles = getAllTagBundles();
		$tagBundlesArray = array("None");
		foreach ($tagBundles as $tagBundle) {
			$tagBundlesArray[] = $tagBundle['tagBundle'];
		}
		//Tag Bundle Dropdown
		$objValidation_tagBundle = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_tagBundle->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_tagBundle->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_tagBundle->setAllowBlank(false);
		$objValidation_tagBundle->setShowDropDown(true);
		$objValidation_tagBundle->setFormula1('"' . implode(",", $tagBundlesArray ) . '"');

		//Line Apperances Dropdown
		$objValidation_line_appearances = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_line_appearances->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_line_appearances->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_line_appearances->setAllowBlank(false);
		$objValidation_line_appearances->setShowDropDown(true);
		$objValidation_line_appearances->setFormula1('"1,2,3,4,5,6"');

		//Calls per LineKey Dropdown
		$objValidation_call_per_linekey = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_call_per_linekey->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_call_per_linekey->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_call_per_linekey->setAllowBlank(false);
		$objValidation_call_per_linekey->setShowDropDown(true);
		$objValidation_call_per_linekey->setFormula1('"1,2,3,4,5,6,7,8,9,10,11,12"');


		//SIP Device Type Dropdown List
		$objReader->getActiveSheet()->SetCellValue('BA1', 'SIP Device Types');
		$sheetRow = 3;
		asort($deviceTypesDigitalVoIP);
		foreach ($deviceTypesDigitalVoIP as $deviceType) {

			$objReader->getActiveSheet()->SetCellValue('BA' . $sheetRow, $deviceType);
			$sheetRow++;
		}
		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange(
				'SIPDeviceTypeRange',
				$objReader->getActiveSheet(),
				'$BA$2:$BA$' . $sheetRow
			)
		);
		$objReader->getActiveSheet()->getColumnDimension('BA')->setVisible(FALSE);
		$objValidation_sipDeviceType = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_sipDeviceType->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_sipDeviceType->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_sipDeviceType->setAllowBlank(false);
		$objValidation_sipDeviceType->setShowDropDown(true);
		$objValidation_sipDeviceType->setFormula1('=SIPDeviceTypeRange');

		//Analog Device Type Dropdown List
		$objReader->getActiveSheet()->SetCellValue('BH1', 'Analog Device Types');
		$sheetRow = 3;
		asort($deviceTypesAnalogGateways);
		foreach ($deviceTypesAnalogGateways as $deviceType) {

			$objReader->getActiveSheet()->SetCellValue('BH' . $sheetRow, $deviceType);
			$sheetRow++;
		}
		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange(
				'AnalogDeviceTypeRange',
				$objReader->getActiveSheet(),
				'$BH$2:$BH$' . $sheetRow
			)
		);
		$objReader->getActiveSheet()->getColumnDimension('BH')->setVisible(FALSE);
		$objValidation_analogDeviceType = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_analogDeviceType->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_analogDeviceType->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_analogDeviceType->setAllowBlank(false);
		$objValidation_analogDeviceType->setShowDropDown(true);
		$objValidation_analogDeviceType->setFormula1('=AnalogDeviceTypeRange');

		//ALL Device Type Dropdown List
		$objReader->getActiveSheet()->SetCellValue('BI1', 'All Device Types');
		$sheetRow = 3;
		foreach ($deviceTypesAnalogGateways as $deviceType) {

			$objReader->getActiveSheet()->SetCellValue('BI' . $sheetRow, $deviceType);
			$sheetRow++;
		}
		foreach ($deviceTypesDigitalVoIP as $deviceType) {

			$objReader->getActiveSheet()->SetCellValue('BI' . $sheetRow, $deviceType);
			$sheetRow++;
		}
		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange(
				'AllDeviceTypeRange',
				$objReader->getActiveSheet(),
				'$BI$2:$BI$' . $sheetRow
			)
		);
		$objReader->getActiveSheet()->getColumnDimension('BI')->setVisible(FALSE);
		$objValidation_allDeviceType = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_allDeviceType->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_allDeviceType->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_allDeviceType->setAllowBlank(false);
		$objValidation_allDeviceType->setShowDropDown(true);
		$objValidation_allDeviceType->setFormula1('=AllDeviceTypeRange');

		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange(
				"AllDeviceTypesRange",
				$objReader->getActiveSheet(),
				'$BI$1:$BI$' . $sheetRow
			)
		);


		$download_status['d_progress'] = 35;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		
		/* Start Service First */
		$serviceObj = new Services();
		$servicesRes = $serviceObj->GroupUserServiceGetAuthorizationListRequest($_SESSION['groupId']);
		$services = $servicesRes;
		//$services = array("Service1", "Service2", "Service3");
		$objReader->getActiveSheet()->SetCellValue('BK1', 'Services');
		$sheetRow = 3;
		asort($services);
		foreach ($services as $service) {
		    
		    $objReader->getActiveSheet()->SetCellValue('BK' . $sheetRow, $service);
		    $sheetRow++;
		}
		$objReader->addNamedRange(
		    new PhpOffice\PhpSpreadsheet\NamedRange(
		        'ServiceRange',
		        $objReader->getActiveSheet(),
		        '$BK$2:$BK$' . $sheetRow
		        )
		    );
		$objReader->getActiveSheet()->getColumnDimension('BK')->setVisible(FALSE);
		$objValidation_services = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_services->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_services->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_services->setAllowBlank(false);
		$objValidation_services->setShowDropDown(true);
		$objValidation_services->setFormula1('=ServiceRange');
		/* End Services */
		
		
		//Service Package Dropdown List
		$objReader->getActiveSheet()->SetCellValue('BB1', 'Service Packages');
		$sheetRow = 3;
		asort($servicePacks);
		foreach ($servicePacks as $servicePack) {

			$objReader->getActiveSheet()->SetCellValue('BB' . $sheetRow, $servicePack);
			$sheetRow++;
		}
		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange(
				'ServicePackageRange',
				$objReader->getActiveSheet(),
				'$BB$2:$BB$' . $sheetRow
			)
		);
		$objReader->getActiveSheet()->getColumnDimension('BB')->setVisible(FALSE);
		$objValidation_servicePackage = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_servicePackage->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_servicePackage->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_servicePackage->setAllowBlank(false);
		$objValidation_servicePackage->setShowDropDown(true);
		$objValidation_servicePackage->setFormula1('=ServicePackageRange');


		//NCOS Dropdown List
		$objReader->getActiveSheet()->SetCellValue('BC1', 'Service Packages');
		$sheetRow = 3;
		if(isset($networkClassesOfService) && is_array($networkClassesOfService)) {
			asort($networkClassesOfService);
		}
		foreach ($networkClassesOfService as $ncos) {

			$objReader->getActiveSheet()->SetCellValue('BC' . $sheetRow, $ncos);
			$sheetRow++;
		}
		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange(
				'NCOSRange',
				$objReader->getActiveSheet(),
				'$BC$2:$BC$' . $sheetRow
			)
		);
		$objReader->getActiveSheet()->getColumnDimension('BC')->setVisible(FALSE);
		$objValidation_NCOS = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_NCOS->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_NCOS->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_NCOS->setAllowBlank(false);
		$objValidation_NCOS->setShowDropDown(true);
		$objValidation_NCOS->setFormula1('=NCOSRange');


		//Phone Profile Dropdown List
		$objReader->getActiveSheet()->SetCellValue('BD1', 'Phone Profiles');
		$sheetRow = 2;
		$objReader->getActiveSheet()->SetCellValue('BD' . $sheetRow++, "None");
		$custom_profiles = getDistictCustomProfiles();
		foreach ($custom_profiles as $profile) {

			$objReader->getActiveSheet()->SetCellValue('BD' . $sheetRow, $profile[expressSheetCustomProfileName]);
			$sheetRow++;
		}
		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange(
				'PhoneProfileRange',
				$objReader->getActiveSheet(),
				'$BD$2:$BD$' . $sheetRow
			)
		);
		$objReader->getActiveSheet()->getColumnDimension('BD')->setVisible(FALSE);
		$objValidation_PhoneProfile = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_PhoneProfile->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_PhoneProfile->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_PhoneProfile->setAllowBlank(false);
		$objValidation_PhoneProfile->setShowDropDown(true);
		$objValidation_PhoneProfile->setFormula1('=PhoneProfileRange');


		//PhoneNumber Dropdown List
		$objReader->getActiveSheet()->SetCellValue('BE1', 'Phone Numbers');
		$sheetRow = 3;
		foreach ($availableNumbers as $phoneNumber) {

			$objReader->getActiveSheet()->SetCellValue('BE' . $sheetRow, $phoneNumber);
			$objReader->getActiveSheet()->SetCellValue('BF' . $sheetRow, '=IF(COUNTIF($F$2:$F$2000,BE' . $sheetRow .')>0,"",BE' . $sheetRow . '&"")');

			$x = $sheetRow - 2;
			$objReader->getActiveSheet()->SetCellValue('BG' . $sheetRow, "=IFERROR(INDEX(CheckPhoneNumberRange,SMALL(IF(LEN(CheckPhoneNumberRange)>2,ROW(CheckPhoneNumberRange)-ROW(\$A\$1)),ROW($x:$x)),1),\"\")");
			$objReader->getActiveSheet()->getCell('BG' . $sheetRow)->setValueExplicit('', PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_FORMULA_ARRAY);

			$sheetRow++;
		}
		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange(
				'PhoneNumberRange',
				$objReader->getActiveSheet(),
				'$BE$2:$BE$' . $sheetRow
			)
		);
		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange(
				'CheckPhoneNumberRange',
				$objReader->getActiveSheet(),
				'$BF$2:$BF$' . $sheetRow
			)
		);
		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange(
				'AvailablePhoneNumberRange',
				$objReader->getActiveSheet(),
				'$BG$2:$BG$' . $sheetRow
			)
		);

		$objReader->getActiveSheet()->getColumnDimension('BE')->setVisible(FALSE);
		$objValidation_PhoneNumber = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_PhoneNumber->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_PhoneNumber->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_PhoneNumber->setAllowBlank(false);
		$objValidation_PhoneNumber->setShowDropDown(true);
		$objValidation_PhoneNumber->setFormula1('=PhoneNumberRange');
		$objReader->getActiveSheet()->getColumnDimension('BF')->setVisible(FALSE);
		$objReader->getActiveSheet()->getColumnDimension('BG')->setVisible(FALSE);
		$objValidation_PhoneNumber_Availale = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_PhoneNumber_Availale->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_PhoneNumber_Availale->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_PhoneNumber_Availale->setAllowBlank(false);
		$objValidation_PhoneNumber_Availale->setShowDropDown(true);
		$objValidation_PhoneNumber_Availale->setFormula1('=AvailablePhoneNumberRange');


		//Call Pickup Groups Dropdown List
		$objReader->getActiveSheet()->SetCellValue('BJ1', 'Call Pickup Groups');
		$objReader->getActiveSheet()->SetCellValue('BJ2', "None");
		$sheetRow = 3;
		require_once("/var/www/lib/broadsoft/adminPortal/groupCallPickup.php");
		$groupCP = new GroupCallPickup();
		$groupCallPickupList = $groupCP->showGroupCallPickupInstanceCSV();
		if (isset($groupCallPickupList)) {
			$groupCallPickupList = subval_sort($groupCallPickupList);
			foreach ( $groupCallPickupList as $groupCallPickup ) {
				$objReader->getActiveSheet()->SetCellValue('BJ' . $sheetRow, $groupCallPickup);
				$sheetRow++;
			}
		}
		$sheetRow--;
		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange(
				'CPGRange',
				$objReader->getActiveSheet(),
				'$BJ$2:$BJ$' . $sheetRow
			)
		);
		$objReader->getActiveSheet()->getColumnDimension('BJ')->setVisible(FALSE);
		$objValidation_CPG = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_CPG->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_CPG->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_CPG->setAllowBlank(false);
		$objValidation_CPG->setShowDropDown(true);
		$objValidation_CPG->setFormula1('=CPGRange');


		//Yes/No Action Dropdown
		$objValidation_yes_no = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation_yes_no->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_yes_no->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_yes_no->setAllowBlank(false);
		$objValidation_yes_no->setShowDropDown(true);
		$objValidation_yes_no->setFormula1('"Yes,No"');


		//Custom Contact Directory Dropdown
		$directories = isset($directories) ? $directories : array();
		array_unshift($directories, "None");
		$objValidation_ccd = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation_ccd->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_ccd->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_ccd->setAllowBlank(false);
		$objValidation_ccd->setShowDropDown(true);
		$objValidation_ccd->setFormula1('"' . implode(',', $directories ). '"');

		$download_status['d_progress'] = 45;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		
		//Insert Users
		$sheetRow = 3;

		if ($_POST['filterUsers'] == 'All' || $_POST['filterUsers'] == 'FilterUsers') {
			//Manipulate 'Users'

			try {

				ob_start();

				if ($_POST['filterUsers'] == 'FilterUsers') {
					$users = $_POST['usersSelected'];
				} else {
					require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/users/getAllUsers.php");
					$users = array_msort($users, array('firstName' => SORT_ASC, 'LastName'=>SORT_ASC));
				}

				if (isset($users)) {

					$download_status['d_progress'] = 50;
					file_put_contents("/tmp/$download_id", json_encode($download_status));
					setServerErrorInTempFile();
					
					require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getGroupDomains.php");
					require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/arrays.php");
					require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getUserAssignedUserServices.php");

					foreach ($users as $user_row) {

						if ($_POST['filterUsers'] == 'FilterUsers') {
							$userId = $user_row;
						} else {
							$userId = $user_row["userId"];
						}

						unset($userInfo);
						require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getUserInfo.php");
						//error_log(print_r($userInfo, true));

						$voicemail = (isset($_SESSION["userInfo"]["voiceMessagingUser"]) && $_SESSION["userInfo"]["voiceMessagingUser"] == "true") ? "Yes" : "No";
						$vm_active = $_SESSION["userInfo"]["isActive"] == "true" ? "Yes" : "No";
						$phoneNumberStatus = (!empty($userInfo["phoneNumber"])) ? ($_SESSION["userInfo"]["uActivateNumber"] == "Yes" ? "Yes" : "No") : "";

						$objReader->getActiveSheet()->SetCellValue(colFirstName . $sheetRow, $userInfo["firstName"]);
						$objReader->getActiveSheet()->SetCellValue(colLastName . $sheetRow, $userInfo["lastName"]);
						$objReader->getActiveSheet()->SetCellValue(colCallingLineIdFirstName . $sheetRow, $userInfo["callingLineIdFirstName"]);
						$objReader->getActiveSheet()->SetCellValue(colCallingLineIdLastName . $sheetRow, $userInfo["callingLineIdLastName"]);
						if($dialingNameFieldsVisible == "true") {
							$objReader->getActiveSheet()->SetCellValue(colNameDialingFirstName . $sheetRow, $userInfo["nameDialingFirstName"]);
							$objReader->getActiveSheet()->SetCellValue(colNameDialingLastName . $sheetRow, $userInfo["nameDialingLastName"]);
						}
						$objReader->getActiveSheet()->SetCellValue(colPhoneNumber . $sheetRow, $userInfo["phoneNumber"]);
						$objReader->getActiveSheet()->SetCellValue(colActivateNumber . $sheetRow, $phoneNumberStatus);

						//Code added @ 16 Jan 2019
						$defaultCountryCode = $userInfo["countryCode"];
						if($defaultCountryCode == ""){
							$countryCodeArr     = $cmnUtilObj->getDefaultCountryCode();
							if(isset($countryCodeArr['Success']) && !empty($countryCodeArr['Success'])){
								$defaultCountryCode = $countryCodeArr['Success']['defaultCountryCode'];
							}
						}
						if(stripos($userInfo["callingLineIdPhoneNumber"],"+") !== false) {
							$defaultCountryCode = "+$defaultCountryCode";
							$callingLineIdNumberArr = explode($defaultCountryCode, $userInfo["callingLineIdPhoneNumber"]);
							$objReader->getActiveSheet()->SetCellValue(colCallerId . $sheetRow, $callingLineIdNumberArr[1]);
						}else{
							$objReader->getActiveSheet()->SetCellValue(colCallerId . $sheetRow, $userInfo["callingLineIdPhoneNumber"]);
						}
						//End code
						//$objReader->getActiveSheet()->SetCellValue(colCallerId . $sheetRow, $userInfo["callingLineIdPhoneNumber"]);

						$objReader->getActiveSheet()->SetCellValue(colExtension . $sheetRow, $userInfo["extension"]);
						if($locationFieldVisible == "true") {
							$objReader->getActiveSheet()->SetCellValue(colLocation . $sheetRow, $userInfo["addressLocation"]);
						}
						$objReader->getActiveSheet()->SetCellValue(colAssignVM . $sheetRow, $voicemail);
						//$objReader->getActiveSheet()->SetCellValue(colEnableVM . $sheetRow, $vm_active);
						$objReader->getActiveSheet()->SetCellValue(colEmail . $sheetRow, $userInfo["emailAddress"]);
						$objReader->getActiveSheet()->SetCellValue(colDeviceType . $sheetRow, $userInfo["deviceType"]);
						$objReader->getActiveSheet()->SetCellValue(colMACAddress . $sheetRow, $userInfo["macAddress"]);
						$objReader->getActiveSheet()->SetCellValue(colNCS . $sheetRow, $userInfo["networkClassOfService"]);
						if(is_array($userInfo["servicePacksAssigned"])) {
							if (($key = array_search($_POST['Users_voiceMailServicePack'], $userInfo["servicePacksAssigned"])) !== false) {
								unset($userInfo["servicePacksAssigned"][$key]);
							}
							$firstServicePack = count($userInfo["servicePacksAssigned"]) > 0 ? array_shift($userInfo["servicePacksAssigned"]) : "";
							$objReader->getActiveSheet()->SetCellValue(colServicePack . $sheetRow, $firstServicePack );
							$objReader->getActiveSheet()->SetCellValue(colServicePacks . $sheetRow, implode(";", $userInfo["servicePacksAssigned"]));
						}
						
						/* user services */
						if(is_array($userInfo["servicesAssigned"])) {
						    $firstService = count($userInfo["servicesAssigned"]) > 0 ? array_shift($userInfo["servicesAssigned"]) : "";
						    $objReader->getActiveSheet()->SetCellValue(colServices1 . $sheetRow, $firstService );
						    $objReader->getActiveSheet()->SetCellValue(colServices2 . $sheetRow, implode(";", $userInfo["servicesAssigned"]));
						}
						
						if ( isset($userInfo["deviceType"]) && strlen($userInfo["deviceType"]) ) {

							if(isset($userInfo["deviceName"])) {
								//get the custom tags added in express
								$currentCustomTags = getCurrentCustomTagsForDevice($sp, $groupId, $userInfo["deviceName"]);
								if(isset($currentCustomTags['%Express_Tag_Bundle%']) && isset($currentCustomTags['%Express_Tag_Bundle%'][0])) {
									$objReader->getActiveSheet()->SetCellValue(colTagBundles . $sheetRow, $currentCustomTags['%Express_Tag_Bundle%'][0]);
								}
								if(isset($currentCustomTags['%phone-template%']) && isset($currentCustomTags['%phone-template%'][0])) {
									$objReader->getActiveSheet()->SetCellValue(colPhoneProfile . $sheetRow, $currentCustomTags['%phone-template%'][0]);
								}

								//error_log(print_r($currentCustomTags, true));
							}

							//Analog Device Type
							if( in_array($userInfo["deviceType"], $deviceTypesAnalogGateways) ) {

								require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
								if (isset($userInfo["deviceName"]) && strlen($userInfo["deviceName"]) > 2 && strpos($deviceNameAnalog, "<" . ResourceNameBuilder::INT_2 . ">") !== false) {
									$deviceIndex = substr($userInfo["deviceName"], -2);
									$objReader->getActiveSheet()->SetCellValue(colDeviceIndex . $sheetRow, intval($deviceIndex));
									$objReader->getActiveSheet()->SetCellValue(colDeviceName . $sheetRow, $userInfo["deviceName"]);
								}
								$objReader->getActiveSheet()->SetCellValue(colPortNumber . $sheetRow, $userInfo["portNumber"]);

								$objReader->getActiveSheet()->getCell(colDeviceType . $sheetRow)->setDataValidation($objValidation_analogDeviceType);

							} else {

								$objReader->getActiveSheet()->getCell(colDeviceType . $sheetRow)->setDataValidation($objValidation_sipDeviceType);

							}

						} else {

							$objReader->getActiveSheet()->getCell(colDeviceType . $sheetRow)->setDataValidation($objValidation_allDeviceType);
						}

						$objReader->getActiveSheet()->SetCellValue(colCallPickupGroup . $sheetRow, isset($userInfo["usersGroupName"]) ? $userInfo["usersGroupName"] : "");

						//Polycom Phone Services - Starts
						$hasPolycomPhoneServices = checkIfUserHasService($userId, 'Polycom Phone Services');
						$userPolycomPhoneServices = getUserPolycomPhoneServiceValues($userId, $userInfo["deviceName"]);

						//error_log($userInfo["firstName"] . " : " . print_r($userPolycomPhoneServices, true));

						if($hasPolycomPhoneServices) {
							$hasPolycomPhoneServices = false;
							if(isset($userPolycomPhoneServices) && isset($userPolycomPhoneServices['integration']) && $userPolycomPhoneServices['integration'] == 'true') {
								$hasPolycomPhoneServices = true;
							}
						}
						$ccd = isset($userPolycomPhoneServices) && isset($userPolycomPhoneServices['ccd']) && $userPolycomPhoneServices['ccd'] != "" ? $userPolycomPhoneServices['ccd'] : "None";

						//error_log($userInfo["firstName"] . " : $hasPolycomPhoneServices");

						$objReader->getActiveSheet()->SetCellValue(colPolycomPhoneServices . $sheetRow, $hasPolycomPhoneServices ? "Yes" : "No");
						$objReader->getActiveSheet()->SetCellValue(colCustomContactDirectory . $sheetRow, $ccd);
						//Polycom Phone Services - Ends

						//error_log(print_r(getUserAssignedUserServices($userId), true));

						$objReader->getActiveSheet()->SetCellValue(colUserID . $sheetRow, $userId);

						$objReader->getActiveSheet()->getCell(colAction . $sheetRow)->setDataValidation($objValidation_action);

						$sheetRow++;

					}


					$download_status['d_progress'] = 60;
					file_put_contents("/tmp/$download_id", json_encode($download_status));
					setServerErrorInTempFile();

				}
				ob_get_clean();

			} catch (Exception $exception) {
				error_log($exception->getMessage());
			}


		}

		$download_status['d_progress'] = 70;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		
		$objReader->getActiveSheet()->setDataValidation(colAction            . "$sheetRow:" . colAction            . "2000", $objValidation_all_actions);
		$objReader->getActiveSheet()->setDataValidation(colPhoneNumber       . "3:"         . colPhoneNumber       . "2000", $objValidation_PhoneNumber_Availale);
		$objReader->getActiveSheet()->setDataValidation(colActivateNumber    . "3:"         . colActivateNumber    . "2000", $objValidation_yes_no);
		$objReader->getActiveSheet()->setDataValidation(colCallerId          . "3:"         . colCallerId          . "2000", $objValidation_PhoneNumber);
		$objReader->getActiveSheet()->setDataValidation(colDeviceType        . "$sheetRow:" . colDeviceType        . "2000", $objValidation_allDeviceType);
		$objReader->getActiveSheet()->setDataValidation(colPhoneProfile      . "3:"         . colPhoneProfile      . "2000", $objValidation_phoneProfile);
		$objReader->getActiveSheet()->setDataValidation(colTagBundles        . "3:"         . colTagBundles        . "2000", $objValidation_tagBundle);
		$objReader->getActiveSheet()->setDataValidation(colNumLineAppearanes . "3:"         . colNumLineAppearanes . "2000", $objValidation_line_appearances);
		$objReader->getActiveSheet()->setDataValidation(colCallsPerLineKey   . "3:"         . colCallsPerLineKey   . "2000", $objValidation_call_per_linekey);
		$objReader->getActiveSheet()->setDataValidation(colLineRegNumber     . "3:"         . colLineRegNumber     . "2000", $objValidation_DeviceRegLines);
		$objReader->getActiveSheet()->setDataValidation(colNCS               . "3:"         . colNCS               . "2000", $objValidation_NCOS);
		$objReader->getActiveSheet()->setDataValidation(colServices1          . "3:"         . colServices1       . "2000", $objValidation_services);
		$objReader->getActiveSheet()->setDataValidation(colServices2          . "3:"         . colServices2       . "2000", $objValidation_services);
		$objReader->getActiveSheet()->setDataValidation(colServicePack       . "3:"         . colServicePack       . "2000", $objValidation_servicePackage);
		$objReader->getActiveSheet()->setDataValidation(colServicePacks      . "3:"         . colServicePacks      . "2000", $objValidation_servicePackage);
		$objReader->getActiveSheet()->setDataValidation(colDeviceName        . "3:"         . colDeviceName        . "2000", $objValidation_DeviceNames);
		$objReader->getActiveSheet()->setDataValidation(colPortNumber        . "3:"         . colPortNumber        . "2000", $objValidation_DevicePorts);
		$objReader->getActiveSheet()->setDataValidation(colCallPickupGroup   . "3:"         . colCallPickupGroup   . "2000", $objValidation_CPG);
		$objReader->getActiveSheet()->setDataValidation(colPolycomPhoneServices . "3:"      . colPolycomPhoneServices   . "2000", $objValidation_yes_no);
		$objReader->getActiveSheet()->setDataValidation(colCustomContactDirectory . "3:"      . colCustomContactDirectory   . "2000", $objValidation_ccd);


		//Hide Columns
		//$objReader->getActiveSheet()->getColumnDimension(colEnableVM)->setVisible(FALSE);
		$objReader->getActiveSheet()->getColumnDimension(colCombinedServices)->setVisible(FALSE);
		$objReader->getActiveSheet()->getColumnDimension(colUserID)->setVisible(FALSE);
		if($locationFieldVisible != "true") {
			$objReader->getActiveSheet()->getColumnDimension(colLocation)->setVisible(FALSE);
		}
		//Name Dialing First and Last Name Columns
		if(!($ociVersion=="20" || $ociVersion=="21" || $ociVersion=="22") || $dialingNameFieldsVisible != "true"){
			$objReader->getActiveSheet()->getColumnDimension(colNameDialingFirstName)->setVisible(FALSE);
			$objReader->getActiveSheet()->getColumnDimension(colNameDialingLastName)->setVisible(FALSE);
		}

		//Custom Tags Sheet
		//All Action Dropdown
		$objValidation_all_actions = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_all_actions->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_all_actions->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_all_actions->setAllowBlank(false);
		$objValidation_all_actions->setShowDropDown(true);
		$objValidation_all_actions->setFormula1('"Add/Modify,Delete"');
		$objReader->setActiveSheetIndex(5);
		$objReader->getActiveSheet()->setDataValidation("A2:A2000", $objValidation_all_actions);
		$objReader->getActiveSheet()->setDataValidation("I2:I2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("K2:K2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("M2:M2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("O2:O2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("Q2:Q2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("S2:S2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("U2:U2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("W2:W2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("Y2:Y2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("AA2:AA2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("AC2:AC2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("AE2:AE2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("AG2:AG2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("AI2:AI2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("AK2:AK2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("AM2:AM2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("AO2:AO2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("AQ2:AQ2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("AS2:AS2000", $objValidation_DeviceCustomTags);
		$objReader->getActiveSheet()->setDataValidation("AU2:AU2000", $objValidation_DeviceCustomTags);


		$objReader->getActiveSheet()->getProtection()->setSheet(true);
		$objReader->getActiveSheet()->getProtection()->setSort(true);
		$objReader->getActiveSheet()->getProtection()->setInsertRows(true);
		$objReader->getActiveSheet()->getProtection()->setFormatCells(true);
		$objReader->getActiveSheet()->getProtection()->setPassword('averistar');

		//Express Sheet
		$objReader->setActiveSheetIndex(6);
		$objReader->getActiveSheet()->getProtection()->setSheet(true);
		$objReader->getActiveSheet()->getProtection()->setPassword('averistar');

		//Express Sheet - Hide Columns
		//Location
		if($locationFieldVisible != "true") {
			$objReader->getActiveSheet()->getColumnDimension(expressSheetColLocation)->setVisible(FALSE);
		}
		//Name Dialing First and Last Name Columns
		if(!($ociVersion=="20" || $ociVersion=="21" || $ociVersion=="22") || $dialingNameFieldsVisible != "true"){
			$objReader->getActiveSheet()->getColumnDimension(expressSheetColNameDialingFirstName)->setVisible(FALSE);
			$objReader->getActiveSheet()->getColumnDimension(expressSheetColNameDialingLastName)->setVisible(FALSE);
		}




		$objReader->setActiveSheetIndex(1);

		$download_status['d_progress'] = 80;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		
		$objWriter = PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objReader, 'Xlsx');
		$tmp_file_name = tempnam(sys_get_temp_dir(), "ExpressSheet_User_$revision\_");
		$objWriter->setPreCalculateFormulas(false);
		$objWriter->save($tmp_file_name);

		ob_clean();
		$file_name = "$expressRevision-USR-ES-$revision-" . time() . ".xlsm";
		/*
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Transfer-Encoding: Binary");
		header("Content-disposition: attachment; filename=\"$file_name\"");
		readfile($tmp_file_name);
		*/

		$download_status['d_progress'] = 100;
		$download_status['fileName'] = $file_name;
		$download_status['filePath'] = $tmp_file_name;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		exit;

	} else if ($expressSheetType == 'SCA') {

		$revision = $express_sheet_revision['SCA'];

		$file = $_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/expressSheets/templates/SCA-Rel-$revision.xlsx";

		$objReader = PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
		$objReader = $objReader->load($file);

		$download_status['d_progress'] = 20;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		//Manipulate 'Group'
		$objReader->setActiveSheetIndex(0);

		$objReader->getActiveSheet()->setCellValue('C12', "Rev.$revision")
			->setCellValue('C13', "Requires Express $expressRevision or higher")
			->setCellValue('G12', date("m/d/Y"))
			->setCellValue('E16', $groupId)
			->setCellValue('E18', $groupInfoData['addressLine1'])
			->setCellValue('E19', $groupInfoData['addressLine2'])
			->setCellValue('E20', $groupInfoData['city'])
			->setCellValue('E21', $groupInfoData['stateOrProvince'])
			->setCellValue('E22', $groupInfoData['zipOrPostalCode'])
			->setCellValue('E24', $groupInfoData["timeZone"])
			->setCellValue('F28', $_POST["SCA_Active"])
			->setCellValue('F29', $_POST["SCA_AllowOrigination"])
			->setCellValue('F30', $_POST["SCA_AllowTermination"])
			->setCellValue('F31', $_POST["SCA_AllowAllClicktoDial"])
			->setCellValue('F32', $_POST["SCA_AllowAllGroupPaging"])
			->setCellValue('F33', $_POST["SCA_AllowCallRetrieve"])
			->setCellValue('F34', $_POST["SCA_MultipleCallArrangement"])
			->setCellValue('F35', $_POST["SCA_CallParkNotification"])
			->setCellValue('F36', $_POST["SCA_AllowBridging"])
			->setCellValue('F37', $_POST["SCA_BridgeWarningTone"]);

		insert_logo($objReader, 'C', 2, $brandingLogoPath);

		$download_status['d_progress'] = 30;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		//Users Sheet
		$objReader->setActiveSheetIndex(2);

		ob_start();
		require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/users/getAllUsers.php");
		$users = array_msort($users, array('firstName' => SORT_ASC, 'LastName'=>SORT_ASC));

		$download_status['d_progress'] = 40;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		$sheetRow = 2;
		$deviceRegLineRow = 2;
		foreach ($users as $user) {

			$userId = $user["userId"];
			$userName = $user["LastName"] . ", " . $user["firstName"];
			$phoneNumber = $user["phoneNumber"];
			$extension = $user["extension"];
			$deviceType = $user["deviceType"];

			$phoneNumber = str_replace ("+1-" , "", $phoneNumber);

			$objReader->getActiveSheet()->SetCellValue('A' . $sheetRow, $userName . " (" . $phoneNumber . "x" . $extension . ")");
			$objReader->getActiveSheet()->SetCellValue('B' . $sheetRow, $userId);

			//Reg. Line Numbers
			$objReader->getActiveSheet()->SetCellValue('D' . ++$deviceRegLineRow, "");
			$deviceRegLineListStartIndex = $deviceRegLineRow;
			if($ports = getPortsForDeviceType($deviceType)) {

				$deviceRegLines = $ports['ports'];
				for ($deviceRegLine = 1; $deviceRegLine <= $deviceRegLines; $deviceRegLine++) {
					$objReader->getActiveSheet()->SetCellValue('D' . ++$deviceRegLineRow, $deviceRegLine);
				}
				$deviceRegLineListEndIndex = $deviceRegLineRow;
				if ($deviceRegLineListEndIndex >= $deviceRegLineListStartIndex) {
					$objReader->addNamedRange(
						new PhpOffice\PhpSpreadsheet\NamedRange(
							"DeviceRegLineNumberRange_" . $sheetRow,
							$objReader->getActiveSheet(),
							'$D$' . $deviceRegLineListStartIndex . ':$D$' . $deviceRegLineListEndIndex
						)
					);
				}
			}

			$sheetRow++;

		}

		$download_status['d_progress'] = 60;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		$user_sheet_row = $sheetRow - 1;

		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange('UsersList', $objReader->getActiveSheet(), 'A2:B' . $user_sheet_row)
		);

		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange('AllUsers', $objReader->getActiveSheet(), 'A2:A' . $user_sheet_row)
		);

		//$objReader->getActiveSheet()->protectCells('A1:A2000', 'averistar');
		//$objReader->getActiveSheet()->getProtection()->setSheet(true);

		//Manipulate SCA SHEET
		$objReader->setActiveSheetIndex(1);

		//Users DropDown
		$objValidation = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation->setAllowBlank(false);
		$objValidation->setShowDropDown(true);
		$objValidation->setFormula1('=\'Users\'!$A$2:$A$' . $user_sheet_row);

		//All Action Dropdown
		$objValidation_all_actions = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation_all_actions->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_all_actions->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_all_actions->setAllowBlank(false);
		$objValidation_all_actions->setShowDropDown(true);
		$objValidation_all_actions->setFormula1('"Add"');

		//Modify or Delete Action Dropdown
		$objValidation_action = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation_action->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_action->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_action->setAllowBlank(false);
		$objValidation_action->setShowDropDown(true);
		$objValidation_action->setFormula1('"Modify,Delete"');

		//Set Dropdown for SCA Users
		$objReader->getActiveSheet()->setDataValidation("D3:D2000", $objValidation);


		$objConditionalStyle_SameUser_error = new PhpOffice\PhpSpreadsheet\Style\Conditional();
		$objConditionalStyle_SameUser_error->setConditionType(PhpOffice\PhpSpreadsheet\Style\Conditional::CONDITION_EXPRESSION)
			->setOperatorType(PhpOffice\PhpSpreadsheet\Style\Conditional::OPERATOR_EQUAL)
			->addCondition('IF(B3<>"",IF(B3<>D3,FALSE,TRUE), FALSE)');
		$objConditionalStyle_SameUser_error->getStyle()->getFont()->getColor()->setRGB('d40808');
		$objConditionalStyle_SameUser_error->getStyle()->getFont()->setBold(true);
		$objConditionalStyle_SameUser_error->getStyle()->getBorders()->getBottom()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK)->getColor()->setRGB('d40808');

		$conditionalStyles = $objReader->getActiveSheet()->getStyle('D3')
			->getConditionalStyles();
		array_push($conditionalStyles, $objConditionalStyle_SameUser_error);
		$objReader->getActiveSheet()->getStyle('D3:D2000')
			->setConditionalStyles($conditionalStyles);

		$phoneProfiles = getAllPhoneProfiles();
		$phoneProfilesArray = array();
		foreach ($phoneProfiles as $phoneProfile) {
			$phoneProfilesArray[] = $phoneProfile['phoneProfile'];
		}
		//Phone Profiles Dropdown
		$objValidation_phoneProfile = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_phoneProfile->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_phoneProfile->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_phoneProfile->setAllowBlank(false);
		$objValidation_phoneProfile->setShowDropDown(true);
		$objValidation_phoneProfile->setFormula1('"' . implode(",", $phoneProfilesArray ) . '"');

		$tagBundles = getAllTagBundles();
		$tagBundlesArray = array();
		foreach ($tagBundles as $tagBundle) {
			$tagBundlesArray[] = $tagBundle['tagBundle'];
		}
		//Tag Bundle Dropdown
		$objValidation_tagBundle = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_tagBundle->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_tagBundle->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_tagBundle->setAllowBlank(false);
		$objValidation_tagBundle->setShowDropDown(true);
		$objValidation_tagBundle->setFormula1('"' . implode(",", $tagBundlesArray ) . '"');

		//Line Apperances Dropdown
		$objValidation_line_appearances = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_line_appearances->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_line_appearances->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_line_appearances->setAllowBlank(false);
		$objValidation_line_appearances->setShowDropDown(true);
		$objValidation_line_appearances->setFormula1('"1,2,3,4,5,6"');

		//Calls per LineKey Dropdown
		$objValidation_call_per_linekey = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_call_per_linekey->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_call_per_linekey->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_call_per_linekey->setAllowBlank(false);
		$objValidation_call_per_linekey->setShowDropDown(true);
		$objValidation_call_per_linekey->setFormula1('"1,2,3,4,5,6,7,8,9,10,11,12"');

		$objValidation_DeviceRegLines = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();
		$objValidation_DeviceRegLines->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_DeviceRegLines->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_DeviceRegLines->setAllowBlank(false);
		$objValidation_DeviceRegLines->setShowInputMessage(false);
		$objValidation_DeviceRegLines->setShowErrorMessage(true);
		$objValidation_DeviceRegLines->setShowDropDown(true);
		$objValidation_DeviceRegLines->setErrorTitle('Input error');
		$objValidation_DeviceRegLines->setError('Value is not in list.');
		$objValidation_DeviceRegLines->setFormula1('=INDIRECT("DeviceRegLineNumberRange_"&I3)');
		//$objValidation_DeviceRegLines->setFormula1('=INDIRECT("DeviceRegLineNumberRange_"&MATCH(B3,$A$2:$A$2000,0))');


		$sheetRow = 3;

		if ($_POST['filterUsers'] == 'All' || $_POST['filterUsers'] == 'FilterUsers') {

			$objReader->setActiveSheetIndex(1);

			try {

				if ($_POST['filterUsers'] == 'FilterUsers') {
					$users = $_POST['usersSelected'];
				}

				if (isset($users)) {

					$user_array = array();

					foreach ($users as $user_row) {

						if ($_POST['filterUsers'] == 'FilterUsers') {
							$userId = $user_row;
						} else {
							$userId = $user_row["userId"];
						}

						//$phoneNumber = $user_row["phoneNumber"];
						unset($userInfo);
						require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getUserInfo.php");

						$userName = $userInfo["lastName"] . ", " . $userInfo["firstName"];
						$phoneNumber = $userInfo["phoneNumber"];
						$extension = $userInfo["extension"];
						$deviceName = $userInfo["deviceName"];

						if (isset($userInfo["sharedCallAppearanceUsers"]))
						{
							$user_array[$userId] = array('userName' => $userName, 'phoneNumber' => $phoneNumber, 'extension' => $extension, 'deviceName' => $deviceName);
							$user_array[$userId]['sharedCallAppearanceUsers'] = $userInfo['sharedCallAppearanceUsers'];
						}

					}


					//error_log(print_r($user_array, true));

					foreach ($user_array as $user_id => $user) {

						if ( isset($user["sharedCallAppearanceUsers"]) && is_array($user["sharedCallAppearanceUsers"]) ) {

							$user_sharedCallAppearanceUsers = array_msort($user["sharedCallAppearanceUsers"], array('port'=>SORT_ASC));

							foreach ($user_sharedCallAppearanceUsers as $sca) {

								if ($sca["endpointType"] == "Shared Call Appearance") {

									//error_log(print_r($sca, true));
									$objReader->getActiveSheet()->SetCellValue('B' . $sheetRow, $user['userName'] . " (" . $user['phoneNumber'] . "x" . $user['extension'] . ")");
									$objReader->getActiveSheet()->SetCellValue('C' . $sheetRow, $sca['port']);
									//$objReader->getActiveSheet()->SetCellValue('D' . $sheetRow, $sca['name'] . " ({$sca['phoneNumber']}x{$sca['extension']})");
									if(!empty($sca['phoneNumber']) && $sca['phoneNumber'] != ""){
										$objReader->getActiveSheet()->SetCellValue('D' . $sheetRow, $sca['name'] . " ({$sca['phoneNumber']}x{$sca['extension']})");
									}else{
										$userDetails = getUserDetails($sca["id"]);
										$objReader->getActiveSheet()->SetCellValue('D' . $sheetRow, $sca['name'] . " ({$userDetails['phoneNumber']}x{$userDetails['extension']})");
									}
									//$objReader->getActiveSheet()->SetCellValue('F' . $sheetRow, $sca['name']);
									//$objReader->getActiveSheet()->SetCellValue('J' . $sheetRow, $sca['linePort']);

									$objReader->getActiveSheet()->getCell('A' . $sheetRow)->setDataValidation($objValidation_action);

									$sheetRow++;
								}
							}

						}
					}
				}

			} catch (Exception $exception) {
				error_log($exception->getMessage());
			}

		}
		$objReader->getActiveSheet()->setDataValidation("B$sheetRow:B2000", $objValidation);
		$objReader->getActiveSheet()->setDataValidation("A$sheetRow:A2000", $objValidation_all_actions);
		//$objReader->getActiveSheet()->setDataValidation("F3:F2000", $objValidation_phoneProfile);
		//$objReader->getActiveSheet()->setDataValidation("G3:G2000", $objValidation_tagBundle);
		$objReader->getActiveSheet()->setDataValidation("F3:F2000", $objValidation_line_appearances);
		$objReader->getActiveSheet()->setDataValidation("G3:G2000", $objValidation_call_per_linekey);
		$objReader->getActiveSheet()->setDataValidation("H3:H2000", $objValidation_DeviceRegLines);

		//Hide Columns
		$objReader->getActiveSheet()->getColumnDimension('I')->setVisible(FALSE);

		//Protect Express Sheet
		$objReader->setActiveSheetIndex(3);
		$objReader->getActiveSheet()->getProtection()->setSheet(true);
		$objReader->getActiveSheet()->getProtection()->setSort(true);
		$objReader->getActiveSheet()->getProtection()->setInsertRows(true);
		$objReader->getActiveSheet()->getProtection()->setFormatCells(true);

		$objReader->getActiveSheet()->getProtection()->setPassword('averistar');

		//Set SCA Sheet as default when opened.
		$objReader->setActiveSheetIndex(1);

		ob_get_clean();


		$download_status['d_progress'] = 80;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		$objWriter = PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objReader, 'Xlsx');
		$tmp_file_name = tempnam(sys_get_temp_dir(), "ExpressSheet_SCA_$revision\_");
		//$objWriter->setPreCalculateFormulas(false);
		$objWriter->save($tmp_file_name);

		ob_clean();
		$file_name = "$expressRevision-SCA-ES-$revision-" . time() . ".xlsx";
		/*
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Transfer-Encoding: Binary");
		header("Content-disposition: attachment; filename=\"$file_name\"");
		readfile($tmp_file_name);
		*/

		$download_status['d_progress'] = 100;
		$download_status['fileName'] = $file_name;
		$download_status['filePath'] = $tmp_file_name;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		exit;

	} else if ($expressSheetType == 'BLF') {

		$revision = $express_sheet_revision['BLF'];

		$file = $_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/expressSheets/templates/BLF-Rel-$revision.xlsx";

		$objReader = PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
		$objReader = $objReader->load($file);

		$download_status['d_progress'] = 20;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		//Manipulate 'Group'
		$objReader->setActiveSheetIndex(0);

		$callingLineIdDisplayPhoneNumber = str_replace("+1-","",$groupInfoData["callingLineIdDisplayPhoneNumber"]);

		//$objReader->getActiveSheet()->getColumnDimensionByColumn('C')->setWidth('40');
		//$objReader->getActiveSheet()->getColumnDimensionByColumn('C28')->setAutoSize(true);

		$objReader->getActiveSheet()->setCellValue('C12', "Rev.$revision")
			->setCellValue('C13', "Requires Express $expressRevision or higher")
			->setCellValue('G12', date("m/d/Y"))
			->setCellValue('E16', $groupId)
			->setCellValue('E18', $groupInfoData['addressLine1'])
			->setCellValue('E19', $groupInfoData['addressLine2'])
			->setCellValue('E20', $groupInfoData['city'])
			->setCellValue('E21', $groupInfoData['stateOrProvince'])
			->setCellValue('E22', $groupInfoData['zipOrPostalCode'])
			->setCellValue('E24', $groupInfoData["timeZone"])
			->setCellValue('E28', $callingLineIdDisplayPhoneNumber)
			->setCellValue('E29', $groupInfoData["callingLineIdFirstName"])
			->setCellValue('E30', $groupInfoData["callingLineIdLastName"])
			->setCellValue('E31', $_POST['BLF_extensionLength']);

		insert_logo($objReader, 'C', 2, $brandingLogoPath);

		//Users Sheet
		$objReader->setActiveSheetIndex(2);

		ob_start();
		require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/users/getAllUsers.php");

		$download_status['d_progress'] = 40;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		
		$sheetRow = 2;
		foreach ($users as $user) {

			$userId = $user["userId"];
			$userName = $user["LastName"] . ", " . $user["firstName"];
			$phoneNumber = $user["phoneNumber"];
			$extension = $user["extension"];

			$phoneNumber = str_replace ("+1-" , "", $phoneNumber);

			$objReader->getActiveSheet()->SetCellValue('A' . $sheetRow, $userName . " (" . $phoneNumber . "x" . $extension . ")");
			$objReader->getActiveSheet()->SetCellValue('B' . $sheetRow, $userId);

			$sheetRow++;

		}

		$user_sheet_row = $sheetRow - 1;

		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange('UsersList', $objReader->getActiveSheet(), 'A2:B' . $user_sheet_row)
		);

		$download_status['d_progress'] = 30;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		
		//Manipulate 'Users'
		$objReader->setActiveSheetIndex(1);

		//Users DropDown
		$objValidation = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation->setAllowBlank(false);
		$objValidation->setShowDropDown(true);
		$objValidation->setFormula1('=\'UsersList\'!$A$2:$A$2000');

		//All Action Dropdown
		$objValidation_all_actions = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation_all_actions->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_all_actions->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_all_actions->setAllowBlank(false);
		$objValidation_all_actions->setShowDropDown(true);
		$objValidation_all_actions->setFormula1('"Add"');

		//Modify or Delete Action Dropdown
		$objValidation_action = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation_action->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_action->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_action->setAllowBlank(false);
		$objValidation_action->setShowDropDown(true);
		$objValidation_action->setFormula1('"Modify,Delete"');

		//Set Dropdown for BLF Users
		$objReader->getActiveSheet()->setDataValidation("D2:D2000", $objValidation);

		$sheetRow = 2;

		if ($_POST['filterUsers'] == 'All' || $_POST['filterUsers'] == 'FilterUsers') {

			try {

				ob_start();

				if ($_POST['filterUsers'] == 'FilterUsers') {
					$users = $_POST['usersSelected'];
				}

				$download_status['d_progress'] = 40;
				file_put_contents("/tmp/$download_id", json_encode($download_status));
				setServerErrorInTempFile();
				
				if (isset($users)) {

					$user_array = array();

					foreach ($users as $user_row) {

						if ($_POST['filterUsers'] == 'FilterUsers') {
							$userId = $user_row;
						} else {
							$userId = $user_row["userId"];
						}

						unset($userInfo);
						require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getUserInfo.php");

						$userName = $userInfo["lastName"] . ", " . $userInfo["firstName"];
						$phoneNumber = $userInfo["phoneNumber"];
						$extension = $userInfo["extension"];

						$user_array[$userId] = array('userName' => $userName, 'phoneNumber' => $phoneNumber, 'extension' => $extension);

						if (isset($userInfo["monitoredUsers"]))
						{
							$user_array[$userId]['monitoredUsers'] = $userInfo['monitoredUsers'];
						}

					}

					$download_status['d_progress'] = 50;
					file_put_contents("/tmp/$download_id", json_encode($download_status));
					setServerErrorInTempFile();
					
					foreach ($user_array as $user) {

						if (isset($user["monitoredUsers"])) {

							$line = 1;
							foreach ($user["monitoredUsers"] as $monitoredUser) {

								$user_id = $monitoredUser['id'];
								//error_log(print_r($monitoredUser, true));
								$objReader->getActiveSheet()->SetCellValue('B' . $sheetRow, $user['userName'] . " (" . $user['phoneNumber'] . "x" . $user['extension'] . ")");
								$objReader->getActiveSheet()->SetCellValue('C' . $sheetRow, $line++);
								$objReader->getActiveSheet()->SetCellValue('D' . $sheetRow, $monitoredUser['name'] . " ({$user_array[$user_id]['phoneNumber']}x{$user_array[$user_id]['extension']})");

								/*
								$objReader->getActiveSheet()->SetCellValue('B' . $sheetRow, $user['phoneNumber']);
								$objReader->getActiveSheet()->SetCellValue('D' . $sheetRow, $user['extension']);
								$objReader->getActiveSheet()->SetCellValue('F' . $sheetRow, $user['userName'] . " (" . $user['phoneNumber'] . "x" . $user['extension'] . ")");

								$objReader->getActiveSheet()->SetCellValue('G' . $sheetRow, $line++);
								$objReader->getActiveSheet()->SetCellValue('H' . $sheetRow, $user_array[$user_id]['phoneNumber']);
								$objReader->getActiveSheet()->SetCellValue('J' . $sheetRow, $user_array[$user_id]['extension']);
								$objReader->getActiveSheet()->SetCellValue('L' . $sheetRow, $monitoredUser['name'] . "({$user_array[$user_id]['phoneNumber']}x{$user_array[$user_id]['extension']})");
								*/

								$objReader->getActiveSheet()->getCell('A' . $sheetRow)->setDataValidation($objValidation_action);

								$sheetRow++;
							}
						}
					}

					$download_status['d_progress'] = 60;
					file_put_contents("/tmp/$download_id", json_encode($download_status));
					setServerErrorInTempFile();
					
				}
				ob_get_clean();

			} catch (Exception $exception) {
				error_log($exception->getMessage());
			}


		}


		$objReader->getActiveSheet()->setDataValidation("B$sheetRow:B2000", $objValidation);
		$objReader->getActiveSheet()->setDataValidation("A$sheetRow:A2000", $objValidation_all_actions);

		$download_status['d_progress'] = 80;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		
		$objWriter = PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objReader, 'Xlsx');
		$tmp_file_name = tempnam(sys_get_temp_dir(), "ExpressSheet_User_$revision\_");
		$objWriter->save($tmp_file_name);

		ob_clean();
		$file_name = "$expressRevision-BLF-ES-$revision-" . time() . ".xlsx";
		/*
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Transfer-Encoding: Binary");
		header("Content-disposition: attachment; filename=\"$file_name\"");
		readfile($tmp_file_name);
		*/

		$download_status['d_progress'] = 100;
		$download_status['fileName'] = $file_name;
		$download_status['filePath'] = $tmp_file_name;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		exit;

	} else if ($expressSheetType == 'Voicemail') {

		$revision = $express_sheet_revision['Voicemail'];

		$file = $_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/expressSheets/templates/VM-Rel-$revision.xlsx";

		$objReader = PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
		$objReader = $objReader->load($file);

		$download_status['d_progress'] = 20;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		
		//Manipulate 'Group'
		$objReader->setActiveSheetIndex(0);

		$Voicemail_Enable_VM = isset($_POST['Voicemail_Enable_VM']) ? $_POST['Voicemail_Enable_VM'] : "Off";
		$Voicemail_Send_All_Calls_to_VM = isset($_POST['Voicemail_Send_All_Calls_to_VM']) ? $_POST['Voicemail_Send_All_Calls_to_VM'] : "No";
		$Voicemail_Send_Busy_Calls_to_VM = isset($_POST['Voicemail_Send_Busy_Calls_to_VM']) ? $_POST['Voicemail_Send_Busy_Calls_to_VM'] : "No";
		$Voicemail_Send_Unanswered_Calls_to_VM = isset($_POST['Voicemail_Send_Unanswered_Calls_to_VM']) ? $_POST['Voicemail_Send_Unanswered_Calls_to_VM'] : "No";
		$Voicemail_Use_Phone_MWI = isset($_POST['Voicemail_Use_Phone_MWI']) ? $_POST['Voicemail_Use_Phone_MWI'] : "No";
		$Voicemail_Email_Message_Carbon_Copy = isset($_POST['Voicemail_Email_Message_Carbon_Copy']) ? $_POST['Voicemail_Email_Message_Carbon_Copy'] : "No";
		$Voicemail_Carbon_Copy_Address = isset($_POST['Voicemail_Carbon_Copy_Address']) ? $_POST['Voicemail_Carbon_Copy_Address'] : "";
		$Voicemail_Transfer_on_0 = isset($_POST['Voicemail_Transfer_on_0']) ? $_POST['Voicemail_Transfer_on_0'] : "No";
		$Voicemail_Transfer_on_0_DN = isset($_POST['Voicemail_Transfer_on_0_DN']) ? $_POST['Voicemail_Transfer_on_0_DN'] : "";
		$Voicemail_Mailbox_Limit = isset($_POST['Voicemail_Mailbox_Limit']) ? $_POST['Voicemail_Mailbox_Limit'] : "";
		$Voicemail_Use_Unified_Messaging = "Yes";

		$objReader->getActiveSheet()->setCellValue('C12', "Rev.$revision")
			->setCellValue('C13', "Requires Express $expressRevision or higher")
			->setCellValue('G12', date("m/d/Y"))
			->setCellValue('E16', $groupId)
			->setCellValue('E18', $groupInfoData['addressLine1'])
			->setCellValue('E19', $groupInfoData['addressLine2'])
			->setCellValue('E20', $groupInfoData['city'])
			->setCellValue('E21', $groupInfoData['stateOrProvince'])
			->setCellValue('E22', $groupInfoData['zipOrPostalCode'])
			->setCellValue('E24', $groupInfoData["timeZone"])
			->setCellValue('F28', $Voicemail_Enable_VM)
			->setCellValue('F29', $Voicemail_Send_All_Calls_to_VM)
			->setCellValue('F30', $Voicemail_Send_Busy_Calls_to_VM)
			->setCellValue('F31', $Voicemail_Send_Unanswered_Calls_to_VM)
			->setCellValue('F32', $Voicemail_Use_Unified_Messaging)
			->setCellValue('F33', $Voicemail_Use_Phone_MWI)
			->setCellValue('F34', $Voicemail_Email_Message_Carbon_Copy)
			->setCellValue('F35', $Voicemail_Carbon_Copy_Address)
			->setCellValue('F36', $Voicemail_Transfer_on_0)
			->setCellValue('F37', $Voicemail_Transfer_on_0_DN)
			->setCellValue('F38', $Voicemail_Mailbox_Limit);

		insert_logo($objReader, 'C', 2, $brandingLogoPath);

		//Users Sheet
		$objReader->setActiveSheetIndex(2);

		ob_start();
		$extraUserInfo = 1;
		require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/users/getAllUsers.php");

		$download_status['d_progress'] = 40;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		
		$sheetRow = 2;
		$users = array_msort($users, array('LastName'=>SORT_ASC, 'firstName' => SORT_ASC));
		foreach ($users as $user) {

			$userId = $user["userId"];
			$userName = $user["LastName"] . ", " . $user["firstName"];
			$phoneNumber = $user["phoneNumber"];
			$extension = $user["extension"];

			$phoneNumber = str_replace ("+1-" , "", $phoneNumber);

			$objReader->getActiveSheet()->SetCellValue('A' . $sheetRow, $userName . " (" . $phoneNumber . "x" . $extension . ")");
			$objReader->getActiveSheet()->SetCellValue('B' . $sheetRow, $userId);

			$sheetRow++;

		}

		$user_sheet_row = $sheetRow - 1;

		$objReader->addNamedRange(
			new PhpOffice\PhpSpreadsheet\NamedRange('UsersList', $objReader->getActiveSheet(), 'A2:B' . $user_sheet_row)
		);

		$download_status['d_progress'] = 30;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		
		//Manipulate 'Users'
		$objReader->setActiveSheetIndex(1);

		//Users DropDown
		$objValidation = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation->setAllowBlank(false);
		$objValidation->setShowDropDown(true);
		$objValidation->setFormula1('=\'Users\'!$A$2:$A$2000');

		//All Action Dropdown
		$objValidation_all_actions = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation_all_actions->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_all_actions->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_all_actions->setAllowBlank(false);
		$objValidation_all_actions->setShowDropDown(true);
		$objValidation_all_actions->setFormula1('"Add"');

		//Modify or Delete Action Dropdown
		$objValidation_action = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation_action->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_action->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_action->setAllowBlank(false);
		$objValidation_action->setShowDropDown(true);
		$objValidation_action->setFormula1('"Modify"');

		//Yes/No Action Dropdown
		$objValidation_yes_no = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation_yes_no->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_yes_no->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_yes_no->setAllowBlank(false);
		$objValidation_yes_no->setShowDropDown(true);
		$objValidation_yes_no->setFormula1('"Yes,No"');

		//On/Off Action Dropdown
		$objValidation_on_off = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation_on_off->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_on_off->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_on_off->setAllowBlank(false);
		$objValidation_on_off->setShowDropDown(true);
		$objValidation_on_off->setFormula1('"On,Off"');

		//Mailbox Limit Dropdown
		$objValidation_mailbox_limit = new PhpOffice\PhpSpreadsheet\Cell\DataValidation();//$objReader->getActiveSheet()->getDataValidation();
		$objValidation_mailbox_limit->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
		$objValidation_mailbox_limit->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
		$objValidation_mailbox_limit->setAllowBlank(false);
		$objValidation_mailbox_limit->setShowDropDown(true);
		$objValidation_mailbox_limit->setFormula1('"Use Group Default, 10,20,30,40,50,60,70,80,90,100,200,300,400,500,600,700,800,900"');

		//Set Dropdowns
		$objReader->getActiveSheet()->setDataValidation("C3:C2000", $objValidation_on_off);
		$objReader->getActiveSheet()->setDataValidation("D3:D2000", $objValidation_on_off);
		$objReader->getActiveSheet()->setDataValidation("E3:E2000", $objValidation_yes_no);
		$objReader->getActiveSheet()->setDataValidation("G3:G2000", $objValidation_yes_no);
		$objReader->getActiveSheet()->setDataValidation("J3:J2000", $objValidation_yes_no);
		$objReader->getActiveSheet()->setDataValidation("K3:K2000", $objValidation_yes_no);
		$objReader->getActiveSheet()->setDataValidation("M3:M2000", $objValidation_yes_no);
		$objReader->getActiveSheet()->setDataValidation("N3:N2000", $objValidation_yes_no);
		$objReader->getActiveSheet()->setDataValidation("O3:O2000", $objValidation_yes_no);
		$objReader->getActiveSheet()->setDataValidation("P3:P2000", $objValidation_yes_no);
		$objReader->getActiveSheet()->setDataValidation("R3:R2000", $objValidation_mailbox_limit);

		$sheetRow = 3;


		if ($_POST['filterUsers'] == 'All' || $_POST['filterUsers'] == 'FilterUsers') {

			try {

				ob_start();

				if ($_POST['filterUsers'] == 'FilterUsers') {
					$users = getFilteredUsers($users, $_POST['usersSelected']);
				}

				$download_status['d_progress'] = 40;
				file_put_contents("/tmp/$download_id", json_encode($download_status));
				setServerErrorInTempFile();
				
				if (isset($users)) {

					$user_array = array();

					foreach ($users as $user_row) {

						$userId = $user_row["userId"];

						if($user_row["voiceMessagingUser"] == "true") {

							$userName = $user_row["LastName"] . ", " . $user_row["firstName"];
							$phoneNumber = $user_row["phoneNumber"];
							$extension = $user_row["extension"];

							$phoneNumber = str_replace ("+1-" , "", $phoneNumber);

							$mailBoxLimit = "";
							if(isset($user_row['vm']["groupMailServerFullMailboxLimit"])) {
								if($user_row['vm']["groupMailServerFullMailboxLimit"] == "true") {
									$mailBoxLimit = "Use Group Default";
								} else {
									$mailBoxLimit = $user_row['vm']["groupMailServerFullMailboxLimit"];
								}
							}

							$usePhoneMessageWaitingIndicator = $user_row['vm']["usePhoneMessageWaitingIndicator"] == "true" ? "Yes" : "No";
							$sendCarbonCopyVoiceMessage = $user_row['vm']["sendCarbonCopyVoiceMessage"] == "true" ? "Yes" : "No";
							$voiceMessageCarbonCopyEmailAddress = $user_row['vm']["voiceMessageCarbonCopyEmailAddress"];
							$alwaysRedirectToVoiceMail = $user_row['vm']["alwaysRedirectToVoiceMail"] == "true" ? "Yes" : "No";
							$busyRedirectToVoiceMail = $user_row['vm']["busyRedirectToVoiceMail"] == "true" ? "Yes" : "No";
							$noAnswerRedirectToVoiceMail = $user_row['vm']["noAnswerRedirectToVoiceMail"] == "true" ? "Yes" : "No";
							$transferOnZeroToPhoneNumber = $user_row['vm']["transferOnZeroToPhoneNumber"] == "true" ? "Yes" : "No";
							$transferPhoneNumber = $user_row['vm']["transferPhoneNumber"];


							$objReader->getActiveSheet()->SetCellValue('B' . $sheetRow, $userName . " (" . $phoneNumber . "x" . $extension . ")");
							$objReader->getActiveSheet()->SetCellValue('C' . $sheetRow, "On");
							$objReader->getActiveSheet()->SetCellValue('D' . $sheetRow, $user_row['vm']["isActive"] == "true" ? "On" : "Off");
							$objReader->getActiveSheet()->SetCellValue('E' . $sheetRow, $user_row['vm']["processing"] == "Unified Voice and Email Messaging" ? "Yes" : "No");
							$objReader->getActiveSheet()->SetCellValue('F' . $sheetRow, $user_row['vm']["voiceMessageDeliveryEmailAddress"]);
							$objReader->getActiveSheet()->SetCellValue('G' . $sheetRow, $user_row['vm']["sendVoiceMessageNotifyEmail"] == "true" ? "Yes" : "No");
							$objReader->getActiveSheet()->SetCellValue('H' . $sheetRow, $user_row['vm']["voiceMessageNotifyEmailAddress"]);

							if($usePhoneMessageWaitingIndicator != $Voicemail_Use_Phone_MWI) {
								$objReader->getActiveSheet()->SetCellValue('J' . $sheetRow, $usePhoneMessageWaitingIndicator);
							}
							if($sendCarbonCopyVoiceMessage != $Voicemail_Email_Message_Carbon_Copy) {
								$objReader->getActiveSheet()->SetCellValue('K' . $sheetRow, $sendCarbonCopyVoiceMessage);
							}
							if($voiceMessageCarbonCopyEmailAddress != $Voicemail_Carbon_Copy_Address) {
								$objReader->getActiveSheet()->SetCellValue('L' . $sheetRow, $voiceMessageCarbonCopyEmailAddress);
							}
							if($alwaysRedirectToVoiceMail != $Voicemail_Send_All_Calls_to_VM) {
								$objReader->getActiveSheet()->SetCellValue('M' . $sheetRow, $alwaysRedirectToVoiceMail);
							}
							if($busyRedirectToVoiceMail != $Voicemail_Send_Busy_Calls_to_VM) {
								$objReader->getActiveSheet()->SetCellValue('N' . $sheetRow, $busyRedirectToVoiceMail);
							}
							if($noAnswerRedirectToVoiceMail != $Voicemail_Send_Unanswered_Calls_to_VM) {
								$objReader->getActiveSheet()->SetCellValue('O' . $sheetRow, $noAnswerRedirectToVoiceMail);
							}
							if($transferOnZeroToPhoneNumber != $Voicemail_Transfer_on_0) {
								$objReader->getActiveSheet()->SetCellValue('P' . $sheetRow, $transferOnZeroToPhoneNumber);
							}
							if($transferPhoneNumber != $Voicemail_Transfer_on_0_DN) {
								$objReader->getActiveSheet()->SetCellValue('Q' . $sheetRow, $transferPhoneNumber);
							}
							if($mailBoxLimit != $Voicemail_Mailbox_Limit) {
								$objReader->getActiveSheet()->SetCellValue('R' . $sheetRow, $mailBoxLimit);
							}


							$objReader->getActiveSheet()->getCell('A' . $sheetRow)->setDataValidation($objValidation_action);

							$sheetRow++;

						}

					}

					$download_status['d_progress'] = 60;
					file_put_contents("/tmp/$download_id", json_encode($download_status));
					setServerErrorInTempFile();
					
				}
				ob_get_clean();

			} catch (Exception $exception) {
				error_log($exception->getMessage());
			}


		}

		$objReader->getActiveSheet()->setDataValidation("B$sheetRow:B2000", $objValidation);
		$objReader->getActiveSheet()->setDataValidation("A$sheetRow:A2000", $objValidation_all_actions);

		$download_status['d_progress'] = 80;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		setServerErrorInTempFile();
		
		$objWriter = PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objReader, 'Xlsx');
		$tmp_file_name = tempnam(sys_get_temp_dir(), "ExpressSheet_User_$revision\_");
		$objWriter->save($tmp_file_name);

		ob_clean();
		$file_name = "$expressRevision-Voicemail-ES-$revision-" . time() . ".xlsx";

		$download_status['d_progress'] = 100;
		$download_status['fileName'] = $file_name;
		$download_status['filePath'] = $tmp_file_name;
		file_put_contents("/tmp/$download_id", json_encode($download_status));
		exit;

	}

}

function scallLoginLogoExp($width, $height, $filename) {
    list($width_orig, $height_orig) = getimagesize($filename);
    if($height_orig > 130) {
        $scallFactor = $height_orig/$height;
        $newWidth = $width_orig / $scallFactor;
        $newHeight = $height;
    } else {
        $newHeight = $height_orig;
        $newWidth = $width_orig;
    }
    return array('imgWidth' => $newWidth, 'imgHeight' => $newHeight);
}

function insert_logo($objPHPSpreadsheet, $column, $row, $filePath) {

	if (is_file($filePath)) {
		$info = pathinfo($filePath);

		$extension = strtolower($info['extension']);
		if (in_array($extension, array('jpg', 'jpeg', 'png', 'gif'))) {

			switch ($extension) {
				case 'png':
					$gdImage = imagecreatefrompng($filePath);
					break;
				case 'gif':
					$gdImage = imagecreatefromgif($filePath);
					break;
				case 'jpg':
				case 'jpeg':
				default:
					$gdImage = imagecreatefromjpeg($filePath);
			}
			if($gdImage) {

				$cell = "$column$row";
                                $imageDetail = scallLoginLogoExp($width = 300, $height = 130, $filePath);
                		$imgWidth = $imageDetail['imgWidth'];
                		$imgHeight = $imageDetail['imgHeight'];
                                
				$objDrawing = new PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing();
				$objDrawing->setImageResource($gdImage);
				$objDrawing->setRenderingFunction(PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing::RENDERING_JPEG);
				$objDrawing->setMimeType(PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing::MIMETYPE_DEFAULT);
				//$objDrawing->setHeight(150);
                                $objDrawing->setCoordinates($cell);
                                $objDrawing->setWidth($imgWidth); 
                                $objDrawing->setHeight($imgHeight); 
				$objDrawing->setWorksheet($objPHPSpreadsheet->getActiveSheet());
				$objPHPSpreadsheet->getActiveSheet()->getStyle($cell)->getAlignment()->setVertical(PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM);

				$image_height = imageSY($gdImage);
                                
				$image_height_pt = PhpOffice\PhpSpreadsheet\Shared\Drawing::pixelsToPoints($imgHeight);
				$objPHPSpreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight($image_height_pt + 10);
                                //$objPHPSpreadsheet->getActiveSheet()->getRowDimension($row)->setRowHeight($imgHeight);
                                //$objPHPSpreadsheet->getActiveSheet()->getColumnDimension($column)->setWidth($imgWidth);

			}
		}
	}
}

function getFilteredUsers($users, $filtersUserIDs) {
	foreach($users as $elementKey => $user) {
		if(!in_array($user['userId'], $filtersUserIDs)) {
			unset($users[$elementKey]);
		}
	}
	return $users;
}

function shutDownFunction() {
	global $download_id;

	$error = error_get_last();
	error_log(print_r($error, true));
	// fatal error, E_ERROR === 1
	if ($error['type'] === E_ERROR) {
		if($download_id) {
			if(file_exists("/tmp/" . $download_id)) {
				$download_status = json_decode(file_get_contents("/tmp/" . $download_id), true);
				$download_status['failed'] = 1;
				file_put_contents("/tmp/$download_id", json_encode($download_status));
			}

		}
	}
}

function getUserDetails($userId){
	global $sessionid, $client;
	$returnResponse = array();
	$xmlinput = xmlHeader($sessionid, "UserGetRequest19");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	$returnResponse["extension"] = ($xml->command->extension);
	$returnResponse["phoneNumber"] = ($xml->command->phoneNumber);
	return $returnResponse;
}

/* Server Check Connection */
function setServerErrorInTempFile() {
    global $download_id;
    
    require_once("/var/www/lib/broadsoft/ServerConnectionUtil.php");
    $serConnUtil = new ServerConnectionUtil();
    //$GLOBALS['serversDetail'] = $serConnUtil->getServerDetails();
    
    $serviceRes = $serConnUtil->checkServerConnectionStatus();
    if( $serviceRes["statusMessage"] != "" ) {
        $download_status = array();
        if(file_exists("/tmp/" . $download_id)) {
            $download_status['failed'] = 1;
            $download_status['errorMsg'] = $serviceRes["statusMessage"];
            file_put_contents("/tmp/$download_id", json_encode($download_status));
        }
        die;
    }
}

?>
