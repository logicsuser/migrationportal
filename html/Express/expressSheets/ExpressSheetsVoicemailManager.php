<?php

/**
 * Created by Dipin Krishna.
 * Date: 10/18/2017
 */
class ExpressSheetsVoicemaillManager
{
    private $keys = array();
    private $headTitles = array();
    private $numColumns = 0;
    private $postName = "";
    private $backEndErrorMsg = "";
    private $oldValues = array();

	private static function userId()                        { return "userId"; }
	private static function action()                        { return "action"; }
	private static function user()                          { return "user"; }
	private static function vm_service()                    { return "vm_service"; }
	private static function enable_vm()                     { return "enable_vm"; }
	private static function send_all_calls_to_vm()          { return "send_all_calls_to_vm"; }
	private static function send_busy_calls_to_vm()         { return "send_busy_calls_to_vm"; }
	private static function send_unanswered_calls_to_vm()   { return "send_unanswered_calls_to_vm"; }
	private static function use_unified_messaging()         { return "use_unified_messaging"; }
	private static function use_phone_MWI()                 { return "use_phone_MWI"; }
	private static function forward_to_email()              { return "forward_to_email"; }
	private static function new_message_notification()      { return "new_message_notification"; }
	private static function new_message_email_address()     { return "new_message_email_address"; }
	private static function email_message_carbon_copy()     { return "email_message_carbon_copy"; }
	private static function carbon_copy_address()           { return "carbon_copy_address"; }
	private static function transfer_on_0()                 { return "transfer_on_0"; }
	private static function transfer_on_0_DN()              { return "transfer_on_0_DN"; }
	private static function mailbox_limit()                 { return "mailbox_limit"; }

    public function __construct($postName) {

        $this->postName = $postName;

	    $this->addAttribute(self::action(),                       "Action");
	    $this->addAttribute(self::user(),                         "User");
	    $this->addAttribute(self::vm_service(),                   "VM Service");
	    $this->addAttribute(self::enable_vm(),                    "Enable VM");
	    $this->addAttribute(self::use_unified_messaging(),        "Use Unified Messaging");
	    $this->addAttribute(self::forward_to_email(),             "Forward to email");
	    $this->addAttribute(self::new_message_notification(),     "New Message Notification");
	    $this->addAttribute(self::new_message_email_address(),    "New Message Email Address");
	    $this->addAttribute(self::use_phone_MWI(),                "Use Phone MWI");
	    $this->addAttribute(self::email_message_carbon_copy(),    "Email Message Carbon Copy");
	    $this->addAttribute(self::carbon_copy_address(),          "Carbon Copy Address");
	    $this->addAttribute(self::send_all_calls_to_vm(),         "Send All Calls to VM");
	    $this->addAttribute(self::send_busy_calls_to_vm(),        "Send Busy Calls to VM");
	    $this->addAttribute(self::send_unanswered_calls_to_vm(),  "Send Unanswered Calls to VM");
	    $this->addAttribute(self::transfer_on_0(),                "Transfer on '0'");
	    $this->addAttribute(self::transfer_on_0_DN(),             "Transfer on '0' DN");
	    $this->addAttribute(self::mailbox_limit(),                "Mailbox Limit");

    }

	public function createTableHeader()
	{
		$header = "";
		$margin = $this->numColumns == 0 ? 100 : (int)(100 / $this->numColumns);

		for ($i = 0; $i < $this->numColumns; $i++) {
			$header .= "<th style=\"width:" . $margin . "%\">" . $this->headTitles[$i] . "</th>";
		}

		return $header;
	}


	public function processBulk()
	{
		global $sessionid, $client, $db, $ociVersion;

		$row = "";
		foreach ($_POST[$this->postName] as $key => $value) {
			$row .= "<tr style=\"font-size: 12px\">";
			$this->executeAction($value);

			for ($i = 0; $i < $this->numColumns; $i++) {
				$row .= "<td>" . $value[$this->keys[$i]] . "</td>";
			}

			$row .= "</tr>";

			if ($this->backEndErrorMsg != "") {
				echo $this->backEndErrorMsg;
				break;
			}
		}

		require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/expressSheetFunctions.php");

		foreach ($this->oldValues as $userId => $oldValue) {

			unset($userInfo);
			require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getUserInfo.php");

			$changeLogs = array();
			foreach ($oldValue as $key => $value) {
				if ($_SESSION["userInfo"][$key] != $value) {
					$changeLogs[] = array(
							"field" => "$key",
							"old" => $value,
							"new" => $_SESSION["userInfo"][$key],
						);
				}
			}

			$changes = array(
				'logs' => $changeLogs,
				'userId' => $userId,
				'entityName' => $_SESSION["userInfo"]["lastName"] . ", " . $_SESSION["userInfo"]["firstName"],
				"action" => "Modify",
			);
			
			$fields = array(
				"voiceMessagingUser" => "VM Service",
				"isActive" => "Enable VM",
				"alwaysRedirectToVoiceMail" => "Send All Calls to VM",
				"busyRedirectToVoiceMail" => "Send Busy Calls to VM",
				"noAnswerRedirectToVoiceMail" => "Send Unanswered Calls to VM",
				"processing" => "Use Unified Messaging",
				"voiceMessageDeliveryEmailAddress" => "Forward to email",
				"usePhoneMessageWaitingIndicator" => "Use Phone MWI",
				"sendVoiceMessageNotifyEmail" => "New Message Notification",
				"voiceMessageNotifyEmailAddress" => "New Message Email Address",
				"sendCarbonCopyVoiceMessage" => "Email Message Carbon Copy",
				"voiceMessageCarbonCopyEmailAddress" => "Carbon Copy Address",
				"transferOnZeroToPhoneNumber" => "Transfer on '0'",
				"transferPhoneNumber" => "Transfer on '0' DN",
				"mailBoxLimit" => "Mailbox Limit"
			);
			insertUserChangeLogs($changes, $fields);

		}

		return $row;
	}


	public function getVMEnabledServicePack($servicePacks, $userServices) {
	    global $sessionid, $client;
	    
	    require_once("/var/www/lib/broadsoft/adminPortal/services/Services.php");
	    $response = new stdClass;
	    $serviceObj = new Services();
	    /* Get VM service pack with VM user service only */
	    foreach ($servicePacks as $servicePack) {
	        $services = $serviceObj->getServicesListOfServicePackBasedOnEnterprise($servicePack, $_SESSION['sp']);
	        if (in_array("Voice Messaging User", $services)) {
	            if (count($services) == 1) {
	                $response->VMOnlyServicePack = $servicePack;
	                break;
	            }
	        }
	    }
	    
	    /* If VM only service pack is not available then check VM user service */
	    if( !(isset($response->VMOnlyServicePack)) ) {
	        if (in_array("Voice Messaging User", $userServices)) {
	            $response->VMService = "Voice Messaging User";
	        }
	    }
	    
	    return $response;
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	private function executeAction($row_value)
	{
		global $sessionid, $client, $db, $ociVersion;

		$action = $row_value[self::action()];
		$userId = $row_value[self::userId()];
		$user = $row_value[self::user()];
		$vm_service = self::normalize(self::vm_service(), $row_value[self::vm_service()]);
		$isActive = self::normalize(self::enable_vm(), $row_value[self::enable_vm()]);
		$alwaysRedirectToVoiceMail = self::normalize(self::send_all_calls_to_vm(), $row_value[self::send_all_calls_to_vm()]);
		$busyRedirectToVoiceMail = self::normalize(self::send_busy_calls_to_vm(), $row_value[self::send_busy_calls_to_vm()]);
		$noAnswerRedirectToVoiceMail = self::normalize(self::send_unanswered_calls_to_vm(), $row_value[self::send_unanswered_calls_to_vm()]);
		$processing = self::normalize(self::use_unified_messaging(), $row_value[self::use_unified_messaging()]);
		$voiceMessageDeliveryEmailAddress = self::normalize(self::forward_to_email(), $row_value[self::forward_to_email()]);
		$usePhoneMessageWaitingIndicator = self::normalize(self::use_phone_MWI(), $row_value[self::use_phone_MWI()]);
		$sendVoiceMessageNotifyEmail = self::normalize(self::new_message_notification(), $row_value[self::new_message_notification()]);
		$voiceMessageNotifyEmailAddress = self::normalize(self::new_message_email_address(), $row_value[self::new_message_email_address()]);
		$sendCarbonCopyVoiceMessage = self::normalize(self::email_message_carbon_copy(), $row_value[self::email_message_carbon_copy()]);
		$voiceMessageCarbonCopyEmailAddress = self::normalize(self::carbon_copy_address(), $row_value[self::carbon_copy_address()]);
		$transferOnZeroToPhoneNumber = self::normalize(self::transfer_on_0(), $row_value[self::transfer_on_0()]);
		$transferPhoneNumber = self::normalize(self::transfer_on_0_DN(), $row_value[self::transfer_on_0_DN()]);
		$mailboxLimit = self::normalize(self::mailbox_limit(), $row_value[self::mailbox_limit()]);

		$processing = $processing == "true" ? "Unified Voice and Email Messaging" : "Deliver To Email Address Only";

		//error_log("vm_service: $vm_service");

		unset($userInfo);
		require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getUserInfo.php");

		$this->oldValues[$userId] = array(
				"voiceMessagingUser" => $_SESSION["userInfo"]["voiceMessagingUser"],
				"isActive" => $_SESSION["userInfo"]["isActive"],
				"alwaysRedirectToVoiceMail" => $_SESSION["userInfo"]["alwaysRedirectToVoiceMail"],
				"busyRedirectToVoiceMail" => $_SESSION["userInfo"]["busyRedirectToVoiceMail"],
				"noAnswerRedirectToVoiceMail" => $_SESSION["userInfo"]["noAnswerRedirectToVoiceMail"],
				"processing" => $_SESSION["userInfo"]["processing"],
				"voiceMessageDeliveryEmailAddress" => $_SESSION["userInfo"]["voiceMessageDeliveryEmailAddress"],
				"usePhoneMessageWaitingIndicator" => $_SESSION["userInfo"]["usePhoneMessageWaitingIndicator"],
				"sendVoiceMessageNotifyEmail" => $_SESSION["userInfo"]["sendVoiceMessageNotifyEmail"],
				"voiceMessageNotifyEmailAddress" => $_SESSION["userInfo"]["voiceMessageNotifyEmailAddress"],
				"sendCarbonCopyVoiceMessage" => $_SESSION["userInfo"]["sendCarbonCopyVoiceMessage"],
				"voiceMessageCarbonCopyEmailAddress" => $_SESSION["userInfo"]["voiceMessageCarbonCopyEmailAddress"],
				"transferOnZeroToPhoneNumber" => $_SESSION["userInfo"]["transferOnZeroToPhoneNumber"],
				"transferPhoneNumber" => $_SESSION["userInfo"]["transferPhoneNumber"],
				"mailBoxLimit" => $_SESSION["userInfo"]["mailBoxLimit"]
			);

		//error_log("voiceMessagingUser: " . $_SESSION["userInfo"]["voiceMessagingUser"]);

		$assignedServices = new UserServiceGetAssignmentList($userId);

		if($vm_service == "false" && $_SESSION["userInfo"]["voiceMessagingUser"] == "true") {

			//error_log("Remove VM");
			$remove_service_packs = array();

			if ($assignedServices->hasService("Voice Messaging User")) {
				$servicePacks = $assignedServices->getservicePacks();
				foreach ($servicePacks as $servicePack => $userServices) {
					if(in_array("Voice Messaging User", $userServices)) {
						if(count($userServices) > 1) {
						    $VMWithMultipleServices = true;
						    $isActive = "false";
							//$this->backEndErrorMsg = "$user: VM User Service cannot be un-assigned because it is assigned through '$servicePack' Service Pack, containing multiple services.";
							//return;
						} else {
						    $remove_service_packs[] = $servicePack;
						}
					}
				}

				$servicePackRemoveReq = xmlHeader($sessionid, "UserServiceUnassignListRequest");
				$servicePackRemoveReq .= "<userId>" . $userId . "</userId>";
				$servicePackRemoveReq .= "<serviceName>" . htmlspecialchars("Voice Messaging User") . "</serviceName>";
				foreach ($remove_service_packs as $servicePack) {
					$servicePackRemoveReq .= "<servicePackName>" . htmlspecialchars($servicePack) . "</servicePackName>";
				}
				$servicePackRemoveReq .= xmlFooter();

				//error_log($servicePackRemoveReq);

				$response = $client->processOCIMessage(array("in0" => $servicePackRemoveReq));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

				if(readError($xml)) {
					$this->backEndErrorMsg = "OCI Error";
					return;
				}
			}

		}

		if($vm_service == "true" && $_SESSION["userInfo"]["voiceMessagingUser"] != "true") {
			//error_log("Add VM");
			$addServicePacks = array();
			$addUserServices = array();
			
			$servicePacks = $assignedServices->getservicePacks();
			foreach ($servicePacks as $servicePack => $userServices) {
				if(in_array("Voice Messaging User", $userServices) && count($userServices) == 1) {
					$remove_service_packs[] = $servicePack;
					break;
				}
			}
			
			require_once("/var/www/lib/broadsoft/adminPortal/services/Services.php");
			if( !$assignedServices->hasService("Voice Messaging User") ) {
    			$serviceObj = new Services();
    			$groupServiceInfo = $serviceObj->getGroupServicePackAndUserServices($_SESSION['sp'], $_SESSION['groupId']);
    			if( empty($groupServiceInfo["Error"])) {
    			        $responseVM = $this->getVMEnabledServicePack($groupServiceInfo["Success"]["servicePacks"], $groupServiceInfo["Success"]["userServices"]);
    			        if( isset($responseVM->VMOnlyServicePack) ) {
    			            $addServicePacks[] = $responseVM->VMOnlyServicePack;
    			        }
    			        if( isset($responseVM->VMService) ) {
    			            $addUserServices[] = $responseVM->VMService;
    			        }
    			}
		  }
			
			$servicePackAddReq = xmlHeader($sessionid, "UserServiceAssignListRequest");
			$servicePackAddReq .= "<userId>" . $userId . "</userId>";
			
			if ( count($addUserServices) > 0 ) {
			    foreach($addUserServices as $user_service) {
			        $servicePackAddReq .= "<serviceName>" . htmlspecialchars($user_service) . "</serviceName>";
			    }
			}
			
			if ( count($addServicePacks) > 0 ) {
			    foreach( $addServicePacks as $service_pack) {
			        $servicePackAddReq .= "<servicePackName>" . htmlspecialchars($service_pack) . "</servicePackName>";
			    }
			}
			$servicePackAddReq .= xmlFooter();

			$response = $client->processOCIMessage(array("in0" => $servicePackAddReq));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if(readError($xml)) {
				$this->backEndErrorMsg = "OCI Error";
				return;
			}

		}

		/* If Assign VM is set to No and Sevice pack has VM and also other services. */
		if ( isset($VMWithMultipleServices) && $VMWithMultipleServices == true )
		{
    	      $voiceManagementIsActive = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
    	      $voiceManagementIsActive .= "<userId>" . $userId . "</userId>";
    	      $voiceManagementIsActive .= "<isActive>" . $isActive . "</isActive>";
    	      $voiceManagementIsActive .= xmlFooter();
    	      
    	      $response = $client->processOCIMessage(array("in0" => $voiceManagementIsActive));
    	      $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    	      $errorResult = readError($xml);
    	      if ($errorResult !== "Error")
    	      {
    	        
    	      } else {
    	          $this->backEndErrorMsg = "OCI Error";
    	          return;
    	      }
		}
		
		if ($vm_service == "true")
		{

			//Mailbox Limit - Start
			if($mailboxLimit) {
				require_once("/var/www/lib/broadsoft/adminPortal/users/VMUsersServices.php");
				$vmAdvancedObj = new VMUsersServices();
				$vmAdvancedResp = $vmAdvancedObj->modifyUsersVoiceMessagingAdvancedServices($userId, array('mailBoxLimit' => $mailboxLimit));
				if ($vmAdvancedResp["Error"] == "") {
					/*
							Log Changes
					*/
				} else {
					$this->backEndErrorMsg = "OCI Error";
					return;
				}
			}
			//Mailbox Limit - End

			if (isset($_SESSION["userInfo"]["thirdPartyVoiceMail"]) and $_SESSION["userInfo"]["thirdPartyVoiceMail"] == "true")
			{
				$voiceManagement = xmlHeader($sessionid, "UserThirdPartyVoiceMailSupportModifyRequest");
			}
			else
			{
				$voiceManagement = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
			}
			$voiceManagement .= "<userId>" . $userId . "</userId>";
			$voiceManagement .= "<isActive>" . $isActive . "</isActive>";

			if (isset($_SESSION["userInfo"]["thirdPartyVoiceMail"]) and $_SESSION["userInfo"]["thirdPartyVoiceMail"] == "true")
			{
				if ($busyRedirectToVoiceMail)
				{
					$voiceManagement .= "<busyRedirectToVoiceMail>" . $busyRedirectToVoiceMail . "</busyRedirectToVoiceMail>";
				}
				if ($noAnswerRedirectToVoiceMail)
				{
					$voiceManagement .= "<noAnswerRedirectToVoiceMail>" . $noAnswerRedirectToVoiceMail . "</noAnswerRedirectToVoiceMail>";
				}
				if ($_SESSION["userInfo"]["serverSelection"])
				{
					$voiceManagement .= "<serverSelection>" . $_SESSION["userInfo"]["serverSelection"] . "</serverSelection>";
				}
				if ($_SESSION["userInfo"]["userServer"])
				{
					$voiceManagement .= "<userServer>" . $_SESSION["userInfo"]["userServer"] . "</userServer>";
				}
				else
				{
					$voiceManagement .= "<userServer xsi:nil=\"true\" />";
				}
				if ($_SESSION["userInfo"]["mailboxIdType"])
				{
					$voiceManagement .= "<mailboxIdType>" . $_SESSION["userInfo"]["mailboxIdType"] . "</mailboxIdType>";
				}
				if ($_SESSION["userInfo"]["mailboxURL"])
				{
					$voiceManagement .= "<mailboxURL>" . $_SESSION["userInfo"]["mailboxURL"] . "</mailboxURL>";
				}
				else
				{
					$voiceManagement .= "<mailboxURL xsi:nil=\"true\" />";
				}
				$voiceManagement .= "<noAnswerNumberOfRings>" . $_SESSION["userInfo"]["noAnswerNumberOfRings"] . "</noAnswerNumberOfRings>";
				if ($alwaysRedirectToVoiceMail)
				{
					$voiceManagement .= "<alwaysRedirectToVoiceMail>" . $alwaysRedirectToVoiceMail . "</alwaysRedirectToVoiceMail>";
				}
			}
			else
			{
				if (isset($processing))
				{
					$voiceManagement .= "<processing>" . $processing . "</processing>";
				}
				if ($voiceMessageDeliveryEmailAddress)
				{
					$voiceManagement .= "<voiceMessageDeliveryEmailAddress>" . $voiceMessageDeliveryEmailAddress . "</voiceMessageDeliveryEmailAddress>";
				}
				else
				{
					$voiceManagement .= "<voiceMessageDeliveryEmailAddress xsi:nil=\"true\" />";
				}
				if ($usePhoneMessageWaitingIndicator)
				{
					$voiceManagement .= "<usePhoneMessageWaitingIndicator>" . $usePhoneMessageWaitingIndicator . "</usePhoneMessageWaitingIndicator>";
				}
				if ($sendVoiceMessageNotifyEmail)
				{
					$voiceManagement .= "<sendVoiceMessageNotifyEmail>" . $sendVoiceMessageNotifyEmail . "</sendVoiceMessageNotifyEmail>";
				}
				if ($voiceMessageNotifyEmailAddress)
				{
					$voiceManagement .= "<voiceMessageNotifyEmailAddress>" . $voiceMessageNotifyEmailAddress . "</voiceMessageNotifyEmailAddress>";
				}
				else
				{
					$voiceManagement .= "<voiceMessageNotifyEmailAddress xsi:nil=\"true\" />";
				}
				if ($sendCarbonCopyVoiceMessage)
				{
					$voiceManagement .= "<sendCarbonCopyVoiceMessage>" . $sendCarbonCopyVoiceMessage . "</sendCarbonCopyVoiceMessage>";
				}
				if ($voiceMessageCarbonCopyEmailAddress)
				{
					$voiceManagement .= "<voiceMessageCarbonCopyEmailAddress>" . $voiceMessageCarbonCopyEmailAddress . "</voiceMessageCarbonCopyEmailAddress>";
				}
				else
				{
					$voiceManagement .= "<voiceMessageCarbonCopyEmailAddress xsi:nil=\"true\" />";
				}
				if ($transferOnZeroToPhoneNumber)
				{
					$voiceManagement .= "<transferOnZeroToPhoneNumber>" . $transferOnZeroToPhoneNumber . "</transferOnZeroToPhoneNumber>";
				}
				if ($transferPhoneNumber)
				{
					$voiceManagement .= "<transferPhoneNumber>" . $transferPhoneNumber . "</transferPhoneNumber>";
				}
				else
				{
					$voiceManagement .= "<transferPhoneNumber xsi:nil=\"true\" />";
				}
				if ($alwaysRedirectToVoiceMail)
				{
					$voiceManagement .= "<alwaysRedirectToVoiceMail>" . $alwaysRedirectToVoiceMail . "</alwaysRedirectToVoiceMail>";
				}
				if ($busyRedirectToVoiceMail)
				{
					$voiceManagement .= "<busyRedirectToVoiceMail>" . $busyRedirectToVoiceMail . "</busyRedirectToVoiceMail>";
				}
				if ($noAnswerRedirectToVoiceMail)
				{
					$voiceManagement .= "<noAnswerRedirectToVoiceMail>" . $noAnswerRedirectToVoiceMail . "</noAnswerRedirectToVoiceMail>";
				}
			}
			$voiceManagement .= xmlFooter();

			//error_log($voiceManagement);

			$response = $client->processOCIMessage(array("in0" => $voiceManagement));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			$errorResult = readError($xml);

			if ($errorResult !== "Error")
			{
				/*
				foreach ($changes["voiceManagement"] as $key => $value)
				{
					if ($value == "")
					{
						$value = "None";
					}
					$message .= "<li>" . $_SESSION["userModNames"][$key] . " has been changed to " . $value . ".</li>";
					$changeLog[$a]["field"] = $key;
					$changeLog[$a]["old"] = $_SESSION["userInfo"][$key] !== "" ? $_SESSION["userInfo"][$key] : "None";
					$changeLog[$a]["new"] = $value;
					$a++;
				}
				*/
			} else {
				$this->backEndErrorMsg = "OCI Error";
				return;
			}
		}

	}

	//-----------------------------------------------------------------------------------------------------------------
	private function addAttribute($key, $headTitle)
	{
		$this->keys[] = $key;
		$this->headTitles[] = $headTitle;

		$this->numColumns++;
	}


	//-----------------------------------------------------------------------------------------------------------------
	private static function normalize($key, $value)
	{
		switch ($key) {
			case self::vm_service():
			case self::enable_vm():
			case self::send_all_calls_to_vm():
			case self::send_busy_calls_to_vm():
			case self::send_unanswered_calls_to_vm():
			case self::use_unified_messaging():
			case self::use_phone_MWI():
			case self::new_message_notification():
			case self::email_message_carbon_copy():
			case self::transfer_on_0():
				$value = (strtoupper($value) == 'ON' || strtoupper($value) == "YES") ? "true" : "false";
				break;
			case self::mailbox_limit():
				$value = strtoupper($value) == 'USE GROUP DEFAULT' ? "Use Group Default" : $value;
				break;
			default:
				break;
		}

		return $value;
	}
}
