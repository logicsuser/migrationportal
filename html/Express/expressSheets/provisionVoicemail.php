<?php
/**
 * Created by Dipin Krishna.
 * Date: 01/01/18
 * Time: 7:50 AM
 */
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

if ($_SESSION["superUser"] != "1" && $_SESSION["permissions"]["expressSheets"] != "1")
{
	echo "<div style='color: red;text-align: center;'>You don't have permission to access Express Sheets.</div>";
	exit;
}

/*
if($_SESSION["superUser"] != "1" && $_SESSION["permissions"]["voicemail"] != "1") {
	echo "<div style='color: red;text-align: center;'>You don't have permission to provision Voicemail.</div>";
	exit;
}
*/

//-------------------------------------------------------------------
function submitOrReSubmit($error)
{
	if ($error) {
		return "<label class=\"labelText\">Please correct errors and resubmit.</label>";
	}

	return "<input class=\"submitBulkClass\" style=\"margin-left: 4px; margin-bottom: 4px\" type=\"button\" name=\"subBulk\" id=\"subBulk\" value=\"Provision Voicemail attributes\">";
}

server_fail_over_debuggin_testing(); /* for fail Over testing. */

include("/var/www/lib/broadsoft/adminPortal/expressSheets/ExpressSheetsExcelManager.php");
include("/var/www/lib/broadsoft/adminPortal/expressSheets/ExpressSheetsVoicemailExcelManager.php");
require_once("/var/www/lib/broadsoft/adminPortal/UserServiceGetAssignmentList.php");

$extraUserInfo = 1;
require_once("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");

$voicemailManager = new ExpressSheetsVoicemailExcelManager("excelData", true);
$header_error = $voicemailManager->validateHeader();

if($header_error) {
	?>
	<div class="alert alert-danger">
		<h2>Column Name Mismatch</h2>
		<?php echo $header_error ?>
	</div>
	<?php
	exit;
}

?>

<script>
    $(function () {
        $("#parseDiv").tooltip();

        var dataString = $("form#parseForm").serialize();

        var buttonsShowHide = function(b,e){
    		if(e == "show"){
    			$(".ui-dialog-buttonpane button:contains("+b+")").button().show();
    		}else{
    		 	$(".ui-dialog-buttonpane button:contains("+b+")").button().hide();
    		}
    	};
    	
      //download excel
        var downloadExpressCsv = function(){
    		var myTableArray = {};
    		$("table#vmTableOutput tr:not('[class*=excelRow]')").each(function() {
    			var thArrayOfThisRow = [];
    		    var tableThData = $(this).find('th');
    		    if (tableThData.length > 0) {
    		    	tableThData.each(function() { 
    		    		var thData = $(this); 
    			        thArrayOfThisRow.push(thData.text()); 
    			    });
    		        myTableArray["columnArray"] = thArrayOfThisRow;
    		        $("#columnArray").val(myTableArray["columnArray"]);
    		        console.log(myTableArray["columnArray"]);
    		    }
    		});
    		
            var myTableValueArray = [];
            var myTableErrorArray = [];
            var i = 0;
    		
    		$("#parseForm table#vmTableOutput tr.errorBad").each(function(i) {
    			var tdValueArray = [];
    			var tdErrorArray = [];
    			var s=0;
    			var tableTdData = $(this).find('td');
    			tableTdData.each(function(s) { 
    			tdValueArray[s] = $(this).find("input").val();
    			tdErrorArray[s] = $(this).find("input").attr("title");
    			s++;
    			});
    			myTableValueArray[i] = tdValueArray;
    			myTableErrorArray[i] = tdErrorArray;
    			i++;
    		});
    		var jsonV = JSON.stringify(myTableValueArray);
    		var jsonE = JSON.stringify(myTableErrorArray);
    		$("#valueArray").val(jsonV);
    		$("#errorArray").val(jsonE);
    		$("#submitErrorForm").submit();

    	}
    	
        //dialogue for voice mail
        $("#showVMDialogueValidate").dialog({
    		autoOpen: false,
    		width: 800,
    		modal: true,
    		position: { my: "top", at: "top" },
    		resizable: false,
    		closeOnEscape: false,
    		
    		buttons: {
    			"Download erroneous records as XLSX": function() {
    				downloadExpressCsv();
    			},
    			"Show All Records": function(){
    				//$("#bulkDiv").show();
    				//$(this).close();
    				tableHtml = $("#parseForm").html();
    				$("#showVMDialogueValidate").html(tableHtml);
    				$("#showVMDialogueValidate excelRow").hide();
    				$("#showVMDialogueValidate errorBad").show();
    				buttonsShowHide('Show All Records', 'hide');
    				buttonsShowHide('Provision', 'hide');
    				$(document).find("#showVMDialogueValidate").find("#vmTableOutput").find("#subBulk").removeClass("submitBulkClass");
    				var errorBadLength = $("#parseForm .errorBad").length;
    				if(errorBadLength > 0){
    					buttonsShowHide('Show Records With Errors Only', 'show');
    				}else{
    					buttonsShowHide('Show Records With Errors Only', 'hide');
    				}
    			},
    			"Show Records With Errors Only": function(){
    				 if ($(".errorBad")[0]){ 
    					tableHtml = $("#parseForm").html();
    					$("#showVMDialogueValidate").html(tableHtml);
    					$("#showVMDialogueValidate .excelRow").hide();
    					$("#showVMDialogueValidate .errorBad").show();
    					buttonsShowHide('Show Records With Errors Only', 'hide');
    					buttonsShowHide('Show All Records', 'show');
    					$(document).find("#showVMDialogueValidate").find("#vmTableOutput").find("#subBulk").removeClass("submitBulkClass");
    				}
    			},
    			"Provision": function(){debugger;
    				$(document).find("#showVMDialogueValidate").find("#vmTableOutput").find("#subBulk").removeClass("submitBulkClass");
    				$(this).dialog("close"); 
    				$(".submitBulkClass").trigger("click");
    			}, 
                        "Cancel": function() {
    				$(this).dialog("close");
    				//$("#loading2").hide();
    			}
    		},
            open: function() {
				setDialogDayNightMode($(this));
				$(":button:contains('Show All Records')").addClass("cancelButton").show();
				$(":button:contains('Show Records With Errors Only')").addClass("cancelButton").show();
				$(":button:contains('Download erroneous records as XLSX')").addClass("cancelButton").show();
				$(":button:contains('Provision')").addClass("cancelButton").show();
				$(":button:contains('Cancel')").addClass("cancelButton addNewLineCancelBtn").show();
                                 
            	buttonsShowHide('Show All Records', 'show');
            	buttonsShowHide('Show Records With Errors Only', 'hide');
            	buttonsShowHide('Download erroneous records as XLSX', 'hide');
            	$(this).closest(".ui-dialog").find(".ui-dialog-titlebar-close").addClass("ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only").html("<span class='ui-button-icon-primary ui-icon ui-icon-closethick'></span>");
            	
            }
    	});

        $(document).on("click", "#showVMDialogueValidate #subBulk", function(){
    		$("#showVMDialogueValidate").dialog("close");
    		$("#parseForm").find("#subBulk").trigger("click");
    	});

        $(document).on("click","#showVMDialogueValidate #vmTableOutput #submitProvision #subBulk",function(e){
    		// do stuff
    		//$("#submitProvision input").trigger("click");
    		$(".submitBulkClass").trigger("click");
    	});


var loadVMDialogueValidate = function(){
    		
    		$("#parseDiv").hide();
    		var excelRowLength = $("#parseForm .excelRow").length;
    		$("#showVMDialogueValidate").html('');
    		var badLength;
    		var goodLength;
    		
    		$(".excelRow").each(function () {
    			var el = $(this);
    			
    			badLength = el.find("td.bad").length;
    			if (badLength > 0){
    				el.addClass('errorBad');
    			}else{
    				el.addClass('errorGood');
    			}
    		});

    		var errorBadLength = $("#parseForm .errorBad").length;
    		var errorGoodLength = $("#parseForm .errorGood").length;
    		
    		$("#showVMDialogueValidate").dialog("option", "title", "Request Complete");
    		$("#showVMDialogueValidate").dialog("open");
    		
    		if(errorBadLength > 0){
    			$("#showVMDialogueValidate").html('Express Sheets has attempted to provision '+ excelRowLength +' records, but detected '+ errorBadLength +' records with erroneous data. All errors must be corrected in the spreadsheet and spreadsheet resubmitted.');
            	buttonsShowHide('Show Records With Errors Only', 'show');
            	buttonsShowHide('Download erroneous records as XLSX', 'show');
            	buttonsShowHide('Provision', 'hide');
    		}else{
    			$("#showVMDialogueValidate").html('Express Sheets has successfully validated '+ errorGoodLength +' records.');
            	buttonsShowHide('Provision', 'show');
            	buttonsShowHide('Show Records With Errors Only', 'hide');
            	buttonsShowHide('Download erroneous records as XLSX', 'hide');
    		}
    		$("#showVMDialogueValidate table#vmTableOutput tr#submitProvision").hide();	

    	};
    	loadVMDialogueValidate();
    	
    	
        $("#subBulk").click(function () {
        	pendingProcess.push("Provision Express Voicemail");
            $("#loadingBulk").dialog("open");
            $("#loadingBulk").html("<div class=\"loading\" id=\"loading3\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
            $("#loading3").show();
            $.ajax({
                type: "POST",
                url: "expressSheets/processVoicemailData.php",
                data: dataString,
                success: function (result) {
                	if(foundServerConErrorOnProcess(result, "Provision Express Voicemail")) {
    					return false;
                  	}
                    $("#loading3").hide();
                    $(".ui-dialog-buttonpane", this.parentNode).show();
                    $("#loadingBulk").html(result);
                    $("#loadingBulk").append(returnLink);
                }
            });
        });

        $("#loadingBulk").dialog(
            {
                autoOpen: false,
                width: 1400,
                modal: true,
                resizable: false,
                position: {my: "top", at: "top"},
                closeOnEscape: false,
                open: function (event, ui) {
					setDialogDayNightMode($(this));
                	$(":button:contains('More changes')").addClass("subButton").show();
                    $(".ui-dialog-titlebar-close", ui.dialog).hide();
                    $(".ui-dialog-buttonpane", this.parentNode).hide();
                }
            });
    });
</script>

<div id="parseDiv" class="well table-responsive">
	<form name="parseForm" id="parseForm" method="POST" action="">
		<table align="center" class="tablesorter dataTable table table-bordered table-striped" style="width:100%;" id="vmTableOutput">
			<thead><tr><?php echo $voicemailManager->createTableHeader(); ?></tr></thead>
			<?php echo $voicemailManager->parse(); ?>
		</table>
		<?php echo submitOrReSubmit($voicemailManager->hasError()); ?>
	</form>
</div>
<div id="loadingBulk"></div>
<div id="showVMDialogueValidate" class="dialogClass"></div>

<form name="submitErrorForm" id="submitErrorForm" action="expressSheets/processVmErrorData.php" method="post">
	<input type="hidden" name="columnArray" id="columnArray" value="">
	<input type="hidden" name="valueArray" id="valueArray" value="">
	<input type="hidden" name="errorArray" id="errorArray" value="">
</form>
