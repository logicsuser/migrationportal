<?php
/**
 * Created by Dipin Krishna.
 * Date: 12/3/17
 * Time: 3:52 PM
 */
//require_once("/var/www/lib/broadsoft/login.php");

if(isset($_GET['download_id']) && file_exists("/tmp/" . $_GET['download_id'])) {

	$download_status = json_decode(file_get_contents("/tmp/" . $_GET['download_id']), true);

	if(isset($_GET['download'])) {
		$file_name = $download_status['fileName'];
		$file_path = $download_status['filePath'];
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header("Content-Transfer-Encoding: Binary");
		header("Content-disposition: attachment; filename=\"$file_name\"");
		readfile($file_path);
		exit;
	} else {
		$download_status['filePath'] = "";
		echo file_get_contents("/tmp/" . $_GET['download_id']);
	}
} else {
	echo json_encode(array("success" => false));
}
exit;
?>