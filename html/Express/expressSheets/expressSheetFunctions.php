<?php
/**
 * Created by Dipin Krishna.
 * Date: 12/3/17
 * Time: 12:00 AM
 */

require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
$expProvDB = new ExpressProvLogsDB();
$GLOBALS['expressProvLogsDb'] = $expProvDB->expressProvLogsDb;

function getDistictCustomProfiles() {
	global $db;

	$stmt = $db->prepare("select distinct " . expressSheetCustomProfileName . " from " . expressSheetCustomProfileTable . " order by " . expressSheetCustomProfileName);
	$stmt->execute();
	$profiles = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $profiles;
}

function getCustomProfilesForDeviceType($deviceType) {
	global $db;

	$stmt = $db->prepare("select distinct " . expressSheetCustomProfileName . " from " . expressSheetCustomProfileTable . " where " . expressSheetCustomProfileType . " = ? order by deviceType, " .
		expressSheetCustomProfileName);
	$stmt->execute(array($deviceType));
	$profiles = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $profiles;
}


function checkDeviceSupports($deviceType) {
	global $ociVersion;

	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");

	$dOP = new DeviceOperations;
	$dOP->ociVersion = $ociVersion;
	$detail = $dOP->getDeviceTypesProfileDetail($deviceType);

	if($detail['Success']['staticLineOrdering'] == "false") {
		$supportType = "dynamic";
	} else if($detail['Success']['staticLineOrdering'] == "true") {
		$supportType = "static";
	}

	return $supportType;
}

function getPortsForDeviceType($deviceType) {
	global $db;

	$stmt = $db->prepare("select ports from devices where deviceName = ? limit 1");
	$stmt->execute(array($deviceType));
	$ports = $stmt->fetch(PDO::FETCH_ASSOC);

	return $ports;
}

function getLinePortDevTypeLookup() {
    global $db;
    //Code added @ 16 July 2019
    $whereCndtn = "";
    if($_SESSION['cluster_support']){
        $selectedCluster = $_SESSION['selectedCluster'];
        $whereCndtn = " WHERE clusterName ='$selectedCluster' ";
    }
    //End code
    $deviceSubList = array();
    $qryLPLU = "select * from systemDevices $whereCndtn ";
    $select_stmt = $db->prepare($qryLPLU);
    $select_stmt->execute();
    $i = 0;
    while($rs = $select_stmt->fetch())
    {
        $deviceSubList[$i]['deviceType'] = $rs['deviceType'];
        $deviceSubList[$i]['substitution'] = $rs['nameSubstitution'];
        $i++;
    }
    return $deviceSubList;
}

function replaceDevNameWithSubstitute($deviceName, $linePort) {
	$substituteData = array();
	$substituteData['deviceName'] = $deviceName;
	$substituteData['linePort'] = $linePort;
	$subList = getLinePortDevTypeLookup();
	//error_log(print_r($substituteData, true));
	foreach ($subList as $key => $value) {
		if (strpos($deviceName, $value['deviceType']) !== false) {
			$substituteData['deviceName'] = str_replace($value['deviceType'], $value['substitution'], $deviceName);
			$substituteData['linePort'] = str_replace(str_replace(" ", "_", $value['deviceType']), $value['substitution'], $linePort);
		}
	}
	//error_log(print_r($substituteData, true));
	return $substituteData;
}

function getAllUserFilters($userId) {
	global $db;

	$stmt = $db->prepare("select * from user_filters WHERE userid = ? and filter_name != 'preset_filters'");
	$stmt->execute(array($userId));
	$filters = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $filters;
}

function addUpdateUserFilter($userId, $filter, $name = "", $id = 0) {

	global $db;

	if($id) {
		$stmt = $db->prepare("update user_filters set filter = ?, filter_name = ? where id = ? and userid = ?");
		$stmt->execute(array($filter, $name, $id, $userId));
	} else {
		$stmt = $db->prepare("insert into user_filters (filter, filter_name, userid) VALUES(?, ?, ?)");
		$stmt->execute(array($filter, $name, $userId));
		$id = $db->lastInsertId();
	}

	return $id;
}

function deleteUserFilter($userId, $id) {

	global $db;

	if($id) {
		$stmt = $db->prepare("delete from user_filters where id = ? and userid = ?");
		if($stmt->execute(array($id, $userId))) {
			return true;
		}
	}

	return false;
}

function toggleUserFilter($userId, $id, $inactive = 0) {

	global $db;

	if($id) {
		$stmt = $db->prepare("update user_filters set inactive = ? where id = ? and userid = ?");
		if($stmt->execute(array($inactive, $id, $userId))) {
			return true;
		}
	}

	return false;
}

function getUserPresetFilters($userId) {
	global $db;

	$stmt = $db->prepare("select * from user_filters WHERE userid = ? and filter_name = 'preset_filters' order by id desc limit 1");
	if($stmt->execute(array($userId))) {
		if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			$filters = $row['filter'] ? json_decode($row['filter']): null;
			return $filters;
		}
	}

	return null;
}

function addUpdatePresetFilter($userId, $filter) {

	global $db;

	if(getUserPresetFilters($userId)) {
		$stmt = $db->prepare("update user_filters set filter = ? where filter_name = 'preset_filters' and userid = ?");
		$stmt->execute(array($filter, $userId));
		return;
	} else {
		$stmt = $db->prepare("insert into user_filters (filter, filter_name, userid) VALUES(?, ?, ?)");
		$stmt->execute(array($filter, 'preset_filters', $userId));
		$id = $db->lastInsertId();
	}

	return $id;
}

function getAllCustomTags() {
	global $db;

	$stmt = $db->prepare("select DISTINCT shortDescription from customTagSpec order by shortDescription");
	$stmt->execute();
	$custom_tags = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $custom_tags;
}

function array_msort($array, $cols)
{
	$colarr = array();
	foreach ($cols as $col => $order) {
		$colarr[$col] = array();
		foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
	}
	$eval = 'array_multisort(';
	foreach ($cols as $col => $order) {
		$eval .= '$colarr[\''.$col.'\'],'.$order.',';
	}
	$eval = substr($eval,0,-1).');';
	eval($eval);
	$ret = array();
	foreach ($colarr as $col => $arr) {
		foreach ($arr as $k => $v) {
			$k = substr($k,1);
			if (!isset($ret[$k])) $ret[$k] = $array[$k];
			$ret[$k][$col] = $array[$k][$col];
		}
	}
	return $ret;

}

function deviceExists($device) {
	global $devices;

	foreach ($devices as $key => $value) {
		if ($value["deviceName"] == $device) {
			return true;
		}
	}
	return false;
}

function getDeviceTypeNumberOfPorts($deviceType) {
	global $staticLineOrderingDeviceTypes;

	foreach ($staticLineOrderingDeviceTypes as $key => $value) {
		if ($value["deviceType"] == $deviceType) {
			return $value["numberOfPorts"];
		}
	}

	return 0;
}

function getDevicePortUsage($deviceName) {
	global $ociVersion, $sessionid, $client;
	$portDetails = array();
	$ociCmd = "GroupAccessDeviceGetRequest18sp1";
	$xmlinput = xmlHeader($sessionid, $ociCmd);
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	// print_r($response);
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$portDetails["numberOfPorts"]         = isset($xml->command->numberOfPorts->quantity) ? strval($xml->command->numberOfPorts->quantity) : 1000;
	$portDetails["numberOfAssignedPorts"] = strval($xml->command->numberOfAssignedPorts);

	return $portDetails;
}

function getDeviceAvailablePorts($deviceName) {
	global $sessionid, $client;

	$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceAvailablePortGetListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<accessDevice>";
	$xmlinput .=    "<deviceLevel>Group</deviceLevel>";
	$xmlinput .=    "<deviceName>" . $deviceName . "</deviceName>";
	$xmlinput .= "</accessDevice>";
	$xmlinput .= xmlFooter();

	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	// print_r($response);       exit;
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	$availablePorts = array();
	foreach ($xml->command->portNumber as $key => $value) {
		$availablePorts[$a++] = strval($value);
	}

	return $availablePorts;
}

function getNumberOfAssignedPorts($device) {
	global $ociVersion, $sessionid, $client;

	$ociCmd = "GroupAccessDeviceGetRequest18sp1";
	$xmlinput = xmlHeader($sessionid, $ociCmd);
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<deviceName>" . $device . "</deviceName>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	return strval($xml->command->numberOfAssignedPorts);
}

function deleteDeviceOnUserRemoval($deviceType) {
	global $db, $deleteAnalogUserDevice;

	$sipGatewayLookup = new DBLookup($db, "systemDevices", "deviceType", "phoneType");
	$deviceIsAnalog = $sipGatewayLookup->get($deviceType) == "Analog";

	// short-circuit for Audio-Codes
	$deviceIsAudioCodes = substr($deviceType, 0, strlen("AudioCodes-")) == "AudioCodes-";

	if ($deviceIsAnalog || $deviceIsAudioCodes) {
		return $deleteAnalogUserDevice == "true";
	}

	// device is not for analog user
	return true;
}

function removeDeviceFromUser($userId, $deviceName, $deviceType)
{
	global $sessionid, $client;

	//unlink user and device
	$xmlinput = xmlHeader($sessionid, "UserModifyRequest17sp4");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= "<endpoint xsi:nil=\"true\" />";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	$errorResult = readError($xml);
	if (getNumberOfAssignedPorts($deviceName) == 0 && deleteDeviceOnUserRemoval($deviceType) && deviceExists($deviceName)) {
		//delete the device
		$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceDeleteRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errorResult = readError($xml);
		if($errorResult == "") {
			resetDevices($deviceName);
		}
		return $errorResult;
	}
	return "";
}

// Delete older device when device updated
function deleteOlderDevice($previousDeviceName)
{
	global $db, $deleteAnalogUserDevice;
	global $sessionid, $client;
	$deviceName = $previousDeviceName;
	$response = "";
	if ($deviceName != "" && getNumberOfAssignedPorts($deviceName) == 0 && $deleteAnalogUserDevice)
	{
		if(deviceExists($deviceName))
		{
			$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceDeleteRequest");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
			$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if(readError($xml) !=""){
				$response = $xml;
			}else{
				$response = "Success";
			}
		}

	}
	return $response;
}

function getAllPhoneProfiles() {
	global $db;

	$stmt = $db->prepare("select * from phoneProfilesLookup where inactive = 0");
	$stmt->execute();
	$phoneProfiles = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $phoneProfiles;
}

function getAllTagBundles() {
	global $db;

	$stmt = $db->prepare("select DISTINCT tagBundle from deviceMgmtTagBundles where inactive = 0");
	$stmt->execute();
	$tagBundles = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $tagBundles;
}

function getPhoneProfileValue($phoneProfile) {
	global $db;

	$stmt = $db->prepare("select value from phoneProfilesLookup where phoneProfile = ?");
	if($stmt->execute(array($phoneProfile))) {
		if($phoneProfile = $stmt->fetch(PDO::FETCH_ASSOC)) {
			return $phoneProfile['value'];
		}
	}

	return null;
}

function getAllTagsforBundle($tagBundle) {
	global $db;

	$stmt = $db->prepare("select * from deviceMgmtTagBundles where tagBundle = ? and inactive = 0");
	if($stmt->execute(array($tagBundle))) {
		if($tags = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
			return $tags;
		}
	}

	return array();
}

function getAllUniqueTagsForBundles() {
	global $db;

	$stmt = $db->prepare("select DISTINCT tag from deviceMgmtTagBundles WHERE inactive = 0");
	if($stmt->execute()) {
		if($tags = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
			$tagArray = array();
			foreach ($tags as $tag) {
				$tagArray[] = $tag['tag'];
			}
			return $tagArray;
		}
	}

	return array();
}

function removeDeviceTag($sp, $groupId, $deviceName, $tagName)
{
	global $sessionid, $client;

	$xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagDeleteListRequest");
	$xmlinput .= "<serviceProviderId>" .  htmlspecialchars($sp) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
	$xmlinput .= "<deviceName>" . htmlspecialchars($deviceName) . "</deviceName>";
	$xmlinput .= "<tagName>" . htmlspecialchars($tagName) . "</tagName>";

	$xmlinput .= xmlFooter();

	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	$error = readErrorXmlGenuine($xml);

	return $error;
}

function addDeviceTag($sp, $groupId, $deviceName, $tagName, $tagValue)
{
	global $sessionid, $client;

	//error_log("Add $tagName: $tagValue");
	$xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagAddRequest");
	$xmlinput .= "<serviceProviderId>" .  htmlspecialchars($sp) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
	$xmlinput .= "<deviceName>" . htmlspecialchars($deviceName) . "</deviceName>";
	$xmlinput .= "<tagName>" . htmlspecialchars($tagName) . "</tagName>";
	if(!empty($tagValue) || $tagValue === "0"){
		$xmlinput .= "<tagValue>" . $tagValue . "</tagValue>";
	}else if(empty($tagValue)){
		$xmlinput .= "<tagValue xsi:nil='true'></tagValue>";
	}

	$xmlinput .= xmlFooter();

	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	$error = readErrorXmlGenuine($xml);

	return $error;
}

function getCurrentCustomTagsForDevice($sp, $groupId, $deviceName)
{
	global $sessionid, $client;

	// get custom tags from back-end
	$xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagGetListRequest");
	$xmlinput .= "<serviceProviderId>" .  htmlspecialchars($sp) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
	$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
	$xmlinput .= xmlFooter();

	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$errMsg = readErrorXmlGenuine($xml);
	if($errMsg == "") {
		$tags = array();
		if(isset($xml->command->deviceCustomTagsTable->row)) {
			$i = 0;
			foreach ($xml->command->deviceCustomTagsTable->row as $key => $value){
				$tagName = strval($value->col[0]);

				$tags[$tagName][] = strval($value->col[1]);
				$i++;
			}
		}
		return $tags;
	}
	return array();
}

function insertUserChangeLogs($changes, $fields = array()) {

	global $db;
	$expressProvLogsDb = $GLOBALS['expressProvLogsDb'];
	$userId = $changes['userId'];
	$entityName = $changes['entityName'];
	$changeLogs = isset($changes['logs']) ? $changes['logs'] : array();
	$action = $changes['action'];

	$date = date("Y-m-d H:i:s");

	$module = ($action == "Delete") ? "Delete User" : ($action == "Modify" ? "User Modify" : ($action == "Add" ? "User Add" : $action));

	$query = "INSERT into changeLog (userName, date, module, groupId, entityName) VALUES (?, ?, ?, ?, ?)";
	$stmt = $expressProvLogsDb->prepare($query);
	$stmt->execute(array($_SESSION["loggedInUserName"], $date, $module, $_SESSION["groupId"], $entityName));
	$lastId = $expressProvLogsDb->lastInsertId();

	switch ($action) {
		case "Add" :

			$insert_query = "INSERT into userAddChanges (id, firstName, lastName, phoneNumber, extension, deviceType, deviceName, emailAddress, address1, address2, city, state, zip, department)";
			$insert_query .= " VALUE(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ";
			$userAddInfo = $changes["userInfo"];
			if(isset($userAddInfo)) {
			    $stmt = $expressProvLogsDb->prepare($insert_query);
				$stmt->execute(array(
					$lastId,
					$userAddInfo["firstName"],
					$userAddInfo["lastName"],
					$userAddInfo["phoneNumber"] ? $userAddInfo["phoneNumber"] : "None",
					$userAddInfo["extension"],
					$userAddInfo["deviceType"] ? $userAddInfo["deviceType"] : "",
					$userAddInfo["deviceName"] ? $userAddInfo["deviceName"] : "",
					$userAddInfo["emailAddress"],
					$userAddInfo["address1"],
					$userAddInfo["address2"] ? $userAddInfo["address2"] : "None",
					$userAddInfo["city"],
					$userAddInfo["state"],
					$userAddInfo["zip"],
					$userAddInfo["department"] ? $userAddInfo["department"] : "None"
				));
			}

			break;

		case "Delete" :
		case "Modify" :
		default :

			//Insert an empty entry with just the userID and the user Name in case there are no changes.
			if (count($changeLogs) <= 0) {
				$changeLogs[] = array("field" => "", "old" => "", "new" => "");
			}

			$query = "INSERT into userModChanges (id, serviceId, entityName, field, oldValue, newValue) VALUES (?, ?, ?, ?, ?, ?)";

			if (isset($changeLogs)) {
				foreach ($changeLogs as $changeLog) {
				    $stmt = $expressProvLogsDb->prepare($query);
					$fieldName = isset($fields[$changeLog["field"]]) ? $fields[$changeLog["field"]] : $changeLog["field"];

					if ($changeLog["field"] == "monitoredUsers")
					{
						$stmt->execute(array($lastId, $userId, $entityName, $fieldName, $lastId, $lastId));

						$exp = explode("&", $changeLog["old"]);
						foreach ($exp as $k => $v)
						{
							if ($v !== "")
							{
								$blf_insert_query = "INSERT into blfs (blfId, groupId, userName, state, blfUser)";
								$blf_insert_query .= " VALUES(?, ?, ?, ?, ?) ";
								$blf_insert_stmt = $db->prepare($blf_insert_query);
								$blf_insert_stmt->execute(array($lastId, $_SESSION["groupId"], $userId, 'oldValue', $v));
							}
						}
						$exp = explode("&", $changeLog["new"]);
						foreach ($exp as $k => $v)
						{
							if ($v !== "")
							{
								$blf_insert_query = "INSERT into blfs (blfId, groupId, userName, state, blfUser)";
								$blf_insert_query .= " VALUES(?, ?, ?, ?, ?) ";
								$blf_insert_stmt = $db->prepare($blf_insert_query);
								$blf_insert_stmt->execute(array($lastId, $_SESSION["groupId"], $userId, 'newValue', $v));
							}
						}
					} else {

						$stmt->execute(array($lastId, $userId, $entityName, $fieldName, addslashes($changeLog["old"]), addslashes($changeLog["new"])));
					}

				}
			}
	}
}

function getServicePackUserServices($sp, $ServicePackName)
{
	global $sessionid, $client;
	$userServices["Error"] = "";
	$userServices["Success"] = "";
	$xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($sp) . "</serviceProviderId>";
	$xmlinput .= "<servicePackName>" . $ServicePackName . "</servicePackName>";
	$xmlinput .= xmlFooter();

	$response = $client->processOCIMessage(array(
		"in0" => $xmlinput
	));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	if (readErrorXmlGenuine($xml) != "") {
		$userServices["Error"] = $xml->command->summaryEnglish;
	}

	$userServices = array();
	foreach ($xml->command->userServiceTable->row as $key => $value) {
		$userServices[] = strval($value->col[0]);
	}
	return $userServices;
}

function getUserPolycomPhoneServiceValues($userId, $deviceName)
{
	global $sessionid, $client;

	$xmlinput = xmlHeader($sessionid, "UserPolycomPhoneServicesGetRequest");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= "<accessDevice>
						<deviceLevel>Group</deviceLevel>
						<deviceName>" . $deviceName . "</deviceName>
					</accessDevice>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));

	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$PolycomPhoneServices = array();
	$PolycomPhoneServices["integration"] = strval($xml->command->integratePhoneDirectoryWithBroadWorks);
	$PolycomPhoneServices["ccd"] = strval($xml->command->groupCustomContactDirectory);

	return $PolycomPhoneServices;
}

function removeUserfromPolycomPhoneServices($sp, $groupId, $userId, $deviceName) {

	global $sessionid, $client;

	removeUserFromGroupCCD($sp, $groupId, $userId);

	$xmlinput = xmlHeader($sessionid, "UserPolycomPhoneServicesModifyRequest");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= "<accessDevice>";
	$xmlinput .= "<deviceLevel>Group</deviceLevel>";
	$xmlinput .= "<deviceName>" . htmlspecialchars($deviceName) . "</deviceName>";
	$xmlinput .= "</accessDevice>";
	$xmlinput .= "<integratePhoneDirectoryWithBroadWorks>false</integratePhoneDirectoryWithBroadWorks>";
	$xmlinput .= "<includeUserPersonalPhoneListInDirectory>false</includeUserPersonalPhoneListInDirectory>";
	$xmlinput .= "<includeGroupCustomContactDirectoryInDirectory>false</includeGroupCustomContactDirectoryInDirectory>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);


	$xmlinput = xmlHeader($sessionid, "UserServiceUnassignListRequest");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= "<serviceName>". htmlspecialchars('Polycom Phone Services') . "</serviceName>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

}

function addUserToPolycomPhoneServices($sp, $groupId, $userId, $deviceName, $ccd = "None") {

	global $sessionid, $client;

	//Add Polycom Phone Services if The user doesn't have it now
	if (!checkIfUserHasService($userId, 'Polycom Phone Services')) {
		$xmlinput = xmlHeader($sessionid, "UserServiceAssignListRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= "<serviceName>Polycom Phone Services</serviceName>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	}

	//Double check Polycom Phone Services
	if (checkIfUserHasService($userId, 'Polycom Phone Services')) {

		$ccd = $ccd ? $ccd : 'None';

		removeUserFromGroupCCD($sp, $groupId, $userId);

		if($ccd != 'None') {
			$result = addUserToCCD($sp, $groupId, $userId, $ccd);
			if($result !== true) {
				return $result;
			}
		}

		$xmlinput = xmlHeader($sessionid, "UserPolycomPhoneServicesModifyRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= "<accessDevice>";
		$xmlinput .= "<deviceLevel>Group</deviceLevel>";
		$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
		$xmlinput .= "</accessDevice>";
		$xmlinput .= "<integratePhoneDirectoryWithBroadWorks>true</integratePhoneDirectoryWithBroadWorks>";
		$xmlinput .= "<includeUserPersonalPhoneListInDirectory>true</includeUserPersonalPhoneListInDirectory>";
		$xmlinput .= "<includeGroupCustomContactDirectoryInDirectory>true</includeGroupCustomContactDirectoryInDirectory>";
		if($ccd != 'None') {
			$xmlinput .= "<groupCustomContactDirectory>" . strval($ccd) . "</groupCustomContactDirectory>";
		} else {
			$xmlinput .= '<groupCustomContactDirectory xsi:nil="true" />';
		}
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		//error_log($xmlinput);
		if (readErrorXmlGenuine($xml) != "") {
			error_log($xml->command->summaryEnglish);
			return $xml->command->summaryEnglish;
		}
	}

	return true;
}

function addUserToCCD($sp, $groupId, $userId, $ccd) {

	global $sessionid, $client;

	//retrieve list of users from custom contact directory and append new user to list
	$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryGetRequest17");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($sp) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
	$xmlinput .= "<name>" . htmlspecialchars(strval($ccd)) . "</name>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	//error_log($xmlinput);
	if (readErrorXmlGenuine($xml) != "") {
		error_log($xml->command->summaryEnglish);
		return $xml->command->summaryEnglish;
	}

	$ccdUsersList = "";
	foreach ($xml->command->userTable->row as $kk => $vv) {
		if(strval($vv->col[0]) != $userId) {
			$ccdUsersList .= "<entry><userId>" . strval($vv->col[0]) . "</userId></entry>";
		}
	}
	$ccdUsersList .= "<entry><userId>" . $userId . "</userId></entry>";

	$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryModifyRequest17");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($sp) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
	$xmlinput .= "<name>" . htmlspecialchars(strval($ccd)) . "</name>";
	$xmlinput .= "<entryList>" . $ccdUsersList . "</entryList>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	//error_log($xmlinput);
	if (readErrorXmlGenuine($xml) != "") {
		error_log($xml->command->summaryEnglish);
		return $xml->command->summaryEnglish;
	}

	return true;
}

function removeUserFromGroupCCD($sp, $groupId, $userId) {

	global $sessionid, $client;

	//retrieve custom contact directories
	$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryGetListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($sp) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	foreach ($xml->command->name as $k => $v) {
		$ccdList = "<groupCustomContactDirectory>" . strval($v) . "</groupCustomContactDirectory>";

		//retrieve list of users from custom contact directory and append new user to list
		$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryGetRequest17");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
		$xmlinput .= "<name>" . htmlspecialchars(strval($v)) . "</name>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		$ccdUsersList = "";
		foreach ($xml->command->userTable->row as $kk => $vv) {
			if(strval($vv->col[0]) != $userId) {
				$ccdUsersList .= "<entry><userId>" . strval($vv->col[0]) . "</userId></entry>";
			}
		}

		$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryModifyRequest17");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
		$xmlinput .= "<name>" . htmlspecialchars(strval($v)) . "</name>";
		$xmlinput .= "<entryList>" . $ccdUsersList . "</entryList>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	}

}

function checkIfUserHasService($userId, $serviceName) {

	global $sessionid, $client;

	//Polycom Services
	$xmlinput = xmlHeader($sessionid, "UserServiceIsAssignedRequest");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= "<serviceName>$serviceName</serviceName>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	if (strval($xml->command->isAssigned) == "true") {
		return true;
	}

	return false;
}

function getColumnNameFromNumber($num) {
	$numeric = $num % 26;
	$letter = chr(65 + $numeric);
	$num2 = intval($num / 26);
	if ($num2 > 0) {
		return getColumnNameFromNumber($num2 - 1) . $letter;
	} else {
		return $letter;
	}
}
?>