<?php
/**
 * Created by Dipin Krishna.
 * Date: 10/18/17
 * Time: 1:44 PM
 */

$express_sheet_revision = array(
	"Users" => "08",
	"SCA" => "03",
	"BLF" => "02",
	"Voicemail" => "01"
);
$softwareRevision = file("/var/www/revision.txt", FILE_IGNORE_NEW_LINES);
$expressRevision = ($softwareRevision && isset($softwareRevision[0])) ? $softwareRevision[0] : "Rel.";



//Custom Phone Profiles
const expressSheetCustomProfileTable = "customProfiles";
const expressSheetCustomProfileName  = "customProfileName";
const expressSheetCustomProfileType  = "deviceType";