<?php

$fileName = 'rollback.csv';
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/octet-stream");
header("Content-Disposition: attachment;filename=\"" . $fileName . ".csv\"");
header("Content-Transfer-Encoding: binary");

//require_once("getCdrsSummary.php");
$message = "Responsible Party, 411, 911, LD, Local, Intl, Toll Free\n";
foreach($_POST["data"]["users"] as $key => $value){
	$message .=
	"=\"" . $value["action"] . "\"," .
	"=\"" . $value["firstName"]. "\"," .
	"=\"" . $value["lastName"]. "\"," .
	"=\"" . $value["callingLineIdFirstName"]. "\"," .
	"=\"" . $value["callingLineIdLastName"]. "\"," .
	"=\"" . $value["phoneNumber"]. "\"," .
	"=\"" . $value["callingLineIdPhoneNumber"]. "\"\n";
}


$fp = fopen("php://output", "a");
fwrite($fp, $message);
fclose($fp);
?>
