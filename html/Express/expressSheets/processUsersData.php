<?php
/**
 * Created by Dipin Krishna.
 * Date: 10/25/17
 * Time: 11:07 AM
 */
?>
<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

//Prevent the Maximum execution time limit
ini_set('max_execution_time', 600);

if ($_SESSION["superUser"] != "1" && $_SESSION["permissions"]["expressSheets"] != "1") {
	echo "<div style='color: red;text-align: center;'>You don't have permission to access Express Sheets.</div>";
	exit;
}

if ($_SESSION["superUser"] != "1" && $_SESSION["permissions"]["deleteUsers"] != "1" && $_SESSION["permissions"]["modifyUsers"] != "1" && $_SESSION["permissions"]["addusers"] != "1") {
	echo "<div style='color: red;text-align: center;'>You don't have permission to provision Users.</div>";
	exit;
}

require_once("/var/www/lib/broadsoft/adminPortal/GroupCallPickupUsers.php");
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/expressSheetFunctions.php");
require_once("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");

const customProfileTable = "customProfiles";
const customProfileName = "customProfileName";
const customTagName = "customTagName";
const customTagValue = "customTagValue";
const customProfileType = "deviceType";

const ExpressCustomProfileName = "%Express_Custom_Profile_Name%";

require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");

$sysOperations = new sysLevelDeviceOperations();
if(count($tagsetName["Success"]) > 0){
	$systemTagList = $sysOperations->getDefaultTagListByTagSet($tagsetName["Success"]);
	$_SESSION['systemTagList'] = $systemTagList["Success"];
}

require_once ("/var/www/lib/broadsoft/adminPortal/httpful.phar");

$fields = array(
	"firstName" => "First Name",
	"lastName" => "Last Name",
	"callingLineIdFirstName" => "Calling Line ID First Name",
	"callingLineIdLastName" => "Calling Line ID Last Name",
	"phoneNumber" => "Phone Number",
	"callingLineIdPhoneNumber" => "Calling Line ID Phone Number",
	"extension" => "Extension",
	"department" => "Department",
	"addressLine1" => "Address",
	"addressLine2" => "Suite",
	"city" => "City",
	"state" => "State/Province",
	"zip" => "Zip/Postal Code",
	"timeZone" => "Time_Zone",
	"language" => "Language",
	"emailAddress" => "Email Address",
	"servicePack" => "Service Pack",
    "userServices" => "User Services",
	"deviceName" => "Device Name",
	"deviceType" => "Device Type",
	"phoneProfile" => "Phone Profile",
	"linePortDomain" => "LinePort Domain",
	"macAddress" => "MAC Address",
	"deviceIndex" => "Device Index",
	"portNumber" => "Port Number",
	"voiceMessaging" => "Assign and Enable VM Service",
	"enableVM" => "Enable VM",
	"polycomPhoneServices" => "Polycom Phone Services",
	"deviceAccessUserName" => "Device Access User Name",
	"deviceAccessPassword" => "Device Access Password",
	"callingLineIdPolicy" => "Calling Line ID Policy",
	"networkClassOfService" => "Network Class of Service",
	"callPickupGroup" => "Call Pickup Group",
	"tagBundles" => "Tag Bundles",
	"numOfLineAppearances" => "Num of Line Apperances",
	"callsPerLineKey" => "Calls per LineKey",
	"polycomLineRegNumber" => "Polycom Line Reg. Number",
	"addressLocation" => "Address Location",
	"uActivateNumber" => "Activate Number",
);

function isVMServicePackOrVMserviceAvailable($servicePackArray, $userServiceArray, $servicePacks, $userServices) {
    /* Start selected servicepacks have VM services */
    $assignedVMService = false;
    $serviceObj = new Services();
    $getSevicePackWithVM = $serviceObj->getVMServicePack($servicePackArray, $_SESSION["sp"]);
    $assignedVMService = count($getSevicePackWithVM) > 0 ? true : false;        /* Check in assinged service packs.*/
    
    if( !$assignedVMService && in_array("Voice Messaging User", $userServiceArray) ) { /* Check in assigned services.*/
        $assignedVMService = true;
    }
    /* Assigned Service Pack and Services List*/
    if( !$assignedVMService ) {
        $assignedVMService = getVMEnabledServicePack($servicePacks, $userServices);
    }
    
    return $assignedVMService;
}


function getVMEnabledServicePack($servicePacks, $userServices) {
    global $sessionid, $client;
    
    $response = new stdClass;
    $serviceObj = new Services();
    /* Get VM service pack with VM user service only */
    foreach ($servicePacks as $servicePack) {
        $services = $serviceObj->getServicesListOfServicePackBasedOnEnterprise($servicePack, $_SESSION['sp']);
        if (in_array("Voice Messaging User", $services)) {
            if (count($services) == 1) {
                $response->VMOnlyServicePack = $servicePack;
                break;
            }
        }
    }
    
    /* If VM only service pack is not available then check VM user service */
    if( !(isset($response->VMOnlyServicePack)) ) {
        if (in_array("Voice Messaging User", $userServices)) {
            $response->VMService = "Voice Messaging User";
        }
    }
    
    return $response;
}

function addDeviceCustomTag($deviceName, $tagName, $tagValue) {
	global $sessionid, $client;

	$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceCustomTagAddRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
	$xmlinput .= "<tagName>" . $tagName . "</tagName>";
	$xmlinput .= "<tagValue>" . $tagValue . "</tagValue>";

	$xmlinput .= xmlFooter();

	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$errMsg = "";
	if (readError($xml) != "") {
		$errMsg = "Failed to add custom tag to device " . $deviceName . " .";
		$errMsg .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
		if ($xml->command->detail) {
			$errMsg .= "%0D%0A" . strval($xml->command->detail);
		}
	}
	return $errMsg;
}


function addDeviceCustomTags($deviceType, $deviceName, $customProfile) {
	global $db;

	// No custom tags can be selected while building a user
	// ----------------------------------------------------
	if ($customProfile == "") {
		return "";
	}
	if ($deviceType == "") {
		return "";
	}

	// get custom tags from database for a specific custom profile
	// and add them to the device
	// -----------------------------------------------------------
	$query = "select " . customTagName . ", " . customTagValue . " from " . customProfileTable . " where " . customProfileType . "='" . $deviceType . "' and " . customProfileName . "='" . $customProfile . "'";
	$results = $db->query($query);

	//$customTags = array();
	while ($row = $results->fetch()) {
		if (($errMsg = addDeviceCustomTag($deviceName, $row[customTagName], $row[customTagValue])) != "") {
			return $errMsg;
		}
	}

	// finally add custom tag with the custom profile name for the reporting purpose
	if (($errMsg = addDeviceCustomTag($deviceName, ExpressCustomProfileName, $customProfile)) != "") {
		return $errMsg;
	}

	return "";
}

/**
 * Get resource value that can be used to build Surgemail name
 * @param $attr - resource type
 * @param $value - user information from users bulk list
 * @return string   - requested resource value
 */
function getResourceForSurgeMailNameCreation($attr, $value) {
	global $surgemailDomain;

	switch ($attr) {
		case ResourceNameBuilder::DN:
			return isset($value["phoneNumber"]) && $value["phoneNumber"] != "" ? $value["phoneNumber"] : "";
		case ResourceNameBuilder::EXT:
			return isset($value["extension"]) && $value["extension"] != "" ? $value["extension"] : "";
		case ResourceNameBuilder::USR_CLID:
			return isset($value["callingLineIdPhoneNumber"]) && $value["callingLineIdPhoneNumber"] != "" ? $value["callingLineIdPhoneNumber"] : '';
		case ResourceNameBuilder::GRP_DN:
			return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
		case ResourceNameBuilder::GRP_ID:
			return str_replace(" ", "_", trim($_SESSION["groupId"])); 
		case ResourceNameBuilder::SYS_DFLT_DOMAIN:
			return $_SESSION["systemDefaultDomain"];
		case ResourceNameBuilder::SRG_DOMAIN:
			return $surgemailDomain;
		case ResourceNameBuilder::FIRST_NAME:
			return $value["firstName"];
		case ResourceNameBuilder::LAST_NAME:
			return $value["lastName"];
		case ResourceNameBuilder::DEV_NAME:
			return isset($value["deviceName"]) && $value["deviceName"] != "" ? $value["deviceName"] : "";
		case ResourceNameBuilder::MAC_ADDR:
			return isset($value["macAddress"]) && $value["macAddress"] != "" ? $value["macAddress"] : "";
		case ResourceNameBuilder::USR_ID:
			$name = $value["userId"];
			if ($name == "") {
				return "";
			}
			$nameSplit = explode("@", $name);
			return $nameSplit[0];
			break;
		default:
			return "";
	}
}

/**
 * Get resource value that can be used to build Voice Mail password
 * @param $attr - resource type
 * @param $value - user information from users bulk list
 * @return string   - requested resource value
 */
function getResourceForVoicePasswordCreation($attr, $value) {
	switch ($attr) {
		case ResourceNameBuilder::DN:
			return isset($value["phoneNumber"]) && $value["phoneNumber"] != "" ? $value["phoneNumber"] : "";
		case ResourceNameBuilder::EXT:
			return isset($value["extension"]) && $value["extension"] != "" ? $value["extension"] : "";
		case ResourceNameBuilder::USR_CLID:
			return isset($value["callingLineIdPhoneNumber"]) && $value["callingLineIdPhoneNumber"] != "" ? $value["callingLineIdPhoneNumber"] : '';
		case ResourceNameBuilder::GRP_DN:
			return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
		case ResourceNameBuilder::GRP_ID:
			return $_SESSION["groupId"];
		case ResourceNameBuilder::MAC_ADDR:
			return isset($value["macAddress"]) && $value["macAddress"] != "" ? $value["macAddress"] : "";
	}
}

?>
<div class="">
	<label class="labelTextGrey">The following users have been provisioned:</label><br><br>
	<div class="well table-responsive">

	<table border="1" align="center" id="bulkUserTableSuccess" class="bulkUserTable tablesorter table table-bordered table-striped dataTable">
		<thead>
		<tr>
			<th class="thsmallExSheet" >Action</th>
			<th class="thsmallExSheet" >First Name</th>
			<th class="thsmallExSheet" >Last Name</th>
			<th class="thsmallExSheet" >Calling Line ID First Name</th>
			<th class="thsmallExSheet" >Calling Line ID Last Name</th>
			<th class="thsmallExSheet" >Phone Number</th>
			<th class="thsmallExSheet" >Calling Line ID Phone Number</th>
			<th class="thsmallExSheet" >Extension</th>
			<th class="thsmallExSheet" >Department</th>
			<th class="thsmallExSheet" >Address</th>
			<th class="thsmallExSheet" >Suite</th>
			<th class="thsmallExSheet" >City</th>
			<th class="thsmallExSheet" >State/Province</th>
			<th class="thsmallExSheet" >Zip/Postal Code</th>
			<th class="thsmallExSheet" >Time Zone</th>
			<th class="thsmallExSheet" >Network Class of Service</th>
			<th class="thsmallExSheet" >Language</th>
			<th class="thsmallExSheet" >Email Address</th>
			<th class="thsmallExSheet" >Service Pack</th>
			<th class="thsmallExSheet" style="min-width: 220px;">User Service</th>
			<th class="thsmallExSheet" >Device Type</th>
			<th class="thsmallExSheet" >MAC Address</th>
			<th class="thsmallExSheet" >Voice Messaging</th>
			<th class="thsmallExSheet" >Polycom Phone Services</th>
			<th class="thsmallExSheet" >Device Access User Name</th>
			<th class="thsmallExSheet" >Device Access Password</th>
			<th class="thsmallExSheet" >Calling Line ID Policy</th>
			<th class="thsmallExSheet" >Voice Mail Password</th>
			<th class="thsmallExSheet" >Web Password</th>
			<th class="thsmallExSheet" >Call Pickup Group</th>

		</tr>
		</thead>
		<?php

		require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
		require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailNameBuilder.php");
		require_once("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
		require_once("/var/www/lib/broadsoft/adminPortal/getAllDevices.php");
		include("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");
		//require_once("/var/www/lib/broadsoft/adminPortal/surgeMail/SurgeMailCommunicator.php");
		require_once("/var/www/lib/broadsoft/adminPortal/util/DownloadUtil.php");
		require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailOperations.php");
                //Code added @ 03 June 2019 Regarding EX-1345
                require_once("/var/www/lib/broadsoft/adminPortal/services/Services.php");    
                $serviceObj  = new Services();
                //End code
                
		$sugeObj = new SurgeMailOperations();   //Added @ 12 May 2018

		//error_log(print_r($devices, true));
		/*$explodeVar = explode(":", $surgemailIp);
		$surgemailIp = $explodeVar[0];
		$surgemailcomm = new SurgeMailCommunicator($surgemailIp, $sshUsername, $sshPassword, $sshPort);
		$createFileResp = $surgemailcomm->createUserListFile($surgemailDomain);

		$downloadUtilObj = new ServerFileDownload();
		$downloadResp = $downloadUtilObj->downloadFile($surgemailIp, $sshUsername, $sshPassword, "/usr/local/surgemail/find_user.txt", "/var/www/html/surgeResult/userList.txt");
		if ($downloadResp) {
			$userList = file_get_contents('/var/www/html/surgeResult/userList.txt');
		} else {
			echo "Users can not be added because Surgemail users list not downloaded";
			exit;
		}*/

        $errorMessage = "";
        $errorInModule = "";
        $errorFlag = 0;
        $errorneousDataArray = array();
        $arrayIndexCount= 0;

		$_POST['users'] = array();

		if(isset($_POST['userData'])) {
			if($userDataJson = html_entity_decode($_POST['userData'])) {
				$userData = json_decode($userDataJson, true);
				//print_r($userDataJson);
				$_POST['users'] = $userData;
			}
		}

		//print_r($_POST['users']);
		$deleteArray = array();        
		foreach ($_POST["users"] as $key => $value) {

			$changeLogs = array();

			//error_log("Deivce Name: " . $value['deviceName']);

			$action = $value['action'];
			$userId = $value["userId"];
			/* Start Substiture device name in device name and line port*/
			$substituteData = replaceDevNameWithSubstitute($value['deviceName'], $value['linePort']);
			$value['deviceName'] = $substituteData['deviceName'];
			$value['linePort'] = $substituteData['linePort'];
			/* End */

			//error_log("Deivce Name: " . $value['deviceName']);

			if (strtolower($action) == 'delete') {

				$lastName = $value['lastName'];
				$firstName = $value['firstName'];
				$clidFirst = $value['callingLineIdFirstName'];
				$clidLast = $value['callingLineIdLastName'];
				$nameDialingFirst = $value['nameDialingFirstName'];
				$nameDialingLast = $value['nameDialingLastName'];
				$callingLineIdPhoneNumber = $value['callingLineIdPhoneNumber'];
				$ext = $value["extension"];
				$ph = $value["phoneNumber"];
				$emailAddress = $value["emailAddress"];
				$servicePackArrays = explode(";", $value["servicePack"]);
				$voiceMessaging = $value['voiceMessaging'];
				$enableVM = strtoupper($voiceMessaging) == "YES" ? "true" : "false";
				//$enableVM = strtoupper($value['enableVM']) == "YES" ? "true" : "false";
				$deleteArray[] = $userId;
				unset($userInfo);
				require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getUserInfo.php");

				// Check for Voice Messaging User service assignment
				$userServices = getUserAssignedUserServices($userId);

				//delete Voice Mailbox account from SurgeMail Server
				if ($policiesSupportDeleteVMOnUserDel == "true" && in_array("Voice Messaging User", $userServices)) {

					$xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetAdvancedVoiceManagementRequest14sp3");
					$xmlinput .= "<userId>" . $userId . "</userId>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
					readError($xml);

					$groupMailServerUserId = isset($xml->command->groupMailServerUserId) ? strval($xml->command->groupMailServerUserId) : "";

					if ($groupMailServerUserId != "") {
						$mailboxPassword = setEmailPassword();
						/*if (strpos($userList, $groupMailServerUserId) !== false) {
							$deleteResult = $surgemailcomm->deleteMailingUser($groupMailServerUserId, $mailboxPassword);

						}*/

						$surgemailPost = array("cmd" => "cmd_user_login",
							"lcmd" => "user_delete",
							"show" => "simple_msg.xml",
							"username" => $surgemailUsername,
							"Lpassword" => $surgemailPassword,
							"lusername" => $groupMailServerUserId,
							"lpassword" => $mailboxPassword,
							"uid" => isset($userid) ? $userid : "");
						//$result = http_post_fields($surgemailURL, $surgemailPost);
						// function to intract with curl operations
						$srgeRsltDelUsr = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPost);
					}
				}

				//delete user
				$xmlinput = xmlHeader($sessionid, "UserDeleteRequest");
				$xmlinput .= "<userId>" . $userId . "</userId>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
				readError($xml);

				//delete device
				if (isset($_SESSION["userInfo"]["deviceName"])) {
					$deviceName = $_SESSION["userInfo"]["deviceName"];
					if ($deviceName != "" && getNumberOfAssignedPorts($deviceName) == 0 && deleteDeviceOnUserRemoval($_SESSION["userInfo"]["deviceType"])) {
						$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceDeleteRequest");
						$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
						$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
						$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
						$xmlinput .= xmlFooter();
						$response = $client->processOCIMessage(array("in0" => $xmlinput));
						$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
						readError($xml);
					}
				}

			} 
                        else if (strtolower($action) == 'modify') {
				$lastName = $value['lastName'];
				$firstName = $value['firstName'];
				$clidLast = $value['callingLineIdLastName'];
				$clidFirst = $value['callingLineIdFirstName'];
				$nameDialingFirst = $value['nameDialingFirstName'];
				$nameDialingLast = $value['nameDialingLastName'];
				$callingLineIdPhoneNumber = $value['callingLineIdPhoneNumber'];
				$ext = $value["extension"];
				$ph = $value["phoneNumber"];
				$emailAddress = $value["emailAddress"];
				$servicePackArrays = explode(";", $value["servicePack"]);
				$userServicesArray = explode(";", $value["userServices"]);
				$voiceMessaging = $value['voiceMessaging'];
				$enableVM = strtoupper($voiceMessaging) == "YES" ? "true" : "false";
				//$enableVM = strtoupper($value['enableVM']) == "YES" ? "true" : "false";
				$addressLocation = $value['addressLocation'];

				$userChange = array("lastName",
					"firstName",
					"callingLineIdLastName",
					"callingLineIdFirstName",
					"phoneNumber",
					"extension",
					"callingLineIdPhoneNumber",
					"emailAddress",
					"networkClassOfService"
				);

				//Update Location only if it is set as visible in the server config.
				if($locationFieldVisible == "true") {
					$userChange[] = "addressLocation";
				}

				//$value will get overwritten by getUserInfo.php, so back it up.
				$row_value = $value;

				unset($userInfo);
				require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getUserInfo.php");
				require_once $_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/UserServiceGetAssignmentList.php";
				require_once $_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/ModifyVMService.php";


				//get the custom tags added in express
				$currentCustomTags = getCurrentCustomTagsForDevice($_POST["sp"], $_POST["groupId"], $userInfo["deviceName"]);
				$current_Express_Tag_Bundle = "";
				$current_Phone_Template = "";
				if(isset($currentCustomTags['%Express_Tag_Bundle%']) && isset($currentCustomTags['%Express_Tag_Bundle%'][0])) {
					$current_Express_Tag_Bundle = $currentCustomTags['%Express_Tag_Bundle%'][0];
				}
				if(isset($currentCustomTags['%phone-template%']) && isset($currentCustomTags['%phone-template%'][0])) {
					$current_Phone_Template = $currentCustomTags['%phone-template%'][0];
				}

				//Build Device
				$sipGateway = false;
				$hasDeviceType = $row_value["deviceType"] != "";
				if(trim($row_value["deviceType"]) == trim($userInfo["deviceType"])){
				    $row_value["deviceName"] = $userInfo["deviceName"];
				    $row_value["linePort"] = $userInfo["linePort"];
				}
				//error_log("Deivce Name: " . $row_value['deviceName']);

				if ($hasDeviceType) {
					// For analog users check whether a device (analog gateway) has already been provisioned
					$sipGatewayLookup = new DBLookup($db, "systemDevices", "deviceType", "phoneType");
					$sipGateway = $sipGatewayLookup->get($row_value["deviceType"]) == "Analog";
					if (!in_array($row_value["deviceName"], $devices)) {
						$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceAddRequest14");
						$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
						$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
						$xmlinput .= "<deviceName>" . $row_value["deviceName"] . "</deviceName>";
						$xmlinput .= "<deviceType>" . $row_value["deviceType"] . "</deviceType>";
						if ($row_value["macAddress"] != "") {
							$xmlinput .= "<macAddress>" . $row_value["macAddress"] . "</macAddress>";
						}
						$xmlinput .= xmlFooter();
						$response = $client->processOCIMessage(array("in0" => $xmlinput));
						$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

						if (count($xml->command->attributes()) > 0) {
							foreach ($xml->command->attributes() as $a => $b) {
								if ($b == "Error" || $b == "Warning") {
									$data = $row_value["deviceName"] . " was attempted to be built but failed.";
									$data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
									if ($xml->command->detail) {
										$data .= "%0D%0A" . strval($xml->command->detail);
									}

									echo "<tr><td colspan='29'>$data</td></tr>";

								}
							}
						}

						// Add Device Custom Tags (user level)
						addDeviceCustomTags($row_value["deviceType"], $row_value["deviceName"], $row_value["customProfile"]);

						// Rebuild and reset device after custom tags are added
						if ($row_value["customProfile"] != "") {
							require_once $_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig.php";
							rebuildAndResetDeviceConfig($row_value["deviceName"], "rebuildAndReset");
							$changeLogs[] = array('field' => 'Reset Device', "old" => "", "new" => "true");

						}
					} else {
						if (trim($userInfo["macAddress"]) != trim($row_value["macAddress"])) {
							$macAddress = trim($row_value["macAddress"]);
							// Change MAC address
							$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyRequest14");
							$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
							$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
							$xmlinput .= "<deviceName>" . $row_value["deviceName"] . "</deviceName>";
							$xmlinput .= $macAddress != "" ? "<macAddress>" . $macAddress . "</macAddress>" : "<macAddress xsi:nil=\"true\" />";
							$xmlinput .= xmlFooter();
							$response = $client->processOCIMessage(array("in0" => $xmlinput));
							$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
							$errorResult = readError($xml);
						}
					}

					$tagsetName = $sysOperations->getTagSetNameByDeviceType($row_value["deviceType"], $ociVersion);

					$systemTagList = array();
					if(count($tagsetName["Success"]) > 0){
						$systemTagListResponse = $sysOperations->getDefaultTagListByTagSet($tagsetName["Success"]);
						$systemTagList = $systemTagListResponse["Success"];
					}

					//error_log(print_r($systemTagList, true));

					//Update %phone-template% & %Express_Custom_Profile_Name%
					removeDeviceTag($_POST["sp"], $_POST["groupId"], $row_value["deviceName"], '%phone-template%');
					removeDeviceTag($_POST["sp"], $_POST["groupId"], $row_value["deviceName"], '%Express_Custom_Profile_Name%');

					//Update Phone Profile
					if($row_value["phoneProfile"] != "") {

						//error_log("Update phoneProfile");
						$phoneProfileValue = getPhoneProfileValue($row_value["phoneProfile"]);

						if($phoneProfileValue != null) {

							if(!isset($systemTagList['%phone-template%']) || $phoneProfileValue != $systemTagList['%phone-template%'][0]){
								addDeviceTag($_POST["sp"], $_POST["groupId"], $row_value["deviceName"], '%phone-template%', $phoneProfileValue);
								addDeviceTag($_POST["sp"], $_POST["groupId"], $row_value["deviceName"], '%Express_Custom_Profile_Name%', $phoneProfileValue);
							}

						}
					}

					//Tag Bundles
					removeDeviceTag($_POST["sp"], $_POST["groupId"], $row_value["deviceName"], '%Express_Tag_Bundle%');

					//Remove all individual tags in the tag bundles.
					$all_tags = getAllUniqueTagsForBundles();
					foreach ($all_tags as $tagName) {

						removeDeviceTag($_POST["sp"], $_POST["groupId"], $row_value["deviceName"], $tagName);

					}

					if(trim($row_value["tagBundles"]) != "" && trim($row_value["tagBundles"]) != "None") {

						//Update %Express_Tag_Bundle% custom tag
						if(!isset($systemTagList['%Express_Tag_Bundle%']) || $row_value["tagBundles"] != $systemTagList['%Express_Tag_Bundle%'][0]) {
							addDeviceTag($_POST["sp"], $_POST["groupId"], $row_value["deviceName"], '%Express_Tag_Bundle%', $row_value["tagBundles"]);
						}

						//Update all individual tags in the bundle
						$tagBundles = explode(";", $row_value["tagBundles"]);

						foreach ($tagBundles as $tagBundle) {
							$tags = getAllTagsforBundle($tagBundle);
							foreach ($tags as $tag) {

								$tagValue = $tag['tagValue'];

								if(!isset($systemTagList[$tag['tag']]) || $tagValue != $systemTagList[$tag['tag']][0]) {
									addDeviceTag($_POST["sp"], $_POST["groupId"], $row_value["deviceName"], $tag['tag'], $tagValue);
								}

							}
						}

					}

					//Num of Line Apperances && Calls Per LineKey
					if($row_value["polycomLineRegNumber"] != "" && $row_value["polycomLineRegNumber"] > 0) {
						if($row_value["numOfLineAppearances"] != "" && $row_value["numOfLineAppearances"] > 0) {

							//error_log("Update numOfLineAppearances");

							//Delete Existing Tag
							removeDeviceTag($_POST["sp"], $_POST["groupId"], $row_value["deviceName"], '%reg' . $row_value["polycomLineRegNumber"] . 'lineKeys%');

							$numOfLineAppearances = $row_value["numOfLineAppearances"];
							// update custom tags from back-end
							if(!isset($systemTagList['%reg' . $row_value["polycomLineRegNumber"] . 'lineKeys%']) || $numOfLineAppearances != $systemTagList['%reg' . $row_value["polycomLineRegNumber"] . 'lineKeys%'][0]) {
								addDeviceTag($_POST["sp"], $_POST["groupId"], $row_value["deviceName"], '%reg' . $row_value["polycomLineRegNumber"] . 'lineKeys%', $numOfLineAppearances);
							}
						}

						if($row_value["callsPerLineKey"] != "" && $row_value["callsPerLineKey"] > 0) {

							//error_log("Update callsPerLineKey");

							//Delete Existing Tag
							removeDeviceTag($_POST["sp"], $_POST["groupId"], $row_value["deviceName"], '%CallsPerLine-' . $row_value["polycomLineRegNumber"] . '%');

							$callsPerLineKey = $row_value["callsPerLineKey"];
							// update custom tags from back-end
							if(!isset($systemTagList['%CallsPerLine-' . $row_value["polycomLineRegNumber"] . '%']) || $callsPerLineKey != $systemTagList['%CallsPerLine-' . $row_value["polycomLineRegNumber"] . '%'][0]) {
								addDeviceTag($_POST["sp"], $_POST["groupId"], $row_value["deviceName"], '%CallsPerLine-' . $row_value["polycomLineRegNumber"] . '%', $callsPerLineKey);
							}

						}
					}

					if($row_value["tagBundles"] != $current_Express_Tag_Bundle) {
						$changeLogs[] = array('field' => 'tagBundles', 'new' => $row_value["tagBundles"], 'old' => $current_Express_Tag_Bundle);
					}
					if($row_value["phoneProfile"] != $current_Phone_Template) {
						$changeLogs[] = array('field' => 'phoneProfile', 'new' => $row_value["phoneProfile"], 'old' => $current_Phone_Template);
					}

				}else{
					$deleteArray[] = $userId;
				}

				$userModHeader = xmlHeader($sessionid, "UserModifyRequest17sp4");
				$userModHeader .= "<userId>" . $userId . "</userId>";
				$userValues = "";
				foreach ($userChange as $k) {
					$v = $row_value[$k];
					if ((isset($userInfo[$k]) and $v !== $userInfo[$k]) or !isset($userInfo[$k])) {

						$changeLogs[] = array('field' => $k, 'new' => $v, 'old' => $userInfo[$k]);

						if ($k == "callingLineIdPhoneNumber" and $v == "") {
							$userValues .= "<" . $k . " xsi:nil=\"true\" />";
						} else if (in_array($k, $userChange)) {
							$userValues .= "<" . $k . ">" . $v . "</" . $k . ">";
						}
					}
				}
				$userModFooter = xmlFooter();
				$userMod = $userModHeader . $userValues . $userModFooter;

				$response = $client->processOCIMessage(array("in0" => $userMod));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
				$error = readError($xml);

				//Name Dialing names
				if(($ociVersion=="20" || $ociVersion=="21" || $ociVersion=="22") && $dialingNameFieldsVisible == "true"){
					$userValues = "";
					if(trim($nameDialingLast)  && trim($nameDialingFirst)) {
						$userValues .= "<nameDialingName>";
						if(trim($nameDialingLast)) {
							$userValues .= "<nameDialingLastName>" . htmlspecialchars(trim($nameDialingLast)) . "</nameDialingLastName>";
						}
						if(trim($nameDialingFirst)) {
							$userValues .= "<nameDialingFirstName>" . htmlspecialchars(trim($nameDialingFirst)) . "</nameDialingFirstName>";
						}
						$userValues .= "</nameDialingName>";
					} else {
						if(trim($nameDialingLast) == "" && trim($nameDialingFirst) == "") {
							$userValues .= "<nameDialingName xsi:type=\"NameDialingName\" xsi:nil=\"true\" />";
						}
					}

					$userModHeader = xmlHeader($sessionid, "UserModifyRequest17sp4");
					$userModHeader .= "<userId>" . $userId . "</userId>";
					$userModFooter = xmlFooter();
					$userMod = $userModHeader . $userValues . $userModFooter;
					//error_log($userMod);

					$response = $client->processOCIMessage(array("in0" => $userMod));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
					$error = readError($xml);
				}

				//Activate Number
				if (!empty($row_value["phoneNumber"])) {

					require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
					require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");

					if($userInfo["uActivateNumber"] == "No"){
						$oldValue = "Deactivated";
					}else{
						$oldValue = "Activated";
					}

					if($row_value["uActivateNumber"] == "No"){
						$newValue = "Deactivated";
					}else{
						$newValue = "Activated";
					}

					if ($row_value["phoneNumber"] != $userInfo["phoneNumber"]) {
						if ($row_value["uActivateNumber"] == "Yes") {
							$uDn = new Dns ();
							$dnPostArray = array(
								"sp" => $_POST['sp'],
								"groupId" => $_POST['groupId'],
								"phoneNumber" => array($row_value['phoneNumber'])
							);

							if ($row_value["uActivateNumber"] == "Yes") {
								$dnsActiveResponse = $uDn->activateDNRequest($dnPostArray);

								$changeLogs[] = array("field" => 'uActivateNumber', "old" => $oldValue, "new" => $newValue);
							}
						}
					} else {
						if ($oldValue != $newValue) {
							$uDn = new Dns ();
							$dnPostArray = array(
								"sp" => $_POST['sp'],
								"groupId" => $_POST['groupId'],
								"phoneNumber" => array($row_value['phoneNumber'])
							);
							if ($row_value["uActivateNumber"] == "Yes") {
								$dnsActiveResponse = $uDn->activateDNRequest($dnPostArray);
							} else {
								$dnsActiveResponse = $uDn->deActivateDNRequest($dnPostArray);
							}

							$changeLogs[] = array("field" => 'uActivateNumber', "old" => $oldValue, "new" => $newValue);
						}
					}
				}

				if ($hasDeviceType) {

					$canHavePortNumber = checkDeviceSupports($row_value["deviceType"]) == 'static' ? true : false;

					$userModHeader = xmlHeader($sessionid, "UserModifyRequest17sp4");
					$userModHeader .= "<userId>" . $userId . "</userId>";
					$userValues = "<endpoint>";
					$userValues .= "<accessDeviceEndpoint>";
					$userValues .= "<accessDevice>";
					$userValues .= "<deviceLevel>Group</deviceLevel>";
					$userValues .= "<deviceName>" . $row_value["deviceName"] . "</deviceName>";
					$userValues .= "</accessDevice>";
					$userValues .= "<linePort>" . $row_value["linePort"] . "</linePort>";
					if ($canHavePortNumber && $row_value["portNumber"] > 0) {
						$userValues .= "<portNumber>" . $row_value["portNumber"] . "</portNumber>";
					}
					$userValues .= "</accessDeviceEndpoint>";
					$userValues .= "</endpoint>";

					if (strlen($userInfo["deviceName"]) > 0 && $row_value["deviceName"] != $userInfo["deviceName"]) {
						deleteOlderDevice($userInfo["deviceName"]);
					}


					//echo "$userValues<br/><br/>";
					$userModFooter = xmlFooter();
					$userMod = $userModHeader . $userValues . $userModFooter;

				}

				$response = $client->processOCIMessage(array("in0" => $userMod));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
				$error = readError($xml);

				//Remove Device
				if (!$hasDeviceType && strlen($userInfo["deviceName"]) > 0) {
					removeDeviceFromUser($userId, $userInfo["deviceName"], $userInfo["deviceType"]);
				}

				$initialUserServicesAssignmentList = new UserServiceGetAssignmentList($userId);
				$currentServicePacks = $initialUserServicesAssignmentList->getservicePacks();
				if ($currentServicePacks && is_array($currentServicePacks)) {
					$currentServicePacks = array_keys($currentServicePacks);
				}
                
				/* current assinged service to user*/
				$currentServices = $initialUserServicesAssignmentList->getUserServices();
				
				$removeServicePacks = array();
				$addServicePacks = array();
				$addServices = array();
				$removeServices = array();

				
                foreach ($currentServicePacks as $service_pack) {
					if (!in_array($service_pack, $servicePackArrays)) {
						$removeServicePacks[] = $service_pack;
					}
				}				 
				     				
				foreach ($currentServices as $user_service) {
				    if (!in_array($user_service, $userServicesArray)) {
				        $removeServices[] = $user_service;
				    }
				}
                
				/* Add VM only service pack to remove service pack array */
				if( strtoupper($voiceMessaging) == "NO" ) {
				    $assignedServices = new UserServiceGetAssignmentList($userId);
				        $currentServicePacks = $assignedServices->getservicePacks();
				        foreach ($currentServicePacks as $servicePack => $userServices) {
				                if (in_array("Voice Messaging User", $userServices)) {
				                    if (count($userServices) == 1) {
				                        $removeServicePacks[] = $servicePack;
				                    } else {
				                        $enableVM = "false";
				                    }
				                }
				        }
				        //$removeServices[] = "Voice Messaging User";
				}
				else if ( (!$initialUserServicesAssignmentList->hasService("Voice Messaging User") && strtoupper($voiceMessaging) == "YES") ) {				    
				    $respomseVMService = isVMServicePackOrVMserviceAvailable($servicePackArrays, $userServicesArray, $servicePacks, $userServices);
				    if ( isset($respomseVMService->VMOnlyServicePack) ) {
				        $addServicePacks[] = $respomseVMService->VMOnlyServicePack;
				    } else if ( isset($respomseVMService->VMService) ) {
				        $addServices[] = $respomseVMService->VMService;
				    }
				}
				
				foreach ($userServicesArray as $user_service) {
				    //if (!in_array($user_service, $userServices)) {
				    $addServices[] = $user_service;
				    //}
				}
				
				foreach ($servicePackArrays as $service_pack) {
					if (!in_array($service_pack, $currentServicePacks)) {
						$addServicePacks[] = $service_pack;
					}
				}

				$surgeMailUserId = "";

				if (count($removeServicePacks) || count($removeServices) || ($initialUserServicesAssignmentList->hasService("Voice Messaging User") && strtoupper($voiceMessaging) != "YES")) {

					// get Surge Mail User ID before VM Service will potentially be unassigned.
					if ($initialUserServicesAssignmentList->hasService("Voice Messaging User")) {
						$xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetAdvancedVoiceManagementRequest14sp3");
						$xmlinput .= "<userId>" . $userId . "</userId>";
						$xmlinput .= xmlFooter();
						$response = $client->processOCIMessage(array("in0" => $xmlinput));
						$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
						$surgeMailUserId = isset($xml->command->groupMailServerUserId) ? strval($xml->command->groupMailServerUserId) : "";
					}

					$servicePackRemoveReq = xmlHeader($sessionid, "UserServiceUnassignListRequest");
					$servicePackRemoveReq .= "<userId>" . $userId . "</userId>";
					if ($initialUserServicesAssignmentList->hasService("Voice Messaging User") && strtoupper($voiceMessaging) != "YES") {
						$servicePackRemoveReq .= "<serviceName>" . htmlspecialchars("Voice Messaging User") . "</serviceName>";
					}
					
					foreach ($removeServices as $user_service) {
					    $servicePackRemoveReq .= "<serviceName>" . htmlspecialchars($user_service) . "</serviceName>";
					}
					
					foreach ($removeServicePacks as $servicePack) {
						$servicePackRemoveReq .= "<servicePackName>" . htmlspecialchars($servicePack) . "</servicePackName>";
					}
					$servicePackRemoveReq .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $servicePackRemoveReq));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
					$errorResult = readError($xml);
					if ($errorResult === "Error") {
						foreach ($removeServicePacks as $pack) {
							$message .= "<li style='color:red'>Service Pack: " . $pack . " can not unassigned from the user.</li>";
						}
					}
				}

				if ( (count($addServicePacks) > 0 || count($addServices) > 0) || (!$initialUserServicesAssignmentList->hasService("Voice Messaging User") && strtoupper($voiceMessaging) == "YES")) {

					$servicePackAddReq = xmlHeader($sessionid, "UserServiceAssignListRequest");
					$servicePackAddReq .= "<userId>" . $userId . "</userId>";
					if (!$initialUserServicesAssignmentList->hasService("Voice Messaging User") && strtoupper($voiceMessaging) == "YES") {
						$xmlinput .= "<serviceName>" . htmlspecialchars("Voice Messaging User") . "</serviceName>";
					}
					
					foreach ($addServices as $userService) {
					    $servicePackAddReq .=  "<serviceName>" . htmlspecialchars($userService) . "</serviceName>";
					}
					
					foreach ($addServicePacks as $servicePack) {
						$servicePackAddReq .= "<servicePackName>" . htmlspecialchars($servicePack) . "</servicePackName>";
					}
					$servicePackAddReq .= xmlFooter();

					$response = $client->processOCIMessage(array("in0" => $servicePackAddReq));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

					if (isset($xml->command->summary) && $xml->command->summary != "") {
						foreach ($addServicePacks as $pack) {
							$message .= "<li style='color:red' >Service Pack: " . $pack . " can not assign to user.</li>";
						}
					}

				}

				if (isset($servicePackAddReq) || isset($servicePackRemoveReq)) {
					$finalUserServicesAssignmentList = new UserServiceGetAssignmentList($userId);

					// Process VM service initialization
					if (!$initialUserServicesAssignmentList->hasService("Voice Messaging User") && $finalUserServicesAssignmentList->hasService("Voice Messaging User")) {
						
					    // Build Voice Mail Password. Enforce fallback
					    // ----------------------------------------------------------------------------------------------------
					    if ($bwVoicePortalPasswordType == "Formula") {
					        require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
					        require_once("/var/www/lib/broadsoft/adminPortal/VoicePasswordNameBuilder.php");
					        
					        $voicePasswordBuilder = new VoicePasswordNameBuilder($userVoicePortalPasscodeFormula,
					            getResourceForVoicePasswordCreation(ResourceNameBuilder::DN, $row_value),
					            getResourceForVoicePasswordCreation(ResourceNameBuilder::EXT, $row_value),
					            getResourceForVoicePasswordCreation(ResourceNameBuilder::USR_CLID, $row_value),
					            getResourceForVoicePasswordCreation(ResourceNameBuilder::GRP_DN, $row_value),
					            true);
					        
					        if (!$voicePasswordBuilder->validate()) {
					            // TODO: Implement validation resolution
					        }
					        
					        do {
					            $attr = $voicePasswordBuilder->getRequiredAttributeKey();
					            $voicePasswordBuilder->setRequiredAttribute($attr, getResourceForVoicePasswordCreation($attr, $row_value));
					        } while ($attr != "");
					        
					        $portalPass = $voicePasswordBuilder->getName();
					        //Code added @ 08 May 2019 regarding Ex-1318
					        if(is_numeric($portalPass) && strlen($portalPass) < 6){
					            $portalPass = "476983";
					        }
					        if(!is_numeric($portalPass)){
					            $portalPass = "476983";
					        }
					        //End code
					        
					    } else {
					        $portalPass = setVoicePassword();
					    }
					    if (is_numeric($portalPass)) {
					        $portalPass = $portalPass;
					    } else {
					        $criteria = $userVoicePortalPasscodeFormula;
					        preg_match('#\((.*?)\)#', $criteria, $match);
					        $var = $match[1];
					        $portalPass = "";
					        for ($i = 1; $i <= $var; $i++) {
					            $portalPass .= $i;
					        }
					    }
					    
					    $AuthPass = setAuthPassword();
					    $emailPass = setEmailPassword();
					    $hasDeviceType = $value["deviceType"] != "";
					    
					    // Create surgemail username from the first formula.
					    // Enforce fallback if there is no second criteria/formula.
					    // -------------------------------------------------------------
					    $surgemailName = "";
					    $surgeMailNameBuilder1 = new SurgeMailNameBuilder($surgeMailIdCriteria1,
					        getResourceForSurgeMailNameCreation(ResourceNameBuilder::DN, $row_value),
					        getResourceForSurgeMailNameCreation(ResourceNameBuilder::EXT, $row_value),
					        getResourceForSurgeMailNameCreation(ResourceNameBuilder::USR_CLID, $row_value),
					        getResourceForSurgeMailNameCreation(ResourceNameBuilder::GRP_DN, $row_value),
					        getResourceForSurgeMailNameCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $row_value),
					        getResourceForSurgeMailNameCreation(ResourceNameBuilder::FIRST_NAME, $row_value),
					        getResourceForSurgeMailNameCreation(ResourceNameBuilder::LAST_NAME, $row_value),
					        $surgeMailIdCriteria2 == "");    // force fallback only if second criteria does not exist
					        
					        if (!$surgeMailNameBuilder1->validate()) {
					            // TODO: Implement resolution when User ID input formula has errors
					        }
					        
					        do {
					            $attr = $surgeMailNameBuilder1->getRequiredAttributeKey();
					            $surgeMailNameBuilder1->setRequiredAttribute($attr, getResourceForSurgeMailNameCreation($attr, $row_value));
					        } while ($attr != "");
					        
					        $surgemailName = $surgeMailNameBuilder1->getName();
					        if ($surgemailName == "") {
					            $surgeMailNameBuilder2 = new SurgeMailNameBuilder($surgeMailIdCriteria2,
					                getResourceForSurgeMailNameCreation(ResourceNameBuilder::DN, $row_value),
					                getResourceForSurgeMailNameCreation(ResourceNameBuilder::EXT, $row_value),
					                getResourceForSurgeMailNameCreation(ResourceNameBuilder::USR_CLID, $row_value),
					                getResourceForSurgeMailNameCreation(ResourceNameBuilder::GRP_DN, $row_value),
					                getResourceForSurgeMailNameCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $row_value),
					                getResourceForSurgeMailNameCreation(ResourceNameBuilder::FIRST_NAME, $row_value),
					                getResourceForSurgeMailNameCreation(ResourceNameBuilder::LAST_NAME, $row_value),
					                true);
					            
					            if (!$surgeMailNameBuilder2->validate()) {
					                // TODO: Implement resolution when User ID input formula has errors
					            }
					            
					            do {
					                $attr = $surgeMailNameBuilder2->getRequiredAttributeKey();
					                $surgeMailNameBuilder2->setRequiredAttribute($attr, getResourceForSurgeMailNameCreation($attr, $row_value));
					            } while ($attr != "");
					            
					            $surgemailName = $surgeMailNameBuilder2->getName();
					        }
					        
					        $emailAddress1 = $surgemailName;
					    
						//$emailAddress = getSurgeMailAddress();
						//$passCode = getVoicePortalPassCode();
						$vmService = new ModifyVMService($userId);
						$vmService->add($portalPass, $emailAddress1);
					}
					//initialize BLF SIP URI
					if ( ! $initialUserServicesAssignmentList->hasService("Busy Lamp Field") && $finalUserServicesAssignmentList->hasService("Busy Lamp Field")) {
					    require_once("/var/www/lib/broadsoft/adminPortal/ModifyBLFService.php");
					    $blfModify = new ModifyBLFService($userId, $row_value["deviceName"]);
					}

					// Remove Surge Mail on VM Service removal
					if ($initialUserServicesAssignmentList->hasService("Voice Messaging User") && !$finalUserServicesAssignmentList->hasService("Voice Messaging User")) {
						$vmService = new ModifyVMService($userId);
						$vmService->remove($surgeMailUserId);
					}
				}


				//Enable or disable VM if Voice mail is assigned
				if (!isset($_SESSION["userInfo"]["isActive"]) || $_SESSION["userInfo"]["isActive"] != $enableVM) {

					if (isset($_SESSION["userInfo"]["thirdPartyVoiceMail"]) and $_SESSION["userInfo"]["thirdPartyVoiceMail"] == "true") {
						$voiceManagement = xmlHeader($sessionid, "UserThirdPartyVoiceMailSupportModifyRequest");
					} else {
						$voiceManagement = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
					}
					$voiceManagement .= "<userId>" . $userId . "</userId>";
					$voiceManagement .= "<isActive>" . $enableVM . "</isActive>";
					$voiceManagement .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $voiceManagement));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

					$changeLog[] = array("field" => "isActive", "old" => $_SESSION["userInfo"]["isActive"], "new" => $enableVM);
				}

				if ($hasDeviceType) {

					$canHavePortNumber = checkDeviceSupports($row_value["deviceType"]) == 'static' ? true : false;

					//Set Primary Line
					$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyUserRequest");
					$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
					$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
					$xmlinput .= "<deviceName>" . $row_value["deviceName"] . "</deviceName>";
					$xmlinput .= "<linePort>" . $row_value["linePort"] . "</linePort>";
					if ($canHavePortNumber && $row_value["portNumber"] > 0) {
						$userValues .= "<portNumber>" . $row_value["portNumber"] . "</portNumber>";
					}
					$xmlinput .= "<isPrimaryLinePort>true</isPrimaryLinePort>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

					//Set PLCM password
					if (strstr($row_value["deviceType"], " ")) {
						$substrMenu = explode(" ", $row_value["deviceType"]);
					} else if (strstr($row_value["deviceType"], "_")) {
						$substrMenu = explode("_", $row_value["deviceType"]);
					} else {
						$substrMenu = explode("-", $row_value["deviceType"]);
					}
					$substrMenu = $substrMenu[0];

					$deviceAccessUserName = $row_value["derivedDeviceAccessUserName"];
					$deviceAccessPassword = $row_value["derivedDeviceAccessPassword"];

					if ($row_value["deviceAccessUserName"] !== "" and $row_value["deviceAccessPassword"] !== "") {
						$deviceAccessUserName = $row_value["deviceAccessUserName"];
						$deviceAccessPassword = $row_value["deviceAccessPassword"];
					}
					/*
					else
					{
						$select = "SELECT customCredentials, username, password from customCredentials where deviceManufacturer='" . $substrMenu . "'";
						$result = $db->query($select);
						while ($row = $result->fetch())
						{
							if ($row["customCredentials"] == "y")
							{
								$deviceAccessUserName = $row["username"];
								$deviceAccessPassword = $row["password"];
							}
						}
					}
					 */

					if (isset($deviceAccessUserName) and isset($deviceAccessPassword)) {
						$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyRequest14");
						$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
						$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
						$xmlinput .= "<deviceName>" . $row_value["deviceName"] . "</deviceName>";

						if ($sipGateway && $analogAccessAuthentication) {
							$xmlinput .= "<useCustomUserNamePassword>false</useCustomUserNamePassword>";
						} else {
							$xmlinput .= "<useCustomUserNamePassword>true</useCustomUserNamePassword>";
							$xmlinput .= "<accessDeviceCredentials>
								<userName>" . $deviceAccessUserName . "</userName>
								<password>" . $deviceAccessPassword . "</password>
							</accessDeviceCredentials>";
						}
						$xmlinput .= xmlFooter();
						$response = $client->processOCIMessage(array("in0" => $xmlinput));
						$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
					}
				}


				//Assigning Call Pickup Group to Users
				if (!empty($row_value["callPickupGroup"])) {
					$Obj = new GroupCallPickupUsers($row_value["callPickupGroup"]);
					$gcpExistingUserinGroup = $Obj->getUsersInCallPickupGroup();
					array_push($gcpExistingUserinGroup, $userId);
					$status = $Obj->modifyUserCallPickupGroup($gcpExistingUserinGroup);
					if ($status == "Success") {
						$Obj = new GroupCallPickupUsers($userId);
						$gpName = $Obj->findCallPickupGroupByUsername();
					}

					if($row_value["callPickupGroup"] != $userInfo['usersGroupName']) {
						$changeLog[] = array("field" => "callPickupGroup", "old" => $userInfo["usersGroupName"], "new" => $row_value["callPickupGroup"]);
					}
				}

				$hasPolycomPhoneServices = checkIfUserHasService($userId, 'Polycom Phone Services');
				$userPolycomPhoneServices = getUserPolycomPhoneServiceValues($userId, $row_value["deviceName"]);

				//error_log($userInfo["firstName"] . " : " . print_r($userPolycomPhoneServices, true));

				if($hasPolycomPhoneServices) {
					$hasPolycomPhoneServices = false;
					if(isset($userPolycomPhoneServices) && isset($userPolycomPhoneServices['integration']) && $userPolycomPhoneServices['integration'] == 'true') {
						$hasPolycomPhoneServices = true;
					}
				}

				$currentpolycomPhoneServices = $hasPolycomPhoneServices ? "Yes" : "No";
				$ccd = isset($userPolycomPhoneServices) && isset($userPolycomPhoneServices['ccd']) && $userPolycomPhoneServices['ccd'] != "" ? $userPolycomPhoneServices['ccd'] : "None";

				//error_log("polycomPhoneServices: $currentpolycomPhoneServices - " . $row_value["polycomPhoneServices"]);
				//error_log("CCD : $ccd, " .  $row_value['customContactDirectory']);

				if(strtoupper($row_value["polycomPhoneServices"]) == "YES") {

					//Check if it is a Polycom device
					//if($substrMenu == "Polycom") { //Karl mentioned that we don't need this check anymore.

						// Update only if the user currently doesn't have PPS turned on or if CCD has changed.
						if(strtoupper($currentpolycomPhoneServices) != "YES" || $row_value['customContactDirectory'] != $ccd) {
							//error_log("polycomPhoneServices: Assign");
							$PPS_result = addUserToPolycomPhoneServices($_POST["sp"], $_POST["groupId"], $row_value["userId"], $row_value["deviceName"], $row_value['customContactDirectory']);
							if($PPS_result !== true) {
								echo "<tr><td colspan='30' class='error'>Couldn't Modify Polycom Phone Services for {$row_value["firstName"]} {$row_value["lastName"]}. $PPS_result</td></tr>";
							}
						}
					//}

				} else if(strtoupper($row_value["polycomPhoneServices"]) == "NO" && strtoupper($currentpolycomPhoneServices) != "NO") {

					//error_log("polycomPhoneServices: Unassign");
					removeUserfromPolycomPhoneServices($_POST["sp"], $_POST["groupId"], $row_value["userId"], $row_value["deviceName"]);

				}

				if($row_value["linePort"] != $userInfo['linePort']) {
					$changeLog[] = array("field" => "linePort", "old" => $userInfo["linePort"], "new" => $row_value["linePort"]);
				}
				if($row_value["deviceIndex"] != $userInfo['deviceIndex']) {
					$changeLog[] = array("field" => "deviceIndex", "old" => $userInfo["deviceIndex"], "new" => $row_value["deviceIndex"]);
				}
				if($row_value["deviceName"] != $userInfo['deviceName']) {
					$changeLog[] = array("field" => "deviceName", "old" => $userInfo["deviceName"], "new" => $row_value["deviceName"]);
				}
				if($row_value["portNumber"] != $userInfo['portNumber']) {
					$changeLog[] = array("field" => "portNumber", "old" => $userInfo["portNumber"], "new" => $row_value["portNumber"]);
				}
				if($row_value["deviceType"] != $userInfo['deviceType']) {
					$changeLog[] = array("field" => "deviceType", "old" => $userInfo["deviceType"], "new" => $row_value["deviceType"]);
				}

				$value = $row_value;

			} 
                        else if(strtolower($action) == 'add') {

                             
				$initialPass = setWebPassword((int)$_SESSION["groupRules"]["minPasswordLength"]);
				$voiceMessaging = $value['voiceMessaging'];
				$enableVM = strtoupper($voiceMessaging) == "YES" ? "true" : "false";
				//$enableVM = strtoupper($value['enableVM']) == "YES" ? "true" : "false";
				$addressLocation = $value["addressLocation"];

				// Build Voice Mail Password. Enforce fallback
				// ----------------------------------------------------------------------------------------------------
				if ($bwVoicePortalPasswordType == "Formula") {
					require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
					require_once("/var/www/lib/broadsoft/adminPortal/VoicePasswordNameBuilder.php");

					$voicePasswordBuilder = new VoicePasswordNameBuilder($userVoicePortalPasscodeFormula,
						getResourceForVoicePasswordCreation(ResourceNameBuilder::DN, $value),
						getResourceForVoicePasswordCreation(ResourceNameBuilder::EXT, $value),
						getResourceForVoicePasswordCreation(ResourceNameBuilder::USR_CLID, $value),
						getResourceForVoicePasswordCreation(ResourceNameBuilder::GRP_DN, $value),
						true);

					if (!$voicePasswordBuilder->validate()) {
						// TODO: Implement validation resolution
					}

					do {
						$attr = $voicePasswordBuilder->getRequiredAttributeKey();
						$voicePasswordBuilder->setRequiredAttribute($attr, getResourceForVoicePasswordCreation($attr, $value));
					} while ($attr != "");

					$portalPass = $voicePasswordBuilder->getName();
                                        //Code added @ 08 May 2019 regarding Ex-1318
                                        if(is_numeric($portalPass) && strlen($portalPass) < 6){
                                            $portalPass = "476983";
                                        }
                                        if(!is_numeric($portalPass)){
                                            $portalPass = "476983";
                                        }
                                        //End code
				} else {
					$portalPass = setVoicePassword();
				}
				if (is_numeric($portalPass)) {
					$portalPass = $portalPass;
				} else {
					$criteria = $userVoicePortalPasscodeFormula;
					preg_match('#\((.*?)\)#', $criteria, $match);
					$var = $match[1];
					$portalPass = "";
					for ($i = 1; $i <= $var; $i++) {
						$portalPass .= $i;
					}
				}

				$AuthPass = setAuthPassword();
				$emailPass = setEmailPassword();
				$hasDeviceType = $value["deviceType"] != "";

				// Create surgemail username from the first formula.
				// Enforce fallback if there is no second criteria/formula.
				// -------------------------------------------------------------
				$surgemailName = "";
				$surgeMailNameBuilder1 = new SurgeMailNameBuilder($surgeMailIdCriteria1,
					getResourceForSurgeMailNameCreation(ResourceNameBuilder::DN, $value),
					getResourceForSurgeMailNameCreation(ResourceNameBuilder::EXT, $value),
					getResourceForSurgeMailNameCreation(ResourceNameBuilder::USR_CLID, $value),
					getResourceForSurgeMailNameCreation(ResourceNameBuilder::GRP_DN, $value),
					getResourceForSurgeMailNameCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $value),
					getResourceForSurgeMailNameCreation(ResourceNameBuilder::FIRST_NAME, $value),
					getResourceForSurgeMailNameCreation(ResourceNameBuilder::LAST_NAME, $value),
					$surgeMailIdCriteria2 == "");    // force fallback only if second criteria does not exist

				if (!$surgeMailNameBuilder1->validate()) {
					// TODO: Implement resolution when User ID input formula has errors
				}

				do {
					$attr = $surgeMailNameBuilder1->getRequiredAttributeKey();
					$surgeMailNameBuilder1->setRequiredAttribute($attr, getResourceForSurgeMailNameCreation($attr, $value));
				} while ($attr != "");

				$surgemailName = $surgeMailNameBuilder1->getName();
				if ($surgemailName == "") {
					$surgeMailNameBuilder2 = new SurgeMailNameBuilder($surgeMailIdCriteria2,
						getResourceForSurgeMailNameCreation(ResourceNameBuilder::DN, $value),
						getResourceForSurgeMailNameCreation(ResourceNameBuilder::EXT, $value),
						getResourceForSurgeMailNameCreation(ResourceNameBuilder::USR_CLID, $value),
						getResourceForSurgeMailNameCreation(ResourceNameBuilder::GRP_DN, $value),
						getResourceForSurgeMailNameCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN, $value),
						getResourceForSurgeMailNameCreation(ResourceNameBuilder::FIRST_NAME, $value),
						getResourceForSurgeMailNameCreation(ResourceNameBuilder::LAST_NAME, $value),
						true);

					if (!$surgeMailNameBuilder2->validate()) {
						// TODO: Implement resolution when User ID input formula has errors
					}

					do {
						$attr = $surgeMailNameBuilder2->getRequiredAttributeKey();
						$surgeMailNameBuilder2->setRequiredAttribute($attr, getResourceForSurgeMailNameCreation($attr, $value));
					} while ($attr != "");

					$surgemailName = $surgeMailNameBuilder2->getName();
				}

				$emailAddress = $surgemailName;

				//Build Device
				$sipGateway = false;

				if ($hasDeviceType) {
					// For analog users check whether a device (analog gateway) has already been provisioned
					$sipGatewayLookup = new DBLookup($db, "systemDevices", "deviceType", "phoneType");
					$sipGateway = $sipGatewayLookup->get($value["deviceType"]) == "Analog";
					if (!$sipGateway || !in_array($value["deviceName"], $devices)) {
						$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceAddRequest14");
						$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
						$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
						$xmlinput .= "<deviceName>" . $value["deviceName"] . "</deviceName>";
						$xmlinput .= "<deviceType>" . $value["deviceType"] . "</deviceType>";
						if ($value["macAddress"] != "") {
							$xmlinput .= "<macAddress>" . $value["macAddress"] . "</macAddress>";
						}
						$xmlinput .= xmlFooter();
						$response = $client->processOCIMessage(array("in0" => $xmlinput));
						$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

						// Add Device Custom Tags (user level)
						addDeviceCustomTags($value["deviceType"], $value["deviceName"], $value["customProfile"]);

						// Rebuild and reset device after custom tags are added
						if ($value["customProfile"] != "") {
							require_once $_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig.php";
							rebuildAndResetDeviceConfig($value["deviceName"], "rebuildAndReset");
							$changeLogs[] = array('field' => 'Reset Device', "old" => "", "new" => "true");
						}
					}

					$tagsetName = $sysOperations->getTagSetNameByDeviceType($value["deviceType"], $ociVersion);

					$systemTagList = array();
					if(count($tagsetName["Success"]) > 0){
						$systemTagListResponse = $sysOperations->getDefaultTagListByTagSet($tagsetName["Success"]);
						$systemTagList = $systemTagListResponse["Success"];
					}

					//Update Phone Profile
					if($value["phoneProfile"] != "") {

						//error_log("Update phoneProfile");
						$phoneProfileValue = getPhoneProfileValue($value["phoneProfile"]);

						if($phoneProfileValue != null) {

							//Update %phone-template% & %Express_Custom_Profile_Name%
							if(!isset($systemTagList['%phone-template%']) || $phoneProfileValue != $systemTagList['%phone-template%'][0]){
								addDeviceTag($_POST["sp"], $_POST["groupId"], $value["deviceName"], '%phone-template%', $phoneProfileValue);
								addDeviceTag($_POST["sp"], $_POST["groupId"], $value["deviceName"], '%Express_Custom_Profile_Name%', $phoneProfileValue);
							}

						}
					}

					//Tag Bundles
					if(trim($value["tagBundles"]) != "" && trim($value["tagBundles"]) != "None") {

						//error_log("Update tagBundles");

						//Update %Express_Tag_Bundle% custom tag
						removeDeviceTag($_POST["sp"], $_POST["groupId"], $value["deviceName"], '%Express_Tag_Bundle%');
						if(!isset($systemTagList['%Express_Tag_Bundle%']) || $value["tagBundles"] != $systemTagList['%Express_Tag_Bundle%'][0]) {
							addDeviceTag($_POST["sp"], $_POST["groupId"], $value["deviceName"], '%Express_Tag_Bundle%', $value["tagBundles"]);
						}

						$all_tags = getAllUniqueTagsForBundles();
						foreach ($all_tags as $tagName) {

							removeDeviceTag($_POST["sp"], $_POST["groupId"], $value["deviceName"], $tagName);
							//error_log("Delete Tag: $tagName");

						}

						//Update all individual tags in the bundle
						$tagBundles = explode(";", $value["tagBundles"]);

						foreach ($tagBundles as $tagBundle) {
							$tags = getAllTagsforBundle($tagBundle);
							foreach ($tags as $tag) {

								$tagValue = $tag['tagValue'];

								if(!isset($systemTagList[$tag['tag']]) || $tagValue != $systemTagList[$tag['tag']][0]) {
									addDeviceTag($_POST["sp"], $_POST["groupId"], $value["deviceName"], $tag['tag'], $tagValue);
								}

							}
						}
					}

					//Num of Line Apperances && Calls Per LineKey
					if($value["polycomLineRegNumber"] != "" && $value["polycomLineRegNumber"] > 0) {
						if($value["numOfLineAppearances"] != "" && $value["numOfLineAppearances"] > 0) {

							//error_log("Update numOfLineAppearances");

							//Delete Existing Tag
							removeDeviceTag($_POST["sp"], $_POST["groupId"], $value["deviceName"], '%reg' . $value["polycomLineRegNumber"] . 'lineKeys%');

							$numOfLineAppearances = $value["numOfLineAppearances"];
							// update custom tags from back-end
							if(!isset($systemTagList['%reg' . $value["polycomLineRegNumber"] . 'lineKeys%']) || $numOfLineAppearances != $systemTagList['%reg' . $value["polycomLineRegNumber"] . 'lineKeys%'][0]) {
								addDeviceTag($_POST["sp"], $_POST["groupId"], $value["deviceName"], '%reg' . $value["polycomLineRegNumber"] . 'lineKeys%', $numOfLineAppearances);
							}
						}

						if($value["callsPerLineKey"] != "" && $value["callsPerLineKey"] > 0) {

							//error_log("Update callsPerLineKey");

							//Delete Existing Tag
							removeDeviceTag($_POST["sp"], $_POST["groupId"], $value["deviceName"], '%CallsPerLine-' . $value["polycomLineRegNumber"] . '%');

							$callsPerLineKey = $value["callsPerLineKey"];
							// update custom tags from back-end
							if(!isset($systemTagList['%CallsPerLine-' . $value["polycomLineRegNumber"] . '%']) || $callsPerLineKey != $systemTagList['%CallsPerLine-' . $value["polycomLineRegNumber"] . '%'][0]) {
								addDeviceTag($_POST["sp"], $_POST["groupId"], $value["deviceName"], '%CallsPerLine-' . $value["polycomLineRegNumber"] . '%', $callsPerLineKey);
							}

						}
					}

					require_once $_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig.php";
					rebuildAndResetDeviceConfig($value["deviceName"], "rebuildAndReset");
					$changeLogs[] = array('field' => 'Reset Device', "old" => "", "new" => "true");
				}

				//Build User
				$xmlinput = xmlHeader($sessionid, "UserAddRequest17sp4");
				$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
				$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
				$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
				$xmlinput .= "<lastName>" . $value["lastName"] . "</lastName>";
				$xmlinput .= "<firstName>" . $value["firstName"] . "</firstName>";
				$xmlinput .= "<callingLineIdLastName>" . $value["callingLineIdLastName"] . "</callingLineIdLastName>";
				$xmlinput .= "<callingLineIdFirstName>" . $value["callingLineIdFirstName"] . "</callingLineIdFirstName>";

				//Name Dialing names
				if(($ociVersion=="20" || $ociVersion=="21" || $ociVersion=="22") && $dialingNameFieldsVisible == "true"){
					$nameDialingFirst = $value["nameDialingFirstName"];
					$nameDialingLast = $value["nameDialingLastName"];

					if(trim($nameDialingLast)  && trim($nameDialingFirst)) {
						$xmlinput .= "<nameDialingName>";
						if(trim($nameDialingLast)) {
							$xmlinput .= "<nameDialingLastName>" . htmlspecialchars(trim($nameDialingLast)) . "</nameDialingLastName>";
						}
						if(trim($nameDialingFirst)) {
							$xmlinput .= "<nameDialingFirstName>" . htmlspecialchars(trim($nameDialingFirst)) . "</nameDialingFirstName>";
						}
						$xmlinput .= "</nameDialingName>";
					}
				}

				if (strlen($value["phoneNumber"]) > 0) {
					$xmlinput .= "<phoneNumber>" . $value["phoneNumber"] . "</phoneNumber>";
				}
				$xmlinput .= "<extension>" . $value["extension"] . "</extension>";
				if (strlen($value["callingLineIdPhoneNumber"]) > 0) {
					$xmlinput .= "<callingLineIdPhoneNumber>" . $value["callingLineIdPhoneNumber"] . "</callingLineIdPhoneNumber>";
				}
				$xmlinput .= "<password>" . $initialPass . "</password>";
				if (strlen($value["department"]) > 0) {
					$xmlinput .= "<department xsi:type=\"GroupDepartmentKey\">
								<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>
								<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>
								<name>" . $value["department"] . "</name>
							</department>";
				}
				if (strlen($value["language"]) > 0) {
					$xmlinput .= "<language>" . $value["language"] . "</language>";
				}
				$xmlinput .= "<timeZone>" . $value["timeZone"] . "</timeZone>";
				if ($hasDeviceType) {
					$canHavePortNumber = checkDeviceSupports($value["deviceType"]) == 'static' ? true : false;
					$xmlinput .= "<accessDeviceEndpoint>";
					$xmlinput .= "<accessDevice>";
					$xmlinput .= "<deviceLevel>Group</deviceLevel>";
					$xmlinput .= "<deviceName>" . $value["deviceName"] . "</deviceName>";
					$xmlinput .= "</accessDevice>";
					$xmlinput .= "<linePort>" . $value["linePort"] . "</linePort>";
					if ($canHavePortNumber && $value["portNumber"] > 0) {
						$xmlinput .= "<portNumber>" . $value["portNumber"] . "</portNumber>";
					}
					$xmlinput .= "</accessDeviceEndpoint>";
				}
				if (strlen($value["emailAddress"]) > 0) {
					$xmlinput .= "<emailAddress>" . $value["emailAddress"] . "</emailAddress>";
				}
				if($locationFieldVisible == "true") {
					if (strlen($value["addressLocation"]) > 0) {
						$xmlinput .= "<addressLocation>" . $value["addressLocation"] . "</addressLocation>";
					}
				}
				if (strlen($value["addressLine1"]) > 0 or strlen($value["addressLine2"]) > 0 or strlen($value["city"]) > 0 or strlen($value["state"]) > 0 or strlen($value["zip"]) > 0) {
					$xmlinput .= "<address>";
					if (strlen($value["addressLine1"]) > 0) {
						$xmlinput .= "<addressLine1>" . $value["addressLine1"] . "</addressLine1>";
					}
					if (strlen($value["addressLine2"]) > 0) {
						$xmlinput .= "<addressLine2>" . $value["addressLine2"] . "</addressLine2>";
					}
					if (strlen($value["city"]) > 0) {
						$xmlinput .= "<city>" . $value["city"] . "</city>";
					}
					if (strlen($value["state"]) > 0) {
						$xmlinput .= "<stateOrProvince>" . $value["state"] . "</stateOrProvince>";
					}
					if (strlen($value["zip"]) > 0) {
						$xmlinput .= "<zipOrPostalCode>" . $value["zip"] . "</zipOrPostalCode>";
					}
					$xmlinput .= "</address>";
				}
				if (strlen($value["networkClassOfService"]) > 0) {
					$xmlinput .= "<networkClassOfService>" . $value["networkClassOfService"] . "</networkClassOfService>";
				}
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
				readError($xml);

				//set call processing policy
				$xmlinput = xmlHeader($sessionid, "UserCallProcessingModifyPolicyRequest14sp7");
				$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
				$xmlinput .= "<useUserCLIDSetting>" . (strtoupper($value["callingLineIdPolicy"]) == "USER" ? "true" : "false") . "</useUserCLIDSetting>";
				$xmlinput .= "<clidPolicy>" . (strtoupper($value["callingLineIdPolicy"]) == "USER" ? "Use Configurable CLID" : "Use DN") . "</clidPolicy>";
				$xmlinput .= "<emergencyClidPolicy>" . (strtoupper($value["callingLineIdPolicy"]) == "USER" ? "Use Configurable CLID" : "Use DN") . "</emergencyClidPolicy>";
				$xmlinput .= "<useGroupName>true</useGroupName>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
				readError($xml);

				//Assign Service Pack
				$xmlinput = xmlHeader($sessionid, "UserServiceAssignListRequest");
				$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
				//$xmlinput .= "<serviceName>Speed Dial 100</serviceName>";
				//$xmlinput .= "<serviceName>Speed Dial 8</serviceName>";
				$servicePackArray  = explode(";", $value["servicePack"]);
				$userServicesArray = explode(";", $value["userServices"]);
				//$addServices = array();
				if (strtoupper($voiceMessaging) == "YES") {
				    $responseVMService = isVMServicePackOrVMserviceAvailable($servicePackArray, $userServicesArray, $servicePacks, $userServices);
				    if ( isset($responseVMService->VMOnlyServicePack) ) {
				        $servicePackArray[] = $responseVMService->VMOnlyServicePack;
				    } else if ( isset($responseVMService->VMService) ) {
				        $userServicesArray[] = $responseVMService->VMService;
				    }
					$servicePackRemoveReq .= "<serviceName>" . htmlspecialchars("Voice Messaging User") . "</serviceName>";
				}
                                
                                //Code added @ 03 June 2019 regarding EX-1345
                                $serviceList = array();                                
                                if(!empty($servicePackArray)){                                        
                                        for ($p = 0; $p < count($servicePackArray); $p++){ 
                                                    $servicePackName = htmlspecialchars($servicePackArray[$p]);
                                                    if(trim($servicePackName) != ""){
                                                    $serviceList = $serviceObj->getTotalServicesListOfServicePacks($servicePackName, $serviceList);
                                                }
                                        }                                   
                                }
                                if(!in_array("Authentication",  $serviceList)){                        
                                    $xmlinput .= "<serviceName>Authentication</serviceName>";
                                }
                                
                                foreach( $userServicesArray as $userService) {
                                    $xmlinput .= "<serviceName>" . $userService . "</serviceName>";
                                }
                                //End code
                                if(!empty($servicePackArray)){                                    
                                    for ($i = 0; $i < count($servicePackArray); $i++) {
                                        $servicePackName1 =  htmlspecialchars($servicePackArray[$i]);
                                        if(trim($servicePackName1) != ""){
                                            $xmlinput .= "<servicePackName>".$servicePackName1."</servicePackName>";
                                        }
                                    }
                                }
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

				//Enable VM if Voice mail is assigned
				$voiceManagement = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
				$voiceManagement .= "<userId>" . $userId . "</userId>";
				$voiceManagement .= "<isActive>" . $enableVM . "</isActive>";
				$voiceManagement .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $voiceManagement));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

				//Build BLF URI
				$xmlinput = xmlHeader($sessionid, "UserBusyLampFieldModifyRequest");
				$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
				$xmlinput .= "<listURI>" . $value["blfUser"] . "</listURI>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

				if ($hasDeviceType) {
					//Set Primary Line
					$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyUserRequest");
					$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
					$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
					$xmlinput .= "<deviceName>" . $value["deviceName"] . "</deviceName>";
					$xmlinput .= "<linePort>" . $value["linePort"] . "</linePort>";
					$xmlinput .= "<isPrimaryLinePort>true</isPrimaryLinePort>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

					//Set PLCM password
					if (strstr($value["deviceType"], " ")) {
						$substrMenu = explode(" ", $value["deviceType"]);
					} else if (strstr($value["deviceType"], "_")) {
						$substrMenu = explode("_", $value["deviceType"]);
					} else {
						$substrMenu = explode("-", $value["deviceType"]);
					}
					$substrMenu = $substrMenu[0];

					$deviceAccessUserName = $value["derivedDeviceAccessUserName"];
					$deviceAccessPassword = $value["derivedDeviceAccessPassword"];

					if ($value["deviceAccessUserName"] !== "" and $value["deviceAccessPassword"] !== "") {
						$deviceAccessUserName = $value["deviceAccessUserName"];
						$deviceAccessPassword = $value["deviceAccessPassword"];
					}
					/*
					else
					{
						$select = "SELECT customCredentials, username, password from customCredentials where deviceManufacturer='" . $substrMenu . "'";
						$result = $db->query($select);
						while ($row = $result->fetch())
						{
							if ($row["customCredentials"] == "y")
							{
								$deviceAccessUserName = $row["username"];
								$deviceAccessPassword = $row["password"];
							}
						}
					}
					 */

					if (isset($deviceAccessUserName) and isset($deviceAccessPassword)) {
						$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyRequest14");
						$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
						$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
						$xmlinput .= "<deviceName>" . $value["deviceName"] . "</deviceName>";

						if ($sipGateway && $analogAccessAuthentication) {
							$xmlinput .= "<useCustomUserNamePassword>false</useCustomUserNamePassword>";
						} else {
							$xmlinput .= "<useCustomUserNamePassword>true</useCustomUserNamePassword>";
							$xmlinput .= "<accessDeviceCredentials>
								<userName>" . $deviceAccessUserName . "</userName>
								<password>" . $deviceAccessPassword . "</password>
							</accessDeviceCredentials>";
						}
						$xmlinput .= xmlFooter();
						$response = $client->processOCIMessage(array("in0" => $xmlinput));
						$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
					}
				}

				//Reason - $value["enableVM"] is not coming with Yes and according to above code if $value['voiceMessaging'] = yes then setting $enableVM = true so We this conidtion if($enableVM == "true") 
				//if (strtoupper($value["enableVM"]) == "YES")  //Commented
				if ($enableVM == "true") {
					//Build front end VM
					$xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
					$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
					$xmlinput .= "<isActive>true</isActive>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
					//Build VM password
					$xmlinput = xmlHeader($sessionid, "UserPortalPasscodeModifyRequest");
					$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
					$xmlinput .= "<newPasscode>" . $portalPass . "</newPasscode>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

					//Configure email account
					$xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyAdvancedVoiceManagementRequest");
					$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
					$xmlinput .= "<groupMailServerEmailAddress>" . $emailAddress . "</groupMailServerEmailAddress>";
					$xmlinput .= "<groupMailServerUserId>" . $emailAddress . "</groupMailServerUserId>";
					$xmlinput .= "<groupMailServerPassword>" . $emailPass . "</groupMailServerPassword>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

					$xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
					$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
					$xmlinput .= "<isActive>true</isActive>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
					/*if (strpos($userList, $emailAddress) !== false) {
						$deleteResult = $surgemailcomm->deleteMailingUser($emailAddress, $emailPass);
					}
					$createResult = $surgemailcomm->createMailingUser($emailAddress, $emailPass);*/

					/*$surgemailPost = array("cmd" => "cmd_user_login",
						"lcmd" => "user_delete",
						"show" => "simple_msg.xml",
						"username" => $surgemailUsername,
						"password" => $surgemailPassword,
						"lusername" => $emailAddress,
						"lpassword" => $emailPass,
						"uid" => isset($userid) ? $userid : "");
					$result = http_post_fields($surgemailURL, $surgemailPost);*/
					$responseData = "";
					$responseData1 = "";
					$responseData2 = "";
					$responseData3 = "";
					$surgemailPost = array("cmd" => "cmd_user_login",
						"lcmd" => "user_create",
						"show" => "simple_msg.xml",
						"username" => $surgemailUsername,
						"password" => $surgemailPassword,
						"lusername" => $emailAddress,
						"lpassword" => $emailPass,
						"uid" => isset($userid) ? $userid : "");
					
					try {
						$responseData = Httpful\Request::post($surgemailURL)
						->body($surgemailPost, Httpful\Mime::FORM)
						->send();
						
						if(strpos($responseData,"Login failed") !== false) {
							$responseData = "LoginFailedError";
							$srgeRsltDelUsr = "Login failed";
						}
					}
					catch(\Exception $e) {
						$responseData= "ServerNotFoundError";
						$srgeRsltDelUsr = "Could Not Connect to the Server";
					}
					
					if(strpos($responseData,"already exists") !== false){
					    $surgemailPostDelete = array("cmd" => "cmd_user_login",
					        "lcmd" => "user_delete",
					        "show" => "simple_msg.xml",
					        "username" => $surgemailUsername,
					        "password" => $surgemailPassword,
					        "lusername" => $emailAddress,
					        "lpassword" => $emailPass,
					        "uid" => isset($userid) ? $userid : "");
					    
					    $responseData1 = Httpful\Request::post($surgemailURL)
					    ->body($surgemailPostDelete, Httpful\Mime::FORM)
					    ->send();
					    					    
					    if(strpos($responseData1, "Account deleted") !== false){
					        $surgemailPostCreate = array("cmd" => "cmd_user_login",
					            "lcmd" => "user_create",
					            "show" => "simple_msg.xml",
					            "username" => $surgemailUsername,
					            "password" => $surgemailPassword,
					            "lusername" => $emailAddress,
					            "lpassword" => $emailPass,
					            "uid" => isset($userid) ? $userid : "");
					        
					        $responseData2 = Httpful\Request::post($surgemailURL)
					        ->body($surgemailPostCreate, Httpful\Mime::FORM)
					        ->send();
					        if(strpos($responseData2, "User saved")){
					            $responseData3 = "Success";
					        }else{
					            $responseData3 = "Error";
					        }
					        
					    }else{
					        $responseData1 = "Error";
					    }
					    
					}
					
					if($responseData1 == "Error" || $responseData3 == "Error" || $responseData == "ServerNotFoundError" || $responseData == "LoginFailedError"){
					    $errorneousDataArray[$arrayIndexCount]["userId"] = $value["userId"];
					    $errorneousDataArray[$arrayIndexCount]["errorMessage"] = "Surgemail account creation failed ".", ".$srgeRsltDelUsr;
					    $errorneousDataArray[$arrayIndexCount]["deviceName"] = $value["deviceName"];
					    $errorneousDataArray[$arrayIndexCount]["errorInModule"] = "Surgemail";
					    $errorneousDataArray[$arrayIndexCount]["phoneNumber"] = $value["phoneNumber"];
					    $errorneousDataArray[$arrayIndexCount]["extension"] = $value["extension"];
					    $errorneousDataArray[$arrayIndexCount]["firsName"] = $value["firstName"];
					    $errorneousDataArray[$arrayIndexCount]["lastName"] = $value["lastName"];
					    $errorneousDataArray[$arrayIndexCount]["action"] = $action;
					    $arrayIndexCount++;
					    $errorFlag = 1;
					    continue;
					}
					
					//$result = http_post_fields($surgemailURL, $surgemailPost);
					//$srgeRsltDelUsr = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPost);
					//print_r($srgeRsltDelUsr);
					//$testData = 'cmd='.$surgemailPost['cmd'].'&lcmd='.$surgemailPost['lcmd'].'&show='.$surgemailPost['show'].'&username='.$surgemailPost['username'].'&password='.$surgemailPost['password'].'&lusername='.$surgemailPost['lusername'].'&lpassword='.$surgemailPost['lpassword'].'&uid='.$surgemailPost['uid'];
					//print_r($testData);
					/*$responseData = \Httpful\Request::post($surgemailURL)
					->method(Http::POST)        // Alternative to Request::post
					->withoutStrictSsl()        // Ease up on some of the SSL checks
					->expectsJson()             // Expect HTML responses
					->sendsType(Mime::FORM)
					->body()
					->send();
					
					var_dump($responseData);*/
					
					/*$surgemailPost = array("cmd" => "cmd_user_login",
					    "lcmd" => "user_delete",
					    "show" => "simple_msg.xml",
					    "username" => $surgemailUsername,
					    "password" => $surgemailPassword,
					    "lusername" => $emailAddress,
					    "lpassword" => $emailPass,
					    "uid" => isset($userid) ? $userid : "");
					//$result = http_post_fields($surgemailURL, $surgemailPost);
					$srgeRsltDelUsr2 = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPost);
					print_r($srgeRsltDelUsr2);
					
					$surgemailPost = array("cmd" => "cmd_user_login",
					    "lcmd" => "user_create",
					    "show" => "simple_msg.xml",
					    "username" => $surgemailUsername,
					    "password" => $surgemailPassword,
					    "lusername" => $emailAddress,
					    "lpassword" => $emailPass,
					    "uid" => isset($userid) ? $userid : "");
					//$result = http_post_fields($surgemailURL, $surgemailPost);
					$srgeRsltDelUsr3 = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPost);
					print_r($srgeRsltDelUsr3);*/
					
					
					/*if($srgeRsltDelUsr != "200"){
					    
					    if( strpos( $srgeRsltDelUsr, 'version="2.0"' ) !== false) {
					         $xmlVal = new SimpleXMLElement($srgeRsltDelUsr);
					         $srgeRsltDelUsr = strval($xmlVal->response);
					    }
					    
					   // $xmlVal = new SimpleXMLElement($srgeRsltDelUsr);
					    //$srgeRsltDelUsr = strval($xmlVal->response);
					    $errorneousDataArray[$arrayIndexCount]["userId"] = $value["userId"];
					    $errorneousDataArray[$arrayIndexCount]["errorMessage"] = "Surgemail account creation failed ".", ".$srgeRsltDelUsr;
					    $errorneousDataArray[$arrayIndexCount]["deviceName"] = $value["deviceName"];
					    $errorneousDataArray[$arrayIndexCount]["errorInModule"] = "Surgemail";
					    $errorneousDataArray[$arrayIndexCount]["phoneNumber"] = $value["phoneNumber"];
					    $errorneousDataArray[$arrayIndexCount]["extension"] = $value["extension"];
					    $errorneousDataArray[$arrayIndexCount]["firsName"] = $value["firstName"];
					    $errorneousDataArray[$arrayIndexCount]["lastName"] = $value["lastName"];
					    $errorneousDataArray[$arrayIndexCount]["action"] = $action;
					    $arrayIndexCount++;
					    $errorFlag = 1;
					    continue;
					}*/
				}


				if (strtoupper($value["polycomPhoneServices"]) == "YES") {

					//Check if it is a Polycom device
					//if ($substrMenu == "Polycom") { //Karl mentioned that we don't need this check anymore.
						//error_log("polycomPhoneServices: Assign");
						$PPS_result = addUserToPolycomPhoneServices($_POST["sp"], $_POST["groupId"], $value["userId"], $value["deviceName"], $value['customContactDirectory']);
						if($PPS_result !== true) {
							echo "<tr><td colspan='30' class='error'>Couldn't add Polycom Phone Services for {$row_value["firstName"]} {$row_value["lastName"]}. $PPS_result</td></tr>";
						}
					//}
				}


				$uidWithDomain = explode("@", $value["userId"]);
				//Auth Settings
				$xmlinput = xmlHeader($sessionid, "UserAuthenticationModifyRequest");
				$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
				//$xmlinput .= "<userName>" . $value["deviceName"] . "</userName>";
				$xmlinput .= "<userName>" . $uidWithDomain[0] . "</userName>";
				$xmlinput .= "<newPassword>" . $AuthPass . "</newPassword>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

				//Turn off international
				/*if ($usrAllowInternationalCalls == "false") {
					$xmlinput = xmlHeader($sessionid, "UserOutgoingCallingPlanOriginatingModifyRequest");
					$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
					$xmlinput .= "<useCustomSettings>true</useCustomSettings>";
					$xmlinput .= "<userPermissions>";
					$xmlinput .= "<international>Disallow</international>";
					$xmlinput .= "</userPermissions>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
				}*/

				//Assigning Call Pickup Group to Users
				if (!empty($value["callPickupGroup"])) {
					$Obj = new GroupCallPickupUsers($value["callPickupGroup"]);
					$gcpExistingUserinGroup = $Obj->getUsersInCallPickupGroup();
					array_push($gcpExistingUserinGroup, $value["userId"]);
					$status = $Obj->modifyUserCallPickupGroup($gcpExistingUserinGroup);
					if ($status == "Success") {
						$Obj = new GroupCallPickupUsers($value["userId"]);
						$gpName = $Obj->findCallPickupGroupByUsername();
					}
				}

				//Activate Number
				if (!empty($value["phoneNumber"]) && $value["uActivateNumber"] == "Yes") {

					require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
					require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");

					$uDn = new Dns ();
					$dnPostArray = array(
						"sp" => $_POST['sp'],
						"groupId" => $_POST['groupId'],
						"phoneNumber" => array($value['phoneNumber'])
					);
					$dnsActiveResponse = $uDn->activateDNRequest($dnPostArray);
				}

			}


			if(isset($_POST["customTags"][$userId])) {

				//error_log(print_r($_POST["customTags"][$userId], true));

				if( isset($_POST["customTags"][$userId]['delete']) ) {
					foreach ($_POST["customTags"][$userId]['delete'] as $tagName) {
						removeDeviceTag($_POST["sp"], $_POST["groupId"], $value["deviceName"], $tagName);
					}
				}

				if( isset($_POST["customTags"][$userId]['modify']) ) {
					foreach ($_POST["customTags"][$userId]['modify'] as $tagName => $tagValue) {

						//Delete Existing Tag
						removeDeviceTag($_POST["sp"], $_POST["groupId"], $value["deviceName"], $tagName);
						// update custom tags
						addDeviceTag($_POST["sp"], $_POST["groupId"], $value["deviceName"], $tagName, $tagValue);
					}
				}
			}



			if (strtolower($action) != 'delete') {
				/*
				------------------------- DONE WITH BUILD---------------------------------
				*/

				//Get basic user info
				if ($ociVersion == "17") {
					$xmlinput = xmlHeader($sessionid, "UserGetRequest17sp4");
				} else if ($ociVersion == "20") {
					$xmlinput = xmlHeader($sessionid, "UserGetRequest20");
				} else {
					$xmlinput = xmlHeader($sessionid, "UserGetRequest19");
				}
				$xmlinput .= "<userId>" . $userId . "</userId>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

				$lastName = strval($xml->command->lastName);
				$firstName = strval($xml->command->firstName);
				$clidLast = strval($xml->command->callingLineIdLastName);
				$clidFirst = strval($xml->command->callingLineIdFirstName);
				$ph = strval($xml->command->phoneNumber);
				$callingLineIdPhoneNumber = strval($xml->command->callingLineIdPhoneNumber);
				$ext = strval($xml->command->extension);
				if (isset($xml->command->department)) {
					$dept = strval($xml->command->department->name);
				} else {
					$dept = "";
				}
//				$deviceName = strval($xml->command->accessDeviceEndpoint->accessDevice->deviceName);
//				$linePort = strval($xml->command->accessDeviceEndpoint->linePort);
				$emailAddress = strval($xml->command->emailAddress);
				if (isset($xml->command->address->addressLine1)) {
					$address1 = strval($xml->command->address->addressLine1);
				} else {
					$address1 = "";
				}
				if (isset($xml->command->address->addressLine2)) {
					$address2 = strval($xml->command->address->addressLine2);
				} else {
					$address2 = "";
				}
				if (isset($xml->command->address->city)) {
					$city = strval($xml->command->address->city);
				} else {
					$city = "";
				}
				if (isset($xml->command->address->stateOrProvince)) {
					$state = strval($xml->command->address->stateOrProvince);
				} else {
					$state = "";
				}
				if (isset($xml->command->address->zipOrPostalCode)) {
					$zip = strval($xml->command->address->zipOrPostalCode);
				} else {
					$zip = "";
				}
				$timeZone = strval($xml->command->timeZone);
				if (isset($xml->command->language)) {
					$language = strval($xml->command->language);
				} else {
					$language = "";
				}

				if (isset($deviceType)) {
					unset($deviceType);
				}
				if (isset($macAddress)) {
					unset($macAddress);
				}

				//Get device info
				if (isset($hasDeviceType) && $hasDeviceType) {
					$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetRequest18sp1");
					$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_POST["sp"]) . "</serviceProviderId>";
					$xmlinput .= "<groupId>" . htmlspecialchars($_POST["groupId"]) . "</groupId>";
					$xmlinput .= "<deviceName>" . $value["deviceName"] . "</deviceName>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

					$deviceType = strval($xml->command->deviceType);
					$macAddress = strval($xml->command->macAddress);
				}

				$xmlinput = xmlHeader($sessionid, "UserServiceGetAssignmentListRequest");
				$xmlinput .= "<userId>" . $value["userId"] . "</userId>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

				$servicePacks = array();
				foreach ($xml->command->servicePacksAssignmentTable->row as $k => $v) {
					if ($v->col[1] == "true") {
						$servicePacks[] = strval($v->col[0]);
					}
				}

				$userServices = array();
				foreach ($xml->command->userServicesAssignmentTable->row as $kService => $vService) {
				    if ($vService->col[1] == "true") {
				        $userServices[] = strval($vService->col[0]);
				    }
				}
				
				$userAddInfo = array(
					"firstName" => $firstName,
					"lastName" => $lastName,
					"phoneNumber" => $ph,
					"extension" => $ext,
					"deviceType" => (isset($deviceType) ? $deviceType : ""),
					"deviceName" => (isset($deviceName) ? $deviceName : ""),
					"emailAddress" => $emailAddress,
					"address1" => $address1,
					"address2" => $address2,
					"city" => $city,
					"state" => $state,
					"zip" => $zip,
					"department" => $dept
				);
			}

			//Log the changes
			$changes = array('logs' => $changeLogs, 'userId' => $userId, 'entityName' => "$lastName, $firstName", "action" => $action, "userInfo" => $userAddInfo);
			insertUserChangeLogs($changes, $fields);

			echo "<tr>
						<td>" . $action . "</td>
						<td>" . $firstName . "</td>
						<td>" . $lastName . "</td>
						<td>" . $clidFirst . "</td>
						<td>" . $clidLast . "</td>
						<td>" . $ph . "</td>
						<td>" . $callingLineIdPhoneNumber . "</td>
						<td>" . $ext . "</td>
						<td>" . $dept . "</td>
						<td>" . $address1 . "</td>
						<td>" . $address2 . "</td>
						<td>" . $city . "</td>
						<td>" . $state . "</td>
						<td>" . $zip . "</td>
						<td>" . $timeZone . "</td>
						<td>" . $value["networkClassOfService"] . "</td>
						<td>" . $language . "</td>
						<td>" . $emailAddress . "</td>
						<td>" . implode(", ", $servicePacks) . "</td>
                        <td>" . implode(", ", $userServices) . "</td>
						<td>" . (isset($deviceType) ? $deviceType : "") . "</td>
						<td>" . (isset($macAddress) ? $macAddress : "") . "</td>
						<td>" . $value["voiceMessaging"] . "</td>
						<td>" . $value["polycomPhoneServices"] . "</td>
						<td>" . (isset($deviceAccessUserName) ? $deviceAccessUserName : "") . "</td>
						<td>" . (isset($deviceAccessPassword) ? $deviceAccessPassword : "") . "</td>
						<td>" . $value["callingLineIdPolicy"] . "</td>
						<td>" . (strtoupper($value["voiceMessaging"]) == "YES" ? $portalPass : "N/A") . "</td>
						<td>" . $initialPass . "</td>
						<td>" . $gpName . "</td>
					</tr>";
		}
		
		if(count($deleteArray) > 0){
			require_once("/var/www/lib/broadsoft/adminPortal/sasOperation/sasOperation.php"); //Added 12 Nov 2018
			
			$fileName = "../../../SASTestingUser/SASTestUsers.csv";
			$sasObject = new sasOperation();
			$sasObject->deleteFromCSVFile($deleteArray, $fileName);
		}
		?>
	</table>
	<div class='mainPageButton' id='mainPageButton'><a href='javascript:void(0)' id='downloadUserSuccessTable'>Download CSV</a></div>
	</div>
	<?php 
	if($errorFlag == 1){
	    if(count($errorneousDataArray) > 0){
	        require_once("/var/www/lib/broadsoft/adminPortal/DeleteFailedUserDetail.php");
	        $DFU = new DeleteFailedUserDetail();
	        $spId = $_SESSION["sp"];
	        $groupId = $_SESSION["groupId"];
	        foreach ($errorneousDataArray as $key => $value){
	            if($value["userId"] != ""){
	                $userDelete = $DFU->deleteUser($value["userId"]);
	                if( empty($userDelete["Error"]) ){
	                    $returnResponse[]["userDeleted"] = "Success";
	                }
	            }
	            
	            if($value["deviceName"] != ""){
	                $deviceDelete = $DFU->deviceDeleteRequest($spId, $groupId, $value["deviceName"]);
	                if( empty($deviceDelete["Error"]) ){
	                    $returnResponse[]["deviceDeleted"] = "Success";
	                }
	            }
	        }
	    }
	}
	?>
	<?php if($errorFlag == 1){

	  // echo "<br><br><b>The following users does not provisioned because there is error in one of module as following:</b><br><br>";
	    echo "<br><br><b>The following users were not provisioned because of the following error(s):</b><br><br>"; //ex-529
	    echo "<div class='well table-responsive'>";
        echo '<table border="1" align="center" class="bulkUserNotCreated table table-bordered table-striped" style="width:96%;">';
	    echo '<thead>
            	<tr>
                	<th>User Id</th>
                	<th>First Name</th>
                	<th>Last Name</th>
                	<th>Device Name</th>
                	<th>Error In Module</th>
            	</tr>
    	     </thead>
    	  <tbody>';
	
	   foreach ($errorneousDataArray as $key => $value){
	       echo "<tr>
                <td>".$value['userId']."</td>
                <td>".$value['firsName']."</td>
                <td>".$value['lastName']."</td>
                <td>".$value['deviceName']."</td>
                <td style='color:red;'>".$value['errorMessage']."</td>
                </tr>";
	   }
	
	echo '</tbody><tfoot style="line-height:2;"><tr><td colspan="5" class="mainPageButton" id="mainPageButton"><a href="javascript:void(0)" id="donwloadRollBackUsers">Download CSV</a></td></tr></tfoot></table></div>';
	} ?>
</div>
<script>
$(document).ready(function(){

	$('#donwloadRollBackUsers').click(function() {
        var titles = [];
        var data = [];
        $('.bulkUserNotCreated th').each(function() {    //table id here
          titles.push($(this).text());
        });

      
        $('.bulkUserNotCreated td').each(function() {    //table id here
          data.push($(this).text());
        });
        
        
        var CSVString = prepCSVRow(titles, titles.length, '');
        CSVString = prepCSVRow(data, titles.length, CSVString);

        var fileName = "RolledBackUsersData.csv";
        var blob = new Blob(["\ufeff", CSVString]);
        if (navigator.msSaveBlob) { // IE 10+
        	navigator.msSaveBlob(blob, fileName);
        }else{
        	var downloadLink = document.createElement("a");
            
            var url = URL.createObjectURL(blob);
            downloadLink.href = url;
            downloadLink.download = fileName;

            
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
       
        
      });

	$('#downloadUserSuccessTable').click(function() {
        var titles = [];
        var data = [];
        $('#bulkUserTableSuccess th').each(function() {    //table id here
          titles.push($(this).text());
        });

      
        $('#bulkUserTableSuccess td').each(function() {    //table id here
          data.push($(this).text());
        });
        
        
        var CSVString = prepCSVRowForSuccessTable(titles, titles.length, '');
        CSVString = prepCSVRowForSuccessTable(data, titles.length, CSVString);

        var blob = new Blob(["\ufeff", CSVString]);
        var fileName = "SuccessUserTable.csv";
        if (navigator.msSaveBlob) { // IE 10+
        	navigator.msSaveBlob(blob, fileName);
        }else{
        	var downloadLink = document.createElement("a");
            
            var url = URL.createObjectURL(blob);
            downloadLink.href = url;
            downloadLink.download = fileName;

            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
       
        
      });
	
});

function prepCSVRow(arr, columnCount, initial) {
    var row = '';
    var delimeter = ',';
    var newLine = '\r\n';

    function splitArray(_arr, _count) {
      var splitted = [];
      var result = [];
      _arr.forEach(function(item, idx) {
        if ((idx + 1) % _count === 0) {
          splitted.push(item);
          result.push(splitted);
          splitted = [];
        } else {
          splitted.push(item);
        }
      });
      return result;
    }
    var plainArr = splitArray(arr, columnCount);
   
    plainArr.forEach(function(arrItem) {
      arrItem.forEach(function(item, idx) {
        row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
      });
      row += newLine;
    });
    return initial + row;
  }

function prepCSVRowForSuccessTable(arr, columnCount, initial) {
    var row = '';
    var delimeter = ',';
    var newLine = '\r\n';

    function splitArray(_arr, _count) {
      var splitted = [];
      var result = [];
      _arr.forEach(function(item, idx) {
        if ((idx + 1) % _count === 0) {
          splitted.push(item);
          result.push(splitted);
          splitted = [];
        } else {
          splitted.push(item);
        }
      });
      return result;
    }
    var plainArr = splitArray(arr, columnCount);
   
    plainArr.forEach(function(arrItem) {
      arrItem.forEach(function(item, idx) {
    	  item = "\""+item+"\"";
        row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
      });
      row += newLine;
    });
    return initial + row;
  }

</script>
