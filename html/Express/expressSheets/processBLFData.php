<?php
/**
 * Created by Karl.
 * Date: 10/12/2016
 */
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

if ($_SESSION["superUser"] != "1" && $_SESSION["permissions"]["expressSheets"] != "1")
{
	echo "<div style='color: red;text-align: center;'>You don't have permission to access Express Sheets.</div>";
	exit;
}

if($_SESSION["superUser"] != "1" && $_SESSION["permissions"]["changeblf"] != "1") {
	echo "You don't have permission to provision BLF.";
	exit;
}

require_once("/var/www/html/Express/expressSheets/ExpressSheetsBLFManager.php");
require_once("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");
//require_once("/var/www/lib/broadsoft/adminPortal/DBLookup.php");

$blfManager = new ExpressSheetsBLFManager("excelData");
?>

<div class="formPadding">
	The following BLF have been provisioned:<br><br>
	<table border="1" align="center" class="bulkBLFTable" style="width:96%;">
		<tr>
			<?php echo $blfManager->createTableHeader(); ?>
		</tr>
		<?php echo $blfManager->processBulk(); ?>
	</table>
	<div class='mainPageButton' id='mainPageButton'><a href='javascript:void(0)' id='downloadBLFSuccessTable'>Download CSV</a></div>
</div>

<script>

$(document).ready(function(){
	$('#downloadBLFSuccessTable').click(function() {
        var titles = [];
        var data = [];
        $('.bulkBLFTable th').each(function() {    //table id here
          titles.push($(this).text());
        });

      
        $('.bulkBLFTable td').each(function() {    //table id here
          data.push($(this).text());
        });
        
        
        var CSVString = prepCSVRowForBLFSuccessTable(titles, titles.length, '');
        CSVString = prepCSVRowForBLFSuccessTable(data, titles.length, CSVString);

        var fileName = "SuccessBLFTable.csv";
        var blob = new Blob(["\ufeff", CSVString]);
        if (navigator.msSaveBlob) { // IE 10+
        	navigator.msSaveBlob(blob, fileName);
        }else{
        	var downloadLink = document.createElement("a");
            
            var url = URL.createObjectURL(blob);
            downloadLink.href = url;
            downloadLink.download = fileName;

            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
       
        
      });
	
});
function prepCSVRowForBLFSuccessTable(arr, columnCount, initial) {
    var row = '';
    var delimeter = ',';
    var newLine = '\r\n';

    function splitArray(_arr, _count) {
      var splitted = [];
      var result = [];
      _arr.forEach(function(item, idx) {
        if ((idx + 1) % _count === 0) {
          splitted.push(item);
          result.push(splitted);
          splitted = [];
        } else {
          splitted.push(item);
        }
      });
      return result;
    }
    var plainArr = splitArray(arr, columnCount);
   
    plainArr.forEach(function(arrItem) {
      arrItem.forEach(function(item, idx) {
    	  item = "\""+item+"\"";
        row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
      });
      row += newLine;
    });
    return initial + row;
  }


</script>
