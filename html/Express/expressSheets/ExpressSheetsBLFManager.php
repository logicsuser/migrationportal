<?php

/**
 * Created by Dipin Krishna.
 * Date: 10/18/2017
 */
class ExpressSheetsBLFManager
{
    private $keys = array();
    private $headTitles = array();
    private $numColumns = 0;
    private $postName = "";
    private $backEndErrorMsg = "";
    private $monitoredUsers = array();
	private $oldBLFUsers = array();


	private static function userId()          { return "userId"; }
	private static function blfUserId()       { return "blfUserId"; }
	private static function action()          { return "action"; }
	private static function primaryUser()     { return "primaryUser"; }
	private static function line()            { return "line"; }
	private static function blfUser()         { return "blfUser"; }

    public function __construct($postName) {

        $this->postName = $postName;

	    $this->addAttribute(self::action(),          "Action");
	    $this->addAttribute(self::primaryUser(),     "User");
	    $this->addAttribute(self::line(),            "BLF Line");
	    $this->addAttribute(self::blfUser(),         "BLF User");

    }

	public function createTableHeader()
	{
		$header = "";
		$margin = $this->numColumns == 0 ? 100 : (int)(100 / $this->numColumns);

		for ($i = 0; $i < $this->numColumns; $i++) {
			$header .= "<th style=\"width:" . $margin . "%\">" . $this->headTitles[$i] . "</th>";
		}

		return $header;
	}


	public function processBulk()
	{
		global $sessionid, $client, $db, $ociVersion;

		$row = "";
		foreach ($_POST[$this->postName] as $key => $value) {
			$row .= "<tr style=\"font-size: 12px\">";
			$this->executeAction($value);

			for ($i = 0; $i < $this->numColumns; $i++) {
				$row .= "<td>" . $value[$this->keys[$i]] . "</td>";
			}

			$row .= "</tr>";

			if ($this->backEndErrorMsg != "") {
				break;
			}
		}

		require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/expressSheetFunctions.php");

		foreach ($this->oldBLFUsers as $primaryUserID => $primaryUser) {

			if (isset($primaryUser['monitoredUsers']))
			{
				$oldBLFUsers = array();
				foreach ($primaryUser['monitoredUsers'] as $blfUser)
				{
					$oldBLFUsers[] = $blfUser["name"];
				}
				if(count($oldBLFUsers) <= 0) {
					unset($oldBLFUsers);
				}
			}

			$userInfo = array();
			$userId = $primaryUserID;
			require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getUserInfo.php");

			if (isset($userInfo['monitoredUsers']))
			{
				$newBLFUSERS = array();
				foreach ($userInfo['monitoredUsers'] as $blfUser)
				{
					$newBLFUSERS[] = $blfUser["name"];
				}
				if(count($newBLFUSERS) <= 0) {
					unset($newBLFUSERS);
				}
			}

			$changeLogs = array(
				array(
					"field" => "monitoredUsers",
					"old" => isset($oldBLFUsers) ? implode("&", $oldBLFUsers) : "None",
					"new" => isset($newBLFUSERS) ? implode("&", $newBLFUSERS) : "None",
				)
			);

			$changes = array(
				'logs' => $changeLogs,
				'userId' => $primaryUserID,
				'entityName' => $primaryUser["lastName"] . ", " . $primaryUser["firstName"],
				"action" => "Modify",
			);
			insertUserChangeLogs($changes, array("monitoredUsers" => "Busy Lamp Fields"));
		}

		return $row;
	}


	//-----------------------------------------------------------------------------------------------------------------
	private function executeAction($row_value)
	{
		global $sessionid, $client, $db;

		$action = $row_value[self::action()];
		$userId = $row_value[self::userId()];

		$userInfo = array();
		if(!isset($this->monitoredUsers[$userId])) {

			$this->monitoredUsers[$userId] = array();

			require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getUserInfo.php");
			if (isset($userInfo["monitoredUsers"])) {
				$this->monitoredUsers[$userId] = $userInfo['monitoredUsers'];
			}
		}

		if(!isset($this->oldBLFUsers[$userId])) {
			$this->oldBLFUsers[$userId] = array(
				"monitoredUsers" => $userInfo['monitoredUsers'],
				'firstName' => $userInfo["firstName"],
				'lastName' => $userInfo["lastName"]
			);
		}


		/*
		echo "<pre>";
		echo "Before User: $userId :<br/> ";
		print_r($this->monitoredUsers[$userId]);
		echo "<br/> ";
		*/

		$line = $row_value[self::line()] - 1;

		//echo "Line: $line :<br/> ";

		$blfUserId = $row_value[self::blfUserId()];
		switch (strtolower($action)) {
			case 'add':
				$this->monitoredUsers[$userId] = $this->removeBLFItem($blfUserId, $this->monitoredUsers[$userId]);
				if(isset($this->monitoredUsers[$userId][$line])) {
					$this->monitoredUsers[$userId][] = array('id' => $blfUserId);
				} else {
					$this->monitoredUsers[$userId][$line] = array('id' => $blfUserId);
				}
				break;
			case 'modify':
				$this->monitoredUsers[$userId] = $this->removeBLFItem($blfUserId, $this->monitoredUsers[$userId]);
				$this->monitoredUsers[$userId][$line] = array('id' => $blfUserId);
				break;
			case 'delete':
				$this->monitoredUsers[$userId] = $this->removeBLFItem($blfUserId, $this->monitoredUsers[$userId]);
				break;
		}

		ksort($this->monitoredUsers[$userId]);
		/*
		echo "<pre>";
		echo "After User: $userId :<br/> ";
		print_r($this->monitoredUsers[$userId]);
		echo "</pre>";
		*/


		// Update BLF List
		$xml_input = xmlHeader($sessionid, "UserBusyLampFieldModifyRequest");
		$xml_input .= "<userId>" . $row_value[self::userId()] . "</userId>";
		if (count($this->monitoredUsers[$userId])) {
			$xml_input .= "<monitoredUserIdList>";
			foreach ($this->monitoredUsers[$userId] as $monitoredUser) {
				$user_id = $monitoredUser['id'];
				$xml_input .= "<userId>" . $user_id . "</userId>";
			}
			$xml_input .= "</monitoredUserIdList>";
		} else {
			$xml_input .= "<monitoredUserIdList xsi:nil=\"true\" />";
		}
		$xml_input .= xmlFooter();

		$response = $client->processOCIMessage(array("in0" => $xml_input));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		if (readError($xml) != "") {
			$this->backEndErrorMsg = "OCI Error";
			return;
		}

	}

	private function removeBLFItem($blfUserId, $monitoredUsers)
	{
		foreach ($monitoredUsers as $line => $monitoredUser) {
			if($monitoredUser['id'] == $blfUserId) {
				unset($monitoredUsers[$line]);
				break;
			}
		}
		return $monitoredUsers;
	}


	//-----------------------------------------------------------------------------------------------------------------
	private function addAttribute($key, $headTitle)
	{
		$this->keys[] = $key;
		$this->headTitles[] = $headTitle;

		$this->numColumns++;
	}


	//-----------------------------------------------------------------------------------------------------------------
	private static function normalize($key, $value)
	{
		switch ($key) {
			case self::userId():
			case self::phoneNumber():
			case self::extension():
			case self::blfPhoneNumber():
			case self::blfExtension():
				return strtolower($value);
				break;
			case self::line():
				return intval($value);
		}

		return $value;
	}
}
