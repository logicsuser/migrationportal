<?php
/**
 * Created by Dipin Krishna.
 * Date: 10/9/17
 * Time: 8:27 AM
 */
?>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
checkLogin();

if ($_SESSION["superUser"] != "1" && $_SESSION["permissions"]["expressSheets"] != "1") {
	echo "<div style='color: red;text-align: center;'>You don't have permission to access Express Sheets.</div>";
	exit;
}
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/expressSheetFunctions.php");

/* Users from USERS Module - START */
$applyFiltersByDefault = false;
$showFiltersByDefault = false;
$usersFromUserModule = array();

if ($_POST["module"] == "userModule") {
    $selectAllButtonExpSheetChk = $_POST['selectAllchk']; 
	$showFiltersByDefault = true;
	if ($_POST["applyFilters"] == "1") {
		$applyFiltersByDefault = true;
	}
	$usersToGenerate = explode("+-+-+", $_POST["userList"]);
	foreach ($usersToGenerate as $user) {

		// Separator "*-*-*" is used to separate between posted user ID and device name
		// Note that not all users IDs may have device name associated with them
		$userAndDevice = explode("*-*-*", $user);
		if (isset($userAndDevice[0])) {
			$usersFromUserModule[] = $userAndDevice[0];
		}
	}

}
/* Users from USERS Module - END */

//Get Group Info
$sp = $_SESSION['sp'];
$groupId = $_SESSION['groupId'];
unset($groupInfoData);
require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/expressSheets/group_info.php");
$polycomPhoneService = in_array("Polycom Phone Services", $userServices) ? "Yes" : "No";

$user_fields = array(
	'Registered' => array('field' => 'col_Registered', 'type' => 'bool'),
	'First Name' => array('field' => 'col_Fist_Name', 'type' => 'string'),
	'Last Name' => array('field' => 'col_Last_Name', 'type' => 'string'),
	'User ID' => array('field' => 'col_User_Id', 'type' => 'int'),
	'Extension' => array('field' => 'col_Extension', 'type' => 'int'),
	'Device Name' => array('field' => 'col_Device_Name', 'type' => 'string'),
	'Device Type' => array('field' => 'col_Device_Type', 'type' => 'string'),
	'MAC Address' => array('field' => 'col_MAC_Address', 'type' => 'string'),
	'Department' => array('field' => 'col_Department', 'type' => 'string'),
	'Service Pack' => array('field' => 'col_Service_Pack', 'type' => 'string'),
	'Expiration' => array('field' => 'col_Expiration', 'type' => 'string'),
	'Type' => array('field' => 'col_Type', 'type' => 'string'),
	'DnD' => array('field' => 'col_DnD', 'type' => 'switch'),
	'Fwd' => array('field' => 'col_Fwd', 'type' => 'string'),
	'Fwd To' => array('field' => 'col_Fwd_To', 'type' => 'string'),
	'Remote Office' => array('field' => 'col_Remote_Office', 'type' => 'string'),
	'Remote Number' => array('field' => 'col_Remote_Number', 'type' => 'string'),
	'Polycom Phone_Services' => array('field' => 'col_Polycom_Phone_Services', 'type' => 'string'),
	'Custom Contact Directory' => array('field' => 'col_Custom_Contact_Directory', 'type' => 'string'),
);
$callingLineIdDisplayPhoneNumber = str_replace("+1-", "", $groupInfoData["callingLineIdDisplayPhoneNumber"]);
?>
<link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v15">
<!--<script src="/Express/js/bootstrap.min.js"></script>-->
<script src="/Express/js/bootstrap/bootstrap.min.js"></script>
<script src="/Express/js/jquery-1.17.0.validate.js"></script>
<script src="/Express/js/jquery.loadTemplate.min.js"></script>

<div class="expressMainDiv">

	<div class="adminUserText">Express Sheets</div>
	<div class="">

		<div class="formWidthEx">
		
			
			<button id="infoIconES" href="#express_sheets_notes" class="btn btn-link no-focus infoIconEx" title="Instructions" data-toggle="collapse">
				<img src="images/NewIcon/info_icon.png">
			</button>
			<div id="express_sheets_notes" class="collapse" role="alert">
				<div class="panel panel-default">
					<div class="panel-heading infoTitleBar">Instructions</div>
					<div class="panel-body" style="padding: 0;">
						<div class="row">
							<div class="col-md-12">
								<div class="progress progress-lg">
									<div class="progress-bar progress-bar-lg progress-bar-info progress-bar-striped" style="width: 33%">1. GENERATE
										<span class="sr-only">GENERATE</span>
									</div>
									<div class="progress-bar progress-bar-lg progress-bar-warning progress-bar-striped" style="width: 33%">2. UPDATE
										<span class="sr-only">UPDATE</span>
									</div>
									<div class="progress-bar progress-bar-lg progress-bar-success progress-bar-striped" style="width: 34%">3. PROVISION
										<span class="sr-only">PROVISION</span>
									</div>
								</div>
							</div>
						</div>
						<div class="">
							<div class="col-md-12">
								<ol>
									<li>GENERATE an Excel file with/without existing data using the generate button at the bottom of this page. This will download an excel file for you to work on.</li>
									<li>UPDATE data in the Excel Sheet using Microsoft excel.</li>
									<li>PROVISION updates by uploading Excel file. Click "Provision" below to upload a file.</li>
									</ol>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="">
		<div class="">
			<div class="">
				<div class="panel with-nav-tabs panel-default overPanel" style="background-color: transparent !important;">
					<div id="tabs" class="panel-heading ExpresSheetTab">
						<div class="divBeforeUl">
						<ul class="nav nav-tabs formWidthEx">
							<li class="active"><a class="expressTabLi" href="#tab1default" data-toggle="tab">Generate &nbsp;&nbsp;<span class="glyphicon glyphicon-save" aria-hidden="true"
							                                                                                       style="color: #ffffff;"></span></a></li>
							<li><a class="expressTabLi" href="#tab2default" data-toggle="tab">Provision &nbsp;&nbsp;<span class="glyphicon glyphicon-open" aria-hidden="true" style="color: #ffffff;"></span></a></li>
						</ul>
						</div>
					</div>
					<div class="panel-body" style="padding: 0">

						<div class="tab-content">

							<div class="tab-pane fade in active" id="tab1default">
								<h2 class="expressSheetHeadText">Generate Express Sheets
									<small>&nbsp;&nbsp;<span class="glyphicon glyphicon-save" aria-hidden="true"></span></small>
								</h2>

<!-- 								<div class="row-fluid formWidthEx"> -->
<!-- 									<button id="infoIconES" href="#generate_notes" class="btn btn-link no-focus" title="Instructions" data-toggle="collapse"><i -->
<!-- 												class="glyphicon glyphicon-info-sign"></i></button> -->
<!-- 									<div id="generate_notes" class="collapse"> -->
<!-- 										<div class="alert alert-info" role="alert">Select what type of excel sheet you would like the system to create and download for you. -->
<!-- 											The -->
<!-- 											choices are "Users", "SCA", (Shared Call Appearances) and "BLF" (Busy Lamp Field). -->
<!-- 										</div> -->
<!-- 									</div> -->
<!-- 								</div> -->

								<div class="row formWidthEx">
										<!-- <div class="col-md-12">-->
											<div class="col-md-12 form-group">
												<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="Select what type of excel sheet you would like the system to create and download for you."><img src="images/NewIcon/info_icon.png"></a>
											</div>
										 <!-- </div> -->
								</div>
								
								<div class="row">

									<form name="expressSheetsForm" id="expressSheetsForm" method="POST" class="" action="/Express/expressSheets/generate.php"
									      target="_blank">

										<input type="hidden" name="modeOption" class="modeOption" value="Generate"/>

										<div class="row">
										
										<div class="col-md-12">
											<div class="col-md-12 form-group">
												<label class="labelText">Select Type: </label>
												<span class="required">*</span>
												<br />
												<div class="dropdown-wrap">
												<select name="expressSheetType" id="expressSheetType" class="form-control" style="width: 100% !important">
														<option value="Users">Users</option>
														<?php
														if ($_SESSION["permissions"]["changeSCA"] == "1") {
															?>
															<option value="SCA">SCA</option>
															<?php
														}
														?>
														<?php
														if ($_SESSION["permissions"]["changeblf"] == "1") {
															?>
															<option value="BLF">BLF</option>
															<?php
														}
														?>
														<option value="Voicemail">Voicemail</option>
													</select>
												</div>
											</div>
										</div>
										
										</div>


										<div id="generateFormSettings">

											<div class="">

												<div class="">

													<div class="row panel-group" id="generateAccordion" role="tablist" aria-multiselectable="true">
													<!-- <div class="panel panel-default panel-forms generateDiv overPanel" id="expressSheetGroupInfo" style="display: none;"> -->	

													<!-- 	<div class="panel panel-default panel-forms generateDiv" id="expressSheetGroupInfo" style="display: none;">


															<div class="panel-heading" role="tab" id="groupInfoAddressTitle">
																<h4 class="panel-title">
																	<a class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" data-parent="#generateAccordion"
																	   href="#groupInfoAddress"
																	   aria-expanded="false"
																	   aria-controls="groupInfoAddress">Group Address</a>
																</h4>
															</div>

															<div id="groupInfoAddress" class="panel-collapse collapse" role="tabpanel" aria-labelledby="groupInfoAddressTitle">
																<div class="panel-body" style="padding: 0">
																<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">GroupID: </label>
																			<span class="required">*</span>
																			<br/><input class="form-control readonly" name="Users_GroupID" disabled value="<?php //echo $_SESSION['groupId'] ?>"/>
																		</div>
																		</div>
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Address: </label>
																			<span class="required">*</span>
																			<br/>
																				<input class="form-control readonly" name="Users_Address" disabled
																				       value="<?php // echo $groupInfoData['addressLine1'] ?>"/>
																			</div>
																		</div>
																	</div>																
																	
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Suite: </label>
																			<span class="required">*</span>
																			<br/>
																			<input class="form-control readonly" name="Users_Suite" disabled value="<?php //echo $groupInfoData['addressLine2'] ?>"/>
																		</div>
																		</div>
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">City: </label>
																			<span class="required">*</span>
																			<br/>
																				<input class="form-control readonly" name="Users_City" disabled value="<?php //echo $groupInfoData['city'] ?>"/>
																			</div>
																		</div>
																	</div>
																	
																	
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">State: </label>
																			<span class="required">*</span>
																			<br/>
																			<input class="form-control readonly" name="Users_State" disabled
																				       value="<?php //echo $groupInfoData['stateOrProvince'] ?>"/>
																		</div>
																		</div>
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Zip: </label>
																			<span class="required">*</span>
																			<br/>
																				<input class="form-control readonly" name="Users_Zip" disabled value="<?php //echo $groupInfoData['zipOrPostalCode'] ?>"/>
																			</div>
																		</div>
																	</div>
																	
																	
																	<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Time Zone: </label>
																			<span class="required">*</span>
																			<br/>
																			<input class="form-control readonly" name="Users_TimeZone" disabled value="<?php //echo $groupInfoData['timeZone'] ?>"/>
																		</div>
																		</div>
																	</div>
																	
																	
																</div>
															</div>
														</div> -->

														<div class="panel panel-default panel-forms generateDiv overPanel" id="expressSheetUsers" style="display: none;">

															<div class="panel-heading" role="tab" id="groupInfoConfigUsersTitle">
																<h4 class="panel-title">
																	<a class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" data-parent="#generateAccordion"
																	   href="#groupInfoConfigUsers"
																	   aria-expanded="true" aria-controls="groupInfoConfigUsers">Configure Defaults</a>
																</h4>
															</div>

															<div id="groupInfoConfigUsers" class="panel-collapse collapse" role="tabpanel" aria-labelledby="groupInfoConfigUsersTitle">
																<div class="panel-body" style="padding: 0;">
																	
																	<div class="form-group"></div>
																	
<!-- 																	<div class="row-fluid"> -->
<!-- 																		<button id="infoIconES" href="#groupInfoConfigUsers_notes" class="btn btn-link no-focus" title="Instructions" -->
<!-- 																		        data-toggle="collapse"><i class="glyphicon glyphicon-info-sign"></i></button> -->
<!-- 																		<div id="groupInfoConfigUsers_notes" class="collapse"> -->
<!-- 																			<div class="alert alert-info" role="alert"> -->
<!-- 																				Below, please configure the common fields that should apply to all User entries added. -->
<!-- 																			</div> -->
<!-- 																		</div> -->
<!-- 																	</div>															 -->
																	
																	<div class="row formWidthEx">
                                    										<div class="col-md-12">
                                    											<div class="form-group">
                                    												<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="Below, please configure the common fields that should apply to all User entries added."><img src="images/NewIcon/info_icon.png"></a>
                                    											</div>
                                    										</div>
                                    								</div>
								
																	<!--<div class="row">
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Group Calling Line ID: </label>
																			<span class="required">*</span>
																			<br/>
																			<input class="form-control users-required readonly" name="Users_CallingLineIdDisplayPhoneNumber" disabled
																				       value="<?php //echo $callingLineIdDisplayPhoneNumber ?>"/>
																		</div>
																		</div>
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Calling Line ID First Name: </label>
																			<span class="required">*</span>
																			<br/>
																			<input class="form-control users-required readonly" name="Users_CallingLineIdFirstName" disabled
																				       value="<?php //echo $groupInfoData['callingLineIdFirstName'] ?>"/>
																		</div>
																		</div>
																	</div>-->
																	
																	
																	 
																	 <div class="row">
																		<!--<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Calling Line ID Last Name: </label>
																			<span class="required">*</span>
																			<br/>
																			<input class="form-control users-required readonly" name="Users_CallingLineIdLastName" disabled
																				       value="<?php //echo $groupInfoData['callingLineIdLastName'] ?>"/>
																		</div>
																		</div> -->
																		<div class="col-md-12">
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Line Port Domain: </label>
																			<span class="required">*</span>
																			<br/>
																			<div class="dropdown-wrap oneColWidth">
																			<select class="form-control group-default users-required" name="Users_linePortDomain">
																					<option value="">Select One</option>
																					<?php
																					$domainList = $groupInfoData['assignedDomain'];
																					if (count($domainList) > 0) {
																						foreach ($domainList as $key => $value) {
																							if (strpos($value, $securityDomainPattern) !== false && $value <> $_SESSION["systemDefaultDomain"]) {
																								unset($domainList[$key]);
																							}
																						}
																					}
																					$domainList = array_values($domainList);
																					foreach ($domainList as $domain) {
																						?>
																						<option value="<?php echo $domain; ?>" <?php if ($domain == $_SESSION["systemDefaultDomain"]) {
																							echo "selected";
																						} ?>>
																							<?php
																							if ($domain == $_SESSION["systemDefaultDomain"]) {
																								echo "$domain (default domain)";
																							} else {
																								echo "$domain";
																							}
																							?>
																						</option>
																						<?php
																					}
																					?>
																				</select>
																				</div>
																		</div>
																		</div>
																		
																		<div class="col-md-6">
																		<div class="form-group">
																			<label class="labelText">Extension Length: </label>
																			<span class="required">*</span>
																			<div class="">
																				<?php 
																				if($useExtensionLengthRange == "false"){
																					$labelShow = "false";																					
																				}else{
																					$labelShow = "true";
																					$extensionResponse = $dns->getGroupExtensionConfigRequest($sp, $groupId);
																					if(($extensionResponse["minExtensionLength"] == $extensionResponse["maxExtensionLength"]) && ($extensionResponse["maxExtensionLength"] == $extensionResponse["defaultExtensionLength"])){
																						$labelShow = "false";
																					}
																				}
																																								
																				if($labelShow == "false"){
																					echo '<span class="labelTextGrey" style="display: block;margin-left: 18px;">'.$_SESSION["groupExtensionLength"]["default"].' (default) <input type="hidden" name="Users_extensionLength" value="'.$_SESSION["groupExtensionLength"]["default"].'" /></span>';
                                                                                                                                                                        
																				}else{?>
																					<select class="form-control group-default users-required" name="Users_extensionLength">
																						<option value="">Select One</option>
																						<?php
																						for ($extensionLength = $groupInfoData['minExtensionLength']; $extensionLength <= $groupInfoData['maxExtensionLength']; $extensionLength++) {
																							?>
																							<option <?php echo $extensionLength == $groupInfoData['defaultExtensionLength'] ? "SELECTED" : ""; ?>
																									value="<?php echo $extensionLength; ?>">
																								<?php echo $extensionLength . ($extensionLength == $groupInfoData['defaultExtensionLength'] ? " (default)" : ""); ?>
																							</option>
																							<?php
																						}
																						?>
																					</select>
																				<?php }?>
																			</div>
																		</div>
																		</div>
																		</div>
																	</div>

								<div class="row">
									<div class="col-md-12">
													<div class="col-md-6">
														<div class="form-group">
														<label class="labelText">Voice Mail Service Pack: </label>
														<br/>
														<div class="dropdown-wrap oneColWidth">
														<select class="form-control group-default" name="Users_voiceMailServicePack">
																<?php
																if (is_array($_SESSION["vmServicePacks"])) {
																	if (count($_SESSION["vmServicePacks"]) > 1) {
																		?>
																		<option value="">None</option>
																		<?php
																		foreach ($_SESSION["vmServicePacks"] as $servicePack) {
																			?>
																			<option value="<?php echo $servicePack; ?>"><?php echo $servicePack; ?></option>
																			<?php
																		}
																	} else {
																		?>
																		<option value="">None</option>
																		<option value="<?php echo $_SESSION["vmServicePacks"][0]; ?>">
																			<?php echo $_SESSION["vmServicePacks"][0]; ?>
																		</option>
																		<?php
																	}
																} else {
																	?>
																	<option value="">None</option>
																	<?php
																}
																?>
															</select>
														</div>	
													</div>
									</div>
										
									<div class="col-md-6">
										<div class="form-group">
										<label class="labelText">Polycom Phone Services: </label>
										<span class="required">*</span>
										<br/>
										<div class="dropdown-wrap oneColWidth">
										<select class="form-control group-default users-required" name="Users_polycomPhoneService">
											<option <?php echo $polycomPhoneService == "Yes" ? "SELECTED" : ""; ?> value="Yes">
												Yes
											</option>
											<option <?php echo $polycomPhoneService == "No" ? "SELECTED" : ""; ?> value="No">
												No
											</option>
													
											</select>
											</div>
										</div>
									</div>
									
								</div>
							</div>								
							<div class="row">
							
								<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
									<label class="labelText">Default Custom Directory: </label>
									<div class="dropdown-wrap oneColWidth">
									<select class="form-control group-default users-required" name="Users_defaultCustomContactDirectory">
										<option value="None">
											None
										</option>
										<?php
										if ($directories && count($directories)) {
											foreach ($directories as $directory) {
												?>
												<option value="<?= $directory ?>">
													<?= $directory ?>
												</option>
												<?php
											}
										}
										?>
									</select>
									</div>
									</div>
								</div>
										<div class="col-md-6">
											<div class="form-group">
											<label class="labelText">Calling Line ID Policy: </label>
											<span class="required">*</span>
											<br/>
											<div class="dropdown-wrap oneColWidth">
											 <select class="form-control group-default users-required" name="Users_callingLineIDPolicy">
													<option value="Group">Group (default)</option>
													<option value="User">User</option>
												</select>
											</div>	
										</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">
											<label class="labelText">Network Class of Service: </label>
											<br/>
											<div class="dropdown-wrap oneColWidth">
											<select class="form-control group-default" name="Users_networkClassofService">
													<option value="">None</option>
													<?php
													foreach ($networkClassesOfService as $ncos) {
														?>
														<option <?php echo $defaultNetworkClassOfService == $ncos ? "SELECTED" : ""; ?>
																value="<?php echo $ncos; ?>">
																<?php echo $ncos . ($defaultNetworkClassOfService == $ncos ? " (default)" : ""); ?>
															</option>
															<?php
														}
														?>
													</select>
												</div>	
											</div>
											</div>
											
										</div>
										</div>

									</div>

								</div>

														</div>

														<div class="panel panel-default panel-forms generateDiv overPanel" id="expressSheetSCA" style="display: none;">

															<div class="panel-heading" role="tab" id="groupInfoConfigSCATitle">
																<h4 class="panel-title">
																	<a class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" data-parent="#generateAccordion"
																	   href="#groupInfoConfigSCA"
																	   aria-expanded="true" aria-controls="groupInfoConfigSCA">Configure Defaults</a>
																</h4>
															</div>

															<div id="groupInfoConfigSCA" class="panel-collapse collapse" role="tabpanel" aria-labelledby="groupInfoConfigSCATitle">
																<div class="panel-body" style="padding: 0;">

																	<div class="row-fluid">
																		<button id="infoIconES" href="#groupInfoConfigSCA_notes" class="btn btn-link no-focus" title="Instructions" data-toggle="collapse"><img src="images/NewIcon/info_icon.png"></button>
																		<div id="groupInfoConfigSCA_notes" class="collapse">
																			<div class="alert alert-info" role="alert">
																				Below, please configure the common fields that should apply to all SCA entries added.
																			</div>
																		</div>
																	</div>
																	
																	<div class="form-group"></div>
																	<div class="row">
																	<div class="col-md-12">
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Active: </label>
																			<span class="required">*</span>
																			<br/>
																			<div class="dropdown-wrap oneColWidth">
																			<select name="SCA_Active" class="form-control group-default sca-required">
																					<option <?php echo $scaIsActive == "true" ? "SELECTED" : ""; ?>
																							value="True">
																						True<?php echo $scaIsActive == "true" ? " (default)" : ""; ?>
																					</option>
																					<option <?php echo $scaIsActive == "false" ? "SELECTED" : ""; ?>
																							value="False">
																						False<?php echo $scaIsActive == "false" ? " (default)" : ""; ?>
																					</option>
																				</select>
																			</div>	
																		</div>
																		</div>
																		
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Allow Origination: </label>
																			<span class="required">*</span>
																			<br/>
																			<div class="dropdown-wrap oneColWidth">
																			<select name="SCA_AllowOrigination" class="form-control group-default sca-required">
																					<option <?php echo $scaAllowOrigination == "true" ? "SELECTED" : ""; ?>
																							value="True">
																						True<?php echo $scaAllowOrigination == "true" ? " (default)" : ""; ?>
																					</option>
																					<option <?php echo $scaAllowOrigination == "false" ? "SELECTED" : ""; ?>
																							value="False">
																						False<?php echo $scaAllowOrigination == "false" ? " (default)" : ""; ?>
																					</option>
																				</select>
																			</div>	
																		</div>
																		</div>
																	</div>
																	</div>
																	
																	
																	
																	<div class="row">
																	<div class="col-md-12">
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Allow Termination: </label>
																			<span class="required">*</span>
																			<br/>
																			<div class="dropdown-wrap oneColWidth">
																			<select name="SCA_AllowTermination" class="form-control group-default sca-required">
																					<option <?php echo $scaIsActive == "true" ? "SELECTED" : ""; ?>
																							value="True">
																						True<?php echo $scaIsActive == "true" ? " (default)" : ""; ?>
																					</option>
																					<option <?php echo $scaIsActive == "false" ? "SELECTED" : ""; ?>
																							value="False">
																						False<?php echo $scaIsActive == "false" ? " (default)" : ""; ?>
																					</option>
																				</select>
																			</div>	
																		</div>
																		</div>
																		
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Allow All Click to Dial: </label>
																			<span class="required">*</span>
																			<br/>
																			<div class="dropdown-wrap oneColWidth">
																			<select name="SCA_AllowAllClicktoDial" class="form-control group-default sca-required">
																					<option <?php echo $scaAlertClickToDial == "true" ? "SELECTED" : ""; ?>
																							value="True">
																						True<?php echo $scaAlertClickToDial == "true" ? " (default)" : ""; ?>
																					</option>
																					<option <?php echo $scaAlertClickToDial == "false" ? "SELECTED" : ""; ?>
																							value="False">
																						False<?php echo $scaAlertClickToDial == "false" ? " (default)" : ""; ?>
																					</option>
																				</select>
																			</div>	
																		</div>
																		</div>
																	</div>
																	</div>
																	
																	
																	
																	
																	<div class="row">
																	<div class="col-md-12">
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Allow All Group Paging: </label>
																			<span class="required">*</span>
																			<br/>
																			<div class="dropdown-wrap oneColWidth">
																			<select name="SCA_AllowAllGroupPaging" class="form-control group-default sca-required">
																					<option <?php echo $scaAllowGroupPaging == "true" ? "SELECTED" : ""; ?>
																							value="True">
																						True<?php echo $scaAllowGroupPaging == "true" ? " (default)" : ""; ?>
																					</option>
																					<option <?php echo $scaAllowGroupPaging == "false" ? "SELECTED" : ""; ?>
																							value="False">
																						False<?php echo $scaAllowGroupPaging == "false" ? " (default)" : ""; ?>
																					</option>
																				</select>
																			</div>	
																		</div>
																		</div>
																		
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Allow Call Retrieve: </label>
																			<span class="required">*</span>
																			<br/>
																			<div class="dropdown-wrap oneColWidth">
																			<select name="SCA_AllowCallRetrieve" class="form-control group-default sca-required">
																					<option <?php echo $scaAllowCallRetrieve == "true" ? "SELECTED" : ""; ?>
																							value="True">
																						True<?php echo $scaAllowCallRetrieve == "true" ? " (default)" : ""; ?>
																					</option>
																					<option <?php echo $scaAllowCallRetrieve == "false" ? "SELECTED" : ""; ?>
																							value="False">
																						False<?php echo $scaAllowCallRetrieve == "false" ? " (default)" : ""; ?>
																					</option>
																				</select>
																			</div>	
																		</div>
																		</div>
																	</div>
																	</div>
																	
																	
																	
																	<div class="row">
																	<div class="col-md-12">
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Multiple Call Arrangement: </label>
																			<span class="required">*</span>
																			<br/>
																			<div class="dropdown-wrap oneColWidth">
																			<select name="SCA_MultipleCallArrangement" class="form-control group-default sca-required">
																					<option <?php echo $scaMultipleCallArrangement == "true" ? "SELECTED" : ""; ?>
																							value="True">
																						True<?php echo $scaMultipleCallArrangement == "true" ? " (default)" : ""; ?>
																					</option>
																					<option <?php echo $scaMultipleCallArrangement == "false" ? "SELECTED" : ""; ?>
																							value="False">
																						False<?php echo $scaMultipleCallArrangement == "false" ? " (default)" : ""; ?>
																					</option>
																				</select>
																			</div>	
																		</div>
																		</div>
																		
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Call Park Notification: </label>
																			<span class="required">*</span>
																			<br/>
																			<div class="dropdown-wrap oneColWidth">
																			<select name="SCA_CallParkNotification" class="form-control group-default sca-required">
																					<option <?php echo $scaCallParkNotification == "true" ? "SELECTED" : ""; ?>
																							value="True">
																						True<?php echo $scaCallParkNotification == "true" ? " (default)" : ""; ?>
																					</option>
																					<option <?php echo $scaCallParkNotification == "false" ? "SELECTED" : ""; ?>
																							value="False">
																						False<?php echo $scaCallParkNotification == "false" ? " (default)" : ""; ?>
																					</option>
																				</select>
																			</div>	
																		</div>
																		</div>
																	</div>
																	</div>
																	
																	
																	
																	<div class="row">
																	<div class="col-md-12">
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Allow Bridging: </label>
																			<span class="required">*</span>
																			<br/>
																			<div class="dropdown-wrap oneColWidth">
																			<select name="SCA_AllowBridging" class="form-control group-default sca-required">
																					<option <?php echo $scaAllowBridging == "true" ? "SELECTED" : ""; ?>
																							value="True">
																						True<?php echo $scaAllowBridging == "true" ? " (default)" : ""; ?>
																					</option>
																					<option <?php echo $scaAllowBridging == "false" ? "SELECTED" : ""; ?>
																							value="False">
																						False<?php echo $scaAllowBridging == "false" ? " (default)" : ""; ?>
																					</option>
																				</select>
																			</div>	
																		</div>
																		</div>
																		
																		<div class="col-md-6">
																			<div class="form-group">
																			<label class="labelText">Bridge Warning Tone: </label>
																			<span class="required">*</span>
																			<br/>
																			<div class="dropdown-wrap oneColWidth">
																			<select name="SCA_BridgeWarningTone" class="form-control group-default sca-required">
																					<option <?php echo $scaBridgeWarningTone == "Barge-In" ? "SELECTED" : ""; ?>
																							value="Barge-In">
																						Barge-In<?php echo $scaBridgeWarningTone == "Barge-In" ? " (default)" : ""; ?>
																					</option>
																					<option <?php echo $scaBridgeWarningTone == "Barge-In and Repeat" ? "SELECTED" : ""; ?>
																							value="Barge-In and Repeat">
																						Barge-In and Repeat<?php echo $scaBridgeWarningTone == "Barge-In and Repeat" ? " (default)" : ""; ?>
																					</option>
																					<option <?php echo $scaBridgeWarningTone == "None" ? "SELECTED" : ""; ?>
																							value="None">
																						None<?php echo $scaBridgeWarningTone == "None" ? " (default)" : ""; ?>
																					</option>
																				</select>
																			</div>	
																		</div>																			
																		</div>
																		</div>
																	</div>
																	
																</div>
															</div>

														</div>


														<div class="panel panel-default panel-forms generateDiv overPanel" id="expressSheetBLF" style="display: none;">

															<div class="panel-heading" role="tab" id="groupInfoConfigBLFTitle">
																<h4 class="panel-title">
																	<a class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" data-parent="#generateAccordion"
																	   href="#groupInfoConfigBLF"
																	   aria-expanded="true" aria-controls="groupInfoConfigBLF">Configure Defaults</a>
																</h4>
															</div>

															<div id="groupInfoConfigBLF" class="panel-collapse collapse" role="tabpanel" aria-labelledby="groupInfoConfigBLFTitle">
																<div class="panel-body" style="padding: 0;">

																	<div class="row-fluid">
																		<button id="infoIconES" href="#groupInfoConfigBLF_notes" class="btn btn-link no-focus" title="Instructions" data-toggle="collapse"><img src="images/NewIcon/info_icon.png"></button>
																		<div id="groupInfoConfigBLF_notes" class="collapse">
																			<div class="alert alert-info" role="alert">
																				Below, please configure the common fields that should apply to all BLF entries added.
																			</div>
																		</div>
																	</div>

																	<div class="form-group"></div>
																	<div class="row">
																<div class="col-md-12">
                                                            			<div class="col-md-6">
                                                            				<div class="form-group">
                                                            				<label class="labelText">Group Calling Line ID: </label>
                                                            				<span class="required">*</span>
                                                            				<br/>
                                                            				 
                                                            				<input class="form-control blf-required readonly" name="BLF_CallingLineIdDisplayPhoneNumber" disabled
																				       value="<?php echo $callingLineIdDisplayPhoneNumber ?>"/>
                                                            				 
                                                            			</div>
                                                            			</div>
                                                            			
                                                            			<div class="col-md-6">
                                                            				<div class="form-group">
                                                            				<label class="labelText">Calling Line ID First Name: </label>
                                                            				<span class="required">*</span>
                                                            				<br/>
                                                            				<input class="form-control blf-required readonly" name="BLF_CallingLineIdFirstName" disabled
																				       value="<?php echo $groupInfoData['callingLineIdFirstName'] ?>"/>
                                                            			</div>
                                                            			</div>
                                                            	</div>
																	</div>
																	
																	
																	<div class="row">
																	<div class="col-md-12">
                                                            			<div class="col-md-6">
                                                            				<div class="form-group">
                                                            				<label class="labelText">Calling Line ID Last Name: </label>
                                                            				<span class="required">*</span>
                                                            				<br/>
                                                            				<input class="form-control blf-required readonly" name="BLF_CallingLineIdLastName" disabled
																				       value="<?php echo $groupInfoData['callingLineIdLastName'] ?>"/>
                                                            			</div>
                                                            			</div>
                                                            			
											<div class="col-md-6">
												<div class="form-group">
												<label class="labelText">Extension Length: </label>
												<span class="required">*</span>
												<br/>
												<div class="dropdown-wrap oneColWidth">
												<select class="form-control group-default blf-required" name="BLF_extensionLength">
														<?php
														for ($extensionLength = $groupInfoData['minExtensionLength']; $extensionLength <= $groupInfoData['maxExtensionLength']; $extensionLength++) {
															?>
															<option <?php echo $extensionLength == $groupInfoData['defaultExtensionLength'] ? "SELECTED" : ""; ?>
																	value="<?php echo $extensionLength; ?>">
																<?php echo $extensionLength . ($extensionLength == $groupInfoData['defaultExtensionLength'] ? " (default)" : ""); ?>
															</option>
															<?php
														}
														?>
													</select>
												</div>	
											</div>
											</div>
									</div>
									</div>
									
									</div>

															</div>

														</div>

														<div class="panel panel-default panel-forms generateDiv overPanel" id="expressSheetVoicemail" style="display: none;">

															<div class="panel-heading" role="tab" id="groupInfoConfigVoicemailTitle">
																<h4 class="panel-title">
																	<a class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" data-parent="#generateAccordion"
																	   href="#groupInfoConfigVoicemail"
																	   aria-expanded="true" aria-controls="groupInfoConfigVoicemail">Configure Defaults</a>
																</h4>
															</div>

															<div id="groupInfoConfigVoicemail" class="panel-collapse collapse" role="tabpanel" aria-labelledby="groupInfoConfigVoicemailTitle">
																<div class="panel-body" style="padding: 0;">

																	<div class="row-fluid">
																		<button id="infoIconES" href="#groupInfoConfigVoicemail_notes" class="btn btn-link no-focus" title="Instructions"
																		        data-toggle="collapse"><img src="images/NewIcon/info_icon.png"></button>
																		<div id="groupInfoConfigVoicemail_notes" class="collapse">
																			<div class="alert alert-info" role="alert">
																				Below, please configure the common fields that should apply to all Voicemail entries added.
																			</div>
																		</div>
																	</div>
																	
																	<div class="form-group"></div>
																	
																	<div class="well">
																		<div class="row">
																		<div class="col-md-12">
                                                                			<div class="col-md-6">
                                                                				<div class="form-group">
                                                            							<label class="labelText">Enable VM: </label>
                                                                				
                                                                					<input class="voicemail-required" id="Voicemail_Enable_VMOn" name="Voicemail_Enable_VM" type="radio" value="On" CHECKED>
																						<label for="Voicemail_Enable_VMOn"><span></span></label>
																						<label class="labelText">On </label>
																				 
																				 
																						<input class="voicemail-required" id="Voicemail_Enable_VMOff" name="Voicemail_Enable_VM" type="radio" value="Off">
																						<label for="Voicemail_Enable_VMOff"><span></span></label>
																						<label class="labelText">off </label>
																					</div>	
                                                                			</div>
                                                                			<div class="col-md-6">
                                                                				<div class="form-group">
    																				<input class="checkbox-right" id="Voicemail_Send_All_Calls_to_VM" name="Voicemail_Send_All_Calls_to_VM" type="checkbox" value="Yes"/>
    																				<label for="Voicemail_Send_All_Calls_to_VM"><span></span></label>
    																				<label class="labelText">Send All Calls to VM: </label>																					
                                                                    			</div>
                                                                			</div>
                                                                		</div>
                                                                		</div>
																		
																		
																		<div class="row">
																		<div class="col-md-12">
                                                                			<div class="col-md-6">
                                                                				<div class="form-group">
    																				<input class="checkbox-right" id="Voicemail_Send_Busy_Calls_to_VM" name="Voicemail_Send_Busy_Calls_to_VM" type="checkbox" value="Yes" CHECKED/>
    																				<label for="Voicemail_Send_Busy_Calls_to_VM"><span></span></label>																					
                                                                    			 	<label class="labelText">Send Busy Calls to VM: </label>
                                                                				</div>
                                                                			</div>
                                                                			<div class="col-md-6">
                                                                				<div class="form-group">
    																				<input class="checkbox-right" id="Voicemail_Send_Unanswered_Calls_to_VM" name="Voicemail_Send_Unanswered_Calls_to_VM" type="checkbox" value="Yes" CHECKED/>
    																				<label for="Voicemail_Send_Unanswered_Calls_to_VM"><span></span></label>
    																				<label class="labelText">Send Unanswered Calls to VM: </label>																					
                                                                    			</div>
                                                                			</div>
                                                                		</div>
                                                                		</div>
                                                                		
																	</div>

																	<div class="well">
																	
																		<div class="row">
																		<div class="col-md-12">
                                                                			<div class="col-md-6">
                                                                				<div class="form-group">
                                                                    				<label class="labelText">Use Unified Messaging: </label>
    																				<input class="form-control blf-required readonly" id="Voicemail_Use_Unified_Messaging" name="Voicemail_Use_Unified_Messaging" disabled
																					       value="Yes"/>
    																				<label for="Voicemail_Use_Unified_Messaging"><span></span></label>																					
                                                                    			</div>
                                                                			</div>
                                                                			
                                                                			<div class="col-md-6">
                                                                				<div class="form-group">
    																				<input class="checkbox-right" id="Voicemail_Use_Phone_MWI" name="Voicemail_Use_Phone_MWI" type="checkbox" value="Yes" CHECKED/>
    																				<label for="Voicemail_Use_Phone_MWI"><span></span></label>																					
                                                                    			 	<label class="labelText">Use Phone MWI: </label>
                                                                				</div>
                                                                			</div>
                                                                		</div>
                                                                		</div>
																	</div>

																	<div class="well">
																	
																		<div class="row">
																		<div class="col-md-12">
                                                                			<div class="col-md-6">
                                                                				<div class="form-group">
    																				<input class="checkbox-right" id="Voicemail_Email_Message_Carbon_Copy" name="Voicemail_Email_Message_Carbon_Copy" type="checkbox" value="Yes"/>
    																				<label for="Voicemail_Email_Message_Carbon_Copy"><span></span></label>
    																				<label class="labelText">Email Message Carbon Copy: </label>																			
                                                                    			</div>
                                                                			</div>
                                                                			
                                                                			<div class="col-md-6">
                                                                				<div class="form-group">
                                                                					<label class="labelText">Carbon Copy Address: </label>
    																				<input class="form-control" id="Voicemail_Carbon_Copy_Address" name="Voicemail_Carbon_Copy_Address" type="email" placeholder="Email"/>
    																				<label for="Voicemail_Carbon_Copy_Address"><span></span></label>																					
                                                                				</div>
                                                                			</div>
                                                                		</div>
																	</div>
																	</div>

																	<div class="well">
																	
																		<div class="row">
																		<div class="col-md-12">
                                                                			<div class="col-md-6">
                                                                				<div class="form-group">
    																				<input class="checkbox-right" id="Voicemail_Transfer_on_0" name="Voicemail_Transfer_on_0" type="checkbox" value="Yes"/>
    																				<label for="Voicemail_Transfer_on_0"><span></span></label>
    																				<label class="labelText">Transfer on '0': </label>																				
                                                                    			</div>
                                                                			</div>
                                                                			
                                                                			<div class="col-md-6">
                                                                				<div class="form-group">
                                                                					<label class="labelText">Transfer on '0' DN: </label>
    																				<input class="form-control" id="Voicemail_Transfer_on_0_DN" name="Voicemail_Transfer_on_0_DN" type="text" value="" placeholder="DN"/>
    																				<label for="Voicemail_Transfer_on_0_DN"><span></span></label>																					
                                                                    			 
                                                                				</div>
                                                                			</div>
                                                                		</div>
                                                                		</div>
                                                                		
                                                                		<div class="row">
                                                                		<div class="col-md-12">
                                                                			<div class="col-md-6">
                                                                				<div class="form-group">
                                                                					<label class="labelText">Mailbox Limit: </label>
                                                                    				<div class="dropdown-wrap oneColWidth">
																					<select class="form-control voicemail-required" name="Voicemail_Mailbox_Limit">
																						<option>Use Group Default</option>
																						<option>10</option>
																						<option>20</option>
																						<option>30</option>
																						<option>40</option>
																						<option>50</option>
																						<option>60</option>
																						<option>70</option>
																						<option>80</option>
																						<option>90</option>
																						<option>100</option>
																						<option>200</option>
																						<option>300</option>
																						<option>400</option>
																						<option>500</option>
																						<option>600</option>
																						<option>700</option>
																						<option>800</option>
																						<option>900</option>
																					</select>
																					</div>	
                                                                    			</div>
                                                                			</div>
                                                                			</div>
                                                                			
                                                                			
                                                                		</div>
                                                                		
																		
																	</div>

																</div>
															</div>

														</div>

														<div class="panel panel-default panel-forms generateDiv overPanel" id="filterUsersDiv">

															<div class="panel-heading" role="tab" id="userFilterTitle">
																<h4 class="panel-title">
																	<a class="accordion-toggle accordion-toggle-full-width" role="button" data-toggle="collapse" data-parent="#generateAccordion"
																	   href="#userFilterDiv" aria-expanded="true"
																	   aria-controls="userFilterDiv">User Options</a>
																</h4>
															</div>

															<div id="userFilterDiv" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="userFilterTitle">
																<div class="panel-body" style="padding: 0;">
																	
																	<div class="form-group"></div>
<!-- 																	<div class="row-fluid"> -->
<!-- 																		<button id="infoIconES" href="#userFilter_notes" class="btn btn-link no-focus" title="Instructions" data-toggle="collapse"><i -->
<!-- 																					class="glyphicon glyphicon-info-sign"></i></button> -->
<!-- 																		<div id="userFilter_notes" class="collapse"> -->
<!-- 																			<div class="alert alert-info" role="alert"> -->
<!-- 																				Please configure if you wish for users CURRENTLY in the system to be in the excel sheet -->
<!-- 																				generated, or if you want a BLANK excel sheet, or if you want SOME users to be in the excel sheet. -->
<!-- 																			</div> -->
<!-- 																		</div> -->
<!-- 																	</div> -->

																	<div class="row formWidthEx">
                                										<!-- <div class="col-md-12">-->
                                										<div class="col-md-12">
                                											<div class="form-group">
                                												<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="Please configure if you wish for users CURRENTLY in the system to be in the excel sheet generated, or if you want a BLANK excel sheet, or if you want SOME users to be in the excel sheet."><img src="images/NewIcon/info_icon.png"></a>
                                											</div>
                                										</div>
                                    								</div>

																	<div class="row">
																		<div class="col-md-12">
																			<div class="col-md-12 form-group">
																			<label class="labelText">Generate Users Info: </label>
																			<br/>
																			<div class="dropdown-wrap">
																			 <select name="filterUsers" id="filterUsers" class="form-control" style="width: 100% !important;">
																					<option value="No">No Users</option>
																					<option value="All">All Users</option>
																					<option <?php echo $showFiltersByDefault ? "SELECTED" : "" ?>
																							value="FilterUsers">
																						Filter Users
																					</option>
																				</select>
																			</div>	
																		</div>
																		</div>
																		
																	</div>
																	
																	
																</div>
															</div>

														</div>

<!-- 													</div> -->

												</div>

											</div>


										</div>
</div>
									</form>

								</div>

								<!-- Filter Users Start -->
								<div class="">
									<div class="row testingClass">

										<?php
										$collapseFiltersByDefault = false;
										$need_user_table = true;
										include_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/userFilters/index.php");
										?>

									</div>
								</div>
								<!-- Filter Users End -->

								<div class="row">
									<div class="generateDiv" id="downloadBtnDiv">
										<div class="form-group">
											<label class=""></label>

											<div class="alignBtn">
												<div class="express_sheet_form_validation_summary error text-center" style="display: none;"></div>
												<input class="btn" type="button" name="downloadExcel" id="downloadExcelBtn" value="Generate"/>
											</div>
										</div>
									</div>

									<div class="generateDiv formWidthEx" id="downloadsDiv" style="display: none;">
										<div class="form-group">
											<div class="alignBtn">
												<div class="progress" id="downloadProgressBarDiv">
													<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
													     style="width: 0;" id="download_progress_bar">0%
													</div>
												</div>
												<div id="downloadExcelLinkDiv" class="alignBtn" style="display: none;color: #6ea0dc;">
												<a id="downloadExcelLink"><img src="images/icons/download_excel.png" data-alt-src="images/icons/download_excel_over.png"/><br><span class="labelTextGrey">Download<br>Excel File</span></a>
<!-- 													<a id="downloadExcelLink" class="btn-averistar">Download Excel File</a> -->
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
</div>
							<!-- PROVISION TAB -->
							<div class="tab-pane fade" id="tab2default">
								<h2 class="expressSheetHeadText">Provision Express Sheets
									<small>&nbsp;&nbsp;<span class="glyphicon glyphicon-open" aria-hidden="true"></span></small>
								</h2>

								<div class="row-fluid formWidthEx">
							<button id="infoIconES" href="#provision_notes" class="btn btn-link no-focus" title="Instructions" data-toggle="collapse" style="margin-left:5px">
									<img src="images/NewIcon/info_icon.png"></button>
									<div id="provision_notes" class="collapse">
										<div class="alert alert-info listStyleType" role="alert">
											Once you have edited an excel sheet that the system generated for you, do the following:
											<ol style="padding: 0 10px;">
												<li>CHOOSE the type of sheet you want to upload.</li>
												<li>SELECT the file from your computer.</li>
												<li>WAIT for the file to upload and for the system to check for any issues.</li>
												<li>CORRECT any issues the system found, and re-upload the file.</li>
												<li>SUBMIT the changes once it passes the system check.</li>
											</ol>
										</div>
									</div>
								</div>

								<form class="">

									<input type="hidden" name="modeOption" class="modeOption" value="Provision"/>

									<div id="provisionFormSettings">
										
										<div class="row formWidthEx">
											<div class="col-md-6">
												<div class="form-group">
												<label class="labelText">Select Type: </label>
												<span class="required">*</span>
												<br/>
												<div class="dropdown-wrap oneColWidth">
												 <select name="expressSheetType" id="provisionExpressSheetType" class="form-control valid" aria-invalid="false">
														<option value="Users">Users</option>
														<?php
														if ($_SESSION["permissions"]["changeSCA"] == "1") {
															?>
															<option value="SCA">SCA</option>
															<?php
														}
														?>
														<?php
														if ($_SESSION["permissions"]["changeblf"] == "1") {
															?>
															<option value="BLF">BLF</option>
															<?php
														}
														?>
														<option value="Voicemail">Voicemail</option>
													</select>
											</div>
											</div>
											</div>
											
											<div class="col-md-6">
												<div class="form-group" style="color:#6ea0dc">
    												<label class="labelText">Upload File: </label>
    												<span class="required">*</span>
    												<br/>
    												 	<input type="file" class="inputfile" id="provisionFile" name="file" style="width:87px;max-width: 82px !important;" onclick="this.value = null;" onchange="handle_files(this.files, 'provision')"/>
												</div>
											</div>
											
										</div>


										<div class="row provisionDiv" id="uploadBtnDiv" style="display: none;">
											<div class="form-group">
												<label class="col-sm-3 control-label"></label>
												<div class="col-sm-1"></div>
												<div class="col-xs-7">
													<input type="button" name="uploadExcel" id="uploadExcelBtn" value="Upload">
												</div>
											</div>
										</div>

										<div class="formWidthEx" id="progress_div" style="display: none;">
											<div id="progress_percentage" style="text-align: center;">0%</div>
											<div class="progress">
												<div id="provision_progress_bar"
												     class="progress-bar progress-bar-striped active"
												     role="progressbar"
												     aria-valuenow="0" aria-valuemin="0"
												     aria-valuemax="100" style="width: 0;">
												</div>
											</div>
										</div>
										<div id="excelData"></div>

									</div>
								</form>

							</div>

							<!-- SYNCRONIZE TAB -->
							<div class="tab-pane fade" id="tab3default">
								<h2>Synchronize
									<small>&nbsp;&nbsp;<span class="glyphicon glyphicon-random" aria-hidden="true"></span></small>
								</h2>

								<div class="row-fluid">
									<button id="infoIconES" href="#synchronize_notes" class="btn btn-link no-focus" title="Instructions" data-toggle="collapse">
									<img src="images/NewIcon/info_icon.png"></button>
									<div id="synchronize_notes" class="collapse">
										<div class="alert alert-info" role="alert">
											Please synchronize your Express Sheets to bring them up to date.
										</div>
									</div>
								</div>

								<div id="synchronizeFormSettings" style="display: none;">

									<div class="row formspace">
										<div class="form-group required">
											<label class="col-sm-3 control-label">Upload File: </label>
											<div class="col-sm-1"></div>
											<div class="col-sm-7">
												<input type="file" class="form-control" id="synchronizeFile" name="file" onchange="handle_files(this.files, 'synchronize')"/>
											</div>
										</div>
									</div>

									<div id="synchronize_progress_div" style="display: none;width: 100%;">
										<div id="synchronize_progress_percentage" style="text-align: center;">0%</div>
										<div class="progress">
											<div id="synchronize_progress_bar"
											     class="progress-bar progress-bar-striped progress-bar-animated"
											     role="progressbar"
											     aria-valuenow="0" aria-valuemin="0"
											     aria-valuemax="100" style="width: 0;">
											</div>
										</div>
									</div>

								</div>
							</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>


<div id="userData"></div>
</div><!-- express main content div closed -->
<script>

    var ex_form_validator;

    $(document).ready(function () {

    	var sourceSwapImage = function() {
            var thisId = this.id;
            var image = $("#"+thisId).find('img');
            var newSource = image.data('alt-src');
            image.data('alt-src', image.attr('src'));
            image.attr('src', newSource);
        }
        
		$("#downloadExcelLink").hover(sourceSwapImage, sourceSwapImage);

        ex_form_validator = $("#expressSheetsForm").validate({
            invalidHandler: function () {
                $(".express_sheet_form_validation_summary").show();
                $(".express_sheet_form_validation_summary").text("There are errors on the page. " + ex_form_validator.numberOfInvalids() + " field(s) are invalid.");
            }
        });

        $("#expressSheetType").change(function () {

            var expressSheetTypeVal = $("#expressSheetType").val();
            var filterUsersVal = $("#filterUsers").val();

            $(".express_sheet_form_validation_summary").hide();
            showOptions();
            ex_form_validator.resetForm();
            $("#expressSheetsForm")[0].reset();
            $("#expressSheetType").val(expressSheetTypeVal);
            $("#filterUsers").val(filterUsersVal);
        });


        $("#filterUsers").change(function () {

            if ($(this).val() === "FilterUsers") {
                $("#filterUsersValuesRow").show();
                $("#filterUsersValuesRow").addClass("filterUsersActive");
                $("#usersTableDiv").show();
                //updateSelectedUsersCount();
                apply_active_filters();
            } else {
                $("#filterUsersValuesRow").hide();
                $("#usersTableDiv").hide();
            }
        });
        

        

        $("#downloadExcelBtn").click(function () {

            $(".express_sheet_form_validation_summary").hide();

            var expressForm = $("#expressSheetsForm");

            var expressSheetTypeValue = $("#expressSheetType").val();

            if (expressSheetTypeValue === 'Users') {
                $("#groupInfoConfigUsers").collapse('show');
            } else if (expressSheetTypeValue === 'SCA') {
                $("#groupInfoConfigSCA").collapse('show');
            } else if (expressSheetTypeValue === 'BLF') {
                $("#groupInfoConfigBLF").collapse('show');
            } else if (expressSheetTypeValue === 'Voicemail') {
                $("#groupInfoConfigVoicemail").collapse('show');
            }

            ex_form_validator.form();

            if (expressForm.valid()) {

                $(this).hide();
                $(".express_sheet_form_validation_summary").text("");

                $("#downloadExcelLinkDiv").hide();
                $("#download_progress_bar").css('width', "0");
                $("#download_progress_bar").html("0%");
                $("#downloadsDiv").show();
                $("#downloadProgressBarDiv").show();

                var dataToSend = "json=1&" + expressForm.serialize();

                if ($("#filterUsers").val() == "FilterUsers") {
                    var users_selected = $(".DTFC_LeftBodyWrapper .users_selected:checkbox:checked").serialize();
                    dataToSend += "&" + users_selected;
                }

//                 pendingProcess.push("Generate Express Sheets");
                $.ajax({
                    type: "POST",
                    url: "/Express/expressSheets/generate.php",
                    data: dataToSend,
                    dataType: 'json',
                    success: function (result) {
                        if (result.success) {
                            if (result.download_id) {
                                var download_id = result.download_id;
                                setTimeout(function () {
                                    getDownloadStatus(download_id, 5);
                                }, 1000);
                            } else {
                                $("#downloadsDiv").html('Failed');
                                $(this).show();
                            }
                        } else {
                            $("#downloadsDiv").html('Failed');
                            $(this).show();
                        }
                    }
                });

            }

        });
        var checkUserLength = function() {
    		var userCount = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").length;
    		var userCountChecked = $(document).find(".DTFC_LeftBodyWrapper table tbody tr td input[type=checkbox]").filter(":checked").length;
    		userCount ? $("#selectAllDiv").show() : $("#selectAllDiv").hide();
    		userCountChecked ? $("#gwModsLabel").show() : $("#gwModsLabel").hide();
    		var checkVal = userCount == userCountChecked ? true: false;
    		$("#selectAllChk").prop("checked", checkVal);
    		selectAllPermission = checkVal;
    	}

        $(document).on("click", ".user_selected_chk",  function(){debugger;
            updateSelectedUsersCount();
        });

        showOptions();
        limit();
        updateSelectedUsersCount();


		<?php
		if($showFiltersByDefault) {
		?>
        apply_active_filters();
		<?php
		}
		?>

    });

 
    function updateSelectedUsersCount() {

        var count = 0;
        var selected_users = $(document).find("table.DTFC_Cloned tbody tr td input.users_selected").filter(':checked');
        if (selected_users) {
        	count = selected_users.length;
//          count = selected_users.filter(':visible').length;
        }
        /*
        if(count==0)
        {
            var selected_users_new = $("table#allUsersTableEx tbody td input.users_selected").filter(':checked');
            if (selected_users_new) {
                count += selected_users_new.filter(':visible').length;
            }
        }*/
        $("#users_selected_count").html(count + " Users selected.");
    }

    function getDownloadStatus(download_id, progress) {
    	pendingProcess.push("Generate Express Sheets");
    	
        $.getJSON("/Express/expressSheets/downloadStatus.php?download_id=" + download_id + "&_t=" + new Date().getTime(), function (data) {

        	if(foundServerConErrorOnProcess(JSON.stringify(data), "Generate Express Sheets")) {
				return false;
     		}
            if (data.success) {
                if(data.failed === 1) {
                    $(".express_sheet_form_validation_summary").text("Generate failed. Please contact Express Administrator.");
                    $(".express_sheet_form_validation_summary").show();
                    $("#downloadProgressBarDiv").hide();
                    $("#downloadExcelBtn").show();

                } else {
                    if (data.d_progress === 100) {
                        $("#downloadExcelLink").prop("href", '/Express/expressSheets/downloadStatus.php?download=1&download_id=' + download_id);
                        $("#downloadExcelLinkDiv").show();
                        $("#downloadProgressBarDiv").hide();
                        $("#downloadExcelBtn").show();
                        //$("#downloadsDiv").html("<a href='/Express/expressSheets/downloadStatus.php?download=1&download_id=" + download_id + "'>Download</a>");
                    } else {
                        if (progress < 95) {
                            if (progress < data.d_progress) {
                                progress = data.d_progress;
                            } else {
                                if (progress > 75) {
                                    progress += 1;
                                } else {
                                    progress += 2;
                                }
                            }
                            $("#download_progress_bar").css('width', progress + "%");
                            $("#download_progress_bar").html(progress + "%");
                        }

                        setTimeout(function () {
                            getDownloadStatus(download_id, progress);
                        }, 2000);
                    }
                }
            }  else {
                alert("Generate Failed");
                $("#downloadExcelBtn").show();
                $("#downloadProgressBarDiv").hide();
            }

        }).fail(function() {
            setTimeout(function () {
                getDownloadStatus(download_id, progress);
            }, 1000);
        });

    }

    function showOptions() {

        $("#provisionFile").val('');
        $("#excelData").html("");

        //if ($("#modeOptionGenerate").is(":checked")) {

        //$("#provisionFormSettings").hide();
        //$("#synchronizeFormSettings").hide();

        $(".generateDiv").hide();
        $("#generateFormSettings").show();
        $("#expressSheetGroupInfo").hide();

        $(".users-required").prop("required", false);
        $(".sca-required").prop("required", false);
        $(".blf-required").prop("required", false);
        $(".voicemail-required").prop("required", false);

        var expressSheetTypeValue = $("#expressSheetType").val();
        if (expressSheetTypeValue === 'Users') {
            $("#expressSheetSCA").hide();
            $("#expressSheetBLF").hide();
            $("#expressSheetVoicemail").hide();
            $("#expressSheetUsers").show();
            $("#filterUsersDiv").show();
            $("#downloadBtnDiv").show();
            $(".users-required").prop("required", true);
            //$("#userFilterDiv").collapse({"toggle": true, 'parent': '#generateAccordion'});
            $("#groupInfoConfigUsers").collapse('show');
        } else if (expressSheetTypeValue === 'SCA') {
            $("#expressSheetUsers").hide();
            $("#expressSheetBLF").hide();
            $("#expressSheetVoicemail").hide();
            $("#expressSheetSCA").show();
            $("#filterUsersDiv").show();
            $("#downloadBtnDiv").show();
            $(".sca-required").prop("required", true);
            //$("#userFilterDiv").collapse({"toggle": true, 'parent': '#generateAccordion'});
            $("#groupInfoConfigSCA").collapse('show');
        } else if (expressSheetTypeValue === 'BLF') {
            $("#expressSheetUsers").hide();
            $("#expressSheetSCA").hide();
            $("#expressSheetVoicemail").hide();
            $("#expressSheetBLF").show();
            $("#filterUsersDiv").show();
            $("#downloadBtnDiv").show();
            $(".blf-required").prop("required", true);
            //$("#userFilterDiv").collapse({"toggle": true, 'parent': '#generateAccordion'});
            $("#groupInfoConfigBLF").collapse('show');
        } else if (expressSheetTypeValue === 'Voicemail') {
            $("#expressSheetUsers").hide();
            $("#expressSheetSCA").hide();
            $("#expressSheetBLF").hide();
            $("#expressSheetVoicemail").show();
            $("#filterUsersDiv").show();
            $("#downloadBtnDiv").show();
            $(".voicemail-required").prop("required", true);
            //$("#userFilterDiv").collapse({"toggle": true, 'parent': '#generateAccordion'});
            $("#groupInfoConfigVoicemail").collapse('show');
        }

        /*
		} else if ($("#modeOptionProvision").is(":checked")) {

			$("#generateFormSettings").hide();
			$("#synchronizeFormSettings").hide();
			$("#provisionFormSettings").show();

		} else if ($("#modeOptionSynchronize").is(":checked")) {

			$("#generateFormSettings").hide();
			$("#provisionFormSettings").hide();
			$("#synchronizeFormSettings").show();

		}
		*/

    }


    //Provision
    function handle_files(files, upload_type) {
        for (i = 0; i < files.length; i++) {
            file = files[i];
            var form_data = new FormData();
            form_data.append('file', file);

            if (upload_type === "synchronize") {

                var expressSheetTypeValue = $("#synchronizeExpressSheetType").val();

                form_data.append('expressSheetType', expressSheetTypeValue);

                $("#synchronize_progress_bar").css('width', "0%");
                $("#synchronize_progress_percentage").html("Uploading 0%");
                $("#synchronize_progress_div").show();

                $("#synchronizeFile").prop("disabled", true);
                $("#synchronizeExpressSheetType").prop("disabled", true);

                var url = "expressSheets/synchronize.php";

                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();

                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $("#synchronize_progress_bar").css('width', percentComplete + "%");
                                $("#synchronize_progress_percentage").html("Uploading ... " + percentComplete + "%");
                                if (percentComplete == 100) {
                                    $("#synchronize_progress_percentage").html("Uploaded 100%. Synchronizing the file ...");
                                }
                            }
                        }, false);

                        return xhr;
                    },
                    url: url,
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'POST',
                    success: function (result) {
                        $("#synchronize_progress_div").hide();
                        $("#synchronizeFile").prop("disabled", false);
                        $("#synchronizeExpressSheetType").prop("disabled", false);

                    }
                });

            } else if (upload_type === "provision") {
            	pendingProcess.push("Provision Express Sheets");
                var expressSheetTypeValue = $("#provisionExpressSheetType").val();

                $("#excelData").html("");

                $("#provision_progress_bar").css('width', "0%");
                $("#progress_percentage").html("Uploading 0%");
                $("#progress_div").show();

                $("#provisionFile").prop("disabled", true);
                $("#provisionExpressSheetType").prop("disabled", true);

                var url = "expressSheets/provisionUsers.php";

                if (expressSheetTypeValue === 'Users') {
                    url = "expressSheets/provisionUsers.php";

                } else if (expressSheetTypeValue === 'SCA') {
                    url = "expressSheets/provisionSCA.php";

                } else if (expressSheetTypeValue === 'BLF') {
                    url = "expressSheets/provisionBLF.php";

                } else if (expressSheetTypeValue === 'Voicemail') {
                    url = "expressSheets/provisionVoicemail.php";
                }

                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();

                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $("#provision_progress_bar").css('width', percentComplete + "%");
                                $("#progress_percentage").html("Uploading ... " + percentComplete + "%");
                                if (percentComplete == 100) {
                                    $("#progress_percentage").html("Uploaded 100%. Parsing the file ...");
                                }
                            }
                        }, false);

                        return xhr;
                    },
                    url: url,
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'POST',
                    success: function (result) {
                    	if(foundServerConErrorOnProcess(result, "Provision Express Sheets")) {
        					return false;
                      	}
                        $("#excelData").html(result);
                        $("#progress_div").hide();
                        $("#provisionFile").prop("disabled", false);
                        $("#provisionExpressSheetType").prop("disabled", false);

                    }
                });

            }

        }
    }

    //function needs to be accessed by usersTable.php as well
    function limit() {
    }

</script>
<style>
	.error, .error text {
		color: red;
	}
	 td, td > .userIdVal {  white-space: normal; word-break: break-all; }
	
/*	input[type=checkbox] {
    display: block;
    width: 0 !important;
    position: absolute;
}*/
</style>

