<?php
/**
 * Created by Dipin Krishna.
 * Date: 10/25/17
 * Time: 4:31 PM
 */
?>
<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
checkLogin();

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/config.php");

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_SESSION['sp']) && isset($_SESSION['groupId']) && $_SESSION['groupId'] != "" && isset($_POST['expressSheetType'])) {

	if (file_exists($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

		$tmp_file_name = tempnam(sys_get_temp_dir(), 'BULK');

		$file_type = $_FILES['file']['type'];

		$path = $_FILES['file']['name'];
		$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

		//print_r($_FILES['file']);

		move_uploaded_file(
			$_FILES['file']['tmp_name'], $tmp_file_name
		);
	}

	if(file_get_contents($tmp_file_name)) {

		$expressSheetType = $_POST['expressSheetType'];
		$filterUsers = $_POST['filterUsers'];

		$sp = $_SESSION['sp'];
		$groupId = $_SESSION['groupId'];

		if ($expressSheetType == 'Users') {

			$revision = $express_sheet_revision['Users'];

			$file = $_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/expressSheets/templates/User-Rel-$revision.xlsx";

			require_once $_SERVER['DOCUMENT_ROOT'] . '/../phpToExcel/Classes/PHPExcel/IOFactory.php';
			require_once $_SERVER['DOCUMENT_ROOT'] . '/../phpToExcel/Classes/PHPExcel.php';

			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			//$objReader->setLoadSheetsOnly('Group');
			$objReader = $objReader->load($file);

			//Get 'Rev'
			$objReader->setActiveSheetIndex(0);

			if ($objReader->getActiveSheet()->getCellValue('C12') == "Rev.$revision") {

			} else {
				echo "This Express Sheet Version is not supported";
				exit;
			}
		}
		
	}
}
?>
