<?php
/**
 * Created by Sollogics Jeetesh.
 * Date: 01/23/18
 * Time: 04:17 PM
 */

require_once("/var/www/lib/broadsoft/adminPortal/getUserErrorExcelSheetReport.php");
//require_once("/var/www/phpToExcel/Classes/PHPExcel.php");
//require_once("/var/www/phpToExcel/Classes/PHPExcel/IOFactory.php");
require_once $_SERVER['DOCUMENT_ROOT'] . "/../vendor/phpoffice/phpspreadsheet/src/Bootstrap.php";

$postArray["columnArray"] = explode(",", $_POST["columnArray"]);
$postArray["valueArray"] = json_decode($_POST["valueArray"]);
$postArray["errorArray"] = json_decode($_POST["errorArray"]);


//echo "<pre>"; print_r($postArray); die;
$filename= "DownloadErrorVMExpressSheetData.xlsx";
$sheetObject = new GetUserErrorExcelSheetReport;
$result = $sheetObject->generateErrorReportXLS($postArray, $filename);

?>