<?php
$settings = parse_ini_file("/var/www/adminPortal.ini", TRUE);

//MySQL database connections
require_once("db.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");

$systemConfig = new DBSimpleReader($db, "systemConfig");
$ldapAuthentication = $systemConfig->get("ldapAuthentication");

$errorMessage = null;
$userName = "";
$success = false;

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['userName'])) {
	$userName = trim($_POST['userName']);
	$query = $db->prepare('SELECT id, userName, firstName, lastName, emailAddress, superUser, use_ldap_authentication, disabled FROM users WHERE userName = ? LIMIT 1');
	if ($query->execute(array($userName))) {

		if ($user = $query->fetch(PDO::FETCH_OBJ)) {

			$details = array();

			if ($user->disabled || ($ldapAuthentication == "true" && $user->superUser != 2 && $user->use_ldap_authentication == 1)) {

				$errorMessage = "Please contact your Admin.";

				$details['Message'] = $errorMessage;
				$details['User Disabled'] = $user->disabled == '1' ? 'Yes' : 'No';
				$details['Super User (Config)'] = $user->superUser == '2' ? 'Yes' : 'No';
				$details['Use LDAP Authentication'] = $user->use_ldap_authentication == '1' ? 'Yes' : 'No';
				$details['LDAP Authentication Enabled'] = $ldapAuthentication == 'true' ? 'Yes' : 'No';

			} else {

				$token = sha1(uniqid(bin2hex(openssl_random_pseudo_bytes(32)), true));

				$update_temporary_token = $db->prepare('UPDATE users SET temporary_token = ? WHERE userName = ? LIMIT 1');
				$update_temporary_token->execute(array($token, $userName));

				$link = "https://" . $_SERVER['HTTP_HOST'] . "/Express/reset_password.php?token=" . $token;
				$to = $user->emailAddress;

				$subject = 'Password Recovery';

				$message = "<a href=\"$link\">Click here</a> to reset password.  If the link does not work copy this url: <a href=\"$link\">$link</a>  and paste it in your browser.";

				send_email($to, $subject, $message);

				$success = true;

				$details['Password Reset Email'] = "Sent to $to";
			}

			//Add Admin Log Entry - Start
			require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");
			$adminLogs = new adminLogs();
			$log = array(
				'adminUserID' => $user->id,
				'eventType' => 'FORGOT_PASSWORD',
				'adminUserName' => $user->userName,
				'adminName' => $user->firstName . ' ' . $user->lastName,
				'updatedBy' => $user->id,
				'details' => $details
			);
			$adminLogs->addLogEntry($log);
			//Add Admin Log Entry - End

		} else {
			$errorMessage = "User not found.";
		}
	} else {
		$errorMessage = "Please try again later.";
	}
}

if (isset($_SESSION["INVALID_TOKEN"])) {
	$errorMessage = "The link has expired.";
}

require_once("header.php");
?>
<style>
	.mainHeader{
		display:none;
	}
	.blueSeparator{
		z-index:0;
	}
	.groupNameSpan{display:none;}
	
</style>
<div class="leftMenu"></div>
<div id="mainBody">

	<div class="login-bg">
		<div class="fadeInUp" id="bodyForm" style="height: fit-content;">
		<img src="images/icons/express_nuovo_logo_login2.png" />

			<h4 class="centerDesc"><strong style="color:#FFF">Password Reset</strong></h4>
			
			<?php

			if ($success) {
				?>
				<div class="resetPassDiv">
					Reset password email has been sent.<br/>
					</div>
					<text>If you do not see an email from support@averistar.com within a few minutes, please check your Spam folder.</text>
				 
				 
				<?php
			} else {
				?>
				<div class="resetPassDiv">
				<small>Please enter your username and click submit. We will send you a link to reset your password.</small>
				 
				</div>
				<?php
				if ($errorMessage) {
					?>
					<div style="clear:both;text-align:center;color:red;"><?php echo $errorMessage; ?></div>
					<?php
				}
				?>
				<div style="width:100%;text-align:center;">
					<form name="forgot_password_form" id="login" method="POST">
						<input placeholder="Username" type="text" name="userName" id="userName" value="<?php echo $userName ?>" class="inptText"/>
				<div class="forgotSubmitDiv">
						<input type="submit" name="forgotSubmit" id="forgotSubmit" value="Submit">
				</div>
<div class="forgotPassDiv">
					<a href="index.php" class="m-t-mini">
						<small>Back to Login</small>
					</a>
				</div>
					</form>
					<div class="clr"></div>
				
				</div>
				<?php
			}
			?>
		</div>
		<div class="copyright">
			Copyright &copy; AveriStar All rights reserved.
		</div>
	</div>

</div>