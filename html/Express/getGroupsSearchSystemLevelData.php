<?php
/**
 * Created by Karl.
 * Date: 4/20/2017
 */
    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();

    $xmlinput = xmlHeader($sessionid, "GroupGetListInSystemRequest");
    $xmlinput .= xmlFooter();
    $response = $client->processOCIMessage(array("in0" => $xmlinput));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
    foreach ($xml->command->groupTable->row as $key => $value) {
        $groupId = strval($value->col[0]);

        if (isset($_SESSION["groups"]) && ! in_array($groupId, $_SESSION["groups"])) {
            continue;
        }

        //$searchData = $groupId;
        $searchData = $groupId . " ";
        if (strval($value->col[1])) {            
            $searchData .= '<span class="search_separator">-</span> ' . strval($value->col[1]);
        }
        //$_SESSION["groupsSearch"][$searchData] = $groupId;
        $allSearchGroups[$searchData] = $groupId;

        if (isset($allSearchGroups)) {
            ksort($allSearchGroups);
        }        
    }

    if (isset($allSearchGroups)) {
        $allSearchGroups1["All Groups"] = "All Groups";
        $totalSearchGroupArr = array_merge($allSearchGroups1, $allSearchGroups);
        foreach ($totalSearchGroupArr as $key => $value) {
            echo $key . ":";
        }
    }
  ?>

