<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();
?>
<script>
	$("#dialog").html("");
</script>
<?php
	switch($_POST["module"])
	{
		case "adminUser":
			include("adminUsers/index.php");
			break;
		case "bulk":
			include("bulk/index.php");
			break;
		case "cdrs":
			include("cdrs/cdr.php");
			break;
		case "siteManagement":
			include("siteReport/siteReport.php");
			break;
		case "siteReport":
		    include("serviceLicense/serviceLicense.php");
		    break;
		case "changeLog":
			include("changeLog/changes.php");
			break;
        case "deviceMgmt":
            include("deviceMgmt/index.php");
            break;
		case "dids":
			include("numbers/index.php");
			break;
		case "findMac":
			include("findMac/findMac.php");
			break;
		case "modAA":
		    $path = autoAttendantRouting($ociVersion);
		    include($path);
			break;
		case "modCallCenter":
			include("callCenters/ccMod.php");
			break;
		case "modCCD":
			include("customContactDirectories/ccdMod.php");
			break;
		//case "modGroup":
		//	include("group/groupMod.php");
		//	break;
		case "modHunt":
			include("huntgroups/hgMod.php");
			break;
		case "modSched":
			include("schedules/schedMod.php");
			break;
		case "registrations":
			include("registrations/index.php");
			break;
		case "tickets":
			include("tickets/main.php");
			break;
		case "userAdd":
			include("userAdd/userAdd.php");
			break;
		case "userMod":
			include("userMod/userMod.php");
			break;
		case "users":
			include("users/index.php");
			break;
		case "webTicket":
			include("webTicket/index.php");
			break;
		case "modGrpCallPickup":
			include("groupCallPickup/groupCallPickupAdd.php");
			break;
		case "groupModify":                        
			include("groupManage/groupModify/index.php");                        
			break;
		case "modAAA":
			include("AutoAttendantGroup/index.php");
			break;
		case "announcement":
		    $path = announcementRouting($ociVersion);
			include($path);
			break;
		case "modVdm":
			include("Vdm/index.php");
			break;
		case "modDevice":
		    include("Devices/index.php");
		    break;
		case "expressSheets":
			include("expressSheets/index.php");
			break;
		case "logout":
			unset($_SESSION);
			break;
		case "adminUserEnt":
		    include("adminUsersEnterprise/index.php");
		    break;
                case "groupAdminUser":
		    include("adminUsers/index.php");
		    break;
		case "adminLogs":
			include("adminLogs/index.php");
			break;
		case "usersEnterprise":
			include("usersEnterprise/index.php");
			break;
                case "entChangeLog":
			include("enterpriseChangeLog/changes.php");
                        break;
		case "cdrsEnterprise":
		    include("cdrsEnterprise/cdr.php");
			break;
		case "servicePackModule":
		    include("servicePacks/index.php");
		    break;
		case "Enterprises":
		    include("enterprise/index.php");
		    break;
		    
		    
	}
	
	function autoAttendantRouting($ociVersion){
	    $path = "";
	    if($ociVersion == "19"){
	        $path = "autoAttendant/aaMod.php";
	    }else{
	        $path = "autoAttendant21/aaMod.php";
	    }
	    return $path;
	}
	
	function announcementRouting($ociVersion){
	    $path = "";
	    if($ociVersion == "19"){
	        $path = "Announcement/index.php";
	    }else{
	        $path = "Announcement21/index.php";
	    }
	    return $path;
	}
	
?>
