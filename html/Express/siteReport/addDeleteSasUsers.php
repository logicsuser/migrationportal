<?php 
    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();
    require_once ("/var/www/lib/broadsoft/adminPortal/phoneProfiles/PhoneProfilesOperations.php");
    require_once("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
    require_once("/var/www/lib/broadsoft/adminPortal/customTagOperations/UserCustomTagOperations.php");
    require_once("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig/rebuildAndResetDeviceConfig.php");
    require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
    require_once("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
    require_once("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
    require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
    require_once("/var/www/html/Express/siteReport/GetCustomTags.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
    
    global $providerid, $groupId;
    $providerid = $_SESSION["sp"];
    $groupId = $_SESSION["groupId"];
    $allowedTags = Array('%SAS_IP%', '%SAS_NAME%', '%SBC_ADDRESS%', '%SBC_PORT%');
    
    $fileName = "../../../SASTestingUser/SASTestUsers.csv";
    $action = isset($_POST['action']) ? $_POST['action'] : "";
    $responseData = array();
    
    if($action == "isUserExist")
    {
        unset($_SESSION['userInfo']);
        $val = $_POST["searchVal"];
        $userId = isset($_SESSION["sasUsersAutoFill"][$val]) ? $_SESSION["sasUsersAutoFill"][$val] : "";
        //$responseData = "";
        if(isUserExist($val) != "true")
        {
            $responseData['Error'] = "User <b>".$val."</b> Does Not Exist";
        }
        else
        {
            $dOP = new DeviceOperations();
            $uCTO = new UserCustomTagOperations();
            $gCT = new GetCustomTags($dOP, $uCTO, $_SESSION["sp"], $_SESSION["groupId"]);
            $customTagError = $gCT->errorHandlingInSAS();
            $tagsFormAutoFill = $gCT->SASDialogData;
            
            $userOperationObj = new UserOperations();
            $userDetail = $userOperationObj->getUserDetail($userId);
            if(empty($userDetail['Error']))
            {
                $userDetail = $userDetail['Success'];
                $userDetail['sp'] = $userDetail['serviceProviderId'];
                $userVdmObj = new VdmOperations;
                $userStatus = $userVdmObj->getUserStatusDetail($userId);
                if(!empty($userStatus)) {
                    $userDetail['ipAddress'] = $userStatus['publicIPAddress'];
                } else {
                    $userDetail['ipAddress'] = "";
                }
                $_SESSION['userInfo'] = $userDetail;
            }
            
            $responseData['form'] = customTagForm($tagsFormAutoFill, $customTagError);
        }
        
        server_fail_over_debuggin_testing(); /* for fail Over testing. */
        
        echo json_encode($responseData); 
        die;
    }
    
    
    // addToSasTesting for Add user to SAS request
    if($action == "addToSasTesting")
    {
      
        $arrForCheckValidation = array();
        $allTagsAttay = "";
        $responseData = "";
        $searchValue = $_POST["searchVal"];
        
        foreach($_POST['tagsData'] as $key => $tagValue)
        {
            if(in_array('%'.$tagValue['name'].'%', $allowedTags))
            {
                if($tagValue['value'] != ""){
                    array_push($arrForCheckValidation, '%'.$tagValue['name'].'%');     //Array for check validation.
                    $allTagsAttay[$tagValue['name']] = $tagValue['value'];
                }
            }
            
        }
        
        $diffArray = array_diff($allowedTags, $arrForCheckValidation);
        if(count($diffArray) == 0)
        {
            if(! filter_var($allTagsAttay['SAS_IP'], FILTER_VALIDATE_IP)
                && ! filter_var($allTagsAttay['SBC_ADDRESS'], FILTER_VALIDATE_IP))
            {
                $responseData['Error'] =  "1. <b class='alignCenter'>". $allTagsAttay['SAS_IP']. "</b><span class='alignCenter'> is not a valid SAS_IP.</span> <br>";
                $responseData['Error'] .= "2. <b>". $allTagsAttay['SBC_ADDRESS']. "</b> is not a valid SBC_ADDRESS. <br>";
            }
            else if(! filter_var($allTagsAttay['SAS_IP'], FILTER_VALIDATE_IP))
            {
                $responseData['Error'] =  "1. <b class='alignCenter'>". $allTagsAttay['SAS_IP']. "</b><span class='alignCenter'> is not a valid SAS_IP</span>";
            }else if(! filter_var($allTagsAttay['SBC_ADDRESS'], FILTER_VALIDATE_IP))
            {
                $responseData['Error'] = "1. <b class='alignCenter'>". $allTagsAttay['SBC_ADDRESS']. "</b><span class='alignCenter'> is not a valid SBC_ADDRESS</span>";
            }
            else
            {
                $isExistTagDeleted = deleteTagIfTagExist($allowedTags);
                if(empty($isExistTagDeleted['Error'])){
                    $responseData = addToSasTesting($searchValue, $fileName, $allTagsAttay, $allowedTags);
                }else{
                    $responseData['Error'] = $isExistTagDeleted['Error'];
                }
            }
        }else{
           $responseData['Error'] = "<span class='alignCenter'style='text-align:center'> All fields are required.</span>";
        }
        
    }
    
    // deleteFromSasTesting for Delete user From SAS request
    else if($action == "deleteFromSasTesting")
    {
        $responseData = deleteFromSasTesting($_POST['dataToSend'], $fileName, $allowedTags);
    }
    
    $userData = Array();
    
    
    //Read CSV file
    if(file_exists($fileName))
    {
        $csvFileData = fopen($fileName,'r');
        while(($data = fgetcsv($csvFileData)) != FALSE)
        {
           if(trim($providerid) == $data[0] && $groupId == $data[1])
           {
                $userData[] = $data;
           }
        }
        fclose($csvFileData);
    }
    // Create Table
    $responseData['Table'] = createTableForCSVUsers($userData);
    
    print_r(json_encode($responseData)); 
    
    
//******************************************************************************************

    function isUserExist($searchValue)
    {
        $isUserExist = "false";
        if (isset($_SESSION["sasUsersAutoFill"]))
        {
            foreach ($_SESSION["sasUsersAutoFill"] as $key => $value)
            {
                if($key == $searchValue)
                {
                    $isUserExist = "true";
                    break;
                }
            }
        }
        return $isUserExist;
    }
    
    
    function addToSasTesting($searchVal, $fileName, $allTagsAttay)
    {
        global $providerid, $groupId;
        $response = "";
        if(isset($_SESSION['userInfo']))
        {
            $userInfo = $_SESSION['userInfo'];
            $customTagObj = new UserCustomTagOperations();
            foreach($allTagsAttay as $tagName => $tagValue)
            {
                if($tagValue != ""){
                    $createTag = $customTagObj->addDeviceCustomTag($providerid, $groupId, $userInfo['deviceName'], "%".$tagName."%", $tagValue);
                }
            }
            if(empty($createTag['Error']))
            {
                $phoneProfileRes = addSASPhoneProfileCustomTag($providerid, $groupId, $userInfo['deviceType'], $userInfo['deviceName']);
                if( !isset($phoneProfileRes['Error']) || empty($phoneProfileRes['Error']) ) {
                    $isCSVCreated = createCSVFile($userInfo, $fileName, $allTagsAttay);
                }
                if(empty($isCSVCreated['Error']))
                {
                     $isDone = rebuildAndResetDevice($userInfo['deviceName']);
                     addSiteManagementChangeLogUtil($allTagsAttay);
                    if(! empty($isDone['Error']))
                    {
                         $response['Error'] = $isDone['Error'];
                    }

                }
            }
            else
            {
                $response['Error'] = $createTag['Error'];
            }
        }
        else
        {
            $response['Error'] = $userDetail['Error'];
        }
        return $response;
    }


    
    function deleteFromSasTesting($dataToSend, $fileName, $allowedTags)
    {
        global $providerid, $groupId;
        
        $response = "";
        foreach($dataToSend as $key => $value)
        {
            if($value['name'] == 'userCheckBox')
            {
                $userData = explode('"&&"', $value['value']);
                $userId = $userData[0];
                $deviceName = $userData[1];
                $deviceType = $userData[2];
                $customTagObj = new UserCustomTagOperations();
                foreach($allowedTags as $key => $tagName)
                {
                   $isTagDeleted = $customTagObj->groupAccessDeviceDeleteCustomTag($providerid, $groupId, $deviceName, $tagName);
                }
                if(empty($isTagDeleted['Error']))
                {
                    $responsePhonePro = deleteSASPhoneProfileCustomTag($providerid, $groupId, $deviceType, $deviceName);
                    if( !isset($responsePhonePro['Error']) || empty($responsePhonePro['Error'])) {
                        $isDeletedFromCsvFile = deleteFromCSVFile($userId, $fileName);
                    }
                    
                    if(empty($isDeletedFromCsvFile['Error']))
                    {
                        $isDone = rebuildAndResetDevice($deviceName);
                        deleteSiteManagementChangeLogUtil($userId);
                        if(!empty($isDone['Error']))
                        {
                            
                            $response['Error'] = $isDone['Error'];
                        }
                    }else {
                        $response['Error'] = $isDeletedFromCsvFile['Error'];
                    }
                }
                else
                {
                    $response['Error'] = $isTagDeleted['Error'];
                }
            }
        }
        return $response;
    }

    
    
    function rebuildAndResetDevice($deviceName)
    {
        global $providerid, $groupId;
        $response = "";
        $rebuildAndRese = new RebuildAndResetDeviceConfig();
        $isRebuild = $rebuildAndRese->GroupCPEConfigRebuildDeviceConfig($providerid, $groupId, $deviceName);
        if(empty($isRebuild['Error']))
        {
            $isReset = $rebuildAndRese->GroupCPEConfigResetDevice($providerid, $groupId, $deviceName);
            if(! empty($isReset['Error']))
            {
                $response['Error'] = $isReset['Error'];
            }
        }
        return $response;
    }
    
    function deleteTagIfTagExist($allowedTags)
    {
        global $providerid, $groupId;
        $response = "";
        
        if(isset($_SESSION['userInfo']))
        {
            $userInfo = $_SESSION['userInfo'];
            $customTagObj = new UserCustomTagOperations();
            $returnedTagList = $customTagObj->getUserCustomTags($providerid, $groupId, $userInfo['deviceName']);
            $userAssignedTags = $returnedTagList['Success'];
            $userAssignedTags = array_intersect($userAssignedTags, $allowedTags);
            foreach($userAssignedTags as $tagkey => $tagValue)
            {
                $isTagDeleted = $customTagObj->groupAccessDeviceDeleteCustomTag($providerid, $groupId, $userInfo['deviceName'], $tagValue);
                if(! empty($isTagDeleted['Error'])){
                    $response .= $isTagDeleted['Error']. 'Error for device name'. $userInfo['deviceName'] ;
                }
            }
        }
        return $response;
    }
    
    
    function createCSVFile($userInfo, $fileName, $allTagsAttay)
    {
        $response = "";
//      $csvColumnList = array('spID', 'groupID', 'deviceName', 'userID', 'userLastName', 'userFirstName', 'phoneNumber', 'extension', 'startTime', 'deviceType', 'Device IP', '%SAS_IP%', '%SAS_NAME%', '%SBC_ADDRESS%', '%SBC_PORT%');
        $csvColumnList = array('Provider Id', 'Group ID', 'Device Name', 'User ID', 'Last Name', 'First Name', 'Phone Number', 'Extension', 'Start Time', 'Device Type', 'SAS_IP', 'SAS_NAME', 'SBC_ADDRESS', 'SBC_PORT', 'Device IP');
        if(!file_exists($fileName))
        {
            $file = fopen($fileName, 'w');
            fputcsv($file, $csvColumnList);
        }else
        {
            if(!checkColumnExists($fileName, $csvColumnList)) {
                createAllNewColumns($fileName, $csvColumnList);
            }
            $file = fopen($fileName, 'a+');
        }
        
        $user_CSV = array(
            $userInfo['serviceProviderId'], $userInfo['groupId'],
            $userInfo['deviceName'], $userInfo['userId'], $userInfo['lastName'],
            $userInfo['firstName'], $userInfo['callingLineIdPhoneNumber'],
            $userInfo['extension'], date("Y-m-d h:i:sa"), $userInfo['deviceType'],
            $allTagsAttay['SAS_IP'], $allTagsAttay['SAS_NAME'],
            $allTagsAttay['SBC_ADDRESS'], $allTagsAttay['SBC_PORT'], $userInfo['ipAddress']
        );
        
        $putValue = fputcsv($file, $user_CSV);
        fclose($file);
        return $response;
    }
    
    
    
    function deleteFromCSVFile($userId, $fileName)
    {
        $tempFileName = "../../../SASTestingUser/SASTestUsers_temp.csv";
        $response = "";
        if(file_exists($fileName))
        {
            $table = fopen($fileName,'r');
            $temp_table = fopen($tempFileName,'w');
            
            while (($data = fgetcsv($table, 1024)) !== FALSE){
                if($data[3] == $userId)
                {
                    continue;
                }
                fputcsv($temp_table,$data);
            }
            fclose($table);
            fclose($temp_table);
            rename($tempFileName, $fileName);
        }
        else
        {
            $response['Error'] = "<span class='alignCenter'>CSV File Does Not Exist</span>";
        }
        return $response;
    }
    
    
    function createTableForCSVUsers($userData)
    {
        $table = "";
        $userDn = new Dns();
        $userOperationObj = new UserOperations();
        if(count($userData) > 0)
        {
            $a = 0;
            foreach($userData as $key => $value){
                    $startTime = $value[8];
                    $deviceName = $value[2];
                    $userId = $value[3];
                    $userName = $value[5]." ".$value[4];
                    $ext = $value[7];
                    $deviceType = $value[9];
                    
                    $SAS_IP = $value[10];
                    $SAS_NAME = $value[11];
                    $SBC_ADDRESS = $value[12];
                    $SBC_PORT = $value[13];
                    
                    $lastName = $value[4];
                    $firstName = $value[5];
                    $deviceName = $value[2];
                    $deviceIP = isset($value[14]) ? $value[14] : "";
                    
                    $numberActivateResponse = $userDn->getUserDNActivateListRequest($userId);
                    if(empty($numberActivateResponse['Error']) && count($numberActivateResponse["Success"]) > 0){
                        $userPhoneNumber = $numberActivateResponse['Success'][0]['phoneNumber'];
                        //$phoneNumberStatus = $numberActivateResponse['Success'][0]['status'];
                    }else{
                        $userPhoneNumber= "";
                    }
                    
                    
                    $userDetail = $userOperationObj->getUserDetail($userId);
                    if(empty($userDetail['Error']))
                    {
                        if(isset($userDetail['Success']['lastName'])){
                            $lastName = $userDetail['Success']['lastName'];
                        }else{
                            $lastName = "";
                        }
                        if(isset($userDetail['Success']['firstName'])){
                            $firstName = $userDetail['Success']['firstName'];
                        }else{
                            $firstName = "";
                        }
                     
                        //$_SESSION['userInfo'] = $userDetail['Success'];
                    }
                    
                    $userIdLinkToModUser = "<a href='#' class='primaryUserModLink' data-phone='".$userPhoneNumber."' data-extension='".$ext."' id='" .$userId. "' data-lastname='" . $lastName . "' data-firstname='" . $firstName . "' >" . $deviceName . "</a>";
                    $table .=  "<tr>

    							<td class=\"thSno\"> 
<input type=\"checkbox\" name =\"userCheckBox\" id = \"userCheckBox".$a."\"  value = $userId\"&&\"$deviceName\"&&\"$deviceType  onclick=\"onCheckUser()\">
<label for = \"userCheckBox".$a."\"><span></span></label>

                                </td>
    							<td class=\"thsmallSasTest\">".$startTime."</td>
    							<td class=\"thsmallSasTest\">".$userName."</td>
    							<td class=\"thsmallSasTest\">".$userIdLinkToModUser."</td>
    							<td class=\"thsmallSasTest\">".$ext."</td>
    							<td class=\"thsmallSasTest\">".$deviceType."</td>
    							<td class=\"thsmallSasTest\">".$deviceName."</td>
								<td class=\"thsmallSasTest\">".$deviceIP."</td>
                                <td class=\"thsmallSasTest\">".$SAS_IP."</td>
    							<td class=\"thsmallSasTest\">".$SAS_NAME."</td>
    							<td class=\"thsmallSasTest\">".$SBC_ADDRESS."</td>
    							<td class=\"thsmallSasTest\">".$SBC_PORT."</td>

                                </tr>";
                    $a++;
            }
        }
        
        return $table;
        
    }
    
    function customTagForm($tagsFormAutoFill, $customTagError)
    {
        $SAS_IP =""; 
        $SAS_NAME="";
        $SBC_ADDRESS="192.168.1.1";
        $SBC_PORT="5060";
        $ipWarningMsg = "";
        $disableIP = "";
        $disableSAS_NAME = "";
        if( !empty((array) $customTagError)) {
            //$customTagError->errorType;
            $ipWarningMsg = "<span class='errorClass'>" .$customTagError->warningMsg. "</span>";
        } else {
                $SAS_IP = $tagsFormAutoFill['ipAddress'];
                $SAS_NAME = $tagsFormAutoFill['deviceName'];
                $disableIP = "nonePointer";
                $disableSAS_NAME = "nonePointer";
        }

       // if(empty($SBC_PORT)){$SBC_PORT = "5060";}
		$customTagForm = "";
        $customTagForm .= '<div id="dialogTagForm" class="dialogTagForm" title="Dialog Form">
                        <form action="" method="post" id="addSasUserForm">
	
							<div class="row">
								<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group">
										'. $ipWarningMsg .'
									</div>
								</div>
								</div>
                             </div>
		 
                            <div class="row">
								<div class="col-md-12">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for=""> SAS_IP :  <span class="required">*</span></label><br/>
											<input class="' .$disableIP. '" type="text" name="SAS_IP" id="SAS_IP" size="40" value="'.$SAS_IP.'">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for=""> SAS_NAME : <span class="required">*</span></label><br/>
											<input class="' .$disableSAS_NAME. '" type="text" name="SAS_NAME" id="SAS_NAME" size="40" value="'.$SAS_NAME.'">
										</div>
									</div>
								</div>
                        	</div>			
                        	<div class="row">	
								<div class="col-md-12">	
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="SBC_ADDRESS"> SBC_ADDRESS : <span class="required">*</span></label><br/>
											<input style="font-size: 15px;" type="text" name="SBC_ADDRESS" id="SBC_ADDRESS" size="40" value='.$SBC_ADDRESS.'>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="SBC_PORT">SBC_PORT: <span class="required">*</span></label><br/>
											<input style="font-size: 15px;" type="text" name="SBC_PORT" id="SBC_PORT" size="40" value='.$SBC_PORT.'>
										</div>
									</div>
								</div>
                        	</div>
                        </form>
            </div>';
        return $customTagForm;
    }
    
    /* changeLog event add new SiteReport
     *' Add Sas Testing User ' module name 
     *  
    */
    
    function addSiteManagementChangeLogUtil($changesArr) {
        $changeLogObj = new ChangeLogUtility($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
        $module = "Add Sas Testing User";
        $tableName = "sitemanagementAddChanges";
        $entity = $changesArr['SAS_NAME'];
        $changeResponse = $changeLogObj->changeLogAddUtility($module, $entity, $tableName, $changesArr);
    }
    
    /*
     * delele change log event 
     * show delete data into change log module
     */
    function deleteSiteManagementChangeLogUtil($userId){
        $objChngLogUtil  = new ChangeLogUtility($_SESSION["sp"], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
        $objChngLogUtil->entityName = "$userId";
        $objChngLogUtil->module = "Delete Sas Testing User";
        $objChngLogUtil->changeLog();
        //$delMsgChangeLog = $objChngLogUtil->tableDelChanges($userId);
    }
    
    function checkColumnExists($fileName, $csvColumnList) {
        if(file_exists($fileName))
        {
            $csvFileData = fopen($fileName,'r');
            while(($data = fgetcsv($csvFileData)) != FALSE)
            {
               $columnDiff = array_diff($csvColumnList, $data);
               fclose($csvFileData);
               if( empty($columnDiff) ) {
                   return true;
               } else {
                   return false;
               }
            }
        }
    }
    
    function createAllNewColumns($fileName, $csvColumnList) {
            $tempFileName = "../../../SASTestingUser/SASTestUsers_temp.csv";
            if(file_exists($fileName))
            {
                $table = fopen($fileName,'r');
                $temp_table = fopen($tempFileName,'w');
                fputcsv($temp_table, $csvColumnList);
                $i = 0;
                while (($data = fgetcsv($table, 1024)) !== FALSE){
                    if($i == 0) {
                        $i++;
                        continue;
                    }
                    fputcsv($temp_table,$data);
                }
                fclose($table);
                fclose($temp_table);
                rename($tempFileName, $fileName);
           }
    }
    
    function addSASPhoneProfileCustomTag($providerid, $groupId, $deviceType, $deviceName) {
        $respose = "";
        $uCTO = new UserCustomTagOperations();
        $pProfile = new PhoneProfilesOperations;
        $phoneProfielTags = $pProfile->getPhoneProfileCustomTag($providerid, $groupId, $deviceName);
        /* Add Tag for previous Phone Profile */
        $respose = $uCTO->addDeviceCustomTag($providerid, $groupId, $deviceName, PhoneProfilesOperations::PREVIOUS_EXPRESS_CUSTOM_PROFILE_NAME, $phoneProfielTags[PhoneProfilesOperations::PREVIOUS_EXPRESS_CUSTOM_PROFILE_NAME]);
        if( empty($respose["Error"])) {
            foreach($phoneProfielTags as $tagName => $tagValue) {
                if($tagName == PhoneProfilesOperations::PREVIOUS_EXPRESS_CUSTOM_PROFILE_NAME) {
                    continue;
                }
                $respose = $uCTO->groupAccessDeviceDeleteCustomTag($providerid, $groupId, $deviceName, $tagName);
            }
            $SASPhoneProfileValue = $pProfile->getPhoneProfileValueByName(PhoneProfilesOperations::SAS_PHONE_PROFILE_NAME);
            $respose = $pProfile->SASPhoneProfileCustomTagAddOperation($providerid, $groupId, $deviceType, $deviceName, $SASPhoneProfileValue['value'], $ociVersion);
        }
        return $respose;
    }
    
    function deleteSASPhoneProfileCustomTag($providerid, $groupId, $deviceType, $deviceName) {
        $respose = "";
        $uCTO = new UserCustomTagOperations();
        $pProfile = new PhoneProfilesOperations;
        $phoneProfielTags = $pProfile->getPhoneProfileCustomTagForRemoveFromSAS($providerid, $groupId, $deviceName);
        if( isset($phoneProfielTags[PhoneProfilesOperations::PREVIOUS_EXPRESS_CUSTOM_PROFILE_NAME]) ) {
            foreach($phoneProfielTags as $tagName => $tagValue) {
                $respose = $uCTO->groupAccessDeviceDeleteCustomTag($providerid, $groupId, $deviceName, $tagName);
            }
            $respose = $pProfile->SASPhoneProfileCustomTagAddOperation($providerid, $groupId, $deviceType, $deviceName, $phoneProfielTags[PhoneProfilesOperations::PREVIOUS_EXPRESS_CUSTOM_PROFILE_NAME], $ociVersion);
        }
        return $respose;
    }
    
?>