<?php
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getUserRegisterationCdrReport.php");
unset($_SESSION["sasUsersAutoFill"]);
?>
 
<style>
element.style {
	margin-top: 25px;
}

.createButton {
	float: left;
}

.ui-dialog .ui-dialog-buttonpane {
	margin-top: 0px;

.errorClass {
    color: red;
}

#tabletr {
    height: auto !important;
}
</style>

<script type="text/javascript">
	var tableRowCount = $('#allUsers tr').length;
	function onCheckUser()
	{
		document.getElementById("selectAllTestUser").checked = true;
		document.getElementById("deleteUsers").style.display = 'none';
		for(var i=0; i < tableRowCount-1; i++)
		{

			var isChecked = $("#userCheckBox"+i).is(":checked");
			if(isChecked){
				document.getElementById("deleteUsers").style.display = 'block';
			}
			if(isChecked == false){
				document.getElementById("selectAllTestUser").checked = false;
			}
		}
	}
	
	$(function()
	{
		$("#go").addClass('disableClassSiteM');
		$("#searchSasUsers").prop('disabled',true);
		$("#loading2").show();
		$("#sasUserList").hide();
		$("#siteReport").hide();

		$('#searchSasUsers').on('keyup keypress', function(e) {
			  var keyCode = e.keyCode || e.which;
			  if (keyCode === 13) {
				e.preventDefault();
				$('#go').trigger('click');
			  }
			});

/* download csv file swap for hover-effect */
 		var sourceSwapImage = function() {
            var thisId = this.id;
            var image = $("#"+thisId).children('img');
            var newSource = image.data('alt-src');
            image.data('alt-src', image.attr('src'));
            image.attr('src', newSource);
        }
        
		$("#downloadCSV,#deleteUsers,#go").hover(sourceSwapImage, sourceSwapImage);

		
		
		$("#sas_dialog").dialog(
		{
			autoOpen: false,
			width: 800,
			open: function(event) {
				setDialogDayNightMode($(this));
				$('.ui-dialog-buttonpane').find('button:contains("Add")').addClass('createButton');
				$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('siteCloseButton');
				$('.ui-dialog-buttonpane').addClass('alignCenterBtn');
				
			},
			modal: true,
			position: { my: "top", at: "top" },
			resizable: false,
			closeOnEscape: false,
			buttons: {
				"Add": function() {
					pendingProcess.push("Add SAS User");
    				var tagFormData = $("form#addSasUserForm").serializeArray();
    				var selectedUser = $("#searchSasUsers").val();
						$("#sas_loader").show();
						$("#tagFormAndMsg").hide();
						$("#errorDiv").html("");
	    				$('.ui-dialog-buttonpane button:contains("Add")').button().hide();
	    				$('.ui-dialog-buttonpane button:contains("Delete")').button().hide();
	    				$('.ui-dialog-buttonpane button:contains("Cancel")').button().hide();
	    				
	    				$.ajax({
							type: "POST",
							url: "siteReport/addDeleteSasUsers.php",
							data: {'tagsData':tagFormData, 'searchVal':selectedUser, 'action':'addToSasTesting'},
							dataType: "html",
							cache: false,
							ajaxOptions: { cache: false },
                                                        beforeSend: function(){
                                                            $("html, body").animate({ scrollTop: 0 }, 0); //Code added @11 Feb 2019
                                                        },
							success: function(result)
							{
								if(foundServerConErrorOnProcess(result, "Add SAS User")) {
			    					return false;
			                  	}
								var result = jQuery.parseJSON(result); 
								if(result.Error && result.Error != "")
								{
									$("#sas_loader").hide();
									$("#errorDiv").html(result.Error);
									$("#tagFormAndMsg").show();
									$('.ui-dialog-buttonpane button:contains("Add")').button().show();
									$('.ui-dialog-buttonpane button:contains("Cancel")').button().show();
								}else{
									init(result.Table);
								}
							}
						});						
    			},
    			
				"Delete": function() {
						pendingProcess.push("Delete SAS User");
        				$(this).dialog("close");
        				$('.ui-dialog-buttonpane button:contains("Delete")').button().hide();
        				$("#searchSasUsers").prop('disabled',true);
        				$("#loading2").show();
        				$("#sasUserList").hide();
        				
        				var dataSend = $("form#siteManagement").serializeArray();
        				$.ajax({
        					type: "POST",
        					url: "siteReport/addDeleteSasUsers.php",
        					data: {'dataToSend':dataSend, 'action':'deleteFromSasTesting'},
        					dataType: "html",
                                                beforeSend: function(){
                                                    $("html, body").animate({ scrollTop: 0 }, 0); //Code added @11 Feb 2019
                                                },
        					success: function(result)
        					{
        						if(foundServerConErrorOnProcess(result, "Delete SAS User")) {
			    					return false;
			                  	}
        						var result = jQuery.parseJSON(result);
        						if(result.Error && result.Error != "")
        						{
        							$("#sas_dialog").dialog("open");
        							$("#sas_dialog").html(result.Error);
        						}
        						init(result.Table);
        					}
        				});
				   },
				
    			"Cancel": function() {
        				$(this).dialog("close");
        			}
			}
		});

		
		var autoCompleteFunction = function ()
		{
			var autoComplete = new Array();
			$.ajax({
				type: "POST",
				url: "siteReport/getAutoFill.php",
				success: function(result)
				{
					if(foundServerConErrorOnProcess(result, "")) {
    					return false;
                  	}
					var explode = result.split(":");
					for (a = 0; a < explode.length; a++)
					{
						autoComplete[a] = explode[a];
					}
					$("#searchSasUsers").autocomplete({
						source: autoComplete,
						appendTo: "#hidden-stuffSAS"
					});
				}
			});
		}
			

		var getTableData = function (result)
		{
			if(result != 0)
			{
				$("#tabletr").html(result);
			}
			else
			{
				$.ajax({
					type: "POST",
					url: "siteReport/addDeleteSasUsers.php",
					success: function(result)
					{ 
						var result = jQuery.parseJSON(result);
						$("#tabletr").html(result.Table);
					}
				});
			}
			
			$("#searchSasUsers").val("");
			$('#selectAllTestUser').prop('checked', false);
			$('#deleteUsers').css('display','none');
		}

		// Initial function.
			var init = function (result)
			{
				$.when($.ajax(getTableData(result))).then(function ()
					{
        				$.when($.ajax(autoCompleteFunction())).then(function ()
        				{
        					$('.ui-dialog-buttonpane button:contains("Delete")').button().hide();
        					$("#searchSasUsers").prop('disabled',false);
        					$("#go").prop('disabled',false);
        					$("#loading2").hide();
        					$("#sasUserList").show();
        					tableRowCount = $('#allUsers tr').length;
        					if(tableRowCount == 1)
        					{

        						$("#allUsers").append("<tr><td colspan ='7' class='alignCenter'>No Users in SAS Test Mode.</td></tr>");

        						$("#selectAllDivChk").hide();							
        					}else{
        						$("#selectAllDivChk").show();
        					}
        					$("#sas_dialog").dialog("close");
        				});
					});
			};
			
			
			$("#deleteUsers").click(function()
			{
				$('.ui-dialog-buttonpane button:contains("Add")').button().hide();
				$('.ui-dialog-buttonpane button:contains("Delete")').button().show().addClass('deleteButton');
				$('.ui-dialog-buttonpane button:contains("Cancel")').button().show().addClass('cancelButton');
				$("#sas_dialog").dialog("open");
				$("#errorDiv").html("")
				$("#sas_loader").hide();
				$("#tagFormAndMsg").show();
				$("#tagFormAndMsg").html(" <b class='alignCenter'> User will be removed from SAS Test mode. </b> ");
			});
		

			$("#searchSasUsers").on('input', function(e) {
				if($.trim($(this).val()) == "") {
					$("#go").addClass('disableClassSiteM');
				} else {
					$("#go").removeClass('disableClassSiteM');	
				}
			});

			$("#searchSasUsers").autocomplete({
				select: function (event, ui) {
					$("#go").removeClass('disableClassSiteM');
				}
			});
			
			
			$("#go").click(function()
			{
				$("#errorDiv").html("");
				$("#sas_loader").show();
				$("#tagFormAndMsg").html("");
				$("#tagFormAndMsg").show();
				
				var selectedUser = $("#searchSasUsers").val();
				if ($.trim(selectedUser) !== "")
				{
					$("#sas_dialog").dialog("open");
					$('.ui-dialog-buttonpane button:contains("Delete")').button().hide();
					$('.ui-dialog-buttonpane button:contains("Add")').button().hide();
					$('.ui-dialog-buttonpane button:contains("Cancel")').button().hide();
					
					$.ajax({
						type: "POST",
						url: "siteReport/addDeleteSasUsers.php",
						data: {'searchVal':selectedUser, 'action':'isUserExist'},
                                                beforeSend: function(){
                                                    $("html, body").animate({ scrollTop: 0 }, 0); //Code added @11 Feb 2019
                                                },
						success: function(result)
						{
							$("#sas_loader").hide();
							var result = jQuery.parseJSON(result);
							if(result.Error && result.Error != "")
							{
								$("#errorDiv").html(result.Error);
								$('.ui-dialog-buttonpane button:contains("Cancel")').button().show();
							}
							else
							{
								$("#tagFormAndMsg").html(result.form);
								$('.ui-dialog-buttonpane button:contains("Delete")').button().hide();
								$('.ui-dialog-buttonpane button:contains("Add")').button().show();
								$('.ui-dialog-buttonpane button:contains("Cancel")').button().show();
							}
						}
					});
				}
				

			});
			
			$(document).on("click", ".primaryUserModLink", function()

					{
								$("#mainBody").html("<div class='col span_11 leftSpace alignCenter' id='loader' style='margin-left:400px;margin-top:10%'> <img src='/Express/images/ajax-loader.gif'></div>");
								var userId = $(this).attr("id");
								var fname = $(this).attr("data-firstname");
								var lname = $(this).attr("data-lastname");
								var extension = $(this).attr("data-extension");
								var phone = $(this).attr("data-phone");
								if($.trim(phone) != ""){
									userIdValue1 = phone+"x"+extension;
								}else{
									userIdValue1 = userId;
								}
								var userIdValue = userIdValue1+" - "+lname+", "+fname;
								$.ajax({
										type: "POST",
										url: "userMod/userInfo.php",
										//data: { searchVal: userId, userFname : fname, userLname : lname },
										success: function(result)
										{
											$("#mainBody").html(result);
											//$(".subBanner").html("User Modify");
											$("#searchVal").val(userIdValue);
											$("#go").trigger("click");
											$(".navMenu").removeClass("active");
											$("#userMod").addClass("active");
											$('#helpUrl').attr('data-module', "userMod");
											activeImageSwapFunction();
										}

					});
				});
			
			// Download as csv function
			
			
			/*$("#downloadSASCSV").click(function()
			{
				$.ajax({
					type: "POST",
					url: "siteReport/downloadAsCSV.php",
					success: function(result)
					{
					}
				});
				
			});*/

			$("#downloadCSV").click(function()
					{
						$("#siteReportCSV").submit();
					});
			
			var selectedUsers ="";
			var checkboxes ="";
			$("#selectAllTestUser").click(function()
			{

				var i=0;
	            var selectAllCheck = $(this).is(":checked");
	            var chkBox = document.getElementById("userCheckBox" + i);
	            while (chkBox && chkBox !== null)
	            {
	                if (! selectedUsers.hasOwnProperty(chkBox.name)) {
	                    selectedUsers[chkBox.name] = false;
	                }
	                
	                selectedUsers[chkBox.name] = selectAllCheck;
	                chkBox.checked = selectAllCheck;

	                ++i;
	                chkBox = document.getElementById("userCheckBox" + i);
	            	
	            }	
	            if(selectAllCheck){

	            	 
	    				document.getElementById("deleteUsers").style.display = 'block';
	    			 
	            }

	            if(selectAllCheck == false){
					document.getElementById("selectAllTestUser").checked = false;
					document.getElementById("deleteUsers").style.display = 'none';
				}	
	            //checkboxes.prop('checked', selectAllCheck);
				//document.getElementById("deleteUsers").style.display = (tableRowCount > 1 && selectAllCheck) ? 'block' : 'none';
			//	var selectAllCheck = $(this).is(":checked");
			//	var checkboxes = $(this).closest('form').find(':checkbox');

			
				
			});

			
			// Initial function.
			init(0);
			
			$("#tabs").tabs();
			
			
	});
	
		$("#submit").click(function()
		{
			var url = "siteReport/data.php";
			$("#cdrResults").html("");
			$("#loading2").show();
			var dataToSend = $("#cdrFilter").serialize();
			$.ajax({
				type: "POST",
				url: url,
				data: dataToSend,
				success: function(result)
				{
					$("#loading2").hide();
						$("#cdrResults").show();
						$("#cdrResults").html(result);
				}
			});
		});
						
		$("#fromDate").datepicker({
			//defaultDate: "-4w",
			changeMonth: true,
			numberOfMonths: 3,
			onClose: function(selectedDate) {
				$("#toDate").datepicker("option", "minDate", selectedDate);
			}
		});
		
		$("#toDate").datepicker({
			//defaultDate: "-8w",
			changeMonth: true,
			numberOfMonths: 3,
			onClose: function(selectedDate) {
				$("#fromDate").datepicker("option", "maxDate", selectedDate);
			}
		});

		function tabClicked(title)
		{
			$("#sitesubBanner").html(title);
		};
		
	// tooltip
		$(document).ready(function(){
				$('[data-toggle="tooltip"]').tooltip();   
			});

</script>

<script>
var activeImageSwapFunction = function()
{	
	previousActiveMenu	= "siteManagement";
	 currentClickedDiv = "userMod";       
  
		//active
  		if(previousActiveMenu != "")
		{ 
  		 	$(".navMenu").removeClass("activeNav");
			var $thisPrev = $("#siteManagement").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);
	       
		}
		// inactive tab
		if(currentClickedDiv != ""){
			$("#userMod").addClass("activeNav");
			var $thisPrev = $("#userMod").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);
			 previousActiveMenu = currentClickedDiv;
			
    	}
   
}

//$('.primaryUserModLink').click(activeImageSwap, activeImageSwap);
</script>

<!-- service License -->


<h2 class="adminUserText" id="sitesubBanner">Site Management</h2>
<div class="selectContainer padding-top-zero">

	<form id="siteManagement" action="siteReport/siteReportXLSX.php" method="POST" style="width:auto">
	<div id="tabs">
		<div id style="width:830px;margin: 0 auto;">
			<div class="divBeforeUl">
				<ul>
					<?php if( isset($_SESSION["permissions"]["siteReports"]) && $_SESSION["permissions"]["siteReports"]== "1"){
						echo "<li><a href='#siteReport' id='siteReporta' class='tabs' onclick='tabClicked(\"Site Report\")'>Site Report</a></li>";
					 }?>
					<?php if(isset($_SESSION["permissions"]["sasTest"]) && $_SESSION["permissions"]["sasTest"]== "1"){?>
					<li><a href="#sasTesting" id="sasTestingf" class="tabs" onclick="tabClicked('SAS Testing')">SAS Testing</a></li>
					<?php }?>
					<?php //if((isset($license["customProfile"]) && $license["customProfile"] == "true") && checkCustomProfiles()){?>
					<?php if($_SESSION["superUser"] == "1"){?>
					<li><a href="#phone_ProfilesTab" id="phoneProfilesTab" class="tabs" onclick="tabClicked('Phone Profiles')">Phone Profiles</a></li>
					<?php }?>
				</ul>
			</div>
			 
			<!-- Site Report -->
			<h2 class="adminUserText" id=""></h2>
			<div id="siteReport" class="siteReportCls">
				<div class="col-md-12" style="padding-right: 0;">
					<div class="form-group exportExcelDiv">
						<input type="submit" name="submit2" value=""
							class="exportExcelBtn">
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="from" class="labelText">From Date</label><br />
							<input type="text" name="fDate" id="fromDate" size="12">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="from" class="labelText">To Date</label><br />
							<input type="text" name="tDate" id="toDate" size="12">

						</div>
					</div>
				</div>
			</div>	 	
		</div>
			<!-- SAS Testing -->
			<div id="sasTesting" class="" style="display: none;">

			 <div class="row" style="width:830px; margin:0px auto">
				<div class="col-md-11 adminSelectDiv" style="padding-left: 0;padding-right: 0;">
					<div class="form-group">
						  
						<label class="labelText" for="searchSasUsers">Search User With Device : </label>
						<a style="margin-left: 5px;"class="customTooltip" data-toggle="tooltip" data-placement="top" title="Only Users with devices will show here"><img style="margin-bottom: 5px;" src="images/NewIcon/info_icon.png"></a><br />
						<input type="text" class="autoFill magnify" name="searchSasUsers" id="searchSasUsers" size="50" style="width:100% !important">
						<div id="hidden-stuffSAS" style="height: 0px;"></div>
					</div>
				</div>
				
				<div class="col-md-1 adminAddBtnDiv" style="padding-right: 0;">
					<p class="register-submit addAdminBtnIcon" style="float: right;" id="go">
						<img src="images\icons\add_admin_rest.png" data-alt-src="images\icons\add_admin_over.png" name="go" id="go" value="Add To SAS Testing">
					</p>
					<label class="labelText labelTextOpp" style="float: right;"><span>Add</span><br><span style="margin: 3px -7px 5px 5px;">SAS Testing</span></label>
				</div>
			</div> 
 
			<div id="sasUserList">
					<div class="row sitReportInputField">
							<div class="col-md-6"></div>
							<div class="col-md-6 csvFileDownloadAlign">
								 
		 							<div class="col-md-10"></div>
		 							
		 							
		 							<div class="col-md-1" name="downloadCSV" id="downloadCSV" value="">
	 									<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" onclick="location.href='siteReport/printSASUserCSV.php';">
	 									<br><span>Download<br>CSV</span>
	 								</div>
	 								
	 								
	 								 <div class="col-md-1 deleteUsers" style="" name="deleteUsers" id="deleteUsers" >
	 								 <img src="images/icons/delete_selected_users_icon.png" data-alt-src="images/icons/delete_selected_users_icon_over.png" />
	 								 <br><span>Remove<br>Selected<br>Users</span></div>
	 								
	 								
	 						</div>
					</div>
					
					 
					 <div id="selectAllDivChk">
					<input type="checkbox" id="selectAllTestUser"><label for="selectAllTestUser" style="padding-left:10px;"><span></span></label>
					<label class="labelText"><b>Select all Users</b></label>
				</div>
				<div class="viewDetail autoHeight viewDetailNew">
					<div style="zoom: 1;">
						<table id="allUsers" class="scroll tablesorter tableCLass dataTable no-footer sasTestTableClass" style="width: 100%; margin: 0 auto;">
							<thead>
								<tr>
									<th class="thSno">&nbsp;</th>
									<th class="thsmall" style="width:115px !important;">Start Time</th>
									<th class="thsmall" style="width:115px !important;">User Name</th>
									<th class="thsmall" style="width:115px !important;">User ID</th>
									<th class="thsmall" style="width:115px !important;">Extension</th>
									<th class="thsmall" style="width:115px !important;">Device Type</th>
									<th class="thsmall" style="width:115px !important;">Device Name</th>
									<th class="thsmall" style="width:115px !important;">Device IP</th>
									<th class="thsmall" style="width:115px !important;">SAS_IP</th>
									<th class="thsmall" style="width:115px !important;">SAS_NAME</th>
									<th class="thsmall" style="width:115px !important;">SBC_ADDRESS</th>
									<th class="thsmall" style="width:115px !important;">SBC_PORT</th>
								</tr>
							</thead>
							<tbody id="tabletr" style="border-bottom:none;">
						 
							</tbody>
						</table>
					</div>
				</div>
			</div>
			
				
		<div class="loading" id="loading2" style="margin-top: 0%">
				<img src="/Express/images/ajax-loader.gif">

		</div>

	</div>	 

			<!-- phone_profiles -->
		<div id="phone_ProfilesTab" class="userDataClass" style="display: none;">
			<?php 
            require_once ("/var/www/html/Express/phoneProfiles/phoneProfiles.php");
            ?>
			
		</div>
			<div id="sas_dialog" class="dialogClass sasValidationText" style="text-align:center">
				<div id="sas_loader" style="text-align:center">
					<img src="/Express/images/ajax-loader.gif">
				</div>
				<div id="errorDiv" class="sasValidationText"></div>
				<div id="tagFormAndMsg" style="width: 860px; margin: 0 auto; text-align:center"></div>
			</div>
		 
</div>
</form>

<form action="siteReport/printSASUserCSV.php" method="POST"
	name="siteReportCSV" id="siteReportCSV">
</form>
<div id="dataReport"></div>
