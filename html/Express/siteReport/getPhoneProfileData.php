<?php 
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
//print_r($_POST);
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/phoneProfiles/PhoneProfilesOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
$dnsObj = new Dns();

$groupId = isset($_SESSION["groupId"]) ? $_SESSION["groupId"] : "";

$deviceType = isset($_POST["ppDeviceType"]) ? $_POST["ppDeviceType"] : "";

$profileName = isset($_POST["ppProfileType"]) ? $_POST["ppProfileType"] : "";

$phoneProfileSearchlevel = isset($_POST["phoneProfileSearchlevel"]) ? $_POST["phoneProfileSearchlevel"] : "";
$obj = new PhoneProfilesOperations();
$devObj = new DeviceOperations();
//echo "sp";print_r($_SESSION["sp"]);
$getSystemData = $obj->getDevicesFromSystemLevel($groupId, $deviceType, $phoneProfileSearchlevel, $_SESSION["sp"]);
//print_r($getSystemData);exit;
$deviceDataArray = array();
if($getSystemData["Success"] =="Success"){
    if(count($getSystemData["SuccessData"]) > 0){
        $i = 0;
        foreach ($getSystemData["SuccessData"] as $key => $value){
            $deviceCT = $obj->getDeviceCustomTags($value["spName"], $value["groupId"], $value["deviceName"]);
            //print_r($deviceCT);
            if($profileName == "all"){  
                if(array_key_exists("%Express_Custom_Profile_Name%",$deviceCT["Success"]) && $deviceCT["Success"]["%Express_Custom_Profile_Name%"] != "None" && $deviceCT["Success"]["%Express_Custom_Profile_Name%"] != ""){
                    if($_POST["tagBunddle"] != ""){
                        $expldBundleArr = explode(";", $deviceCT["Success"]["%Express_Tag_Bundle%"]);
                        if(in_array($_POST["tagBunddle"], $expldBundleArr)){
                            $string = $deviceCT["Success"]["%Express_Tag_Bundle%"];
                            $deviceDataArray[$i]["spName"] = $value["spName"];
                            $deviceDataArray[$i]["groupId"] = $value["groupId"];
                            $deviceDataArray[$i]["deviceName"] = $value["deviceName"];
                            $deviceDataArray[$i]["deviceType"] = $value["deviceType"];
                            $deviceDataArray[$i]["netAddress"] = $value["netAddress"];
                            $deviceDataArray[$i]["macAddress"] = $value["macAddress"];
                            $deviceDataArray[$i]["customProfileName"] = $deviceCT["Success"]["%Express_Custom_Profile_Name%"];
                            //$deviceDataArray[$i]["tagBundle"] = str_replace(";", ",",  $string);
                            $deviceDataArray[$i]["tagBundle"] = $string;
                        }
                        
                    }else{
                        $string = $deviceCT["Success"]["%Express_Tag_Bundle%"];
                        $deviceDataArray[$i]["spName"] = $value["spName"];
                        $deviceDataArray[$i]["groupId"] = $value["groupId"];
                        $deviceDataArray[$i]["deviceName"] = $value["deviceName"];
                        $deviceDataArray[$i]["deviceType"] = $value["deviceType"];
                        $deviceDataArray[$i]["netAddress"] = $value["netAddress"];
                        $deviceDataArray[$i]["macAddress"] = $value["macAddress"];
                        $deviceDataArray[$i]["customProfileName"] = $deviceCT["Success"]["%Express_Custom_Profile_Name%"];
                        //$deviceDataArray[$i]["tagBundle"] = $deviceCT["Success"]["%Express_Tag_Bundle%"];
                        //$deviceDataArray[$i]["tagBundle"] = str_replace(";", ",",  $string);
                        $deviceDataArray[$i]["tagBundle"] = $string;
                    }
                    $i++;
                }
            }else{
                if(array_key_exists("%Express_Custom_Profile_Name%",$deviceCT["Success"]) && $deviceCT["Success"]["%Express_Custom_Profile_Name%"] != "None" && $deviceCT["Success"]["%Express_Custom_Profile_Name%"] != "" && $deviceCT["Success"]["%Express_Custom_Profile_Name%"] == $profileName){
                    if($_POST["tagBunddle"] != ""){
                        
                        if(isset($deviceCT["Success"]["%Express_Tag_Bundle%"]) && $deviceCT["Success"]["%Express_Tag_Bundle%"] != ""){
                            $bundleName = $deviceCT["Success"]["%Express_Tag_Bundle%"];
                            $bundleNameExploded = explode(";", $bundleName);
                            if(in_array($_POST["tagBunddle"], $bundleNameExploded)){
                                $deviceDataArray[$i]["spName"] = $value["spName"];
                                $deviceDataArray[$i]["groupId"] = $value["groupId"];
                                $deviceDataArray[$i]["deviceName"] = $value["deviceName"];
                                $deviceDataArray[$i]["deviceType"] = $value["deviceType"];
                                $deviceDataArray[$i]["netAddress"] = $value["netAddress"];
                                $deviceDataArray[$i]["macAddress"] = $value["macAddress"];
                                $deviceDataArray[$i]["customProfileName"] = $deviceCT["Success"]["%Express_Custom_Profile_Name%"];
                                $deviceDataArray[$i]["tagBundle"] = $deviceCT["Success"]["%Express_Tag_Bundle%"];
                            }
                                
                            }
                        
                        
                    }else{
                        $deviceDataArray[$i]["spName"] = $value["spName"];
                        $deviceDataArray[$i]["groupId"] = $value["groupId"];
                        $deviceDataArray[$i]["deviceName"] = $value["deviceName"];
                        $deviceDataArray[$i]["deviceType"] = $value["deviceType"];
                        $deviceDataArray[$i]["netAddress"] = $value["netAddress"];
                        $deviceDataArray[$i]["macAddress"] = $value["macAddress"];
                        $deviceDataArray[$i]["customProfileName"] = $deviceCT["Success"]["%Express_Custom_Profile_Name%"];
                        $deviceDataArray[$i]["tagBundle"] = $deviceCT["Success"]["%Express_Tag_Bundle%"];
                        
                    }
                    //print_r($deviceCT["Success"]["%Express_Custom_Profile_Name%"]);
                    
                    $i++;
                }
            }
        }
        
    }
}
//echo "Device Array";print_r($deviceDataArray);


if(count($deviceDataArray) > 0){
    $allData = array();
    $i = 0;
    foreach ($deviceDataArray as $key => $value){
        
        $allData[$i]["spName"] = $value["spName"];
        $allData[$i]["groupId"] = $value["groupId"];
        $allData[$i]["deviceName"] = $value["deviceName"];
        $allData[$i]["deviceType"] = $value["deviceType"];
        $allData[$i]["netAddress"] = $value["netAddress"];
        $allData[$i]["macAddress"] = $value["macAddress"];
        $allData[$i]["customProfileName"] = $value["customProfileName"];
        $allData[$i]["tagBundle"] = $value["tagBundle"];
        
        $devResp = $devObj->getAllUsersforDevice($value["spName"], $value["groupId"], $value["deviceName"]);
        //print_r($devResp);
        if(empty($devResp["Error"])){
            if($devResp["Success"] != ""){
                foreach ($devResp["Success"] as $key1 => $value1){
                    if($value1["endpointType"] == "Primary"){
                        
                        $allData[$i]["linePort"] = $value1["linePort"];
                        $allData[$i]["lastName"] = $value1["lastName"];
                        $allData[$i]["firstName"] = $value1["firstName"];
                        $allData[$i]["phoneNumber"] = $value1["phoneNumber"];
                        $allData[$i]["userId"] = $value1["userId"];
                        $allData[$i]["userType"] = $value1["userType"];
                        $allData[$i]["endpointType"] = $value1["endpointType"];
                        $allData[$i]["extn"] = $value1["extn"];
                        $dnsIsActivated = $dnsObj->getUserDNActivateListRequest($value1["userId"]);
                        if(isset($dnsIsActivated["Success"][0]["status"])){
                            $allData[$i]["registered"] = $dnsIsActivated["Success"][0]["status"];
                        }
                    }
                }
            }
        }
        $i++;
    }
}
//echo "All Array";print_r($allData); 
if(isset($_SESSION["groupId"])){
    $isGroupAcc = $_SESSION["groupId"];
}else{
    $isGroupAcc = "";
}
if($isGroupAcc == ""){
    $spNames = array();
    $grpNames = array();
    $profileNames = array();
    
    foreach ($allData as $key => $row) {
        $spNames[$key]  = $row['spName'];
        $grpNames[$key] = $row['groupId'];
        $profileNames[$key] = $row['customProfileName'];
    }
    
    array_multisort($spNames, SORT_ASC, $grpNames, SORT_ASC,$profileNames, SORT_ASC, $allData);
 
    /*uasort($allData, function($a,$b){
        $c = $a['spName'] - $b['spName'];
        $c .= $a['groupId'] - $b['groupId'];
        return $c;
    });*/
}else{
    uasort($allData, function($a, $b) {
        return strnatcmp($a['customProfileName'], $b['customProfileName']);
    });
}
//print_r($allData);
processData($allData, $isGroupAcc);

function processData($allData, $isGroupAcc){
    if(count($allData) > 1000){
        echo "<div style='color:red'>".count($allData)." devices will be queried for Phone Profiles information. Please narrow your search by selecting Device Type.";
    }else{ ?>
        
		<table id="ppScrollTable" class="scroll tablesorter" style="width: 100%; margin: 0;margin-bottom:100px;">
			<thead>
    			<tr>
    				<?php
    				if($isGroupAcc == ""){ ?>
    				    <th style="width:6%;">Enterprise</th>
    					<th style="width:6%;">Group</th>
    				<?php } ?>
    				<th style="width:8%;">User ID</th>
    				<th style="width:8%;">Phone Profile</th>
    				<th style="width:8%;">Tag Bundle</th>
    				<th style="width:8%;">Device Name</th>
    				<th style="width:8%;">Device Type</th>
    				<th style="width:8%;">MAC Address</th>
    				<th class="registeredColumn" style="width:8%;">DN Activated</th>
    				<th class="phoneColumn" style="width:8%;">Phone Number</th>
    				<th class="extColumn" style="width:8%;">Extension</th>
    				<th class="fNameColumn" style="width:8%;">First Name</th>
    				<th class="lNameColumn" style="width:8%;">Last Name</th>
    			</tr>
			</thead>
			<tbody>
			<?php 
			     if(count($allData) > 0){
			         foreach ($allData as $key => $value){
			             if(isset($value["userId"]) && !empty($value["userId"])){$userId = $value["userId"];}else{$userId = "";}
			             if(isset($value["phoneNumber"]) && !empty($value["phoneNumber"])){$phoneNumber = $value["phoneNumber"];}else{$phoneNumber = "";}
			             if(isset($value["extn"]) && !empty($value["extn"])){$extension = $value["extn"];}else{$extension = "";}
			             if(isset($value["firstName"]) && !empty($value["firstName"])){$firstName = $value["firstName"];}else{$firstName = "";}
			             if(isset($value["lastName"]) && !empty($value["lastName"])){$lastName = $value["lastName"];}else{$lastName = "";}
			             if(isset($value["registered"]) && !empty($value["registered"])){$registered = $value["registered"];}else{$registered = "";}
			             if($isGroupAcc != ""){
			                 $userIdLink = "<a href='#' class='primaryUserModLinkPhoneProfile' data-phone='".$phoneNumber."' data-extension='".$extension."' id='" . $userId . "' data-lastname='" . $lastName . "' data-firstname='" . $firstName . "' >" . $userId . "</a>";
			             } else {
			                 $userIdLink = $userId;
			             }
			             
			             
			             
			             echo "<tr>";
			             if($isGroupAcc == ""){
			                 echo '<td style="width:6%;">'.$value["spName"].'&nbsp;</td>';
			                 echo '<td style="width:6%;">'.$value["groupId"].'&nbsp;</td>';
			             }
			                 echo '<td style="width:8%;">'.$userIdLink.'&nbsp;</td>';
			                 echo '<td style="width:8%;">'.$value["customProfileName"].'&nbsp;</td>';
			                 echo '<td style="width:8%;">'.$value["tagBundle"].'&nbsp;</td>';
			                 echo '<td style="width:8%;">'.$value["deviceName"].'&nbsp;</td>';
			                 echo '<td style="width:8%;">'.$value["deviceType"].'&nbsp;</td>';
			                 echo '<td style="width:8%;">'.$value["macAddress"].'&nbsp;</td>';
			                 echo '<td class="registeredColumn" style="width:8%;">'.$registered.'&nbsp;</td>';
			                 echo '<td class="phoneColumn" style="width:8%;">'.$phoneNumber.'&nbsp;</td>';
			                 echo '<td class="extColumn" style="width:8%;">'.$extension.'&nbsp;</td>';
			                 echo '<td class="fNameColumn" style="width:8%;">'.$firstName.'&nbsp;</td>';
			                 echo '<td class="lNameColumn" style="width:8%;">'.$lastName.'&nbsp;</td>';
			             echo "</tr>";
			         }
			     }
			?>
			</tbody>
		</table>
		
    <?php }
    
    	
				
    
    
}

?>