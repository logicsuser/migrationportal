<?php 
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/adminPortal/phoneProfiles/DBOperations.php");
$obj = new PhoneProfilesDBOperations();

if(isset($_POST["allProfileData"]) && !empty($_POST["allProfileData"])){
    $resp = $obj->getCustomProfiles("customProfiles");
    echo json_encode($resp);
}

if(isset($_POST["allDeviceTypeData"]) && !empty($_POST["allDeviceTypeData"])){
    $resp = $obj->getDeviceType("customProfiles");
    echo json_encode($resp);
}

if(isset($_POST["selectedDeviceTypeData"]) && !empty($_POST["selectedDeviceTypeData"])){
    $resp = $obj->getSelectedDeviceTypeProfiles("customProfiles", $_POST["selectedDeviceTypeData"]);
    echo json_encode($resp);
}


?>