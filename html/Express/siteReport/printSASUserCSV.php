<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	$DwldfileName = $_SESSION["groupId"] . "-SASTestUsers";
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment;filename=\"" . $DwldfileName . ".csv\"");
	header("Content-Transfer-Encoding: binary");

	
	$providerid = $_SESSION["sp"];
	$groupId = $_SESSION["groupId"];
	$fileName = "../../../SASTestingUser/SASTestUsers.csv";
	
	if(file_exists($fileName))
	{
	    $table = fopen($fileName,'r');
	    $i = 0;
	    while (($data = fgetcsv($table, 1024)) !== FALSE){
	       
	        if(($data[0] == trim($providerid) && $data[1] == $groupId) || $i == 0)
	        {
	            if($i == 0){
	                $data[10] = rtrim(ltrim($data[10], "%"), "%");
	                $data[11] = rtrim(ltrim($data[11], "%"), "%");
	                $data[12] = rtrim(ltrim($data[12], "%"), "%");
	                $data[13] = rtrim(ltrim($data[13], "%"), "%");
	            }
	            $fp = fopen("php://output", "w");
	            fputcsv($fp, $data);
	        }
	        $i++;
	    }
	    fclose($fp);
	}
		
?>