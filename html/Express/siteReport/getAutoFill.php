<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	require_once("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
	require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	
	require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
	
	$fileName = "../../../SASTestingUser/SASTestUsers.csv";
	
	
	if(! isset($_SESSION["sasUsersAutoFill"]))
	{
	    $userOperation = new UserOperations();
	    $userListcontainingDevice= $userOperation->userListOfGroup($_SESSION["sp"], $_SESSION["groupId"]);
	    $userList = $userOperation->removeElementWithValue($userListcontainingDevice);
	    if(empty($userList['Error']))
	    {
	        $userList = $userList['Success'];
	        usersAutoFillSession($userList);
	    }
	}
	
	$csvFileUserArr = storeCsvFileUserIdInArr($fileName);
	
    if (isset($_SESSION["sasUsersAutoFill"]))
	{
		foreach ($_SESSION["sasUsersAutoFill"] as $key => $value)
		{
		if(!in_array($value, $csvFileUserArr))
		    {
		        echo $key . ":";
		    }
			
		}
	}
	
	
	function usersAutoFillSession($userList)
	{
	    $filterDeviceList = getFilterList();
	    //$customDeviceListArr = customDeviceListFilter();
	    foreach($userList as $key => $value)
	    {
	            $name = strval($value->col[1]) . ", " . strval($value->col[2]);
	             
	            $userId = strval($value->col[0]);
	            if(strval($value->col[4]))
	            {
	                $phoneNumber = substr(strval($value->col[4]), 3) . "x" . strval($value->col[10]) . " - " . $name;
	            }
	            else
	            {
	                $phoneNumber = strval($value->col[0]) . " - " . $name;
	            }
	            
	            
	            //codes to show registered or not registered
	            	$userVdmObj = new VdmOperations();
	            	$userStatus = $userVdmObj->getUserStatus($userId);
	            	if(empty($userStatus)){ $userStatus = " (Not Registered) ";}else{ $userStatus = " (Registered) ";}
	            //end codes
	            	$phoneNumber .= $userStatus;
	            $_SESSION["sasUsersAutoFill"][$phoneNumber] = strval($value->col[0]);
	        
	    }
	}
	
	function storeCsvFileUserIdInArr($fileName)
	{
	    $userIdArray = array();
	    if(file_exists($fileName))
	    {
	        $csvFileData = fopen($fileName,'r');
	        $count = 0;
	        while(($data = fgetcsv($csvFileData)) != FALSE)
	        {
                if($count > 0){
                    if(trim($_SESSION["sp"]) == $data[0] && $_SESSION["groupId"] == $data[1]){
                        array_push($userIdArray, $data[3]);
                    }
                }
                $count++;
	        }
	        fclose($csvFileData);
	    }
	    return $userIdArray;
	}
	
	function getFilterList() {
	    global $db;
	    $filterList = array();
            //Code added @ 16 July 2019
            $whereCndtn = "";
            if($_SESSION['cluster_support']){
                $selectedCluster = $_SESSION['selectedCluster'];
                $whereCndtn = " AND clusterName ='$selectedCluster' ";
            }
            //End code
	    $query = "SELECT deviceType from systemDevices where `sasTest` = 'Yes' $whereCndtn ";
	    $result = $db->query($query);
	    while ($row = $result->fetch()) {
	        $filterList[] = $row["deviceType"];
	    }
	    
	    return array_values($filterList);
	}
	
	function checkScaOnlyIsTrue($userId, $filterDeviceList, $customDeviceListArr)
	{
	    global $db;
	    $sasTest = "true";
	    $isExistInCustomDeviceList = "false";
	    $returnData = "false";
	    $uop = new UserOperations();
	    $userDetail = $uop->getUserDetail($userId);
	    if(empty($userDetail["Error"])){
	        
	        $userDetail = $userDetail["Success"];
            if(in_array($userDetail["deviceType"] ,$filterDeviceList) && in_array($userDetail["deviceType"], $customDeviceListArr)){
                $isExistInCustomDeviceList = "true";
	        }
            
	    }
	   
	    return $isExistInCustomDeviceList;
	}
	
	
?>
