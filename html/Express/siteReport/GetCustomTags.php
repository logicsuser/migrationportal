<?php 

require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");

/* CREATED BY : PANKAJ.
 * 28-JULY-2018.
 * TO GET %ACCT.SAS_IP% CUSTOM TAG EXISTANCE IN ALL 'FXO, FXS/FXO' AUDIO CODE DEVICES.
 * */

Class GetCustomTags {
    
    private $providerId;
    private $groupId;
    private $dOObject;
    public $errorResponse = "";
    private $deviceNameList = array();
    private $devicesWith_SAS_IP_tag = array();
    public $SASDialogData = array();
    private $deviceTypeList = array(
                            'AUDIO-MP-114FXO',
                            //'AUDIO-MP-114FXS',
                            'AUDIO-MP-114FXSFXO',
                            'AUDIO-MP-118FXO',
                            //'AUDIO-MP-118FXS',
                            'AUDIO-MP-118FXSFXO'
                        );
    
    public function __construct(DeviceOperations $dOP, UserCustomTagOperations $uCTO, $sp, $groupId) {
        $this->dOObject = $dOP;
        $this->uCTO = $uCTO;
        $this->providerId = $sp;
        $this->groupId = $groupId;
    }
    
    public function GroupAccessDeviceGetList() {
        foreach($this->deviceTypeList as $key => $deviceType) {
            $deviceRes = $this->dOObject->GroupAccessDeviceGetListRequest($this->providerId, $this->groupId, $deviceType);
            if(empty($deviceRes['Error'])) {
                foreach($deviceRes['Success'] as $key => $value) {
                    $this->deviceNameList[] = $value['deviceName'];
                }
            }
        }
    }
    
    private function getCustomTagListOfDeviceName() {
        if(! empty($this->deviceNameList)) {
            foreach($this->deviceNameList as $key => $deviceName) {
                $customTagRes = $this->uCTO->getUserCustomTagsWithValue($this->providerId, $this->groupId, $deviceName);
                if(empty($customTagRes['Error'])) {
                    if(isset( $customTagRes['Success']['%ACCT.SAS_IP%'] )) {
                        $customData = new stdClass();
                        $customData->tagName = '%ACCT.SAS_IP%';
                        $customData->tagValue = $customTagRes['Success']['%ACCT.SAS_IP%'];
                        $this->devicesWith_SAS_IP_tag[$deviceName] = $customData;
                    }
                }
            }
        }
        return $this->devicesWith_SAS_IP_tag;
    }
    
    public function errorHandlingInSAS() {
        $errorStatus = new stdClass();
        $this->GroupAccessDeviceGetList();
        $deviceWithSASTag = $this->getCustomTagListOfDeviceName();
        if(empty($deviceWithSASTag)) {
            $errorStatus->errorType = "TagNotFound";
            $errorStatus->warningMsg = "Custom Tag %ACCT.SAS_IP% not found in group`s Audio Codes";
        } else if(count($deviceWithSASTag) > 1) {
            $i = 0;
            $devicesNameErr = "";
            foreach($deviceWithSASTag as $deviceName => $value) {
                $devicesNameErr .= $deviceName. ", ";
                if(filter_var($value->tagValue, FILTER_VALIDATE_IP)) {
                    $i++;
                    $this->SASDialogData['ipAddress'] = $value->tagValue;
                    $this->SASDialogData['deviceName'] = $deviceName;
                }
            }
            
            if($i > 1) {                                    /*If Multiple Valid IP Addresses found*/
                $errorStatus->errorType = "MultipleIPAddresses";
                $errorStatus->warningMsg = "SAS_IP address is set in more than one Audio Code device: ". $devicesNameErr;
            } else if($i == 0) {                            /*If No Valid IP Addresses found*/
                $errorStatus->errorType = "IPAddressNotSetOrInvalid";
                $errorStatus->warningMsg = "Non-existent or invalid SAS_IP Address: ".$value->tagValue." in ".$deviceName." Audio Code";
            }
        } else if(count($deviceWithSASTag) == 1) {
            foreach($deviceWithSASTag as $deviceName => $value) {
                if(filter_var($value->tagValue, FILTER_VALIDATE_IP)) {
                    $this->SASDialogData['ipAddress'] = $value->tagValue;
                    $this->SASDialogData['deviceName'] = $deviceName;
                } else {
                    $errorStatus->errorType = "IPAddressNotSetOrInvalid";
                    $errorStatus->warningMsg = "Non-existent or invalid SAS_IP Address: ".$value->tagValue." in ".$deviceName." Audio Code";
                }
            }
        }
        
        $this->errorResponse = $errorStatus;
        return $this->errorResponse;
    }
 
    public function __destruct() {
        
    }
}
?>