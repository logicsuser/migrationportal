<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/autoAttendant21/aaOperation.php");

$announcement = new AaOperation();
$getAutoAttendantList= "";

$getAutoAttendantList= $announcement->getAutoAttendantList($ociVersion);

unset($_SESSION["usedAANames"]);
$usedAANames = array();
foreach($getAutoAttendantList as $autoAtt)
{
    $usedAANames[] = $autoAtt["name"];
}
$_SESSION["usedAANames"] = $usedAANames;

echo json_encode($getAutoAttendantList);
?>