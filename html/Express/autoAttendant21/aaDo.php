<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/autoAttendant21/aaController.php");
	$aaController = new AaController();
	
	require_once("/var/www/lib/broadsoft/adminPortal/autoAttendant21/aaOperation.php");
	
	require_once ("/var/www/lib/broadsoft/adminPortal/Announcement21/Announcement21.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php"); //Code added @ 23 Jan 2019 
        
	
	$announcement21 = new Announcement21();
	$aaOpObj = new AaOperation();
	
	require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
	require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
	$expProvDB = new ExpressProvLogsDB();
	
        $sequenceArray = array(
                                "aaId",
                                "aaName",
                                "callingLineIdLastName",
                                "callingLineIdFirstName",
                                "hiraganaLastName",
                                "hiraganaFirstName",
                                "phoneNumber",
                                "extension",
                                "countryCode",
                                "nationalPrefix",
                                "department",
                                "language",
                                "timeZone",
                                "timeZoneDisplayName"
                        );
	$sp = $_SESSION["sp"]; 
	$groupId = $_SESSION["groupId"]; 
	
	$aaId = $_POST["aaId"];
        //Code added @ 23 Jan 2019 
        $cLUObj = new ChangeLogUtility($aaId, $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
        //End code
        
	$updateCallPolicyArray = array();
	$xmlData = "";
	$menuData = "";
	$sip = "";
	$a = 0;
	$dnPostArray = array();
	
	if(!array_key_exists("eVSupport", $_POST)){
		$_POST["eVSupport"] = "false";
	}
	if(!array_key_exists("assignedServices", $_POST)){
		$_POST["assignedServices"] = array();
	}
	if(!array_key_exists("aaActivateNumber", $_POST)){
		$_POST["aaActivateNumber"] = "No";
	}
	if(!array_key_exists("aaPhoneActivateNumber", $_POST)){
		$_POST["aaPhoneActivateNumber"] = "";
	}
	
	$announcementAutoAttendantArray = array();
	$announcementAutoAttendantArray['bhAnnouncementId'] = "";
	$announcementAutoAttendantArray['ahAnnouncementId'] = "";
	$announcementAutoAttendantArray['hAnnouncementId']  = "";
	$announcementAutoAttendantArray['vpAnnouncementId'] = "";
	$checkifNewSubCreated= "";
	$submenuInsertionIntoDB = "";
	
	$arrSpl = $_POST["firstDigitTimeoutSeconds"];
	unset($_POST["firstDigitTimeoutSeconds"]);

	$offset = 9;
	$_POST = array_slice($_POST, 0, $offset, true) +
	array('firstDigitTimeoutSeconds' => $arrSpl) +
	array_slice($_POST, $offset, NULL, true);
	
        server_fail_over_debuggin_testing(); /* for fail Over testing. */
        $_POST = sequencePostData($_POST, $sequenceArray);
        
        foreach ($_POST as $key => $value)
	{
		if ($key != "aaId" && $key != "aaPhoneActivateNumber") //hidden form fields that don't change don't need to be checked or displayed
		{
			if (!is_array($value))
			{
				if ($value !== $_SESSION["aa"][$key])
				{
					if ($key == "aaName" or $key == "callingLineIdLastName" or $key == "callingLineIdFirstName" or $key == "timeZone" or $key == "department" or $key == "phoneNumber" or $key == "extension")
					{
						$sipDo = 1;
						$sipHeader = "<serviceInstanceProfile>";
						if ($key == "aaName")
						{
							$sip .= "<name>" . $value . "</name>";
						}
                                               if ($key == "callingLineIdLastName"){
                                                    $sip .= "<callingLineIdLastName>" . $value . "</callingLineIdLastName>";
                                                }
                                                
						if ($key == "callingLineIdFirstName"){
                                                    $sip .= "<callingLineIdFirstName>" . $value . "</callingLineIdFirstName>";
						}
						if($key == "phoneNumber"){
							if(!empty($value)){
								$sip .= "<phoneNumber>".$value."</phoneNumber>";
							}else{
								$sip .= "<phoneNumber xsi:nil='true'>".$value."</phoneNumber>";
							}
						}
						if($key == "extension"){
							if(!empty($value)){
								$sip .= "<extension>".$value."</extension>";
							}else{
								$sip .= "<extension xsi:nil='true'>".$value."</extension>";
							}
						}
						if ($key == "timeZone")
						{
							$sip .= "<timeZone>" . $value . "</timeZone>";
						}
						if ($key == "department"){
							if(strlen($value) > 0){
								$sip.= "<department xsi:type='GroupDepartmentKey'>";
								$sip.= "<serviceProviderId>". htmlspecialchars($_SESSION['sp'])."</serviceProviderId>";
								$sip.= "<groupId>".htmlspecialchars($_SESSION['groupId'])."</groupId>";
									$sip.= "<name>".$value."</name>";
								$sip.= "</department>";
							}else{
								$sip .= "<department xsi:nil=\"true\" xsi:type='GroupDepartmentKey'/>";
							}
							
						}
						$sipFooter = "</serviceInstanceProfile>";
					}
					if($key == "eVSupport"){
						$xmlData.= "<enableVideo>".$value."</enableVideo>";
					}
					if($key == "nameDialingScope"){
						$xmlData.= "<nameDialingScope>".$value."</nameDialingScope>";
					}
					if($key == "extensionDialingScope"){
						$xmlData.= "<extensionDialingScope>".$value."</extensionDialingScope>";
					}
					if ($ociVersion == "21" || $ociVersion == "22"){
						if($key == "firstDigitTimeoutSeconds"){
							$xmlData.= "<firstDigitTimeoutSeconds>".$value."</firstDigitTimeoutSeconds>";
						}
					}
					if ($key == "businessHoursSchedule")
					{
						$businessValueExplode = explode("**", $value);
						$value = $businessValueExplode[0];
						if (strlen($value) > 0)
						{
							$xmlData .= "<businessHours>
										<type>". htmlspecialchars($businessValueExplode[1]) ."</type>
										<name>" . htmlspecialchars($value) . "</name>
									</businessHours>";
						}
						else
						{
							$xmlData .= "<businessHours xsi:nil=\"true\" />";
						}
					}
					if ($key == "holidaySchedule")
					{
						$holidayValueExplode = explode("**", $value);
						$value = $holidayValueExplode[0];
						if (strlen($value) > 0)
						{
							$xmlData .= "<holidaySchedule>
										<type>". htmlspecialchars($holidayValueExplode[1]) ."</type>
										<name>" . htmlspecialchars($value) . "</name>
									</holidaySchedule>";
						}
						else
						{
							$xmlData .= "<holidaySchedule xsi:nil=\"true\" />";
						}
					}
					if ($key == "nameDialingEntries")
					{
						$xmlData .= "<nameDialingEntries>" . $value . "</nameDialingEntries>";
					}
					$changes[$a][$_SESSION["aaNames"][$key]]["oldValue"] = $_SESSION["aa"][$key];
					$changes[$a][$_SESSION["aaNames"][$key]]["newValue"] = $value;
					
					if ($key == "aaActivateNumber" && !empty($_POST['aaPhoneActivateNumber'])){
						
						if($_SESSION["aa"][$key] == "No"){
							$oldCodes = "Deactivated";
						}else{
							$oldCodes = "Activated";
						}
						
						if($_POST[$key] == "No"){
							$newCodes = "Deactivated";
						}else{
							$newCodes = "Activated";
						}
						
						$changes[$a][$_SESSION["aaNames"][$key]]["oldValue"] = $oldCodes;
						$changes[$a][$_SESSION["aaNames"][$key]]["newValue"] = $newCodes;
					}
					$a++;
				}
				
				if($key == "redirectedCallsCOLPPrivacy" || $key == "callBeingForwardedResponseCallType"){
					$updateCallPolicyArray['autoAttendantId'] = $aaId;

					if($key == "redirectedCallsCOLPPrivacy"){
						$changes[$a][$_SESSION["aaNames"][$key]]["oldValue"] = $_SESSION["aa"][$key];
						$changes[$a][$_SESSION["aaNames"][$key]]["newValue"] = $value;
						$updateCallPolicyArray['redirectedCallsCOLPPrivacy'] = $value;
					}
					if($key == "callBeingForwardedResponseCallType"){ 
						$changes[$a][$_SESSION["aaNames"][$key]]["oldValue"] = $_SESSION["aa"][$key];
						$changes[$a][$_SESSION["aaNames"][$key]]["newValue"] = $value;
						$updateCallPolicyArray['callBeingForwardedResponseCallType'] = $value;
					} 
				}
			}
			else
			{	
				$announcementAutoAttendantArray['auto_attendant_id'] = $aaId;
				
				if ($key == "vp" && isset($_POST['vp']['announcementId'])){
					if(isset($_POST['vp']['announcementId']) && !empty($_POST['vp']['announcementId'])){
						
						$aaOpObj->voicePortalCustomAnnouncementModifyRequest($aaId, $_POST['vp']['announcementId']);
					}
				}
				
				if ($key == "bh" or $key == "ah" or $key == "h" or strpos($key, "submenu") === 0)
				{	
					if ($key == "bh")
					{
						$header = "<businessHoursMenu>";
						$header .= "<announcementSelection>".$_POST['bh']['announcementSelection']."</announcementSelection>";						
						
						if(isset($_POST['bh']['announcementId']) && !empty($_POST['bh']['announcementId'])){
						    $bhArr = explode(".", $_POST['bh']['announcementId']);
						    $header .= $aaOpObj->xmlInputForAudio($bhArr);
						}else{
						    $header .= "<audioFile xsi:nil='true'></audioFile>";
						}
						if($_SESSION["aa"]["eVSupport"] == "true"){
						    if(isset($_POST['bh']['vidAnnouncementId']) && !empty($_POST['bh']['vidAnnouncementId'])){
						        $bhArrVid = explode(".", $_POST['bh']['vidAnnouncementId']);
						        $header .= $aaOpObj->xmlInputForVideo($bhArrVid);
						    }else{
						        $header .= "<videoFile xsi:nil='true'></videoFile>";
						    }
						}
					}
					if ($key == "ah")
					{
						$header = "<afterHoursMenu>";
						$header .= "<announcementSelection>".$_POST['ah']['announcementSelection']."</announcementSelection>";						
						
						if(isset($_POST['ah']['announcementId']) && !empty($_POST['ah']['announcementId'])){
						    $ahArr = explode(".", $_POST['ah']['announcementId']);
						    $header .= $aaOpObj->xmlInputForAudio($ahArr);
						}else{
						    $header .= "<audioFile xsi:nil='true'></audioFile>";
						}
						if($_SESSION["aa"]["eVSupport"] == "true"){
						    if(isset($_POST['ah']['vidAnnouncementId']) && !empty($_POST['ah']['vidAnnouncementId'])){
						        $bhArrVid = explode(".", $_POST['ah']['vidAnnouncementId']);
						        $header .= $aaOpObj->xmlInputForVideo($bhArrVid);
						    }else{
						        $header .= "<videoFile xsi:nil='true'></videoFile>";
						    }
						}
					}
					if ($key == "h")
					{
						$header = "<holidayMenu>";
						$header .= "<announcementSelection>".$_POST['h']['announcementSelection']."</announcementSelection>";
						
						
						if(isset($_POST['h']['announcementId']) && !empty($_POST['h']['announcementId'])){
						    $hhArr = explode(".", $_POST['h']['announcementId']);
						    $header .= $aaOpObj->xmlInputForAudio($hhArr);
						}else{
						    $header .= "<audioFile xsi:nil='true'></audioFile>";
						}
						if($_SESSION["aa"]["eVSupport"] == "true"){
						    if(isset($_POST['h']['vidAnnouncementId']) && !empty($_POST['h']['vidAnnouncementId'])){
						        $hhArrVid = explode(".", $_POST['h']['vidAnnouncementId']);
						        $header .= $aaOpObj->xmlInputForVideo($hhArrVid);
						    }else{
						        $header .= "<videoFile xsi:nil='true'></videoFile>";
						    }
						}
					}					
					if (strpos($key, "submenu") === 0)
					{
						if ($ociVersion == "19")
						{
						    $header = xmlHeader($sessionid, "GroupAutoAttendantSubmenuModifyRequest");
						}
						else
						{
						    $header = xmlHeader($sessionid, "GroupAutoAttendantSubmenuModifyRequest20");
						}
						$header .= "<serviceUserId>" . $aaId . "</serviceUserId>";
						$header .= "<submenuId>" . htmlspecialchars($_SESSION["aa"][$key]["id"]) . "</submenuId>";
						
                                                if ($value["id"] !== $_SESSION["aa"][$key]["id"])
						{
							$header .= "<newSubmenuId>" . htmlspecialchars($value["id"]) . "</newSubmenuId>";
							$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]][$_SESSION["aaNames"]["id"]]["oldValue"] = $_SESSION["aa"][$key]["id"];
							$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]][$_SESSION["aaNames"]["id"]]["newValue"] = $value["id"];
							$a++;
						}
                                                
                                                $header .= "<announcementSelection>".$value['announcementSelection']."</announcementSelection>";
						if(isset($value["announcementId"]) && !empty($value["announcementId"])){
						    
						    $subArr = explode(".", $value["announcementId"]);
						    $header .= $aaOpObj->xmlInputForAudio($subArr);
						    
						}else{
						    $header .= "<audioFile xsi:nil='true'></audioFile>";
						}
						if($_SESSION["aa"]["eVSupport"] == "true"){
						    if(isset($value["vidAnnouncementId"]) && !empty($value["vidAnnouncementId"])){
						        $subArrVid = explode(".", $value["vidAnnouncementId"]);
						        $header .= $aaOpObj->xmlInputForVideo($subArrVid);
						    }else{
						        $header .= "<videoFile xsi:nil='true'></videoFile>";
						    }
						}
						// code comment for wrong sequence
						/* if ($value["id"] !== $_SESSION["aa"][$key]["id"])
						{
							$header .= "<newSubmenuId>" . htmlspecialchars($value["id"]) . "</newSubmenuId>";
							$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]][$_SESSION["aaNames"]["id"]]["oldValue"] = $_SESSION["aa"][$key]["id"];
							$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]][$_SESSION["aaNames"]["id"]]["newValue"] = $value["id"];
							$a++;
						} */
					}
					if ($value["enableLevelExtensionDialing"] !== $_SESSION["aa"][$key]["enableLevelExtensionDialing"])
					{
						if (strpos($key, "submenu") === 0)
						{
							$header .= "<enableLevelExtensionDialing>" . $value["enableLevelExtensionDialing"] . "</enableLevelExtensionDialing>";
						}
						else
						{
							$header .= "<enableFirstMenuLevelExtensionDialing>" . $value["enableLevelExtensionDialing"] . "</enableFirstMenuLevelExtensionDialing>";
						}
						if ($key == "bh")
						{
							$changes[$a]["BusinessHours"][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["oldValue"] = $_SESSION["aa"][$key]["enableLevelExtensionDialing"];
							$changes[$a]["BusinessHours"][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["newValue"] = $value["enableLevelExtensionDialing"];
						}
						if ($key == "ah")
						{
							$changes[$a]["AfterHours"][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["oldValue"] = $_SESSION["aa"][$key]["enableLevelExtensionDialing"];
							$changes[$a]["AfterHours"][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["newValue"] = $value["enableLevelExtensionDialing"];
						}
						if ($key == "h")
						{
							$changes[$a]["Holiday"][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["oldValue"] = $_SESSION["aa"][$key]["enableLevelExtensionDialing"];
							$changes[$a]["Holiday"][$_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"]]["newValue"] = $value["enableLevelExtensionDialing"];
						}
						if (strpos($key, "submenu") === 0)
						{
							$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]][$_SESSION["aaNames"]["enableLevelExtensionDialing"]]["oldValue"] = $_SESSION["aa"][$key]["enableLevelExtensionDialing"];
							$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]][$_SESSION["aaNames"]["enableLevelExtensionDialing"]]["newValue"] = $value["enableLevelExtensionDialing"];
						}
						$a++;
					}
					$keys = "";
					foreach ($value["keys"] as $k => $v)
					{
						if (
							((isset($_SESSION["aa"][$key]["keys"][$k]["desc"]) and trim($_SESSION["aa"][$key]["keys"][$k]["desc"]) !== trim($v["desc"])) or !isset($_SESSION["aa"][$key]["keys"][$k]["desc"]))
							or
							((isset($_SESSION["aa"][$key]["keys"][$k]["action"]) and trim($_SESSION["aa"][$key]["keys"][$k]["action"]) !== trim($v["action"])) or !isset($_SESSION["aa"][$key]["keys"][$k]["action"]))
							or
							((isset($_SESSION["aa"][$key]["keys"][$k]["phoneNumber"]) and trim($_SESSION["aa"][$key]["keys"][$k]["phoneNumber"]) !== trim($v["phoneNumber"])) or !isset($_SESSION["aa"][$key]["keys"][$k]["phoneNumber"]))
							or
							((isset($_SESSION["aa"][$key]["keys"][$k]["submenuId"]) and trim($_SESSION["aa"][$key]["keys"][$k]["submenuId"]) !== trim($v["submenu"])) or !isset($_SESSION["aa"][$key]["keys"][$k]["submenuId"]))
							or
							((isset($_SESSION["aa"][$key]["keys"][$k]["announcements"]) and trim($_SESSION["aa"][$key]["keys"][$k]["announcements"]) !== trim($v["announcements"])) or !isset($_SESSION["aa"][$key]["keys"][$k]["announcements"]))
						)
						{
							$keys .= "<keyConfiguration>";
							$keys .= "<key>" . $k . "</key>";

							if ($v["action"] == "Transfer To Submenu")
							{
								$actionOld = isset($_SESSION["aa"][$key]["keys"][$k]["submenuId"]) ? $_SESSION["aa"][$key]["keys"][$k]["submenuId"] : "";
								$actionNew = $v["submenu"];
							}
							else
							{
								$actionOld = isset($_SESSION["aa"][$key]["keys"][$k]["phoneNumber"]) ? $_SESSION["aa"][$key]["keys"][$k]["phoneNumber"] : "";
								$actionNew = $v["phoneNumber"];
							}
							if ($key == "bh")
							{
								$changes[$a]["BusinessHours"]["keys"][$k]["oldValue"] = (isset($_SESSION["aa"][$key]["keys"][$k]["desc"]) ? $_SESSION["aa"][$key]["keys"][$k]["desc"] : "") . ":" . (isset($_SESSION["aa"][$key]["keys"][$k]["action"]) ? $_SESSION["aa"][$key]["keys"][$k]["action"] : "") . ":" . $actionOld;
								$changes[$a]["BusinessHours"]["keys"][$k]["newValue"] = $v["desc"] . ":" . $v["action"] . ":" . $actionNew;
							}
							if ($key == "ah")
							{
								$changes[$a]["AfterHours"]["keys"][$k]["oldValue"] = (isset($_SESSION["aa"][$key]["keys"][$k]["desc"]) ? $_SESSION["aa"][$key]["keys"][$k]["desc"] : "") . ":" . (isset($_SESSION["aa"][$key]["keys"][$k]["action"]) ? $_SESSION["aa"][$key]["keys"][$k]["action"] : "") . ":" . $actionOld;
								$changes[$a]["AfterHours"]["keys"][$k]["newValue"] = $v["desc"] . ":" . $v["action"] . ":" . $actionNew;
							}
							if ($key == "h")
							{
								$changes[$a]["Holiday"]["keys"][$k]["oldValue"] = (isset($_SESSION["aa"][$key]["keys"][$k]["desc"]) ? $_SESSION["aa"][$key]["keys"][$k]["desc"] : "") . ":" . (isset($_SESSION["aa"][$key]["keys"][$k]["action"]) ? $_SESSION["aa"][$key]["keys"][$k]["action"] : "") . ":" . $actionOld;
								$changes[$a]["Holiday"]["keys"][$k]["newValue"] = $v["desc"] . ":" . $v["action"] . ":" . $actionNew;
							}
							if (strpos($key, "submenu") === 0)
							{
								$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]]["keys"][$k]["oldValue"] = (isset($_SESSION["aa"][$key]["keys"][$k]["desc"]) ? $_SESSION["aa"][$key]["keys"][$k]["desc"] : "") . ":" . (isset($_SESSION["aa"][$key]["keys"][$k]["action"]) ? $_SESSION["aa"][$key]["keys"][$k]["action"] : "") . ":" . $actionOld;
								$changes[$a]["Submenu-" . $_SESSION["aa"][$key]["id"]]["keys"][$k]["newValue"] = $v["desc"] . ":" . $v["action"] . ":" . $actionNew;
							}
							$a++;
							
							if (trim($v["desc"]) == "" and trim($v["action"]) == "" and trim($v["phoneNumber"]) == "" and trim($v["submenu"]) == "")
							{
								$keys .= "<entry xsi:nil=\"true\" />";
							}
							else
							{
								$keys .= "<entry>";
								if (trim($v["desc"]) == "")
								{
									$keys .= "<description xsi:nil=\"true\" />";
								}
								else
								{
									$keys .= "<description>" . $v["desc"] . "</description>";
								}
								$keys .= "<action>" . $v["action"] . "</action>";
								if (trim($v["phoneNumber"]) == "")
								{
									$keys .= "<phoneNumber xsi:nil=\"true\" />";
								}
								else
								{
									$keys .= "<phoneNumber>" . $v["phoneNumber"] . "</phoneNumber>";
								}
								
								if($v["action"] == "Transfer To Submenu"){
								    if (trim($v["submenu"]) == "")
								    {
								        $keys .= "<submenuId xsi:nil=\"true\" />";
								    }
								    else
								    {
								        $keys .= "<submenuId>" . htmlspecialchars($v["submenu"]) . "</submenuId>";
								    }
								}
								
								if($v["action"] == "Play Announcement"){
								    if(trim($v["announcements"]) != ""){
								        $hhArrAnn = explode(".", $v["announcements"]);
								        $keys .= $aaOpObj->xmlInputForAudio($hhArrAnn);
								        
								    }else{
								        $keys .= "<audioFile xsi:nil='true'></audioFile>";
								    }
								    if($_SESSION["aa"]["eVSupport"] == "true"){
								        if(trim($v["vidAnnouncements"]) != ""){
								            $ArrAnnVid = explode(".", $v["vidAnnouncements"]);
								            $keys .= $aaOpObj->xmlInputForVideo($ArrAnnVid);
								        }else{
								            $keys .= "<videoFile xsi:nil='true'></videoFile>";
								        }
								    }
								}
								$keys .= "</entry>";
							}
							$keys .= "</keyConfiguration>";
						}
					}
					if ($key == "bh")
					{
						$footer = "</businessHoursMenu>";
					}
					if ($key == "ah")
					{
						$footer = "</afterHoursMenu>";
					}
					if ($key == "h")
					{
						$footer = "</holidayMenu>";
					}
					if (strpos($key, "submenu") === 0)
					{
						$footer = xmlFooter();
					}

					if (strpos($key, "submenu") === 0)
					{
					    
						$response = $client->processOCIMessage(array("in0" => $header . $keys . $footer));
						$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
						readError($xml);
					}
					else
					{
						$menuData .= $header . $keys . $footer;
					}
					//echo $menuData; die;
				}
			}
			
			if ($key == "assignedServices"){
				$controllerObj = new AaController();
				$compareObj = new AaOperation();
				$compareResult = $compareObj->checkAssignedServiceChange($_POST["assignedServices"], $_SESSION["serviceData"]["assigned"]);
				if(count($compareResult["newAssign"]) > 0){
					$serviceAddResp = $controllerObj->addServicesToAA($aaId, $compareResult["newAssign"]);
				}
				if(count($compareResult["unAssign"]) > 0){
					$serviceRemoveResp = $controllerObj->removeServicesFromAA($aaId, $compareResult["unAssign"]);
				}
			}
			
			if($key == "newSubmenu" && $_POST["checkNewSubMenuStatus"] == "true"){
			    
			    $headerForXML = "";
			    $playAnn = array();
			    $controllerObj = new AaController();
			    foreach($_POST["newSubmenu"]["keys"] as $key => $value){
			        if($value["action"] == "Play Announcement"){
			            
			            if($_SESSION["aa"]["eVSupport"] == "true"){
			                if($value["announcements"] != ""){
			                    $subGreetingAr = explode(".", $value["announcements"]);
			                    $playAnn[$key] = $aaOpObj->xmlInputForAudio($subGreetingAr);
			                }
			                if($value["vidAnnouncements"] != ""){
			                    $subGreetingArn = explode(".", $value["vidAnnouncements"]);
			                    $playAnn[$key] .= $aaOpObj->xmlInputForVideo($subGreetingArn);
			                }
			                
			            }else{
			                if($value["announcements"] != ""){
			                    $subGreetingAr = explode(".", $value["announcements"]);
			                    $playAnn[$key] = $aaOpObj->xmlInputForAudio($subGreetingAr);
			                }
			            }
			        }
			    }
			    
			    
			    if($_POST['newSubmenu']['bhGreeting'] == "Personal Greeting"){
			        if($_SESSION["aa"]["eVSupport"] == "true"){
			            if($_POST['newSubmenu']['announcementId'] != ""){
			                $subGreetingArrAnn = explode(".", $_POST['newSubmenu']['announcementId']);
			                $headerForXML .= $aaOpObj->xmlInputForAudio($subGreetingArrAnn);
			                
			            }else{
			                $headerForXML .= "<audioFile xsi:nil='true'></audioFile>";
			            }
			            
			            if($_POST['newSubmenu']['vidAnnouncementId'] != ""){
			                $subGreetingArrAnnVid = explode(".", $_POST['newSubmenu']['vidAnnouncementId']);
			                $headerForXML .= $aaOpObj->xmlInputForVideo($subGreetingArrAnnVid);
			            }else{
			                $header .= "<videoFile xsi:nil='true'></videoFile>";
			            }
			            
			        }else{
			            if($_POST['newSubmenu']['announcementId'] != ""){
			                $subGreetingArrAnn = explode(".", $_POST['newSubmenu']['announcementId']);
			                $headerForXML .= $aaOpObj->xmlInputForAudio($subGreetingArrAnn);
			                
			            }else{
			                $headerForXML .= "<audioFile xsi:nil='true'></audioFile>";
			            }
			        }
			    }
			    
			    $subMenuResponse = $controllerObj->createSubmenu($_POST["newSubmenu"], $_POST["aaId"], $headerForXML, $playAnn);
			  
			}
		}
	}
	//$getAnnouncementInfo = $announcement->saveAutoattendantAnnouncement($db, $announcementAutoAttendantArray);
	$callPolicy = $aaController->modifyCallPolicies($updateCallPolicyArray);
	
	
	$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantModifyInstanceRequest20");
	
	$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
	if (isset($sipDo) and $sipDo == 1)
	{
            $xmlinput .= $sipHeader . $sip . $sipFooter;
	}
	$xmlinput .= $xmlData . $menuData . xmlFooter();

	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	
	readError($xml);
	
	if (isset($_POST["aaActivateNumber"]) && $_POST["aaActivateNumber"] == "Yes"){
		if(isset($_POST["aaPhoneActivateNumber"])  && !empty($_POST["aaPhoneActivateNumber"])){
			$aaDn = new Dns ();
			$dnPostArray = array(
					"sp" => $_SESSION['sp'],
					"groupId" => $_SESSION['groupId'],
					"phoneNumber" => array($_POST['aaPhoneActivateNumber'])
			);
			$dnsActiveResponse = $aaDn->activateDNRequest($dnPostArray);
		}
	}else if(isset($_POST["aaActivateNumber"]) && $_POST["aaActivateNumber"] == "No"){
		if(isset($_POST["aaPhoneActivateNumber"])  && !empty($_POST["aaPhoneActivateNumber"])){
			$aaDn = new Dns ();
			$dnPostArray = array(
					"sp" => $_SESSION['sp'],
					"groupId" => $_SESSION['groupId'],
					"phoneNumber" => array($_POST['aaPhoneActivateNumber'])
			);
			$dnsActiveResponse = $aaDn->deActivateDNRequest($dnPostArray);
		}
	}
	//echo "checkifNewSubCreated";echo $checkifNewSubCreated;
	/*if($checkifNewSubCreated == "1"){
	   $where['auto_attendant_id'] = $_POST['aaId'];
	   $announcementSub = $announcement->getAnnouncementSubMenu($db, $where);
	   
	   array_push($announcementSub,$submenuInsertionIntoDB);
	   $var12 =  $announcement->insertAndUpdateSubmenu($db, $announcementSub,$_POST['aaId']);
	}*/
	require_once("/var/www/lib/broadsoft/adminPortal/getFinishedAAInfo21.php");

	$data = "<table cellspacing=\"0\" cellpadding=\"5\" align=\"center\" class=\"dialogClass\" width=\"70%\" style='margin:0 auto;'>";
	$data .= "<tr><th colspan=\"2\" align=\"center\">Changes complete. Here are your new auto attendant settings.</th></tr>";
	$data .= "<tr><td>&nbsp;</td></tr>";
	foreach ($aa as $key => $value)
	{
		if (!is_array($value))
		{
			if($key != "aaActivateNumber"){
				$data .= "<tr><td style=\"font-weight:bold;\">" . $_SESSION["aaNames"][$key] . "</td><td>" . $value . "</td></tr>";
			}else{
				if($key == "aaActivateNumber" && !empty($_POST['aaPhoneActivateNumber'])){
					if($value == "Yes"){
					    $value = $_POST['aaPhoneActivateNumber']." have been Activated";
					}else{
					    $value = $_POST['aaPhoneActivateNumber']." have been Deactivated";
					}
				}
				$data .= "<tr><td style=\"font-weight:bold;\">" . $_SESSION["aaNames"][$key] . "</td><td>" . $value . "</td></tr>";
			}
		}
		if ($key == "vp" or $key == "bh" or $key == "ah" or $key == "h" or strpos($key, "submenu") === 0)
		{
			$data .= "<tr><td colspan=\"2\"><table align=\"center\" width=\"100%\">";
			$data .= "<tr><td align=\"center\" colspan=\"4\" class=\"miniBanner\">";
			if ($key == "vp")
			{
				$data .= "Voice Portal Menu";
				$keyLabel = $_SESSION["aaNames"]["vpAnnouncementId"];
			}
			if ($key == "bh")
			{
				$data .= "Business Hours Menu";
				$keyLabel = $_SESSION["aaNames"]["bhAnnouncementId"];
			}
			if ($key == "ah")
			{
				$data .= "After Hours Menu";
				$keyLabel = $_SESSION["aaNames"]["ahAnnouncementId"];
			}
			if ($key == "h")
			{
				$data .= "Holiday Menu";
				$keyLabel = $_SESSION["aaNames"]["hAnnouncementId"];
			}
			if (strpos($key, "submenu") === 0)
			{
				$data .= "Submenu - " . $aa[$key]["id"];
			}
			$data .= "</td></tr>";
			if(!empty($aa[$key]['announcementId'])){
				$data .= "<tr><td style=\"font-weight:bold;\" colspan=\"2\">".$keyLabel."</td><td colspan=\"2\">".$aa[$key]['announcementId']."</td></tr>";
			}
			$data .= "<tr><td colspan=\"4\"><span style=\"font-weight:bold;\">";
			if (strpos($key, "submenu") === 0)
			{
				$data .= $_SESSION["aaNames"]["enableLevelExtensionDialing"];
			}
			else
			{
				$data .= $_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"];
			}
			$data .= "</span> " . $value["enableLevelExtensionDialing"] . "</td></tr>";
			$data .= "<tr><th style=\"width:10%\">Key</th><th width=\"30%\">Action</th><th width=\"30%\">Description</th><th width=\"30%\">Action Data</th></tr>";
			foreach ($value["keys"] as $k => $v)
			{
				$data .= "<tr><td style=\"width:10%\">" . $k . "</td><td style=\"width:30%\">" . $v["action"] . "</td><td style=\"width:30%\">" . $v["desc"] . "</td><td>";
				if ($v["action"] == "Transfer To Submenu")
				{
					$data .= $v["submenuId"];
				}else if($v["action"] == "Play Announcement"){		
				    $data .= $v["announcements"];
									
				}else
				{
					$data .= $v["phoneNumber"];
				}
				$data .= "</td></tr>";
			}
			$data .= "</table></td></tr>";
		}
	}
	$data .= "</table>";
	echo $data;

	$date = date("Y-m-d H:i:s");

        /*
	$query = "INSERT into changeLog (userName, date, module, groupId, entityName)";
	$query .= " VALUES ('" . $_SESSION["loggedInUserName"] . "', '" . $date . "', 'Auto Attendant Modify', '" . $_SESSION["groupId"] . "', '" . $_SESSION["aa"]["aaName"] . "')";
	$sth = $expProvDB->expressProvLogsDb->query($query);
	$lastId = $expProvDB->expressProvLogsDb->lastInsertId();
	*/
        
	$postArray = $_POST;
	if(isset($_POST["newSubmenu"]["id"]) && empty($_POST["newSubmenu"]["id"])){
	    unset($postArray["newSubmenu"]);
	}
	
	unset($postArray["availableServices"]);
	unset($postArray["checkNewSubMenuStatus"]);
	$sessionArray = $_SESSION["aa"];
	unset($sessionArray["submenuPersonalGreetings"]);
	unset($sessionArray["aaActivateNumber"]);
	unset($postArray["aaActivateNumber"]);
	
	$diffInArray = $aaOpObj->diffArrayFinder($postArray, $sessionArray);
	
	unset($diffInArray["aaId"]);
	//print_r($diffInArray);
	//exit;
        
        //Code added @ 23 Jan 2019
        if(isset($diffInArray) && count($diffInArray)> 0){
            //$cLUObj         = new ChangeLogUtility($userId, $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
            $module         = "Auto Attendant Modify";
            $tableName      = "aaModChanges";
            $entityName     = $_SESSION["aa"]["aaName"];
            $changeLogArr   = array(); 
            
            
            
            foreach ($diffInArray as $changeLogKey => $changeLogVal )
            { 
                if($changeLogKey == "bh" || $changeLogKey == "ah" || $changeLogKey == "h" || strpos($key, "submenu") === 0 || $key == "newSubmenu")
                {  
                        foreach ($changeLogVal as $keyIn => $valIn)
                        {
                            if($keyIn== "announcementId" || $keyIn== "vidAnnouncementId" || $keyIn== "enableLevelExtensionDialing" || $keyIn== "announcementSelection"){
                                
                                $changeLogArr[$changeLogKey.":".$keyIn]['oldValue'] = $valIn["oldValue"];
                                $changeLogArr[$changeLogKey.":".$keyIn]['newValue'] = $valIn["newValue"];
                                $cLUObj->changesArr = $changeLogArr;
                            }
                            else
                            {
                                foreach ($valIn as $keyTLabel => $valTLabel)
                                {
                                    foreach ($valTLabel as $keyFLabel => $valFLabel)
                                    {                                        
                                        $changeLogArr[$changeLogKey.": ".$keyIn.": keys-".$keyTLabel.": ".$keyFLabel]['oldValue'] = $valFLabel["oldValue"];
                                        $changeLogArr[$changeLogKey.": ".$keyIn.": keys-".$keyTLabel.": ".$keyFLabel]['newValue'] = $valFLabel["newValue"];
                                        $cLUObj->changesArr = $changeLogArr;
	                            }
	                       }
                            }
	            }
                } 
                else if($changeLogKey == "vp")
                {
                    $changeLogArr[$changeLogKey]['oldValue'] = $changeLogVal["announcementId"]["oldValue"];
                    $changeLogArr[$changeLogKey]['newValue'] = $changeLogVal["announcementId"]["newValue"];
                    $cLUObj->changesArr = $changeLogArr;
                }
                else 
                {
                    $changeLogArr = $diffInArray;
                    $cLUObj->changesArr = $changeLogArr;
                }
            }
                        
            $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
        }
        //End code
        
        
	/*
	if(isset($diffInArray) && count($diffInArray)> 0){
	    foreach ($diffInArray as $changeLogKey => $changeLogVal ){
	        if($changeLogKey == "bh" || $changeLogKey == "ah" || $changeLogKey == "h" || strpos($key, "submenu") === 0 || $key == "newSubmenu"){
	            foreach ($changeLogVal as $keyIn => $valIn){
	                if($keyIn== "announcementId" || $keyIn== "vidAnnouncementId" || $keyIn== "enableLevelExtensionDialing" || $keyIn== "announcementSelection"){
	                    $insert = "INSERT into aaModChanges (id, serviceId, entityName, field, oldValue, newValue)";
	                    $values = " VALUES ('" . $lastId . "', '" . $aaId . "', '" . $_SESSION["aa"]["aaName"] . "', '" . $changeLogKey.":".$keyIn . "', '" . $valIn["oldValue"] . "', '" . $valIn["newValue"] . "')";
	                    $query = $insert . $values;
	                    $uth = $expProvDB->expressProvLogsDb->query($query);
	                }else{
	                    foreach ($valIn as $keyTLabel => $valTLabel){
	                        foreach ($valTLabel as $keyFLabel => $valFLabel){
	                            $insert = "INSERT into aaModChanges (id, serviceId, entityName, field, oldValue, newValue)";
	                            $values = " VALUES ('" . $lastId . "', '" . $aaId . "', '" . $_SESSION["aa"]["aaName"] . "', '" . $changeLogKey.": ".$keyIn.": keys-".$keyTLabel.": ".$keyFLabel . "', '" . $valFLabel["oldValue"] . "', '" . $valFLabel["newValue"] . "')";
	                            $query = $insert . $values;
	                            $uth = $expProvDB->expressProvLogsDb->query($query);
	                        }
	                        
	                    }
	                }
	            }
	            
	        }else if($changeLogKey == "vp"){
	            $insert = "INSERT into aaModChanges (id, serviceId, entityName, field, oldValue, newValue)";
	            $values = " VALUES ('" . $lastId . "', '" . $aaId . "', '" . $_SESSION["aa"]["aaName"] . "', '" . $changeLogKey . "', '" . $changeLogVal["announcementId"]["oldValue"] . "', '" . $changeLogVal["announcementId"]["newValue"] . "')";
	            $query = $insert . $values;
	            $uth = $expProvDB->expressProvLogsDb->query($query);
	        }else{
	            //$key = key($changes[$a]);
	            $insert = "INSERT into aaModChanges (id, serviceId, entityName, field, oldValue, newValue)";
	            $values = " VALUES ('" . $lastId . "', '" . $aaId . "', '" . $_SESSION["aa"]["aaName"] . "', '" . $changeLogKey . "', '" . $changeLogVal["oldValue"] . "', '" . $changeLogVal["newValue"] . "')";
	            $query = $insert . $values;
	            $uth = $expProvDB->expressProvLogsDb->query($query);
	        }
	    }
	}
        */
        
        function sequencePostData($postData, $sequenceArray) {
            $tempArray = array();
            $newArray = array();
            foreach ($sequenceArray as $element) {
                if( isset( $postData[$element] ) ) {
                    $tempArray[$element] = $postData[$element];
                    unset($postData[$element]);
                }
            }
            $newArray = array_merge($tempArray, $postData);
            return $newArray;
        }
?>

