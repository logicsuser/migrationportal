$(function()
{
	$("#aaTabs").tabs();
	$("#endUserId").html("<?php echo $aaId; ?>");
	
	$("#submenu0AnnouncementId").prop('disabled', 'disabled');
	$("#submenu1AnnouncementId").prop('disabled', 'disabled');
	$("#submenu2AnnouncementId").prop('disabled', 'disabled');
	$("#submenu3AnnouncementId").prop('disabled', 'disabled');
	$("#submenu4AnnouncementId").prop('disabled', 'disabled');
	$("#submenu5AnnouncementId").prop('disabled', 'disabled');
	$("#submenu6AnnouncementId").prop('disabled', 'disabled');
	$("#submenu7AnnouncementId").prop('disabled', 'disabled');
	$("#newSubmenuAnnouncementId").prop("disabled", "disabled");
	$("#newSubmenuVidAnnouncementId").prop("disabled", "disabled");
	$("#newSubmenuAnnouncementId").css({
		'background-color' : '#e9e9e9'
	});
	
	$("#submenu0AnnouncementId").css({
		'background-color' : '#e9e9e9'
	});
	$("#submenu1AnnouncementId").css({
		'background-color' : '#e9e9e9'
	});
	$("#submenu2AnnouncementId").css({
		'background-color' : '#e9e9e9'
	});
	$("#submenu3nnouncementId").css({
		'background-color' : '#e9e9e9'
	});
	$("#submenu4AnnouncementId").css({
		'background-color' : '#e9e9e9'
	});
	$("#submenu5AnnouncementId").css({
		'background-color' : '#e9e9e9'
	});
	$("#submenu6AnnouncementId").css({
		'background-color' : '#e9e9e9'
	});
	$("#submenu7AnnouncementId").css({
		'background-color' : '#e9e9e9'
	});
	
	
	$("#dialogAA").dialog($.extend({}, defaults, {
		width: 900,
		buttons: {
			"Complete": function() {
				pendingProcess.push("Modify Attendants");
				var dataToSend = $("#aaInfo").serialize();
				$.ajax({
					type: "POST",
					url: "autoAttendant21/aaDo.php",
					data: dataToSend,
					success: function(result) {
						if(foundServerConErrorOnProcess(result, "Modify Attendants")) {
	    					return false;
	                  	}
						$("#dialogAA").dialog("option", "title", "Request Complete");
						$(".ui-dialog-titlebar-close", this.parentNode).hide();
						//$(".ui-dialog-buttonpane", this.parentNode).hide();
						$("#dialogAA").html(result);
						//$("#dialogAA").append(returnLink);
						$(".ui-dialog-buttonpane button:contains('More Changes')").button().show().addClass('cancelButton');
						$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
						$(".ui-dialog-buttonpane button:contains('Complete')").button().hide().addClass('subButton');
						$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
						$(".ui-dialog-buttonpane button:contains('Return to Main')").button().show().addClass('cancelButton');
					}
				});
			},
			"Cancel": function() {
				$(this).dialog("close");
			},
			"Delete" : function(){
				pendingProcess.push("Delete Attendants");
				var aaId = $("#modAAId").val();
				$("#dialogAA").html('Deleting the Auto Attendant....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
				$(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
				$.ajax({
					type: "POST",
					url: "autoAttendant21/deleteAA.php",
					data: {modAAId : aaId, deleteAA : 1},
					success: function(result) {
						if(foundServerConErrorOnProcess(result, "Delete Attendants")) {
	    					return false;
	                  	}
						$("#loading2").hide(); 
						var obj = jQuery.parseJSON(result);
						if(obj.Error){
							$("#dialogAA").html(obj.Error);
							$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
							$(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass('deleteButton');;
						}else{							
							$("#dialogAA").html("<div class='labelTextGrey' style='text-align:center'>Auto Attendant has been deleted</div>");
							$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
							$(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
							$(".ui-dialog-buttonpane button:contains('Done')").button().show().addClass('subButton');
							$(".ui-dialog-buttonpane button:contains('Next')").button().hide();
							$(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide().addClass('cancelButton');
							$("#AAData").hide();
                                                        $("#addNewSubmenu").hide();
                                                        $("#AAChoice").val('');
							//to be implemented
							//getAutoAttendantList(0);
						}
					}
				});
			},
			"More Changes" : function(){
				$('html, body').animate({scrollTop: '0px'}, 300);
            	$("#loading2").show();
            	$("#addAutoAttendant").hide();
            	
            	var aaId = $("#modAAId").val();
            	//var autoAttendantId = aaId.split("@");
            	getAAList(aaId);            	
            	$(this).dialog("close"); 
            	$("#AAData").hide();
			},
			"Done" : function(){
				$(this).dialog("close");
				getAAList(0);
				$('#AAData').hide();
				$("html, body").animate({ scrollTop: 0 }, "slow");
			},
                "Return to Main": function() {
                    location.href="main.php";
                            }
                    },
            open: function() {
                            setDialogDayNightMode($(this));
        }
	}));

	$("#subButtonAA").click(function() {
		$('#availableServices option').prop('selected', true);
		$('#assignedServices option').prop('selected', true);
		$(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide();
		var dataToSend = $("#aaInfo").serialize();
		$.ajax({
			type: "POST",
			url: "autoAttendant21/checkData.php",
			data: dataToSend,
			success: function(result) {
				$("#dialogAA").dialog("open");
				$("#dialogAA").dialog("option", "title", "Confirm Setting");
				$(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');;
				$(".ui-dialog-buttonpane button:contains('Cancel')").button().addClass('cancelButton').show();
				$(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
				$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
				$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide().addClass('cancelButton');;
				if (result.slice(0, 1) == "1")
				{
					$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton").removeClass("ui-state-hover");
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().addClass('cancelButton');
				}
				else
				{
					$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled ui-state-hover").addClass("subButton");
					$(".ui-dialog-buttonpane button:contains('Cancel')").button().addClass('cancelButton');
				}
				result = result.slice(1);
				$("#dialogAA").html(result);
			}
		});
	});
	
	$("#subButtonDelAA").click(function(){
		//alert("ewfegg");
		$("#dialogAA").dialog("open");
		$("#dialogAA").dialog("option", "title", "Request Complete");
		
		$("#dialogAA").html("<div class='labelTextGrey' style='text-align:center'>Are you sure you want to delete this Auto Attendant?</div>");
		$(".ui-dialog-buttonpane button:contains('Complete')").button().hide().addClass('subButton');;
		$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
		$(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
		$(".ui-dialog-buttonpane button:contains('Next')").button().hide();
		$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
		$(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass('deleteButton');
		$(".ui-dialog-buttonpane button:contains('Return to Main')").button().hide().addClass('cancelButton');;
		$(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled deleteButton").addClass('deleteButton');
		
	});
	
	$(".deleteThisSubmenu").hide("");
	$("#aaTabs li").click(function(){ 
		var el = $(this);
		var str = el.text();
		if (str.indexOf("Submenu") >= 0 && str != "New Submenu"){
			var splittedData = str.split("-");
			var subMenuName = splittedData[1];
			$(".deleteThisSubmenu").show("");
			$('.deleteThisSubmenuButton').attr('data-submenu',subMenuName);
		}else{
			$(".deleteThisSubmenu").hide("");
		}
	});
	
	$("#dialogSubmenu").dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		buttons: {
			"Delete Submenu": function() {
				var subMenuNameDel = $(".deleteThisSubmenuButton").data('submenu');	
				deleteSubmenuAA(subMenuNameDel);
			},
			"Cancel": function() {
				$(this).dialog("close");
			},
			"Done" : function(){
				$('html, body').animate({scrollTop: '0px'}, 300);
            	$("#loading2").show();
            	$("#addAutoAttendant").hide();
            	
            	var aaId = $("#modAAId").val();
            	var autoAttendantId = aaId.split("@");
            	
            	getAAList(autoAttendantId[0]);            	
            	$(this).dialog("close"); 
            	$("#AAData").hide();
			}
		},
            open: function() {
                    setDialogDayNightMode($(this));
            }
	});
	
	var deleteSubmenuAA = function(subMenuNameDel){
		var aaId = $("#modAAId").val();
		$("#dialogSubmenu").html('Deleting the Submenu....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
		$(":button:contains('Delete Submenu')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
		$("#loading2").show();  
		if(aaId){
			$.ajax({
				type: "POST",
				url: "autoAttendant21/deleteSubmenu.php",
				data: {subName : subMenuNameDel, aaId : aaId},
				success: function(result) {
					$("#loading2").hide();
					var obj = jQuery.parseJSON(result);
					if(obj.Error){
						$("#dialogSubmenu").append('<br/><b>' + obj.Error.Detail+'</b>');
						$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass("cancelButton");
						$(".ui-dialog-buttonpane button:contains('Delete Submenu')").button().show().addClass("deleteButton");
					}else{
						$("#dialogSubmenu").html("");
						$("#dialogSubmenu").html('Submenu has been deleted');
						$(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
						$(".ui-dialog-buttonpane button:contains('Delete Submenu')").button().hide();
						$(".ui-dialog-buttonpane button:contains('Done')").button().show().addClass("subButton");
					}
				}
			});
		}
	}
	
	$(".deleteThisSubmenuButton").click(function(){ 
		var aaId = $("#modAAId").val();
		//var subMenuName = $(this).data('submenu');
		var subMenuName = $(this).attr('data-submenu');
		//alert(subMenuName);
		//alert(subMenuName1);
		$("#dialogSubmenu").dialog("open");
		$("#dialogSubmenu").html('Checking if Submenu is Used....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
		$("#loading2").show();  
		$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
		$(":button:contains('Delete Submenu')").attr("disabled", "disabled").addClass("ui-state-disabled deleteButton");
		if(aaId){
			$.ajax({
				type: "POST",
				url: "autoAttendant21/checkSubMenuUsedStatus.php",
				data: {subName : subMenuName, aaId : aaId},
				success: function(result) {
					$("#loading2").hide();
					//var obj = jQuery.parseJSON(result);
					var errorExist = result.search("noexist");
					if(errorExist > 0){
						$("#dialogSubmenu").html("");
						$("#dialogSubmenu").html('Are you sure want to delete this Submenu <b>'+subMenuName+'</b> ?');
						$(".ui-dialog-buttonpane button:contains('Delete Submenu')").button().show().addClass("deleteButton");
                                                $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass("cancelButton");
						$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
						$(":button:contains('Delete Submenu')").removeAttr("disabled").removeClass("ui-state-disabled");
						//$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
						
					}else{
						$("#dialogSubmenu").html("");
						$("#dialogSubmenu").append('<br/><b>Submenu '+subMenuName+' cannot be deleted as it is being used in AA menu option.</b>');
						$(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass("cancelButton");
						$(".ui-dialog-buttonpane button:contains('Delete Submenu')").button().show().addClass("deleteButton");
						$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
						$(":button:contains('Delete Submenu')").attr("disabled", "disabled").addClass("ui-state-disabled");
					}
				}
			});
		}
		
		/*$("#dialogSubmenu").dialog("open");
		$("#dialogSubmenu").dialog("option", "title", "Request Complete");
		$("#dialogSubmenu").html("Are you sure you want to delete this Submenu - <b>"+subName+"</b> ?");
		$(".ui-dialog-buttonpane button:contains('Cancel')").button().show();
		$(".ui-dialog-buttonpane button:contains('Delete Submenu')").button().show();
		$(".ui-dialog-buttonpane button:contains('Done')").button().hide();
		$(":button:contains('Delete Submenu')").removeAttr("disabled").removeClass("ui-state-disabled");*/
	});

	
	$("#hAnnouncementId").prop('disabled', 'disabled');
	$("#hAnnouncementId").css({ 'background-color' : '#e9e9e9'});
	$("#ahAnnouncementId").prop('disabled', 'disabled');
	$("#ahAnnouncementId").css({ 'background-color' : '#e9e9e9'});
	$("#bhAnnouncementId").prop('disabled', 'disabled');
	$("#bhAnnouncementId").css({ 'background-color' : '#e9e9e9'});
	//$("#vpAnnouncementId").prop('disabled', 'disabled');
	//$("#vpAnnouncementId").css({ 'background-color' : '#e9e9e9'});
	
	$('input[data-announcementSelection=bh]').click(function() {
		
		var el = $(this);
		var ilId = el.attr('id');
		if(ilId == "bhGreetingP"){
			$("#bhAnnouncementId").prop('disabled', false);
			$("#bhAnnouncementId").css({ 'background-color' : '#fff'});
			
			$("#bhvidAnnouncementId").prop('disabled', false);
			$("#bhvidAnnouncementId").css({ 'background-color' : '#fff'});
			
			$("#bhAnnouncementId").parent('div').find(".bhtype").remove();
		}else{
			$("#bhAnnouncementId").prop('disabled', 'disabled');
			$("#bhAnnouncementId").css({ 'background-color' : '#e9e9e9'});
			$("#bhAnnouncementId").val('');
			
			$("#bhvidAnnouncementId").prop('disabled', 'disabled');
			$("#bhvidAnnouncementId").css({ 'background-color' : '#e9e9e9'});
			$("#bhvidAnnouncementId").val('');
			
			$("#bhAnnouncementId").parent('div').append('<input class="bhtype" type="hidden" name="bh[announcementId]" value=""/>');
			$("#bhaudio").hide();
			$("#bhdownload").hide();
		}
	});
	
	$('input[data-announcementSelection=ah]').click(function() {
		
		var el = $(this);
		var ilId = el.attr('id');
		if(ilId == "ahGreetingP"){
			$("#ahAnnouncementId").prop('disabled', false);
			$("#ahAnnouncementId").css({ 'background-color' : '#fff'});
			
			$("#ahvidAnnouncementId").prop('disabled', false);
			$("#ahvidAnnouncementId").css({ 'background-color' : '#fff'});
			
			$("#ahAnnouncementId").parent('div').find(".ahtype").remove();
		}else{
			$("#ahAnnouncementId").prop('disabled', 'disabled');
			$("#ahAnnouncementId").css({ 'background-color' : '#e9e9e9'});
			$("#ahAnnouncementId").val('');
			
			$("#ahvidAnnouncementId").prop('disabled', 'disabled');
			$("#ahvidAnnouncementId").css({ 'background-color' : '#e9e9e9'});
			$("#ahvidAnnouncementId").val('');
			
			$("#ahAnnouncementId").parent('div').append('<input class="ahtype" type="hidden" name="ah[announcementId]" value=""/>');
			$("#ahaudio").hide();
			$("#ahdownload").hide();
		}
	});
	
	$('input[data-announcementSelection=h]').click(function() {
		
		var el = $(this);
		var ilId = el.attr('id');
		if(ilId == "hGreetingP"){
			$("#hAnnouncementId").prop('disabled', false);
			$("#hAnnouncementId").css({ 'background-color' : '#fff'}); 	
			
			$("#hvidAnnouncementId").prop('disabled', false);
			$("#hvidAnnouncementId").css({ 'background-color' : '#fff'});
			
			$("#hAnnouncementId").parent('div').find(".htype").remove();
		}else{
			$("#hAnnouncementId").prop('disabled', 'disabled');
			$("#hAnnouncementId").css({ 'background-color' : '#e9e9e9'});
			$("#hAnnouncementId").val('');
			
			$("#hvidAnnouncementId").prop('disabled', 'disabled');
			$("#hvidAnnouncementId").css({ 'background-color' : '#e9e9e9'});
			$("#hvidAnnouncementId").val('');
			
			$("#hAnnouncementId").parent('div').append('<input class="htype" type="hidden" name="h[announcementId]" value=""/>');
			$("#haudio").hide();
			$("#hdownload").hide();
		}
	});
	
	$('input[data-announcementSelection=vp]').click(function() {
		
		var el = $(this);
		var ilId = el.attr('id');
		if(ilId == "vpGreetingP"){
			$("#vpAnnouncementId").prop('disabled', false);
			$("#vpAnnouncementId").css({ 'background-color' : '#fff'}); 	
			$("#vpAnnouncementId").parent('div').find(".vptype").remove();
		}else{
			$("#vpAnnouncementId").prop('disabled', 'disabled');
			$("#vpAnnouncementId").css({ 'background-color' : '#e9e9e9'});
			$("#vpAnnouncementId").val('');
			$("#vpAnnouncementId").parent('div').append('<input class="vptype" type="hidden" name="vp[announcementId]" value=""/>');
		}
	});
$('input[data-announcementSelection=submenu0]').click(function() {
		
		var el = $(this);
		var ilId = el.attr('id');
		if(ilId == "submenu0GreetingP"){
			$("#submenu0AnnouncementId").prop('disabled', false);
			$("#submenu0AnnouncementId").css({ 'background-color' : '#fff'});
			
			$("#submenu0vidAnnouncementId").prop('disabled', false);
			$("#submenu0vidAnnouncementId").css({ 'background-color' : '#fff'});
			
			$("#submenu0AnnouncementId").parent('div').find(".vptype").remove();
		}else{
			$("#submenu0AnnouncementId").prop('disabled', 'disabled');
			$("#submenu0AnnouncementId").css({ 'background-color' : '#e9e9e9'});
			$("#submenu0AnnouncementId").val('');
			
			$("#submenu0vidAnnouncementId").prop('disabled', 'disabled');
			$("#submenu0vidAnnouncementId").css({ 'background-color' : '#e9e9e9'});
			$("#submenu0vidAnnouncementId").val('');
			//$("#submenu0AnnouncementId").parent('div').append('<input class="vptype" type="hidden" name="vp[announcementId]" value=""/>');
		}
	});

/*$('input[data-announcementSelection=submenu0]').click(function() {
	
	var el = $(this);
	var ilId = el.attr('id');
	if(ilId == "submenu0GreetingP"){
		$("#submenu0AnnouncementId").prop('disabled', false);
		$("#submenu0AnnouncementId").css({ 'background-color' : '#fff'}); 	
		$("#submenu0AnnouncementId").parent('div').find(".vptype").remove();
	}else{
		$("#submenu0AnnouncementId").prop('disabled', 'disabled');
		$("#submenu0AnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu0AnnouncementId").val('');
		//$("#submenu0AnnouncementId").parent('div').append('<input class="vptype" type="hidden" name="vp[announcementId]" value=""/>');
	}
});*/

$('input[data-announcementSelection=submenu1]').click(function() {
	
	var el = $(this);
	var ilId = el.attr('id');
	if(ilId == "submenu1GreetingP"){
		$("#submenu1AnnouncementId").prop('disabled', false);
		$("#submenu1AnnouncementId").css({ 'background-color' : '#fff'}); 
		
		$("#submenu1vidAnnouncementId").prop('disabled', false);
		$("#submenu1vidAnnouncementId").css({ 'background-color' : '#fff'});
		
		$("#submenu1AnnouncementId").parent('div').find(".vptype").remove();
	}else{
		$("#submenu1AnnouncementId").prop('disabled', 'disabled');
		$("#submenu1AnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu1AnnouncementId").val('');
		
		$("#submenu1vidAnnouncementId").prop('disabled', 'disabled');
		$("#submenu1vidAnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu1vidAnnouncementId").val('');
		//$("#submenu0AnnouncementId").parent('div').append('<input class="vptype" type="hidden" name="vp[announcementId]" value=""/>');
	}
});

$('input[data-announcementSelection=submenu2]').click(function() {
	
	var el = $(this);
	var ilId = el.attr('id');
	if(ilId == "submenu2GreetingP"){
		$("#submenu2AnnouncementId").prop('disabled', false);
		$("#submenu2AnnouncementId").css({ 'background-color' : '#fff'});
		
		$("#submenu2vidAnnouncementId").prop('disabled', false);
		$("#submenu2vidAnnouncementId").css({ 'background-color' : '#fff'});
		$("#submenu2AnnouncementId").parent('div').find(".vptype").remove();
	}else{
		$("#submenu2AnnouncementId").prop('disabled', 'disabled');
		$("#submenu2AnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu2AnnouncementId").val('');
		$("#submenu2vidAnnouncementId").prop('disabled', 'disabled');
		$("#submenu2vidAnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu2vidAnnouncementId").val('');
		//$("#submenu0AnnouncementId").parent('div').append('<input class="vptype" type="hidden" name="vp[announcementId]" value=""/>');
	}
});

$('input[data-announcementSelection=submenu3]').click(function() {
	
	var el = $(this);
	var ilId = el.attr('id');
	if(ilId == "submenu3GreetingP"){
		$("#submenu3AnnouncementId").prop('disabled', false);
		$("#submenu3AnnouncementId").css({ 'background-color' : '#fff'}); 
		$("#submenu3vidAnnouncementId").prop('disabled', false);
		$("#submenu3vidAnnouncementId").css({ 'background-color' : '#fff'});
		$("#submenu3AnnouncementId").parent('div').find(".vptype").remove();
	}else{
		$("#submenu3AnnouncementId").prop('disabled', 'disabled');
		$("#submenu3AnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu3AnnouncementId").val('');
		
		$("#submenu3vidAnnouncementId").prop('disabled', 'disabled');
		$("#submenu3vidAnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu3vidAnnouncementId").val('');
		//$("#submenu0AnnouncementId").parent('div').append('<input class="vptype" type="hidden" name="vp[announcementId]" value=""/>');
	}
});
$('input[data-announcementSelection=submenu4]').click(function() {
	
	var el = $(this);
	var ilId = el.attr('id');
	if(ilId == "submenu4GreetingP"){
		$("#submenu4AnnouncementId").prop('disabled', false);
		$("#submenu4AnnouncementId").css({ 'background-color' : '#fff'}); 	
		$("#submenu4AnnouncementId").parent('div').find(".vptype").remove();
		
		$("#submenu4vidAnnouncementId").prop('disabled', false);
		$("#submenu4vidAnnouncementId").css({ 'background-color' : '#fff'}); 
	}else{
		$("#submenu4AnnouncementId").prop('disabled', 'disabled');
		$("#submenu4AnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu4AnnouncementId").val('');
		
		$("#submenu4vidAnnouncementId").prop('disabled', 'disabled');
		$("#submenu4vidAnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu4vidAnnouncementId").val('');
		//$("#submenu0AnnouncementId").parent('div').append('<input class="vptype" type="hidden" name="vp[announcementId]" value=""/>');
	}
});
$('input[data-announcementSelection=submenu5]').click(function() {
	
	var el = $(this);
	var ilId = el.attr('id');
	if(ilId == "submenu5GreetingP"){
		$("#submenu5AnnouncementId").prop('disabled', false);
		$("#submenu5AnnouncementId").css({ 'background-color' : '#fff'}); 
		
		$("#submenu5vidAnnouncementId").prop('disabled', false);
		$("#submenu5vidAnnouncementId").css({ 'background-color' : '#fff'});
		$("#submenu5AnnouncementId").parent('div').find(".vptype").remove();
	}else{
		$("#submenu5AnnouncementId").prop('disabled', 'disabled');
		$("#submenu5AnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu5AnnouncementId").val('');
		
		$("#submenu5vidAnnouncementId").prop('disabled', 'disabled');
		$("#submenu5vidAnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu5vidAnnouncementId").val('');
		//$("#submenu0AnnouncementId").parent('div').append('<input class="vptype" type="hidden" name="vp[announcementId]" value=""/>');
	}
});
$('input[data-announcementSelection=submenu6]').click(function() {
	
	var el = $(this);
	var ilId = el.attr('id');
	if(ilId == "submenu6GreetingP"){
		$("#submenu6AnnouncementId").prop('disabled', false);
		$("#submenu6AnnouncementId").css({ 'background-color' : '#fff'});
		
		$("#submenu6vidAnnouncementId").prop('disabled', false);
		$("#submenu6vidAnnouncementId").css({ 'background-color' : '#fff'});
		
		$("#submenu6AnnouncementId").parent('div').find(".vptype").remove();
	}else{
		$("#submenu6AnnouncementId").prop('disabled', 'disabled');
		$("#submenu6AnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu6AnnouncementId").val('');
		
		$("#submenu6vidAnnouncementId").prop('disabled', 'disabled');
		$("#submenu6vidAnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu6vidAnnouncementId").val('');
		//$("#submenu0AnnouncementId").parent('div').append('<input class="vptype" type="hidden" name="vp[announcementId]" value=""/>');
	}
});
$('input[data-announcementSelection=submenu7]').click(function() {
	
	var el = $(this);
	var ilId = el.attr('id');
	if(ilId == "submenu7GreetingP"){
		$("#submenu7AnnouncementId").prop('disabled', false);
		$("#submenu7AnnouncementId").css({ 'background-color' : '#fff'});
		
		$("#submenu7vidAnnouncementId").prop('disabled', false);
		$("#submenu7vidAnnouncementId").css({ 'background-color' : '#fff'});
		$("#submenu7AnnouncementId").parent('div').find(".vptype").remove();
	}else{
		$("#submenu7AnnouncementId").prop('disabled', 'disabled');
		$("#submenu7AnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu7AnnouncementId").val('');
		
		$("#submenu7vidAnnouncementId").prop('disabled', 'disabled');
		$("#submenu7vidAnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#submenu7vidAnnouncementId").val('');
		//$("#submenu0AnnouncementId").parent('div').append('<input class="vptype" type="hidden" name="vp[announcementId]" value=""/>');
	}
});

$('input[name="newSubmenu[bhGreeting]"]').click(function() {
	var el = $(this);
	var ilId = el.attr('id');
	if(ilId == "subGreetingP"){
		$("#newSubmenuAnnouncementId").prop('disabled', false);
		$("#newSubmenuAnnouncementId").css({ 'background-color' : '#fff'});
		
		$("#newSubmenuVidAnnouncementId").prop('disabled', false);
		$("#newSubmenuVidAnnouncementId").css({ 'background-color' : '#fff'});
		
		$("#newSubmenuAnnouncementId").parent('div').find(".vptype").remove();
	}else{
		$("#newSubmenuAnnouncementId").prop('disabled', 'disabled');
		$("#newSubmenuAnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#newSubmenuAnnouncementId").val('');
		
		$("#newSubmenuVidAnnouncementId").prop('disabled', 'disabled');
		$("#newSubmenuVidAnnouncementId").css({ 'background-color' : '#e9e9e9'});
		$("#newSubmenuVidAnnouncementId").val('');
		//$("#submenu0AnnouncementId").parent('div').append('<input class="vptype" type="hidden" name="vp[announcementId]" value=""/>');
	}
});
	//$('input[data-announcementSelection=vp]').trigger('click');
	
	$("#eVSupport").on('change', function() {
		  if ($(this).is(':checked')) {
		    $(this).attr('value', 'true');
		  } else {
		    $(this).attr('value', 'false');
		  }
		  
		  $('#eVSupport').text($('#checkbox1').val());
	});

	$("#addService, #addAllService").click(function(){ 
		var BtnId = $(this).attr('id');
		if(BtnId == "addAllService"){
			$('#availableServices option').prop('selected', true);
		}
		addServicesToAssignList();
	});
	
	var addServicesToAssignList = function(){
		var html = "";
		var availableServices = $("#availableServices").val();	
		if(availableServices){
			for (var i = 0; i < availableServices.length; i++) {
				html += '<option value="'+availableServices[i]+'">' +availableServices[i]+ '</option>';
				
				$("#availableServices option[value='"+availableServices[i]+"']").remove();			
			}
		}
		$("#assignedServices").append(html);
	}

	$("#removeService, #removeAllService").click(function(){ 
		var BtnId = $(this).attr('id');
		if(BtnId == "removeAllService"){
			$('#assignedServices option').prop('selected', true);
		}
		removeServicesToAssignList();
	});
	
	var removeServicesToAssignList = function(){
		var html = "";
		var availableServices = $("#assignedServices").val();	
		if(availableServices){
			for (var i = 0; i < availableServices.length; i++) {
				html += '<option value="'+availableServices[i]+'">' +availableServices[i]+ '</option>';
				
				$("#assignedServices option[value='"+availableServices[i]+"']").remove();			
			}
		}
		$("#availableServices").append(html);
	}
	
	$('#aaModCancel').click(function(){
		getAAList(0);
		$("#AAChoice").val("");	
		$('#modifyAttendant').show();
		$('#addAutoAttendant').hide();
		$('#AAData').hide();
		$('#aaInfo').hide();
		$('#addNewSubmenu').hide();
		$('html, body').animate({scrollTop: '0px'}, 300);
    });
  
  var getAAList = function(autoAttendantId){
		var html = '<option value=""></option>';
		
		$.ajax({
			url: 'autoAttendant21/getAutoAttendantList.php',
			type: 'POST',
			success: function(data)
			{
				if(foundServerConErrorOnProcess(data, "")) {
					return false;
              	}
				$("#loading2").hide();
				var obj = jQuery.parseJSON(data);
				if(obj.length > 0){
					for (var i = 0; i < obj.length; i++) {
						html += '<option value="'+ obj[i].id +'">' + obj[i].name + '</option>';
					}
            	}
				$("#AAChoice").html(html);
				if(autoAttendantId != "0" ){
						$("#AAChoice").val(autoAttendantId);
						$("#AAChoice").trigger('change');
				}
				//$("#addAutoAttendant").show();
				//$("#loading2").hide();
			}			
		});
	};
	
	var playAnnouncement = function(fileName, audioId, downloadId){
		
		if(fileName != ""){
			var filePath = window.location.origin+'/broadsoft/announcementUpload/'+fileName;
			//console.log(filePath);
			$.get(filePath, function(data, textStatus) {
		        if (textStatus == "success") {
		        	var downloadPath = window.location.origin+'/broadsoft/download.php?file=' + fileName;
		            // execute a success code
					$("#"+audioId).attr("src", filePath);
					$("#"+audioId).show();
					
					$("#"+downloadId).attr("href", downloadPath);
					$("#"+downloadId).show();

		        }
		    });
			
		}
	}
	
	$(".pGreetingSelect").change(function(){
			
		$(".audio").trigger('pause');
		//$(".audio").prop("currentTime",0);

		var el  = $(this);
		var selectId = el.attr('id');
		
		var audioId = $("#"+selectId).parent('div').siblings().find('.audio').attr('id');
		var downloadId = $("#"+selectId).parent('div').siblings().find('.download').attr('id');
		
		$("#"+audioId).hide();
		$("#"+downloadId).hide();
		
		if(el.val() == ""){
			return false;
		}
		var fileName = $('option:selected', this).attr('data-file');
		playAnnouncement(fileName, audioId, downloadId);
	});
	
	$("#phoneNumber").change(function(){
		var el = $(this);
		var phoneVal = el.val();
		
		if(phoneVal == ""){
			$("#aaPhoneActivateNumber").attr("disabled", true);
			$("#aaPhoneActivateNumber").val("");
			$("#divActivateNumber").hide("");
			$("#extension").animate({width: "400px"}, 800 );
		}else{
			$("#divActivateNumber").show("");
			$("#aaPhoneActivateNumber").attr("disabled", false);
			$("#aaPhoneActivateNumber").val(phoneVal);
			$("#extension").animate({width: "235px"}, 800 );
		}
		
	});
	
	var checkPhoneValueForActivate = function(){
	var phoneVal = $("#phoneNumber").val();
	if(phoneVal == ""){
		$("#aaPhoneActivateNumber").attr("disabled", true);
		$("#aaPhoneActivateNumber").val("");
		$("#divActivateNumber").hide("");
	}
};
checkPhoneValueForActivate();
	
}); 

function extensionBuilder(num, extLen) {
	var numLen = num.length;
	var ind = numLen - extLen;
	var extension = num.substring(ind);
	//$("#extension").val(extension);
	//var extension = num.substring("5");
	$("#extension").val(extension);
}
if ($.fn.button.noConflict) {
	var bootstrapButton = $.fn.button.noConflict();
	$.fn.bootstrapBtn = bootstrapButton;
	}	