<?php
	require_once ("/var/www/html/Express/config.php");
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin ();
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/autoAttendant21/aaOperation.php");
	
	$possibleActions["menuData"] = array(
			"Transfer With Prompt",
			"Transfer Without Prompt",
			"Transfer To Operator",
			"Transfer To Submenu", //only for Standard AAs
			"Name Dialing",
			"Extension Dialing",
			"Transfer To Mailbox",
			"Play Announcement",
			"Repeat Menu",
			"Return to Previous Menu", //only for submenus
			"Exit"
	);
	echo json_encode($possibleActions);