<style>
.ui-tabs .ui-tabs-nav {
    margin: 0 !important;
    padding: none !important;
    margin-left: -2px !important;
}
select::-ms-expand {	display: none; }
select{
    -webkit-appearance: none;
    appearance: none;
}
</style>
<?php
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();

$aaId = $_POST ["AAId"];

// session array of all fieldd
$_SESSION ["aaNames"] = array (
		"aaName" => "Auto Attendant Name",
		"callingLineIdFirstName" => "Calling Line ID First Name",
                "callingLineIdLastName" => "Calling Line ID Last Name",
		"timeZone" => "Time Zone",
		"type" => "Auto Attendant Type",
		"businessHoursSchedule" => "Business Hours Schedule",
		"holidaySchedule" => "Holiday Schedule",
		"nameDialingEntries" => "Dial By Name Entries",
		"phoneNumber" => "Phone Number",
		"extension" => "Extension",
		"id" => "Submenu ID",
		"enableFirstMenuLevelExtensionDialing" => "Enable first-level extension dialing",
		"enableLevelExtensionDialing" => "Enable extension dialing at anytime",
		"department" => "Department",
		"extensionDialingScope" => "Scope of Extension Dialing",
		"nameDialingScope" => "Scope of Name Dialing",
		"eVSupport" => "Enable Video Support",
		"bhannouncementSelection" => "Business Hours Greetings",
		"ahannouncementSelection" => "After Business Hours Greetings",
		"hannouncementSelection" => "Holidays Greetings",
		"vpannouncementSelection" => "Voice Portal Custom Greetings",
		"bhAnnouncementId" => "Business Hours Audio Announcement",
		"bhvidAnnouncementId" => "Business Hours Video Announcement",
		"ahAnnouncementId" => "After Hours Audio Announcement",
		"ahvidAnnouncementId" => "After Hours Video Announcement",
		"hAnnouncementId" => "Holiday Audio Announcement",
		"hvidAnnouncementId" => "Holiday Video Announcement",
		"vpAnnouncementId" => "Voice Portal Custom Announcement",
		"redirectedCallsCOLPPrivacy" => "Connected Line Identification Privacy on Redirected Calls",
		"callBeingForwardedResponseCallType" => "Send Call Being Forwarded Response on Redirected Calls",
		"aaActivateNumber" => "Activate Number",
		"aaPhoneActivateNumber" => "Activate Phone Number",
		"extensionAdd" => "Extension", 
		"firstDigitTimeoutSeconds" => "Transfer to operator after" ,
		"vidAnnouncements" => "Video Announcement"
);
?>
<script type="text/javascript" src="autoAttendant21/js/autoAttendant.js"></script>
<script>
	function displayPh(dropdownValue, hours, value)
	{
		
		if (value == "*" || value == "#")
		{
			value = "\\" + value;
		}
		if (dropdownValue == "Transfer With Prompt" || dropdownValue == "Transfer Without Prompt" || dropdownValue == "Transfer To Operator")
		{
			$("#phone" + hours + value).css("display", "inline-block");
		}
		else
		{
			$("#phone" + hours + value).css("display", "none");
			$("#" + hours + "\\[keys\\]\\[" + value + "\\]\\[phoneNumber\\]").val("");
		}
		if (dropdownValue == "Transfer To Submenu")
		{
			$("#sub" + hours + value).css("display", "inline-block");
		}
		else
		{
			$("#sub" + hours + value).css("display", "none");
			$("#" + hours + "\\[keys\\]\\[" + value + "\\]\\[submenu\\]").val("");
		}
		if (dropdownValue == "Play Announcement"){
			$("#announcements" + hours + value).css("display", "inline-block");
		}else{
			$("#announcements" + hours + value).css("display", "none");
			$("#" + hours + "\\[keys\\]\\[" + value + "\\]\\[announcements\\]").val("");
		}
	}
</script>
<?php

//require_once ("/var/www/lib/broadsoft/adminPortal/Announcement/Announcement.php");
//$announcement = new Announcement ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getAutoAttendantInfo21.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getAvailableNumbers.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getAllSchedules.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getTimeZones.php");

require_once ("/var/www/lib/broadsoft/adminPortal/autoAttendant21/aaOperation.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
//print_r($_SESSION["aa"]);
$sp = $_SESSION['sp'];
$groupId = $_SESSION['groupId'];
$dns = new Dns();
$extensionResponse = $dns->getGroupExtensionConfigRequest($sp, $groupId);


require_once ("/var/www/lib/broadsoft/adminPortal/Announcement21/Announcement21.php");

$announcement21 = new Announcement21 ();

$announcement21->sp = $_SESSION["sp"];
$announcement21->groupId = $_SESSION["groupId"];
//$announcement21->announcementFileType = $_POST["searchCriteria"] = "Audio";
$announcement21->includeAnnouncementTable = "true";

$announcementList = $announcement21->getAnnouncementList();

$resultPlayAnn1="";
if($announcementList["Error"] == ""){
    $nm = 0;
    $vd = 0;
    foreach ($announcementList["Success"]["row"] as $key => $val){
        if($val["announcementMediaType"] == "WAV" || $val["announcementMediaType"] == "WMA"){
            $resultPlayAnn1[$nm]["announcement_name"] = $val["announcementName"].".".$val["announcementMediaType"];
            $nm++;
        }else{
            $resultPlayAnn1Vid[$vd]["announcement_name"] = $val["announcementName"].".".$val["announcementMediaType"];
            $vd++;
        }
    }
}
//print_r($resultPlayAnn1);exit;

$keys = array (
		"0",
		"1",
		"2",
		"3",
		"4",
		"5",
		"6",
		"7",
		"8",
		"9",
		"*",
		"#" 
);
$menuTypes = array (
		"bh" => "Business Hours Menu",
		"ah" => "After Hours Menu" 
);
if ($_SESSION ["aa"] ["type"] == "Standard") {
	$menuTypes ["h"] = "Holiday Menu";
	for($i = 0; $i < count ( preg_grep ( "/^submenu(\d)+$/", array_keys ( $_SESSION ["aa"] ) ) ); $i ++) {
		$menuTypes ["submenu" . $i] = "Submenu - " . $_SESSION ["aa"] ["submenu" . $i] ["id"];
	}
}
$possibleActions = array (
		"Transfer With Prompt",
		"Transfer Without Prompt",
		"Transfer To Operator",
		"Transfer To Submenu", // only for Standard AAs
		"Name Dialing",
		"Extension Dialing",
		"Transfer To Mailbox",
		"Play Announcement",
		"Repeat Menu",
		"Return to Previous Menu", // only for submenus
		"Exit" 
);
function isDepartmentVisibleAA() {
	global $useDepartments, $departmentsFieldVisible;
	
	if ($useDepartments == "false") {
		return false;
	}
	
	return $departmentsFieldVisible == "true";
}
?>


<input type="hidden" name="modAAId" id="modAAId" value="<?php echo $aaId; ?>">
<form name="aaInfo" id="aaInfo" method="POST" class="aaInfoForm">
	<input type="hidden" name="aaId" id="aaId" value="<?php echo $aaId; ?>">

		<div id="aaTabs">
			<div class="divBeforeUl" style="">
    			<ul>
        			<li><a href="#aaSettings_1" class="tabs" id="basicInfo">Basic Info</a></li>
        			<li><a href="#businessHours_1" class="tabs" id="businessHours">Business
        					Hours Menu</a></li>
        			<li><a href="#afterHours_1" class="tabs" id="afterHours">After Hours
        					Menu</a></li>
        			<?php
        			if ($_SESSION ["aa"] ["type"] == "Standard") {
        				echo "<li><a href=\"#holiday_1\" class=\"tabs\" id=\"holiday\">Holiday Menu</a></li>";
        				
        				for($i = 0; $i < count ( preg_grep ( "/^submenu(\d)+$/", array_keys ( $_SESSION ["aa"] ) ) ); $i ++) {
        					echo "<li><a href=\"#submenu_" . $i . "\" class=\"tabs\" id=\"submenu" . $i . "\">Submenu - " . $_SESSION ["aa"] ["submenu" . $i] ["id"] . "</a></li>";
        				}
        			}
        			?>
        			<li><a href="#call_policy_1" class="tabs" id="call_policy">Call Policies</a></li>
			<li style="display: none"><a href="#services_1" class="tabs" id="services"
				style="display: none">Services</a></li>
			<li id="newSubMenu"><a href="#new_subMenu" class="tabs" id="newSubMenuATag">New Submenu</a></li>
    			</ul>
			</div>
			<div id="aaSettings_1" class="userDataClass" style="display: none;">
			<h2 class="subBannerCustom">Basic Info</h2>
				<div style="clear: right;">&nbsp;</div>
				<div class="row">
				
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText">Auto Attendant Name</label>
							<span class="required">*</span>
							<input type="text" name="aaName" id="aaName" size="35"
							maxlength="30" value="<?php echo $_SESSION["aa"]["aaName"]; ?>">
						</div>
					</div>
					
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText">Auto Attendant Type</label>
						
							<div class="dropdown-wrap">   
						<select name="" id="aaType" disabled style="">
								<option selected value="<?php echo $_SESSION["aa"]["type"]; ?>"><?php echo $_SESSION["aa"]["type"]; ?></option>
							</select>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText" for="callingLineIdFirstName">Calling Line ID First Name:</label>
							<span class="required">*</span>
								<input type="text" name="callingLineIdFirstName"
								id="callingLineIdFirstName" size="35" maxlength="30"
								value="<?php echo $_SESSION["aa"]["callingLineIdFirstName"]; ?>">
						</div>
					</div>
					
					
                                    
					
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText" for="callingLineIdLastName">Calling
							Line ID Last Name:
						</label>
						<span class="required">*</span>
						<input type="text" name="callingLineIdLastName"
							id="callingLineIdLastName" size="35" maxlength="30"
							value="<?php echo $_SESSION["aa"]["callingLineIdLastName"]; ?>">
						</div>
					</div>
                                    
					
					
				
				</div>
				
				 
				<div class="row">
					<div class="col-md-6">
							<div class="form-group">
								<label class="labelText">Phone Number:</label>
								<div class="dropdown-wrap">   
								<select name="phoneNumber" onchange="extensionBuilder(this.value, <?php echo $extensionResponse["defaultExtensionLength"];?>)"
									id="phoneNumber"
									<?php if ($_SESSION ["aa"] ["phoneNumber"]) {echo "disabledx style='xbackground-color:#ddd'"; }?>>
									<option value="">None</option>
									<?php
										if ($_SESSION ["aa"] ["phoneNumber"]) {
											echo "<option value=\"" . $_SESSION ["aa"] ["phoneNumber"] . "\" SELECTED" . ">" . $_SESSION ["aa"] ["phoneNumber"] . "</option>";
										}
										if (isset ( $availableNumbers )) {
											foreach ( $availableNumbers as $value ) {
												echo "<option value=\"" . $value . "\">" . $value . "</option>";
											}
										}
									  ?>
								</select>
									</div>
							</div>
						</div>
				
				
				
					<div class="col-md-3">
						<div class="form-group">
							<label class="labelText">Extension:</label>
							<!--<input type="text" name="extension" id="extension"
								value="<?php // echo $_SESSION["aa"]["extension"]; ?>"
								<?php // if($_SESSION["aa"]["extension"]){echo "disabledex";}?>> -->
								
							<input type="text" name="extension" id="extension"
							size="<?php echo $_SESSION['groupExtensionLength']['max'];?>"
                                                        maxlength="<?php echo $_SESSION['groupExtensionLength']['max'];?>"
                                                        value="<?php echo $_SESSION["aa"]["extension"]; ?>"
							<?php if($_SESSION["aa"]["extension"]){echo "disabledx";}?> style="width:235px !important">	
						</div>
					</div>
					<div class="col-md-3" id="divActivateNumber">
						<div class="form-group" style="float:right">
							<div class="form-group"></div>
							<input type="checkbox" name="aaActivateNumber" id="aaActivateNumber" value="Yes" style="width: auto"
							<?php if($_SESSION["aa"]["aaActivateNumber"] == "Yes"){echo "checked";}?> />
							<label for="aaActivateNumber"><span></span></label> 
							<label class="labelText" for="aaActivateNumber">Activate Number:</label>
							<input type="hidden" name="aaPhoneActivateNumber"
							id="aaPhoneActivateNumber"
							value="<?php echo $_SESSION["aa"]["phoneNumber"]; ?>" />
 							<label for="aaPhoneActivateNumber"><span></span></label>  
						</div>
						
					</div>
				</div>
				 
				<div class="row">
					<div class="col-md-6" style="display: <?php echo (isDepartmentVisibleAA() ? " block" : " none"); ?> ">
						<div class="form-group">
							<label class="labelText" for="department">Department:</label>
							<div class="dropdown-wrap">   
							<select name="department" id="department">
    							<option value="">None</option>
    							<?php
            						$obj = new AaOperation ();
            						$deptList = $obj->getDepartmentList ( $_SESSION ['sp'], $_SESSION ['groupId'] );
            						if (isset ( $deptList ["Success"] ["department"] )) {
            							foreach ( $deptList ["Success"] ["department"] as $key => $value ) {
            								if ($value ["name"] == $_SESSION ["aa"] ["department"]) {
            									$selected = "SELECTED";
            								} else {
            									$selected = "";
            								}
            								echo "<option value='" . $value ["name"] . "' " . $selected . ">" . $value ["fpName"] . "</option>";
            							}
            						}
        						?>
							</select>
								</div>
						</div>
					</div>
					
					
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText" for="timeZone">Time Zone:</label>
							<span class="required">*</span>
							<div class="dropdown-wrap">   
							<select name="timeZone" id="timeZone">
								<option value=""></option>
    							<?php
    							foreach ( $timeZones as $key => $value ) {
    								if ($value == $_SESSION ["aa"] ["timeZone"]) {
    									$sel = "SELECTED";
    								} else {
    									$sel = "";
    								}
    								echo "<option value=\"" . $value . "\"" . $sel . ">" . $value . "</option>";
    							}
    							?>
							</select>
							</div>
						</div>
					</div>
					
				</div>

				
				
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText" for="">Business Hours
							Schedule:</label>
							<div class="dropdown-wrap">   
							<select name="businessHoursSchedule" id="businessHoursSchedule">
								<option value=""></option>
    							<?php
    							/*
                                                        if (isset ( $schedules )) {
    								for($a = 0; $a < count ( $schedules ); $a ++) {
    									if ($schedules [$a] ["type"] == "Time") {
    										if ($schedules [$a] ["name"] == $_SESSION ["aa"] ["businessHoursSchedule"]) {
    											$sel = "SELECTED";
    										} else {
    											$sel = "";
    										}
    										echo "<option value=\"" . $schedules [$a] ["name"] . "\"" . $sel . ">" . $schedules [$a] ["name"] . "</option>\n";
    									}
    								}
    							}
                                                         */
    							?>
                                                        <?php
							if (isset ( $schedules )) {
								for($a = 0; $a < count ( $schedules ); $a ++) {
									if ($schedules [$a] ["type"] == "Time") {
										if ($schedules [$a] ["name"] == $_SESSION ["aa"] ["businessHoursSchedule"]) {
											$sel = "SELECTED";
										} else {
											$sel = "";
										}
										
										if ($schedules [$a] ["level"] == "System") {
											$level = " (System)";
										}
										elseif ($schedules [$a] ["level"] == "Service Provider") {
											$level = " (Enterprise)";
										}
										elseif ($schedules [$a] ["level"] == "Group") {
											$level = " (Group)";
										}
										else { $level = " (User)";}
										
										echo "<option value=\"" . $schedules [$a] ["name"] ."**". $schedules [$a] ["level"]. "\"" . $sel . ">" . $schedules [$a] ["name"] . $level ."</option>\n";
									}
								}
							}
							?>
							</select>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText" for="">Holiday Schedule:</label>
							<div class="dropdown-wrap">   
							<select name="holidaySchedule" id="holidaySchedule">
								<option value=""></option>
    							<?php
    							/*if (isset ( $schedules )) {
    								for($a = 0; $a < count ( $schedules ); $a ++) {
    									if ($schedules [$a] ["type"] == "Holiday") {
    										if ($schedules [$a] ["name"] == $_SESSION ["aa"] ["holidaySchedule"]) {
    											$sel = "SELECTED";
    										} else {
    											$sel = "";
    										}
    										echo "<option value=\"" . $schedules [$a] ["name"] ."**". $schedules [$a] ["level"] . "\"" . $sel . ">" . $schedules [$a] ["name"] . "</option>\n";
    									}
    								}
    							}*/
    							?>
                                                        <?php
							if (isset ( $schedules )) {
								for($a = 0; $a < count ( $schedules ); $a ++) {
									if ($schedules [$a] ["type"] == "Holiday") {
										if ($schedules [$a] ["name"] == $_SESSION ["aa"] ["holidaySchedule"]) {
											$sel = "SELECTED";
										} else {
											$sel = "";
										}
										
										if ($schedules [$a] ["level"] == "System") {
											$level = " (System)";
										}
										elseif ($schedules [$a] ["level"] == "Service Provider") {
											$level = " (Enterprise)";
										}
										elseif ($schedules [$a] ["level"] == "Group") {
											$level = " (Group)";
										}
										else { $level = " (User)";}
										
										echo "<option value=\"" . $schedules [$a] ["name"]."**". $schedules [$a] ["level"]. "\"" . $sel . ">" . $schedules [$a] ["name"] . $level ."</option>\n";
									}
								}
							}
							?>
							</select>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<input type="checkbox"
								<?php if($_SESSION["aa"]["eVSupport"]== 'true'){ ?> checked	<?php } ?>
								value="<?php echo $_SESSION["aa"]["eVSupport"]; ?>"
								name="eVSupport" id="eVSupport" ><label for="eVSupport"><span></span></label>
								<label class="labelText" for="eVSupport">Enable Video Support:</label>
						</div>
					</div>
				</div>

				

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText">Scope of Extension Dialing:</label>
							<br/>
							<?php
							if ($_SESSION ["enterprise"] == "true") {
							    if($useDepartments == "true"){
							        $eds = array (
							            "Enterprise",
							            "Group",
							            "Department"
							        );
							    }else{
							        $eds = array (
							            "Enterprise",
							            "Group"
							        );
							    }
								
							} else {
							    if($useDepartments == "true"){
							        $eds = array (
							            "Group",
							            "Department"
							        );
							    }else{
							        $eds = array (
							            "Group"
							        );
							    }
								
							}
							
							foreach ( $eds as $value ) {
								if ($value == $_SESSION ["aa"] ["extensionDialingScope"]) {
									$sel = "checked";
								} else {
									$sel = "";
								}
								if ($value == "Department"){
								    $style = "margin-left:25px;";
								}else{
								    $style= "";
								}
								
								echo "<input name='extensionDialingScope' id=\"".$value."\" type = 'radio' value=\"" . $value . "\"" . $sel . "/>";
								echo "<label for=\"".$value."\" style='".$style."'><span></span></label>";
								echo "<label class=\"labelText\">".$value."</label>";
							}
							?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText" for="nameDialingScope">Scope of Name
							Dialing:</label><br/>
						  
							<?php
							if ($_SESSION ["enterprise"] == "true") {
							    if($useDepartments == "true"){
							        $nds = array (
							            "Enterprise",
							            "Group",
							            "Department"
							        );
							    }else{
							        $nds = array (
							            "Enterprise",
							            "Group"
							        );
							    }
								
							} else {
							    if($useDepartments == "true"){
							        $nds = array (
							            "Group",
							            "Department"
							        );
							    }else{
							        $nds = array (
							            "Group"
							        );
							    }
								
							}
							
							foreach ( $nds as $value ) {
								if ($value == $_SESSION ["aa"] ["nameDialingScope"]) {
									$sel = "checked";
								} else {
									$sel = "";
								}
								
								if ($value == "Department"){
								    $style = "margin-left:25px;";
								}else{
								    $style= "";
								}
								
								echo "<input id=\"".$value."1\" name='nameDialingScope' style='width:20px' type='radio' value=\"" . $value . "\"" . $sel . "/>";
								echo "<label for=\"".$value."1\" style='".$style."'><span></span></label>";
								echo "<label class=\"labelText\">".$value."</label>";
							}
							?>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
    					<div class="form-group">
    						<label class="labelText" for="nameDialingEntries">Dial By Name
    							Entries:</label><br/>
    							<?php
        							$dbns = array (
        									"LastName + FirstName",
        									"LastName + FirstName or FirstName + LastName" 
        							);
        							foreach ( $dbns as $value ) {
        								if ($value == $_SESSION ["aa"] ["nameDialingEntries"]) {
        									$sel = "checked";
        								} else {
        									$sel = "";
        								}
        								
        								if ($value == "LastName + FirstName or FirstName + LastName"){
        								    $style = "margin-left:25px;";
        								}else{
        								    $style= "";
        								}
        								
        								echo "";
        								echo "<input id=\"".$value."\" name='nameDialingEntries' type='radio' value=\"" . $value . "\"" . $sel . "/>" ;
        								echo "<label for=\"".$value."\" style='".$style."'><span></span></label>";
        								echo "<label class=\"labelText\">".$value."</label>";
        								
        							}
        						?>
    					</div>
					</div>
				</div>
				<?php if($ociVersion == "21" || $ociVersion == "22"){?>
				<div class="row">
    				<div class="col-md-6">
        				<div class="form-group">
            				<label class="labelText" for="">Transfer to the operator after: (seconds of inactivity)</label>
            				
            				<input type="text" name="firstDigitTimeoutSeconds" id="firstDigitTimeoutSeconds" value="<?php echo $_SESSION["aa"]["firstDigitTimeoutSeconds"];?>" min="1" />
            				
        				</div>
    				</div>
				</div>
				<?php }?>
				
<!--New code found Start-->
					<?php if($ociVersion == "21"){?>
						<!-- <div class="row formspace">
							<div class="col span_8">
								<label class="labelpadding" for="nameDialingEntries">Transfer to the operator after:</label>
							</div>
							<div class="col span_15" style="padding: 7px 0;">
									<input type="text" name="firstDigitTimeoutSeconds" id="firstDigitTimeoutSeconds" value="<?php echo $_SESSION["aa"]["firstDigitTimeoutSeconds"];?>" style="width:7%;" min="1" /> seconds of inactivity
							</div>
						</div> -->
					<?php }?>
<!--New code found End-->

				<div class="row">
					<div class="col-md-6">
    					<div class="form-group">
							<label class="labelText">Voice Portal Custom Announcement:</label>
							<div class="dropdown-wrap">   
							 <select class="pGreetingSelect" name="vp[announcementId]" id="vpAnnouncementId"></select>
								</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
    						<a href="javascript:void(0)" style="display: none;"
    							id="vpdownload" class="download" style="display: block"><img
    							src="images/downloadAudio.png" />
    						</a>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
    						<audio controls style="display: none; width: 196px; height: 32px;"
    							id="vpaudio" class="audio" controlsList="nodownload">
    						</audio>
    					</div>
					</div>
				</div>
			</div>
		    <!--end aaSettings_1 div-->
			
<!-- new code found submenu Start-->
				<div id="new_subMenu" class="userDataClass" style="display: none;padding:0;">
					
					<h2 class="subBannerCustom">Add New Submenu</h2>
				 	
				 	<input type="hidden" name="checkNewSubMenuStatus" value="false" id="checkNewSubMenuStatus">
					<div class="row">
    					<div class="col-md-6">
    						<div class="form-group" id="element">
    							<label class="labelText">Submenu Greeting:</label><br />
    							<input type="radio" value = "Default Greeting" name="newSubmenu[bhGreeting]" id="subGreeting" style="margin:10px 0px 11px 20px;width:20px;" checked/> 
    							<label for="subGreeting"><span></span></label>
    							<label class="labelText">Default Greeting</label>
    							<input type="radio" value = "Personal Greeting" name="newSubmenu[bhGreeting]" id="subGreetingP" style="margin:10px 0px 11px 20px;width:20px;"/>
    							<label for ="subGreetingP" style="margin-left:25px;"><span></span></label>
    							<label class="labelText">Personal Greeting</label>
    						</div>
    					</div>
    					
    					<div class="col-md-6">
    						<div class="form-group">
    						<div class="form-group"></div>
    							<div class="dropdown-wrap">
    								<select class="pGreetingSelect" name="newSubmenu[announcementId]" id="newSubmenuAnnouncementId" style="width:95%;">
    								</select>
    							</div>
    							
    							<?php 
        						if($_SESSION["aa"]["eVSupport"] == "true"){ ?>
        						    <div class="dropdown-wrap" style="margin-top:10px;">
        							<select class="pGreetingSelect" name="newSubmenu[vidAnnouncementId]" id="newSubmenuVidAnnouncementId" style="width:95%;">
        							</select>
        						</div>
        						<?php } ?>
    						</div>
    					</div>
    					
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<span class="required">*</span>
								<label for ="newSubmenu[id]" class="labelText">Submenu ID:</label><br />
								<input type="text" name="newSubmenu[id]" id="newSubmenu[id]" size="40" maxlength="40" style="width:55%;margin-bottom:10px;">
								
								<input type="hidden" name="newSubmenu[enableLevelExtensionDialing]" value="false" /> 
                				<input style="width: 20px;" type="checkbox" name="newSubmenu[enableLevelExtensionDialing]"
                				id="newSubmenu[enableLevelExtensionDialing]" value="true">
                				<label for = "newSubmenu[enableLevelExtensionDialing]"><span></span> </label>
    							<label class="labelText" for ="newSubmenu[enableLevelExtensionDialing]">Enable extension dialing at anytime</label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
							<div class="form-group"></div>
    							
							</div>
						</div>
					</div>
					
					<!-- <div class="row formspace fcorn-register container">
        				<span class="required">*</span> <label>Submenu ID:</label>
        				<input type="text" name="newSubmenu[id]" id="newSubmenu[id]" size="40" maxlength="40"><div class="clr"></div>
        				
        				<label for="newSubmenu[enableLevelExtensionDialing]" style='padding-right:30px;'>Enable extension dialing at anytime</label>
						<input type="hidden" name="newSubmenu[enableLevelExtensionDialing]" value="false" /> 
        				<input style="width: 20px;" type="checkbox" name="newSubmenu[enableLevelExtensionDialing]"
        				id="newSubmenu[enableLevelExtensionDialing]" value="true">
    				</div> -->
					
					<!-- <div class="row formspace fcorn-register container" style="padding:0 !important; background:#c9c9c9 !important;">
						<div class="col span_2"><label class="labelpadding"><b>Key</b></label></div>
						<div class="col span_7"><label class="labelpadding"><b>Action</b></label></div>
						<div class="col span_7"><label class="labelpadding"><b>Description</b></label></div>
						<div class="col span_8"><label class="labelpadding"><b>Action Data</b></label></div>
					</div> -->
				<div class="viewDetail autoHeight viewDetailNew">	
				<table class="scroll tablesorter aATable" style="width: 100%; margin-bottom:40px;">
				<thead>
				<tr>
				<th style="width:25%;">Key</th>
				<th style="width:25%;">Action</th>
				<th style="width:25%;">Description</th>
				<th style="width:25%;">Action Data</th>
				</tr>
				</thead>
				<tbody>
				
				
				
				
				<?php 
				$dataArray = array("0"=>0, "1"=>1, "2"=>2, "3"=>3, "4"=>4, 
				    "5"=>5, "6"=>6, "7"=>7, "8"=>8, "9"=>9, "10"=>"*", "11"=>"#");
				    foreach($dataArray as $key2 => $value1) {
				 ?>
				 
				 	<tr>
						<td style="width:25%;"> <?php echo $value1; ?></td>
						<td style="width:25%;">
							<?php
							
							$newSubmenu_action = "<span class='dropdown-wrap dropdown-wrap-ui'><select style = 'width:95%;' name=\"" . "newSubmenu" . "[keys][" . $value1 . "][action]\" id=\"" . "newSubmenu" . "[keys][" . $value1 . "][action]\" onchange=\"displayPh(this.value, '" . "newSubmenu" . "', '" . $value1 . "')\">";
							$newSubmenu_action .= "<option value=\"\"></option>";
							$newSubmenuDescription = "<input style='width:95%;' type=\"text\" name=\"" . "newSubmenu" . "[keys][" . $value1 . "][desc]\" id=\"" . "newSubmenu" . "[keys][" . $value1 . "][desc]\" size=\"25\" maxlength=\"20\">";
							foreach ( $possibleActions as $vActions ) {
							    if (($_SESSION ["aa"] ["type"] == "Standard" ) or ($_SESSION ["aa"] ["type"] == "Standard" and $vActions !== "Return to Previous Menu") or ($_SESSION ["aa"] ["type"] == "Basic" and $vActions !== "Transfer To Submenu" and $vActions !== "Return to Previous Menu")) {
							        $sel = "";
							        $newSubmenu_action .= "<option value=\"" . $vActions . "\"" . $sel . ">" . $vActions . "</option>\n";
							    }
							}
							$newSubmenu_action .= "</select></span>";
							$newSubmenuPhoneNumber = "<input placeholder=\"Phone Number\" type=\"text\" name=\"" . "newSubmenu" . "[keys][" . $value1 . "][phoneNumber]\" id=\"" . "newSubmenu" . "[keys][" . $value1 . "][phoneNumber]\" size=\"30\" maxlength=\"30\">";
							$newSubmenu_announcement = createAnnouncement("newSubmenu", $value1, $announcementList, $_SESSION["aa"]["eVSupport"]);
							$newSubmenu_submenu = createSubmenu("newSubmenu", $value1);
							echo $newSubmenu_action;
							?>
							
						</td>
						<td style="width:25%;">
							<?php 
							
							echo $newSubmenuDescription;
							?>
						</td>
						
							<?php 
							
							echo "<td style='display:none; width:25%;' class=\"inputText col span_7\" id=\"phone" . "newSubmenu" . $value1 . "\">";
							echo $newSubmenuPhoneNumber;
							echo "</td>";
							
							echo "<td style='display:none; width:25%;' class=\"inputText\" id=\"sub" . "newSubmenu" . $value1 . "\"><span class='dropdown-wrap dropdown-wrap-ui'>";
							echo $newSubmenu_submenu;
							echo "</span></td>";
							
							echo "<td style='display:none; width:25%;' class=\"inputText col span_7\" id=\"announcements" . "newSubmenu" . $value1 . "\"><span class='dropdown-wrap dropdown-wrap-ui'>";
							echo $newSubmenu_announcement;
							echo "</span></td>";
							
							?>
						
					</tr>
				 <?php 
				    }
				?>
				</tbody>
				</table>
				</div>
				</div>
<!-- new code found submenu End-->
    	<?php
		//print_r($menuTypes);
		foreach ( $menuTypes as $key => $value ) {
			if ($key == "bh") {
				$divId = "businessHours_1";
				$label = "Business Hours";
			}
			if ($key == "ah") {
				$divId = "afterHours_1";
				$label = "After Hours";
			}
			if ($key == "h") {
				$divId = "holiday_1";
				$label = "Holiday Hours";
			}
			if (strpos ( $key, "submenu" ) === 0) {
				$divId = "submenu_" . substr ( $key, 7 );
				$label = "Submenu";
			}
			$subClass = "";
			if( strpos( $value, 'Submenu' ) !== false ){
			   // echo "rajesh".$value;
			   // $exploded = explode("***",$value);
			    $subClass .= $value;
			}
			?>
		<div id="<?php echo $divId; ?>" class="userDataClass" style="display: none;">
			<div class="Personal Greeting"><h2 class="subBannerCustom"> <?php echo $value; ?> </h2></div>
			<div class="row">
				<div class="col-md-6" id="element<?php echo $key;?>">
					<div class="form-group">
						<label class="labelText"><?php echo $label;?> Greeting:</label>
						<br/>
						<input type="radio" data-announcementSelection="<?php echo $key;?>" name="<?php echo $key; ?>[announcementSelection]" id="<?php echo $key;?>Greeting" value="Default" checked/><label for="<?php echo $key;?>Greeting"><span></span></label>
					    <label class="labelText">Default Greeting</label>
						<input type="radio" data-announcementSelection="<?php echo $key;?>" name="<?php echo $key; ?>[announcementSelection]" id="<?php echo $key;?>GreetingP"
						 value="Personal" class="<?php echo $subClass."1"?>"/><label for="<?php echo $key;?>GreetingP" style="margin-left:25px;"><span></span></label>
						 <label class="labelText">Personal Greeting</label>
					</div>	 
				</div>
				<div class="col-md-6">
					<div class="form-group">
					<!--  input class="bhtype" type="hidden" name="<?php //echo $key; ?>[announcementId]" value=""/-->
						<div class="form-group"></div>
						<div class="dropdown-wrap">   
    						<select class="pGreetingSelect <?php echo $subClass; ?>" name="<?php echo $key; ?>[announcementId]"id="<?php echo $key;?>AnnouncementId" style="width: 95%;"></select>
						</div>
						
						<?php 
        				if($_SESSION["aa"]["eVSupport"] == "true"){ ?>
        				    <div class="dropdown-wrap" style="margin-top:10px;">
        					<select class="pGreetingSelect <?php echo $subClass."Vid"; ?>"
        						name="<?php echo $key; ?>[vidAnnouncementId]"
        						id="<?php echo $key;?>vidAnnouncementId" style="width: 95%;"></select>
        				</div>
        				<?php } ?>
						
						</div>
				</div>
				
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<audio controls style="display: none; width: 196px; height: 32px;"
    						id="<?php echo $key; ?>audio" class="audio"
    						controlsList="nodownload">
    						<!-- source id="<?php echo $key; ?>audioOgg" src="" type="audio/ogg">
    						<source id="<?php echo $key; ?>audioMpeg" src="" type="audio/mpeg"-->
						</audio>
					</div>
				</div> 
				<div class="col-md-1">
					<div class="form-group">
						<a href="#" id="<?php echo $key;?>download" style="display: none"
						class="download" style="display: block"><img src="images/downloadAudio.png"/></a> 
					</div>
				</div>
				<div class="col-md-5">
					<div class="form-group">
				 	</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						
    					<?php
    			             if (strpos ( $key, "submenu" ) === 0) {
    				        ?>
    								<span class="required">*</span> <label class="labelText"
    							for="<?php echo $key; ?>[id]">Submenu ID</label>
    					 		<input type="text" style="margin-bottom:10px;" name="<?php echo $key; ?>[id]" id="<?php echo $key; ?>[id]" size="40" maxlength="40" value="<?php echo $_SESSION["aa"][$key]["id"]; ?>">
    								
    				                
    			                <?php } 
    			          ?>
    			          
    			          <input type="hidden" name="<?php echo $key; ?>[enableLevelExtensionDialing]" value="false" /> 
						<input type="checkbox" name="<?php echo $key; ?>[enableLevelExtensionDialing]" id="<?php echo $key; ?>[enableLevelExtensionDialing]" value="true"
					       <?php echo $_SESSION["aa"][$key]["enableLevelExtensionDialing"] == "true" ? " checked" : ""; ?>>
						<label for="<?php echo $key; ?>[enableLevelExtensionDialing]"><span></span></label>
						
						<?php
    			             if (strpos ( $key, "submenu" ) === 0) {
    				        ?>
    								
    								<?php
    				                echo "<label for=\"" . $key . "[enableLevelExtensionDialing]\" class='labelText'>Enable extension dialing at anytime</label>";
    			                 } else {
    				                    echo "<label for=\"" . $key . "[enableLevelExtensionDialing]\" class='labelText'>Enable first-level extension dialing</label>";
    			                }
    			          ?>
    			          
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
					</div>
				</div>
			</div>

			<div class="viewDetail autoHeight viewDetailNew">
				<table class="scroll tablesorter aATable" style="width: 100%; margin-bottom:40px;">
            		<thead>
            		<th style="width:25%;">Key</th>
            		<th style="width:25%;">Action<span class="required">*</span></th>
            		<th style="width:25%;">Description</th>
            		<th style="width:25%;">Action Data</th>
            	  	</thead>
						<?php

						echo"<tbody>";
                			foreach ( $keys as $v ) {
                			    echo "<tr>";
                				echo "<td style='width:25%;'>" . $v . "</td>";
                				$desc = (isset ( $_SESSION ["aa"] [$key] ["keys"] [$v] ["desc"] ) ? $_SESSION ["aa"] [$key] ["keys"] [$v] ["desc"] : "");
                				$act = (isset ( $_SESSION ["aa"] [$key] ["keys"] [$v] ["action"] ) ? $_SESSION ["aa"] [$key] ["keys"] [$v] ["action"] : "");
                				$ph = (isset ( $_SESSION ["aa"] [$key] ["keys"] [$v] ["phoneNumber"] ) ? $_SESSION ["aa"] [$key] ["keys"] [$v] ["phoneNumber"] : "");
                				$sub = (isset ( $_SESSION ["aa"] [$key] ["keys"] [$v] ["submenuId"] ) ? $_SESSION ["aa"] [$key] ["keys"] [$v] ["submenuId"] : "");
                				$announce = (isset ( $_SESSION ["aa"] [$key] ["keys"] [$v] ["announcements"] ) ? $_SESSION ["aa"] [$key] ["keys"] [$v] ["announcements"] : "");
                				$announceVid = (isset ( $_SESSION ["aa"] [$key] ["keys"] [$v] ["vidAnnouncements"] ) ? $_SESSION ["aa"] [$key] ["keys"] [$v] ["vidAnnouncements"] : "");
                				
                				$description = "<input style='' type=\"text\" name=\"" . $key . "[keys][" . $v . "][desc]\" id=\"" . $key . "[keys][" . $v . "][desc]\" size=\"25\" maxlength=\"20\" value=\"" . $desc . "\">";
                				$action = "<select name=\"" . $key . "[keys][" . $v . "][action]\" id=\"" . $key . "[keys][" . $v . "][action]\" onchange=\"displayPh(this.value, '" . $key . "', '" . $v . "')\">";
                				$action .= "<option value=\"\"></option>";
                				foreach ( $possibleActions as $vActions ) {
                					if (($_SESSION ["aa"] ["type"] == "Standard" and strpos ( $key, "submenu" ) === 0) or ($_SESSION ["aa"] ["type"] == "Standard" and strpos ( $key, "submenu" ) !== 0 and $vActions !== "Return to Previous Menu") or ($_SESSION ["aa"] ["type"] == "Basic" and $vActions !== "Transfer To Submenu" and $vActions !== "Return to Previous Menu")) {
                						if ($act == $vActions) {
                							$sel = "SELECTED";
                						} else {
                							$sel = "";
                						}
                						$action .= "<option value=\"" . $vActions . "\"" . $sel . ">" . $vActions . "</option>\n";
                					}
                				}
                				$action .= "</select>";
                				$phoneNumber = "<label class='labelText' style='display:none;' for=\"" . $key . "[keys][" . $v . "][phoneNumber]\"></label> ";
                				$phoneNumber .= "<input placeholder=\"Phone Number\" type=\"text\" name=\"" . $key . "[keys][" . $v . "][phoneNumber]\" id=\"" . $key . "[keys][" . $v . "][phoneNumber]\" size=\"30\" maxlength=\"30\" value=\"" . $ph . "\">";
                				$announcements = "<label style='display:none;' for=\"" . $key . "[keys][" . $v . "][announcements]\"></label> ";
								$announcements .= "<select name=\"" . $key . "[keys][" . $v . "][announcements]\" id=\"" . $key . "[keys][" . $v . "][announcements]\" class=\"aGreetingSelect\">";
								
								$announcements .= "<option value=''>-Select Audio Announcements-</option>";
								if(count($resultPlayAnn1) > 0){
								    foreach($resultPlayAnn1 as $keyA=>$valA){
								        //$announcements .= "<option>1212121</option>";
								        $announcements .= "<option value='".$valA["announcement_name"]."'";
								        if($announce == $valA["announcement_name"]){ $announcements .= "selected";}
								        $announcements .= ">".$valA["announcement_name"]."</option>";
								    }
								}
								
								
								$announcements .= "</select>";
								
								if($_SESSION["aa"]["eVSupport"] == "true"){
								    $announcements .= "<div class='dropdown-wrap' style='margin-top:5px;'><select name=\"" . $key . "[keys][" . $v . "][vidAnnouncements]\" id=\"" . $key . "[keys][" . $v . "][vidAnnouncements]\" class=\"aGreetingSelect\">";
								    
								    $announcements .= "<option value=''>-Select Video Announcements-</option>";
								    if(count($resultPlayAnn1Vid) > 0){
								        foreach($resultPlayAnn1Vid as $keyA=>$valA){
								            //$announcements .= "<option>1212121</option>";
								            $announcements .= "<option value='".$valA["announcement_name"]."'";
								            if($announceVid == $valA["announcement_name"]){ $announcements .= "selected";}
								            $announcements .= ">".$valA["announcement_name"]."</option>";
								        }
								    }
								    
								    
								    $announcements .= "</select></div>";
								}
								//$announcements .= "<div class=\"clr\"></div>";
								//$submenu = "<span class=\"required\"></span>";
								$submenu = "";
                				$submenu .= "<select name=\"" . $key . "[keys][" . $v . "][submenu]\" id=\"" . $key . "[keys][" . $v . "][submenu]\">";
                				$submenu .= "<option value=\"\">-Select-</option>";
                				for($i = 0; $i < count ( preg_grep ( "/^submenu(\d)+$/", array_keys ( $_SESSION ["aa"] ) ) ); $i ++) {
                					if ((strpos ( $key, "submenu" ) === 0 and $i != substr ( $key, 7 )) or strpos ( $key, "submenu" ) !== 0) {
                						if ($sub == $_SESSION ["aa"] ["submenu" . $i] ["id"]) {
                							$sel = "SELECTED";
                						} else {
                							$sel = "";
                						}
                						$submenu .= "<option value=\"" . $_SESSION ["aa"] ["submenu" . $i] ["id"] . "\"" . $sel . ">" . $_SESSION ["aa"] ["submenu" . $i] ["id"] . "</option>\n";
                					}
                				}
                				$submenu .= "</select>";
                				
                				echo "<td style='width:25%;'><span class='dropdown-wrap dropdown-wrap-ui'>" . $action . "</span></td>";
                				echo "<td style='width:25%;'>" . $description . "</td>";
                				echo "<td id=\"phone" . $key . $v . "\"";
                				if ($act == "Transfer With Prompt" or $act == "Transfer Without Prompt" or $act == "Transfer To Operator") {
                					echo " style=\"display:inline-block;width:25%;\"";
                				} else {
                					echo " style=\"display:none;width:25%;\"";
                				}
                				echo ">";
                				echo $phoneNumber;
                				echo "</td>";
                				echo "<td id=\"sub" . $key . $v . "\"";
                				if ($act == "Transfer To Submenu") {
                					echo " style=\"display:inline-block;width:25%;\"";
                				} else {
                					echo " style=\"display:none;width:25%;\"";
                				}
                				echo "><span class='dropdown-wrap dropdown-wrap-ui'>";
                				echo $submenu;
                				echo "</span></td>";
								echo "<td id=\"announcements" . $key . $v . "\"";
								if ($act == "Play Announcement") {
									echo " style=\"display:inline-block;width:25%;\"";
								} else {
									echo " style=\"display:none;width:25%;\"";
								}
								echo "><span class='dropdown-wrap dropdown-wrap-ui'>";
								echo $announcements;
								echo "</span></td>";
								// echo "<div style=\"clear:right;\">&nbsp;</div>";
                			    echo"</tr>";
                				// echo "<script>displayPh($(\"#ah\\\\[keys\\\\]\\\\[" . $v . "\\\\]\\\\[action\\\\] option:selected\").value, \"ah\", \"" . $v . "\");</script>";
                			
                			}
            			echo "</tbody>";
            			?>
				</table>
			</div>
		<?php
			if (strpos ( $key, "submenu" ) === 0) {
		?>
				<!-- <input type="button" name="" id="" class="deleteSubMenu" value="Delete" data-subName = "<?php //echo $value; ?>"> -->
		<?php 
			}
		?>
		</div>
		<!--end <?php echo $divId; ?> div-->
			
		
			
				<?php
		}
		?>
		
		<!-- Jeetesh -->
		<div id="services_1" class="userDataClass" style="display: none;">
			<div class="subBanner" style="margin-bottom: 25px;">Services</div>
			<div style="clear: right"></div>
			<div class="row formspace fcorn-register container">
				<div class="col span_6">
					<p style="text-align: center">Available Services</p>
					<br />
					<div style="width: 100%;">
					<div class="dropdown-wrap">   
						<select style="height: 250px;" name="availableServices[]"
							id="availableServices" multiple="">

						</select>
						</div>
					</div>
				</div>
				<div class="col span_4" style="text-align: center; padding: 70px 0;">
					<div style="max-width: 120px; margin: 0 auto; text-align: center;">
						<input id="addService" type="button"
							style="width: 110px; font-size: 12px; margin: 5px 0;"
							value="Add >"> <input id="removeService" type="button"
							style="width: 110px; font-size: 12px; margin: 5px 0;"
							value="< Remove"> <input id="addAllService" type="button"
							style="width: 110px; font-size: 12px; margin: 5px 0;"
							value="AddAll >>"> <input id="removeAllService" type="button"
							style="width: 110px; font-size: 12px; margin: 5px 0;"
							value="<< Remove All">
					</div>

				</div>
				<div class="col span_6">
					<p style="text-align: center">User Services</p>
					<br />
					<div style="width: 100%;">
					<div class="dropdown-wrap">   
						<select style="height: 250px;" name="assignedServices[]"
							id="assignedServices" multiple="">

						</select>
	</div>
					</div>
				</div>
			</div>
		</div>
<!-- call Policy start -->
		<div id="call_policy_1">
			<h2 class="subBannerCustom">Call Policies</h2>
				<div class="row">
					<div class="col-md-6">
    					<div class="form-group">
    						<label class="labelText" for="iprCalls">Connected Line Identification Privacy on Redirected Calls:</label>
    					</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
    						<input type="radio" name="redirectedCallsCOLPPrivacy" id="noPrivacyRadio" checked value="No Privacy" /><label for="noPrivacyRadio"><span></span></label>
    						<label class="labelText">No Privacy</label><br>
							<input type="radio" name="redirectedCallsCOLPPrivacy" id="privacyForECRadio" value="Privacy For External Calls" /><label for="privacyForECRadio"><span></span></label>
    						<label class="labelText">Privacy For External Calls</label><br>

							<input type="radio" name="redirectedCallsCOLPPrivacy" id="privacyForACRadio" value="Privacy For All Calls" /><label for="privacyForACRadio"><span></span></label>
    						<label class="labelText">Privacy For All Calls</label>
						</div>
					</div>
				</div>
				<div class="row">
    				<div class="col-md-6">
    					<div class="form-group">
    					<label class="labelText" for="frrCalls">Send Call Being Forwarded Response on Redirected Calls:</label>
    					</div>
    				</div>
    				<div class="col-md-6">
    					<div class="form-group">
    						<input type="radio" name="callBeingForwardedResponseCallType" id="neverRadio" value="Never" checked /><label for="neverRadio"><span></span></label>
    					<label class="labelText">Never</label><br>
    
    					<input type="radio" name="callBeingForwardedResponseCallType" id="internalCallsRadio" value="Internal Calls" /><label for="internalCallsRadio"><span></span></label>
    					<label class="labelText">Internal Calls</label><br>
    
    					<input type="radio" name="callBeingForwardedResponseCallType" id="allCallsRadio" value="All Calls" /><label for="allCallsRadio"><span></span></label>
    					<label class="labelText">All Calls</label>
    					</div>
    				</div>
				</div>			
			</div>
		</div>
		<div class="row">
			<div class="form-group alignBtn autoAttendantButton">
				<input id="subButtonDelAA" value="Delete Auto Attendant" type="button" class="deleteButton marginRightButton">
				<span class="deleteThisSubmenu marginRightButton"><input class='deleteThisSubmenuButton deleteButton' id='deleteThisSubmenuButton' type='button' value = 'Delete This Submenu'></span>
		
				<input type="button" id="aaModCancel" value="Cancel" class="cancelButton marginRightButton">
			
				<input type="button" id="subButtonAA" value="Confirm Settings" onClick="window.location='#top'" class="subButton marginRightButton" style="">
    			
    		</div>
		</div>
	</form>

<div id="dialogAA" class="dialogClass"></div>
<div id="dialogSubmenu" class="dialogClass"></div>


<?php 
function createAnnouncement($subMenu, $v, $announcementList, $evSupport) {
    $resultPlayAnn="";
    if($announcementList["Error"] == ""){
        $r = 0;
        $n = 0;
        foreach ($announcementList["Success"]["row"] as $key => $val){
            if($val["announcementMediaType"] == "WMA" || $val["announcementMediaType"] == "WAV"){
                $resultPlayAnn[$r]["announcement_name"] = $val["announcementName"].".".$val["announcementMediaType"];
                $r++;
            }else{
                $resultPlayAnnVid[$n]["announcement_name"] = $val["announcementName"].".".$val["announcementMediaType"];
                $n++;
            }
            
        }
    }
    
    $announcements = "<label style='display:none;' for=\"" . $subMenu . "[keys][" . $v . "][announcements]\"></label> ";
    $announcements .= "<select name=\"" . $subMenu . "[keys][" . $v . "][announcements]\" id=\"" . $subMenu . "[keys][" . $v . "][announcements]\" class=\"aGreetingSelect\">";
    
    $announcements .= "<option value=''>-Select Audio Announcements-</option>";
    if(count($resultPlayAnn) > 0){
        foreach($resultPlayAnn as $keyA=>$valA){
            $announcements .= "<option value='".$valA["announcement_name"]."'";
            $announcements .= ">".$valA["announcement_name"]."</option>";
        }
    }
    
    $announcements .= "</select>";
    if($evSupport == "true"){
        $announcements .= "<select style='margin-top:5px;' name=\"" . $subMenu . "[keys][" . $v . "][vidAnnouncements]\" id=\"" . $subMenu . "[keys][" . $v . "][vidAnnouncements]\" class=\"aGreetingSelect\">";
        
        $announcements .= "<option value=''>-Select Video Announcements-</option>";
        if(count($resultPlayAnnVid) > 0){
            foreach($resultPlayAnnVid as $keyVid=>$valVid){
                $announcements .= "<option value='".$valVid["announcement_name"]."'";
                $announcements .= ">".$valVid["announcement_name"]."</option>";
            }
        }
        
        $announcements .= "</select>";
    }
    
    //$announcements .= "<div class=\"clr\"></div>";
    return $announcements;
}

function createSubmenu($key, $v) {
    //$submenu = "<span class=\"required\"></span>";
    $submenu = "";
    $submenu .= "<select name=\"" . $key . "[keys][" . $v . "][submenu]\" id=\"" . $key . "[keys][" . $v . "][submenu]\">";
    $submenu .= "<option value=\"\">-Select-</option>";
    for($i = 0; $i < count ( preg_grep ( "/^submenu(\d)+$/", array_keys ( $_SESSION ["aa"] ) ) ); $i ++) {
        if ((strpos ( $key, "submenu" ) === 0 and $i != substr ( $key, 7 )) or strpos ( $key, "submenu" ) !== 0) {
            $sel = "";
            $submenu .= "<option value=\"" . $_SESSION ["aa"] ["submenu" . $i] ["id"] . "\"" . $sel . ">" . $_SESSION ["aa"] ["submenu" . $i] ["id"] . "</option>\n";
        }
    }
    $submenu .= "</select>";
    return $submenu;
}
?>