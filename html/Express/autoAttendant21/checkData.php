<?php
	require_once("/var/www/html/Express/config.php");
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once("/var/www/lib/broadsoft/adminPortal/autoAttendant21/aaOperation.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/Announcement/Announcement.php");
	
	//echo "<pre>"; print_r($_SESSION['aa']); print_r($_POST); die;
	$hidden = array("aaId", "aaPhoneActivateNumber","checkNewSubMenuStatus");
	$required = array("aaName", "callingLineIdLastName", "callingLineIdFirstName", "timeZone", "nameDialingEntries");
	
	if($ociVersion == "21" || $ociVersion == "22"){
		$checkForCharacters = array("aaName", "callingLineIdLastName", "callingLineIdFirstName", "firstDigitTimeoutSeconds");
	}else{
		$checkForCharacters = array("aaName", "callingLineIdLastName", "callingLineIdFirstName");
	}
	
	$data = $errorTableHeader;
	$changes = 0;
	$error = 0;
	if(!array_key_exists("eVSupport", $_POST)){
		$_POST['eVSupport'] = 'false';
	}
	if(!array_key_exists("assignedServices", $_POST)){
		$_POST["assignedServices"] = array();
	}
	if(!array_key_exists("aaActivateNumber", $_POST)){
		$_POST["aaActivateNumber"] = "No";
	}
	$k=0;
	foreach ($_POST as $key => $value)
	{
		if (!in_array($key, $hidden)) //hidden form fields that don't change don't need to be checked or displayed
		{
			$bg = UNCHANGED;
		
			if (!is_array($value))
			{
				if (trim($value) !== trim($_SESSION["aa"][$key]))
				{
					$changes = 1;
					$bg = CHANGED;

					if (in_array($key, $checkForCharacters))
					{
						if (strlen($value) > 0)
						{
							if (!preg_match("/^[ A-Za-z0-9-_]+$/", $value))
							{
								$error = 1;
								$bg = INVALID;
								$value = $_SESSION["aaNames"][$key] . " must be letters, numbers, and dashes only.";
							}
							
							if($key == "aaName"){
							    if(in_array($value, $_SESSION["usedAANames"])){
							        $error = 1;
							        $bg = INVALID;
							        $value = $_SESSION["aaNames"][$key] . " is already in use.";
							    }
							}
						}
						if($key == "firstDigitTimeoutSeconds"){
							if($value < 1 || $value > 60){
								$error = 1;
								$bg = INVALID;
								$value = $value." <br/>".$_SESSION["aaNames"][$key] . " is a required field. It's minimum value should be 1 and maximum value should be 60.";
							}
						}
						
					}
					//check for required fields
					if (in_array($key, $required) and $value == "")
					{
						$error = 1;
						$bg = INVALID;
						$value = $_SESSION["aaNames"][$key] . " is a required field.";
					}
					
				
				}
				if ($bg != UNCHANGED)
				{
					if($key !== "aaActivateNumber"){
						 
						if($key == "holidaySchedule" || $key == "businessHoursSchedule"){ //condition for business and holiday type schedule
							$holidayValueExplode = explode("**", $value);
							$value = $holidayValueExplode[0];
						}
												
						$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"][$key] . "</td><td class=\"errorTableRows\">";
						if ($value)
						{
							$data .= $value;
						}
						else
						{
							$data .= "None";
						}
						$data .= "</td></tr>";
					}else{
						if($key == "aaActivateNumber" && !empty($_POST['aaPhoneActivateNumber'])){
							if ($value !== $_SESSION["aa"][$key])
							{
								$changes = 1;
								$bg = CHANGED;
								if($value == "Yes"){
									$value = "Activated";
								}else{
									$value = "Deactivated";
								} 
								$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"]["aaActivateNumber"] . ": </td><td class=\"errorTableRows\">" . $value . "</td></tr>";
							}
						}
					}
				}
			}

			if ($key == "vp"){				
				if($value["announcementId"] == ""){ 
					if($_SESSION["aa"][$key]["announcementId"] !== ""){
						$changes = 1;
						$bg = INVALID;
						$error = 1;
						$vpAnnouncementLabel = $value["id"] = $_SESSION["aaNames"]["vpAnnouncementId"] . " is required.";
						$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"]["vpAnnouncementId"] . "</td><td class=\"errorTableRows\">Required</td></tr>";
					}
				}else{				
					if($value["announcementId"] <> $_SESSION["aa"][$key]["announcementId"]){
						$changes = 1;
						$bg = CHANGED;				
						$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"]["vpAnnouncementId"] . "</td><td class=\"errorTableRows\">".$value["announcementId"]."</td></tr>";
					}
				}			
			}
		
			if ($key == "bh" or $key == "ah" or $key == "h" or strpos($key, "submenu") === 0)
			{
				$menuData = "";
				$menuExtensionData = "";
				if (strpos($key, "submenu") === 0 and $value["id"] !== $_SESSION["aa"][$key]["id"])
				{
					$changes = 1;
					$bg = CHANGED;
					if ($value["id"] == "")
					{
						$bg = INVALID;
						$error = 1;
						$value["id"] = $_SESSION["aaNames"]["id"] . " is required.";
					}
					//$menuExtensionData .= "<tr style=\" color:#fff; background:" . $bg . "\"><td colspan=\"4\" class=\"errorTableRows\">" . $_SESSION["aaNames"]["id"] . ": " . $value["id"] . "</td></tr>";
                                        $menuExtensionData .= "<tr><td colspan=\"2\" style=\"background:" . $bg . ";width:47%\" class=\"errorTableRows\">" . $_SESSION["aaNames"]["id"]. "</td><td colspan=\"2\" class=\"errorTableRows\">".$value["id"]."</td></tr>";
				}
				
				if (strpos($key, "submenu") === 0 && $value["announcementSelection"] !== $_SESSION["aa"][$key]["announcementSelection"])
				{
				    $changes = 1;
				    $bg = CHANGED;
				    $annKey1 = $key. "Greetings";
				    if ($value["announcementSelection"] == "")
				    {
				        $bg = INVALID;
				        $error = 1;
				        $annKey1 = $key . " is required.";
				    }
				    $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $annKey1 . "</td><td class=\"errorTableRows\">".$value["announcementSelection"]."</td></tr>";
				}
				
				/*if($_SESSION["aa"]["eVSupport"] == "true"){
				    if ((isset($value["announcementId"]) && strpos($key, "submenu") === 0 && $value["announcementId"] !== $_SESSION["aa"][$key]["announcementId"] && $value["announcementSelection"] == "Personal") || isset($value["vidAnnouncementId"]) && strpos($key, "submenu") === 0 && $value["vidAnnouncementId"] !== $_SESSION["aa"][$key]["vidAnnouncementId"] && $value["announcementSelection"] == "Personal")
				    {
				        $changes = 1;
				        $bg = CHANGED;
				        $subGreetingVal = "Submenu ".$key." greeting";
				        if ($value["announcementId"] == "" && $value["vidAnnouncementId"]== "")
				        {
				            $bg = INVALID;
				            $error = 1;
				            $subAnnouncementLabel = $value["id"] = $subGreetingVal . " is required.";
				        }else{
				            $subAnnouncementLabel = $value["announcementId"]." ".$value["vidAnnouncementId"];
				        }
				        
				        $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $subGreetingVal . "</td><td class=\"errorTableRows\">".$subAnnouncementLabel."</td></tr>";
				    }
				    
				}else{
				    if (isset($value["announcementId"]) && strpos($key, "submenu") === 0 && $value["announcementId"] !== $_SESSION["aa"][$key]["announcementId"] && $value["announcementSelection"] == "Personal")
				    {
				        $changes = 1;
				        $bg = CHANGED;
				        $subGreetingVal = "Submenu ".$key." greeting";
				        if ($value["announcementId"] == "")
				        {
				            $bg = INVALID;
				            $error = 1;
				            $subAnnouncementLabel = $value["id"] = $subGreetingVal . " is required.";
				        }else{
				            $subAnnouncementLabel = $value["announcementId"];
				        }
				        
				        $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $subGreetingVal . "</td><td class=\"errorTableRows\">".$subAnnouncementLabel."</td></tr>";
				    }
				}*/
				
				if($_SESSION["aa"]["eVSupport"] == "true"){
				    if(strpos($key, "submenu") === 0 && $value["announcementSelection"] == "Personal"){
				        if($value["announcementId"] == "" && $value["vidAnnouncementId"] ==""){
				            $changes = 1;
				            $bg = INVALID;
				            $error = 1;
				            $subAnnouncementLabel = $value["id"] = "A WAV, WMA, MOV or 3GP file must be uploaded for the Personal Greeting.";
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Submenu Personal Greeting</td><td class=\"errorTableRows\">".$subAnnouncementLabel."</td></tr>";
				        }else if(($value["announcementId"] != $_SESSION["aa"][$key]["announcementId"]) || ($value["vidAnnouncementId"] != $_SESSION["aa"][$key]["vidAnnouncementId"])){
				            $changes = 1;
				            $bg = CHANGED;
				            $subAnnouncementLabel = $value["announcementId"]." ".$value["vidAnnouncementId"];
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Submenu Personal Greeting</td><td class=\"errorTableRows\">".$subAnnouncementLabel."</td></tr>";
				        }
				        
				        
				    }
				}else{
				    if(strpos($key, "submenu") === 0 && $value["announcementSelection"] == "Personal"){
				        if($value["announcementId"] == ""){
				            $changes = 1;
				            $bg = INVALID;
				            $error = 1;
				            $subAnnouncementLabel = $value["id"] = "A WAV or WMA file must be uploaded for the Personal Greeting.";
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td class=\"errorTableRows\">".$subAnnouncementLabel."</td></tr>";
				        }else if($value["announcementId"] != $_SESSION["aa"][$key]["announcementId"]){
				            $changes = 1;
				            $bg = CHANGED;
				            $subAnnouncementLabel = $value["announcementId"];
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td class=\"errorTableRows\">".$subAnnouncementLabel."</td></tr>";
				        }
				        
				        
				    }
				}
			
				
				/*if (strpos($key, "submenu") === 0 and $value["announcementSelection"] !== $_SESSION["aa"][$key]["announcementSelection"])
				{
			
				    $changes = 1;
				    $bg = CHANGED;
				    //$dat = "Default Greetings";
				    if (isset($value["announcementId"]) && $value["announcementId"] == "" && $value["announcementSelection"] == "Personal")
				    {
				        
				        $bg = INVALID;
				        $error = 1;
				        $subLabel = $value["announcementSelection"] = "Personal greeting is required.";
				    }
				    if($value["announcementId"] <> 0){
				        $where['announcement_id'] = $value["announcementId"];
				        $announcement = new Announcement();
				        $getAnnouncementInfo = $announcement->getAnnouncement($db, $where);
				        $value["announcementSelection"] = $getAnnouncementInfo[0]['announcement_name'];
				    }else{
				        $value["announcementSelection"] .= "";
				    }
				    
				    $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Submenu Greetings ".$key."</td><td class=\"errorTableRows\">".$value["announcementSelection"]."</td></tr>";
				}*/
				//comments for bh
				if ($key == "bh" && $value["announcementSelection"] !== $_SESSION["aa"][$key]["announcementSelection"])
				{
					$changes = 1;
					$bg = CHANGED;
					if ($value["announcementSelection"] == "")
					{
						$bg = INVALID;
						$error = 1;
						$value["id"] = $_SESSION["aaNames"]["announcementSelection"] . " is required.";
					}
					$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"]["bhannouncementSelection"] . "</td><td class=\"errorTableRows\">".$value["announcementSelection"]."</td></tr>";
				}
				if($_SESSION["aa"]["eVSupport"] == "true"){
				    if($key == "bh" && $value["announcementSelection"] == "Personal"){
				        if($value["announcementId"] == "" && $value["vidAnnouncementId"] ==""){
				            $changes = 1;
				            $bg = INVALID;
				            $error = 1;
				            $bhAnnouncementLabel = $value["id"] = "A WAV, WMA, MOV or 3GP file must be uploaded for the Personal Greeting.";
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td class=\"errorTableRows\">".$bhAnnouncementLabel."</td></tr>";
				        }else if(($value["announcementId"] != $_SESSION["aa"][$key]["announcementId"]) || ($value["vidAnnouncementId"] != $_SESSION["aa"][$key]["vidAnnouncementId"])){
				            $changes = 1;
				            $bg = CHANGED;
				            $bhAnnouncementLabel = $value["announcementId"]." ".$value["vidAnnouncementId"];
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td class=\"errorTableRows\">".$bhAnnouncementLabel."</td></tr>";
				        }
				        
				        
				    }
				    
				    /*if ((isset($value["announcementId"]) && $key == "bh" && $value["announcementId"] != $_SESSION["aa"][$key]["announcementId"] && $value["announcementSelection"] == "Personal") ||
				        (isset($value["vidAnnouncementId"]) && $key == "bh" && $value["vidAnnouncementId"] != $_SESSION["aa"][$key]["vidAnnouncementId"] && $value["announcementSelection"] == "Personal"))
				    {
				        $changes = 1;
				        $bg = CHANGED;
				        if ($value["announcementId"] == "" && $value["vidAnnouncementId"] == "")
				        {
				            $bg = INVALID;
				            $error = 1;
				            $bhAnnouncementLabel = $value["id"] = $_SESSION["aaNames"]["bhAnnouncementId"] . " is required.";
				        }else{
				            $bhAnnouncementLabel = $value["announcementId"]." ".$value["vidAnnouncementId"];
				        }
				        
				        $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"]["bhAnnouncementId"] . "</td><td class=\"errorTableRows\">".$bhAnnouncementLabel."</td></tr>";
				    }*/
				    
				}else{
				    /*if (isset($value["announcementId"]) && $key == "bh" && $value["announcementId"] !== $_SESSION["aa"][$key]["announcementId"] && $value["announcementSelection"] == "Personal")
				    {
				        $changes = 1;
				        $bg = CHANGED;
				        if ($value["announcementId"] == "")
				        {
				            $bg = INVALID;
				            $error = 1;
				            $bhAnnouncementLabel = $value["id"] = $_SESSION["aaNames"]["bhAnnouncementId"] . " is required.";
				        }else{
				            $bhAnnouncementLabel = $value["announcementId"];
				        }
				        
				        $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"]["bhAnnouncementId"] . "</td><td class=\"errorTableRows\">".$bhAnnouncementLabel."</td></tr>";
				    }*/
				    
				    if($key == "bh" && $value["announcementSelection"] == "Personal"){
				        if($value["announcementId"] == ""){
				            $changes = 1;
				            $bg = INVALID;
				            $error = 1;
				            $bhAnnouncementLabel = $value["id"] = "A WAV or WMA file must be uploaded for the Personal Greeting.";
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td class=\"errorTableRows\">".$bhAnnouncementLabel."</td></tr>";
				        }else if($value["announcementId"] != $_SESSION["aa"][$key]["announcementId"]){
				            $changes = 1;
				            $bg = CHANGED;
				            $bhAnnouncementLabel = $value["announcementId"];
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td class=\"errorTableRows\">".$bhAnnouncementLabel."</td></tr>";
				        }
				        
				        
				    }
				}
				if ($key == "ah" && $value["announcementSelection"] !== $_SESSION["aa"][$key]["announcementSelection"])
				{
					$changes = 1;
					$bg = CHANGED;
					if ($value["announcementSelection"] == "")
					{
						$bg = INVALID;
						$error = 1;
						$value["id"] = $_SESSION["aaNames"]["announcementSelection"] . " is required.";
					}
					$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"]["ahannouncementSelection"] . "</td><td class=\"errorTableRows\">".$value["announcementSelection"]."</td></tr>";
				}

				/*if($_SESSION["aa"]["eVSupport"] == "true"){
				    if ((isset($value["announcementId"]) && $key == "ah" && $value["announcementId"] !== $_SESSION["aa"][$key]["announcementId"] && $value["announcementSelection"] == "Personal") || isset($value["vidAnnouncementId"]) && $key == "ah" && $value["vidAnnouncementId"] !== $_SESSION["aa"][$key]["vidAnnouncementId"] && $value["announcementSelection"] == "Personal")
				    {
				        $changes = 1;
				        $bg = CHANGED;
				        if ($value["announcementId"] == "" && $value["vidAnnouncementId"] == "")
				        {
				            $bg = INVALID;
				            $error = 1;
				            $ahAnnouncementLabel = $value["id"] = $_SESSION["aaNames"]["ahAnnouncementId"] . " is required.";
				        }else{
				            $ahAnnouncementLabel = $value["announcementId"]." ".$value["vidAnnouncementId"];
				        }
				        
				        $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"]["ahAnnouncementId"] . "</td><td class=\"errorTableRows\">".$ahAnnouncementLabel."</td></tr>";
				    }
				    
				}else{
				    if (isset($value["announcementId"]) && $key == "ah" && $value["announcementId"] !== $_SESSION["aa"][$key]["announcementId"] && $value["announcementSelection"] == "Personal")
				    {
				        $changes = 1;
				        $bg = CHANGED;
				        if ($value["announcementId"] == "")
				        {
				            $bg = INVALID;
				            $error = 1;
				            $ahAnnouncementLabel = $value["id"] = $_SESSION["aaNames"]["ahAnnouncementId"] . " is required.";
				        }else{
				            $ahAnnouncementLabel = $value["announcementId"];
				        }
				        
				        $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"]["ahAnnouncementId"] . "</td><td class=\"errorTableRows\">".$ahAnnouncementLabel."</td></tr>";
				    }
				}*/
				
				if($_SESSION["aa"]["eVSupport"] == "true"){
				    if($key == "ah" && $value["announcementSelection"] == "Personal"){
				        if($value["announcementId"] == "" && $value["vidAnnouncementId"] ==""){
				            $changes = 1;
				            $bg = INVALID;
				            $error = 1;
				            $ahAnnouncementLabel = $value["id"] = "A WAV, WMA, MOV or 3GP file must be uploaded for the Personal Greeting.";
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td class=\"errorTableRows\">".$ahAnnouncementLabel."</td></tr>";
				        }else if(($value["announcementId"] != $_SESSION["aa"][$key]["announcementId"]) || ($value["vidAnnouncementId"] != $_SESSION["aa"][$key]["vidAnnouncementId"])){
				            $changes = 1;
				            $bg = CHANGED;
				            $ahAnnouncementLabel = $value["announcementId"]." ".$value["vidAnnouncementId"];
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td class=\"errorTableRows\">".$ahAnnouncementLabel."</td></tr>";
				        }
				        
				        
				    }
				}else{
				    if($key == "ah" && $value["announcementSelection"] == "Personal"){
				        if($value["announcementId"] == ""){
				            $changes = 1;
				            $bg = INVALID;
				            $error = 1;
				            $ahAnnouncementLabel = $value["id"] = "A WAV or WMA file must be uploaded for the Personal Greeting.";
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td class=\"errorTableRows\">".$ahAnnouncementLabel."</td></tr>";
				        }else if($value["announcementId"] != $_SESSION["aa"][$key]["announcementId"]){
				            $changes = 1;
				            $bg = CHANGED;
				            $ahAnnouncementLabel = $value["announcementId"];
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td class=\"errorTableRows\">".$ahAnnouncementLabel."</td></tr>";
				        }
				        
				        
				    }
				}
				if ($key == "h" && $value["announcementSelection"] !== $_SESSION["aa"][$key]["announcementSelection"])
				{
					$changes = 1;
					$bg = CHANGED;
					if ($value["announcementSelection"] == "")
					{
						$bg = INVALID;
						$error = 1;
						$value["id"] = $_SESSION["aaNames"]["announcementSelection"] . " is required.";
					}
					$data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"]["hannouncementSelection"] . "</td><td class=\"errorTableRows\">".$value["announcementSelection"]."</td></tr>";
				}
				
				/*if($_SESSION["aa"]["eVSupport"] == "true"){
				    if ((isset($value["announcementId"]) && $key == "h" && $value["announcementId"] !== $_SESSION["aa"][$key]["announcementId"] && $value["announcementSelection"] == "Personal") || isset($value["vidAnnouncementId"]) && $key == "h" && $value["vidAnnouncementId"] !== $_SESSION["aa"][$key]["vidAnnouncementId"] && $value["announcementSelection"] == "Personal")
				    {
				        $changes = 1;
				        $bg = CHANGED;
				        if ($value["announcementId"] == "" && $value["vidAnnouncementId"] == "")
				        {
				            $bg = INVALID;
				            $error = 1;
				            $hAnnouncementLabel= $value["id"] = $_SESSION["aaNames"]["hAnnouncementId"] . " is required.";
				        }else{
				            $hAnnouncementLabel = $value["announcementId"]." ".$value["vidAnnouncementId"];
				        }
				        
				        $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"]["hAnnouncementId"] . "</td><td class=\"errorTableRows\">".$hAnnouncementLabel."</td></tr>";
				    }
				    
				}else{
				    if (isset($value["announcementId"]) && $key == "h" && $value["announcementId"] !== $_SESSION["aa"][$key]["announcementId"] && $value["announcementSelection"] == "Personal")
				    {
				        $changes = 1;
				        $bg = CHANGED;
				        if ($value["announcementId"] == "")
				        {
				            $bg = INVALID;
				            $error = 1;
				            $hAnnouncementLabel= $value["id"] = $_SESSION["aaNames"]["hAnnouncementId"] . " is required.";
				        }else{
				            $hAnnouncementLabel = $value["announcementId"];
				        }
				        
				        $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"]["hAnnouncementId"] . "</td><td class=\"errorTableRows\">".$hAnnouncementLabel."</td></tr>";
				    }
				}*/
				
				if($_SESSION["aa"]["eVSupport"] == "true"){
				    if($key == "h" && $value["announcementSelection"] == "Personal"){
				        if($value["announcementId"] == "" && $value["vidAnnouncementId"] ==""){
				            $changes = 1;
				            $bg = INVALID;
				            $error = 1;
				            $hAnnouncementLabel = $value["id"] = "A WAV, WMA, MOV or 3GP file must be uploaded for the Personal Greeting.";
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td class=\"errorTableRows\">".$hAnnouncementLabel."</td></tr>";
				        }else if(($value["announcementId"] != $_SESSION["aa"][$key]["announcementId"]) || ($value["vidAnnouncementId"] != $_SESSION["aa"][$key]["vidAnnouncementId"])){
				            $changes = 1;
				            $bg = CHANGED;
				            $hAnnouncementLabel = $value["announcementId"]." ".$value["vidAnnouncementId"];
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td class=\"errorTableRows\">".$hAnnouncementLabel."</td></tr>";
				        }
				        
				        
				    }
				}else{
				    if($key == "h" && $value["announcementSelection"] == "Personal"){
				        if($value["announcementId"] == ""){
				            $changes = 1;
				            $bg = INVALID;
				            $error = 1;
				            $hAnnouncementLabel = $value["id"] = "A WAV or WMA file must be uploaded for the Personal Greeting.";
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td class=\"errorTableRows\">".$hAnnouncementLabel."</td></tr>";
				        }else if($value["announcementId"] != $_SESSION["aa"][$key]["announcementId"]){
				            $changes = 1;
				            $bg = CHANGED;
				            $hAnnouncementLabel = $value["announcementId"];
				            $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";width: 47%;\">Personal Greeting</td><td class=\"errorTableRows\">".$hAnnouncementLabel."</td></tr>";
				        }
				        
				        
				    }
				}
				if ($value["enableLevelExtensionDialing"] !== $_SESSION["aa"][$key]["enableLevelExtensionDialing"])
				{
					$changes = 1;
					$bg = CHANGED;
				//	$menuExtensionData .= "<tr style=\"background:" . $bg . ";\"><td colspan=\"4\" class=\"errorTableRows\">";
				
                                $menuExtensionData .= "<tr><td colspan='2' class=\"errorTableRows\" style=\"background:" . $bg . ";width: 47%;\">";        
                                        if (strpos($key, "submenu") === 0)
					{
						$menuExtensionData .= $_SESSION["aaNames"]["enableLevelExtensionDialing"];
					}
					else
					{
						$menuExtensionData .= $_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"];
					}
					//$menuExtensionData .= ": " . $value["enableLevelExtensionDialing"] . "</td></tr>";
                                      $menuExtensionData .= "<td colspan='2' class=\"errorTableRows\">". $value["enableLevelExtensionDialing"] . "</td></tr>";
				}
                                
                              /*  if ($value["enableLevelExtensionDialing"] !== $_SESSION["aa"][$key]["enableLevelExtensionDialing"])
				{
                                    $changes = 1;
                                    $bg = CHANGED;
                                    $menuExtensionDataNew ="";

                                    if (strpos($key, "submenu") === 0)
                                    {
                                      $menuExtensionDataNew = $_SESSION["aaNames"]["enableLevelExtensionDialing"];
                                    }
                                    else
                                    {
                                        $menuExtensionDataNew = $_SESSION["aaNames"]["enableFirstMenuLevelExtensionDialing"];
                                    }

                                    $menuExtensionData .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";width:47%;\">" . $menuExtensionDataNew. "</td><td class=\"errorTableRows\">".$value["enableLevelExtensionDialing"]."</td></tr>";
                                        
				}*/
                                
				foreach ($value["keys"] as $k => $v)
				{
					$bg = UNCHANGED;
					$desc = (isset($_SESSION["aa"][$key]["keys"][$k]["desc"]) ? $_SESSION["aa"][$key]["keys"][$k]["desc"] : "");
					$act = (isset($_SESSION["aa"][$key]["keys"][$k]["action"]) ? $_SESSION["aa"][$key]["keys"][$k]["action"] : "");
					$ph = (isset($_SESSION["aa"][$key]["keys"][$k]["phoneNumber"]) ? $_SESSION["aa"][$key]["keys"][$k]["phoneNumber"] : "");
					$sub = (isset($_SESSION["aa"][$key]["keys"][$k]["submenuId"]) ? $_SESSION["aa"][$key]["keys"][$k]["submenuId"] : "");
					$announcements = (isset($_SESSION["aa"][$key]["keys"][$k]["announcements"]) ? $_SESSION["aa"][$key]["keys"][$k]["announcements"] : "");
					$announcementsVid = (isset($_SESSION["aa"][$key]["keys"][$k]["vidAnnouncements"]) ? $_SESSION["aa"][$key]["keys"][$k]["vidAnnouncements"] : "");
					if (trim($v["desc"]) !== trim($desc) or trim($v["action"]) !== trim($act) or trim($v["phoneNumber"]) !== trim($ph) or trim($v["submenu"]) !== trim($sub) or trim($v["announcements"]) !== trim($announcements) or trim($v["vidAnnouncements"]) !== trim($announcementsVid))
					{
						$changes = 1;
						$bg = CHANGED;
					}
					if (strlen($v["desc"]) > 0 or strlen($v["desc"]) > 0)
					{
						if (!preg_match("/^[ A-Za-z0-9-._]+$/", $v["desc"]))
						{
							$error = 1;
							$bg = INVALID;
							$v["desc"] = "Description must be letters, numbers, and dashes only.";
						}
						if (strlen($v["action"]) == 0)
						{
							$error = 1;
							$bg = INVALID;
							$v["action"] = "Action is required.";
						}
					}
					if (substr($v["phoneNumber"], 0, 3) === "011")
					{
						$bg = INVALID;
						$error = 1;
						$v["phoneNumber"] = "Number cannot be an international Number.";
					}
					if (strlen($v["phoneNumber"]) > 0 and !preg_match("/^[ 0-9-*#]+$/", $v["phoneNumber"]))
					{
						$bg = INVALID;
						$error = 1;
						$v["phoneNumber"] = "Invalid phone number.";
					}
					if (strlen($v["phoneNumber"]) == 0 and ($v["action"] == "Transfer With Prompt" or $v["action"] == "Transfer Without Prompt"))
					{
						$bg = INVALID;
						$error = 1;
						$v["phoneNumber"] = "Number is required.";
					}					
					if (strlen($v["submenu"]) == 0 and $v["action"] == "Transfer To Submenu")
					{
						$bg = INVALID;
						$error = 1;
						$v["submenu"] = "Submenu is required.";
					}
					
					if($_SESSION["aa"]["eVSupport"] == "true"){
					    if ((empty($v["announcements"]) && empty($v["vidAnnouncements"])) and $v["action"] == "Play Announcement")
					    {
					        $bg = INVALID;
					        $error = 1;
					        $v["announcements"] = "Announcement is required.";
					    }else{
					        if($v["announcements"] <> "" || $v["vidAnnouncements"] <> ""){
					            $v["announcements"] = $v["announcements"]." ".$v["vidAnnouncements"];
					        }else{
					            $v["announcements"] .= "";
					        }
					    }
					}else{
					    
					    if (empty($v["announcements"]) and $v["action"] == "Play Announcement")
					    {
					        $bg = INVALID;
					        $error = 1;
					        $v["announcements"] = "Announcement is required.";
					    }else{
					        if($v["announcements"] <> ""){
					            $v["announcements"] = $v["announcements"];
					        }else{
					            $v["announcements"] .= "";
					        }
					    }
					}
					
					/*if (empty($v["announcements"]) and $v["action"] == "Play Announcement")
					{
						$bg = INVALID;
						$error = 1;
						$v["announcements"] = "Announcement is required.";
					}else{
						if($v["announcements"] <> ""){
						    $v["announcements"] = $v["announcements"];
						}else{
							$v["announcements"] .= "";
						}
					}
					
					if($_SESSION["aa"]["eVSupport"] == "true"){
					    if (empty($v["vidAnnouncements"]) and $v["action"] == "Play Announcement")
					    {
					        $bg = INVALID;
					        $error = 1;
					        $v["vidAnnouncements"] = "Announcement is required.";
					    }else{
					        if($v["vidAnnouncements"] <> ""){
					            $v["vidAnnouncements"] = $v["vidAnnouncements"];
					        }else{
					            $v["vidAnnouncements"] .= "";
					        }
					    }
					}*/
					
					if ($bg != UNCHANGED)
					{
						$menuData .= "<tr><td style=\"background:" . $bg . ";width:10%;\" class=\"errorTableRows\">" . $k . "</td><td style=\"background:" . $bg . ";width:30%;color:#fff;\" class=\"errorTableRows\">" . $v["action"] . "</td><td class=\"errorTableRows\">" . $v["desc"] . "</td><td class=\"errorTableRows\">";
						if ($v["action"] == "Transfer To Submenu")
						{
							$menuData .= $v["submenu"];
						}elseif($v["action"] == "Play Announcement"){
							//$menuData .= $v["announcements"];
							//if($_SESSION["aa"]["bh"]["keys"][$k]["announcements"] != $v["announcements"]){
							    $menuData .= $v["announcements"]." ";
							//}
							/*if($_SESSION["aa"]["eVSupport"] == "true"){
							    if($_SESSION["aa"]["bh"]["keys"][$k]["vidAnnouncements"] != $v["vidAnnouncements"]){
							        $menuData .= $v["vidAnnouncements"]." ";
							    }
							    
							}*/
						}
						else
						{
							$menuData .= $v["phoneNumber"];
						}
						$menuData .= "</td></tr>";
					}
				}

				if ($menuData !== "" or $menuExtensionData !== "")
				{
					$data .= "<tr><td colspan=\"2\" style=\"padding:0;\"><table align=\"center\" width=\"100%\" style=\"margin-bottom: 20px;\">";
					$data .= "<tr><td align=\"center\" colspan=\"4\" class=\"miniBanner\">";
					if ($key == "bh")
					{
						$data .= "Business Hours Menu";
					}
					if ($key == "ah")
					{
						$data .= "After Hours Menu";
					}
					if ($key == "h")
					{
						$data .= "Holiday Menu";
					}
					if (strpos($key, "submenu") === 0)
					{
						$data .= "Submenu - " . $_SESSION["aa"][$key]["id"];
					}
					$data .= "</td></tr>";
					if ($menuExtensionData !== "")
					{
						$data .= $menuExtensionData;
					}
					if ($menuData !== "")
					{
						$data .= "<tr><th style=\"width:10%\" class=\"labelTextGrey\">Key</th><th style=\"width:30%\" class=\"labelTextGrey\">Action</th><th style=\"width:30%\" class=\"labelTextGrey\">Description</th><th style=\"width:30%\" class=\"labelTextGrey\">Action Data</th></tr>";
						$data .= $menuData;
					}
					$data .= "</table></td></tr>";
				}
			}
			if($key == "newSubmenu" && $_POST["checkNewSubMenuStatus"] == "true"){
			    $obj = new AaOperation();
			    $subList = $obj->getSubMenuListRequest($_POST["aaId"]);
			    
			    if(is_array($subList["Success"]) && array_key_exists($_POST["newSubmenu"]["id"], $subList["Success"])){
			        $error = 1;
			        $bg = INVALID;
			        $changes=1;
			        $value =  $_POST["newSubmenu"]["id"] ." is already exist.";
			        $data .= "<tr><td class=\"errorTableRows\" style=\"background:" . $bg . ";\">" . $_SESSION["aaNames"]["id"] . "</td><td class=\"errorTableRows\">".$value."</td></tr>";
			    }else{
			        $bg = CHANGED;
			        
			        $subMenuData1 = "";
			        $subMenuData1 .= "<tr><td align='center' class='miniBanner' colspan='4'>Submenu-".$_POST["newSubmenu"]["id"]."</td></tr>";
			        if($_POST["newSubmenu"]["id"] ==""){
			            $bg = INVALID;
			            $error = 1;
			            $changes=1;
			            $_POST["newSubmenu"]["id"] = "Submenu ID can not be blank";
			            $subMenuData1 .= "<tr><td colspan = '2' class=\"errorTableRows\" style=\"background:" . $bg . ";width:47%;\">" . $_SESSION["aaNames"]["id"] . "</td><td colspan = '2' class=\"errorTableRows\">".$_POST["newSubmenu"]["id"]."</td></tr>";
			           // $subMenuData .= "<tr><td colspan='4' class=\"errorTableRows\" style=\"background:".$bg.";border-bottom:1px solid white;\">" . $_SESSION["aaNames"]["id"] .": ".$_POST["newSubmenu"]["id"]. "</td></tr>";
			        }else{
			            $changes=1;
			            $subMenuData1 .= "<tr><td colspan='2' class=\"errorTableRows\" style=\"background:#72ac5d;border:1px solid white;color:white;width:47%;\">" . $_SESSION["aaNames"]["id"] ."</td><td colspan = '2' class=\"errorTableRows\">".$_POST["newSubmenu"]["id"]. "</td></tr>";                          
			        }			        
			        $subMenuData1 .= "<tr><td colspan='2' class=\"errorTableRows\" style=\"background:#72ac5d;border:1px solid white;color:white;width:47%;\">" . $_SESSION["aaNames"]["enableLevelExtensionDialing"] ."</td><td colspan = '2' class=\"errorTableRows\"> ".$_POST["newSubmenu"]["enableLevelExtensionDialing"]. "</td></tr>";
			        
			        if($_POST["newSubmenu"]["bhGreeting"] == "Personal Greeting"){
			            if($_SESSION["aa"]["eVSupport"] == "true"){
			                if($_POST["newSubmenu"]["announcementId"] == "" && $_POST["newSubmenu"]["vidAnnouncementId"] == ""){
			                    $bg = INVALID;
			                    $error = 1;
			                    $changes=1;
			                    $_POST["newSubmenu"]["announcementId"] = "Personal Greeting can not be blank";
			                    $subMenuData1 .= "<tr><td colspan = '2' class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td colspan = '2' class=\"errorTableRows\">".$_POST["newSubmenu"]["announcementId"]."</td></tr>";
			                }else{
			                    $newSubmenuLabel = $_POST["newSubmenu"]["announcementId"]." ".$_POST["newSubmenu"]["vidAnnouncementId"];
			                    $changes=1;
			                    $subMenuData1 .= "<tr><td colspan = '2' class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td colspan = '2' class=\"errorTableRows\">".$newSubmenuLabel."</td></tr>";
			                }
			                
			            }else{
			                if($_POST["newSubmenu"]["announcementId"] == ""){
			                    $bg = INVALID;
			                    $error = 1;
			                    $changes=1;
			                    $_POST["newSubmenu"]["announcementId"] = "Personal Greeting can not be blank";
			                    $subMenuData1 .= "<tr><td colspan = '2' class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td colspan = '2' class=\"errorTableRows\">".$_POST["newSubmenu"]["announcementId"]."</td></tr>";
			                }else{
			                    $newSubmenuLabel = $_POST["newSubmenu"]["announcementId"];
			                    $changes=1;
			                    $subMenuData1 .= "<tr><td colspan = '2' class=\"errorTableRows\" style=\"background:" . $bg . ";\">Personal Greeting</td><td colspan = '2' class=\"errorTableRows\">".$newSubmenuLabel."</td></tr>";
			                }
			            }
			            
			        }
			        
			        $subMenuData2 = "";
			        foreach ($_POST["newSubmenu"]["keys"] as $key => $value){
			            
			            $bg = CHANGED;
			           // print_r($value["action"]);
			            if($value["action"] != "" || $value["desc"] != "" || $value["submenu"] != "" || $value["phoneNumber"] != ""){
			                if ($value["phoneNumber"] != "" && substr($value["phoneNumber"], 0, 3) === "011")
			                {
			                    $bg = INVALID;
			                    $error = 1;
			                    $value["phoneNumber"] = "Number cannot be an international Number.";
			                }
			                if (strlen($value["phoneNumber"]) > 0 and !preg_match("/^[ 0-9-*#]+$/", $value["phoneNumber"]))
			                {
			                    $bg = INVALID;
			                    $error = 1;
			                    $value["phoneNumber"] = "Invalid phone number.";
			                }
			                if (strlen($value["phoneNumber"]) == 0 and ($value["action"] == "Transfer With Prompt" or $value["action"] == "Transfer Without Prompt"))
			                {
			                    $bg = INVALID;
			                    $error = 1;
			                    $value["phoneNumber"] = "Number is required.";
			                }
			                if($value["desc"] != "" && $value["action"] == ""){
			                    $bg = INVALID;
			                    $error = 1;
			                    $value["action"] = "Action is a required field.";
			                }
			                if ($value["desc"] != "" && !preg_match("/^[ A-Za-z0-9-._]+$/", $value["desc"]))
			                {
			                    $error = 1;
			                    $bg = INVALID;
			                    $value["desc"] = "Description must be letters, numbers, and dashes only.";
			                }
			                if (strlen($value["submenu"]) == 0 and $value["action"] == "Transfer To Submenu")
			                {
			                    $bg = INVALID;
			                    $error = 1;
			                    $value["submenu"] = "Submenu is required.";
			                }
			                
			                
			                if($_SESSION["aa"]["eVSupport"] == "true"){
			                    if (empty($value["announcements"]) && empty($value["vidAnnouncements"]) and $value["action"] == "Play Announcement")
			                    //if (empty($value["announcements"] && empty($value["vidAnnouncements"])) and $value["action"] == "Play Announcement")
			                    {
			                        $bg = INVALID;
			                        $error = 1;
			                        $value["announcements"] = "Announcement is required.";
			                    }else{
			                        
			                        if($value["announcements"] <> "" || $value["vidAnnouncements"] <> ""){
			                            $value["announcements"] = $value["announcements"]." ".$value["vidAnnouncements"];
			                        }else{
			                            $value["announcements"] .= "";
			                        }
			                    }
			                }else{
			                    if (empty($value["announcements"]) and $value["action"] == "Play Announcement")
			                    {
			                        $bg = INVALID;
			                        $error = 1;
			                        $value["announcements"] = "Announcement is required.";
			                    }else{
			                        
			                        if($value["announcements"] <> ""){
			                            $value["announcements"] = $value["announcements"];
			                        }else{
			                            $value["announcements"] .= "";
			                        }
			                    }
			                }
			                $subMenuData2 .= "<tr><td style=\"background:#72ac5d;border:1px solid white;color:white;\">" . $key . "</td><td style=\"background:".$bg.";border:1px solid white;width:30%;color:white;\">" . $value["action"] . "</td><td class=\"errorTableRows\">" . $value["desc"] . "</td><td class=\"errorTableRows\">";
			                if ($value["action"] == "Transfer To Submenu")
			                {
			                    $subMenuData2 .= $value["submenu"];
			                }elseif($value["action"] == "Play Announcement"){
			                    //if($_SESSION["aa"]["eVSupport"] == "true"){
			                        $subMenuData2 .= $value["announcements"]." ";
			                    //}
			                    //$subMenuData2 .= $value["announcements"]." ";
			                }
			                else
			                {
			                    $subMenuData2 .= $value["phoneNumber"];
			                }
			                $subMenuData2 .= "</td></tr>";
			            }
			            
			            
			        }
			    
			    }
			}
			
			if($key == "assignedServices"){
				$obj = new AaOperation();
				$assignList = $obj->checkAssignedServiceChange($_POST["assignedServices"], $_SESSION["serviceData"]["assigned"]);
				//print_r($assignList);
				if(count($assignList["newAssign"]) > 0){
					$changes = 1;
					//$bg = CHANGED;
					$data .= "<tr><td class='errorTableRows' style='background:#72ac5d'>New Assigned Service</td><td style=background:".$bg.">".implode(", ", $assignList["newAssign"])."</td></tr>";
				}
				if(count($assignList["unAssign"]) > 0){
					$changes = 1;
					//$bg = CHANGED;
					$data .= "<tr><td class='errorTableRows' style='background:#72ac5d'> UnAssigned Service</td><td style=background:".$bg.">".implode(", ", $assignList["unAssign"])."</td></tr>";
				}
			}
			
			
			
			/*if($key == "availableServices"){
				$obj = new AaOperation();
				$availList = $obj->checkAssignedServiceChange($_POST["availableServices"], $_SESSION["serviceData"]["avail"]);
				//print_r($assignList);
				if(count($availList["newAssign"]) > 0){
					$changes = 1;
					//$bg = CHANGED;
					$data .= "<tr><td class='errorTableRows' style=background:#72ac5d>New Assigned Service</td><td style=background:".$bg.">".implode(", ", $availList["newAssign"])."</td></tr>";
				}
				if(count($availList["unAssign"]) > 0){
					$changes = 1;
					//$bg = CHANGED;
					$data .= "<tr><td class='errorTableRows' style=background:#72ac5d> UnAssigned Service</td><td style=background:".$bg.">".implode(", ", $availList["unAssign"])."</td></tr>";
				}
			}*/
		}
		$k++;
	}
	$data .= "</table>";
	$subMenuData = "<table style='width: 850px;margin: 0 auto;' align=\"center;\" class=\"confSettingTable\">";
	$subMenuData .= $subMenuData1;
	if($subMenuData2 != ""){
	    $subMenuData .= "<tr><th width=\"10%\">Key</th><th width=\"30%\">Action</th><th width=\"30%\">Description</th><th width=\"30%\">Action Data</th></tr>";
	    $subMenuData .= $subMenuData2;
	}
	$subMenuData .= "</table>";
	//$data .= $subMenuData;

	if ($changes == 0)
	{
		$error = 1;
		$data = "<label class=\"labelTextGrey\">You have made no changes.</label>";
	}
	echo $error . $data . $subMenuData;
?>