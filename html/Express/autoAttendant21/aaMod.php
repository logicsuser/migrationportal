<script type="text/javascript" src="autoAttendant21/js/aaJsLib.js"></script>
 
<?php
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	require_once ("/var/www/lib/broadsoft/adminPortal/getAvailableNumbers.php");
	$extLength = "";
	if(isset($_SESSION['groupExtensionLength']['default'])){
		$extLength = $_SESSION['groupExtensionLength']['default'];
	}
	function isDepartmentVisibleAAAdd() {
		global $useDepartments, $departmentsFieldVisible;
		
		if ($useDepartments == "false") {
			return false;
		}
		
		return $departmentsFieldVisible == "true";
	}
	
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupOperation.php");
	$groupId = $_SESSION['groupId'];
	$sp = $_SESSION['sp'];
	$info = new GroupOperation($sp, $groupId);
	$basicInfo = $info->getGroupBasicInfo();
	$groupName = strval($basicInfo["Success"]->groupName);
	
	//services assign check
	require_once '/var/www/lib/broadsoft/adminPortal/services/Services.php';
	$serviceObj = new Services();
	$serviceResponse = $serviceObj->GroupServiceGetAuthorizationListRequest($groupId);
	
	$autoAttendantServicesBasic = array();
	$autoAttendantServicesStandard = array();
	$autoAttendantServicesVideo = array();
	foreach($serviceResponse["Success"]["groupServicesAuthorizationTable"] as $key=>$val){
		if($val[0] == "Auto Attendant"){
		    $autoAttendantServicesBasic[] = $val;
		}else if($val[0] == "Auto Attendant - Standard"){
		    $autoAttendantServicesStandard[] = $val;
		}else if($val[0] == "Auto Attendant - Video"){
		    $autoAttendantServicesVideo[] = $val;
		}
	}
	//echo "<pre>"; print_r($serviceResponse["Success"]["groupServicesAuthorizationTable"]);
	//echo "<pre>"; print_r($autoAttendantServices);
	//ends assign check
	
	require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
	
	$sp = $_SESSION['sp'];
	$groupId = $_SESSION['groupId'];
	$dns = new Dns();
	$extensionResponse = $dns->getGroupExtensionConfigRequest($sp, $groupId);
?>
<script>
$(function(){

	var checkCriteriaValues = function(aaNameCriteria, enterpriseID, groupId, dn, ext, groupName){

		aaStr = aaNameCriteria.replace("<EnterpriseID>", enterpriseID).replace("<enterpriseID>", enterpriseID).replace("<GROUP_NAME>", groupName).replace("<groupId>", groupId).replace("<GroupID>", groupId).replace("<group_Name>", groupName).replace("<EXT>", ext).replace("<DN>", dn);
		
		if(aaStr.indexOf("<") > -1 || aaStr.indexOf("<") > -1 ){
			aaStr = enterpriseID + "_" + groupId;
		}

		return aaStr;
	};
	var checkClidCriteriaValues = function(aaNameCriteria, enterpriseID, groupId, dn, ext, groupName){

		aaStr = aaNameCriteria.replace("<EnterpriseID>", enterpriseID).replace("<enterpriseID>", enterpriseID).replace("<GROUP_NAME>", groupName).replace("<groupId>", groupId).replace("<GroupID>", groupId).replace("<group_Name>", groupName).replace("<EXT>", ext).replace("<DN>", dn);
		if(aaStr.indexOf("<") > -1 || aaStr.indexOf("<") > -1 ){
			aaStr = enterpriseID + "_" + groupId;
		}
		
		return aaStr;
	};

	var generateCriteriaAA = function(aaNameCriteria, enterpriseID, groupId, dn, ext, groupName){
		aaName = checkCriteriaValues(aaNameCriteria, enterpriseID, groupId, dn, ext, groupName);
		$("#aaName").val(aaName);
	};

	var generateCriteriaClidF = function(clidFname, enterpriseID, groupId, dn, ext, groupName){		
		clidFnameStr = checkClidCriteriaValues(clidFname, enterpriseID, groupId, dn, ext, groupName);		
		$("#callingLineIdFirstName").val(clidFnameStr);
	};

	var generateCriteriaClidL = function(clidLname, enterpriseID, groupId, dn, ext, groupName){
		clidLnameStr = checkClidCriteriaValues(clidLname, enterpriseID, groupId, dn, ext, groupName);
		$("#callingLineIdLastName").val(clidLnameStr);
	};

	
	
	var createAutoCriteria = function(){
		
		var enterpriseID = "<?php echo $_SESSION['sp'];?>";
		var groupId = "<?php echo $_SESSION['groupId'];?>";
		var groupName = "<?php echo $groupName;?>";
		var dn = $("#phoneNumberAdd").val();
		var ext = $("#extensionAdd").val();

		var aaNameCriteria = "<?php echo $aaNameCriteriaValue;?>";		
		var clidFname = "<?php echo $aaClidFnameValue;?>";		
		var clidLname = "<?php echo $aaClidLnameValue;?>";		

		generateCriteriaAA(aaNameCriteria, enterpriseID, groupId, dn, ext, groupName);
		generateCriteriaClidF(clidFname, enterpriseID, groupId, dn, ext, groupName);
		generateCriteriaClidL(clidLname, enterpriseID, groupId, dn, ext, groupName);
		
	};
	
	$('#newAttendant').click(function() {
		if($(".broadSoftRelError")){
			$(".broadSoftRelError").remove();
		}
		createAutoCriteria();
	});

	$("#phoneNumberAdd").change(function(){
		createAutoCriteria();
	});
	$("#extensionAdd").keyup(function(){
		createAutoCriteria();
	});
	
});
</script>

<style>select::-ms-expand {	display: none; }
select{
    -webkit-appearance: none;
    appearance: none;
}
.labelTextOpp{
        margin-left: 0px !important;
    }
    .labelTextOpp span:first-child{
        margin-left: 12px; 
    }
</style>
<h2 class="subBannerCustom">Auto Attendants</h2>
<form method="POST" id="hgMod" class="aaInfoForm">
	<div  id="modifyAttendant">
		<div class="row">
            <div class="col-md-9 adminSelectDiv">
             	<div class="form-group">
				   <label class="labelText"> Auto Attendant</label>
				   <div class="dropdown-wrap">   
                      	<select name="AAChoice" id="AAChoice" style="width:100% !important;">
						<option value=""></option>
						<?php
						/*
							require_once("/var/www/lib/broadsoft/adminPortal/getAllAutoAttendants.php");
							if (isset($autoAttendants))
							{
								foreach ($autoAttendants as $key => $value)
								{
									echo "<option value=\"" . $value["id"] . "\">" . $value["name"] . "</option>";
								}
							}
						*/
						?>
					</select>
					</div>
        		</div>
            </div>
            
            
            <div class="col-md-1 adminAddBtnDiv" style="width: 8.1% !important;">
    			<div class="form-group">
        			<?php
                                        //Code added @ 03 April 2019
                                        $tempServicesPermissionsArr         = explode("_", $_SESSION["permissions"]["groupAdminServicesPermission"]);
                                        $groupAdminServicesPrmson           = $tempServicesPermissionsArr[0];
                                        
        				if ($_SESSION["superUser"] == "1" || $groupAdminServicesPrmson == "1")
        				{
        						echo "<p class=\"register-submit addAdminBtnIcon\">";
        						echo "<img src=\"images\icons\add_admin_rest.png\" name=\"newAttendant\" id=\"newAttendant\" class=\"center-block\" style=\"margin-left: 54px !important;\">";
        					echo "</p>";
        					echo "<label class=\"labelText labelTextOpp\"><p style=\"text-align: center; margin-bottom: 0px;\">Add</p><p style=\"text-align: center;\">Auto Attendant</p></label>";
        				}
        			?> 			
				</div> 
			</div>
			<div class="col-md-1 adminSelectDiv" style="padding:0;width:6%;">&nbsp;
            </div>
			
			<div class="col-md-1 adminAddBtnDiv" id = "addNewSubmenu" style="width: 8.1% !important;">
    			<div class="form-group">
        			<?php
        				 
                                        if ($_SESSION["superUser"] == "1" || $groupAdminServicesPrmson == "1")
        				{
        						echo "<p class=\"register-submit addAdminBtnIcon newAutoAttendentButton\">";
        						echo "<img src=\"images\icons\add_admin_rest.png\" name=\"addNewSubmenu\" class=\"center-block\">";
        					echo "</p>";
        					echo "<label class=\"labelText labelTextOpp\"><p style=\"text-align: center; margin-bottom: 0px;\">Add</p><p style=\"text-align: center;\">New Submenu</p></label>";
        				}
                               ?>
					
				</div> 
			</div>
			
<!-- New code found from Express Start-->
				<!-- <textarea id="aaNameExist" style="visibility:hidden; display:none"><</textarea>
				<div style="text-align:center">
					<p class="register-submit newAutoAttendentButton" style="width:auto !important;float:none !important">
						<input name = "newAttendant" value = "Add New Auto Attendant" id = "newAttendant" type = "button" class = "email" style="width: 200px !important;">
					</p>
					<p class="register-submit newAutoAttendentButton" style="width:auto !important;float:none !important">
						<input name = "addNewSubmenu" value = "Add New Submenu" id = "addNewSubmenu" type = "button" class = "addNewSubmenu" style="width: 200px !important;">
					</p>           
             		
				</div> -->
<!-- New code found from Express End-->				
		</div>
	</div>
</form>

<div class="loading" id="loading2">
	<img src="/Express/images/ajax-loader.gif">
</div>
<div id="addAutoAttendant" style="margin-top: 20px;">
	<input type="hidden" name = "aaModOperation" id="aaModOperation" />
		<form action="" method="POST" id="autoAttendantadd" class="aaInfoForm">
			<div id="tabs">
			<div class="divBeforeUl">
				<ul>
					<li><a href="#basic_info" class="tabs" id="basicInfo">Basic Info</a></li>
					<li><a href="#business_hours_menu" class="tabs" id="businessHoursMenu">Business Hours Menu</a></li>
					<li><a href="#after_hours_menu" class="tabs" id="afterHoursMenu">After Hours Menu</a></li>
					<li><a href="#holiday_menu" class="tabs" id="holidayMenu">Holiday Menu</a></li>
					<li style="display:none"><a href="#services_1" class="tabs" id="services" style="display:none">Services</a></li>
					<!-- <li><a href="#call_application_policy_1" class="tabs" id="call_application_policy">Call Application Policies</a></li> -->
					<li><a href="#call_policy_1" class="tabs" id="call_policy">Call Policies</a></li>
				</ul>
					</div>
				<div id="basic_info">
					<div class="row">
                                            <h2 class="subBannerCustom">Basic Info</h2>
                		
                                                
                                            <div class="col-md-6" id="autoAttendantNameDiv">
                                                <div class="form-group">
                                                    <label class="labelText" for="name">Auto Attendant Name:<span class="required">*</span></label>
                                                    <input type="text" name="aaName" id="aaName" size="35" maxlength="" />	 
                                                </div>
                                            </div>
                                                
                                                
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="labelText">Select Attendant Type:<span class="required">*</span></label>
                                                    <div class="dropdown-wrap">
                                                        <select name="aaType" id="aaType">
                                                            <option value="">--Select Attendant Type--</option>
                                                            <option value="Basic">Basic</option>
                                                            <option value="Standard">Standard</option>
                                                        </select>
                                                    </div>
                                                    <label class="labelText" id="noServicesAssign">Auto Attendant Services Not Assigned</label>
                                                </div>
                                            </div>
                                        </div>
                                                
                                <!--calling line id first name --> 
                                        <div class="row">
                                            <div  class="col-md-6" id="autoAttendantClidFDiv">
                                                <div class="form-group">
                                                    <label class="labelText" for="callingLineIdFirstName">Calling Line Id First Name:<span class="required"></span></label>
                                                    <input type="text" name="callingLineIdFirstName" id="callingLineIdFirstName" size="35" maxlength="30" />
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-6" id="autoAttendantClidLDiv">
                                                <div class="form-group">
                                                    <label class="labelText" for="callingLineIdLastName">Calling Line Id Last Name:<span class="required"></span></label>
                                                    <input type="text" name="callingLineIdLastName" id="callingLineIdLastName" size="35" maxlength="30" />
                                                </div>
                                            </div>
					</div>
                                
                                
                                <!--end calling line id  -->
                                
                                    <div class="row">        
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="labelText">Phone Number:</label>
                                                <div class="dropdown-wrap">
                                                    <select name="phoneNumberAdd" onchange="extensionBuilderOnAddAA(this.value, '<?php echo $extLength; ?>')"
                                                                            id="phoneNumberAdd">
                                                        <option value="">None</option>
                                                            <?php
                                                            if (isset ( $availableNumbers )) {
                                                                foreach ( $availableNumbers as $value ) {
                                                                    echo "<option value=\"" . $value . "\">" . $value . "</option>";
                                                                }
                                                            }
                                                            ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
					 
                                    <div class="col-md-3">
                                            <div class="form-group">
                                        		<label class="labelText">Extension:<span class="required">*</span></label>
        			                             <!--	<input type="text" name="extensionAdd" id="extensionAdd" value=""> -->
												<input type="text" name="extensionAdd" id="extensionAdd" value=""
                                                size="<?php echo $_SESSION['groupExtensionLength']['max'];?>"
                                                maxlength="<?php echo $_SESSION['groupExtensionLength']['max'];?>" style="width:400px !important">
                                            </div>
                                    </div>
                		
                                	<div class="col-md-3" id="divActivateNumberAdd" style="display: none">
                						<div class="form-group" style="float:right">
                							<div class="form-group"></div>
                							<input type="checkbox" name="aaActivateNumberAdd" id="aaActivateNumberAdd" value="Yes" style="width: auto" />
                							<label for="aaActivateNumberAdd"><span></span></label> 
                							<label class="labelText" for="aaActivateNumber">Activate Number:</label>
                						</div>
                					</div>
					
                                    </div>
					

                                    <div class="row ">
									
									<div class="col-md-6"  style="display: <?php echo (isDepartmentVisibleAAAdd() ? " block" : " none"); ?> ">
										<div class="form-group">
											<label class="labelText" for="department">Department:</label>
											<div class="dropdown-wrap">
											<select name="department" id="department"> </select>
											</div>
											
										</div>
									</div>
									
									<div class="col-md-6">
														<div class="form-group">
															<label class="labelText" for="timeZone">Time zone:</label><span class="required">*</span>
															<div class="dropdown-wrap">   
																<select name="timeZone" id="timeZone"></select>
															</div>
														</div>
									</div>
									
    						
					</div>
					
					
					<div class="row">
    					<div class="col-md-6" id="businessHoursScheduleDisplay">
    						<div class="form-group">
    							<label class="labelText">Business Hours Schedule:</label>
								<div class="dropdown-wrap">
    							<select name="businessHoursSchedule" id="businessHoursSchedule"></select>
								</div>
    						</div>
    					</div>
    					<div class="col-md-6" id="holidayScheduleDisplay">
    						<div class="form-group">
        						<label class="labelText" for="holidaySchedule">Holiday Schedule:</label>
								<div class="dropdown-wrap">
        						<select name="holidaySchedule" id="holidaySchedule"></select>
								</div>
    						</div>	
    					</div>
    				</div>
					
					<!--new line added-->
					<div class="row">
						<div class="col-md-6">
    						<div class="form-group">
								<input type="checkbox" id="eVsupport" name="eVsupport" value="true">
    							<label for="eVsupport"><span></span></label>
    							<label class="labelText" for="eVsupport">Enable Video Support:</label>
								<input type="hidden" name="aaBasicServiceAssign" id="aaBasicServiceAssign" value="<?php if(!empty($autoAttendantServicesBasic[0][2])){echo $autoAttendantServicesBasic[0][2];}else{echo "false";}?>">
								<input type="hidden" name="aaStandardServiceAssign" id="aaStandardServiceAssign" value="<?php if(!empty($autoAttendantServicesStandard[0][2])){echo $autoAttendantServicesStandard[0][2];}else{echo "false";}?>">
								<input type="hidden" name="aaVideoServiceAssign" id="aaVideoServiceAssign" value="<?php if(!empty($autoAttendantServicesVideo[0][2])){echo $autoAttendantServicesVideo[0][2];}else{echo "false";}?>">
    						</div>
    					</div>
    					 
						
						<div class="col-md-6"></div>
					</div>
					
					<!--end new line add -->
					
 
					
					 
					<div class="row">					
    					<div class="col-md-6" id="holidayScheduleDisplay">
    						<div class="form-group">
        						 <label class="labelText">Scope Of Extension Dialing:<span class="required">*</span></label><br/>
    							<?php 
        							if($_SESSION["enterprise"] == "true"){
        								echo "<input type='radio' id='extensionDialingEnterprise' name='extensionDialing' value='Enterprise' checked />";
                                        echo "<label for='extensionDialingEnterprise'><span></span></label><label class='labelText'>Enterprise</label>";
        								echo "<input type='radio' name='extensionDialing' id= 'extensionDialingGroup' value ='Group'/>";
        								echo "<label for='extensionDialingGroup'><span></span></label><label class='labelText'>Group</label>";
        								if($useDepartments == "true"){
        								    echo "<input type='radio' name='extensionDialing' value ='Department' id='extensionDialingDepartment' />";
        								    echo "<label for='extensionDialingDepartment'><span></span></label><label class='labelText'>Department</label>";
        								}
        								
        							}else {
        							    echo "<input type='radio' name='extensionDialing' id= 'extensionDialingGroup' value ='Group' checked/>";
        							    echo "<label for='extensionDialingGroup'><span></span></label><label class='labelText'> Group</label>";
        							    if($useDepartments == "true"){
        							        echo "<input type='radio' name='extensionDialing' value ='Department' id='extensionDialingDepartment' />";
        							        echo "<label for='extensionDialingDepartment'><span></span></label><label class='labelText'>Department</label>";
        							    }
        							    
        							}
    							?>
    						</div>	
    					</div>
    					<div class="col-md-6">
    						<div class="form-group">
        						<label class="labelText">Scope Of Name Dialing:<span class="required">*</span></label>
        						<br/>
        						<?php 
        							if($_SESSION["enterprise"] == "true"){
        								echo "<input type='radio' name='nameDialing' value='Enterprise' checked id='nameDialingEnterprise'/>";
        								echo "<label for='nameDialingEnterprise'><span></span></label><label class='labelText'>Enterprise</label>";
        								
        								echo "<input type='radio' name='nameDialing' value ='Group' id= 'nameDialingGroup'/>";
        								echo "<label for='nameDialingGroup'><span></span></label><label class='labelText'>Group</label>";
        								if($useDepartments == "true"){
        								    echo "<input type='radio' name='nameDialing' value ='Department' id= 'nameDialingDepartment'/>";
        								    echo "<label for='nameDialingDepartment'><span></span></label><label class='labelText'>Department</label>";
        								}
        								
        							
        							}else {
        							    echo "<input type='radio' name='nameDialing' value ='Group' id= 'nameDialingGroup' checked/>";
        							    echo "<label for='nameDialingGroup'><span></span></label><label class='labelText'>Group</label>";
        							    if($useDepartments == "true"){
        							        echo "<input type='radio' name='nameDialing' value ='Department' id= 'nameDialingDepartment'/>";
        							        echo "<label for='nameDialingDepartment'><span></span></label><label class='labelText'>Department</label>";
        							    }
        							    
        							}
        						?>
    						</div>
    					</div>
					</div>				
					<div class="row">
    					<div class="col-md-12">
    						<div class="form-group">
    			 				<label class="labelText">Name Dialing Entries:<span class="required">*</span></label><br/>
    							<input type = "radio" name="nameDialingEntries" id ="nameDialingEntries" checked value = "LastName + FirstName"/> 
    							<label for ="nameDialingEntries"><span></span></label><label class='labelText'>LastName + FirstName</label>
    							<input type = "radio" id ="nameDialingEntriess" name="nameDialingEntries" value = "LastName + FirstName or FirstName + LastName" />
    							<label for ="nameDialingEntriess"><span></span></label>
    							<label class='labelText'>LastName + FirstName and FirstName + LastName</label>
    						</div>
    					</div>
					</div>
				</div><!-- end userInfoData -->					
 
 <!-- Business Hours Menu start -->
				<div id="business_hours_menu" class="userDataClass" style="display: none;">
					<h2 class="subBannerCustom">Business Hours Menu</h2>
					<div class="row">
                    	<div class="col-md-6">
                    		<div class="form-group">
                    			<label class="labelText">Business Hours Greeting:</label>
                    			<input type="radio" name="bhGreeting" id="bhGreeting1"  checked/><label for="bhGreeting1"><span></span></label>Default Greeting
                    			<input type="radio" name="bhGreeting" id="bhGreetingP2" /><label for="bhGreetingP2"><span></span></label>Personal Greeting
                    		
                    		</div>
                    	</div>
						<div class="col-md-6">
    						<div class="form-group">
							<div class="dropdown-wrap">
                				<select class="pGreetingSelect" name="pGreetingSelect" id="bhmGreetingSelect">
                					<option value="">-Select Greeting-</option>
                				</select>
								</div>
    						</div>
						</div>
					</div>

					<div class="row">
                    	<div class="col-md-6">
                    		<div class="form-group">
                    			<label class="labelText">Menu Options</label>
                    			<input type="checkbox" id="enableFLExtDialogBhMenu"/><label for="enableFLExtDialogBhMenu"><span></span></label>Enable first-level extension dialing
                    		</div>
                    	</div>
                    	<div class="col-md-6">
                    		<div class="form-group">
                    		</div>
                    	</div>
					</div>

    				<div class="viewDetail autoHeight viewDetailNew">
    					<table class="scroll tablesorter aATable" style="width: 100%; margin: 0;">
                    		<thead>
                    		<th style="width:25%;">Key</th>
                    		<th style="width:25%;">Action<span class="required">*</span></th>
                    		<th style="width:25%;">Description</th>
                    		<th style="width:25%;">Action Data</th>
                    	  	</thead>
    						<tbody> 
                    			<tr> 
                    				<td style='width:25%;'>0</td>
                    				<td style='width:25%;'>	<div class="dropdown-wrap"><select name="bhData0" id="bhData0"></select></div></td>
                    				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                    				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                    			</tr>
                    			<tr> 
                    				<td style='width:25%;'>1</td>
                    				<td style='width:25%;'><div class="dropdown-wrap"><select name="bhData1" id="bhData1"></select></div></td>	
                    				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                    				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                    			</tr>
            					<tr> 
                					<td style='width:25%;'>2</td>
                					<td style='width:25%;'>	<div class="dropdown-wrap"><select name="bhData2" id="bhData2"></select></div></td>	
                					<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                					<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            					</tr>
            			
            					<tr> 
            						<td style='width:25%;'>3</td>
            						<td style='width:25%;'>	<div class="dropdown-wrap"><select name="bhData3" id="bhData3"></select></div></td>	
            				 		<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            						<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            					</tr>
            					<tr> 
            						<td style='width:25%;'>4</td>
            						<td style='width:25%;'>	<div class="dropdown-wrap"><select name="bhData4" id="bhData4"></select></div></td>	
                    				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                    				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            					</tr>
            					<tr> 
            						<td style='width:25%;'>5</td>
            						<td style='width:25%;'>	<div class="dropdown-wrap"><select name="bhData5" id="bhData5"></select></div></td>	
            				 		<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            						<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            					</tr>
            					<tr> 
            						<td style='width:25%;'>6</td>
            						<td style='width:25%;'>	<div class="dropdown-wrap"><select name="bhData6" id="bhData6"></select></div></td>	
            				 		<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            						<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            					</tr>
            					<tr> 
            						<td style='width:25%;'>7</td>
            						<td style='width:25%;'>
									<div class="dropdown-wrap"><select name="bhData7" id="bhData7"></select></div></td>	
            			 			<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            						<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            					</tr>
            					<tr> 
            						<td style='width:25%;'>8</td>
            						<td style='width:25%;'><div class="dropdown-wrap"><select name="bhData8" id="bhData8"></select></div></td>	
            			 			<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            						<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            					</tr>
            					<tr> 
            						<td style='width:25%;'>9</td>
            						<td style='width:25%;'><div class="dropdown-wrap"><select name="bhData9" id="bhData9"></select></div></td>	
            			 			<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            						<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            					</tr>
            					<tr> 
            						<td style='width:25%;'>*</td>
            						<td style='width:25%;'><div class="dropdown-wrap"><select name="bhData10" id="bhData10"></select></div></td>	
            			 			<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            						<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            					</tr>
            					<tr> 
            						<td style='width:25%;'>#</td>
            						<td style='width:25%;'><div class="dropdown-wrap"><select name="bhData11" id="bhData11"></select></div></td>	
            			 			<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            						<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            					</tr>
            			 	</tbody> 
    			 		</table>
    				</div>			 
				</div><!-- Business Hours Menu -->



<!--<div class="row ">
    					<div class="col-md-6">
    						<div class="form-group">
								<label class="labelText" for="timeZone">Time zone:</label><span class="required">*</span>
								<div class="dropdown-wrap">   
								<select name="timeZone" id="timeZone"></select>
									</div>
    						</div>
    					</div>
    					<div class="col-md-6">
    						<div class="form-group">
    						<div class="form-group"></div>
    							<input type="checkbox" id="eVsupport" name="eVsupport" value="true">
    							<label for="eVsupport"><span></span></label>
    							<label class="labelText" for="eVsupport">Enable Video Support:</label>
    						</div>
    					</div>
    					<div class="form-group">
                    		<input type="hidden" name="aaBasicServiceAssign" id="aaBasicServiceAssign" value="<?php // if(!empty($autoAttendantServicesBasic[0][2])){echo $autoAttendantServicesBasic[0][2];}else{echo "false";}?>">
                			<input type="hidden" name="aaStandardServiceAssign" id="aaStandardServiceAssign" value="<?php // if(!empty($autoAttendantServicesStandard[0][2])){echo $autoAttendantServicesStandard[0][2];}else{echo "false";}?>">
                			<input type="hidden" name="aaVideoServiceAssign" id="aaVideoServiceAssign" value="<?php // if(!empty($autoAttendantServicesVideo[0][2])){echo $autoAttendantServicesVideo[0][2];}else{echo "false";}?>">
                    	</div>		
					</div>-->
 
					<!--<div class="row">
    					<div class="col-md-6" id="businessHoursScheduleDisplay">
    						<div class="form-group">
    							<label class="labelText">Business Hours Schedule:</label>
								<div class="dropdown-wrap">
    							<select name="businessHoursSchedule" id="businessHoursSchedule"></select>
								</div>
    						</div>
    					</div>
    					<div class="col-md-6" id="holidayScheduleDisplay">
    						<div class="form-group">
        						<label class="labelText" for="holidaySchedule">Holiday Schedule:</label>
								<div class="dropdown-wrap">
        						<select name="holidaySchedule" id="holidaySchedule"></select>
								</div>
    						</div>	
    					</div>
    				</div> -->
					
					<!-- <div class="row">					
    					<div class="col-md-6" id="holidayScheduleDisplay">
    						<div class="form-group">
        						 <label class="labelText">Scope Of Extension Dialing:<span class="required">*</span></label><br/>
    							<?php /*
        							if($_SESSION["enterprise"] == "true"){
        								echo "<input type='radio' id='extensionDialingEnterprise' name='extensionDialing' value='Enterprise' checked />";
                                        echo "<label for='extensionDialingEnterprise'><span></span></label><label class='labelText'>Enterprise</label>";
        								echo "<input type='radio' name='extensionDialing' id= 'extensionDialingGroup' value ='Group'/>";
        								echo "<label for='extensionDialingGroup'><span></span></label><label class='labelText'>Group</label>";
        								echo "<input type='radio' name='extensionDialing' value ='Department' id='extensionDialingDepartment' />";
        								echo "<label for='extensionDialingDepartment'><span></span></label><label class='labelText'>Department</label>";
        							}else {
        							    echo "<input type='radio' name='extensionDialing' id= 'extensionDialingGroup' value ='Group' checked/>";
        							    echo "<label for='extensionDialingGroup'><span></span></label><label class='labelText'> Group</label>";
        							    
        							    echo "<input type='radio' name='extensionDialing' value ='Department' id='extensionDialingDepartment' />";
        							    echo "<label for='extensionDialingDepartment'><span></span></label><label class='labelText'>Department</label>";
        							}*/
    							?>
    						</div>	
    					</div>
    					<div class="col-md-6">
    						<div class="form-group">
        						<label class="labelText">Scope Of Name Dialing:<span class="required">*</span></label>
        						<br/>
        						<?php /*
        							if($_SESSION["enterprise"] == "true"){
        								echo "<input type='radio' name='nameDialing' value='Enterprise' checked id='nameDialingEnterprise'/>";
        								echo "<label for='nameDialingEnterprise'><span></span></label><label class='labelText'>Department</label>";
        								
        								echo "<input type='radio' name='nameDialing' value ='Group' id= 'nameDialingGroup'/>";
        								echo "<label for='nameDialingGroup'><span></span></label><label class='labelText'>Group</label>";
        								
        								echo "<input type='radio' name='nameDialing' value ='Department' id= 'nameDialingDepartment'/>";
        							    echo "<label for='nameDialingDepartment'><span></span></label><label class='labelText'>Department</label>";
        							
        							}else {
        							    echo "<input type='radio' name='nameDialing' value ='Group' id= 'nameDialingGroup' checked/>";
        							    echo "<label for='nameDialingGroup'><span></span></label><label class='labelText'>Group</label>";
        							    
        							    echo "<input type='radio' name='nameDialing' value ='Department' id= 'nameDialingDepartment'/>";
        							    echo "<label for='nameDialingDepartment'><span></span></label><label class='labelText'>Department</label>";
        							}*/
        						?>
    						</div>
    					</div>
					</div>	 -->			
<!--New code found-->
	<?php if($ociVersion == "21" || $ociVersion == "22"){?>
				<div class="row">
					<div class="col-md-6">
					<label class="labelText" for="department">Transfer to the operator after: (seconds of inactivity)</label>
					<input type="text" name="firstDigitTimeoutSeconds" id="firstDigitTimeoutSeconds" value="10" min="1" />

					</div>
	
				</div>
			<?php }?>
<!--New code found-->			
					<!-- <div class="row">
    					<div class="col-md-12">
    						<div class="form-group">
    			 				<label class="labelText">Name Dialing Entries:<span class="required">*</span></label><br/>
    							<input type = "radio" name="nameDialingEntries" id ="nameDialingEntries" checked value = "LastName + FirstName"/> 
    							<label for ="nameDialingEntries"><span></span></label><label class='labelText'>LastName + FirstName</label>
    							<input type = "radio" id ="nameDialingEntriess" name="nameDialingEntries" value = "LastName + FirstName or FirstName + LastName" />
    							<label for ="nameDialingEntriess"><span></span></label>
    							<label class='labelText'>LastName + FirstName and FirstName + LastName</label>
    						</div>
    					</div>
					</div> -->
				</div><!-- end userInfoData -->					
				
				
				<div id="after_hours_menu" class="userDataClass" style="display: none;">
					<h2 class="subBannerCustom">After Hours Menu</h2>
                	<div class="row">
                		<div class="col-md-6">
                			<div class="form-group">
            					<label class="labelText">Business Hours Greeting:</label>
            					<input type="radio" name="bhGreetingAfter" id="bhGreeting1"checked/><label for="bhGreeting1"><span></span></label> Default Greeting
            					<input type="radio" name="bhGreetingAfter" id="bhGreetingPAfter"/><label for="bhGreetingPAfter"><span></span></label> Personal Greeting
                			</div>
                		</div>
                		<div class="col-md-6">
                			<div class="form-group">
							<div class="dropdown-wrap">
                				<select class="pGreetingSelectAfter" name="pGreetingSelect" id="ahmGreetingSelect"></select>
							</div>
                			</div>
                		</div>
                	</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
                				<input type="checkbox" id="enableFLExtDialogAhMenu"/><label for="enableFLExtDialogAhMenu"><span></span></label>  Enable first-level extension dialing
                				<label class="labelText">Menu Options</label>
							</div>
						</div>
                		<div class="com-md-6">	
                			<div class="form-group">
                			</div>
                		</div>
					</div>
					<table class="scroll tablesorter aATable" style="width: 100%; margin: 0;">
                		<thead>
                		<th style="width:25%;">Key</th>
                		<th style="width:25%;">Action<span class="required">*</span></th>
                		<th style="width:25%;">Description</th>
                		<th style="width:25%;">Action Data</th>
                	  	</thead>
						<tbody> 
                			<tr> 
                				<td style='width:25%;'>0</td>
                				<td style='width:25%;'>	<div class="dropdown-wrap"><select name="ahData0" id="ahData0"></select></div></td>	
                				 <td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                			</tr>
                			<tr> 
                				<td style='width:25%;'>1</td>
                				<td style='width:25%;'>	<div class="dropdown-wrap"><select name="ahData1" id="ahData1"></select></div></td>	
                				 <td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                			</tr>
                			<tr> 
                				<td style='width:25%;'>2</td>
                				<td style='width:25%;'>	<div class="dropdown-wrap"><select name="ahData2" id="ahData2"></select></div></td>	
                				 <td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                			</tr>
                			<tr> 
                				<td style='width:25%;'>3</td>
                				<td style='width:25%;'><div class="dropdown-wrap"><select name="ahData3" id="ahData3"></select></div></td>	
                				 <td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                			</tr>
                			<tr> 
                				<td style='width:25%;'>4</td>
                				<td style='width:25%;'><div class="dropdown-wrap"><select name="ahData4" id="ahData4"></select></div></td>	
                				 <td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                			</tr>
                			<tr> 
                				<td style='width:25%;'>5</td>
                				<td style='width:25%;'><div class="dropdown-wrap"><select name="ahData5" id="ahData5"></select></div></td>	
                				 <td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                			</tr>
                			<tr> 
                				<td style='width:25%;'>6</td>
                				<td style='width:25%;'><div class="dropdown-wrap"><select name="ahData6" id="ahData6"></select></div></td>	
                				 <td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                			</tr>
                			<tr> 
                				<td style='width:25%;'>7</td>
                				<td style='width:25%;'><div class="dropdown-wrap"><select name="ahData7" id="ahData7"></select></div></td>	
                				 <td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                			</tr>
                			<tr> 
                				<td style='width:25%;'>8</td>
                				<td style='width:25%;'><div class="dropdown-wrap"><select name="ahData8" id="ahData8"></select></div></td>	
                				 <td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                			</tr>
                			<tr> 
                				<td style='width:25%;'>9</td>
                				<td style='width:25%;'><div class="dropdown-wrap"><select name="ahData9" id="ahData9"></select></div></td>	
                				 <td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                			</tr>
                			<tr> 
                				<td style='width:25%;'>*</td>
                				<td style='width:25%;'><div class="dropdown-wrap"><select name="ahData10" id="ahData10"></select></div></td>	
                				 <td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                			</tr>
                			<tr> 
                				<td style='width:25%;'>#</td>
                				<td style='width:25%;'><div class="dropdown-wrap"><select name="ahData11" id="ahData11"></select></div></td>	
                				 <td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
                				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
                			</tr>
        				</tbody> 
					</table>					
				</div>
<!-- Holiday Menu -->
				<div id="holiday_menu" class="userDataClass" style="display: none;">
    				<h2 class="subBannerCustom">Holiday Menu</h2>
    				<div class="row">
    					<div class="col-md-6" id="element2">
    						<div class="form-group">
    							<label class="labelText">Business Hours Greeting:</label>
    							<input type="radio" name="bhGreetingHoliday" id="bhGreeting2" checked/><label for="bhGreeting2"><span></span></label> Default Greeting
    							<input type="radio" name="bhGreetingHoliday" id="bhGreetingPHoliday"/><label for="bhGreetingPHoliday"><span></span></label> Personal Greeting
    						</div>
    					</div>
                    	<div class="col-md-6">
                    		<div class="form-group">
							<div class="dropdown-wrap">
                    			<select class="pGreetingSelectHoliday" name="pGreetingSelect" id="bhgGreetingSelect"></select>
								</div>
                    		</div>
                    	</div>
    				</div>
    				<div class="row">
    					<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText">Menu Options</label>
    							<input type="checkbox"id="enableFLExtDialogHdMenu"/><label for="enableFLExtDialogHdMenu"><span></span></label> Enable first-level extension dialing
    						</div>
    					</div>
    					<div class="col-md-6">
    						<div class="form-group">
    							
    						</div>
    					</div>
    				</div>
    				<table class="scroll tablesorter aATable" style="width: 100%; margin: 0;">
    				<thead>
                		<th style="width:25%;">Key</th>
                		<th style="width:25%;">Action<span class="required">*</span></th>
                		<th style="width:25%;">Description</th>
                		<th style="width:25%;">Action Data</th>
    	  			</thead>
    				<tbody> 
            			<tr> 
            				<td style='width:25%;'>0</td>
            				<td style='width:25%;'>	<div class="dropdown-wrap"><select name="hdData0" id="hdData0"></select></div></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            			</tr>
            			<tr> 
            				<td style='width:25%;'>1</td>
            				<td style='width:25%;'>	<div class="dropdown-wrap"><select name="hdData1" id="hdData1"></select></div></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            			</tr>
            			<tr> 
            				<td style='width:25%;'>2</td>
            				<td style='width:25%;'>	<div class="dropdown-wrap"><select name="hdData2" id="hdData2"></select></div></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            			</tr>
            			<tr> 
            				<td style='width:25%;'>3</td>
            				<td style='width:25%;'>	<div class="dropdown-wrap"><select name="hdData3" id="hdData3"></select></div></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            			</tr>
            			<tr> 
            				<td style='width:25%;'>4</td>
            				<td style='width:25%;'>	<div class="dropdown-wrap"><select name="hdData4" id="hdData4"></select></div></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            			</tr>
            			<tr> 
            				<td style='width:25%;'>5</td>
            				<td style='width:25%;'>	<div class="dropdown-wrap"><select name="hdData5" id="hdData5"></select></div></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            			</tr>
            			<tr> 
            				<td style='width:25%;'>6</td>
            				<td style='width:25%;'>	<div class="dropdown-wrap"><select name="hdData6" id="hdData6"></select></div></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            			</tr>
            			<tr> 
            				<td style='width:25%;'>7</td>
            				<td style='width:25%;'>	<div class="dropdown-wrap"><select name="hdData7" id="hdData7"></select></div></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            			</tr>
            			<tr> 
            				<td style='width:25%;'>8</td>
            				<td style='width:25%;'>	<div class="dropdown-wrap"><select name="hdData8" id="hdData8"></select></div></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30"/></td>
            				<td style='width:25%;'><input type="text" name="" id="" size="35" maxlength="30" placeholder="Phone Number"/></td>		
            			</tr>
            		</tbody> 
    			</table>
			</div>
			<div id="services_1" class="userDataClass" style="display: none;">
			<h2 class="subBannerCustom">Services</h2>
			<div class="row">
				<div class="col-md-4">
			 		<div class="form-group">
			 			<label class="labelText">Available Services</label>
						 <div class="dropdown-wrap">
			 			<select style="height: 250px;" name="" id="" multiple=""></select>
						 </div>
			 		</div>
				 </div>
				 <div class="col-md-4">
				 	<div class="form-group">
    			 		 <div class="row">
    			 		 	<div class="col-md-4">
    			 		 		<input type="button" value="Add >">
    			 		 	</div>
    			 		 	
    			 		 	<div class="col-md-4">
    			 		 		<input type="button"value="< Remove">
    			 		 	</div>
    			 		 	
    			 		 	<div class="col-md-4">
    			 		 		<input type="button" value="AddAll >>">
    			 		 	</div>
    			 		 	
    			 		 	<div class="col-md-4">
    			 		 		<input type="button"  value="<< Remove All">
    			 		 	</div>
    			 		 </div>
    			 	</div>
				 </div>
				 <div class="col-md-4">
				 	<div class="form-group">
				 		<label class="labelText">User Services</label>
				 		<select style="height: 250px;" name="" id="" multiple="">
				 		</select>
				 	</div>
				 </div>
			</div>
		</div>
		<div id="call_application_policy_1" class="userDataClass" style="display: none;">
			<h2 class="subBannerCustom">Call Application Policies</h2>
		</div>
		<div id="call_policy_1" class="userDataClass" style="display: none;">
			<h2 class="subBannerCustom">Call Policies</h2>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="labelText">Connected Line Identification Privacy on Redirected Calls:</label>
						<input type="radio" name="iprCalls" id="iprCalls1" checked /> <label for="iprCalls1"><span></span></label>No Privacy
						<input type="radio" name="iprCalls" id="iprCalls2"/><label for="iprCalls2"><span></span></label> Privacy For External Calls
						<input type="radio" name="iprCalls" id="iprCalls3"/><label for="iprCalls3"><span></span></label> Privacy For All Calls
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="labelText">Send Call Being Forwarded Response on Redirected Calls:</label>
						<input type="radio" name="frrCalls" id="frrCalls1"  checked /> <label for="frrCalls1"><span></span></label>Never
						<input type="radio" name="frrCalls" id="frrCalls2" /> <label for="frrCalls2"><span></span></label>Internal Calls
						<input type="radio" name="frrCalls" id="frrCalls3" /><label for="frrCalls3"><span></span></label> All Calls
					</div>
				</div>
			</div>
		 </div>
		 <div class="row">
    	
    			<div class="form-group alignBtn">
    				<input type="button" id="aaCancel" class="cancelButton marginRightButton" value="Cancel" style="">
    		
    		
    				<input type="button" id="subButton" class="marginRightButton" value="Confirm Settings">
    			
    			</div>
    	</div>
		 </form>
		</div>
    	
	

	<div id="AAData"></div>
        <div class="loading" style="display:none" id="aaInfoLoading"><img src="/Express/images/ajax-loader.gif"></div>

<div id="dialogAutoAttendant" class="dialogClass"></div>
