<?php
require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

// echo "<pre>"; print_r($_SESSION['aa']);

$where = array();

$annouceArray = array();
if (isset($_SESSION['aa']['bh']['announcementSelection']) && ! empty($_SESSION['aa']['bh']['announcementSelection'])) {
    if (isset($_SESSION["aa"]["bh"]["announcementId"]) || isset($_SESSION["aa"]["bh"]["vidAnnouncementId"])) {
        $annouceArray['bh']['announcementSelection'] = $_SESSION['aa']['bh']['announcementSelection'];
        $annouceArray['bh']['announcementId'] = $_SESSION["aa"]["bh"]["announcementId"];
        $annouceArray['bh']['vidAnnouncementId'] = $_SESSION["aa"]["bh"]["vidAnnouncementId"];
    } else {
        $annouceArray['bh']['announcementSelection'] = "Default";
        $annouceArray['bh']['announcementId'] = "";
        $annouceArray['bh']['vidAnnouncementId'] = "";
    }
}

if (isset($_SESSION['aa']['ah']['announcementSelection']) && ! empty($_SESSION['aa']['ah']['announcementSelection'])) {
    if (isset($_SESSION["aa"]["ah"]["announcementId"]) || isset($_SESSION["aa"]["ah"]["vidAnnouncementId"])) {
        $annouceArray['ah']['announcementSelection'] = $_SESSION['aa']['ah']['announcementSelection'];
        $annouceArray['ah']['announcementId'] = $_SESSION["aa"]["ah"]["announcementId"];
        $annouceArray['ah']['vidAnnouncementId'] = $_SESSION["aa"]["ah"]["vidAnnouncementId"];
    } else {
        $annouceArray['ah']['announcementSelection'] = "Default";
        $annouceArray['ah']['announcementId'] = "";
        $annouceArray['ah']['vidAnnouncementId'] = "";
    }
}

if (isset($_SESSION['aa']['h']['announcementSelection']) && ! empty($_SESSION['aa']['h']['announcementSelection'])) {
    if (isset($_SESSION["aa"]["h"]["announcementId"]) || isset($_SESSION["aa"]["h"]["vidAnnouncementId"])) {
        $annouceArray['h']['announcementSelection'] = $_SESSION['aa']['h']['announcementSelection'];
        $annouceArray['h']['announcementId'] = $_SESSION["aa"]["h"]["announcementId"];
        $annouceArray['h']['vidAnnouncementId'] = $_SESSION["aa"]["h"]["vidAnnouncementId"];
    } else {
        $annouceArray['h']['announcementSelection'] = "Default";
        $annouceArray['h']['announcementId'] = "";
        $annouceArray['h']['vidAnnouncementId']= "";
    }
}

// print_r($_SESSION['aa']['vp']['announcementId']);
if (! empty($_SESSION['aa']['vp']['announcementId'])) {
    $annouceArray['vp']['announcementId'] = $_SESSION['aa']['vp']['announcementId'];
} else {
    $annouceArray['vp']['announcementId'] = "";
}

if (isset($_POST["aaId"]) && ! empty($_POST["aaId"])) {
    //$annouceArray['audioVideoArr'] = $_SESSION["aa"]["greetingsDetails"];
    $annouceArray['submenu'] = $_SESSION["aa"]["submenuPersonalGreetings"];
    //$annouceArray['submenuVidPersonalGreetings'] = $_SESSION["aa"]["submenuPersonalVideoGreetings"];
}

echo json_encode($annouceArray);
?>