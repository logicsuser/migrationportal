<?php

    require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();
	require_once ("/var/www/html/Express/config.php");
	require_once("/var/www/lib/broadsoft/adminPortal/GroupCallPickupUsers.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
	require_once("/var/www/lib/broadsoft/adminPortal/customTagManagement.php");
    require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailOperations.php");
    require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
    
    //Code added @ 21 Dec 2018
    require_once("/var/www/lib/broadsoft/adminPortal/services/Services.php");    
    //End code
    const customProfileTable = "customProfiles";
    const customProfileName  = "customProfileName";
    const customTagName      = "customTagName";
    const customTagValue     = "customTagValue";
    const customProfileType  = "deviceType";

    const ExpressCustomProfileName = "%Express_Custom_Profile_Name%";
    const ExpressPhoneProfileName = "%phone-template%";
    const ExpressTagBundleName = "%Express_Tag_Bundle%";

    include("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig.php");

//     global $userAddModuleStatus;
    
    /*Start New Code*/
    function getLinePortDevTypeLookup() {
        global $db;
        $deviceSubList = array();
        //Code added @ 16 July 2019
        $whereCndtn = "";
        if($_SESSION['cluster_support']){
            $selectedCluster = $_SESSION['selectedCluster'];
            $whereCndtn = " WHERE clusterName ='$selectedCluster' ";
        }
        //End code
        $qryLPLU = "select * from systemDevices $whereCndtn ";
        $select_stmt = $db->prepare($qryLPLU);
        $select_stmt->execute();
        $i = 0;
        while($rs = $select_stmt->fetch())
        {
            $deviceSubList[$i]['deviceType'] = $rs['deviceType'];
            $deviceSubList[$i]['substitution'] = $rs['nameSubstitution'];
            $i++;
        }
        return $deviceSubList;
    }
    
    function replaceDevNameWithSubstitute($deviceName) {
        $newDeviceName = $deviceName;
        $subList = getLinePortDevTypeLookup();
        foreach($subList as $key => $value) {
            if(strpos($deviceName, $value['deviceType']) !== false) {
                $newDeviceName = str_replace($value['deviceType'], $value['substitution'], $deviceName);
            }
        }
        return $newDeviceName;
    }
    /*End New Code*/
    
    function debugAlert($var) {
        echo '<script type="text/javascript">';
        echo 'alert("'.$var.'")';
        echo '</script>';
    }
   function addDeviceCustomTag($deviceName, $tagName, $tagValue) {
        global $sessionid, $client;

        $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagAddRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= "<tagName>" . $tagName . "</tagName>";
        $xmlinput .= "<tagValue>" . $tagValue . "</tagValue>";

        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $errMsg = "";
        if (readError($xml) != "") {
            $errMsg = "Failed to add custom tag to device " . $deviceName . " .";
            $errMsg .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
            if ($xml->command->detail)
            {
                $errMsg .= "%0D%0A" . strval($xml->command->detail);
            }
        }
        return $errMsg;
    }


    function addDeviceCustomTags($deviceType, $deviceName, $customProfile, $deviceMgtTagBundle, $ociVersion) {
        global $db;
        
        require_once("/var/www/html/Express/userAdd/UserDBOperations.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
        $syslevelObj = new sysLevelDeviceOperations();
        $setName = $syslevelObj->getTagSetNameByDeviceType($deviceType, $ociVersion);
        $systemDefaultTagVal = "";
        if($setName["Success"] != "" && !empty($setName["Success"])){
            $defaultSettags = $syslevelObj->getDefaultTagListByTagSet($setName["Success"]);
            $phoneTemplateName = "%phone-template%";
            if(array_key_exists($phoneTemplateName, $defaultSettags["Success"])){
                $systemDefaultTagVal = $defaultSettags["Success"]["%phone-template%"][0];
            }
        }
        // No custom tags can be selected while building a user
        // ----------------------------------------------------
//         if ($customProfile == "None") { return ""; }

        
        // and add them to the device
        // -----------------------------------------------------------
        
        // add custom tag with the custom profile name for the reporting purpose
        if($customProfile != "None" && !empty($customProfile)) {
            
            if($systemDefaultTagVal != $customProfile){
                if (($errMsg = addDeviceCustomTag($deviceName, ExpressPhoneProfileName, $customProfile)) != "") {
                    return $errMsg;
                }
                
            }
            if (($errMsg = addDeviceCustomTag($deviceName, ExpressCustomProfileName, $customProfile)) != "") {
                return $errMsg;
            }
            
        }
   
        
        // get custom tags from database for a specific tag bundle.
        if(!empty($deviceMgtTagBundle) && $deviceMgtTagBundle !="None") {
            $tagBundleArr = json_decode($deviceMgtTagBundle);
            if (($errMsg = addDeviceCustomTag($deviceName, ExpressTagBundleName, implode(";", $tagBundleArr))) != "") {
                return $errMsg;
            }
            $uMDBO = new UserDBOperations;
            $customTagArray = $uMDBO->getAllCustomTagsFromTagBundles($tagBundleArr);
            if(!empty($customTagArray)) {
                //             $deviceMgtTagBundle = json_decode($deviceMgtTagBundle);
                foreach($customTagArray as $key => $val) {
                    if (($errMsg = addDeviceCustomTag($deviceName, $key, $val)) != "") {
                        return $errMsg;
                    }
                }
                
            }
        }
        
        
        
        
//         $query = "select " . customTagName . ", " . customTagValue . " from " . customProfileTable . " where " . customProfileType . "='" . $deviceType . "' and " . customProfileName . "='" . $customProfile . "'";
//         $results = $db->query($query);

//         while ($row = $results->fetch()) {
//             if (($errMsg = addDeviceCustomTag($deviceName, $row[customTagName], $row[customTagValue])) != "") {
//                 return $errMsg;
//             }
//         }

        return "";
    }


    function getResourceForUserIdCreation($attr) {

        switch ($attr) {
            case ResourceNameBuilder::DN:               return $_POST["phoneNumber"] != "" ? $_POST["phoneNumber"] : "";
            case ResourceNameBuilder::EXT:              return $_POST["extension"];
            case ResourceNameBuilder::USR_CLID:         return $_POST["callerId"];
            case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:           return str_replace(" ", "_", trim($_SESSION["groupId"]));
            case ResourceNameBuilder::SYS_DFLT_DOMAIN:  return $_SESSION["systemDefaultDomain"];
            case ResourceNameBuilder::DEFAULT_DOMAIN:   return $_SESSION["defaultDomain"];
            case ResourceNameBuilder::GRP_PROXY_DOMAIN: return $_SESSION["proxyDomain"];
            case ResourceNameBuilder::FIRST_NAME:       return $_POST["firstName"];
            case ResourceNameBuilder::LAST_NAME:        return $_POST["lastName"];
            case ResourceNameBuilder::DEV_NAME:         return isset($_SESSION["devName"]) && $_SESSION["devName"] != "" ? $_SESSION["devName"] : "";
            case ResourceNameBuilder::MAC_ADDR:         return isset($_SESSION["macAddress"]) && $_SESSION["macAddress"] != "" ? $_SESSION["macAddress"] : "";
            case ResourceNameBuilder::ENT:              return strtolower(trim($_SESSION["sp"]));
            case ResourceNameBuilder::USR_ID:
                $name = isset($_SESSION["userId"]) ? $_SESSION["userId"] : "";
                if ($name == "") {
                    return "";
                }
                $nameSplit = explode("@", $name);
                return $nameSplit[0];
                break;
            default:                                    return "";
        }
    }

    function getResourceForSurgeMailNameCreation($attr) {
        global $surgemailDomain;

        switch ($attr) {
            case ResourceNameBuilder::DN:               return isset($_SESSION["complete"][$_SESSION["userAddNames"]["phoneNumber"]]) && $_SESSION["complete"][$_SESSION["userAddNames"]["phoneNumber"]] != "" ? $_SESSION["complete"][$_SESSION["userAddNames"]["phoneNumber"]] : "";
            case ResourceNameBuilder::EXT:              return isset($_SESSION["complete"][$_SESSION["userAddNames"]["extension"]]) && $_SESSION["complete"][$_SESSION["userAddNames"]["extension"]] != "" ? $_SESSION["complete"][$_SESSION["userAddNames"]["extension"]] : "";
            case ResourceNameBuilder::USR_CLID:         return isset($_SESSION["complete"][$_SESSION["userAddNames"]["callerId"]]) && $_SESSION["complete"][$_SESSION["userAddNames"]["callerId"]] != "" ? $_SESSION["complete"][$_SESSION["userAddNames"]["callerId"]] : "";
            case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:           return str_replace(" ", "_", trim($_SESSION["groupId"]));
            case ResourceNameBuilder::SYS_DFLT_DOMAIN:  return $_SESSION["systemDefaultDomain"];
            case ResourceNameBuilder::DEFAULT_DOMAIN:   return $_SESSION["defaultDomain"];
            case ResourceNameBuilder::SRG_DOMAIN:       return $surgemailDomain;
            case ResourceNameBuilder::FIRST_NAME:       return $_SESSION["complete"][$_SESSION["userAddNames"]["firstName"]];
            case ResourceNameBuilder::LAST_NAME:        return $_SESSION["complete"][$_SESSION["userAddNames"]["lastName"]];
            case ResourceNameBuilder::DEV_NAME:         return isset($_SESSION["devName"]) && $_SESSION["devName"] != "" ? $_SESSION["devName"] : "";
            case ResourceNameBuilder::MAC_ADDR:         return isset($_SESSION["macAddress"]) && $_SESSION["macAddress"] != "" ? $_SESSION["macAddress"] : "";
            case ResourceNameBuilder::USR_ID:
                $name = isset($_SESSION["userId"]) ? $_SESSION["userId"] : "";
                if ($name == "") {
                    return "";
                }
                $nameSplit = explode("@", $name);
                return $nameSplit[0];
                break;
            default: return "";
        }
    }


    function getResourceForDeviceNameCreation($attr) {
        switch ($attr) {
            case ResourceNameBuilder::DN:               return $_POST["phoneNumber"] != "" ? $_POST["phoneNumber"] : "";
            case ResourceNameBuilder::EXT:              return $_POST["extension"];
            case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:           return $_SESSION["groupId"];
            case ResourceNameBuilder::FIRST_NAME:       return $_POST["firstName"];
            case ResourceNameBuilder::LAST_NAME:        return $_POST["lastName"];
            case ResourceNameBuilder::DEV_TYPE:         return $_POST["deviceType"];
            case ResourceNameBuilder::MAC_ADDR:         return $_POST["macAddress"];
            case ResourceNameBuilder::INT_1:
            case ResourceNameBuilder::INT_2:            return $_POST["deviceIndex"];
            default: return "";
        }
    }

    function getResourceForVoicePasswordCreation($attr) {
        switch ($attr) {
            case ResourceNameBuilder::DN:       return isset($_SESSION["complete"][$_SESSION["userAddNames"]["phoneNumber"]]) && $_SESSION["complete"][$_SESSION["userAddNames"]["phoneNumber"]] != "" ? $_SESSION["complete"][$_SESSION["userAddNames"]["phoneNumber"]] : "";
                //return $_POST["phoneNumber"] != "" ? $_POST["phoneNumber"] : "";
            case ResourceNameBuilder::EXT:      return isset($_SESSION["complete"][$_SESSION["userAddNames"]["extension"]]) && $_SESSION["complete"][$_SESSION["userAddNames"]["extension"]] != "" ? $_SESSION["complete"][$_SESSION["userAddNames"]["extension"]] : "";
                //return $_POST["extension"];
            case ResourceNameBuilder::USR_CLID: return isset($_SESSION["complete"][$_SESSION["userAddNames"]["callerId"]]) && $_SESSION["complete"][$_SESSION["userAddNames"]["callerId"]] != "" ? $_SESSION["complete"][$_SESSION["userAddNames"]["callerId"]] : "";
            case ResourceNameBuilder::GRP_DN:   return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:   return $_SESSION["groupId"];
            case ResourceNameBuilder::MAC_ADDR: return isset($_SESSION["macAddress"]) && $_SESSION["macAddress"] != "" ? $_SESSION["macAddress"] : "";
        }
    }

	$phoneType = isset($_POST["deviceType"]) ? $_POST["deviceType"] : "";
	if (strstr($phoneType, " "))
	{
		$substrMenu = explode(" ", $phoneType);
	}
	else if (strstr($phoneType, "_"))
	{
		$substrMenu = explode("_", $phoneType);
	}
	else
	{
		$substrMenu = explode("-", $phoneType);
	}
	$substrMenu = $substrMenu[0];

	$data = "Done";
        $userType = "";
               

	if ($_POST["module"] == "device")
	{
	    server_fail_over_debuggin_testing();      /* Only for testing purpose. */
	    
	    $_SESSION["userAddModuleStatus"]["module"][] = $_POST["module"];
	    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Success";
	    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["deviceType"] = $phoneType;
	    
        require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
        require_once("/var/www/lib/broadsoft/adminPortal/DeviceNameBuilder.php");

        // Build Device Name.
        // ----------------------------------------------------------------------------------------
        // Choose formula based on device type: DID or analog.
        // For Analog type always enforce fallback
        // For DID type, enforce fallback if there is no second criteria/formula.

        $forceResolve = $_POST["userType"] == "analog" ? true : $deviceNameDID2 == "";

        $deviceNameBuilder = new DeviceNameBuilder($_POST["userType"] == "analog" ? $deviceNameAnalog : $deviceNameDID1,
            getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
            getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
            getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
            $forceResolve);

        if (! $deviceNameBuilder->validate()) {
            // TODO: Implement resolution when Device name input formula has errors
        }

        do {
            $attr = $deviceNameBuilder->getRequiredAttributeKey();
            $deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
        } while ($attr != "");

        $_SESSION["devName"] = $deviceNameBuilder->getName();

        if ($_SESSION["devName"] == "") {
            // Create Device Name builder from the second formula (DID type only).
            // At this point we know that there is second formula, because without second formula
            // name would be forcefully resolved in the first formula.

            $deviceNameBuilder = new DeviceNameBuilder($deviceNameDID2,
                getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
                getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
                getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
                true);

            if (! $deviceNameBuilder->validate()) {
                // TODO: Implement resolution when Device name input formula has errors
            }

            do {
                $attr = $deviceNameBuilder->getRequiredAttributeKey();
                $deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
            } while ($attr != "");

            $_SESSION["devName"] = $deviceNameBuilder->getName();
// $data .= " Second Resolve:" . $_SESSION["devName"];
        }
        if($_POST["userType"] != "analog") {
            $_SESSION["devName"] = replaceDevNameWithSubstitute($_SESSION["devName"]);
        }
//         print_r($_SESSION["devName"]); echo"UUUUUUUUUUUU"; exit;
        $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["devName"] = $_SESSION["devName"];
        // Build Device Access User Name - fallback definitions are based on Device Name fallback
        // ----------------------------------------------------------------------------------------
        // Enforce fallback if there is no second criteria/formula.

        $deviceAccessUserNameBuilder = new DeviceNameBuilder($deviceAccessUserName1,
            getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
            getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
            getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
            $deviceAccessUserName2 == "");    // force fallback only if second criteria does not exist

        if (! $deviceAccessUserNameBuilder->validate()) {
            // TODO: Implement resolution when Device name input formula has errors
        }

        do {
            $attr = $deviceAccessUserNameBuilder->getRequiredAttributeKey();
            $deviceAccessUserNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
        } while ($attr != "");

        $_SESSION["deviceAccessUserName"] = $deviceAccessUserNameBuilder->getName();

        if ($_SESSION["deviceAccessUserName"] == "") {
            $deviceAccessUserNameBuilder = new DeviceNameBuilder($deviceAccessUserName2,
                getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
                getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
                getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
                true);

            if (! $deviceAccessUserNameBuilder->validate()) {
                // TODO: Implement resolution when Device name input formula has errors
            }

            do {
                $attr = $deviceAccessUserNameBuilder->getRequiredAttributeKey();
                $deviceAccessUserNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
            } while ($attr != "");

            $_SESSION["deviceAccessUserName"] = $deviceAccessUserNameBuilder->getName();
        }

        // Build Device Access Password - fallback definitions are based on Device Name fallback
        // ----------------------------------------------------------------------------------------
        $deviceAccessPasswordBuilder = new DeviceNameBuilder($deviceAccessPassword,
            getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
            getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
            getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
            true);

        if (! $deviceAccessPasswordBuilder->validate()) {
            // TODO: Implement resolution when Device name input formula has errors
        }

        do {
            $attr = $deviceAccessPasswordBuilder->getRequiredAttributeKey();
            $deviceAccessPasswordBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
        } while ($attr != "");

        $_SESSION["deviceAccessPassword"] = $deviceAccessPasswordBuilder->getName();

        $_SESSION["userType"] = $_POST["userType"];
        
        $executeAddDeviceRequest = false;
        if ( ($_SESSION["userType"] == "analog" && isset($_POST["portNumber"]) ) ||
            ( $_SESSION["userType"] == "digital" && isset($_POST["portNumberSIP"]) )
        ) {
            require_once("/var/www/lib/broadsoft/adminPortal/getAllDevices.php");
            $executeAddDeviceRequest = true;
        }

        $_SESSION["deviceIndex"] = $_POST["deviceIndex"];
        $_SESSION["portNumber"] = "";
        if( $_SESSION["userType"] == "analog" && $_POST["portNumber"] !="") {
            $_SESSION["portNumber"] = $_POST["portNumber"];
        } else if( $_SESSION["userType"] != "analog" && $_POST["portNumberSIP"] != "" ) {
            $_SESSION["portNumber"] = $_POST["portNumberSIP"];
        }
        $_SESSION["proxyDomain"] = $_POST["proxyDomain"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["deviceName"]] = $_SESSION["devName"];

        // Add a device
        // For analog users check whether a device (analog gateway) has already been provisioned
		if ($executeAddDeviceRequest && ! in_array($_SESSION["devName"], $devices)) {

            $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceAddRequest14");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
            $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
            $xmlinput .= "<deviceName>" . $_SESSION["devName"] . "</deviceName>";
            $xmlinput .= "<deviceType>" . $phoneType . "</deviceType>";
            if ($_POST["macAddress"] !== "")
            {
                $_SESSION["macAddress"] = $_POST["macAddress"];
                $xmlinput .= "<macAddress>" . $_POST["macAddress"] . "</macAddress>";
            }
            $xmlinput .= xmlFooter();
            
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

            if (count($xml->command->attributes()) > 0)
            {
                foreach ($xml->command->attributes() as $a => $b)
                {
                    if ($b == "Error" || $b == "Warning")
                    {
                        $data = $_SESSION["devName"] . " was attempted to be built but failed.";
                        $data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
                        if ($xml->command->detail)
                        {
                            $data .= "%0D%0A" . strval($xml->command->detail);
                        }
                        
                        $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Error";
                        $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["ErrorMsg"] = $data;
                    }
                }
            }

            // Add Device Custom Tags (user level)
//$data .= "CustomProfile" . $_POST["customProfile"];
            
            $resultMsg = addDeviceCustomTags($phoneType, $_SESSION["devName"], $_POST["customProfile"], $_POST["deviceMgtTagBundle"], $ociVersion);
            if ($resultMsg != "") {
                $data = $resultMsg;
            }

            
            // Rebuild and reset device after custom tags are added
            if ($_POST["customProfile"] != "None") {
                rebuildAndResetDeviceConfig($_SESSION["devName"], "rebuildAndReset");
            }
        }

		$_SESSION["complete"][$_SESSION["userAddNames"]["deviceType"]] = $phoneType;
		$_SESSION["complete"][$_SESSION["userAddNames"]["macAddress"]] = $_POST["macAddress"];
		echo $data;
	}

	if ($_POST["module"] == "addUser")
	{
	    $_SESSION["userAddModuleStatus"]["module"][] = $_POST["module"];
	    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Success";
	    
        if ($_POST["hasDeviceName"] == "N")
		{
			if (isset($_SESSION["devName"]))
			{
				unset($_SESSION["devName"]);
			}
		}

		$password = setWebPassword((int)$_SESSION["groupRules"]["minPasswordLength"]);

        //retrieve default domain
        $defaultDomain = $_SESSION["defaultDomain"];

        require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
        require_once("/var/www/lib/broadsoft/adminPortal/UserIdNameBuilder.php");
        require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailNameBuilder.php");
        require_once("/var/www/lib/broadsoft/adminPortal/LineportNameBuilder.php");


        // Build User ID.
        // ------------------------------------------------------------------------------------------------------

        // Create user ID builder from the first formula.
        // Enforce fallback if there is no second criteria/formula.
        // --------------------------------------------------------
        $userName = "";
        $userIdBuilder1 = new UserIdNameBuilder($userIdCriteria1,
            getResourceForUserIdCreation(ResourceNameBuilder::DN),
            getResourceForUserIdCreation(ResourceNameBuilder::EXT),
            getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID),
            getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN),
            getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
            getResourceForUserIdCreation(ResourceNameBuilder::ENT),             
            $userIdCriteria2 == "");    // force fallback only if second criteria does not exist

        if (! $userIdBuilder1->validate()) {
            // TODO: Implement resolution when User ID input formula has errors
        }

        do {
            
            $attr = $userIdBuilder1->getRequiredAttributeKey();
            $userIdBuilder1->setRequiredAttribute($attr, getResourceForUserIdCreation($attr));
        } while ($attr != "");

        $userName = $userIdBuilder1->getName();
        $userIdWithoutDomain = $userIdBuilder1->getNameWithoutDomain();
        if ($userName == "") {
            // Create user ID builder from the second formula. At this point we know that there is second formula,
            // because without second formula name would be forcefully resolved in the first formula.

            $userIdBuilder2 = new UserIdNameBuilder($userIdCriteria2,
                getResourceForUserIdCreation(ResourceNameBuilder::DN),
                getResourceForUserIdCreation(ResourceNameBuilder::EXT),
                getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID),
                getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN),
                getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
                getResourceForUserIdCreation(ResourceNameBuilder::ENT),
                true);

            if (! $userIdBuilder2->validate()) {
                // TODO: Implement resolution when User ID input formula has errors
            }

            do {
                $attr = $userIdBuilder2->getRequiredAttributeKey();
                $userIdBuilder2->setRequiredAttribute($attr, getResourceForUserIdCreation($attr));
            } while ($attr != "");

            $userName = $userIdBuilder2->getName();
            $userIdWithoutDomain = $userIdBuilder2->getNameWithoutDomain();
        }
        
        $_SESSION["userId"] = $userName;
        //die();
        $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["userId"] = $_SESSION["userId"];

        // Build Line Port.
        // ------------------------------------------------------------------------------------------------------

        // Create lineport name from the first formula.
        // Enforce fallback if there is no second criteria/formula.
        // --------------------------------------------------------
        if (isset($_SESSION["devName"])) {
            $linePort = "";
            $linePortBuilder1 = new LineportNameBuilder($linePortCriteria1,
                getResourceForUserIdCreation(ResourceNameBuilder::DN),
                getResourceForUserIdCreation(ResourceNameBuilder::EXT),
                getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID),
                getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN),
                getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
                $linePortCriteria2 == "");  // force fallback only if second criteria does not exist

            if (! $linePortBuilder1->validate()) {
                // TODO: Implement resolution when lineport input formula has errors
            }

            do {
                $attr = $linePortBuilder1->getRequiredAttributeKey();
                $linePortBuilder1->setRequiredAttribute($attr, getResourceForUserIdCreation($attr));
            } while ($attr != "");

            $linePort = $linePortBuilder1->getName();

            if ($linePort == "") {
                // Create lineport name builder from the second formula. At this point we know that there is second
                // formula, because without second formula name would be forcefully resolved in the first formula.

                $linePortBuilder2 = new LineportNameBuilder($linePortCriteria2,
                    getResourceForUserIdCreation(ResourceNameBuilder::DN),
                    getResourceForUserIdCreation(ResourceNameBuilder::EXT),
                    getResourceForUserIdCreation(ResourceNameBuilder::USR_CLID),
                    getResourceForUserIdCreation(ResourceNameBuilder::GRP_DN),
                    getResourceForUserIdCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
                    true);

                if (! $linePortBuilder2->validate()) {
                    // TODO: Implement resolution when User ID input formula has errors
                }

                do {
                    $attr = $linePortBuilder2->getRequiredAttributeKey();
                    $linePortBuilder2->setRequiredAttribute($attr, getResourceForUserIdCreation($attr));
                } while ($attr != "");

                $linePort = $linePortBuilder2->getName();
            }
            
            $linePort = str_replace(" ", "_", $linePort);
            
            if($_POST["proxyDomain"] != ""){
                $lineArr = explode("@",$linePort);
                $selectedDomain = $lineArr[1];
                if($selectedDomain != $_POST["proxyDomain"]){
                    $linePort = $lineArr[0]."@".$_POST["proxyDomain"];
                }
            }
            
            $_SESSION["linePort"] = str_replace(" ", "_", $linePort);
        }

        // for BLF names use userID without defaultDomain portion
        $_SESSION["blfUser"] = $userIdWithoutDomain . "-blf@" . $defaultDomain;
		//sleep(2);

		$xmlinput = xmlHeader($sessionid, "UserAddRequest17sp4");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<userId>" . $_SESSION["userId"] . "</userId>";
		$xmlinput .= "<lastName>" . $_POST["lastName"] . "</lastName>";
		$xmlinput .= "<firstName>" . $_POST["firstName"] . "</firstName>";
		$xmlinput .= "<callingLineIdLastName>" . $_POST["callingLineIdLastName"] . "</callingLineIdLastName>";
		$xmlinput .= "<callingLineIdFirstName>" . $_POST["callingLineIdFirstName"] . "</callingLineIdFirstName>";
		//Code added @ 23 Oct 2018
                if($ociVersion=="20" || $ociVersion=="21" || $ociVersion=="22"){
                    if($_POST["nameDialingFirstName"] != "" && $_POST["nameDialingLastName"]!=""){                 
                            $xmlinput .= "<nameDialingName>";
                            $xmlinput .= "<nameDialingLastName>" . trim($_POST["nameDialingLastName"]) . "</nameDialingLastName>";
                            $xmlinput .= "<nameDialingFirstName>" . trim($_POST["nameDialingFirstName"]) . "</nameDialingFirstName>";
                            $xmlinput .= "</nameDialingName>";                            
                    }
                }
                
                if ($_POST["phoneNumber"] !== "")
		{
			$xmlinput .= "<phoneNumber>" . $_POST["phoneNumber"] . "</phoneNumber>";
		}
		$xmlinput .= "<extension>" . $_POST["extension"] . "</extension>";
		if ($_POST["callerId"] !== "")
		{
			$xmlinput .= "<callingLineIdPhoneNumber>" . $_POST["callerId"] . "</callingLineIdPhoneNumber>";
		}
		$xmlinput .= "<password>" . $password . "</password>";
		if ($_POST["dept"] !== "")
		{
			$xmlinput .= "<department xsi:type=\"GroupDepartmentKey\">
						<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>
						<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>
						<name>" . $_POST["dept"] . "</name>
					</department>";
		}
		if ($_POST["language"] !== "")
		{
			$xmlinput .= "<language>" . $_POST["language"] . "</language>";
		}
		$xmlinput .= "<timeZone>" . $_POST["timeZone"] . "</timeZone>";
		if (isset($_SESSION["devName"]))
		{
            $xmlinput .= "<accessDeviceEndpoint>";
            $xmlinput .=    "<accessDevice>";
            $xmlinput .=        "<deviceLevel>Group</deviceLevel>";
            $xmlinput .=        "<deviceName>" . $_SESSION["devName"] . "</deviceName>";
            $xmlinput .=    "</accessDevice>";
            $xmlinput .=    "<linePort>" . $_SESSION["linePort"] . "</linePort>";
            if ($_SESSION["portNumber"] != "") {
                $xmlinput .= "<portNumber>" . $_SESSION["portNumber"] . "</portNumber>";
            }
            $xmlinput .= "</accessDeviceEndpoint>";
		}
		if (strlen($_POST["email"]) > 0)
		{
			$xmlinput .= "<emailAddress>" . $_POST["email"] . "</emailAddress>";
		}
                //Code added @ 02 May 2018
                if (strlen($_POST["lcationVal"]) > 0)
		{
			$xmlinput .= "<addressLocation>" . $_POST["lcationVal"] . "</addressLocation>";
		}
                //End Code
		if (strlen($_POST["address"]) > 0 or strlen($_POST["suite"]) > 0 or strlen($_POST["city"]) > 0 or strlen($_POST["state"]) > 0 or strlen($_POST["zip"]) > 0)
		{
			$xmlinput .= "<address>";
			if (strlen($_POST["address"]) > 0)
			{
				$xmlinput .= "<addressLine1>" . $_POST["address"] . "</addressLine1>";
			}
			if (strlen($_POST["suite"]) > 0)
			{
				$xmlinput .= "<addressLine2>" . $_POST["suite"] . "</addressLine2>";
			}
			if (strlen($_POST["city"]) > 0)
			{
				$xmlinput .= "<city>" . $_POST["city"] . "</city>";
			}
			if (strlen($_POST["state"]) > 0)
			{
				$xmlinput .= "<stateOrProvince>" . $_POST["state"] . "</stateOrProvince>";
			}
			if (strlen($_POST["zip"]) > 0)
			{
				$xmlinput .= "<zipOrPostalCode>" . $_POST["zip"] . "</zipOrPostalCode>";
			}
			$xmlinput .= "</address>";
			if (strlen($_POST["networkClassOfService"]) > 0) {
			    $xmlinput .= "<networkClassOfService>" . $_POST["networkClassOfService"] . "</networkClassOfService>";
            }
		}
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		if (count($xml->command->attributes()) > 0)
		{
			foreach ($xml->command->attributes() as $a => $b)
			{
			    if ($b == "Error" || $b == "Warning")
				{
					$data = $_SESSION["userId"] . " was attempted to be built but failed.";
					$data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
					if ($xml->command->detail)
					{
						$data .= "%0D%0A" . strval($xml->command->detail);
                    }
                    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Error";
                    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["ErrorMsg"] = $data;
				}else if($data == "Done") {
                	if ($_POST["phoneNumber"] !== "" && (isset($_POST["activateNumber"]) && $_POST["activateNumber"] == "true"))
					{
							$dnPostArray['phoneNumber'][0] = $_POST["phoneNumber"];
							$dnPostArray['groupId'] = $_SESSION["groupId"];
							$dnsObj = new Dns();
							$dnsActiveResponse = $dnsObj->activateDNRequest($dnPostArray);
							
					} else if ($_POST["phoneNumber"] !== "" && (isset($_POST["activateNumber"]) && $_POST["activateNumber"] == "false")) {
					    $dnPostArray['phoneNumber'][0] = $_POST["phoneNumber"];
					    $dnPostArray['groupId'] = $_SESSION["groupId"];
					    $dnsObj = new Dns();
					    $dnsActiveResponse = $dnsObj->deActivateDNRequest($dnPostArray);
					}
				}

			}
		}

		//set call processing policy
		$xmlinput = xmlHeader($sessionid, "UserCallProcessingModifyPolicyRequest14sp7");
		$xmlinput .= "<userId>" . $_SESSION["userId"] . "</userId>";
		$xmlinput .= "<useUserCLIDSetting>" . ($_POST["callingLineIdPolicy"] == "User" ? "true" : "false") . "</useUserCLIDSetting>";
		$xmlinput .= "<clidPolicy>" . $policiesClidPolicy . "</clidPolicy>";
		$xmlinput .= "<emergencyClidPolicy>" . $policiesClidPolicy . "</emergencyClidPolicy>";
		$xmlinput .= "<useGroupName>true</useGroupName>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		// Modify user's password, so the user does not have to reset it on first time logon
		if ($policiesUsrAuthFirstLoginPasswordChange != "true") {
			$password = setWebPassword((int)$_SESSION["groupRules"]["minPasswordLength"]);
			//sleep(2);
			$xmlinput = xmlHeader($sessionid, "UserModifyRequest17sp4");
			$xmlinput .= "<userId>" . $_SESSION["userId"] . "</userId>";
			$xmlinput .= "<newPassword>" . $password . "</newPassword>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		}
		$_SESSION["complete"][$_SESSION["userAddNames"]["firstName"]] = $_POST["firstName"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["lastName"]] = $_POST["lastName"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["callingLineIdFirstName"]] = $_POST["callingLineIdFirstName"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["callingLineIdLastName"]] = $_POST["callingLineIdLastName"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["email"]] = $_POST["email"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["phoneNumber"]] = $_POST["phoneNumber"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["extension"]] = $_POST["extension"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["callerId"]] = $_POST["callerId"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["address"]] = $_POST["address"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["suite"]] = $_POST["suite"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["city"]] = $_POST["city"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["state"]] = $_POST["state"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["zip"]] = $_POST["zip"];
		$_SESSION["complete"][$_SESSION["userAddNames"]["timeZone"]] = $_POST["timeZone"];
		
                $_SESSION["complete"][$_SESSION["userAddNames"]["networkClassOfService"]] = $_POST["networkClassOfService"] != "" ? $_POST["networkClassOfService"] : "None";
		$_SESSION["complete"][$_SESSION["userAddNames"]["web"]] = $password;
		$_SESSION["complete"][$_SESSION["userAddNames"]["dept"]] = $_POST["dept"];
                
                $nameDialingFirstName = "";
                $nameDialingLastName  = "";
                if($ociVersion=="20" || $ociVersion=="21" || $ociVersion=="22"){                
                        if ($_POST["nameDialingFirstName"] !== "" || $_POST["nameDialingLastName"]!=""){
                                $nameDialingFirstName = trim($_POST["nameDialingFirstName"]);
                                $nameDialingLastName  = trim($_POST["nameDialingLastName"]);
                        }
                }
                $_SESSION["complete"][$_SESSION["userAddNames"]["nameDialingFirstName"]] = $nameDialingFirstName;
                $_SESSION["complete"][$_SESSION["userAddNames"]["nameDialingLastName"]]  = $nameDialingLastName;
                
                if(isset($_POST["sharedCallAppearance"]) && $_POST["sharedCallAppearance"]=="Yes"){
                    $_SESSION["complete"][$_SESSION["userAddNames"]["sharedCallAppearance"]] = $_POST["sharedCallAppearance"];
                }
		
		if(isset($showActivateMessage)){
		    $_SESSION["complete"][$_SESSION["userAddNames"]["uActivateNumber"]] = $showActivateMessage;
		}
		
		echo $data;
	}

	if ($_POST["module"] == "services")
	{
	    $_SESSION["userAddModuleStatus"]["module"][] = $_POST["module"];
	    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Success";
	    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["userId"] = $_SESSION["userId"];
	    //sleep(2);
            
            $serviceObj  = new Services();            
            
		$xmlinput = xmlHeader($sessionid, "UserServiceAssignListRequest");
		$xmlinput .= "<userId>" . $_SESSION["userId"] . "</userId>";
		//$xmlinput .= "<serviceName>Speed Dial 100</serviceName>";
		//$xmlinput .= "<serviceName>Speed Dial 8</serviceName>";
		if ($_POST["thirdParty"] == "1")
		{
			$xmlinput .= "<serviceName>Third-Party Voice Mail Support</serviceName>";
		}
		//echo "<pre>"; print_r($_POST["servicePack"]);
		
                //Code added @ 03 June 2019 regarding EX-1345
        $serviceList = array();
        if(!empty($_POST["servicePack"][0]))
        {  
			for ($p = 0; $p < count($_POST["servicePack"]); $p++)
			{ 
                $serviceList = $serviceObj->getTotalServicesListOfServicePacks($_POST["servicePack"][$p], $serviceList);
			}
		}
        if(!in_array("Authentication",  $serviceList)){                        
            $xmlinput .= "<serviceName>Authentication</serviceName>";
        }

        if(!empty($_POST["userService"][0])){
            for ($k = 0; $k < count($_POST["servicePack"]); $k++)
            {
                $xmlinput .= "<serviceName>" . htmlspecialchars($_POST["userService"][$k]) . "</serviceName>";
            }
        }
		if(!empty($_POST["servicePack"][0])){
			for ($i = 0; $i < count($_POST["servicePack"]); $i++)
			{                                
				$xmlinput .= "<servicePackName>" . htmlspecialchars($_POST["servicePack"][$i]) . "</servicePackName>";                                
			}
		}                
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                
		if (count($xml->command->attributes()) > 0)
		{
                        $errorArr = array();
                        $cnter    = 0;
			foreach ($xml->command->attributes() as $a => $b)
			{
			    if ($b == "Error" || $b == "Warning")
				{
					$data = $_POST["servicePack"][0] . " was attempted to be assigned but failed.";
					$data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
					if ($xml->command->detail)
					{
						$data .= "%0D%0A" . strval($xml->command->detail);
                                                //Code added @ 27 Dec 2018
                                                $tmpArr1 = array();
                                                $tmpArr2 = array();
                                                if(stripos(strval($xml->command->detail), "Service pack") !==false){
                                                    $tmpArr1 = explode(":", $xml->command->detail);
                                                    $servicePackName1 = $tmpArr1[0];
                                                    $errorArr[$cnter][$servicePackName1] = "Error In Service Pack";
                                                }else{
                                                    $tmpArr2 = explode(":", $xml->command->detail);
                                                    $servicePackName2 = $tmpArr2[0];
                                                    $errorArr[$cnter][$servicePackName2] = "Error In Service";
                                                    for ($x = 0; $x < count($_POST["servicePack"]); $x++)
                                                    {
                                                            $serviceList = $serviceObj->getSericesListOfServicePack($_POST["servicePack"][$x]);                                                    
                                                            $spName      = $_POST["servicePack"][$x];
                                                            $serviceErrorList = $serviceObj->getServiceLicenseErrorOnAssignServicePackToUser($_SESSION["groupId"], $serviceList);
                                                            foreach($serviceErrorList as $errorServiceName => $serviceErrorMessage){
                                                                $data .= "<br /><b>$errorServiceName ($spName) : $serviceErrorMessage</b>";
                                                            }
                                                    }                                                    
                                                }                                                
                                                //End code
					}
                                        $cnter = $cnter + 1;
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Error";
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["ErrorMsg"] = $data;					
				}
			}
		}
		for ($x = 0; $x < count($_POST["servicePack"]); $x++){
		    $serviceList = $serviceObj->getSericesListOfServicePack($_POST["servicePack"][$x]);
		    if(in_array("Busy Lamp Field", $serviceList)){
		        require_once("/var/www/lib/broadsoft/adminPortal/ModifyBLFService.php");
		        $blfModify = new ModifyBLFService($_SESSION["userId"], $_SESSION["devName"]);
		    }
		}

		$_SESSION["complete"][$_SESSION["userAddNames"]["servicePack"]] = implode(", ", $_POST["servicePack"]);
		echo $data;
	}

	if ($_POST["module"] == "assigningcallpickupgroup")
	{
	    $_SESSION["userAddModuleStatus"]["module"][] = $_POST["module"];
	    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Success";
	    
		//sleep(2);
		$gcp = new GroupCallPickupUsers($_POST["groupCallPickupList"]);
		$gcpUsers = $gcp->getUsersInCallPickupGroup();
		$newval = $_SESSION["userId"];
		array_push($gcpUsers, $newval);
		$gcmModifiedUsers = $gcp->modifyUserCallPickupGroup($gcpUsers);
		$_SESSION["complete"][$_SESSION["userAddNames"]["groupCallPickupList"]] = $_POST["groupCallPickupList"];
		$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Success";
		$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["groupCallPickupList"] = $_POST["groupCallPickupList"];
		echo $data;
	}

	if ($_POST["module"] == "modifyDev")
	{
	    $_SESSION["userAddModuleStatus"]["module"][] = $_POST["module"];
	    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Success";
	    //sleep(2);

		//Build blf uri
		$xmlinput = xmlHeader($sessionid, "UserBusyLampFieldModifyRequest");
		$xmlinput .= "<userId>" . $_SESSION["userId"] . "</userId>";
		$xmlinput .= "<listURI>" . $_SESSION["blfUser"] . "</listURI>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));

		$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyUserRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<deviceName>" . $_SESSION["devName"] . "</deviceName>";
		$xmlinput .= "<linePort>" . $_SESSION["linePort"] . "</linePort>";
		$xmlinput .= "<isPrimaryLinePort>true</isPrimaryLinePort>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		if (count($xml->command->attributes()) > 0)
		{
			foreach ($xml->command->attributes() as $a => $b)
			{
			    if ($b == "Error" || $b == "Warning")
				{
					$data = $_SESSION["devName"] . " prime line failed.";
					$data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
					if ($xml->command->detail)
					{
						$data .= "%0D%0A" . strval($xml->command->detail);
					}
					
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Error";
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["ErrorMsg"] = $data;
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["userId"] = $_SESSION["userId"];
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["blfUser"] = $_SESSION["blfUser"];
				}
			}
		}

        $deviceAccessUserName = $_SESSION["deviceAccessUserName"];
        $deviceAccessPassword = $_SESSION["deviceAccessPassword"];

		if ($_POST["deviceAccessUserName"] !== "" and $_POST["deviceAccessPassword"] !== "")
		{
			$deviceAccessUserName = $_POST["deviceAccessUserName"];
			$deviceAccessPassword = $_POST["deviceAccessPassword"];
		}
        /*
		else
		{
			$select = "SELECT customCredentials, username, password from customCredentials where deviceManufacturer='" . $substrMenu . "'";
			$result = $db->query($select);
			while ($row = $result->fetch())
			{
				if ($row["customCredentials"] == "y")
				{
					$deviceAccessUserName = $row["username"];
					$deviceAccessPassword = $row["password"];
				}
			}
		}
         */

		if (isset($deviceAccessUserName) and isset($deviceAccessPassword))
		{
			$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyRequest14");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
			$xmlinput .= "<deviceName>" . $_SESSION["devName"] . "</deviceName>";
			

            if ($_SESSION["userType"] == "analog" && $analogAccessAuthentication == "false") {
                $xmlinput .= "<useCustomUserNamePassword>false</useCustomUserNamePassword>";
            }
            else {
            	$checkDeviceAlgo = strpos($_POST["deviceType"], "Algo");
            	
            	if($checkDeviceAlgo > -1){
            		$xmlinput .= "<useCustomUserNamePassword>false</useCustomUserNamePassword>";
            	}else{
            		$xmlinput .= "<useCustomUserNamePassword>true</useCustomUserNamePassword>";
            	}
            	
                $xmlinput .= "<accessDeviceCredentials>
						<userName>" . $deviceAccessUserName . "</userName>
						<password>" . $deviceAccessPassword . "</password>
					</accessDeviceCredentials>";
            }
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

			if (count($xml->command->attributes()) > 0)
			{
				foreach ($xml->command->attributes() as $a => $b)
				{
					if ($b == "Error")
					{
						$data = $_SESSION["devName"] . " authorization failed.";
						$data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
						if ($xml->command->detail)
						{
							$data .= "%0D%0A" . strval($xml->command->detail);
						}
					}
				}
			}
			$_SESSION["complete"][$_SESSION["userAddNames"]["deviceAccessUserName"]] = $deviceAccessUserName;
			//$_SESSION["complete"][$_SESSION["userAddNames"]["deviceAccessPassword"]] = str_repeat("*", strlen($deviceAccessPassword));
			$_SESSION["complete"][$_SESSION["userAddNames"]["deviceAccessPassword"]] = $deviceAccessPassword;
			$_SESSION["deviceAccessPasswordUserAdd"] = $deviceAccessPassword;
		}
		echo $data;
	}

	if ($_POST["module"] == "buildVM")
	{
	    $_SESSION["userAddModuleStatus"]["module"][] = $_POST["module"];
	    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Success";
		if ($_POST["voiceMessaging"] == "Yes")
		{
			//sleep(2);

			if ($_POST["thirdParty"] == "1")
			{
				$xmlinput = xmlHeader($sessionid, "UserThirdPartyVoiceMailSupportModifyRequest");
			}
			else
			{
				$xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
			}
			$xmlinput .= "<userId>" . $_SESSION["userId"] . "</userId>";
			$xmlinput .= "<isActive>true</isActive>";
			if ($_POST["thirdParty"] == "1")
			{
				$xmlinput .= "<mailboxIdType>URL</mailboxIdType>";
				$xmlinput .= "<mailboxURL>" . $_SESSION["complete"][$_SESSION["userAddNames"]["phoneNumber"]] . "@pingtone.com</mailboxURL>";
			}
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

			if (count($xml->command->attributes()) > 0)
			{
				foreach ($xml->command->attributes() as $a => $b)
				{
				    if ($b == "Error" || $b == "Warning")
					{
						$data = $_SESSION["userId"] . " Voicemail Creation failed.";
						$data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
						if ($xml->command->detail)
						{
							$data .= "%0D%0A" . strval($xml->command->detail);
						}
						
						$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Error";
						$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["ErrorMsg"] = $data;
						$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["userId"] = $_SESSION["userId"];
						$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["phoneNumber"] = $_SESSION["complete"][$_SESSION["userAddNames"]["phoneNumber"]];
					}
				}
			}
		}
		echo $data;
	}

	if ($_POST["module"] == "confVM")
	{
	    $_SESSION["userAddModuleStatus"]["module"][] =  $_POST["module"];
	    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Success";
            $_SESSION["complete"][$_SESSION["userAddNames"]["voiceMessaging"]] = $_POST["voiceMessaging"]; //
		if ($_POST["voiceMessaging"] == "Yes")
		{
            //sleep(1);
		    // Build Voice Mail Password. Enforce fallback
            // ------------------------------------------------------------------------------------------------------
            if ($bwVoicePortalPasswordType == "Formula") {
                require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
                require_once("/var/www/lib/broadsoft/adminPortal/VoicePasswordNameBuilder.php");

                $voicePasswordBuilder = new VoicePasswordNameBuilder($userVoicePortalPasscodeFormula,
                    getResourceForVoicePasswordCreation(ResourceNameBuilder::DN),
                    getResourceForVoicePasswordCreation(ResourceNameBuilder::EXT),
                    getResourceForVoicePasswordCreation(ResourceNameBuilder::USR_CLID),
                    getResourceForVoicePasswordCreation(ResourceNameBuilder::GRP_DN),
                    true);

                if (! $voicePasswordBuilder->validate()) {
                    // TODO: Implement validation resolution
                }

                do {
                    $attr = $voicePasswordBuilder->getRequiredAttributeKey();
                    $voicePasswordBuilder->setRequiredAttribute($attr, getResourceForVoicePasswordCreation($attr));
                } while ($attr != "");

                $Portalpass = $voicePasswordBuilder->getName();
                //Code added @ 08 May 2019 regarding Ex-1318
                if(is_numeric($Portalpass) && strlen($Portalpass) < 6){
                    $Portalpass = "476983";
                }
                if(!is_numeric($portalPassWord)){
                    $Portalpass = "476983";
                }
                //End code
                
                
            } else {
                $Portalpass = setVoicePassword();
            }
            if(is_numeric($Portalpass)){
                $Portalpass = $Portalpass;
            }else{
                $criteria = $userVoicePortalPasscodeFormula;
                preg_match('#\((.*?)\)#', $criteria, $match);
                $var = $match[1];
                $Portalpass = "";
                for($i = 1; $i<= $var; $i++){
                    $Portalpass .= $i;
                }
            }

			$xmlinput = xmlHeader($sessionid, "UserPortalPasscodeModifyRequest");
			$xmlinput .= "<userId>" . $_SESSION["userId"] . "</userId>";
			$xmlinput .= "<newPasscode>" . $Portalpass . "</newPasscode>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

			if (count($xml->command->attributes()) > 0)
			{
				foreach ($xml->command->attributes() as $a => $b)
				{
				    if ($b == "Error" || $b == "Warning")
					{
						$data = $_SESSION["userId"] . " Voicemail Creation failed.";
						$data .= strval($xml->command->summaryEnglish);
						if ($xml->command->detail)
						{
							$data .= strval($xml->command->detail);
						}
						
						$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Error";
						$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["ErrorMsg"] = $data;
						$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["userId"] = $_SESSION["userId"];
						$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Portalpass"] = $Portalpass;
					}
				}
			}

			if ($_POST["thirdParty"] !== "1")
			{
                require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
                require_once("/var/www/lib/broadsoft/adminPortal/UserIdNameBuilder.php");
                require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailNameBuilder.php");

                // Build Surgemail Username.
                // ------------------------------------------------------------------------------------------------------

                // Create surgemail username from the first formula.
                // Enforce fallback if there is no second criteria/formula.
                // -------------------------------------------------------------
                $surgemailName = "";
                $surgeMailNameBuilder1 = new SurgeMailNameBuilder($surgeMailIdCriteria1,
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::DN),
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::EXT),
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::USR_CLID),
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::GRP_DN),
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::FIRST_NAME),
                    getResourceForSurgeMailNameCreation(ResourceNameBuilder::LAST_NAME),
                    $surgeMailIdCriteria2 == "");    // force fallback only if second criteria does not exist

                if (! $surgeMailNameBuilder1->validate()) {
                    // TODO: Implement resolution when User ID input formula has errors
                }

                do {
                    $attr = $surgeMailNameBuilder1->getRequiredAttributeKey();
                    $surgeMailNameBuilder1->setRequiredAttribute($attr, getResourceForSurgeMailNameCreation($attr));
                } while ($attr != "");

                $surgemailName = $surgeMailNameBuilder1->getName();

                if ($surgemailName == "") {
                    // Create surgemail name from the second formula. At this point we know that there is second formula,
                    // because without second formula name would be forcefully resolved in the first formula.

                    $surgeMailNameBuilder2 = new SurgeMailNameBuilder($surgeMailIdCriteria2,
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::DN),
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::EXT),
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::USR_CLID),
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::GRP_DN),
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::FIRST_NAME),
                        getResourceForSurgeMailNameCreation(ResourceNameBuilder::LAST_NAME),
                        true);

                    if (! $surgeMailNameBuilder2->validate()) {
                        // TODO: Implement resolution when User ID input formula has errors
                    }

                    do {
                        $attr = $surgeMailNameBuilder2->getRequiredAttributeKey();
                        $surgeMailNameBuilder2->setRequiredAttribute($attr, getResourceForSurgeMailNameCreation($attr));
                    } while ($attr != "");

                    $surgemailName = $surgeMailNameBuilder2->getName();
                }

                $emailAddress = $surgemailName;
                $emailPassword = setEmailPassword();
                
               // $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["emailAddress"] =  $emailAddress;
               // $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["emailPassword"] =  $emailPassword;
                
                $sugeObj = new SurgeMailOperations();   //Added @ 12 May 2018
                
                
				$xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyAdvancedVoiceManagementRequest");
				$xmlinput .= "<userId>" . $_SESSION["userId"] . "</userId>";
				$xmlinput .= "<groupMailServerEmailAddress>" . $emailAddress . "</groupMailServerEmailAddress>";
				$xmlinput .= "<groupMailServerUserId>" . $emailAddress . "</groupMailServerUserId>";
				$xmlinput .= "<groupMailServerPassword>" . $emailPassword . "</groupMailServerPassword>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));

				$xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
				$xmlinput .= "<userId>" . $_SESSION["userId"] . "</userId>";
				$xmlinput .= "<isActive>true</isActive>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				
				/*
				 $surgemailPost = array ("cmd" => "cmd_user_login",
							"lcmd" => "user_delete",
							"show" => "simple_msg.xml",
							"username" => $surgemailUsername,
							"password" => $surgemailPassword,
							"lusername" => $emailAddress,
							"lpassword" => $emailPassword,
							"uid" => isset($userid) ? $userid : "");
				//$result = http_post_fields($surgemailURL, $surgemailPost);     //Commented @ 12 May 2018                             
                                $srgeRsltDelUsr = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPost); //Added @ 12 May 2018
                    
				 */
				$srgeRsltDelUsrStatus = "200";
                 $surgemailPost  = array ("cmd" => "cmd_user_login",
					"lcmd" => "user_create",
					"show" => "simple_msg.xml",
					"username" => $surgemailUsername,
					"password" => $surgemailPassword,
					"lusername" => $emailAddress,
					"lpassword" => $emailPassword,
					"uid" => isset($userid) ? $userid : "");
				    //$result = http_post_fields($surgemailURL, $surgemailPost);  //Commented @ 12 May 2018 
                    $srgeRsltCrteUsr = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPost); //Added @ 12 May 2018
                    if( strpos( $srgeRsltCrteUsr, 'version="2.0"' ) !== false) {
                        $xmlVal = new SimpleXMLElement($srgeRsltCrteUsr);
                        $srgeRsltCrteUsr = strval($xmlVal->response);
                    }
                    
                    if($srgeRsltCrteUsr == "") {
                        $srgeRsltCrteUsr = "200";
                    }
                    
                    /* Delete surgemail accont if account already exist */
                    if(strpos($srgeRsltCrteUsr, "already exists") !== false) {
                         $surgemailPostDelete = array ("cmd" => "cmd_user_login",
                         "lcmd" => "user_delete",
                         "show" => "simple_msg.xml",
                         "username" => $surgemailUsername,
                         "password" => $surgemailPassword,
                         "lusername" => $emailAddress,
                         "lpassword" => $emailPassword,
                         "uid" => isset($userid) ? $userid : "");
                         //$result = http_post_fields($surgemailURL, $surgemailPost);     //Commented @ 12 May 2018
                         $srgeRsltDelUsr = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPostDelete); //Added @ 12 May 2018
                         /*If User Already Exist then Delete user and Again add surgemail*/
                         if(strpos($srgeRsltDelUsr, "Account deleted") !== false) {
                             $srgeRsltCrteUsr = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgemailPost); //Added @ 12 May 2018
                         } else {
                             $srgeRsltDelUsrStatus = "Error";
                         }
                    }
                    //print_r($srgeRsltCrteUsr); echo"hhhhhhh";  print_r($srgeRsltDelUsrStatus); echo"yyyyy"; exit;
                    if($srgeRsltCrteUsr != "200" || $srgeRsltDelUsrStatus != "200") {
                        $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Error";
                        $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["ErrorMsg"] = $srgeRsltCrteUsr != "200" ? $srgeRsltCrteUsr : $srgeRsltDelUsrStatus;
                        $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["userId"] = $_SESSION["userId"];
                        $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Portalpass"] = $Portalpass;
                        
                        $data = rtrim($srgeRsltCrteUsr, "''").'|ErrorInSurgeMail';
                    }
                                
//print_r($result);
			}
			$_SESSION["complete"][$_SESSION["userAddNames"]["portal"]] = $Portalpass;
		}
		echo $data;
	}

	if ($_POST["module"] == "addServ")
	{
	    $_SESSION["userAddModuleStatus"]["module"][] =  $_POST["module"];
	    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Success";
            
                if(isset($_POST["polycomPhoneServices"]) && $_POST["polycomPhoneServices"]=="Yes"){
                    $_SESSION["complete"][$_SESSION["userAddNames"]["polycomPhoneServices"]] = $_POST["polycomPhoneServices"];
                }
                if(isset($_POST["ccd"]) && $_POST["ccd"]!=""){
                    $_SESSION["complete"][$_SESSION["userAddNames"]["ccd"]] = $_POST["ccd"];
                }
                //echo "<pre><br /> polycomPhoneServices - ".$_POST["polycomPhoneServices"];
                //echo "<br /> devName - ".$_SESSION["devName"];
                //echo "<br /> ccd - ".$_POST["ccd"];
		if ($_POST["polycomPhoneServices"] == "Yes" && isset($_SESSION["devName"]))
		{
			//sleep(2);
			
			require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
			require_once("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
			
			$objSysLevel = new sysLevelDeviceOperations();
			$returnResp = $objSysLevel->getSysytemDeviceTypeServiceRequest($_POST["deviceType"], $ociVersion);
			//print_r($returnResp);
			$xmlinput = xmlHeader($sessionid, "UserServiceIsAssignedRequest");
			$xmlinput .= "<userId>" . $_SESSION["userId"] . "</userId>";
			$xmlinput .= "<serviceName>Polycom Phone Services</serviceName>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

			if (empty($returnResp["Error"]) && strval($returnResp["Success"]->supportsPolycomPhoneServices) == "true" && strval($xml->command->isAssigned) == "true")

			{
				$xmlinput = xmlHeader($sessionid, "UserPolycomPhoneServicesModifyRequest");
				$xmlinput .= "<userId>" . $_SESSION["userId"] . "</userId>";
				$xmlinput .= "<accessDevice>
							<deviceLevel>Group</deviceLevel>
							<deviceName>" . $_SESSION["devName"] . "</deviceName>
						</accessDevice>";
				$xmlinput .= "<integratePhoneDirectoryWithBroadWorks>true</integratePhoneDirectoryWithBroadWorks>";
				$xmlinput .= "<includeUserPersonalPhoneListInDirectory>true</includeUserPersonalPhoneListInDirectory>";
				$xmlinput .= "<includeGroupCustomContactDirectoryInDirectory>true</includeGroupCustomContactDirectoryInDirectory>";
				if ($_POST["ccd"])
				{
					$xmlinput .= "<groupCustomContactDirectory>" . htmlspecialchars($_POST["ccd"]) . "</groupCustomContactDirectory>";
				}
				else
				{
					$xmlinput .= "<groupCustomContactDirectory xsi:nil=\"true\" />";
				}
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                                                               
                                
				if (count($xml->command->attributes()) > 0)
				{                                        
					foreach ($xml->command->attributes() as $a => $b)
					{
					    if ($b == "Error" || $b == "Warning")
						{                                                        
							$data = $_SESSION["userId"] . " Configuring Additional Services failed P.";
							$data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
							if ($xml->command->detail)
							{
								$data .= "%0D%0A" . strval($xml->command->detail);
							}
							
							$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Error";
							$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["ErrorMsg"] = $data;
							$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["userId"] = $_SESSION["userId"];
							$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["devName"] = $_SESSION["devName"];
						}
					}                                        
                                        
				}

				if ($_POST["ccd"])
				{
					//retrieve list of users from custom contact directory and append new user to list
					$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryGetRequest17");
					$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
					$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
					$xmlinput .= "<name>" . htmlspecialchars($_POST["ccd"]) . "</name>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

					$ccdUsersList = "";
					foreach ($xml->command->userTable->row as $key => $value)
					{
						$ccdUsersList .= "<entry><userId>" . strval($value->col[0]) . "</userId></entry>";
					}
					$ccdUsersList .= "<entry><userId>" . $_SESSION["userId"] . "</userId></entry>";

					$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryModifyRequest17");
					$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
					$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
					$xmlinput .= "<name>" . htmlspecialchars($_POST["ccd"]) . "</name>";
					$xmlinput .= "<entryList>" . $ccdUsersList . "</entryList>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                                                                                

					if (count($xml->command->attributes()) > 0)
					{                                                
						foreach ($xml->command->attributes() as $a => $b)
						{
							if ($b == "Error")
							{                                                                
								$data = $_SESSION["userId"] . " Configuring Additional Services failed C.";
								$data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
								if ($xml->command->detail)
								{
									$data .= "%0D%0A" . strval($xml->command->detail);
								}
							}
						}                                                
					}
				}
			}
		}
		echo $data;
	}

	if ($_POST["module"] == "auth")
	{
	    $_SESSION["userAddModuleStatus"]["module"][] =  $_POST["module"];
	    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Success";
		//sleep(2);

        // for authentication name use userId without default domain
        $uidWithDomain = explode("@", $_SESSION["userId"]);
        //$AuthPass = setAuthPassword();
        $AuthPass = $_SESSION["authPass"];

		$xmlinput = xmlHeader($sessionid, "UserAuthenticationModifyRequest");
		$xmlinput .= "<userId>" . $_SESSION["userId"] . "</userId>";
		$xmlinput .= "<userName>" . $uidWithDomain[0] . "</userName>";
		$xmlinput .= "<newPassword>" . $AuthPass . "</newPassword>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));

		if (count($xml->command->attributes()) > 0)
		{
			foreach ($xml->command->attributes() as $a => $b)
			{
			    if ($b == "Error" || $b == "Warning")
				{
					$data = $authName . " Authentication configuration failed.";
					$data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
					if ($xml->command->detail)
					{
						$data .= "%0D%0A" . strval($xml->command->detail);
					}
					
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Error";
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["ErrorMsg"] = $data;
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["userId"] = $_SESSION["userId"];
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["uidWithDomain"] = $uidWithDomain[0];
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["AuthPass"] = $AuthPass;
				}
			}
		}
		echo $data;
	}

	if ($_POST["module"] == "dp")
	{
	    $_SESSION["userAddModuleStatus"]["module"][] =  $_POST["module"];
	    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Success";
		//sleep(2);

        //Turn off international
        /*if ($usrAllowInternationalCalls == "false") {
            $xmlinput = xmlHeader($sessionid, "UserOutgoingCallingPlanOriginatingModifyRequest");
            $xmlinput .= "<userId>" . $_SESSION["userId"] . "</userId>";
            $xmlinput .= "<useCustomSettings>true</useCustomSettings>";
            $xmlinput .= "<userPermissions><international>Disallow</international></userPermissions>";
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
        }*/

		if (count($xml->command->attributes()) > 0)
		{
			foreach ($xml->command->attributes() as $a => $b)
			{
			    if ($b == "Error" || $b == "Warning")
				{
					$data = $_SESSION["userId"] . " Dial Plan configuration failed.";
					$data .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
					if ($xml->command->detail)
					{
						$data .= "%0D%0A" . strval($xml->command->detail);
					}
					
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Error";
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["ErrorMsg"] = $data;
					$_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["userId"] = $_SESSION["userId"];
				}
			}
		}
		
		echo $data;
	}
	
	if ($_POST["module"] == "addCustomTag")
	{
	    $custTagList = json_decode($_POST["custTagList"]);
	    $addedCustomTags = array();
	    foreach($custTagList->addTagList as $tagKey => $tagVal) {
	        $isTagAdded = addDeviceCustomTag($_SESSION["devName"], $tagVal->tagName, $tagVal->tagValue);
	        if($isTagAdded != "") {
	            $addedCustomTags[$tagKey]['tagName'] = $tagVal->tagName;
	            $addedCustomTags[$tagKey]['status'] = "Failure";
	        } else {
	            $addedCustomTags[$tagKey]['tagName'] = $tagVal->tagName;
	            $addedCustomTags[$tagKey]['status'] = "Success";
	        }
	        
	    }
	    
	    print_r(json_encode($addedCustomTags));
	    
	}
	
	if($_POST["module"] == "addSoftPhone"){
	    require_once("/var/www/html/Express/userAdd/softPhoneDeviceOperations.php");
	}
?>
