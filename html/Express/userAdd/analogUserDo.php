<?php
/**
 * Created by Karl.
 * Date: 11/12/2016
 */

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

    require_once("/var/www/lib/broadsoft/login.php");
    checkLogin();
    require_once("/var/www/html/Express/config.php");

    require_once("/var/www/lib/broadsoft/adminPortal/getDeviceList.php");
    require_once("/var/www/lib/broadsoft/adminPortal/getDevicesR19.php");
    require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/DeviceNameBuilder.php");
    require_once("/var/www/html/Express/userAdd/UserDBOperations.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
    require_once("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
    require_once("/var/www/lib/broadsoft/adminPortal/phoneProfiles/PhoneProfilesOperations.php");
    
    const customProfileTable = "customProfiles";
    const customProfileName  = "customProfileName";
    const customProfileType  = "deviceType";

    function deviceExists($device) {
        global $devices;

        foreach ($devices as $key => $value) {
            if ($value["deviceName"] == $device) {
                return true;
            }
        }
        return false;
    }


    function getDeviceTypeNumberOfPorts($deviceType) {
        global $staticLineOrderingDeviceTypes;

        foreach ($staticLineOrderingDeviceTypes as $key => $value) {
            if ($value["deviceType"] == $deviceType) {
                return $value["numberOfPorts"];
            }
        }
    }


    function getDevicePortUsage($deviceName) {
        global $ociVersion, $sessionid, $client;

        $ociCmd = "GroupAccessDeviceGetRequest18sp1";        
        $xmlinput = xmlHeader($sessionid, $ociCmd);
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $_SESSION["devicePortUsage"]["numberOfPorts"]         = isset($xml->command->numberOfPorts->quantity) ? strval($xml->command->numberOfPorts->quantity) : 1000;
        $_SESSION["devicePortUsage"]["numberOfAssignedPorts"] = strval($xml->command->numberOfAssignedPorts);
    }


    function getDeviceAvailablePorts($deviceName) {
        global $sessionid, $client;

        $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceAvailablePortGetListRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<accessDevice>";
        $xmlinput .=    "<deviceLevel>Group</deviceLevel>";
        $xmlinput .=    "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= "</accessDevice>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $a = 0;
        $availablePorts = array();
        foreach ($xml->command->portNumber as $key => $value) {
            $availablePorts[$a++] = strval($value);
        }

        return $availablePorts;
    }


    function getResourceForDeviceNameCreation($attr, $index="0") {
        switch ($attr) {
            //TODO: Revise rules for Device Name Creation to remove commented out portions through the entire Express
            //case ResourceNameBuilder::DN:               return $_POST["phoneNumber"] != "" ? $_POST["phoneNumber"] : "";
            //case ResourceNameBuilder::EXT:              return $_POST["extension"];
            case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:           return $_SESSION["groupId"];
            //case ResourceNameBuilder::FIRST_NAME:       return $_POST["firstName"];
            //case ResourceNameBuilder::LAST_NAME:        return $_POST["lastName"];
            case ResourceNameBuilder::DEV_TYPE:         return $_POST["deviceType"];
            case ResourceNameBuilder::MAC_ADDR:         return $_POST["macAddress"];
            case ResourceNameBuilder::INT_1:
            case ResourceNameBuilder::INT_2:            return $index;
            default: return "";
        }
    }


    function getResourceForDeviceNameCreationForDigital($attr) {
        switch ($attr) {
            case ResourceNameBuilder::DN:               return $_POST["phoneNumber"] != "" ? $_POST["phoneNumber"] : "";
            case ResourceNameBuilder::EXT:              return $_POST["extension"];
            case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
            case ResourceNameBuilder::GRP_ID:           return $_SESSION["groupId"];
            case ResourceNameBuilder::FIRST_NAME:       return $_POST["firstName"];
            case ResourceNameBuilder::LAST_NAME:        return $_POST["lastName"];
            case ResourceNameBuilder::DEV_TYPE:         return $_POST["deviceType"];
            case ResourceNameBuilder::MAC_ADDR:         return $_POST["macAddress"];
            case ResourceNameBuilder::INT_1:
            case ResourceNameBuilder::INT_2:            return $_POST["deviceIndex"];
            default: return "";
        }
    }
    
    function getDeviceName($index) {
        global $deviceNameAnalog;

        // create device builder... since this is for analog devices, force resolve is always set to true.
        $deviceNameBuilder = new DeviceNameBuilder($deviceNameAnalog,
            getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
            getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
            getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
            true);

        if (! $deviceNameBuilder->validate()) {
            // TODO: Implement resolution when Device name input formula has errors
        }

        do {
            $attr = $deviceNameBuilder->getRequiredAttributeKey();
            $deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr, $index));
        } while ($attr != "");

        return $deviceNameBuilder->getName();
    }

    function getDeviceNameForDigital($POST, $deviceNameDID2, $deviceNameAnalog, $deviceNameDID1) {
        $deviceName = "";
        $forceResolve = $POST["userType"] == "analog" ? true : $deviceNameDID2 == "";
        
        $deviceNameBuilder = new DeviceNameBuilder($POST["userType"] == "analog" ? $deviceNameAnalog : $deviceNameDID1,
            getResourceForDeviceNameCreationForDigital(ResourceNameBuilder::DN),
            getResourceForDeviceNameCreationForDigital(ResourceNameBuilder::EXT),
            getResourceForDeviceNameCreationForDigital(ResourceNameBuilder::GRP_DN),
            $forceResolve);
        
        if (! $deviceNameBuilder->validate()) {
            // TODO: Implement resolution when Device name input formula has errors
        }
        
        do {
            $attr = $deviceNameBuilder->getRequiredAttributeKey();
            $deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreationForDigital($attr));
        } while ($attr != "");
        
        $deviceName = $deviceNameBuilder->getName();
        
        if($deviceName == "") {
                
            $deviceNameBuilder = new DeviceNameBuilder($deviceNameDID2,
                getResourceForDeviceNameCreationForDigital(ResourceNameBuilder::DN),
                getResourceForDeviceNameCreationForDigital(ResourceNameBuilder::EXT),
                getResourceForDeviceNameCreationForDigital(ResourceNameBuilder::GRP_DN),
                true);
            
            if (! $deviceNameBuilder->validate()) {
                // TODO: Implement resolution when Device name input formula has errors
            }
            
            do {
                $attr = $deviceNameBuilder->getRequiredAttributeKey();
                $deviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreationForDigital($attr));
            } while ($attr != "");
            
            $deviceName = $deviceNameBuilder->getName();
            
        }
        
        $deviceName = replaceDevNameWithSubstitute($deviceName);
        return $deviceName;
    }
    
    /*Start New Code*/
    function getLinePortDevTypeLookup() {
        global $db;
        $deviceSubList = array();
        //Code added @ 16 July 2019
        $whereCndtn = "";
        if($_SESSION['cluster_support']){
            $selectedCluster = $_SESSION['selectedCluster'];
            $whereCndtn = " WHERE clusterName ='$selectedCluster' ";
        }
        //End code
        $qryLPLU = "select * from systemDevices $whereCndtn ";
        $select_stmt = $db->prepare($qryLPLU);
        $select_stmt->execute();
        $i = 0;
        while($rs = $select_stmt->fetch())
        {
            $deviceSubList[$i]['deviceType'] = $rs['deviceType'];
            $deviceSubList[$i]['substitution'] = $rs['nameSubstitution'];
            $i++;
        }
        return $deviceSubList;
    }
    
    function replaceDevNameWithSubstitute($deviceName) {
        $newDeviceName = $deviceName;
        $subList = getLinePortDevTypeLookup();
        foreach($subList as $key => $value) {
            if(strpos($deviceName, $value['deviceType']) !== false) {
                $newDeviceName = str_replace($value['deviceType'], $value['substitution'], $deviceName);
            }
        }
        return $newDeviceName;
    }
    /*End New Code*/
    
    function getCustomProfiles($deviceType) {
        global $db;

        $query = "select " . customProfileName . " from " . customProfileTable . " where " . customProfileType . "='" . $deviceType . "' order by " . customProfileName;
        $results = $db->query($query);
        $profiles = array();
        $i = 0;
       
        if($results->fetch() != ""){
            $obj = new UserDBOperations();
            $profiles = $obj->getAllPhoneProfiles();
        }
        
       /* while ($row = $results->fetch()) {
            if ( ! in_array($profile = $row[customProfileName], $profiles)) {
                $profiles[$i++] = $profile;
            }
        }*/

        //return $profiles;
        /*$profiles = array();
        $obj = new UserDBOperations();
        $customProfiles = $obj->getAllCustomProfiles();
        if(in_array($deviceType, $customProfiles)){
            $profiles = $obj->getAllPhoneProfiles();
            return $profiles;
        }*/
        return $profiles;
    }


    function buildDeviceIndexSelection() {
        global $numSIPGatewayInstances;

        $str  = "<select name=\"deviceIndex\" id=\"deviceIndex\" onchange=\"processPortNumbers(this)\">";
        $str .= "<option value=\"\"></option>";     // empty row

        for ($i = 1; $i <= $numSIPGatewayInstances; $i++) {
            $_SESSION["devicePortUsage"]["numberOfPorts"] = 0;

            $deviceName = getDeviceName($i);
            if (isset($deviceName) && deviceExists($deviceName)) {
                getDevicePortUsage($deviceName);
            }

            $style = "";
            $disabled = "";
            $numberOfPorts = $_SESSION["devicePortUsage"]["numberOfPorts"];
            $availablePorts = $numberOfPorts == 0 ? 0 : $numberOfPorts - $_SESSION["devicePortUsage"]["numberOfAssignedPorts"];

            $displayInfo  = $i . ":  " . $deviceName . "   (";

            if ($numberOfPorts == 0) {
                $displayInfo .= "new device";
            }
            elseif ($availablePorts == 0) {
                $displayInfo .= "No ports available";
                $disabled = " disabled";
                $style = "style=\"color: gray\"";
            }
            else {
                $displayInfo .= "available " . $availablePorts;
                $displayInfo .= $availablePorts == 1 ? " port" : " ports";
            }

            $displayInfo .= ")";
            $str .= "<option " . $style . " value=\"" . $i . "\"" . $disabled . ">" . $displayInfo . "</option>";
        }

        $str .= "</select>";
        return $str;
    }


    function buildPortListForDigital($POST, $deviceNameDID2, $deviceNameAnalog, $deviceNameDID1) {
        $availablePorts = array();
        $numberOfPorts = getDeviceTypeNumberOfPorts($POST["deviceType"]);
        if( (isset($POST['actionModule']) && $POST['actionModule'] == "userModify") && $_SESSION["userInfo"]["deviceType"] == $POST["deviceType"]) {
            $deviceName = $_SESSION["userInfo"]["deviceName"];
        } else {
            $deviceName = getDeviceNameForDigital($POST, $deviceNameDID2, $deviceNameAnalog, $deviceNameDID1);
        }
        $deviceDoesNotExist = ! deviceExists($deviceName);
        if (deviceExists($deviceName)) {
            $availablePorts = getDeviceAvailablePorts($deviceName);
        }
        
        $str  = "<select name=\"portNumberSIP\" id=\"portNumberSIP\">";
        $str .= "<option value=\"\"></option>";     // empty row
        
        for ($i = 1; $i <= $numberOfPorts; $i++) {
            // port is always available for non-existent (not yet created devices),
            // otherwise check whether port is on the list of available ports
            $portInUse = $deviceDoesNotExist ? false : ! in_array($i, $availablePorts);
            $style = $portInUse ? "style=\"color: gray\"" : "";
            $disabled = $portInUse ? " disabled" : "";
            
            $str .= "<option " . $style . " value=\"" . $i . "\"" . $disabled . ">" . $i . ":  Port " . $i . "</option>";
        }
        
        $str .= "</select>";
        return $str;
        
    }
    
    function buildPortSelection() {
        $availablePorts = array();
        $numberOfPorts = getDeviceTypeNumberOfPorts($_POST["deviceType"]);
        $deviceName = getDeviceName($_POST["deviceIndex"]);
        $deviceDoesNotExist = ! deviceExists($deviceName);
        if (deviceExists($deviceName)) {
            $availablePorts = getDeviceAvailablePorts($deviceName);
        }

        $str  = "<select name=\"portNumber\" id=\"portNumber\">";
        $str .= "<option value=\"\"></option>";     // empty row

        for ($i = 1; $i <= $numberOfPorts; $i++) {
            // port is always available for non-existent (not yet created devices),
            // otherwise check whether port is on the list of available ports
            $portInUse = $deviceDoesNotExist ? false : ! in_array($i, $availablePorts);
            $style = $portInUse ? "style=\"color: gray\"" : "";
            $disabled = $portInUse ? " disabled" : "";

            $str .= "<option " . $style . " value=\"" . $i . "\"" . $disabled . ">" . $i . ":  Port " . $i . "</option>";
        }

        $str .= "</select>";
        return $str;

    }


    function buildCustomProfilesList($defaultTag) {
        $str = "";
        //$str  = "<select name=\"customProfile\" id=\"customProfile\">";
       // $str .= "<option value=\"None\" selected>None</option>";

        $profiles = getCustomProfiles($_POST["deviceType"]);
        foreach ($profiles as $key => $profile) {
            if( in_array($profile[0], PhoneProfilesOperations::$restrictedPhoneProfiles) ) {
                continue;
            }
            
            if($defaultTag == $profile[1]){
                $selected = "selected";
            }else{
                $selected = "";
            }
            $str .= "<option ".$selected." value=\"" . $profile[1] . "\"";
            $str .= ">" . $profile[0] . "</option>";
        }

       // $str .= "</select>";
        return $str;
    }
    
    function buildTagBundleSelection($deviceType) {
        $obj = new UserDBOperations();
        $customProfiles = $obj->getAllCustomProfiles();
        $str = "";
        
        //$str = "<select name='deviceMgtTagBundle[]' id='deviceMgtTagBundle' size='3' multiple=''>";
        if(in_array($deviceType, $customProfiles)){
            $modBundleArr = $obj->getAllTagBundles();
            if(count($modBundleArr) > 0){
                
                $i = 0;
                foreach ($modBundleArr as $keyTB => $valueTB){
                    $bundleID = "bundleID".$i;
                    $str .= "<div class='form-group'><input type='checkbox' name='deviceMgtTagBundle[]' value=".$valueTB." id=".$bundleID."><label for=".$bundleID."><span></span></label><label class='labelText'> ".$valueTB."</label></div>";
                    $i++;
                    //$str .= "<option value = ".$valueTB.">".$valueTB."</option>";
                }
            }
        }
        //$str .= "</select>";
        return $str;
    }


    // --------------------------------
    // Ajax POST processing starts here
    // --------------------------------

    $error = false;
    $resp = "";

    if ($_POST["module"] == "buildIndexList") {
        $resp = buildDeviceIndexSelection();
    }

    if ($_POST["module"] == "buildPortListForDigital") {
        $resp = buildPortListForDigital($_POST, $deviceNameDID2, $deviceNameAnalog, $deviceNameDID1);
    }
    
    if ($_POST["module"] == "buildPortList") {
        $resp = buildPortSelection();
    }


    if ($_POST["module"] == "buildCustomProfilesList") {
        require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
        $syslevelObj = new sysLevelDeviceOperations();
        $setName = $syslevelObj->getTagSetNameByDeviceType($_SESSION ["userInfo"] ["deviceType"], $ociVersion);
        $systemDefaultTagVal = "";
        if($setName["Success"] != "" && !empty($setName["Success"])){
            $defaultSettags = $syslevelObj->getDefaultTagListByTagSet($setName["Success"]);
            $phoneTemplateName = "%phone-template%";
            if(array_key_exists($phoneTemplateName, $defaultSettags["Success"])){
                $systemDefaultTagVal = $defaultSettags["Success"]["%phone-template%"][0];
            }
        }
        $resp = buildCustomProfilesList($systemDefaultTagVal);
    }
    
    if ($_POST["module"] == "buildTagBundle") {
        $resp = buildTagBundleSelection($_POST["deviceTypeForTagBundle"]);
    }
    
    if ($_POST["module"] == "checkDeviceSIPPolycomPhoneService") {
        //$resp = buildTagBundleSelection($_POST["deviceTypeForSIPDeviceCheck"]);
        $obj = new sysLevelDeviceOperations();
        $returnResp = $obj->getSysytemDeviceTypeServiceRequest($_POST["deviceTypeForSIPDeviceCheck"], $ociVersion);
        
        if($returnResp["Error"] == "" && empty($returnResp["Error"])){
            
            $resp = strval($returnResp["Success"]->supportsPolycomPhoneServices);
        }else{
            $resp = "false";
        }
    }


    $errFlag = $error ? "1" : "0";
    echo $errFlag . $resp;

?>
