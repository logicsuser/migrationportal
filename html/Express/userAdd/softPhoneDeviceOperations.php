<?php 
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
require_once("/var/www/lib/broadsoft/adminPortal/DeviceNameBuilder.php");
require_once("/var/www/lib/broadsoft/adminPortal/LineportNameBuilder.php");
require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
require_once("/var/www/lib/broadsoft/adminPortal/sca/SCAOperations.php");

require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
$grpServiceList = new Services();
require_once ("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/userServices.php");

$data1 = "";

require_once("/var/www/lib/broadsoft/adminPortal/counterPathStretto/CPSOperations.php");
$cpsobj = new CPSOperations();
require_once ("/var/www/lib/broadsoft/adminPortal/ocpFeatures/OCPOperations.php");
$objSoft = new OCPOperations();

require_once("/var/www/lib/broadsoft/adminPortal/customTagManagement.php");
$objCustomTagMgMt = new CustomTagManagement();
//require_once("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig.php");
require_once("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig/rebuildAndResetDeviceConfig.php");
$rebuldSoftPhoneDevice = new RebuildAndResetDeviceConfig();
$softPhoneRollBack = array();

$deviceObjSoftPhone = new DeviceOperations();
$scaOpsObj = new SCAOperations();
$softPhoneUserName1 = "";
$softPhonePassword1 = "";

$deviceFileGet = false;

if(isset($_POST["email"]) && $_POST["email"] != ""){
    $_POST["accountEmailAddress"] = $_POST["email"];
}
$uidWithDomain1 = explode("@", $_SESSION["userId"]);

$scaArray = array(
    "Shared Call Appearance",
    "Shared Call Appearance 10",
    "Shared Call Appearance 15",
    "Shared Call Appearance 20",
    "Shared Call Appearance 25",
    "Shared Call Appearance 30",
    "Shared Call Appearance 35",
    "Shared Call Appearance 5",
);

/*$changeSCAAssignLog = array();
$grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
$servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
$servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray);
$scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);


foreach($scaServiceArray as $ssa=>$ssv){
    $pos = strpos($ssv->col[0], "Shared Call Appearance");
    if($pos === false){
    }else{
        if($ssv->count == "1"){
            $servicePackToBeAssigned = $servicePackArray[$ssa][0];
        }
    }
}

$userServices = new userServices();
$userSessionservicePackArray = $userServices->getUserServicesAll($_SESSION["userId"], $_SESSION["sp"]);
$servicesSessionArray = $grpServiceList->getServicePackServicesListUsers($userSessionservicePackArray["Success"]["ServicePack"]["assigned"]);
$scaSessionServiceArray = $grpServiceList->checkScaUserServiceInServicepack($servicesSessionArray);

//code ends
if(count($scaSessionServiceArray) == 0){
    $responseServicePackUser = $grpServiceList->assignServicePackUser($_SESSION["userId"], $servicePackToBeAssigned);
    if(!empty($responseServicePackUser["Error"])){
        $data1 .= "There is an Error: ".$servicePackToBeAssigned." Service Pack can not be assigned.".$responseServicePackUser["Error"]."";
        
    }
}*/



require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
$syslevelObj = new sysLevelDeviceOperations();

$softPhoneArray = array();
$softPhoneArray["sp"] = $_SESSION["sp"];
$softPhoneArray["groupId"] = $_SESSION["groupId"];
$softPhoneArray["userId"] = $_SESSION["userId"];
$softPhoneArray["deviceType"] = $_POST["deviceType"];

$setName = $syslevelObj->getTagSetNameByDeviceType($_POST["deviceType"], $ociVersion);
$systemDefaultTagVal = "";
if($setName["Success"] != "" && !empty($setName["Success"])){
    $defaultSettags = $syslevelObj->getDefaultTagListByTagSet($setName["Success"]);
    $phoneTemplateName = "%COUNTERPATH_SBC%";
    if(array_key_exists($phoneTemplateName, $defaultSettags["Success"])){
        $systemDefaultTagVal = $defaultSettags["Success"]["%COUNTERPATH_SBC%"][0];
    }
}

$payloadData = "account.notification._userEmailAddress=<express_email_value>
account1Sip.accountName=<express_account_name>
account1Sip.credentials.authorizationName=<express_authorizationName_value>
account1Sip.credentials.password=<express_password_value>
account1Sip.credentials.username=<express_username_value>
account1Sip.domain=<express_domain_value>
account1Sip.proxy=<express_proxy_value>";

function getResourceForLinePortCreation($attr) {
    
    switch ($attr) {
        case ResourceNameBuilder::DN:               return $_POST["phoneNumber"] != "" ? $_POST["phoneNumber"] : "";
        case ResourceNameBuilder::EXT:              return $_POST["extension"];
        case ResourceNameBuilder::USR_CLID:         return $_POST["callerId"];
        case ResourceNameBuilder::GRP_DN:           return isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "";
        case ResourceNameBuilder::GRP_ID:           return str_replace(" ", "_", trim($_SESSION["groupId"]));
        case ResourceNameBuilder::SYS_DFLT_DOMAIN:  return $_SESSION["systemDefaultDomain"];
        case ResourceNameBuilder::DEFAULT_DOMAIN:   return $_SESSION["defaultDomain"];
        case ResourceNameBuilder::GRP_PROXY_DOMAIN: return $_POST["softPhoneDomain"];
        case ResourceNameBuilder::FIRST_NAME:       return $_POST["firstName"];
        case ResourceNameBuilder::LAST_NAME:        return $_POST["lastName"];
        case ResourceNameBuilder::DEV_NAME:         return isset($_SESSION["softPhoneDeviceName"]) && $_SESSION["softPhoneDeviceName"] != "" ? $_SESSION["softPhoneDeviceName"] : "";
        case ResourceNameBuilder::MAC_ADDR:         return isset($_SESSION["macAddress"]) && $_SESSION["macAddress"] != "" ? $_SESSION["macAddress"] : "";
        case ResourceNameBuilder::ENT:              return strtolower(trim($_SESSION["sp"]));
        case ResourceNameBuilder::USR_ID:
            $name = isset($_SESSION["userId"]) ? $_SESSION["userId"] : "";
            if ($name == "") {
                return "";
            }
            $nameSplit = explode("@", $name);
            return $nameSplit[0];
            break;
        default:                                    return "";
    }
}

function getLinePortDevTypeLookupSoftPhone() {
    global $db;
    $deviceSubList = array();
    //Code added @ 16 July 2019
    $whereCndtn = "";
    if($_SESSION['cluster_support']){
        $selectedCluster = $_SESSION['selectedCluster'];
        $whereCndtn = " WHERE clusterName ='$selectedCluster' ";
    }
    //End code
    $qryLPLU = "select * from systemDevices $whereCndtn ";
    $select_stmt = $db->prepare($qryLPLU);
    $select_stmt->execute();
    $i = 0;
    while($rs = $select_stmt->fetch())
    {
        $deviceSubList[$i]['deviceType'] = $rs['deviceType'];
        $deviceSubList[$i]['substitution'] = $rs['nameSubstitution'];
        $i++;
    }
    return $deviceSubList;
}

function replaceDevNameWithSubstituteSoftPhone($deviceName, $devType) {
    $newDeviceName = $deviceName;
    $subList = getLinePortDevTypeLookupSoftPhone();
    foreach($subList as $key => $value) {
        if($value["deviceType"] == $devType){
            $newDeviceName = str_replace($value['deviceType'], $value['substitution'], $deviceName);
        }
        /*if(strpos($deviceName, $value['deviceType']) !== false) {
            $newDeviceName = str_replace($value['deviceType'], $value['substitution'], $deviceName);
        }*/
    }
    return $newDeviceName;
}

$softPhonedeviceNameBuilder = new DeviceNameBuilder($deviceNameDID1,
    getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
    getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
    getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
    $forceResolve);

if (! $softPhonedeviceNameBuilder->validate()) {
    // TODO: Implement resolution when Device name input formula has errors
}

do {
    $attr = $softPhonedeviceNameBuilder->getRequiredAttributeKey();
    $softPhonedeviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
} while ($attr != "");

$softPhoneDeviceName = $softPhonedeviceNameBuilder->getName();

if ($softPhoneDeviceName == "") {
    // Create Device Name builder from the second formula (DID type only).
    // At this point we know that there is second formula, because without second formula
    // name would be forcefully resolved in the first formula.
    
    $softPhonedeviceNameBuilder = new DeviceNameBuilder($deviceNameDID2,
        getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
        getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
        getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
        true);
    
    if (! $softPhonedeviceNameBuilder->validate()) {
        // TODO: Implement resolution when Device name input formula has errors
    }
    
    do {
        $attr = $softPhonedeviceNameBuilder->getRequiredAttributeKey();
        $softPhonedeviceNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
    } while ($attr != "");
    
    $softPhoneDeviceName = $softPhonedeviceNameBuilder->getName();
}

$softPhoneDeviceNameWithoutSpace = replaceDevNameWithSubstituteSoftPhone($softPhoneDeviceName, $_POST["deviceType"]);
//$softPhoneDeviceNameWithoutSpace = str_replace(" ", "", $softPhoneDeviceName);
$softPhoneArray["deviceName"] = $softPhoneDeviceNameWithoutSpace;
$_SESSION["softPhoneDeviceName"] = $softPhoneDeviceNameWithoutSpace;

//Device Access UserName and Password
$deviceAccessUserNameBuilder = new DeviceNameBuilder($deviceAccessUserName1,
    getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
    getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
    getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
    $deviceAccessUserName2 == "");    // force fallback only if second criteria does not exist
    
    if (! $deviceAccessUserNameBuilder->validate()) {
        // TODO: Implement resolution when Device name input formula has errors
    }
    
    do {
        $attr = $deviceAccessUserNameBuilder->getRequiredAttributeKey();
        $deviceAccessUserNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
    } while ($attr != "");
    
    $softPhoneDeviceAccessUserName = $deviceAccessUserNameBuilder->getName();
    
    if ($softPhoneDeviceAccessUserName == "") {
        $deviceAccessUserNameBuilder = new DeviceNameBuilder($deviceAccessUserName2,
            getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
            getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
            getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
            true);
        
        if (! $deviceAccessUserNameBuilder->validate()) {
            // TODO: Implement resolution when Device name input formula has errors
        }
        
        do {
            $attr = $deviceAccessUserNameBuilder->getRequiredAttributeKey();
            $deviceAccessUserNameBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
        } while ($attr != "");
        
        $softPhoneDeviceAccessUserName = $deviceAccessUserNameBuilder->getName();
    }
    
    // Build Device Access Password - fallback definitions are based on Device Name fallback
    // ----------------------------------------------------------------------------------------
    $deviceAccessPasswordBuilder = new DeviceNameBuilder($deviceAccessPassword,
        getResourceForDeviceNameCreation(ResourceNameBuilder::DN),
        getResourceForDeviceNameCreation(ResourceNameBuilder::EXT),
        getResourceForDeviceNameCreation(ResourceNameBuilder::GRP_DN),
        true);
    
    if (! $deviceAccessPasswordBuilder->validate()) {
        // TODO: Implement resolution when Device name input formula has errors
    }
    
    do {
        $attr = $deviceAccessPasswordBuilder->getRequiredAttributeKey();
        $deviceAccessPasswordBuilder->setRequiredAttribute($attr, getResourceForDeviceNameCreation($attr));
    } while ($attr != "");
    
    $softPhoneDeviceAccessPassword = $deviceAccessPasswordBuilder->getName();
    
    $softPhoneArray["deviceAccessPassword"] = $softPhoneDeviceAccessPassword;
    
    if ($_POST["softPhoneDeviceAccessUserName"] !== "" and $_POST["softPhoneDeviceAccessPassword"] !== "")
    {
        $softPhoneDeviceAccessUserName = $_POST["softPhoneDeviceAccessUserName"];
        $softPhoneDeviceAccessPassword = $_POST["softPhoneDeviceAccessPassword"];
    }
    
    $softPhoneArray["deviceAccessUserName"] = $softPhoneDeviceAccessUserName."-softphone";
    $softPhoneArray["deviceAccessPassword"] = $softPhoneDeviceAccessPassword;
    
    //LinePort creation
    $linePort = "";
    $linePortBuilder1 = new LineportNameBuilder($softPhoneLinePortCriteria1,
        getResourceForLinePortCreation(ResourceNameBuilder::DN),
        getResourceForLinePortCreation(ResourceNameBuilder::EXT),
        getResourceForLinePortCreation(ResourceNameBuilder::USR_CLID),
        getResourceForLinePortCreation(ResourceNameBuilder::GRP_DN),
        getResourceForLinePortCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
        $softPhoneLinePortCriteria2 == "");  // force fallback only if second criteria does not exist
        
        if (! $linePortBuilder1->validate()) {
            // TODO: Implement resolution when lineport input formula has errors
        }
        
        do {
            $attr = $linePortBuilder1->getRequiredAttributeKey();
            $linePortBuilder1->setRequiredAttribute($attr, getResourceForLinePortCreation($attr));
        } while ($attr != "");
        
        $linePort = $linePortBuilder1->getName();
        
        if ($linePort == "") {
            // Create lineport name builder from the second formula. At this point we know that there is second
            // formula, because without second formula name would be forcefully resolved in the first formula.
            
            $linePortBuilder2 = new LineportNameBuilder($softPhoneLinePortCriteria2,
                getResourceForLinePortCreation(ResourceNameBuilder::DN),
                getResourceForLinePortCreation(ResourceNameBuilder::EXT),
                getResourceForLinePortCreation(ResourceNameBuilder::USR_CLID),
                getResourceForLinePortCreation(ResourceNameBuilder::GRP_DN),
                getResourceForLinePortCreation(ResourceNameBuilder::SYS_DFLT_DOMAIN),
                true);
            
            if (! $linePortBuilder2->validate()) {
                // TODO: Implement resolution when User ID input formula has errors
            }
            
            do {
                $attr = $linePortBuilder2->getRequiredAttributeKey();
                $linePortBuilder2->setRequiredAttribute($attr, getResourceForLinePortCreation($attr));
            } while ($attr != "");
            
            $linePort = $linePortBuilder2->getName();
        }
        
        $softPhoneLinePort = str_replace(" ", "_", $linePort);
        
        $softPhoneArray["linePort"] = $softPhoneLinePort;
        
        $softPhoneArray["cpsArray"]["groupName"] = $counterpathGroupName;
        $softPhoneArray["cpsArray"]["userName"] = $_POST["accountUserName"];
        $softPhoneArray["cpsArray"]["profileName"] = $_POST["accountProfile"];
        $softPhoneArray["cpsArray"]["userEmailAddress"] = $_POST["accountEmailAddress"];
        $softPhoneArray["cpsArray"]["accountName"] = $softPhoneDeviceNameWithoutSpace;
        //$softPhoneArray["cpsArray"]["authorizationName"] = $_SESSION["userId"];
        $softPhoneArray["cpsArray"]["authorizationName"] = $uidWithDomain1[0];
        
        //$softPhoneArray["cpsArray"]["password"] = $softPhoneDeviceAccessPassword;
        $softPhoneArray["cpsArray"]["password"] = $_SESSION["authPass"];
        $softPhoneArray["cpsArray"]["crndusername"] = $softPhoneLinePort;
        $softPhoneArray["cpsArray"]["domain"] = $_POST["softPhoneDomain"];
        $softPhoneArray["cpsArray"]["proxy"] = $systemDefaultTagVal;
        
        if(!empty($softPhoneArray["deviceName"])){
            $devAddResp = $deviceObjSoftPhone->deviceAddRequest($softPhoneArray);
            
            if(!empty($devAddResp["Error"])){
                $data1 .= $_SESSION["softPhoneDeviceName"] . " was attempted to be built but failed.";
                $data1 .= "%0D%0A%0D%0A" . $devAddResp["Error"];
                
                $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Error";
                $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["ErrorMsg"] = $data1;
                $softPhoneRollBack["deviceAdd"]["status"] = "failed";
                $softPhoneRollBack["deviceAdd"]["deviceName"] = $softPhoneArray["deviceName"];
                
            }else{
                $softPhoneRollBack["deviceAdd"]["status"] = "added";
                $softPhoneRollBack["deviceAdd"]["deviceName"] = $softPhoneArray["deviceName"];
                $addDeviceAsSCA = $scaOpsObj->addDeviceToUserAsSCA($softPhoneArray["userId"], $softPhoneArray["deviceName"], $softPhoneArray["linePort"], "false", "");
                $softPhoneRollBack["deviceSCAAssign"]["deviceName"] = $softPhoneArray["deviceName"];
                if(!empty($addDeviceAsSCA["Error"])){
                    $softPhoneRollBack["deviceSCAAssign"]["status"] = "failed";
                    $data1 .= $_SESSION["softPhoneDeviceName"] . " was attempted to assign as SCA Device but failed.";
                    $data1 .= "%0D%0A%0D%0A" . $addDeviceAsSCA["Error"];
                    
                    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["Status"] =  "Error";
                    $_SESSION["userAddModuleStatus"]["moduleAndStatus"][$_POST["module"]]["ErrorMsg"] = $data1;
                }
            }
            
            
        }
        
        if($_POST["softPhoneType"] == "counterPath" && !empty($softPhoneArray["deviceName"])){
            
            $deviceName = $softPhoneArray["deviceName"];
            $customTagArray["groupName"] = $softPhoneArray["cpsArray"]["groupName"];
            $customTagArray["CPUsername"] = $softPhoneArray["cpsArray"]["userName"];
            $customTagArray["profileName"] = $softPhoneArray["cpsArray"]["profileName"];
            $customTagArray["ctrpathuserEmailAddress"] = $_POST["accountEmailAddress"];
            
            foreach($customTagArray as $keyCpsRow => $valCpsRow){
                $tagName     = "%".$keyCpsRow."%";
                $tagValue    = $valCpsRow;
                $customTagResultResponse = $objCustomTagMgMt->addDeviceCustomTagDuplicate($deviceName, $tagName, $tagValue);
            }
            
        }
        
        //rebuildAndResetDeviceConfig($softPhoneArray["deviceName"], "rebuildAndReset");
        $deviceRebuildResp = $rebuldSoftPhoneDevice->GroupCPEConfigRebuildDeviceConfig($_SESSION["sp"], $_SESSION["groupId"], $softPhoneArray["deviceName"], "true");
        if($deviceRebuildResp["Success"] == "Success"){
            $deviceResetResp = $rebuldSoftPhoneDevice->GroupCPEConfigResetDevice($_SESSION["sp"], $_SESSION["groupId"], $softPhoneArray["deviceName"]);
            if($deviceResetResp["Success"] == "Success"){
                if($_POST["softPhoneType"] == "counterPath"){
                    
                    $devUrl = $deviceObjSoftPhone->getDeviceFileUrl($softPhoneArray["deviceName"]);
                    
                    if(isset($softPhoneArray["deviceName"]) && $softPhoneArray["deviceName"] != ""){
                        $respVal = $objSoft->getDevicesByUsingUserId($softPhoneArray["userId"]);
                        
                        if(empty($respVal["Error"])){
                            foreach ($respVal["Success"] as $arrKey => $arrVal){
                                if($arrKey == $softPhoneArray["deviceName"]){
                                    $softPhoneUserName1 = $arrVal["userName"];
                                    $softPhonePassword1 = $arrVal["password"];
                                }
                                
                            }
                        }
                        
                    }
                    
                    $filePath = $devUrl["Success"];
                    //preg_match("/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/", $filePath, $matches);
                    //$findIPFromStr = $matches[0];
                    
                    $privateIp = $xspPrivateIP;
                    
                    //$privateIp      = "10.100.10.207";
                    //$changeURL = str_replace($findIPFromStr, $privateIp, $filePath);
                    
                    //$var1 = "http://69.169.36.217:80/dms/Counterpath_Bria_-_Desktop/configuration.txt";
                    $findIPFromStr = parse_url($filePath, PHP_URL_HOST);
                    //$privateIp      = "10.100.10.207";
                    $changeURL = str_replace($findIPFromStr, $privateIp, $filePath);
                    //print_r($softPhoneUserName1);
                    //print_r($softPhonePassword1);
                    //print_r($changeURL);
                    $valRes = getRecursiveData($changeURL, $softPhoneUserName1, $softPhonePassword1, $sleep = 0);
                    //print_r($valRes);
                    //$valRes = curlgetFile($changeURL, $softPhoneUserName1, $softPhonePassword1);
                    
                    //print_r($valRes);
                    //$valRes = curlgetFile($var1, $softPhoneUserName1, $softPhonePassword1);
                    if($valRes["status"] == "200"){
                        $info = parseConfigFile($valRes["result"]);
                        $deviceFileGet = true;
                        //print_r($info);
                    }
                    
                    $softPhoneArray["cpsArray"]["groupName"] = $counterpathGroupName;
                    $softPhoneArray["cpsArray"]["userName"] = $_POST["accountUserName"];
                    $softPhoneArray["cpsArray"]["profileName"] = $_POST["accountProfile"];
                    $softPhoneArray["cpsArray"]["userEmailAddress"] = $_POST["accountEmailAddress"];
                    $softPhoneArray["cpsArray"]["accountName"] = $softPhoneDeviceNameWithoutSpace;
                    //$softPhoneArray["cpsArray"]["authorizationName"] = $_SESSION["userId"];
                    $softPhoneArray["cpsArray"]["authorizationName"] = $info["account1Sip.credentials.authorizationName"];
                    
                    //$softPhoneArray["cpsArray"]["password"] = $softPhoneDeviceAccessPassword;
                    $softPhoneArray["cpsArray"]["password"] = $info["account1Sip.credentials.password"];
                    $softPhoneArray["cpsArray"]["crndusername"] = $info["account1Sip.credentials.username"];
                    $softPhoneArray["cpsArray"]["domain"] = $info["account1Sip.domain"];
                    $softPhoneArray["cpsArray"]["proxy"] = trim($info["account1Sip.proxy"]);
                    $_SESSION["complete"]['addConterPathData'] = $softPhoneArray["cpsArray"];
                    $_SESSION["complete"]['addConterPathData']['counterPathAcctDeviceType'] = $_POST["deviceType"];
                    $softPhoneRollBack["cps"]["uName"] = $softPhoneArray["cpsArray"]["userName"];
                    
                    
                    if($deviceFileGet){
                        $cpsResult = $cpsobj->counterPathAccountAdd($softPhoneArray, $payloadData);
                    }else{
                        $data1 .= "<li style='color:red;'>Unable to read configuration from Broadworks. Please contact support.</li>";
                    }
                    
                    //echo "1- ";print_r($cpsResult);
                    if($cpsResult["status"] == "200" && $deviceFileGet){
                        $softPhoneRollBack["cps"]["status"] = "added";
                    }else{
                        $softPhoneRollBack["cps"]["status"] = "failed";
                        $data1 .= "<li style='color:red;'>Counter Path Account ".$softPhoneArray["cpsArray"]["userName"] . " was attempted to be add but failed.</li>";
                    }
                }
            }
        }
       
        //sleep(10);
        
        if(!empty($softPhoneRollBack)){
            if($softPhoneRollBack["cps"]["status"] == "failed"){
                $scaUnAssign = $scaOpsObj->deleteDevicesFromSCA($softPhoneArray["userId"], $softPhoneArray["deviceName"], $softPhoneArray["linePort"]);
                if($scaUnAssign["Error"] == ""){
                    $data1 .= "<li style='color:red;'>".$softPhoneArray["deviceName"]." unsaaigned successfully.</li>";
                    $deviceDelResp = $deviceObjSoftPhone->getDeleteDevice($softPhoneRollBack["deviceSCAAssign"]["deviceName"], $_SESSION["sp"], $_SESSION["groupId"]);
                    $deviceFaillure = true;
                    if(!empty($deviceDelResp["Error"])){
                        $data1 .= "<li style='color:red;'>".$softPhoneArray["deviceName"]." ".$deviceDelResp["Error"].".</li>";
                    }else{
                        $data1 .= "<li style='color:red;'>".$softPhoneArray["deviceName"]." deleted successfully.</li>";
                    }
                }else{
                    $data1 .= "<li style='color:red;'>".$softPhoneArray["deviceName"]." ".$scaUnAssign["Error"].".</li>";
                }
            }
            if($softPhoneRollBack["deviceAdd"]["status"] == "failed" || $softPhoneRollBack["deviceSCAAssign"]["status"] == "failed"){
                if($_POST["softPhoneType"] == "counterPath"){
                    $cpsDelResult = $cpsobj->counterPathAccountDelete($softPhoneArray);
                    $deviceFaillure = true;
                }
                if($softPhoneRollBack["deviceSCAAssign"]["status"] == "failed"){
                    $deviceDelResp = $deviceObjSoftPhone->getDeleteDevice($softPhoneRollBack["deviceSCAAssign"]["deviceName"], $_SESSION["sp"], $_SESSION["groupId"]);
                    $deviceFaillure = true;
                }
                
            }
        }
        
        if($data1 != ""){
            echo $data1;
        }else{
            echo "Done";
        }
        //echo $data;
        $_SESSION["complete"][$_SESSION["userAddNames"]["softPhoneDeviceType"]] = $_POST["deviceType"];
        $_SESSION["complete"][$_SESSION["userAddNames"]["softPhoneDeviceName"]] = $softPhoneDeviceNameWithoutSpace;
        $_SESSION["complete"][$_SESSION["userAddNames"]["softPhoneLinePortDomain"]] = $softPhoneLinePort;
        if($_POST["softPhoneType"] == "counterPath"){
            $_SESSION["complete"][$_SESSION["userAddNames"]["accountProfile"]] = $_POST["accountProfile"];
            $_SESSION["complete"][$_SESSION["userAddNames"]["accountUserName"]] = $_POST["accountUserName"];
            $_SESSION["complete"][$_SESSION["userAddNames"]["accountEmailAddress"]] = $_POST["accountEmailAddress"];
        }

        /* Soft Phone Data for Change Log */
        $_SESSION["complete"]['addSoftPhone']['softPhoneDeviceType'] = $_POST["deviceType"];
        $_SESSION["complete"]['addSoftPhone']['softPhoneDeviceName'] = $softPhoneDeviceNameWithoutSpace;
        $_SESSION["complete"]['addSoftPhone']['softPhoneLinePortDomain'] = $softPhoneLinePort;
        
        function curlgetFile($URL, $userName, $password){
            //$URL = 'http://10.100.10.207:80/dms/Counterpath_Bria_with_Broadworks/configuration.txt';
            //$userName = "9810090020";
            //$password = "Admin123";
            $arr = array();
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$URL);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_USERPWD, "$userName:$password");
            $result=curl_exec ($ch);
            //print_r($result);
            $arr["result"]=$result;
            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
            $arr["status"]=$status_code;
            ///print_r($status_code);
            curl_close ($ch);
            return $arr;
        }
        
        function parseConfigFile($result){
            $tmpResultArr = explode("\n", $result);
            unset($tmpResultArr[0]);
            unset($tmpResultArr[1]);
            unset($tmpResultArr[2]);
            $customizeResultArr = array();
            
            $count = 0;
            
            foreach($tmpResultArr as $ky => $vl){
                if($vl!=""){
                    $tmpArr = explode("=", $vl);
                    $customizeResultArr[$tmpArr[0]]  = str_replace('"', "", $tmpArr[1]);
                }
                $count = $count + 1;
            }
            return $customizeResultArr;
        }
        
        function getRecursiveData ($changeURL, $softPhoneUserName1, $softPhonePassword1, $sleep){
            //$sleep++;
            //echo "Sleep is". $sleep."<br />";
            $valRes1 = curlgetFile($changeURL, $softPhoneUserName1, $softPhonePassword1);
            //print_r($valRes1);
            if($valRes1["result"] == "") {
                sleep(1);
                if($sleep <= 9) {
                    //echo "2229";
                    $sleep++;
                    $valRes1 = getRecursiveData($changeURL, $softPhoneUserName1, $softPhonePassword1, $sleep);
                }
                
            }
            //print_r($valRes1);
            return $valRes1;
        }
?>