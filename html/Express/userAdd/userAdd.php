<?php
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

unset($_SESSION["userAddModuleStatus"]);
unset($_SESSION["complete"]);

require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getGroupDomains.php");
require_once ("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
require_once ("/var/www/lib/broadsoft/adminPortal/customTagOperations/TagOperations.php");
require_once ("/var/www/html/Express/userAdd/UserDBOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
// echo "counterpathGroupName- ";print_r($counterpathGroupName);
// echo "softPhoneLinePortCriteria1- ";print_r($softPhoneLinePortCriteria1);
// echo "softPhoneLinePortCriteria2- ";print_r($softPhoneLinePortCriteria2);
// Code added @ 09 July 2018
/*
 * require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
 * require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
 * $grpServiceList = new Services();
 * $grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
 * $servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
 * $servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray, $client);
 * $countScaService = $grpServiceList->checkScaServiceInServicepack($servicesArray);
 * $scaServicesArr = array();
 * foreach($countScaService as $key => $scaServiceRow)
 * {
 * $scaServicesArr[] = strval($scaServiceRow->col[0]);
 * }
 */
// End Code
require_once ("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");
// Code @ 07 Sep 2018 EX-789
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
$serviceObj = new Services();
$plycmServicePacksArr = $serviceObj->servicePacksHavePolycomPhoneServices($servicePacks);
$_SESSION['plycmServicePacksArr'] = $plycmServicePacksArr;
$_SESSION["plycmServicePacksStr"] = implode(",", $plycmServicePacksArr);
$userServicesArr = $_SESSION['groupInfoData']['userServiceAuth'];
// End code

$dns = new Dns();
$availableDnsStatus = $dns->GroupDnGetListRequest($_SESSION["sp"], $_SESSION["groupId"]);
if ($availableDnsStatus["Error"] == "") {
    $availableDnsStatus = json_encode($availableDnsStatus["Success"]);
}

$uMO = new UserDBOperations();
$customProfile = $uMO->getAllPhoneProfiles();
$tagBundle = $uMO->getAllTagBundles();

const customProfileTable = "customProfiles";

const customProfileName = "customProfileName";

const customProfileType = "deviceType";

function clidNumberRequired()
{
    global $policiesClidPolicyLevel, $portalRequiresClidNumber;
    
    if ($policiesClidPolicyLevel == "Group") {
        return false;
    }
    return $portalRequiresClidNumber == "true";
}

function isDepartmentVisible()
{
    global $useDepartments, $departmentsFieldVisible;
    
    if ($useDepartments == "false") {
        return false;
    }
    
    return $departmentsFieldVisible == "true";
}

function getCustomProfiles($deviceType)
{
    global $db;
    
    $query = "select " . customProfileName . " from " . customProfileTable . " where " . customProfileType . "='" . $deviceType . "' order by " . customProfileName;
    $results = $db->query($query);
    $profiles = array();
    $i = 0;
    while ($row = $results->fetch()) {
        if (! in_array($profile = $row[customProfileName], $profiles)) {
            $profiles[$i ++] = $profile;
        }
    }
    
    return $profiles;
}

function buildDeviceTypesSelection($deviceList, $devClass)
{
    // $str = "<div class=\"dropdown-wrap\"><select name=\"deviceType\" class=\"$devClass form-control\" id=\"deviceType\" onchange=\"enableDeviceFields(this)\">";
    $str = '<div class="dropdown-wrap"><select name="deviceType" class="' . $devClass . ' form-control" id="deviceType" onChange="enableDeviceFields(this);">';
    $str .= "<option value=\"\">Select Device</option>";
    
    foreach ($deviceList as $key => $value) {
        $str .= "<option value=\"" . $value . "\">" . $value . "</option>";
    }
    
    $str .= "</select></div>";
    return $str;
}

function selectGroupDomain($securityDomainPattern)
{
    global $groupDefaultDomain, $groupDomains;
    global $proxyDomainType, $staticProxyDomain;
    
    $selectedDomain = "";
    switch ($proxyDomainType) {
        case "Static":
            $selectedDomain = $staticProxyDomain;
            break;
        case "Default":
            $selectedDomain = $groupDefaultDomain;
            break;
    }
    
    if ($selectedDomain == "") {
        $selectedDomain = $groupDefaultDomain;
    }
    
    if (count($groupDomains) > 0) {
        foreach ($groupDomains as $key => $value) {
            if (strpos($value, $securityDomainPattern) !== false) {
                unset($groupDomains[$key]);
            }
        }
    }
    $groupDomains = array_values($groupDomains);
    
    for ($i = 0; $i < count($groupDomains); $i ++) {
        echo "<option value=\"" . $groupDomains[$i] . "\"";
        if ($groupDomains[$i] == $staticProxyDomain) {
            echo " selected";
        } // Code add by Sollogics Developer @ 01-Nov-2018
else if ($groupDomains[$i] == $groupDefaultDomain) {
            echo " selected";
        }
        // end comment
        echo ">" . $groupDomains[$i];
        if ($groupDomains[$i] == $groupDefaultDomain) {
            echo " (default domain)";
        }
        echo "</option>";
    }
}

function softPhoneGroupDomain($securityDomainPattern)
{
    global $groupDefaultDomain, $groupDomains;
    global $staticProxyDomain, $softPhoneLinePortCriteria1;
    // $criteriaVar = strtoupper($softPhoneLinePortCriteria1);
    $softPhoneProxyDomainType = "";
    // echo $criteriaVar;
    if (strpos($softPhoneLinePortCriteria1, "groupProxyDomain") === false) {
        $softPhoneProxyDomainType = "Default";
    } else {
        $softPhoneProxyDomainType = "Static";
    }
    // print_r($softPhoneProxyDomainType);
    $selectedDomain = "";
    switch ($softPhoneProxyDomainType) {
        case "Static":
            $selectedDomain = $staticProxyDomain;
            break;
        case "Default":
            $selectedDomain = $groupDefaultDomain;
            break;
    }
    
    if ($selectedDomain == "") {
        $selectedDomain = $groupDefaultDomain;
    }
    
    if (count($groupDomains) > 0) {
        foreach ($groupDomains as $key => $value) {
            if (strpos($value, $securityDomainPattern) !== false) {
                unset($groupDomains[$key]);
            }
        }
    }
    $groupDomains = array_values($groupDomains);
    
    for ($i = 0; $i < count($groupDomains); $i ++) {
        echo "<option value=\"" . $groupDomains[$i] . "\"";
        if ($groupDomains[$i] == $selectedDomain) {
            echo " selected";
        }
        echo ">" . $groupDomains[$i];
        if ($groupDomains[$i] == $groupDefaultDomain) {
            echo " (default domain)";
        }
        echo "</option>";
    }
}

function selectAddressState($useGroupAddress)
{
    global $states;
    
    $selectedState = $useGroupAddress == "true" ? $_SESSION["groupInfo"]["stateOrProvince"] : "";
    
    $str = "<option value=\"\"";
    $str .= $selectedState == "" ? " selected" : "";
    $str .= "></option>";
    
    foreach ($states as $key => $value) {
        $str .= "<option value=\"" . $value . "\"";
        $str .= $selectedState == $value ? " selected" : "";
        $str .= ">" . $value . "</option>";
    }
    
    return $str;
}

function selectTimeZone($useGroupTimeZone)
{
    global $timeZones;
    
    $selectedTimeZone = $useGroupTimeZone == "true" ? $_SESSION["groupInfo"]["timeZone"] : "";
    
    $str = "<option value=\"\"";
    $str .= $selectedTimeZone == "" ? " selected" : "";
    $str .= "></option>";
    
    foreach ($timeZones as $key => $value) {
        $str .= "<option value=\"" . $value . "\"";
        $str .= $selectedTimeZone == $value ? " selected" : "";
        $str .= ">" . $value . "</option>";
    }
    
    return $str;
}

function selectNetworkClassOfService()
{
    global $networkClassesOfService, $defaultNetworkClassOfService;
    
    $str = "";
    if (count($networkClassesOfService) > 0) {
        foreach ($networkClassesOfService as $key => $value) {
            $str .= "<option value=\"" . $value . "\"";
            $str .= $defaultNetworkClassOfService == $value ? " selected" : "";
            $str .= ">" . $value;
            $str .= $defaultNetworkClassOfService == $value ? "   (default NCOS)" : "";
            $str .= "</option>";
        }
    } else {
        $str .= "<option value=\"\" selected>None</option>";
    }
    
    return $str;
}

if (! isset($_POST["customProfile"]) && $license["customProfile"] == "false") {
    $_POST["customProfile"] = "None";
}
?>
<script>
    var checkPhoneProfileOpt ="";
    $(function()
    { 
        var customProfileDivVisible = "no";  //Code added @ 13 March 2019
    });
    
function changeUserValueFirstName(firstName)
{
	 $("#callingLineIdFirstName").val(firstName);
         var bwRelVersion      = "<?php echo $ociVersion; ?>";
         var nameDialVisiblty  = "<?php echo $dialingNameFieldsVisible; ?>";         
         if(bwRelVersion!="19" && nameDialVisiblty=="true"){          
            $("#nameDialingFirstName").val(firstName);
         }
}
function changeUserValueLastName(LastName)
{
	 $("#callingLineIdLastName").val(LastName);
         var bwRelVersion      = "<?php echo $ociVersion; ?>";
         var nameDialVisiblty  = "<?php echo $dialingNameFieldsVisible; ?>";
         if(bwRelVersion!="19" && nameDialVisiblty=="true"){
            $("#nameDialingLastName").val(LastName);
         }
}

$("#activateNumberDiv").hide();
$("#activateNumberDiv").prop("disabled", true);
var availableDnsStatus = '<?php echo $availableDnsStatus; ?>';
//var preSelector = "";
    function verifySelectedServicePackOptions(sel) {debugger;
        var opts = [];
        var optsService = [];
        var opt;
        /*var len = sel.options.length;
        for (var i = 0; i < len; i++) {
            opt = sel.options[i];

            if (opt.selected) {
                opts.push(opt.value);
            }
        }*/

        $('#servicePack input:checked').each(function() {
        	opts.push($(this).attr('value'));
        });

        $('#servicePack1 input:checked').each(function() {
        	optsService.push($(this).attr('value'));
        });
        
        var vmServicePacks      = "<?php echo (isset($_SESSION["vmServicePacksFlat"]) ? $_SESSION["vmServicePacksFlat"] : ""); ?>";
        var scaServicePacks     = "<?php echo (isset($_SESSION["scaServicePacksStr"]) ? $_SESSION["scaServicePacksStr"] : ""); ?>";
        var polycomServicePacks = "<?php echo (isset($_SESSION["plycmServicePacksStr"]) ? $_SESSION["plycmServicePacksStr"] : ""); ?>";
        var vmFound             = false;
        var scaFound            = false;
        var polycomFound        = false;
        
        if ( vmServicePacks != "") 
        {
            vmServicePacks = vmServicePacks.split(",");

            for (i = 0; i < opts.length; i++) {
                for (var j = 0; j < vmServicePacks.length; j++) {
                    if (vmServicePacks[j] == opts[i]) {
                        vmFound = true;
                        break;
                    }
                }
                if (vmFound) {
                    break;
                }
            }
        }

        if (vmFound) {
            document.getElementById("voiceMessagingYes").checked = true;
            $("#errorIDVM").text("");
        } else {
        	if(jQuery.inArray("Voice Messaging User", optsService) !== -1){
        		document.getElementById("voiceMessagingYes").checked = true;
        		$("#errorIDVM").text("");
            }else{
            	document.getElementById("voiceMessagingNo").checked = true;
            }
            
        }
        
        
        //Code added @ 09 July 2018       
        if ( scaServicePacks != "") 
        {
            scaServicePacks = scaServicePacks.split(",");
            for (k = 0; k < opts.length; k++) {
                for (var l = 0; l < scaServicePacks.length; l++) {
                    if (scaServicePacks[l] == opts[k]) {
                        scaFound = true;
                        break;
                    }
                }
                if (scaFound) {
                    break;
                }
            }
        }

        if(scaFound == false){
            if(optsService != ""){
            	for (m = 0; m < optsService.length; m++) {
            		if (optsService[m].indexOf("Shared Call Appearance") >= 0){
                        scaFound = true;
                        break;
                    }
                }
            }
        }
        
        if (scaFound) {
            document.getElementById("sharedCallAppearanceYes").checked = true;
            $("#errorID").text("");
        } else {
            document.getElementById("sharedCallAppearanceNo").checked = true;
        }
        //End Code
        
        //Code added @ 07 Sep 2018       
        if ( polycomServicePacks != "") 
        {
            polycomServicePacks = polycomServicePacks.split(",");
            for (p = 0; p < opts.length; p++) {
                for (var q = 0; q < polycomServicePacks.length; q++) {
                    if (polycomServicePacks[q] == opts[p]) {
                        polycomFound = true;
                        break;
                    }
                }
                if (polycomFound) {
                    break;
                }
            }
        }
        if (polycomFound) {
            checkPhoneProfileOpt ="yes";
            //document.getElementById("polycomPhoneServicesYes").checked = true;
            if($("#checkPolicomPhoneServicesOpt").val() == "true"){
            $("#polycomPhoneServicesYes").prop("checked", "checked");
            $("#errorIDPol").text("");
                }else{
                    document.getElementById("polycomPhoneServicesNo").checked = true;
                }
                
            } else {
            checkPhoneProfileOpt ="";
        	if(jQuery.inArray("Polycom Phone Services", optsService) !== -1){
                    if($("#checkPolicomPhoneServicesOpt").val() == "true"){
                        document.getElementById("polycomPhoneServicesYes").checked = true;
                        $("#errorIDPol").text("");
                        checkPhoneProfileOpt = "yes";
                    }
                    else{
                    	checkPhoneProfileOpt ="";
                        document.getElementById("polycomPhoneServicesNo").checked = true;
                    }
                }else{
                	checkPhoneProfileOpt ="";
                    document.getElementById("polycomPhoneServicesNo").checked = true;
                }
            //document.getElementById("polycomPhoneServicesNo").checked = true;
            //$("#polycomPhoneServicesNo").prop("checked", "checked");
        }
        //End Code
    //    var deviceTypeVal = $("#deviceType").val();
    //    polycomPhoneServiceIsAssigned(deviceTypeVal);
    }
    
    
    

	function extensionBuilder(num, extLen)
	{
		var numLen = num.length;
		var ind = numLen - extLen;
		var extension = num.substring(ind);
		$("#extension").val(extension);

		//
		if(num == "") {
			$("#activateNumberDiv").hide();
			$("#activateNumberDiv").prop("disabled", true);
		}
		else {
    		availableDnsStatusParsed = JSON.parse(availableDnsStatus);
    		availableDnsStatusParsed.some(function(el){
    			if(el.phoneNumber == num) {
    				isNumberActivated = el.activated == "false" ? false : true;
    			}
    		});
    
    		$("#activateNumberDiv").show();
    		$("#activateNumberDiv").prop("disabled", false);
    		$("#activateNumber").prop("checked", isNumberActivated);
    		
		}
	}

	


	//if Polycom device is selected, automatically select Polycom phone service and prevent user from changing value
	function polycom(dev)
	{
		if (dev.search("Polycom") !== -1)
		{
			$("#polycomPhoneServicesYes").prop("checked", true);
			$("#polycomPhoneServicesNo").prop("checked", false);
			$("#polycomPhoneServicesNo").attr("disabled", "disabled");
		}
		else
		{
			$("#polycomPhoneServicesNo").removeAttr("disabled");
		}
	}

    function getRadioGroupValue(name) {
        var radioGroup = document.getElementsByName(name);
        for (var i = 0; i < radioGroup.length; i++) {
            if (radioGroup[i].checked) {
                return radioGroup[i].value;
            }
        }
        return "";
    }

	function createDeviceIndexDiv(deviceType) {
             var settings = {
                 type: "POST",
                 url: "userAdd/analogUserDo.php",
                 data: { module: "buildIndexList", deviceType: deviceType },
                 async: false
             };

             $.ajax(settings).done(function(result)
             {
                 result = result.trim();
                 if (result.slice(0, 1) == "1") {
                     alert( "Error: " + result.slice(1));
                 } else {
                     $("#deviceIndexDiv").html(result.slice(1));
                 }
             });
	}

	$("#portForSipDeviceDiv").hide();
	$("#macAddressShowHide").addClass("addMaxWithFull").removeClass("addWithPortNumber");
    function enableDeviceFields(selector) {
        $("#polycomPhoneServicesNo").prop("checked", true);
        $("#checkPolicomPhoneServicesOpt").val(false);
        document.getElementById("deviceFields").style.display = (selector.value != "") ? "block" : "none";
        document.getElementById("deviceTypeResult").value = selector.value;
        $("#customProfileDiv").hide();
        $("#tagBundleDiv").hide();
        customProfileDivVisible = "yes"; //Code added @ 13 March 2019

        var deviceType = document.getElementById("deviceTypeResult").value;
        //alert('deviceType - '+deviceType);
        
        var settings = {
            type: "POST",
            url: "userAdd/analogUserDo.php",                                            //TODO: Rename this file as it is also used to process devices other than analog
            data: { module: "buildCustomProfilesList", deviceType: deviceType },
            async: false
        };

        $.ajax(settings).done(function(result)
        {debugger;
            result = result.trim();
            if (result.slice(0, 1) == "1") {
                alert( "Error: " + result.slice(1));
            } else {
                if(result.slice(1) != ""){
                	$("#customProfileID").html(result.slice(1));
                	$("#customProfileDiv").show();
                        getTagBundlesOnDeviceChange(deviceType);
                        //getPolycom phone services
                        
                        polycomPhoneServiceIsAssigned(deviceType);
                        verifySelectedServicePackOptions();
                        
                        $("#tagBundleDiv").show();
                        customProfileDivVisible = "yes"; //Code added @ 13 March 2019
                }else{
                	$("#customProfileID").empty();
                	$("#deviceMgtTagBundle").empty();
                }
            }
        });

         if($("#checkPolicomPhoneServicesOpt").val() == "true" && checkPhoneProfileOpt == "yes"){
                document.getElementById("polycomPhoneServicesYes").checked = true;
                $("#errorIDPol").text("");
            }
        else{
            document.getElementById("polycomPhoneServicesNo").checked = true;
        }
            
           // polycomPhoneServiceIsAssigned(deviceType);
	    var settings = {
            type: "POST",
            url: "userAdd/checkDeviceSupports.php",                                            //TODO: Rename this file as it is also used to process devices other than analog
            data: { action: "checkDeviceSupport", deviceType: deviceType },
            async: false
        };

	    if(deviceType != "") {
	        $.ajax(settings).done(function(result)
	                {
	                    if(userType == "analog") {
	                    	if(result.indexOf("dynamic") != "-1"){
	                        	$("#supportPortNumberType").val("dynamic");
	                        	document.getElementById("deviceFields").style.display = "none";
	                        } else {
	                        		document.getElementById("deviceFields").style.display = "block";
	                        		createDeviceIndexDiv(deviceType);
	        						$("#portForSipDeviceDiv").hide();
	        						$("#portForSipDeviceSelect").html("");
	                        }
	                     }

	                    else if(userType == "digital") {
	                    	if(result.indexOf("static") != "-1"){
	                        	$("#supportPortNumberType").val("static");
	                        	portDropdownForSIP(deviceType);
	                        	$("#portForSipDeviceDiv").show();
	                        	$("#macAddressShowHide").addClass("addWithPortNumber").removeClass("addMaxWithFull");
	                        } else {
	                        	$("#portForSipDeviceDiv").hide();
	        					$("#portForSipDeviceSelect").html("");
	                        	$("#macAddressShowHide").addClass("addMaxWithFull").removeClass("addWithPortNumber");
	                        }
	                     }
	                    
	                });
		}
        
    }

    function portDropdownForSIP(deviceType) {
    	var phoneNumber = $("#phoneNumber").val();
        var extension = $("#extension").val();
        var settings = {
            type: "POST",
            url: "userAdd/analogUserDo.php",
            data: { module: "buildPortListForDigital", deviceType: deviceType, userType : userType, phoneNumber: phoneNumber, extension:  extension},
            async: false
        };

        $.ajax(settings).done(function(result)
        {
            result = result.trim();
            if (result.slice(0, 1) == "1") {
                alert( "Error: " + result.slice(1));
            } else {
                $("#portForSipDeviceSelect").html(result.slice(1));
            }
        });
    }
    
    function polycomPhoneServiceIsAssigned(deviceType){
    	var settings = {
                type: "POST",
                url: "userAdd/analogUserDo.php",
                //data: { deviceTypeForTagBundle: deviceType },
                data: { module: "checkDeviceSIPPolycomPhoneService", deviceTypeForSIPDeviceCheck: deviceType },
                async: false
            };
            $.ajax(settings).done(function(result) {
                result = result.trim();
                if (result.slice(0, 1) == "1") {
                    alert( "Error: " + result.slice(1));
                } else {
                    if(result.slice(1) == "true"){
                    	
                        $("#checkPolicomPhoneServicesOpt").val(true);
                        
                     // $("#polycomPhoneServicesYes").prop("checked", true);
                    }
                    else{
                        $("#checkPolicomPhoneServicesOpt").val(false);
                    //	$("#polycomPhoneServicesNo").prop("checked", true);
                    } 

                }
            });
    }

    function getTagBundlesOnDeviceChange(deviceType){
        
    	var settings = {
                type: "POST",
                url: "userAdd/analogUserDo.php",
                //data: { deviceTypeForTagBundle: deviceType },
                data: { module: "buildTagBundle", deviceTypeForTagBundle: deviceType },
                async: false
            };
            $.ajax(settings).done(function(result) {
                result = result.trim();
                if (result.slice(0, 1) == "1") {
                    alert( "Error: " + result.slice(1));
                } else {
                   
                    analogFlag = result.slice(0,1);
                    $("#deviceMgtTagBundle").html(result.slice(1));

                	
                }
            });
    }


    function processPortNumbers(selector) {
        var deviceType = document.getElementById("deviceTypeResult").value;
        var $deviceIndex = selector.options[selector.selectedIndex].value;
        var settings = {
            type: "POST",
            url: "userAdd/analogUserDo.php",
            data: { module: "buildPortList", deviceType: deviceType, deviceIndex: $deviceIndex },
            async: false
        };

        $.ajax(settings).done(function(result)
        {
            result = result.trim();
            if (result.slice(0, 1) == "1") {
                alert( "Error: " + result.slice(1));
            } else {
                $("#portNumberDiv").html(result.slice(1));
            }
        });
    }

   
    function selectDeviceTypeList(selector) {        
        var rgValue = getRadioGroupValue("userType");
		userType = selector;
		$("#macAddressShowHide").addClass("addMaxWithFull").removeClass("addWithPortNumber");
        $("#portForSipDeviceDiv").hide();
        $("#portForSipDeviceSelect").html("");
        $("#deviceFields").hide();
        $("#supportPortNumberType").val("");
		
		if(selector == 'digital'){
                    $("#changImg").css('background-image','url(images/icons/digital_voip_rollover.png)');
                    $('#changImg').css('color','#ffb200');
                                $('#changImg2').css('background-image','url(images/icons/analog.png)');
                    $('#changImg2').css('color','#6ea0dc');
                    $('#changImg3').css('background-image','url(images/NewIcon/no_device_rest.png)');
                    $('#changImg3').css('color','#6ea0dc');
                    $("#useExistingDevice").prop("checked", false);
                    $("#useExistingDevice1").show();
                    $(".existingDevHide").hide();
                    $(".analogDevice").val("");
            
		}else if(selector == 'analog'){                    
             
            //   $("#deviceInformationPanel select").val('').trigger("click");
             
                    $('#changImg2').css('background-image','url(images/icons/analog_rollover.png)');
                    $('#changImg2').css('color','#ffb200');

                    $("#changImg").css('background-image','url(images/icons/digital_voip.png)');                        
                    $('#changImg').css('color','#6ea0dc');
                    $('#changImg3').css('background-image','url(images/NewIcon/no_device_rest.png)');
                    $('#changImg3').css('color','#6ea0dc'); 
                    $("#useExistingDevice").prop("checked", false);
                    $("#useExistingDevice1").hide();
                    $(".existingDevHide").hide();
                    $(".digitalDevice").val("");
            
            }else if(selector == 'None') {
                rgValue = "None";
                var deviceTypeDivVisible = "None"; //Code added @ 13 March 2019 because Device Type Div was not hidding on click on No Device after the openning the Analog and Digital device
                $('#changImg3').css('background-image','url(images/NewIcon/no_device_rollover.png)');
                $('#changImg3').css('color','#ffb200');
            
                $("#changImg").css('background-image','url(images/icons/digital_voip.png)');
                $('#changImg2').css('background-image','url(images/icons/analog.png)');                        
                $('#changImg').css('color','#6ea0dc');
                $('#changImg2').css('color','#6ea0dc');  
                $("#useExistingDevice").prop("checked", false);
                $("#useExistingDevice1").hide();
                $(".existingDevHide").hide(); 
                
                $(".digitalDevice").val("");
                $(".analogDevice").val("");
            }
		
        //$(".digitalDevice").val("");
        //$(".analogDevice").val("");
        
        if(window.customProfileDivVisible == "yes"){
            $(".digitalDevice").trigger("change");
        }
        
       // $(".analogDevice").val("change");
        var rgValue = getRadioGroupValue("userType");
        $("#userTypeNew").val(userType);
        document.getElementById("deviceTypeVisible").style.display  = (deviceTypeDivVisible == "None") ? "None" : "Block";  //Code added @ 13 March 2019 because Device Type Div was not hidding on click on No Device after the openning the Analog and Digital device
        document.getElementById("deviceTypeDigitalVoIP").style.display  = (selector == "digital") ? "block" : "none";
        document.getElementById("deviceTypeAnalogDevice").style.display = (selector == "analog")  ? "block" : "none";
        document.getElementById("deviceIndexGroup").style.display = (selector == "analog")  ? "block" : "none";
        document.getElementById("portNumberGroup").style.display = (selector == "analog")  ? "block" : "none";
        if(selector == "analog"){
			$("#generateAccordion2").hide();
			$("#softPhoneDeviceType").val("");
			$("#softPhoneDeviceAccessUserName").val("");
			$("#softPhoneDeviceAccessPassword").val("");
			$("#accountUserName").val("");
			$("#accountEmailAddress").val("");
			$("#softPhoneDeviceType").trigger("change");
			
		}else{
			$("#generateAccordion2").show();
		}
    }
	
	$("#softPhoneDeviceType").on("change", function(){
		var deviceTypeNameValue = document.getElementById("softPhoneDeviceType").value;
        if(deviceTypeNameValue){
        	var softPType = $("#softPhoneDeviceType").find(':selected').data('type');
        	$("#softPhoneType").val(softPType);
        	$("#showSoftPhoneFildsChk").show();
        	if(softPType == "counterPath"){
        		$(".genericHide").show();
            }else{
            	$(".genericHide").hide();
            }
            $("#softPhoneCheckbox").prop("checked", true);
 		}else{
 			$("#showSoftPhoneFildsChk").hide();
 			$("#softPhoneType").val("");
 			$("#softPhoneCheckbox").prop("checked", false);
 			$(".genericHide").hide();
 	 	} 
	});


    $(function()
	{
    	$(".existingDevHide").hide();
    	$("#useExistingDevice").change(function(){debugger;
        	if($('#useExistingDevice').is(":checked")){
            	$(".existingDevHide").show();
            }else{
            	$(".existingDevHide").hide();
            }
       	});
    	
    	/*counterPath soft phone */        
    	 //$("#hideSoftPhoneDeviceType").hide();
		 $("#showSoftPhoneFildsChk").hide();
		 $(".genericHide").hide();
		 //$("#hideSoftPhoneUserNamePassword").hide();
    	
    	/*end counter path soft phone */
    	$("#deviceTypeVisible").hide();
    	
        $("#module").html("> Add User");
		$("#endUserId").html("");

		$("#phoneNumber").change(function()
		{
			$("#callerId").val($("#phoneNumber").val());
		});
		
		//$("#changImg").attr('src','./images/icons/digital_voip_rollover.png');
		//$("#digiVoipText").addClass('active');
			
		var userType = "digital";
		
		$("#dialog").dialog(
		{
			autoOpen: false,
			width: 800,
			modal: true,
			position: { my: "top", at: "top" },
			resizable: false,
 			closeOnEscape: false,
			buttons: {
				"Complete": function() {
					pendingProcess.push("Add User");
					var dataToSend = $("form#userAdd").serializeArray();
					$.ajax({
						type: "POST",
						url: "userAdd/userAddDo.php",
						data: dataToSend,
                                                /*beforeSend: function(){
                                                    //$("#dialog").dialog("close");
                                                    $("#dialog").html("<div>Please wait....User creation is in Progress. &nbsp;&nbsp;<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
                                                    $(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
                                                    $(".ui-dialog-buttonpane button:contains('Cancel')").button().hide();
                                                    $("html, body").animate({ scrollTop: 0 }, 600);
                                                },*/
						success: function(result) {
// 							alert("11111");
// 							console.log(result);
// 							if(foundServerConErrorOnProcess(result, "Add User")) {
// 	        					return;
// 	                      	}
							$("#dialog").dialog("open");
							$("#dialog").dialog("option", "title", "Request Complete");
							$(".ui-dialog-titlebar-close", this.parentNode).hide();
							$(".ui-dialog-buttonpane", this.parentNode).hide();
							$("#dialog").html(result);
							//$(":button:contains('Add Another User')").addClass("subButton");
							//$(":button:contains('Return to Main')").addClass("cancelButton");
						//	$(".ui-dialog-buttonpane button:contains('Complete')").button().hide().addClass('subButton');;
						//	$(".ui-dialog-buttonpane button:contains('Cancel')").button().addClass('cancelButton').hide();
						//	$(".ui-dialog-buttonpane button:contains('Add Another User')").button().show().addClass('subButton');
						//	$(".ui-dialog-buttonpane button:contains('Return to Main')").button().show().addClass('returnToMainButton');;
						}
					});
				},
				"Cancel": function() {
					$(this).dialog("close");
				},
			"Return to Main": function() {
	                location.href="main.php";
			},
				"Add Another User": function() {
					$(this).dialog("close");
					$("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
					$("#loading").show();
					$.ajax({
						type: "POST",
						url: "navigate.php",
						data: { module: 'userAdd' },
						success: function(result)
						{
							$("#loading").hide();
							$("#mainBody").html(result);
							$(":button:contains('Complete')").show().addClass('subButton');
                            $(":button:contains('Cancel')").show().addClass('cancelButton');
						}
					});
					$(".ui-dialog-titlebar-close", this.parentNode).show();
					$(".ui-dialog-buttonpane", this.parentNode).show();


 				}
			},
			open: function() {
				setDialogDayNightMode($(this));
            }
		});

	 	$("#subButton").click(function()
	 	{
	 		$(":button:contains('Complete')").show();
			$(":button:contains('Add Another User')").hide();
			$(":button:contains('Return to Main')").hide();
			$("html, body").animate({ scrollTop: 0 }, 0);
			var dataToSend = $("form#userAdd").serializeArray();
			$.ajax({
				type: "POST",
				url: "userAdd/checkData.php",
				data: dataToSend,
				success: function(result) {
					$("html, body").animate({ scrollTop: 0 }, 0);
					$("#dialog").dialog("open");
					$("#dialog").dialog("option", "title", "Confirm Settings");
					if (result.slice(0, 1) == "1")
					{
						$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled subButton");
						$(":button:contains('Complete')").show();
						$(":button:contains('Cancel')").show().addClass("cancelButton");
						$(".ui-dialog-titlebar-close", this.parentNode).show();
					}
					else
					{
						$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled").addClass("subButton");
						$(":button:contains('Cancel')").show().addClass('cancelButton');
					 
					}
					//$(":button:contains('Add Another User')").hide();
// 					$(":button:contains('Return to Main')").hide();
					$("#dialog").html(result.slice(1));
				}
			});
		});
                
                
                
                


	 	 
	 	
// 	 	$(document).on("change", "#customProfile", function()
// 		{
// 			var value = $(this).val();
// 		if( $("#deviceType").val().indexOf("Polycom") == -1) {
// 		} else {
// 			if(value == "None") {
// 				var selectHtml = "";
// 			} else {
// 				$.ajax({
// 						'type': 'POST',
// 						'url': 'userAdd/TagOperations.php',
// 						'data': {phoneProfile : value},
// 						 success: function(result) {
// 							var result = JSON.parse(result);
// 							var selectHtml = "";
// 							$.each(result, function (index, value) {
// 								selectHtml += '<option value="'+value+'">'+value+'</option>'; 
// 							});
// 							$("#deviceMgtTagBundle").html(selectHtml);
							
// 						}
// 					});
// 			}

// // 			$("#deviceMgtTagBundle").html(selectHtml);
// 		}
		
// 		});


		
	 	/*$("#phoneNumber").change(function(){
			var el = $(this);
			var phoneValue = el.val();
			
			if(phoneValue == ""){
				$("#divActivateNumber").hide();
				$("#uActivateNumber").prop('checked', false);
			}else{
				$("#divActivateNumber").show();
			}
		});*/

                //Code added @ 28 June 2018 to select Service Pack if that have VM service
        $("#voiceMessagingYes").click(function()
	 	{    
        	var userServicePacks = [];
            $.each($("#servicePack input[type=checkbox]:checked"), function(){
                userServicePacks.push($(this).val());
            });                                    
        	$.ajax({
				type: "POST",
				url: "userAdd/checkServicePackWithVMService.php",
				data: { userServicePacks: userServicePacks },
				success: function(result) 
                {debugger;
                    if(result!="NONE")
                    {
                        /*$.each($("#servicePack"), function(){ 
                        	$('#servicePack input[type=checkbox][value='+result+']').prop('checked', true);
                        });*/
                    }else{
                    	var optsService1 = [];
                    	var vmServiceFound = false;
                    	$('#servicePack1 input:checked').each(function() {
                        	optsService1.push($(this).attr('value'));
                        });
                        if(jQuery.inArray("Voice Messaging User", optsService1) !== -1){
                        	vmServiceFound = true;
                        	/*$.each($("#servicePack1"), function(){ 
                            	$('#servicePack1 input[type=checkbox][value="Voice Messaging User"]').prop('checked', true);
                            });*/
                        }

                        if(vmServiceFound == false){
                            $("#errorIDVM").text("A Service Pack with 'Voice Messaging User' service, or 'Voice Messaging User' User Service must be selected to enable this service.");
                            document.getElementById("voiceMessagingNo").checked = true;
                        }else{
                        	$("#errorIDVM").text("");
                        	
                        }
                    }
				}
			});
		});

		$("#polycomPhoneServicesYes").click(function()
			 	{    
		        	var userServicePacks2 = [];
		            $.each($("#servicePack input[type=checkbox]:checked"), function(){
		                userServicePacks2.push($(this).val());
		            });                                    
		        	$.ajax({
						type: "POST",
						url: "userAdd/checkServicePackWithPolycomService.php",
						data: { userServicePacksPol: userServicePacks2 },
						success: function(result) 
		                {debugger;
		                    if(result!="NONE")
		                    {
		                        
		                    }else{
		                    	var optsService1 = [];
		                    	var polycomServiceFound = false;
		                    	$('#servicePack1 input:checked').each(function() {
		                        	optsService1.push($(this).attr('value'));
		                        });
		                        if(jQuery.inArray("Polycom Phone Services", optsService1) !== -1){
		                        	polycomServiceFound = true;
		                        	/*$.each($("#servicePack1"), function(){ 
		                            	$('#servicePack1 input[type=checkbox][value="Voice Messaging User"]').prop('checked', true);
		                            });*/
		                        }

		                        if(polycomServiceFound == false){
		                            $("#errorIDPol").text("A Service Pack with 'Polycom Phone Services' service, or 'Polycom Phone Services' User Service must be selected to enable this service.");
		                            document.getElementById("polycomPhoneServicesNo").checked = true;
		                        }else{
		                        	$("#errorIDPol").text("");
		                        	
		                        }
		                    }
						}
					});
				});
                
                
                //Code added @ 28 June 2018 to Un select Service Pack if that have only 1 VM service
        /*$("#voiceMessagingNo").click(function()
	 	{         
            var userServicePacks = [];
            $.each($("#servicePack input[type=checkbox]:checked"), function(){
                userServicePacks.push($(this).val());
            });
                                    
            $.ajax({
    			type: "POST",
    			url: "userAdd/checkServicePackWithVMServiceOnly.php",
                data: { userServicePacks: userServicePacks },
    			success: function(result) 
                {     
                    if(result!="NONE")
                    {                                            
                        $.each($("#servicePack input[type=checkbox]:checked"), function(){ 
                        	$('#servicePack input[type=checkbox][value='+result+']').prop('checked', false);
                        });
                    }else{
                    	var optsService2 = [];
                    	$('#servicePack1 input').each(function() {
                        	optsService2.push($(this).attr('value'));
                        });
                        if(jQuery.inArray("Voice Messaging User", optsService2) !== -1){
                        	$.each($("#servicePack1"), function(){ 
                            	$('#servicePack1 input[type=checkbox][value="Voice Messaging User"]').prop('checked', false);
                            });
                        }
                    }
                                
    			}
    		});
		});*/
                
                //Code added @ 09 July 2018 to select Service Pack if that have SCA service
        $("#sharedCallAppearanceYes").click(function()
	 	{ 
        	var userSelectedServicePacks1 = [];
        	$.each($("#servicePack input[type=checkbox]:checked"), function(){
                userSelectedServicePacks1.push($(this).val());
            });     
        	$.ajax({
				type: "POST",
				url: "userAdd/checkServicePackWithSCAService.php",
                data: {userSelectedServicePacks1 : userSelectedServicePacks1, act: "selectSCAOnAdd" },
				success: function(result) 
                    {             
                        if(result!="NONE")
                        {
                            /*$.each($("#servicePack"), function(){ 
                            	$('#servicePack input[type=checkbox][value='+result+']').attr('checked', true);
                            });*/
                        }else{
                        	var optsService3 = [];
                        	var scaServiceFound = false;
                        	$('#servicePack1 input:checked').each(function() {
                            	optsService3.push($(this).attr('value'));
                            });


                        	if(optsService3 != ""){
                            	for (n = 0; n < optsService3.length; n++) {
                            		if (optsService3[n].indexOf("Shared Call Appearance") >= 0){
                            			scaServiceFound = true;
                            			//$.each($("#servicePack1"), function(){ 
                                        	//$('#servicePack1 input[type=checkbox][value="'+optsService3[n]+'"]').prop('checked', true);
                                       //});
                                        break;
                                    }
                                }
                            }
                            if(scaServiceFound == false){
                                $("#errorID").text("A Service Pack with a SCA service, or any SCA User Service must be selected to enable this service.");
                                document.getElementById("sharedCallAppearanceNo").checked = true;
                            }else{
                            	$("#errorID").text("");
                            	
                            }
                       }
					}
			});
		});
                
        /*$("#sharedCallAppearanceNo").click(function()
	 	{         
            var userSelectedServicePacks = [];
            $.each($("#servicePack option:selected"), function(){
                userSelectedServicePacks.push($(this).val());
            });
                                    
            $.ajax({
    			type: "POST",
    			url: "userAdd/checkServicePackWithSCAService.php",
                data: { userSelectedServicePacks: userSelectedServicePacks, act: "unSelectSCAOnAdd"  },
    			success: function(result){     
                    if(result!="NONE")
                    {                       
                        $.each($("#servicePack option:selected"), function(){ 
                        	$('#servicePack input[type=checkbox][value='+result+']').attr('checked', false);
                        });
                    }else{
                    	var optsService4 = [];
                    	$('#servicePack1 input').each(function() {
                        	optsService4.push($(this).attr('value'));
                        });
                    	if(optsService4 != ""){
                        	for (n = 0; n < optsService4.length; n++) {
                        		if (optsService4[n].indexOf("Shared Call Appearance") >= 0){
                        			$.each($("#servicePack1"), function(){ 
                                    	$('#servicePack1 input[type=checkbox][value="'+optsService4[n]+'"]').prop('checked', false);
                                    
                                    });
                                }
                            }
                        }
                    }
    			}
    		});
		});*/
	});

// tooltip
$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip();   
	}); 
</script>

<style>
.addMaxWithFull {
	width: 50% !important;
}

.addWithPortNumber {
	width: 33.33333333%;
}
</style>
<?php
$_SESSION["userAddNames"] = array(
    "firstName" => "First Name",
    "lastName" => "Last Name",
    "callingLineIdFirstName" => "Calling Line ID First Name",
    "callingLineIdLastName" => "Calling Line ID Last Name",
    "nameDialingFirstName" => "Name Dialing First Name",
    "nameDialingLastName" => "Name Dialing Last Name",
    "email" => "Email Address",
    "dept" => "Department",
    "address" => "Address",
    "suite" => "Suite",
    "city" => "City",
    "state" => "State/Province",
    "zip" => "Zip/Postal Code",
    "timeZone" => "Time Zone",
    "networkClassOfService" => "Network Class of Service",
    "location" => "Location",
    "language" => "Language",
    "phoneNumber" => "Phone Number",
    "callerId" => "Calling Line ID Phone Number",
    "extension" => "Extension",
    "ccd" => "Custom Contact Directory",
    "deviceType" => "Device Type",
    "deviceTypeResult" => "Device Type",
    "deviceIndex" => "Device Index",
    "portNumber" => "Port Number",
    "portNumberSIP" => "Port Number",
    "macAddress" => "MAC Address",
    "customProfile" => "Custom Profile",
    "proxyDomain" => "Lineport (Proxy) Domain",
    "servicePack" => "Service Pack",
    "thirdParty" => "Third-Party Voice Mail",
    "deviceAccessUserName" => "Device Access User Name",
    "deviceAccessPassword" => "Device Access Password",
    "voiceMessaging" => "Voice Messaging",
    "sharedCallAppearance" => "Shared Call Appearance",
    "polycomPhoneServices" => "Polycom Phone Services",
    "callingLineIdPolicy" => "Calling Line ID Policy",
    "deviceName" => "Device Name",
    "web" => "Web Password",
    "portal" => "Voice Mail Password",
    "groupCallPickupList" => "Group Call Pickup",
    "deleteTagList" => "Tag will be deleted.",
    // "addTagList" => "Tag will be added.",
    "deviceMgtTagBundle" => "Device Management Tag Bundles",
    "activateNumber" => "Activate Number",
    "softPhoneDeviceType" => "Soft Phone Device Type",
    "softPhoneDomain" => "Soft Phone Line/Port Domain",
    "accountUserName" => "Account User Name",
    "accountProfile" => "Account Profile",
    "accountEmailAddress" => "Account Email Address",
    "softPhoneDeviceAccessUserName" => "Soft Phone Device Access UserName",
    "softPhoneDeviceAccessPassword" => "Soft Phone Device Access Password",
    "softPhone" => "Soft Phone Creation",
    "softPhoneDeviceName" => "Soft Phone Device name",
    "softPhoneLinePortDomain" => "Soft Phone LinePort",
    "userService" => "User Services"
);

require_once ("/var/www/lib/broadsoft/adminPortal/arrays.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getAvailableNumbers.php");
require_once ("/var/www/lib/broadsoft/adminPortal/allNumbers.php");
// require_once ("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getStates.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getTimeZones.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getNetworkClassesOfService.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getLanguages.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getDepartments.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getDevices.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getAllCustomContactDirectories.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getThirdPartyVoiceMailSupport.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getGroupCallPickupRequest.php");
require_once ("/var/www/html/Express/util/getAuthorizeDeviceType.php");

$authDeviceTypeInstance = new GetAuthorizeDeviceType();

$extLen = $_SESSION['groupExtensionLength']['default'];

// Build devices lists: digitalVoIPDevices and analogGatewayDevices
/*
 * $deviceTypesDigitalVoIP = array ();
 * $deviceTypesAnalogGateways = array ();
 * $sipGatewayLookup = new DBLookup ( $db, "devices", "deviceName", "sipGateway" );
 * $sipSoftPhoneLookup = new DBLookup ( $db, "devices", "deviceName", "softPhoneType" );
 */
// Get Custom Devices List from DB.
// If the list exists, only devices from the list will be available in drop-down for assignment.
/*
 * $customDevices = array ();
 * if ($useCustomDeviceList == "true") {
 * $query = "select deviceName from customDeviceList";
 * $result = $db->query ( $query );
 *
 * while ( $row = $result->fetch () ) {
 * $customDevices [] = $row ["deviceName"];
 * }
 * }
 */
// $deviceTypesAnalogGateways = $authDeviceTypeInstance->getAnalogDeviceType($nonObsoleteDevices, $sipGatewayLookup, $customDevices, $sipSoftPhoneLookup, $useCustomDeviceList);

// $deviceTypesDigitalVoIP = $authDeviceTypeInstance->getDigitalDeviceType($nonObsoleteDevices, $sipGatewayLookup, $customDevices, $sipSoftPhoneLookup, $useCustomDeviceList);

// $deviceTypesSoftPhones = $authDeviceTypeInstance->getSoftPhoneDeviceType($nonObsoleteDevices, $sipGatewayLookup, $customDevices, $sipSoftPhoneLookup, $useCustomDeviceList);
/*
 * foreach ( $nonObsoleteDevices as $key => $value ) {
 * // shortcut custom device list for Audio Codes until they are resolved
 * $deviceIsAudioCodes = substr ( $value, 0, strlen ( "AudioCodes-" ) ) == "AudioCodes-";
 *
 * if ($useCustomDeviceList == "true" && ! in_array ( $value, $customDevices )) {
 * if (! $deviceIsAudioCodes) {
 * continue;
 * }
 * }
 * if ($sipGatewayLookup->get ( $value ) == "true" || $deviceIsAudioCodes) {
 * $deviceTypesAnalogGateways [] = $value;
 * } else {
 * $deviceTypesDigitalVoIP [] = $value;
 * }
 * }
 */
require_once ("/var/www/html/Express/util/getAuthorizeDeviceTypes.php");
?>

<script>
$(function(){ 
	var loadExtensionAuto = function(){
		var phoneNumber = $("#phoneNumber").val(); 
		if(phoneNumber != ""){
			$("#phoneNumber").trigger("change");
		}
	}
	loadExtensionAuto();
});
$(document).ready(function () {
		//$('img.rolloverImg').hover(sourceSwap, sourceSwap); //Commented By Anshu 01 Feb 2018 Due to conflict of Hover and Selected image
});

</script>

<h2 class="addUserText">
	Add User<a style="margin-left: 5px; vertical-align: text-top;"
		class="customTooltip" data-toggle="tooltip" data-placement="top"
		title='Choose device: "SIP phone/Analog" or "No Device" for users without a device.'><img
		src="images/NewIcon/info_icon.png"></a>
</h2>

<div style="" class="icons-div userAdd-icons-div">
	<ul style="" class="feature-list">
		<li class="changeOnMouseHover">
			<div class="mhover" onclick="selectDeviceTypeList('digital')">
				<div id="changImg">SIP Phone</div>
			</div>
		</li>
		<li>
			<div class="mhover2" onclick="selectDeviceTypeList('analog')">
				<div id="changImg2">Analog</div>
			</div>
		</li>
		<li>
			<div id="receptionDiv" class="mhover3"
				onclick="selectDeviceTypeList('None')">
				<div id="changImg3">No Device</div>
			</div>
		</li>
	</ul>
</div>

<form action="userAddConfirm.php" method="POST" id="userAdd"
	class="userRegisterForm">
	<div style="display: none;">
		<input type="text" autocomplete="new-password"> <input type="password"
			autocomplete="new-password">
	</div>
	<input type="hidden" name="userType" id="userTypeNew" value="None" />


	<div class="row">
		<!-- Basic Information Start -->
		<div class="row panel-group" id="generateAccordion1" role="tablist"
			aria-multiselectable="true">
			<div class="panel panel-default panel-forms generateDiv overPanel"
				id="expressSheetUsers" style="">
				<div class="panel-heading" role="tab" id="groupInfoConfigUsersTitle">
					<h4 class="panel-title">
						<a class="accordion-toggle accordion-toggle-full-width"
							role="button" data-toggle="collapse"
							data-parent="#generateAccordion1" href="#userInformationPanel"
							aria-expanded="true" aria-controls="userInformationPanel">User
							Information</a>
					</h4>
				</div>

				<div id="userInformationPanel" class="panel-collapse collapse in"
					role="tabpanel" aria-labelledby="groupInfoConfigUsersTitle"
					aria-expanded="true" style="">
					<div class="panel-body" style="padding: 0;">
						<div class="form-group"></div>
						<div class="row formWidthEx" style="display: none;">
							<div class="col-md-12">
								<div class="form-group">
									<a style="margin-left: 5px;" class="customTooltip"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Below, please configure the common fields that should apply to all User entries added."><img
										src="images/NewIcon/info_icon.png"></a>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="firstName"> First Name: </label>
										<span class="required">*</span> <input type="text"
											class="form-control" name="firstName" id="firstName"
											maxlength="30"
											onChange="changeUserValueFirstName(this.value);">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="lastName"> Last Name: </label> <span
											class="required">*</span> <input type="text" name="lastName"
											id="lastName" class="form-control" maxlength="30"
											onChange="changeUserValueLastName(this.value);">
									</div>
								</div>
							</div>
						</div>

						<div style="display: <?php echo ($clidNameFieldsVisible == "true" ? " block" : " none"); ?> " id="callingLineIdNameGroup">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="callingLineIdFirstName">
												Calling Line ID First Name: </label><span class="required">*</span>
											<input type="text" name="callingLineIdFirstName"
												class="form-control" size="35" maxlength="30"
												id="callingLineIdFirstName">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="callingLineIdLastName"> Calling
												Line ID Last Name: </label><span class="required">*</span> <input
												type="text" name="callingLineIdLastName"
												class="form-control" size="35" maxlength="30"
												id="callingLineIdLastName">
										</div>
									</div>
								</div>
							</div>
						</div>
        					
        					<?php
            $cssStyle = "display: none;";
            if ($ociVersion == "20" || $ociVersion == "21" || $ociVersion == "22") {
                if ($dialingNameFieldsVisible == "true") {
                    $cssStyle = "display: block;";
                }
            }
            ?>
                            <div style="<?php echo $cssStyle; ?>" id="nameDialingNamesDiv">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="nameDialingFirstName"><b>Name
													Dialing First Name:</b></label> <input type="text"
												name="nameDialingFirstName" id="nameDialingFirstName"
												size="35" maxlength="30">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="nameDialingLastName"><b>Name
													Dialing Last Name:</b></label> <input type="text"
												name="nameDialingLastName" id="nameDialingLastName"
												size="35" maxlength="30">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6" style="display: <?php echo ($emailFieldVisible == "true" ? " block" : " none"); ?> ">
									<div class="form-group">
										<label class="labelText" for="email"> Email Address: </label>
										<input type="text" name="email" class="form-control"
											id="email" size="40" maxlength="80">
									</div>
								</div>
								<div class="col-md-6" style="display: <?php echo (isDepartmentVisible() ? " block" : " none"); ?> ">
									<div class="form-group">
										<label class="labelText" for="dept"> Department: </label>
										<div class="dropdown-wrap">
											<select name="dept" id="dept" class="form-control">
												<option value=""></option>
                    					<?php
                                        if (isset($departments)) {
                                            foreach ($departments as $value) {
                                                echo "<option value=\"" . $value . "\"" . $sel . ">" . $value . "</option>";
                                            }
                                        }
                                        ?>
                    					</select>
										</div>
									</div>
								</div>
								<div class="col-md-6" style="display: <?php echo ($locationFieldVisible == "true" ? " block" : " none"); ?> ">
									<div class="form-group">
										<label class="labelText" for="lastName"> Location: </label> <input
											type="text" name="location" id="userLocation" size="40"
											maxlength="80" value="">
									</div>
								</div>
								
								<?php if ($portalShowLanguageField == "true") { ?>
                            		<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="language"> Language: </label>
											<div class="dropdown-wrap">
												<select name="language" id="language">
													<option value=""></option>
                                            			<?php
                                                            foreach ($languages as $key => $value) {
                                                                echo "<option value=\"" . $value . "\">" . $value . "</option>";
                                                            }
                                                            ?>
                                				</select>
											</div>
										</div>
									</div>
                               <?php } ?>
                               <?php
                                    $displayNCOS = "style='display: none;'";
                                    $networkClsOfServicePermission = $_SESSION["permissions"]["userNCOS"];
                                    if ($networkClsOfServicePermission == "1") {
                                        $displayNCOS = "style='display: block;'";
                                    }
                                ?>
                               <div class="col-md-6" <?php echo $displayNCOS; ?>>
									<div class="form-group">
										<label class="labelText" for="networkClassOfService"> Network
											Class of Service: </label>
										<div class="dropdown-wrap">
											<select class="form-control" name="networkClassOfService"
												id="networkClassOfService">
                        		            <?php echo selectNetworkClassOfService(); ?>
                        					</select>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row displayNone" style="display: <?php echo ($addressFieldsVisible == "true" ? " block" : " none"); ?> ">
							<div class="col-md-12">
								<div class="col-md-6">
									<div id="addressGroup">
										<div class="form-group">
											<label class="labelText" for="address"> Address: </label><?php if ($bwRequireAddress == "true") echo " <span class=\"required\">*</span>"; ?>
                            					<input type="text" name="address"
												maxlength="80" class="form-control" id="address"
												value="<?php echo $useGroupAddress == "true" ? $_SESSION["groupInfo"]["addressLine1"] : "";?>">
										</div>
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group">
										<label class="labelText" for="suite"> Suite: </label> <input
											type="text" name="suite" maxlength="80" id="suite"
											class="form-control"
											value="<?php echo $useGroupAddress == "true" ? $_SESSION["groupInfo"]["addressLine2"] : "";?>">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="labelText" for="city"> City: </label><?php if ($bwRequireAddress == "true") echo " <span class=\"required\">*</span>"; ?>
                            				<input type="text" name="city" id="city"
											class="form-control" maxlength="50"
											value="<?php echo $useGroupAddress == "true" ? $_SESSION["groupInfo"]["city"] : "";?>">
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div style="display: <?php echo ($addressFieldsVisible == "true" ? " block" : " none"); ?> ">
									<div class="col-md-6">
										<div class="form-group">
											<label class="labelText" for="state"> State/Province: </label><?php if ($bwRequireAddress == "true") echo " <span class=\"required\">*</span>"; ?>
                                            <select name="state"
												id="state" class="form-control"><?php echo selectAddressState($useGroupAddress); ?>  
                                            </select>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<label class="labelText" style="width: 106px" for="zip"> Zip/Postal Code: <?php if ($bwRequireAddress == "true") echo " <span class=\"required\">*</span>"; ?></label>
											<input type="text" name="zip" id="zip" maxlength="10"
												class="form-control"
												value="<?php echo $useGroupAddress == "true" ? $_SESSION["groupInfo"]["zipOrPostalCode"] : "";?>">
										</div>
									</div>
								</div>
                            
                               <?php
                            $displayStyle = "style='display: none;'";
                            $timeZonePermission = $_SESSION["permissions"]["timeZone"];
                            if ($timeZoneFieldVisible == "true" && $timeZonePermission == "1") {
                                $displayStyle = "style='display: block;'";
                            }
                            if ($addressFieldsVisible == "true") {
                                $className = "col-md-4";
                            } else {
                                $className = "col-md-6";
                            }
                            ?>
                    			<div class="<?php echo $className; ?>"
									<?php echo $displayStyle; ?>>
									<div class="form-group">
										<label class="labelText" for="timeZone"> Time Zone: </label> <span
											class="required">*</span>
										<div class="dropdown-wrap">
											<select name="timeZone" id="timeZone" class="form-control">
                    						<?php echo selectTimeZone($useGroupTimeZone); ?>
                    					</select>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="phoneNumber"> Phone Number: </label>
										<div class="dropdown-wrap">
											<select name="phoneNumber" class="form-control"
												onchange="extensionBuilder(this.value, '<?php echo $extLen; ?>')"
												id="phoneNumber">
												<option value=""></option>
                        						<?php
                                                if (isset($availableNumbers)) {
                                                    foreach ($availableNumbers as $value) {
                                                        if ($value == $_POST["phoneNumber"]) {
                                                            $sel = "SELECTED";
                                                        } else {
                                                            $sel = "";
                                                        }
                                                        echo "<option value=\"" . $value . "\"" . $sel . ">" . $value . "</option>";
                                                    }
                                                }
                                                ?>
                        					</select>
										</div>
									</div>
								</div>
                    
                              	<?php
                                $displayCallingLineIdNumber = "style='display: none;'";
                                $userCallingLineIdNumberPermission = $_SESSION["permissions"]["userCallingLineIdNumber"];
                                if ($userCallingLineIdNumberPermission == "1") {
                                    $displayCallingLineIdNumber = "style='display: block;'";
                                }
                                ?>
                    		    <div class="col-md-4"
									<?php echo $displayCallingLineIdNumber; ?>>
									<div class="form-group fullWidthDiv">
										<label class="labelText" for="callerId"> Calling Line ID Phone
											Number: </label><?php echo clidNumberRequired() ? " <span class=\"required\">*</span>" : ""; ?>
                    						<div class="dropdown-wrap">
											<select name="callerId" class="form-control" id="callerId"
												<?php echo $policiesClidPolicyLevel == "Group" ? " Disabled" : ""; ?>>
												<option value=""></option>
                    								<?php
                                                        if (isset($allNumbers)) {
                                                            foreach ($allNumbers as $value) {
                                                                echo "<option value=\"" . $value . "\">" . $value . "</option>";
                                                            }
                                                        }
                                                    ?>
                    							</select>
										</div>
									</div>
								</div>


								<div <?php if($userCallingLineIdNumberPermission == '1'){echo "class='col-md-2'"; }else{ echo "class='col-md-6'"; } ?>>
									<div class="form-group fullWidthDiv"
										style="margin: -2px 0 0 0;">
										<label class="labelText" for="extension"> Extension:</label> <span
											class="required">*</span> <input type="text" name="extension"
											value=""
											size="<?php echo $_SESSION['groupExtensionLength']['max'];?>"
											maxlength="<?php echo $_SESSION['groupExtensionLength']['max'];?>"
											class="form-control" id="extension">
									</div>
								</div>
							</div>
						</div>

						<div class="row" id="activateNumberDiv">
							<div class="col-md-12">
                        		<?php
                        if ($_SESSION["permissions"]["activateNumber"] == "1") {
                            ?>
                        		<div class="col-md-6" style="padding-left: 0;">
									<div class="col-md-6 voiceMsgTxt">
										<div class="form-group">
											<input type="hidden" name="activateNumber" value="false"> <input
												style="width: auto;" type="checkbox" name="activateNumber"
												id="activateNumber" value="true"> <label
												for="activateNumber"><span></span></label> <label
												class="labelText" for=""> Activate Number:</label>
										</div>
									</div>
								</div>
                        		<?php } ?>
                        		
							</div>
						</div>

					</div>
				</div>
			</div>



		</div>
		<!-- Basic Information End -->

		<!-- Device Information Start -->
		<div class="row panel-group" id="deviceTypeVisible" role="tablist"
			aria-multiselectable="true" style="margin-top: 5px;">
			<div class="panel panel-default panel-forms generateDiv overPanel"
				id="expressSheetUsers1" style="">
				<div class="panel-heading" role="tab"
					id="groupInfoConfigUsersTitle2">
					<h4 class="panel-title">
						<a class="accordion-toggle accordion-toggle-full-width collapsed deviceInformationPanel"
							role="button" data-toggle="collapse"
							data-parent="#deviceTypeVisible" href="#deviceInformationPanel"
							aria-expanded="true" aria-controls="deviceInformationPanel">Device</a>
					</h4>
				</div>

				<div id="deviceInformationPanel" class="panel-collapse collapse"
					role="tabpanel" aria-labelledby="groupInfoConfigUsersTitle2"
					aria-expanded="true" style="">
					<div class="panel-body" style="padding: 0;">
						<div class="form-group"></div>
						<div class="row formWidthEx" style="display: none;">
							<div class="col-md-12">
								<div class="form-group">
									<a style="margin-left: 5px;" class="customTooltip"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Below, please configure the common fields that should apply to all User entries added."><img
										src="images/NewIcon/info_icon.png"></a>
								</div>
							</div>
						</div>
						<?php if ($sharedDeviceVisible == "true"){ ?>
						<div class="row" id="useExistingDevice1">
							<div class="col-md-12">
							<div class="col-md-6">
								<div class="form-group">
									<input type="checkbox" id="useExistingDevice">
									<label for="useExistingDevice">
										<span></span>
									</label>
									<label class="labelText">Add to existing device</label>
								</div>
								</div>
							</div>
						</div>
						
						<div class="row existingDevHide">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="deviceExistingName"> Device Name: </label>
										<input type="text" name="" id="deviceExistingName" maxlength="161">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="existingDeviceMac"> Mac Address: </label> 
										<input type="text" name="" id="existingDeviceMac" size="40" maxlength="60">
									</div>
								</div>
							</div>
						</div>
						
						<div class="row existingDevHide">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="deviceExistingName1"> Line/Port Domain: </label>
										<input type="text" name="" id="deviceExistingName1" maxlength="161">
									</div>
								</div>

								<div class="col-md-6" style="display:none;">
									<div class="form-group">
										<label class="labelText" for="existingDeviceMac1"> Mac Address: </label> 
										<input type="text" name="" id="existingDeviceMac1" size="40" maxlength="60">
									</div>
								</div>
							</div>
						</div>
						
						<div class="row existingDevHide">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="deviceExistingName2"> Phone Profile: </label>
										<input type="text" name="" id="deviceExistingName2" maxlength="161">
									</div>
								</div>

								<div class="col-md-6" style="display:none;">
									<div class="form-group">
										<label class="labelText" for="existingDeviceMac2"> Mac Address: </label> 
										<input type="text" name="" id="existingDeviceMac2" size="40" maxlength="60">
									</div>
								</div>
							</div>
						</div>
						
						<div class="row existingDevHide">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="deviceExistingName3"> Device Management Tag Bundle: </label>
										<input type="text" name="" id="deviceExistingName3" maxlength="161">
									</div>
								</div>

								<div class="col-md-6" style="display:none;">
									<div class="form-group">
										<label class="labelText" for="existingDeviceMac3"> Mac Address: </label> 
										<input type="text" name="" id="existingDeviceMac3" size="40" maxlength="60">
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
						
						

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="deviceType">Device Type: <span class="required">*</span></label><!--<a
											style="margin-left: 5px;" class="customTooltip"
											data-toggle="tooltip" data-placement="top"
											title="NOTE: Not all phones listed are supported"><img
											class="infoIcon" src="images/NewIcon/info_icon.png"></a> -->
										<div class="" style="display: block"
											id="deviceTypeDigitalVoIP">
                        					 <?php echo buildDeviceTypesSelection($deviceTypesDigitalVoIP, "digitalDevice"); ?>
                        				</div>
										<div class="" style="display: none"
											id="deviceTypeAnalogDevice">
                        				   <?php echo buildDeviceTypesSelection($deviceTypesAnalogGateways, "analogDevice"); ?>
                        				</div>
									</div>
								</div>

								<div class="col-md-4" id="macAddressShowHide" style="display: <?php echo ($macAddressFieldVisible == "true" ? " block" : " none"); ?> ">
									<div class="form-group">
										<label class="labelText" for="macAddress">MAC Address:</label>
										<div>
											<input type="text" name="macAddress" id="macAddress"
												size="13" maxlength="12">
										</div>
									</div>
								</div>
								<div class="col-md-2" id="portForSipDeviceDiv">
									<div class="form-group">
										<label class="labelText" for="">Port Number:</label> <span
											class="required">*</span>
										<div class="dropdown-wrap" id="portForSipDeviceSelect">
											<select name="portNumber" class="form-control"
												id="portNumberSIP">
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="deviceTypeResult" id="deviceTypeResult"
							value="">

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="proxyDomain"> Line/Port Domain:
										</label>
										<div class="dropdown-wrap">
											<select name="proxyDomain" id="proxyDomain"
												class="form-control">
                        					<?php selectGroupDomain($securityDomainPattern); ?>
                        					</select>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div style="display: none" id="deviceFields">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-6" id="deviceIndexGroup"
										style="display: none">
										<div class="form-group">
											<label class="labelText" for="deviceIndex"> Device Instance:
											</label> <span class="required">*</span>
											<div class="dropdown-wrap" id="deviceIndexDiv">
												<select name="deviceIndex" id="deviceIndex"></select>
											</div>
										</div>
									</div>

									<div class="col-md-6" id="portNumberGroup"
										style="display: none">
										<div class="form-group">
											<label class="labelText" for="portNumber"> Port Number: </label>
											<span class="required">*</span>
											<div class="" id="portNumberDiv">
												<div class="dropdown-wrap">
													<select name="portNumber" id="portNumber"></select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
                 
                    			<?php if(isset($license["customProfile"]) && $license["customProfile"] == "true"){ ?>
                    			<div class="row form-group" id="customProfileDiv">
								<div class="col-md-12">
									<div class="col-md-6">
										<label class="labelText" for="customProfile"><b>Phone Profile:</b></label>
										<div class="dropdown-wrap" id="">
											<select name="customProfile" id="customProfileID">
											</select>
										</div>
									</div>
								</div>
							</div>

							<div class="row form-group" id="tagBundleDiv">
								<div class="col-md-12">
									<div class="col-md-12">
										<label class="labelText" for="customProfile"><b>Device
												Management Tag Bundles:</b></label> <a href="#"
											style="margin-left: 5px;" class="customTooltip"
											data-toggle="tooltip" data-placement="top"
											title="Select multiple Tag Bundles with CTRL Left Click"> <img
											class="infoIcon" src="images/NewIcon/info_icon.png"></a>
										<div class="" style="margin-left: 1px;" id="tagBundleId">
											<div class="form-control servicePackSelect"
												id="deviceMgtTagBundle"></div>
										</div>
									</div>
								</div>
							</div>
                    			<?php  } ?>
                    		</div>
                    		
                    		<?php
                    if ($portalShowDeviceFields == "true") {
                        ?>
                            <div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="deviceAccessUserName"> Device
											Access User Name: </label> <input type="text"
											name="deviceAccessUserName" id="deviceAccessUserName"
											maxlength="161">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="deviceAccessPassword"> Device
											Access Password: </label> <input type="password"
											name="deviceAccessPassword" id="deviceAccessPassword"
											size="40" maxlength="60">
									</div>
								</div>
							</div>
						</div>
                            <?php
                    }
                    ?>
        				</div>
				</div>
			</div>
		</div>
		<!-- Device information End -->

		<!-- Softphone Information Start -->
		<?php if($softPhoneVisible == "true") { ?>
                
		<div class="row panel-group" id="generateAccordion2" role="tablist"
			aria-multiselectable="true" style="margin-top: 5px;">
			<div class="panel panel-default panel-forms generateDiv overPanel"
				id="expressSheetUsers1" style="">
				<div class="panel-heading" role="tab"
					id="groupInfoConfigUsersTitle1">
					<h4 class="panel-title">
						<a class="accordion-toggle accordion-toggle-full-width collapsed"
							role="button" data-toggle="collapse"
							data-parent="#generateAccordion2"
							href="#softPhoneInformationPanel" aria-expanded="true"
							aria-controls="softPhoneInformationPanel">Soft Phone</a>
					</h4>
				</div>

				<div id="softPhoneInformationPanel" class="panel-collapse collapse"
					role="tabpanel" aria-labelledby="groupInfoConfigUsersTitle1"
					aria-expanded="true" style="">
					<div class="panel-body" style="padding: 0;">
						<div class="form-group"></div>
						<div class="row formWidthEx" style="display: none;">
							<div class="col-md-12">
								<div class="form-group">
									<a style="margin-left: 5px;" class="customTooltip"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Below, please configure the common fields that should apply to all User entries added."><img
										src="images/NewIcon/info_icon.png"></a>
								</div>
							</div>
						</div>
						<input type="checkbox" id="softPhoneCheckbox" name="softPhone">
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div id="hideSoftPhoneDeviceType">
										<div class="form-group">
											<label class="labelText" for="language">Soft Phone Device
												Type:
											</label>
											<div class="dropdown-wrap">
												<select name="softPhoneDeviceType" id="softPhoneDeviceType">
													<option value=""></option>
                        							<?php
                                                        foreach ($deviceTypesSoftPhones as $softPhoneKey => $softPhoneVal) {
                                                            echo "<option value = '" . $softPhoneVal["softDeviceType"] . "' data-type='" . $softPhoneVal["softPhoneType"] . "'>" . $softPhoneVal["softDeviceType"] . "</option>";
                                                        }
                                                    ?>
                        							</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="language">Soft Phone Line/Port
											Domain: <span class="required">*</span>
										</label>
										<div class="dropdown-wrap">
											<select name="softPhoneDomain" id="softPhoneDomain">
                            						<?php softPhoneGroupDomain($securityDomainPattern); ?>
                            					</select>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="language">Soft Phone Device
											Access UserName:</label> <input type="text"
											name="softPhoneDeviceAccessUserName"
											id="softPhoneDeviceAccessUserName" size="40" maxlength="60">
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label class="labelText" for="language">Soft Phone Device
											Access Password:</label> <input type="password"
											name="softPhoneDeviceAccessPassword"
											id="softPhoneDeviceAccessPassword" size="40" maxlength="60">
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6 genericHide">
									<div class="form-group">
										<label class="labelText" for="language">Account UserName: <span
											class="required">*</span></label> <input type="text"
											name="accountUserName" id="accountUserName" size="40"
											maxlength="60">
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6 genericHide">
									<div class="form-group">
										<label class="labelText" for="language">Account Profile:</label>
										<div class="dropdwon-wrap">
											<select name="accountProfile" id="accountProfile">
												<option value="GOLD">GOLD</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-6 genericHide">
									<div class="form-group">
										<label class="labelText" for="language">Account Email Address:</label>
										<input type="text" name="accountEmailAddress"
											id="accountEmailAddress" value="">
									</div>
								</div>
							</div>
						</div>





					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<!-- Softphone Information End -->

		<!-- Services Information Start -->
		<div class="row panel-group" id="generateAccordion3" role="tablist"
			aria-multiselectable="true" style="margin-top: 5px;">
			<div class="panel panel-default panel-forms generateDiv overPanel"
				id="expressSheetUsers3" style="">
				<div class="panel-heading" role="tab"
					id="groupInfoConfigUsersTitle2">
					<h4 class="panel-title">
						<a class="accordion-toggle accordion-toggle-full-width collapsed"
							role="button" data-toggle="collapse"
							data-parent="#generateAccordion3" href="#serviceInformationPanel"
							aria-expanded="true" aria-controls="serviceInformationPanel">Services</a>
					</h4>
				</div>


				<div id="serviceInformationPanel" class="panel-collapse collapse"
					role="tabpanel" aria-labelledby="groupInfoConfigUsersTitle2"
					aria-expanded="true" style="">
					<div class="panel-body" style="padding: 0;">
						<div class="form-group"></div>
						<div class="row formWidthEx" style="display: none;">
							<div class="col-md-12">
								<div class="form-group">
									<a style="margin-left: 5px;" class="customTooltip"
										data-toggle="tooltip" data-placement="top" title=""
										data-original-title="Below, please configure the common fields that should apply to all User entries added."><img
										src="images/NewIcon/info_icon.png"></a>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12" style="">
								<div class="form-group fullWidthDiv col-md-12">
									<label class="labelText" for="servicePack"> Service Pack: </label>
									<a style="margin-left: 5px;" class="customTooltip"
										data-toggle="tooltip" data-placement="top"
										title="Select multiple Service Packs with CTRL Left Click"><img
										class="infoIcon" src="images/NewIcon/info_icon.png"></a>
									<!--<span class="required">*</span> -->
									<input type="hidden" class="form-control" name="servicePack"
										value="">
									<div class="scrollableListCustom1 scrollableListCustom1Copy"
										style="margin-left: 1px;">

										<div id="servicePack" class="form-control servicePackSelect"
											onchange="verifySelectedServicePackOptions(this)"
											style="overflow-y: scroll; padding: 12px;">
                        					<?php
                                                if (isset($servicePacks)) {
                                                    $i = 0;
                                                    foreach ($servicePacks as $key => $value) {
                                                        $servicePackID = "servicePacksID" . $i;
                                                        echo "<div class='form-group'><input type='checkbox' name='servicePack[]' value='" . $value . "' id=" . $servicePackID . "><label for=" . $servicePackID . "><span></span></label><label class='labelText'>" . $value . "</label></div>";
                                                        $i ++;
                                                    }
                                                }
                                                ?>
                        					</div>
									</div>
								</div>
							</div>

						</div>
						<?php if($serviceAssignmentVisible == "true"){ ?>
						<div class="row">
							<div class="col-md-12" style="">
								<div class="form-group fullWidthDiv col-md-12"
									style="margin-bottom: 0;">
									<label class="labelText" for="servicePack"> Services : </label>
									<a style="margin-left: 5px;" class="customTooltip"
										data-toggle="tooltip" data-placement="top"
										title="Select multiple Service Packs with CTRL Left Click"><img
										class="infoIcon" src="images/NewIcon/info_icon.png"></a>
									<!--<span class="required">*</span> -->
									<input type="hidden" class="form-control" name="userService"
										value="">
									<div class="scrollableListCustom1 scrollableListCustom1Copy"
										style="margin-left: 1px;">

										<div id="servicePack1" class="form-control servicePackSelect"
											onchange="verifySelectedServicePackOptions(this)" style="">
                        					<?php
                                                if (isset($userServices)) {
                                                    $i = 0;
                                                    asort($userServices);
                                                    foreach ($userServices as $key => $value) {
                                                        $servicePackID = "userServiceID" . $i;
                                                        echo "<div class='form-group'><input type='checkbox' name='userService[]' value='" . $value . "' id=" . $servicePackID . "><label for=" . $servicePackID . "><span></span></label><label class='labelText'>" . $value . "</label></div>";
                                                        $i ++;
                                                    }
                                                }
                                                ?>
                        					</div>
									</div>
								</div>
							</div>

						</div>
						<?php } ?>

						<div class="row" style="height: 30px;">
							<div class="col-md-12">
								<div class="col-md-12">
									<div id="errorID"
										style="color: red; font-weight: bold; margin-bottom: 5px;"></div>
									<div id="errorIDVM"
										style="color: red; font-weight: bold; margin-bottom: 5px;">
									</div>
									<div id="errorIDPol"
										style="color: red; font-weight: bold; margin-bottom: 5px;">
									</div>
								</div>
							</div>
						</div>

						<div class="row" style="display: <?php echo ($voiceMessagingVisible == "true" ? " block" : " none"); ?> ">
							<div class="col-md-6">
								<div class="col-md-6 voiceMsgTxt">
									<div class="form-group">
										<label class="labelText" style="margin-left: 0;" for=""> Voice
											Messaging:</label>
									</div>
								</div>
								<div class="col-md-6 voiceMsgRBtn">
									<div class="col-md-6 rBtnYes">
										<div class="form-group">
											<input type="radio" name="voiceMessaging"
												id="voiceMessagingYes" value="Yes" style=""> <label
												class="labelText labelTextMargin" for="voiceMessagingYes"><span></span>Yes</label>

										</div>
									</div>

									<div class="col-md-6 rBtnNo">
										<div class="form-group">
											<input type="radio" name="voiceMessaging"
												id="voiceMessagingNo" value="No" style="" checked> <label
												class="labelText labelTextMargin" for="voiceMessagingNo"><span></span>No</label>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="col-md-6 voiceMsgTxt" style="padding-left: 0;">
									<div class="form-group">
										<label class="labelText" style="margin-left: 0;" for="">Shared
											Call Appearance:</label>
									</div>
								</div>
								<div class="col-md-6 voiceMsgRBtn">
									<div class="col-md-6 rBtnYes">
										<div class="form-group">
											<input type="radio" name="sharedCallAppearance"
												id="sharedCallAppearanceYes" value="Yes" style=""> <label
												class="labelText labelTextMargin"
												for="sharedCallAppearanceYes"><span></span>Yes</label>
										</div>
									</div>
									<div class="form-group">
										<input type="radio" name="sharedCallAppearance"
											id="sharedCallAppearanceNo" value="No" style="" checked> <label
											class="labelText labelTextMargin"
											for="sharedCallAppearanceNo"><span></span>No</label>
									</div>
								</div>
							</div>
						</div>


						<div class="row">
                        		<?php if ($portalShowClidFields == "true") { ?>
                        		<div class="col-md-6">
								<div class="col-md-6 voiceMsgTxt">
									<div class="form-group">
										<label class="labelText" style="margin-left: 0;" for="">
											Calling Line ID Policy:</label>
									</div>
								</div>
								<div class="col-md-6 voiceMsgRBtn">
									<div class="col-md-6 rBtnYes">
										<div class="form-group">
											<input type="radio" name="callingLineIdPolicy"
												id="callingLineIdPolicyUser" value="User" style=""
												<?php echo $policiesClidPolicyLevel == "User" ? "checked" : ""; ?>>
											<label class="labelText labelTextMargin"
												for="callingLineIdPolicyUser"><span></span>User</label>
										</div>
									</div>

									<div class="col-md-6 rBtnNo">
										<div class="form-group">
											<input type="radio" name="callingLineIdPolicy"
												id="callingLineIdPolicyGroup" value="Group" style=""
												<?php echo $policiesClidPolicyLevel == "Group" ? "checked" : ""; ?>>
											<label class="labelText labelTextMargin"
												for="callingLineIdPolicyGroup"><span></span>Group</label>
										</div>
									</div>
								</div>
							</div>
                                <?php } else { ?>
                    				<input type="hidden" name="callingLineIdPolicy"
								<?php echo $policiesClidPolicyLevel == "User" ? "value=\"User\"" : "value=\"Group\""; ?>>
                    			<?php } ?>
                    			
                    			<?php
                    if ($thirdPartyVoiceMailSupport == "true") {
                        ?>
                        		<div class="inputText">
								<input type="hidden" name="thirdParty" value="0"> <input
									type="checkbox" id="thirdParty" name="thirdParty" value="1"> <label
									for="thirdParty"><span></span></label> <label class="labelText"
									style="margin-left: 0;" for="thirdParty"> Third-Party Voice
									Mail: </label>
							</div>
                        		<?php } ?>
                    		</div>

						<div class="row rBtnRow">
							<div class="col-md-6">
                        		 <?php
                        // Code @ 07 Sep 2018
                        $cssStyle = "display:block;";
                        if ($polycomServicesFieldVisible == "false" && ! in_array("Polycom Phone Services", $userServicesArr)) {
                            $cssStyle = "display:none;";
                        }
                        $polycomPhoneServicesBtnNo = "";
                        if (in_array("Polycom Phone Services", $userServicesArr) && $polycomServicesFieldVisible == "false") {
                            $polycomPhoneServicesBtnNo = "checked='checked'";
                        }
                        // End code
                        ?>
                        			<div class="pPServicesRadio"
									<?php echo $cssStyle;  ?>>
									<div class="col-md-6 voiceMsgTxt">
										<div class="form-group">
											<label class="labelText" style="margin-left: 0;" for="">
												Polycom Phone Services:</label>
										</div>
									</div>
									<div class="col-md-6 voiceMsgRBtn">
										<div class="col-md-6 rBtnYes">
											<div class="form-group">
												<input type="radio" name="polycomPhoneServices"
													id="polycomPhoneServicesYes" value="Yes" style=""> <label
													class="labelText labelTextMargin"
													for="polycomPhoneServicesYes"><span></span>Yes</label>

											</div>
										</div>
										<div class="col-md-6 rBtnNo">
											<div class="form-group">
												<input type="radio" name="polycomPhoneServices"
													id="polycomPhoneServicesNo" value="No" style="" checked> <label
													class="labelText labelTextMargin"
													for="polycomPhoneServicesNo"><span></span>No</label>
											</div>
										</div>
                                                                            <input type="hidden" name ="" id ="checkPolicomPhoneServicesOpt" value="false">
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6" style="display: <?php echo ($polycomServicesFieldVisible == "true" ? " block" : " none"); ?> ">
									<div class="form-group">
										<label class="labelText" for="ccd">Custom Contact Directory:</label>
										<div class="dropdown-wrap">
											<select name="ccd" id="ccd" class="form-control">
												<option value=""></option>
                                							<?php
                                    if (isset($directories)) {
                                        foreach ($directories as $key => $value) {
                                            echo "<option value=\"" . $value . "\">" . $value . "</option>";
                                        }
                                    }
                                    ?>
                                					</select>
										</div>
									</div>
								</div>

								<div class="col-md-6">
                            			<?php if($cpgFieldVisible == "true" && $_SESSION["groupServicesAuthorizationTable"] == "1") { ?>
                            			
                                            <div class="form-group">
										<label class="labelText" for="extension">Call Pickup Group:</label>
										<div class="dropdown-wrap">
											<select name="groupCallPickupList" class="form-control"
												id="groupCallPickupList">
												<option value="">None</option>
                                                        <?php
                                // echo "<pre>"; print_r($groupCallPickupList[0]); die;
                                if (isset($groupCallPickupList[0])) {
                                    foreach ($groupCallPickupList[0] as $key => $value) {
                                        echo "<option value=\"" . $value . "\">" . $value . "</option>";
                                    }
                                }
                                
                                ?>
                                					</select>
										</div>
									</div>
                                		<?php  } ?>
                                		</div>
							</div>
						</div>


















					</div>
				</div>
			</div>
		</div>
		<!-- Services Information End -->
	</div>



























	<div class="row">
		<div class="col-md-12 alignBtn">
			<div class="form-group confirmSettingsDiv">
				<input type="button" id="subButton" value="Confirm Settings">
			</div>
		</div>
	</div>
	<!--</div>-->
	<input type="hidden" value="" name="softPhoneType" id="softPhoneType">
	<input type="hidden" value="" name="supportPortNumberType"
		id="supportPortNumberType">
</form>

<div id="dialog" class="dialogClass"></div>
<!-- </div> -->
 
 