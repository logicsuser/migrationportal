<?php
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");

$flag   = 0;
$anFlag = 0;
$servicesArr = array();
$spId = $_SESSION["sp"]; 
$groupId = $_SESSION["groupId"];

$servicePacksWithSCAServiceArr = $_SESSION["scaServicePacksArr"];

if(isset($_POST['act']) && $_POST['act']=="selectSCAOnAdd")
{    
    $servicePackCOM = $_POST["userSelectedServicePacks1"];
    for($i=0;$i<count($servicePacks);$i++)
    {
        if(in_array($servicePacks[$i], $servicePackCOM))
        {
            $flag = $flag + 1;
            $servicePackWithSCAService = $servicePacks[$i];
        }
    }
    
    if($flag==1)
    {        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId). "</serviceProviderId>";
        $xmlinput .= "<servicePackName>" . $servicePackWithSCAService . "</servicePackName>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        if(count($xml->command->userServiceTable->row)=="1")
        {
            $pos = strpos(strval($xml->command->userServiceTable->row->col[0]), "Shared Call Appearance");
            if($pos === false)
            {
                echo "NONE";
            }
            else
            {            
                echo $servicePackWithSCAOnly =  $servicePackWithSCAService;
            }
        }
        else
        {
            echo "NONE";
        }
    }
    else 
    {
        echo "NONE";
    }
}

if(isset($_POST['act']) && $_POST['act']=="unSelectSCAOnAdd")
{
    $userServicePacks = $_POST['userSelectedServicePacks'];
    //$userServicePacks = array("SCA5, SCA10, SCA15, SCA20, SCA25, SCA30, SCA35");
    $flag = 0;
    foreach ($userServicePacks as $servicePack) {
            $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId). "</serviceProviderId>";
            $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);     
            
            if(count($xml->command->userServiceTable->row)=="1")
            {
                $pos = strpos(strval($xml->command->userServiceTable->row->col[0]), "Shared Call Appearance");
                if($pos === false)
                {
                    //continue;
                }
                else
                {  
                    $flag = 1;
                    $servicePackWithSCAOnly =  $servicePack;
                }  
            }
    }
    if($flag==1){
        echo $servicePackWithSCAOnly;
    }
     else {
        echo "NONE";
    }
}

?>