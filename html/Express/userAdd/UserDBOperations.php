<?php

require_once("/var/www/html/Express/config.php");
checkLogin();


class UserDBOperations{
    
    
    
    function getAllPhoneProfiles(){
        global $db;
        
        $phoneProfileArray = array();
        $stmt = $db->prepare("SELECT * FROM phoneProfilesLookup");
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i =0;
        foreach($row as $key => $val) {
            $phoneProfileArray[$i][] = $val->phoneProfile;
            $phoneProfileArray[$i][] = $val->value;
            $i++;
        }
        
        return $phoneProfileArray;
    }
    
    
     
    
   /* function getAllTagBundles() {
        global $db;
        $tagBundleArray = array();
        
        //         $tagBundleArray = array("nhForward","Forward","N-Way","Pickup","TCP","ACD");
        
        $stmt = $db->prepare("SELECT `Tag_Bundle_ID`, `Tag_Bundle_Name` FROM deviceMgmtTagBundle");
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i =0;
        foreach($row as $key => $val) {
            $tagBundleArray[$i]['Tag_Bundle_ID'] = $val->Tag_Bundle_ID;
            $tagBundleArray[$i]['Tag_Bundle_Name'] = $val->Tag_Bundle_Name;
            
            $i++;
        }
        //         print_r($tagBundleArray); exit;
        return $tagBundleArray;
    }*/
    
    function getAllTagBundles() {
        global $db;
        $tagBundleArray = array();
        
        $stmt = $db->prepare("SELECT `tagBundle` FROM deviceMgmtTagBundles");
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i =0;
        foreach($row as $key => $val) {
            if(!in_array($val->tagBundle, $tagBundleArray)){
                $tagBundleArray[$i] = $val->tagBundle;
                
                $i++;
            }
            
        }
        return $tagBundleArray;
    }
    
    function getAllCustomTagsFromTagBundles($tagBundle)
    {
        global $db;
        
        $customTagArray = array();
        $tagBundle = join("','", $tagBundle);
        
        $stmt = $db->prepare("SELECT `tag`, `tagValue` FROM deviceMgmtTagBundles where `tagBundle` in('$tagBundle')");
        $stmt->execute();
        $customTagArrayRel = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($customTagArrayRel as $key => $value){
            $customTagArray[strval($value->tag)] = strval($value->tagValue);
        }
        
        return $customTagArray;
    }
    
    function getAllCustomProfiles(){
        global $db;
        
        $customProfileArray = array();
        
        $stmt = $db->prepare("SELECT * FROM customProfiles");
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i =0;
        foreach($row as $key => $val) {
            $customProfileArray[$i++] = $val->deviceType;
            
        }
        
        return $customProfileArray;
    }
    
   /* function getAllCustomTagsFromTagBundles($tagBundle)
    {
        global $db;
        
        $Express_Tag_Bundle = implode(":", $tagBundle);
        $customTagArray = "";
        $tagBundle = join("','", $tagBundle);
        
        $stmt = $db->prepare("SELECT `Tag_Name`, `Default_Value` FROM deviceMgmtTagBundleDefaults where `Tag_Bundle_ID` in('$tagBundle')");
        $stmt->execute();
        $customTagArray = $stmt->fetchAll(PDO::FETCH_OBJ);
        
        $customTagArray[] = (object)array("Tag_Name" => "%Express_Tag_Bundle%", "Default_Value" => $Express_Tag_Bundle);
        return $customTagArray;
    }*/
    
}
?>