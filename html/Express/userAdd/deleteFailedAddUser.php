<?php
// error_reporting(E_ALL);
// ini_set('display_errors', '1');
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once("/var/www/lib/broadsoft/adminPortal/DeleteFailedUserDetail.php");

$spId = $_SESSION["sp"]; 
$groupId = $_SESSION["groupId"];
$DFU = new DeleteFailedUserDetail();

$returnResponse = array();

if($_POST["module"] == "deleteUser")
{
    if(isset($_SESSION["errorInAddUser"]) && $_SESSION["errorInAddUser"] == 1)
    {
	             
        if(isset($_SESSION["userAddModuleStatus"]["moduleAndStatus"]["addUser"]) && $_SESSION["userAddModuleStatus"]["moduleAndStatus"]["addUser"]["Status"] == "Success") {
            $userId = $_SESSION["userAddModuleStatus"]["moduleAndStatus"]["addUser"]["userId"];
            $userDelete = $DFU->deleteUser($userId);
            if( empty($userDelete["Error"]) ){
                $returnResponse[]["userDeleted"] = "Success";
            }
        }
        
        
        if(isset($_SESSION["userAddModuleStatus"]["moduleAndStatus"]["device"]) && $_SESSION["userAddModuleStatus"]["moduleAndStatus"]["device"]["Status"] == "Success") {
            $deviceName = $_SESSION["userAddModuleStatus"]["moduleAndStatus"]["device"]["devName"];
            $deviceDelete = $DFU->deviceDeleteRequest($spId, $groupId, $deviceName);
            if( empty($deviceDelete["Error"]) ){
                $returnResponse[]["deviceDeleted"] = "Success";
            }
        }
        
        
        if(isset($_SESSION["userAddModuleStatus"]["moduleAndStatus"]["confVM"]) && $_SESSION["userAddModuleStatus"]["moduleAndStatus"]["confVM"]["Status"] == "Success"){
            $emailAddress = $_SESSION["userAddModuleStatus"]["moduleAndStatus"]["confVM"]["emailAddress"];
            $emailPassword = $_SESSION["userAddModuleStatus"]["moduleAndStatus"]["confVM"]["emailPassword"];
            
            $surgeMailDeleted = $DFU->deleteSurgeMail($emailAddress, $emailPassword, $surgemailUsername, $surgemailPassword, $surgemailURL);
            if( empty($surgeMailDeleted["Error"]) ){
                $returnResponse[]["surgeMailDeleted"] = "Success";
            }
		}

        
    }
    
}

print_r(json_encode($returnResponse));
?>