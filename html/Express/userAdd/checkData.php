<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();
	
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
	
	require_once("/var/www/lib/broadsoft/adminPortal/counterPathStretto/CPSOperations.php");
	$cpsobj = new CPSOperations();
	
	$objSysLevel = new sysLevelDeviceOperations();
        
        require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");   //Code added @ 10 July 2018
        $srvceObj = new Services();
        
        
        
	function servicePacksHaveVMService($servicePacks) {
        global $sessionid, $client;

        foreach ($servicePacks as $servicePack) {
            $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]). "</serviceProviderId>";
            $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

            foreach ($xml->command->userServiceTable->row as $key => $value){
                if (strval($value->col[0]) == "Voice Messaging User") {
                    return true;
                }
            }
        }
        return false;
    }
    
    //Code added @ 02 July 2018 to check Service packs have more then 1 VM Service
    function servicePacksHaveMoreOneVMService($servicePacks)
    {
        $counter = 0;
        $servicePackWithVMService = "";
        $servicePacksWithVMServiceArr = explode(",", $_SESSION["vmServicePacksFlat"]);        
        for($i=0;$i<count($servicePacks);$i++)
        {
            if(in_array($servicePacks[$i], $servicePacksWithVMServiceArr))
            {
                $counter = $counter + 1;
                $servicePackWithVMService = $servicePacks[$i];
            }
        }        
        if($counter!=1)
            return true;
        else
            return false;        
    }
    //End Code
  
    // Tag list for adding.
//     function getCustomTagData($tagBundles) {
//         $customTag = array();
//         $customTag["addTagList"] = array("tag1", "tag2", "tag3", "tag4");
        
//         return $customTag;
//     }
//     if(isset($_POST["deviceMgtTagBundle"])) {
//         $_POST["deviceMgtTagBundle"] = implode(", ", $_POST["deviceMgtTagBundle"]);
//         $custTagList = getCustomTagData("testParam");
//         $_POST["addTagList"] = implode(",", $custTagList["addTagList"]);
//     }
    
    
    
    if(!isset($_POST["customProfile"]) && $license["customProfile"] =="false"){
        $_POST["customProfile"] = "None";
    } 
    if(!isset($_POST["softPhone"]) && $_POST["softPhone"] != "true"){
        unset($_POST["softPhoneDomain"]);
        unset($_POST["accountProfile"]);
    }
    if($_POST["softPhoneType"] == "" || $_POST["softPhoneType"] == "generic"){
        
        unset($_POST["accountUserName"]);
        unset($_POST["accountEmailAddress"]);
        unset($_POST["accountProfile"]);
    }
    if(isset($_POST["supportPortNumberType"])) {
        $supportPortNumberType = $_POST["supportPortNumberType"];
        unset($_POST["supportPortNumberType"]);
    }
    
    if(isset($_POST["userType"] ) && $_POST["userType"] == "None") {
        unset($_POST["deviceTypeResult"]);
         unset($_POST["deviceType"]);
    }
    
    
    
    
    
	$_SESSION["userInfo"]["resetWeb"] = "false";
	$_SESSION["userInfo"]["resetPortal"] = "false";

	unset($_POST["data"]);
	$softPhonetype = $_POST["softPhoneType"];
	unset($_POST["softPhoneType"]);
	$nonRequired = array("macAddress", "suite", "phoneNumber", "dept", "language", "ccd", "deviceAccessUserName", "deviceAccessPassword", "deviceType", "networkClassOfService", "groupCallPickupList", "servicePack", "softPhoneDeviceAccessUserName", "softPhoneDeviceAccessPassword", "softPhoneDeviceType", "userService");
	if ($portalRequiresClidNumber == "false")
	{
		$nonRequired[] = "callerId";
	}
	if ($bwRequireAddress !== "true")
	{
		$nonRequired[] = "address";
		$nonRequired[] = "city";
		$nonRequired[] = "state";
		$nonRequired[] = "zip";
	}
	$nonRequired[] = "email";
        //Code added @ 02 May 2018
        $nonRequired[] = "location";
        //End Code

    $nonRequired[] = "softPhone";
    $nonRequired[] = "accountEmailAddress";
    $nonRequired[] = "accountUserName";

        //Code added @ 23 Oct 2018
    if($_POST['nameDialingFirstName']=="" && $_POST['nameDialingLastName']==""){
        $nonRequired[] = "nameDialingFirstName";
        $nonRequired[] = "nameDialingLastName";
    }        
        //End code
        
	$checkForCharacters = array("firstName", "lastName", "address", "city", "zip", "callingLineIdFirstName", "callingLineIdLastName");

	$data = $errorTableHeader;
	
	/*if(!array_key_exists("uActivateNumber", $_POST)){
		$_POST['uActivateNumber'] = "";
	}*/
// 	print_r($_POST["deleteTagList"]); exit;
	$error = 0;
       
	foreach ($_POST as $key => $value)
	{
        // Exclude internal controls
        if ($key == "userType") {
            if ($value != "analog") {
                $nonRequired[] = "deviceIndex";
                $nonRequired[] = "portNumber";
            }
            if($value == "analog" || $value != "analog" && ! isset($_POST['portNumberSIP'])) {
                $nonRequired[] = "portNumberSIP";
            }
            if($value == "analog" && $supportPortNumberType == "dynamic") {
                $nonRequired[] = "deviceIndex";
                $nonRequired[] = "portNumber";
            }
             continue;
        }
        
        /*if ($key == "softPhone") {
            continue;
        }*/

        // Show device type only if specified
         

		if ($value !== "")
		{
			$bg = CHANGED;
		}
		else
		{
			$bg = UNCHANGED;
		}

		if (!in_array($key, $nonRequired) and $value == "")
		{
			$error = 1;
			$bg = INVALID;
			$value = $_SESSION["userAddNames"][$key] . " is a required field.";
		}
		if (in_array($key, $checkForCharacters) and $value !== "")
		{
			//if (!preg_match("/^[ A-Za-z0-9\s-]+$/", $value))
			if (!preg_match("/^[0-9a-zA-Z\- \/_?;:.,\s]+$/", $value))
			{
				$error = 1;
				$bg = INVALID;
				$value = $_SESSION["userAddNames"][$key] . " must be letters, numbers, and dashes only.";
			}
		}
		if ($key == "macAddress" and $value !== "")
		{
			include("../checkMac.php");
			if (isset($errorMsg))
			{
				$error = 1;
				$bg = INVALID;
				$value = $errorMsg;
			}
		}
		
		if ($key == "accountUserName" and $softPhonetype == "counterPath"){
		    if($value == ""){
		        $error = 1;
		        $bg = INVALID;
		        $value = $_SESSION["userAddNames"][$key] . " is a required field.";
		    }
	    }
	    
	    if ($key == "accountUserName" and $softPhonetype == "counterPath"){
	        if($value != ""){
	            $cpsReslt = $cpsobj->counterPathAccountGet(trim($value));
	            if($cpsReslt == "true"){
	                $error = 1;
	                $bg = INVALID;
	                $value = $_SESSION["userAddNames"][$key] . " is already exist.";
	            }
	        }
	    }
		
		if ($key == "polycomPhoneServices" and $value !== "")
		{
		    if($value == "Yes"){
		    	if($_POST["userType"] == "digital"){
			        if(isset($_POST["deviceTypeResult"]) && !empty($_POST["deviceTypeResult"])){
			                $returnResp = $objSysLevel->getSysytemDeviceTypeServiceRequest($_POST["deviceType"], $ociVersion);
			                if($returnResp["Error"] == "" && empty($returnResp["Error"])){
			                    $resp = strval($returnResp["Success"]->supportsPolycomPhoneServices);
			                    if($resp == "false"){
			                        $error = 1;
			                        $bg = INVALID;
			                        $value = "Polycom Phone Services' is not enabled for Device Type: ".$_POST['deviceType']."";
			                    }
			                }
			        }else{
                                    $error = 1;
                                    $bg = INVALID;
                                    $value = "Polycom Phone Services requires supported Polycom device type to be assigned to User.";
                                }
		    	}else{
		    		$error = 1;
		    		$bg = INVALID;
		    		$value = "Polycom Phone Services requires supported Polycom device type to be assigned to User.";

		    	}
		        
		    }
		    
		}
		
		if ($key == "extension")
		{
			include("../checkExtension.php");
		}
		if ($key == "phoneNumber")
		{
			//if third-party voice mail is checked and phone number wasn't set, generate an error
			if (isset($_POST["thirdParty"]) and $_POST["thirdParty"] == "1" and $value == "")
			{
				$error = 1;
				$bg = INVALID;
				$value = $_SESSION["userAddNames"][$key] . " is required for third-party voice mail.";
			}
		}
		/*if($key == "uActivateNumber"){
			if($value == "Yes"){
				$bg = CHANGED;
				$value = "Activated";
			}			
		}*/
		/*if ($key == "deviceAccessPassword")
		{
			if ($value)
			{
				$value = str_repeat("*", strlen($value)); //don't display password
			}
		}*/
		/*if ($key == "softPhoneDeviceAccessPassword")
		{
			if ($value)
			{
				$value = str_repeat("*", strlen($value)); //don't display password
			}
		}*/
		if ($key == "servicePack")
		{
			if ($value == "")
			{
				$value = "None"; //don't display password
				$bg = CHANGED;
			}
		}
         
        //Code added @ 04 July 2018   
        if($key == "voiceMessaging" && $value == "Yes")
        {
                if(empty($_POST["servicePack"]))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Express cannot unambiguously assign Service Pack with Voice Messaging User service.";
                }
        }       
        //End code
        
	if ($key == "voiceMessaging" && $value == "Yes" && isset($_POST["servicePack"]) && !empty($_POST["servicePack"])) 
        {
            if (! servicePacksHaveVMService($_POST["servicePack"])) {
                $error = 1;
                $bg = INVALID;
                $value = "Selected Service Packs to be assigned to User do not include 'Voice Messaging User' service.";              
                
            }
        }
        //Code added @ 10 July 2018
        if($key == "sharedCallAppearance" && $value == "Yes")
        {
            if(empty($_POST["servicePack"]) && empty($_POST["userService"]))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Express cannot unambiguously assign Service Pack with Shared Call Appearance User service.";
                }
        }    
        /*if ($key == "sharedCallAppearance" && $value == "Yes" && isset($_POST["servicePack"]) && !empty($_POST["servicePack"])) 
        {            
            if (!$srvceObj->servicePacksHaveSCAService($_POST["servicePack"])) {
                $error = 1;
                $bg = INVALID;
                $value = "Selected Service Packs to be assigned to User do not include 'Shared Call Appearance' service.";              
            }            
        }*/
        
        if ($key == "softPhone" && $_POST["softPhoneDeviceType"] !="")
        {
            if ($srvceObj->checkIfSCAService($_POST["servicePack"], $_POST["userService"])) {
                //$error = 1;
                //$bg = INVALID;
                //$value = "Selected Service Packs to be assigned to User do not include 'Shared Call Appearance' service.";
            }else{
                $error = 1;
                $bg = INVALID;
                $value = "A Service Pack with a SCA service, or any SCA User Service must be selected to enable this service.";
                /*require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
                $grpServiceList = new Services();
                //echo "1";print_r($servicePackAssigned);
                
                $grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
                $servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
                $servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray);
                $scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);
                //echo "sdsdsd";print_r($scaServiceArray);
                $oneServicePack = "false";
                $multipleServicePack = "false";
                $noServicePack = "false";
                $r = 0;
                foreach($scaServiceArray as $ssa=>$ssv){
                    $pos = strpos($ssv->col[0], "Shared Call Appearance");
                    if($pos === false){
                        
                    }else{
                        if($ssv->count == 1){
                            $oneServicePack = "true";
                            $r++;
                            //$countMore = "0";
                        }else if($ssv->count > 1){
                            $multipleServicePack = "true";
                            //$countMore = "true";
                        }else{
                            $noServicePack = "true";
                            //$countMore = "0";
                        }
                    }
                    
                }
                //print_r($oneServicePack);
                //print_r($multipleServicePack);
                //print_r($r);
                if($oneServicePack == "false" || $r != "1"){
                    $posServiceStatus = false;
                    $userServicesArr = $_SESSION['groupInfoData']['userServiceAuth'];
                    
                    if(count($userServicesArr) > 0){
                        foreach ($userServicesArr as $valServiceName){
                            $posService = strpos($valServiceName, "Shared Call Appearance");
                            if($posService === false){
                                
                            }else{
                                $posServiceStatus = true;
                            }
                        }
                    }
                    if($posServiceStatus == false){
                        $error = 1;
                        $bg = INVALID;
                        $value = "Express cannot unambiguously assign Service Pack with Shared Call Appearance User service.";
                    }
                    
                }*/
            }
            
            
            
            /*if(empty($_POST["servicePack"]))
            {
                $error = 1;
                $bg = INVALID;
                $value = "Soft Phone Creation needs Service Pack that contain Shared Call Appearance Service.";
            }else{
                if (!$srvceObj->servicePacksHaveSCAService($_POST["servicePack"])) {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Selected Service Packs to be assigned to User do not include 'Shared Call Appearance' service.";
                }  
            }*/
        }
        if ($key == "softPhone" && $_POST["softPhoneDeviceType"] =="")
        {
            $error = 1;
            $bg = INVALID;
            $value = "Soft Phone Device Type can not be empty.";  
        }
        
        //End code
        
        //Code added @ 10 Sep 2018
        //Code commented @ 25 Sep 2018 Regarding EX-817
        /*if($key == "polycomPhoneServices" && $value == "Yes")
        {
                $userServicesArr = $_SESSION['groupInfoData']['userServiceAuth'];
                if(!in_array("Polycom Phone Services", $userServicesArr))
                {
                    $error = 1;
                    $bg = INVALID;
                    $value = "Polycom Phone Services is not assigned.";
                }
        }*/
        /*if ($key == "polycomPhoneServices" && $value == "Yes" && isset($_POST["servicePack"]) && !empty($_POST["servicePack"])) 
        {            
            if (!$srvceObj->checkservicePacksHasPolycomPhoneService($_POST["servicePack"])) {
                $error = 1;
                $bg = INVALID;
                $value = "Selected Service Packs to be assigned to User do not include 'Polycom Phone Services.";              
            }            
        }*/
        /*if($key == "polycomPhoneServices" && $value == "Yes" && empty($_POST["servicePack"]))
        {
            $error = 1;
            $bg = INVALID;
            $value = "Please select service pack that contains 'Polycom Phone Services'. ";
        }*/
        if ($key == "deviceTypeResult") {
            if ($value == "") {
               $error = 1;
               $bg = INVALID;
               $value = $_SESSION["userAddNames"][$key] . " is a required field.";
           } 
       } 
       if ($key =="deviceType") {
             continue;
       } 
       
        
		if ($bg != UNCHANGED)
		{
			$data .= "<tr><td class=\"errorTableRows \" style=\"background:" . $bg . ";\">" . $_SESSION["userAddNames"][$key] . "</td><td class=\"errorTableRows\">";
			if (is_array($value))
			{
				foreach ($value as $k => $v)
				{
					$data .= $v . "<br>";
				}
			}
			else if ($key == "thirdParty")
			{
				if ($value == "1")
				{
					$data .= "True";
				}
				else
				{
					$data .= "False";
				}
			}
			else if ($value !== "")
			{
				$data .= $value;
			}
			else
			{
				$data .= "None";
			}
			$data .= "</td></tr>";
		}
	}
	$data .= "</table>";

	echo $error . $data;
?>
