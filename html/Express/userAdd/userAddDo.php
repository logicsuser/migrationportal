<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();

	require_once("/var/www/lib/broadsoft/adminPortal/getGroupClid.php");
	unset($_SESSION["userAddModuleStatus"]);
	$_SESSION["authPass"] = setAuthPassword();
?>

	<div class="progress" id="addUserProgressBarDiv">
			<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
			     style="width: 0;" id="addUser_progress_bar">0%
		</div>
	</div>
	<div id="loadingModule"></div>
<script>
	$(function() {
		var firstName = "<?php echo isset($_POST["firstName"]) ? $_POST["firstName"] : ""; ?>";
		var lastName = "<?php echo isset($_POST["lastName"]) ? $_POST["lastName"] : ""; ?>";
		var callingLineIdFirstName = "<?php echo isset($_POST["callingLineIdFirstName"]) ? $_POST["callingLineIdFirstName"] : ""; ?>";
		var callingLineIdLastName = "<?php echo isset($_POST["callingLineIdLastName"]) ? $_POST["callingLineIdLastName"] : ""; ?>";
		var email = "<?php echo isset($_POST["email"]) ? $_POST["email"] : ""; ?>";
		var dept = "<?php echo isset($_POST["dept"]) ? $_POST["dept"] : ""; ?>";
		var address = "<?php echo isset($_POST["address"]) ? $_POST["address"] : ""; ?>";
		var suite = "<?php echo isset($_POST["suite"]) ? $_POST["suite"] : ""; ?>";
		var city = "<?php echo isset($_POST["city"]) ? $_POST["city"] : ""; ?>";
		var state = "<?php echo isset($_POST["state"]) ? $_POST["state"] : ""; ?>";
		var zip = "<?php echo isset($_POST["zip"]) ? $_POST["zip"] : ""; ?>";
		var timeZone = "<?php echo isset($_POST["timeZone"]) ? $_POST["timeZone"] :""; ?>";
                var networkClassOfService = "<?php echo isset($_POST["networkClassOfService"]) ? $_POST["networkClassOfService"] : ""; ?>";
		//Code added @ 02 May 2018
		var lcationVal = "<?php echo isset($_POST["location"]) ? $_POST["location"] : ""; ?>";
		//End code	
                //Code added @ 23 Oct 2018
		var nameDialingFirstName = "<?php echo isset($_POST["nameDialingFirstName"]) ? $_POST["nameDialingFirstName"] : ""; ?>";
                var nameDialingLastName  = "<?php echo isset($_POST["nameDialingLastName"])  ? $_POST["nameDialingLastName"]  : ""; ?>";
		//End code
		var language = "<?php echo isset($_POST["language"]) ? $_POST["language"] : ""; ?>";
		var phoneNumber = "<?php echo isset($_POST["phoneNumber"]) ? $_POST["phoneNumber"] : ""; ?>";
		var activateNumber = "<?php echo isset($_POST["activateNumber"]) ? $_POST["activateNumber"] : ""; ?>";
		var callerId = "<?php echo isset($_POST["callerId"]) ? $_POST["callerId"] : ""; ?>";
		var extension = "<?php echo isset($_POST["extension"]) ? $_POST["extension"] : ""; ?>";
		var ccd = "<?php echo isset($_POST["ccd"]) ? $_POST["ccd"] : ""; ?>";
                //var deviceType = "<?php echo isset($_POST["deviceType"]) ? $_POST["deviceType"] :""; ?>";
                var deviceType = "<?php echo isset($_POST["deviceTypeResult"]) ? $_POST["deviceTypeResult"] :""; ?>";
		//var customProfile = "<?php //echo isset($_POST["customProfile"]) ? $_POST["customProfile"] : "None"; ?>";
		var customProfile = "<?php echo isset($_POST["customProfile"]) ? $_POST["customProfile"] : "None"; ?>";
		var deviceMgtTagBundle = '<?php echo isset($_POST["deviceMgtTagBundle"]) ? json_encode($_POST["deviceMgtTagBundle"]) : "None"; ?>';
		var userType = "<?php echo isset($_POST["userType"]) ? $_POST["userType"] : ""; ?>";
		var deviceIndex = "<?php echo isset($_POST["deviceIndex"]) ? $_POST["deviceIndex"] : ""; ?>";
    
		var portNumber = "<?php echo isset($_POST["portNumber"]) ? $_POST["portNumber"] : ""; ?>";
		var portNumberSIP = "<?php echo isset($_POST["portNumberSIP"]) ? $_POST["portNumberSIP"] : ""; ?>";
		var macAddress = "<?php echo isset($_POST["macAddress"]) ? $_POST["macAddress"] : ""; ?>";
        var proxyDomain = "<?php echo isset($_POST["proxyDomain"]) ? $_POST["proxyDomain"] : ""; ?>";
		var servicePack = "<?php echo implode(",", $_POST["servicePack"]); ?>".split(",");
		var userService = "<?php echo implode(",", $_POST["userService"]); ?>".split(",");
		var thirdParty = "<?php echo isset($_POST["thirdParty"]) ? $_POST["thirdParty"] : ""; ?>";
			var deviceAccessUserName = "<?php echo isset($_POST["deviceAccessUserName"]) ? $_POST["deviceAccessUserName"] : ""; ?>";
	
			var deviceAccessPassword = "<?php echo isset($_POST["deviceAccessPassword"]) ? $_POST["deviceAccessPassword"] : ""; ?>";
	
		var voiceMessaging = "<?php echo isset($_POST["voiceMessaging"]) ? $_POST["voiceMessaging"] : ""; ?>";
                var sharedCallAppearance = "<?php echo isset($_POST["sharedCallAppearance"]) ? $_POST["sharedCallAppearance"] : ""; ?>";
		var polycomPhoneServices = "<?php echo isset($_POST["polycomPhoneServices"]) ? $_POST["polycomPhoneServices"] : ""; ?>";
		var callingLineIdPolicy = "<?php echo isset($_POST["callingLineIdPolicy"]) ? $_POST["callingLineIdPolicy"] : ""; ?>";
		<?php if(isset($_POST["groupCallPickupList"]) && !empty($_POST["groupCallPickupList"])){?>
			var groupCallPickupList = "<?php echo isset($_POST["groupCallPickupList"]) ? $_POST["groupCallPickupList"] : ""; ?>";
		<?php }else{?>
		var groupCallPickupList = "";
		<?php }?>

		//soft Phone details
		var softPhoneDeviceType = "<?php echo isset($_POST["softPhoneDeviceType"]) ? $_POST["softPhoneDeviceType"] : ""; ?>";
		var softPhoneDomain = "<?php echo isset($_POST["softPhoneDomain"]) ? $_POST["softPhoneDomain"] : ""; ?>";
		var accountUserName = "<?php echo isset($_POST["accountUserName"]) ? $_POST["accountUserName"] : ""; ?>";
		var accountProfile = "<?php echo isset($_POST["accountProfile"]) ? $_POST["accountProfile"] : ""; ?>";
		var accountEmailAddress = "<?php echo isset($_POST["accountEmailAddress"]) ? $_POST["accountEmailAddress"] : ""; ?>";
		var softPhoneDeviceAccessUserName = "<?php echo isset($_POST["softPhoneDeviceAccessUserName"]) ? $_POST["softPhoneDeviceAccessUserName"] : ""; ?>";
		var softPhoneDeviceAccessPassword = "<?php echo isset($_POST["softPhoneDeviceAccessPassword"]) ? $_POST["softPhoneDeviceAccessPassword"] : ""; ?>";
		var softPhoneType = "<?php echo isset($_POST["softPhoneType"]) ? $_POST["softPhoneType"] : ""; ?>";

		
		//var uActivateNumber = "<?php //echo isset($_POST["uActivateNumber"]) ? $_POST["uActivateNumber"] : ""; ?>";

		//set device name
        var fallbackNumber = "<?php echo isset($_SESSION["groupClid"]) && $_SESSION["groupClid"] != "" ? $_SESSION["groupClid"] : "000x"; ?>" + extension;

		$("#dialogMU").remove();
		$("#result").html("");
		$("html, body").animate({ scrollTop: 0 }, 0);
		var errorMessage = "";
		var continueUserBuild = 1;
        var hasDeviceName = "N";
        var errorInModule = "";
		<?php $_SESSION["errorInAddUser"] = 0; ?>
        // Add Identity Device Profile if DeviceType has been specified
		
		var totalSteps = 0;	
		checkNoOfOperationsOnAddUser(deviceType, groupCallPickupList, softPhoneDeviceType);
		function checkNoOfOperationsOnAddUser(deviceType, groupCallPickupList, softPhoneDeviceType) {
    	    var totalTasks = ["addUser",
    	        "Assigning Services",
    	        "Building Voicemail",
    	        "Configuring Voicemail",
    	        "Configuring Additional Services",
    	        "Configuring Authentication",
    	        "Configuring Dial Plans"];
    	    
    	    if( deviceType.length != 0 ) {
    	    	totalTasks.push("adding device");
    	    	totalTasks.push("Configuring Device");
    	    }
    	    if ( groupCallPickupList.length != 0 )
    	    {
    	    	totalTasks.push("Assigning call pickup group");
    	    }
    	    if ( softPhoneDeviceType.length != 0 )
    	    {
    	    	totalTasks.push("Creating Soft Phone");
    	    }
    	    totalSteps = totalTasks.length;
    	}
    	
		var steps = 0;
		var width = 0;
		progressBar(status="");
		function progressBar(status) {
			steps += 1;
            var id = setInterval(frame, 50);
            function frame() {
    			var status_percentage = steps * (100/totalSteps);
    			if(status == 100) {
    				status_percentage = 100;
    			} else if(status_percentage > 95 ) {
    				status_percentage = 95;
    			}
    			status_percentage = parseInt(status_percentage);
    			
                if (width >= 100) {
                  clearInterval(id);
                } else {
                    if(width < status_percentage ) {
                        width++; 
                    	$("#addUser_progress_bar").css('width', width + "%");
                        $("#addUser_progress_bar").html(width + "%");
                     } else {
                    	 clearInterval(id);
                     }
                }
              }
		}

//-------------------------------------------------------------
				
		if (deviceType.length != 0)
		{ 
			hasDeviceName = "Y";
          	$("#loadingModule").html("Adding Device...");
			//$("#result").append("<div id=\"buildDev\">Adding Device <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
			//pendingProcess.push("Add User");
			$.ajax({
				url: "userAdd/do.php",
	            type:'POST',
	            data: { module: "device", userType: userType, deviceType: deviceType, customProfile: customProfile, deviceIndex: deviceIndex, portNumber: portNumber, portNumberSIP: portNumberSIP, macAddress: macAddress, proxyDomain: proxyDomain, firstName: firstName, lastName: lastName, phoneNumber: phoneNumber, extension: extension, callerId: callerId, deviceMgtTagBundle: deviceMgtTagBundle},
	            
	            success: function(result)
	            {
	            	if(foundServerConErrorOnProcess(result, "")) {
    					return;
                  	}
	            	if (result.trim() !== "Done")
	                {
	                    continueUserBuild = 0;
	                    <?php $_SESSION["errorInAddUser"] = 1; ?>
	                    errorInModule = "Adding Device";
	                    //$("#buildDev").html("There was an error building Device. Please <a href=\"mailto:<?php echo $supportEmailSupport; ?>?body=" + result + "\">Click here</a> to send email to support.");
	                    //$("#buildDev").html("There was an error building Device. " + result+"<br/>");
	                    $("#buildDev").html("Adding Device....<span style='color:red; width:15px; height:15px'>&#10005</span>");
	                    errorMessage += "There was an error building Device. " + result+"<br/>";
	                    deleteAddedUser(errorInModule, errorMessage, continueUserBuild);
	                    //$("#buildDev").append(returnLink);
	                }
	                else
	                {
	                	progressBar(status="");
	                    //$("#buildDev").html("Adding Device....<img src=\"images/greenCheck.png\" width=\"15px\" height=\"15px\">");
	                    addUserToBW();
	                }
	            }
	        });

		}else{
			//pendingProcess.push("Add User");
// 			$("#result").append("<div id=\"buildUser\">Building User <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
			$("#loadingModule").html("Building User...");
			$.ajax({
				url: "userAdd/do.php",
	            type:'POST',
	            data: { module: "addUser", hasDeviceName: hasDeviceName, firstName: firstName, lastName: lastName, callingLineIdFirstName: callingLineIdFirstName, callingLineIdLastName: callingLineIdLastName, address: address, suite: suite, city: city, state: state, zip: zip, phoneNumber: phoneNumber, extension: extension, callerId: callerId, email: email, lcationVal: lcationVal, nameDialingFirstName: nameDialingFirstName, nameDialingLastName: nameDialingLastName, timeZone: timeZone, networkClassOfService: networkClassOfService, language: language, dept: dept, callingLineIdPolicy: callingLineIdPolicy, activateNumber: activateNumber, sharedCallAppearance: sharedCallAppearance, proxyDomain : proxyDomain},
	            
		         success: function(result)
	            {
		        	 if(foundServerConErrorOnProcess(result, "")) {
     					return;
                   	}
	            	if (result.trim() !== "Done")
	                {
	                    continueUserBuild = 0;
	                    <?php $_SESSION["errorInAddUser"] = 1; ?>
	                    errorInModule = "Building User";
	                    //$("#buildUser").html("There was an error building User. Please <a href=\"mailto:<?php echo $supportEmailSupport; ?>?body=" + result + "\">Click here</a> to send email to support.");
	                    //$("#buildUser").html("There was an error building User. "+ result+"<br/>");
	                    //$("#buildUser").append(returnLink);
	                    $("#buildUser").html("Building User....<span style='color:red; width:15px; height:15px'>&#10005</span>");
	                    errorMessage += "There was an error building User. "+ result+"<br/>";
	                    deleteAddedUser(errorInModule, errorMessage, continueUserBuild);
	                 }
	                else
	                {
	                	progressBar(status="");
	                    //$("#buildUser").html("Building User....<img src=\"images/greenCheck.png\" width=\"15px\" height=\"15px\">");
	                    assigningService();
	                }
	            }
	        });
		}

		var addUserToBW = function(){
			//pendingProcess.push("Add User");
// 			$("#result").append("<div id=\"buildUser\">Building User <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
			$("#loadingModule").html("Building User...");
			$.ajax({
				url: "userAdd/do.php",
	            type:'POST',
	            data: { module: "addUser", hasDeviceName: hasDeviceName, firstName: firstName, lastName: lastName, callingLineIdFirstName: callingLineIdFirstName, callingLineIdLastName: callingLineIdLastName, address: address, suite: suite, city: city, state: state, zip: zip, phoneNumber: phoneNumber, extension: extension, callerId: callerId, email: email, lcationVal: lcationVal, nameDialingFirstName: nameDialingFirstName, nameDialingLastName: nameDialingLastName, timeZone: timeZone, networkClassOfService: networkClassOfService, language: language, dept: dept, callingLineIdPolicy: callingLineIdPolicy, activateNumber: activateNumber, sharedCallAppearance: sharedCallAppearance, proxyDomain : proxyDomain},
	            
		         success: function(result)
	            {
		        	 if(foundServerConErrorOnProcess(result, "")) {
     					return;
                   	}
	            	if (result.trim() !== "Done")
	                {
	                    continueUserBuild = 0;
	                    <?php $_SESSION["errorInAddUser"] = 1; ?>
	                    errorInModule = "Building User";
	                    //$("#buildUser").html("There was an error building User. Please <a href=\"mailto:<?php echo $supportEmailSupport; ?>?body=" + result + "\">Click here</a> to send email to support.");
	                    //$("#buildUser").html("There was an error building User. "+ result+"<br/>");
	                    //$("#buildUser").append(returnLink);
	                    $("#buildUser").html("Building User....<span style='color:red; width:15px; height:15px'>&#10005</span>");
	                    errorMessage += "There was an error building User. "+ result+"<br/>";
	                    deleteAddedUser(errorInModule, errorMessage, continueUserBuild);
	                 }
	                else
	                {
	                	progressBar(status="");
	                    //$("#buildUser").html("Building User....<img src=\"images/greenCheck.png\" width=\"15px\" height=\"15px\">");
	                    assigningService();
	                }
	            }
	        });
		
		};

		var assigningService = function(){
			//pendingProcess.push("Add User");
// 			$("#result").append("<div id=\"assignServices\">Assigning Services <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
			$("#loadingModule").html("Assigning Services...");
			$.ajax({
				url: "userAdd/do.php",
	            type:'POST',
	            data: { module: "services", servicePack: servicePack, thirdParty: thirdParty, userService : userService },
	            
		         success: function(result)
	            {
		        	 if(foundServerConErrorOnProcess(result, "")) {
     					return;
                   	}
		        	 if (result.trim() !== "Done")
	                    {
	                        continueUserBuild = 0;
	                        <?php $_SESSION["errorInAddUser"] = 1; ?>
	                        errorInModule = "Assigning Services";
	                        //$("#assignServices").html("There was an error. Please <a href=\"mailto:<?php echo $supportEmailSupport; ?>?body=" + result + "\">Click here</a> to send email to support.");
	                        //$("#assignServices").html("There was an error. "+ result+"<br/>");
	                        //$("#assignServices").append(returnLink);
	                        $("#assignServices").html("Assigning Services....<span style='color:red; width:15px; height:15px'>&#10005</span>");
	                        errorMessage += "There was an error. "+ result+"<br/>";
	                        deleteAddedUser(errorInModule, errorMessage, continueUserBuild);
	                    }
	                    else
	                    {
	                    	progressBar(status="");
	                        //$("#assignServices").html("Assigning Services....<img src=\"images/greenCheck.png\" width=\"15px\" height=\"15px\">");
	                        assigningGroupCallPickup();
	                    }
	            }
	        });
		};

		var assigningGroupCallPickup = function(){
			if (groupCallPickupList.length != 0)
            {
				//pendingProcess.push("Add User");
// 				$("#result").append("<div id=\"addcallpick\">Assigning call pickup group <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
				$("#loadingModule").html("Assigning Call Pickup Group to User...");
				$.ajax({
					url: "userAdd/do.php",
		            type:'POST',
		            data: { module: "assigningcallpickupgroup", groupCallPickupList: groupCallPickupList },
		            
			         success: function(result)
		            {
			        	 if(foundServerConErrorOnProcess(result, "")) {
	        					return;
	                      }
			        	 if (result.trim() !== "Done")
	                     {
	                         continueUserBuild = 0;
	                         <?php $_SESSION["errorInAddUser"] = 1; ?>
	                         errorInModule = "Assigning call pickup group....<span style='color:red; width:15px; height:15px'>&#10005</span>";
	                         errorMessage += "There was an error. " + result +"<br/>";
	                         deleteAddedUser(errorInModule, errorMessage, continueUserBuild);
	                     }
	                     else
	                     {
	                    	 progressBar(status="");
// 	                         $("#addcallpick").html("Assigning Call Pickup Group to User....<img src=\"images/greenCheck.png\" width=\"15px\" height=\"15px\">");
	                         modifyingDevice();
	                     }
		            }
		        });
            }else{
            	modifyingDevice();
            }
			
		};

		var modifyingDevice = function(){
			if (deviceType.length != 0)
            {
				//pendingProcess.push("Add User");
// 				$("#result").append("<div id=\"modDev\">Configuring Device <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
				$("#loadingModule").html("Configuring Device...");
				$.ajax({
					url: "userAdd/do.php",
		            type:'POST',
		            data: { module: "modifyDev", deviceType: deviceType, deviceAccessUserName: deviceAccessUserName, deviceAccessPassword: deviceAccessPassword },
		            
			         success: function(result)
		            {
			        	 if(foundServerConErrorOnProcess(result, "")) {
	        					return;
	                      }
			        	 if (result.trim() !== "Done")
	                     {
	                         continueUserBuild = 0;
	                         <?php $_SESSION["errorInAddUser"] = 1; ?>
	                         errorInModule = "Configuring Device";
	                        $("#result").append("<div id=\"modDev\">Configuring Device....<span style='color:red; width:15px; height:15px'>&#10005</span>");
	                        errorMessage += "There was an error. " + result+"<br/>";
	                        deleteAddedUser(errorInModule, errorMessage, continueUserBuild);
	                     }
	                     else
	                     {
	                    	 progressBar(status="");
// 	                         $("#modDev").html("Configuring Device....<img src=\"images/greenCheck.png\" width=\"15px\" height=\"15px\">");
	                         softPhoneCreation();
	                     }
		            }
		        });
            }else{
            	softPhoneCreation();
            }
			
		};

		var softPhoneCreation = function(){
			if (softPhoneDeviceType.length != 0)
            {
				//pendingProcess.push("Add User");
// 				$("#result").append("<div id=\"addSoftPhone\">Creating Soft Phone <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
				$("#loadingModule").html("Creating Soft Phone...");
				$.ajax({
					url: "userAdd/do.php",
		            type:'POST',
		            data: { module: "addSoftPhone",userType: userType, deviceType: softPhoneDeviceType, softPhoneDomain : softPhoneDomain, accountUserName : accountUserName, accountUserName : accountUserName, accountProfile : accountProfile, accountEmailAddress : accountEmailAddress, softPhoneDeviceAccessUserName : softPhoneDeviceAccessUserName, softPhoneDeviceAccessPassword : softPhoneDeviceAccessPassword, proxyDomain : proxyDomain, firstName : firstName, lastName : lastName, phoneNumber : phoneNumber, extension : extension, callerId : callerId, softPhoneType : softPhoneType, email : email},
		            
			         success: function(result)
		            {
			        	 if(foundServerConErrorOnProcess(result, "")) {
	        					return;
	                     }
			        	 if (result.trim() !== "Done")
	                     {
	                         continueUserBuild = 0;
	                         <?php $_SESSION["errorInAddUser"] = 1; ?>
	                         errorInModule = "Creating Soft Phone....<span style='color:red; width:15px; height:15px'>&#10005</span>";
	                         errorMessage += "There was an error. " + result +"<br/>";
	                         deleteAddedUser(errorInModule, errorMessage, continueUserBuild);
	                     }
	                     else
	                     {
	                    	 progressBar(status="");
// 	                         $("#addSoftPhone").html("Creating Soft Phone....<img src=\"images/greenCheck.png\" width=\"15px\" height=\"15px\">");
	                         buildingVoiceMail();
	                     }
		            }
		        });
            }else{
            	buildingVoiceMail();
           	}
			
		};

		var buildingVoiceMail = function(){
			//pendingProcess.push("Add User");
// 			$("#result").append("<div id=\"buildVM\">Building Voicemail <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
			$("#loadingModule").html("Building Voicemail...");
			$.ajax({
				url: "userAdd/do.php",
	            type:'POST',
	            data: { module: "buildVM", thirdParty: thirdParty, voiceMessaging: voiceMessaging },
	            
		         success: function(result)
	            {
		        	 if(foundServerConErrorOnProcess(result, "")) {
     					return;
                   	 }
		        	 if (result.trim() !== "Done")
                     {
                         $("#buildVM").html("Building Voicemail....<span style='color:red; width:15px; height:15px'>&#10005</span>");
                         continueUserBuild = 0;
                         <?php $_SESSION["errorInAddUser"] = 1; ?>
                         errorInModule = "Configuring Device";
                     	errorMessage += "There was an error. " +result+"<br/>";
                     	deleteAddedUser(errorInModule, errorMessage, continueUserBuild);
                     }else{
                    	 progressBar(status="");
//                     	 $("#buildVM").html("Building Voicemail....<img src=\"images/greenCheck.png\" width=\"15px\" height=\"15px\">");
                    	 configuringVoiceMail();
                     }
	            }
	        });
		};

		var configuringVoiceMail = function(){
			//pendingProcess.push("Add User");
// 			$("#result").append("<div id=\"confVM\">Configuring Voicemail <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
			$("#loadingModule").html("Configuring Voicemail...");
			$.ajax({
				url: "userAdd/do.php",
	            type:'POST',
	            data: { module: "confVM", thirdParty: thirdParty, voiceMessaging: voiceMessaging },
	            
		         success: function(result)
	            {
		        	 if(foundServerConErrorOnProcess(result, "")) {
     					return;
                   	 }
		        	 if (result.trim().length != 0 && result[0].indexOf("Voicemail Creation failed") > 0 || result.trim() !== "Done")
                     {
                        $("#confVM").html("Configuring Voicemail....<span style='color:red; width:15px; height:15px'>&#10005</span>");
                        continueUserBuild = 0;
            			   <?php $_SESSION["errorInAddUser"] = 1; ?>
                 		var resultTrimmed = result.split("|");
                 		if(resultTrimmed[1] && resultTrimmed[1].indexOf("ErrorInSurgeMail") > -1) {
                 			errorInModule = "Configuring SurgeMail";
                 			errorMessage += "There was an error. " + resultTrimmed[0] +"<br/>";
                         } else {
                         	errorInModule = "Configuring Voicemail";
                     		errorMessage += "There was an error. " + result +"<br/>";
                         }
                 		deleteAddedUser(errorInModule, errorMessage, continueUserBuild);
                     }else{
                    	 progressBar(status="");
                    	 //$("#confVM").html("Configuring Voicemail....<img src=\"images/greenCheck.png\" width=\"15px\" height=\"15px\">");
                    	 configuringAdditionalServices();
                     }
	            }
	        });
		};

		var configuringAdditionalServices = function(){
			//pendingProcess.push("Add User");
// 			$("#result").append("<div id=\"addServ\">Configuring Additional Services <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
			$("#loadingModule").html("Configuring Additional Services...");
			$.ajax({
				url: "userAdd/do.php",
	            type:'POST',
	            data: { module: "addServ", deviceType: deviceType, ccd: ccd, polycomPhoneServices: polycomPhoneServices },
	            
		         success: function(result)
	            {
		        	 if(foundServerConErrorOnProcess(result, "")) {
	     					return;
	                 }
		        	 if (result.trim() !== "Done")
                     {
                         //$("#addServ").html("There was an error. Please <a href=\"mailto:<?php echo $supportEmailSupport; ?>?body=" + result + "\">Click here</a> to send email to support.");
                         //$("#addServ").html("There was an error. " + result +"<br/>");
                        // $("#addServ").append(returnLink);
                        $("#addServ").html("Configuring Additional Services....<span style='color:red; width:15px; height:15px'>&#10005</span>");
                        	errorInModule = "Configuring Voicemail";
                     	errorMessage += "There was an error. " + result +"<br/>";
                     	continueUserBuild = 0;
             			<?php $_SESSION["errorInAddUser"] = 1; ?>
                         deleteAddedUser(errorInModule, errorMessage, continueUserBuild);
                     }else{
                    	 progressBar(status="");
//                     	 $("#addServ").html("Configuring Additional Services....<img src=\"images/greenCheck.png\" width=\"15px\" height=\"15px\">");
                    	 configuringAuthentication();
                     }
	            }
	        });
		};

		var configuringAuthentication = function(){
			//pendingProcess.push("Add User");
// 			$("#result").append("<div id=\"auth\">Configuring Authentication <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
			$("#loadingModule").html("Configuring Authentication...");
			$.ajax({
				url: "userAdd/do.php",
	            type:'POST',
	            data: { module: "auth" },
	            
		         success: function(result)
	            {
		        	 if(foundServerConErrorOnProcess(result, "")) {
	     					return;
	                 }
		        	 if (result.trim() !== "Done")
                     {
                       $("#auth").html("Configuring Authentication....<span style='color:red; width:15px; height:15px'>&#10005</span>");
                       	errorInModule = "Configuring Additional Services";
                     	errorMessage += "There was an error. " + result+"<br/>";
                     	continueUserBuild = 0;
             			<?php $_SESSION["errorInAddUser"] = 1; ?>
                         deleteAddedUser(errorInModule, errorMessage, continueUserBuild);
                     }else{
                    	 progressBar(status="");
//                     	 $("#auth").html("Configuring Authentication....<img src=\"images/greenCheck.png\" width=\"15px\" height=\"15px\">");
                    	 configuringDialPlans();
                     }
	            }
	        });
		};

		var configuringDialPlans = function(){
			//pendingProcess.push("Add User");
// 			$("#result").append("<div id=\"dp\">Configuring Dial Plans <img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\"></div>");
			$("#loadingModule").html("Configuring Dial Plans...");
			$.ajax({
				url: "userAdd/do.php",
	            type:'POST',
	            data: { module: "dp" },
	            
		         success: function(result)
	            {
		        	 if(foundServerConErrorOnProcess(result, "")) {
	     					return;
	                 }
		        	 if (result.trim() !== "Done")
                     {
                         $("#dp").html("Configuring Dial Plans....<span style='color:red; width:15px; height:15px'>&#10005</span>");
                         errorInModule = "Configuring Authentication";
                     	errorMessage += "There was an error. " + result+"<br/>";
                     	continueUserBuild = 0;
             			<?php $_SESSION["errorInAddUser"] = 1; ?>
                         deleteAddedUser(errorInModule, errorMessage, continueUserBuild);
                     }
                     else
                     {
                         //Since the dial plans were configured, show completion message
                         //$("#dp").html("Configuring Dial Plans....<img src=\"images/greenCheck.png\" width=\"15px\" height=\"15px\">");
                         progressBar(status = 100);
                         $("#loadingModule").html("");
                         $("#result").append("Your user has been successfully created.");
                         $.ajax({
                             type: "POST",
                             url: "userAdd/complete.php",
                             data: { complete: "1" },
                             success: function(result)
                             {
                            	if(foundServerConErrorOnProcess(result, "Add User")) {
     	        					return;
     	                      	}
                             	deleteAddedUser(errorInModule, errorMessage, continueUserBuild);
                                 	$("#result").append(result);
									$(".ui-dialog-buttonpane", this.parentNode).show();
									$(":button:contains('Add Another User')").removeAttr("disabled").removeClass("ui-state-disabled");
									$(":button:contains('Add Another User')").show();
									$(":button:contains('Complete')").hide();
									$(":button:contains('Cancel')").hide();

                             }
                         });
                     }
	            }
	        });
		};
	});//end of function

	
	function deleteAddedUser(errorInModule, errorMessage, continueUserBuild)
	{
		if(continueUserBuild == 0)
		{
			$.ajax({
		        type: "POST",
		        url: "userAdd/deleteFailedAddUser.php",
		        data: { module: "deleteUser"},
		        success: function(result)
		        {
					var result = JSON.parse(result);
			      	if( (result.length > 0 && ((result[0].userDeleted && result[0].userDeleted == "Success") || (typeof(result[1]) != "undefined" ? result[1].deviceDeleted == "Success":""))) || (continueUserBuild == 0))
					{
			      		if(foundServerConErrorOnProcess(result, "Add User")) {
	        				return;
	                    }
						var backgroundColor = 'background:#ac5f5d; width:49%;';
			        	var changeString = "";
			        	changeString += '<table style="width:850px; margin:0px auto;" class="confSettingTable"><tr><td class="errorTableRows" style="'+backgroundColor+'">' + errorInModule + '</td><td class="errorTableRows">' + errorMessage + ' </td></tr></table>';
			        	$("#result").append("</br><div class=\"userNotAddalign\"> User Not Added Successfully. </div>");
			        	$("#result").append(changeString);

			        	$("#result").append("<div>" +returnLink + "</div>");	
					}
					else{
						$("#result").append("<div>" +returnLink + "</div>");	

					}
		        }
			
			});
			$(".ui-dialog-buttonpane", this.parentNode).show();
			$(":button:contains('Cancel')").addClass("cancelButton").show();
		} else {
			$(".ui-dialog-buttonpane", this.parentNode).show();
      		$(":button:contains('Cancel')").hide();
      		$(":button:contains('Add Another User')").addClass("subButton").show();
			$(":button:contains('Return to Main')").addClass("cancelButton").show();
	    }

// 		$(".ui-dialog-buttonpane", this.parentNode).show();
		$(":button:contains('Complete')").hide();
	}


</script>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<div id="result"></div>
		</div>
	</div>
</div>

