              <!----- BroadWorks Servers ----->
              		 <!--cluster data -->
             
      		<?php				
			if(isset($_POST['clusterNumber'])) {  /* If Request is a Ajax call. */
				require_once("/var/www/html/Express/config.php");
				checkLogin();
				
				$i = $_POST['clusterNumber'];
				$primaryServerResponse = "";
				$secondaryServerResponse = "";
				$clustVal = "";
				$clustName = "";
                                $hiddenClustName = "";
			}
				/* Input Names*/
				$count =  (string)$i;
				$clusterName = "cluster" .$count. "[clusterName]";
                                $hiddenClusterName = "cluster" .$count. "[hiddenClusterName]";
				$clusterId = "cluster" .$count. "[clusterId]";
				$clusterDivId = "clusterDivId" . $count;
				$clusterIdVal = (string)$clustVal['primary']['id'] . '::' . (string)$clustVal['secondary']['id'];
				$bwServerChoiceName = "cluster" .$count. "[bwServerChoice]";
				$primaryBwServerIP = "cluster" .$count. "[primaryBwServerIP]";
				$ociReleasePrimary = "cluster" .$count. "[ociReleasePrimary]";
                                //$primaryfqdn = "cluster" .$count. "[primaryfqdn]";
                                //$secondaryfqdn = "cluster" .$count. "[secondaryfqdn]";
				$ociReleaseSecondary = "cluster" .$count. "[ociReleaseSecondary]";
				$primaryBwUserName = "cluster" .$count. "[primaryBwUserName]";
				$primaryBwPassword = "cluster" .$count. "[primaryBwPassword]";
				$oldPrimaryBwPassword = "cluster" .$count. "[oldPrimaryBwPassword]";
				$primaryBwProtocol = "cluster" .$count. "[primaryBwProtocol]";
				$primaryBwPort = "cluster" .$count. "[primaryBwPort]";
				$secondaryBwServerIP = "cluster" .$count. "[secondaryBwServerIP]";
				$secondaryBwUserName = "cluster" .$count. "[secondaryBwUserName]";
				$oldSecondaryBwPassword = "cluster" .$count. "[oldSecondaryBwPassword]";
				$secondaryBwPassword = "cluster" .$count. "[secondaryBwPassword]";
				$secondaryBwProtocol = "cluster" .$count. "[secondaryBwProtocol]";
				$secondaryBwPort = "cluster" .$count. "[secondaryBwPort]";
                                $bwServerChoicePrimary = "bwServerChoicePrimary" . $count;
				$bwServerChoiceSecondary = "bwServerChoiceSecondary" . $count;
				
				/* Input Values */
				$countCluster = isset($getClusterInfo) ? count($getClusterInfo) : "";	
                                $primaryBwServerIPVal = isset($clustVal['primary']['ip']) ? $clustVal['primary']['ip'] : "";
				$ociReleasePrimaryVal = isset($clustVal['primary']['ociRelease']) ? $clustVal['primary']['ociRelease'] : "";
				$primaryBwUserNameVal = isset($clustVal['primary']['userName']) ? $clustVal['primary']['userName'] : "";
				$oldPrimaryBwPasswordVal = isset($clustVal['primary']['password']) ? $clustVal['primary']['password'] : "";
				$primaryBwPasswordVal = $oldPrimaryBwPasswordVal != "" ? "******" : "";
				$primaryBwProtocolVal = isset($clustVal['primary']['protocol']) ? $clustVal['primary']['protocol']: "";
				$primaryBwPortVal = isset($clustVal['primary']['port']) ? $clustVal['primary']['port']: "";
				$bwServerChoicePrimaryVal = isset($clustVal['primary']['active']) ? $clustVal['primary']['active']: "";
                                //$primaryfqdnVal = isset($clustVal['primary']['fqdn']) ? $clustVal['primary']['fqdn']: "";
				
				//$softwareRevisionPrimary = isset($testConn) ? $testConn->getSoftwaReversion($primaryBwProtocolVal, $primaryBwServerIPVal, $primaryBwPortVal, $primaryBwUserNameVal, $oldPrimaryBwPasswordVal) : "";
				
				$secondaryBwServerIPVal = isset($clustVal['secondary']['ip']) ? $clustVal['secondary']['ip']: "";
				$ociReleaseSecondaryVal=isset($clustVal['secondary']['ociRelease']) ? $clustVal['secondary']['ociRelease']: "";
				$secondaryBwUserNameVal = isset($clustVal['secondary']['userName']) ? $clustVal['secondary']['userName']: "";
				$oldSecondaryBwPasswordVal = isset($clustVal['secondary']['password']) ? $clustVal['secondary']['password']: "";
				$secondaryBwPasswordVal = $oldSecondaryBwPasswordVal != "" ? "******" : "";
				$secondaryBwProtocolVal = isset($clustVal['secondary']['protocol']) ? $clustVal['secondary']['protocol']: "";
				$secondaryBwPortVal =  isset($clustVal['secondary']['port']) ? $clustVal['secondary']['port']: "";
                                $bwServerChoiceSecondaryVal = isset($clustVal['secondary']['active']) ? $clustVal['secondary']['active']: "";   
                                //$secondaryfqdnVal = isset($clustVal['secondary']['fqdn']) ? $clustVal['secondary']['fqdn']: "";
				//$softwareRevisionSecondary = isset($testConn) ? $testConn->getSoftwaReversion($secondaryBwProtocolVal, $secondaryBwServerIPVal, $secondaryBwPortVal, $secondaryBwUserNameVal, $oldSecondaryBwPasswordVal) : "";
				
                                
                /* XSP Server Details */
                $xspPrimaryBwServerIP = "cluster" .$count. "[primaryXspServerIP]";
                $xspPrimaryBwServerIPValDup = isset($getXSPInfo[$clustName]["primary"]['ip']) ? $getXSPInfo[$clustName]["primary"]['ip'] : "";
                $xspPrimaryBwProtocol = "cluster" .$count. "[primaryXspProtocol]";
                $xspPrimaryBwProtocolValDup = isset($getXSPInfo[$clustName]["primary"]['protocol']) ? $getXSPInfo[$clustName]["primary"]['protocol'] : "";
                $xspPrimaryBwPort = "cluster" .$count. "[primaryXspPort]";
                $xspPrimaryBwPortValDup = isset($getXSPInfo[$clustName]["primary"]['port']) ? $getXSPInfo[$clustName]["primary"]['port'] : "";
                
                $xspSecondaryBwServerIP = "cluster" .$count. "[secondaryXspServerIP]";
                $xspSecondaryBwServerIPValDup = isset($getXSPInfo[$clustName]["secondary"]['ip']) ? $getXSPInfo[$clustName]["secondary"]['ip'] : "";
                $xspSecondaryBwProtocol = "cluster" .$count. "[secondaryXspProtocol]";
                $xspSecondaryBwProtocolValDup = isset($getXSPInfo[$clustName]["secondary"]['protocol']) ? $getXSPInfo[$clustName]["secondary"]['protocol'] : "";
                $xspSecondaryBwPort = "cluster" .$count. "[secondaryXspPort]";
                $xspSecondaryBwPortValDup = isset($getXSPInfo[$clustName]["secondary"]['port']) ? $getXSPInfo[$clustName]["secondary"]['port'] : "";
                $bwXspServerChoiceName = "cluster" .$count. "[xspServerChoice]";
                $bwXspServerChoicePrimaryValDup = isset($getXSPInfo[$clustName]['primary']['active']) ? $getXSPInfo[$clustName]['primary']['active']: "";
                $bwXspServerChoiceSecondaryValDup = isset($getXSPInfo[$clustName]['secondary']['active']) ? $getXSPInfo[$clustName]['secondary']['active']: "";
                
                $xspServerChoicePrimary = "xspServerChoicePrimary" . $count;
                $xspServerChoiceSecondary = "xspServerChoiceSecondary" . $count;
                
                $xspServersId = "cluster" .$count. "[xspServersId]";
                $xspServersIdValDup = (string)$getXSPInfo[$clustName]['primary']['id'] . '::' . (string)$getXSPInfo[$clustName]['secondary']['id'];
                /* End XSP Server Details */
                                
                                
						if(! function_exists('getBroadSoftReleases')){
							function getBroadSoftReleases($revision) {
								global $db;
								$str = "";
								// initially revision number is not initialized
								$str = "<option value=\"\"";
								if ($revision == "") {
									$str .= " selected";
								}
								$str .= "></option>";
								$releases = getAllBwReleasesFormDB();
								foreach ($releases as $index=>$release) {
									$str .= "<option value=\"" . $release . "\"";
									if ($release == $revision) {
										$str .= " selected";
									}

									$str .= ">Rel. " . $release . "</option>";
								}

							//         echo $str;
								return $str;
							}
								
							function getAllBwReleasesFormDB() {
								global $db;
								$releases = array();

								$query   = "select version from versionLookup";
								$results = $db->query($query);

								while ($row = $results->fetch()) {
									$releases[] = $row["version"];
								}
								return $releases;
							}
						}

					   $ociReleasePrimaryValue = getBroadSoftReleases($ociReleasePrimaryVal);
					   $ociReleaseSecondaryValue = getBroadSoftReleases($ociReleaseSecondaryVal);
			       
			?>
			
			<script>
			 $('.clusterName').on("input",function(){
				$(this).siblings( ".clusterNameErrorMsg" ).css( "display", "none" );

				if( ! $(this).val().trim() ) {
				$(this).siblings( ".clusterNameErrorMsg" ).css( "display", "block" );
				$("#submitServersConfig").prop('disabled', true);
				}else{
				$("#submitServersConfig").prop('disabled',false);
				}
				});
				
				
				/*
				
				*/
			 </script>
      					
 						<div class="clusterServer" id="<?php echo $clusterDivId; ?>">
								<!----- BroadWorks Servers ----->	
										<div class="row" style="margin-left:0px !important">    
											<div class="col-md-5"> 
												<label class="labelText" for=" ">Cluster Name:<span class="required">*</span></label><br/>
												<div class="form-group">
													<input type="hidden" name="<?php echo $clusterId; ?>" value="<?php echo $clusterIdVal; ?>">
													<input type="hidden" name="<?php echo $xspServersId; ?>" value="<?php echo $xspServersIdValDup; ?>">
													<input type="text" class="clusterName" name="<?php echo $clusterName; ?>" id="" size="17" value="<?php echo $clustName; ?>" oninput="updateChangeStatus(this,'toggleServersConfig')">
													<input type="hidden"   name="<?php echo $hiddenClusterName; ?>" id="" size="17" value="<?php echo $clustName; ?>" oninput="updateChangeStatus(this,'toggleServersConfig')">
                                                                                                        <label class="labelText error clusterNameErrorMsg" style="display: none" for="">Cluster Name is required field.</label>
												</div>
											</div>
					
					
											<div class="col-md-4"> 
												<div class="form-group">
													
												 &nbsp;
												</div>
											</div>
										
											<div class="col-md-3"> 
												<div class="form-group">
												<input <?php echo $countCluster == 1 ? "disabled" : ""?> type="button" class="deleteButton delButton" value="Delele Cluster" style="float:right;" onClick="deleteCluster('<?php echo $clusterDivId;?>','<?php echo $clusterIdVal;?>', '<?php  echo $clustName?>');">
												</div>
											</div>
					
										</div>
    
                        
                        <!--server details -->
                        <div class="singleServerDiv">
										<div class="fcorn-registerTwo">
											<div style="margin-left:0px">
												<div class="col-md-4">
													<div class="">
														<label class="labelText labelTextZero">Primary Server</label><br/>
														<label class="labelTextGrey">Broadworks Realease: <label class="bwRelease"><?php echo $primaryServerResponse["version"] ; ?></label>
													</div>					
												</div>
				 
            		
											<div class="col-md-6">
												<div class="form-group">
													<input type="radio" name="<?php echo $bwServerChoiceName ;?>" id="<?php echo $bwServerChoicePrimary; ?>" value="Primary" <?php echo $bwServerChoicePrimaryVal == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServersConfig')">
    				
													<label for="<?php echo $bwServerChoicePrimary; ?>"><span></span></label>
													<label class="labelText customLabelText">Connect to Primary Server</label>
												   <?php 
															if($primaryServerResponse["status"])
																 echo "<label class='connectedPrimary' style='font-size: 11px;color: #72ac5d;font-weight: 600;'>( CONNECTED )</label>";  
															else
																echo "<label style='font-size: 11px;color: #ac5f5d;font-weight: 600;'>( NOT CONNECTED ) </label>";
													?>                                                    
												</div>
											</div>
                     
											<div class="col-md-2" style="width:100%">            				 
											</div>
                    
											</div>  
										</div>
                        
                        
                        <!--end server details --->
                        
                        <!--ip setting -->
										<div class="col-md-2" style="width:22% !important;"> 
											<label class="labelText" for=" ">Ip Address / FQDN:</label><br/>
											<div class="form-group">
												
												<input type="text" name="<?php echo $primaryBwServerIP; ?>" class = "clusterFirstTab ipAddress priIpAddress" size="17" <?php echo "value=\"" . $primaryBwServerIPVal . "\""; ?>  onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')" > 
											</div>
										</div>
                                                                                
                                                                                
                        
										<div class="col-md-2" > 
											<label class="labelText" for=" ">OCI Release:</label><br/>
											<div class="form-group">
												<div class="dropdown-wrap oneColWidth cluster"> 
														 <select name="<?php echo $ociReleasePrimary; ?>" id="<?php echo $ociReleasePrimary; ?>" class = "OCIReleasePrimary ociRelease cluster" onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')" ><?php echo $ociReleasePrimaryValue; ?></select>
												</div>
											</div>
										</div>
                        
                        
                        
										 <div class="col-md-2" > 
											<label class="labelText" for=" ">Username:</label><br/>
											<div class="form-group">
															<input type="text" name="<?php echo $primaryBwUserName; ?>" class = "cluster userName" size="17" <?php echo "value=\"" . $primaryBwUserNameVal . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')"> 
											</div>
										</div>
                        
                        
										 <div class="col-md-2" > 
											<label class="labelText" for=" ">Password:</label><br/>
											<div class="form-group">
												
												 <input type="hidden" class="oldPassword" name="<?php echo $oldPrimaryBwPassword; ?>" value="<?php echo $oldPrimaryBwPasswordVal; ?>" />
											<input type="password" name="<?php echo $primaryBwPassword; ?>" size="17"
												   placeholder="<?php echo  $primaryBwPasswordVal; ?>"
												    class = "cluster password"  onchange="updateChangeStatus(this, 'toggleServersConfig')"
                                          oninput="updateChangeStatus(this,'toggleServersConfig')"> 
											</div>
										</div>
                        
                        
									 <div class="col-md-2" > 
										<label class="labelText" for=" ">Protocol:</label><br/>
										<div class="form-group cluster" >
											<div class="dropdown-wrap">
												<select name="<?php echo $primaryBwProtocol; ?>" class="cluster protocol" onchange="updateBwPort(this.value, 'primaryBwPort')" oninput="updateChangeStatus(this,'toggleServersConfig')"><option value="https" <?php if($primaryBwProtocolVal=="https") {  echo "selected='selected'"; } ?>>HTTPS</option>
												<option value="http" <?php if($primaryBwProtocolVal =="http") {  echo "selected='selected'"; } ?>>HTTP</option></select>
											</div>
										</div>
									</div>
                        
                        
									<div class="col-md-2" style="width:10% !important;"> 
										<label class="labelText" for=" ">Port:</label><br/>
										<div class="form-group">
											<input type="text" name="<?php echo $primaryBwPort; ?>" class = "clusterLastTab port" size="17" <?php echo "value=\"" . $primaryBwPortVal . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')">
										</div>
									</div>
                        </div>
                        <!--end ip setting -->
                        
    <!-- known secondary server name -->
      					<div class="singleServerDiv">
									<div class="fcorn-registerTwo">
										<div class="" style="margin-left:0px">
											<div class="col-md-4">
												<div class="">
													<label class="labelText labelTextZero">Secondary Server:</label> <br/>
													<label class="labelTextGrey">Broadworks Realease: <label class="bwRelease"><?php echo $secondaryServerResponse["version"] ; ?> </label>
												</div>					
											</div>
				 
            		
											<div class="col-md-6">
												<div class="form-group">
													<input  type="radio" name="<?php echo $bwServerChoiceName; ?>" id="<?php echo $bwServerChoiceSecondary; ?>" value="Secondary" <?php   echo $bwServerChoiceSecondaryVal == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServersConfig')">
            				
													<label for="<?php echo $bwServerChoiceSecondary; ?>"><span></span></label>
													<label class="labelText customLabelText">Connect to Secondary Server</label>
                                                    <?php 
                                                            if($secondaryServerResponse["status"])
                                                                echo "<label class='connectedSecondary' style='font-size: 11px;color: #72ac5d;font-weight: 600;'>( CONNECTED )</label>";
                                                                else
                                                                    echo "<label class='' style='font-size: 11px;color: #ac5f5d;font-weight: 600;'>( NOT CONNECTED ) </label>";
                                                    ?>
                                                </div>
                                           </div>
                     
										<div class="col-md-2" style="width:100%"></div>
									</div>  
								</div>
    
    <!--end known server name -->
               <!--secondary ip address details -->
                <!--ip setting -->
							<div class="col-md-2" style="width:22% !important;"> 
								<label class="labelText" for=" ">Ip Address / FQDN:</label><br/>
								<div class="form-group">                
									<input type="text" name="<?php echo $secondaryBwServerIP; ?>" size="17" <?php echo "value=\"" . $secondaryBwServerIPVal . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')" class = "clusterFirstTab ipAddress secIpAddress"> 
								</div>
							</div>
                
							<div class="col-md-2"> 
								<label class="labelText" for=" ">OCI Release:</label><br/>
							<div class="form-group">
										<div class="dropdown-wrap oneColWidth cluster"> 
												 <select name="<?php echo $ociReleaseSecondary; ?>" id="<?php echo $ociReleaseSecondary; ?>" class="OCIReleaseSecondary cluster" onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')"><?php echo $ociReleaseSecondaryValue; ?></select>
										</div>
							</div>
											
											
							</div>                        
                        
							 <div class="col-md-2" > 
								<label class="labelText" for=" ">Username:</label><br/>
								<div class="form-group">
												<input type="text" name="<?php echo $secondaryBwUserName; ?>" class = "cluster userName" size="17" <?php echo "value=\"" . $secondaryBwUserNameVal . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')">
								</div>
							</div>
                        
                        
							 <div class="col-md-2" > 
								<label class="labelText" for=" ">Password:</label><br/>
								<div class="form-group">
									 <input type="hidden" class="oldPassword" name="<?php echo $oldSecondaryBwPassword; ?>" value="<?php echo $oldSecondaryBwPasswordVal; ?>"/>

								<input type="password" name="<?php echo $secondaryBwPassword; ?>" size="17"
									   placeholder="<?php echo $secondaryBwPasswordVal; ?>"
									   onchange="updateChangeStatus(this,'toggleServersConfig')"
									   oninput="updateChangeStatus(this,'toggleServersConfig')" class="cluster password">
								</div>
							</div>
                        
							 <div class="col-md-2"> 
								<label class="labelText" for=" ">Protocol:</label><br/>
								<div class="form-group cluster">
									<div class="dropdown-wrap">
												<select name="<?php echo $secondaryBwProtocol; ?>" class = "cluster protocol" onchange="updateBwPort(this.value, 'secondaryBwPort')" oninput="updateChangeStatus(this,'toggleServersConfig')">
											<option value="https" <?php if($secondaryBwProtocolVal =="https") {  echo "selected='selected'"; } ?>>HTTPS</option>
											<option value="http" <?php if($secondaryBwProtocolVal=="http") {  echo "selected='selected'"; } ?>>HTTP</option>
										</select>
								</div>
												</div>
							</div>
                        
                         <div class="col-md-2" style="width:10% !important;"> 
                            <label class="labelText" for=" ">Port:</label><br/>
                			<div class="form-group">
                				<input type="text" name="<?php echo $secondaryBwPort; ?>" size="17" <?php echo "value=\"" . $secondaryBwPortVal . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')" class="clusterLastTab port">
            				</div>
                		</div>
                		
                	</div>
    
    
     <!--primary xsp config -->
               <!----- BroadWorks Servers ----->
               <div class="singleServerDiv">
                    <div class="fcorn-registerTwo">
                        <div style="margin-left:0px">
                            <div class="col-md-4">
                                <div class="">
                                    <label class="labelText labelTextZero">Primary XSP Server</label><br/>
                                </div>					
                            </div>

                            <div class="col-md-6">
                                <div class="">
                                    <input type="radio" name="<?php echo $bwXspServerChoiceName ;?>" id="<?php echo $xspServerChoicePrimary; ?>" value="Primary" <?php echo $bwXspServerChoicePrimaryValDup == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServersConfig')">
                                    <label for="<?php echo $xspServerChoicePrimary; ?>"><span></span></label>
                                    <label class="labelText customLabelText">Connect to Primary XSP server</label>
                                </div>
                            </div>

                            <div class="col-md-2" style="width:100%"></div>
                        </div>  
                    </div>
            
            
            <!--end xsp server details --->
            
            <!--xsp ip setting -->
                    <div class="col-md-5"> 
                        <label class="labelText" for="">XSP IP / FQDN:</label><br/>
                        <div class="form-group">
                            <input type="text" name="<?php echo $xspPrimaryBwServerIP; ?>" class = "clusterFirstTab ipAddress priIpAddress" size="17" <?php echo "value=\"" . $xspPrimaryBwServerIPValDup . "\""; ?>  onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')" style="width:100% !important"> 
                        </div>
                    </div>
                    
                <div class="col-md-1"></div>
                
                    <div class="col-md-3"> 
                        <label class="labelText" for="">Protocol:</label><br/>
                        <div class="form-group" >
                            <div class="dropdown-wrap">
                                <select name="<?php echo $xspPrimaryBwProtocol; ?>" class="protocol" onchange="updateBwPort(this.value, 'primaryBwPort')" oninput="updateChangeStatus(this,'toggleServersConfig')" style="width:100% !important"><option value="https" <?php if($xspPrimaryBwProtocolValDup=="https") {  echo "selected='selected'"; } ?> >HTTPS</option>
                                    <option value="http" <?php if($xspPrimaryBwProtocolValDup =="http") {  echo "selected='selected'"; } ?>>HTTP</option></select>
                            </div>
                        </div>
                   </div>
                     
                    <div class="col-md-1"></div>
            
                    <div class="col-md-2"> 
                            <label class="labelText" for="">Port:</label><br/>
                            <div class="form-group">
                                    <input type="text" name="<?php echo $xspPrimaryBwPort; ?>" class = "clusterLastTab port" size="17" <?php echo "value=\"" . $xspPrimaryBwPortValDup . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')" style="width:97% !important">
                            </div>
                    </div>
            </div>
     <!-- end primary xsp config -->
                        
    <!--secondary xsp cluster server ip -->
     
    <div class="singleServerDiv">
        <div class="fcorn-registerTwo">
            <div class="" style="margin-left:0px">
                <div class="col-md-4">
                    <div class="">
                            <label class="labelText labelTextZero">Secondary XSP Server:</label> <br/>
                    </div>					
                </div>

                <div class="col-md-6">
                    <div class="">
                        <input  type="radio" name="<?php echo $bwXspServerChoiceName; ?>" id="<?php echo $xspServerChoiceSecondary; ?>" value="Secondary" <?php   echo $bwXspServerChoiceSecondaryValDup == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServersConfig')">
                        <label for="<?php echo $xspServerChoiceSecondary; ?>"><span></span></label>
                        <label class="labelText customLabelText">Connect to Secondary XSP server</label>
                        <?php 
                          /*  if($secondaryServerResponse)
                                echo "<label class='connectedSecondary' style='font-size: 11px;color: #72ac5d;font-weight: 600;'>( CONNECTED )</label>";
                            else
                                echo "<label class='' style='font-size: 11px;color: #ac5f5d;font-weight: 600;'>( NOT CONNECTED ) </label>";
                            */
                            ?>
                    </div>
                </div>
                <div class="col-md-2" style="width:100%"></div>
            </div>  
        </div>
    
    <!--end known server name -->
               <!--secondary ip address details -->
                <!--secondary xsp ip setting -->
        <div class="col-md-5"> 
            <label class="labelText" for=" ">XSP IP / FQDN:</label><br/>
            <div class="form-group">                
                <input type="text" name="<?php echo $xspSecondaryBwServerIP; ?>" size="17" <?php echo "value=\"" . $xspSecondaryBwServerIPValDup . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')" class = "clusterFirstTab ipAddress secIpAddress" style="width:100% !important"> 
            </div>
        </div>

        <div class="col-md-1"></div>

        <div class="col-md-3"> 
            <label class="labelText" for=" ">Protocol:</label><br/>
            <div class="form-group">
                 <div class="dropdown-wrap">
                     <select name="<?php echo $xspSecondaryBwProtocol; ?>" class = "protocol" onchange="updateBwPort(this.value, 'secondaryBwPort')" oninput="updateChangeStatus(this,'toggleServersConfig')" style="width:100% !important">
                         <option value="https" <?php if($xspSecondaryBwProtocolValDup =="https") {  echo "selected='selected'"; } ?>>HTTPS</option>
                         <option value="http" <?php if($xspSecondaryBwProtocolValDup=="http") {  echo "selected='selected'"; } ?>>HTTP</option>
                     </select>
                 </div>
             </div>
       </div>

        <div class="col-md-1"></div>

        <div class="col-md-2"> 
            <label class="labelText" for=" ">Port:</label><br/>
            <div class="form-group">
                <input type="text" name="<?php echo $xspSecondaryBwPort; ?>" size="17" <?php echo "value=\"" . $xspSecondaryBwPortValDup . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')" class="clusterLastTab port" style="width:97% !important">
            </div>
        </div>

    </div>
    
    
    
    
    <!--end secondary xsp server ip -->
                    <div class="row"><div class="col-md-6">&nbsp; </div></div>
    				<div class="row"><div class="col-md-6">&nbsp; </div></div>
                </div>
                <?php
				//$i++;
          		//}
          		?>     
 					
              <!----- End BroadWorks Servers ----->	 
                   