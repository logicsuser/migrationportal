 <!--cluster data -->
 
 <?php 
		$bwReleasePrimary = isset($testConn) ? $testConn->getSoftwaReversion($primaryBwProtocol, $primaryBwServerIP, $primaryBwPort, $primaryBwUserName, $primaryBwPassword) : "";
		
		$bwReleaseSecondary = isset($testConn) ? $testConn->getSoftwaReversion($secondaryBwProtocol, $secondaryBwServerIP, $secondaryBwPort, $secondaryBwUserName, $secondaryBwPassword) : "";
 ?>
 <div id="clusterServer">	
 
										<div class="fcorn-registerTwo">
											<div style="margin-left:0px">
													<div class="col-md-4">
														<div class="">
															<label class="labelText labelTextZero">Primary Server</label><br/>
															<label class="labelTextGrey">Broadworks Realease.<?php echo $bwReleasePrimary; ?>
														</div>					
													</div>
				 
            		
													<div class="col-md-6">
														<div class="form-group">
															<input type="radio" name="bwServerChoice" id="bwServerChoicePrimary" value="Primary" <?php echo $bwServerChoice == "Primary" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServersConfig')">
            				
															<label for="bwServerChoicePrimary"><span></span></label>
															<label class="labelText customLabelText">Connect to Primary Server</label>
														   <?php 
																	if($primaryServerResponse)
																		 echo "<label style='font-size: 11px;color: #72ac5d;font-weight: 600;'>( CONNECTED )</label>";  
																	else
																		echo "<label style='font-size: 11px;color: #ac5f5d;font-weight: 600;'>( NOT CONNECTED ) </label>";
															?>                                                    
														</div>
													</div>
                     
                     
                     
												<div class="col-md-2" style="width:100%">
            				 
												</div>
                    
											</div>  
										</div>
                        
                        
                        <!--end server details --->
                        
                        <!--ip setting -->
										<div class="col-md-2" style="width:22% !important;"> 
											<label class="labelText" for=" ">Ip Address / FQDN:</label><br/>
											<div class="form-group">
												
												<input type="text" name="primaryBwServerIP" class = "clusterFirstTab" id="primaryBwServerIP" size="17" <?php echo "value=\"" . $primaryBwServerIP . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')"> 
											</div>
										</div>
                        
										<div class="col-md-2" > 
											<label class="labelText" for=" ">OCI Release:</label><br/>
											<div class="form-group">
												<div class="dropdown-wrap oneColWidth cluster"> 
														 <select name="ociReleasePrimary" id="bwRelease" class = "cluster" onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')">
														 <?php echo getBroadSoftRelease($ociReleasePrimary); ?></select>
												</div>
											</div>
										</div>
                        
                        
                        
										 <div class="col-md-2" > 
											<label class="labelText" for=" ">Username:</label><br/>
											<div class="form-group">
															<input type="text" name="primaryBwUserName" id="primaryBwUserName" class = "cluster" size="17" <?php echo "value=\"" . $primaryBwUserName . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')"> 
											</div>
										</div>
                        
                        
										 <div class="col-md-2" > 
											<label class="labelText" for=" ">Password:</label><br/>
											<div class="form-group">
												
												 <input type="hidden" name="oldPrimaryBwPassword" value="<?php echo $primaryBwPassword; ?>" />
											<input type="password" name="primaryBwPassword" id="primaryBwPassword" size="17"
												   placeholder="<?php echo $primaryBwPassword ? "******" : ""; ?>"
												   onchange="updateChangeStatus(this, 'toggleServersConfig')"
												   oninput="updateChangeStatus(this,'toggleServersConfig')" class = "cluster"> 
											</div>
										</div>
                        
                        
									 <div class="col-md-2" > 
										<label class="labelText" for=" ">Protocol:</label><br/>
										<div class="form-group cluster" >
											<div class="dropdown-wrap">
												<select name="primaryBwProtocol" id="primaryBwProtocol" class = "cluster" onchange="updateBwPort(this.value, 'primaryBwPort')" oninput="updateChangeStatus(this,'toggleServersConfig')"><option value="https" <?php if($primaryBwProtocol=="https") {  echo "selected='selected'"; } ?>>HTTPS</option>
												<option value="http" <?php if($primaryBwProtocol=="http") {  echo "selected='selected'"; } ?>>HTTP</option></select>
											</div>
										</div>
									</div>
                        
                        
									<div class="col-md-2" style="width:10% !important;"> 
										<label class="labelText" for=" ">Port:</label><br/>
										<div class="form-group">
                				
											<input type="text" name="primaryBwPort" id="primaryBwPort" class = "clusterLastTab" size="17" <?php echo "value=\"" . $primaryBwPort . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')">
										</div>
									</div> 
                        <!--end ip setting -->
                        
                  
    <!-- known secondary server name -->
      
									<div class="fcorn-registerTwo">
										<div class="" style="margin-left:0px">
											<div class="col-md-4">
												<div class="">
													<label class="labelText labelTextZero">Secondary Server:</label> <br/>
													<label class="labelTextGrey">Broadworks Realease.<?php echo $bwReleaseSecondary; ?>
												</div>					
											</div>
				 
            		
											<div class="col-md-6">
												<div class="form-group">
													<input  type="radio" name="bwServerChoice" id="bwServerChoiceSecondary" value="Secondary" <?php echo $bwServerChoice == "Secondary" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServersConfig')">
            				
													<label for="bwServerChoiceSecondary"><span></span></label>
													<label class="labelText customLabelText">Connect to Secondary Server</label>
                                                    <?php 
                                                            if($secondaryServerResponse)
                                                                echo "<label style='font-size: 11px;color: #72ac5d;font-weight: 600;'>( CONNECTED )</label>";
                                                                else
                                                                    echo "<label style='font-size: 11px;color: #ac5f5d;font-weight: 600;'>( NOT CONNECTED ) </label>";
                                                    ?>
                                                </div>
                                           </div>
                     
										<div class="col-md-2" style="width:100%"></div>
									</div>  
								</div>
    
    <!--end known server name -->
               <!--secondary ip address details -->
                <!--ip setting -->
							<div class="col-md-2" style="width:22% !important;"> 
								<label class="labelText" for=" ">Ip Address / FQDN:</label><br/>
								<div class="form-group">
									<input type="text" name="secondaryBwServerIP" id="secondaryBwServerIP" size="17" <?php echo "value=\"" . $secondaryBwServerIP . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')" class = "clusterFirstTab"> 
								</div>
							</div>
                        
                
							<div class="col-md-2" > 
								<label class="labelText" for=" ">OCI Release:</label><br/>
							<div class="form-group">
										<div class="dropdown-wrap oneColWidth cluster"> 
												 <select name="ociReleaseSecondary" id="bwRelease" class = "cluster" onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')"><?php echo getBroadSoftRelease($ociReleaseSecondary); ?></select>
										</div>
							</div>
											
											
							</div>
                        
                        
                        
							 <div class="col-md-2" > 
								<label class="labelText" for=" ">Username:</label><br/>
								<div class="form-group">
												<input type="text" name="secondaryBwUserName" id="secondaryBwUserName" class = "cluster" size="17" <?php echo "value=\"" . $secondaryBwUserName . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')">
								</div>
							</div>
                        
                        
							 <div class="col-md-2" > 
								<label class="labelText" for=" ">Password:</label><br/>
								<div class="form-group">
									 <input type="hidden" name="oldSecondaryBwPassword" value="<?php echo $secondaryBwPassword; ?>"/>

								<input type="password" name="secondaryBwPassword" id="secondaryBwPassword" size="17"
									   placeholder="<?php echo $secondaryBwPassword ? "******" : ""; ?>"
									   onchange="updateChangeStatus(this,'toggleServersConfig')"
									   oninput="updateChangeStatus(this,'toggleServersConfig')" class = "cluster">
								</div>
							</div>
                        
                        
							 <div class="col-md-2" > 
								<label class="labelText" for=" ">Protocol:</label><br/>
								<div class="form-group cluster">
									<div class="dropdown-wrap">
												<select name="secondaryBwProtocol" id="secondaryBwProtocol"  class = "cluster" onchange="updateBwPort(this.value, 'secondaryBwPort')" oninput="updateChangeStatus(this,'toggleServersConfig')">
											<option value="https" <?php if($secondaryBwProtocol=="https") {  echo "selected='selected'"; } ?>>HTTPS</option>
											<option value="http" <?php if($secondaryBwProtocol=="http") {  echo "selected='selected'"; } ?>>HTTP</option>
										</select>
								</div>
												</div>
							</div>
                        
                        
                         <div class="col-md-2" style="width:10% !important;"> 
                            <label class="labelText" for=" ">Port:</label><br/>
                			<div class="form-group">
                				<input type="text" name="secondaryBwPort" id="secondaryBwPort" size="17" <?php echo "value=\"" . $secondaryBwPort . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')" class = "clusterLastTab">
            				</div>
                		</div>
                     
 </div>