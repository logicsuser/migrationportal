<?php
require_once("/var/www/html/Express/config.php");
checkLogin();

$allowed_types = array('image/jpeg', 'image/png');
$logo_directory = '/var/www/logos';
$loginLogo_directory = '/var/www/logos/loginPageLogo';
$errorInConfig = false;
$responseMsg = "";
//$errorMessage = "";

if(isset($_GET['remove_file'])) {

    $logo_filename = $_GET["remove_file"];
    $logoType = isset($_GET['logoType']) ? $_GET['logoType'] : "";
    $directory = $logoType == "loginPage" ? $loginLogo_directory : $logo_directory;
	$logo_filepath = "$directory/$logo_filename";
	
	if (file_exists($logo_filepath)) {
		if(unlink($logo_filepath)) {
		    if(deleteLogoInfo($logo_filename, $logoType)) {
		        echo "File has been deleted.";
		    } else {
		        echo "Couldn't delete the file.";
		    }
			
		} else {
			echo "Couldn't delete the file.";
		}
	} else {
		echo "File doesn't exist.";
	}
	exit;
}

/* update branding config */
updateCustomerLogoOnLoginPage($_POST);
setLoginPageLogo($_POST, $_FILES, $loginLogo_directory, $allowed_types);

$colorSchema = isset($_POST["colorSchema"]) && $_POST["colorSchema"] ? $_POST["colorSchema"] : "Express Original";
setColorSchema($colorSchema);
//upload new image
if (isset($_FILES["logo"]) && isset($_FILES["logo"]["name"]) && strlen($_FILES["logo"]["name"]) > 0) {

	if (!file_exists($logo_directory)) {
		mkdir($logo_directory, 0777, true);
	}

	if (file_exists($logo_directory)) {

		if (in_array($_FILES['logo']['type'], $allowed_types)) {

			$ext = pathinfo($_FILES["logo"]["name"], PATHINFO_EXTENSION);

			$logo_filename = "logo_" . time() . ".$ext";
			$logo_filepath = "$logo_directory/logo_" . time() . ".$ext";

			if (move_uploaded_file($_FILES["logo"]["tmp_name"], $logo_filepath)) {
				$updated_by = isset($_SESSION["adminId"]) ? $_SESSION["adminId"] : 0;
				logoUnselect("header");
				if ( setLogoInfoIntoExpressLogoTable($logo_filename, "header", "true") ) {
				    $responseMsg .= "Logo has been changed.\n";
					//return;
				} else {
				    echo "Logo was not uploaded.\n";
					return;
				}

			} else {

			}

		} else {
		    $errorInConfig = true;
			echo "Logo was not uploaded. Please select a jpeg or png image file.\n";
			exit;
		}

	} else {
	    $errorInConfig = true;
		echo "Couldn't create directory: '$logo_directory'";
		exit;
	}

} else if(isset($_POST["set_logo"])) {
	$logo_filename = $_POST["set_logo"];
	$res = updateLogoInfoIntoExpressLogoTable($logo_filename, "header");
	if($res === "NoChanges") {
	} else if ($res) {
	    $responseMsg .= "Logo has been changed.\n";
		//return;
	}
	else {
	    $responseMsg .= "Logo was not changed.\n";
		//return;
	}
}

if($errorInConfig) {
    echo "Logo was not uploaded.";
} else {
    echo "Express branding configuration have been updated";
    //echo $responseMsg;
}


/* ========================================================================================================= */

function setLoginPageLogo($POST, $FILES, $loginLogo_directory, $allowed_types) {
    if (isset($FILES["loginLogo"]) && isset($FILES["loginLogo"]["name"]) && strlen($FILES["loginLogo"]["name"]) > 0) {
        
        if (!file_exists($loginLogo_directory)) {
            mkdir($loginLogo_directory, 0777, true);
        }
        if (file_exists($loginLogo_directory)) {
            if (in_array($FILES['loginLogo']['type'], $allowed_types)) {
                $ext = pathinfo($FILES["loginLogo"]["name"], PATHINFO_EXTENSION);
                $logo_filename = "loginLogo_" . time() . ".$ext";
                $logo_filepath = "$loginLogo_directory/loginLogo_" . time() . ".$ext";
                if (move_uploaded_file($FILES["loginLogo"]["tmp_name"], $logo_filepath)) {
                    $updated_by = isset($_SESSION["adminId"]) ? $_SESSION["adminId"] : 0;
                    logoUnselect("loginPage");
                    if(setLogoInfoIntoExpressLogoTable($logo_filename, "loginPage", "true")) {
                        $GLOBALS['responseMsg'] .= "Login Page Logo has been changed.\n";
                        //return;
                    } else {
                        echo "Login Page Logo was not uploaded.\n";
                        return;
                    }
                } else {
                    
                }
                
            } else {
                $GLOBALS['errorInConfig'] = true;
                echo "Login Page Logo was not uploaded. Please select a jpeg or png image file.\n";
                exit;
            }
            
        } else {
            $GLOBALS['errorInConfig'] = true;
            echo "Couldn't create directory: '$loginLogo_directory'\n";
            exit;
        }
        
    } else if(isset($POST["set_loginLogo"])) {
        $logo_filename = $POST["set_loginLogo"];
        $res = updateLogoInfoIntoExpressLogoTable($logo_filename, "loginPage");
        if( $res === "NoChanges" ) {
            //return;
        } else if ($res) {
            $GLOBALS['responseMsg'] .= "Login Page Logo has been changed.\n";
            //return;
        }
        else {
            echo "Login Page Logo was not changed.\n";
            //return;
        }
        
    } else if( !isset($POST["set_loginLogo"]) ) {
        logoUnselect("loginPage");
    }
}

function setLogoInfoIntoExpressLogoTable($logoName, $type, $selected) {
    global $db;
    $params = array($type, $selected, $logoName);
    $insert_logo = $db->prepare("insert into expressLogos (type, selected, filename) VALUES (?, ?, ?)");
    if ($insert_logo->execute($params)) {
        return true;
    } else {
        return false;
    }
}

function updateLogoInfoIntoExpressLogoTable($logoName, $type) {
    if( getCurrentSelectedLogo($logoName, $type) != $logoName ) {
        logoUnselect($type);
        if(selectLogo($logoName, $type)) {
            return true;
        }
    } else {
        //echo"GGG";
        return "NoChanges";
    }
}

function selectLogo($logoName, $type) {
    global $db;
    $update_query = 'UPDATE expressLogos SET '
        . 'selected = ?';
        $update_query .= " where filename = ? and type = ?";
        $params = array("true", $logoName, $type);
        $update_stmt = $db->prepare($update_query);
        if($update_stmt->execute($params)) {
            return true;
        } else {
            return false;
        }
}

function logoUnselect($type) {
    global $db;    
    $update_query = 'UPDATE expressLogos SET '
        . 'selected = ?';
        $update_query .= " where type = ? and selected = ?";
        $params = array("false", $type, "true");
        $update_stmt = $db->prepare($update_query);
        if($update_stmt->execute($params)) {
            return true;
        } else {
            return false;
        }
}

function getCurrentSelectedLogo($logoName, $type) {
    global $db;
    $querySel = "SELECT * from expressLogos where type = '" . $type . "' and selected = 'true'";
    $sthSel = $db->query($querySel);
    if( $row = $sthSel->fetch() ) {
        return $row['filename'];
    }
    return "";
}

function updateCustomerLogoOnLoginPage($POST) {
    global $db;
    $customerLogoOnLoginPage = isset($POST['customerLogoOnLoginPage']) ? "true" : "false";
    $updateQuery = $db->prepare('UPDATE brandingConfig SET customerLogoOnLoginPage = ?');
    if ($updateQuery->execute(array($customerLogoOnLoginPage))) {
        return true;
    }
}

function setColorSchema($colorSchema) {
    global $db;
    $update_colorSchema = $db->prepare('UPDATE brandingConfig SET colorSchema = ?');
    if( $update_colorSchema->execute(array($colorSchema)) ) {
        //echo "Color Schema is updated.";
    } else {
        //echo "Color Schema not updated.";
    }
}

function deleteLogoInfo($logoName, $type) {
    global $db;
    $queryDel = "DELETE FROM expressLogos WHERE filename ='" . $logoName . "' and type='" . $type . "'";
    if($db->query($queryDel)) {
        return true;
    } else {
        return false;
    }
}
?>