<?php 

    /*end server connection response */
    //require_once("/var/www/html/Express/header.php");
    //$expressHostName = gethostname();
    
    //error_reporting(E_ALL & E_WARNING & E_NOTICE);
    //error_reporting(1);
    //ini_set('display_errors', '1');

    require_once("/var/www/html/Express/config.php");
    //checkLogin();
    //require_once("/var/www/html/Express/adminCheckFilter.php");
    //redirectUser("sysAdmin");
    //$_SESSION["redirectUrl"] = "sysAdmin";
    
    
    require_once("/var/www/lib/broadsoft/adminPortal/ServerConfiguration.php");
    require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/PasswordRulesConfiguration.php");
    
    
    require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/UserIdNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/LineportNameBuilder.php");
    require_once("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
    require_once("/var/www/html/Express/adminUsers/DBAccessPrivileges.php");
    require_once("/var/www/html/Express/adminUsers/DBEnterpriseAccess.php");    
        
    const maxNumberOfSIPGateways = 30;
    const appsServer             = "bwAppServer";
    const surgeMailServer        = "surgeMail";
    const counterPathServer      = "counterPath";
    const primaryServer          = "primary";
    const secondaryServer        = "secondary";
    $clusterSupport              = (isset($license["bwClusters"]) && $license["bwClusters"] == "true") && $enableClusters == "true" ? true : false;

    
    
    require_once("/var/www/lib/broadsoft/adminPortal/util/ServerConnCheckUtil.php");
    $testConn =  new ServerConnCheckUtil();
    
    //TODO: Remove when development is done
    function resolveUserNameAttribute($attr) {
        switch ($attr) {
            case ResourceNameBuilder::EXT: return "3355";
            case ResourceNameBuilder::GRP_ID: return "274054";
            case ResourceNameBuilder::DEFAULT_DOMAIN: return "express.oconus.com";
            case ResourceNameBuilder::SYS_DFLT_DOMAIN: return "expresslab.averistar.com";
            case ResourceNameBuilder::DN: return "3019167056";
            case ResourceNameBuilder::USR_CLID: return "2408998224";
            case ResourceNameBuilder::GRP_DN: return "7032910200";
            case ResourceNameBuilder::GRP_PROXY_DOMAIN: return "ipt-2087435-b2b.usps.gov";
        }
    }

    //TODO: Remove when development is done
    function getResourceNameforSurgeMail($attr, $value) {
        global $surgemailDomain;

        switch ($attr) {
            case ResourceNameBuilder::DN:               return "3019167056";
            case ResourceNameBuilder::EXT:              return "3355";
            case ResourceNameBuilder::USR_CLID:         USR_CLID: return "2408998224";
            case ResourceNameBuilder::GRP_DN:           return "7036872000";
            case ResourceNameBuilder::GRP_ID:           return "274054";
            case ResourceNameBuilder::DEFAULT_DOMAIN:   return "express.oconus.com";
            case ResourceNameBuilder::SYS_DFLT_DOMAIN:  return "expresslab.averistar.com";
            case ResourceNameBuilder::SRG_DOMAIN:       return $surgemailDomain;
            case ResourceNameBuilder::FIRST_NAME:       return $value["firstName"];
            case ResourceNameBuilder::LAST_NAME:        return $value["lastName"];
            case ResourceNameBuilder::DEV_NAME:         return "Karl";
            case ResourceNameBuilder::MAC_ADDR:         return "Dalka";
            case ResourceNameBuilder::USR_ID:
                $name = "2024866555@expresslab.averistar.com";
                if ($name == "") { return ""; }
                $nameSplit = explode("@", $name);
                return $nameSplit[0];
            default: return "";
        }
    }

    function scallLogo($width, $height, $filename) {
        list($width_orig, $height_orig) = getimagesize($filename);
        if($height_orig > 130) {
            $scallFactor = $height_orig/$height;
            $newWidth = $width_orig / $scallFactor;
            $newHeight = $height;             
        } else {
            $newHeight = $height_orig;
            $newWidth = $width_orig;
        }
        return array('imgWidth' => $newWidth, 'imgHeight' => $newHeight);
    }

    function selectNumberSIPGatewayInstances($numInstances) {

        $str = "";
        $currNumInstances = (int)($numInstances);

        for ($i = 1; $i <= maxNumberOfSIPGateways; $i++) {
            $str .= "<option value=\"" . $i . "\"";
            $str .= $i == $currNumInstances ? " selected" : "";
            $str .= ">" . $i . "</option>";
        }

        return $str;

    }


    function showBillingConnectionStatus($connected, $errMsg) {
        $color = $connected ? "#72ac5d" : "#ac5f5d";
        $msg = $connected ? "CONNECTED" : "NOT CONNECTED: ";

        echo "<label style=\"font-size: 11px; font-weight: 600; color: " . $color . "\">(" . $msg . $errMsg . ")</label>";
    }


    function getAllBwReleases() {
        global $db;
        $releases = array();

        $query   = "select version from versionLookup";
        $results = $db->query($query);

        while ($row = $results->fetch()) {
            $releases[] = $row["version"];
        }

        return $releases;
    }


    function getAllServersLabels() {
        global $db;
        $labels = array();

        $query   = "select label from labelLookup";
        $results = $db->query($query);

        while ($row = $results->fetch()) {
            $labels[] = $row["label"];
        }

        return $labels;
    }


    function getBroadSoftRelease($revision) {
        global $db;
        $str = "";

        // initially revision number is not initialized
        $str = "<option value=\"\"";
        if ($revision == "") {
            $str .= " selected";
        }

        $str .= "></option>";

        $releases = getAllBwReleases();
        foreach ($releases as $index=>$release) {
            $str .= "<option value=\"" . $release . "\"";
            if ($release == $revision) {
                $str .= " selected";
            }

            $str .= ">Rel. " . $release . "</option>";
        }

//         echo $str;
        return $str;
    }


    function getBwLabel($selectedLabel) {
        global $db;
        $str = "";

        // initially label is not initialized
        $str = "<option value=\"\"";
        if ($selectedLabel == "") {
            $str .= " selected";
        }

        $str .= "></option>";

        $labels = getAllServersLabels();
        foreach ($labels as $index=>$label) {
            $str .= "<option value=\"" . $label . "\"";
            if ($label == $selectedLabel) {
                $str .= " selected";
            }

            $str .= ">" . $label . "</option>";
        }

        echo $str;
    }

    function getLdapUsers()
    {
            global $db;

            $query = "select CONCAT(firstName, ' ', lastName) as fullName, id, userName from users where disabled = 0 and use_ldap_authentication = 1";
            $stmt = $db->prepare($query);

            if ($stmt->execute(array())) {
                    $ldap_users = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    return $ldap_users;
            }

            return null;
    }

    
    
    function getDomainForSurgeMail($surgeMailDomain)
    {
         global $surgeMailIdCriteria1, $surgeMailIdCriteria2;

        if (isset($surgeMailIdCriteria1) && $surgeMailIdCriteria1 != "") {
            $criteria = $surgeMailIdCriteria1;
        }
        elseif (isset($surgeMailIdCriteria2) && $surgeMailIdCriteria2 != "") {
            $criteria = $surgeMailIdCriteria2;
        }
        else {
            $criteria = "";
        }

        $smDomain = explode("@", $criteria);

        if (! isset($smDomain[1]) || $smDomain[1] == "") {
            return "";
        }

        switch (strtolower($smDomain[1])) {
            case "<systemdefaultdomain>":   return $_SESSION["systemDefaultDomain"];
            case "<defaultdomain>":         return $_SESSION["defaultDomain"];
            case "<surgemaildomain>":       return $surgeMailDomain;
            default:                        return "";
        }
    }
    
    function getAllLogos() {
             global $db;
             $querySel = "SELECT * from expressLogos";
             $sthSel = $db->query($querySel);
             $allRowData = array();
             while( $row = $sthSel->fetch() ) {
                 $allRowData[$row['type']][] = $row;
             }
             return $allRowData;
        }
    
    
    //Get Servers Config Database Details
    //echo "IN IF - ".$_POST['adminConfigType'];
        $primarySurgeMailServerIP   = "172.31.0.110:7026";                                                                    //TODO: NOT CONNECTED TO DB YET
        $secondarySurgeMailServerIP = "172.31.0.111:7026";                                                                  //TODO: NOT CONNECTED TO DB YET
        $surgeMailServerChoice      = "Primary";                                                                                 //TODO: NOT CONNECTED TO DB YET
        $primarySurgeMailUserName   = "express_admin";                                                                        //TODO: NOT CONNECTED TO DB YET
        $secondarySurgeMailUserName = "express_admin";                                                                      //TODO: NOT CONNECTED TO DB YET
        $primarySurgeMailPassword   = "q1w2_Bwadmin!";                                                                        //TODO: NOT CONNECTED TO DB YET
        $secondarySurgeMailPassword = "q1w2_Bwadmin!";                                                                      //TODO: NOT CONNECTED TO DB YET

        

        //Get System COnfig Database Details
        // get System configuration
        // ------------------------
        $softwareRevision = file("/var/www/revision.txt", FILE_IGNORE_NEW_LINES); // read software revision from file

        $systemConfig = new DBSimpleReader($db, "systemConfig");
        $schemaRevision = $systemConfig->get("schemaRevision");
        $groupsDomainType = $systemConfig->get("groupsDomainType");
        $staticDomain = $systemConfig->get("staticDomain");
        $proxyDomainType = $systemConfig->get("proxyDomainType");
        $staticProxyDomain = $systemConfig->get("staticProxyDomain");
        $securityDomainPattern = $systemConfig->get("securityDomainPattern");
        $bwPortalPasswordType = $systemConfig->get("bwPortalPasswordType");
        $enableContinuePreviousSession = $systemConfig->get("enableContinuePreviousSession");
        //Decrypt Password if Salt is present
        if($staticBwPortalPasswordSalt = $systemConfig->get("staticBwPortalPasswordSalt")) {
                $staticBwPortalPassword = decryptString($systemConfig->get("staticBwPortalPassword"), "StaticBwPortalPassword_" . $staticBwPortalPasswordSalt);
        } else {
                $staticBwPortalPassword = $systemConfig->get("staticBwPortalPassword");
        }
        $bwPortalForcePasswordChange = $systemConfig->get("bwPortalForcePasswordChange");
        $viewCDR = $systemConfig->get("viewCDR");
        $useDepartments = $systemConfig->get("useDepartments");
        $bwAuthentication = $systemConfig->get("bwAuthentication");
        $surgeMailDebug = $systemConfig->get("surgeMailDebug");
        $surgeMailIdCriteria1 = $systemConfig->get("surgeMailIdCriteria1");
        $surgeMailIdCriteria2 = $systemConfig->get("surgeMailIdCriteria2");
        $surgeMailDomain = $systemConfig->get("surgeMailDomain");
        $surgeMailUserPasswordType = $systemConfig->get("surgeMailUserPasswordType");
        $surgeMailUserPassword = $systemConfig->get("surgeMailUserPassword");
        //Decrypt Password if Salt is present
        if($surgeMailUserPasswordSalt = $systemConfig->get("surgeMailUserPasswordSalt")) {
                $surgeMailUserPassword = decryptString($systemConfig->get("surgeMailUserPassword"), "SurgeMailPassword_" . $surgeMailUserPasswordSalt);
        } else {
                $surgeMailUserPassword = $systemConfig->get("surgeMailUserPassword");
        }
        $deleteVMOnUserDel = $systemConfig->get("deleteVMOnUserDel");
        $supportEmail = $systemConfig->get("supportEmail");
        $billingHost = $systemConfig->get("billingHost");
        $billingDB = $systemConfig->get("billingDB");
        $billingUser = $systemConfig->get("billingUser");
        //End code
    
    
    if(isset($_POST['adminConfigType']) && $_POST['adminConfigType'] == "servers_config")
    {
	
        // get Servers configuration
        // -------------------------
        unset($_SESSION['allClusters']);
        if($clusterSupport) {
            $serverConfigObj = new ServerConfiguration($db, appsServer, "");
            $getClusterInfo = $serverConfigObj->getClusterInfo();
            $_SESSION['allClusters'] = $getClusterInfo;
            //XSP Code
            $serverConfigObj = new ServerConfiguration($db, xspServer, "");
            $getXSPInfo = $serverConfigObj->getClusterInfo();
            //End
        } else {
            $serverConfig = new ServerConfiguration($db, appsServer, primaryServer);
            $primaryBwServerIP = $serverConfig->getIP();
            $primaryBwUserName = $serverConfig->getUserName();
            $primaryBwPassword = $serverConfig->getPassword();
            $primaryBwProtocol = $serverConfig->getProtocol();
            $primaryBwPort     = $serverConfig->getPort();
            $ociReleasePrimary = $serverConfig->getOCIRelease();
            if ($serverConfig->getActive() == "true") {
                $bwServerChoice = "Primary";
            }

            $serverConfig = new ServerConfiguration($db, appsServer, secondaryServer);
            $secondaryBwServerIP = $serverConfig->getIP();
            $secondaryBwUserName = $serverConfig->getUserName();
            $secondaryBwPassword = $serverConfig->getPassword();
            $secondaryBwProtocol = $serverConfig->getProtocol();
            $secondaryBwPort     = $serverConfig->getPort();
            $ociReleaseSecondary = $serverConfig->getOCIRelease();

            if ($serverConfig->getActive() == "true") {
                $bwServerChoice = "Secondary";
            }

            $ociRelease = $serverConfig->getOCIRelease();
        }

        //$bwLabel = $serverConfig->getLabel();
    
        //Code added by @03 April 2018
        // Get surge mail servers configuration details from serversconfig table
        // -------------------------
        $surgMailServerConfig            = new ServerConfiguration($db, surgeMailServer, primaryServer);

        $primarySurgeMailServerIP        = $surgMailServerConfig->getIP();
        $primarySurgeMailServerIPChkCnt  = $surgMailServerConfig->getIP().":".$surgMailServerConfig->getPort();
        $primarySurgeMailUserName        = $surgMailServerConfig->getUserName();
        $primarySurgeMailPassword        = $surgMailServerConfig->getPassword();    
        $primarySurgeMailBwProtocol      = $surgMailServerConfig->getProtocol();
        $primarySurgeMailBwPort          = $surgMailServerConfig->getPort();    
        //$primarySurgeMailLabel           = $surgMailServerConfig->getLabel();
        if ($surgMailServerConfig->getActive() == "true") {
            $surgeMailServerChoice = "Primary";
        }

        $surgMailServerConfig              = new ServerConfiguration($db, surgeMailServer, secondaryServer);
        $secondarySurgeMailServerIP        = $surgMailServerConfig->getIP();    
        $secondarySurgeMailServerIPChkCnt  = $surgMailServerConfig->getIP().":".$surgMailServerConfig->getPort();    
        $secondarySurgeMailUserName        = $surgMailServerConfig->getUserName();
        $secondarySurgeMailPassword        = $surgMailServerConfig->getPassword();    
        $secondarySurgeMailBwProtocol      = $surgMailServerConfig->getProtocol();
        $secondarySurgeMailBwPort          = $surgMailServerConfig->getPort();

        //$secondarySurgeMailLabel     = $surgMailServerConfig->getLabel();
        if ($surgMailServerConfig->getActive() == "true") {
            $surgeMailServerChoice = "Secondary";
        }
    
    
	$sgMlSrvrCnfgTstUser = new ServerConfiguration($db, surgeMailServer, primaryServer);
	if ($sgMlSrvrCnfgTstUser->getActive() != "true") {
	    $sgMlSrvrCnfgTstUser = new ServerConfiguration($db, surgeMailServer, secondaryServer);
	}
	$surgemailIpToTestUser      = $sgMlSrvrCnfgTstUser->getIP().":".$sgMlSrvrCnfgTstUser->getPort();
	$surgemailIp                = $sgMlSrvrCnfgTstUser->getIP();
	$surgMailConnectedUsrName   = $sgMlSrvrCnfgTstUser->getUserName();
	$surgMailConnectedPass      = $sgMlSrvrCnfgTstUser->getPassword();
	$httpPrtcl                  = $sgMlSrvrCnfgTstUser->getProtocol();

	
        //End code
        
        /////////
	/* counter Path server counter code @ 29/10/2018 */
	$cPServerConfig               = new ServerConfiguration($db, counterPathServer, primaryServer);
	$primaryCoutnerPathServerIP   = $cPServerConfig->getIP();
	$primaryCoutnerPathUserName   = $cPServerConfig->getUserName();
	$primaryCoutnerPathPassword   = $cPServerConfig->getPassword();
	$primaryCoutnerPathBwProtocol = $cPServerConfig->getProtocol();
	$primaryCoutnerPathBwPort     = $cPServerConfig->getPort();
	
	$primaryCPServerIPChkCnt  = $cPServerConfig->getIP().":".$cPServerConfig->getPort();
	
	if ($cPServerConfig->getActive() == "true") {
	    $coutnerPathServerChoiceRes = "Primary";
	}
	
	$seCPServerConfig               = new ServerConfiguration($db, counterPathServer, secondaryServer);
	$secondaryCoutnerPathServerIP   = $seCPServerConfig->getIP();
	$secondaryCoutnerPathUserName   = $seCPServerConfig->getUserName();
	$secondaryCoutnerPathPassword   = $seCPServerConfig->getPassword();
	$secondaryCoutnerPathBwProtocol = $seCPServerConfig->getProtocol();
	$secondaryCoutnerPathBwPort     = $seCPServerConfig->getPort();
	
	$secondaryCPServerIPChkCnt  = $seCPServerConfig->getIP().":".$seCPServerConfig->getPort();   
	
	if ($seCPServerConfig->getActive() == "true") {
	    $coutnerPathServerChoiceRes = "Secondary";
	}
        
        
        
	$tstUserName  = "testuser_tst_".rand();
	$emailAddress =  $tstUserName. "@" . getDomainForSurgeMail($surgeMailDomain);
        
	/*require_once("/var/www/lib/broadsoft/adminPortal/util/ServerConnCheckUtil.php");
	$testConn =  new ServerConnCheckUtil();
                
        $primaryServerResponse = $testConn->check_login_connection($primaryBwProtocol, $primaryBwServerIP, $primaryBwPort, $primaryBwUserName, $primaryBwPassword);
	$secondaryServerResponse = $testConn->check_login_connection($secondaryBwProtocol, $secondaryBwServerIP, $secondaryBwPort, $secondaryBwUserName, $secondaryBwPassword);
	*/
        
	$getServerconnConnectResponse = $testConn->createSurgeMailTestUser($surgemailIpToTestUser, $surgMailConnectedUsrName, $surgMailConnectedPass, $httpPrtcl, $emailAddress);
        $getServerconnDeleteResponse  = $testConn->deleteSurgeMailTestUser($surgemailIpToTestUser, $surgMailConnectedUsrName, $surgMailConnectedPass, $httpPrtcl, $emailAddress) ;
	
	$primarySurgeMailServerResponse   = $testConn->createSurgeMailTestUser($primarySurgeMailServerIPChkCnt, $primarySurgeMailUserName, $primarySurgeMailPassword, $primarySurgeMailBwProtocol, $emailAddress);
        $secondarySurgeMailServerResponse = $testConn->createSurgeMailTestUser($secondarySurgeMailServerIPChkCnt, $secondarySurgeMailUserName, $secondarySurgeMailPassword, $secondarySurgeMailBwProtocol, $emailAddress);
	
        
        $primaryCoutnerPathServerResponse = $testConn->checkCounterPathConnection($primaryCoutnerPathBwProtocol, $primaryCoutnerPathServerIP, $primaryCoutnerPathBwPort, $primaryCoutnerPathUserName, $primaryCoutnerPathPassword);
        $secondaryCoutnerPathServerResponse = $testConn->checkCounterPathConnection($secondaryCoutnerPathBwProtocol, $secondaryCoutnerPathServerIP, $secondaryCoutnerPathBwPort, $secondaryCoutnerPathUserName, $secondaryCoutnerPathPassword);
        
	
        /*end server connection response */
        
        
        ?> 
                <!--------------------------->
                <!-- Servers Configuration -->
                <!--------------------------->
                <div id="divServersConfig">
                    <div class="">
                    	<div   id="divServersConfig">
       
                <!-- form identifier -->
                <form action="#" method="post" id="serversConfig" name="serversConfig" class="fcorn-registerTwo sytemAdminForm">
                    <input type="hidden" id="tabId" value="servers_config" />
                <div class="row fcorn-registerTwo">
					<div class="col-md-12">
						<div class="form-group">
							<h2 class="adminUserText">BroadWorks Servers</h2>
						</div>
					</div>
				</div>
										
                <input type="hidden" name="serversConfigForm" value="true">
				<?php
				if($clusterSupport) {
					echo '<div id="allClusterDiv">';
					$i = 1;
                                        
					foreach($getClusterInfo as $clustName => $clustVal) {
					    $secondaryServerResponse = "";
					    $primaryServerResponse = "";
					    if( $clustVal['primary']['ip'] != "") {
					        $primaryServerResponse = $testConn->getSoftwaReversionAndConnection($clustVal['primary']['protocol'], $clustVal['primary']['ip'], $clustVal['primary']['port'], $clustVal['primary']['userName'], $clustVal['primary']['password']);
					    }
					    if( $clustVal['secondary']['ip'] != "") {
					        $secondaryServerResponse = $testConn->getSoftwaReversionAndConnection($clustVal['secondary']['protocol'], $clustVal['secondary']['ip'], $clustVal['secondary']['port'], $clustVal['secondary']['userName'], $clustVal['secondary']['password']);
					    }
						
						require("/var/www/html/Express/sysAdmin/template/serverConfigClusterTemplate.php");
						$i++;
					}
					echo "</div>";
					?>
					<div class="col-md-12">
						<div class="form-group" style="float:left">
							<input <?php echo count($getClusterInfo) == 4 ? "disabled" : ""?> type="button" class="subButton" name="" id="addCluster" value="Add Cluster">
						</div>
					</div>
				<?php	
				} else {
					require("/var/www/html/Express/sysAdmin/template/serverConfigTemplate.php");
				}
				
				?>
               
                       
               <!-- newui work start -->        
                        <!--start dev counterPath  -->
                        <?php if(isset($license["softPhone"]) && $license["softPhone"] == "true"){ ?>
                        <div style="display: block;" id="hideCounterPath">
                        
										<div class="row fcorn-registerTwo">
											<div class="col-md-12">
												<div class="form-group">
													<h2 class="adminUserText">Counterpath Servers</h2>
													 
												</div>
											</div>
										</div>
                		
								<div class="fcorn-registerTwo">
									<div style="margin-left:0px">
    									<div class="col-md-4">
    										<div class="">
    											<label class="labelText labelTextZero">Primary Server</label><br/>
    										</div>					
    									</div>
	 
                						<div class="col-md-6">
                							<div class="form-group">
                            					<input type="radio" name="coutnerPathServerChoice" id="coutnerPathServerChoicePrimary" value="Primary" <?php echo $coutnerPathServerChoiceRes == "Primary" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServersConfig')">
                            					<label for="coutnerPathServerChoicePrimary"><span></span></label>
                            					<label class="labelText customLabelText">Connect to Primary Server</label>
                                    				<?php 
                                                    if($primaryCoutnerPathServerResponse == "true"){
                                                        echo "<label style='font-size: 11px;color: #72ac5d;font-weight: 600;'>( CONNECTED )</label>";
                                                    }
                                                    else{
                                                        echo "<label style='font-size: 11px;color: #ac5f5d;font-weight: 600;'>( NOT CONNECTED ) </label>";
                                                    }
                                                   ?>
                            				</div>
                						</div>
                						<div class="col-md-2" style="width:100%">            				 
										</div>                   
									</div>  
								</div>
                        
                        <!--ip setting -->
										<div class="col-md-2"> 
											<label class="labelText" for=" ">Ip Address:</label><br/>
											<div class="form-group">
												<input type="text" name="primaryCoutnerPathServerIP" class="clusterCounterpath" id="primaryCoutnerPathServerIP" size="17" <?php echo "value=\"" . $primaryCoutnerPathServerIP . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')"> 
												 
											</div>
										</div>
                        
										 <div class="col-md-2 marginLeft"> 
											<label class="labelText" for=" ">Username:</label><br/>
											<div class="form-group">
												<input type="text" name="primaryCoutnerPathUserName" id="primaryCoutnerPathUserName" class="widthAlign clusterCounterpath" size="17" <?php echo "value=\"" . $primaryCoutnerPathUserName . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')"> 
											</div>
										</div>
                        
                        
										 <div class="col-md-2 marginLeft"> 
											<label class="labelText" for=" ">Password:</label><br/>
											<div class="form-group">
												<input type="password" name="primaryCoutnerPathPassword" id="primaryCoutnerPathPassword" size="17"
	                                   placeholder="<?php echo $primaryCoutnerPathPassword ? "******" : ""; ?>"
	                                   onchange="updateChangeStatus(this,'toggleServersConfig')"
	                                   oninput="updateChangeStatus(this,'toggleServersConfig')" class="clusterCounterpath widthAlign">
											</div>
										</div>
                        
                        
									 <div class="col-md-2 marginLeft"> 
										<label class="labelText" for=" ">Protocol:</label><br/>
										<div class="form-group clusterCounterpath" >
											<div class="dropdown-wrap">
												<select name="primaryCoutnerPathBwProtocol" id="primaryCoutnerPathBwProtocol" onchange="updateCounterPathPort(this.value, 'primaryCoutnerPathBwPort')" oninput="updateChangeStatus(this,'toggleServersConfig')" class="widthAlign clusterCounterpath">
                                    		<option value="https" <?php if($primaryCoutnerPathBwProtocol=="https") {  echo "selected='selected'"; } ?>>HTTPS</option>
                                    		<option value="http" <?php if($primaryCoutnerPathBwProtocol=="http") {  echo "selected='selected'"; } ?>>HTTP</option>
                                		</select>
											</div>
										</div>
									</div>
                        
                        
									<div class="col-md-2 marginLeft"> 
										<label class="labelText" for=" ">Port:</label><br/>
										<div class="form-group">
											<input type="text"  name="primaryCoutnerPathBwPort" id="primaryCoutnerPathBwPort" class="clusterCounterpath" size="17" <?php echo "value=\"" . $primaryCoutnerPathBwPort . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')">
										</div>
									</div>
                        
                        <!--end ip setting -->
                        
                  
    <!-- known secondary server name -->
      
							<div class="fcorn-registerTwo">
								<div class="" style="margin-left:0px">
									<div class="col-md-4">
										<div class="">
											<label class="labelText labelTextZero">Secondary Server:</label> <br/>
										</div>					
									</div>
				 
    								<div class="col-md-6">
    									<div class="form-group">
                        					<input type="radio" name="coutnerPathServerChoice" id="coutnerPathServerChoiceSecondary" value="Secondary" <?php echo $coutnerPathServerChoiceRes == "Secondary" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServersConfig')">
                        					<label for="coutnerPathServerChoiceSecondary"><span></span></label>
                        					<label class="labelText customLabelText">Connect to Secondary Server</label>
                                           <?php 
                                           if($secondaryCoutnerPathServerResponse == "true"){
                                               echo "<label style='font-size: 11px;color: #72ac5d;font-weight: 600;'>( CONNECTED )</label>";
                                           }
                                           else{
                                               echo "<label style='font-size: 11px;color: #ac5f5d;font-weight: 600;'>( NOT CONNECTED ) </label>";
                                           }
                                           ?>                         		  
    						   
                    					</div>
                                    </div>
                                    <div class="col-md-2" style="width:100%">            				 
									</div>
								</div>  
							</div>
    
    <!--end known server name -->
               <!--secondary ip address details -->
                <!--ip setting -->
							<div class="col-md-2"> 
								<label class="labelText" for=" ">Ip Address:</label><br/>
								<div class="form-group">
									<input type="text" name="secondaryCoutnerPathServerIP" id="secondaryCoutnerPathServerIP" size="17" <?php echo "value=\"" . $secondaryCoutnerPathServerIP . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')" class="widthAlign clusterCounterpath"> 
								</div>
							</div>
                        
							 <div class="col-md-2 marginLeft"> 
								<label class="labelText" for=" ">Username:</label><br/>
								<div class="form-group">
												<input type="text" name="secondaryCoutnerPathUserName" id="secondaryCoutnerPathUserName" size="17" <?php echo "value=\"" . $secondaryCoutnerPathUserName . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')" class="widthAlign clusterCounterpath">
								</div>
							</div>
                        
                        
							 <div class="col-md-2 marginLeft"> 
								<label class="labelText" for=" ">Password:</label><br/>
								<div class="form-group">
									 <input type="password" name="secondaryCoutnerPathPassword" id="secondaryCoutnerPathPassword" size="17"
	                                   placeholder="<?php echo $secondaryCoutnerPathPassword ? "******" : ""; ?>"
	                                   onchange="updateChangeStatus(this,'toggleServersConfig')"
	                                   oninput="updateChangeStatus(this,'toggleServersConfig')" class="clusterCounterpath">
								</div>
							</div>
                        
                        
							 <div class="col-md-2 marginLeft"> 
								<label class="labelText" for=" ">Protocol:</label><br/>
								<div class="form-group clusterCounterpath">
									<div class="dropdown-wrap">
        								<select name="secondaryCoutnerPathBwProtocol" id="secondaryCoutnerPathBwProtocol" class="clusterCounterpath" onchange="updateCounterPathPort(this.value, 'secondaryCoutnerPathBwPort')" oninput="updateChangeStatus(this,'toggleServersConfig')">
                                            <option value="https" <?php if($secondaryCoutnerPathBwProtocol=="https") {  echo "selected='selected'"; } ?>>HTTPS</option>
                                            <option value="http" <?php if($secondaryCoutnerPathBwProtocol=="http") {  echo "selected='selected'"; } ?>>HTTP</option>
                                        </select>
    								</div>
								</div>
							</div>
                        
                         <div class="col-md-2 marginLeft"> 
                            <label class="labelText" for=" ">Port:</label><br/>
                			<div class="form-group">
                				<input type="text" name="secondaryCoutnerPathBwPort" id="secondaryCoutnerPathBwPort" class="clusterCounterpath" size="17" <?php echo "value=\"" . $secondaryCoutnerPathBwPort . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')">
            				</div>
                		</div>
                             
                		</div>
      		 
 						<?php } ?>
                     <!-- end dev counterPath -->
                        
                        
                        <!----- SurgeMail Servers ----->
                        <div style="display: block;" id="hideSurgeMail">

						 <!--cluster data -->
 <div>	
								<!----- BroadWorks Servers ----->	
										<div class="row fcorn-registerTwo">
											<div class="col-md-12">
												<div class="form-group">
													<h2 class="adminUserText">SurgeMail Servers</h2>
													 
												</div>
											</div>
										</div>
                		
                        <!--server details -->
                        
										<div class="fcorn-registerTwo">
											<div style="margin-left:0px">
													<div class="col-md-4">
														<div class="">
															<label class="labelText labelTextZero">Primary Server</label><br/>
														</div>					
													</div>
				 
            		
													<div class="col-md-6">
														<div class="form-group">
															<input type="radio" name="smServerChoice" id="smServerChoicePrimary" value="Primary" <?php echo $surgeMailServerChoice == "Primary" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServersConfig')">
															<label for="bwServerChoicePrimary"><span></span></label>
															<label class="labelText customLabelText">Connect to 	Primary Server</label>
																<?php 
																	if($primarySurgeMailServerResponse == "true")
																		echo "<label style='font-size: 11px;color: #72ac5d;font-weight: 600;'>( CONNECTED )</label>";
																	else
																		echo "<label style='font-size: 11px;color: #ac5f5d;font-weight: 600;'>( NOT CONNECTED ) </label>";
																?>	
														</div>
													</div>
                    								<div class="col-md-2" style="width:100%">            				 
													</div>
											</div>  
										</div>
                        
                        
                        <!--end server details --->
                        
                        <!--ip setting -->
										<div class="col-md-2"> 
											<label class="labelText" for=" ">Ip Address:</label><br/>
											<div class="form-group">
												
												<input class="clusterSurgemail" type="text" name="primarySurgeMailServerIP" id="primarySurgeMailServerIP" size="17" <?php echo "value=\"" . $primarySurgeMailServerIP . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')">
											</div>
										</div>
                        
										 <div class="col-md-2 marginLeft"> 
											<label class="labelText" for=" ">Username:</label><br/>
											<div class="form-group">
															<input class="clusterSurgemail" type="text" name="primarySurgeMailUserName" id="primarySurgeMailUserName" size="17" <?php echo "value=\"" . $primarySurgeMailUserName . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')">
											</div>
										</div>
                        
                        
										 <div class="col-md-2 marginLeft"> 
											<label class="labelText" for=" ">Password:</label><br/>
											<div class="form-group">
												
											<input class="clusterSurgemail" type="password" name="primarySurgeMailPassword" id="primarySurgeMailPassword" size="17"
	                                   placeholder="<?php echo $primarySurgeMailPassword ? "******" : ""; ?>"
	                                   onchange="updateChangeStatus(this,'toggleServersConfig')"
	                                   oninput="updateChangeStatus(this,'toggleServersConfig')"> 
											</div>
										</div>
                        
                        
									 <div class="col-md-2 marginLeft"> 
										<label class="labelText" for=" ">Protocol:</label><br/>
										<div class="form-group clusterSurgemail" >
											<div class="dropdown-wrap">
												<select class="clusterSurgemail" name="primarySurgeMailBwProtocol" id="primarySurgeMailBwProtocol" onchange="updateSurgeMailPort(this.value, 'primarySurgeMailBwPort')" oninput="updateChangeStatus(this,'toggleServersConfig')">
												<option value="https" <?php if($primarySurgeMailBwProtocol=="https") {  echo "selected='selected'"; } ?>>HTTPS</option>
												<option value="http" <?php if($primarySurgeMailBwProtocol=="http") {  echo "selected='selected'"; } ?>>HTTP</option>
											</select>
											</div>
										</div>
									</div>
                        
                        
									<div class="col-md-2 marginLeft"> 
										<label class="labelText" for=" ">Port:</label><br/>
										<div class="form-group">
                				
											<input class="clusterSurgemail" type="text"name="primarySurgeMailBwPort" id="primarySurgeMailBwPort" size="17" <?php echo "value=\"" . $primarySurgeMailBwPort . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')">
										</div>
									</div>
                        
                        <!--end ip setting -->
                        
                  
    <!-- known secondary server name -->
      
									<div class="fcorn-registerTwo">
										<div class="" style="margin-left:0px">
											<div class="col-md-4">
												<div class="">
													<label class="labelText labelTextZero">Secondary Server:</label> <br/>
												</div>					
											</div>
				 
            		
											<div class="col-md-6">
												<div class="form-group">
													<input type="radio" name="smServerChoice" id="smServerChoiceSecondary" value="Secondary" <?php echo $surgeMailServerChoice == "Secondary" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServersConfig')">
													<label for="bwServerChoiceSecondary"><span></span></label>
													<label class="labelText customLabelText">Connect to Secondary Server</label>
                                                    <?php 
														if($secondarySurgeMailServerResponse == "true")
														echo "<label style='font-size: 11px;color: #72ac5d;font-weight: 600;'>( CONNECTED )</label>";
														else
														echo "<label style='font-size: 11px;color: #ac5f5d;font-weight: 600;'>( NOT CONNECTED ) </label>";
													   ?>
                                                </div>
                                           </div>
                                           <div class="col-md-2" style="width:100%">            				 
											</div>
									</div>  
								</div>
    
    <!--end known server name -->
               <!--secondary ip address details -->
                <!--ip setting -->
							<div class="col-md-2"> 
								<label class="labelText" for=" ">Ip Address:</label><br/>
								<div class="form-group">
									<input class="clusterSurgemail" type="text" name="secondarySurgeMailServerIP" id="secondarySurgeMailServerIP" size="17" <?php echo "value=\"" . $secondarySurgeMailServerIP . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')">
								</div>
							</div>
                        
							 <div class="col-md-2 marginLeft"> 
								<label class="labelText" for=" ">Username:</label><br/>
								<div class="form-group">
										<input class="clusterSurgemail" type="text" name="secondarySurgeMailUserName" id="secondarySurgeMailUserName" size="17" <?php echo "value=\"" . $secondarySurgeMailUserName . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')">
								</div>
							</div>
                        
							 <div class="col-md-2 marginLeft"> 
								<label class="labelText" for=" ">Password:</label><br/>
								<div class="form-group">
									 <input class="clusterSurgemail" type="password" name="secondarySurgeMailPassword" id="secondarySurgeMailPassword" size="17"
	                                   placeholder="<?php echo $secondarySurgeMailPassword ? "******" : ""; ?>"
	                                   onchange="updateChangeStatus(this,'toggleServersConfig')"
	                                   oninput="updateChangeStatus(this,'toggleServersConfig')">
								</div>
							</div>
                        
							 <div class="col-md-2 marginLeft"> 
								<label class="labelText" for=" ">Protocol:</label><br/>
								<div class="form-group clusterSurgemail">
									<div class="dropdown-wrap">
										<select class="clusterSurgemail" name="secondarySurgeMailBwProtocol" id="secondarySurgeMailBwProtocol" onchange="updateSurgeMailPort(this.value, 'secondarySurgeMailBwPort')" oninput="updateChangeStatus(this,'toggleServersConfig')">
                                            <option value="https" <?php if($secondarySurgeMailBwProtocol=="https") {  echo "selected='selected'"; } ?>>HTTPS</option>
                                            <option value="http" <?php if($secondarySurgeMailBwProtocol=="http") {  echo "selected='selected'"; } ?>>HTTP</option>
                                        </select>
								</div>
												</div>
							</div>
                        
                         <div class="col-md-2 marginLeft"> 
                            <label class="labelText" for=" ">Port:</label><br/>
                			<div class="form-group">
                				<input type="text" class="clusterSurgemail" name="secondarySurgeMailBwPort" id="secondarySurgeMailBwPort" size="17" <?php echo "value=\"" . $secondarySurgeMailBwPort . "\""; ?> onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')">
            				</div>
                		</div>
                    </div>	
                    	   	
                    <div class="row fcorn-registerTwo">
                	<!-- 
                        		<div class="col-md-6">
                        			<div class="form-group">
                        				<label class="labelText" for=" ">SurgeMail Servers:</label>
                    					<div class="dropdown-wrap oneColWidth">
                    						
                                       <select name="smLabel" id="bwLabel" onchange="updateChangeStatus(this,'toggleServersConfig')" oninput="updateChangeStatus(this,'toggleServersConfig')"><?php //getBwLabel($secondarySurgeMailLabel); ?></select>
                    					</div>
                        			
                        			</div>
                				</div>
                     -->  		
                        		<div class="col-md-6">
                    			<div class="form-group">
                    				
                    					<label class="labelText" for=" ">SurgeMail Test User:</label>
                						<input name="smSurgeMailUser" id="smSurgeMailUser" size="25" type="text">
				 
                    			</div>
                                        </div>
                                        <!-- Code Added -->
                                        <?php 
                                            if($getServerconnConnectResponse == "true"){
                                              echo   "<div class='col-md-12' style='color: #72ac5d; font-weight: bold; float: left;'>( $tstUserName created successfully with the connected server )</div>";
                                            }else{
                                              echo   "<div class='col-md-12' style='color: #ac5f5d; font-weight: bold; float: left;'>(unable to create user - $tstUserName)</div>";
                                            }
                                            ?>

                                            <?php if($getServerconnDeleteResponse == "true"){
                                                echo "<div class='col-md-12' style='color: #72ac5d; font-weight: bold;  float: left;'>( $tstUserName deleted successfully with the connected server )</div>";
                                            }else{
                                               echo  "<div class='col-md-12' style='color: #ac5f5d; font-weight: bold; float: left;'>(unable to delete user - $tstUserName)</div>";
                                            }
                                            ?>
                                        <!-- End code   -->
                                 
                		 
			    	 	
                    	</div>
                   
                   </div>      
           
               <!-- end SurgeMail Servers -->
               </form>
             	<div class="row">
             		<div class="form-group alignBtn">
             			<input type="button" class="subButton" name="submitServersConfig" id="submitServersConfig" value="Submit" onclick="submitConfig(this, 'serversConfig', 'toggleServersConfig')">
             		</div>
             	</div>
  
            </div>

                    </div>

                </div>
	
        <?php
        $getXSPInfo = array();
    }
    if(isset($_POST['adminConfigType']) && $_POST['adminConfigType'] == "system_config")
    { 
        //Decrypt Password if Salt is present
        //echo "<br />Yes Hello New ";
        //die();
        
        if($billingPasswordSalt = $systemConfig->get("billingPasswordSalt")) {
                $billingPassword = decryptString($systemConfig->get("billingPassword"), "BillingPassword_" . $billingPasswordSalt);
        } else {
                $billingPassword = $systemConfig->get("billingPassword");
        }
        $billingForceSSL = $systemConfig->get("billingForceSSL");
        $billingCACert = $systemConfig->get("billingCACert");
        $userLimitGroup = $systemConfig->get("userLimitGroup");
        $smsAuthentication = $systemConfig->get("smsAuthentication");
        $ldapAuthentication = $systemConfig->get("ldapAuthentication");
        $ldapHost = $systemConfig->get("ldapHost");
        $smsToken = $systemConfig->get("smsToken");
        $smsSID = $systemConfig->get("smsSID");
        $smsFrom = $systemConfig->get("smsFrom");
        $inactivityTimeout = $systemConfig->get("inactivityTimeout");
        $advancedUserFilters = $systemConfig->get("advancedUserFilters");
        $isAdminPasswordFieldVisible = $systemConfig->get("isAdminPasswordFieldVisible");
        $maxFailedLoginAttempts = $systemConfig->get("maxFailedLoginAttempts");
        $ldapDomain = $systemConfig->get("ldapDomain");
        $ldapCapitalization = $systemConfig->get("ldapCapitalization");
        /*password expiration super user */
        $passwordExpiration_SU = $systemConfig->get("passwordExpiration_superUser");
        $passwordExpiration_RU = $systemConfig->get("passwordExpiration_regularUser");
        /*end password expiration  */	

        $passwordReusePrevention = $systemConfig->get("passwordReusePrevention"); //Code added @ 13 July 2018

	/*password expiration super user */
	$passwordExpiration_SU = $systemConfig->get("passwordExpiration_superUser");
	$passwordExpiration_RU = $systemConfig->get("passwordExpiration_regularUser");
	/*end password expiration  */	
	
	$ldap_users = null;
	if ($ldapAuthentication == "true") {
		$ldap_users = getLdapUsers();
	}

	/*email report configuration user */
	$newUsrAdminReport = $systemConfig->get("newUsrAdminReport");
	$newUsrSupervisorReport = $systemConfig->get("newUsrSupervisorReport");
	$newUsrSupervisorEmail = $systemConfig->get("newUsrSupervisorEmail");
	
	//// Get Counter Path Stretto Group name
	$counterpathGroupName = $systemConfig->get("counterpathGroupName");
	
	/*xsp private ip */
	/*$xspPrimaryServerIP = $systemConfig->get("xspPrimaryServerIP");
	$xspSecondaryServerIP = $systemConfig->get("xspSecondaryServerIP");
	$xspActive = $systemConfig->get("xspActive");
	
	if($xspActive == 'primary'){
	    $xspServerChoice = 'Primary';
	}
	
	if($xspActive == 'secondary'){
	    $xspServerChoice = 'Secondary';
	}*/
        
        
        
        $warningsThreshold = $systemConfig->get("warningsThreshold");
        $alertsThreshold = $systemConfig->get("alertsThreshold");
        
        $hasBillingConfig = $billingHost != "" && $billingDB != "" && $billingUser != "" && $billingPassword != "";
        $billingDbConnected = false;
        $error_message = "";
        //ini_set("default_socket_timeout", 7);
        ini_set('mysql.connect_timeout',10);
        if ($hasBillingConfig) {
            try {

                    $options = array(
                            PDO::ATTR_TIMEOUT => 10,
                            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                    );

                    if($billingForceSSL == "true" && $billingCACert) {
                            $options[PDO::MYSQL_ATTR_SSL_CA] = $billingCACert;
                            $options[PDO::MYSQL_ATTR_SSL_CIPHER] = 'AES128-SHA';
                    }

                $billDB = new PDO("mysql:host=" . $billingHost . ";dbname=" . $billingDB . ";charset=utf8;",
                    $billingUser,
                    $billingPassword,
                        $options
                );
            }
            catch (Exception $e) {
                $error_message = $e->getMessage();
            }
         
            $billingDbConnected = ! is_null($billDB);            
            $billDB = null;
        }
        ?>
            <!-------------------------->
            <!-- System Configuration -->
            <!-------------------------->
           
              <div id="divSystemConfig">
                    <div class="">
                    	<div id="divSystemConfig">
            
            	<form action="#" method="post" id="systemConfig" name="systemConfig" class="fcorn-registerTwo sytemAdminForm" enctype="multipart/form-data">
		    <input type="hidden" id="tabId" value="system_config" />
                    <!-- form identifier -->
                    <input type="hidden" name="systemConfigForm" value="true">
	 
            	 	 <div class="row fcorn-registerTwo">
            	 	 
                				<div class="col-md-12">
                        			<div class="form-group">
                        				<h2 class="adminUserText">Domains</h2>
                        				 
                        			</div>
            					</div>
            					<!-- Group Domains -->
            					 
                				<div class="col-md-6">
                					<label class="labelText" for=" ">Group Domain:</label><br/>
                    				<div class="form-group">
                    			 		<input type="radio" name="groupsDomainType" id="groupsDomainTypeDefault" value="Default" <?php echo $groupsDomainType == "Default" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                        				<label for="groupsDomainTypeDefault"><span></span></label>
                    					<label class="labelText customLabelText">Groups use Default Enterprise domain</label><br/>
                    					<input type="radio" name="groupsDomainType" id="groupsDomainTypeStatic" value="Static" <?php echo $groupsDomainType == "Static" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                    					<label for="groupsDomainTypeStatic"><span></span></label>
                    					<label class="labelText customLabelText">Groups use alternate (static) Enterprise domain</label><br/>
                    					
                    				</div>
                    				
                    				<div class="form-group">
                    					<div id="staticDomainDiv">
                    				 		<label class="labelText" for=" ">Alternate/Static Domain:</label>
                        					<input type="text" name="staticDomain" id="staticDomain" size="45" <?php echo "value=\"" . $staticDomain . "\""; ?> onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')">
                        				</div>
                        			</div>
                				</div>

                        <!-- Group Line/Port Domain -->
                			<!--Use Default Domain-->
                				<div class="col-md-6">
                    				<div class="form-group">
                    					 <label class="labelText" for="">Group Line/Port Domain:</label></br>
                    					 <input type="radio" name="proxyDomainType" id="proxyDomainTypeDefault" value="Default" <?php echo $proxyDomainType == "Default" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                    				
                    					 <label for="proxyDomainTypeDefault"><span></span></label>
                    					 <label class="labelText customLabelText">Groups use Default Group domain</label><br/>
                    			<!--Use Static Domain-->
                				<!--Use Group-Specific Persisted Domain-->
                                <!-- TODO: This component is hidden for now, but it has to be removed (with other associated code), as well as ENUM choice in database: 'GroupSpecific' that this radio button is setting-->		  
                    					 <input type="radio" name="proxyDomainType" id="proxyDomainTypeStatic" value="Static" <?php echo $proxyDomainType == "Static" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                    					<label for="proxyDomainTypeStatic"><span></span></label> 
                    					<label class="labelText customLabelText">Groups use alternate (static) Group domain</label><br/>
                    					
                    				</div>
                    				<div class="form-group">
                        				<div id="staticProxyDomainDiv">
                    				 		<label class="labelText" for=" ">Alternate Line/Port Domain:</label>
                        					<input type="text" name="staticProxyDomain" id="staticProxyDomain" size="45" <?php echo "value=\"" . $staticProxyDomain . "\""; ?> onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')">
                            			</div>
                            		</div>
                				</div>
                	 			 
                				<div style="display:none">
                    				 <div class="col-md-6">
                        				<div class="form-group">
                        					 
                        					
                        					<input type="radio" name="proxyDomainType" id="proxyDomainTypeGroupSpecific" value="GroupSpecific" <?php echo $proxyDomainType == "GroupSpecific" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                        				
                        					<label for="proxyDomainTypeStatic"><span></span></label>
                        					 <label class="labelText customLabelText">Groups use group-specific persisted domains</label>
                        					 
                        				</div>
                    				</div>
                				</div>
		
                        <!--Security Domain Pattern-->
                				<div class="col-md-6">
                    				<div class="form-group">
                    					  <label class="labelText" for=" ">Security Domain Pattern:</label>
                    					  <input type="text" name="securityDomainPattern" id="securityDomainPattern" size="45" <?php echo "value=\"" . $securityDomainPattern . "\""; ?> onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')">
                    				
                    					<input type="hidden" name="hidSecurityDomainPattern" id="hidSecurityDomainPattern" <?php echo "value=\"" . $securityDomainPattern . "\""; ?>>
										<input type="hidden" name="confirmSecurityDomainSubmit" id="confirmSecurityDomainSubmit" value="0" />
                    				</div>
                				</div>
                				<!----- Permissions and Authentications ----->
                				<div class="col-md-12">
                        			<div class="form-group">
                        				<h2 class="adminUserText">Permissions and Authentications</h2>
                        				 
                        			</div>
            					</div>
            					<!-- end SSH Configuration -->
            	 			
            	 			                	<!--BroadWorks Authentication-->			
                			 <div class="col-md-12">
                					<div class="form-group">
                						<label class="labelText" for=" ">Express Login:</label><br/>
                						<input type="checkbox" name="enableContinuePreviousSession" id="enableContinuePreviousSession" value="true" <?php echo $enableContinuePreviousSession == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                						
                						<label for="enableContinuePreviousSession"  style="margin-left: 6px;"><span></span></label>
    									<label class="labelText" for="enableContinuePreviousSession" style="margin-left: 8px;">Enable Continue Previous Session</label>
                					</div>
                				</div>
                	 			
                	 			
                	 			  <!-- BW Portal Login -->

                       			 <!--Users require dynamic BW portal password-->
                				  
                				<div class="col-md-6">
                				<label class="labelText" for=" ">BW Portal Login:</label>
                    				<div class="form-group">
                    					 
                    					
                    					 <input type="radio" name="bwPortalPasswordType" id="bwPortalPasswordTypeDynamic" value="Dynamic" <?php echo $bwPortalPasswordType == "Dynamic" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                    				
                    					<label for="bwPortalPasswordTypeDynamic"><span></span></label>
                    					 <label class="labelText customLabelText">Users require dynamic password to access BW portal</label><br/>
                    					 
                    					 <input type="radio" name="bwPortalPasswordType" id="bwPortalPasswordTypeStatic" value="Static" <?php echo $bwPortalPasswordType == "Static" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                    		 <!--Static BW Portal Password-->
                						
                    					<label for="bwPortalPasswordTypeStatic"><span></span></label>
                    					 <label class="labelText customLabelText">Users require static password to access BW portal</label><br/>
                    					 <div id="staticBwPortalPasswordDiv">
                    					 	<label class="labelText" for=" ">Static BW Portal Login:</label><br/>
                    						
								 <input type="text" name="staticBwPortalPassword" id="staticBwPortalPassword" size="45" placeholder="<?php echo $staticBwPortalPassword ? "******" : ""; ?>" onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')"><br/>
                    					</div>	<br/>
                    					 <input type="checkbox" name="bwPortalForcePasswordChange" id="bwPortalForcePasswordChange" value="true" <?php echo $bwPortalForcePasswordChange == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                    					 <label for="bwPortalForcePasswordChange" style="margin-left:6px"><span></span></label>
    									<label class="labelText" for="bwPortalForcePasswordChange" style="margin-left:8px">Enforce users to change password on the first login</label>
                    					
                    					 
                    				</div>
                				</div>
            	 			
            	 				 
                				
                				<div class="col-md-6">
                					<div class="form-group">
                						<label class="labelText" for=" ">View CDRs:</label><br/>
                						<input type="checkbox" name="viewCDR" id="viewCDR" value="true" <?php echo $viewCDR == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                						
                						<label for="viewCDR"><span></span></label>
										<label class="labelText" for="viewCDR">Enable viewing Call Detail Records database</label>
                					</div>
                				</div>
                			
                			 <!--Use Departments-->
                			 
                			 
                			 <div class="col-md-6">
                					<div class="form-group">
                						<label class="labelText" for=" ">Use Departments:</label><br/>
                						<input type="checkbox" name="useDepartments" id="useDepartments" value="true" <?php echo $useDepartments == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                						
                						<label for="useDepartments"><span></span></label>
										<label class="labelText" for="useDepartments">Enable filtering by groups' Departments</label>
                					</div>
							</div>
                
                			<div class="col-md-6">
                					<div class="form-group"></div>
                						 
							</div>
                			
                				
                	<!--BroadWorks Authentication-->			
                			 <div class="col-md-6">
                					<div class="form-group">
                						<label class="labelText" for=" ">BroadWorks Authentication:</label><br/>
                						<input type="checkbox" name="bwAuthentication" id="bwAuthentication" value="true" <?php echo $bwAuthentication == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                						
                						<label for="bwAuthentication"  style="margin-left: 6px;"><span></span></label>
    			<label class="labelText" for="bwAuthentication" style="margin-left: 8px;">Use BroadWorks accounts to authenticate Express users</label>
                					</div>
                				</div>


 <!----- New code ----->

                          <!----- Authentication ----->
                          
                				<div class="col-md-12">
                        			<div class="form-group">
                        				<h2 class="adminUserText">Authentication</h2>
                        				 
                        			</div>
            					</div>
								 
            					<div class="col-md-12 fcorn-registerTwo">
									<div class="form-group alignBtn">
         								<button type="button" id="createNewAdminBtn" class="cancelButton">Create Admin User</button>
									</div>
								</div>         
                          
                				<div class="col-md-6">
                					<div class="form-group">
                					<!--SMS Authentication Type-->
                						<label class="labelText" for=" ">SMS Authentication:</label><br/>
                						<input type="radio" name="smsAuthentication" id="smsAuthentication"  value="true" <?php echo $smsAuthentication == "true" ? "checked" : ""; ?>
        	                               onclick="updateChangeStatus(this,'toggleSystemConfig')">
                						
                						<label for="smsAuthentication"><span></span></label>
    									<label class="labelText customLabelText" for="smsAuthentication">Use SMS authentication</label><br/>
    									
    									<input type="radio" name="smsAuthentication"
	                               id="smsNoAuthentication"
	                               value="false" <?php echo $smsAuthentication != "true" ? "checked" : ""; ?>
	                               onclick="updateChangeStatus(this,'toggleSystemConfig')">
	                               
	                               
	                                <label for="smsNoAuthentication"><span></span></label>
									<label class="labelText customLabelText" for="smsNoAuthentication">Do NOT use SMS authentication</label><br/>
    			
    			 							
                    		  			</div>
                				</div>
<!----- End code ----->
                         <!--LDAP Authentication Type-->
	                    
	                     <div class="col-md-6">
                    					<div class="form-group">
                    				<label class="labelText" for=" ">LDAP Authentication:</label><br/>
                    						
                    				<input type="radio" name="ldapAuthentication"
		                           id="ldapAuthenticationTrue"
		                           class="ldapAuthentication"
		                           value="true" <?php echo $ldapAuthentication == "true" ? "checked" : ""; ?>
		                           onclick="updateChangeStatus(this,'toggleSystemConfig')">
                    						
										<label for="ldapAuthenticationTrue"><span></span></label>
										<label class="labelText customLabelText" for="ldapAuthenticationTrue">Use LDAP</label><br/>
                    					</div>
                    	<?php
	                    if ($ldapAuthentication == "true" && $ldap_users && count($ldap_users) > 0) {
		                    ?>
		                     	
		                    
		                    <div class="labelTextGrey">
			                    LDAP is turned ON for <a id="ldap-users-list-btn" href="#ldap-users-list" style="color: #428BCA;text-decoration: underline;"><?php echo count($ldap_users) ?> users</a>.
			                    You will need to disable them individually before you can disable LDAP.
		                    </div>
		                    <div id="ldap-users-list" title="LDAP Users" style="display: none;">
			                    <p>
				                   
				                    LDAP Authentication is enabled for the following users:<br/><br/>
			                    </p>
			                    <p>
				                    <?php
				                        foreach ($ldap_users as $ldap_user) {
				                        	echo "{$ldap_user["fullName"]} ({$ldap_user["userName"]})<br/>";
				                        }
				                    ?>
			                    </p>
		                    </div>
		                    <script>
                                $(function () {
                                    $("#ldap-users-list-btn").click(function () {
                                        $("#ldap-users-list").dialog({
                                            modal: true,
                                            open: function() {
                                             	//buttonsShowHide('Cancel', 'show');
                                             	//buttonsShowHide('Delete', 'show');
												setDialogDayNightMode($(this));
                                             	$(":button:contains('Ok')").addClass("subButton");
                                             	 
                                             },
                                            buttons: {
                                            	
                                                Ok: function () {
                                                    $(this).dialog("close");
                                                }
                                            }
                                        });
                                    });
									
										// tooltip
										$(document).ready(function(){
											$('[data-toggle="tooltip"]').tooltip();   
										});
                                });
		                    </script>
	                    <?php
	                    } else {
		                    ?>
		               
		                    <div class="diaPN diaP12">
			                    <input type="radio" name="ldapAuthentication"  id="ldapAuthenticationFalse" class="ldapAuthentication" value="false" <?php echo $ldapAuthentication != "true" ? "checked" : ""; ?>
			                           onclick="updateChangeStatus(this,'toggleSystemConfig')">
			                           <label for="ldapAuthenticationFalse"><span></span></label>
		                    </div>
		                    <div class="diaPN diaP12"><label style="margin-left: 6px; margin-top: 4px" class="labelText">Do NOT use LDAP</label></div>
		                    <?php
	                    }
	                    ?>
	                    
                    	</div>
                     	<div class="col-md-6">
		  		<div class="form-group">
					 
				</div>
		  	
		  	</div>
		  <div class="row fcorn-registerTwo">
		  <div id="smsCredential" style="display: none">
		  	<div class="col-md-6">
		  		<div class="form-group">
					<label class="labelText" for=" ">Token:</label><br/>
					<input type="text" name="smsToken" id="smsToken" size="45" <?php echo "value=\"" . $smsToken . "\""; ?> >
				</div>
		  	
		  	</div>
		  	
		  	<div class="col-md-6">
		  	<div class="form-group">
				<label class="labelText" for=" ">SID:</label>
				<input type="text" name="smsSID" id="smsSID" size="45" <?php echo "value=\"" . $smsSID . "\""; ?> >
			</div>
		  	</div>
		  	
		  	
		  	<div class="col-md-6">
		  	<div class="form-group">
				<label class="labelText" for=" ">From:</label>
				 <input type="text" name="smsFrom" id="smsFrom" size="45" <?php echo "value=\"" . $smsFrom . "\""; ?> >
			</div>
		  	</div>
		  	</div>
		  	   <div <?php echo $ldapAuthentication != "true" ? " style='display:none;' " : "" ?> id="ldapHostDiv">
	                    	<div class="col-md-6">
                					<div class="form-group">
                						<label class="labelText" for=" ">LDAP Host:</label><br/>
                						 <input type="text" name="ldapHost" id="ldapHost"
			                                <?php echo "value=\"" . $ldapHost . "\"";?>
			                   				 <?php echo $ldapAuthentication != "true" ? " disabled " : "" ?>required>
											 
											  <?php
			                        if($ldapHost) {
				                        $ldap_connection = ldap_connect($ldapHost);
				                        echo ($ldap_connection && ldap_bind($ldap_connection)) ? "<label style=\"font-size: 11px; font-weight: 600; color: #72ac5d\">(CONNECTED - Anonymous)</label>" :
					                        "<label style=\"font-size: 11px;color: #ac5f5d;font-weight: 600;\">( NOT CONNECTED - Anonymous ) </label>";
			                        }?>
											 
                				 	</div>
                    		</div>
	                   </div>
	         
		</div>                  
	         
	                    <!--Admin Password Field-->
	                    <div class="col-md-6">
							<div class="form-group">
								<label class="labelText">Allow Admins to set User Passwords:</label><br/>
						   
								<input type="radio"
									   name="isAdminPasswordFieldVisible"
									   id="isAdminPasswordFieldVisibleTrue"
									   value="true" <?php echo $isAdminPasswordFieldVisible == "true" ? "checked" : ""; ?>
								/><label for="isAdminPasswordFieldVisibleTrue"><span></span></label>
								<label class="labelText customLabelText">Yes. Password Field is Visible</label><br/>
								
								<input type="radio"
		                           name="isAdminPasswordFieldVisible"
		                           id="isAdminPasswordFieldVisibleFalse"
		                           value="false" <?php echo $isAdminPasswordFieldVisible != "true" ? "checked" : ""; ?>
		                    /><label for="isAdminPasswordFieldVisibleFalse"><span></span></label>
							<label class="labelText customLabelText">No. Email a Password reset link when a new Admin is added.</label>
							</div>
	                    </div>
	                   
	                   <div <?php echo $ldapAuthentication != "true" ? " style='display:none;' " : "" ?> id="ldapDomainDiv">
    	                   <div class="col-md-6">
                					<div class="form-group">
                						<label class="labelText" for=" ">LDAP Domain:</label><br/>
                						 <input type="text" name="ldapDomain" id="ldapDomain"
                						 <?php echo "value=\"" . $ldapDomain . "\"";?>>
                				 	</div>
                    		</div>
                    		<div class="col-md-6">
                            	<div class="form-group">
                                	<input type="checkbox" name="ldapCapitalization" id="ldapCapitalization" value="true" <?php echo $ldapCapitalization == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                    				<label for="ldapCapitalization"><span></span></label>
                    				<label class="labelText" for="ldapCapitalization">Username Capitalization</label>			
                				</div>
                            </div>
                    		
	                   </div>
                    		
	                    <!--Max Failed Login Attempts-->
	                    <div class="col-md-6">
							<div class="form-group">
		                    <label class="labelText">Max Failed Login Attempts:</label>
	                   
		                    <input type="number"
		                           name="maxFailedLoginAttempts"
		                           id="maxFailedLoginAttempts"
		                           style="width: 60px;"
		                           min="1" max="10"
		                           value="<?php echo $maxFailedLoginAttempts; ?>"
		                    />
							</div>
	                    </div>
 <!-- new ui password expiration -->
 <div class="col-md-12 fcorn-registerTwo">
     <label class="labelText">Password Expiration:</label><br/>
</div> 
 
 
<div class="col-md-6">
<div class="form-group">
	<label class="labelText">Super User Expiration time:</label>
    <input type="number" name="passwordExpiration_SU" id="passwordExpiration_SU" size="45"  min="1" max="1000"  <?php echo "value=\"" .$passwordExpiration_SU . "\""; ?>   onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')" />
	</div>
</div>
	                    
<div class="col-md-6">
	<div class="form-group">
		<label class="labelText">Regular User Expiration time:</label>
	    <input type="number" name="passwordExpiration_RU" id="passwordExpiration_RU" size="45"  min="1" max="1000"  <?php echo "value=\"" .$passwordExpiration_RU . "\""; ?>  onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')"/>
	</div>
</div>
 
 
 
 <!-- end password expriration new ui -->
 
 
 <div class="col-md-6">
	<div class="form-group">
		<label class="labelText">Password Reuse Prevention:</label>
	    <input type="number" name="passwordReusePrevention" id="passwordReusePrevention" style="width: 60px; margin-right: 20px;" min="0" max="20" value="<?php echo $passwordReusePrevention; ?>" />
	  <span id="errorSpanForPRPvton" style="color: red;clear: both;">&nbsp;</span>
	</div>
</div>
 
 
                            
                            <!-- Code added @ 13 July 2018 -->
                            <!-- Password Reuse Prevention 
	                    <div class="diaPL diaP12" style="width: 16%">
		                    <label style="font-style: italic; margin-top: 4px"><strong>Password Reuse Prevention:</strong></strong></label>
	                    </div>
	                    <div class="diaPN diaP12">                                
                                <input type="number" name="passwordReusePrevention" id="passwordReusePrevention" style="width: 60px; margin-right: 20px;" min="0" max="20" value="<?php echo $passwordReusePrevention; ?>" />
                                <span id="errorSpanForPRPvton" style="color: red;clear: both;">&nbsp;</span>
                            </div>-->
                            <!-- End code -->

 
                        <!----- SurgeMail ----->
                        
                        <div class="col-md-12">
                			<div class="form-group">
                				<h2 class="adminUserText">SurgeMail</h2>
                				 
                			</div>
    					</div>
    					
    			  <!--Username Criteria-->		
                   <div class="col-md-6">
                    					<div class="form-group">
                    						<label class="labelText" for=" ">Username Criteria #1:</label><br/>
                    						
                    						 <input type="text" name="surgeMailIdCriteria1" id="surgeMailIdCriteria1" size="45" <?php echo "value=\"" . $surgeMailIdCriteria1 . "\""; ?> onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')">
                    						
                    						 
                    					</div>
                    	</div>  
                    	
                    	 <div class="col-md-6">
                    					<div class="form-group">
                    						<label class="labelText" for=" ">Username Criteria #2:</label><br/>
                    						
                    						 <input type="text" name="surgeMailIdCriteria2" id="surgeMailIdCriteria2" size="45" <?php echo "value=\"" . $surgeMailIdCriteria2 . "\""; ?> onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')">
                    						
                    						 
                    					</div>
                    	</div>     

                        <!--Domain-->
                        
                        <div class="col-md-6">
                    					<div class="form-group">
                    						<label class="labelText" for=" ">Domain:</label><br/>
                    						
                    						 <input type="text" name="surgeMailDomain" id="surgeMailDomain" size="45" <?php echo "value=\"" . $surgeMailDomain . "\""; ?> onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')">
                    						
                    						 
                    					</div>
                    	</div> 
                    	
                    	
                    	<div class="col-md-6">
                    					<div class="form-group">
                    						<label class="labelText" for=" ">User Password Type:</label><br/>
                    						
                    						<input type="radio" name="surgeMailUserPasswordType" id="surgeMailUserPasswordTypeStatic" value="Static" <?php echo $surgeMailUserPasswordType == "Static" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                    						
                    						 <label for="surgeMailUserPasswordTypeStatic"><span></span></label>
    										<label class="labelText customLabelText" for="surgeMailUserPasswordTypeStatic">System-wide password for mailbox account</label><br/>
    						  <!--SurgeMail User Password Type-->			
    									<div id="surgeMailUserPasswordDiv">	
    										
    										<label class="labelText" for=" ">SurgeMail User Password:</label><br/>
    									
    										<input type="password" name="surgeMailUserPassword" id="surgeMailUserPassword" size="45"  placeholder="<?php echo $surgeMailUserPassword ? "******" : ""; ?>"
	                                   onchange="updateChangeStatus(this,'toggleSystemConfig')"
	                                   oninput="updateChangeStatus(this,'toggleSystemConfig')">
    									</div>
    										
    										
    									<input type="radio" name="surgeMailUserPasswordType" id="surgeMailUserPasswordTypeDynamic" value="Dynamic" <?php echo $surgeMailUserPasswordType == "Dynamic" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                    						
                    						 <label for="surgeMailUserPasswordTypeDynamic"><span></span></label>
    										<label class="labelText customLabelText" for="surgeMailUserPasswordTypeDynamic">Generate random mailbox password on user creation</label><br/>	
                    					</div>
                    	</div>  
 

                        <!-- Delete VM Account on User Deletion -->
                        
                      <div class="col-md-8">
                    					<div class="form-group">
                    						<label class="labelText" for=" ">Delete Mailbox on User Deletion:</label><br/>
                    						
                    						 <input type="checkbox" name="deleteVMOnUserDel" id="deleteVMOnUserDel" value="true" <?php echo $deleteVMOnUserDel == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                    				
                    				
                						<label for="deleteVMOnUserDel"><span></span></label>
    			<label class="labelText" for="deleteVMOnUserDel">On User deletion, delete voice mailbox account in SurgeMail server</label>			
                    				
                    				 
                    					</div>
                    	</div> 
                    	
                    	
                    	 <!-- Start CounterPath -->
               
                 
               <?php if(isset($license["softPhone"]) && $license["softPhone"] == "true"){ ?>
                           <!-- start Counterpath global configuration  -->
                            <div class="row fcorn-registerTwo">
								<div class="col-md-12">
									<div class="form-group">
										<h2 class="adminUserText">Counterpath Stretto</h2>
									</div>
								</div>
                          <!-- star Counterpath Stretto field work -->
                           
                           		<div id="counterPathStrettoDiv" style="/* display: none; */">
                           	 		<div class="col-md-6">
                        				<div class="form-group">
                        					<label class="labelText" for=" ">Group Name:</label>
                    						<input type="text" name="counterpathGroupName" id="counterpathGroupName" size="45" value="<?php echo $counterpathGroupName; ?>" onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')"> 
                    					</div>
                    				</div>
                                
                              	</div>
                        	</div>
                           
                           <!-- end line/port naming convention -->
                           <?php } ?>          
               <!-- End CounterPath stretto-->
                    	
                    	
                     
<div class="col-md-4">
	<div class="form-group">
	</div>

</div>

                        <!----- System Support ----->
                        
                        <div class="col-md-12">
                        			<div class="form-group">
                        				<h2 class="adminUserText">System Support</h2>
                        				 
                        			</div>
            					</div>
                        
                          <!--Surge Mail Debugging-->
                          <div class="col-md-6">
                    			<div class="form-group">
                						<label class="labelText" for=" ">SurgeMail Debugging:</label><br/>
                						
                						 <input type="checkbox" name="surgeMailDebug" id="surgeMailDebug" value="true" <?php echo $surgeMailDebug == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleSystemConfig')">
                    				
                    				
                						<label for="surgeMailDebug"><span></span></label>
    			<label class="labelText" for="surgeMailDebug">Enable to debug SurgeMail server</label>			
                    		</div>
                    	</div>
                        
                     

                        <!--Support Email-->
                        
                        <div class="col-md-6">
                    					<div class="form-group">
                    						<label class="labelText" for=" ">Support Email:</label><br/>
                    						
                    						 <input type="text" name="supportEmail" id="supportEmail" size="45" <?php echo "value=\"" . $supportEmail . "\""; ?> onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')">
                    			
                    					</div>
                    	</div>
              
                        <!----- Call Detail Records Database ----->
                        
                        <div class="col-md-12">
                        			<div class="form-group">
                        				<h2 class="adminUserText">Call Detail Records Database<br/> <?php showBillingConnectionStatus($billingDbConnected, $error_message); ?>
										</h2>
                        				
                        			</div>
						</div>
                        
                         <!--Host-->
                          <div class="col-md-6">
                    			<div class="form-group">
                						<label class="labelText" for=" ">Host IP Address:</label><br/>
                						
                						 <input type="text" name="billingHost" id="billingHost" size="45" <?php echo "value=\"" . $billingHost . "\""; ?> onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')">		
                    		</div>
                    	</div>
                        
                     

                         <!--Database Name-->
                        
                        <div class="col-md-6">
                    					<div class="form-group">
                    						<label class="labelText" for=" ">CDR Database Name:</label><br/>
                    						
                    						 <input type="text" name="billingDB" id="billingDB" size="45" <?php echo "value=\"" . $billingDB . "\""; ?> onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')">
                    				
                    					</div>
                    	</div>

                        <!--Username-->

                          <div class="col-md-6">
                    					<div class="form-group">
                    						<label class="labelText" for=" ">Username:</label><br/>
                    						
                    						<input type="text" name="billingUser" id="billingUser" size="45" <?php echo "value=\"" . $billingUser . "\""; ?> onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')">
                    				</div>
                    	</div>
                    	
                    	 <!--Password-->                        
                        <div class="col-md-6">
                    					<div class="form-group">
                    						<label class="labelText" for=" ">Password:</label><br/>
                    						
											 <input type="password" name="billingPassword" id="billingPassword" size="45"
	                               placeholder="<?php echo $billingPassword ? "******" : ""; ?>"
	                               onchange="updateChangeStatus(this,'toggleSystemConfig')"
	                               oninput="updateChangeStatus(this,'toggleSystemConfig')">
                    				
                    					</div>
                    	</div>
 <!--New Code-->     
	                    <!-- Billing Force SSl -->
	                    <div class="col-md-6">
							<div class="form-group">
								<label class="labelText">Use SSL:</label><br/>
								<input type="checkbox"
		                           name="billingForceSSL"
		                           id="billingForceSSL"
		                           value="true"
			                    <?php echo $billingForceSSL == "true" ? "CHECKED" : ""; ?>/><label for="billingForceSSL"><span></span></label>
							</div>
	                    </div>

	                    <!-- Billing CA Cert -->
						<div class="col-md-6">
							<div class="form-group" id="billingCACertDiv" <?php echo $billingForceSSL == "true" ? "" : "style='display:none;'"; ?>>
		                    <label style="" class="labelText">SSL CA Cert:</label>
			                 <label class="labelTextGrey"><?php echo $billingCACert ? $billingCACert . "<br/>" : ""; ?> </label>
				                    <input type="file" class="inputfile" name="billingCACert" id="billingCACert"/>
				                    <label class="BrowseLabel labelTextGrey" for="billingCACert">Browse...</label>		                   
		                    </div>
	                    </div>

<!--New code --> 
	
			   <!----- User Limit Group Configuration----->
                        <div class="col-md-12">
							<div class="form-group">
								<h2 class="adminUserText">New User Email Report</h2>
							</div>
						</div>

                        <!--Surge Mail Debugging-->
						<div class="col-md-12" style="padding:0;">
						<div class="col-md-6">
							<div class="form-group">
								<input type="checkbox" name="newUsrAdminReport" id="newUsrAdminReport" value="true" <?php echo $newUsrAdminReport == "true" ? "checked" : ""; ?>>
								<label for="newUsrAdminReport"><span></span></label>
								<label class="labelText">Send Report to Admin</label>
								
							</div>
						</div>
                        <div class="col-md-6"></div>
						</div>
						<div class="col-md-6 sendReportSupervisorInpt">
								<input type="checkbox" name="newUsrSupervisorReport" id="newUsrSupervisorReport" value="true" <?php echo $newUsrSupervisorReport == "true" ? "checked" : ""; ?>>
								<label for="newUsrSupervisorReport"><span></span></label>
								<label class="labelText">Send Report to Supervisor</label>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label class="labelText">Supervisor Email Account:</label><br/>
								<input type="text" name="newUsrSupervisorEmail" id="newUsrSupervisorEmail" value="<?php echo $newUsrSupervisorEmail; ?>" />
							</div>
						</div>
<!--New Code End-->
                        <!----- Call Detail Records Database ----->
                        <!----- User Limit Group Configuration----->
                        
                         <div class="col-md-12">
                        			<div class="form-group">
                        				<h2 class="adminUserText">User Limit Group Configuration</h2>
                        				
                        			</div>
            					</div>
                        
                          <!--Surge Mail Debugging-->
                          	<div class="col-md-6">
                    			<div class="form-group">
            						<label class="labelText" for=" ">User Limit:</label><br/>
            						
            						 <input type="checkbox" name="userLimitGroup" id="userLimitGroup" value="true" <?php echo $userLimitGroup == "true" ? "checked" : ""; ?>>	
            						 <label for="userLimitGroup"><span></span></label>
									<label class="labelText" for="userLimitGroup">Visible</label>		
                    			</div>
                    		</div>
                    	
                    	
                    	
	                    <!----- Inactivity Configuration----->
	                    <div class="col-md-12">
                			<div class="form-group">
                				<h2 class="adminUserText">Admin Inactivity Configuration</h2>
                				
                			</div>
            			</div>
	                    <!--Timeout-->
	                   
	                    <div class="col-md-8">
                    			<div class="form-group">
                						<label class="labelText" for=" ">Timeout:</label><br/>
                						
                						<input type="number" name="inactivityTimeout" id="inactivityTimeout" value="<?php echo $inactivityTimeout / 60 ?>" style="width: 60px;">
    			<label class="labelText" for="userLimitGroup">Minutes</label>		
                    		</div>
                    	</div>
                    	
                    	<div class="col-md-4">
                    		<div class="form-group">
                    		</div>
                    	</div>
<!--</div>-->
						<!----- Group UI Options----->
						<div class="col-md-12">
                			<div class="form-group">
                				<h2 class="adminUserText">Group UI Options</h2>
                			</div>
            			</div>
	                    
	<!--Use Extension Length Range-->
							<div class="col-md-6">
                    			<div class="form-group">
            						<label class="labelText" for=" ">Use Extension Length Range:</label><br/>
            						
            						<input type="checkbox" name="useExtensionLengthRange" id="useExtensionLengthRange" value="true" <?php echo $useExtensionLengthRange == "true" ? "checked" : ""; ?>>	
            						 <label for="useExtensionLengthRange"><span></span></label>
									<label class="labelText" for="userLimitGroup">Visible</label>		
                    			</div>
                    		</div>
	                    
	   <!-- end --> 
           
           
           <!--start code for express license -->
           
           	<div class="col-md-12">
                    <div class="form-group">
                            <h2 class="adminUserText">BroadWorks Licensing Thresholds</h2>
                    </div>
                </div>
           
           
           <!--warning fields added-->
           <div class="col-md-6">
                <div class="form-group">
                    <label class="labelText" for=" ">Warnings:</label><br/>
                    <input type="text" name="warningsThreshold" id="warningsThreshold" <?php echo "value=\"" . $warningsThreshold . "\""; ?> onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')">	
                    <span id="errorwarningThreshold" style="color: red;clear: both;">&nbsp;</span>
                </div>
            </div>
          <!--end warning fileds content -->
          
           <!--warning fields added-->
           <div class="col-md-6">
                <div class="form-group">
                    <label class="labelText" for=" ">Alerts:</label><br/>
                    <input type="text" name="alertsThreshold" id="alertsThreshold" <?php echo "value=\"" . $alertsThreshold . "\""; ?> onchange="updateChangeStatus(this,'toggleSystemConfig')" oninput="updateChangeStatus(this,'toggleSystemConfig')">
                        <span id="erroralertsThreshold" style="color: red;clear: both;">&nbsp;</span>
                </div>
            </div>
          <!--end warning fileds content -->
           <!--end start code for express licese-->
           
           
	   
<!-- 	   Start cluster       -->
		<?php if(isset($license["bwClusters"]) && $license["bwClusters"] == "true"){?> 
				<div class="col-md-4">
            		<div class="form-group">
            		</div>
            	</div>
				<div class="col-md-12">
        			<div class="form-group">
        				<h2 class="adminUserText">BroadWorks Clusters</h2>
        			</div>
    			</div>
                
				<div class="col-md-6">
        			<div class="form-group">
        				<?php $numberOfCluster = 3;?>            						
						<input type="checkbox" name="enableClusters" id="enableClusters" value="true" <?php echo $enableClusters == "true" ? "checked" : ""; ?> onchange="enableClusterChange( <?php echo count($getClusterInfo); ?> )">
						 <label for="enableClusters"><span></span></label>
						<label class="labelText" for="">Enable Clusters</label>								
        			</div>
        		</div>
		<?php }?>
       	
<!-- 	   End cluster         -->

	                  <!----- Express Sheets Configuration----->
	         <!-----             
	                    <div class="col-md-12">
                			<div class="form-group">
                				<h2 class="adminUserText">Express Sheets Configuration</h2>
                			 </div>
    					</div>-->
    					<!--Advanced Filters-->
    			<!----- 		<div class="col-md-6">
                			<div class="form-group">
            						<label class="labelText" for=" ">User Filters:</label><br/>
            						
            						<input type="checkbox" name="advancedUserFilters" id="advancedUserFilters" value="true" <?php //echo $advancedUserFilters == "true" ? "checked" : ""; ?>>
            						<label for="advancedUserFilters"><span></span></label>
									<label class="labelText" for="advancedUserFilters">Enable Advanced Filters</label>	
                			</div>
                    	</div>
                    	 
					-->
                
                    <div class="col-md-12 form-group alignBtn">
        			 	<input type="button" name="submitSystemConfig" class="submitAndView marginRightButton" id="submitSystemConfig" value="Submit" onclick="submitConfig(this, 'systemConfig', 'toggleSystemConfig')">
        			 	
        			 </div>
    			 </div>
            </form>
        </div><!-- end system config div -->
                    </div>
                </div>
            
         <?php
    }
    
    if(isset($_POST['adminConfigType']) && $_POST['adminConfigType'] == "group_config")
    {
        // get Group Configuration
        // -----------------------
        /*broadwork expressLicense warning and alert */
        /*end code */

        $groupConfig = new DBSimpleReader($db, "groupConfig");
        $groupDomainRequired = $groupConfig->get("groupDomainRequired");
        //echo "IN IF - ".$_POST['adminConfigType'];

       ?>          
          <div id="divGroupConfig">
                    <div class="">
                    <form action="#" method="post" id="groupConfig" name="groupConfig" class="sytemAdminForm">
                    <input type="hidden" id="tabId" value="group_config" />
                    
                    <input type="hidden" name="groupConfigForm" value="true">
                        <div style="">
                        <div class="col-md-12">
                            <div class="form-group">
                                <h2 class="adminUserText">New Group Policies</h2>
                            </div>
                        </div>
                       
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="labelText" for="userLimitGroup">Group Domain:</label><br>
                                <input type="checkbox" name="groupDomainRequired" id="groupDomainRequired" value="true" onclick="updateChangeStatus(this,'toggleGroupConfig')">
                                <label for="groupDomainRequired"><span></span></label>
                                <label class="labelText" for="groupDomainRequired">Required</label>
                            </div>
                        </div>
                    </div>                      
                </form>
           </div>
          </div>
         <?php
    }
        
    
    if(isset($_POST['adminConfigType']) && $_POST['adminConfigType'] == "pass_rule_config")
    {
        //echo "IN IF - ".$_POST['adminConfigType'];
        ?>
           <!--------------------------->
           <!-- Password Rules Configuration -->
           <!--------------------------->
           <div style="" id="divPasswordConfig">
			 <div class="">
				<!-- form -- form -- form -- form -- form -- form -- form -- form -->
				<form action="#" method="post" id="passwordRulesConfig" class="fcorn-registerTwo sytemAdminForm" name="passwordRulesConfig">
					<input type="hidden" id="tabId" value="pass_rule_config" />
                                        
                                        <!-- form identifier -->
					<input type="hidden" name="passwordRulesConfigForm" value="true">

					<div style="">

						<?php
						$passwordRulesConfiguration = new PasswordRulesConfiguration();
						$passwordTypes = $passwordRulesConfiguration->getAllPasswordTypes();
						foreach ($passwordTypes as $passwordType) {
							$passwordRulesConfiguration->loadRulesFor($passwordType);
							$passwordRulesLabel = $passwordRulesConfiguration->getLabel();
							$passwordRulesMinimumLength = $passwordRulesConfiguration->getMinimumLength();
							$passwordRulesNumberOfDigits = $passwordRulesConfiguration->getNumberOfDigits();
							$passwordRulesSpecialCharacters = $passwordRulesConfiguration->getSpecialCharacters();
							$passwordRulesUpperCaseLetter = $passwordRulesConfiguration->getUpperCaseLetter();
							$passwordRulesLowerCaseLetter = $passwordRulesConfiguration->getLowerCaseLetter();
							?>
							<!-- Password Rules -->

							<input type="hidden" name="passwordTypes[]" value="<?php echo $passwordType; ?>">
								<div class="col-md-12">
									<div class="form-group">
										<h2 class="adminUserText"><?php echo $passwordRulesLabel ?></h2>
										 
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
									<label class="labelText">Minimum Password Length:</label>
										<input type="number"
										   name="passwordRules[<?php echo $passwordType; ?>][MinimumLength]"
										   class="passwordRulesMinimumLength"
										   id="MinimumLength_<?php echo $passwordType; ?>"
										   min="8"
										   max="40"
										   value="<?php echo $passwordRulesMinimumLength; ?>"
									/> <label class="labelTextGrey">Minimum 8</label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
									<label class="labelText">Number of Digits:</label>
									<input type="number"
								       name="passwordRules[<?php echo $passwordType; ?>][NumberOfDigits]"
								       class="passwordRulesNumberOfDigits"
								       id="NumberOfDigits_<?php echo $passwordType; ?>"
								       min="1"
								       max="5"
								       value="<?php echo $passwordRulesNumberOfDigits; ?>"
									/> <label class="labelTextGrey">Minimum 1</label>
									</div>
								</div>
							
							
							<div class="col-md-6">
							<div class="form-group">
								
								<input type="checkbox"
								       name="passwordRules[<?php echo $passwordType; ?>][SpecialCharacters]"
								       class="passwordRulesSpecialCharacters"
								       id="SpecialCharacters_<?php echo $passwordType; ?>"
								       value="true"
									<?php echo $passwordRulesSpecialCharacters == "true" ? "CHECKED" : "" ?>
								/> <label for="SpecialCharacters_<?php echo $passwordType; ?>"><span></span></label>
								<label class="labelText">Need a Special Character:</label>
								<label class="labelTextGrey">Eg: !@#$%^&*()_+{}|[]\:";'<>?,./`~</label>
							</div>
							</div>
							
							<div class="col-md-6">
							<div class="form-group">
								
								<input type="checkbox"
								       name="passwordRules[<?php echo $passwordType; ?>][UpperCaseLetter]"
								       class="passwordRulesUpperCaseLetter"
								       id="UpperCaseLetter_<?php echo $passwordType; ?>"
								       value="true"
									<?php echo $passwordRulesUpperCaseLetter == "true" ? "CHECKED" : "" ?>
								/> <label for="UpperCaseLetter_<?php echo $passwordType; ?>"><span></span></label>
								<label class="labelText">Need an Upper Case Letter:</label>
								<label class="labelTextGrey">A-Z</label>
							
							</div>
							</div>
							
							<div class="col-md-6">
							<div class="form-group">
								
								<input type="checkbox"
								       name="passwordRules[<?php echo $passwordType; ?>][LowerCaseLetter]"
								       class="passwordRulesLowerCaseLetter"
								       id="LowerCaseLetter_<?php echo $passwordType; ?>"
								       value="true"
									<?php echo $passwordRulesLowerCaseLetter == "true" ? "CHECKED" : "" ?>
								/> <label for="LowerCaseLetter_<?php echo $passwordType; ?>"><span></span></label>
								<label class="labelText">Need a Lower Case Letter:</label>
								<label class="labelTextGrey">a-z</label>
						
							</div>
							</div>
							<?php
						}
						?>
					</div>
				</form>
					<div class="col-md-12 form-group alignBtn">
						<input type="button"
							   name="submitPasswordRulesConfig"
							   id="submitPasswordRulesConfig"
							   value="Submit"
							   class="submitAndView marginRightButton"
							   onclick="submitConfig(this, 'passwordRulesConfig', 'togglePasswordRulesConfig')"
						/>
					</div>
					
				</div>

			</div>
        <?php  
    }
    
    if(isset($_POST['adminConfigType']) && $_POST['adminConfigType'] == "user_config")
    {
            // get User configuration
            // ----------------------
            $userConfig = new DBSimpleReader($db, "userConfig");
            $userIdCriteria1 = $userConfig->get("userIdCriteria1");
            $userIdCriteria2 = $userConfig->get("userIdCriteria2");
            $clidPolicyLevel = $userConfig->get("clidPolicyLevel");
            $clidPolicy = $userConfig->get("clidPolicy");
            $clidNameFieldsVisible = $userConfig->get("clidNameFieldsVisible");
            /* code added at @23-oct-2018,  Name Dialing Names feature */
            $dialingNameFieldsVisible = $userConfig->get("dialingNameFieldsVisible");
            $clidNumberRequired = $userConfig->get("clidNumberRequired");
            $clidPolicyFieldVisible = $userConfig->get("clidPolicyFieldVisible");
            $postalAddressRequired = $userConfig->get("postalAddressRequired");
            $useGroupAddress = $userConfig->get("useGroupAddress");
            $addressFieldsVisible = $userConfig->get("addressFieldsVisible");
            //Modifiable address field
            $addressFieldsModifiable = $userConfig->get("addressFieldsModifiable");
            //end 
            $useGroupTimeZone = $userConfig->get("useGroupTimeZone");
            $timeZoneFieldVisible = $userConfig->get("timeZoneFieldVisible");
            $emailFieldVisible = $userConfig->get("emailFieldVisible");
            //Code added for location visible fields
            $locationFieldVisible = $userConfig->get("locationFieldVisible");
            //End code
            $languageFieldVisible = $userConfig->get("languageFieldVisible");
            $deviceFieldsVisible = $userConfig->get("deviceFieldsVisible");
            $departmentsFieldVisible = $userConfig->get("departmentsFieldVisible");
            //$ccdFieldVisible = $userConfig->get("ccdFieldVisible"); //Code commented @ 07 Sep 2018
            $cpgFieldVisible = $userConfig->get("cpgFieldVisible");
            $macAddressFieldVisible = $userConfig->get("macAddressFieldVisible");
            $voiceMessagingVisible = $userConfig->get("voiceMessagingVisible");
            $polycomServicesFieldVisible = $userConfig->get("polycomServicesFieldVisible");
            $numSIPGatewayInstances = $userConfig->get("numSIPGatewayInstances");
            //$numberActivation = $userConfig->get("numberActivation");
            //New code
            $serviceAssignmentVisible = $userConfig->get("serviceAssignmentVisible");
            $sharedDeviceVisible = $userConfig->get("sharedDeviceVisible");
            $softPhoneVisible = $userConfig->get("softPhoneVisible");
            //End code
            //echo "IN IF - ".$_POST['adminConfigType'];
            ?>
             <!------------------------>
             <!-- User Configuration -->
            <!------------------------>
            <div id="divUserConfig">
                    <div class="">
                    	<form action="#" method="post" id="userConfig" name="userConfig" class="fcorn-registerTwo sytemAdminForm ">
                         <input type="hidden" id="tabId" value="user_config" />
                         
                    <!-- form identifier -->
                    <input type="hidden" name="userConfigForm" value="true">

                    
                    <div class="col-md-12">
            			<div class="form-group">
            				<h2 class="adminUserText">User ID Naming Convention</h2>
            				 
            			</div>
            		</div>
            		
            		<!----- User ID Naming Convention ----->
            		 <!--Criteria #1-->
            		<div class="col-md-6">
            			<div class="form-group">
            			 	<label class="labelText" for="userLimitGroup">Criteria #1:</label>	<br/>
            			 	<input type="text" name="userIdCriteria1" id="userIdCriteria1" size="50" <?php echo "value=\"" . $userIdCriteria1 . "\""; ?> onchange="updateChangeStatus(this,'toggleUserConfig')" oninput="updateChangeStatus(this,'toggleUserConfig')">
            			</div>
					</div>
                    
               <!--Criteria #2-->      
                    <div class="col-md-6">
            			<div class="form-group">
            			 	<label class="labelText" for="userLimitGroup">Criteria #2:</label><br/>
            			 	<input type="text" name="userIdCriteria2" id="userIdCriteria2" size="50" <?php echo "value=\"" . $userIdCriteria2 . "\""; ?> onchange="updateChangeStatus(this,'toggleUserConfig')" oninput="updateChangeStatus(this,'toggleUserConfig')">
            			</div>
					</div>
					
			<!----- User Calling Line ID Policies ----->
			
			 <div class="col-md-12">
            			<div class="form-group">
            				<h2 class="adminUserText">User Calling Line ID Policies</h2>
            				 
            			</div>
            		</div>
            		 <!--Calling Line ID Policy Level-->		
					 <div class="col-md-6">
            			<div class="form-group">
            			 	<label class="labelText" for="userLimitGroup">Calling Line ID Policy Level:</label><br/>
            			 	<input type="radio" name="clidPolicyLevel" id="clidPolicyLevelUser" value="User" <?php echo $clidPolicyLevel == "User" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
            			 	
            			 	<label for="clidPolicyLevelUser"><span></span></label>
    						<label class="labelText customLabelText" for="clidPolicyLevelUser">Use User Calling Line Id policy</label><br/>
    						
    						<input type="radio" name="clidPolicyLevel" id="clidPolicyLevelGroup" value="Group" <?php echo $clidPolicyLevel == "Group" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
    						
    						<label for="clidPolicyLevelGroup"><span></span></label>
    						<label class="labelText customLabelText" for="clidPolicyLevelGroup">Use Group Calling Line Id policy</label><br/>
    						
            			</div>
					</div>
					
					
					<!--Calling Line ID Policy-->
					
					 <div class="col-md-6">
                                            <div class="form-group">
                                                    <label class="labelText" for="userLimitGroup">Calling Line ID Policy:</label><br/>
                                                    <input type="radio" name="clidPolicy" id="clidPolicyDN" value="Use DN" <?php echo $clidPolicy == "Use DN" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                                                    <label for="clidPolicyDN"><span></span></label>
                                                            <label class="labelText customLabelText" for="clidPolicyDN">Use user phone number</label><br/>

                                                             <input type="radio" name="clidPolicy" id="clidPolicyConfigCLID" value="Use Configurable CLID" <?php echo $clidPolicy == "Use Configurable CLID" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">

                                                            <label for="clidPolicyConfigCLID"><span></span></label>
                                                            <label class="labelText customLabelText" for="clidPolicyConfigCLID">Use user configurable CLID</label><br/>

                                                            <input type="radio" name="clidPolicy" id="clidPolicyGroupCLID" value="Use Group CLID" <?php echo $clidPolicy == "Use Group CLID" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">

                                                            <label for="clidPolicyGroupCLID"><span></span></label>
                                                            <label class="labelText customLabelText" for="clidPolicyGroupCLID">Use group/department phone number</label><br/>

                                            </div>
					</div>             

                        <!----- New User Policies ----->

                        <div class="col-md-12">
            			<div class="form-group">
            				<h2 class="adminUserText">User UI Options</h2>
            			</div>
            		</div>            		
                        <!--Calling Line ID Name  -->
                        <div class="col-md-6">
                                <div class="form-group">
            			 	<label class="labelText" for="userLimitGroup">Calling Line ID Name:</label><br/>
            			 	<input type="checkbox" name="clidNameFieldsVisible" id="clidNameFieldsVisible" value="true" <?php echo $clidNameFieldsVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
            			 	<label for="clidNameFieldsVisible"><span></span></label>
    						<label class="labelText" for="clidPolicyDN">Visible Fields</label><br/>
            			</div>
			</div>
                        <!--  Code added @ 25 Oct 2018 Name Dailing Names -->
                         <?php 
                            if($ociVersion=="20" || $ociVersion=="21" || $ociVersion=="22"){
                         ?>                        
                        <div class="col-md-6">
                            <div class="form-group">
                                    <label class="labelText" for="nameDialNMS">Name Dialing Names:</label><br/>
                                    <input type="checkbox" name="dialingNameFieldsVisible" id="dialingNameFieldsVisible" value="true" <?php echo $dialingNameFieldsVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                                    <label for="dialingNameFieldsVisible"><span></span></label>
                                            <label class="labelText" for="NameDM">Visible Fields</label><br/>
                            </div>
                        </div>
                        <?php } ?>
                        <!-- End code --> 
                        
                    <!--Calling Line ID Phone Number-->
                    <div class="col-md-6">
                            <div class="form-group">
                                    <label class="labelText" for="userLimitGroup">Calling Line ID Phone Number:</label><br/>
                                    <input type="checkbox" name="clidNumberRequired" id="clidNumberRequired" value="true" <?php echo $clidNumberRequired == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                                    <label for="clidNumberRequired"><span></span></label>
                                            <label class="labelText" for="clidPolicyDN">Required</label><br/>


                            </div>
                    </div>
                  
                        <!--Calling Line ID Policy-->
                         <div class="col-md-6">
            			<div class="form-group">
            			 	<label class="labelText" for="userLimitGroup">Calling Line ID Policy:</label><br/>
            			 	<input type="checkbox" name="clidPolicyFieldVisible" id="clidPolicyFieldVisible" value="true" <?php echo $clidPolicyFieldVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
            			 	<label for="clidPolicyFieldVisible"><span></span></label>
    						<label class="labelText" for="clidPolicyFieldVisible">Visible Field</label><br/>
    						 
    						
            			</div>
					</div>
					
                      <!--Postal Address-->
                      <div class="col-md-6">
            			<div class="form-group">
            			 	<label class="labelText" for="userLimitGroup">Postal Address:</label><br/>
            			 	<input type="checkbox" name="postalAddressRequired" id="postalAddressRequired" value="true" <?php echo $postalAddressRequired == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
            			 	<label for="postalAddressRequired"><span></span></label>
    						<label class="labelText" for="postalAddressRequired">Required</label><br/>
    						
    						
    						<input type="checkbox" name="useGroupAddress" id="useGroupAddress" value="true" <?php echo $useGroupAddress == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
            			 	<label for="useGroupAddress"><span></span></label>
    						<label class="labelText" for="useGroupAddress">Use Group's postal address</label><br/>
    						
    						
    						<input type="checkbox" name="addressFieldsVisible" id="addressFieldsVisible" value="true" <?php echo $addressFieldsVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
            			 	<label for="addressFieldsVisible"><span></span></label>
    						<label class="labelText" for="addressFieldsVisible">Visible Address Fields</label><br/>
    						
    						<input type="checkbox" name="addressFieldsModifiable" id="addressFieldsModifiable" value="true" <?php echo $addressFieldsModifiable == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
            			 	<label for="addressFieldsModifiable"><span></span></label>
    						<label class="labelText" for="addressFieldsVisible">Modifiable Address Fields</label><br/>
    						
    					</div>
					</div>       
					
<!----- New code found Start ----->	
                        <!-- ex-404 - 26-02-2018 -->
                         
						<!-- end ex-404 -->
<!----- New code found End ----->	

                        <!--Use Group's Time Zone-->
             <div class="row" style="margin: auto 0px;">       
                        <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Time Zone:</label><br/>
                			 	<input type="checkbox" name="useGroupTimeZone" id="useGroupTimeZone" value="true" <?php echo $useGroupTimeZone == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                			 	<label for="useGroupTimeZone"><span></span></label>
        						<label class="labelText" for="useGroupTimeZone">Use Group's time zone</label><br/>
        						
        						<input type="checkbox" name="timeZoneFieldVisible" id="timeZoneFieldVisible" value="true" <?php echo $timeZoneFieldVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                			 	<label for="timeZoneFieldVisible"><span></span></label>
        						<label class="labelText" for="timeZoneFieldVisible">Visible Field</label><br/>
        						
    						</div>
						</div>
                        
                     <!--Email Address-->
                     
                     <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Email Address:</label><br/>
                			 	<input type="checkbox" name="emailFieldVisible" id="emailFieldVisible" value="true" <?php echo $emailFieldVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                			 	<label for="emailFieldVisible"><span></span></label>
        					 
        						<label class="labelText" for="emailFieldVisible">Visible Field</label><br/>
        						
    					</div>
					</div>
				</div>    	
					
			<!-- New Code found @ 02 May 2018 -->
                        <!--Location-->		
					  <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Location:</label><br/>
                			 	<input type="checkbox" name="locationFieldVisible" id="locationFieldVisible" value="true" <?php echo $locationFieldVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                			 	<label for="locationFieldVisible"><span></span></label>
        					 
        						<label class="labelText" for="locationFieldVisible">Visible Field</label><br/>
        						
    					</div>
					</div>
                      
 
<!-- New Code found @ 02 May 2018 -->
                       
					   <!--Language-->
                         <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Language:</label><br/>
                			 	<input type="checkbox" name="languageFieldVisible" id="languageFieldVisible" value="true" <?php echo $languageFieldVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                			 	<label for="languageFieldVisible"><span></span></label>
        						 
        						<label class="labelText" for="languageFieldVisible">Visible Field</label><br/>
        						
    					</div>
					</div>
                         
                    
                        <!--Device Access-->
                        <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Device Access:</label><br/>
                			 	<input type="checkbox" name="deviceFieldsVisible" id="deviceFieldsVisible" value="true" <?php echo $deviceFieldsVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                			 	<label for="deviceFieldsVisible"><span></span></label>
        						<label class="labelText" for="deviceFieldsVisible">Visible Fields</label><br/>
        					 
        						
    					</div>
					</div>
                                            
                        <!--Departments-->
                        
					<div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Department:</label><br/>
                			 	<input type="checkbox" name="departmentsFieldVisible" id="departmentsFieldVisible" value="true" <?php echo $departmentsFieldVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                			 	<label for="departmentsFieldVisible"><span></span></label>
        						<label class="labelText" for="departmentsFieldVisible">Visible Fields</label><br/>
        					 
        						
    					</div>
					</div>
                         

                        <!--Custom Contact Directory-->
                        
                         <!--<div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Custom Contact Directory:</label><br/>
                			 	<input type="checkbox" name="ccdFieldVisible" id="ccdFieldVisible" value="true" <?php //echo $ccdFieldVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                			 	<label for="ccdFieldVisible"><span></span></label>
        						<label class="labelText" for="ccdFieldVisible">Visible Fields</label><br/>
        					 </div>
						</div>-->
                         
                
                        <!--Call Pickup Group-->
                        <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Call Pickup Group:</label><br/>
                			 	<input type="checkbox" name="cpgFieldVisible" id="cpgFieldVisible" value="true" <?php echo $cpgFieldVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                			 	<label for="cpgFieldVisible"><span></span></label>
        						<label class="labelText" for="cpgFieldVisible">Visible Fields</label><br/>
        					 </div>
						</div>
                        
                         
                       

                        <!--MAC Address-->
                        <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">MAC Address:</label><br/>
                			 	<input type="checkbox" name="macAddressFieldVisible" id="macAddressFieldVisible" value="true" <?php echo $macAddressFieldVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                			 	<label for="macAddressFieldVisible"><span></span></label>
        						<label class="labelText" for="macAddressFieldVisible">Visible Fields</label><br/>
        					 </div>
						</div>
                         
                      

                        <!--Voice Messaging-->
                         <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Voice Messaging:</label><br/>
                			 	<input type="checkbox" name="voiceMessagingVisible" id="voiceMessagingVisible" value="true" <?php echo $voiceMessagingVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                			 	<label for="voiceMessagingVisible"><span></span></label>
        						<label class="labelText" for="voiceMessagingVisible">Visible Fields</label><br/>
        					 </div>
						</div>
                         
                         
                        

                        <!--Polycom Phone Services-->
                         <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Polycom Phone Services:</label><br/>
                			 	<input type="checkbox" name="polycomServicesFieldVisible" id="polycomServicesFieldVisible" value="true" <?php echo $polycomServicesFieldVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                			 	<label for="polycomServicesFieldVisible"><span></span></label>
        						<label class="labelText" for="polycomServicesFieldVisible">Visible Fields</label><br/>
        					 </div>
						</div>
                         
                         <!--SIP Gateway Instances-->
                        <div class="col-md-12">
                			<div class="form-group">
                			 	<label class="labelText" for="">SIP Gateway Instances:</label><br/>
                			 	<div class="dropdown-wrap oneColWidth">
                			 	<select name="numSIPGatewayInstances" id="numSIPGatewayInstances" onclick="updateChangeStatus(this,'toggleUserConfig')"><?php echo selectNumberSIPGatewayInstances($numSIPGatewayInstances); ?></select>
                			 	</div>
                			  
        						<label class="labelText" for="voiceMessagingVisible">Maximum number of SIP gateways instances</label><br/>
        					 </div>
						</div>
                          
                        
						
						<!--new code added for  Services Assignment -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="labelText" for="">Services Assignment:</label><br/>
                                <input type="checkbox" name="serviceAssignmentVisible" id="serviceAssignmentVisible" value="true" <?php echo $serviceAssignmentVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                                <label for="serviceAssignmentVisible"><span></span></label>
                                <label class="labelText" for="serviceAssignmentVisible">Visible Fields</label><br/>
                            </div>
                        </div>
                         
                        <!--end new code -->
                        
                         <!--new code added for  Shared Devices -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="labelText" for="">Shared Devices:</label><br/>
                                <input type="checkbox" name="sharedDeviceVisible" id="sharedDeviceVisible" value="true" <?php echo $sharedDeviceVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                                <label for="sharedDeviceVisible"><span></span></label>
                                <label class="labelText" for="sharedDeviceVisible">Visible Fields</label><br/>
                            </div>
                        </div>
                         
                        <!--end new code -->
                        
                        <!--new code added for  Soft Phones -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="labelText" for="">Soft Phones:</label><br/>
                                <input type="checkbox" name="softPhoneVisible" id="softPhoneVisible" value="true" <?php echo $softPhoneVisible == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleUserConfig')">
                                <label for="softPhoneVisible"><span></span></label>
                                <label class="labelText" for="softPhoneVisible">Visible Fields</label><br/>
                            </div>
                        </div>
                         
                        <!--end new code -->
						
						
                        <!--SIP Gateway Instances-->
                        <!-- <div class="diaPL diaP6" style="width: 16%"><label style="font-style: italic; margin-top: 4px"><strong>Number Activation:</strong></strong></label></div>
                        <div class="diaPN diaP6"><input type="checkbox" name="numberActivation" id="numberActivation" value="Yes" <?php //echo $numberActivation == "Yes" ? "checked" : ""; ?> /></div>
                        <div class="diaPN diaP6"><label style="margin-left: 6px; margin-top: 4px">Activate On User Creation</label></div> -->

					<div class="col-md-12 form-group alignBtn">
						<input type="button" name="submitUserConfig" class="submitAndView marginRightButton" id="submitUserConfig"value="Submit" onclick="submitConfig(this, 'userConfig', 'toggleUserConfig')">
					</div>
				</form>
                    </div>
                </div>
            
            <?php
    }
    
    if(isset($_POST['adminConfigType']) && $_POST['adminConfigType'] == "devices_config")
    {
        // get Device configuration
        // ----------------------
        $deviceConfig = new DBSimpleReader($db, "deviceConfig");
        //$useCustomDeviceList = $deviceConfig->get("useCustomDeviceList");
        $deleteAnalogUserDevice = $deviceConfig->get("deleteAnalogUserDevice");
        $linePortCriteria1 = $deviceConfig->get("linePortCriteria1");
        $linePortCriteria2 = $deviceConfig->get("linePortCriteria2");
        $scaLinePortCriteria1 = $deviceConfig->get("scaLinePortCriteria1");
        $scaLinePortCriteria2 = $deviceConfig->get("scaLinePortCriteria2");
        $deviceNameDID1 = $deviceConfig->get("deviceNameDID1");
        $deviceNameDID2 = $deviceConfig->get("deviceNameDID2");
        $deviceNameAnalog = $deviceConfig->get("deviceNameAnalog");
        $deviceAccessUserName1 = $deviceConfig->get("deviceAccessUserName1");
        $deviceAccessUserName2 = $deviceConfig->get("deviceAccessUserName2");
        $deviceAccessPassword = $deviceConfig->get("deviceAccessPassword");
        $analogAccessAuthentication = $deviceConfig->get("analogAccessAuthentication");
        $scaOnlyCriteria = $deviceConfig->get("scaOnlyCriteria");
        $rebuildResetDevice = $deviceConfig->get("rebuildResetDevice");
        $softPhoneLinePortCriteria1 = $deviceConfig->get("softPhoneLinePortCriteria1");
        $softPhoneLinePortCriteria2 = $deviceConfig->get("softPhoneLinePortCriteria2");
        //echo "IN IF - ".$_POST['adminConfigType'];
        ?>
        <!-------------------------->
        <!-- Device Configuration -->
        <!-------------------------->
        <div id="divDeviceConfig">
                    <div class="">
                    	<form action="#" method="post" id="deviceConfig" name="deviceConfig" class="fcorn-registerTwo sytemAdminForm">
                        <input type="hidden" id="tabId" value="devices_config" />
                        
                    <!-- form identifier -->
                    <input type="hidden" name="deviceConfigForm" value="true">

                    <div class="col-md-12">
            			<div class="form-group">
            				<h2 class="adminUserText">Device Inventory</h2>
            				 
            			</div>
            		</div>
                    
                   
			<!--Delete Analog User Device-->		
					 <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Delete Analog User Device:</label><br/>
                			 	<input type="checkbox" name="deleteAnalogUserDevice" id="deleteAnalogUserDevice" value="true" <?php echo $deleteAnalogUserDevice == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleDeviceConfig')">
                			 	
                			 	<label for="deleteAnalogUserDevice"><span></span></label>
    			<label class="labelText" for="deleteAnalogUserDevice">Delete shared device on analog User deletion</label>
						</div>
					</div>	
					
					
				<!--Rebuild and Reset Device-->	
					 <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Rebuild and Reset Option:</label><br/>
                			 	<input type="checkbox" name="rebuildResetDevice" id="rebuildResetDevice" value="true" <?php echo $rebuildResetDevice == "true" ? "checked" : ""; ?> onclick="updateChange
                            Status(this,'toggleDeviceConfig')">
                			 	
                			 	<label for="rebuildResetDevice"><span></span></label>
    			<label class="labelText" for="rebuildResetDevice">Rebuild and Reset</label>
						</div>
					</div>	
					
					
				  <!----- Device Naming Convention ----->	
				 <div class="col-md-12">
            			<div class="form-group">
            				<h2 class="adminUserText">Device Naming Convention</h2>
            				 
            			</div>
            		</div>
                <!--DID User1-->
                
                <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">DID User - Criteria #1:</label><br/>
                			 	<input type="text" name="deviceNameDID1" id="deviceNameDID1" size="50" <?php echo "value=\"" . $deviceNameDID1 . "\""; ?> onchange="updateChangeStatus(this,'toggleDeviceConfig')" oninput="updateChangeStatus(this,'toggleDeviceConfig')">
						</div>
					</div>
					
				 <!--DID User2-->	
					
					 <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">DID User - Criteria #2:</label><br/>
                			 	<input type="text" name="deviceNameDID2" id="deviceNameDID2" size="50" <?php echo "value=\"" . $deviceNameDID2 . "\""; ?> onchange="updateChangeStatus(this,'toggleDeviceConfig')" oninput="updateChangeStatus(this,'toggleDeviceConfig')">
						</div>
					</div>	
					
					
					
				 <!--Analog User-->
				 	 <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Analog User:</label><br/>
                			 	<input type="text" name="deviceNameAnalog" id="deviceNameAnalog" size="50" <?php echo "value=\"" . $deviceNameAnalog . "\""; ?> onchange="updateChangeStatus(this,'toggleDeviceConfig')" oninput="updateChangeStatus(this,'toggleDeviceConfig')">
						</div>
					</div>	
					
					<!--Sca Device Name-->
					<div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Sca Device Name:</label><br/>
                			 	<input type="text" name="scaDeviceName" id="scaDeviceName" size="50" <?php echo "value=\"" . $scaOnlyCriteria . "\""; ?> onchange="updateChangeStatus(this,'toggleDeviceConfig')" oninput="updateChangeStatus(this,'toggleDeviceConfig')">
						</div>
					</div>	
					
				  <!----- Lineport Naming Convention ----->	
					<div class="col-md-12">
            			<div class="form-group">
            				<h2 class="adminUserText">Lineport Naming Convention</h2>
            				 
            			</div>
            		</div>
					   <!--Criteria #1-->
					<div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Criteria #1:</label><br/>
                			 	<input type="text" name="linePortCriteria1" id="linePortCriteria1" size="50" <?php echo "value=\"" . $linePortCriteria1 . "\""; ?> onchange="updateChangeStatus(this,'toggleDeviceConfig')" oninput="updateChangeStatus(this,'toggleDeviceConfig')">
						</div>
					</div>	
                 <!--Criteria #2-->
                <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Criteria #2:</label><br/>
                			 	<input type="text" name="linePortCriteria2" id="linePortCriteria2" size="50" <?php echo "value=\"" . $linePortCriteria2 . "\""; ?> onchange="updateChangeStatus(this,'toggleDeviceConfig')" oninput="updateChangeStatus(this,'toggleDeviceConfig')">
						</div>
					</div>	
			  <!----- Shared Appearance Lineport Naming Convention ----->		
										<div class="col-md-12">
							<div class="form-group">
								<h2 class="adminUserText">Shared Appearance Lineport Naming Convention</h2>
								 
							</div>
						</div>
					   <!--Criteria #1-->	
						
					<div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Criteria #1:</label><br/>
                			 	<input type="text" name="scaLinePortCriteria1" id="scaLinePortCriteria1" size="50" <?php echo "value=\"" . $scaLinePortCriteria1 . "\""; ?> onchange="updateChangeStatus(this,'toggleDeviceConfig')" oninput="updateChangeStatus(this,'toggleDeviceConfig')">
						</div>
					</div>	
                
                 <!--Criteria #2-->
                 
                 <div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Criteria #2:</label><br/>
                			 	<input type="text" name="scaLinePortCriteria2" id="scaLinePortCriteria2" size="50" <?php echo "value=\"" . $scaLinePortCriteria2 . "\""; ?> onchange="updateChangeStatus(this,'toggleDeviceConfig')" oninput="updateChangeStatus(this,'toggleDeviceConfig')">
						</div>
					</div>
					
					
					
					 <!-- Start CounterPath -->
               
                 
               <?php if(isset($license["softPhone"]) && $license["softPhone"] == "true"){ ?>
                           
                                
                                <div class="col-md-12">
									<div class="form-group">
										<h2 class="adminUserText">Soft Phone Lineport Naming Convention</h2>
									</div>
								</div>
								
								
								<div class="col-md-6">
                        			<div class="form-group">
                        				<label class="labelText" for=" ">Criteria #1:</label>
                    					<input type="text" name="softPhoneLinePortCriteria1" id="counterPathIdCriteria1" size="45" value="<?php echo $softPhoneLinePortCriteria1; ?>" onchange="updateChangeStatus(this,'toggleDeviceConfig')" oninput="updateChangeStatus(this,'toggleDeviceConfig')">
                    				</div>
                    			</div>
                    			
                    			<div class="col-md-6">
                        			<div class="form-group">
                        				<label class="labelText" for=" ">Criteria #2:</label>
                    					<input type="text" name="softPhoneLinePortCriteria2" id="counterPathIdCriteria2" size="45" value="<?php echo $softPhoneLinePortCriteria2; ?>" onchange="updateChangeStatus(this,'toggleDeviceConfig')" oninput="updateChangeStatus(this,'toggleDeviceConfig')">
                    				</div>
                    			</div>
                                
                           
                        
                           <!-- end line/port naming convention -->
                           <?php } ?>          
               <!-- End CounterPath stretto-->
					
				<!----- Device Access Authentication ----->		
                 		<div class="col-md-12">
							<div class="form-group">
								<h2 class="adminUserText">Device Access Authentication</h2>
								 
							</div>
						</div>
					 <!--Device Access User Name Criteria #1-->	
						<div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">User Name - Criteria #1:</label><br/>
                			 	<input type="text" name="deviceAccessUserName1" id="deviceAccessUserName1" size="50" <?php echo "value=\"" . $deviceAccessUserName1 . "\""; ?> onchange="updateChangeStatus(this,'toggleDeviceConfig')" oninput="updateChangeStatus(this,'toggleDeviceConfig')">
						</div>
					</div>
					
					
					
					 <!--Device Access User Name Criteria #2-->
						<div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">User Name - Criteria #2:</label><br/>
                			 	<input type="text" name="deviceAccessUserName2" id="deviceAccessUserName2" size="50" <?php echo "value=\"" . $deviceAccessUserName2 . "\""; ?> onchange="updateChangeStatus(this,'toggleDeviceConfig')" oninput="updateChangeStatus(this,'toggleDeviceConfig')">
						</div>
					</div>
					
					<!--Device Access Password-->
					<div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Device Access Password:</label><br/>
                			 	<input type="text" name="deviceAccessPassword" id="deviceAccessPassword" size="50" <?php echo "value=\"" . $deviceAccessPassword . "\""; ?> onchange="updateChangeStatus(this,'toggleDeviceConfig')" oninput="updateChangeStatus(this,'toggleDeviceConfig')">
						</div>
					</div>
			 <!--Analog Access Authentication-->		
					<div class="col-md-6">
                			<div class="form-group">
                			 	<label class="labelText" for="">Analog Access Authentication:</label><br/>
                			 	<input type="checkbox" name="analogAccessAuthentication" id="analogAccessAuthentication" value="true" <?php echo $analogAccessAuthentication == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleDeviceConfig')">
                			 	
                			 	<label for="analogAccessAuthentication"><span></span></label>
    							<label class="labelText" for="analogAccessAuthentication">Apply Device Access Authentication to analog users</label>
    			
						</div>
					</div>
					
					
                
                 </form>
                 
                 <div class="col-md-12 form-group alignBtn sytemAdminForm">
    			 	<input type="button" name="submitDeviceConfig" class="submitAndView marginRightButton" id="submitDeviceConfig" value="Submit" onclick="submitConfig(this, 'deviceConfig', 'toggleDeviceConfig')">
    			 </div>
                    </div>
                </div>
            

 
        <?php
    }
    
    if(isset($_POST['adminConfigType']) && $_POST['adminConfigType'] == "services_config")
    {
        // get Services Configuration
        // --------------------------
        $serviceConfig = new DBSimpleReader($db, "serviceConfig");
        $userVoicePortalPasscodeType = $serviceConfig->get("userVoicePortalPasscodeType");
        $userVoicePortalPasscodeFormula = $serviceConfig->get("userVoicePortalPasscodeFormula");
        $userVoicePortalStaticPasscode = $serviceConfig->get("userVoicePortalStaticPasscode");
        //$usrAllowInternationalCalls = $serviceConfig->get("usrAllowInternationalCalls");
        $aaCriteria1 = $serviceConfig->get("aacriteria1");
        $aaCriteria2 = $serviceConfig->get("aacriteria2");
        $aaClidFname= $serviceConfig->get("aaClidFname");
        $aaClidLname= $serviceConfig->get("aaClidLname");
        $aaNameCriteria = $serviceConfig->get("aaNameCriteria");

            // get Voice Messaging Configuration
        // --------------------------
        $voiceportalconfig = new DBSimpleReader($db, "voiceportalconfig"); 
        $voicePortalID = $voiceportalconfig->get("voicePortalID");
        $voicePortalName = $voiceportalconfig->get("voicePortalName");
        $clidFirstName = $voiceportalconfig->get("clidFirstName");
        $clidLastName = $voiceportalconfig->get("clidLastName");

        // get Shared Call Appearance Configuration
        // (part of Service Configuration action)
        $scaConfig = new DBSimpleReader($db, "scaConfig");
        $scaIsActive = $scaConfig->get("scaIsActive");
        $scaAllowOrigination = $scaConfig->get("scaAllowOrigination");
        $scaAllowTermination = $scaConfig->get("scaAllowTermination");
        $scaAlertClickToDial = $scaConfig->get("scaAlertClickToDial");
        $scaAllowGroupPaging = $scaConfig->get("scaAllowGroupPaging");
        $scaAllowCallRetrieve = $scaConfig->get("scaAllowCallRetrieve");
        $scaAllowBridging = $scaConfig->get("scaAllowBridging");
        $scaCallParkNotification = $scaConfig->get("scaCallParkNotification");
        $scaMultipleCallArrangement = $scaConfig->get("scaMultipleCallArrangement");
        $scaBridgeWarningTone = $scaConfig->get("scaBridgeWarningTone");
        //echo "IN IF - ".$_POST['adminConfigType'];
        ?>
        <!---------------------------->
        <!-- Services Configuration -->
        <!---------------------------->
        <div id="divServiceConfig">
                    <div class="">
                     
            	<div class="row fcorn-registerTwo">
                <!----- User Voice Portal Passcode ----->
                <div class="col-md-12">
                        <div class="form-group">
                                <h2 class="adminUserText">User Voice Portal</h2>
                        </div>
                </div>
                </div>
                
                
                    <!-- form -- form -- form -- form -- form -- form -- form -- form -->
                    <form action="#" method="post" id="serviceConfig" name="serviceConfig" class="fcorn-registerTwo sytemAdminForm ">
                        <input type="hidden" id="tabId" value="services_config" />
                        
                        <!-- form identifier -->
                        <input type="hidden" name="serviceConfigForm" value="true">
    						
    						
    						<div class="row fcorn-registerTwo">
    						 
    							<div class="col-md-6">
    								<div class="form-group">
    									<label class="labelText" for="">Voice Portal Passcode Type:</label><br/>
    									<input type="radio" name="userVoicePortalPasscodeType" id="userVoicePortalPasscodeTypeStatic" value="Static" <?php echo $userVoicePortalPasscodeType == "Static" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
    									
    									<label for="userVoicePortalPasscodeTypeStatic"><span></span></label>
										<label class="labelText customLabelText" for="userVoicePortalPasscodeTypeStatic">Apply static passcode system-wide</label><br/>
										
										<input type="radio" name="userVoicePortalPasscodeType" id="userVoicePortalPasscodeTypeDynamic" value="Dynamic" <?php echo $userVoicePortalPasscodeType == "Dynamic" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
										
										<label for="userVoicePortalPasscodeTypeDynamic"><span></span></label>
										<label class="labelText customLabelText" for="userVoicePortalPasscodeTypeDynamic">Generate random passcode on user creation</label><br/>
										
										<input type="radio" name="userVoicePortalPasscodeType" id="userVoicePortalPasscodeTypeFormula" value="Formula" <?php echo $userVoicePortalPasscodeType == "Formula" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
										
										<label for="userVoicePortalPasscodeTypeFormula"><span></span></label>
										<label class="labelText customLabelText" for="userVoicePortalPasscodeTypeFormula">Define formula for automatic passcode generation</label>
					
    								</div>
    							</div>
    							
    						 <!--Passcode Formula-->	
    						<div id="userVoicePortalPasscodeFormulaDiv">	
        						<div class="col-md-6">
        								<div class="form-group">
        									<label class="labelText" for="">Passcode Formula:</label><br/>
        									
        									<input type="text" name="userVoicePortalPasscodeFormula" id="userVoicePortalPasscodeFormula" size="48" <?php echo "value=\"" . $userVoicePortalPasscodeFormula . "\""; ?> onchange="updateChangeStatus(this,'toggleServiceConfig')" oninput="updateChangeStatus(this,'toggleServiceConfig')">
        								</div>
        						</div>
    						</div>
    						
    						 <!--Static Passcode-->
    						<div id="userVoicePortalStaticPasscodeDiv">	
        						<div class="col-md-6">
        								<div class="form-group">
        									<label class="labelText" for="">System-wide passcode:</label><br/>
        									
        									<input type="password" name="userVoicePortalStaticPasscode" id="userVoicePortalStaticPasscode" size="48" <?php echo "value=\"" . $userVoicePortalStaticPasscode . "\""; ?> onchange="updateChangeStatus(this,'toggleServiceConfig')" oninput="updateChangeStatus(this,'toggleServiceConfig')">
        								</div>
        						</div>
    						</div>
    				<!----- Voice Portal Configuration ----->		
    						<div class="col-md-12">
							 
								<h2 class="adminUserText">Voice Portal Configuration</h2>
								 
							 
						</div>
						<div id="voiceMsgConVoicePortalIDDiv">
    						<div class="col-md-6">
    							<div class="form-group">
    								<label class="labelText" for="">Voice Portal ID:</label><br/>
    							<input type="text" name="voiceMsgConVoicePortalID" id="voiceMsgConVoicePortalID" size="48" <?php echo "value=\"".$voicePortalID. "\""; ?> onchange="updateChangeStatus(this,'toggleVoiceMsgConfig')" oninput="updateChangeStatus(this,'toggleVoiceMsgConfig')">
    							</div>
    						
    						</div>
    					</div>	
    					
    					
    			 <!--Voice Portal Name-->		
    					<div id="voiceMsgConVoicePortalNameDiv">
    						<div class="col-md-6">
    							<div class="form-group">
    								<label class="labelText" for="">Voice Portal Name:</label><br/>
    							<input type="text" name="voiceMsgConVoicePortalName" id="voiceMsgConVoicePortalName" size="48" <?php echo "value=\"".$voicePortalName . "\""; ?> onchange="updateChangeStatus(this,'toggleVoiceMsgConfig')" oninput="updateChangeStatus(this,'toggleVoiceMsgConfig')">
    							</div>
    						 </div>
    					</div>	
    					
    					
    					
    					<!--Clid First Name-->
                        <div id="voiceMsgConClidFirstNameDiv">
                        
    						<div class="col-md-6">
    							<div class="form-group">
    								<label class="labelText" for="">Clid First Name:</label><br/>
    							<input type="text" name="voiceMsgConClidFirstName" id="voiceMsgConClidFirstName" size="48" <?php echo "value=\"".$clidFirstName . "\""; ?> onchange="updateChangeStatus(this,'toggleVoiceMsgConfig')" oninput="updateChangeStatus(this,'toggleVoiceMsgConfig')">
    							</div>
    						
    						</div>
    					</div>	
    					
    					
    					<!--Clid Last Name-->
                        <div id="voiceMsgConClidLastNameDiv">                       
    						<div class="col-md-6">
    							<div class="form-group">
    								<label class="labelText" for="">Clid Last Name:</label><br/>
    							<input type="text" name="voiceMsgConClidLastName" id="voiceMsgConClidLastName" size="48" <?php echo "value=\"".$clidLastName . "\""; ?> onchange="updateChangeStatus(this,'toggleVoiceMsgConfig')" oninput="updateChangeStatus(this,'toggleVoiceMsgConfig')">
    							</div>
    						
    						</div>
    					</div>
    					
    					 <!-- Auto Attendand Configuration -->
    					<div class="col-md-12">
						 	<h2 class="adminUserText">Auto Attendant </h2>
						</div>
						
						<div class="col-md-6">
							<div class="form-group">
								<label class="labelText" for="">Criteria #1:</label><br/>
							<input type="text" name="aaCriteri1" id="criteria1" size="50" <?php echo "value=\"" . $aaCriteria1 . "\""; ?> onchange="updateChangeStatus(this,'toggleServiceConfig')" oninput="updateChangeStatus(this,'toggleServiceConfig')">
							</div>
    						
    					</div>	
					
    					<div class="col-md-6">
							<div class="form-group">
								<label class="labelText" for="">Criteria #2:</label><br/>
							<input type="text" name="aaCriteri2" id="criteria2" size="50" <?php echo "value=\"" . $aaCriteria2 . "\""; ?> onchange="updateChangeStatus(this,'toggleServiceConfig')" oninput="updateChangeStatus(this,'toggleServiceConfig')">
							</div>
    						
    					</div>	
    						
    					<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Auto Attendant Name:</label><br/>
    						<input type="text" name="aaNameCriteria" id="aaNameCriteria" size="50" <?php echo "value=\"" . $aaNameCriteria. "\""; ?> onchange="updateChangeStatus(this,'toggleServiceConfig')" oninput="updateChangeStatus(this,'toggleServiceConfig')">
    						</div>
    						
						</div>	
						
						
						<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Calling Line ID First Name:</label><br/>
    						<input type="text" name="aaClidFname" id="aaClidFname" size="50" <?php echo "value=\"" . $aaClidFname. "\""; ?> onchange="updateChangeStatus(this,'toggleServiceConfig')" oninput="updateChangeStatus(this,'toggleServiceConfig')">
    						</div>
    						
						</div>
						
						
						<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Calling Line ID Last Name:</label><br/>
    						<input type="text" name="aaClidLname" id="aaClidLname" size="50" <?php echo "value=\"" . $aaClidLname. "\""; ?> onchange="updateChangeStatus(this,'toggleServiceConfig')" oninput="updateChangeStatus(this,'toggleServiceConfig')">
    						</div>
    						
						</div>
						
						  <!----- User Outgoing Calling Plan ----->

                        <!--Allow International Calls-->
                        
						<!--<div class="col-md-12">
							 
								<h2 class="adminUserText">User Outgoing Calling Plan</h2>
								 
							 
						</div>	
							
							
						<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Allow International Calls:</label><br/>
    							<input type="checkbox" name="usrAllowInternationalCalls" id="usrAllowInternationalCalls" value="true" <?php //echo $usrAllowInternationalCalls == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
    						
    							<label for="usrAllowInternationalCalls"><span></span></label>
								<label class="labelText" for="usrAllowInternationalCalls">Allow users to make international calls</label>
    						</div>
						
						</div>-->
					  <!----- User Shared Call Appearances ----->	
						<div class="col-md-12">
							 
								<h2 class="adminUserText">User Shared Call Appearances</h2>
								 
							 
						</div>
						
						 <!--Is Active-->
						<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Is Active:</label><br/>
    							<input  type="radio" name="scaIsActive" id="scaIsActiveTrue" value="true" <?php echo $scaIsActive == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
    						
    							<label for="scaIsActiveTrue"><span></span></label>
								<label class="labelText marginRight25" for="scaIsActiveTrue">True</label> 
								
								
								<input  type="radio" name="scaIsActive" id="scaIsActiveFalse" value="false" <?php echo $scaIsActive == "false" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
								
								<label for="scaIsActiveFalse"><span></span></label>
								<label class="labelText" for="scaIsActiveFalse">False</label><br/>
								
    						</div>
						</div>
						
					 <!--Allow Origination-->	
						<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Allow Origination:</label><br/>
    							<input style="margin-right: 4px" type="radio" name="scaAllowOrigination" id="scaAllowOriginationTrue" value="true" <?php echo $scaAllowOrigination == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
    						
    							<label for="scaAllowOriginationTrue"><span></span></label>
								<label class="labelText marginRight25" for="scaAllowOriginationTrue">True</label> 
								
								
								<input style="margin-right: 4px" type="radio" name="scaAllowOrigination" id="scaAllowOriginationFalse" value="false" <?php echo $scaAllowOrigination == "false" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
								
								<label for="scaAllowOriginationFalse"><span></span></label>
								<label class="labelText" for="scaAllowOriginationFalse">False</label><br/>
								
    						</div>
						</div>	
						
						
						
						  <!--Allow Termination-->
						<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Allow Termination:</label><br/>
    							<input type="radio" name="scaAllowTermination" id="scaAllowTerminationTrue" value="true" <?php echo $scaAllowTermination == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
    						
    							<label for="scaAllowTerminationTrue"><span></span></label>
								<label class="labelText marginRight25" for="scaAllowTerminationTrue">True</label> 
								
								
								<input style="margin-right: 4px" type="radio" name="scaAllowTermination" id="scaAllowTerminationFalse" value="false" <?php echo $scaAllowTermination == "false" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
								
								<label for="scaAllowTerminationFalse"><span></span></label>
								<label class="labelText" for="scaAllowTerminationFalse">False</label><br/>
								
    						</div>
						</div>	
						
						
						  <!--Alert Click to Dial-->
						  
						<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Alert Click to Dial:</label><br/>
    							<input style="margin-right: 4px" type="radio" name="scaAlertClickToDial" id="scaAlertClickToDialTrue" value="true" <?php echo $scaAlertClickToDial == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
    						
    							<label for="scaAlertClickToDialTrue"><span></span></label>
								<label class="labelText marginRight25" for="scaAlertClickToDialTrue">True</label> 
								
								
								<input style="margin-right: 4px" type="radio" name="scaAlertClickToDial" id="scaAlertClickToDialFalse" value="false" <?php echo $scaAlertClickToDial == "false" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
								
								<label for="scaAlertClickToDialFalse"><span></span></label>
								<label class="labelText" for="scaAlertClickToDialFalse">False</label><br/>
								
    						</div>
						</div>	
						
					<!--Allow Group Paging-->	
						<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Allow Group Paging:</label><br/>
    							<input style="margin-right: 4px" type="radio" name="scaAllowGroupPaging" id="scaAllowGroupPagingTrue" value="true" <?php echo $scaAllowGroupPaging == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
    						
    							<label for="scaAllowGroupPagingTrue"><span></span></label>
								<label class="labelText marginRight25" for="scaAllowGroupPagingTrue">True</label> 
								
								
								<input style="margin-right: 4px" type="radio" name="scaAllowGroupPaging" id="scaAllowGroupPagingFalse" value="false" <?php echo $scaAllowGroupPaging == "false" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
								
								<label for="scaAllowGroupPagingFalse"><span></span></label>
								<label class="labelText" for="scaAllowGroupPagingFalse">False</label><br/>
								
    						</div>
						</div>	
						
					<!--Allow Call Retrieve-->	
						<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Allow Call Retrieve:</label><br/>
    							<input style="margin-right: 4px" type="radio" name="scaAllowCallRetrieve" id="scaAllowCallRetrieveTrue" value="true" <?php echo $scaAllowCallRetrieve == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
    						
    							<label for="scaAllowCallRetrieveTrue"><span></span></label>
								<label class="labelText marginRight25" for="scaAllowCallRetrieveTrue">True</label> 
								
								
								<input style="margin-right: 4px" type="radio" name="scaAllowCallRetrieve" id="scaAllowCallRetrieveFalse" value="false" <?php echo $scaAllowCallRetrieve == "false" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
								
								<label for="scaAllowCallRetrieveFalse"><span></span></label>
								<label class="labelText" for="scaAllowCallRetrieveFalse">False</label><br/>
								
    						</div>
						</div>				
    							
    			  <!--Allow Bridging-->				
    					<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Allow Bridging:</label><br/>
    							<input style="margin-right: 4px" type="radio" name="scaAllowBridging" id="scaAllowBridgingTrue" value="true" <?php echo $scaAllowBridging == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
    						
    							<label for="scaAllowBridgingTrue"><span></span></label>
								<label class="labelText marginRight25" for="scaAllowBridgingTrue">True</label> 
								
								
								<input style="margin-right: 4px" type="radio" name="scaAllowBridging" id="scaAllowBridgingFalse" value="false" <?php echo $scaAllowBridging == "false" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
								
								<label for="scaAllowBridgingFalse"><span></span></label>
								<label class="labelText" for="scaAllowBridgingFalse">False</label><br/>
								
    						</div>
						</div>	
						
						
			  <!--Call Park Notification-->			
						<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Call Park Notification:</label><br/>
    							<input style="margin-right: 4px" type="radio" name="scaCallParkNotification" id="scaCallParkNotificationTrue" value="true" <?php echo $scaCallParkNotification == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
    						
    							<label for="scaCallParkNotificationTrue"><span></span></label>
								<label class="labelText marginRight25" for="scaCallParkNotificationTrue">True</label> 
								
							<input style="margin-right: 4px" type="radio" name="scaCallParkNotification" id="scaCallParkNotificationFalse" value="false" <?php echo $scaCallParkNotification == "false" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
								
								<label for="scaCallParkNotificationFalse"><span></span></label>
								<label class="labelText" for="scaCallParkNotificationFalse">False</label><br/>
								
    						</div>
						</div>	
						
				 <!--Multiple Call Arrangement-->		
						<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Multiple Call Arrangement:</label><br/>
    							<input style="margin-right: 4px" type="radio" name="scaMultipleCallArrangement" id="scaMultipleCallArrangementTrue" value="true" <?php echo $scaMultipleCallArrangement == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
    						
    							<label for="scaMultipleCallArrangementTrue"><span></span></label>
								<label class="labelText marginRight25" for="scaMultipleCallArrangementTrue">True</label> 
								
								
							<input style="margin-right: 4px" type="radio" name="scaMultipleCallArrangement" id="scaMultipleCallArrangementFalse" value="false" <?php echo $scaMultipleCallArrangement == "false" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
								
								<label for="scaMultipleCallArrangementFalse"><span></span></label>
								<label class="labelText" for="scaMultipleCallArrangementFalse">False</label><br/>
								
    						</div>
						</div>	
						
				  <!--Bridge Warning Tone-->		
						<div class="col-md-6">
    						<div class="form-group">
    							<label class="labelText" for="">Bridge Warning Tone:</label><br/>
    							<input style="margin-right: 4px" type="radio" name="scaBridgeWarningTone" id="scaBridgeWarningToneBargeIn" value="Barge-In" <?php echo $scaBridgeWarningTone == "Barge-In" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
    						
    							<label for="scaBridgeWarningToneBargeIn"><span></span></label>
								<label class="labelText marginRight25" for="scaBridgeWarningToneBargeIn">Barge-In</label> 
								
								
							<input style="margin-right: 4px" type="radio" name="scaBridgeWarningTone" id="scaBridgeWarningToneBargeInRepeat" value="Barge-In and Repeat" <?php echo $scaBridgeWarningTone == "Barge-In and Repeat" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
								
								<label for="scaBridgeWarningToneBargeInRepeat"><span></span></label>
								<label class="labelText marginRight25" for="scaBridgeWarningToneBargeInRepeat">Barge-In and Repeat</label> <br/>
								
								
								<input style="margin-right: 4px" type="radio" name="scaBridgeWarningTone" id="scaBridgeWarningToneNone" value="None" <?php echo $scaBridgeWarningTone == "None" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleServiceConfig')">
								
								<label for="scaBridgeWarningToneNone"><span></span></label>
								<label class="labelText" for="scaBridgeWarningToneNone">None</label><br/>
								
								
    						</div>
						</div>				
    		
    					</div><!-- end row -->
    				</form>
    				<div class="col-md-12 form-group alignBtn">
    			 	<input type="button" name="submitServiceConfig" class="submitAndView marginRightButton" id="submitServiceConfig" value="Submit" onclick="submitConfig(this, 'serviceConfig', 'toggleServiceConfig')">
    			 </div>
                    </div>
        </div> 
        <?php
    }
    
    if(isset($_POST['adminConfigType']) && $_POST['adminConfigType'] == "branding_config")
    {        
        $loginLogo_files = getAllLogos();        
        ?>
         <!---------------------------->
         <!-- Company Logo -->
         <!---------------------------->
         <div id="divBrandingConfig">
                    <div class="">
                    	<form action="#" method="POST" id="sysAdminLogo" name="sysAdminLogo" enctype="multipart/form-data"  class="fcorn-registerTwo">
				
    					<div class="col-md-12">
    					 	<div class="">
    					 	<div class="infoIconCls">
    					 		<label class="labelText" for="">Company Logo:</label> 
    					 <?php 	if( !empty($loginLogo_files['header']) ) { ?>
    					 	<label class="labelTextGrey"><a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="Please select a logo or upload a new one"><img src="../../Express/images/NewIcon/info_icon.png"></a></label>
    					 <?php	} else { ?>
    					 	<label class="labelTextGrey"><a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="Please choose an image below to replace the current logo"><img src="../../Express/images/NewIcon/info_icon.png"></a></label>
    					 <?php	} ?>
    					 	</div>	
    					 		<?php
    							     //$logo_files = glob("/var/www/logos/*.{jpg,png}", GLOB_BRACE);
    					 		if( !empty($loginLogo_files['header']) ) {
    						      ?>								  
    					 				  <br/>
    					 				<table>
    									<?php
										$x=0; // ex-1048 
										 
										foreach ($loginLogo_files['header'] as $logo_file) {
    										$logo_filename = pathinfo($logo_file['filename'], PATHINFO_BASENAME);
											 $x ++;
    										?>
    										<tr>
    											<td>
    											<input type="radio"
    												   name="set_logo"
    												   id="set_logo<?php echo $x ; ?>"
    												   class="set_logo"
    												   value="<?php echo $logo_filename; ?>"
    												   <?php echo $logo_file['selected'] == "true" ? "CHECKED" : "" ?>><label for="set_logo<?php echo $x ; ?>"><span></span></label>
    											</td>
    											<td><img style="max-height: 100px;max-width: 200px;" src="/logos/<?php echo $logo_filename; ?>"/></td>
    											<!-- <td style="padding: 10px;vertical-align: middle;">-->
    											<td>
    												<?php
    												if($logo_file['selected'] == "false") {
    													?>
    													<div class="form-group sytemAdminRmBtn">
        			 										<button type="button" class="deleteBtn marginRightButton removeLogoBtn" data-filename="<?php echo $logo_filename; ?>">Remove</button>
        			 									</div>    													
    													<?php
    												}
    												?>
    											</td>
    										</tr>
    										<?php
    									}
    									?>
    								</table>
    								<br/>
    								<?php
    							} else {
    								?>
    								<!-- <label class="labelTextGrey"><a href="#" style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="Please choose an image below to replace the current logo"><img src="../../Express/images/NewIcon/info_icon.png"></a></label>--> 
    								<?php
    							}
    							?>
    							 
    					 	</div>
    					
    					 <div class="col-md-6">
    							<div style="color: #6ea0dc;">
    								<div> <input type="file" class="inputfile" name="logo" id="logoFile" accept="image/jpeg,image/png" />
    								 <label class="BrowseLabel" for="logoFile">Browse...</label> </div>
    								<br/>
    								
    							</div>
        				</div>
        			
    				</div>
    				
    				<!-- Start Login Page Logo -->
    					<div class="col-md-12">
    					 	<div class="">
    					 	<div class="infoIconCls">
    					 		<label class="labelText" for="">Login Page Logo:</label>
								<?php
								//$loginLogo_files = glob("/var/www/logos/loginLogo/*.{jpg,png}", GLOB_BRACE);
								if ( !empty($loginLogo_files['loginPage']) ) {
								?>	
										<label class="labelTextGrey"><a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="Please select a logo or upload a new one"><img src="../../Express/images/NewIcon/info_icon.png"></a></label>
								<?php } else { ?>
										<label class="labelTextGrey"><a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="Please choose an image below to replace the current logo"><img src="../../Express/images/NewIcon/info_icon.png"></a></label> 
								<?php } ?>
							</div>
							<br/>
                            <div class="">
                            	<label class="labelText" for="">Login Page Branding:</label><br>
                            	<input type="checkbox" name="customerLogoOnLoginPage" id="customerLogoOnLoginPage" value="true" <?php echo $customerLogoOnLoginPage == "true" ? "checked" : ""; ?> onclick="updateChangeStatus(this,'toggleDeviceConfig')">
                            	<label for="customerLogoOnLoginPage"><span></span></label>
                            	<label class="labelText" for="customerLogoOnLoginPage">Include Customer Logo on Login Page</label>
                            </div>
    					 		<?php
    					 		if( !empty($loginLogo_files['loginPage']) ) {
    						      ?>
    					 				  <br/>
    					 				<table>
    									<?php
										$y=0; // ex-1048 
										foreach ($loginLogo_files['loginPage'] as $loginLogo_file) {
										    $loginLogo_filename = pathinfo($loginLogo_file['filename'], PATHINFO_BASENAME);
										    $y ++;
    										?>
    										<tr>
    											<td>
    											<input type="radio"
    												   name="set_loginLogo"
    												   id="set_loginLogo<?php echo $y ; ?>"
    												   class="set_loginLogo <?php echo $loginLogo_file['selected'] == "true" ? 'active' : '' ?>"
    												   value="<?php echo $loginLogo_filename; ?>"
    												   <?php echo $loginLogo_file['selected'] == "true" ? "CHECKED" : "" ?>><label for="set_loginLogo<?php echo $y ; ?>"><span></span></label>
    											</td>
    											<?php 
    											$imageDetail = scallLogo($width = 300, $height = 130, $filename = "/var/www/logos/loginPageLogo/$loginLogo_filename");
//     											$imageDetail = getimagesize("/var/www/logos/loginPageLogo/$loginLogo_filename");
    											$imgWidth = $imageDetail['imgWidth'];
    											$imgHeight = $imageDetail['imgHeight'];
    											?>
    											<td><img style="max-height: <?php echo $imgHeight; ?>px;max-width: <?php echo $imgWidth?>px;" src="/logos/loginPageLogo/<?php echo $loginLogo_filename; ?>"/></td>
    											<!-- <img style="max-height: 100px;max-width: 200px;" src="/logos/loginPageLogo/<?php echo $loginLogo_filename; ?>"/></td> -->
    											<td>
    												<?php
    												if( $loginLogo_file['selected'] == "false" ) {
    													?>
    													<div class="form-group sytemAdminRmBtn">
        			 										<button type="button" class="deleteBtn marginRightButton removeLoginLogoBtn" data-filename="<?php echo $loginLogo_filename; ?>">Remove</button>
        			 									</div>
    													<?php
    												}
    												?>
    											</td>
    										</tr>
    										<?php
    									}
    									?>
    								</table>
    								<br/>
    								<?php
    							}
    							?> 
    					 	</div>
    					
    					 <div class="col-md-6">
    							<div style="color: #6ea0dc;">
    								<div> <input type="file" class="inputfile" name="loginLogo" id="loginLogoFile" accept="image/jpeg,image/png" />
    								 <label class="BrowseLabel" for="loginLogoFile">Browse...</label> </div>
    								<br/>
    							</div>
        				</div>
    				</div>
    				
        			
        			<div class="col-md-12 form-group alignBtn">
    			 		<input type="button" name="changeLogo" class="submitAndView marginRightButton" id="changeLogo" value="Submit">
    			 	</div>
				</form>
              </div>
          </div>
        <?php
    }
 ?>
         <script>
             initAdmin();
         </script>
         
         <link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v15">
<script src="/Express/js/bootstrap.min.js"></script>
<script src="/Express/js/jquery-1.17.0.validate.js"></script>
<script> 
  function showAdminConfigData(divId)
  {
        var divID = "#"+divId;        
        $.ajax({
              type: "POST",
              url: "ajax/showSystemAdminData.php",
              data: {adminConfigType : divId},
              async: true,
              beforeSend: function() {
                  $(divID).show();
                  $(divID).html("<div id='loding' style='text-align:center; font-weight: 700; color: #9c9c9c; border: 0px solid red; width: auto;text-align:center'>&nbsp;Please wait, data is loading <br /><br /><img src='/Express/images/ajax-loader.gif'></div>");
              },
              success: function (result) {
                    $(divID).html("");
                    
                    var mainChangesDiv =  $("#"+divId).closest('div.uiModalAccordian').attr('id');
                    var noChangesText = $("#"+mainChangesDiv).find('h4:first').find('a:first').text();
                    noChangesText = noChangesText.replace("(Not Saved)", "");
                    $("#"+mainChangesDiv).find('h4:first').find('a:first').text(noChangesText);
                    $("#"+mainChangesDiv).find('h4:first').find('a:first').removeClass("changesCollapsedConfigColor");
                    $(divID).html(result);
                  //  FormChangesPrevent(divId);
                    
                    $("#servers_config").hide();
                    $("#system_config").hide();
                    $("#group_config").hide();
                    $("#pass_rule_config").hide();
                    $("#user_config").hide();
                    $("#devices_config").hide();
                    $("#services_config").hide();
                    $("#branding_config").hide(); 
                    $(".accordion-toggle").addClass("collapse");
                    $(".accordion-toggle").removeClass("in");
                    $(divID).show();
                    
                    
                    checkLogoutClick = "";
                    checkChangePass = "";
                    var checkTrueVal  =  $(".accordion-toggle").hasClass('changesCollapsedConfigColor');
                    if(checkTrueVal){
                       changesSytemAdmin = true ;
                   }else{
                      changesSytemAdmin = false ; 
                   }
                    
                    
              }
        });
  }
  
  
  /* function FormChangesPrevent(divId){
        //alert('divId');
        dataId =  $("#"+divId).closest('div.uiModalAccordian').attr('id');
        formDataId  = $("#"+dataId).find("form").attr('id');
        oldTextName = $("#"+dataId).find('h4:first').find('a:first').text();
        form_data = $('#'+formDataId).serialize();
        $("#"+formDataId).on('change','input, select', function(){
            newform_data = $('#'+formDataId).serialize();
            if ( form_data == newform_data ) {
                changesSytemAdmin = false;
                $("#"+dataId).find('h4:first').find('a:first').text(oldTextName);
                $("#"+dataId).find('h4:first').find('a:first').removeClass("changesCollapsedConfigColor");    
            } else {
                changeTextName = $("#"+dataId).find('h4:first').find('a:first').text();
                
                if($("#"+dataId).find('h4:first').find('a:first').text().indexOf("Not Saved") == -1){
                    $("#"+dataId).find('h4:first').find('a:first').text(changeTextName+"(Not Saved)"); 
                }
                
                $("#"+dataId).find('h4:first').find('a:first').addClass("changesCollapsedConfigColor");
                changesSytemAdmin = true;
            }

        });
    }  */

<?php 
//if(isset($_POST['adminConfigType']) && $_POST['adminConfigType'] == "servers_config") { ?>
/*$().ready(function () {
	getBwReleaseDetails();
});*/
<?php //} ?>
    
    
    
    $().ready(function () {

        $(".uiModalAccordian").show();
        $(".rivisionDiv").show();
        $("#configurationIsLoading").hide();
        
           if($('#smsAuthentication').is(':checked')){
            $("#smsCredential").show();
           } else if ($('#smsNoAuthentication').is(':checked')){
            $("#smsCredential").hide();
           }
        });

    var erroInForm = false;
     var saveChangesCount = false;
    $(function() {
        
 $("#addCluster").click(function(){
	 var numItems = $('.clusterServer').length + 1;
	        if (numItems > 4) { return; }
	    	else{
				$.ajax({
	    			type: "POST",
	    			url: "/Express/sysAdmin/template/serverConfigClusterTemplate.php",
	    			data: { clusterNumber: numItems },
	    			success: function(result) {
	    				$("#allClusterDiv").append(result);
	    			}
	    		});
	        }
		buttonsBehaviour();	
	});
	
		$("#smServer").change(function()
		{
			var dataToSend = $("#sysAdmin").serializeArray();
			dataToSend.push({ name: "sm", value: "true" });
			$.ajax({
				type: "POST",
				url: "sysAdminInfo.php",
				data: dataToSend,
                                async: false,
				success: function(result) {
					$("#smData").html(result);
				}
			});
		});


		$("#changeServers").click(function(event)
		{
			var dataToSend = $("#sysAdmin").serialize();
			$.ajax({
				type: "POST",
				url: "sysAdminDo.php",
				data: dataToSend,
				success: function(result) {
					alert(result);
					window.location.reload();
				}
			});
		});
        $('#smsAuthentication').change(function () {

             $("#smsCredential").show();

            });

        $('#smsNoAuthentication').change(function () {

            $("#smsCredential").hide();
            });

        $("#changeLogo").click(function (event) {

            if($("#logoFile").val() || $("input.set_logo:radio:checked").length > 0) {

                var dataToSend = new FormData($("#sysAdminLogo")[0]);
                $.ajax({
                    type: "POST",
                    url: "sysAdminLogoDo.php",
                    data: dataToSend,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        alert(result);
                        window.location.reload();
                    }
                });

            } else {
				alert("Please choose a logo");
            }

        });

        $(".removeLogoBtn, .removeLoginLogoBtn").click(function () {
			var logoType = this.className.indexOf('removeLoginLogoBtn') != -1 ? "loginPage" : "header";
            var filename = $(this).data('filename');

            if(confirm("Are you sure you want to remove the logo?")) {

                $.ajax({
                    type: "GET",
                    url: "sysAdminLogoDo.php?remove_file=" + filename + "&logoType=" + logoType,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        alert(result);
                        window.location.reload();
                    }
                });
		    } else {
                return false;
            }
	    });

       $("input.set_loginLogo").click(function() {	
           var isActive = $(this).hasClass('active');
           $("input.set_loginLogo").removeClass('active');
           
           if( !isActive) {
        	   $(this).addClass('active');
           } else {
        	   $(this).prop('checked', false);
            }
        });
      
		var changePermissionRecords = function(){

			 $.ajax({
                 type: "POST",
                 url: "sysAdminUserPermission.php",
                 async: false,
                 success: function(result) {
                 	console.log('Success');
                 }
          });
	          
		}

        $("#dialogSecurityDomainConfiguration").dialog({
    		autoOpen: false,
    		width: 800,
    		modal: true,
    		position: { my: "top", at: "top" },
    		resizable: false,
    		closeOnEscape: false,
    		buttons: {
    			"Cancel": function() {
    				$(this).dialog("close"); 
    			},
    			"Ok" : function(){
    				$("#confirmSecurityDomainSubmit").val('1'); 
 	    			$(this).dialog("close"); 
    				changePermissionRecords(); 
    				 setTimeout(function () {
 	    				$("#submitSystemConfig").trigger("click");
                     }, 1000);
    			},
    		},
            open: function() {
				setDialogDayNightMode($(this));
                $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
                $(".ui-dialog-buttonpane button:contains('Ok')").button().show().addClass('subButton');
            }
    	});

	    $("#submitSystemConfig").click(function(){
		   var newSecurityDomainPatternVal = $("#securityDomainPattern").val(); 
		   var oldSecurityDomainPatternVal = $("#hidSecurityDomainPattern").val(); 

		   if(newSecurityDomainPatternVal != oldSecurityDomainPatternVal){
			   	$("#dialogMU").html('Creating the group request');		
		   }
                   
		});
                
                
                


	});

    // Handler for opening configuration section
    // This handler manages multiple sections to show/display configuration sections
    // button: button which was toggled and which toggles directional arrows
    // section: section to toggle show/hide content
    // -----------------------------------------------------------------------------
    function toggleSection(button, section) {

        button.value = button.value == 'â–¼' ? 'â–²' : 'â–¼';

        var div = document.getElementById(section);
        div.style.display = div.style.display == "none" ? "block" : "none";
    }


    // Handler for submitting configuration section
    // This handler manages multiple sections to submit data for configuration DB update
    // button: submit button (not used)
    // formId: form with data controls that will be serialized and submitted by Ajax
    // toggleButtonId: arrow in toggle button will change from red to green
    // -----------------------------------------------------------------------------
    
    function enableClusterChange(noOfCluster) {
		
		  var checkBox = document.getElementById("enableClusters");
		  var clustersLength = $('.clusterServer').length;
		  if (checkBox.checked == false){
			  if( clustersLength > 1 ) {
				  var confirmAction = confirm("Disabling cluster support. First cluster settings will be used.");
				  if ( confirmAction != true ) {
					checkBox.checked = true;
				  }
				  //text.style.display = "block";			  
				}
		  }
    }

    function checkClusterNameRules() {
        
    var tempArry = [];
    erroInForm = false;
    $(".clusterName").each(function() {
		$thisVal = $(this).val();
		if($thisVal == "") {
			alert("Cluster Name is required field");
			erroInForm = true;
			return;
		} else {
				if(tempArry.indexOf($thisVal) == -1) {
					tempArry.push($thisVal);
				} else {
					alert("Cluster Names must be unique");
					erroInForm = true;
					return;
				}
		}
		});
		return erroInForm;
	}


function checkOCIReleaseValidation () {
	OCIIsUnique = true;	
	var serverConnected = "";
	var priServerConnected = null;
	var secServerConnected = null;
	$(".clusterServer").each(function() {
		if( $(this).find('.connectedPrimary').length > 0 ) {
			priServerConnected = $(this).find('.OCIReleasePrimary :selected').text();
		}
		if( $(this).find('.connectedSecondary').length > 0) {
			secServerConnected = $(this).find('.OCIReleaseSecondary :selected').text(); 
		}

		priServerConnected = ( secServerConnected != null ) ? secServerConnected : priServerConnected;
		if( priServerConnected != null) {
			return false;
		}
	});

	if( priServerConnected != null ) {
		$(".clusterServer").each(function() {
			var pRelease = $(this).find('.OCIReleasePrimary :selected').text();
			var sRelease = $(this).find('.OCIReleaseSecondary :selected').text();
			var pIP = $(this).find('.priIpAddress').val();
			var sIP = $(this).find('.secIpAddress').val();
			if( pIP != "" && ( pRelease != priServerConnected ) ) {
				OCIIsUnique = false;
				return false;
			}
			if( sIP != "" && (sRelease != priServerConnected) ) {
				OCIIsUnique = false;
				return false;
			}
			
		});
	}
		
	return OCIIsUnique;
}

    function submitConfig(button, formId, toggleButtonId) {
    	erroInForm = false;
    	saveChangesCount = false;
        
        /*if( getChangesSerializeData(formId) ) {
            return false;
    	}*/
        
        if( checkClusterNameRules() ) {
			return false;
    	}
		if( ! checkOCIReleaseValidation() ) {
			alert("OCI Release must be identical for all connected BW Servers.");
			return false;
		}
		
		if(formId == "systemConfig"){ 
		 var success = true;
		 /* validation for password expiration max length and min lenght  
		  * Super User Expiration time 1day and maximum time 1000day by default 30 days
		  * Regular User Expiration time  Expiration time 1day and maximum time 1000day by default 90 days
		 */
            var passwordExpiration_SU = document.getElementById("passwordExpiration_SU").value; 
            var passwordExpiration_RU = document.getElementById("passwordExpiration_RU").value; 
	        if((passwordExpiration_SU < 1) || (passwordExpiration_SU >1000)){
        		  alert("Super User Expiration minimum time should be atleast '1 day' and maximum time '1000 days'.");
                  $(this).focus();
                  success = false;
                  return false;
        
        	} 
 
        	if((passwordExpiration_RU < 1 )|| (passwordExpiration_RU >1000)){
        		alert("Regular User Expiration minimum time should be atleast '1 day' and maximum time '1000 days'.");
                $(this).focus();
                success = false;
                return false;
			} 
 		/* end */
			var newSecurityDomainPatternVal = document.getElementById("securityDomainPattern").value; 
  			var oldSecurityDomainPatternVal = document.getElementById("hidSecurityDomainPattern").value; 
  			var confirmSecurityDomainSubmitVal = document.getElementById("confirmSecurityDomainSubmit").value; 

  			if(newSecurityDomainPatternVal != oldSecurityDomainPatternVal && confirmSecurityDomainSubmitVal == 0){

				if(newSecurityDomainPatternVal == "" && oldSecurityDomainPatternVal != ""){
	  		 		$("#dialogSecurityDomainConfiguration").html('Warning: All existing non Super User admins must be configured to set valid Service Provider and Groups');
				}else if(newSecurityDomainPatternVal != "" && oldSecurityDomainPatternVal == ""){
					$("#dialogSecurityDomainConfiguration").html('Warning: All existing non Super User admins must be configured to set valid Security Domain');
				}else if(newSecurityDomainPatternVal != "" && oldSecurityDomainPatternVal != newSecurityDomainPatternVal){
					$("#dialogSecurityDomainConfiguration").html('Warning: All existing non Super User admins must be configured to set valid Security Domain');
				}
	  		 	
	  		 	$("#dialogSecurityDomainConfiguration").dialog("open");
	  		 	return false;
  			}

            if ( $('#smsAuthentication').is(':checked') && ($("#smsToken").val() === "" || $("#smsSID").val() === "" || $("#smsFrom").val() === "") ) {
                alert("Please enter Token, SID and the From phone number.");
                return false;
            } else if ($('#ldapAuthenticationTrue').is(':checked') && $("#ldapHost").val() === "") {
                alert("Please enter the LDAP host.");
                $("#ldapHost").focus();
                return false;
            }

			<?php
			if(!$billingCACert) {
			?>
            if ($('#billingForceSSL').is(':checked') && !$('#billingCACert').val()) {
                alert("You need to select a CA Cert to force SSL.");
                $("#billingCACert").focus();
                return false;
            }
			<?php
			}
			?>
                                    
                    //Code added @ 13 July 2018
                    var passwordReusePrevention = document.getElementById("passwordReusePrevention").value;
                    if(document.getElementById("passwordReusePrevention").value!="")
                    {
                        var errorMsg = false;
                        var passwordReusePrevention = document.getElementById("passwordReusePrevention").value; 
                         if((passwordReusePrevention < 0) || (passwordReusePrevention >20))
                         {
                               alert("Password Reuse Prevention's value must be between 0 to 20.");
                               //document.getElementById("errorSpanForPRPvton").html("");
                               $("#errorSpanForPRPvton").html("Password Reuse Prevention's value must be between 0 to 20.");
                               $(this).focus();
                               return false;
                         }
                    }                    
                    if(document.getElementById("passwordReusePrevention").value=="")
                    {
                        alert("Password Reuse Prevention's value can't be empty.");                        
                        $("#errorSpanForPRPvton").html("Password Reuse Prevention's value can't be empty.");
                        $(this).focus();
                        return false;
                    }
                    //End Code
                    
                  
                  /*validation for bw license warning and alert */
                    var warningsThreshold = document.getElementById("warningsThreshold").value;
                     var alertsThreshold = document.getElementById("alertsThreshold").value;
                    if(warningsThreshold !="" || alertsThreshold !="")
                    {
                        var errorMsg = false;
                        
                        if(!($.isNumeric($("#warningsThreshold").val())) || ((warningsThreshold < 0) || (warningsThreshold >100)))
                         {
                            alert("BroadWorks Licensing Warnings value only allow numeric number and must be between 1 to 100 ");
                            //document.getElementById("errorSpanForPRPvton").html("");
                               $("#errorwarningThreshold").html("BroadWorks Licensing Warnings value only allow numeric number and must be between 1 to 100");
                               $(this).focus();
                               return false;
                            
                         }
                         
                          if(!($.isNumeric($("#alertsThreshold").val())) || ((alertsThreshold < 0) || (alertsThreshold >100)))
                         {
                            alert("BroadWorks Licensing Alerts value only allow numeric number and must be between 1 to 100 ");
                           //document.getElementById("errorSpanForPRPvton").html("");
                           $("#erroralertsThreshold").html("BroadWorks Licensing Alerts value only allow numeric number must be between 1 to 100 ");
                           $(this).focus();
                           return false;
                        }
                         
                    }
                 /*end */
               }

	    if(formId === "passwordRulesConfig"){

		    var success = true;

            $(".passwordRulesMinimumLength").each(function () {
                if($(this).val() < 8 || $(this).val() > 40) {
                    alert("Password Rules: Minimum Password Length should be between '8' and '40'.");
                    $(this).focus();
                    success = false;
                    return false;
                }
            });

            if(success) {

                $(".passwordRulesNumberOfDigits").each(function () {
                    if ($(this).val() < 1 || $(this).val() > 5) {
                        alert("Password Rules: Number of Digits should be between '1' and '5.");
                        $(this).focus();
                        success = false;
                        return false;
                    }
                });
            }

            if(success) {

	            $(".passwordTypes").each(function () {

					var passwordType = $(this).val();

	                var minimumLength = parseInt($("#MinimumLength_" + passwordType).val());
	                var numberOfDigits = parseInt($("#NumberOfDigits_" + passwordType).val());
	                var specialCharacters = $("#SpecialCharacters_" + passwordType).prop('checked') ? 1 : 0;
	                var upperCaseLetter = $("#UpperCaseLetter_" + passwordType).prop('checked') ? 1 : 0;
	                var lowerCaseletter = $("#LowerCaseLetter_" + passwordType).prop('checked') ? 1 : 0;

	                var totalLength = numberOfDigits + specialCharacters + upperCaseLetter + lowerCaseletter;

	                if(minimumLength < totalLength) {
	                    alert("Password Rules: Minimum Length cannot be less than " + totalLength  + ", the minimum number of digits required plus count of every checked rule.");
	                    $("#MinimumLength_" + passwordType).focus();
	                    success = false;
	                    return false;
	                }
	            });

            }


            // toggle button to green - just an indication that changes have been submitted
        //    var toggleButton = document.getElementById(toggleButtonId);
        //    toggleButton.style.color = 'green';

            if(!success) {
                return false;
            }

        }

        // submit configuration changes in Ajax call
        var dataToSend = $("#" + formId ).serialize();
        //alert(dataToSend);


        // Get form
        //Code added @ 19 July 2019
        adminConfigType = formId;
        var form = $('#' + formId)[0];
        var tabId = $(form).find('#tabId').val();
        //alert('Tab Id - '+ tabId);
        // Create an FormData object
        var data = new FormData(form);
            $.ajax({
	            type: "POST",
	            enctype: 'multipart/form-data',
	            url: "sysAdminPoliciesDo.php",
	            data: data,
	            processData: false,
	            contentType: false,
	            async: true,
	            success: function (result) {
	                alert(result);
	                //window.location.reload();  //Code commented @ 19 July 2019
                        showAdminConfigData(tabId); //Code added @ 19 July 2019  
                        //get the top offset of the target anchor
                        var target_offset = $("#"+tabId).offset();
                        var target_top = target_offset.top;
                        //goto that anchor by setting the body scroll top to anchor top
                        $('html, body').animate({scrollTop:target_top}, 1500, 'easeInSine');
	            }
	    });

        // toggle button to green - just an indication that changes have been submitted
        var toggleButton = document.getElementById(toggleButtonId);
        if (typeof(toggleButton) != 'undefined' && toggleButton != null){
            toggleButton.style.color = '#72ac5d';
        }

    }
    function getRadioGroupValue(name) {
        var radioGroup = document.getElementsByName(name);
        for (var i = 0; i < radioGroup.length; i++) {
            if (radioGroup[i].checked) {
                return radioGroup[i].value;
            }
        }
        return "";
    }

    function initAdmin() {
        //alert('Test');
        // Show/hide Static domain based on domain type setting
        // ----------------------------------------------------
        var staticDomainDiv                   = document.getElementById("staticDomainDiv");
        var staticBwPortalPasswordDiv         = document.getElementById("staticBwPortalPasswordDiv");
        var staticProxyDomainDiv              = document.getElementById("staticProxyDomainDiv");
        var surgeMailUserPasswordDiv          = document.getElementById("surgeMailUserPasswordDiv");
        var surgeMailUserPasswordDiv          = document.getElementById("surgeMailUserPasswordDiv");
        var userVoicePortalPasscodeFormulaDiv = document.getElementById("userVoicePortalPasscodeFormulaDiv");
        var userVoicePortalStaticPasscodeDiv  = document.getElementById("userVoicePortalStaticPasscodeDiv");
        
        if (typeof(staticDomainDiv) != 'undefined' && staticDomainDiv != null){
            document.getElementById("staticDomainDiv").style.display =
            (getRadioGroupValue("groupsDomainType") == "Static") ? "block" : "none";
        }

        // Show/hide Static BW portal password based on BW portal password type setting
        // ----------------------------------------------------------------------------
        if (typeof(staticBwPortalPasswordDiv) != 'undefined' && staticBwPortalPasswordDiv != null){
                 document.getElementById("staticBwPortalPasswordDiv").style.display =
                (getRadioGroupValue("bwPortalPasswordType") == "Static") ? "block" : "none";
        }

        // Show/hide Static proxy domain based on domain type setting
        // ----------------------------------------------------------
        if (typeof(staticProxyDomainDiv) != 'undefined' && staticProxyDomainDiv != null){
                document.getElementById("staticProxyDomainDiv").style.display =
                (getRadioGroupValue("proxyDomainType") == "Static") ? "block" : "none";
        }

        // Show/hide SurgeMail password based on SurgeMail password type setting
        // ---------------------------------------------------------------------
        if (typeof(surgeMailUserPasswordDiv) != 'undefined' && surgeMailUserPasswordDiv != null){
            document.getElementById("surgeMailUserPasswordDiv").style.display =
                (getRadioGroupValue("surgeMailUserPasswordType") == "Static") ? "block" : "none";
        }

        // Show/hide passcode fields based on Voice Portal Passcode type setting
        // ---------------------------------------------------------------------
        if (typeof(userVoicePortalPasscodeFormulaDiv) != 'undefined' && userVoicePortalPasscodeFormulaDiv != null){
        document.getElementById("userVoicePortalPasscodeFormulaDiv").style.display = (getRadioGroupValue("userVoicePortalPasscodeType") == "Formula") ? "block" : "none";
        }
        if (typeof(userVoicePortalStaticPasscodeDiv) != 'undefined' && userVoicePortalStaticPasscodeDiv != null){
        document.getElementById("userVoicePortalStaticPasscodeDiv").style.display =  (getRadioGroupValue("userVoicePortalPasscodeType") == "Static") ? "block" : "none";
        }
      }


    // Change indication handlers
    // If anything changes in the form, the directional arrow changes to red
    // -----------------------------------------------------------------------------
    function updateChangeStatus(control, toggleButtonId) {
     //   var toggleButton = document.getElementById(toggleButtonId);
     //   toggleButton.style.color = 'red';

        // Show/hide Static Domain based on domain type selection
        if (control.name == "groupsDomainType" && typeof document.getElementById("staticDomainDiv") !== 'undefined') {
            document.getElementById("staticDomainDiv").style.display = (control.value == "Static") ? "block" : "none";
        }

        // Show/hide Static BW portal password based on BW portal password type selection
        if (control.name == "bwPortalPasswordType" && typeof document.getElementById("staticBwPortalPasswordDiv") !== 'undefined') {
            document.getElementById("staticBwPortalPasswordDiv").style.display = (control.value == "Static") ? "block" : "none";
        }

        // Show/hide Static Proxy Domain based on Proxy domain type selection
        if (control.name == "proxyDomainType" && typeof document.getElementById("staticProxyDomainDiv") !== 'undefined') {
            document.getElementById("staticProxyDomainDiv").style.display = (control.value == "Static") ? "block" : "none";
        }

        // Show/hide Surgemail password based on password type
        if (control.name == "surgeMailUserPasswordType" && typeof document.getElementById("surgeMailUserPasswordDiv") !== 'undefined') {
            document.getElementById("surgeMailUserPasswordDiv").style.display = (control.value == "Static") ? "block" : "none";
        }

        // Show/hide User Voice Portal passcode fields based on passcode type
        if (control.name == "userVoicePortalPasscodeType" && typeof document.getElementById("userVoicePortalPasscodeFormulaDiv") !== 'undefined') {
            document.getElementById("userVoicePortalPasscodeFormulaDiv").style.display = (control.value == "Formula") ? "block" : "none";
            document.getElementById("userVoicePortalStaticPasscodeDiv").style.display  = (control.value == "Static")  ? "block" : "none";
        }
    }

	function deleteCluster(clusterId , clusterIdValue, clustName) {
		
		var action = confirm("Express Administrators associated with the cluster being removed will also be removed. Proceed ?");
                
		if (action == true) {
			if(clusterIdValue != "::") {
				$.ajax({
					type: "POST",
					url: "ajax/deleteCluster.php",
					data: { serverConfigClusterId: "", clusterRowId: clusterIdValue,delUsersCluster:clustName},
					success: function(result) {
						$("#"+clusterId).remove();
						buttonsBehaviour();
					}
				});
			} else {
				$("#"+clusterId).remove();
				buttonsBehaviour();
			}
		}
	}
	
	function buttonsBehaviour() {
		var clustersLength = $('.clusterServer').length;
		if(clustersLength > 3 ) {
				$("#addCluster").prop("disabled", true);
				$(".deleteButton").prop("disabled", false);
		}
		else if( clustersLength == 1) {
			$("#addCluster").prop("disabled", false);
			$(".deleteButton").prop("disabled", true);
		} 
	}

	function getBwReleaseDetails() {
		var serversInfo = [];
		$(".singleServerDiv").each(function() {
				var  tempVar = {};
				var ipAddress = $(this).find(".ipAddress").val();
				var userName = $(this).find(".userName").val();
				var password = $(this).find(".oldPassword").val();
				var protocol = $(this).find(".protocol").val();
				var port = $(this).find(".port").val();
				tempVar = {ipAddress: ipAddress, userName: userName, password: password, protocol: protocol, port: port}
				serversInfo.push(tempVar);			
		});

		$.ajax({
			type: "POST",
			url: "ajax/getBwReleaseDetail.php",
			data: { serverDetails: serversInfo },
			success: function(result) {
				var resultTemp = $.parseJSON(result);
				$(".singleServerDiv").each(function() {
					var ipAddress = $(this).find(".ipAddress").val();
					$(this).find(".bwRelease").html(resultTemp[ipAddress]['bwrelease']);
				});
			}
		});

	}
	
</script>
<style>
    .changesCollapsedConfigColor{
        color : red !important;
    }
       .uiModalAccordian, .rivisionDiv {
            display: none;
        }
    .diaPL {
         margin-left: 143px !important;
    }
    .panel {
            background-color: initial !important;
    }
    .cluster{width:125px !important;}
    .clusterFirstTab { width:165px !important;}
    .clusterLastTab { width:60px !important;}
    .clusterSurgemail{width:125px !important;}
    .clusterCounterpath{width:125px !important;}
	.marginLeft {
		margin-left:4%;
	}
	.clusterName {
	   width: 82% !important;
	}
	
        .setContentMargin{padding-top: 48px !important;}
</style>
 <script>
/* $('#logoFile').filestyle({
    buttonText : ' Choose a file from System',
    buttonName : 'btn-info'
   }); */

/*changes start ../ */
var  systemConfigType = '<?php echo $_POST['adminConfigType']?>';
var collapseClick =  $("#"+systemConfigType).closest('div.uiModalAccordian').attr('id');
var collapseClickformDataId  = $("#"+collapseClick).find("form").attr('id');
var oldTextName = $("#"+collapseClick).find('h4:first').find('a:first').text();        
var formSerializeRecored = $('#'+collapseClickformDataId).serialize();
objOldFormData.push({"formId":collapseClickformDataId, "headingId":collapseClick,"tabText":oldTextName, "form_data":formSerializeRecored});
//localStorage.setItem("test",JSON.stringify(obj));

/* function check if system admin form data change */
$('input, select').change(function(){
    var mainCollapsedDiv    =   $(this).closest('div.uiModalAccordian').attr('id');
    var formDataId          =   $("#"+mainCollapsedDiv).find("form").attr('id');
    unSaveChnagesSystemConfig(formDataId,mainCollapsedDiv);
});
    function unSaveChnagesSystemConfig(formId,mainCollapsedDiv){
        var checkOldDataChange = [];
        checkOldDataChange  = JSON.parse(JSON.stringify(objOldFormData));
        $.each(checkOldDataChange, function (i, field) {  
            if(field.formId == formId){
                var onChangeFormData = $('#'+formId).serialize();
                if(field.form_data == onChangeFormData){
                    $("#"+mainCollapsedDiv).find('h4:first').find('a:first').text(field.tabText);                
                    $("#"+mainCollapsedDiv).find('h4:first').find('a:first').removeClass("changesCollapsedConfigColor"); 
                }else{
                    $("#"+mainCollapsedDiv).find('h4:first').find('a:first').text(field.tabText+"(Not Saved)");
                    $("#"+mainCollapsedDiv).find('h4:first').find('a:first').addClass("changesCollapsedConfigColor");
                }
            }  
        });
    }
/*end changes ../ */
	$(".ldapAuthentication").click(function() {
		if($("#ldapAuthenticationTrue").is(':checked')) {
			$("#ldapHostDiv").show();
		    $("#ldapHost").prop("disabled", false);
		    $("#ldapDomainDiv").show();
		    $("#ldapDomain").prop("disabled", false);
		} else {
            $("#ldapHostDiv").hide();
            $("#ldapHost").prop("disabled", true);
            $("#ldapDomainDiv").hide();
            $("#ldapDomain").prop("disabled", true);
		}
	});
        
        function updateBwPort(htpPrtcl, updateField)
        {
            document.getElementById(updateField).value = "";
            if(htpPrtcl=="https")
                document.getElementById(updateField).value = "443";
            if(htpPrtcl=="http")
                document.getElementById(updateField).value = "80";
        }
        
        function updateSurgeMailPort(htpPrtcl, updateField)
        {
            document.getElementById(updateField).value = "";
            if(htpPrtcl=="https")
                document.getElementById(updateField).value = "7025";
            if(htpPrtcl=="http")
                document.getElementById(updateField).value = "7026";
        }

        function updateCounterPathPort(htpPrtcl, updateField)
        {
            document.getElementById(updateField).value = "";
            if(htpPrtcl=="https")
                document.getElementById(updateField).value = "443";
            if(htpPrtcl=="http")
                document.getElementById(updateField).value = "80";
        }

    $().ready(function () {
        $("#newAdminForm").validate();
        $("#create_new_admin_div").dialog($.extend({}, defaults, {
            width: 900,
            buttons: [
	                {
	                    text: "Create Admin",
		                class: "subButton",
		                id: "createNewAdminFormSubmitBtn",
		                click: function () {
		                    if($("#createNewAdminForm").valid()) {

			                    $("#createAdminStatus").hide();
                                $("#createNewAdminFormSubmitBtn").prop('disabled', true);
                                $("#createNewAdminFormSubmitBtn").html("Please Wait ...");
			                    var formData = $("#createNewAdminForm").serialize();
			                    $.ajax({
			                        type: "POST",
			                        url: "createAdmin.php",
			                        data: formData,
			                        dataType:'json',
			                        success: function (result) {
			                            if(result.success) {
			                                $("#createAdminStatus").html('<div class="alignCenter">'  + result.msg + '</div>');
			                                $("#createAdminStatus").show();
			                                $("#createNewAdminForm").hide();
                                            $("#createNewAdminFormSubmitBtn").hide();
			                            } else {

                                            $("#createNewAdminFormSubmitBtn").prop('disabled', false);
                                            $("#createNewAdminFormSubmitBtn").html("Create Admin");
			                                $("#createAdminStatus").html('<div class="alignCenter">'  + result.msg + '</div>');
			                                $("#createAdminStatus").show();
			                            }
			                        }
			                    });
		                    }
		                }
	                },
		            {
                        text: "Close",
                        class: "cancelButton",
                        click: function () {
	                        $(this).dialog("close");
	                    }
		            }
                ]
        }));

        $("#createNewAdminBtn").click(function () {
            $("#createAdminStatus").html("");
            $("#createNewAdminForm").show();
            $("#createNewAdminForm")[0].reset();
            $("#createNewAdminFormSubmitBtn").prop('disabled', false);
            $("#createNewAdminFormSubmitBtn").html("Create Admin");
            $("#createNewAdminFormSubmitBtn").show();
            $("#create_new_admin_div").dialog("open");
            $("#create_new_admin_div").show();
        });

        $("#billingForceSSL").click(function() {
            if($("#billingForceSSL").is(':checked')) {
                $("#billingCACertDiv").show();
            } else {
                $("#billingCACertDiv").hide();
            }
        });
    });
</script>
<style>
.ui-dialog-titlebar-close{
    display:none !important;
}
.error, .error text {
            color: red;
	}
	/*.widthAlign{
	width:86%;
	margin-left:10px;
	} */
	.widthPass{
	width:70px;
	margin-left:10px;
	}
	.panel-heading {
    padding: 10px 15px;
    /* border-bottom: 1px solid transparent; */
    border-radius: 4px;
    border: 0px !important;
}

.removeLogoBtn{margin-left: -25px;}
</style>


         