<?php
/**
 * Created by Pankaj.
 * Date: 07/02/19
 * Time: 04:58 PM
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
checkLogin();

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");
require_once("/var/www/lib/broadsoft/adminPortal/ServerConfiguration.php");

if( isset($_POST['serverConfigClusterId']) && $_POST['clusterRowId'] != "::") {
    $clusterRowId = explode("::", $_POST['clusterRowId']);
    $clusterPrimaryRowId = $clusterRowId[0];
    $clusterSecondaryRowId = $clusterRowId[1];
    
    $serverConfig = new ServerConfiguration($db, appsServer, "");
    $getClusterInfo = $serverConfig->deleteCluster($clusterPrimaryRowId, $clusterSecondaryRowId, $_POST['delUsersCluster']);
    
    //code add for delete cluster from table users and adminChangeGroup.
    deleteClusterNameTblUsers($db,$_POST['delUsersCluster']);
    echo "Success";
}

function deleteClusterNameTblUsers($db,$clusterDel){
    
    $enterPriseUser = '3';
    $groupUser      = '0';
    $delete_Cluster_user = $db->prepare('DELETE u, p FROM users as u INNER JOIN permissions as p ON u.id=p.userId WHERE u.clusterName = ? AND (u.superUser = ? OR u.superUser = ? )');
    $delete_Cluster_user->execute(array($clusterDel, $enterPriseUser, $groupUser));
    
    $delete_System_Device_Cluster = $db->prepare('DELETE FROM `systemDevices`  WHERE `clusterName` = ?');
    $delete_System_Device_Cluster->execute(array($clusterDel));
    
    $delete_Ent_Device_Cluster = $db->prepare('DELETE FROM `enterpriseDevices`  WHERE `clusterName` = ?');
    $delete_Ent_Device_Cluster->execute(array($clusterDel));
    
    $newUpdateClusterName = NULL;
    $oldCluserName        = $clusterDel;
    $update_last_sel_cluster_user = $db->prepare('UPDATE users SET cluster_name_selected = ? WHERE cluster_name_selected = ? ');
    $update_last_sel_cluster_user->execute(array($newUpdateClusterName, $oldCluserName));
}

?>