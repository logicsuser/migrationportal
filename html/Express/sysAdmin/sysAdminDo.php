<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

	require_once("/var/www/html/Express/config.php");
	checkLogin();

    function getDomainForSurgeMail() {
        global $surgeMailIdCriteria1, $surgeMailIdCriteria2, $surgemailDomain;

        if (isset($surgeMailIdCriteria1) && $surgeMailIdCriteria1 != "") {
        	$criteria = $surgeMailIdCriteria1;
		}
		elseif (isset($surgeMailIdCriteria2) && $surgeMailIdCriteria2 != "") {
        	$criteria = $surgeMailIdCriteria2;
		}
		else {
        	$criteria = "";
		}

        $smDomain = explode("@", $criteria);

        if (! isset($smDomain[1]) || $smDomain[1] == "") {
            return "";
        }

        switch (strtolower($smDomain[1])) {
            case "<systemdefaultdomain>":   return $_SESSION["systemDefaultDomain"];
            case "<defaultdomain>":         return $_SESSION["defaultDomain"];
            case "<surgemaildomain>":       return $surgemailDomain;
            default:                        return "";
        }
    }

	//BroadSoft server changes
	if ($_POST["bsServer"] !== "")
	{
		$bsQuery = "update config set active='0' where type='BS'";
		$bsResults = $db->query($bsQuery);

		if ($_POST["bsServer"] == "newServer")
		{
			//add new server to database and set as default
            $bsQuery = "INSERT into config (type, ip, label, username, password, version, voicePortalPasswordType, voicePortalPassword, webPortalPasswordType, webPortalPassword, domainType, broadSoftDomain, broadWorksUserId, deviceName, linePortName, active)";
            $bsQuery .= " VALUES ('BS', '" . $_POST["bsIp"] . "', '" . $_POST["bsLabel"] . "', '" . $_POST["bsUsername"] . "', '" . $_POST["bsPassword"] . "', '" . $_POST["bsVersion"] . "', '" . $_POST["bsVoicePortalPasswordType"] . "', '" . $_POST["bsVoicePortalPassword"] . "', '" . $_POST["bsWebPortalPasswordType"] . "', '" . $_POST["bsWebPortalPassword"] . "', '" . $_POST["bsDomainType"] . "', '" . $_POST["bsDomain"] . "', '" . $_POST["bsBroadWorksUserId"] . "', '" . $_POST["bsDeviceName"] . "', '" . $_POST["bsLinePortName"] . "', '1')";
		}
		else
		{
			//update existing server info and set as default
            $bsQuery = "UPDATE config set ip='" . $_POST["bsIp"] . "', username='" . $_POST["bsUsername"] . "', password='" . $_POST["bsPassword"] . "', version='" . $_POST["bsVersion"] . "', voicePortalPasswordType='" . $_POST["bsVoicePortalPasswordType"] . "', voicePortalPassword='" . $_POST["bsVoicePortalPassword"] . "', webPortalPasswordType='" . $_POST["bsWebPortalPasswordType"] . "', webPortalPassword='" . $_POST["bsWebPortalPassword"] . "', domainType='" . $_POST["bsDomainType"] . "', broadSoftDomain='" . $_POST["bsDomain"] . "', broadWorksUserId='" . $_POST["bsBroadWorksUserId"] . "', deviceName='" . $_POST["bsDeviceName"] . "', linePortName='" . $_POST["bsLinePortName"] . "', label='" . $_POST["bsLabel"] . "', active='1'";
			$bsQuery .= " where id='" . $_POST["bsServer"] . "' and type='BS'";
		}
		$bsResults = $db->query($bsQuery);
	}


	//SurgeMail server changes
	if ($_POST["smServer"] !== "")
	{
		$smQuery = "UPDATE config set active='0' where type='SM'";
		$smResults = $db->query($smQuery);

		if ($_POST["smServer"] == "newServer")
		{
			//add new server to database and set as default
            $smQuery = "INSERT into config (type, ip, label, username, password, active)";
            $smQuery .= " VALUES ('SM', '" . $_POST["smIp"] . "', '" . $_POST["smLabel"] . "', '" . $_POST["smUsername"] . "', '" . $_POST["smPassword"] . "', '1')";
/*
 * smIp
 * smUsername   // server username
 * smPassword   // server password
 * smLabel
 * smSurgeMailUser
 */
        }
		else
		{
			//update existing server info and set as default
            $smQuery = "UPDATE config set ip='" . $_POST["smIp"] . "', username='" . $_POST["smUsername"] . "', password='" . $_POST["smPassword"] . "', label='" . $_POST["smLabel"] . "', active='1'";
			$smQuery .= " where id='" . $_POST["smServer"] . "' and type='SM'";
		}
		$smResults = $db->query($smQuery);

		//SurgeMail test
		if (isset($_POST["smSurgeMailUser"]) and $_POST["smSurgeMailUser"] !== "")
		{
			$emailAddress = $_POST["smSurgeMailUser"] . "@" . getDomainForSurgeMail();
            $emailPassword = setEmailPassword();
//echo "Email:" . $emailAddress . " Password:" . "$emailPassword";
             
            $httpProtocol = $policiesSupportSurgeMailDebug == "true" ? "http://" : "https://";
			$surgemailPost = array ("cmd" => "cmd_user_login",
						"lcmd" => "user_delete",
						"show" => "simple_msg.xml",
						"username" => $_POST["smUsername"],
						"password" => $_POST["smPassword"],
						"lusername" => $emailAddress,
						"lpassword" => $emailPassword,
						"uid" => isset($userid) ? $userid : "");
            $result = http_post_fields($httpProtocol . $_POST["smIp"] . "/cgi/user.cgi", $surgemailPost);

			$surgemailPost = array ("cmd" => "cmd_user_login",
						"lcmd" => "user_create",
						"show" => "simple_msg.xml",
						"username" => $_POST["smUsername"],
						"password" => $_POST["smPassword"],
						"lusername" => $emailAddress,
						"lpassword" => $emailPassword,
						"uid" => isset($userid) ? $userid : "");
            $result = http_post_fields($httpProtocol . $_POST["smIp"] . "/cgi/user.cgi", $surgemailPost);
		}
	}

	echo "Servers have been changed.";
?>
