<?php
/**
 * Created by Dipin Krishna.
 * Date: 6/7/18
 * Time: 11:51 AM
 */
?>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/config.php");
if (!isset($_SESSION['INITIAL_SETUP'])) {
	checkLogin();
}
require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");

$required_fields = array("userName", "firstName", "lastName", "emailAddress", "password", "adminType");
$errors = array();
foreach ($required_fields as $required_field) {
	if (!isset($_POST[$required_field])) {
		$errors[] = $required_field;
	}
}

if (count($errors)) {
	echo json_encode(array("success" => false, "error_fields" => $errors, "msg" => "Please fill in all required fields."));
	exit;
}

$stmt = $db->prepare('SELECT userName, emailAddress from users where userName = ? limit 1');
if ($stmt->execute(
	array(
		$_POST['userName']
	)
)) {

	if($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
		if(strtolower($result['userName']) == strtolower($_POST['userName'])) {
			echo json_encode(array("success" => false, "msg" => "An user with user name {$_POST['userName']} already exist."));
		}
		/* else if(strtolower($result['emailAddress']) == strtolower($_POST['emailAddress'])) {
			echo json_encode(array("success" => false, "msg" => "An user with email address {$_POST['emailAddress']} already exist."));
		}*/
		exit;
	}

	error_log("Success");
	$stmt = $db->prepare('INSERT into users (userName, firstName, lastName, password, emailAddress, superUser, disabled, lastPasswordChange) VALUES (?,?,?,?,?,?,0,NOW())');
	if($stmt->execute(
		array(
			$_POST['userName'],
			$_POST['firstName'],
			$_POST['lastName'],
			md5($_POST['password']),
			$_POST['emailAddress'],
			$_POST['adminType']
		)
	)) {
		if (isset($_SESSION['INITIAL_SETUP'])) {
			unset($_SESSION['INITIAL_SETUP']);
			$user = get_user_from_username($_POST['userName']);
			setLoginSession($user);
		}
		echo json_encode(array("success" => true,  "msg" => "Admin has been created successfully."));
		exit;
	}
}
echo json_encode(array("success" => false, "msg" => "An unknown error occurred"));
exit;
?>