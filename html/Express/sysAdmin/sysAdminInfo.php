<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();
?>
<script>
	$(function()
	{
		function enablePasswords()
		{
			if ($("#smSurgeMailPasswordType option:selected").text() == "Static")
			{
				$(".smSMP").css("display", "inline-block");
			}
			else
			{
				$(".smSMP").css("display", "none");
			}
		}

		$("#smSurgeMailPasswordType").change(function()
		{
			enablePasswords();
		});

		enablePasswords();
	});
</script>
<?php
	$labelQuery = "select id, label from labelLookup";
//	$passwordQuery = "select id, passwordType from passwordLookup";

	if (isset($_POST["sm"]) and $_POST["sm"] == "true" and $_POST["smServer"] !== "")
	{
			if ($_POST["smServer"] !== "newServer")
			{
				$smQuery = "select * from config where id='" . $_POST["smServer"] . "'";
				$smResults = $db->query($smQuery);
				$smRow = $smResults->fetch();

				$smIp = $smRow["ip"];
				$smUsername = $smRow["username"];
				$smPassword = $smRow["password"];
//				$smDomain = $smRow["domain"];
//				$smSurgeMailPasswordType = $smRow["surgeMailPasswordType"];
//				$smSurgeMailPassword = $smRow["surgeMailPassword"];
				$smLabel = $smRow["label"];
			}
		?>
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="labelText" for="userNameUpdate">IP:</label>
					<input type="text" name="smIp" id="smIp" value="<?php echo isset($smIp) ? $smIp : ""; ?>" size="20" />
				</div>
			</div>
		
			<div class="col-md-6">
				<div class="form-group">
					<label class="labelText" for="userNameUpdate">Username:</label>
					<input type="text" name="smUsername" id="smUsername" value="<?php echo isset($smUsername) ? $smUsername : ""; ?>" size="25" />
				</div>
			</div>
		
		</div>
		 
<!-- password -->
		 
		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="labelText" for="userNameUpdate">Password:</label>
					<input type="password" name="smPassword" id="smPassword" value="<?php echo isset($smPassword) ? $smPassword : ""; ?>" size="25" />
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="form-group">
					<label class="labelText" for="userNameUpdate">Label:</label>
					<div class="dropdown-wrap">
            			<select name="smLabel" id="smLabel">
            				<option value=""></option>
            				<?php
            					$labelResults = $db->query($labelQuery);
            					while ($labelRow = $labelResults->fetch())
            					{
            						echo "<option value=\"" . $labelRow["id"] . "\"";
            						if (isset($smLabel) and $smLabel == $labelRow["id"])
            						{
            							echo " selected";
            						}
            						echo ">" . $labelRow["label"] . "</option>";
            					}
            				?>
            			</select>
					</div>
				</div>
			</div>
		
		</div>	 
<!-- serge Mail test -->	

<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<label class="labelText" for="userNameUpdate">SurgeMail Test User:</label>
					<input type="text" name="smSurgeMailUser" id="smSurgeMailUser" size="25">
				</div>
			</div>
		
			<div class="col-md-6">
				<div class="form-group">
					 
				</div>
			</div>
		
		</div>	 
 
		<?php
	}
?>
