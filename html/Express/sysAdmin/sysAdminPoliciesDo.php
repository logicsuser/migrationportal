<?php
/**
 * Created by Karl
 * Date: 5/21/2016
 */
require_once("/var/www/html/Express/config.php");
checkLogin();

require_once("/var/www/lib/broadsoft/adminPortal/ServerConfiguration.php");
require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailOperations.php"); //Added 12 May 2018

const appsServer = "bwAppServer";
const surgeMailServer = "surgeMail";
/*counterPath */
const counterPathServer = "counterPath";
/*end counterPath */
const primaryServer = "primary";
const secondaryServer = "secondary";



if (isset($_POST["serversConfigForm"]) && $_POST["serversConfigForm"] == "true") {
	//msg="SERVERS"
	$policiesQuery = "";
      
	if( isset($_SESSION['allClusters']) && ! empty($_SESSION['allClusters']) ) {
	    updateClusterOperation($_POST);	    
	} else {
	    
	$primaryBwServerIP   = $_POST["primaryBwServerIP"];
	$primaryBwUserName   = $_POST["primaryBwUserName"];
        $primaryBwPassword   = $_POST["primaryBwPassword"];
        
        $primaryBwProtocol   = $_POST["primaryBwProtocol"];
        $primaryBwPort       = $_POST["primaryBwPort"];
        

	$secondaryBwServerIP = $_POST["secondaryBwServerIP"];
	$secondaryBwUserName = $_POST["secondaryBwUserName"];
        $secondaryBwPassword = $_POST["secondaryBwPassword"];        
        
        $secondaryBwProtocol   = $_POST["secondaryBwProtocol"];
        $secondaryBwPort       = $_POST["secondaryBwPort"];

	$bwServerChoice = $_POST["bwServerChoice"];
	//$bwRelease = $_POST["bwRelease"];
	$ociReleasePrimary = $_POST["ociReleasePrimary"];
	$ociReleaseSecondary = $_POST["ociReleaseSecondary"];
	
    	$serverConfig = new ServerConfiguration($db, appsServer, primaryServer);
    	if($primaryBwPassword == '') {
    	    $primaryBwPassword = $serverConfig->getPassword();
    	}
    	$serverConfig->updateConfig($primaryBwServerIP, $primaryBwUserName, $primaryBwPassword, $ociReleasePrimary, ($bwServerChoice == "Primary" ? "true" : "false"), $primaryBwProtocol, $primaryBwPort);
    	
    	$serverConfig = new ServerConfiguration($db, appsServer, secondaryServer);
    	if($secondaryBwPassword == '') {
    	    $secondaryBwPassword = $serverConfig->getPassword();
    	}
    	$serverConfig->updateConfig($secondaryBwServerIP, $secondaryBwUserName, $secondaryBwPassword, $ociReleaseSecondary, ($bwServerChoice == "Secondary" ? "true" : "false"), $secondaryBwProtocol, $secondaryBwPort);
    	
	
   }
	//$bwLabel = $_POST["bwLabel"];
        
        //Code added by @ 03 April 2018
       $primarySurgeMailServerIP     = $_POST["primarySurgeMailServerIP"];
	$primarySurgeMailUserName     = $_POST["primarySurgeMailUserName"];
	$primarySurgeMailPassword     = $_POST["primarySurgeMailPassword"];
        
        $primarySurgeMailBwProtocol   = $_POST["primarySurgeMailBwProtocol"];
        $primarySurgeMailBwPort       = $_POST["primarySurgeMailBwPort"];

	$secondarySurgeMailServerIP   = $_POST["secondarySurgeMailServerIP"];
	$secondarySurgeMailUserName   = $_POST["secondarySurgeMailUserName"];
	$secondarySurgeMailPassword   = $_POST["secondarySurgeMailPassword"];
        
        $secondarySurgeMailBwProtocol = $_POST["secondarySurgeMailBwProtocol"];
        $secondarySurgeMailBwPort     = $_POST["secondarySurgeMailBwPort"];
        
        $smRelease                  = "";
        $smLabel                    = $_POST["smLabel"];
        $surgeMailServerChoice      = $_POST["smServerChoice"];
        //End Code        

        ////
        /*counterPath update server  value */
        
        /*primary server update value post data */
        $primaryCoutnerPathServerIP     = $_POST["primaryCoutnerPathServerIP"];
        $primaryCoutnerPathUserName     = $_POST["primaryCoutnerPathUserName"];
        $primaryCoutnerPathPassword     = $_POST["primaryCoutnerPathPassword"];
        
        $primaryCoutnerPathBwProtocol   = $_POST["primaryCoutnerPathBwProtocol"];
        $primaryCoutnerPathBwPort       = $_POST["primaryCoutnerPathBwPort"];
       
        /*end primary server update value post data */
        
        /*primary server update value post data */
        $secondaryCoutnerPathServerIP   = $_POST["secondaryCoutnerPathServerIP"];
        $secondaryCoutnerPathUserName   = $_POST["secondaryCoutnerPathUserName"];
        $secondaryCoutnerPathPassword  = $_POST["secondaryCoutnerPathPassword"];
        
        $secondaryCoutnerPathBwProtocol = $_POST["secondaryCoutnerPathBwProtocol"];
        $secondaryCoutnerPathBwPort     = $_POST["secondaryCoutnerPathBwPort"];
        
        $smRelease                  = "";
         $smLabel                    = $_POST["smLabel"];
        $coutnerPathServerChoice      = $_POST["coutnerPathServerChoice"];
        //code comment for ex-964
       /* $counterpathGroupName = $_POST["counterpathGroupName"];
       $softPhoneLinePortCriteria1 = $_POST["softPhoneLinePortCriteria1"];
        $softPhoneLinePortCriteria2 = $_POST["softPhoneLinePortCriteria2"]; */
        
        /*end primary server update value post data */
        
        
        
        /*end counterPath server value */
        
	// get Servers configuration
	// -------------------------
   
	//Code added by @ 03 April 2018
	$surgMailServerConfig  = new ServerConfiguration($db, surgeMailServer, primaryServer);
	if($primarySurgeMailPassword == '') {
		$primarySurgeMailPassword = $surgMailServerConfig->getPassword();
	}
    $surgMailServerConfig->updateConfig($primarySurgeMailServerIP, $primarySurgeMailUserName, $primarySurgeMailPassword, $smRelease, ($surgeMailServerChoice == "Primary" ? "true" : "false"), $primarySurgeMailBwProtocol, $primarySurgeMailBwPort);

	$surgMailServerConfig  = new ServerConfiguration($db, surgeMailServer, secondaryServer);
	if($secondarySurgeMailPassword == '') {
		$secondarySurgeMailPassword = $surgMailServerConfig->getPassword();
	}
    $surgMailServerConfig->updateConfig($secondarySurgeMailServerIP, $secondarySurgeMailUserName, $secondarySurgeMailPassword, $smRelease, ($surgeMailServerChoice == "Secondary" ? "true" : "false"), $secondarySurgeMailBwProtocol, $secondarySurgeMailBwPort);
  
    //End Code
        
     
        
  /*counter path update logic code @ Sollogics Developer */
        $cPServerConfig  = new ServerConfiguration($db, counterPathServer, primaryServer);
        if($primaryCoutnerPathPassword == '') {
            $primaryCoutnerPathPassword = $cPServerConfig->getPassword();
        }
        $cPServerConfig->updateConfig($primaryCoutnerPathServerIP, $primaryCoutnerPathUserName, $primaryCoutnerPathPassword, $smRelease, ($coutnerPathServerChoice == "Primary" ? "true" : "false"), $primaryCoutnerPathBwProtocol, $primaryCoutnerPathBwPort);
        
        $secondaryCPServerConfig  = new ServerConfiguration($db, counterPathServer, secondaryServer);
        if($secondaryCoutnerPathPassword == '') {
            $secondaryCoutnerPathPassword = $secondaryCPServerConfig->getPassword();
        }
        $secondaryCPServerConfig->updateConfig($secondaryCoutnerPathServerIP, $secondaryCoutnerPathUserName, $secondaryCoutnerPathPassword, $smRelease, ($coutnerPathServerChoice == "Secondary" ? "true" : "false"), $secondaryCoutnerPathBwProtocol, $secondaryCoutnerPathBwPort);
        
        //code comment for ex-964 
        
        /*   $cpGroupNameQuery = "UPDATE systemConfig set counterpathGroupName = ? where id = 0";
         $cpGroupNameQuerystmt = $db->prepare($cpGroupNameQuery);
        $cpGroupNameQuerystmt->execute(array($counterpathGroupName));
        
        $cpCriteriaQuery = "UPDATE deviceConfig set " .
            "softPhoneLinePortCriteria1='" . $softPhoneLinePortCriteria1 . "', " .
            "softPhoneLinePortCriteria2='" . $softPhoneLinePortCriteria2 . "' " .
            "where id='0'";
        
        $db->query($cpCriteriaQuery);
        */
  
        /*end counterPath update logic code */
        $msg = "Express server configuration have been updated";
            //Code added by @ 03 April 2018 to check SurgeMail test user 
            // Code taken from sysAdminDo.php 
            if (isset($_POST["smSurgeMailUser"]) and $_POST["smSurgeMailUser"] !== "")
            {
            
                    $sugeObj = new SurgeMailOperations();
                    
                    $emailAddress = $_POST["smSurgeMailUser"] . "@" . getDomainForSurgeMail();
                    $emailPassword = setEmailPassword();
                    $emailAddress . " Password:" . "$emailPassword";     
                    
                    if($surgeMailServerChoice == "Primary"){
                        
                        $surgMailConnectedIp       = $primarySurgeMailServerIP.":".$primarySurgeMailBwPort;
                        $surgMailConnectedUsrName  = $primarySurgeMailUserName;
                        $surgMailConnectedPass     = $primarySurgeMailPassword;  
                        $httpPrtcl                 = $primarySurgeMailBwProtocol;
                    }
                    else {
                        $surgMailConnectedIp       = $secondarySurgeMailServerIP.":".$secondarySurgeMailBwPort;
                        $surgMailConnectedUsrName  = $secondarySurgeMailUserName;
                        $surgMailConnectedPass     = $secondarySurgeMailPassword; 
                        $httpPrtcl                 = $secondarySurgeMailBwProtocol;
                    }
                                        
                    $httpProtocol = $policiesSupportSurgeMailDebug == "true" ? "http://" : "$httpPrtcl://";
                    $surgemailPost = array ("cmd" => "cmd_user_login",
                                                "lcmd" => "user_delete",
                                                "show" => "simple_msg.xml",
                                                "username" => $surgMailConnectedUsrName,
                                                "password" => $surgMailConnectedPass,
                                                "lusername" => $emailAddress,
                                                "lpassword" => $emailPassword,
                                                "uid" => isset($userid) ? $userid : "");
                    //$result = http_post_fields($httpProtocol . $surgMailConnectedIp . "/cgi/user.cgi", $surgemailPost);
                    $surgeResult = $sugeObj->surgeMailAccountOperations($httpProtocol . $surgMailConnectedIp . "/cgi/user.cgi", $surgemailPost);
                    
                    $surgemailPost = array ("cmd" => "cmd_user_login",
                                                "lcmd" => "user_create",
                                                "show" => "simple_msg.xml",
                                                "username" => $surgMailConnectedUsrName,
                                                "password" => $surgMailConnectedPass,
                                                "lusername" => $emailAddress,
                                                "lpassword" => $emailPassword,
                                                "uid" => isset($userid) ? $userid : "");
                    //$resultCreateUser = http_post_fields($httpProtocol . $surgMailConnectedIp . "/cgi/user.cgi", $surgemailPost);
                    $createResponse = $sugeObj->surgeMailAccountOperations($httpProtocol . $surgMailConnectedIp . "/cgi/user.cgi", $surgemailPost);
                    
                    if($createResponse=="200")
                        $msg = "Express server configuration have been updated and User created successfully";
                    else
                        $msg = "Express server configuration have been updated but User could not be created";
               }               
        echo $msg;
        exit;
        //End Code
        
} elseif (isset($_POST["passwordRulesConfigForm"]) && $_POST["passwordRulesConfigForm"] == "true")
{

	foreach ($_POST['passwordTypes'] as $passwordType) {

		$passwordRulesMinimumLength = ($_POST["passwordRules"][$passwordType]["MinimumLength"] && $_POST["passwordRules"][$passwordType]["MinimumLength"] >= 8) ? $_POST["passwordRules"][$passwordType]["MinimumLength"] : 8;
		$passwordRulesNumberOfDigits = ($_POST["passwordRules"][$passwordType]["NumberOfDigits"] && $_POST["passwordRules"][$passwordType]["NumberOfDigits"] >= 1) ? $_POST["passwordRules"][$passwordType]["NumberOfDigits"] : 1;
		$passwordRulesSpecialCharacters = isset($_POST["passwordRules"][$passwordType]["SpecialCharacters"]) && $_POST["passwordRules"][$passwordType]["SpecialCharacters"] == "true" ? "true" : "false";
		$passwordRulesUpperCaseLetter = isset($_POST["passwordRules"][$passwordType]["UpperCaseLetter"]) && $_POST["passwordRules"][$passwordType]["UpperCaseLetter"] == "true" ? "true" : "false";
		$passwordRulesLowerCaseLetter = isset($_POST["passwordRules"][$passwordType]["LowerCaseLetter"]) && $_POST["passwordRules"][$passwordType]["LowerCaseLetter"] == "true" ? "true" : "false";

		require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/PasswordRulesConfiguration.php");
		$passwordRulesConfiguration = new PasswordRulesConfiguration($passwordType);
		$passwordRulesConfiguration->setMinimumLength($passwordRulesMinimumLength);
		$passwordRulesConfiguration->setNumberOfDigits($passwordRulesNumberOfDigits);
		$passwordRulesConfiguration->setSpecialCharacters($passwordRulesSpecialCharacters);
		$passwordRulesConfiguration->setUpperCaseLetter($passwordRulesUpperCaseLetter);
		$passwordRulesConfiguration->setLowerCaseLetter($passwordRulesLowerCaseLetter);
		$passwordRulesConfiguration->updateConfig();
	}

} elseif (isset($_POST["systemConfigForm"]) && $_POST["systemConfigForm"] == "true")
{
	//$msg="SYSTEM";
	$groupsDomainType = $_POST["groupsDomainType"];
	$staticDomain = $_POST["staticDomain"];
	$proxyDomainType = $_POST["proxyDomainType"];
	$staticProxyDomain = $_POST["staticProxyDomain"];
	$securityDomainPattern = $_POST["securityDomainPattern"];
	$bwPortalPasswordType = $_POST["bwPortalPasswordType"];
	$bwPortalForcePasswordChange = isset($_POST["bwPortalForcePasswordChange"]) && $_POST["bwPortalForcePasswordChange"] == "true" ? "true" : "false";
	$viewCDR = isset($_POST["viewCDR"]) && $_POST["viewCDR"] == "true" ? "true" : "false";
	$useDepartments = isset($_POST["useDepartments"]) && $_POST["useDepartments"] == "true" ? "true" : "false";
	$bwAuthentication = isset($_POST["bwAuthentication"]) && $_POST["bwAuthentication"] == "true" ? "true" : "false";
	$surgeMailDebug = isset($_POST["surgeMailDebug"]) && $_POST["surgeMailDebug"] == "true" ? "true" : "false";
	$deleteVMOnUserDel = isset($_POST["deleteVMOnUserDel"]) && $_POST["deleteVMOnUserDel"] == "true" ? "true" : "false";
	$enableContinuePreviousSession = isset($_POST["enableContinuePreviousSession"]) && $_POST["enableContinuePreviousSession"] == "true" ? "true" : "false";

	$surgeMailIdCriteria1 = $_POST["surgeMailIdCriteria1"];
	$surgeMailIdCriteria2 = $_POST["surgeMailIdCriteria2"];
	$surgeMailDomain = $_POST["surgeMailDomain"];
	$surgeMailUserPasswordType = $_POST["surgeMailUserPasswordType"];
	$supportEmail = $_POST["supportEmail"];
	$billingHost = $_POST["billingHost"];
	$billingDB = $_POST["billingDB"];
	$billingUser = $_POST["billingUser"];
	$userLimitGroup = $_POST["userLimitGroup"];
	$smsAuthentication = $_POST["smsAuthentication"];
	$ldapAuthentication = $_POST["ldapAuthentication"];
	$ldapDomain = $_POST["ldapDomain"];
	$ldapCapitalization = isset($_POST["ldapCapitalization"]) && $_POST["ldapCapitalization"] == "true" ? "true" : "false";
	$ldapHost = $_POST["ldapHost"];
	$smsToken = $_POST["smsToken"];
	$smsSID = $_POST["smsSID"];
	$smsFrom = $_POST["smsFrom"];
	$inactivityTimeout = $_POST["inactivityTimeout"] ? $_POST["inactivityTimeout"] * 60 : 900;
	$advancedUserFilters = isset($_POST["advancedUserFilters"]) && $_POST["advancedUserFilters"] == "true" ? "true" : "false";
	$useExtensionLengthRange = isset($_POST["useExtensionLengthRange"]) && $_POST["useExtensionLengthRange"] == "true" ? "true" : "false";
	//$sshUsername= $_POST["sshUsername"];
	//$sshPort= $_POST["sshPort"];
	$isAdminPasswordFieldVisible = $_POST["isAdminPasswordFieldVisible"];
 
	$maxFailedLoginAttempts = $_POST["maxFailedLoginAttempts"];
	/* password expiration super user */
	$passwordExpiration_SU =  $_POST["passwordExpiration_SU"];;
	$passwordExpiration_RU =  $_POST["passwordExpiration_RU"];;
	/*end password expiration  */	
    $passwordReusePrevention = $_POST["passwordReusePrevention"];  //Code added @ 13 July 2018
	$postBillingForceSSL = isset($_POST["billingForceSSL"]) && $_POST["billingForceSSL"] == "true" ? "true" : "false";
	
	//echo "<pre>"; print_r($_POST);die;
	//post email report configuration
	$newUsrAdminReport = isset($_POST["newUsrAdminReport"]) && $_POST["newUsrAdminReport"] == "true" ? "true" : "false";
	$newUsrSupervisorReport = isset($_POST["newUsrSupervisorReport"]) && $_POST["newUsrSupervisorReport"] == "true" ? "true" : "false";
	$newUsrSupervisorEmail = $_POST["newUsrSupervisorEmail"];
	$enableContinuePreviousSession = isset($_POST["enableContinuePreviousSession"]) && $_POST["enableContinuePreviousSession"] == "true" ? "true" : "false";
	
	$counterpathGroupName = $_POST["counterpathGroupName"];
	/*xsp private id */
	//$xspPrimaryServerIP = $_POST["xspPrimaryServerIP"];
	//$xspSecondaryServerIP = $_POST["xspSecondaryServerIP"];
	
    /* BW Licensing thresholds */    
        $warningsThreshold  = $_POST["warningsThreshold"];
	$alertsThreshold    = $_POST["alertsThreshold"];
        
    /* end BW Licensing thresholds  */    
	//$xspServerChoice = $_POST["xspServerChoice"];
	$enableClusters = isset($_POST["enableClusters"]) && $_POST["enableClusters"] == "true" ? "true" : "false";
	// update system configuration attributes in 'systemConfig' table
	$policiesQuery = "UPDATE systemConfig set "
		. "groupsDomainType = ?, "
		. "staticDomain = ?, "
		. "proxyDomainType = ?, "
		. "staticProxyDomain = ?, "
		. "securityDomainPattern = ?, "
		. "bwPortalPasswordType = ?, "
		. "bwPortalForcePasswordChange = ?, "
		. "viewCDR = ?, "
		. "useDepartments = ?, "
		. "bwAuthentication = ?, "
		. "surgeMailDebug = ?, "
		. "surgeMailIdCriteria1 = ?, "
		. "surgeMailIdCriteria2 = ?, "
		. "surgeMailDomain = ?, "
		. "surgeMailUserPasswordType = ?, "
		. "deleteVMOnUserDel = ?, "
		. "supportEmail = ?, "
		. "billingHost = ?, "
		. "billingDB = ?, "
		. "billingUser = ?, "
		. "userLimitGroup = ?, "
		. "smsAuthentication = ?, "
		. "ldapAuthentication = ?, "
		. "ldapDomain = ?, "
		. "ldapCapitalization = ?, "		
		. "ldapHost = ?, "
		. "smsToken = ?, "
		. "smsSID = ?, "
		. "smsFrom = ?, "
		. "inactivityTimeout = ?, "
		. "advancedUserFilters = ?, "
		. "useExtensionLengthRange = ?, "
		/*. "sshUsername = ?, "
		. "sshPort = ?, "*/
		. "isAdminPasswordFieldVisible = ?, "
		. "maxFailedLoginAttempts = ?, "
        . "passwordExpiration_superUser = ?, "
		. "passwordExpiration_regularUser = ?, "
        . "passwordReusePrevention = ?, "
        . "newUsrAdminReport = ?, "
        . "newUsrSupervisorReport = ?, "
        . "newUsrSupervisorEmail = ? , "
        . "enableContinuePreviousSession = ? ,"
            //counterpath group Name 
	       . "counterpathGroupName = ? ,"
            //xsp private server
        /*. "xspPrimaryServerIP = ? , "
        . "xspSecondaryServerIP = ? , "
        . "xspActive = ? , "*/
        . "enableClusters = ? ,"
        . "warningsThreshold = ? ,"
        . "alertsThreshold = ? "
        . "where id = 0";

	$policiesQueryValues = array($groupsDomainType,
		$staticDomain,
		$proxyDomainType,
		$staticProxyDomain,
		$securityDomainPattern,
		$bwPortalPasswordType,
		$bwPortalForcePasswordChange,
		$viewCDR,
		$useDepartments,
		$bwAuthentication,
		$surgeMailDebug,
		$surgeMailIdCriteria1,
		$surgeMailIdCriteria2,
		$surgeMailDomain,
		$surgeMailUserPasswordType,
		$deleteVMOnUserDel,
		$supportEmail,
		$billingHost,
		$billingDB,
		$billingUser,
		$userLimitGroup,
		$smsAuthentication,
		$ldapAuthentication,
	    $ldapDomain,
	    $ldapCapitalization,
		$ldapHost,
		$smsToken,
		$smsSID,
		$smsFrom,
		$inactivityTimeout,
		$advancedUserFilters,
	    $useExtensionLengthRange,
		/*$sshUsername,
		$sshPort,*/
		$isAdminPasswordFieldVisible,
		$maxFailedLoginAttempts,
        $passwordExpiration_SU,
	    $passwordExpiration_RU,
        $passwordReusePrevention,
		$newUsrAdminReport,
		$newUsrSupervisorReport,
		$newUsrSupervisorEmail, 
		$enableContinuePreviousSession,
	    $counterpathGroupName,	    
	    $enableClusters,
	    $warningsThreshold,
            $alertsThreshold
	)
		;
		
	$policiesQueryStmt = $db->prepare($policiesQuery);	
	$policiesQueryStmt->execute($policiesQueryValues);

    //Encrypt Surge Mail Password if passed in
    $updateSurgeMailUserPassword = false;
    if(isset($_POST["surgeMailUserPassword"]) && $_POST["surgeMailUserPassword"]) {
	    $updateSurgeMailUserPassword = true;
    } else if(!isset($surgeMailUserPasswordSalt) || !$surgeMailUserPasswordSalt) {
	    $_POST["surgeMailUserPassword"] = $smSurgeMailPassword;
	    $updateSurgeMailUserPassword = true;
    }
    if($updateSurgeMailUserPassword) {
	    $surgeMailUserPasswordSalt = sha1(uniqid(bin2hex(openssl_random_pseudo_bytes(16)), true));
	    $surgeMailUserPassword = encryptString($_POST["surgeMailUserPassword"], "SurgeMailPassword_" . $surgeMailUserPasswordSalt);

	    $surgeMailUserPasswordQuery = "UPDATE systemConfig set surgeMailUserPassword = ?, surgeMailUserPasswordSalt = ? where id = 0";
	    $surgeMailUserPasswordStmt = $db->prepare($surgeMailUserPasswordQuery);
	    $surgeMailUserPasswordStmt->execute(array($surgeMailUserPassword, $surgeMailUserPasswordSalt));
    }

    //Encrypt Billing Password if passed in
    $updateBillingPassword = false;
    if(isset($_POST["billingPassword"]) && $_POST["billingPassword"]) {
	    $updateBillingPassword = true;
    } else if(!isset($billingPasswordSalt) || !$billingPasswordSalt) {
	    $_POST["billingPassword"] = $billingPassword;
	    $updateBillingPassword = true;
    }
    if($updateBillingPassword) {
	    $billingPasswordSalt = sha1(uniqid(bin2hex(openssl_random_pseudo_bytes(16)), true));
	    $billingPassword = encryptString($_POST["billingPassword"], "BillingPassword_" . $billingPasswordSalt);

	    $billingPasswordQuery = "UPDATE systemConfig set billingPassword = ?, billingPasswordSalt = ? where id = 0";
	    $billingPasswordStmt = $db->prepare($billingPasswordQuery);
	    $billingPasswordStmt->execute(array($billingPassword, $billingPasswordSalt));
    }

    //Encrypt SSH Password if passed in
	/*$updateSSHPassword = false;
    if(isset($_POST["sshPassword"]) && $_POST["sshPassword"]) {
	    $updateSSHPassword = true;
    } else if(!isset($sshPasswordSalt) || !$sshPasswordSalt) {
	    $_POST["sshPassword"] = $sshPassword;
	    $updateSSHPassword = true;
    }
    if($updateSSHPassword) {
        $sshPasswordSalt = sha1(uniqid(bin2hex(openssl_random_pseudo_bytes(16)), true));
	    $sshPassword = encryptString($_POST["sshPassword"], "SSHPassword_" . $sshPasswordSalt);

        $sshPasswordQuery = "UPDATE systemConfig set sshPassword = ?, sshPasswordSalt = ? where id = 0";
        $sshPasswordStmt = $db->prepare($sshPasswordQuery);
        $sshPasswordStmt->execute(array($sshPassword, $sshPasswordSalt));
    }*/

	//Encrypt Static BW Portal Login Password if passed in
	$updateStaticBwPortalPassword = false;
	if(isset($_POST["staticBwPortalPassword"]) && $_POST["staticBwPortalPassword"]) {
		$updateStaticBwPortalPassword = true;
	} else if(!isset($staticBwPortalPasswordSalt) || !$staticBwPortalPasswordSalt) {
		$_POST["staticBwPortalPassword"] = $bwWebPortalPassword;
		$updateStaticBwPortalPassword = true;
	}
	if($updateStaticBwPortalPassword) {
		$staticBwPortalPasswordSalt = sha1(uniqid(bin2hex(openssl_random_pseudo_bytes(16)), true));
		$staticBwPortalPassword = encryptString($_POST["staticBwPortalPassword"], "StaticBwPortalPassword_" . $staticBwPortalPasswordSalt);

		$staticBwPortalPasswordQuery = "UPDATE systemConfig set staticBwPortalPassword = ?, staticBwPortalPasswordSalt = ? where id = 0";
		$staticBwPortalPasswordStmt = $db->prepare($staticBwPortalPasswordQuery);
		$staticBwPortalPasswordStmt->execute(array($staticBwPortalPassword, $staticBwPortalPasswordSalt));
	}

	//Force Billing SSL
	if ($postBillingForceSSL == "true" && isset($_FILES["billingCACert"]) && isset($_FILES["billingCACert"]["name"]) && strlen($_FILES["billingCACert"]["name"]) > 0) {

		if (!file_exists($uploads_directory)) {
			mkdir($uploads_directory, 0777, true);
		}

		if (file_exists($uploads_directory)) {

			$ext = pathinfo($_FILES["billingCACert"]["name"], PATHINFO_EXTENSION);

			$cert_filename = "billingCACert_" . date("Y-m-d-H-i-s-u") . ".$ext";
			$cert_filepath = "$uploads_directory/$cert_filename";

			if (move_uploaded_file($_FILES["billingCACert"]["tmp_name"], $cert_filepath)) {
				//echo "CA Cert has been uploaded.";

				if(file_exists($cert_filepath)) {

					$update_cert_stmt = $db->prepare('UPDATE systemConfig set billingCACert = ? where id = 0');
					if ($update_cert_stmt->execute(array($cert_filepath))) {
						$billingCACert = $cert_filepath;
					} else {
						error_log("ERROR: CA Cert was uploaded but not changed.");
					}

				}

			} else {
				error_log("ERROR: CA Cert was not uploaded.");
			}

		} else {
			error_log("ERROR: Couldn't create directory: '$uploads_directory'");
		}

	}
	if($billingForceSSL != $postBillingForceSSL) {

		if($postBillingForceSSL == "false" || ($postBillingForceSSL == "true" && $billingCACert)) {
			$billingForceSSLQuery = "UPDATE systemConfig set billingForceSSL = ? where id = 0";
			$billingForceSSLQueryStmt = $db->prepare($billingForceSSLQuery);
			$billingForceSSLQueryStmt->execute(array($postBillingForceSSL));
		}

	}

} elseif (isset($_POST["groupConfigForm"]) && $_POST["groupConfigForm"] == "true") {
	//$msg="GROUP";
	$groupDomainRequired = isset($_POST["groupDomainRequired"]) && $_POST["groupDomainRequired"] == "true" ? "true" : "false";

	// update group configuration attributes in 'groupConfig' table
	$policiesQuery = "UPDATE groupConfig set groupDomainRequired = ? where id = 0";

	$policiesQueryStmt = $db->prepare($policiesQuery);
	$policiesQueryStmt->execute(array($groupDomainRequired));

} elseif (isset($_POST["userConfigForm"]) && $_POST["userConfigForm"] == "true") {
	//$msg="USER";
	$userIdCriteria1 = $_POST["userIdCriteria1"];
	$userIdCriteria2 = $_POST["userIdCriteria2"];
	$clidPolicyLevel = $_POST["clidPolicyLevel"];
	$clidPolicy = $_POST["clidPolicy"];
	$clidNameFieldsVisible = isset($_POST["clidNameFieldsVisible"]) && $_POST["clidNameFieldsVisible"] == "true" ? "true" : "false";
	/* code added at @23-oct-2018,  Name Dialing Names feature */
	$dialingNameFieldsVisible = isset($_POST["dialingNameFieldsVisible"]) && $_POST["dialingNameFieldsVisible"] == "true" ? "true" : "false";
	$clidNumberRequired = isset($_POST["clidNumberRequired"]) && $_POST["clidNumberRequired"] == "true" ? "true" : "false";
	$clidPolicyFieldVisible = isset($_POST["clidPolicyFieldVisible"]) && $_POST["clidPolicyFieldVisible"] == "true" ? "true" : "false";
	$postalAddressRequired = isset($_POST["postalAddressRequired"]) && $_POST["postalAddressRequired"] == "true" ? "true" : "false";
	$useGroupAddress = isset($_POST["useGroupAddress"]) && $_POST["useGroupAddress"] == "true" ? "true" : "false";
	$addressFieldsVisible = isset($_POST["addressFieldsVisible"]) && $_POST["addressFieldsVisible"] == "true" ? "true" : "false";
	//modifiable address field
	$addressFieldsModifiable = isset($_POST["addressFieldsModifiable"]) && $_POST["addressFieldsModifiable"] == "true" ? "true" : "false";
	//end
	$useGroupTimeZone = isset($_POST["useGroupTimeZone"]) && $_POST["useGroupTimeZone"] == "true" ? "true" : "false";
	$timeZoneFieldVisible = isset($_POST["timeZoneFieldVisible"]) && $_POST["timeZoneFieldVisible"] == "true" ? "true" : "false";
	$emailFieldVisible = isset($_POST["emailFieldVisible"]) && $_POST["emailFieldVisible"] == "true" ? "true" : "false";
	//Code added for location visible fields
        $locationFieldVisible = isset($_POST["locationFieldVisible"]) && $_POST["locationFieldVisible"] == "true" ? "true" : "false";
        //End code
        $languageFieldVisible = isset($_POST["languageFieldVisible"]) && $_POST["languageFieldVisible"] == "true" ? "true" : "false";
	$deviceFieldsVisible = isset($_POST["deviceFieldsVisible"]) && $_POST["deviceFieldsVisible"] == "true" ? "true" : "false";
	$departmentsFieldVisible = isset($_POST["departmentsFieldVisible"]) && $_POST["departmentsFieldVisible"] == "true" ? "true" : "false";
	//$ccdFieldVisible = isset($_POST["ccdFieldVisible"]) && $_POST["ccdFieldVisible"] == "true" ? "true" : "false";
	$cpgFieldVisible = isset($_POST["cpgFieldVisible"]) && $_POST["cpgFieldVisible"] == "true" ? "true" : "false";
	$macAddressFieldVisible = isset($_POST["macAddressFieldVisible"]) && $_POST["macAddressFieldVisible"] == "true" ? "true" : "false";
	$voiceMessagingVisible = isset($_POST["voiceMessagingVisible"]) && $_POST["voiceMessagingVisible"] == "true" ? "true" : "false";
	$polycomServicesFieldVisible = isset($_POST["polycomServicesFieldVisible"]) && $_POST["polycomServicesFieldVisible"] == "true" ? "true" : "false";
	$numSIPGatewayInstances = $_POST["numSIPGatewayInstances"];
	//$numberActivation = isset($_POST["numberActivation"]) && $_POST["numberActivation"] == "Yes" ? "Yes" : "No";

        /* code added for user confige service assignment, softphone shared */
        $serviceAssignmentVisible = isset($_POST["serviceAssignmentVisible"]) && $_POST["serviceAssignmentVisible"] == "true" ? "true" : "false";
        $sharedDeviceVisible = isset($_POST["sharedDeviceVisible"]) && $_POST["sharedDeviceVisible"] == "true" ? "true" : "false";
        $softPhoneVisible = isset($_POST["softPhoneVisible"]) && $_POST["softPhoneVisible"] == "true" ? "true" : "false";
        
        /*end code */
        
        
	// update user configuration attributes in 'userConfig' table
	$policiesQuery = "UPDATE userConfig set " .
		"userIdCriteria1='" . $userIdCriteria1 . "', " .
		"userIdCriteria2='" . $userIdCriteria2 . "', " .
		"clidPolicyLevel='" . $clidPolicyLevel . "', " .
		"clidPolicy='" . $clidPolicy . "', " .
		"clidNameFieldsVisible='" . $clidNameFieldsVisible . "', " .
		"dialingNameFieldsVisible='" . $dialingNameFieldsVisible . "', " .
		"clidNumberRequired='" . $clidNumberRequired . "', " .
		"clidPolicyFieldVisible='" . $clidPolicyFieldVisible . "', " .
		"postalAddressRequired='" . $postalAddressRequired . "', " .
		"useGroupAddress='" . $useGroupAddress . "', " .
		"addressFieldsVisible='" . $addressFieldsVisible . "', " .
		"addressFieldsModifiable='" . $addressFieldsModifiable . "', " .
		"useGroupTimeZone='" . $useGroupTimeZone . "', " .
		"timeZoneFieldVisible='" . $timeZoneFieldVisible . "', " .
		"emailFieldVisible='" . $emailFieldVisible . "', " .
                "locationFieldVisible='" . $locationFieldVisible . "', " .
		"languageFieldVisible='" . $languageFieldVisible . "', " .
		"deviceFieldsVisible='" . $deviceFieldsVisible . "', " .
		"departmentsFieldVisible='" . $departmentsFieldVisible . "', " .
		"cpgFieldVisible='" . $cpgFieldVisible . "', " .
		"macAddressFieldVisible='" . $macAddressFieldVisible . "', " .
		"voiceMessagingVisible='" . $voiceMessagingVisible . "', " .
		"polycomServicesFieldVisible='" . $polycomServicesFieldVisible . "', " .
		"numSIPGatewayInstances='" . $numSIPGatewayInstances . "', " .
                "serviceAssignmentVisible='" . $serviceAssignmentVisible . "', " .
                "sharedDeviceVisible='" . $sharedDeviceVisible . "', " .
                "softPhoneVisible='" . $softPhoneVisible . "' " .
		//"numberActivation='" . $numberActivation. "' " .

		"where id='0'";

	$db->query($policiesQuery);
} elseif (isset($_POST["deviceConfigForm"]) && $_POST["deviceConfigForm"] == "true") {
	//$msg="DEVICE"
	//ex-964
    $softPhoneLinePortCriteria1 = $_POST["softPhoneLinePortCriteria1"];
    $softPhoneLinePortCriteria2 = $_POST["softPhoneLinePortCriteria2"];
    //end
	//$useCustomDeviceList = isset($_POST["useCustomDeviceList"]) && $_POST["useCustomDeviceList"] == "true" ? "true" : "false";
	$deleteAnalogUserDevice = isset($_POST["deleteAnalogUserDevice"]) && $_POST["deleteAnalogUserDevice"] == "true" ? "true" : "false";
	$deviceNameDID1 = $_POST["deviceNameDID1"];
	$deviceNameDID2 = $_POST["deviceNameDID2"];
	$deviceNameAnalog = $_POST["deviceNameAnalog"];
	$linePortCriteria1 = $_POST["linePortCriteria1"];
	$linePortCriteria2 = $_POST["linePortCriteria2"];
	$scaLinePortCriteria1 = $_POST["scaLinePortCriteria1"];
	$scaLinePortCriteria2 = $_POST["scaLinePortCriteria2"];
	$deviceAccessUserName1 = $_POST["deviceAccessUserName1"];
	$deviceAccessUserName2 = $_POST["deviceAccessUserName2"];
	$deviceAccessPassword = $_POST["deviceAccessPassword"];
	$analogAccessAuthentication = isset($_POST["analogAccessAuthentication"]) && $_POST["analogAccessAuthentication"] == "true" ? "true" : "false";
	$scaDeviceNameCriteria = $_POST["scaDeviceName"];
	
	if(isset($_POST["rebuildResetDevice"])){
		$rebuildResetDevice = $_POST["rebuildResetDevice"];
	}else{
		$rebuildResetDevice = "false";
	}
	
	// update device configuration attributes in 'userConfig' table
	$policiesQuery = "UPDATE deviceConfig set " .
		//"useCustomDeviceList='" . $useCustomDeviceList . "', " .
		"deleteAnalogUserDevice='" . $deleteAnalogUserDevice . "', " .
		"deviceNameDID1='" . $deviceNameDID1 . "', " .
		"deviceNameDID2='" . $deviceNameDID2 . "', " .
		"deviceNameAnalog='" . $deviceNameAnalog . "', " .
		"linePortCriteria1='" . $linePortCriteria1 . "', " .
		"linePortCriteria2='" . $linePortCriteria2 . "', " .
		"scaLinePortCriteria1='" . $scaLinePortCriteria1 . "', " .
		"scaLinePortCriteria2='" . $scaLinePortCriteria2 . "', " .
		"deviceAccessUserName1='" . $deviceAccessUserName1 . "', " .
		"deviceAccessUserName2='" . $deviceAccessUserName2 . "', " .
		"deviceAccessPassword='" . $deviceAccessPassword . "', " .
		"analogAccessAuthentication='" . $analogAccessAuthentication . "', " .
		"scaOnlyCriteria='" . $scaDeviceNameCriteria . "', " .
		"softPhoneLinePortCriteria1='" . $softPhoneLinePortCriteria1 . "', " .
		"softPhoneLinePortCriteria2='" . $softPhoneLinePortCriteria2 . "', " .
		"rebuildResetDevice='" . $rebuildResetDevice. "'" .
		"where id='0'";

	$db->query($policiesQuery);
} elseif (isset($_POST["serviceConfigForm"]) && $_POST["serviceConfigForm"] == "true") {
	//$msg="SERVICE";
	$userVoicePortalPasscodeType = $_POST["userVoicePortalPasscodeType"];
	$userVoicePortalPasscodeFormula = $_POST["userVoicePortalPasscodeFormula"];
	$userVoicePortalStaticPasscode = $_POST["userVoicePortalStaticPasscode"];
	//$usrAllowInternationalCalls = isset($_POST["usrAllowInternationalCalls"]) && $_POST["usrAllowInternationalCalls"] == "true" ? "true" : "false";
	$aacriteria1 = $_POST["aaCriteri1"];
	$aacriteria2 = $_POST["aaCriteri2"];
	$aaNameCriteria = $_POST["aaNameCriteria"];
	$aaClidFname = $_POST["aaClidFname"];
	$aaClidLname = $_POST["aaClidLname"];

	// update service configuration attributes in 'serviceConfig' table
	$policiesQuery = "UPDATE serviceConfig set " .
		"userVoicePortalPasscodeType='" . $userVoicePortalPasscodeType . "', " .
		"userVoicePortalPasscodeFormula='" . $userVoicePortalPasscodeFormula . "', " .
		"userVoicePortalStaticPasscode='" . $userVoicePortalStaticPasscode . "', " .
		/*"usrAllowInternationalCalls='" . $usrAllowInternationalCalls . "', " .*/
		"aacriteria1='" . $aacriteria1 . "', " .
		"aacriteria2='" . $aacriteria2 . "', " .
		"aaNameCriteria='" . $aaNameCriteria. "', " .
		"aaClidFname='" . $aaClidFname. "', " .
		"aaClidLname='" . $aaClidLname. "'" .

		"where id='0'";
	$db->query($policiesQuery);


	$scaIsActive = $_POST["scaIsActive"];
	$scaAllowOrigination = $_POST["scaAllowOrigination"];
	$scaAllowTermination = $_POST["scaAllowTermination"];
	$scaAlertClickToDial = $_POST["scaAlertClickToDial"];
	$scaAllowGroupPaging = $_POST["scaAllowGroupPaging"];
	$scaAllowCallRetrieve = $_POST["scaAllowCallRetrieve"];
	$scaAllowBridging = $_POST["scaAllowBridging"];
	$scaCallParkNotification = $_POST["scaCallParkNotification"];
	$scaMultipleCallArrangement = $_POST["scaMultipleCallArrangement"];
	$scaBridgeWarningTone = $_POST["scaBridgeWarningTone"];

	// update SCA service configuration attributes in 'scaConfig' table
	$scaQuery = "UPDATE scaConfig set " .
		"scaIsActive='" . $scaIsActive . "', " .
		"scaAllowOrigination='" . $scaAllowOrigination . "', " .
		"scaAllowTermination='" . $scaAllowTermination . "', " .
		"scaAlertClickToDial='" . $scaAlertClickToDial . "', " .
		"scaAllowGroupPaging='" . $scaAllowGroupPaging . "', " .
		"scaAllowCallRetrieve='" . $scaAllowCallRetrieve . "', " .
		"scaAllowBridging='" . $scaAllowBridging . "', " .
		"scaCallParkNotification='" . $scaCallParkNotification . "', " .
		"scaMultipleCallArrangement='" . $scaMultipleCallArrangement . "', " .
		"scaBridgeWarningTone='" . $scaBridgeWarningTone . "' " .

		"where id='0'";

	$db->query($scaQuery);

	storeVoiceMsgConfig();
}

function storeVoiceMsgConfig()
{
	global $db;
	//$msg="VOICE MESSAGING";
	$voiceMsgConVoicePortalID = $_POST["voiceMsgConVoicePortalID"];
	$voiceMsgConVoicePortalName = $_POST["voiceMsgConVoicePortalName"];
	$voiceMsgConClidFirstName = $_POST["voiceMsgConClidFirstName"];
	$voiceMsgConClidLastName = $_POST["voiceMsgConClidLastName"];

	// update voice messaging configuration attributes in 'voiceportalconfig' table
	$voiceQuery = "UPDATE voiceportalconfig set " .
		"voicePortalID='" . $voiceMsgConVoicePortalID . "', " .
		"voicePortalName='" . $voiceMsgConVoicePortalName . "', " .
		"clidFirstName='" . $voiceMsgConClidFirstName . "', " .
		"clidLastName='" . $voiceMsgConClidLastName . "' " . "where id ='1'";

	$db->query($voiceQuery);
}


function getDomainForSurgeMail() {
        global $surgeMailIdCriteria1, $surgeMailIdCriteria2, $surgemailDomain;

        if (isset($surgeMailIdCriteria1) && $surgeMailIdCriteria1 != "") {
        	$criteria = $surgeMailIdCriteria1;
		}
		elseif (isset($surgeMailIdCriteria2) && $surgeMailIdCriteria2 != "") {
        	$criteria = $surgeMailIdCriteria2;
		}
		else {
        	$criteria = "";
		}

        $smDomain = explode("@", $criteria);

        if (! isset($smDomain[1]) || $smDomain[1] == "") {
            return "";
        }

        switch (strtolower($smDomain[1])) {
            case "<systemdefaultdomain>":   return $_SESSION["systemDefaultDomain"];
            case "<defaultdomain>":         return $_SESSION["defaultDomain"];
            case "<surgemaildomain>":       return $surgemailDomain;
            default:                        return "";
        }
    }

    function updateClusterOperation($POST) { 
        
        global $db;
        $clusters = array();
        $allowedClusters = array("cluster1", "cluster2", "cluster3", "cluster4");
        foreach( $POST as $clusterName => $postVal ) {            
          if( in_array($clusterName, $allowedClusters) ) {
                $primaryclusterName = $postVal['clusterName'];
                $primaryBwServerIP = $postVal['primaryBwServerIP'];
                $primaryBwUserName = $postVal['primaryBwUserName'];
                $primaryBwPassword = $postVal['primaryBwPassword'];
                $ociReleasePrimary = $postVal['ociReleasePrimary'];
                $bwServerChoice = $postVal['bwServerChoice'];
                $primaryBwProtocol = $postVal['primaryBwProtocol'];
                $primaryBwPort = $postVal['primaryBwPort'];                
                $secondaryclusterName = $postVal['clusterName'];
                $secondaryBwServerIP = $postVal['secondaryBwServerIP'];
                $secondaryBwUserName = $postVal['secondaryBwUserName'];
                $secondaryBwPassword = $postVal['secondaryBwPassword'];
                $ociReleaseSecondary = $postVal['ociReleaseSecondary'];
                //$bwServerChoice = $postVal['bwServerChoice'];
                $secondaryBwProtocol = $postVal['secondaryBwProtocol'];
                $secondaryBwPort = $postVal['secondaryBwPort'];
                
                /* xsp server details */
                $primaryXspServerIP = $postVal['primaryXspServerIP'];
                $primaryXspProtocol = $postVal['primaryXspProtocol'];
                $primaryXspPort = $postVal['primaryXspPort'];  
                
                $secondaryXspServerIP = $postVal['secondaryXspServerIP'];
                $secondaryXspProtocol = $postVal['secondaryXspProtocol'];
                $secondaryXspPort = $postVal['secondaryXspPort'];
                $xspServerChoice = $postVal['xspServerChoice'];
                $xspServersId = $postVal['xspServersId'];
                
                if( isset($postVal['clusterId']) && $postVal['clusterId'] != "::") {
                    /* Modify Cluster */
                    $clusterId = explode("::", $postVal['clusterId']);
                    $primaryRowId = $clusterId[0];
                    $secondaryRowId = $clusterId[1];
                    
                    $serverConfig = new ServerConfiguration($db, appsServer, primaryServer);
                    if($primaryBwPassword == '') {
                        $primaryBwPassword = $serverConfig->getClusterPassword($primaryRowId);
                    }
                    $serverConfig->updateClusterConfig($primaryRowId, $primaryclusterName, $primaryBwServerIP, $primaryBwUserName, $primaryBwPassword, $ociReleasePrimary, ($bwServerChoice == "Primary" ? "true" : "false"), $primaryBwProtocol, $primaryBwPort);
                    $serverConfig = new ServerConfiguration($db, appsServer, secondaryServer);
//                     $serverConfig->id = $secondaryRowId;
                    if($secondaryBwPassword == '') {
                        $secondaryBwPassword = $serverConfig->getClusterPassword($secondaryRowId);
                    }
                    $serverConfig->updateClusterConfig($secondaryRowId, $secondaryclusterName, $secondaryBwServerIP, $secondaryBwUserName, $secondaryBwPassword, $ociReleaseSecondary, ($bwServerChoice == "Secondary" ? "true" : "false"), $secondaryBwProtocol, $secondaryBwPort);
                    
                    /* update cluster Name tbl users and adminChangeGroup */
                    $hiddenClusterName = $postVal['hiddenClusterName'];
                    $updateClusterName = $postVal['clusterName'];
                    updateClusertTblUsersAndChangeGroups($db,$hiddenClusterName,$updateClusterName);
                    /*end code */
                    
                    if( isset($xspServersId) && $xspServersId != "::") {
                        /* Update Xsp */
                        $xspServersId = explode("::", $xspServersId);
                        $primaryXspRowId = $xspServersId[0];
                        $secondaryXspRowId = $xspServersId[1];
                        
                        $serverConfig = new ServerConfiguration($db, xspServer, primaryServer);
                        $serverConfig->updateClusterXspConfig($primaryXspRowId, $primaryclusterName, $primaryXspServerIP, ($xspServerChoice == "Primary" ? "true" : "false"), $primaryXspProtocol, $primaryXspPort);
                        
                        $serverConfig = new ServerConfiguration($db, xspServer, secondaryServer);
                        $serverConfig->updateClusterXspConfig($secondaryXspRowId, $secondaryclusterName, $secondaryXspServerIP, ($xspServerChoice == "Secondary" ? "true" : "false"), $secondaryXspProtocol, $secondaryXspPort);
                    } else {
                        addXspServerConfig($db, $postVal, $primaryclusterName, $primaryXspServerIP, $activePrimaryServerXsp, $primaryXspProtocol, $primaryXspPort, $secondaryclusterName, $secondaryXspServerIP, $activeSecondaryServerXsp, $secondaryXspProtocol, $secondaryXspPort);
                    }

                    
                   } else {
                    /* Add Cluster */
                       if( ! isset($postVal["bwServerChoice"]) || ($postVal["bwServerChoice"] != "Primary" && $postVal["bwServerChoice"] != "Secondary") ) {
                           $activePrimaryServer = "true";
                           $activeSecondaryServer = "false";
                       } else {
                           $bwServerChoice = $postVal["bwServerChoice"];
                           $activePrimaryServer = $bwServerChoice == "Primary" ? "true" : "false";
                           $activeSecondaryServer = $bwServerChoice == "Secondary" ? "true" : "false";
                       }
                    $serverConfig = new ServerConfiguration($db, appsServer, primaryServer);
                    $serverConfig->addNewCluster($primaryclusterName, $primaryBwServerIP, $primaryBwUserName, $primaryBwPassword, $ociReleasePrimary, $activePrimaryServer, $primaryBwProtocol, $primaryBwPort);
                
                    $serverConfig = new ServerConfiguration($db, appsServer, secondaryServer);
                    $serverConfig->addNewCluster($secondaryclusterName, $secondaryBwServerIP, $secondaryBwUserName, $secondaryBwPassword, $ociReleaseSecondary, $activeSecondaryServer, $secondaryBwProtocol, $secondaryBwPort);
                    
                    /* Add xsp server */
                    addXspServerConfig($db, $postVal, $primaryclusterName, $primaryXspServerIP, $activePrimaryServerXsp, $primaryXspProtocol, $primaryXspPort, $secondaryclusterName, $secondaryXspServerIP, $activeSecondaryServerXsp, $secondaryXspProtocol, $secondaryXspPort);
                   /* if( ! isset($postVal["xspServerChoice"]) || ($postVal["xspServerChoice"] != "Primary" && $postVal["xspServerChoice"] != "Secondary") ) {
                        $activePrimaryServerXsp = "true";
                        $activeSecondaryServerXsp = "false";
                    } else {
                        $xspServerChoice = $postVal["xspServerChoice"];
                        $activePrimaryServerXsp = $xspServerChoice == "Primary" ? "true" : "false";
                        $activeSecondaryServerXsp = $xspServerChoice == "Secondary" ? "true" : "false";
                    }
                    
                    $serverConfig = new ServerConfiguration($db, xspServer, primaryServer);
                    $serverConfig->addNewClusterXspServer($primaryclusterName, $primaryXspServerIP, $activePrimaryServerXsp, $primaryXspProtocol, $primaryXspPort);
                    
                    $serverConfig = new ServerConfiguration($db, xspServer, secondaryServer);
                    $serverConfig->addNewClusterXspServer($secondaryclusterName, $secondaryXspServerIP, $activeSecondaryServerXsp, $secondaryXspProtocol, $secondaryXspPort);
                 */
                }
            }
        }
    }
    
    
    function addXspServerConfig($db, $postVal, $primaryclusterName, $primaryXspServerIP, $activePrimaryServerXsp, $primaryXspProtocol, $primaryXspPort, $secondaryclusterName, $secondaryXspServerIP, $activeSecondaryServerXsp, $secondaryXspProtocol, $secondaryXspPort) {
        /* Add xsp server */
        if( ! isset($postVal["xspServerChoice"]) || ($postVal["xspServerChoice"] != "Primary" && $postVal["xspServerChoice"] != "Secondary") ) {
            $activePrimaryServerXsp = "true";
            $activeSecondaryServerXsp = "false";
        } else {
            $xspServerChoice = $postVal["xspServerChoice"];
            $activePrimaryServerXsp = $xspServerChoice == "Primary" ? "true" : "false";
            $activeSecondaryServerXsp = $xspServerChoice == "Secondary" ? "true" : "false";
        }
        
        $serverConfig = new ServerConfiguration($db, xspServer, primaryServer);
        $serverConfig->addNewClusterXspServer($primaryclusterName, $primaryXspServerIP, $activePrimaryServerXsp, $primaryXspProtocol, $primaryXspPort);
        
        $serverConfig = new ServerConfiguration($db, xspServer, secondaryServer);
        $serverConfig->addNewClusterXspServer($secondaryclusterName, $secondaryXspServerIP, $activeSecondaryServerXsp, $secondaryXspProtocol, $secondaryXspPort);
    }
    
    function updateClusertTblUsersAndChangeGroups($db,$oldCluserName,$newUpdateClusterName){
      $update_Cluster_user = $db->prepare('UPDATE users SET clusterName = ? WHERE clusterName = ? ');
      $update_Cluster_user->execute(array($newUpdateClusterName, $oldCluserName));
      
      $update_last_sel_cluster_user = $db->prepare('UPDATE users SET cluster_name_selected = ? WHERE cluster_name_selected = ? ');
      $update_last_sel_cluster_user->execute(array($newUpdateClusterName, $oldCluserName));
      
      $update_Cluster_systemDevices = $db->prepare('UPDATE systemDevices SET clusterName = ? WHERE clusterName = ? ');
      $update_Cluster_systemDevices->execute(array($newUpdateClusterName, $oldCluserName));
      
      $update_Cluster_entDevices = $db->prepare('UPDATE enterpriseDevices SET clusterName = ? WHERE clusterName = ? ');
      $update_Cluster_entDevices->execute(array($newUpdateClusterName, $oldCluserName));
      
      $update_Cluster_AdminChangeGroup = $db->prepare('UPDATE `adminChangeGroup` SET `clusterName` = ? WHERE `clusterName` = ? ');
      $update_Cluster_AdminChangeGroup->execute(array($newUpdateClusterName, $oldCluserName));
      
    }
//     exit;
echo "Express configuration and policies have been updated";
//echo $msg;
?>
