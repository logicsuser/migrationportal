<!-- <link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v15">
<script src="/Express/js/jquery-ui-1.12.4.min.js"></script>--> 
<?php 
        //error_reporting(E_ALL);
        //error_reporting(1);
        //ini_set('display_errors', '1');


        require_once("/var/www/html/Express/config.php");
        checkLogin();
        require_once("/var/www/html/Express/adminCheckFilter.php");
        redirectUser("sysAdmin");
        $_SESSION["redirectUrl"] = "sysAdmin";
        
        $softwareRevision = file("/var/www/revision.txt", FILE_IGNORE_NEW_LINES); // read software revision from file
        $systemConfig = new DBSimpleReader($db, "systemConfig");
        $schemaRevision = $systemConfig->get("schemaRevision");
        
        
        require_once("/var/www/html/Express/header.php");
        $expressHostName = gethostname();
        
	
 ?>
<link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v15">
<script src="/Express/js/bootstrap.min.js"></script>
<script src="/Express/js/jquery-1.17.0.validate.js"></script>
<script>   
    
  //Code added @ 19 July 2019  
   
        var objOldFormData = []; 
        
        //Code added @ 22 July 2019
        var servers_config   = "no";
        var system_config    = "no";
        var group_config     = "no";
        var pass_rule_config = "no";
        var user_config      = "no";
        var devices_config   = "no";
        var services_config  = "no";
        var branding_config  = "no";
        //End code
        
  function showConfigTabData(divId, ref)
  { 
        var comDivId = "#"+divId;
        var tmpDivId = "";
        var clsName = $(ref).attr('class');
        if(clsName.indexOf('collapsed') >= 0){
            
            if(divId == "servers_config")
                tmpDivId = window.servers_config;
            if(divId == "system_config")
                tmpDivId = window.system_config;
            if(divId == "group_config")
                tmpDivId = window.group_config;
            if(divId == "pass_rule_config")
                tmpDivId = window.pass_rule_config;
            if(divId == "user_config")
                tmpDivId = window.user_config;
            if(divId == "devices_config")
                tmpDivId = window.devices_config;
            if(divId == "services_config")
                tmpDivId = window.services_config;
            if(divId == "branding_config")
                tmpDivId = window.branding_config;
                      
            if(tmpDivId == "no"){
                    $.ajax({
                            type: "POST",
                            url: "ajax/showSystemAdminData.php",
                            data: {adminConfigType : divId},	
                            beforeSend: function() {
                                $(comDivId).show();
                                checkResultNew = "false";
                                $(comDivId).html("<div id='loding' style='text-align:center; font-weight: 700; color: #9c9c9c; border: 0px solid red; width: auto;text-align:center'>&nbsp;Please wait, data is loading <br /><br /><img src='/Express/images/ajax-loader.gif'></div>");
                            },
                            success: function (result) { 
                                    //debugger ;
                                    $(comDivId).html("");
                                    checkResultNew = "true";
                                    $(comDivId).html(result);
//                                    if(checkResultNew == "true"){
//                                        FormChangesPrevent(divId);
//                                    } 
                                    
                                    if(divId == "servers_config")
                                        window.servers_config = "yes";
                                    if(divId == "system_config")
                                        window.system_config = "yes";
                                    if(divId == "group_config")
                                        window.group_config = "yes";
                                    if(divId == "pass_rule_config")
                                        window.pass_rule_config = "yes";
                                    if(divId == "user_config")
                                        window.user_config = "yes";
                                    if(divId == "devices_config")
                                        window.devices_config = "yes";
                                    if(divId == "services_config")
                                        window.services_config = "yes";
                                    if(divId == "branding_config")
                                        window.branding_config = "yes";
                            }
                    });
            }
            else{
                $(comDivId).show();
            }
        }else{            
            $(comDivId).hide();
        }        
  }
  //End code
   
    $().ready(function () {
        $(".uiModalAccordian").show();
        $(".rivisionDiv").show();
        $("#configurationIsLoading").hide();
        getBwReleaseDetails();
           if($('#smsAuthentication').is(':checked')){
            $("#smsCredential").show();
           } else if ($('#smsNoAuthentication').is(':checked')){
            $("#smsCredential").hide();
           }
        });

    var erroInForm = false;
     var saveChangesCount = false;
    $(function() {
        
 $("#addCluster").click(function(){
	 var numItems = $('.clusterServer').length + 1;
	        if (numItems > 4) { return; }
	    	else{
				$.ajax({
	    			type: "POST",
	    			url: "/Express/sysAdmin/template/serverConfigClusterTemplate.php",
	    			data: { clusterNumber: numItems },
	    			success: function(result) {
	    				$("#allClusterDiv").append(result);
	    			}
	    		});
	        }
		buttonsBehaviour();	
	});
	
		$("#smServer").change(function()
		{
			var dataToSend = $("#sysAdmin").serializeArray();
			dataToSend.push({ name: "sm", value: "true" });
			$.ajax({
				type: "POST",
				url: "sysAdminInfo.php",
				data: dataToSend,
                                async: false,
				success: function(result) {
					$("#smData").html(result);
				}
			});
		});


		$("#changeServers").click(function(event)
		{
			var dataToSend = $("#sysAdmin").serialize();
			$.ajax({
				type: "POST",
				url: "sysAdminDo.php",
				data: dataToSend,
				success: function(result) {
					alert(result);
					window.location.reload();
				}
			});
		});
        $('#smsAuthentication').change(function () {

             $("#smsCredential").show();

            });

        $('#smsNoAuthentication').change(function () {

            $("#smsCredential").hide();
            });

        $("#changeLogo").click(function (event) {

            if($("#logoFile").val() || $("input.set_logo:radio:checked").length > 0) {

                var dataToSend = new FormData($("#sysAdminLogo")[0]);
                $.ajax({
                    type: "POST",
                    url: "sysAdminLogoDo.php",
                    data: dataToSend,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        alert(result);
                        window.location.reload();
                    }
                });

            } else {
				alert("Please choose a logo");
            }

        });

        $(".removeLogoBtn, .removeLoginLogoBtn").click(function () {
			var logoType = this.className.indexOf('removeLoginLogoBtn') != -1 ? "loginPage" : "header";
            var filename = $(this).data('filename');

            if(confirm("Are you sure you want to remove the logo?")) {

                $.ajax({
                    type: "GET",
                    url: "sysAdminLogoDo.php?remove_file=" + filename + "&logoType=" + logoType,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        alert(result);
                        window.location.reload();
                    }
                });
		    } else {
                return false;
            }
	    });

       $("input.set_loginLogo").click(function() {	
           var isActive = $(this).hasClass('active');
           $("input.set_loginLogo").removeClass('active');
           
           if( !isActive) {
        	   $(this).addClass('active');
           } else {
        	   $(this).prop('checked', false);
            }
        });
      
		var changePermissionRecords = function(){

			 $.ajax({
                 type: "POST",
                 url: "sysAdminUserPermission.php",
                 async: false,
                 success: function(result) {
                 	console.log('Success');
                 }
          });
	          
		}

        $("#dialogSecurityDomainConfiguration").dialog({
    		autoOpen: false,
    		width: 800,
    		modal: true,
    		position: { my: "top", at: "top" },
    		resizable: false,
    		closeOnEscape: false,
    		buttons: {
    			"Cancel": function() {
    				$(this).dialog("close"); 
    			},
    			"Ok" : function(){
    				$("#confirmSecurityDomainSubmit").val('1'); 
 	    			$(this).dialog("close"); 
    				changePermissionRecords(); 
    				 setTimeout(function () {
 	    				$("#submitSystemConfig").trigger("click");
                     }, 1000);
    			},
    		},
            open: function() {
				setDialogDayNightMode($(this));
                $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
                $(".ui-dialog-buttonpane button:contains('Ok')").button().show().addClass('subButton');
            }
    	});

	    $("#submitSystemConfig").click(function(){
		   var newSecurityDomainPatternVal = $("#securityDomainPattern").val(); 
		   var oldSecurityDomainPatternVal = $("#hidSecurityDomainPattern").val(); 

		   if(newSecurityDomainPatternVal != oldSecurityDomainPatternVal){
			   	$("#dialogMU").html('Creating the group request');		
		   }
                   
		});
                
                
                


	});

    // Handler for opening configuration section
    // This handler manages multiple sections to show/display configuration sections
    // button: button which was toggled and which toggles directional arrows
    // section: section to toggle show/hide content
    // -----------------------------------------------------------------------------
    function toggleSection(button, section) {

        button.value = button.value == 'â–¼' ? 'â–²' : 'â–¼';

        var div = document.getElementById(section);
        div.style.display = div.style.display == "none" ? "block" : "none";
    }


    // Handler for submitting configuration section
    // This handler manages multiple sections to submit data for configuration DB update
    // button: submit button (not used)
    // formId: form with data controls that will be serialized and submitted by Ajax
    // toggleButtonId: arrow in toggle button will change from red to green
    // -----------------------------------------------------------------------------
    
    function enableClusterChange(noOfCluster) {
		
		  var checkBox = document.getElementById("enableClusters");
		  var clustersLength = $('.clusterServer').length;
		  if (checkBox.checked == false){
			  if( clustersLength > 1 ) {
				  var confirmAction = confirm("Disabling cluster support. First cluster settings will be used.");
				  if ( confirmAction != true ) {
					checkBox.checked = true;
				  }
				  //text.style.display = "block";			  
				}
		  }
    }

    function checkClusterNameRules() {
        
    var tempArry = [];
    erroInForm = false;
    $(".clusterName").each(function() {
		$thisVal = $(this).val();
		if($thisVal == "") {
			alert("Cluster Name is required field");
			erroInForm = true;
			return;
		} else {
				if(tempArry.indexOf($thisVal) == -1) {
					tempArry.push($thisVal);
				} else {
					alert("Cluster Names must be unique");
					erroInForm = true;
					return;
				}
		}
		});
		return erroInForm;
	}


function checkOCIReleaseValidation () {
	OCIIsUnique = true;	
	var serverConnected = "";
	var priServerConnected = null;
	var secServerConnected = null;
	$(".clusterServer").each(function() {
		if( $(this).find('.connectedPrimary').length > 0 ) {
			priServerConnected = $(this).find('.OCIReleasePrimary :selected').text();
		}
		if( $(this).find('.connectedSecondary').length > 0) {
			secServerConnected = $(this).find('.OCIReleaseSecondary :selected').text(); 
		}

		priServerConnected = ( secServerConnected != null ) ? secServerConnected : priServerConnected;
		if( priServerConnected != null) {
			return false;
		}
	});

	if( priServerConnected != null ) {
		$(".clusterServer").each(function() {
			var pRelease = $(this).find('.OCIReleasePrimary :selected').text();
			var sRelease = $(this).find('.OCIReleaseSecondary :selected').text();
			var pIP = $(this).find('.priIpAddress').val();
			var sIP = $(this).find('.secIpAddress').val();
			if( pIP != "" && ( pRelease != priServerConnected ) ) {
				OCIIsUnique = false;
				return false;
			}
			if( sIP != "" && (sRelease != priServerConnected) ) {
				OCIIsUnique = false;
				return false;
			}
			
		});
	}
		
	return OCIIsUnique;
}

    function submitConfig(button, formId, toggleButtonId) {
    	erroInForm = false;
    	saveChangesCount = false;
        
        /*if( getChangesSerializeData(formId) ) {
            return false;
    	}*/
        
        if( checkClusterNameRules() ) {
			return false;
    	}
		if( ! checkOCIReleaseValidation() ) {
			alert("OCI Release must be identical for all connected BW Servers.");
			return false;
		}
		
		if(formId == "systemConfig"){ 
		 var success = true;
		 /* validation for password expiration max length and min lenght  
		  * Super User Expiration time 1day and maximum time 1000day by default 30 days
		  * Regular User Expiration time  Expiration time 1day and maximum time 1000day by default 90 days
		 */
            var passwordExpiration_SU = document.getElementById("passwordExpiration_SU").value; 
            var passwordExpiration_RU = document.getElementById("passwordExpiration_RU").value; 
	        if((passwordExpiration_SU < 1) || (passwordExpiration_SU >1000)){
        		  alert("Super User Expiration minimum time should be atleast '1 day' and maximum time '1000 days'.");
                  $(this).focus();
                  success = false;
                  return false;
        
        	} 
 
        	if((passwordExpiration_RU < 1 )|| (passwordExpiration_RU >1000)){
        		alert("Regular User Expiration minimum time should be atleast '1 day' and maximum time '1000 days'.");
                $(this).focus();
                success = false;
                return false;
			} 
 		/* end */
			var newSecurityDomainPatternVal = document.getElementById("securityDomainPattern").value; 
  			var oldSecurityDomainPatternVal = document.getElementById("hidSecurityDomainPattern").value; 
  			var confirmSecurityDomainSubmitVal = document.getElementById("confirmSecurityDomainSubmit").value; 

  			if(newSecurityDomainPatternVal != oldSecurityDomainPatternVal && confirmSecurityDomainSubmitVal == 0){

				if(newSecurityDomainPatternVal == "" && oldSecurityDomainPatternVal != ""){
	  		 		$("#dialogSecurityDomainConfiguration").html('Warning: All existing non Super User admins must be configured to set valid Service Provider and Groups');
				}else if(newSecurityDomainPatternVal != "" && oldSecurityDomainPatternVal == ""){
					$("#dialogSecurityDomainConfiguration").html('Warning: All existing non Super User admins must be configured to set valid Security Domain');
				}else if(newSecurityDomainPatternVal != "" && oldSecurityDomainPatternVal != newSecurityDomainPatternVal){
					$("#dialogSecurityDomainConfiguration").html('Warning: All existing non Super User admins must be configured to set valid Security Domain');
				}
	  		 	
	  		 	$("#dialogSecurityDomainConfiguration").dialog("open");
	  		 	return false;
  			}

            if ( $('#smsAuthentication').is(':checked') && ($("#smsToken").val() === "" || $("#smsSID").val() === "" || $("#smsFrom").val() === "") ) {
                alert("Please enter Token, SID and the From phone number.");
                return false;
            } else if ($('#ldapAuthenticationTrue').is(':checked') && $("#ldapHost").val() === "") {
                alert("Please enter the LDAP host.");
                $("#ldapHost").focus();
                return false;
            }

			<?php
			if(!$billingCACert) {
			?>
            if ($('#billingForceSSL').is(':checked') && !$('#billingCACert').val()) {
                alert("You need to select a CA Cert to force SSL.");
                $("#billingCACert").focus();
                return false;
            }
			<?php
			}
			?>
                                    
                    //Code added @ 13 July 2018
                    var passwordReusePrevention = document.getElementById("passwordReusePrevention").value;
                    if(document.getElementById("passwordReusePrevention").value!="")
                    {
                        var errorMsg = false;
                        var passwordReusePrevention = document.getElementById("passwordReusePrevention").value; 
                         if((passwordReusePrevention < 0) || (passwordReusePrevention >20))
                         {
                               alert("Password Reuse Prevention's value must be between 0 to 20.");
                               //document.getElementById("errorSpanForPRPvton").html("");
                               $("#errorSpanForPRPvton").html("Password Reuse Prevention's value must be between 0 to 20.");
                               $(this).focus();
                               return false;
                         }
                    }                    
                    if(document.getElementById("passwordReusePrevention").value=="")
                    {
                        alert("Password Reuse Prevention's value can't be empty.");                        
                        $("#errorSpanForPRPvton").html("Password Reuse Prevention's value can't be empty.");
                        $(this).focus();
                        return false;
                    }
                    //End Code
                    
                  
                  /*validation for bw license warning and alert */
                    var warningsThreshold = document.getElementById("warningsThreshold").value;
                     var alertsThreshold = document.getElementById("alertsThreshold").value;
                    if(warningsThreshold !="" || alertsThreshold !="")
                    {
                        var errorMsg = false;
                        
                        if(!($.isNumeric($("#warningsThreshold").val())) || ((warningsThreshold < 0) || (warningsThreshold >100)))
                         {
                            alert("BroadWorks Licensing Warnings value only allow numeric number and must be between 1 to 100 ");
                            //document.getElementById("errorSpanForPRPvton").html("");
                               $("#errorwarningThreshold").html("BroadWorks Licensing Warnings value only allow numeric number and must be between 1 to 100");
                               $(this).focus();
                               return false;
                            
                         }
                         
                          if(!($.isNumeric($("#alertsThreshold").val())) || ((alertsThreshold < 0) || (alertsThreshold >100)))
                         {
                            alert("BroadWorks Licensing Alerts value only allow numeric number and must be between 1 to 100 ");
                           //document.getElementById("errorSpanForPRPvton").html("");
                           $("#erroralertsThreshold").html("BroadWorks Licensing Alerts value only allow numeric number must be between 1 to 100 ");
                           $(this).focus();
                           return false;
                        }
                         
                    }
                 /*end */
               }

	    if(formId === "passwordRulesConfig"){

		    var success = true;

            $(".passwordRulesMinimumLength").each(function () {
                if($(this).val() < 8 || $(this).val() > 40) {
                    alert("Password Rules: Minimum Password Length should be between '8' and '40'.");
                    $(this).focus();
                    success = false;
                    return false;
                }
            });

            if(success) {

                $(".passwordRulesNumberOfDigits").each(function () {
                    if ($(this).val() < 1 || $(this).val() > 5) {
                        alert("Password Rules: Number of Digits should be between '1' and '5.");
                        $(this).focus();
                        success = false;
                        return false;
                    }
                });
            }

            if(success) {

	            $(".passwordTypes").each(function () {

					var passwordType = $(this).val();

	                var minimumLength = parseInt($("#MinimumLength_" + passwordType).val());
	                var numberOfDigits = parseInt($("#NumberOfDigits_" + passwordType).val());
	                var specialCharacters = $("#SpecialCharacters_" + passwordType).prop('checked') ? 1 : 0;
	                var upperCaseLetter = $("#UpperCaseLetter_" + passwordType).prop('checked') ? 1 : 0;
	                var lowerCaseletter = $("#LowerCaseLetter_" + passwordType).prop('checked') ? 1 : 0;

	                var totalLength = numberOfDigits + specialCharacters + upperCaseLetter + lowerCaseletter;

	                if(minimumLength < totalLength) {
	                    alert("Password Rules: Minimum Length cannot be less than " + totalLength  + ", the minimum number of digits required plus count of every checked rule.");
	                    $("#MinimumLength_" + passwordType).focus();
	                    success = false;
	                    return false;
	                }
	            });

            }


            // toggle button to green - just an indication that changes have been submitted
        //    var toggleButton = document.getElementById(toggleButtonId);
        //    toggleButton.style.color = 'green';

            if(!success) {
                return false;
            }

        }

        // submit configuration changes in Ajax call
        var dataToSend = $("#" + formId ).serialize();
        //alert(dataToSend);


        // Get form
        //Code added @ 19 July 2019
        adminConfigType = formId;
        var form = $('#' + formId)[0];

        // Create an FormData object
        var data = new FormData(form);
            $.ajax({
	            type: "POST",
	            enctype: 'multipart/form-data',
	            url: "sysAdminPoliciesDo.php",
	            data: data,
	            processData: false,
	            contentType: false,
	            async: false,
	            success: function (result) {
	                //alert(result);
	                //window.location.reload();  //Code commented @ 19 July 2019
                        showAdminConfigData(adminConfigType); //Code added @ 19 July 2019
                        
	            }
	        });

        // toggle button to green - just an indication that changes have been submitted
        var toggleButton = document.getElementById(toggleButtonId);
        toggleButton.style.color = '#72ac5d';

    }
    function getRadioGroupValue(name) {
        var radioGroup = document.getElementsByName(name);
        for (var i = 0; i < radioGroup.length; i++) {
            if (radioGroup[i].checked) {
                return radioGroup[i].value;
            }
        }
        return "";
    }

    function initAdmin() {
        //alert('Test');
        // Show/hide Static domain based on domain type setting
        // ----------------------------------------------------
        var staticDomainDiv                   = document.getElementById("staticDomainDiv");
        var staticBwPortalPasswordDiv         = document.getElementById("staticBwPortalPasswordDiv");
        var staticProxyDomainDiv              = document.getElementById("staticProxyDomainDiv");
        var surgeMailUserPasswordDiv          = document.getElementById("surgeMailUserPasswordDiv");
        var surgeMailUserPasswordDiv          = document.getElementById("surgeMailUserPasswordDiv");
        var userVoicePortalPasscodeFormulaDiv = document.getElementById("userVoicePortalPasscodeFormulaDiv");
        var userVoicePortalStaticPasscodeDiv  = document.getElementById("userVoicePortalStaticPasscodeDiv");
        
        if (typeof(staticDomainDiv) != 'undefined' && staticDomainDiv != null){
            document.getElementById("staticDomainDiv").style.display =
            (getRadioGroupValue("groupsDomainType") == "Static") ? "block" : "none";
        }

        // Show/hide Static BW portal password based on BW portal password type setting
        // ----------------------------------------------------------------------------
        if (typeof(staticBwPortalPasswordDiv) != 'undefined' && staticBwPortalPasswordDiv != null){
                 document.getElementById("staticBwPortalPasswordDiv").style.display =
                (getRadioGroupValue("bwPortalPasswordType") == "Static") ? "block" : "none";
        }

        // Show/hide Static proxy domain based on domain type setting
        // ----------------------------------------------------------
        if (typeof(staticProxyDomainDiv) != 'undefined' && staticProxyDomainDiv != null){
                document.getElementById("staticProxyDomainDiv").style.display =
                (getRadioGroupValue("proxyDomainType") == "Static") ? "block" : "none";
        }

        // Show/hide SurgeMail password based on SurgeMail password type setting
        // ---------------------------------------------------------------------
        if (typeof(surgeMailUserPasswordDiv) != 'undefined' && surgeMailUserPasswordDiv != null){
            document.getElementById("surgeMailUserPasswordDiv").style.display =
                (getRadioGroupValue("surgeMailUserPasswordType") == "Static") ? "block" : "none";
        }

        // Show/hide passcode fields based on Voice Portal Passcode type setting
        // ---------------------------------------------------------------------
        if (typeof(userVoicePortalPasscodeFormulaDiv) != 'undefined' && userVoicePortalPasscodeFormulaDiv != null){
        document.getElementById("userVoicePortalPasscodeFormulaDiv").style.display = (getRadioGroupValue("userVoicePortalPasscodeType") == "Formula") ? "block" : "none";
        }
        if (typeof(userVoicePortalStaticPasscodeDiv) != 'undefined' && userVoicePortalStaticPasscodeDiv != null){
        document.getElementById("userVoicePortalStaticPasscodeDiv").style.display =  (getRadioGroupValue("userVoicePortalPasscodeType") == "Static") ? "block" : "none";
        }
      }


    // Change indication handlers
    // If anything changes in the form, the directional arrow changes to red
    // -----------------------------------------------------------------------------
    function updateChangeStatus(control, toggleButtonId) {
     //   var toggleButton = document.getElementById(toggleButtonId);
     //   toggleButton.style.color = 'red';

        // Show/hide Static Domain based on domain type selection
        if (control.name == "groupsDomainType") {
            document.getElementById("staticDomainDiv").style.display = (control.value == "Static") ? "block" : "none";
        }

        // Show/hide Static BW portal password based on BW portal password type selection
        if (control.name == "bwPortalPasswordType") {
            document.getElementById("staticBwPortalPasswordDiv").style.display = (control.value == "Static") ? "block" : "none";
        }

        // Show/hide Static Proxy Domain based on Proxy domain type selection
        if (control.name == "proxyDomainType") {
            document.getElementById("staticProxyDomainDiv").style.display = (control.value == "Static") ? "block" : "none";
        }

        // Show/hide Surgemail password based on password type
        if (control.name == "surgeMailUserPasswordType") {
            document.getElementById("surgeMailUserPasswordDiv").style.display = (control.value == "Static") ? "block" : "none";
        }

        // Show/hide User Voice Portal passcode fields based on passcode type
        if (control.name == "userVoicePortalPasscodeType") {
            document.getElementById("userVoicePortalPasscodeFormulaDiv").style.display = (control.value == "Formula") ? "block" : "none";
            document.getElementById("userVoicePortalStaticPasscodeDiv").style.display  = (control.value == "Static")  ? "block" : "none";
        }
    }

	function deleteCluster(clusterId , clusterIdValue, clustName) {
		
		var action = confirm("Express Administrators associated with the cluster being removed will also be removed. Proceed ?");
                
		if (action == true) {
			if(clusterIdValue != "::") {
				$.ajax({
					type: "POST",
					url: "ajax/deleteCluster.php",
					data: { serverConfigClusterId: "", clusterRowId: clusterIdValue,delUsersCluster:clustName},
					success: function(result) {
						$("#"+clusterId).remove();
						buttonsBehaviour();
					}
				});
			} else {
				$("#"+clusterId).remove();
				buttonsBehaviour();
			}
		}
	}
	
	function buttonsBehaviour() {
		var clustersLength = $('.clusterServer').length;
		if(clustersLength > 3 ) {
				$("#addCluster").prop("disabled", true);
				$(".deleteButton").prop("disabled", false);
		}
		else if( clustersLength == 1) {
			$("#addCluster").prop("disabled", false);
			$(".deleteButton").prop("disabled", true);
		} 
	}

	function getBwReleaseDetails() {
		var serversInfo = [];
		$(".singleServerDiv").each(function() {
				var  tempVar = {};
				var ipAddress = $(this).find(".ipAddress").val();
				var userName = $(this).find(".userName").val();
				var password = $(this).find(".oldPassword").val();
				var protocol = $(this).find(".protocol").val();
				var port = $(this).find(".port").val();
				tempVar = {ipAddress: ipAddress, userName: userName, password: password, protocol: protocol, port: port}
				serversInfo.push(tempVar);			
		});

		$.ajax({
			type: "POST",
			url: "ajax/getBwReleaseDetail.php",
			data: { serverDetails: serversInfo },
			success: function(result) {
				var resultTemp = $.parseJSON(result);
				$(".singleServerDiv").each(function() {
					var ipAddress = $(this).find(".ipAddress").val();
					$(this).find(".bwRelease").html(resultTemp[ipAddress]['bwrelease']);
				});
			}
		});

	}
	
</script>
<style>
    .changesCollapsedConfigColor{
        color : red !important;
    }
       .uiModalAccordian, .rivisionDiv {
            display: none;
        }
    .diaPL {
         margin-left: 143px !important;
    }
    .panel {
            background-color: initial !important;
    }
    .cluster{width:125px !important;}
    .clusterFirstTab { width:165px !important;}
    .clusterLastTab { width:60px !important;}
    .clusterSurgemail{width:125px !important;}
    .clusterCounterpath{width:125px !important;}
	.marginLeft {
		margin-left:4%;
	}
	.clusterName {
	   width: 82% !important;
	}
	
        .setContentMargin{padding-top: 48px !important;}
</style>
 
<!--div class="leftMenu"></div-->
<div id="mainBody" class="header_scroll setContentMargin">
    
        <div style="text-align: center;" id="configurationIsLoading"><img src="/Express/images/ajax-loader.gif"></div>					
        <div class="fcorn-registerTwo rivisionDiv">
        	<div class="row">
            	<div class="col-md-12">
					<div class="">
						<label class="labelText labelTextZero">Software Revision:</label>
						<label class="labelTextGrey">Rev.<?php echo $softwareRevision[0]; ?>
					</div>					
				</div>
				<div class="col-md-12">
					<div class="" style="margin-right: 10px;">
						<label class="labelText labelTextZero">Schema Revision:</label>
						<label class="labelTextGrey">Rev.<?php echo $schemaRevision; ?> 
					</div>
				</div>
            	<div class="col-md-12">
					<div class="form-group" style="margin-right: 10px;">
						<label class="labelText labelTextZero">Express Hostname:</label>
						<label class="labelTextGrey"><?php echo $expressHostName; ?> 
					</div>
				</div>	
            		
		</div>  
        </div>
        
        
        <!-- Start Code added @ 19 July 2019 -->
        <div class="col-md-12 uiModalAccordian form-group" id="mainServerConfigDivId">
           <div class="">
        	<div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                    <div class="panel-heading" role="tab" id="">
                            <h4 class="panel-title">
                            <a id=""  class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                                href="#servers_config" aria-expanded="false" aria-controls="toggleServersConfig" class="toggleServersConfig"   id="toggleServersConfig" onclick="javascript: showConfigTabData('servers_config', this);">Servers Configuration
                            </a>
                        </h4>
                    </div>
                    <div id="servers_config" style="display: none;" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="supervisoryTitleAdd">&nbsp;</div>
                </div>
            </div>
        </div>
               
        <div class="col-md-12 uiModalAccordian form-group" id="mainSystemConfigDivId">
        <div class="">
        <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
        	<div class="panel-heading systemAdminCollapseHeading" role="tab" id="">
        		<h4 class="panel-title">
                	<a id=""  class="accordion-toggle accordion-toggle-full-width collapsed systemAdminChanges" role="button" data-toggle="collapse"
                            href="#system_config" aria-expanded="false" aria-controls="divSystemConfig" class="toggleSystemConfig" name="toggleSystemConfig" id="toggleSystemConfig" onclick="javascript: showConfigTabData('system_config', this);">System Configuration
                    	</a>
                    </h4>
                </div>
                <div id="system_config" style="display: none;" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="supervisoryTitleAdd">&nbsp;</div>
           </div>
        </div>
        </div>
        
        <div class="col-md-12 uiModalAccordian form-group" style="display:none" id="mainGroupConfigDivId">
        <div class="">
        <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
        	<div class="panel-heading" role="tab" id="">
        	    <h4 class="panel-title">
                		<a class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                            href="#group_config" aria-expanded="false" aria-controls="divGroupConfig"  class="toggleGroupConfig" name="toggleGroupConfig" id="toggleGroupConfig" onclick="javascript: showConfigTabData('group_config', this);">Group Configuration
                    	</a>
                    </h4>
                </div>
                <div id="group_config" style="display: none;" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="supervisoryTitleAdd">&nbsp;</div>
         </div>            
        </div>
        </div>           
        
                
         <div class="col-md-12 uiModalAccordian form-group" id="mainPassConfigDivId">
            <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
            <div class="panel-heading" role="tab" id="">
                    <h4 class="panel-title">
                    <a class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                    href="#pass_rule_config" aria-expanded="false" aria-controls="divPasswordConfig"  class="divPasswordRulesConfig" name="divPasswordRulesConfig" id="divPasswordRulesConfig" onclick="javascript: showConfigTabData('pass_rule_config', this);">Password Configuration
                    </a>
                    </h4>
            </div> 
            <div id="pass_rule_config" style="display: none;" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="supervisoryTitleAdd">&nbsp;</div>
            </div>
	</div>
        
        <div class="col-md-12 uiModalAccordian form-group" id="mainUserConfigDivId">
            <div class="">
        	<div class="panel panel-default panel-forms fcorn-registerTwo" id="">
        	<div class="panel-heading" role="tab" id="">
        			<h4 class="panel-title">
                		<a id=""  class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                            href="#user_config" aria-expanded="false" aria-controls="divUserConfig"  name="toggleUserConfig" id="toggleUserConfig" onclick="javascript: showConfigTabData('user_config', this);">User Configuration
                    	</a>
                    </h4>
                </div>  
                <div id="user_config" style="display: none;" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="supervisoryTitleAdd">&nbsp;</div>                
        </div>
        </div>
        </div>       
                
        
        <div class="col-md-12 uiModalAccordian form-group" id="mainDeviceConfigDivId">
        <div class="">
            <div class="panel panel-default panel-forms fcorn-registerTwo" id="">
                    <div class="panel-heading" role="tab" id="">
                            <h4 class="panel-title">
                            <a id=""  class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                                href="#devices_config" aria-expanded="false" aria-controls="divDeviceConfig" class="toggleDeviceConfig" name="toggleDeviceConfig" id="toggleDeviceConfig" onclick="javascript: showConfigTabData('devices_config', this);">Device Configuration
                            </a>
                        </h4>
                    </div>
                    <div id="devices_config" style="display: none;" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="supervisoryTitleAdd">&nbsp;</div>
            </div>
        </div>
        </div>
        
        <div class="col-md-12 uiModalAccordian form-group" id="mainServicesConfigDivId">
        <div class="">
        	<div class="panel panel-default panel-forms fcorn-registerTwo" id="">
        	<div class="panel-heading" role="tab" id="">
        			<h4 class="panel-title">
                		<a id=""  class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                            href="#services_config" aria-expanded="false" aria-controls="divServiceConfig" class="toggleServiceConfig" name="toggleServiceConfig" id="toggleServiceConfig" onclick="javascript: showConfigTabData('services_config', this);">Service Configuration
                    	</a>
                    </h4>
                </div>
                    <div id="services_config" style="display: none;" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="supervisoryTitleAdd">&nbsp;</div>
        </div>
        </div>
        </div>     
            
        <div class="col-md-12 uiModalAccordian form-group" id="mainBrandingConfigDivId">
        <div class="">
        	<div class="panel panel-default panel-forms fcorn-registerTwo" id="">
        	<div class="panel-heading" role="tab" id="">
        			<h4 class="panel-title">
                		<a id=""  class="accordion-toggle accordion-toggle-full-width collapsed" role="button" data-toggle="collapse"
                            href="#branding_config" aria-expanded="false" aria-controls="divBrandingConfig" class="toggleBrandingConfig" name="toggleBrandingConfig" id="toggleBrandingConfig" onclick="javascript: showConfigTabData('branding_config', this);">Branding Configuration
                    	</a>
                    </h4>
                </div>    
                    <div id="branding_config" style="display: none;" class="collapsideDivAdd panel-collapse collapse" role="tabpanel" aria-labelledby="supervisoryTitleAdd">&nbsp;</div>
                </div>
        </div>	
        <body onload="initAdmin()"></body>
        </div>
        
        <!-- End Code -->     

<!--New code -->
<div id="create_new_admin_div" style="display: none;" title="Create Admin">
	<div id="createAdminStatus"></div>
	<form action="#" method="POST" name="createNewAdminForm" id="createNewAdminForm" class="fcorn-registerTwo">
	  	<div class="row">
    		<div class="col-md-6">
    			<div class="form-group">
    				<label class="labelText" for="userName">Username:</label>
    				<input type="text" name="userName" id="newAdminUsername" class="form-control" autocomplete="off" required/>
    			</div>
    		</div>
	
    		<div class="col-md-6">
    			<div class="form-group">
    				<label class="labelText" for="newAdminFirstName">First Name:</label>
    				<input type="text" name="firstName" id="newAdminFirstName" class="form-control" autocomplete="off" required/>
    			</div>
    		</div>
		
		
    		<div class="col-md-6">
    			<div class="form-group">
    				<label class="labelText" for="newAdminLastName">Last Name:</label>
    				<input type="text" name="lastName" id="newAdminLastName" class="form-control" autocomplete="off" required/>
    			</div>
    		</div>
		
    		<div class="col-md-6">
    			<div class="form-group">
    				<label class="labelText" for="newAdminEmail">Email Address:</label>
    				<input type="email" name="emailAddress" id="newAdminEmail" class="form-control" autocomplete="off" required/>
    			</div>
    		</div>
		
    		<div class="col-md-6">
    			<div class="form-group">
    				<label class="labelText" for="newAdminPassword">Password:</label>
    				<input type="password" name="password" id="newAdminPassword" class="form-control" autocomplete="off" required/>
    			</div>
    		</div>
		
    		<div class="col-md-6">
        		<div class="form-group">
        			<label class="labelText">Admin Type:</label>
        			<div class="dropdown-wrap">
        				<select name="adminType" id="newAdminType" class="form-control" autocomplete="off" required>
        						<option value="1">Admin</option>
        						<option value="2">Super Admin</option>
        				</select>
        			</div>
        		</div>
			</div>	
    	</div>
	</form>
</div>
<!--End code -->
<div id="dialogSecurityDomainConfiguration" class="dialogClass"></div>
 
<script>
/* $('#logoFile').filestyle({
    buttonText : ' Choose a file from System',
    buttonName : 'btn-info'
   }); */
 
	$(".ldapAuthentication").click(function() {
		if($("#ldapAuthenticationTrue").is(':checked')) {
			$("#ldapHostDiv").show();
		    $("#ldapHost").prop("disabled", false);
		    $("#ldapDomainDiv").show();
		    $("#ldapDomain").prop("disabled", false);
		} else {
            $("#ldapHostDiv").hide();
            $("#ldapHost").prop("disabled", true);
            $("#ldapDomainDiv").hide();
            $("#ldapDomain").prop("disabled", true);
		}
	});
        
        function updateBwPort(htpPrtcl, updateField)
        {
            document.getElementById(updateField).value = "";
            if(htpPrtcl=="https")
                document.getElementById(updateField).value = "443";
            if(htpPrtcl=="http")
                document.getElementById(updateField).value = "80";
        }
        
        function updateSurgeMailPort(htpPrtcl, updateField)
        {
            document.getElementById(updateField).value = "";
            if(htpPrtcl=="https")
                document.getElementById(updateField).value = "7025";
            if(htpPrtcl=="http")
                document.getElementById(updateField).value = "7026";
        }

        function updateCounterPathPort(htpPrtcl, updateField)
        {
            document.getElementById(updateField).value = "";
            if(htpPrtcl=="https")
                document.getElementById(updateField).value = "443";
            if(htpPrtcl=="http")
                document.getElementById(updateField).value = "80";
        }

    $().ready(function () {
        $("#newAdminForm").validate();
        $("#create_new_admin_div").dialog($.extend({}, defaults, {
            width: 900,
            buttons: [
	                {
	                    text: "Create Admin",
		                class: "subButton",
		                id: "createNewAdminFormSubmitBtn",
		                click: function () {
		                    if($("#createNewAdminForm").valid()) {

			                    $("#createAdminStatus").hide();
                                $("#createNewAdminFormSubmitBtn").prop('disabled', true);
                                $("#createNewAdminFormSubmitBtn").html("Please Wait ...");
			                    var formData = $("#createNewAdminForm").serialize();
			                    $.ajax({
			                        type: "POST",
			                        url: "createAdmin.php",
			                        data: formData,
			                        dataType:'json',
			                        success: function (result) {
			                            if(result.success) {
			                                $("#createAdminStatus").html('<div class="alignCenter">'  + result.msg + '</div>');
			                                $("#createAdminStatus").show();
			                                $("#createNewAdminForm").hide();
                                            $("#createNewAdminFormSubmitBtn").hide();
			                            } else {

                                            $("#createNewAdminFormSubmitBtn").prop('disabled', false);
                                            $("#createNewAdminFormSubmitBtn").html("Create Admin");
			                                $("#createAdminStatus").html('<div class="alignCenter">'  + result.msg + '</div>');
			                                $("#createAdminStatus").show();
			                            }
			                        }
			                    });
		                    }
		                }
	                },
		            {
                        text: "Close",
                        class: "cancelButton",
                        click: function () {
	                        $(this).dialog("close");
	                    }
		            }
                ]
        }));

        $("#createNewAdminBtn").click(function () {
            $("#createAdminStatus").html("");
            $("#createNewAdminForm").show();
            $("#createNewAdminForm")[0].reset();
            $("#createNewAdminFormSubmitBtn").prop('disabled', false);
            $("#createNewAdminFormSubmitBtn").html("Create Admin");
            $("#createNewAdminFormSubmitBtn").show();
            $("#create_new_admin_div").dialog("open");
            $("#create_new_admin_div").show();
        });

        $("#billingForceSSL").click(function() {
            if($("#billingForceSSL").is(':checked')) {
                $("#billingCACertDiv").show();
            } else {
                $("#billingCACertDiv").hide();
            }
        });
    });
</script>
<style>
.ui-dialog-titlebar-close{
    display:none !important;
}
.error, .error text {
            color: red;
	}
	/*.widthAlign{
	width:86%;
	margin-left:10px;
	} */
	.widthPass{
	width:70px;
	margin-left:10px;
	}
	.panel-heading {
    padding: 10px 15px;
    /* border-bottom: 1px solid transparent; */
    border-radius: 4px;
    border: 0px !important;
}

.removeLogoBtn{margin-left: -25px;}
</style>
<script>
</script>
