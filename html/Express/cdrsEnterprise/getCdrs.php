<?php
    require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once("/var/www/lib/broadsoft/login.php");
	 checkLogin();
	require_once ("/var/www/lib/broadsoft/adminPortal/ServiceProviderOperations.php");
	require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupOperation.php");
	 
	unset($_SESSION['groupListCDRsDetail']);
	unset($_SESSION['cdrsCSVFileName']);
	$postedGroup = trim($_POST["groupName"]);
	$postedEnterprise = $_POST["enterprise"];
//  $postedGroup = trim($_POST["selectedGroupVal"]);
    $searchLevel = $_POST["searchlevel"];
    $limitRow = $_POST["limitTableRow"];
    
    /* Check group existance start */
    $groupExist = false;
    foreach( $_SESSION["ExistedGroups"] as $groupId => $groupVal) {
        if($groupVal == $postedGroup) {
            $postedGroup = trim($groupId);
            $groupExist = true;
            break;
        }
    }
    if($postedGroup != "All Groups" && $groupExist == false) {
        echo"Group Not Exist";
        die;
    }
    /* Check group existance End */
    
	if($searchLevel == "system") {
    	     if($postedGroup == "All Groups") {
        	         $spObj = new ServiceProviderOperations();
        	         $serviceProviders = $spObj->getSystemServerProviderList();
        	         $groupList = getAllGroupSystemWide($serviceProviders);
        	         $_SESSION['cdrsCSVFileName'] = "System-wide-groups";
    	     } else {
    	           $groupList[] = $postedGroup;
    	           $_SESSION['cdrsCSVFileName'] = $postedGroup;
    	     }
	 } else if($searchLevel == "enterprise") {
    	     $serviceProviders[] = $postedEnterprise;
    	       if($postedGroup == "All Groups") {
        	         $spObj = new ServiceProviderOperations();
        	         $groupList = getAllGroupSystemWide($serviceProviders);
        	         $_SESSION['cdrsCSVFileName'] = "Enterprise-wide-groups";
    	     } else {
    	           $groupList[] = $postedGroup;
    	           $_SESSION['cdrsCSVFileName'] = $postedGroup;
    	     }
	 }
	 
	function getAllGroupSystemWide($serviceProviders) {
	     $sp = 0;
	     $groupId = 0;
	     $systemWideGroup = array();
	     $group = new GroupOperation($sp, $groupId);
	     foreach($serviceProviders as $spKey => $sp) {
	         $group->sp = $sp;
	         $groupListResponse = $group->getGroupList();
	         foreach ($groupListResponse as $groupData) {
	             $groupData = explode("<span class='search_separator'>-</span>", $groupData);
	             $systemWideGroup[] = trim($groupData[0]);
	         }
	     }
	     return $systemWideGroup;
	 }
         
         
         
	 
	 function checkSpAndGroupExistance($spId, $groupId, $serviceProviders, $groupList, $searchLevel, $postedGroup) {
	     $response = new stdClass();
	     if($searchLevel == "system" && $postedGroup == "All Groups") {
	         if(! in_array($spId, $serviceProviders)) {
	             $response->serviceProvider = $spId . ' <span>(Deleted)</span>';
	             $response->customerId = $groupId . ' <span>(Deleted)</span>';
	         } else if( in_array($spId, $serviceProviders) && ! in_array($groupId, $groupList) ) {
	             $response->serviceProvider = $spId;
	             $response->customerId = $groupId . ' <span>(Deleted)</span>';
	         } else {
	             $response->serviceProvider = $spId;
	             $response->customerId = $groupId;
	         }
	     } else {
	         $response->serviceProvider = $spId;
	         $response->customerId = $groupId;
	     }
	     
	     return $response;
	 }

         
         
         
         //Code added @ 05 Feb 2019
         if (strlen($_POST["from"]) > 0){
		$from = trim($_POST["from"]);
		$expFrom = explode("/", $from);
		$y = $expFrom[2];
		$m = $expFrom[0];
		$d = $expFrom[1];
		$from = $y . "-" . $m . "-" . $d;
	}else{
		$from = date("Y-m-d", strtotime("-30 days")); //limit CDRs to the past 30 days
	}if (strlen($_POST["to"]) > 0){
		$to = trim($_POST["to"]);
		$expTo = explode("/", $to);
		$y = $expTo[2];
		$m = $expTo[0];
		$d = $expTo[1];
		$to = $y . "-" . $m . "-" . $d;
	}else{
		$to = date("Y-m-d", strtotime("+1 day"));
	}
        
        if (strlen($_POST["user"]) > 0){
		$user = trim($_POST["user"]);
	}else{
		$user = "0";
	}
        
	if (strlen($_POST["calledNum"]) > 0){
		$called = trim($_POST["calledNum"]);
	}else{
		$called = "0";
	}
        
	if (strlen($_POST["callingNum"]) > 0){
		$calling = trim($_POST["callingNum"]);
	}else{
		$calling = "0";
	}
        
	if (strlen($_POST["department"]) > 0){
		$department = trim($_POST["department"]);
	}else{
		$department = "0";
	}
        //End code
         
         
         
         
         
         ?>
<script>
	$(function()
	{
		$("#callDetail").dialog(
		{
			autoOpen: false,
			width: 1400,
			modal: true,
			resizable: false,
			position: { my: "top", at: "top" },
			closeOnEscape: true,
			resizable: false,
			open: function(event) {
                                $("html, body").animate({ scrollTop: 0 }, 0); //Code added @12 Feb 2019
				setDialogDayNightMode($(this));
                                $('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('closeButton');
        	},
        	buttons: {
        		"Close": function()
        		{
        			$(this).dialog("close");
        		}

        	}
		});

		$("#summaryEnt tr, #summary2Ent td").hover(function() {
//			$(this).addClass("sumOn");
		}, function() {
//			$(this).removeClass("sumOn");
		});

		$("#summaryEnt tbody tr, #summary2Ent tbody tr").click(function() {
			$.ui.dialog.prototype._focusTabbable = function(){};		/*revmove autofocus from dialog buttons.*/
			$("#callDetail").dialog("open");
			$("#callDetail").html("<div class=\"loading\" id=\"loading3\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
			$("#loading3").show();
			var from = "<?php echo $from; ?>";
			var to = "<?php echo $to; ?>";
			var called = "<?php echo $called; ?>";
			var calling = "<?php echo $calling; ?>";
			var department = "<?php echo $department; ?>";
			var user = "<?php echo $user; ?>";
		    
			var userId = $(this).find('span').attr("id");
			var filter = $(this).find('span').attr("id");
			var limitRow = "<?php echo $limitRow; ?>";
			$.ajax(
			{
				type: "POST",
				url: "cdrsEnterprise/callDetails.php",
				//data: { userId: userId, filter: filter, limitRow: limitRow},
                                data: { userId: userId, from: from, to: to, called: called, calling: calling, department: department, user: user, filter: filter, limitRow: limitRow },
				success: function(result)
				{
					$("#loading3").hide();
			 		$("#callDetail").html(result);
				}
			});
		});

		$("#summaryEnt").tablesorter();
	});
</script>
<?php
    
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getAllNumberAssignments.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");
        
        $whereCondition = " WHERE 1=1 ";
        
        if ($department !== "0"){
		require_once("getDeptUsers.php");
		$whereCondition .= " AND responsibleParty in (" . ($deptUsers != "" ? rtrim($deptUsers, ",") : "NULL") . ")";
	}if ($user !== "0"){
		$whereCondition .= " AND responsibleParty='" . $user . "'";
	}if ($from !== "0"){
		$whereCondition .= " AND startDate>='" . $from . "'";
	}if ($to !== "0"){
		$whereCondition .= " AND startDate<='" . $to . "'";
	}if ($called !== "0"){
		$whereCondition .= " AND calledNumber like '%" . $called . "%'";
	}if ($calling !== "0"){
		$whereCondition .= " AND callingNumber like '%" . $calling . "%'";
	}        

// 	$query = "SELECT * from broadsoft_cdrs where customerId='" . $_SESSION["groupId"] . "'";
	if($searchLevel == "system" && $postedGroup == "All Groups") {
	    $query = "SELECT * from broadsoft_cdrs $whereCondition";
	} else {
            $whereCondition .= "AND BINARY customerId IN ('" . implode("','", $groupList) . "') ";
	    //$query = "SELECT * from broadsoft_cdrs where BINARY customerId IN ('" . implode("','", $groupList) . "')";
	    $query = "SELECT * from broadsoft_cdrs $whereCondition";
            //$query = "SELECT * from broadsoft_cdrs where";
// 	    $i=0;
// 	    foreach($groupList as $custKey => $custVal) {
// 	        $query .= " customerId='". $custVal ."'";
// 	        if( ++$i !== count($groupList) ) {
// 	            $query .= " or";
// 	        }
// 	    }
	}
	$query .= " order by startDate desc";
//	$query .= " limit ". "3";
        //echo "<br />SQL - ".$query;
	$_SESSION["query"] = $query;
	openssl_error_string();
	$sth = $billDB->query($query);

	$totalInbound = 0;
	$ibCount = 0;
	$totalOutbound = 0;
	$obCount = 0;
	$totalOutboundExternal = 0;
	$obeCount = 0;
	$totalInboundOutbound = 0;
	$ibobCount = 0;
	$totalIntl = 0;
	$intCount = 0;
	$total411 = 0;
	$total411Count = 0;
	$totalTollFree = 0;
	$tfCount = 0;
	$total911 = 0;
	$total911Count = 0;
	$a = 0;
	$allSystemGroups = array();
	$responsibleParty = array();
	while ($row = $sth->fetch(PDO::FETCH_ASSOC))
	{
		//All inbound
		if ($row["direction"] == "Terminating")
		{
			$totalInbound += $row["duration"];
			$ibCount++;

			$totalInboundOutbound += $row["duration"];
			$ibobCount++;
		}

		//All outbound
		if ($row["direction"] == "Originating")
		{
			$totalOutbound += $row["duration"];
			$obCount++;

			$totalInboundOutbound += $row["duration"];
			$ibobCount++;

			//run separate count of external outbound calls for Rhema Bible Church
			if ($_SESSION["groupId"] == "Rhema Bible Church")
			{
				//check if any calls are toll free
				$tollFreeFlag = 0;
				foreach ($tollFree as $key => $value)
				{
					if (substr($row["calledNumber"], 0, strlen($value)) === $value)
					{
						$tollFreeFlag = 1;
					}
				}

				//check if any calls are to assigned numbers
				$numberFlag = 0;
				foreach ($numberAssignments as $key => $value)
				{
					$key = explode("-", $key);
					$key = $key[1];
					if (substr($row["calledNumber"], -strlen($key)) === $key)
					{
						$numberFlag = 1;
					}
				}

				if ($row["calledNumber"] !== "pingtone.com" and strlen($row["calledNumber"]) >= 10 and substr($row["calledNumber"], 0, 3) !== "011" and $row["answerIndicator"] !== "No" and $tollFreeFlag == 0 and $numberFlag == 0)
				{
					$totalOutboundExternal += $row["duration"];
					$obeCount++;
				}
			}
		}

		//All outbound International
		if (substr($row["calledNumber"], 0, 3) === "011" and $row["direction"] == "Originating")
		{
			$totalIntl += $row["duration"];
			$intCount++;
		}

		//All 411 calls
		if ($row["calledNumber"] === "411")
		{
			$total411 += $row["duration"];
			$total411Count++;
		}

		//substring for checking 800's
		$first3 = substr($row["calledNumber"], 0, 3);
		$first4 = substr($row["calledNumber"], 0, 4);
		$first5 = substr($row["calledNumber"], 0, 5);

		if (in_array($first3, $tollFree) and $row["direction"] == "Terminating")
		{
			$totalTollFree += $row["duration"];
			$tfCount++;
		}
		if (in_array($first4, $tollFree) and $row["direction"] == "Terminating")
		{
			$totalTollFree += $row["duration"];
			$tfCount++;
		}
		if (in_array($first5, $tollFree) and $row["direction"] == "Terminating")
		{
			$totalTollFree += $row["duration"];
			$tfCount++;
		}

		//All emergency calls
		if ($row["calledNumber"] === "911")
		{
			$total911 += $row["duration"];
			$total911Count++;
		}

		$resp = $row["responsibleParty"];
		$resp = $resp . ":" . (isset($userPh[$resp]["name"]) ? $userPh[$resp]["name"] : "");

		if ($row["direction"] == "Terminating")
		{
			if (!isset($calls[$resp]["totalInboundCount"]))
			{
				$calls[$resp]["totalInboundCount"] = 0;
			}
			$calls[$resp]["totalInboundCount"]++;
			if (!isset($calls[$resp]["totalInboundMinutes"]))
			{
				$calls[$resp]["totalInboundMinutes"] = 0;
			}
			$calls[$resp]["totalInboundMinutes"] += $row["duration"];
		}
		if ($row["direction"] == "Originating")
		{
			if (!isset($calls[$resp]["totalOutboundCount"]))
			{
				$calls[$resp]["totalOutboundCount"] = 0;
			}
			$calls[$resp]["totalOutboundCount"]++;
			if (!isset($calls[$resp]["totalOutboundMinutes"]))
			{
				$calls[$resp]["totalOutboundMinutes"] = 0;
			}
			$calls[$resp]["totalOutboundMinutes"] += $row["duration"];
		}
		
		$allSystemGroups[] = $row["customerId"];
		$spAndGroupInfo = checkSpAndGroupExistance($row["serviceProvider"], $row["customerId"], $serviceProviders, $groupList, $searchLevel, $postedGroup);
		$calls[$resp]["serviceProvider"] = $spAndGroupInfo->serviceProvider;
		$calls[$resp]["customerId"] = $spAndGroupInfo->customerId;
		
		if(count($calls) == $limitRow && $limitRow != "All Rows") {
		    break;
		}
	}
	
	$allSystemGroups = array_unique($allSystemGroups);
?>
    <?php 
    if($searchLevel == "system" && $postedGroup == "All Groups") {
        $_SESSION['groupListCDRsDetail'] = $allSystemGroups;
    } else {
        $_SESSION['groupListCDRsDetail'] = $groupList;
    }
    ?>

<div class="row">
	<div class="col-md-4">
	<div class="viewDetail autoHeight viewDetailNew" style="width:100%!important;">
	<div style="zoom: 1;">
	
			<!--<table id="summary2" style="width:100%;margin:0;" class="scroll tablesorter dataTable">-->
			<table id="summary2Ent" style="width:100%;margin:0;" class="scroll dataTable display table table-striped table-bordered">
			
				<thead><th class="thsmall">Totals for Filters Applied</th></thead>
				
				<tr>
					<td class="thClassCDR">Total Outbound</td>
					<td class="thClassCDR"><span id="outbound"><?php echo $obCount; ?> calls, <?php echo floor($totalOutbound/60) . ":" . str_pad($totalOutbound%60, 2, "0", STR_PAD_LEFT); ?> Minutes</span></td>
				</tr>
				<?php
					if ($_SESSION["groupId"] == "Rhema Bible Church")
					{
						?>
						<tr>
							<td class="thClassCDR">Total Outbound (External Only)</td>
							<td class="thClassCDR"><span id="outboundExternal"><?php echo $obeCount; ?> calls, <?php echo floor($totalOutboundExternal/60) . ":" . str_pad($totalOutboundExternal%60, 2, "0", STR_PAD_LEFT); ?> Minutes</span></td>
						</tr>
						<?php
					}
				?>
				<tr>
					<td class="thClassCDR">Total Inbound</td>
					<td class="thClassCDR"><span id="inbound"><?php echo $ibCount; ?> calls, <?php echo floor($totalInbound/60) . ":" . str_pad($totalInbound%60, 2, "0", STR_PAD_LEFT); ?> Minutes</span></td>
				</tr>
				<tr>
					<td class="thClassCDR">Total Outbound + Inbound</td>
					<td class="thClassCDR"><span id="inboundoutbound"><?php echo $ibobCount; ?> calls, <?php echo floor($totalInboundOutbound/60) . ":" . str_pad($totalInboundOutbound%60, 2, "0", STR_PAD_LEFT); ?> Minutes</span></td>
				</tr>
				<tr>
					<td class="thClassCDR">Total International</td>
					<td class="thClassCDR"><span id="international"><?php echo $intCount; ?> calls, <?php echo floor($totalIntl/60) . ":" . str_pad($totalIntl%60, 2, "0", STR_PAD_LEFT); ?> Minutes</span></td>
				</tr>
				<tr>
					<td class="thClassCDR">Total Toll Free</td>
					<td class="thClassCDR"><span id="tollFree"><?php echo $tfCount; ?> calls, <?php echo floor($totalTollFree/60) . ":" . str_pad($totalTollFree%60, 2, "0", STR_PAD_LEFT); ?> Minutes</span></td>
				</tr>
				<tr>
					<td class="thClassCDR">Total Dir Asst</td>
					<td class="thClassCDR"><span id="dirAsst"><?php echo $total411Count; ?> calls, <?php echo floor($total411/60) . ":" . str_pad($total411%60, 2, "0", STR_PAD_LEFT); ?> Minutes</span></td>
				</tr>
				<tr>
					<td class="thClassCDR">Total Emergency Asst</td>
					<td class="thClassCDR"><span id="emergencyAsst"><?php echo $total911Count; ?> calls, <?php echo floor($total911/60) . ":" . str_pad($total911%60, 2, "0", STR_PAD_LEFT); ?> Minutes</span></td>
				</tr>
			</table>
			</div>
			</div>
		</div>
		<div class="col-md-8">
		<div class="viewDetail123 autoHeight123 viewDetailNew123" style="width:100%!important;">
		<div style="zoom: 1;">
				<table id="summaryEnt" class="tablesorter scroll tablesorter dataTable" style="width:100%;margin:0;">
					<thead>
						<tr>
						<th class="" style='width:13%;'>Enterprise</th>
						<th class="" style='width:13%;'>Groups</th>
						<th class="" style='width:13%;'>User</th>
						<th class="" style='width:13%;'>Inbound Count</th>
						<th class="" style='width:13%;'>Inbound Minutes</th>
						<th class="" style='width:13%;'>Outbound Count</th>
						<th class="" style='width:13%;'>Outbound Minutes</th>
						</tr>
					</thead>
					<tbody>
						
						<?php
							if (isset($calls))
							{
								foreach ($calls as $key => $value)
								{
								    if($key == ":") {
								        continue;
								    }
									if (!isset($value["totalInboundCount"]))
									{
										$value["totalInboundCount"] = 0;
									}
									if (!isset($value["totalInboundMinutes"]))
									{
										$value["totalInboundMinutes"] = 0;
									}
									if (!isset($value["totalOutboundCount"]))
									{
										$value["totalOutboundCount"] = 0;
									}
									if (!isset($value["totalOutboundMinutes"]))
									{
										$value["totalOutboundMinutes"] = 0;
									}
									$exp = explode(":", $key);
									$id = $exp[0];
									$key = $exp[1];
									echo "<tr>";
									echo "<td class=\"callSums \" style='width:13%;'>" . $value["serviceProvider"] . "</td>";
									echo "<td class=\"callSums \" style='width:13%;'>" . $value["customerId"] . "</td>";
									if (strlen($key) > 1)
									{
									    $responsibleParty[] = $id . ":" . $key;
										echo "<td id=\"" . $key . "\" class=\"callSums \" style='width:13%;'><span href=\"#\" id=\"" . $id . ":" . $key . "\" data-groupId=\"" . $value["customerId"] . "\">" . $key . "</span></td>";
									}
									else
									{
									    $responsibleParty[] = $id;
										echo "<td id=\"" . $id . "\" class=\"callSums \" style='width:13%;'><span href=\"#\" id=\"" . $id . "\" data-groupId=\"" . $value["customerId"] . "\">" . $id . "</span></td>";
									}
									echo "<td class=\"callSums \" style='width:13%;'>" . $value["totalInboundCount"] . "</td>";
									echo "<td class=\"callSums \" style='width:13%;'>" . floor($value["totalInboundMinutes"]/60) . ":" . str_pad($value["totalInboundMinutes"]%60, 2, "0", STR_PAD_LEFT) . "</td>";
									echo "<td class=\"callSums \" style='width:13%;'>" . $value["totalOutboundCount"] . "</td>";
									echo "<td class=\"callSums \" style='width:13%;'>" . floor($value["totalOutboundMinutes"]/60) . ":" . str_pad($value["totalOutboundMinutes"]%60, 2, "0", STR_PAD_LEFT) . "</td>";
									echo "</tr>";
								}
							} else {
							    echo "<tr><td style='text-align:center;'> No Data Found </td></tr>";
							}
							
							$_SESSION['responsibleParty'] = array_unique($responsibleParty);
						?>
					</tbody>
				</table>
		</div>
		</div>
		</div>
</div>
<div id="callDetail"></div>
