$(function()
{
	
	$('#downloadCDRCSV').click(function() {
        var titles = [];
        var data = [];
        $('#summaryEnt th').each(function() {    //table id here
          titles.push($(this).text());
        });

     
        $('#summaryEnt td').each(function() {    //table id here
          data.push($(this).text());
        });
       
       
        var CSVString = prepCSVRow(titles, titles.length, '');
        CSVString = prepCSVRow(data, titles.length, CSVString);

        var blob = new Blob(["\ufeff", CSVString]);
        var fileName = "CallDetailReport.csv";
        if (navigator.msSaveBlob) { // IE 10+
        	navigator.msSaveBlob(blob, fileName);
        }else{
        	var downloadLink = document.createElement("a");
            
            var url = URL.createObjectURL(blob);
            downloadLink.href = url;
            downloadLink.download = fileName;
           
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);
        }
       
        
      });

      
});

function prepCSVRow(arr, columnCount, initial) {
    var row = '';
    var delimeter = ',';
    var newLine = '\r\n';

    function splitArray(_arr, _count) {
      var splitted = [];
      var result = [];
      _arr.forEach(function(item, idx) {
        if ((idx + 1) % _count === 0) {
          splitted.push(item);
          result.push(splitted);
          splitted = [];
        } else {
          splitted.push(item);
        }
      });
      return result;
    }
    var plainArr = splitArray(arr, columnCount);
   
    plainArr.forEach(function(arrItem) {
      arrItem.forEach(function(item, idx) {
        row += item + ((idx + 1) === arrItem.length ? '' : delimeter);
      });
      row += newLine;
    });
    return initial + row;
  }