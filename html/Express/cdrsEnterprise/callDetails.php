<?php
        
	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	//retrieve user names from BroadSoft
	require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	require_once("/var/www/lib/broadsoft/adminPortal/getAllNumberAssignments.php");
	//require_once("/var/www/lib/broadsoft/adminPortal/getAllUsers.php");
// 	$query = "SELECT SQL_CALC_FOUND_ROWS * from broadsoft_cdrs where customerId='" . $_POST["groupId"] . "'";
	$groupIds = implode("','", $_SESSION['groupListCDRsDetail']);
// 	print_r($_SESSION['responsibleParty']); exit;
	$query = "SELECT SQL_CALC_FOUND_ROWS * from broadsoft_cdrs where binary customerId IN ('".$groupIds."')";
	$and = "";
	$filters = array("inbound", "outbound", "outboundExternal", "inboundoutbound", "international", "tollFree", "dirAsst", "emergencyAsst");
	 
        //Code added @ 05 Feb 2019
        if ($_POST["from"] !== "0"){
		$from = $_POST["from"];
		$and .= " AND startDate>='" . $from . "'";
	}if ($_POST["to"] !== "0"){
		$to = $_POST["to"];
		$and .= " and startDate<='" . $to . "'";
	}if ($_POST["called"] !== "0"){
		$called = $_POST["called"];
		$and .= " AND calledNumber like '%" . $called . "%'";
	}if ($_POST["calling"] !== "0"){
		$calling = $_POST["calling"];
		$and .= " AND callingNumber like '%" . $calling . "%'";
	}if ($_POST["department"] !== "0"){
		$department = $_POST["department"];
		require_once("getDeptUsers.php");
		$and .= " AND responsibleParty in (" . rtrim($deptUsers, ",") . ")";
	}
        //End code
        
	
	if ($_POST["userId"] !== "0")
	{
		if (!in_array($_POST["userId"], $filters))
		{
			$exp = explode(":", trim($_POST["userId"]));
			$userId = $exp[0];

			if ($exp[1])
			{
				$userName = $exp[1];
			}
			else
			{
				$userName = $exp[0];
			}
			$and .= " AND responsibleParty='" . $userId . "'";
		} else {
		    $responsibleParty = $groupIds = implode("','", $_SESSION['responsibleParty']);
		    $and .= " AND responsibleParty IN ('".$responsibleParty."')";
		}
	}

	 
	if ($_POST["filter"] == "inbound")
	{
		$userName = "Inbound";
		$and .= " AND direction='Terminating'";
	}
	if ($_POST["filter"] == "outbound")
	{
		$userName = "Outbound";
		$and .= " AND direction='Originating'";
	}
	if ($_POST["filter"] == "outboundExternal") //for Rhema Bible Church only
	{
		$userName = "Outbound (External Only)";
		$and .= " AND direction='Originating'";
		$and .= " AND calledNumber<>'pingtone.com' AND length(calledNumber)>=10 AND calledNumber not like '011%' AND answerIndicator<>'No'";
		foreach ($tollFree as $key => $value)
		{
			$and .= " AND calledNumber not like '" . $value . "%'";
		}
		foreach ($numberAssignments as $key => $value)
		{
			$key = explode("-", $key);
			$key = $key[1];
			$and .= " AND calledNumber not like '%" . $key . "'";
		}
	}
	if ($_POST["filter"] == "inboundoutbound")
	{
		$userName = "Inbound + Outbound";
		$and .= " AND (direction='Originating' OR direction='Terminating')";
	}
	if ($_POST["filter"] == "international")
	{
		$userName = "International";
		$and .= " and calledNumber like '011%'";
	}
	if ($_POST["filter"] == "dirAsst")
	{
		$userName = "Directory Assistance";
		$and .= " and calledNumber='411'";
	}
	if ($_POST["filter"] == "emergencyAsst")
	{
		$userName = "Emergency Assistance";
		$and .= " and calledNumber='911'";
	}
	if ($_POST["filter"] == "tollFree")
	{
		$userName = "Toll Free";
		$and .= " AND (";
		foreach ($tollFree as $value)
		{
			$and .= "calledNumber like '" . $value . "%' or ";
		}
		$and = substr($and, 0, -3);
		$and .= ") AND direction='Terminating'";
	}
	$query .= $and . " order by startDate desc";
	$_SESSION["query"] = $query;
//echo $query;
	$limit = 1000;
	$query .= " limit " . $limit; //add limit to query for result display
	openssl_error_string();
	$sth = $billDB->query($query);
// 	print_r($sth); echo"uuuuuuuuuuuu"; exit;
	//get total rows without limit
	$query2 = "SELECT FOUND_ROWS() as total";
	openssl_error_string();
	$sth2 = $billDB->query($query2);
?>
<script>
	$(function()
	{
		$("#usercallDetails").tablesorter();
	});
</script>
<script>
var sourceSwapImage = function() {
            var thisId = this.id;
            var image = $("#"+thisId).children('img');
            var newSource = image.data('alt-src');
            image.data('alt-src', image.attr('src'));
            image.attr('src', newSource);
        }
        
		$("#downloadCSV").hover(sourceSwapImage, sourceSwapImage);
</script>
<style>
/*#usercallDetails tbody td {
    border-bottom: 1px solid #ccc;
} */
</style>
 

<!--  <input type="button" name="downloadCSV" id="downloadCSV" value="Download as CSV" onclick="location.href='cdrs/printCSV.php?userName=<?php echo $userName; ?>';">
     -->

<h2 class="fcorn-registerTwo adminUserText"><?php echo $userName; ?></h2>

<?php
	if (isset($called))
	{
		echo "<div class=\"miniBanner\">Called Number: " . $called . "</div>";
	}
	if (isset($calling))
	{
		echo "<div class=\"miniBanner\">Calling Number: " . $calling . "</div>";
	}
	if (isset($department))
	{
		echo "<div class=\"miniBanner\">Department: " . $department . "</div>";
	}

	//if total number of matching records is greater than limit, show warning and direct user to CSV file
	while ($row2 = $sth2->fetch(PDO::FETCH_ASSOC))
	{
		if ($row2["total"] > $limit)
		{
			echo "<div class=\"centerDesc\" style=\"color:red;\">WARNING: Total number of records is too large to display. For full results, click the Download as CSV button above.</div>";
		}
	}
?>
 
 <div class="row delDownIconDivWidth">
	<div class="col-md-6"></div>
    	<div class="col-md-6 csvFileDownloadAlign">
    		 
    			<div class="col-md-11"></div>
    			<div class="col-md-1 csvFileDownloadAlign">   				
         			<div id="downloadCSV1" style="margin:0px !important;">
            			<div id="downloadCSV">
        	    			<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" onclick="location.href='cdrsEnterprise/printCSV.php?userName=<?php echo $userName; ?>';"/><br><span>Download<br>CSV</span>
            			</div>
         			</div>        			
    			</div>
    
    
    	 
    
    	 </div>
</div>
<div class="">

<div class="">

	
<div style="zoom: 1;">
	<table id="usercallDetails" class="tablesorter scroll">
		<thead>
			<tr>
				<th style="width:7%">Customer Id</th>
				<th style="width:7%">Start Date</th>
				<th style="width:7%">Start Time</th>
				<th style="width:7%">End Date</th>
				<th style="width:7%">End Time</th>
				<th style="width:10%">Responsible Party</th>
				<th style="width:7%">User</th>
				<th style="width:7%">Direction</th>
				<th style="width:9%">Answer Indicator</th>
				<th style="width:9%">Calling Number</th>
				<th style="width:9%">Called Number</th>
				<th style="width:7%">Country</th>
				<th style="width:7%">Duration(mins)</th>
			</tr>
		</thead>
		<tbody>
			<?php
                                
				while ($row = $sth->fetch(PDO::FETCH_ASSOC))
				{
					if (strlen($row["customerId"]) > 10)
					{
						$custId = substr($row["customerId"], 0, 10) . "...";
					}
					else
					{
						$custId = $row["customerId"];
					}
                                        $groupIdTmp = trim($row["customerId"]);
                                        $serviceProvider = trim($row["serviceProvider"]);
                                        //$userPhone = array();
                                        $userPhone = getUserNameDetails($sessionid, $client, $serviceProvider, $groupIdTmp);
                                        
					//echo "<tr style=\"padding:8px;\">";
					echo "<tr>";
					echo "<td style=\"width:7%\">" . $custId . "</td>";
					echo "<td style=\"width:7%\">" . $row["startDate"] . "</td>";
					echo "<td style=\"width:7%\">" . $row["startTime"] . "</td>";
					echo "<td style=\"width:7%\">" . $row["endDate"] . "</td>";
					echo "<td style=\"width:7%\">" . $row["endTime"] . "</td>";
					echo "<td style=\"width:10%\">" . $row["responsibleParty"] . "</td>";
					echo "<td style=\"width:7%\">" . (isset($userPhone[$row["responsibleParty"]]["name"]) ? $userPhone[$row["responsibleParty"]]["name"] : "") . "</td>";
					echo "<td style=\"width:7%\">" . $row["direction"] . "</td>";
					echo "<td style=\"width:9%\">" . $row["answerIndicator"] . "</td>";
					echo "<td style=\"width:9%\">" . $row["callingNumber"] . "</td>";
					echo "<td style=\"width:9%\">" . $row["calledNumber"] . "</td>";
					echo "<td style=\"width:7%\">";

					//display country for international calls
					if (strpos($row["calledNumber"], "011") === 0)
					{
						$internationalNumber = substr($row["calledNumber"], 3); //remove "011" from beginning of number
						$intQuery = "select destinationName from internationalCodes where '" . $internationalNumber . "' like concat(countryCode, digits, '%') order by length(digits) desc limit 1";
						$intResults = $db->query($intQuery);
						if ($intRow = $intResults->fetch(MYSQL_ASSOC))
						{
							echo $intRow["destinationName"];
						}
						else
						{
							echo "N/A";
						}
					}

					echo "</td>";
					echo "<td style=\"width:7%\">" . floor($row["duration"]/60) . ":" . str_pad($row["duration"]%60, 2, "0", STR_PAD_LEFT) . "</td>"; //minutes and seconds
					echo "</tr>";
				}
				
				if($sth->rowCount() == 0) {
				    echo "<tr><td colspan='13'>No Data Found.</td></tr>";
				}
			?>
		</tbody>
	</table>
	</div>
</div>
</div>
<?php 
function getUserNameDetails($sessionid, $client, $serviceProvider, $groupId)
{
    $xmlinput = xmlHeader($sessionid, "UserGetListInGroupRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvider) . "</serviceProviderId>";
	$xmlinput .= "<GroupId>" . htmlspecialchars($groupId) . "</GroupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);

	$userCheck = $xml;
	$a = 0;        
	if (isset($xml->command->userTable->row))
	{
		foreach ($xml->command->userTable->row as $key => $value)
		{
			$chkUserId[$a] = strval($value->col[0]);
			$users[$a]["id"] = strval($value->col[0]);
			$users[$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
			$users[$a]["phoneNumber"] = strval($value->col[4]);
                        $users[$a]["extension"] = strval($value->col[10]);
			$a++;
			$ext = strval($value->col[10]);
                        $phoneNum = str_replace("-", "", strval($value->col[4]));
                        if(empty($phoneNum)){
                            $phoneNum = strval($value->col[10]);
                        }
			$userPh[$phoneNum]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
		}
           return $userPh;     
	}

	if (isset($users))
	{
		$users = subval_sort($users, "name");
	}
        
}

?>
