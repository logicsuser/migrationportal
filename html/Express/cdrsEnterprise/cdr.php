<?php
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/ServiceProviderOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupOperation.php");
$spObjTmp = new ServiceProviderOperations();
?>

<?php 

function getAllGroupSystemWideListOnly($serviceProviders) {
        $sp = 0;
        $groupId = 0;
        $systemWideGroup = array();
        $group = new GroupOperation($sp, $groupId);
        foreach($serviceProviders as $spKey => $sp) {
            $group->sp = $sp;
            $groupListResponse = $group->getGroupList();
            foreach ($groupListResponse as $groupData) {
                $groupData = explode("<span class='search_separator'>-</span>", $groupData);
                $systemWideGroup[] = trim($groupData[0]);
            }
        }
        return $systemWideGroup;
}

function getServiceProvidersList($selectedSp)
{
//     $serviceProviders[] = $selectedSp;
    $spObj = new ServiceProviderOperations();
    $serviceProviders = $spObj->getSystemServerProviderList();
    $str = "";
    foreach ($serviceProviders as $v) {
        if($_SESSION['superUser'] == "3"){
            if(in_array($v, $_SESSION['spListNameId'])){
                $str .= "<option value=\"" . $v . "\"";
                if($selectedSp == $v){
                    $str .= " selected";
                }
                $str .= ">" . $v . "</option>";
            }
        }
        else{            
            $str .= "<option value=\"" . $v . "\"";
            if($selectedSp == $v){
                $str .= " selected";
            }
            $str .= ">" . $v . "</option>";            
        }
        
    }
    return $str;
}
?>
<style>
.cdrsFormDiv{width:860px; margin:0 auto;}
.cdrsFormDiv{width:860px; margin:0 auto;}
div#ui-datepicker-div {
    top: 52px !important;
    left: 15px !important;
}
</style>

<script type="text/javascript" src="cdrsEnterprise/js/downloadAsCsv.js"></script>
<script>

function initial(val) {
    switch(val)
    {
       case 'system':
             $("#loadingUsers").show();
             $('#DNUsersList').html('');
             var tempUrl = "cdrsEnterprise/getDnInfo.php";
             $.ajax({
		type: "POST",
		url: tempUrl,
		data: { cdrLevel: 'system'},
		success: function(result)
		{
                    $("#loadingUsers").hide();
                    $('#DNUsersList').html(result);
		}
             });                
             $('.enterpriseDiv').hide();
             break;
       case 'enterprise':
             var selectedEnt = $('#enterprise').val();
             $("#loadingUsers").show();
             $('#DNUsersList').html('');
             var tempUrl = "cdrsEnterprise/getDnInfo.php";
             $.ajax({
		type: "POST",
		url: tempUrl,
		data: {enterpriseId: selectedEnt, cdrLevel: 'enterprise'},
		success: function(result)
		{           
                    $("#loadingUsers").hide();
                    $('#DNUsersList').html(result);
		}
             });
             $('.enterpriseDiv').show();
             break;
   }
}

function getCDRTableData(numberOfRow) {
	var url = "cdrsEnterprise/getCdrs.php";
	$("#cdrResults").html("");
	var dataToSend = $("#cdrsForm").serializeArray();
	dataToSend.push({name: 'limitTableRow', value: numberOfRow});
	$.ajax({
		type: "POST",
		url: url,
		data: dataToSend,
		success: function(result)
		{
			if( result.indexOf("Group Not Exist") != -1 ) {
				alert( $("#selectGroupCDRs").val() + " Group Not Found.");
			} else {
				$("#cdrResults").html(result);
				$("#cdrResults").show();
				$(".limitDropdownDiv").show();
				$("#downloadCDRCSVDiv").show();
			}
			$(".loadingTable").hide();
		}
	});
}

function groupAutoComplete(autoComplete) {
    $("#selectGroupCDRs").autocomplete({
        source: autoComplete,
        appendTo: "#hidden-stuffCDRS"
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        ul.addClass('groupAutoClass'); //Ul custom class here
		if(item.value.search('<span class="search_separator">-</span>') > 0){
			return $("<li></li>")
			.append("<a href='#'>" + item.value + "</a>")
			.data("ui-autocomplete-item", item)
			.appendTo(ul);
		}else{
			return $("<li class='ui-menu-item'></li>")
            .append("<a href='#'>" + item.label + "</a>")
            .data("ui-autocomplete-item", item)
            .appendTo(ul);						
		}
    };
}

$(document).ready(function() {


        //Code added @ 05 feb 2019
        initial('enterprise');
        $("#from").datepicker({
		defaultDate: "-4w",
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function(selectedDate) {
			$("#to").datepicker("option", "minDate", selectedDate);
		},
                beforeShow: function(input, obj) {
                    $(input).after($(input).datepicker('widget'));
                }
	});
	$("#to").datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function(selectedDate) {
			$("#from").datepicker("option", "maxDate", selectedDate);
		},
                beforeShow: function(input, obj) {
                    $(input).after($(input).datepicker('widget'));
                }
	});
        //End code

	/* Ent CDRs start*/
	$(".limitDropdownDiv").hide();
	$("#loadingGroups").show();
	$("#selectGroupCDRs").hide();
	$(".loadingTable").show();
	getCDRTableData("All Rows");
	
	var showGroupListInSearch = function(){
// 		var searchLevel = $("input[name='searchlevel']").val();
		var searchLevel = $("#searchlevelSys").prop('checked') ? $("#searchlevelSys").val() : $("#searchlevelEnt").val();
		var selectedEnt = $("#enterprise").val();                
		var autoComplete = new Array();
	    $.ajax({
	        type: "POST",
	        data: {searchLevel: searchLevel, enterpriseId: selectedEnt},
	        url: "cdrsEnterprise/groupList.php",
	        success: function(result)
	        {
	        	var obj = jQuery.parseJSON(result);
				for (var i = 0; i < obj.length; i++) {
					autoComplete[i] = obj[i];
				}
				groupAutoComplete(autoComplete);
	            $("#loadingGroups").hide();
	            $("#selectGroupCDRs").show();
// 	            $("#submitEntCDRs").prop('disabled', false);
	            
	        }
	    });
	};
	showGroupListInSearch();
	
    $("input[name='searchlevel'], #enterprise").on('change', function() {
		var currentEnt = "<?php echo $_SESSION["sp"]; ?>";
		var defaultGroup = "All Groups";
        $("#selectGroupCDRs").val(defaultGroup);
		var autoComplete = new Array();
		groupAutoComplete(autoComplete);
        var val = $(this).val();
                var searchLevel = $("#searchlevelSys").prop('checked') ? $("#searchlevelSys").val() : $("#searchlevelEnt").val();
		initial(searchLevel);
		showGroupListInSearch();
		//$('#enterprise option[value=currentEnt]').attr('selected','selected');
    });

    $("#limitTableRow").on('change', function() {
    	$("#downloadCDRCSVDiv").hide();
    	$(".loadingTable").show();
        var val = $(this).val();
        	getCDRTableData(val);
    });

    
    $("#submitEntCDRs").click(function()
    {
        $(".loadingTable").show();
        $(".limitDropdownDiv").hide();
        var defaultNumberOfRow = "All Rows";
        getCDRTableData(defaultNumberOfRow);
    });

	$('#selectGroupCDRs').on('keyup keypress', function(e) {
		var keyCode = e.keyCode || e.which;
		
        if (keyCode === 38) 
        {
        	var selectGroupId = $("#selectGroupCDRs").val();
            var tmpFormatedGroupId = selectGroupId.replace("<span class='search_separator'>-</span>", "-");
            $('#selectGroupCDRs').val(tmpFormatedGroupId);                        
        }
        if (keyCode === 40) 
        {
            var selectGroupId = $("#selectGroupCDRs").val();
            var tmpFormatedGroupId = selectGroupId.replace("<span class='search_separator'>-</span>", "-");
			$('#selectGroupCDRs').val(tmpFormatedGroupId);                        
        }
        
        if(keyCode === 13) {
        	var selectGroupId = $("#selectGroupCDRs").val();
			if(selectGroupId.indexOf("<span class='search_separator'>-</span>") != -1 ) {
	    		var selectGroupVal = selectGroupId.replace("<span class='search_separator'>-</span>", "-");
	    		$('#selectGroupCDRs').val(selectGroupVal);
			}
        	
        		if(e.type == "keyup") {
        			$("#submitEntCDRs").trigger("click");
            	}
        		return false;        	
        	}
       
	});

		$(document).on("click", ".groupAutoClass > li > a", function(){
			var el = $(this);
			var selectGroupId = el.html();
			
			var groupVal = decodString(el[0].innerHTML.replace('<span class="search_separator">-</span>', '-'));
			$('#selectGroupCDRs').val(decodString(groupVal));
			
		});
	
});
$("#allUsers2").tablesorter();
	function decodString(encodedStr) {
		var parser = new DOMParser;
		var dom = parser.parseFromString(
		    '<!doctype html><body>' + encodedStr,
		    'text/html');
		var decodedString = dom.body.textContent;
		return decodedString;
	}
</script>

<?php

//require_once("getDnInfo.php");  //Line commented because we are retrieving users list from ajax call
require_once("/var/www/lib/broadsoft/adminPortal/getDepartments.php");
?>
<div class="selectContainer">

<div class="row cdrsFormDiv">
	<div class="col-md-12">	
    		<div class="form-group">
				<h2 class="adminUserText" id="showCDRName"> Call Detail Report </h2>
    		</div>
	</div>
</div>



<form action="#" method="POST" id="cdrsForm" class="cdrsForm fcorn-registerTwo cdrsFormDiv">
	<?php 
                $cssStyle = "style='display: none; text-align:center;'";
                if($_SESSION["superUser"] == "1"){
                        $cssStyle = "style='display: block; text-align:center;'";
                }
        ?>
	<div class="row">
            <div class="form-group" <?php echo $cssStyle; ?> >
                <label class="labelText" for="limitSearchTo" style="margin-right: 20px;">Search In :</label>
                <input type="radio" class="searchlevel" name="searchlevel" id="searchlevelSys" value="system" style="width: 5%;">
                <label for="searchlevelSys">
                        <span></span>
                </label>
                <label class="labelText" for="">System</label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" class="searchlevel" name="searchlevel" id="searchlevelEnt" value="enterprise" checked="" style="width: 5%;">
                        <label for="searchlevelEnt"><span></span></label>
                        <label class="labelText" for="">Enterprise</label>
            </div>
        </div>
	
    <div class="row cdrsFormDiv">
    	<div class="col-md-6 enterpriseDiv">	
    		<div class="form-group">
				<label class="labelText" for="enterprise">Enterprise</label>
				<div class="dropdown-wrap">
					<select name="enterprise" id="enterprise"><?php echo getServiceProvidersList($_SESSION["sp"]); ?></select>
    			</div>
    		</div>
		</div>
		
		<div class="col-md-6">
    		<div class="form-group">
				<label class="labelText" for="to">Groups</label>
				<div class="loading" style="margin-top: 11px;" id="loadingGroups"><img src="/Express/images/ajax-loader.gif"></div>
				<input class="autoFill magnify ui-autocomplete-input" type="text" name="groupName" id="selectGroupCDRs" size="" value="All Groups">
				<div id="hidden-stuffCDRS" style="height: 0px;"></div>
    		</div>
		</div>
     </div>
    
    <!-- Code added @ 05 Feb 2019 -->
    <!--<div class="row cdrsFormDiv">	 
	<div class="col-md-6" style="padding:0px">	
		<div class="form-group">
			<input type="radio" name="cdrs" id="cdrs1" checked="checked" value="1" />
			<label class="labelText" for="cdrs1"><span></span>Call Detail Report</label>
			<input type="radio" name="cdrs" id="cdrs2" value="2" />
			<label class="labelText" for="cdrs2"><span></span>CDR Summary</label>
		</div>
	</div>	 
    </div>-->
    <div class="row cdrsFormDiv">
    	<div class="col-md-6">	
    		<div class="form-group">
				<label class="labelText" for="from">From Date</label>
				<input type="text" name="from" id="from" size="12">
    		</div>
		</div>
		
		<div class="col-md-6">	
    		<div class="form-group">
				<label class="labelText" for="to">To Date</label>
				<input type="text" name="to" id="to" size="12">
    		</div>
		</div>
     </div>
    <div id="hideFields">
	
    	<div class="row cdrsFormDiv">
        	<div class="col-md-6">
            	<div class="form-group">
					<label class="labelText" for="user">User</label>
                                        <div class="loading" style="margin-top: 11px;" id="loadingUsers"><img src="/Express/images/ajax-loader.gif"></div>
					<div id='DNUsersList'>
                                            &nbsp;&nbsp; 
                                        </div>
            	</div>
        	</div>
        	<div class="col-md-6"></div>
             
       	</div>
       	
       	
       	<div class="row cdrsFormDiv">
        	<div class="col-md-6">	
            	<div class="form-group">
            		<label class="labelText" for="calledNum">Called Number</label>
					<input value="" type="text" name="calledNum" id="calledNum" class="form-control date_field">
            	</div>
            </div>
            <div class="col-md-6">	
        		<div class="form-group">
        			<label class="labelText" for="callingNum">Calling Number</label>
					<input value="" type="text" name="callingNum" id="callingNum" class="form-control date_field">
        		</div>
        	</div>
        </div>
       	
       	
       	<div class="row cdrsFormDiv" <?php echo "style= \"display: " . ($useDepartments == "true" ? "block" : "none") . "\""; ?>>
        	<div class="col-md-6">	
        		<div class="form-group">
					<label class="department labelText" for="calledNum">Department</label>
					<div class="dropdown-wrap">   
                		<select name="department" id="department">
                        	<option value=""></option>
                            <?php
                            if (isset($departments))
                            {
                                foreach ($departments as $key => $value)
                                {
                                    echo "<option value=\"" . $value . "\">" . $value . "</option>";
                                }
                            }
                            ?>
    					</select>
					</div>
            	</div>
            </div>
            <div class="col-md-6"></div>
       	</div>
       	
    </div>
    <!-- End code -->
    
<!--     <input name="selectedGroupVal" id="selectedGroupVal" type="hidden" value="All Groups">	 -->
    <div class="alignBtn">
    	<input class="subButton" type="button" id="submitEntCDRs" value="Submit">
    </div>
</form>

<!-- Limit to Table data -->
<div class="row limitDropdownDiv">
 <div class="col-md-3">
    <div class="form-group">
     	<label class="labelText" for="to">Rows</label>
			<div class="dropdown-wrap" style="width: 395px;">   
				<select name="limitTableRow" id="limitTableRow">
					<option value="All Rows">All Rows</option>
					<option value="10">10</option>
					<option value="20">20</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="500">500</option>
				</select>
			</div>
        </div>
    </div>
	
	<div id="downloadCDRCSVDiv" style="">
		<img src="images/icons/download_csv.png" data-alt-src="images/icons/download_csv_over.png" id="downloadCDRCSV">
 		<br><span>Download<br>CSV</span>
	</div>
</div>

<!-- <div class ="desc" id="CDR1"> -->
<div id="cdrResults"></div>
<!-- </div> -->

<div class="loadingTable" style="text-align: center;" id="loadingGroups"><img src="/Express/images/ajax-loader.gif"></div>
</div>

