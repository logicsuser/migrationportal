<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupOperation.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ServiceProviderOperations.php");

$searchLevel = $_POST['searchLevel'];
$groupId = 0;
unset($_SESSION["ExistedGroups"]);

// $searchLevel = "system";
    
    
if($searchLevel == "system") {
    $spObj = new ServiceProviderOperations();
    $serviceProviders = $spObj->getSystemServerProviderList();
} else if($searchLevel == "enterprise") {
    $serviceProviders[] = $_POST['enterpriseId'];
}

$group = new GroupOperation($sp, $groupId);

$systemWideGroup = array();
$systemWideGroup[] = "All Groups";
foreach($serviceProviders as $spKey => $sp) {
    $group->sp = $sp;
    $groupListResponse = $group->getGroupList();
    foreach ($groupListResponse as $groupData) {
        $groupDataKey = explode("<span class='search_separator'>-</span>", $groupData);
        $groupDataVal = str_replace("<span class='search_separator'>-</span>","-", $groupData);
        $_SESSION["ExistedGroups"][trim($groupDataKey[0])] = trim($groupDataVal);
        $systemWideGroup[] = $groupData;
    }
}

// print_r($_SESSION["ExistedGroups"]); exit;
echo json_encode($systemWideGroup);

?>