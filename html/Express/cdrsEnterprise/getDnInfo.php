<?php

	require_once("/var/www/lib/broadsoft/login.php");
	checkLogin();

	require_once("/var/www/lib/broadsoft/adminPortal/getDNAssignments.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/ServiceProviderOperations.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/groupManage/groupModify/GroupOperation.php");
        
        $spObjTmp = new ServiceProviderOperations();
        
        if($_POST['cdrLevel'] == "system"){ 
                 $sPLists    = $spObjTmp->getSystemServerProviderList();
        	 $groupList = getAllGroupSystemWideListOnly($sPLists);
        }
        
        if($_POST['cdrLevel'] == "enterprise"){
                $enterpriseId = $_POST['enterpriseId'];
                $sPList[]    = $enterpriseId;
                $groupList   = getAllGroupSystemWideListOnly($sPList);
        }
                
        //Code added @ 05 Feb 2019 regarding EX-848 
        $dns = array();
   
        $groupEnteropriseArr = getGroupEnterpriseList($sessionid, $client);

        foreach($groupList as $groupId){        
              if(array_key_exists($groupId, $groupEnteropriseArr)){
                 $sp      = $groupEnteropriseArr[$groupId];
                 $groupId = $groupId;
             }
             $dns = getDNAssignmentsList($sessionid, $client, $sp, $groupId, $dns);
         }
   
        $whrCndn  = " WHERE 1=1 ";
        $whrCndn .= " AND BINARY customerId IN ('".implode("','", $groupList)."') ";

        $query = "SELECT distinct responsibleParty from broadsoft_cdrs  $whrCndn ";
        $sth = $billDB->query($query);
        while ($row = $sth->fetch(PDO::FETCH_ASSOC))
        {
                $resp = $row["responsibleParty"];

                $users[$resp]["id"] = $resp;
                if (isset($dns) and array_key_exists($resp, $dns))
                {
                        $users[$resp]["name"] = $dns[$resp];
                }
                else
                {
                        $users[$resp]["name"] = $resp;
                }
        }

        if (isset($users))
        {
                $users = subval_sort($users, "name");
        }
        
    function getDNAssignmentsList($sessionid, $client, $sp, $groupId, $dns)
        {
                if ($ociVersion == "17")
                {
                        $xmlinput = xmlHeader($sessionid, "GroupDnGetAssignmentListRequest");
                }
                else
                {
                        $xmlinput = xmlHeader($sessionid, "GroupDnGetAssignmentListRequest18");
                }
                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($sp) . "</serviceProviderId>";
                $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
                $xmlinput .= xmlFooter();
                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
                foreach ($xml->command->dnTable->row as $key => $value)
                {
                        $exp = explode("-", strval($value->col[0]));
                        $number = "+1" . $exp[1];
                        $extension = substr($number, -4);

                        if ($ociVersion == "17")
                        {
                                $name = str_replace(",", ", ", strval($value->col[1]));
                        }
                        else
                        {
                                $name = strval($value->col[4]) . ", " . strval($value->col[5]);
                        }
        

                        $dns[$number]    = $name;
                        $dns[$extension] = $name;
                }
                return $dns;
                //openssl_error_string();
   }
   
    
    
    function getGroupEnterpriseList($sessionid, $client){
            
            $xmlinput = xmlHeader($sessionid, "GroupGetListInSystemRequest");
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                $groupEntprieseIdArray = array () ;

                foreach ($xml->command->groupTable->row as $key => $value) {
                $groupId = strval($value->col[0]);
                        $groupEntprieseIdArray[$groupId]  =  strval($value->col[3]) ;
             }
                 return  $groupEntprieseIdArray ;
    }
    
    function getAllGroupSystemWideListOnly($serviceProviders) {
        $sp = 0;
        $groupId = 0;
        $systemWideGroup = array();
        $group = new GroupOperation($sp, $groupId);
        foreach($serviceProviders as $spKey => $sp) {
            $group->sp = $sp;
            $groupListResponse = $group->getGroupList();
            foreach ($groupListResponse as $groupData) {
                $groupData = explode("<span class='search_separator'>-</span>", $groupData);
                $systemWideGroup[] = trim($groupData[0]);
            }
        }
        return $systemWideGroup;
}
    
    //End code
?>
<div class="dropdown-wrap">  	
        <select name="user" id="user">
            <option value=""></option>
            <?php
                    if (isset($users))
                    {
                            foreach ($users as $key => $value)
                            {
                                    echo "<option value=\"" . $value["id"] . "\">" . $value["name"] . "</option>";
                            }
                    }
            ?>
        </select>
</div>