<?php
$settings = parse_ini_file("/var/www/adminPortal.ini", TRUE);
require_once("db.php");
require_once("config.php");
require_once("functions.php");

require_once ("/var/www/lib/broadsoft/adminPortal/PreventPasswordOperation.php");

$error_message = "";
$user = null;
$expired_token = false;
$token = "";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        
	if (isset($_POST['new_password']) && isset($_POST['confirm_password']) && isset($_POST['token'])) {
                
		if ($_POST['new_password'] == $_POST['confirm_password']) {

			if (isset($_SESSION["loggedInUserName"])) {
				$user = get_user_from_username($_SESSION["loggedInUserName"]);
			} else {
				$token = $_POST['token'];
				$user = get_user_from_token($token);
			}

			if ($user) {

				if (checkPasswordStrength($adminPasswordRules, $_POST['new_password'], $errors)) {

					$smsAuthentication = null;
					$smsToken = null;
					$smsSID = null;
					$smsFrom = null;

					if ($systemConfig = get_systemConfig()) {
						$smsAuthentication = $systemConfig->smsAuthentication;
						$smsToken = $systemConfig->smsToken;
						$smsSID = $systemConfig->smsSID;
						$smsFrom = $systemConfig->smsFrom;
					}

					if ($smsAuthentication == "true" && $user->use_sms_on_password_reset == 1 && $user->user_cell_phone_number) {

						$_SESSION["needTwoFactorAuthentication"] = 1;
						$_SESSION["twoFactorAuthUserName"] = $user->userName;
						$_SESSION["isPasswordReset"] = 1;
						$_SESSION["newPassword"] = $_POST['new_password'];

						header("Location: ./twoFactorAuthentication.php");
						exit;

					}                                         
                                        else {                                            
                                            $endPassword = md5($_POST['new_password']);
                                            $objPPO = new PreventPasswordOperation($user->userName, $endPassword);
                                            if($objPPO->passwordReuseValidationBasedOnPreventPassConfig())
                                            {                                                   
                                                   $error_message = "Password has been used already. Try another.";
                                            }
                                            else
                                            {
                                                    
                                                    update_user_password($_POST['new_password'], $user->userName);
                                                    setLoginSession($user);

                                                    unset($_SESSION['RESET_PASSWORD_MESSAGE']);

                                                    header("Location: ./chooseGroup.php");
                                                    exit;
                                            }
					}

				} else {
					$error_message = "<span style='font-size: 75%;'>" . '<li class=\'error\'>' . implode('</li><li class=\'error\'>',$errors) . '</li>' . "</span>";
				}

			} else {

				$expired_token = true;
			}

		} else {
			$error_message = "Passwords doesn't match. Please try again.";
		}

	}

} else if (isset($_GET['token'])) {
	session_unset();
	$token = $_GET['token'];
	$user = get_user_from_token($token);

} else if (isset($_SESSION["loggedInUserName"])) {

	$user = get_user_from_username($_SESSION["loggedInUserName"]);
}

if (!$user) {
	$expired_token = true;
}

if (!$error_message && isset($_SESSION['RESET_PASSWORD_MESSAGE'])) {
	$error_message = $_SESSION['RESET_PASSWORD_MESSAGE'];
}

require_once("config.php");
require_once("header.php");

$passwordRulesMinimumLength = $adminPasswordRules->getMinimumLength();
$passwordRulesNumberOfDigits = $adminPasswordRules->getNumberOfDigits();
$passwordRulesSpecialCharacters = $adminPasswordRules->getSpecialCharacters();
$passwordRulesUpperCaseLetter = $adminPasswordRules->getUpperCaseLetter();
$passwordRulesLowerCaseLetter = $adminPasswordRules->getLowerCaseLetter();

?>
<style>

	.mainHeader{
		display:none;
	}
		.blueSeparator{
		z-index:0;
	}
	.spAddressDiv {
		display:none;
	}
	.retypeAlignment label{
    margin-bottom:5px ;
    font-family: inherit;
    font-size: 100%;
    font-weight: inherit;
    font-style: inherit;
}

</style>
<div class="leftMenu"></div>
<div id="mainBody">

	<div class="login-bg">
		<div class="fadeInUp" id="bodyForm" style="height: fit-content;">

		<img src="images/icons/express_nuovo_logo_login2.png" />

			<?php
			if ($expired_token) {
				?>
				<div class="error centerDesc">The Link has expired.</div>
					<br/><br/>
					<a href="forgot.php" class="pull-left m-t-mini">
						<small>Forgot password?</small>
					</a>
				<?php
				} else {
				?>
				<h4 class="centerDesc"><strong style="color:#FFF">Reset Password</strong></h4>
				<br/>
				<?php
				if ($error_message) {
				?>
				<div class="error centerDesc"><?php echo $error_message ?></div>
				<br/><br/>
				<?php
				}
				?>
				<form name="reset_password_form" id="login" action="reset_password.php" method="POST">
					<small id="password_strength">
    
    					<?php
    					echo "Your password must be at least $passwordRulesMinimumLength characters long. ";
    					echo "Contain at least $passwordRulesNumberOfDigits digit/s";
    					echo $passwordRulesLowerCaseLetter == 'true' ? ", 1 lower case letter" : "";
    					echo $passwordRulesUpperCaseLetter == 'true' ? ", 1 upper case letter" : "";
    					echo $passwordRulesSpecialCharacters == 'true' ? ", 1 special character" : ""; ?>.
    
    				</small><br/><br/>
    				 
					<input type="hidden" class="inptText" name="username" id="username"
					       value="<?php echo $_SESSION["loggedInUserName"]; ?>">
					<input type="hidden" name="token" id="token" value="<?php echo $token; ?>">
					<input type="hidden" name="action" id="action" value="password">
					<input placeholder="Enter New Password" type="password" class="inptText" name="new_password" id="new_password" size="30" autocomplete='off'/>
				
					<label for="" class="labelText">Retype Password:</label><br>
					<input required placeholder="Verify Password" type="password" class="" name="confirm_password" id="confirm_password" size="30" autocomplete='off' class="inptPass"/><br/><br/>

					<div class="loginSubmitDiv">
					<input type="submit" name="resetSubmit" id="reset_password_submit" value="Submit">
					</div>
				</form>
				<?php
				if (isset($_SESSION["loggedInUserName"])) {
					?>
					<div class="clr"></div>
					<div class="forgotPassDiv">
					<a href="chooseGroup.php" class="m-t-mini">
						<small>Go Back</small>
					</a>
					</div>
					<?php
				}
			}
			?>

		</div>

		<div class="copyright">
			Copyright © AveriStar All rights reserved.
		</div>

	</div>
</div>
<?php

if (!$expired_token) {
	?>

	<script src="/Express/js/jquery-1.17.0.validate.js"></script>
	<script>
        $().ready(function () {

            $.validator.addMethod("strength", function (value) {

                //TextBox left blank.
                if (value.length === 0) {
                    $("#password_strength").addClass("error");
                    return false;
                }

                var password_strength = 1;

                //Validate for length of Password.
                if (value.length < <?php echo $passwordRulesMinimumLength ? $passwordRulesMinimumLength : 8 ?>) {
                    password_strength = 0;
                } else if(value.replace(/[^0-9]/g,"").length < <?php echo $passwordRulesNumberOfDigits ? $passwordRulesNumberOfDigits : 1 ?>) {
                    password_strength = 0;
                } else {
                    //Regular Expressions.
                    var regex = new Array();

	                <?php
	                if( $passwordRulesLowerCaseLetter == 'true') {
	                ?>
                    regex.push("[a-z]"); //Lowercase Alphabet.
	                <?php
	                }
	                ?>
	                <?php
	                if( $passwordRulesUpperCaseLetter == 'true') {
	                ?>
                    regex.push("[A-Z]"); //Uppercase Alphabet.
	                <?php
	                }
	                ?>
	                <?php
	                if( $passwordRulesSpecialCharacters == 'true') {
	                ?>
                    regex.push("[^a-zA-Z0-9]"); //Special Character.
	                <?php
	                }
	                ?>

                    //Validate for each Regular Expression.
                    for (var i = 0; i < regex.length; i++) {
                        if (!new RegExp(regex[i]).test(value)) {
                            password_strength = 0;
                        }
                    }
                }

                if (password_strength === 0) {
                    $("#password_strength").addClass("error");
                    return false;
                }

                $("#password_strength").removeClass("error");
                return true;

            }, '');

            $("#login").validate({
                rules: {
                    new_password: "strength",
                    confirm_password: {
                        required: true,
                        equalTo: "#new_password"
                    }
                },
                messages: {
                    confirm_password: {
                        required: "<text>Please enter the same password as above</text>",
                        equalTo: "<text>Please enter the same password as above</text>"
                    }
                }
            });

        });
	</script>
	<?php

}
?>
<style>
	.error, .error text {

		color: red;
	}
</style>