<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
//print_r($_POST);
require_once ("/var/www/lib/broadsoft/adminPortal/servicePacks/servicePacksController.php");
unset($_POST["enterprise"]);
unset($_POST["clonedServicePackName"]);
$serviceObj = new ServicePacksController();
$spList = $serviceObj->serviceProviderServicePackGetListRequest($_SESSION["sp"]);
$data = $errorTableHeader;
$error = 0;
$nonRequired = array();
foreach ($_POST as $key => $value)
{
    
    if ($value !== ""){
        $bg = CHANGED;
    }else{
        $bg = UNCHANGED;
    }
    if($key == "oldSPName"){
        continue;
    }
    
    if($key == "spName" && $value == ""){
        $error = 1;
        $bg = INVALID;
        $value = "Service Pack name can not be blank.";
    }
    
    if($key == "spName" && $value != ""){
        if(in_array($value, $spList["Success"])){
            $error = 1;
            $bg = INVALID;
            $value = "Service Pack already exist.";
        }
    }
    
    if($key == "qLimitation" && $value != ""){
        if (array_key_exists("limitedQuantity",$_POST)){
            if($_POST["limitedQuantity"] == ""){
                $error = 1;
                $bg = INVALID;
                $value = "Limit to quantity can not be blank.";
            }else if(!is_numeric($_POST["limitedQuantity"])){
                $error = 1;
                $bg = INVALID;
                $value = "Limit to quantity should be numeric.";
            }else{
                $value = $_POST["limitedQuantity"];
            }
            
        }else{
            $value = "unlimited";
        }
    }
    if($key == "limitedQuantity"){
        continue;
    }
    
    if($key == "servicePackUserServiceList"){
        if($value == ""){
            $error = 1;
            $bg = INVALID;
            $value = "The service pack must contain at least one service.";
        }else{
            $expVal = explode(";",$value);
            array_pop($expVal);
            $empVal = implode(", ",$expVal);
            $value = $empVal;
        }
        
    }
    
    if ($bg != UNCHANGED)
    {
        $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:" . $bg . ";\">" . $_SESSION ["servicePackDialogArr"][$key] . "</td><td class=\"errorTableRows\">";
        if (is_array($value))
        {
            foreach ($value as $k => $v)
            {
                $data .= $v . "<br>";
            }
        }
        else if ($value !== "")
        {
            if($key=="softPhoneDeviceAccUserName"){
                if($value=="Soft Phone Device Access UserName is required." || $value=="Soft Phone Device Access UserName is already exist."){
                    $data .= $value;
                }else{
                    $data .= $value;
                }
                
            }else{
                $data .= $value;
            }
        }
        else
        {
            $data .= "None";
        }
        $data .= "</td></tr>";
    }
}
$data .= "</table>";
echo $error . $data;
?>