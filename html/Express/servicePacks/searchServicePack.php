<?php
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/servicePacks/servicePacksController.php");

$usageObj = new ServicePacksController();
$usageList = $usageObj->serviceProviderServicePackGetUtilizationListRequest($_SESSION["sp"]);
$i =0;
$array_product = array();

if(empty($usageList["Error"])){
    if(count($usageList["Success"]) > 0){
        $i = 0;
        foreach ($usageList["Success"] as $key => $value){
            if(!empty($value)){
                foreach ($value as $innerKey => $innerValue){
                    
                    $array_product[$i]["sp"]= $key ;
                    $array_product[$i]["group"]= $innerValue['group'];
                    $array_product[$i]["totalPacks"]= $innerValue['totalPacks'];
                    $array_product[$i]["assigned"]= $innerValue['assigned'];
                    $i++;
                }
            }
            $i++;
        }
        $i++;
    }
}

// natcasesort($array_product);
$dataGroupAccording = array_values($array_product);
//print_r($dataGroupAccording) ;

function getArrayMatchValue($groupValueGet,$dataGroupAccording){
    $rersultData = array();
    $i = 0;
    foreach($dataGroupAccording as $matchValue){
        
        // return $matchValue ;
        if($matchValue['group'] == $groupValueGet){
            
            $rersultData[$i]["sp"]= $matchValue['sp'];
            $rersultData[$i]["totalPacks"]= $matchValue['totalPacks'];
            $rersultData[$i]["assigned"]= $matchValue['assigned'];
            $i++;
            
        }
    }
    return $rersultData ;
}

$dataRee = getArrayMatchValue(trim($_POST['searchGroupName']),$dataGroupAccording);
//print_r($dataRee);
echo '<div class="row" id="serviceUsageTables">';
            
            echo "<div class='col-md-6'>";
            echo "<table class='tableServiceName'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th style='padding:10px 10px 0px 10px !important'>".$_POST['searchGroupName']."</th>";
            echo "</tr>";
            echo "</thead>";
            echo "</table>";
            echo "<table class='scroll tablesorter dataTable' id=''>";
            //echo "<thead><tr><th>".$key."</th></tr></thead>";
            //echo "<thead><tr><th>".$key."</th></tr></thead>";
            echo "<thead>";
            echo "<tr><th class='header thsmall'>Service Pack</th><th class='header thsmall'>Total Packs</th><th class='header thsmall'>Assigned</th></tr>";
            echo "</thead>";
            echo "<tbody>";
            if(count($dataRee) > 0){
                foreach ($dataRee as $innerKey => $innerValue){
                    echo "<tr>";
                    echo "<td>". $innerValue['sp']. "</td>";
                    echo "<td>". $innerValue['totalPacks']. "</td>";
                    echo "<td>". $innerValue['assigned']. "</td>";
                    echo "</tr>";
                }
            }else{
                echo "<tr>";
                echo "<td>No Data Found</td>";
                
                echo "</tr>";
            }
            
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
            
    echo '</div>';
?>