<?php
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/servicePacks/servicePacksController.php");

$obj = new ServicePacksController();

//Code added @ 31 Jan 2019
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
$expProvDB = new ExpressProvLogsDB();
//End code

$serviceArray = array();
foreach ($_POST["servicePackName"] as $key => $value){
    $resp[$value] = $obj->serviceProviderServicePackDeleteRequest($_SESSION["sp"], $value);
}
if(count($resp) > 0){
    foreach ($resp as $key1 => $value1){
        if(empty($value1["Error"])){
            echo $key1." : Service Pack has been deleted.<br />";
            //Code added @ 31 Jan 2019
            $objChngLogUtil  = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
            $objChngLogUtil->changeLogDeleteUtility("Delete Service Pack", $key1, "servicePackModChanges", "");
            //End code
        }else{
            echo $key1." : "; print_r($value1["Error"]); echo "<br />";
        }
    }
}

?>