<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

//print_r($_SESSION["spDetailsSession"]);
$serviceArray = array();
$spChangedArray = array();
$originalPost = $_POST;
$diff = array();
if(!empty($_SESSION["spDetailsSession"])){
    $serviceArray["spName"] = $_SESSION["spDetailsSession"]["servicePackName"];
    $serviceArray["spDescription"] = $_SESSION["spDetailsSession"]["servicePackDescription"];
    $serviceArray["availableForUse"] = $_SESSION["spDetailsSession"]["isAvailableForUse"];
    if(isset($_SESSION["spDetailsSession"]["servicePackQuantity"]["unlimited"])){
        $serviceArray["spQuantity"] = "unlimited";
    }else{
        $serviceArray["spQuantity"] = $_SESSION["spDetailsSession"]["servicePackQuantity"]["quantity"];
    }
    $_SESSION["oldServicePackName"] = $serviceArray["spName"];
}
if(isset($_POST["limitQuantity"])){
    $_POST["spQuantity"] = $_POST["limitQuantity"];
}else{
    $_POST["spQuantity"] = "unlimited";
}
unset($_POST["qLimitation"]);
unset($_POST["limitQuantity"]);
foreach($_POST as $nKey => $nValue){
    if(trim($nValue) != $serviceArray[$nKey]){
        $diff[$nKey] = $nValue;
    }
}
//$diff=array_diff($_POST,$serviceArray);
//print_r($diff);
if(!empty($diff)){
    $data = $errorTableHeader;
    $error = 0;
    //print_r($_SESSION["servicePackToAdd"]);
    $nonRequired = array();
    foreach ($_POST as $key => $value)
    {
        if ($value !== $serviceArray[$key]){
            $bg = CHANGED;
        }else{
            $bg = UNCHANGED;
        }
        
        if($key == "spName"){
            if($value != $serviceArray["spName"]){
                $spChangedArray["servicePackName"]["oldValue"]=$serviceArray["spName"];
                $_SESSION["oldServicePackName"] = $serviceArray["spName"];
                $spChangedArray["servicePackName"]["newValue"]=$value;
                if(trim($value) == ""){
                    $error = 1;
                    $bg = INVALID;
                    $value = "Service Pack name can not be blank.";
                    
                }
            }
        }
        
        if($key == "spQuantity"){
            if($value != $serviceArray["spQuantity"]){
                $spChangedArray["spQuantity"]["oldValue"]=$serviceArray["spQuantity"];
                $spChangedArray["spQuantity"]["newValue"]=$value;
                
                if(array_key_exists("limitQuantity", $originalPost)){
                    if($value == ""){
                        $error = 1;
                        $bg = INVALID;
                        $value = "Limit to quantity can not be blank.";
                    }else if(!is_numeric($value)){
                        $error = 1;
                        $bg = INVALID;
                        $value = "Limit to quantity should be numeric.";
                    }
                    
                }
               
                
            }
        }
        
        if($key == "spDescription"){
            if($value != $serviceArray["spDescription"]){
                $spChangedArray["spDescription"]["oldValue"]=$serviceArray["spDescription"];
                $spChangedArray["spDescription"]["newValue"]=$value;
            }
        }
        if($key == "availableForUse"){
            if($value != $serviceArray["availableForUse"]){
                $spChangedArray["availableForUse"]["oldValue"]=$serviceArray["availableForUse"];
                $spChangedArray["availableForUse"]["newValue"]=$value;
            }
        }
        
            if ($bg != UNCHANGED)
            {
                $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:" . $bg . ";\">" . $_SESSION ["servicePackDialogArr"][$key] . "</td><td class=\"errorTableRows\">";
                if (is_array($value))
                {
                    foreach ($value as $k => $v)
                    {
                        $data .= $v . "<br>";
                    }
                }
                else if ($value !== "")
                {
                    $data .= $value;
                }
                else
                {
                    $data .= "None";
                }
                $data .= "</td></tr>";
            }
        
    }
    $data .= "</table>";
}else{
    $error = 1;
    $data = "You have made no changes.";
}
$_SESSION["servicePackToMod"][$serviceArray["spName"]] = $spChangedArray;
//print_r($spChangedArray);

echo $error . $data;
?>