<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
unset($_SESSION["allServicePackArray"]);
unset($_SESSION["servicePackToAdd"]);
unset($_SESSION["servicePackToMod"]);
unset($_SESSION["servicePackProcessModification"]);

$_SESSION ["servicePackDialogArr"] = array(
    "spName" => "Service Pack Name",
    "spDescription" => "Service Pack Description",
    "availableForUse" => "Available for Use",
    "qLimitation" => "Limit to Quantity",
    "servicePackUserServiceList" => "Assigned Services",
    "servicePackName" => "Service Pack Name",
    "servicePackDescription" => "Service Pack Description",
    "isAvailableForUse" => "Available for Use",
    "servicePackQuantity" => "Limit to Quantity",
    "spQuantity" => "Limit to Quantity"
);
 
$entServicePacksLogsArr               = explode("_", $_SESSION["permissions"]["entServicePacks"]);
$entServicePacksLogPermission         = $entServicePacksLogsArr[0];
$entServicePacksModLogPermission      = $entServicePacksLogsArr[1];
$entServicePacksAddDelLogPermission   = $entServicePacksLogsArr[2];
?>
<style>
.groupAutoClass {
    z-index: 9999 !important;
}
    
    .ui-autocomplete {
  
    width: 400px !important;
    max-height: 250px !important;
    position: absulate;
    height: auto !important;
    overflow-y: auto;
}
    
</style>
<script src="/Express/servicePacks/js/getGroupsListBySP.js"></script>
<script>
	  var selectedSP = '<?php echo $_SESSION["sp"]?>'; 
      getGroupList(selectedSP) ;
 
</script>
<script src="servicePacks/js/servicePacks.js"></script>
<h2 class="adminUserText" id="deviceSearchBanner">Service Packs</h2>

<div style="" class="icons-div userAdd-icons-div">
    <ul style="" class="feature-list">
        <li class="changeOnMouseHover">
            <div class="mhoverSpList" onclick="selectServicePackIcons('servicePackList')">
                <div id="changImgSpList">Service Packs List</div>                
            </div></li>
        <li>
            <div class="mhoverSpUsage" onclick="selectServicePackIcons('servicePackUsage')">
                <div id="changImgSpUsage">Usage</div>
            </div></li>
    </ul>
</div>

<div class="selectContainer padding-top-zero">
<div class="loading" id="loadingBar"><img src="/Express/images/ajax-loader.gif"></div>
<div id = "servicePackList">
<?php if($entServicePacksAddDelLogPermission == "yes"){ ?>
	<div class="row delDownIconDiv">
		<!-- <div class="col-md-6"></div> -->
                <div class="col-md-6" style="padding-left:0;">
			 
				<!-- <div class="col-md-10"></div> -->
				<div class="col-md-2" style="padding-left:0;">
					<div id="downloadCSVBtn" class="csvFileDownloadAlign" style="display: block;float:left;">
             			<div id="downloadDeviceCSV" class="csvFileDownloadAlign">
                			<div id="servicePacksAdd">
            	    			<img src="images/icons/service-pack-add.png" style="width:40px;" data-alt-src="images/icons/download_csv_over.png">
            					<br><span class="addServicePack" style="color:#6ea0dc;">Add<br>Service Pack</span>
                			</div>
             			</div>
         			</div>
				</div>
               </div>
                <div class="col-md-6" style="padding-right:0">
                        <div class="deleteButtonContainer" style="float:right;">
                                <input type="button" id="spDeleteButton" value="Delete Service Pack" class="deleteButton marginRightButton deleteSmallButton">
                        </div>
                </div>
        </div>
    <?php } ?>
	<div class="row" style="margin:0 auto;">
	<form name="servicePackListTableFrom" id="servicePackListTableFrom">
		<div id="servicePacksTableDiv">
		</div>
	</form>
	</div>
        <?php if($entServicePacksModLogPermission == "yes"){ ?>
                <div class="row">
			<div class="form-group">
				<div class="col-md-4"></div>
				<div class="col-md-4" style="text-align:center;">
					<input type="button" id="servicePackConfirm" class="marginRightButton" value="Confirm Settings" style="margin-top:30px;">
				</div>
				<div class="col-md-4" style="text-align:right;">
					
				</div>
			</div>
		</div>
        <?php } ?>
	</div>
	<div id = "servicepackUsage" style="display:none;">
    	<div class="row">
    		<div class="col-md-12" style="margin-bottom:10px;margin-top:53px;">
    		<div class="form-group" id="searchUsageId">
    			<label class="labelText">Restrict To Group</label><br />
    			<input type="text" name="" id="searchGroupName"> <input type="button" class="subButton" id="getServiceProviderGroupWide" value="Get" style="height:30px;width:100px;">
    			<div id="hidden-stuffSP" style="height: 0px;"></div>
    		</div>
    		
    		</div>
    	</div>
    	<div id="servicePackUsageTableId">
    	
    	</div>
	</div>
	
	<div id = "servicePackSearchTable" style="display:none;">
    	
    	<div class="row" style="margin:0 auto;">
    		<b style="padding-left:25px;">Basic:</b>
    	</div>
    	<div class="row" style="margin:0 auto;">
        	<div style="zoom: 1;" class="viewDetail autoHeight">
        		<table id="servicePacksUsageTable" class="scroll tablesorter dataTable" style="width: 100%; margin: 0;">
                		<thead>
                			<tr>
                				
                				<th class="header thsmall">Group</th>
                				<th class="header thsmall">Total Packs</th>
                				<th class="header thsmall">Assigned</th>
                			</tr>
                		</thead>
                		<tbody>
                	    <tr>
                    	    
                    	    <td class="macTable thsmall">Lorem Ipsum Content</td>
                    	    <td class="macTable thsmall">Lorem Ipsum Content</td>
                    	    <td class="macTable thsmall">Lorem Ipsum Content</td>
                	    </tr>
                	    
                	    <tr>
                    	    
                    	    <td class="macTable thsmall">T Lorem Ipsum Content</td>
                    	    <td class="macTable thsmall">T Lorem Ipsum Content</td>
                    	    <td class="macTable thsmall">T Lorem Ipsum Content</td>
                	    </tr>	
                	    
                	    <tr>
                    	    
                    	    <td class="macTable thsmall">T Lorem Ipsum Content</td>
                    	    <td class="macTable thsmall">T Lorem Ipsum Content</td>
                    	    <td class="macTable thsmall">T Lorem Ipsum Content</td>
                	    </tr>    
                	    </tbody>
                	</table>
            	</div>
        	</div>
	</div>
	
</div>

<div id="servicePacksDialog" class="dialogClass"></div>
<div id="updateServicePackDialog" style="display:none"></div>
<div id="spPreventDialog" class="dialogClass"></div>
