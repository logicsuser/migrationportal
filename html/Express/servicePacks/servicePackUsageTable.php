<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/servicePacks/servicePacksController.php");
$usageObj = new ServicePacksController();
$usageList = $usageObj->serviceProviderServicePackGetUtilizationListRequest($_SESSION["sp"]);
//print_r($usageList);
$arrayWithBlankVal = array();
foreach ($usageList["Success"] as $key => $value){
    //echo "rajesh"; print_r($value);echo "ww";
    if($value == ""){
        $arrayWithBlankVal[$key] = "";
        unset($usageList["Success"][$key]);
    }
}

$usageList["Success"] = array_merge($usageList["Success"],$arrayWithBlankVal);
if(empty($usageList["Error"])){
    if(count($usageList["Success"]) > 0){
        $i = 0;
        foreach ($usageList["Success"] as $key => $value){
            
            if($i%2 == 0){
                echo '<div class="row" id="serviceUsageTables">';
            }
            echo "<div class='col-md-6'>";
            echo "<table class='tableServiceName'>";
            echo "<thead>";
            echo "<tr>";
            echo "<th style='padding:10px 10px 0px 10px !important'>".$key."</th>";
            echo "</tr>";
            echo "</thead>";
            echo "</table>";
            echo "<table class='scroll tablesorter dataTable' id=''>";
            //echo "<thead><tr><th>".$key."</th></tr></thead>";
            //echo "<thead><tr><th>".$key."</th></tr></thead>";
            echo "<thead>";
            echo "<tr><th class='header thsmall'>Group</th><th class='header thsmall'>Total Packs</th><th class='header thsmall'>Assigned</th></tr>";
            echo "</thead>";
            echo "<tbody>";
            if(!empty($value)){
                foreach ($value as $innerKey => $innerValue){
                    echo "<tr>";
                    echo "<td>". $innerValue['group']. "</td>";
                    echo "<td>". $innerValue['totalPacks']. "</td>";
                    echo "<td>". $innerValue['assigned']. "</td>";
                    echo "</tr>";
                }
            }else{
                echo "<tr>";
                echo "<td>No Entries Present</td>";
                
                echo "</tr>";
            }
            
            echo "</tbody>";
            echo "</table>";
            echo "</div>";
            if($i%2 != 0){
                echo '</div>';
            }
            $i++;
        }
    }else{
        echo "<div class='col-md-12'><div class='form-group' style='text-align: center;'><b style='font-weight:bold;color:#6ea0dc;'>--- No Data Found ---</b></div></div>";
    }
}


?>