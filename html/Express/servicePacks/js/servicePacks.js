var buttonNameClickEvent ="";
var servicePackAction ="";
var isModify = "";
var getServicePackList = function (){
	//$("#deviceCustomTagCongLoader").show() ;
	$("#servicePacksTableDiv").html("");
	$("#spDeleteButton").hide();
	$("#servicePacksAdd").hide();
	$("#servicePackConfirm").hide();
	$("#loadingBar").show();
	
    $.ajax({
            method:"POST",
              url:"servicePacks/servicePackLogic.php",
              data:{"funcAction":"showSPList"},
     }).then(successResonseGetServicePackList);
};

var getServicePackUsageTable = function (){
	$("#searchUsageId").hide();
	$("#servicePackUsageTableId").html("");
	//$("#servicePackUsageTableId").html("<div id=\"loading3\" class=\"loading\" style=\"margin:0;text-align:center;\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
    $("#loadingBar").show();
	//$("#servicePackUsageTableId").html('<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
	$.ajax({
		type: "POST",
		url:"servicePacks/servicePackUsageTable.php",
		//data:{"funcAction":"servicePackAdd", "data":dataToSend},
		success: function(result) {
			if( result.search("No Data Found") <0) { //no match
        		$("#searchUsageId").show();
        	  }else{
        		  $("#searchUsageId").hide();
        	  }
			//$("#searchUsageId").show();
			$("#servicePackUsageTableId").html(result);
			$("#loadingBar").hide();
		}
	});
};

var isCheckBoxIsClicked_CustomTag = false;
function checkBoxDelBtnShowNavo(status) {
	isCheckBoxIsClicked_CustomTag = status;
}

//$("#servicePacksTable tbody tr").click(function(){


var getServicePackListAdd = function (){
	var dataToSend = $("form#servicePackDialogForm").serializeArray();
	$.ajax({
		type: "POST",
		url:"servicePacks/servicePackLogic.php",
		data:{"funcAction":"servicePackAdd", "data":dataToSend},
		success: function(result) {
			successResonseGetServicePackList(result);
			$("#updateServicePackDialog").dialog("close");
		}
	});
};
var getServicePackListMod = function (){
	var dataToSend = $("form#servicePackModDialogForm").serializeArray();
	$.ajax({
		type: "POST",
		url:"servicePacks/servicePackLogic.php",
		data:{"funcAction":"servicePackMod", data : dataToSend},
		success: function(result) {
			successResonseGetServicePackList(result);
			$("#updateServicePackDialog").dialog("close");
		}
	});
}

var successResonseGetServicePackList = function (data){
//alert("success"+data) ;
//var tableCls   = "";
		
        var tableData  = "";
        var result     = JSON.parse(data);
        //var deviceList = result.deviceList;
        //var deviceTypeClass = "SortingClass";
        //var newClassForSorting = "";            
        //delete result.deviceList; //Removing last element mean deviceType from array 
        tableCls = "servicePackTableDiv";
        if(result!=undefined && result !=""){ 
                tableData         += "<table border='1' class='"+tableCls+" tableAlignMentDesign scroll tablesorter tagTableHeight' id='servicePacksTable'>";
                tableData         += "<thead> ";
                tableData         += "<tr>  ";

                tableData         += "<th id='disableTh' class='header' style='width:10%;'>Delete</th>";
                tableData         += "<th class='header thsmall' style='height: auto !important; width:10% !important;'>Available For Use</th>";
                tableData         += "<th class='header thsmall' style='height: auto !important; width:30% !important;'>Service Pack Name</th>";
                tableData         += "<th class='header thsmall' style='height: auto !important; width:30% !important;'>Descriptions</th>";
                tableData         += "<th class='header thsmall' style='height: auto !important; width:20% !important;'>In Use</th>";
                tableData         += "</tr>";

                tableData         += "</thead>";
                tableData         += "<tbody id='customTagTable' style='overflow-y:scroll !important;'>"; 
                //var pos = 1;
                $.each(result, function(key, value){ 
                	
                	 var servicePackKey=key;
                     var servicePackName = value.servicePackName;
                     var servicePackDescription = value.servicePackDescription;  
                     var inUse = value.inUse;
                     var isAvailableForUse = value.isAvailableForUse;
                     var servicePackQuantity = value.servicePackQuantity;
                     var servicePackUserServiceList = value.servicePackUserServiceList;
                     var isadded = value.isadded;
                     
                     tableData += "<tr class='tableTrAlignment' style='' id='servicePacksTable'>";
                     if(inUse == "Yes"){
                    	 tableData += "<td class='deleteCheckboxtd thSno header' style='width:10% !important'>" +
                  		"<input name='servicePackName[]' disabled data-servicepackuserservicelist = '"+servicePackUserServiceList+"' data-isadded = '"+isadded+"' data-servicepackquantity='"+servicePackQuantity+"' data-isavailableforuse = '"+isAvailableForUse+"' data-inuse='"+inUse+"' data-servicepackdescription = '"+servicePackDescription+"' data-spname = '"+servicePackKey+"' value='"+servicePackName+"' type='checkbox' style='width:17px' class='servicePackDeleteCheckBox' id='" +servicePackName+ "'>" +
                  		"<label for='" +servicePackName+ "'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label>" +
                  		"</td>";
                     }else{
                    	 tableData += "<td class='deleteCheckboxtd thSno header' style='width:10% !important'>" +
                  		"<input name='servicePackName[]' data-servicepackuserservicelist = '"+servicePackUserServiceList+"'  data-isadded = '"+isadded+"' data-servicepackquantity='"+servicePackQuantity+"' data-isavailableforuse = '"+isAvailableForUse+"' data-inuse='"+inUse+"' data-servicepackdescription = '"+servicePackDescription+"' data-spname = '"+servicePackName+"' value='"+servicePackName+"' type='checkbox' style='width:17px' class='servicePackDeleteCheckBox' id='" +servicePackName+ "'>" +
                  		"<label for='" +servicePackName+ "'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label>" +
                  		"</td>";
                     }
                    
                     if(isAvailableForUse == "true"){
                    	 tableData += "<td class='deleteCheckboxtd thSno header' style='width:10% !important'>" +
                   		"<input name='servicePackNameUse[]' value='"+servicePackName+"' disabled type='checkbox' checked style='width:17px' class='servicePackUseCheckBox' id='" +servicePackName+"Use'>" +
                   		"<label for='" +servicePackName+"Use'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label>" +
                   		"</td>";
                     }else{
                    	 tableData += "<td class='deleteCheckboxtd thSno header' style='width:10% !important'>" +
                   		"<input name='servicePackNameUse[]' value='"+servicePackName+"' disabled type='checkbox' style='width:17px' class='servicePackUseCheckBox' id='" +servicePackName+"Use'>" +
                   		"<label for='" +servicePackName+"Use'><span onmouseover='checkBoxDelBtnShowNavo(true)' onmouseout='checkBoxDelBtnShowNavo(false)'></span></label>" +
                   		"</td>";
                     }
                     
                     tableData += "<td class='header thsmall' style='width:30% !important'>"+ servicePackName + "</td>";
                     tableData += "<td class='header thsmall' style='width:30% !important'>"+ servicePackDescription + "</td>";
                     tableData += "<td class='header thsmall' style='width:20% !important'>"+ inUse +"</td>";

                     tableData += "</tr>"; 
                     //pos++;
                })
                tableData         += "</tbody>";
                tableData         += "</table>";
                
                $("#servicePacksTableDiv").html(tableData);
                $("#servicePacksTableDiv").tablesorter();
                $("#servicePackConfirm").show();
                $("#servicePacksAdd").show();
                $("#spDeleteButton").show();
                $("#loadingBar").hide();
                $("."+tableCls).tablesorter();
                
        }else{  
        	$("#loadingBar").hide();
        	$("#servicePacksTableDiv").html("<div class='col-md-12'><div class='form-group' style='text-align: center;'><b style='font-weight:bold;color:#6ea0dc;'>--- No Service Pack Found ---</b></div></div>");
        }
        $("#servicePacksAdd").show();
        //$("#customConfigTagDeviceNameDiv").show();
        //$("#deviceCustomTagCongLoader").hide() ;
};

$(function(){
	
    
    
      $("#spPreventDialog").dialog({
		  	autoOpen: false,
		  	width: 600,
		  	modal: true,
		  	position: { my: "top", at: "top" },
		  	resizable: false,
		  	closeOnEscape: false,
		  	buttons: {
		  		"Yes": function() {
		  			checkResult(false);
					 if(buttonNameClickEvent =='delButtonClickEvent'){delSpBtn();}
		  			$(this).dialog("close");
				},
		  		"No": function() {
		  			checkResult(true);
					
					$('#selectGroup').prop('disabled', false);
		  			$(this).dialog("close");
					selectGroupEnabled();
		  			return false; 
		  		} 
		  	},  
		      open: function() {
		      	$(".ui-dialog-buttonpane button:contains('Yes')").button().show().addClass('subButton');
		          $(".ui-dialog-buttonpane button:contains('No')").button().show().addClass('cancelButton');
		      }
		  });
    
    
    
    
    
    
    
    
    
    
    
    
    
    $("#spDeleteButton").hide();
	$("#servicePacksAdd").hide();
	$("#servicePackConfirm").hide();
	$("#searchUsageId").hide();
	
	getServicePackList();
	getServicePackUsageTable();
	$("#servicePacksTable").tablesorter();
	
	$("#servicePacksDialog").dialog({
        autoOpen: false,
        width: 800,
        modal: true,
        title: "Service Pack Dialog",
        position: { my: "top", at: "top" },
        resizable: false,
        closeOnEscape: false,
        buttons: {
      	  Update: function() {
      		  $("html, body").animate({ scrollTop: 0 }, 600);
      		  //$("#selectSoftPhoneDeviceType").prop("disabled", false);
      		  //$("#softPhoneDeviceTypeLinePort").prop("disabled", false);
      		  
          	  var dataToSend = $("form#servicePackModDialogForm").serializeArray();
                $.ajax({
                    type: "POST",
                    url: "servicePacks/modValidate.php",
                    data: dataToSend,
                    success: function(result)
                    {                 
                        result = result.trim();                      	
                        if (result.slice(0, 1) == 1){
                            $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                        }else{
                            $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                        }
                        isModify = "modify";
                        $("#updateServicePackDialog").html(result.slice(1));
                        $("#servicePacksDialog").dialog("close");
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        $("#updateServicePackDialog").dialog("open");
                        
                        $(":button:contains('Complete')").show().addClass('subButton');
                        $(":button:contains('Cancel')").show().addClass('cancelButton');
                        $(":button:contains('Create')").hide();
                        $(":button:contains('Close')").hide();
                        $(":button:contains('Update')").hide();
                        $(":button:contains('Confirm')").hide();
                        $(":button:contains('More Changes')").hide();
                        $(":button:contains('Return To Main')").hide();
                        /*var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                        getCustomTagList(deviceTypeNameValue);*/
                    }
              });  
          	   
           },
          
          	Create: function() {
          		$("html, body").animate({ scrollTop: 0 }, 600);
                  var dataToSend = $("form#servicePackDialogForm").serializeArray();
                  var pageUrl = "addCheckData.php"; 
                  $.ajax({
                    type: "POST",
                    url: "servicePacks/"+pageUrl,
                    data: dataToSend,
                    success: function(result)
                    {

                        result = result.trim();
                        if (result.slice(0, 1) == 1)
                        {
                            $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                        }
                        else
                        {
                            $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                        }
                        $("#updateServicePackDialog").html(result.slice(1));
                        $("#servicePacksDialog").dialog("close");
                        $("html, body").animate({ scrollTop: 0 }, 600);
                        $("#updateServicePackDialog").dialog("open");

                        $(":button:contains('Complete')").show().addClass('subButton');;
                        $(":button:contains('Cancel')").show().addClass('cancelButton');;
                        $(":button:contains('More Changes')").hide();
                        $(":button:contains('Return To Main')").hide();
                        $(":button:contains('Create')").hide();
                        isModify = "add";
                        // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                    }
                  });  
	 	 
           },
           Complete: function(){
        	   pendingProcess.push("Service Packs Modify");
        	   $("html, body").animate({ scrollTop: 0 }, 600);
               //var dataToSend = $("form#servicePackDialogForm").serializeArray();
               var pageUrl = "servicePackOperations.php"; 
               $.ajax({
                 type: "POST",
                 url: "servicePacks/"+pageUrl,
                 //data: dataToSend,
                 success: function(result)
                 {
                	 if(foundServerConErrorOnProcess(result, "Service Packs Modify")) {
     					return false;
                     }
                	 checkResult(false); 
                     /*result = result.trim();
                     if (result.slice(0, 1) == 1)
                     {
                         $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                     }
                     else
                     {
                         $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                     }*/
                     $("#servicePacksDialog").html(result);
                     //$("#servicePacksDialog").dialog("close");
                     $("html, body").animate({ scrollTop: 0 }, 600);
                     $("#servicePacksDialog").dialog("open");

                     $(":button:contains('Complete')").hide();
                     //$(":button:contains('Cancel')").show().addClass('cancelButton');
                     $(":button:contains('More Changes')").show().addClass('moreChangesButton');
                     $(":button:contains('Return To Main')").show().addClass('moreChangesButton');
                     $(":button:contains('Create')").hide();
                     $(":button:contains('Cancel')").hide();
                     
                     // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                 }
               });
           },
           'More Changes': function(){
        	   $("html, body").animate({ scrollTop: 0 }, 600);
        	   $(this).dialog("close");
        	   getServicePackList();
        	   getServicePackUsageTable();
                   checkResult(false); 
           },
           "Return To Main" : function(){
        	  checkResult(false);
               location.href="main_enterprise.php";
           },
           Delete : function(){
        	   pendingProcess.push("Delete Service Pack");
        	   $("html, body").animate({ scrollTop: 0 }, 600);
               var dataToSend = $("form#servicePackListTableFrom").serializeArray();
               var pageUrl = "servicePackDelete.php"; 
               $.ajax({
                 type: "POST",
                 url: "servicePacks/"+pageUrl,
                 data: dataToSend,
                 success: function(result)
                 {
                	 if(foundServerConErrorOnProcess(result, "Delete Service Pack")) {
                		 return false;
                	 }
                	 checkResult(false); 
                     /*result = result.trim();
                     if (result.slice(0, 1) == 1)
                     {
                         $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
                     }
                     else
                     {
                         $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
                     }*/
                     $("#servicePacksDialog").html(result);
                     //$("#servicePacksDialog").dialog("close");
                     $("html, body").animate({ scrollTop: 0 }, 600);
                     $("#servicePacksDialog").dialog("open");

                     $(":button:contains('Complete')").hide();
                     $(":button:contains('Cancel')").hide();
                     $(":button:contains('More Changes')").show().addClass('moreChangesButton');
                     $(":button:contains('Return To Main')").show().addClass('moreChangesButton');
                     //$(":button:contains('Return To Main')").hide();
                     $(":button:contains('Create')").hide();
                     $(":button:contains('Delete')").hide();
                     // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
                 }
               });
           },
            Close: function() {
                $(this).dialog("close");
            },              
            Cancel: function() {
                $(this).dialog("close");
            }

        },
        open: function() {
        	setDialogDayNightMode($(this));
      	  	$(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
 		     	$(".ui-dialog-buttonpane button:contains('Create')").button().hide();
 		     	$(".ui-dialog-buttonpane button:contains('Close')").button().hide();
      	   	$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
      	    $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
      	  $(".ui-dialog-buttonpane button:contains('More Changes')").button().hide();
      	$(".ui-dialog-buttonpane button:contains('Delete')").button().hide();
      	$(".ui-dialog-buttonpane button:contains('Return To Main')").button().hide();
      	   
      	    if(servicePackAction == "Create"){
      		   $(".ui-dialog-buttonpane button:contains('Create')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      		}
      	   
      	   if(servicePackAction == "Update"){
      		   $(".ui-dialog-buttonpane button:contains('Update')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      	   }
      	   
      	   if(servicePackAction == "Delete"){
      		   $(".ui-dialog-buttonpane button:contains('Delete')").button().show().addClass('subButton');
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');
      	   }
      	   
       }
    });
	
	$("#servicePacksAdd").click(function(){
		servicePackAction ="Create" ;
	    $.ajax({
            url: "servicePacks/servicePackDialog.php",
            type:'GET',
            //data:{"userId":userId},
            success: function(result)
            {
            	$("#servicePacksDialog").html(result);
        	    $("#servicePacksDialog").dialog("open");
        	    $("#servicePacksDialog").dialog("option","title", "Add Service Pack");
            }
        });
	 });
	
	
	
	
	
	
	$("#updateServicePackDialog").dialog({
        autoOpen: false,
        width: 800,
        modal: true,
        title: "Service Pack Dialog",
        position: { my: "top", at: "top" },
        resizable: false,
        closeOnEscape: false,
        buttons: {
      	  	
      	Create: function() {
                          
      	},
           Update: function() {
               
          	   
           },
          Confirm: function() {   
          	 
           },
           Complete: function(){
	         //debugger ;
               //servicePackAction ="Modify";
                                checkResult(true); // this function used for user modify or group if any changes and switch to different module.It is define in main.php and main_enterprise.php.
                                getPreviousModuleId("updateServicePack"); // this function used for check previous id it is define in main.php and main_enterprise.php.	
                	
          	 if(isModify == "modify"){
          		 //$(this).dialog("close");
          		 //$("html, body").animate({ scrollTop: 0 }, 600);
          		getServicePackListMod();
          	 }else{
          		getServicePackListAdd();
          	 }
          	 
           },
           Close: function() {
               $(this).dialog("close");
           },              
           Cancel: function() {
				if(servicePackAction == "Create" || servicePackAction == "Update"){
					$("#servicePacksDialog").dialog("open");
				}
			    $(this).dialog("close");
           }
           

        },
       
  	  open: function() {
				setDialogDayNightMode($(this));
      	  	$(".ui-dialog-buttonpane button:contains('Confirm')").button().hide();
 		     	$(".ui-dialog-buttonpane button:contains('Create')").button().hide();
 		     	$(".ui-dialog-buttonpane button:contains('Close')").button().hide();
      	   	$(".ui-dialog-buttonpane button:contains('Complete')").button().hide();
      	    $(".ui-dialog-buttonpane button:contains('Update')").button().hide();
      	   
      	    if(servicePackAction == "Create"){
      		   $(".ui-dialog-buttonpane button:contains('Complete')").button().show().addClass('subButton');;
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');;
      		}
      	   
      	   if(servicePackAction == "Update"){
      		   $(".ui-dialog-buttonpane button:contains('Update')").button().show().addClass('subButton');;
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');;
      	   }
      	   
      	   if(servicePackAction == "Delete"){
      		   $(".ui-dialog-buttonpane button:contains('Confirm')").button().show().addClass('subButton');;
      		   $(".ui-dialog-buttonpane button:contains('Cancel')").button().show().addClass('cancelButton');;
      	   }
       }
     
    });
	
	
	$("#servicePackConfirm").click(function(){
		$("html, body").animate({ scrollTop: 0 }, 600);
        //var dataToSend = $("form#servicePackDialogForm").serializeArray();
        var pageUrl = "validate.php"; 
        $.ajax({
          type: "POST",
          url: "servicePacks/"+pageUrl,
          data: "spadd",
          success: function(result)
          {

              result = result.trim();
              if (result.slice(0, 1) == 1)
              {
                  $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
              }
              else
              {
                  $(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
              }
              $("#servicePacksDialog").html(result.slice(1));
              //$("#servicePacksDialog").dialog("close");
              $("html, body").animate({ scrollTop: 0 }, 600);
              $("#servicePacksDialog").dialog("open");

              $(":button:contains('Complete')").show().addClass('subButton');;
              $(":button:contains('Cancel')").show().addClass('cancelButton');;
              $(":button:contains('More Changes')").hide();
              $(":button:contains('Return To Main')").hide();
              $(":button:contains('Create')").hide();
              $(":button:contains('Update')").hide();
              servicePackAction = "Confirm";
              // var deviceTypeNameValue = $("$deviceTypeCustomTag").val();
              $("#servicePacksDialog").dialog("option","title", "Service Pack Dialog");
          }
        });  
	});
	
        
        function checkGroupInfoHasChange(delButtonClickEvent){
		buttonNameClickEvent = delButtonClickEvent;
            var pageUrl = "validate.php"; 
        $.ajax({
          type: "POST",
          url: "servicePacks/"+pageUrl,
          data: "spadd",
          success: function(result)
          {

              result = result.trim();
              if (result.slice(0, 1) == 1)
              {
                  checkResult(false);
                  if(buttonNameClickEvent =='delButtonClickEvent'){delSpBtn();}
                  
                  $(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
              }
              else
              {
                  $("#spPreventDialog").html('Are you sure you want to leave? <br/> You might lose any change you have made for this Service Pack ');
                  $("#spPreventDialog").dialog('open');                             
              }
              
          }
        });
		}
        
        
        
        /*del service pack on entlevel */
        var delSpBtn = function(){
            servicePackAction == "Delete";
		$("html, body").animate({ scrollTop: 0 }, 600);
        var dataToSend = $("form#servicePackListTableFrom").serializeArray();
        var pageUrl = "servicePackDeleteValidation.php"; 
        $.ajax({
          type: "POST",
          url: "servicePacks/"+pageUrl,
          data: dataToSend,
          success: function(result)
          {

              result = result.trim();
              if (result.slice(0, 1) == 1)
              {
                  $(":button:contains('Delete')").attr("disabled", "disabled").addClass("ui-state-disabled");
              }
              else
              {
                  $(":button:contains('Delete')").removeAttr("disabled").removeClass("ui-state-disabled");                                 
              }
              $("#servicePacksDialog").html(result.slice(1));
              //$("#servicePacksDialog").dialog("close");
              $("html, body").animate({ scrollTop: 0 }, 600);
              $("#servicePacksDialog").dialog("open");

              $(":button:contains('Complete')").hide();
              $(":button:contains('Cancel')").show().addClass('cancelButton');
              $(":button:contains('Delete')").show().addClass('deleteButton');
              $(":button:contains('More Changes')").hide();
              $(":button:contains('Return To Main')").hide();
              $(":button:contains('Create')").hide();
              $(":button:contains('Update')").hide();
              $("#servicePacksDialog").dialog("option","title", "Delete Service Pack");
              
          }
        }); 
        }
        
        /* end del sp pack */
	
	$("#spDeleteButton").click(function(){
	 checkGroupInfoHasChange('delButtonClickEvent') ;
	});
	
	$("#getServiceProviderGroupWide").click(function(){
		var searchGroupName = $("#searchGroupName").val();
		if(searchGroupName !=""){
			var pageUrl = "searchServicePack.php"; 
	        $("#servicePackUsageTableId").html("<div id=\"loading3\" class=\"loading\" style=\"margin:0;text-align:left;\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
	        $("#loading3").show();
	        $.ajax({
	          type: "POST",
	          url: "servicePacks/"+pageUrl,
	          data:{searchGroupName : searchGroupName},
	          success: function(result)
	          {
	        	  $("#servicePackUsageTableId").html(result);
	          }
	        });
		}else{
			
			getServicePackUsageTable();
		}
		
          
	});
	
});

$(document).on("click", "tr#servicePacksTable", function(event) {
	servicePackAction = "Update";
	if (!isCheckBoxIsClicked_CustomTag) {
		//servicePackAction ="Modify";
		
		var servicePackName = $(this).closest('tr#servicePacksTable').find('.servicePackDeleteCheckBox').data("spname");
		var isadded = $(this).closest('tr#servicePacksTable').find('.servicePackDeleteCheckBox').data("isadded");
		var isavailableforuse = $(this).closest('tr#servicePacksTable').find('.servicePackDeleteCheckBox').data("isavailableforuse");
		var servicepackdescription = $(this).closest('tr#servicePacksTable').find('.servicePackDeleteCheckBox').data("servicepackdescription");
		var servicepackquantity = $(this).closest('tr#servicePacksTable').find('.servicePackDeleteCheckBox').data("servicepackquantity");
		var servicepackuserservicelist = $(this).closest('tr#servicePacksTable').find('.servicePackDeleteCheckBox').data("servicepackuserservicelist");

		if(isadded == "added"){
			servicePackAction ="Create" ;
			$.ajax({
		        url: "servicePacks/servicePackDialog.php",
		        type:'POST',
		        data:{"servicePackUserServiceList":servicepackuserservicelist},
		        success: function(result)
		        {
		        	$("#servicePacksDialog").html(result);
		    	    $("#servicePacksDialog").dialog("open");
		    	    $("#servicePacksDialog").dialog("option","title", "Modify Service Pack (not submitted)");
		    	    if(isadded == "added"){
		    	    	$("#spName").val(servicePackName);
		    	    	$("#spDescription").val(servicepackdescription);
		    	    	if(isavailableforuse == true){
		    	    		$("#availableForUse").prop('checked', true);
		    	    	}else{
		    	    		$("#availableForUse").prop('checked', false);
		    	    	}
		    	    	if($.isNumeric(servicepackquantity)){
		    	    		
		    	    		$("#qLimitedTo").prop('checked', true);
		    	    		$("#limitedQuantity").val(servicepackquantity);
		    	    		$("#limitedQuantity").removeAttr("disabled");
		    	    	}else{
		    	    		$("#qUnlimited").prop('checked', true);
		    	    		$("#limitedQuantity").val("");
		    	    		$("#limitedQuantity").attr("disabled", "disabled");
		    	    	}
		    	    }
		    	    $('#sortable_2').sortable('option','update')(null, {
					      item: $(' ').appendTo($('#sortable_2'))
					});
		    	    $("#oldSPName").val(servicePackName);
		        }
		    });
		}else{
			$.ajax({
		        url: "servicePacks/servicePackModifyDialog.php",
		        type:'POST',
		        data:{"servicePackName":servicePackName},
		        success: function(result)
		        {
		        	$("#servicePacksDialog").html(result);
		    	    $("#servicePacksDialog").dialog("open");
		    	    $("#servicePacksDialog").dialog("option","title", "Modify Service Pack");
		    	    
		        }
		    });
		}
	    
	}
 });

function selectServicePackIcons(selector) {
	if(selector == "servicePackUsage"){
		$("#changImgSpUsage").css('background-image','url(images/icons/usage-orange.png)');
        $('#changImgSpUsage').css('color','#ffb200');
        
		$('#changImgSpList').css('background-image','url(images/icons/service-pack-list-rest.png)');
        $('#changImgSpList').css('color','#6ea0dc');
		
		$("#servicepackUsage").show();
		$("#servicePackList").hide();
		getServicePackUsageTable();
	}else if(selector == "servicePackList"){
		$("#changImgSpUsage").css('background-image','url(images/icons/usage-rest.png)');
        $('#changImgSpUsage').css('color','#6ea0dc');
        
		$('#changImgSpList').css('background-image','url(images/icons/service-pack-list-orange.png)');
        $('#changImgSpList').css('color','#ffb200');
		
		$("#servicepackUsage").hide();
		$("#servicePackList").show();
	}
}