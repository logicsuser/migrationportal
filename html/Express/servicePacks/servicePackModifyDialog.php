<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
//print_r($_POST);
require_once ("/var/www/lib/broadsoft/adminPortal/servicePacks/servicePacksController.php");
$serviceObj = new ServicePacksController();
$servicePackDetails = $serviceObj->serviceProviderServicePackGetDetailListRequest($_SESSION["sp"], $_POST["servicePackName"]);
$spDetails = $servicePackDetails["Success"];

array_multisort(array_column($spDetails["Services"], "serviceName"), SORT_ASC, $spDetails["Services"]);

$_SESSION["spDetailsSession"] = $spDetails;
?>
<script type="text/javascript">
$(function() {
	$('input:radio[name="qLimitation"]').change(function() {
		if ($('#qLimitedTo').is(':checked')) {
			$("#limitQuantity").removeAttr("disabled");
		}else{
			$("#limitQuantity").val("");
			$("#limitQuantity").attr("disabled", "disabled");
		}
	});
});

</script>

<form id="servicePackModDialogForm" name="servicePackModDialogForm" >
    
    <div class="row">
    	<div class="col-md-6">
    		<div class="form-group">
                <label class="labelText">Name:</label> 
                <span class="required">*</span>   
                <input type="text" name="spName" class="form-control" size="35" maxlength="30" id="spName" value="<?php echo $spDetails["servicePackName"]; ?>">
            </div>
    	</div>
    	<div class="col-md-6">
    		<div class="form-group">
                <label class="labelText">Description:</label> 
                <input type="text" name="spDescription" class="form-control" size="35" maxlength="30" id="spDescription" value="<?php echo $spDetails["servicePackDescription"]; ?>">
            </div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<input type="hidden" name="availableForUse" id="" value="false">
    		<input type="checkbox" name="availableForUse" id="availableForUse" value="true" <?php if($spDetails["isAvailableForUse"] =="true"){ echo "checked"; } ?>> 
    		<label for="availableForUse"><span></span></label>
    		<label class="labelText">Available For Use: </label>
    	</div>
    </div>
    
    <h2 class="subBannerCustom">Quantities</h2>
    <div class="row">
    	<div class="col-md-6">
    		<label class="labelText">Maximum Quantity Allowed: Unlimited</label> <br />
    		<label class="labelText" style="width:110px;">Quantity</label>
    		<input type="radio" name="qLimitation" id="qUnlimited" value="true" <?php if(isset($spDetails["servicePackQuantity"]["unlimited"])){echo "checked"; } ?>> 
    		<label for="qUnlimited"><span></span></label>
    		<label class="labelText">Unlimited</label><br />
    		
    		<label class="labelText" style="width:110px;">&nbsp;</label>
    		<input type="radio" name="qLimitation" id="qLimitedTo" value="true" <?php if(isset($spDetails["servicePackQuantity"]["quantity"])){echo "checked"; } ?>> 
    		<label for="qLimitedTo"><span></span></label>
    		<label class="labelText">Limited To: </label> <input type="text" maxlength="6" name="limitQuantity" id="limitQuantity" style="width:auto; float:right;" <?php if(isset($spDetails["servicePackQuantity"]["quantity"])){echo "value='".$spDetails["servicePackQuantity"]["quantity"]."'"; }else{echo "disabled";} ?>>
    		
    	</div>
    </div>
    
    <div class="row" id="serviceUsageTables" style="margin:0 auto;padding-top:25px;">
        		<table id="servicePackModifyTable" class="scroll tablesorter dataTable">
                		<thead>
                			<tr>
                				
                				<th class="header thsmall">Services in Pack</th>
                				<th class="header thsmall">Authorized</th>
                				<th class="header thsmall">Allocated</th>
                				<th class="header thsmall">Available</th>
                			</tr>
                		</thead>
                		<tbody style="overflow-y:auto;">
                		<?php
                		foreach ($spDetails["Services"] as $key => $value){ ?>
                		    <tr>
                    	    
                    	    <td class="macTable thsmall"><?php echo $value["serviceName"] ?></td>
                    	    <td class="macTable thsmall"><?php echo $value["authorized"] ?></td>
                    	    <td class="macTable thsmall"><?php echo $value["allocated"] ?></td>
                    	    <td class="macTable thsmall"><?php echo $value["available"] ?></td>
                	    </tr>
                		<?php } ?>
                	        
                	    </tbody>
                	</table>
	</div>
    
</form>
<?php 

?>