<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/servicePacks/servicePacksController.php");
$obj = new ServicePacksController();
$assocArray = array();
$processedArray["notAssigned"] = array();
$processedArray["assigned"] = array();
$processedArray["details"] = array();
$processedArray["filteredAllServices"] = array();
if($_POST["funcAction"] == "getEnterPrises" ){
    $spList = $obj->serviceProviderServicePackGetListRequest($_POST["enterprise"]);
    echo  json_encode($spList);
}

if($_POST["funcAction"] == "compareServices" ){
    $serviceListResp = $obj->serviceProviderServiceGetAuthorizationListRequest($_SESSION["sp"]);
    //print_r($serviceListResp);
    $assignedService = $obj->serviceProviderServicePackGetDetailListRequest($_POST["enterprise"], $_POST["serviceName"]);
    $processedArray["details"] = $assignedService;
    //print_r($assignedService);
    if(count($assignedService["Success"]["Services"]) > 0){
        foreach ($assignedService["Success"]["Services"] as $key => $value){
            $assocArray[] = $value["serviceName"];
        }
    }
    $processedArray["assigned"] = $assocArray;
    //print_r($assocArray);
    if(count($assocArray) > 0){
        foreach ($assocArray as $assocKey => $assocVal){
            if(!in_array($assocVal, $serviceListResp["Success"]["userServices"])){
                $processedArray["notAssigned"][] = $assocVal;
            }
        }
    }
    if(count($serviceListResp["Success"]["userServices"]) > 0){
        foreach ($serviceListResp["Success"]["userServices"] as $sKey => $sVal){
            if(in_array($sVal, $assocArray)){
                unset($serviceListResp["Success"]["userServices"][$sKey]);
            }
        }
        $processedArray["filteredAllServices"] = array_values($serviceListResp["Success"]["userServices"]);
        //print_r($processedArray["filteredAllServices"]);
    }
    
    echo  json_encode($processedArray);
    
}

?>