<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
$bwAuthlevel == "system";
require_once ("/var/www/lib/broadsoft/adminPortal/getServiceProviders.php");
require_once ("/var/www/lib/broadsoft/adminPortal/servicePacks/servicePacksController.php");

$serviceObj = new ServicePacksController();

$serviceList = $serviceObj->serviceProviderServiceGetAuthorizationListRequest($_SESSION["sp"]);
//print_r($serviceList);
//print_r($_POST);
$arr = explode(";", $_POST["servicePackUserServiceList"]);

array_pop($arr);
//print_r($arr);
if(isset($arr) && !empty($arr)){
    foreach ($serviceList["Success"]["userServices"] as $key => $val){
        if(in_array($val, $arr)){
            unset($serviceList["Success"]["userServices"][$key]); 
        }
    }
}
//print_r($arr);
//print_r($serviceList);
?>
<style type="text/css">
#sortable_1 li.selected, #sortable_2 li.selected{
    background:#ffb200;
}
#sortable_1 li:first-child, #sortable_2 li:first-child{
    margin-top:0 !important;
}
#sortable_1, #sortable_2{
    padding:25px 0 0 0 !important;
}

</style>
<script src="servicePacks/js/multiDragAndDrop.js"></script>
<script type="text/javascript">
$(function() {
	$("#cloneId").hide();
	$('[data-toggle="tooltip"]').tooltip();
	$("#limitedQuantity").attr("disabled", "disabled");
/*	$("#sortable_1, #sortable_2").multisortable({
		placeholder: "ui-state-highlight",
		connectWith: ".connectedSortable",
		cursor: "crosshair",
		update: function(event, ui)
		{ //debugger;
			var servicePackUserServiceList = "";                                                
			var order = $("#sortable_2").sortable("toArray");
			for (i = 0; i < order.length; i++)
			{
				servicePackUserServiceList += order[i] + ";";
			}
			$("#servicePackUserServiceList").val(servicePackUserServiceList);
			//sortUnorderedList(event.target.id);
			//setTimeout(sortUnorderedList(event.target.id), 3000)
			setTimeout(function() {
				sortUnorderedList(event.target.id)
            }, 2000)
			//$("#sortable_1 li").css("background", "#F6F6F6");
			//$("#sortable_2 li").css("background", "#F6F6F6");
			//$("#sortable_1 li").removeClass("selected");
			//$("#sortable_2 li").removeClass("selected");
			//$("#servicePackUserServiceList").val(servicePackUserServiceList);
		}
	}).disableSelection();
*/

 /* shorting list blank data handling into the sortlist  */
        $("#sortable_1, #sortable_2").multisortable({
		placeholder: "ui-state-highlight",
		connectWith: ".connectedSortable",
		cursor: "crosshair",
		update: function(event, ui)
		{  
                    var servicePackUserServiceList = "";                                                
                    var order = $("#sortable_2").sortable("toArray");
                    for (i = 0; i < order.length; i++)
                    {
                            servicePackUserServiceList += order[i] + ";";
                    }
                    $("#servicePackUserServiceList").val(servicePackUserServiceList);
			 
		},
                stop : function(){
                                            
                    getArrayFromLi('#sortable_1');
                    getArrayFromLi('#sortable_2');
                }
	}).disableSelection();
        
        /* end */
        
        var insensitive = function(s1, s2) {
            var s1lower = s1.toLowerCase();
            var s2lower = s2.toLowerCase();
            return s1lower > s2lower? 1 : (s1lower < s2lower? -1 : 0);
        }

				//make new html for the ul tag for ordering the li data
        var getArrayFromLi = function(ulId){
                                   
            var liArray = [];
            var liNewArrayUsers = [];
            var liStr = "";

            $(ulId+" li").each(function(){
                    var el = $(this);
                    liArray.push(el.text());
            });

            var liNewArray = liArray.sort(insensitive);
            $(ulId).html('');
            for (i = 0; i < liNewArray.length; i++)
            {
                    liStr += '<li class="ui-state-default" id="'+liNewArray[i]+'">'+liNewArray[i]+'</li>';
            }
            $(ulId).html(liStr);
            /*alert('liStr - '+liStr);
            alert('liNewArray - '+liNewArray);
            alert('liNewArrayUsers - '+liNewArrayUsers);
            console.log(liNewArrayUsers);*/
    }
    getArrayFromLi('#sortable_1');
    getArrayFromLi('#sortable_2'); 
    /* end code service pack sorter */
    /*$('ul.sortable').multisortable({
		update: function(event, ui)
		{
			var servicePackUserServiceList = "";                                                
			var order = $("#sortable_2").sortable("toArray");
			for (i = 0; i < order.length; i++)
			{
				servicePackUserServiceList += order[i] + ";";
			}
			$("#servicePackUserServiceList").val(servicePackUserServiceList);

			//$("#servicePackUserServiceList").val(servicePackUserServiceList);
		}
	});
	$('ul#sortable_1').sortable('option', 'connectWith', 'ul#sortable_2');
	$('ul#sortable_2').sortable('option', 'connectWith', 'ul#sortable_1');*/
	
	$("#cloneExisting").click(function(){
		if ($('#cloneExisting').is(':checked')) {
			$("#cloneId").show();
		}else{
			$("#cloneId").hide();
		}
	});

	$('#enterprise').on('change', function() {
		var selectedVal = this.value;
		$("#clonedServicePackName").html("");
		if(selectedVal != ""){
			$.ajax({
				type: "POST",
				url: "servicePacks/getData.php",
				data:{"funcAction":"getEnterPrises", "enterprise" : selectedVal},
				success: function(data) {
					var result = JSON.parse(data);
                                        console.log(data);
					if(result.Error.length > 0){
						$("#errorDisplay").html(result.Error);
					}else{
						var sucData = result.Success;
						var html = "<option value = ''>None</option>";
						for (var i = 0; i < sucData.length; i++) {
							html += '<option value="'+sucData[i]+'">' +sucData[i]+ '</option>';		
						}
						$("#clonedServicePackName").html(html);
					}
				}
			}); 
		}else{
			var html = "<option value = ''>None</option>"; 
			$("#clonedServicePackName").html(html);
		}
		
	});

	$('#clonedServicePackName').on('change', function() {
		var selectedVal = this.value;
		var enterprise = $('#enterprise').val();
		if(selectedVal != ""){
			$.ajax({
				type: "POST",
				url: "servicePacks/getData.php",
				data:{"funcAction":"compareServices", "serviceName" : selectedVal, enterprise : enterprise},
				success: function(data) {
					var result = JSON.parse(data);
					if(result.notAssigned.length > 0){
						var notassignedArr = result.notAssigned;
						notassignedArr.toString();
						$("#errorDisplay").html("Warning: This Service Pack can not be cloned, because the following services are not authorized: "+notassignedArr+"");
					}else{
						if(result.details.Error == 0){
							var spData = result.details.Success;
							$("#spName").val(spData.servicePackName);
							$("#spDescription").val(spData.servicePackDescription);
							if(spData.isAvailableForUse == "true"){
								$('#availableForUse').prop('checked', true);
							}
							if (typeof spData.servicePackQuantity.unlimited !== 'undefined') {
								if(spData.servicePackQuantity.unlimited == "unlimited"){
									$("#qUnlimited").prop('checked', true);
									$("#limitedQuantity").val("");
									$("#limitedQuantity").attr("disabled", "disabled");
								}
							}
							if (typeof spData.servicePackQuantity.quantity !== 'undefined') {
								$("#qLimitedTo").prop('checked', true);
								$("#limitedQuantity").removeAttr("disabled");
								$("#limitedQuantity").val(spData.servicePackQuantity.quantity);
							}
							if(result.assigned.length > 0){
								var asgnHtml = "";
								for (var i = 0; i < result.assigned.length; i++) {
									asgnHtml += '<li class="ui-state-highlight" id="'+result.assigned[i]+'">' +result.assigned[i]+ '</li>';		
								}
								$("#sortable_2").html(asgnHtml);
							}
							if(result.filteredAllServices.length > 0){
								var allHtml = "";
								for (var i = 0; i < result.filteredAllServices.length; i++) {
									allHtml += '<li class="ui-state-highlight" id="'+result.filteredAllServices[i]+'">' +result.filteredAllServices[i]+ '</li>';		
								}
								$("#sortable_1").html(allHtml);
							}
							//apend blank data to retrive assigned data into hidden field
							$('#sortable_2').sortable('option','update')(null, {
							      item: $(' ').appendTo($('#sortable_2'))
							});
						}
					}
				}
			}); 
		}else{
			var html = "<option value = ''>None</option>"; 
			$("#clonedServicePackName").html(html);
		}
		
	});

	$('input:radio[name="qLimitation"]').change(function() {
		if ($('#qLimitedTo').is(':checked')) {
			$("#limitedQuantity").removeAttr("disabled");
		}else{
			$("#limitedQuantity").attr("disabled", "disabled");
			$("#limitedQuantity").val("");
		}
	});



	
});
/*
function sortUnorderedList(ul, sortDescending) {
    
            
           //Code added @ 08 Feb 2019           
            var ulId = "#"+ul+" li";
            $(ulId).each(function() {
                    $(this).removeClass("selected");
            });
            //End code
                  
	  if(typeof ul == "string")
	    ul = document.getElementById(ul);

	  // Get the list items and setup an array for sorting
	  var lis = ul.getElementsByTagName("LI");
	  var vals = [];

	  // Populate the array
	  for(var i = 0, l = lis.length; i < l; i++)
	    vals.push(lis[i].innerHTML);

	  // Sort it
	  vals.sort();

	  // Sometimes you gotta DESC
	  if(sortDescending)
	    vals.reverse();

	  // Change the list on the page
	  for(var i = 0, l = lis.length; i < l; i++)
	    lis[i].innerHTML = vals[i];
	} */
</script>

<form id="servicePackDialogForm" name="servicePackForm" >
    <div class="row">
    	<div class="col-md-12">
    		<div class="form-group">
    			<input type="checkbox" name="" id="cloneExisting" value="true"> 
    			<label for="cloneExisting"><span></span></label>
    			<label class="labelText">Clone from Existing:</label>
    		</div>
    	
    	</div>
    </div>
    
    <div class="row" id="cloneId">
    	<div class="col-md-6">
    		<div class="form-group">
                <label class="labelText">Enterprise:</label>
                <div class="dropdown-wrap">
                   <?php 
                        $spList     = $sps;
                        if($_SESSION['superUser'] == "3"){
                            $spList = $_SESSION['spList'];
                        }                     
                   ?>
                    <select name="enterprise" id="enterprise" class="selectSPClass">
                        <?php 
                            echo "<option value=''>None</option>";
                            foreach ($sps as $key => $value){
                                if(in_array($value, $spList)){
                                    echo "<option value='".$value."'>".$value."</option>";
                                }
                            }
                        ?>                                    
                    </select>
                </div>
            </div>
    	</div>
    	<div class="col-md-6">
    		<div class="form-group">
                <label class="labelText">Service Pack:</label>    
                <div class="dropdown-wrap">
                    <select name="clonedServicePackName" id="clonedServicePackName" class="selectSPClass">
                                                              
                    </select>
                </div>
            </div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12" id="errorDisplay" style="color:red;">
    		
   		</div>
    </div>
    
    <div class="row">
    	<div class="col-md-6">
    		<div class="form-group">
                <label class="labelText">Name:</label> 
                <span class="required">*</span>   
                <input type="text" name="spName" class="form-control selectSPClass" size="35" maxlength="30" id="spName">
            </div>
    	</div>
    	
    </div>
    <div class="row">
    	<div class="col-md-12">
    		<div class="form-group">
                <label class="labelText">Description:</label> 
                <input type="text" name="spDescription" class="form-control" size="35" maxlength="30" id="spDescription">
            </div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    	
    		<input type="checkbox" name="availableForUse" id="availableForUse" value="true"> 
    		<label for="availableForUse"><span></span></label>
    		<label class="labelText">Available For Use: </label>
    	</div>
    </div>
    
    <h2 class="subBannerCustom">Quantities</h2>
    <div class="row">
    	<div class="col-md-6">
    		<label class="labelText">Maximum Quantity Allowed: Unlimited</label> <br />
    		<label class="labelText" style="width:110px;">Quantity</label>
    		<input type="radio" name="qLimitation" id="qUnlimited" value="true" checked> 
    		<label for="qUnlimited"><span></span></label>
    		<label class="labelText">Unlimited</label><br />
    		
    		<label class="labelText" style="width:110px;">&nbsp;</label>
    		<input type="radio" name="qLimitation" id="qLimitedTo" value="true"> 
    		<label for="qLimitedTo"><span></span></label>
    		<label class="labelText">Limited To: </label> <input type="text" name="limitedQuantity" id="limitedQuantity" style="width:auto; float:right;" maxlength="6">
    		
    	</div>
    </div>
    
    <div class="clr"></div>
    <div class="row">
    	<div class="col-md-12">
    		<div class="">
                <div class="col-md-6 availNCurrentGroup">
                    <div class="form-group">Available Services <a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Drag Services that you want to appear on your Service Pack from Left to Right; Reorder Services by holding Left mouse and Drag; Select multiple by holding Ctrl and click;"><img src="images/NewIcon/info_icon.png"></a></div>
                </div>
                <div class="col-md-6 availNCurrentGroup">
                    <div class="form-group">Assigned Services <a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Drag Services that you want to appear on your Service Pack from Left to Right; Reorder Services by holding Left mouse and Drag; Select multiple by holding Ctrl and click;"><img src="images/NewIcon/info_icon.png"></a></div>
                </div> 
            </div>
    		<div class="">
                <div class="col-md-6" style="padding-left:0;">
                    <div class="form-group scrollableListCustom1">
						<ul id="sortable_1" class="sortable connectedSortable connectedSortableCustom ui-sortable">
							<?php 
							if(empty($serviceList["Error"])){
							    if(count($serviceList["Success"]["userServices"]) > 0){
							        foreach ($serviceList["Success"]["userServices"] as $key => $value){
							            echo "<li class=\"ui-state-default\" id=\"" . $value . "\">" . $value. "</li>";
							        }
							    }
							}
							     
							?>
						</ul>
					</div>
				</div>
				<div class="col-md-6" style="padding-right:0;">
                    <div class="form-group scrollableListCustom2">
						<ul id="sortable_2" class="sortable connectedSortable connectedSortableCustom ui-sortable">
							
							<?php 
							if(!empty($arr)){
							    if(count($arr) > 0){
							        foreach ($arr as $key => $value){
							            echo "<li class=\"ui-state-default\" id=\"" . $value . "\">" . $value. "</li>";
							        }
							    }
							}
							     
							?>
						</ul>
						<input type="hidden" name="servicePackUserServiceList" id="servicePackUserServiceList" value="" style="">
					</div>
				</div>
            </div>
    	</div>
    </div>
    <input type="hidden" name="oldSPName" value="" id="oldSPName">
    
</form>
<?php 

?>