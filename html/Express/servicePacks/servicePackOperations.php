<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/servicePacks/servicePacksController.php");
$obj = new ServicePacksController();
//print_r($_SESSION["servicePackToAdd"]);
//print_r($_SESSION["servicePackProcessModification"]);exit;

//Code added @ 31 Jan 2019
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
$expProvDB = new ExpressProvLogsDB();
//End code


if(isset($_SESSION["servicePackToAdd"]) && !empty($_SESSION["servicePackToAdd"])){
    $changeLogArr = array();
    foreach ($_SESSION["servicePackToAdd"] as $key => $value){
        $spArray = array();
        $spArray["serviceProviderId"] = $_SESSION["sp"];
        $spArray["servicePackName"] = $value["servicePackName"];
        $spArray["servicePackDescription"] = $value["servicePackDescription"];
        
        if(isset($value["isAvailableForUse"])){
            $spArray["isAvailableForUse"] = $value["isAvailableForUse"];
        }else{
            $spArray["isAvailableForUse"] = "false";
        }
        $spArray["servicePackQuantity"] = $value["servicePackQuantity"];
        
        $expVal = explode(";",$value["servicePackUserServiceList"]);
        array_pop($expVal);
        
        $spArray["serviceName"] = $expVal;
        
        $spAddResp[$value["servicePackName"]] = $obj->serviceProviderServicePackAddRequest($spArray);
        
        //Code added @ 31 Jan 2019 Regarding EX-1044
        $changeLogArr[$value["servicePackName"]]  = array(
                                'spName'                     => $value["servicePackName"],
                                'spDescription'              => ($value["servicePackDescription"]!="") ? $value["servicePackDescription"]: "None",
                                'availableForUse'            => $value["isAvailableForUse"],
                                'servicePackQuantity'        => ($value["servicePackQuantity"]!="") ? $value["servicePackQuantity"]: "None",   
                                'servicePackUserServiceList' => $value["servicePackUserServiceList"]
                               );
        //End code
        //print_r($spAddResp);
    }
    
    
    
    if(isset($spAddResp) && count($spAddResp) > 0){
        foreach ($spAddResp as $pKey => $pValue){
            if(!empty($pValue["Error"])){
                echo $pKey." : ".print_r($pValue["Error"])."<br />";
            }else{
                echo $pKey." : Service Pack has been added <br />";
                if(array_key_exists($pKey,  $changeLogArr)){
                        $changes        = array();
                        $changes        = $changeLogArr[$pKey];
                        $module         = "Service Pack Add";
                        $tableName      = "servicePackAddChanges";
                        $entityName     = $value["servicePackName"];
                        //echo "<pre>Changes - ";
                        //print_r($changes);
                        $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
                        $changeResponse = $cLUObj->changeLogAddUtility($module, $entityName, $tableName, $changes);
                }
            }
            
        }
    }
    
    
    unset($_SESSION["servicePackToAdd"]);
}
//print_r($_SESSION["servicePackProcessModification"]);
if(isset($_SESSION["servicePackProcessModification"]) && !empty($_SESSION["servicePackProcessModification"])){
    foreach ($_SESSION["servicePackProcessModification"] as $key => $value){
        $spArrayMod = array();
        $spArrayMod["serviceProviderId"] = $_SESSION["sp"];
        $spArrayMod["servicePackName"] = $key;
        $changes = array();
        
        if(isset($value["spName"])){            
            $spArrayMod["newServicePackName"] = $value["spName"];
        }
        
        if(isset($value["spDescription"])){
            $spArrayMod["servicePackDescription"] = $value["spDescription"];
        }
        
        if(isset($value["availableForUse"])){
            $spArrayMod["isAvailableForUse"] = $value["availableForUse"];
        }
        if(isset($value["spQuantity"])){
            $spArrayMod["servicePackQuantity"] = $value["spQuantity"];
        }
        //echo "modified array"; print_r($spArrayMod);
        $spModResp[$key] = $obj->serviceProviderServicePackModifyRequest($spArrayMod);
    }
    
    if(isset($spModResp) && count($spModResp) > 0){
        foreach ($spModResp as $pKey => $pValue){
            if(!empty($pValue["Error"])){
                echo "<b style='color:red;'>".$pKey." : "; print_r($pValue["Error"]); echo "</b><br />";
            }else{
                echo "<b>".$pKey." : Service Pack has been modified </b><br />";
                //Code added @ 31 Jan 2019
                foreach($_SESSION['servicePackToMod'] as $oldSPName => $oldSPRow){
                    if($pKey==$oldSPName){
                        if(isset($oldSPRow["servicePackName"])){
                            $changes["servicePackName"]["oldValue"] = $oldSPRow["servicePackName"]['oldValue'];
                            $changes["servicePackName"]["newValue"] = $oldSPRow["servicePackName"]['newValue'];
                        }
                        if(isset($oldSPRow["spDescription"])){
                            $changes["spDescription"]["oldValue"] = $oldSPRow["spDescription"]['oldValue'];
                            $changes["spDescription"]["newValue"] = $oldSPRow["spDescription"]['newValue'];
                        }
                        if(isset($oldSPRow["availableForUse"])){
                            $changes["availableForUse"]["oldValue"] = $oldSPRow["availableForUse"]['oldValue'];
                            $changes["availableForUse"]["newValue"] = $oldSPRow["availableForUse"]['newValue'];
                        }
                        if(isset($oldSPRow["spQuantity"])){
                            $changes["spQuantity"]["oldValue"] = $oldSPRow["spQuantity"]['oldValue'];
                            $changes["spQuantity"]["newValue"] = $oldSPRow["spQuantity"]['newValue'];
                        }
                    }
                }                
                $module         = "Service Pack Modify";
                $tableName      = "servicePackModChanges";
                $entityName     = $pKey;
                $cLUObj         = new ChangeLogUtility($_SESSION["sp"], "None", $_SESSION["loggedInUserName"]);
                $cLUObj->changesArr = $changes;
                $changeResponse = $cLUObj->changeLogModifyUtility($module, $entityName, $tableName);
                //End code
            }
            
        }
    }
    unset($_SESSION["servicePackProcessModification"]);
}

if((isset($_SESSION["servicePackProcessModification"]) && !empty($_SESSION["servicePackProcessModification"])) || (isset($_SESSION["servicePackToAdd"]) && !empty($_SESSION["servicePackToAdd"]))){
    unset($_SESSION["allServicePackArray"]);   
}
?>