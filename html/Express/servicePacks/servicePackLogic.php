<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/servicePacks/servicePacksController.php");
require_once("/var/www/html/Express/util/formDataUtil.php");
$formObj = new FormDataUtil();



$arrayProcessed = array();
$obj = new ServicePacksController();
$splist = $obj->serviceProviderServicePackGetListRequest($_SESSION["sp"]);
$usageResult = $obj->serviceProviderServicePackGetUtilizationListRequest($_SESSION["sp"]);
//$var = $splist["Success"];
//asort($var, SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
asort($splist["Success"], SORT_STRING | SORT_FLAG_CASE | SORT_NATURAL);
//print_r($splist);
if($_POST["funcAction"]=="showSPList"){
    unset($_SESSION["allServicePackArray"]);
    unset($_SESSION["servicePackToAdd"]);
    unset($_SESSION["servicePackToMod"]);
    unset($_SESSION["servicePackProcessModification"]);
    if(empty($splist["Error"])){
        if(count($splist["Success"]) > 0){
            
            foreach ($splist["Success"] as $key => $val){
                $resp = $obj->serviceProviderServicePackGetDetailListRequest($_SESSION["sp"], $val);
                if(!empty($resp["Success"])){
                    $arrayProcessed[$resp["Success"][servicePackName]] = $resp["Success"];
                }
            }
            
        }
    }
    
    
    if(count($arrayProcessed) > 0){
        foreach ($arrayProcessed as $tkey => $tval){
            if(array_key_exists($tkey, $usageResult["Success"]) && !empty($usageResult["Success"][$tkey])){
                $arrayProcessed[$tkey]["inUse"] = "Yes";
            }else{
                $arrayProcessed[$tkey]["inUse"] = "No";
            }
        }
    }
    //ksort($arrayProcessed);
    //print_r($arrayProcessed);
    echo  json_encode($arrayProcessed);
}

if($_POST["funcAction"]=="servicePackAdd"){
    $servicepackAddDummyArray = array();
    $formData = $formObj->getFormDataNameValue($_POST["data"]);
    //print_r($formData);
    if(isset($_SESSION["allServicePackArray"]) && count($_SESSION["allServicePackArray"]) > 0){
        $arrayProcessed = $_SESSION["allServicePackArray"];
    }else{
        if(empty($splist["Error"])){
            if(count($splist["Success"]) > 0){
                
                foreach ($splist["Success"] as $key => $val){
                    $resp = $obj->serviceProviderServicePackGetDetailListRequest($_SESSION["sp"], $val);
                    if(!empty($resp["Success"])){
                        $arrayProcessed[$resp["Success"][servicePackName]] = $resp["Success"];
                    }
                }
                
            }
        }
        
        if(count($arrayProcessed) > 0){
            foreach ($arrayProcessed as $tkey => $tval){
                if(array_key_exists($tkey, $usageResult["Success"]) && !empty($usageResult["Success"][$tkey])){
                    $arrayProcessed[$tkey]["inUse"] = "Yes";
                }else{
                    $arrayProcessed[$tkey]["inUse"] = "No";
                }
            }
        }
    }
    
    
    if(isset($formData["limitedQuantity"])){
        $servicePackQuantity = $formData["limitedQuantity"];
    }else{
        $servicePackQuantity = "unlimited";
    }
    $servicepackAddDummyArray = array(
        "isadded" => "added",
        "servicePackName" => $formData["spName"],
        "servicePackDescription" => $formData["spDescription"],
        "isAvailableForUse" => $formData["availableForUse"],
        "servicePackQuantity" => $servicePackQuantity,
        "servicePackUserServiceList" => $formData["servicePackUserServiceList"],
        "inUse" => "No"
    );
    $arrayProcessed[$formData["spName"]] = $servicepackAddDummyArray;
    $_SESSION["allServicePackArray"] = $arrayProcessed;
    if(isset($_SESSION["servicePackToAdd"]) && !empty($_SESSION["servicePackToAdd"])){
        foreach($_SESSION["servicePackToAdd"] as $newKey => $newVal){
            if($newVal["servicePackName"] == $formData["oldSPName"]){
                unset($_SESSION["servicePackToAdd"][$newKey]);
            }
        }
        
    }
    $_SESSION["servicePackToAdd"][] = $servicepackAddDummyArray;
    echo  json_encode($arrayProcessed);
}

if($_POST["funcAction"]=="servicePackMod"){
    $formDataMod = $formObj->getFormDataNameValue($_POST["data"]);
    //print_r($formDataMod);
    //print_r($_SESSION["spDetailsSession"]);
    $serviceArray = array();
    $spChangedArray = array();
    $originalPost = $_POST;
    $diff1 = array();
    if(!empty($_SESSION["spDetailsSession"])){
        $serviceArray["spName"] = $_SESSION["spDetailsSession"]["servicePackName"];
        $serviceArray["spDescription"] = $_SESSION["spDetailsSession"]["servicePackDescription"];
        $serviceArray["availableForUse"] = $_SESSION["spDetailsSession"]["isAvailableForUse"];
        if(isset($_SESSION["spDetailsSession"]["servicePackQuantity"]["unlimited"])){
            $serviceArray["spQuantity"] = "unlimited";
        }else{
            $serviceArray["spQuantity"] = $_SESSION["spDetailsSession"]["servicePackQuantity"]["quantity"];
        }
    }
    if(isset($formDataMod["limitQuantity"])){
        $formDataMod["spQuantity"] = $formDataMod["limitQuantity"];
    }else{
        $formDataMod["spQuantity"] = "unlimited";
    }
    unset($formDataMod["qLimitation"]);
    unset($formDataMod["limitQuantity"]);
    foreach($formDataMod as $nKey => $nValue){
        if(trim($nValue) != $serviceArray[$nKey]){            
            $diff1[$nKey]   = $nValue;
        }
    }
    
    //print_r($diff1);
    $valSess = $_SESSION["oldServicePackName"];
    $_SESSION["servicePackProcessModification"][$valSess]    = $diff1;
    //exit;
    $servicepackAddDummyArray = array();
    //$formData = $formObj->getFormDataNameValue($_POST["data"]);
    if(isset($_SESSION["allServicePackArray"]) && count($_SESSION["allServicePackArray"]) > 0){
        $arrayProcessed = $_SESSION["allServicePackArray"];
    }else{
        if(empty($splist["Error"])){
            if(count($splist["Success"]) > 0){
                
                foreach ($splist["Success"] as $key => $val){
                    $resp = $obj->serviceProviderServicePackGetDetailListRequest($_SESSION["sp"], $val);
                    if(!empty($resp["Success"])){
                        $arrayProcessed[$resp["Success"][servicePackName]] = $resp["Success"];
                    }
                }
                
            }
        }
        
        if(count($arrayProcessed) > 0){
            foreach ($arrayProcessed as $tkey => $tval){
                if(array_key_exists($tkey, $usageResult["Success"]) && !empty($usageResult["Success"][$tkey])){
                    $arrayProcessed[$tkey]["inUse"] = "Yes";
                }else{
                    $arrayProcessed[$tkey]["inUse"] = "No";
                }
            }
        }
    }
    //print_r($arrayProcessed);
    //print_r($_SESSION["servicePackProcessModification"]);
    foreach ($_SESSION["servicePackProcessModification"] as $modKey => $modValue){
        if (array_key_exists($modKey, $arrayProcessed)){
            if(isset($modValue["spName"])){
                $arrayProcessed[$modKey]["servicePackName"] = $modValue["spName"];
            }
            if(isset($modValue["spDescription"])){
                $arrayProcessed[$modKey]["servicePackDescription"] = $modValue["spDescription"];
            }
        }
    }
    
    /*if(isset($formData["limitedQuantity"])){
        $servicePackQuantity = $formData["limitedQuantity"];
    }else{
        $servicePackQuantity = "unlimited";
    }
    $servicepackAddDummyArray = array(
        "servicePackName" => $formData["spName"],
        "servicePackDescription" => $formData["spDescription"],
        "isAvailableForUse" => $formData["availableForUse"],
        "servicePackQuantity" => $servicePackQuantity,
        "servicePackUserServiceList" => $formData["servicePackUserServiceList"],
        "inUse" => ""
    );
    $arrayProcessed[$formData["spName"]] = $servicepackAddDummyArray;
    $_SESSION["allServicePackArray"] = $arrayProcessed;
    $_SESSION["servicePackToAdd"][] = $servicepackAddDummyArray;*/
    $_SESSION["allServicePackArray"] = $arrayProcessed;
    //$_SESSION["servicePackModToProcess"] = $_SESSION["servicePackToMod"];
    echo  json_encode($arrayProcessed);
}


?>