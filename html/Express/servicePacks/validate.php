<?php 
require_once("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();
$modValidate = false;
$addValidate = false;
if(isset($_SESSION["servicePackToAdd"]) && !empty($_SESSION["servicePackToAdd"])){
    $addValidate = true;
    $data = $errorTableHeader;
    $error = 0;
    //print_r($_SESSION["servicePackToAdd"]);
    $nonRequired = array();
    foreach ($_SESSION["servicePackToAdd"] as $key1 => $value1)
    {
        foreach ($value1 as $key => $value){
            if ($value !== ""){
                $bg = CHANGED;
            }else{
                $bg = UNCHANGED;
            }
            if($key == "inUse"){
                continue;
            }
            if($key == "isadded"){
                continue;
            }
            
            
            if($key == "servicePackUserServiceList"){
                if($value == ""){
                    $error = 1;
                    $bg = INVALID;
                    $value = "The service pack must contain at least one service.";
                }else{
                    $expVal = explode(";",$value);
                    array_pop($expVal);
                    $empVal = implode(", ",$expVal);
                    $value = $empVal;
                }
                
            }
            if ($bg != UNCHANGED)
            {
                $data .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:" . $bg . ";\">" . $_SESSION ["servicePackDialogArr"][$key] . "</td><td class=\"errorTableRows\">";
                if (is_array($value))
                {
                    foreach ($value as $k => $v)
                    {
                        $data .= $v . "<br>";
                    }
                }
                else if ($value !== "")
                {
                    $data .= $value;
                }
                else
                {
                    $data .= "None";
                }
                $data .= "</td></tr>";
            }
        }
        
        
    }
    $data .= "</table>";
}
//print_r($_SESSION["servicePackProcessModification"]);
if(isset($_SESSION["servicePackProcessModification"]) && !empty($_SESSION["servicePackProcessModification"])){
    $modValidate = true;
    $dataMod = '<table cellspacing="0" cellpadding="5" style="width:850px; margin: 0 auto;margin-top:50px;" class="confSettingTable">';
    $error = 0;
    //print_r($_SESSION["servicePackProcessModification"]);
    $nonRequired = array();
    foreach ($_SESSION["servicePackProcessModification"] as $key1 => $value1)
    {
        foreach ($value1 as $key => $value){
            $bg = CHANGED;
            /*if (isset($value["newValue"])){
                $bg = CHANGED;
            }else{
                $bg = UNCHANGED;
            }*/
            
            
            if ($bg != UNCHANGED)
            {
                $dataMod .= "<tr><td class=\"errorTableRows ccccc\" style=\"background:" . $bg . ";width:50%;\">" . $_SESSION ["servicePackDialogArr"][$key] . "</td><td class=\"errorTableRows\">";
                if (is_array($value))
                {
                    foreach ($value as $k => $v)
                    {
                        $dataMod .= $v . "<br>";
                    }
                }
                else if ($value !== "")
                {
                    $dataMod .= $value;
                }
                else
                {
                    $dataMod .= "None";
                }
                $dataMod .= "</td></tr>";
            }
        }
        
        
    }
    $dataMod .= "</table>";
}

if(!$modValidate && !$addValidate){
    $error = 1;
    $data = "You have made no changes.";
}



echo $error . $data . $dataMod;
?>