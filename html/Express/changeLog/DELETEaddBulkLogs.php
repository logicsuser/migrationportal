<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();

	require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
	$expProvDB = new ExpressProvLogsDB();
	
	$file = fopen('thorpeLogs.csv', 'r');
	$lineNumber = 0;
	while (($line = fgetcsv($file)) !== FALSE)
	{
		if ($lineNumber > 0) //don't use header row
		{
			echo $lineNumber . ":<br>";

			if (preg_match("/Huntgroup/", $line[5]))
			{
				$module = "Hunt Group Modify";
				if (preg_match("/:/", $line[5]))
				{
					$exp = explode(":", $line[5]);
					$entity = $exp[1];
				}
				else
				{
					$entity = $line[8];
				}				
			}
			else if (preg_match("/User:Add/", $line[5]))
			{
				$module = "User Add";
				$exp = explode("-", $line[7]);
				$entity = $exp[0];
				$phoneNumber = $exp[1];
			}
			else
			{
				$module = "User Modify";
				if (preg_match("/:/", $line[5]))
				{
					$exp = explode(":", $line[5]);
					$entity = $exp[1];
				}
				else
				{
					$entity = $line[8];
				}
			}

			$qry = "INSERT into changeLog (userName, date, module, groupId, entityName)";
			$qry .= " VALUES ('" . $line[2] . "', '" . $line[3] . " " . $line[4] . "', '" . $module . "', '" . $line[1] . "', '" . $entity . "')";
			echo $qry . "<br>";
//			$tpl = $expProvDB->expressProvLogsDb->query($qry);

			$lastId = $expProvDB->expressProvLogsDb->lastInsertId();

			if (preg_match("/Huntgroup/", $line[5]))
			{
				$insert = "INSERT into huntgroupModChanges (id, serviceId, entityName, field, oldValue, newValue)";
				$insert .= " VALUES ('" . $lastId . "', '" . $entity . "', '" . $entity . "', '" . $line[6] . "', '', '" . $line[7] . "')";
			}
			else if (preg_match("/User:Add/", $line[5]))
			{
				$exp = explode(" ", $entity);
				$insert = "INSERT into userAddChanges (id, firstName, lastName, phoneNumber)";
				$insert .= " VALUES ('" . $lastId . "', '" . $exp[0] . "', '" . $exp[1] . "', '" . $phoneNumber . "')";
			}
			else
			{
				$insert = "INSERT into userModChanges (id, serviceId, entityName, field, oldValue, newValue)";
				$insert .= " VALUES ('" . $lastId . "', '" . $entity . "', '" . $entity . "', '" . $line[6] . "', '', '" . $line[7] . "')";
			}

			echo $insert . "<br>";
			//			$pjo = $expProvDB->expressProvLogsDb->query($insert);
		}

		$lineNumber++;
	}
	fclose($file);
?>
