<?php
require_once("/var/www/html/Express/config.php");
checkLogin();

require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");


require_once("/var/www/lib/broadsoft/login.php");
require_once("/var/www/html/Express/userMod/UserCustomTagManager.php");
require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
$expProvDB = new ExpressProvLogsDB();

//Filters
$filters = array(
	array('changeLog.groupId', '=', $_SESSION["groupId"])
);
$startdate = "";
$enddate = "";
$module = "";
$entity_name = "";
$user_name = "";
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	if (isset($_POST['start_date']) && $_POST['start_date'] != "") {
		$startdate = trim($_POST['start_date']);
		$start_date = date("Y-m-d", strtotime($startdate));

		$filters[] = array('DATE(changeLog.date)', '>=', $start_date);
	}
	if (isset($_POST['end_date']) && $_POST['end_date'] != "") {
		$enddate = trim($_POST['end_date']);
		$end_date = date("Y-m-d", strtotime($enddate));

		$filters[] = array('DATE(changeLog.date)', '<=', $end_date);
	}

	if (isset($_POST['modules']) && $_POST['modules'] != "0") {
		$module = trim($_POST['modules']);
		$query .= " and module LIKE '%{$module}%'";
		$filters[] = array('changeLog.module', '=', "$module");
	}

	if (isset($_POST['entity_name']) && $_POST['entity_name'] != "0") {
		$entity_name = trim($_POST['entity_name']);
		$filters[] = array('changeLog.entityName', '=', "$entity_name");

	}
	if (isset($_POST['user_name']) && $_POST['user_name'] != "0") {
		$user_name = trim($_POST['user_name']);
		$filters[] = array('changeLog.userName', '=', "$user_name");

	}

}



$_SESSION['ChangeLogfilters'] = $filters ;


?>
<link rel="stylesheet" href="/Express/css/custom_bootstrap.css">
<link rel="stylesheet" href="/Express/css/jquery.dataTables.min.css">
<style>
    
 td.errorTableRows:nth-child(2) {
    color: #9c9c9c !important;
}

.noColumnTable td.errorTableRows:first-child {
   color: #6ea0dc !important;
}
table#example tbody tr td{display:flex;}
table.dataTable tbody th, table.dataTable tbody td {
     word-break: break-all;
 }
 
 div#ui-datepicker-div {
  top: 52px !important;
    left: 15px !important;
}
</style>
 <script>
    $(function () {      
    	var sourceSwapImage = function() {
            var thisId = this.id;
            var image = $("#"+thisId).children('img');
            var newSource = image.data('alt-src');
            image.data('alt-src', image.attr('src'));
            image.attr('src', newSource);
        }
        
		$("#downloadCSV1").hover(sourceSwapImage, sourceSwapImage);

    	
    	//$('##example_length').after('<div class="dropdown-wrap"></div>');

    	$('.ui-dialog-buttonpane').removeClass('.ui-button .ui-widget .ui-state-default .ui-corner-all .ui-button-icon-only .ui-dialog-titlebar-close .ui-state-focus');
        
        $("#module").html("> View Change Log");
        $("#endUserId").html("");

        $(".showLog").click(function () {
            var changeId = $(this).attr("id");
            $("#expand_" + changeId).dialog({
                autoOpen: false,
                width: 1000,
                modal: true,
                position: {my: "top", at: "top"},
                resizable: false,
                closeOnEscape: false,
                open: function(event) {
					setDialogDayNightMode($(this));
            		$('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('closeButton');
            	},
            	buttons: {
            		"Close": function()
            		{
            			$(this).dialog("close");
            		}

            	}
            });
                	$(this).closest( ".ui-dialog" ).find(":button").blur();
            	//},
            	
             //});

            $("#expand_" + changeId).dialog("open");
            if ($("#expand_" + changeId).length == 0) {
                alert("There is no log for that event.");
            }
        });
    });

    $(document).ready(function () {
        $("#example").dataTable({
        	 "order": [0, "desc"],
        	 "paging": true
             
        });
       // $('input.date_field').datepicker({dateFormat: 'mm/dd/yy', altFormat: "yy-mm-dd"});
        $('input.date_field').datepicker({dateFormat: 'mm/dd/yy', altFormat: "yy-mm-dd",beforeShow: function(input, obj) {
            $(input).after($(input).datepicker('widget'));
       }});
    });

    function filterChangeLog(filterForm) {
        var form_data = $(filterForm).serialize();
	  $("#mainBody").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
        $("#loading").show();
       // var form_data = $(filterForm).serialize();
        $.ajax({
            type: "POST",
            url: "changeLog/changes.php",
            data: form_data,
            success: function (result) {
                $("#loading").hide();
                $("#mainBody").html(result);
            }
        });

    }	       
	 
</script>
 
<h2 class="adminUserText">Change Log</h2>
<!-- <div class="selectContainer"> -->
<div class="selectContainer" style="padding-top: 0% !important;">
	<div class="">
			<form class="" id="change_log_form" name="change_log_form">
				<div class="row">

					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText">Start Date</label>
								<input value="<?php echo $startdate; ?>" type="text" data-date-format="mm-dd-yyyy" name="start_date" id="start_date" class="form-control date_field">


						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText">End Date</label>
							<input value="<?php echo $enddate; ?>" type="text" data-date-format="mm-dd-yyyy" name="end_date" id="end_date" class="form-control date_field">
						</div>
					</div>

				</div>
				<div class="row">

					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText">Module</label>
							<div class="dropdown-wrap">   
    							<select class="form-control" id="modules" name="modules">
    								<option value="0">Please Select a Module</option>
    								<?php
    								$module_types = get_change_log_modules($_SESSION["groupId"]);
    								 
    								foreach ($module_types as $module_type) {
    									?>
    									<option <?php echo $module_type->module == $module ? "SELECTED" : ""; ?> value="<?php echo $module_type->module ?>"><?php echo $module_type->module ?></option>
    									<?php
    								}
    								?>
								</select>
								</div>
						</div>
						
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText">Modified By</label>
							<div class="dropdown-wrap">   
							<select class="form-control" id="user_name" name="user_name">
								<option value="0">Please Select a User</option>
								<?php
								$users = get_all_admin_users();
								foreach ($users as $user) {
									?>
									<option <?php echo $user->userName == $user_name ? "SELECTED" : ""; ?> value="<?php echo $user->userName ?>"><?php echo $user->full_name ?></option>
									<?php
								}
								?>
							</select>
							</div>
						</div>
					</div>

				</div>

				<!--
				<div class="row form-group">
					<label class="col-xs-3 control-label">Entity Name</label>
					<div class="col-xs-8">
						<select class="form-control" id="entity_name" name="entity_name">
							<option value="0">Please Select a Entity Name</option>
							<?php
				$entityNames = get_change_log_entityNames($_SESSION["groupId"]);
				foreach ($entityNames as $entityName) {
					?>
								<option <?php echo $entityName->entityName == $entity_name ? "SELECTED" : ""; ?>
										value="<?php echo $entityName->entityName ?>"><?php echo $entityName->entityName ?></option>
								<?php
				}
				?>
						</select>
					</div>
				</div>
				-->


				<div class="row">
				<div class="col-md-12 alignBtn">
					<input type="button" onclick="filterChangeLog(this.form);" value="Search" class="subButton"/>
				</div>
				</div>
			</form>
	</div>
	<!-- <div style="clear:both;height:40px;/*width:80%;*/margin:0 auto;text-align:right;margin-bottom:8px;">
	 <input type="" name="downloadCSV" id="downloadCSV1" onclick="location.href='changeLog/printCSV.php';" style="cursor:pointer;"> 
    </div>-->
<div class="" name="downloadCSV" id="downloadCSV1" value="" style="margin-bottom: 8px;">
	<img src="images/icons/download_csv.png"  data-alt-src="images/icons/download_csv_over.png" onclick="location.href='changeLog/printCSV.php';">
	<br>
	<span>Download<br>CSV</span>
	
	</div>
	<div id = "changelogTableId" class="">
	   <!-- Data Append Here -->
	   
		<table class="display table table-bordered table-striped scroll changeLogTableClass" id="example">
			<thead>
			<tr>
				<th class="change thsmall">Date</th>
				<th class="change thsmall">Module</th>
				<th class="change thsmall">Entity Name</th>
				<th class="change thsmall">Modified By</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$log = "";

			$rows = get_change_logs($filters);
 			//echo "<pre>"; print_r($rows);

			foreach ($rows as $row) {
// 			    print_r($row); 
				$a = 0;
				$id = $row["id"];
                                //Code added @ 24 May 2019
                                $clusterName = $row["clusterName"];
                                $bwAppServer = $row["bwAppServer"];
                                if($bwAppServer == "primary"){
                                    $bwAppServer = "Primary";
                                }
                                if($bwAppServer == "secondary"){
                                    $bwAppServer = "Secondary";
                                }
                                $fqdn        = $row["fqdn"];
                                //End code
                                
                                
				echo "<tr>";
				echo "<td class=\"change thsmall\">" . $row["date"] . "</td>";
				echo "<td class=\"change thsmall\"><a href=\"#\" class=\"showLog\" id=\"" . $id . "\">" . $row["module"] . "</a></td>";
				echo "<td class=\"change thsmall\">" . $row["entityName"] . "</td>";
				echo "<td class=\"change thsmall\">" . ($row["modified_by"] ? $row["modified_by"] : $row["userName"]) . "</td>";
				echo "</tr>";

				$log .= "<div class=\"hideMe\" id=\"expand_" . $id . "\">";


				if ($row["module"] == "User Modify" or $row["module"] == "Soft Phone Modify" or $row["module"] == "Hunt Group Modify" or $row["module"] == "Auto Attendant Modify" or $row["module"] == "Call Center Modify" or $row["module"] == "Device Management Modification" or $row["module"] == "Delete User" or $row["module"] == "SCA-Only Devices Modification" or $row["module"] == "Modify Announcement" or $row["module"] == "Group Modify" or $row["module"] == "Change User Status" or $row["module"] == "Users:Group-Wide Modify" or $row["module"] == "Modify Device Config CustomTags" or $row["module"] == "Modify Sim Ring" or  $row["module"] == "Modify Sim Ring Criteria" or $row["module"] == "User Modify (Modify User's device profile Custom Tags)") {

                                        if ($row["module"] == "User Modify") {
						$tableName = "userModChanges";
					}else if ($row["module"] == "Group Modify"){
                                            $tableName = "groupModChanges";
                                        }else if ($row["module"] == "Modify Sim Ring Criteria"){
                                            $tableName = "simRingCriteriaModChanges";
                                        }else if ($row["module"] == "Modify Sim Ring"){
                                            $tableName = "simRingModChanges";
                                        }else if ($row["module"] == "Users:Group-Wide Modify"){
                                            $tableName = "userModChanges";
                                        }else if ($row["module"] == "Hunt Group Modify") {
						$tableName = "huntgroupModChanges";
					} else if ($row["module"] == "Auto Attendant Modify") {
						$tableName = "aaModChanges";
					}else if($row["module"] == "Soft Phone Modify"){
					    $tableName = "softPhoneModChanges";
					} else if ($row["module"] == "Call Center Modify") {
						$tableName = "callCenterModChanges";
					} else if($row["module"] == "Device Management Modification") {
					    $tableName = "deviceMgmtModChanges";					    
					} else if ($row["module"] == "Delete User") {
					    $tableName = "callCenterModChanges";
					}else if ($row["module"] == "Change User Status") {
					    $tableName = "userModChanges";
					} else if ($row["module"] == "Add Announcement") {
					    $tableName = $ociVersion == "19" ? "announcementAddChanges" : "announcementAddChanges21";
                                        } else if ($row["module"] == "SCA-Only Devices Modification") {
                                            $tableName = "deviceInventoryModChanges";
                                        } else if ($row["module"] == "Modify Announcement") {
                                            $tableName = "announcementModChanges";
                                        }else if ($row["module"] == "Call Pickup Groups Add") {
                                            $tableName = "callPickupGroupAddChanges";
                                        }                                        
                                        else if ($row["module"] == "Modify Device Config CustomTags") {
                                            $tableName = "deviceConfigCustomTagsModChanges";
                                        }
                                        else if ($row["module"] == "User Modify (Modify User's device profile Custom Tags)") {  
                                            $tableName = "userCustomTagsModChanges";
                                        }else if ($row["entityName"] == "BW app server connection") {
                                            $tableName = "serversFailOverChanges";
                                        }
                                        
                                        $select = "SELECT * from " . $tableName . " where id='" . $id . "'";
                                        //$select = "SELECT mod_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from $tableName as mod_tbl INNER JOIN changeLog as clog_tbl ON mod_tbl.id=clog_tbl.id  WHERE mod_tbl.id='$id'  ";
                                        
                                        
                                        $gth = $expProvDB->expressProvLogsDb->query($select);
                                        
					while ($r = $gth->fetch()) {
						if ($a == 0) {

                                                    $r["serviceId"] = getServiceIdForAnnouncement($r, $row, $ociVersion);
        
						    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
						    $log.="<tbody>";
                                                    
                                                    //Code added @ 24 May 2019                                                    
                                                    if($clusterName != "" && $clusterName != "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                                    if($fqdn != ""){
                                                            if($fqdn == "None"){
                                                                $log.="<tr>";
                                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                                $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                                $log.="</tr>";
                                                            }
                                                            if($fqdn != "None"){
                                                                $log.="<tr>";
                                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                                $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                                $log.="</tr>";
                                                            }
                                                    }
                                                    //End code
                                                    
						    $log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
						    $log.="</tr>";
						    $log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $row["entityName"] . '</td>';
						    $log.="</tr>";
						    
						    if ($row["module"] == "User Modify (Modify User's device profile Custom Tags)") {
						        $log.="<tr>";
						        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Type</td>';
						        $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceType"] . '</td>';
						        $log.="</tr>";
						        
						        $log.="<tr>";
						        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Name</td>';
						        $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceName"] . '</td>';
						        $log.="</tr>";
						        
						        $customTags = new UserCustomTagManager($r["deviceName"], $r["deviceType"]);
						        $customTagType = $customTags->checkCustomTagType($r["tagName"], $r["newValue"]);
						        $log.="<tr>";
						        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Tag Type</td>';
						        $log.='<td class="errorTableRows" style="width:50%">' . $customTagType . '</td>';
						        $log.="</tr>";
						        
						        $r["field"] = "Tag Value";
						    }
						    
						    $log.="</tbody>";
						    $log.="</table>"; 
                                                    //table for callPickgroup	     
						    $log.='<table style="width:830px; margin:0 auto;margin-top:30px" class="scroll tablesorter dataTable no-footer tableHeadHeight" cellspacing="0" role="grid" aria-describedby="example_info">';
						    //$log.='<table class="scroll tablesorter dataTable no-footer" style="width:830px; margin: 0 auto; margin-top:30px">';
						    $log.="<thead>";
						    $log.='<tr>';
						    $log.='<th class="thsmall" style="text-align: left !important;">Field Name</th>';
						    $log.='<th class="thsmall" style="text-align: left !important;">Old Value</th>';
						    $log.='<th class="thsmall" style="text-align: left !important;">New Value</th>';
						    $log.="</tr>";
						    $log.="</thead>";
						    $log.="<tbody>";						    
						    $a = 1;
							$log.='<tr>';
						}
						if ($r["oldValue"] == "") {
						     $r["oldValue"] = "&nbsp;";
						}
						if ($r["newValue"] == "") {
						     $r["newValue"] = "&nbsp;";
						}

						$log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["field"].'</th>';
						//$log .= "<div class=\"changeLeftB3\">" . $r["field"] . "</div>";


						if ($r["field"] == "Busy Lamp Fields") {
							$blfOld = "";
							$blfNew = "";
							$blfSelect = "SELECT state, blfUser from blfs where blfId='" . $id . "'";
							$blfResults = $db->query($blfSelect);

							while ($blfRow = $blfResults->fetch()) {
								if ($blfRow["state"] == "oldValue") {
									$blfOld .= $blfRow["blfUser"] . "; ";
								} else if ($blfRow["state"] == "newValue") {
									$blfNew .= $blfRow["blfUser"] . "; ";
								}
							}


							$log.='<th class="changeLogDilogTableWidth change thsmall">'.substr($blfOld, 0, -2).'</th>';
							$log.='<th class="changeLogDilogTableWidth change thsmall">'.substr($blfNew, 0, -2).'</th>';
						} 
                                                else if($r["field"] == "Announcement Type"){
                                                    
                                                       if($ociVersion == "19") {
						        $aOld = "";
						        $aNew = "";
						        $aSelect = "SELECT * from announcement_types where announcement_type_id = '" . $r["newValue"]. "'";
						        $aResults = $db->query($aSelect);
						        $gTthF = $aResults->fetch();						        
						        
                                                        $log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["oldValue"].'</th>';
                                                        $log.='<th class="changeLogDilogTableWidth change thsmall">'.$gTthF["name"].'</th>';
                                                        
						    } else {						        
                                                        $log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["oldValue"].'</th>';
                                                        $log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["announcementType"].'</th>';
						    }
						}
                                                else {
						    $log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["oldValue"].'</th>';
						    $log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["newValue"].'</th>';

						}
						$log.='</tr>';
					}
					
					//$log .= "<div class=\"vertSpacer\">&nbsp;</div>";
					
					//$log.="</thead>";
					$log.="</tbody>";
                                        $log.="</table>";
				}
				/* BW app server connection */
				if ( $row["entityName"] == "BW app server connection" ) {
				    $select = "SELECT * from serversFailOverChanges where id = '" . $id . "'";
				    $gth = $expProvDB->expressProvLogsDb->query($select);
				    
				    while ($r = $gth->fetch()) {
				        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
				        $log.="<tbody>";
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Connection Status</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["connection_status"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Status Message</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["status_message"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Server Detail</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["server_detail"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="</tbody>";
				        $log.="</table>";
				    }
				    
				}
				
				if ($row["module"] == "User Add") {
				    $select = "SELECT * from userAddChanges where id = '" . $id . "'";
                                    //$select = "SELECT usr_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from userAddChanges as usr_add_tbl INNER JOIN changeLog as clog_tbl ON usr_add_tbl.id=clog_tbl.id  WHERE usr_add_tbl.id='$id'  ";
                                    
				    $gth = $expProvDB->expressProvLogsDb->query($select);

					while ($r = $gth->fetch()) {
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
                                            
                                            //Code added @ 24 May 2019                                                    
                                            if($clusterName != "" && $clusterName != "None"){
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                $log.="</tr>";
                                            }
                                            if($fqdn != ""){
                                                    if($fqdn == "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                                    if($fqdn != "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                            }
                                            //End code
                                            
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Username</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["lastName"] . ", " . $r["firstName"] . '</td>';
					    $log.="</tr>";
        
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Name Dialing First Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["nameDialingFirstName"] . '</td>';
					    $log.="</tr>";
        
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Name Dialing Last Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["nameDialingLastName"] . '</td>';
					    $log.="</tr>";
        
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Phone Number</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["phoneNumber"] ? $r["phoneNumber"] : "None") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceType"] . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Email Address</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' .($r["emailAddress"] ? $r["emailAddress"] : "None"). '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Address</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' .($r["address1"] ? $r["address1"] : "None"). '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Suite</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' .($r["address2"] ? $r["address2"] : "None"). '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">City</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["city"] ? $r["city"] : "None") . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">State/Province</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["state"] ? $r["state"] : "None") . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Zip/Postal Code</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["zip"] ? $r["zip"] : "None") . '</td>';
					    $log.="</tr>";
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Pack</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["servicePacks"] ? $r["servicePacks"] : "None") . '</td>';
					    $log.="</tr>";
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Voice Messaging</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["voiceMessaging"] ? $r["voiceMessaging"] : "None") . '</td>';
					    $log.="</tr>";
                                            
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Shared Call Appreance</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["sharedCallAppreance"] ?$r["sharedCallAppreance"] : "None") . '</td>';
					    $log.="</tr>";
                                            
                                            
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Department</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["department"] ? $r["department"] : "None") . '</td>';
					    $log.="</tr>";
                                   
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Polycom Phone Services</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["polycomPhoneServices"] ? $r["polycomPhoneServices"] : "None") . '</td>';
					    $log.="</tr>";
						
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Custom Contact Directory</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["customContactDirectory"] ? $r["customContactDirectory"] : "None") . '</td>';
					    $log.="</tr>";						
					
					    $log.="</tbody>";
					    $log.="</table>";	
					}
					
					/* New Code found for change log*/
					
					/* Add User Soft Phone detail*/
					$selectSoft = "SELECT * from softPhoneAddChanges where id = '" . $id . "'";
					$gthSoft = $expProvDB->expressProvLogsDb->query($selectSoft);
					while ($rSoft = $gthSoft->fetch()) {

					    
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Soft Phone Account Device Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rSoft["softPhoneAcctDeviceType"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Soft Phone Account Device Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rSoft["softPhoneAcctDeviceName"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Soft Phone Account Line Port Domain</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rSoft["softPhoneAcctLinePortDomain"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="</tbody>";
					    $log.="</table>";
					     
					    /* $log .= "<div class=\"changeLeftB\">SoftPhone Acct Device Type</div>";
					    $log .= "<div class=\"changeD\">" . $rSoft["softPhoneAcctDeviceType"] . "&nbsp;</div>";
					    $log .= "<div class=\"changeB\">SoftPhone Acct Device Name</div>";
					    $log .= "<div class=\"changeD\">" . $rSoft["softPhoneAcctDeviceName"] . "&nbsp;</div>";
					    $log .= "<div class=\"changeLeftB\">SoftPhone Acct LinePort Domain</div>";
					    $log .= "<div class=\"changeD\">" . $rSoft["softPhoneAcctLinePortDomain"] . "&nbsp;</div>";
					    $log .= "<div class=\"vertSpacer\">&nbsp;</div>"; */

					}
					
					/* Add User Counter Path Detail*/
					$selectCounterPath = "SELECT * from counterPathAddChanges where id = '" . $id . "'";
					$gthCounterPath = $expProvDB->expressProvLogsDb->query($selectCounterPath);
					while ($rCounterPath = $gthCounterPath->fetch()) {

					    
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Group Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctGroupname"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Profile Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctProfilename"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account User Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctUsername"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Email Address</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctEmailAddr"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Authentication Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctAuthName"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account SIP Domain</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctSipDomain"] . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Proxy</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctSipProxy"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Device Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctDeviceType"] . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Device Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctDeviceName"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Counter Path Account Line Port Domain</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctLinePortDomain"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="</tbody>";
					    $log.="</table>";
					      
					}	
					/* End New Code found for change log*/
					
				}
				 
				 
				 if($row["module"] == "Soft Phone Added"){
				    /* Add User Soft Phone detail*/
				    $selectSoft = "SELECT * from softPhoneAddChanges where id = '" . $id . "'";
                                    //$selectSoft = "SELECT sftphone_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from softPhoneAddChanges as sftphone_add_tbl INNER JOIN changeLog as clog_tbl ON sftphone_add_tbl.id=clog_tbl.id  WHERE sftphone_add_tbl.id='$id'  ";
                                    
                                    
				    $gthSoft = $expProvDB->expressProvLogsDb->query($selectSoft);
				    while ($rSoft = $gthSoft->fetch()) {
                                                        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                                        $log.="<tbody>";
                                                        
                                                        //Code added @ 24 May 2019                                                    
                                                        if($clusterName != "" && $clusterName != "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                        if($fqdn != ""){
                                                                if($fqdn == "None"){
                                                                    $log.="<tr>";
                                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                                    $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                                    $log.="</tr>";
                                                                }
                                                                if($fqdn != "None"){
                                                                    $log.="<tr>";
                                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                                    $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                                    $log.="</tr>";
                                                                }
                                                        }
                                                        //End code
						 
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SoftPhone Acct Device Type</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rSoft["softPhoneAcctDeviceType"] . '</td>';
							$log.="</tr>";
						
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SoftPhone Acct Device Name</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rSoft["softPhoneAcctDeviceName"] . '</td>';
							$log.="</tr>";
						
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SoftPhone Acct LinePort Domain</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rSoft["softPhoneAcctLinePortDomain"] . '</td>';
							$log.="</tr>";
						
						$log.="</tbody>";
					    $log.="</table>";
						 
				    }
				}
				
				if($row["module"] == "Counter Path Account Added"){
				    
				    /* Add User Counter Path Detail*/
				    $selectCounterPath = "SELECT * from counterPathAddChanges where id = '" . $id . "'";
                                    //$selectCounterPath = "SELECT cntr_path_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from counterPathAddChanges as cntr_path_tbl INNER JOIN changeLog as clog_tbl ON cntr_path_tbl.id=clog_tbl.id  WHERE cntr_path_tbl.id='$id'  ";
                                    
				    $gthCounterPath = $expProvDB->expressProvLogsDb->query($selectCounterPath);
				    while ($rCounterPath = $gthCounterPath->fetch()) {
                                                        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                                        $log.="<tbody>";
						//
                                                        //Code added @ 24 May 2019                                                    
                                                        if($clusterName != "" && $clusterName != "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                        if($fqdn != ""){
                                                                if($fqdn == "None"){
                                                                    $log.="<tr>";
                                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                                    $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                                    $log.="</tr>";
                                                                }
                                                                if($fqdn != "None"){
                                                                    $log.="<tr>";
                                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                                    $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                                    $log.="</tr>";
                                                                }
                                                        }
                                                        //End code   
                                                        
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Group Name</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctGroupname"] . '</td>';
							$log.="</tr>";
						
						
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Profile Name</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctProfilename"] . '</td>';
							$log.="</tr>";
							
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct User Name</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctUsername"] . '</td>';
							$log.="</tr>";
							
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Email Address</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctEmailAddr"] . '</td>';
							$log.="</tr>";
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Authentication Name</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctAuthName"] . '</td>';
							$log.="</tr>";
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct SIP Domain</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctSipDomain"] . '</td>';
							$log.="</tr>";
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Proxy</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctSipProxy"] . '</td>';
							$log.="</tr>";
							
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Device Type</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctDeviceType"] . '</td>';
							$log.="</tr>";
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct Device Name</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctDeviceName"] . '</td>';
							$log.="</tr>";
							
							$log.="<tr>";
							$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">CounterPath Acct LinePort Domain</td>';
							$log.='<td class="errorTableRows" style="width:50%">' . $rCounterPath["counterPathAcctLinePortDomain"] . '</td>';
							$log.="</tr>";
						 
						$log.="</tbody>";
					    $log.="</table>";
					}
				}
				
			/* Group level custom tag */		
		            
                    if ($row["module"] == "Add Device Config CustomTags") {
				    $select = "SELECT * from deviceConfigCustomTagsAddChanges where id = '" . $id . "'";
                                    //$select = "SELECT dcctag_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from deviceConfigCustomTagsAddChanges as dcctag_tbl INNER JOIN changeLog as clog_tbl ON dcctag_tbl.id=clog_tbl.id  WHERE dcctag_tbl.id='$id'  ";
                                    
				    $gth = $expProvDB->expressProvLogsDb->query($select);

					while ($r = $gth->fetch()) {
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
                                            //Code added @ 24 May 2019                                                    
                                            if($clusterName != "" && $clusterName != "None"){
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                $log.="</tr>";
                                            }
                                            if($fqdn != ""){
                                                    if($fqdn == "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                                    if($fqdn != "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                            }
                                            //End code
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
					    $log.="</tr>";
						
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceType"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Tag Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["tagName"] . '</td>';
					    $log.="</tr>";	
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Tag Value</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["tagValue"] ? $r["tagValue"] : "None") . '</td>';
					    $log.="</tr>";
					
					    $log.="</tbody>";
					    $log.="</table>";	
					}
				}
      
				/* User level custom tag */
				if ($row["module"] == "User Modify (Add User's device profile Custom Tags)") {
				    $select = "SELECT * from userCustomTagsAddChanges where id = '" . $id . "'";
                                    //$select = "SELECT usr_CusTag_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from userCustomTagsAddChanges as usr_CusTag_tbl INNER JOIN changeLog as clog_tbl ON usr_CusTag_tbl.id=clog_tbl.id  WHERE usr_CusTag_tbl.id='$id'  ";
                                    
				    $gth = $expProvDB->expressProvLogsDb->query($select);

					while ($r = $gth->fetch()) {
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
                                            //Code added @ 24 May 2019                                                    
                                            if($clusterName != "" && $clusterName != "None"){
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                $log.="</tr>";
                                            }
                                            if($fqdn != ""){
                                                    if($fqdn == "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                                    if($fqdn != "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                            }
                                            //End code
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
					    $log.="</tr>";
						
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceType"] . '</td>';
					    $log.="</tr>";
					    
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceName"] . '</td>';
					    $log.="</tr>";
						
                                            $customTags = new UserCustomTagManager($r["deviceName"], $r["deviceType"]);
                                            $customTagType = $customTags->checkCustomTagType($r["tagName"], $r["tagValue"]);
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%"> Tag Type </td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $customTagType . '</td>';
					    $log.="</tr>";
						
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Tag Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["tagName"] . '</td>';
					    $log.="</tr>";	
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Tag Value</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["tagValue"] . '</td>';
					    $log.="</tr>";
					
					    $log.="</tbody>";
					    $log.="</table>";	
					}
					
				}
				
		
				//Code added @ 31 May 2018
				if ($row["module"] == "Add Group") {
                                    
				    $select = "SELECT * from groupAddChanges where id = '" . $id . "'";
                                    //$select = "SELECT grp_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from groupAddChanges as grp_add_tbl INNER JOIN changeLog as clog_tbl ON grp_add_tbl.id=clog_tbl.id  WHERE grp_add_tbl.id='$id'  ";
                                    
				    $gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) { 
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
                                            //Code added @ 24 May 2019                                                    
                                            if($clusterName != "" && $clusterName != "None"){
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                $log.="</tr>";
                                            }
                                            if($fqdn != ""){
                                                    if($fqdn == "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                                    if($fqdn != "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                            }
                                            //End code
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Group Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["groupName"] ? $r["groupName"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Default Domain</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["defaultDomain"] ? $r["defaultDomain"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Time Zone</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["timeZone"] . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Extension Length</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' .($r["extensionLength"] ? $r["extensionLength"] : ""). '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">User Limits</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' .($r["userLimit"] ? $r["userLimit"] : ""). '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Calling Line ID Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' .($r["callingLineIdName"] ? $r["callingLineIdName"] : ""). '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Calling Line ID Group Number</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["callingLineIdDisplayPhoneNumber"] ? $r["callingLineIdDisplayPhoneNumber"] : "") . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Contact Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["contactName"] ? $r["contactName"] : "") . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Contact E-mail</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["contactEmail"] ? $r["contactEmail"] : "") . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Contact Phone</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["contactNumber"] ? $r["contactNumber"] : "") . '</td>';
					    $log.="</tr>";
					 /////////////////////////////////////////////////
					 
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Address Line1</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["addressLine1"] ? $r["addressLine1"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Address Line2</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["addressLine2"] ? $r["addressLine2"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">City</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["city"] ? $r["city"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">State / Province</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["stateOrProvince"] ? $r["stateOrProvince"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Zip / Postal Code</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["zipOrPostalCode"] ? $r["zipOrPostalCode"] : "") . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Country</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["country"] ? $r["country"] : "") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="</tbody>";
					    $log.="</table>";
					    
					}
				}
                                //End Code
                                
				/* 
				 *  Site Management Module
				 * Add Sas Testing User 
				 * Show change log details 
				 */
				if ($row["module"] == "Add Sas Testing User") {
				    $select = "SELECT * from `sitemanagementAddChanges` WHERE `id` = '" . $id . "'";
                                    //$select = "SELECT site_magmt_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from sitemanagementAddChanges as site_magmt_add_tbl INNER JOIN changeLog as clog_tbl ON site_magmt_add_tbl.id=clog_tbl.id  WHERE site_magmt_add_tbl.id='$id'  ";
				    $gth = $expProvDB->expressProvLogsDb->query($select);
                                    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                    $log.="<tbody>";
                                    
                                    
                                    
				    while ($r = $gth->fetch()) 
                                    {
                                            //Code added @ 24 May 2019                                                    
                                            if($clusterName != "" && $clusterName != "None"){
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                $log.="</tr>";
                                            }
                                            if($fqdn != ""){
                                                    if($fqdn == "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                                    if($fqdn != "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                            }
                                            //End code

					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["SAS_NAME"] .'</td>';
					    $log.="</tr>";
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">'. $row["entityName"] . '</td>';
					    $log.="</tr>";
						
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SAS_IP</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["SAS_IP"] . '</td>';
					    $log.="</tr>";
						
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SAS_NAME</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["SAS_NAME"] . '</td>';
					    $log.="</tr>";
						
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SBC_ADDRESS</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["SBC_ADDRESS"] . '</td>';
					    $log.="</tr>";
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SBC_PORT</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["SBC_PORT"] . '</td>';
					    $log.="</tr>";
					    
				    }
				     $log.="</tbody>"; 
                                    $log.="</table>";
				}
				
				

                                /*
				 * Sim Ring Record
				 * Add Sim Ring Criteria
				 * Show change log details
				 */
                                if ($row["module"] == "Add Sim Ring Criteria") {
				    $select = "SELECT * from `simRingCriteriaAddChanges` WHERE `id` = '" . $id . "'";
                                    //$select = "SELECT add_sim_ring_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from simRingCriteriaAddChanges as add_sim_ring_tbl INNER JOIN changeLog as clog_tbl ON add_sim_ring_tbl.id=clog_tbl.id  WHERE add_sim_ring_tbl.id='$id'  ";
                                    
                                    
				    $gth = $expProvDB->expressProvLogsDb->query($select);
                                    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                    $log.="<tbody>";
                                    
                                    
                                    
				    while ($simRingResult = $gth->fetch()) {
				    $unavailableNumber =  $simRingResult['specificPhoneNumbers'];				

                                            //Code added @ 24 May 2019                                                    
                                            if($clusterName != "" && $clusterName != "None"){
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                $log.="</tr>";
                                            }
                                            if($fqdn != ""){
                                                    if($fqdn == "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                                    if($fqdn != "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                            }
                                            //End code
                                    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $simRingResult["serviceId"] .'</td>';
					    $log.="</tr>";
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Criteria Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">'. $simRingResult["simRingCriteriaDescription"] . '</td>';
					    $log.="</tr>";
						
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Time Schedule</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $simRingResult["timeSchedule"] . '</td>';
					    $log.="</tr>";
						
						$log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Holiday Schedule</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $simRingResult["holidaySchedule"] . '</td>';
					    $log.="</tr>";
						
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Call From</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $simRingResult["callFrom"] . '</td>';
					    $log.="</tr>";
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Private Number</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $simRingResult["privateNumber"] . '</td>';
					    $log.="</tr>";
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Unavailable Number</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $simRingResult["unavailableNumber"] . '</td>';
					    $log.="</tr>";
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Specific PhoneNumbers</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $unavailableNumber . '</td>';
					    $log.="</tr>";
					    
				    }
				     $log.="</tbody>"; 
                                    $log.="</table>";
				}				

				
				
				
			/*  Delete Sas Testing User
			 * Module Name Delete Sas Testing User
			 * use in place serviceId value table entityName
			*/	
				if ($row["module"] == "Delete Sas Testing User") {
				    $tableName = "changeLog";
				    $select = "SELECT * from " . $tableName . " WHERE `id` = ? ";
                                    
                                    
				    $pdoRes = $expProvDB->expressProvLogsDb->prepare($select);
				    $pdoArr = $pdoRes->execute(array($id));
				    $rows = $pdoRes->fetchAll(PDO::FETCH_ASSOC);
				    foreach ($rows as $r) {
                                        
                                            $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
                                            //Code added @ 24 May 2019                                                    
                                            if($clusterName != "" && $clusterName != "None"){
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                $log.="</tr>";
                                            }
                                            if($fqdn != ""){
                                                    if($fqdn == "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                                    if($fqdn != "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                            }
                                            //End code
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["entityName"] . '</td>';
					    $log.="</tr>";
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">'. $row["entityName"] . '</td>';
					    $log.="</tr>";					   
                                            $log.="</tbody>"; 
                                            $log.="</table>";				        
				    }
				}
				/* end */
				
				/*  Delete Device Custom Tag Config.
				 * Module Name Delete Sas Testing User
				 * use in place serviceId value table entityName
				 */
				if ($row["module"] == "Delete Device Config CustomTags") {
				    $tableName = "deviceConfigCustomTagsModChanges";
				   // $tableName = "changeLog";
				     $select = "SELECT * from " . $tableName . " WHERE `id` = ? ";
                                     //$select = "SELECT dcctag_mod_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from deviceConfigCustomTagsModChanges as dcctag_mod_tbl INNER JOIN changeLog as clog_tbl ON dcctag_mod_tbl.id=clog_tbl.id  WHERE dcctag_mod_tbl.id = ?  ";
                                    
                                    
                                    
				    $pdoRes = $expProvDB->expressProvLogsDb->prepare($select);
				    $pdoArr = $pdoRes->execute(array($id));
				    $rows = $pdoRes->fetchAll(PDO::FETCH_ASSOC);
				    foreach ($rows as $r) {                                        
                                        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                        $log.="<tbody>";
                                        
                                        //Code added @ 24 May 2019                                                    
                                        if($clusterName != "" && $clusterName != "None"){
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                $log.="</tr>";
                                            }
                                            if($fqdn != ""){
                                                    if($fqdn == "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                                    if($fqdn != "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                            }
                                        //End code
                                        
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
                                        $log.="</tr>";
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $r["entityName"] . '</td>';
                                        $log.="</tr>";					   
                                        $log.="</tbody>"; 
                                        $log.="</table>";
				    }
                                    
				}
				/* end */
                                
                                /* Group Modify Delete Department.
				 * Module Name Delete Sas Testing User
				 * use in place serviceId value table entityName
				 */
				if ($row["module"] == "Group Modify Delete Department") {
				    $tableName = "groupModChanges";
				   // $tableName = "changeLog";
				    $select = "SELECT * from " . $tableName . " WHERE `id` = ? ";
				    $pdoRes = $expProvDB->expressProvLogsDb->prepare($select);
				    $pdoArr = $pdoRes->execute(array($id));
				    $rows = $pdoRes->fetchAll(PDO::FETCH_ASSOC);
				    foreach ($rows as $r) {                                        
                                        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                        $log.="<tbody>";
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
                                        $log.="</tr>";
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $r["entityName"] . '</td>';
                                        $log.="</tr>";					   
                                        $log.="</tbody>"; 
                                        $log.="</table>";
				    }
                                    
				}
				/* end */
                                

                                if ($row["module"] == "Delete Sim Ring Criteria") {
				    $tableName = "simRingCriteriaModChanges";
				    // $tableName = "changeLog";
				    $select = "SELECT * from " . $tableName . " WHERE `id` = ? ";
                                    //$select = "SELECT sim_ring_mod_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from simRingCriteriaModChanges as sim_ring_mod_tbl INNER JOIN changeLog as clog_tbl ON sim_ring_mod_tbl.id=clog_tbl.id  WHERE sim_ring_mod_tbl.id = ?  ";
                                    
				    $pdoRes = $expProvDB->expressProvLogsDb->prepare($select);
				    $pdoArr = $pdoRes->execute(array($id));
				    $rows = $pdoRes->fetchAll(PDO::FETCH_ASSOC);
				    foreach ($rows as $r) {                                        
                                        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                        $log.="<tbody>";
                                        
                                        //Code added @ 24 May 2019                                                    
                                        if($clusterName != "" && $clusterName != "None"){
                                            $log.="<tr>";
                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                            $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                            $log.="</tr>";
                                        }
                                        if($fqdn != ""){
                                                if($fqdn == "None"){
                                                    $log.="<tr>";
                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                    $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                    $log.="</tr>";
                                                }
                                                if($fqdn != "None"){
                                                    $log.="<tr>";
                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                    $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                    $log.="</tr>";
                                                }
                                        }   
                                        //End code
                                        
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
                                        $log.="</tr>";
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $r["entityName"] . '</td>';
                                        $log.="</tr>";					   
                                        $log.="</tbody>"; 
                                        $log.="</table>";
				    }
                                    
				}
                                
                                

			    
				/* User custom tags delete*/
				if ($row["module"] == "User Modify (Delete User's device profile Custom Tags)") {
				    $tableName = "userCustomTagsModChanges";
				    // $tableName = "changeLog";
				    $select = "SELECT * from " . $tableName . " WHERE `id` = ? ";
                                    //$select = "SELECT usr_ctag_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from userCustomTagsModChanges as usr_ctag_tbl INNER JOIN changeLog as clog_tbl ON usr_ctag_tbl.id=clog_tbl.id  WHERE usr_ctag_tbl.id = ?   ";
                                    
				    $pdoRes = $expProvDB->expressProvLogsDb->prepare($select);
				    $pdoArr = $pdoRes->execute(array($id));
				    $rows = $pdoRes->fetchAll(PDO::FETCH_ASSOC);
				    foreach ($rows as $r) {                                        
                                        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                        $log.="<tbody>";
                                        
                                        //Code added @ 24 May 2019                                                    
                                        if($clusterName != "" && $clusterName != "None"){
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                $log.="</tr>";
                                            }
                                            if($fqdn != ""){
                                                    if($fqdn == "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                                    if($fqdn != "None"){
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                        $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                        $log.="</tr>";
                                                    }
                                            }
                                        //End code
                                        
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
                                        $log.="</tr>";
                                        $log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $r["entityName"] . '</td>';
                                        $log.="</tr>";
										
										$log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Type</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $r["deviceType"] . '</td>';
                                        $log.="</tr>";
										
										$log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Name</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $r["deviceName"] . '</td>';
                                        $log.="</tr>";
										
										$customTags = new UserCustomTagManager($r["deviceName"], $r["deviceType"]);
										$customTagType = $customTags->checkCustomTagType($r["tagName"], $r["newValue"]);
										$log.="<tr>";
                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Tag Type</td>';
                                        $log.='<td class="errorTableRows" style="width:50%">'. $customTagType . '</td>';
                                        $log.="</tr>";
										
                                        $log.="</tbody>"; 
                                        $log.="</table>";
				    }
                                    
				}
				
                if ($row["module"] == "Delete Announcement") {
				    $tableName = "announcementModChanges";
				   // $tableName = "changeLog";
				    $select = "SELECT * from " . $tableName . " WHERE `id` = ? ";
                                    //$select = "SELECT ann_mod_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from announcementModChanges as ann_mod_tbl INNER JOIN changeLog as clog_tbl ON ann_mod_tbl.id=clog_tbl.id  WHERE ann_mod_tbl.id = ?  ";
                                    
				    $pdoRes = $expProvDB->expressProvLogsDb->prepare($select);
				    $pdoArr = $pdoRes->execute(array($id));
				    $rows = $pdoRes->fetchAll(PDO::FETCH_ASSOC);
				    foreach ($rows as $r) {
                                    $r["serviceId"] = getServiceIdForAnnouncement($r, $row, $ociVersion);
                                                $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
						$log.="<tbody>";
                                                
                                                //Code added @ 24 May 2019                                                    
                                                if($clusterName != "" && $clusterName != "None"){
                                                    $log.="<tr>";
                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                    $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                    $log.="</tr>";
                                                }
                                                if($fqdn != ""){
                                                        if($fqdn == "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                        if($fqdn != "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                }
                                                //End code
                                                
						$log.="<tr>";
						$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
						$log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
						$log.="</tr>";
						$log.="<tr>";
						$log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
						$log.='<td class="errorTableRows" style="width:50%">'. $r["entityName"] . '</td>';
						$log.="</tr>";					   
						$log.="</tbody>"; 
						$log.="</table>";
				    }
				}
				//End code
				
				if ($row["module"] == "New Schedule" or $row["module"] == "Soft Phone Delete" or $row["module"] == "Edit Schedule" or $row["module"] == "Delete Schedule" or $row["module"] == "New Event" or $row["module"] == "Edit Event" or $row["module"] == "Delete Event" or $row["module"] == "New Custom Contact Directory" or $row["module"] == "Edit Custom Contact Directory" or $row["module"] == "Delete User" or $row["module"] == "Delete SCA Device"  or $row["module"] == "Delete AutoAttendant" or $row["module"] == "Delete Device" or $row["module"] == "Call Pickup Groups Delete") {
					if ($row["module"] == "New Custom Contact Directory" or $row["module"] == "Edit Custom Contact Directory") {
						$tableName = "ccdModChanges";
					} else if ($row["module"] == "Delete User") {
						$tableName = "userModChanges";
					}else if($row["module"] == "Soft Phone Delete"){
					    $tableName = "softPhoneModChanges";
					}else if ($row["module"] == "Delete AutoAttendant") {
						$tableName = "aaModChanges";
                                        } else if ($row["module"] == "Call Pickup Groups Delete") {
						$tableName = "callPickGroupModchanges";
                                        }else if ($row["module"] == "Delete Device") {
					    $tableName = "deviceInventoryModChanges";
					}else if ($row["module"] == "Delete SCA Device") {
					    $tableName = "deviceInventoryModChanges";
					}else {
						$tableName = "scheduleModChanges";
					}
					 
					$select = "SELECT * from " . $tableName . " where id='" . $id . "'";
					$gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) 
                                        {					    
                                                $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                                $log.="<tbody>";
                                                
                                                //Code added @ 24 May 2019                                                    
                                                if($clusterName != "" && $clusterName != "None"){
                                                    $log.="<tr>";
                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                    $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                    $log.="</tr>";
                                                }
                                                if($fqdn != ""){
                                                        if($fqdn == "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                        if($fqdn != "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                }
                                                //End code

                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
                                                $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
                                                $log.="</tr>";

                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
                                                $log.='<td class="errorTableRows" style="width:50%">' . $row["entityName"] . '</td>';
                                                $log.="</tr>";

                                                $log.="</tbody>"; 
						$log.="</table>";
						 
						 
						if ($row["module"] == "New Custom Contact Directory" or $row["module"] == "Edit Custom Contact Directory") 
                                                { 
						    $log.='<table class="scroll tablesorter dataTable no-footer tableHeadHeight" style="width:830px; margin: 0 auto; margin-top:30px">';
						    $log.="<thead>";
						    $log.='<tr>';
						    $log.='<th class="thsmall" style="text-align: left !important;">Field Name</th>';
						    $log.='<th class="thsmall" style="text-align: left !important;">New Value</th>';
						   
						    $log.="</tr>";
						    $log.="</thead>";
						    $log.="<tbody>";							
						    $log.="<tr>";							
							
							if ($r["newValue"] == "") 
                                                        {
							     $r["newValue"] = "&nbsp;";
							}
							
							$log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["field"].'</th>';
							$log.='<th class="changeLogDilogTableWidth change thsmall">'.$r["newValue"].'</th>';
							 
							$log.="</tr>";							
							$log.="</tbody>"; //table for field name 
							$log.="</table>";							
						} 
                                                else if ($row["module"] == "Delete User") 
                                                {
						    /*$log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $row["entityName"] . '</td>';
						    $log.="</tr>";*/
						     
                                                } 
                                                else if ($row["module"] == "Delete AutoAttendant") 
                                               {
                                                    /*$log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $row["entityName"] . '</td>';
						    $log.="</tr>";*/
                               
                                                }else if ($row["module"] == "Call Pickup Groups Delete") {
                                                    
                                                        /*$log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
                                                        $log.='<td class="errorTableRows" style="width:50%">' . $row["entityName"] . '</td>';
                                                        $log.="</tr>"; */
							
                                                        //End New work start
                                                }else if ($row["module"] == "Delete Device") 
                                                {

                                                    /*$log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $row["entityName"] . '</td>';
						    $log.="</tr>"; */


                                                } 
                                                else if ($row["module"] == "Delete SCA Device") 
                                                {
                                                    /*$log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Entity Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $row["entityName"] . '</td>';
						    $log.="</tr>";*/

						} 
                                                else 
                                                {
							if ($row["module"] == "New Event" or $row["module"] == "Edit Event" or $row["module"] == "Delete Event") 
                                                        {							    
							    
							    $log.="<tr>";
							    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Event Name</td>';
							    $log.='<td class="errorTableRows" style="width:50%">' . $r["eventName"] . '</td>';
							    $log.="</tr>";						    
							    
							}
							if ($row["module"] == "New Event" or $row["module"] == "Edit Event") 
                                                        {
							    
							    $log.="<tr>";
							    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Start Date</td>';
							    $log.='<td class="errorTableRows" style="width:50%">' . $r["startDate"] . '</td>';
							    $log.="</tr>";
							    
							    $log.="<tr>";
							    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Start Time</td>';
							    $log.='<td class="errorTableRows" style="width:50%">' . $r["startTime"] . '</td>';
							    $log.="</tr>";
							    
							    $log.="<tr>";
							    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">End Date</td>';
							    $log.='<td class="errorTableRows" style="width:50%">' . $r["endDate"] . '</td>';
							    $log.="</tr>";
							    
							    $log.="<tr>";
							    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">End Time</td>';
							    $log.='<td class="errorTableRows" style="width:50%">' . $r["endTime"] . '</td>';
							    $log.="</tr>";
							    
							    
							    $log.="<tr>";
							    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Recurs</td>';
							    $log.='<td class="errorTableRows" style="width:50%">' . $r["recurrence"] . '</td>';
							    $log.="</tr>";
							}
						}
						
						$log.="</tbody>"; //service Id and first table
						$log.="</table>";
						///table
					}
				}
				if ($row["module"] == "Admin Add" or $row["module"] == "Admin Modify" or $row["module"] == "Admin Delete") {
					$select = "SELECT * from adminChanges where id='" . $id . "'";
					$gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) {
					    
					    $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
					    $log.="<tbody>";
                                            
                                            //Code added @ 24 May 2019                                                    
                                            if($clusterName != "" && $clusterName != "None"){
                                                    $log.="<tr>";
                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                    $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                    $log.="</tr>";
                                                }
                                                if($fqdn != ""){
                                                        if($fqdn == "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                        if($fqdn != "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                }
                                            //End code
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["userName"] . '</td>';
					    $log.="</tr>";
					     
                                            if ($row["module"] == "Admin Add" or $row["module"] == "Admin Modify") {
						    
						    $log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">First Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $r["firstName"] . '</td>';
						    $log.="</tr>";
						    
						    
						    $log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Last Name</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $r["lastName"] . '</td>';
						    $log.="</tr>";
						    
						    
						    $log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Email Address</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $r["emailAddress"] . '</td>';
						    $log.="</tr>";
							 
						    $log.="<tr>";
						    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Disabled</td>';
						    $log.='<td class="errorTableRows" style="width:50%">' . $r["disabled"] . '</td>';
						    $log.="</tr>";							 
							 
                                                    if ($row["module"] == "Admin Add") 
                                                    {
                                                        $log.="<tr>";
                                                        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Super User</td>';
                                                        $log.='<td class="errorTableRows" style="width:50%">' . $r["superUser"] . '</td>';
                                                        $log.="</tr>";
                                                    }
							//$log .= "<div class=\"vertSpacer\">&nbsp;</div>";
						}
						
						$log.="</tbody>";
						$log.="</table>";
					}
				}
				if ($row["module"] == "Support Email") {
                                    
					$select = "SELECT * from supportEmailChanges where id='" . $id . "'";
                                        //$select = "SELECT sprt_email_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from supportEmailChanges as sprt_email_tbl INNER JOIN changeLog as clog_tbl ON sprt_email_tbl.id=clog_tbl.id  WHERE sprt_email_tbl.id='$id'  ";
                                        
                                        
					$gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) {
                                            
                                            //Code added @ 24 May 2019                                                    
                                            if($clusterName != "" && $clusterName != "None"){
                                                    $log.="<tr>";
                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                    $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                    $log.="</tr>";
                                                }
                                                if($fqdn != ""){
                                                        if($fqdn == "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                        if($fqdn != "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                }
                                            //End code
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["serviceId"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Description</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["description"] . '</td>';
					    $log.="</tr>";
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Details</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["details"] . '</td>';
					    $log.="</tr>";
					    
					 }
				}

				
				//add announcement
				if ($row["module"] == "Add Announcement") {
				    $annTableName = $ociVersion == "19" ? "announcementAddChanges" : "announcementAddChanges21";
					$select = "SELECT * from " . $annTableName . " where id = '" . $id . "'";
                                        //$select = "SELECT annc_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from $annTableName as annc_add_tbl INNER JOIN changeLog as clog_tbl ON annc_add_tbl.id=clog_tbl.id  WHERE annc_add_tbl.id='$id'  ";
                                        
                                        
                                        
					$gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) {
					  
                                            if($ociVersion == "19") {
						    $selectType = "SELECT * from announcement_types where announcement_type_id = '" . $r["announcementType"]. "'";
						    $gTth = $db->query($selectType);
						    $gTthF = $gTth->fetch();
						    $annType = $gTthF["name"];
                                            } else {
                                                $annType = $r["announcementType"];
                                            }
                                            
                                            $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                            $log.="<tbody>";
                                            
                                            //Code added @ 24 May 2019                                                    
                                            if($clusterName != "" && $clusterName != "None"){
                                                    $log.="<tr>";
                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                    $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                    $log.="</tr>";
                                                }
                                                if($fqdn != ""){
                                                        if($fqdn == "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                        if($fqdn != "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                } 
                                            //End code

                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["announcementName"] . '</td>';
					    $log.="</tr>";
        
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Announcement Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["announcementName"] . '</td>';
					    $log.="</tr>";

                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Announcement Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $annType . '</td>';
					    $log.="</tr>";

                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Provider</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["sp"] ? $r["sp"] : "None") . '</td>';
					    $log.="</tr>";

                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Filename</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["filenames"] . '</td>';
					    $log.="</tr>";

                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Group</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["annGroupName"] ? $r["annGroupName"] : "None") . '</td>';
					    $log.="</tr>";
                                            
                                            $log.="</tbody>"; 
                                            $log.="</table>";

					}
				}
                                
                                
                                
                                //Code added for Add Call Pickup Group
                                //Add Call Pickup Groups
                                //New work start
				if ($row["module"] == "Call Pickup Groups Add") {
                                    
					$select = "SELECT * from callPickupGroupAddChanges where id = '" . $id . "'";
                                        //$select = "SELECT cpg_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from callPickupGroupAddChanges as cpg_add_tbl INNER JOIN changeLog as clog_tbl ON cpg_add_tbl.id=clog_tbl.id  WHERE cpg_add_tbl.id='$id'  ";
                                        
					$gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) {
                                            
                                                $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                                $log.="<tbody>";
                                                
                                                //Code added @ 24 May 2019                                                    
                                            if($clusterName != "" && $clusterName != "None"){
                                                    $log.="<tr>";
                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                    $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                    $log.="</tr>";
                                                }
                                                if($fqdn != ""){
                                                        if($fqdn == "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                        if($fqdn != "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                }
                                            //End code
                                            
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
                                                $log.='<td class="errorTableRows" style="width:50%">' . $r["name"] . '</td>';
                                                $log.="</tr>";
                                                
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Call Pickup Group Name</td>';
                                                $log.='<td class="errorTableRows" style="width:50%">' . $r["name"] . '</td>';
                                                $log.="</tr>";
                                                
                                                $log.="</tbody>"; 
                                                $log.="</table>";
											
					}
				}
                                //End Code
                                
				
				//Add Auto Attendant
				if ($row["module"] == "Add AutoAttendant") {
					$select = "SELECT * from autoAttendantAddChanges where id = '" . $id . "'";
                                        //$select = "SELECT aa_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from autoAttendantAddChanges as aa_add_tbl INNER JOIN changeLog as clog_tbl ON aa_add_tbl.id=clog_tbl.id  WHERE aa_add_tbl.id='$id'  ";
                                        
					$gth = $expProvDB->expressProvLogsDb->query($select);
					while ($r = $gth->fetch()) { 
                                            
                                            $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                            $log.="<tbody>";
                                            
                                            //Code added @ 24 May 2019                                                    
                                            if($clusterName != "" && $clusterName != "None"){
                                                    $log.="<tr>";
                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                    $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                    $log.="</tr>";
                                                }
                                                if($fqdn != ""){
                                                        if($fqdn == "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                        if($fqdn != "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                }
                                            //End code
                                            
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["aaName"] . '</td>';
					    $log.="</tr>";
					    
					     
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Announcement Name</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["aaName"] . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Auto Attendant Type</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . $r["aaType"] . '</td>';
					    $log.="</tr>";
					    
					     
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Phone Number</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["phoneNumberAdd"] ? $r["phoneNumberAdd"] : "None") . '</td>';
					    $log.="</tr>";
					    

					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Calling Line Id LastName</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["callingLineIdLastName"] ? $r["callingLineIdLastName"] : "None") . '</td>';
					    $log.="</tr>";
					    
					     
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Extension</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["extensionAdd"] ? $r["extensionAdd"] : "None") . '</td>';
					    $log.="</tr>";
					    
					     
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Calling Line Id FirstName</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["callingLineIdFirstName"] ? $r["callingLineIdFirstName"] : "None") . '</td>';
					    $log.="</tr>";
						
						 
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">EV Support</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["eVSupport"] ? $r["eVSupport"] : "None") . '</td>';
					    $log.="</tr>";
					    
					     
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Time Zone</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["timeZone"] ? $r["timeZone"] : "None") . '</td>';
					    $log.="</tr>";
					    
					    
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Extension Dialing</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["extensionDialing"] ? $r["extensionDialing"] : "None") . '</td>';
					    $log.="</tr>";
						
						 
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Name Dialing</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["nameDialing"] ? $r["nameDialing"] : "None") . '</td>';
					    $log.="</tr>";
					    
					     
					    $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Name Dialing Entries</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["nameDialingEntries"] ? $r["nameDialingEntries"] : "None") . '</td>';
					    $log.="</tr>";
                                            
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Business Hours Schedule</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["businessHoursSchedule"] ? $r["businessHoursSchedule"] : "None") . '</td>';
					    $log.="</tr>";
                                            
                                            
                                            $log.="<tr>";
					    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Holiday Schedule</td>';
					    $log.='<td class="errorTableRows" style="width:50%">' . ($r["holidaySchedule"] ? $r["holidaySchedule"] : "None") . '</td>';
					    $log.="</tr>";                                            
                                            
                                            if($ociVersion == "21")
                                            {						    
                                                $log.="<tr>";
                                                $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">First Digit Time out Seconds</td>';
                                                $log.='<td class="errorTableRows" style="width:50%">' . ($r["firstDigitTimeoutSeconds"] ? $r["firstDigitTimeoutSeconds"] : "None") . '</td>';
                                                $log.="</tr>";
                                            }
                                            //$log .= "<div class=\"vertSpacer\">&nbsp;</div>";
                                            
                                            $log.="</tbody>"; 
                                            $log.="</table>";
					}
				}
				
				// Device Inventory Add Sca Device
				if ($row["module"] == "Add SCA-Only Devices") {
				    $select = "SELECT * from `deviceInventoryAddChanges` WHERE `id` = '" . $id . "'";
                                    //$select = "SELECT dInvnt_add_tbl.*, clog_tbl.clusterName, clog_tbl.bwAppServer, clog_tbl.FQDN  from deviceInventoryAddChanges as dInvnt_add_tbl INNER JOIN changeLog as clog_tbl ON dInvnt_add_tbl.id=clog_tbl.id  WHERE dInvnt_add_tbl.id='$id'  ";
                                    
				    $gth = $expProvDB->expressProvLogsDb->query($select);
				    while ($r = $gth->fetch()) {
                                        
                                        $log.="<table cellspacing='0' cellpadding='5' style='width:830px; margin: 0 auto;' class='confSettingTable noColumnTable'>";
                                        $log.="<tbody>";
                                        
                                        //Code added @ 24 May 2019                                                    
                                        if($clusterName != "" && $clusterName != "None"){
                                                    $log.="<tr>";
                                                    $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Cluster Name</td>';
                                                    $log.="<td class='errorTableRows' style='width:50%'>$clusterName &nbsp;</td>";
                                                    $log.="</tr>";
                                                }
                                                if($fqdn != ""){
                                                        if($fqdn == "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$bwAppServer &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                        if($fqdn != "None"){
                                                            $log.="<tr>";
                                                            $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">BW App Server</td>';
                                                            $log.="<td class='errorTableRows' style='width:50%'>$fqdn ($bwAppServer) &nbsp;</td>";
                                                            $log.="</tr>";
                                                        }
                                                }
                                        //End code

				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Service Id</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceName"] . '</td>';
				        $log.="</tr>";
 
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Device Type</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["deviceType"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Protocol</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["protocol"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Net Address</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["netAddress"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Port</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["port"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Transport Protocol</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["transportProtocol"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Mac Address</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["macAddress"] . '</td>';
				        $log.="</tr>";				        
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">SerialNumber</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["serialNumber"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Description</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["description"] . '</td>';
				        $log.="</tr>";			        
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">STUN Server</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["stunServerNetAddress"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Physical Location</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["physicalLocation"] . '</td>';
				        $log.="</tr>";
				        
				        $log.="<tr>";
				        $log.='<td class="errorTableRows" style="background:#72ac5d;width:50%">Shared Call Appearance Users</td>';
				        $log.='<td class="errorTableRows" style="width:50%">' . $r["scaUsers"] . '</td>';
				        $log.="</tr>";
                                        
                                        $log.="</tbody>"; 
                                        $log.="</table>";
				        
				    }
				}

				$log .= "</div>";
			}
			?>
			</tbody>
		</table>
		<?php
		echo $log;
		
		
		function getServiceIdForAnnouncement($r, $row, $ociVersion) {
		    if( ($row["module"] == "Modify Announcement" || $row["module"] == "Delete Announcement") && $ociVersion != "19") {
		        return $row["entityName"];
		    } else {
		        return $r["serviceId"];
		    }
		}
		?>
	</div>
       </div>
	<script>
 
	$(document).ready(function(){
    //	$('#example_length').attr('style', 'width: 00px !important');
    	$(".dataTables_length select:before,.dataTables_length label").addClass("dropdown-wrap");   
	});
 
	</script>
