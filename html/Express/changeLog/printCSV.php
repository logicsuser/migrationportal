<?php
	require_once("/var/www/html/Express/config.php");
	checkLogin();
	require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
	$expProvDB = new ExpressProvLogsDB();
	
	$fileName = $_SESSION["groupId"] . rand(1092918, 9281716);
	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment;filename=\"" . $fileName . ".csv\"");
	header("Content-Transfer-Encoding: binary");

        require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/functions.php");
        $changeLogData = get_change_logs($_SESSION['ChangeLogfilters'], "desc");
        
        /*end code */
	// $query = "SELECT changeLog.*, CONCAT(firstName, ' ' , lastName) as modified_by FROM expressProvLogs.changeLog LEFT JOIN adminPortal.users on users.userName = changeLog.userName where changeLog.groupId='" . $_SESSION["groupId"] . "' order by id desc";
	
	//  $fth = $expProvDB->expressProvLogsDb->query($query);
	$message = "S.No.,Date,Module,Entity Name,Modified By\n";
	$count = 1;
	//while ($row = $fth->fetch())
        foreach($changeLogData as $row)
	{
	    $modified_by = $row["modified_by"] != "" ? $row["modified_by"] : $row["userName"];
		$message .=
// 			"=\"" . $row["id"] . "\"," .
		    "=\"" . $count . "\"," .		
			"=\"" . $row["date"] . "\"," .
			"=\"" . $row["module"] . "\"," .
			"\"=\"\"" . $row["entityName"] . "\"\"\"," .
			"=\"" . $modified_by . "\"\n";
	       
		    $count++;
	}
	$fp = fopen("php://output", "a");
	fwrite($fp, $message);
	fclose($fp);
	
?>
