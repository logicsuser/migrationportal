<?php
	/* set the content type header */
	header("Content-type: text/css; charset: UTF-8");
	
	require_once ("/var/www/html/Express/config.php");
	
	$colorSchemaName = "fff";
	$iconNamePrefix = "../ExpressIcons/" . $colorSchemaName;
	
?>

.averistar-bs3 .panel.panel-forms .panel-heading {
    padding: 0 !important;
}

#groupEntTitleAdd:hover .tooltiptextEntGroups {
		visibility: visible;
	}
        
#example_paginate .disabled{background: <?php echo $buttonBase; ?> !important;	}

#example_previous .disabled:hover, #example_next .disabled:hover{
		background:tansparent !important;
}

#downloadExcelLink{
	text-decoration:none !important;
}


/* Nav menu Start*/

.activeNav:hover span, .activeNav:hover img, .activeNav span, .activeNav img {
	opacity:1 !important;
}

.rolloverIcon .activeNav:hover {
	background: <?php echo $buttonPressed; ?> !important;
	color: #ffffff !important;
	opacity:1;	
}

.rolloverIcon .activeNav span:hover {
	color: #ffffff !important;
	opacity:1;	
}

.activeNav span {
	color: #ffffff !important;
	/* border:1px; */
}

.activeNav {
	background: <?php echo $buttonPressed; ?> !important;
	color: #ffffff !important;
	/* border:1px; */
}

.navButton span, .navButton img {
	opacity:0.7;
}

.leftMenu .navButton span {
	color: <?php echo $navigationTextBase; ?> ;	
}

.leftMenu .navButton {
	background: <?php echo $buttonBase; ?> ;
	color: <?php echo $navigationTextBase; ?> ;
	box-shadow: none !important;
	/* border:1px; */
}

.navButton:hover span, .navButton:hover img {
	color: #ffffff !important;
	opacity:0.7;
}

.navButton:hover {
	background: <?php echo $buttonHover; ?> !important;
	color: #ffffff !important;
}

/* .navButton:focus {
	background: <?php echo $buttonPressed; ?> !important;
	color: #ffffff !important;
} */

/* Nav menu End*/

#tabs > ul > .active > a, #tabs > ul > .active > a:hover, #example_paginate .current{
	background: <?php echo $buttonPressed; ?> !important;
	color: <?php echo $buttonTextActive; ?> !important;
}

#example_paginate .current:hover{
	background: <?php echo $buttonHover; ?> !important;
	color: <?php echo $buttonTextActive; ?> !important;
}


/* class for active tabs*/
#tabs > ul .ui-state-active, #aaTabs > ul .ui-state-active, #hgTabs > ul .ui-state-active,
#tabs > ul .ui-state-active:hover, #aaTabs > ul .ui-state-active:hover, #hgTabs > ul .ui-state-active:hover,
#ccTabs > ul .ui-state-active, #ccTabs > ul .ui-state-active:hover
{
	background: <?php echo $buttonPressed; ?> !important;
}

#mainPageButton a:hover, #downloadExcelLink:hover, #mainPageButtonLink a:hover, .dataTables_paginate > span > .paginate_button:hover, .paginate_button .previous:hover, .paginate_button .next:hover {
	background: <?php echo $buttonHover; ?> !important;
	color: <?php echo $buttonTextActive; ?> !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button:hover{
background: <?php echo $buttonHover; ?> !important;
}

.dataTables_wrapper .dataTables_paginate .paginate_button.active{
	background: <?php echo $buttonHover; ?> !important;
	color: <?php echo $buttonTextActive; ?> !important;
}

#mainPageButton a:active, #downloadExcelLink:active, #mainPageButtonLink a:active
{
	background: <?php echo $buttonPressed; ?> !important;
	color: <?php echo $buttonTextActive; ?> !important;
}

input[type="submit"], input[type=button], #login input[type="submit"], button ,#downloadExcelLink,mainPageButtonLink, #login input[type="submit"]:focus {
	background: <?php echo $buttonBase; ?> !important;
	color: <?php echo $buttonTextBase; ?> !important;
	box-shadow: none !important;
	border:1px;
}

input[type="submit"]:hover, input[type=button]:hover, #login input[type="submit"]:hover, button:hover {
	background: <?php echo $buttonHover; ?> !important;
	color: <?php echo $buttonTextActive; ?> !important;
}

input[type="submit"]:active, input[type=button]:active, #login input[type="submit"]:active, button:active, button:focus {
	background: <?php echo $buttonPressed; ?> !important;
	color: <?php echo $buttonTextActive; ?> !important;
}


a:not([href="#"]), a small, .tablesorter a, .primaryUserModifyLink{
	 color: <?php echo $textLinkActive; ?> !important;
}

a:not([href="#"]):hover, a small:hover, .tablesorter a:hover, .primaryUserModifyLink:hover {
	color: <?php echo $textLinkHover; ?> !important;
}

/* Dailog box buttons start*/
.ui-button .ui-button-text {
	text-align: center;
    color: <?php echo $buttonTextBase; ?> !important;
}

.ui-dialog-buttonset .ui-button:focus {
    background-color: <?php echo $buttonPressed; ?> !important;
	color: <?php echo $buttonTextActive; ?> !important;
}
	
.ui-dialog-buttonset .ui-button:focus .ui-button-text{
color: <?php echo $buttonTextActive; ?> !important;
}


button[disabled=disabled]:hover .ui-button-text, button[disabled=disabled]:active .ui-button-text {
    color: <?php echo $buttonTextBase; ?> !important;
}

.ui-button:hover .ui-button-text, .ui-button:active .ui-button-text {
    color: <?php echo $buttonTextActive; ?> !important;
}

/* Dailog box buttons end*/

.usersTableColor{

	/* background-color : <?php //echo$userTableHiglight; ?> !important; 
	border: 2px solid <?php echo $userTableHiglight; ?> !important;
	margin:3px 0px;*/
	
}
table.customDataTable{border-spacing: 0px 10px;}

/* Start Code Added by Anshu @ 06 March 2018 - css for unregister user */
.unRegisterUsr{
	background-color : <?php echo $userTableHiglight; ?> !important;
/*	border: 1px solid <?php //echo $userTableHiglight2; ?> !important; */
}
.unRegisterUsr td {
   border-top: 2px solid <?php echo $userTableHiglight2; ?> !important;
   border-bottom: 2px solid <?php echo $userTableHiglight2; ?> !important;
}
.unRegisterUsr td:first-child {
   border-left: 2px solid <?php echo $userTableHiglight2; ?> !important;
}

#usersTable1 tbody tr.unRegisterUsr td:last-child {
   border-right: 2px solid <?php echo $userTableHiglight2; ?> !important;
}

.unRegisterUsr > .sorting_1{background-color : <?php echo $userTableHiglight; ?> !important;}

.usersTableColor td{
    border-top: 2px solid #d8dada !important;
  	border-bottom: 2px solid #d8dada !important;
   }
   .usersTableColor td:first-child{
   	border-left: 2px solid #d8dada !important;
   }
  /*  .DTFC_LeftBodyLiner{top: -10px !Important;} */
    table#usersTable1 {margin-top: -10px;} 
    .DTFC_RightWrapper{display:none;}

/* End Code */
.mainPageButton a {
	background: <?php echo $buttonBase; ?> !important;
	color: <?php echo $buttonTextBase; ?> !important;
	box-shadow: none !important;
}


/* Datepicker start */


#ui-datepicker-div .ui-state-active {
	background: <?php echo $buttonPressed; ?> !important;
 	color: <?php echo $buttonTextActive; ?> !important;
}

/* active date color*/
#ui-datepicker-div .ui-state-default ui-state-highlight {
 	background: <?php echo $buttonPressed; ?> !important;
 	color: <?php echo $buttonTextActive; ?> !important;
}


/* unselectable background color*/
#ui-datepicker-div .ui-datepicker-unselectable {
   /* background: */
}

/* box background color*/
#ui-datepicker-div .ui-state-default {
   background: <?php echo $buttonBase; ?>;
}

#ui-datepicker-div .ui-state-default:hover {
	 background: <?php echo $buttonHover; ?> !important;
	 color: <?php echo $buttonTextActive; ?> !important;
}

<?php 
//if($brandingcolorSchema=="Express Original" || $brandingcolorSchema == "Mountain Spring" || $brandingcolorSchema == "Averistar"){ ?>
    /*.ui-widget-header .ui-icon {
        background-image: url(../js/jquery-ui/css/smoothness/images/ui-icons_222222_256x240.png);
    }
    .ui-state-hover .ui-icon, .ui-state-focus .ui-icon{
        background-image:url(../images/ui-icons_ffffff_256x240.png);
    }*/
<?php //} ?>
<?php 
//if($brandingcolorSchema == "Honey"){ ?>
    /*.ui-widget-header .ui-icon {
        background-image: url(../images/ui-icons_ffffff_256x240.png);
    }
    .ui-state-hover .ui-icon, .ui-state-focus .ui-icon{
        background-image:url(../js/jquery-ui/css/smoothness/images/ui-icons_222222_256x240.png);
    }*/
<?php //} ?>

<?php 
//if($brandingcolorSchema == "London Fog" || $brandingcolorSchema == "Sea Monster" || $brandingcolorSchema=="Verizon"){ ?>
   /* .ui-widget-header .ui-icon {
        background-image: url(../images/ui-icons_ffffff_256x240.png);
    }
    .ui-state-hover .ui-icon, .ui-state-focus .ui-icon{
        background-image: url(../images/ui-icons_ffffff_256x240.png);
    }*/
<?php // } ?>

/* text color*/
#ui-datepicker-div .ui-datepicker-calendar a {
    color: <?php echo $buttonTextBase; ?>;
}


/*header background color */
#ui-datepicker-div .ui-datepicker th {
  //  background-color: #fff !important;
}

/* header text color */
#ui-datepicker-div .ui-datepicker th span {
  //  color: #31e72c;
}

/* datepicker title div */
#ui-datepicker-div .ui-datepicker-title {
  background: <?php echo $tabsBackground; ?> !important;
  color: <?php echo $buttonTextBase; ?> !important;
}

/* datepicker title text color */
#ui-datepicker-div span.ui-datepicker-year, span.ui-datepicker-month {
    color: <?php echo $buttonTextBase; ?> !important;
}

/* datepicker prev next button background-color */
.ui-datepicker-prev > .ui-icon, .ui-datepicker-next > .ui-icon {
    background: none !important;
}
a.ui-datepicker-prev.ui-corner-all:before {
    font-size: 15px;
    margin:5px;
    content: ' \25C4';
    position: absolute;
}
a.ui-datepicker-next.ui-corner-all:before {
    font-size: 15px;
    margin: 5px 8px;
    content: ' \25BA';
    position: absolute;
}

#ui-datepicker-div .ui-datepicker-prev {
	 background: <?php echo $buttonBase; ?> !important;
  	 color: <?php echo $buttonTextBase; ?> !important;
}

#ui-datepicker-div .ui-datepicker-prev:hover {
	 background: <?php echo $buttonHover; ?> !important;
  	 color: <?php echo $buttonTextActive; ?> !important;
   /*  text-decoration: none !important; */
}

#ui-datepicker-div .ui-datepicker-prev:active {
	background: <?php echo $buttonPressed; ?> !important;
	color: <?php echo $buttonTextActive; ?> !important;
}

/* datepicker new button background-color */
#ui-datepicker-div .ui-datepicker-next {
  	 background: <?php echo $buttonBase; ?> !important;
  	 color: <?php echo $buttonTextBase; ?> !important;
}

#ui-datepicker-div .ui-datepicker-next:hover {
  	 background: <?php echo $buttonHover; ?> !important;
  	 color: <?php echo $buttonTextActive; ?> !important;
  	 /* text-decoration: none !important;*/
}

#ui-datepicker-div .ui-datepicker-next:active {
	background: <?php echo $buttonPressed; ?> !important;
	color: <?php echo $buttonTextActive; ?> !important;
}
/* title border background color */
#ui-datepicker-div .ui-widget-header {
 	background: <?php echo $tabsBackground; ?> !important;
}

/* Datepicker start */



/* for tab Start */

.ExpresSheetTab {
   background: <?php echo $tabsBackground; ?> !important;
}

.ExpresSheetTab > ul > li > a{
   background: <?php echo $buttonBase; ?> !important;
}

.ExpresSheetTab > ul > li > a:hover {
  background: <?php echo $buttonHover; ?> !important;
  color: <?php echo $buttonTextActive; ?> !important;
}

.ExpresSheetTab > ul .active > a, .ExpresSheetTab > ul .active > a:hover
{
	background: <?php echo $buttonPressed; ?> !important;
}

.ExpresSheetTab > ul > li > a:focus, .ExpresSheetTab > ul > li > a:active
{
  background: <?php echo $buttonPressed; ?> !important;
}


#tabs > ul > li, #aaTabs > ul > li, #ccTabs > ul > li, #tabs > ul > li,#hgTabs > ul > li{
   background: <?php echo $buttonBase; ?> !important;
}

#tabs > ul > li:hover, #aaTabs > ul > li:hover, #ccTabs > ul > li:hover, #hgTabs > ul > li:hover {
  background: <?php echo $buttonHover; ?> !important;
}

#tabs > ul > li:focus, #tabs > ul > li:active, #aaTabs > ul > li:focus, #aaTabs > ul > li:active, #hgTabs > ul > li:active,#hgTabs > ul > li:focus,
#ccTabs > ul > li:focus, #ccTabs > ul > li:active
 {
  background: <?php echo $buttonPressed; ?> !important;
}

/* tab pressed */
.ui-tabs > ul > .ui-state-active > a, #tabs > ul > .ui-state-active > a, #aaTabs > ul > .ui-state-active > a,
#ccTabs > ul > .ui-state-active > a, #tabs > ul > .active > a, #hgTabs > ul > .ui-state-active > a
{
	color: <?php echo $buttonTextActive; ?> !important;
}

.ui-tabs > ul > .ui-state-active > a:hover, #tabs > ul > .ui-state-active > a:hover, #aaTabs > ul > .ui-state-active > a:hover,
#ccTabs > ul > .ui-state-active > a:hover, #tabs > ul > .active > a:hover, #hgTabs > ul > .ui-state-active > a:hover
{
	color: <?php echo $buttonTextActive; ?> !important;
}

.ui-tabs > ul > li > a, #tabs > ul > li > a, #aaTabs > ul > li > a, 
#ccTabs > ul > li > a, #hgTabs > ul > li > a
{
	color: <?php echo $buttonTextBase; ?> !important;
}

.ui-tabs > ul > li > a:hover, #tabs > ul > li > a:hover,
.ui-tabs > ul > li > a:active, #tabs > ul > li > a:active,
#aaTabs > ul > li > a:hover, #aaTabs > ul > li > a:active,
#hgTabs > ul > li > a:hover, #hgTabs > ul > li > a:active, 
#ccTabs > ul > li > a:hover, #ccTabs > ul > li > a:active
{
	color: <?php echo $buttonTextActive; ?> !important;
}

.ui-tabs .ui-tabs-nav{
	background: <?php echo $tabsBackground; ?> !important;
}

/* ExpressSheet start */

/* autoComplete */
.ui-autocomplete a {
    color: #333333 !important;
}

.adminAutoClass li a.aClassSuper {
	color: <?php echo "red"; ?> !important;
}

.ui-autocomplete li:hover a {
	color: <?php echo $buttonTextActive; ?> !important;
	background-color: <?php echo $buttonHover; ?> !important;
}
.ui-autocomplete li:hover .search_separator{color: <?php echo $buttonTextActive; ?> !important;}

.ui-state-hover, .ui-widget-content .ui-state-hover, 
.ui-widget-header .ui-state-hover, .ui-state-focus, 
.ui-widget-content .ui-state-focus, 
.ui-widget-header .ui-state-focus{
	background: <?php echo $buttonHover; ?> !important;
	color: <?php echo $buttonTextActive; ?> !important;
}

.ui-dialog-buttonset button[disabled=disabled], button[disabled=disabled],
.ui-dialog-buttonset button[disabled=disabled]:hover, button[disabled=disabled]:hover{
		background: <?php echo $buttonBase; ?> !important;	
		color: <?php echo $buttonTextBase; ?> !important;		
}


.fcorn-register p.register-submit input:active {
    box-shadow: none;
}



/* acodian collapse true or false*/
#generateAccordion > .panel-default > .panel-heading a[aria-expanded="true"], 
#adminPermissions .panel-default > .panel-heading a[aria-expanded="true"],
.filterTitleBtn[aria-expanded="true"],
#generateAccordion > .panel-default > .panel-heading a[aria-expanded="true"]:hover,
#adminPermissions .panel-default > .panel-heading a[aria-expanded="true"]:hover,
.filterTitleBtn[aria-expanded="true"]:hover {
    background: <?php echo $buttonPressed; ?> !important;
    color: <?php echo $buttonTextActive; ?> !important;
}

#generateAccordion > .panel-default > .panel-heading a,
#adminPermissions .panel-default > .panel-heading a,
.filterTitleBtn {
	padding: 10px 15px !important;
	background: <?php echo $buttonBase; ?> !important;
	color: <?php echo $buttonTextBase; ?> !important;
}

#generateAccordion > .panel-default > .panel-heading a:hover,
#adminPermissions .panel-default > .panel-heading a:hover,
.filterTitleBtn:hover {
	background: <?php echo $buttonHover; ?> !important;
	color: <?php echo $buttonTextActive; ?> !important;
}

#supervisoryPerRow, #devicePerRow, #servicesPerRow, #groupPerRow, #userPerRow {
	width: 100%;
    text-align: right;
    float: right;
}

.filterTitleBtn {
	text-align: center;
}

/* input type file */
input[type=file] {
	width: 0.1px;
	height: 0.1px;
	opacity: 0;
	overflow: hidden;
	position: absolute;
	z-index: -1;
}

.inputfile + .BrowseLabel {
    font-size: 1em;
    font-weight: 500;
    color: <?php echo $buttonTextBase; ?> !important;
    background-color:  <?php echo $buttonBase; ?> !important;
    display: inline-block;
}

.inputfile:focus + .BrowseLabel,
.inputfile + .BrowseLabel:hover {
    background-color:  <?php echo $buttonHover; ?> !important;
    color: <?php echo $buttonTextActive; ?> !important;
}

.inputfile:focus + .BrowseLabel,
.inputfile + .BrowseLabel:active {
    background-color:   <?php echo $buttonPressed; ?> !important;
    color: <?php echo $buttonTextActive; ?> !important;
}

.BrowseLabel{
/* margin-bottom: 16px; */
	cursor: pointer;
    height: 30px;
    width: 95.422px;
    padding-left: 16px;
    padding-top: 5px;
}
/* input type file End*/

.ui-dialog .ui-dialog-buttonpane button {
    border: 1px solid #eeeeee !important;
}


#infoIconES {
	border-color: transparent !important;
	background-color: transparent !important;
}
/* Disabled Browse Button*/

.mainPageIconDivUser,
.mainPageIconDivGroup,
.mainPageIconDivInv {
    border-radius: 70px;
    padding: 12px; 
    width: 70px;
    height: 70px;
    text-align:center;
        margin: 0 auto;
}

.mainPageIconDivUser img,
.mainPageIconDivGroup img,
.mainPageIconDivInv img {
    width: 45px !important;
}


.mainPageIconDivUser
{
	background:#00088c;
}

.mainPageIconDivGroup
{
	background:#1bb553;
}

.mainPageIconDivInv{
	background:#ed7000;
}

.feature-list li a {
    text-align: center;
    text-align: -moz-center;	
}


/* Tool tip on Administrator page */
#userPerRowAdd, #groupPerRowAdd, #servicesPerRowAdd, #devicePerRowAdd, #supervisoryPerRowAdd,
#userPerRow, #groupPerRow, #servicesPerRow, #devicePerRow, #supervisoryPerRow
{
    position: relative;
    display: inline-block;
}

.panel-forms .tooltiptext {
    visibility: hidden;
    width: 250px;
    background-color: #d7d7d7;
    color: white;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    
    /* Position the tooltip */
    position: absolute;
    z-index: 1;
    top: 0%;
    left: 16%;
    margin-left: -127px;
}
 

#usersTitle:hover .tooltiptextUsers, #usersTitleAdd:hover .tooltiptextUsers {
    visibility: visible;
}

#usersTitleMod:hover .tooltiptextUsers, #usersTitleAdd:hover .tooltiptextUsers {
    visibility: visible;
}



#groupTitle:hover .tooltiptextGroups, #groupTitleAdd:hover .tooltiptextGroups {
    visibility: visible;
}

#groupTitleMod:hover .tooltiptextGroups, #groupTitleMod:hover .tooltiptextGroups {
    visibility: visible;
}
 
 

#servicesTitle:hover .tooltiptextServices, #servicesTitleAdd:hover .tooltiptextServices {
    visibility: visible;
}

#servicesTitleMod:hover .tooltiptextServices{
    visibility: visible;
}
 
#supervisoryTitleMod:hover .tooltiptextSupervisory  {
    visibility: visible;
}



#devicesTitle:hover .tooltiptextDevices, #devicesTitleAdd:hover .tooltiptextDevices {
    visibility: visible;
}

#devicesTitleMod:hover .tooltiptextDevices{
    visibility: visible;
}
#supervisoryTitle:hover .tooltiptextSupervisory, #supervisoryTitleAdd:hover .tooltiptextSupervisory {
    visibility: visible;
}

/* color for switch button in user filter */
.switch-input:checked ~ .switch-label {
	background: <?php echo $buttonBase; ?> !important;
	box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.15), inset 0 0 3px rgba(0, 0, 0, 0.2);
	width:66px;
}

/* DataTables Custom Pagination */
.dataTables_bottom_info_bar {
	text-align: center;
}
.dataTables_bottom_info_bar .dataTables_info {
	text-align: center;
	display: inline-block;
}
.dataTables_bottom_info_bar .currently_showing_info {
	display: inline-block;
}
.dataTables_bottom_info_bar .retrieving_info {
	display: inline-block;
	float: left;
	padding-top: 0.755em;
}
.paginate_jump_input {
	width: 50px;
	text-align: center;
}
.paginate_jump_to {
	margin-left: 30px;
}
.paginate_jump_text {
	color: gray;
}

/* DataTables - Detailed User - Top Bar */
.usersDetailedListTable_top_bar {
	margin-top: 20px;
}
.usersDetailedListTable_top_bar .column1,
.usersDetailedListTable_top_bar .column2,
.usersDetailedListTable_top_bar .column3,
.usersDetailedListTable_top_bar .column4{
	display: inline-block;
	vertical-align: top;
}
.usersDetailedListTable_top_bar .column1,
.usersDetailedListTable_top_bar .column2 {
	width: 15%;
}
.usersDetailedListTable_top_bar .column3 {
	width: 30%;
}
.usersDetailedListTable_top_bar .column4{
	width: 40%;
}
.hideInitialDetailInfo_selectAllDiv {
	margin-top: 15px;
}
.usersDetailedListTable_action_buttons {
	text-align: right;
}

/* DataTables - Basic User - Top Bar */
.usersBasicListTable_top_bar {
margin-top: 20px;
}
.usersBasicListTable_top_bar .column1,
.usersBasicListTable_top_bar .column2,
.usersBasicListTable_top_bar .column3,
.usersBasicListTable_top_bar .column4{
display: inline-block;
vertical-align: top;
}
.usersBasicListTable_top_bar .column1,
.usersBasicListTable_top_bar .column2 {
width: 15%;
}
.usersBasicListTable_top_bar .column3 {
width: 30%;
}
.usersBasicListTable_top_bar .column4{
width: 40%;
}
.hideInitialBasicInfo_selectAllDiv {
margin-top: 15px;
}
.usersBasicListTable_action_buttons {
text-align: right;
}

.usersDetailedListTable_top_bar .column4 .dataTables_length {
float: right;
}
.usersBasicListTable_top_bar .column4 .dataTables_length {
float: right;
}