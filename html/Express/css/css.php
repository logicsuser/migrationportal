<?php
	/* get settings from .ini file */
	$settings = parse_ini_file("/var/www/adminPortal.ini", TRUE);

	/* set the content type header */
	header("Content-type: text/css");

	$font = "proxima-nova, sans-serif";
?>
/* element selectors 
* {
	margin: 0px;
	font-size: 12px;
	color: #444444;
}*/
html {
	height: 100%;
	/* overflow-y: scroll; */
}
body {
	height: 100%;
	font-family: <?php echo $font; ?> !important;
	font-size: 15px;
	<!-- background: #FFF !important; -->
}
form {
	width: 99%;
	margin-left: auto;
	margin-right: auto;
}
label {
	display: inline-block;
}
input[type = checkbox], input[type = radio] {
	border: 0px;
	margin: 2px;
}
a:link, a:visited {
	color: #222222;
	font-size: 100%;
}
a:active{
	color: #0066CC;
}

a:hover {
	color: red;
}
ol {
	padding-left: 0px;
	padding-right: 0px;
	margin: 0px;
	list-style-type: none;
}
p {
	margin-top: 10px;
	margin-bottom: 10px;
}
blockquote {
	margin: 20px;
}

/* other selectors */
.separator {
	float: left;
	clear: both;
	height: 1px;
	width: 100%;
	background-color: #DDDDDD;
}
.leftMenu {
	float: left;
	clear: both;
	width: 15%;
	color: black;
	margin-top: -0px;
	height: 100vh;
	background: #26344B !important;
}
.spacer {
	display: inline-block;
	width: 25px;
}
.vertSpacer {
	height: 25px;
	width: 100%;
	float: left;
	clear: both;
	z-index: 10;
}
.vertMiniSpacer {
	height: 1px;
	width: 100%;
	float: left;
	clear: both;
}
#location {
	height: 20px;
	width: 49%;
}
.socialMedia {
	border: 0px;
	display: block;
	float: left;
	padding: 2px;
}
 
#passwordResetForm{
	position: absolute;
	top: 0;
	left: 0;
	float: left;
}

#bodyForm {
	/*max-height: 560px;
	width: 460px;
	overflow: auto;
	margin: auto;
	text-align: center;
	background: #FFFFFF;
	padding: 60px;
	bottom: 0;
	right: 0;*/
}
#bodyForm img {
	width: 80%;
	padding-bottom: 25px;
}
#groupForm {
	/* margin-left: 40%; */
	/* margin-top: 20%; */
	width: 500px;
	margin: auto;
	text-align: center;
	bottom: 0;
	right: 0;
	height: 200px;
}

#passwordResetForm {
	margin-left: 40%;
	margin-right: 40%;
	margin-top: 20%;
	width: 20%;
}
<!-- #sysAdminForm { -->
<!-- 	margin-left: 19%; -->
<!-- 	margin-right: 19%; -->
<!-- 	margin-top: 14%; -->
<!-- 	width: 62%; -->
<!-- } -->

.navButton, .navButtonSuperUser {
	display: block;
	//text-align: center;
	vertical-align: middle;
	width: auto;
	text-decoration: none !important;
	//color: #7C8BA2 !important; //TODO: Make it configurable;
  color: #F6F6F6;
	//border-bottom: 1px solid #3E4A62; //TODO: Make it configurable;
  border-bottom: 1px solid #747676;
	padding: 15px;
	font-size: 14px !important;
	font-weight: 500;
}
.navButton:hover, .navButtonSuperUser:hover {
//	background-color: #212D41; //TODO: Make it configurable;
  background-color: #A80309;
	color: white;
}
.navButton {
	//background-color: #26344B; //TODO: Make it configurable;
	background-color: #000000;
}
.navButtonSuperUser {
	background-color: #C0C0C0;
}
a.active {
	background-color: #FFF;
	color: white;
}
.subBanner, .miniBanner {
	letter-spacing: .05em;
	padding: 0.8em;
	margin: 0 auto;
	-moz-border-radius: 10px 10px 0 0;
	-webkit-border-radius: 10px 10px 0 0;
	border-radius: 10px 10px 0 0;
	text-align: center;
	clear: both;
}
.subBanner {
	font-size: 2em;
	width: 100%;
	color: #242424;
	background: #FFF;
	border-bottom: 1px solid #D8DADA;
}
.miniBanner {
	font-size: 1em;
	font-style: italic;
	width: 95%;
	color: #AEB6AA;
}
.leftDesc, .rightDesc, .leftScrollableDesc, .rightScrollableDesc, .leftDesc2, .quarterDesc, .inputText, .leftDesc3, .thirdDesc {
	text-align: left;
	float: left;
	min-height: 25px;
	height: auto;
}
.leftDesc {
	clear: both;
	width: 40%;
}
.rightDesc {
	width: 59%;
}
.leftScrollableDesc {
	clear: both;
	width: 50%;
}
.rightScrollableDesc {
	width: 49%;
}
.leftDesc2 {
	clear: both;
	width: 20%;
}
.quarterDesc {
	width: 20%;
}
.inputText {
	width: 29%;
}
.leftDesc3 {
	clear: both;
	width: 25%;
}
.thirdDesc {
	width: 37%;
}
.centerDesc {
	clear: both;
	text-align: center;
	min-width: 30%;
	margin-left: auto;
	margin-right: auto;
	height: 25px;
}
.ui-widget-overlay {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #000;
	opacity: .60;
	filter: Alpha(Opacity=60);
}
.ui-dialog .ui-dialog-buttonpane button {
	margin-left: 25px;
}
.center {
	position: relative;
	float: left;
	height: 25px;
	margin: auto auto auto auto;
}
.required {
	display: inline;
	color: red;
}
.errorTableRows {
	border: 1px solid;
	border-color: #FFFFFF;
	background: #EEEEEE;
}
#availableUserServiceToAssign, #assignedUserService,#sortable1, #sortable_1, #sortable2, #sortable_2, #sortable3, #sortable_3, #sortable4, #sortable_4 {
	min-height: 250px;
}
#availableUserServiceToAssign li, #assignedUserService li ,#sortable1 li, #sortable_1 li, #sortable2 li, #sortable_2 li, #sortable3 li, #sortable_3 li, #sortable4 li, #sortable_4 li  {
	margin: 5px 0 5px 0;
	padding: 5px;
	font-size: 1.2em;
	width: 95%;
	min-height: 20px;
	background: #F6F6F6;
        color; #555555;
	/* color: black; */
}
.scrollableList {
	height: 250px;
	width: 60%;
	margin-left: auto;
	margin-right: auto;
	overflow-y: scroll;
	border: 1px solid;
}
#userData,#userData2, #hgData, #ccData, #AAData, #schedData, #eventData, #adminData, #csvData, #cdrResults {
	float: left;
	clear: both;
	height: auto;
	width: 100%;
	margin-top: 20px;
}
.dialogClass {
	font-size: 12px;
}
.filters {
	width: 50%;
	height: 100%;
	margin: 0 auto;
}
#id#from {
	font-size: 13px;
	height: 15px;
	width: 25%;
}
table.tablesorter {
	font-family: <?php echo $font; ?>;
	<!-- border: 1px solid #CDCDCD; -->
	border-spacing: 0px;
	margin: 10px 0pt 15px;
	font-size: 8pt;
	width: 100%;
	text-align: left;
}
table.tablesorter thead tr th, table.tablesorter tfoot tr th {
	background-color: #E6EEEE;
	<!-- border: 1px solid #CDCDCD; -->
	font-size: 8pt;
	<!-- padding: 4px;--> 
}
	<!--  table.tablesorter thead tr .header {
background-image: url(/broadsoft/adminPortal/images/blue/bg.gif);
	background-image: url(/Express/images/blue/bg.gif);
	background-repeat: no-repeat;
	background-position: center right;
	cursor: pointer;
} -->
table.tablesorter tbody td {
	<!-- border: 1px solid #CDCDCD; -->
	color: #3D3D3D;
	<!-- padding: 4px; -->
	vertical-align: top;
}
table.tablesorter tbody tr.odd td {
	<!-- border: 1px solid #CDCDCD; -->
}
<!-- table.tablesorter thead tr .headerSortUp {

	background-image: url(/broadsoft/adminPortal/images/blue/asc.gif); 
	background-image: url(/Express/images/blue/asc.gif);
	}-->

<!--table.tablesorter thead tr .headerSortDown {
	 background-image: url(/broadsoft/adminPortal/images/blue/desc.gif); 
	background-image: url(/Express/images/blue/desc.gif);
}-->
table.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp {
	<!-- background-color: #8DBDD8; -->
}
.getCSV {
	display: block;
	width: 115px;
	height: 25px;
	background: #4E9CAF;
	padding: 10px;
	text-align: center;
	border-radius: 5px;
	color: white;
	font-weight: bold;
}
<!-- input.deleteButton { -->
<!-- 	border: 1px solid #666666; -->
<!-- 	-webkit-box-shadow: -->
<!-- 		inset 0 0 8px rgba(0, 0, 0, 0.1), -->
<!-- 			0 0 16px rgba(0, 0, 0, 0.1); -->
<!-- 	-moz-box-shadow: -->
<!-- 		inset 0 0 8px rgba(0, 0, 0, 0.1), -->
<!-- 			0 0 16px rgba(0, 0, 0, 0.1); -->
<!-- 	box-shadow: -->
<!-- 		inset 0 0 8px rgba(0, 0, 0, 0.1), -->
<!-- 			0 0 16px rgba(0, 0, 0, 0.1); -->
<!-- 	background: #EE0000; -->
<!-- 	color: #FFFFFF; -->
<!-- } -->
.ui-autocomplete {
	font-size: 11px;
	color: #666666;
}
.autoFill {
	font-size: 11px;
	color: #666666;
}
.connectedSortable {
	font-size: 11px;
	color: #666666;
	padding-left: 25px;
	padding-right: 25px;
}
.locationField {
	font-size: 15px;
	font-weight: bold;
	float: left;
	margin-left: 1%;
}
.numberList {
	/*float: left;
	clear: both;
	width: 10%;
	margin-left: 0%;*/
}
.numberData {
	float: left;
	width: 10%;
}
a.numberList {
	color: blue;
}
.permissions {
	float: left;
	clear: both;
	width: 100%;
	height: 220px;
	background: #E0E0E0;
}
.permGroup {
	float: left;
	clear: both;
	width: 45%;
	height: 25px;
}
.permNewData {
	float: left;
	clear: both;
	width: 15%;
}
.permData {
	float: left;
	width: 15%;
}
.changeDetail {
	width: 80%;
	text-align: center;
	margin: 0 auto;
	height: 500px;
}
.viewDetail {
	/*width: 90%;*/
	/*border: 1px solid;*/
	text-align: center;
	margin: 0 auto;
	height: 450px;
	zoom: 1;
	overflow-y: scroll;
	overflow-x: auto;
}
.viewDetailRegistration {
<!-- 	width: 90%; -->
<!-- 	border: 1px solid; -->
<!-- 	text-align: center; -->
<!-- 	margin: 0 auto; -->
<!-- 	height: 450px; -->
<!-- 	zoom: 1; -->
<!-- 	overflow-y: scroll; -->
<!-- 	overflow-x: auto; -->
}
.viewRadioUserDetail {
	width: 90%;
	margin:0 auto;
	text-align:left;
}
.change {
	width: 25%;
	padding: 0px;
}
.changeRow {
	height: 25px;
	width: 100%;
	padding: 0px;
}
.hideMe {
	display: none;
	width: 100%;
}
.changeLeftB {
	float: left;
	clear: both;
	width: 24%;
	margin-left: 2%;
	font-weight: bold;
}
.changeLeftB3 {
	float: left;
	clear: both;
	width: 32%;
	margin-left: 2%;
	font-weight: bold;
}
.changeB {
	float: left;
	width: 24%;
	font-weight: bold;
}
.changeB3 {
	float: left;
	width: 32%;
	font-weight: bold;
}
.changeD {
	float: left;
	width: 24%;
	word-break: break-all;
}
.changeD3 {
	float: left;
	width: 32%;
	word-wrap: break-word;
}
.callSums {
	text-align: center;
}
.sumOn {
	background-color: #FFFFFF;
}
.cdrTable {
	height: 300px;
	overflow: auto;
}
.callDetail {
	height: 450px;
}
#header ul {
	list-style: none;
	padding: 0;
	margin: 0;
}
#header li {
	display: inline;
	border: 1px solid;
	border-bottom-width: 0;
	margin: 0 0.5em 0 0;
	font-size: 19px;
}
#header #selected {
	padding-bottom: 1px;
	background: #FFFFFF;
}
#header {
	margin-left: 30%;
	margin-top: 2%;
}
#content {
	border: 1px solid;
}
.tabsData {
	border: 1px solid;
}
.tabsHover, .tabsHover a {
	background-color: #666666;
	font-style: bold;
	color: white;
}
.listEntry {
	background-color: blue;
}
.userDataClass {
	margin-left: 3%;
	margin-right: 3%;
	height: auto;
	min-height: 500px;
	border: 1px solid;
}
.fileinput-button {
	position: relative;
	overflow: hidden;
}
.fileinput-button input {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	opacity: 0;
	-ms-filter: "alpha(opacity = 0)";
	font-size: 200px;
	direction: ltr;
	cursor: pointer;
}
td.bad {
	background-color: #aa1111 !important;
	color: white;
}
td.good {
	background-color: white;
}
.noBorder {
	border: 0px;
	font-size: 10px;
}
.bulkUserTable {
	border: 1px solid;
	border-color: #BEBEBE;
	border-collapse: collapse;
	padding: 4px;
}
.ui-tooltip {
	max-width: 200px;
	width: auto;
	padding: 3px;
}
.ui-tooltip-content {
	background-color: red;
	color: white;
	padding: 3px;
}
#macResult {
	clear: both;
	height: 400px;
	width: 60%;
	margin: 0 auto;
	margin-top: 20px;
	zoom: 1;
	overflow: auto;
}
.macTable {
	padding: 9px;
	text-align: center;
	width: 100%;
	margin: 0 auto;
}
.loading {
	display: none;
	text-align: center;
	width: 100%;
	margin-left: auto;
	margin-right: auto;
	margin-top: 10%;
}
.feature-list ul {
	margin: 0 auto !important;
	text-align: center;
}
.feature-list li {
	display: inline-block;
	text-align: center;
	<!-- padding: 30px; -->
	min-width: 200px;
}
.feature-list li a {
	text-decoration: none;
}
.feature-list li a:hover {
	background: none;
	text-decoration: none;
}
.feature-list li img {
	padding-bottom: 15px;
	width: 70px;
}
.list-div {
	text-align: center !important;
	margin: 0 auto !important;
	width: 100%;
	display: table !important;
}
.formspace {
	padding: 6px 0px !important;
}

/* Registration Form */
/* .fcorn-register .register-info {
	background: #B5B5B5;
	color: #FFF;
	padding: 8px 15px;
	font-size: 12px;
	font-weight: 600;
	margin-top: 30px;
} */

/* .fcorn-register input, .fcorn-register select {
	display: inline-block;
	width: 100%;
	padding: 7px 10px;
	font-weight: 300;
	color: #686868;
	border: 1px solid #E4E4E4;
} */
/* .fcorn-register input:focus, .fcorn-register select:focus {
	border: 1px solid #63C6EF;
	outline: none;
}
.fcorn-register select:focus {
	border: 1px solid rgba(159, 159, 159, 0.2);
} */
.fcorn-register select { /** To hide default arrow */
	text-indent: 0.05px; /** if it doesn't work change to something 0.05 etc... **/
	text-overflow: "";
	-webkit-appearance: none;
	-moz-appearance: none;
}
.fcorn-register select::-ms-expand {
	display: none;
}
.fcorn-register .dropdown-wrap, .fcorn-register .country-wrap {
	position: relative;
}
.fcorn-register .dropdown-wrap:before, .fcorn-register .country-wrap:before { /** For custom arrow in select box **/
	position: absolute;
	content: "#";
	font-family: "entypo";
	font-size: 18px;
	color: #686868;
	top: 9px;
	right: 9px;
}
.fcorn-register span.extern-type {
	float: right;
	text-align: right;
	font-size: 12px;
	display: block;
	color: #B5B5B5;
}
.fcorn-register .register-toggle { /** Register Toogle Btn **/
	position: relative;
}
.fcorn-register p.register-toggle .info {
	font-size: 13px;
	margin-left: 20px;
}
.fcorn-register p.register-toggle label.toggle-label {
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	-o-user-select: none;
	user-select: none;
	cursor: pointer;
	display: inline-block;
	background: #4CBEED;
	width: 60px;
	height: 24px;
	float: left;
	position: relative;
	font-size: 10px;
	color: #FFF;
}
.fcorn-register p.register-toggle label.toggle-label input.toggle-input {
	position: absolute;
	top: 0;
	left: 0;
	opacity: 0;
}
.fcorn-register p.register-toggle label.toggle-label:before, .fcorn-register p.register-toggle label.toggle-label:after { /** this is for labeling of on/off text **/
	position: absolute;
	top: 30%;
	line-height: 1.1;
	font-weight: 100;
}
.fcorn-register p.register-toggle label.toggle-label:before {
	content: attr(data-on);
	left: 7px;
}
.fcorn-register p.register-toggle label.toggle-label:after {
	content: attr(data-off);
	right: 7px;
}
.fcorn-register p.register-toggle label.toggle-label .toggle-handle { /** handle switch **/
	position: absolute;
	background: #FFF;
	left: 0;
	top: 0;
	border: 1px solid #4CBEED;
	display: inline-block;
	-webkit-transition: 0.2s;
	transition: 0.2s;
	box-shadow: inset 0 0 5px rgba(50, 50, 50, 0.1);
	width: 50%;
	height: inherit;
}
.fcorn-register p.register-toggle label.toggle-label input.toggle-input:checked .toggle-handle {
	left: 30px;
}
.fcorn-register p.register-submit { /* Register button */
	display: inline-block;
	margin-top: 30px;
}
.fcorn-register p.register-submit input {
	border: none;
	min-width: 200px;
	color: #FFF;
	padding: 10px 15px;
	background: #43BBEC;
	text-align: center;
	letter-spacing: 1px;
	box-shadow: 0 1px 1px #1DAAE3;
}
.fcorn-register p.register-submit input:focus {
	border: none;
	outline: none;
}
.fcorn-register p.register-submit input:hover {
	background: #39B7EB;
}
.fcorn-register p.register-submit input:active {
	box-shadow: inset 0 0 10px #1DAAE3;
	-webkit-transform: translate(0, 1px);
	-ms-transform: translate(0, 1px);
	transform: translate(0, 1px);
}

fieldset {
	border: 1px solid #C0C0C0;
	margin: 0 2px;
	padding: 0.35em 0.625em 0.75em;
}
button, input {
	line-height: normal;
}
button, select {
	text-transform: none;
}
button, html input[type="button"], input[type="reset"], input[type="submit"] {
	-webkit-appearance: button;
	cursor: pointer;
}
button[disabled], html input[disabled] {
	cursor: default;
}
input[type="checkbox"], input[type="radio"] {
	box-sizing: border-box;
	padding: 0;
}
input[type="search"] {
	-webkit-appearance: textfield;
	-moz-box-sizing: content-box;
	-webkit-box-sizing: content-box;
	box-sizing: content-box;
}
input[type="search"]::-webkit-search-cancel-button, input[type="search"]::-webkit-search-decoration {
	-webkit-appearance: none;
}
button::-moz-focus-inner, input::-moz-focus-inner {
	border: 0;
	padding: 0;
}
textarea {
	overflow: auto;
	vertical-align: top;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}
input, button, select, textarea {
	font-family: inherit;
	font-size: inherit;
	line-height: inherit;
	margin: 0;
}
a {
	color: #428BCA;
	text-decoration: none;
}
a:hover, a:focus {
	color: #2A6496;
	text-decoration: underline;
}
a:focus {
	outline: thin dotted #333;
	outline: 5px auto -webkit-focus-ring-color;
	outline-offset: -2px;
}
img {
	vertical-align: middle;
}
hr {
	margin-top: 20px;
	margin-bottom: 20px;
	border: 0;
	border-top: 1px solid #EEEEEE;
}
.sr-only {
	position: absolute;
	width: 1px;
	height: 1px;
	margin: -1px;
	padding: 0;
	overflow: hidden;
	clip: rect(0, 0, 0, 0);
	border: 0;
}
p {
	margin: 0 0 10px;
}
.lead {
	margin-bottom: 20px;
	font-size: 16px;
	font-weight: 200;
	line-height: 1.4;
}
@media (min-width: 768px) {
	.lead {
		font-size: 21px;
	}
}
small, .small {
	font-size: 85%;
}
cite {
	font-style: normal;
}
.text-left {
	text-align: left;
}
.text-right {
	text-align: right;
}
.text-center {
	text-align: center;
}
h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
	font-family: inherit;
	font-weight: 500;
	line-height: 1.1;
	color: inherit;
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small, .h1 small, .h2 small, .h3 small, .h4 small, .h5 small, .h6 small, h1 .small, h2 .small, h3 .small, h4 .small, h5 .small, h6 .small, .h1 .small, .h2 .small, .h3 .small, .h4 .small, .h5 .small, .h6 .small {
	font-weight: normal;
	line-height: 1;
	color: #999999;
}
h1, h2, h3 {
	margin-top: 20px;
	margin-bottom: 10px;
}
h1 small, h2 small, h3 small, h1 .small, h2 .small, h3 .small {
	font-size: 65%;
}
h4, h5, h6 {
	margin-top: 10px;
	margin-bottom: 10px;
}
h4 small, h5 small, h6 small, h4 .small, h5 .small, h6 .small {
	font-size: 75%;
}
h1, .h1 {
	font-size: 36px;
}
h2, .h2 {
	font-size: 30px;
}
h3, .h3 {
	font-size: 24px;
}
h4, .h4 {
	font-size: 18px;
}
h5, .h5 {
	font-size: 14px;
}
h6, .h6 {
	font-size: 12px;
}
.page-header {
	padding-bottom: 9px;
	margin: 40px 0 20px;
	border-bottom: 1px solid #EEEEEE;
}
ul, ol {
	margin-top: 0;
	margin-bottom: 10px;
}
ul ul, ol ul, ul ol, ol ol {
	margin-bottom: 0;
}
.list-unstyled {
	padding-left: 0;
	list-style: none;
}
.list-inline {
	padding-left: 0;
	list-style: none;
}
.list-inline > li {
	display: inline-block;
	padding-left: 5px;
	padding-right: 5px;
}
.list-inline > li:first-child {
	padding-left: 0;
}
dl {
	margin-bottom: 20px;
}
dt, dd {
	line-height: 1.428571429;
}
dt {
	font-weight: bold;
}
dd {
	margin-left: 0;
}
@media (min-width: 768px) {
	.dl-horizontal dt {
		float: left;
		width: 160px;
		clear: left;
		text-align: right;
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
	}
	.dl-horizontal dd {
		margin-left: 180px;
	}
	.dl-horizontal dd:before, .dl-horizontal dd:after {
		content: " ";
		/* 1 */
		display: table;
		/* 2 */
	}
	.dl-horizontal dd:after {
		clear: both;
	}
}
abbr[title], abbr[data-original-title] {
	cursor: help;
	border-bottom: 1px dotted #999999;
}
abbr.initialism {
	font-size: 90%;
	text-transform: uppercase;
}
blockquote {
	padding: 10px 20px;
	margin: 0 0 20px;
	border-left: 5px solid #EEEEEE;
}
blockquote p {
	font-size: 17.5px;
	font-weight: 300;
	line-height: 1.25;
}
blockquote p:last-child {
	margin-bottom: 0;
}
blockquote small {
	display: block;
	line-height: 1.428571429;
	color: #999999;
}
blockquote small:before {
	content: "\2014 \00A0";
}
blockquote.pull-right {
	padding-right: 15px;
	padding-left: 0;
	border-right: 5px solid #EEEEEE;
	border-left: 0;
}
blockquote.pull-right p, blockquote.pull-right small, blockquote.pull-right .small {
	text-align: right;
}
blockquote.pull-right small:before, blockquote.pull-right .small:before {
	content: "";
}
blockquote.pull-right small:after, blockquote.pull-right .small:after {
	content: "\00A0 \2014";
}
blockquote:before, blockquote:after {
	content: "";
}
address {
	margin-bottom: 20px;
	font-style: normal;
	line-height: 1.428571429;
}
fieldset {
	padding: 0;
	margin: 0;
	border: 0;
	min-width: 0;
}
legend {
	display: block;
	width: 100%;
	padding: 0;
	margin-bottom: 20px;
	font-size: 21px;
	line-height: inherit;
	color: #333333;
	border: 0;
	border-bottom: 1px solid #E5E5E5;
}
label {
	display: inline-block;
	margin-bottom: 5px;
	font-weight: bold;
}
input[type="search"] {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
}
input[type="radio"], input[type="checkbox"] {
	margin: 4px 0 0;
	margin-top: 1px \9;
	/* IE8-9 */
	line-height: normal;
}
input[type="file"] {
	display: block;
}
input[type="range"] {
	display: block;
	width: 100%;
}
select[multiple], select[size] {
	height: auto !important;
}
input[type="file"]:focus, input[type="radio"]:focus, input[type="checkbox"]:focus {
	outline: thin dotted #333;
	outline: 5px auto -webkit-focus-ring-color;
	outline-offset: -2px;
}
output {
	display: block;
	padding-top: 7px;
	font-size: 14px;
	line-height: 1.428571429;
	color: #555555;
}
.col span_12 label {
	font-weight: bold !important;
	padding: 7px;
}
.labelpadding {
	padding: 7px;
}
strong, b {
	font-weight: bold !important;
}
.clr {
	padding-top: 10px;
	margin-bottom: 10px;
}
.diaP3 {
    padding-top: 3px;
}
.diaP6 {
    padding-top: 6px;
}
.diaP12 {
    padding-top: 12px;
}
.diaP18 {
    padding-top: 18px;
}
.diaPL {
    text-align: left;
    float: left;
    clear: both;
}
.diaPN {
    text-align: left;
    float: left;
}
.diaBox12 {
   <!--  padding-top: 1px !important; -->
    <!-- padding-bottom: 1px !important; -->
    <!-- padding-left: 2px !important; -->
    <!-- padding-right: 2px !important; -->
    <!-- margin-top: -3px !important; -->
}
.fcorn-register p.register-submit input.small{
		min-width:100px;
}
.mainPageButton{
	margin:1.5em .4em .5em 0;
}
.register-submit-position{float:right;margin-top:0px;}
 
.mainPageButton a:hover{
	border-radius:2px;
	background: #A80309;
}
<--new css code sollogics -->
.fcorn-register p.register-submit input.small{
    min-width:100px;
}
.register-submit-position{
    flat:right;
    margin-top:0px;
}
.autoHeight{
	height:auto !important;
	overflow: auto !important;
}
.scroll {
  border: 0;
  border-collapse: collapse;
}

.scroll tr {
  display: flex;
}

.scroll td {
  padding: 3px;
  flex: 1 auto;
  <!-- border: 1px solid #aaa; -->
  width: 1px;
  word-wrap: break;
  word-break:break-all;
}

.scroll thead tr:after {
  content: '';
  overflow-y: scroll;
  visibility: hidden;
  height: 0;
}

.scroll thead th {
  <!--  flex: 1 auto;
  display: block;
  border: 1px solid #000; -->
}
.fixedHeight tbody{
	height:350px !important;
}
.scroll tbody {
  display: block;
  width: 100%;
  overflow-y: scroll;
  height: 450px;
}
.scroll tbody td a{word-break:break-all; width:100%}
#\%Express_Custom_Profile_Name\%, #\%Previous_Express_Custom_Profile_Name\% {
    display: none;
}
.tagTableHeight tbody{
	height:auto !important;
	max-height:220px !important;
}
@media screen and (-webkit-min-device-pixel-ratio:0) {
    .basicAnnouncement{display:none !important;}
}

.buttonSetClass{width:100%;padding:.5em 1em !important;}
.closeButton{
<!-- float:right; -->
}
.videoButton{float:left;margin-left:0 !important;}


 

 

 

.extraInputRow {
    margin: 12px 0;
}
.extraInputRowDomain {
    margin: 12px 0;
}
.cssLeftDeleteButton{
	float: left; 
	margin-left: 47px !important; 
	padding-top: 10px;
}
.newAutoAttendentButton{
	text-align: center;
	width: 100% !important;
	float: left;
}


/* User Modify */

.voiceMess{height: 109px !important;
width: 13%;
text-align: center;
padding: 30;
padding-top: 10px;
}
.usersTableColor{
	/* background-color:#FFBC3D; */
}



#allUsers thead tr th {
	border-top: none;
    border-bottom: none;
}

.tableBorder{
 border: 2px solid #000000;
    width: 90%;
    margin: 0 auto;
}

.usersTableColor td{
 /* background-color:#ffbc3d; */
}
input#subButtonDelEnt,input#deleteSimRingCriteria,input#deleteUsers, input#subButtonDel, input#delBut, input#announcementButtonDelete, input#EVDELsubButton, input#deleteDevice, input#deleteGroupCallPickupInstance, input#scaDeleteBtn, input#deleteThisSubmenuButton, input#deleteUsers_Basic, #input#deleteAnnouncement {
    background: #ac5f5d !important;
    color: white !important;
   /* border: 1px solid #333; */
}


input#deleteUsers:hover, input#subButtonDel:hover, input#delBut:hover, input#announcementButtonDelete:hover, input#EVDELsubButton:hover, input#deleteDevice:hover, input#deleteGroupCallPickupInstance:hover, input#scaDeleteBtn:hover, input#deleteThisSubmenuButton:hover,input#deleteSimRingCriteria:hover,input#deleteUsers_Basic:hover , input#deleteAnnouncement:hover {

    background: #c9302c !important;
    color: white !important;
}

/* Added @ 15 March 2018 due to the hide of disable filter overlay */
.ui-dialog {
z-index: 1000;
}
/*#users_selected_count{   
width: 98%;
margin: 5px auto;
}*/
/*.ui-datepicker-next, .ui-datepicker-prev{
	background:white;
}*/
     
  /* CDRS and Numbers */
     #allUsers2{ border: 1px solid #D8DADA; }
  table#allUsers2 tbody tr,table#allNumbers tbody tr{ border: 1px solid #D8DADA !important;}
  
  .filtersApplied{
  	color:red;
  }
#userCustomTagsTable tbody tr{
border: 1px solid #ddd;
}

.switch {
	position: relative;
	display: block;
	vertical-align: top;
	width: 66px;
	height: 25px;
	padding: 0px;
	margin:0;
	background: linear-gradient(to bottom, #eeeeee, #FFFFFF 25px);
	background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF 25px);
	border-radius: 18px;
	box-shadow: inset 0 -1px white, inset 0 1px 1px rgba(0, 0, 0, 0.05);
	cursor: pointer;
  box-sizing:content-box;
}
.switch-input {
	position: absolute;
	top: 0;
	left: 0;
	opacity: 0;
  box-sizing:content-box;
}
.switch-label {
	position: relative;
	display: block;
	height: inherit;
	font-size: 10px;
	text-transform: uppercase;
	background: #eceeef;
	border-radius: inherit;
	box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.12), inset 0 0 2px rgba(0, 0, 0, 0.15);
  box-sizing:content-box;
  width:66px;
}
.switch-label:before, .switch-label:after {
	position: absolute;
	top: 50%;
	margin-top: -.5em;
	line-height: 1;
	-webkit-transition: inherit;
	-moz-transition: inherit;
	-o-transition: inherit;
	transition: inherit;
  box-sizing:content-box;
}
.switch-label:before {
	content: attr(data-off);
	right: 11px;
	color: #aaaaaa;
	text-shadow: 0 1px rgba(255, 255, 255, 0.5);
}
.switch-label:after {
	content: attr(data-on);
	left: 11px;
	color: #FFFFFF;
	text-shadow: 0 1px rgba(0, 0, 0, 0.2);
	opacity: 0;
}

.switch-input:checked ~ .switch-label:before {
	opacity: 0;
}
.switch-input:checked ~ .switch-label:after {
	opacity: 1;
}
.switch-handle {
	position: absolute;
	top: 1px;
	left: 2px;
	width: 22px;
	height: 22px;
	background: linear-gradient(to bottom, #FFFFFF 40%, #f0f0f0);
	background-image: -webkit-linear-gradient(top, #FFFFFF 40%, #f0f0f0);
	border-radius: 100%;
	box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2);
}
.switch-handle:before {
	content: "";
	position: absolute;
	top: 50%;
	left: 50%;
	margin: -6px 0 0 -6px;
	width: 12px;
	height: 12px;
	background: linear-gradient(to bottom, #eeeeee, #FFFFFF);
	background-image: -webkit-linear-gradient(top, #eeeeee, #FFFFFF);
	border-radius: 6px;
	box-shadow: inset 0 1px rgba(0, 0, 0, 0.02);
}
.switch-input:checked ~ .switch-handle {
	left: 37px;
	box-shadow: -1px 1px 5px rgba(0, 0, 0, 0.2);
}

.resetF{
	border-radius:7px;
	padding: 5px 15px;
	
	font-weight:normal !important;
}

/*
#resetAllFilters{
	border-radius: 7px;
	min-width: 16% !important;
	padding: 5px 15px;
	margin-right:2%;
}*/

#showActiveFilterBtn, #showAllFilterBtn{
	min-width:19% !important;
}
.bulkUserNotCreated th{
	padding: 5px;
    width: 20%;
    border: 1px solid #ddd;
}
.bulkUserNotCreated td{
	padding: 5px;
    width: 20%;
    border: 1px solid #ddd;
}
Table.bulkUserTable tbody tr {background-color: inherit !important;}
Table.bulkUserTable td, Table.bulkUserNotCreated td { background-color: #fff !important; }
Table.bulkUserTable th, Table.bulkUserNotCreated th { background-color: rgba(0, 0, 0, 0.05) !important; }
.displayBlock{ display: block !important;}
.bulkUserTableTd{width:100px !important}
.bulkSCATable th {
    border: 1px solid #ddd;
    padding: 4px;
}
.bulkSCATable tr td {
    border: 1px solid #ddd;
    padding: 4px;
}
.bulkBLFTable td {
    padding: 5px;
    border: 1px solid #ddd;
}

.bulkBLFTable th {
    border: 1px solid #ddd;
    padding: 5px;
}
.marginTopOCP{
	margin-bottom:25px;
}
#ocp_1 select{
	height: auto;
}
.rowHoverEffect:hover{
background-color:red !important;
}
.mouseHoverEffect:hover{
background-color:red !important;
}
#subButtonAnn, #cancelAnnButton{
min-width:200px !important;
width:200px;
}
#addAnnouncementForm{
padding:30px;
}

.announcementLink{
	text-align:center;
}
#announcementTable tbody {
	height: auto;
	max-height: 450px;
}
#announceAddModal{
padding:25px;
height:270px !important;
overflow:hidden;
}

#announcementTable thead{
     background-color: #e6eeee;
}

#announcementTable input[type="checkbox"]{
margin:8px 0 0;
}
#announcementTable tbody tr img{
margin: 3px 0 0;
}
table#announcementTable thead tr th:nth-child(1) {
    background: none;
}

table#announcementTable thead tr {
    
}


.modalDropDownClass, .modalInputClass{
	display: inline-block;
    width: 100%;
    padding: 7px 10px;
    font-weight: 300;
    color: #686868;
    border: 1px solid #E4E4E4;
    }

.criteriaDelBtn{
    background: #ac5f5d !important;
    color: white !important;
    border: 1px solid #333;
}

.criteriaDelBtn:hover{
    background: #c9302c !important;
    color: white !important;
}

#softPhoneTable tbody {
    height: auto;
    max-height: 350px;
}
#softPhoneTable tbody td{
padding:10px !important;
}
#softPhoneTable thead th{
padding:10px !important;
}

#simRingCriteriaList tbody {
    height: auto;
    max-height: 350px;
}
#simRingCriteriaList tbody td{
padding:10px !important;
}
#simRingCriteriaList thead th{
padding:10px !important;
}

/* hide outline on Dialog close icon*/
.ui-dialog-titlebar .ui-dialog-titlebar-close {
	outline: none;
}
table#simRingCriteriaList thead tr th.header:first-child{
      	background-image: none !important;
      }
 table#simRingCriteriaList thead tr .headerSortDown, table#simRingCriteriaList thead tr .headerSortUp {
    /* background-color:#E6EEEE !important; */
}
table#simRingCriteriaList tbody tr td {
    flex: 0 auto;
    width: 47%;
    cusor:pointer;
    padding: 10px 12px !important;
    border: 1px solid #D8DADA !important;
   /* margin: 2px 0px; */
}
table#simRingCriteriaList tbody tr:hover {
    background-color: #dcfac9 !important;
    cursor: pointer;
 } 