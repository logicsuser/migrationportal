<?php
require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/devices/deviceTypeManagement.php");
$devobj = new DeviceTypeManagement();
$vdmArray = $devobj->getVDMTemplate();

/**
 *
 * @param
 *            deviceType
 */

$deviceInfo = explode("||", $_POST["deviceName"]);
$deviceType = $_SESSION['vdm_device_type'] = $deviceInfo[0];
$deviceName = $deviceInfo[1];
$operationType = $_POST["operationType"];

$algoAlerters = array();
$algoAlerters[0] = "Algo_8180";
$algoAlerters[1] = "Algo_8301";

$sp = $_SESSION['sp'];
$groupId = $_SESSION["groupId"];
$deviceInfo = getVDMDeviceInfo($db, $deviceType, $vdmArray);
$vdmType = $deviceInfo["vdmTemplate"];

function getVDMDeviceInfo($db, $deviceType, $vdmArray) {
    
    //Code added @ 16 July 2019
    $whereCndtn = "";
    if($_SESSION['cluster_support']){
        $selectedCluster = $_SESSION['selectedCluster'];
        $whereCndtn = " AND clusterName ='$selectedCluster' ";
    }
    //End code
    
    $vdmDeviceInfo = array();
    $query = "SELECT deviceType,vdmTemplate from systemDevices where deviceType = '" . $deviceType . "' $whereCndtn ";
    $qwr = $db->query($query);
    while ($r = $qwr->fetch()) {
        $vdmDeviceInfo["sysDevice"] = $r["deviceType"];
        $vdmDeviceInfo["vdmTemplate"] = $vdmArray[$r["vdmTemplate"]];
    }
    return $vdmDeviceInfo;
}

function renderDeviceDetails($deviceType, $deviceName, $sp, $groupId, $vdmType, $ociVersion, $rebuildResetDevice,$license)
{
    if ($deviceType == "Algo_8128") {
        require_once ("/var/www/html/Express/Vdm/devices/algoDevices/Algo-8128-Js.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
        require_once ("/var/www/html/Express/Vdm/devices/algoDevices/Algo-8128-function.php");
        require_once ("/var/www/html/Express/Vdm/devices/algoDevices/Algo-8128-View.php");
       // require_once ("/var/www/html/Express/Vdm/warningMsg-Js.php");
    } else if ($deviceType == "Algo_8028") {
        require_once ("/var/www/html/Express/Vdm/devices/algoDevices/Algo-8028-Js.php");
        require_once ("/var/www/html/Express/Vdm/devices/algoDevices/Algo-8028-View.php");
//         require_once ("/var/www/html/Express/Vdm/warningMsg-Js.php");
    } else if ($vdmType == "algoAlerter") {
        require_once ("/var/www/html/Express/Vdm/devices/algoDevices/Algo-Alerter-Js.php");
        require_once ("/var/www/html/Express/Vdm/devices/algoDevices/Algo-Alerter-View.php");
        //require_once ("/var/www/html/Express/Vdm/warningMsg-Js.php");
    } else if ($vdmType == "audioCodesMP") {
        require_once ("/var/www/html/Express/Vdm/devices/audioCodes/audioCodes-Js.php");
        require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
        require_once ("/var/www/html/Express/Vdm/devices/audioCodes/audioCodesFunctions.php");
        require_once ("/var/www/html/Express/Vdm/devices/audioCodes/index.php");
       // require_once ("/var/www/html/Express/Vdm/warningMsg-Js.php");
    } else if ($vdmType == "polycomVVX310" || $vdmType == "polycomVVX410" || $vdmType == "polycomVVX500" || $vdmType == "polycomVVX600") {
    	require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
    	require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
    	
    	require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
    	$deviceObj = new DeviceOperations();
    	$customProfiles = $deviceObj->getCustomProfiles($deviceType);
    	require_once ("/var/www/html/Express/Vdm/devices/polycom/polycom-function.php");
        require_once ("/var/www/html/Express/Vdm/devices/polycom/polycom-Js.php");
        require_once ("/var/www/html/Express/Vdm/devices/polycom/polycom-View.php");
        //require_once ("/var/www/html/Express/Vdm/warningMsg-Js.php");
    }
}

function validateDeviceDetails($deviceType) {
    if ($deviceType == "Algo_8180") {
        require_once 'Algo-8180-Validate.php';
    } else if ($deviceType == "Algo_8028") {}
}

/**
 */
function processRequest($deviceType, $deviceName, $operationType, $sp, $groupId, $vdmType, $ociVersion, $rebuildResetDevice,$license) {
    if ($operationType == "GET") {
        renderDeviceDetails($deviceType, $deviceName, $sp, $groupId, $vdmType, $ociVersion, $rebuildResetDevice, $license);
    }
    
    if ($operationType == "Validate") {
        validateDeviceDetails($deviceType);
    }
}

processRequest($deviceType, $deviceName, $operationType, $sp, $groupId, $vdmType, $ociVersion, $rebuildResetDevice,$license);

