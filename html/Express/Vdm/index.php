 <?php
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once ("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
require_once ("/var/www/lib/broadsoft/adminPortal/getDevices.php");
require_once ("/var/www/html/Express/Vdm/warningMsg-Js.php");

/*function validAnalog($db, $useCustomDeviceList, $nonObsoleteDevices){
    // Build devices lists: digitalVoIPDevices and analogGatewayDevices
    $deviceTypesDigitalVoIP = array ();
    $deviceTypesAnalogGateways = array ();
    $sipGatewayLookup = new DBLookup ( $db, "devices", "deviceName", "sipGateway" );
    
    // Get Custom Devices List from DB.
    // If the list exists, only devices from the list will be available in drop-down for assignment.
    $customDevices = array ();
    if ($useCustomDeviceList == "true") {
        $query = "select deviceName from customDeviceList";
        $result = $db->query ( $query );
        
        while ( $row = $result->fetch () ) {
            $customDevices [] = $row ["deviceName"];
        }
    }
    
    //echo "<pre>"; print_r($customDevices);
    foreach ( $nonObsoleteDevices as $key => $value ) {
        // shortcut custom device list for Audio Codes until they are resolved
        $deviceIsAudioCodes = substr ( $value, 0, strlen ( "AudioCodes-" ) ) == "AudioCodes-";
        
        if ($useCustomDeviceList == "true" && ! in_array ( $value, $customDevices )) {
            if (! $deviceIsAudioCodes) {
                continue;
            }
        }
        if ($sipGatewayLookup->get ( $value ) == "true" || $deviceIsAudioCodes) {
            $deviceTypesAnalogGateways [] = $value;
        } else {
            $deviceTypesDigitalVoIP [] = $value;
        }
    }
    return $deviceTypesAnalogGateways;
    
}

$validAnalog = validAnalog($db, $useCustomDeviceList, $nonObsoleteDevices);*/
//echo "<pre>"; print_r($validAnalog);
?>
<div id="vdmMainPageReplace">
<link rel="stylesheet" type="text/css" href="/Express/Vdm/css/vdm.css">
<!--<link rel="stylesheet" type="text/css" href="/Express/css/colorSchema.php" media="screen" /> -->
	<style>
.leftSpace {
	margin-left: 20px;
}
td.thPolyD .dropdown-wrap {
    width: 100% !important;
}
td.thPolyD .dropdown-wrap:before {
    top: 0px !important;
}
/* .inputBackground{
	background-color:#eeeeee !important;
	color:#333333 !important;
	font-size: 15px;
    font-weight: 600 !important;
} */

</style>
<script>

</script>
<script>
var navVdmType = "";
var navVdmDeviceName = "";
var navVdmDeviceType = "";
var deviceValChk = [];
var deviceTypeChanged = "false";
<?php
// coming from modify user
$isUserNavigate = false;
if (isset ( $_POST ["deviceName"] )) {
	$isUserNavigate = true;
	$navVdmDeviceType = $_POST ["vdmDeviceType"];
	$navVdmDeviceName = $_POST ["deviceName"];
	$navVdmType = $_POST ["vdmType"];
	 //print_r($_POST);
	
	?>
navVdmType = "<?php echo $navVdmType?>";
navVdmDeviceName = "<?php echo $navVdmDeviceName?>";
navVdmDeviceType = "<?php echo $navVdmDeviceType?>";


<?php }?> 
$(function()
{
	$("#vdmType").change(function() {
		
// 			loadDeviceName();
	});

	
	
	$("#loader").hide();
	if(navVdmType) {
		$("#vdmType").val(navVdmType);
	}	
	$("#subButtonDevice").click(function(){

		//set temp values for discard message.
		$("#isGetClick").val("true");
		$("#tempDeviceType").val($("#vdmType").val());
		$("#tempDeviceName").val($("#deviceName").val());
		
        var deviceResultSearch = deviceValChk;
        var deviceVal = $("#deviceName").val();
        var deviceNameLatest = [];
        var len = deviceResultSearch.length;
        for(var i=0; i<len; i++){
        	var navVdmDeviceName = deviceResultSearch[i].deviceName;
        	if(navVdmDeviceName == $.trim(deviceVal) ){
        		deviceNameLatest = deviceResultSearch[i].deviceInfo;
        		break;
        	}
        }
   		navVdmType = "";
		navVdmDeviceName = "";
		navVdmDeviceType = "";

		if($("#vdmType").val() == ""){
			return false;
		} else if(deviceVal == "") { return false;}

		$('#indexFormSelect .loading').show();
		if($('#vdmLightForm').length > 0){
			$('#vdmLightForm').hide();
		}
		$("#indexForm").hide();	
		$("#deviceDetails").html("");
		var dataToSend = $("form#vdmLightSearchForm").serializeArray();
		$("#selectedDeviceType").val(deviceNameLatest.split('||')[0]);
		dataToSend.push({name: 'deviceName', value: deviceNameLatest});
		deviceValue = $.trim(deviceVal);
		if(deviceValue !=''){
    	 	$.ajax({
    				type: "post",
    				url:"Vdm/VDMController.php",
    				data : dataToSend,
    				success: function(result)
    				{
    					if(foundServerConErrorOnProcess(result, "")) {
	    					return false;
	                  	}
    					$("#indexForm").show();
    					$("#deviceDetails").html(result);
    					if($('#vdmLightForm').length > 0){
    						$('#vdmLightForm').show();
    					}
    					$('#indexFormSelect .loading').hide();
    				}
    			});
		}
		else{ $('#indexFormSelect .loading').hide(); return false;}
	});

	
// 	$(".mainDeviceName").change(function(){
// 		$("#deviceDetails").html("");
// 		$("#deviceName").html("");
// 		//$("#deviceName").val();
		
// 		//mainDeviceName
// 		$("#deviceName").empty();
// 		$("#indexForm").hide();
// 	});
});


$('#deviceName').on('keyup keypress', function(e) {

	  var keyCode = e.keyCode || e.which;
// 	  alert(keyCode);
	  if (keyCode === 13) {


		  
		e.preventDefault();
		//return false; 
		 var deviceVal = $.trim($("#deviceName").val());
		 if(deviceVal !=""){
		  $("#subButtonDevice").trigger("click");
		 }else{
			 $('#indexFormSelect .loading').hide();
			 return false;
		 }
		 
	  }
	  
	});


	function loadDeviceName()
	{
		
		$("#deviceDetails").html("");
		$("#indexForm").hide();
		$("#deviceName").val("");
		
		$(".ui-autocomplete").html("");
		$("#subButtonDevice").prop("disabled", "disabled");
		var html ="";
		var bck = jQuery.Event("keydown", { keyCode: 20 });
		$("#deviceName").trigger( bck );

		if($("#vdmType").val() != "")
		{
			$("#deviceNameDiv").hide();
			$("#loader").show();
			var autoComplete = new Array();
			var deviceType = $("#vdmType").val();
			
			var deviceName="";
			$.ajax({
					type: "POST",
					url: "Vdm/getDeviceName.php",
					dataType: 'json',
					data: {deviceType: deviceType},
					success: function(result) {
						deviceValChk = result;
						var len = result.length;
 						for(var i=0; i<len; i++){
			            var deviceName = result[i].deviceName;
			                html += result[i].deviceInfo;
				                autoComplete[i] = " " + deviceName;
				         }
						 $("#deviceName").html(html);
						   $("#deviceName").autocomplete({
				                source: autoComplete,
				                appendTo: "#hidden-stuffDevice"
				            });
							
						if(result == ""){
							$("#subButtonDevice").prop("disabled", "disabled");
						}else{
							$("#subButtonDevice").prop("disabled", false);
						}
						$("#deviceNameDiv").show();
						$("#loader").hide();
						//var autocomplete = $("#deviceName").val(result);
						
						if(navVdmType) {
						$("#deviceName").val(navVdmDeviceName);
						var deviceInfo = navVdmDeviceType + "||" + navVdmDeviceName;
						 	//$("#deviceName").val(navVdmDeviceName);
							var dataToSend = $("form#vdmLightSearchForm").serializeArray();
							dataToSend.push({name: 'deviceName', value: deviceInfo});
							$.ajax({
									type: "post",
									url:"Vdm/VDMController.php",
									data : dataToSend,
									success: function(result)
									{
										$("#deviceDetails").html(result);
										
										$("#tempDeviceType").val($("#vdmType").val());
										$("#tempDeviceName").val($("#deviceName").val());
									}
								});
						}
					}				
				});
		}else{
			$("#deviceName").html(html);
			   $("#deviceName").autocomplete({
	                source: autoComplete,
	                appendTo: "#hidden-stuffDevice"
	            });
			}
		
	}
	if(navVdmType) {
		loadDeviceName();
	}

	function ValidateIPaddress(ipaddressControl, submitButtonId) {  
		var ipAddressId = ipaddressControl.id;
		
		$("#"+submitButtonId).prop("disabled", false);
		document.getElementById(submitButtonId).style.setProperty("background-color","#72ac5d", "important");
		document.getElementById(ipAddressId).style.setProperty('border-color', '#002c60 ', 'important');

		if(ipaddressControl.value){
			if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddressControl.value)) {  
				document.getElementById(ipAddressId).style.setProperty('border-color', '#002c60 ', 'important');
				$("#"+submitButtonId).prop("disabled", false);
		 		return (true);
		 	}else{
			 	document.getElementById(ipAddressId).style.setProperty('border-color', '#ac5f5d', 'important');
			 	$("#"+submitButtonId).prop("disabled", "disabled");
			 	document.getElementById(submitButtonId).style.setProperty("background-color","#002c60", "important");
				return (false);
		 	}
		}
	}

	function refreshOnSearchDevice() {
		$("#deviceDetails").html("");
		$("#indexForm").hide();
		var bck = jQuery.Event("keydown", { keyCode: 20 });
		$("#deviceName").trigger( bck );
	}

	if($("#vdmType").val() == "all") {
		$("#vdmType").trigger("change");
	}
	
	//e.stopPropagation();
</script>

<h2 class="addUserText">Device Management</h2>
<div class="vdmForm vdmFormMainDiv" id="indexFormSelect">
	<form name="vdmLightSearchForm" id="vdmLightSearchForm" method="POST"
		autocomplete="off">
		<input type="hidden" name="operationType" id="operationType"
			value="GET"> <input type="hidden" name="f1" id="f1" value="GET"> <input
			type="hidden" name="f2" id="f2" value="GET">
	<div class="row" style="">
			<div class="col-md-6">
				<div class="form-group">
				<label class="labelText labelAboveInput" for="">Type:</label>
					<div class="dropdown-wrap">
						<select name="vdmType" id="vdmType" class="vdmDeviceChange inputBackground">
							
							<option value="all">All</option>
							<option value="audioCodesMP">Audio Codes</option>
							<option value="algoAlerter">Algo Alerter</option>
							<option value="algoDoorPhone">Algo Door Phone</option>
							<option value="algoStrobe">Algo Strobe</option>
							<option value="polycomVVX310">Polycom VVX310</option>
							<option value="polycomVVX410">Polycom VVX410</option>
							<option value="polycomVVX500">Polycom VVX500</option>
							<option value="polycomVVX600">Polycom VVX600</option>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-6" id="deviceNameDiv">
				<div class="form-group">
					<label class="labelText labelAboveInput" for="">Search Device:</label>
					<input type="text"  name="deviceName" id="deviceName" class="autoFill mainDeviceName inputBackground magnify">
					<div id="hidden-stuffDevice" style="height: 0px;"></div>

				</div>
			</div>
			<div class="col-md-6" id="loader" style="margin-top: 28px;">
				<div class="form-group">
					<img src="/Express/images/ajax-loader.gif">					
				</div>
			</div>
			
	</div>
		
		<div class="col-md-12 alignBtn">
			<input type="button" id="subButtonDevice" class="go" value="Get">
		</div>
	</form>
	<div class="loading" id="loading2">
		<img src="/Express/images/ajax-loader.gif">
	</div>
	
	
	<input type="hidden" id="isGetClick" value="false">
	<input type="hidden" id="tempDeviceType" value="false">
	<input type="hidden" id="tempDeviceName" value="false">
<!-- 	for devicetype in case of all -->
	<input type="hidden" id="selectedDeviceType" value="">

</div>

<div id="deviceDetails"></div>
<div id="dialogForChangesFormData" class="dialogClass"></div>
</div>