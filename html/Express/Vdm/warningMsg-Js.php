<script>
$(function()
{
	
	// Warning msg start
// 	var deviceTypeChanged = "false";
	function getUrl(vdmTypeHidden) {
		var url = "";
		 	
		if(vdmTypeHidden.indexOf("Algo_8028") != -1){
			 url = "Vdm/devices/algoDevices/Algo-8028-Validate.php";
		} else if(vdmTypeHidden.indexOf("Algo_8128") != -1){
			 url = "Vdm/devices/algoDevices/Algo-8128-Validate.php";
		} else if(vdmTypeHidden.indexOf("algoAlerter") != -1) {
			 url = "Vdm/devices/algoDevices/Algo-Alerter-Validate.php";
		} else if(   vdmTypeHidden.indexOf("audioCodesMP") != -1) {
			 url = "Vdm/devices/audioCodes/audioCodes-Validate.php";
		} else if(vdmTypeHidden.indexOf("polycomVVX310") != -1 || vdmTypeHidden.indexOf("polycomVVX410") != -1 || vdmTypeHidden.indexOf("polycomVVX500") != -1 || vdmTypeHidden.indexOf("polycomVVX600") != -1) {
			 url = "Vdm/devices/polycom/polycom-Validate.php";
		}
		return url;
	}

	function getFormId(vdmTypeHidden) {
		var formId = "";

		if(vdmTypeHidden.indexOf("Algo_8028") != -1){
			 formId = $("form#vdmLightFormDoorPhone");
		} else if(vdmTypeHidden.indexOf("Algo_8128") != -1){
			 formId = $("form#vdmLightForm");
		} else if(vdmTypeHidden.indexOf("algoAlerter") != -1) {
			 formId = $("form#vdmLightFormAlerter");
		} else if(vdmTypeHidden.indexOf("audioCodesMP") != -1) {
			 formId = $("form#vdmLightForm");
		} else if(vdmTypeHidden.indexOf("polycomVVX310") != -1 || vdmTypeHidden.indexOf("polycomVVX410") != -1 || vdmTypeHidden.indexOf("polycomVVX500") != -1 || vdmTypeHidden.indexOf("polycomVVX600") != -1) {
			 formId = $("form#vdmLightPlycomPhoneForm");
		}
		
		return formId;
	}
	
		
		$("#vdmType").change(function() {
			var url = "";
			var formId = "";
			var vdmTypeHiddenLenght = "";
			var vdmTypeHidden = "";
			
			if($("#indexForm").is(":visible"))
			{
    			if($('#vdmTypeHidden').length) {
    				vdmTypeHiddenLenght = $('#vdmTypeHidden').length;
    				vdmTypeHidden = $("#vdmTypeHidden").val();
    			} else if($("#selectedDeviceType").length) {
    				vdmTypeHiddenLenght = $("#selectedDeviceType").length;
    				vdmTypeHidden = $("#selectedDeviceType").val();
    			}
    			
    			if(vdmTypeHiddenLenght) {
        			url = getUrl(vdmTypeHidden);
        			formId = getFormId(vdmTypeHidden);
    				console.log(formId);
        			navVdmType = "";
        			var dataToSend = formId.serializeArray();
        	 		$.ajax({
        	 			type: "POST",
        	 			url: url,
        	 			data: dataToSend,
        	 			success: function(result) {
        	 				var noChangesExist = result.search("No Changes");
        	 				if(noChangesExist == -1 && $("#indexForm").is(":visible")) {
        	 					deviceTypeChanged = "true";
        	 					$("#dialogForChangesFormData").dialog("open");
        		 			} else {
        		 				$("#isGetClick").val("false");
        						loadDeviceName();
        			 		}
        	 			}
        	 		});
    			}
			} else {
				loadDeviceName();
			}
		});


		$("#deviceName").autocomplete({
		    select: function (event , ui) {
		    	var url = "";
				var formId = "";
				deviceTypeChanged = "false";
				if($("#indexForm").is(":visible")) {
    				if($('#vdmTypeHidden').length){
            				var vdmTypeHidden = $("#vdmTypeHidden").val();
            				url = getUrl(vdmTypeHidden);
            				formId = getFormId(vdmTypeHidden);
            		    	var dataToSend = formId.serializeArray();
            		    	if(ui.item.label.trim() != $("#tempDeviceName").val()) {
            		 		$.ajax({
            		 			type: "POST",
            		 			url: url,
            		 			data: dataToSend,
            		 			success: function(result) {
            		 				var noChangesExist = result.search("No Changes");
            		 				if(noChangesExist == -1 && $("#indexForm").is(":visible")){
            		 					$("#dialogForChangesFormData").dialog("open");
            		 				} else {
            							refreshOnSearchDevice();
            			 			}
            		 			}
            		 		});
            		    }
    		    	}
    		    }
		    },
		});


	$("#dialogForChangesFormData").dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		buttons: {
			"Discard Changes and Continue": function() {
				if(deviceTypeChanged == "true") {
					$("#isGetClick").val("false");
					loadDeviceName();
				} else {
					$("#deviceDetails").html("");
					$("#indexForm").hide();
					var bck = jQuery.Event("keydown", { keyCode: 20 });
					$("#deviceName").trigger( bck );
				}
				$(this).dialog("close");
			},
			"Cancel": function() {
				$(this).dialog("close");
				$("#loading2").hide();
				$("#vdmLightForm").show();
				var tempDeviceType = $("#tempDeviceType").val();
				var tempDeviceName = $("#tempDeviceName").val();
				$('[name=vdmType]').val(tempDeviceType);
				$("#deviceName").val(tempDeviceName);				
			},
		},
		open: function() {
				$("html, body").animate({ scrollTop: 0 }, 0); //
                                setDialogDayNightMode($(this));
				$("#dialogForChangesFormData").dialog("option", "title", "Warning Message");
				$("#dialogForChangesFormData .ui-dialog-buttonpane button:contains('Cancel')").button().show();
                                //$("#dialogForChangesFormData .ui-dialog-buttonpane button:contains('Discard Changes and Continue')").button().addClass("subButton");
                                $(".ui-dialog-buttonpane button:contains('Discard Changes and Continue')").button().show().addClass("subButton");;
				$("#dialogForChangesFormData").html("<b>Attempted search for another device will discard changes made on this page. </b><br>");
				$("#dialogForChangesFormData").append("<b>Please click on 'Discard Changes and Continue' to proceed with a search without saving changes. Press 'Cancel' to stay on the page</b>");
        }
	});
	// Warning msg end
});
</script>