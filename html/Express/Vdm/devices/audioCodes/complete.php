<?php 
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
include("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");

$cLUObj = new ChangeLogUtility($_POST['deviceName'], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);

$deviceArray = array();
$modifyDevices['devices'] = array();
$modifyTags['tags'] = array();
$changeString = "";

if(!array_key_exists("rebuildPhoneFiles", $_POST)){
	$_POST['rebuildPhoneFiles'] = "false";
}

if(!array_key_exists("resetPhone", $_POST)){
	$_POST['resetPhone'] = "false";
}

foreach($_POST['customTags'] as $key=>$val){
	if(!array_key_exists("'%ACCT.CUTOFF_TIMERS%'", $_POST['customTags'])){
		$_POST['customTags']['%ACCT.CUTOFF_TIMERS%'] = "0";
	}
}
//tage array name

$deviceTagsArray = array(
		//"%ACCT.DHCP.USE%" => "IP Address",
		//"%ACCT.LOCATION%" => "Location",
		//"%ACCT.SN%" => "Serial Number",
		//"%ACCT.SHIP_DATE%" => "Ship Date",
		"%ACCT.CUTOFF_TIMERS%" => "Cut Off Times",
		"%ACCT.SILENCE_DETECTION_PERIOD%" => "Silence Detection Period",
        "deviceName" => "Device Name",
        "site" => "Site",
        "macAddress" => "Mac Address",
        "sasLines" => "SAS Lines",
        "serialNumber" => "Serial Number",
        "physicalLocation" => "Physical Location",
        "description" => "Ship Date",
        "netAddress" => "IP Address",
		
    //�	Automatic Dialing
    "%ACCT.AUTODIAL_STATUS1%" => "Line Feature 1",
    "%ACCT.AUTODIAL_STATUS2%" => "Line Feature 2",
    "%ACCT.AUTODIAL_STATUS3%" => "Line Feature 3",
    "%ACCT.AUTODIAL_STATUS4%" => "Line Feature 4",
    "%ACCT.AUTODIAL_STATUS5%" => "Line Feature 5",
    "%ACCT.AUTODIAL_STATUS6%" => "Line Feature 6",
    "%ACCT.AUTODIAL_STATUS7%" => "Line Feature 7",
    "%ACCT.AUTODIAL_STATUS8%" => "Line Feature 8",
    "%ACCT.AUTODIAL_STATUS9%" => "Line Feature 9",
    "%ACCT.AUTODIAL_STATUS10%" => "Line Feature 10",
    "%ACCT.AUTODIAL_STATUS11%" => "Line Feature 11",
    "%ACCT.AUTODIAL_STATUS12%" => "Line Feature 12",
    "%ACCT.AUTODIAL_STATUS13%" => "Line Feature 13",
    "%ACCT.AUTODIAL_STATUS14%" => "Line Feature 14",
    "%ACCT.AUTODIAL_STATUS15%" => "Line Feature 15",
    "%ACCT.AUTODIAL_STATUS16%" => "Line Feature 16",
    "%ACCT.AUTODIAL_STATUS17%" => "Line Feature 17",
    "%ACCT.AUTODIAL_STATUS18%" => "Line Feature 18",
    "%ACCT.AUTODIAL_STATUS19%" => "Line Feature 19",
    "%ACCT.AUTODIAL_STATUS20%" => "Line Feature 20",
    "%ACCT.AUTODIAL_STATUS21%" => "Line Feature 21",
    "%ACCT.AUTODIAL_STATUS22%" => "Line Feature 22",
    "%ACCT.AUTODIAL_STATUS23%" => "Line Feature 23",
    "%ACCT.AUTODIAL_STATUS24%" => "Line Feature 24",
    
    
    //Feature Option
    "%ACCT.AUTODIAL_NUMBER1%" => "Feature Option 1",
    "%ACCT.AUTODIAL_NUMBER2%" => "Feature Option 2",
    "%ACCT.AUTODIAL_NUMBER3%" => "Feature Option 3",
    "%ACCT.AUTODIAL_NUMBER4%" => "Feature Option 4",
    "%ACCT.AUTODIAL_NUMBER5%" => "Feature Option 5",
    "%ACCT.AUTODIAL_NUMBER6%" => "Feature Option 6",
    "%ACCT.AUTODIAL_NUMBER7%" => "Feature Option 7",
    "%ACCT.AUTODIAL_NUMBER8%" => "Feature Option 8",
    "%ACCT.AUTODIAL_NUMBER9%" => "Feature Option 9",
    "%ACCT.AUTODIAL_NUMBER10%" => "Feature Option 10",
    "%ACCT.AUTODIAL_NUMBER11%" => "Feature Option 11",
    "%ACCT.AUTODIAL_NUMBER12%" => "Feature Option 12",
    "%ACCT.AUTODIAL_NUMBER13%" => "Feature Option 13",
    "%ACCT.AUTODIAL_NUMBER14%" => "Feature Option 14",
    "%ACCT.AUTODIAL_NUMBER15%" => "Feature Option 15",
    "%ACCT.AUTODIAL_NUMBER16%" => "Feature Option 16",
    "%ACCT.AUTODIAL_NUMBER17%" => "Feature Option 17",
    "%ACCT.AUTODIAL_NUMBER18%" => "Feature Option 18",
    "%ACCT.AUTODIAL_NUMBER19%" => "Feature Option 19",
    "%ACCT.AUTODIAL_NUMBER20%" => "Feature Option 20",
    "%ACCT.AUTODIAL_NUMBER21%" => "Feature Option 21",
    "%ACCT.AUTODIAL_NUMBER22%" => "Feature Option 22",
    "%ACCT.AUTODIAL_NUMBER23%" => "Feature Option 23",
    "%ACCT.AUTODIAL_NUMBER24%" => "Feature Option 24",
    
    //SAS Port
    "%ACCT.SAS_1%" => "SAS_1",
    "%ACCT.SAS_2%" => "SAS_2",
    "%ACCT.SAS_3%" => "SAS_3",
    "%ACCT.SAS_4%" => "SAS_4",
);




//custom tags set checkbox array
foreach($_POST['customTags'] as $key=>$val){
	if(!array_key_exists("'%ALGOMCASTMODE%'", $_POST['customTags'])){
		$_POST['customTags']['%ALGOMCASTMODE%'] = "0";
	}
	if(!array_key_exists("'%ALGO.DHCP.USE%'", $_POST['customTags'])){
		$_POST['customTags']['%ALGO.DHCP.USE%'] = "0";
	}
}

//filter post and tags array
foreach($_POST as $key=>$val){
	if($key != "customTags"){
		$deviceArray[$key] = $_POST[$key];
	}else{
		$tagsArray[$key] = $_POST[$key];
	}
}
$isModifiedAudioCodes = false;
$deviceArray['spId'] = $sp = $_SESSION['sp']; 
$deviceArray['groupId'] = $groupId = $_SESSION['groupId']; 

$vdm = new VdmOperations();

$modifyDevices['devices']= $vdm->modifyUserDeviceDetail($deviceArray);
server_fail_over_debuggin_testing(); /* for fail Over testing. */

if($_SESSION['customTagsUpdated']){
	$i = 0;
	
	foreach($tagsArray['customTags'] as $key1=>$val1){
		//remove single quote from tagname
		$tagName = str_replace("'", "", $key1);
		
		//loop of session to filter which tags has been changed
		foreach($_SESSION['device']['customTags'] as $key2=>$val2){
			if($tagName == $key2){
				if($val1 <> $val2[0]){
					$tagUpdateArray['tagName'] = $tagName;
					$tagUpdateArray['tagValue'] = $val1;
					$tagUpdateArray['spId'] = $_SESSION['sp'];
					$tagUpdateArray['groupId'] = $_SESSION['groupId']; 
					$tagUpdateArray['deviceName'] = $_POST['deviceName']; 
					$modifyTags['tags'][$i][$tagName] = $vdm->modfiyCustomTags($tagUpdateArray);
					$i++;
					$isModifiedAudioCodes = true;
					
					$cLUObj->createChangesArray($module = $deviceTagsArray[$tagName], $oldValue = $val2[0] , $newValue = $val1);					
// 					echo"hello"; print_r($cLUObj->changesArr); exit;
				}
			}
		}
	}
}

$changeString .= rebuildResetDevice($_POST, $sp, $groupId);

if ($_POST ['rebuildPhoneFiles'] == "true" && $_POST ['resetPhone'] == "true") {
    $cLUObj->createChangesArray($module = "Rebuild and Reset", $oldValue = "" , $newValue = "Yes");
} else if ($_POST ['rebuildPhoneFiles'] == "true" && $_POST ['resetPhone'] == "false") {
    $cLUObj->createChangesArray($module = "Rebuild", $oldValue = "" , $newValue = "Yes");
} else if ($_POST ['rebuildPhoneFiles'] == "false" && $_POST ['resetPhone'] == "true") {
    $cLUObj->createChangesArray($module = "Reset", $oldValue = "" , $newValue = "Yes");
}

if( !empty($modifyDevices["devices"]["Error"])){
    //$changeString .= '<tr><td>' .$modifyDevices["devices"]["Error"]. '</td></tr>';
    $changeString .= '<tr><td class="errorTableRows" style="background-color: #ac5f5d">Device Not Updated</td><td class="errorTableRows">'.$modifyDevices["devices"]["Error"].' </td></tr>';
}else{
    foreach($deviceArray as $key=>$val){
        foreach($_SESSION['device']['details'] as $key2=>$val2){
            if($key == $key2){
                if($val <> $val2){

                    $changeString .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">'.$deviceTagsArray[$key].'</td><td class="errorTableRows">Successfully Updated </td></tr>';
                    
                    $cLUObj->createChangesArray($module = $key, $oldValue = $val2 , $newValue = $val);

                }
            }
        }
    }
}

//echo "<pre>"; print_r($_SESSION['device']['customTags']);print_r($tagsArray['customTags']);print_r($modifyTag['tags']); die;
foreach($modifyTags['tags'] as $key=>$val){
	foreach($val as $key1=>$val1){
		foreach($_SESSION['device']['customTags'] as $key2=>$val2){
			if($key1 == $key2){
				if($val1 <> $val2[0]){
					if(!empty($val1['Error'])){
						//$modifyDevices['tags']['Error'][$key1] = $val1['Error'];
						$changeString .= '<tr><td class="errorTableRows" style="background-color: #ac5f5d">'.$deviceTagsArray[$key1].'</td><td class="errorTableRows">'.$val1['Error'].' </td></tr>';
					}else{
						//$modifyDevices['tags']['Success'][$key1] = $val1['Success'];

						$changeString .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">'.$deviceTagsArray[$key1].'</td><td class="errorTableRows">Successfully Updated </td></tr>';
					   
						$cLUObj->createChangesArray($module = $deviceTagsArray[$key1], $oldValue = $val2[0] , $newValue = $val1['Success']);

					}
				}
			}
		}
	}
}

/* Logger */
$module = "Device Management Modification";
$cLUObj->changeLogModifyUtility($module, $_POST['deviceName'], $tableName = "deviceMgmtModChanges");

echo $changeString;

?>