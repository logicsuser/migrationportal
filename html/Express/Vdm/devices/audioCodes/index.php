<?php 
require_once("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
?>
<link rel="stylesheet" type="text/css"
	href="/Express/Vdm/css/vdm.css">
<style>
.leftSpace {margin-left: 20px;}
#sasLines select option {background: white;}
#serialNumber{border: 1px solid #002c60 !important;}
</style>


<div class="selectContainer" style="padding-top: 0% !important;">
	
		<form name="vdmLightForm" id="vdmLightForm" method="POST">
                    <div class="vdmForm" id="indexForm">
                    <div id="vdmLightDisplayContent">
			<div class="" style="">
				<h2 class="addUserText"><?php echo $deviceType; ?></h2>

				<input type="hidden" name="FXO1" id="FXO1" value="FXO1"> <input
					type="hidden" name="FXO2" id="FXO2" value="FXO2">

				<div class="row">
					<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="">Device Name :</label>
						<input style="font-size: 15px;" type="text" name="deviceName" class="inputColor"
							id="deviceName" size="50"
							value="<?php if(isset($deviceDetails['deviceName'])){echo $deviceDetails['deviceName'];} ?>"
							readonly>
					</div>
					</div>

					<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="">IP Address:</label> <br>
						<input style="font-size: 15px;" type="text"
							name="netAddress" id="netAddress" size="50"
							value="<?php if(isset($deviceDetails['netAddress'])){echo $deviceDetails['netAddress'];} ?>"
							>
					</div>
					</div>

				</div>
			</div>

			<div class="row" style="">
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="">Site :</label>
						<input style="font-size: 15px;" type="text" name="site" id="site" class="inputColor"
							size="50" value="<?php echo $_SESSION["groupId"]; ?>" readonly>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="">Mac Address:</label> <br>
						<input style="font-size: 15px;" class="macAddress" type="text" name="macAddress"
							id="macAddress" size="12" maxlength="12"
							value="<?php if(isset($deviceDetails['macAddress'])){echo $deviceDetails['macAddress'];}?>">
					</div>
				</div>

			</div>
		

			<div class="row" style="">
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="">Location :</label>
						<input style="font-size: 15px;" type="text"
							name="physicalLocation" id="physicalLocation" size="50"
							value="<?php if(isset($deviceDetails['physicalLocation'])){echo $deviceDetails['physicalLocation'];} ?>">
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="">Serial Number :</label> <br>
						<input type="text" name="serialNumber" id="serialNumber" class="serialNumber fontAudio" size="50" autocomplete="off" value="<?php if(isset($deviceDetails['serialNumber'])){echo $deviceDetails['serialNumber'];}?>">
					</div>
				</div>

			</div>

			<div class="row" style="">
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for=""># SAS Lines:</label> <br>
						<div class="dropdown-wrap">
						<?php
							$disableSAS = $sasPorts == 0 ? "disabled" : "";
							echo '<select name="sasLines" class="inputBackground" id="sasLines"' . $disableSAS.'>';
							for ($totalSasPort = 0; $totalSasPort <= $numberOfSASPorts; $totalSasPort++) {
							    $sasSelected = $configuredSASTags == $totalSasPort ? "selected" : "";
							    echo '<option value=' . $totalSasPort .' '. $sasSelected .'>' . $totalSasPort . '</option>';
                                }
                              echo '</select>';
                            ?>
						
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText" for="%ACCT.SHIP_DATE%">Ship Date :</label> <br>
						<input style="font-size: 15px;" type="text"
							name="description" id="description" size="50"
							value="<?php if(isset($deviceDetails['description'])){echo $deviceDetails['description'];} ?>">
					</div>
				</div>
			</div>
			
                        
                        
                </div>
                </div>   
                        
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
				<div class="viewDetailNew">
					<table id="allUsers" class="table tableLineType audioCodesTableClass scroll"
						style="width: 100%; margin: 0;" border="0">
						<thead>
							<tr class="tableVdmColor">
								<th class="thAudio">Port Number</th>
								<th class="thAudio">User Number/Ext</th>
								<th class="thAudio">Line Feature</th>
								<th class="thAudio">Feature Option/Ext</th>
							</tr>
						</thead>

						<tbody id="tabletr" class="vdmTbody">						
                            
                        <?php 
                        //print_r($userDeviceArray); exit;
                        $portNumber = 1;
                        foreach($userDeviceArray as $userDeviceKey => $userDeviceValue)
                        {
                            $disableLineDropdown = isset($userDeviceValue['isSAS']) && $userDeviceValue['isSAS'] =="true" ? "disabled" : "";
                            $selectAutoDialingVal = $userDeviceValue['lineFeature'] == "1" ? "selected" : "";
                            $addSASHidden = isset($userDeviceValue['isSASLine']) && $userDeviceValue['isSASLine'] == "true"; 
                            $sasLineValue = isset($userDeviceValue['sasTagValue']) ? $userDeviceValue['sasTagValue'] :"";
                            $sasLineDispValue = isset($userDeviceValue['sasTagDisplayName']) ? $userDeviceValue['sasTagDisplayName'] :"";
                            $userNumberColumn = $userDeviceValue['userInfo'];
                            $featureOption = isset($userDeviceValue['featureValue']) ? $userDeviceValue['featureValue'] : "";
                            $portNumberColomn = $portNumber. " < " . $userDeviceValue['label'] . ">";
                            $lineFeatureTagName = $userDeviceValue['lineFeatureTagName'];
                            $featureTagName = $userDeviceValue['featureValueTagName'];
                            $registrationStatus = $userDeviceValue['registration'];
                            if($registrationStatus == "Registered"){$color = "#72ac5d";}else{$color = "#ac5f5d";}
                            
                            $userId = isset($userDeviceValue['userId']) ? $userDeviceValue['userId'] : "" ;
                            if(!empty($userId)){
                                $userOperationObj = new UserOperations();
                                $userDetail = $userOperationObj->getUserDetail($userId);
                                //print_r($userDetail);
                                if(empty($userDetail['Error']))
                                {
                                    if(isset($userDetail['Success']['lastName'])){
                                        $lastName = $userDetail['Success']['lastName'];
                                    }else{
                                        $lastName = "";
                                    }
                                    if(isset($userDetail['Success']['firstName'])){
                                        $firstName = $userDetail['Success']['firstName'];
                                    }else{
                                        $firstName = "";
                                    }
                                    if(isset($userDetail['Success']['callingLineIdPhoneNumber'])){
                                        $userPhone = $userDetail['Success']['callingLineIdPhoneNumber'];
                                    }else{
                                        $userPhone = "";
                                    }
                                    
                                    if(isset($userDetail['Success']['extension'])){
                                        $userExtension = $userDetail['Success']['extension'];
                                    }else{
                                        $userExtension = "";
                                    }
                                    
                                    //$_SESSION['userInfo'] = $userDetail['Success'];
                                }
                            }
                            //$firstName = isset($userDeviceValue['firstName']) ? $userDeviceValue['firstName'] : "";
                            //$lastName = isset($userDeviceValue['lastName']) ? $userDeviceValue['lastName'] : "";
                            
                            $userNumberColumnLink = "<a style='color:".$color."' href='#' class='primaryUserModifyLink'
                            id='" . $userId . "' data-phone='".$userPhone."' data-extension='".$userExtension."' data-lastname='" . $lastName . "' data-firstname='" . $firstName . "'>" . $userNumberColumn . "</a>";
                            ?>
                            
                            <tr>
								<td class="thAudio"> Port # <?php echo $portNumberColomn; ?></td>
								
								<?php if($addSASHidden)
								{
								    ?><td class="thAudio">
								     <span class="spanSas"><?php echo $sasLineDispValue; ?></span>
								     <input type="hidden" name="customTags['<?php echo $userDeviceValue['sasTagName']; ?>']" value ='<?php echo $sasLineValue; ?>' data ='<?php echo $sasLineValue; ?>' />    
								<?php 
                                } else {
								?>
									<td class="thAudio"><span style="color:<?php echo $color;?>"><?php  echo $userNumberColumnLink; ?></span>
								<?php }?>
								</td>
								<td class="thAudio">	
								<div class="dropdown-wrap">								
									<select style="" name="customTags['<?php echo $lineFeatureTagName;?>']" id="<?php echo $lineFeatureTagName;?>"<?php echo $disableLineDropdown; ?>  class="lineFeatureClass">
										<option value="0">None</option>
										<option value="1" <?php echo $selectAutoDialingVal; ?>>Automatic Dialing</option>
									</select>
								</div>
								</td>
								<td class="thAudio"> <input type="text" class="vdmFormtest inputBox" name="customTags['<?php echo $featureTagName; ?>']" value ='<?php echo $featureOption; ?>' /></td>
							</tr>
							
                        <?php
                        $portNumber++;
                        }
                        
                         ?>	
							
							
						</tbody>
					</table>
				</div>
				</div>
			</div>
		</div>
		
                    <div class="vdmForm" id="indexForm">
			 <?php if($rebuildResetDevice == "true"){?>
				<div class="row">
					<div class="col-md-12">
					<div class="form-group">
							<label class="labelText labelAboveInput" for="">Rebuild and Reset
								Device :</label>
					</div>
					</div>
					<div class="">
						<div class="col-md-3" style="padding:0">
							<div class="form-group">
								<input type="checkbox" name="rebuildPhoneFiles"
									id="rebuildPhoneFiles" value="true"> <label class="labelText"
									for="rebuildPhoneFiles"><span></span>Rebuild Phone Files</label>
							</div>
						</div>
						<div class="col-md-3" style="padding:0">
						<div class="form-group">
								<input type="checkbox" name="resetPhone" id="resetPhone"
									value="true"> <label class="labelText" for="resetPhone"><span></span>Reset
									the Phone</label>
						</div>
						</div>
						<div class="col-md-6">
						<div class="form-group"></div>
						</div>
					</div>
				</div>
				
				
				<?php }else{?>
					<input type="hidden" name="rebuildPhoneFiles"
					id="rebuildPhoneFiles" value="true" /> <input type="hidden"
					name="resetPhone" id="resetPhone" value="true" />
				<?php }?>
				
			
			<?php if (isset($_SESSION["permissions"]["vdmAdvanced"]) && $_SESSION["permissions"]["vdmAdvanced"] == "1") { ?>
			<div style="" class="alignBtn">
				<input type="button" id="advancedOption" class="go" value="Show Advanced Options">
			</div>
			<?php }?>

			<!-- Show Advanced Options Div -->
			<div class="" id="AlgoAdvancedOption"  style="display: block;">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label class="labelText">Silence Detection Period :</label>
						</div>

					</div>
				</div>

					<div class="row">
						<div class="col-md-12">
							<span class="selectInputRadio"> <label class="labelText" for="">
									Cut Off Timers : </label>
							</span>
							<!--  	<input type="radio" name="cutOffTimes" value="cutOffTimes" style="width: 25px" /> -->
							<input type="checkbox" id="%ACCT.CUTOFF_TIMERS%"
								name="customTags['%ACCT.CUTOFF_TIMERS%']"
								value="1"
                         <?php if(isset($customTags['%ACCT.CUTOFF_TIMERS%']) && $customTags['%ACCT.CUTOFF_TIMERS%'][0] == 1){ echo "checked";} ?> />

							<label for="%ACCT.CUTOFF_TIMERS%" class="labelText"><span></span></label>
						</div>

						<div class="col-md-12">
						<input type="text" name="customTags['%ACCT.SILENCE_DETECTION_PERIOD%']" value="<?php if(isset($customTags['%ACCT.SILENCE_DETECTION_PERIOD%'][0])){echo $customTags['%ACCT.SILENCE_DETECTION_PERIOD%'][0];}?>" id="%ACCT.SILENCE_DETECTION_PERIOD%">
						</div>
					</div>
			
			</div>
			<div class="spacer" style="margin-top: 20px">&nbsp</div>

			<div class="alignBtn">
			<input type="button" id="subButtonCancel" value="Cancel" class="cancelButton"> <span class="spacer">&nbsp;</span> <input
					type="button" id="subButton" class="subButton" value="Save" style="">
			</div>
                    </div>
                
		</form>
		
		<input type="hidden" id="vdmTypeHidden" name="vdmTypeHidden" value="audioCodesMP">
		

	
    
    </div>
	<div id="dialogAudio" class="dialogClass"></div>
<!-- 	Dialog for changes -->
<!-- 	<div id="dialogForChangesAudio" class="dialogClass"></div> -->
