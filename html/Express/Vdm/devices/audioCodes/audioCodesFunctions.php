<?php
    
    function compareByName($a, $b)
    {
        return strcmp($a["order"], $b["order"]);
    }
    
    // Create Custom Tags
    
    function checkDefaultTagsExistAudioCode($postArray, $sasPorts, $ports){
        
        $missingTags = array();
        $totalSasPort = array();
        for($sasNumber = 0; $sasNumber < $sasPorts; $sasNumber++) {
            $totalSasPort["%ACCT.SAS_".($sasNumber+1)."%"] = "";
        }
        
        //�	Automatic Dialing
        $totalLineFeatPort = array();
        for($lineFeat = 0; $lineFeat < $ports; $lineFeat++) {
            $totalLineFeatPort["%ACCT.AUTODIAL_STATUS".($lineFeat+1)."%"] = "0";
        }
        
        //Feature Option
        $totalFeatOption = array();
        for($featOption = 0; $featOption < $ports; $featOption++) {
            $totalFeatOption["%ACCT.AUTODIAL_NUMBER".($featOption+1)."%"] = "0";
        }
        
        $defaultTags = array(
            "%ACCT.DHCP.USE%" => "",
            "%ACCT.LOCATION%" => "",
            "%ACCT.SN%" => "",
            "%ACCT.SHIP_DATE%" => "",
            "%ACCT.CUTOFF_TIMERS%" => "0",
            "%ACCT.SILENCE_DETECTION_PERIOD%" => "",
            
            //�	Automatic Dialing
//             "%ACCT.AUTODIAL_STATUS1%" => "0",
//             "%ACCT.AUTODIAL_STATUS2%" => "0",
//             "%ACCT.AUTODIAL_STATUS3%" => "0",
//             "%ACCT.AUTODIAL_STATUS4%" => "0",
//             "%ACCT.AUTODIAL_STATUS5%" => "0",
//             "%ACCT.AUTODIAL_STATUS6%" => "0",
//             "%ACCT.AUTODIAL_STATUS7%" => "0",
//             "%ACCT.AUTODIAL_STATUS8%" => "0",
            
            
            //Feature Option
//             "%ACCT.AUTODIAL_NUMBER1%" => "0",
//             "%ACCT.AUTODIAL_NUMBER2%" => "0",
//             "%ACCT.AUTODIAL_NUMBER3%" => "0",
//             "%ACCT.AUTODIAL_NUMBER4%" => "0",
//             "%ACCT.AUTODIAL_NUMBER5%" => "0",
//             "%ACCT.AUTODIAL_NUMBER6%" => "0",
//             "%ACCT.AUTODIAL_NUMBER7%" => "0",
//             "%ACCT.AUTODIAL_NUMBER8%" => "0",
            
        );
       
        $allDefaultTags = array_merge(
                                        array_merge(
                                                    array_merge($totalLineFeatPort, $totalFeatOption),
                                         $totalSasPort),
                           $defaultTags);
        //print_r($allDefaultTags); exit;
        //$allDefaultTags = array_merge($totalSasPort, $defaultTags);
        foreach($allDefaultTags as $key=>$val){
            if(!array_key_exists($key, $postArray)){
                $missingTags[$key] = $val;
            }
        }
        
        return $missingTags;
    }
    
    function addCustomTags($spId, $groupId, $deviceName, $tagsArray){
        
        require_once ("/var/www/lib/broadsoft/adminPortal/customTagOperations/UserCustomTagOperations.php");
        
        $userCustomTags = new UserCustomTagOperations();
        foreach($tagsArray as $key=>$val){
            $tagName = $key;
            $tagValue = $val;
            $responseAdd = $userCustomTags->addDeviceCustomTag($spId, $groupId, $deviceName, $tagName, $tagValue);
        }
    }
    
    
    global $configuredSASTags;
    $result = array();
    $configuredSASTags =0;
    $vdm = new VdmOperations();
    
    global $db;
	//Code added @ 16 July 2019
    $whereCndtn = "";
    if($_SESSION['cluster_support']){
        $selectedCluster = $_SESSION['selectedCluster'];
        $whereCndtn = " AND clusterName ='$selectedCluster' ";
    }
    //End code
    $portQuery = "SELECT ports, fxoPorts, sasLines from systemDevices where deviceType = '" . $deviceType . "' $whereCndtn ";
    $qwr = $db->query($portQuery);
    while ($r = $qwr->fetch()) {
        $ports = $r["ports"];
        $fxoPorts = $r["fxoPorts"];
        $sasPorts = $r["sasLines"];
    }
    
    $getDeviceDetails = $vdm->getUserDeviceDetail($sp, $groupId, $deviceName);
    if (empty($getDeviceDetails['Error'])) {
        $deviceDetails = $_SESSION['device']['details'] = $getDeviceDetails['Success'];
        $getCustomTags = $vdm->getDeviceCustomTags($sp, $groupId, $deviceName);
        
        
        if(empty($getCustomTags['Error'])){
            $customTags = $getCustomTags['Success'];
            
            //check If custom tags exist or not and if not than add and again get the tags
            $checkMissingCustomTags = checkDefaultTagsExistAudioCode($customTags, $sasPorts, $ports);
            
            if(count($checkMissingCustomTags) > 0){
                addCustomTags($sp, $groupId, $deviceName, $checkMissingCustomTags);
                
                //again get the custom tags
                $getCustomTags = $vdm->getDeviceCustomTags($sp, $groupId, $deviceName);
                if(empty($getCustomTags['Error'])){
                    $customTags = $getCustomTags['Success'];
                }
            }
            $allCustomTags = $_SESSION['device']['customTags'] = $customTags;
        }
        
        $getUserDevice = $vdm->getAllUserOfDevice($sp, $groupId, $deviceName);
    }

    $userDevice = "";
	if (empty($getUserDevice['Error'])) {
	    if (count($getUserDevice['Success']) > 0) {
	        // sort user array by order field
		        usort($getUserDevice['Success'], 'compareByName');
		        $userDevice = $getUserDevice['Success'];
            }
	}
    
    $fxsPort = $ports - $fxoPorts;
    $userLinePort = $ports - $sasPorts;     //SAS Port start from
   
    //Start Algo
    $userDeviceArray = array();
    $numberOfSASPorts = $sasPorts;
    for ($devicePortNumber = 0; $devicePortNumber < $ports; $devicePortNumber++) 
    {
            if($devicePortNumber < $fxsPort){
                $userDeviceArray[$devicePortNumber]['label'] = "FXS";
            }else{
                $userDeviceArray[$devicePortNumber]['label'] = "FXO";
                }
            
            // 
            $userInfo = "";
            if($userDevice != "")
            {
               $userPortInfo = searchForKey("order", $devicePortNumber+1, $userDevice);
               if($userPortInfo != "")
                {
                    if($devicePortNumber >= $userLinePort)
                    {
                        $numberOfSASPorts--;
                    }
                    if (!empty($userPortInfo['phoneNumber'])) {
                        $userInfo = $userPortInfo['phoneNumber'];
                    } else {
                        $userInfo = $userPortInfo['extn'];
                    }
                    $userDeviceArray[$devicePortNumber]['userId'] = $userPortInfo['userId'];
                }
            }
            $userDeviceArray[$devicePortNumber]['userInfo'] = $userInfo;
           
            if(isset($userPortInfo['userId']) && !empty($userPortInfo['userId'])){
            	$userDeviceArray[$devicePortNumber]['registration'] = $vdm->getUserStatus($userPortInfo['userId']);
            }else{
           		$userDeviceArray[$devicePortNumber]['registration'] = "";
            }
            
            $lfTagNo = $devicePortNumber + 1;
            $userDeviceArray[$devicePortNumber]['lineFeatureTagName'] = "%ACCT.AUTODIAL_STATUS".$lfTagNo."%";
            if(array_key_exists("%ACCT.AUTODIAL_STATUS".$lfTagNo."%", $allCustomTags))
            {
                $userDeviceArray[$devicePortNumber]['lineFeature'] = $allCustomTags["%ACCT.AUTODIAL_STATUS".$lfTagNo."%"][0];
            }else{
                $userDeviceArray[$devicePortNumber]['lineFeature'] = 0;
            }
            
            $userDeviceArray[$devicePortNumber]['featureValueTagName'] = "%ACCT.AUTODIAL_NUMBER".$lfTagNo."%";
            if(array_key_exists("%ACCT.AUTODIAL_NUMBER".$lfTagNo."%", $allCustomTags))
            {
                $userDeviceArray[$devicePortNumber]['featureValue'] = $allCustomTags["%ACCT.AUTODIAL_NUMBER".$lfTagNo."%"][0];
            }else{
                $userDeviceArray[$devicePortNumber]['featureValue'] = "";
            }
            
    }   

    $sasCustomTagList = getSASCustomTags($allCustomTags, $sasPorts);
    $userDeviceArray = processSASPorts($sasCustomTagList, $userLinePort, $ports, $userDeviceArray);

    // SAS Processing
    function processSASPorts($sasCustomTagList, $userLinePort, $ports, $userDeviceArray)
    {
        global $configuredSASTags;
        if(count($sasCustomTagList) > 0 && $userLinePort != $ports)
        {
            $sasIndex = 1;
            for ($sasLinePort = $userLinePort; $sasLinePort < $ports; $sasLinePort++) {
                if(array_key_exists("%ACCT.SAS_".$sasIndex."%", $sasCustomTagList) && !empty($sasCustomTagList["%ACCT.SAS_".$sasIndex."%"]))
                {
                    $userDeviceArray[$sasLinePort]['isSAS'] = "true";
                    $userDeviceArray[$sasLinePort]['isSASLine'] = "true";
                    $userDeviceArray[$sasLinePort]['isSASTagExist'] = "true";
                    $userDeviceArray[$sasLinePort]['sasTagValue'] = $sasCustomTagList["%ACCT.SAS_".$sasIndex."%"];
                    $userDeviceArray[$sasLinePort]['sasTagName'] = "%ACCT.SAS_".$sasIndex."%";
                    $userDeviceArray[$sasLinePort]['sasTagDisplayName'] = "SAS_".$sasIndex;
                    $configuredSASTags++;
                }else if(! isset($userDeviceArray[$sasLinePort]['userId'])){
                    $userDeviceArray[$sasLinePort]['isSAS'] = "false";
                    $userDeviceArray[$sasLinePort]['isSASLine'] = "true";
                    $userDeviceArray[$sasLinePort]['isSASTagExist'] = "false";
                    $port = $sasLinePort + 1;
                    $userDeviceArray[$sasLinePort]['sasTagValue'] = $port ;
                    $userDeviceArray[$sasLinePort]['sasTagName'] = "%ACCT.SAS_".$sasIndex."%";
                    $userDeviceArray[$sasLinePort]['sasTagDisplayName'] = "SAS_".$sasIndex;
                }
                if(isset($userDeviceArray[$sasLinePort]['userId'])){
                $userDeviceArray[$sasLinePort]['isSAS'] = "false";
                $userDeviceArray[$sasLinePort]['isSASLine'] = "false";
                }
                $sasIndex++;
                
            }
        } 
        //print_r($userDeviceArray); exit;
        return $userDeviceArray;
    }
    
    // getSASCustomTags
    function getSASCustomTags($getCustomTags, $sasPorts)
    {
        $listOfSASCustomTags = array();
        for ($count = 1; $count <= $sasPorts; $count++) {
           
            if(array_key_exists("%ACCT.SAS_".$count."%", $getCustomTags))
            {
                $listOfSASCustomTags["%ACCT.SAS_".$count."%"] = $getCustomTags["%ACCT.SAS_".$count."%"][0];
            }
        }
        return $listOfSASCustomTags;
    }
    
    // searchForKey
    function searchForKey($searchKey, $searchValue, $array) {
        $returnVal = "";
        foreach ($array as $key => $value) {
            
            $result = $value[$searchKey];
            if($result == $searchValue)
            {
                $returnVal = $value;
                break;
            }
        }
        return $returnVal;        
    }
?>
