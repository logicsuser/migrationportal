<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
$vdm = new VdmOperations();

$changeString = "";
$changeStringReset = "";

if(!array_key_exists("rebuildPhoneFiles", $_POST)){
	$_POST['rebuildPhoneFiles'] = false;
}

if(!array_key_exists("resetPhone", $_POST)){
	$_POST['resetPhone'] = false;
}

foreach($_POST['customTags'] as $key=>$val){
	if(!array_key_exists("'%ACCT.CUTOFF_TIMERS%'", $_POST['customTags'])){
		$_POST['customTags']['%ACCT.CUTOFF_TIMERS%'] = "0";
	}
}

foreach($_POST as $key=>$val){
	if($key != "customTags"){
		$deviceArray['details'][$key] = $_POST[$key];
	}else{
		if($val <> "Error" || $val <> "Success"){
			$deviceArray[$key] = $_POST[$key];
		}
	}
}

//remove the single quotes from key coming from post
foreach($deviceArray['customTags'] as $key=>$val){
	$keyc = str_replace("'", "", $key); 
	$tagsArray['customTags'][$keyc] = $val;
}

//echo "<pre>SESSION"; print_r($_SESSION['device']);echo "<pre>POST"; print_r($deviceArray);
//$readOnlyFields = array("%AC.DHCP.USE%", "%AC.SILENCE_DETECTION%", "site");

$lineFeatureArray = array(
                        "0" => "None",
                        "1" => "Automatic Dialing",
                    );


$deviceDetailsArray = array(
                            "deviceName" => "Device Name",
                            "site" => "Site",
                            "macAddress" => "Mac Address",
                            "sasLines" => "SAS Lines",
                            "serialNumber" => "Serial Number",
                            "physicalLocation" => "Physical Location",
                            "description" => "Ship Date",
                            "netAddress" => "IP Address",
                       );


$deviceTagsArray = array(
   // "%ACCT.DHCP.USE%" => "IP Address",
   // "%ACCT.LOCATION%" => "Location",
   // "%ACCT.SN%" => "Serial Number",
    //"%ACCT.SHIP_DATE%" => "Ship Date",
    "%ACCT.CUTOFF_TIMERS%" => "Cut Off Times",
    "%ACCT.SILENCE_DETECTION_PERIOD%" => "Silence Detection Period",

    //�	Automatic Dialing
    "%ACCT.AUTODIAL_STATUS1%" => "Line Feature 1",
    "%ACCT.AUTODIAL_STATUS2%" => "Line Feature 2",
    "%ACCT.AUTODIAL_STATUS3%" => "Line Feature 3",
    "%ACCT.AUTODIAL_STATUS4%" => "Line Feature 4",
    "%ACCT.AUTODIAL_STATUS5%" => "Line Feature 5",
    "%ACCT.AUTODIAL_STATUS6%" => "Line Feature 6",
    "%ACCT.AUTODIAL_STATUS7%" => "Line Feature 7",
    "%ACCT.AUTODIAL_STATUS8%" => "Line Feature 8",
    "%ACCT.AUTODIAL_STATUS9%" => "Line Feature 9",
    "%ACCT.AUTODIAL_STATUS10%" => "Line Feature 10",
    "%ACCT.AUTODIAL_STATUS11%" => "Line Feature 11",
    "%ACCT.AUTODIAL_STATUS12%" => "Line Feature 12",
    "%ACCT.AUTODIAL_STATUS13%" => "Line Feature 13",
    "%ACCT.AUTODIAL_STATUS14%" => "Line Feature 14",
    "%ACCT.AUTODIAL_STATUS15%" => "Line Feature 15",
    "%ACCT.AUTODIAL_STATUS16%" => "Line Feature 16",
    "%ACCT.AUTODIAL_STATUS17%" => "Line Feature 17",
    "%ACCT.AUTODIAL_STATUS18%" => "Line Feature 18",
    "%ACCT.AUTODIAL_STATUS19%" => "Line Feature 19",
    "%ACCT.AUTODIAL_STATUS20%" => "Line Feature 20",
    "%ACCT.AUTODIAL_STATUS21%" => "Line Feature 21",
    "%ACCT.AUTODIAL_STATUS22%" => "Line Feature 22",
    "%ACCT.AUTODIAL_STATUS23%" => "Line Feature 23",
    "%ACCT.AUTODIAL_STATUS24%" => "Line Feature 24",
    
    
    //Feature Option
    "%ACCT.AUTODIAL_NUMBER1%" => "Feature Option 1",
    "%ACCT.AUTODIAL_NUMBER2%" => "Feature Option 2",
    "%ACCT.AUTODIAL_NUMBER3%" => "Feature Option 3",
    "%ACCT.AUTODIAL_NUMBER4%" => "Feature Option 4",
    "%ACCT.AUTODIAL_NUMBER5%" => "Feature Option 5",
    "%ACCT.AUTODIAL_NUMBER6%" => "Feature Option 6",
    "%ACCT.AUTODIAL_NUMBER7%" => "Feature Option 7",
    "%ACCT.AUTODIAL_NUMBER8%" => "Feature Option 8",
    "%ACCT.AUTODIAL_NUMBER9%" => "Feature Option 9",
    "%ACCT.AUTODIAL_NUMBER10%" => "Feature Option 10",
    "%ACCT.AUTODIAL_NUMBER11%" => "Feature Option 11",
    "%ACCT.AUTODIAL_NUMBER12%" => "Feature Option 12",
    "%ACCT.AUTODIAL_NUMBER13%" => "Feature Option 13",
    "%ACCT.AUTODIAL_NUMBER14%" => "Feature Option 14",
    "%ACCT.AUTODIAL_NUMBER15%" => "Feature Option 15",
    "%ACCT.AUTODIAL_NUMBER16%" => "Feature Option 16",
    "%ACCT.AUTODIAL_NUMBER17%" => "Feature Option 17",
    "%ACCT.AUTODIAL_NUMBER18%" => "Feature Option 18",
    "%ACCT.AUTODIAL_NUMBER19%" => "Feature Option 19",
    "%ACCT.AUTODIAL_NUMBER20%" => "Feature Option 20",
    "%ACCT.AUTODIAL_NUMBER21%" => "Feature Option 21",
    "%ACCT.AUTODIAL_NUMBER22%" => "Feature Option 22",
    "%ACCT.AUTODIAL_NUMBER23%" => "Feature Option 23",
    "%ACCT.AUTODIAL_NUMBER24%" => "Feature Option 24",
    
    //SAS Port
    "%ACCT.SAS_1%" => "SAS_1",
    "%ACCT.SAS_2%" => "SAS_2",
    "%ACCT.SAS_3%" => "SAS_3",
    "%ACCT.SAS_4%" => "SAS_4",

);




$_SESSION['deviceUpdated'] = false;
$_SESSION['customTagsUpdated'] = false;
//newcode starts
$error = "";
$backgroundColor = 'background:ac5f5d;'; $errorMessage = '';

if(isset($deviceArray['details']['deviceName']) && !empty($deviceArray['details']['deviceName'])){
	
	foreach($deviceArray['details'] as $key=>$val){
		foreach($_SESSION['device']['details'] as $key1=>$val1){
			$backgroundColor = 'background:#ac5f5d;'; $errorMessage = '';
			if($key == $key1){
				$val = trim($val);
				if($val <> $val1){
				    if($key == "macAddress"){
				    	if(!empty($val)){
					        if(strlen($val) <> 12 || !ctype_xdigit($val)){
					            $backgroundColor = 'background:#72ac5d;'; $error = 'Error'; $errorMessage = "is not a valid Mac Address (Mac Address should be 12 characters hexadecimal value).";
					        }else if($deviceArray['details']['serialNumber'] != "" && strtoupper("00908F".dechex($deviceArray['details']['serialNumber'])) != $val){
					            $backgroundColor = 'background:#72ac5d;'; $error = 'Error'; $errorMessage = " Mac Address doesn't match with Serial Number).";
					        }
				    	}
				    }else if($key == "serialNumber"){
				        if(!empty($val)){
    				    	$lengthArray = array("8", "7");
    				    	//if(strlen($val) <> 8){
    				    	$length = strlen($val);
    				    	if(!in_array($length, $lengthArray)){
    				    		$backgroundColor = 'background:#72ac5d;'; $error = 'Error'; $errorMessage = "is not a valid Serial Number (Serial Number should be 8 characters value).";
    				    	}else if($val > 16777215){
    				    	    $backgroundColor = 'background:#72ac5d;'; $error = 'Error'; $errorMessage = "Serial number value cannot be greater than 16777215.";
    				    	}
				        }
				    }
				   if($val == ""){$val = "None";}
					if (array_key_exists($key, $deviceDetailsArray)) {
						$_SESSION['deviceUpdated'] = true;
						$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$deviceDetailsArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
					}
				}
			}
		}
	}	
	
	foreach($tagsArray['customTags'] as $key3=>$val3){
		foreach($_SESSION['device']['customTags'] as $key4=>$val4){
			$backgroundColor = 'background:#ac5f5d;'; $errorMessage = '';
			if($key3 == $key4){
			    // Check if any changes
				if($val4[0] <> $val3){
				    /*if($key3 == "%ACCT.SN%"){
				        if(strlen($val3) <> 7){
				            $backgroundColor = 'background:#72ac5d;'; $error = 'Error'; $errorMessage = "is not a valid Serial Number (Serial Number should be 7 characters value).";
				        }
				    }
				    else*/ if($val3 == ""){
				        $val3 = "None";
				    }else if($key3 == "%ACCT.CUTOFF_TIMERS%"){
				        if($val3 == 1){
				            $val3 = "On";
				        }else{
				            $val3 = "Off";
				        }
				    }
				    
					if (array_key_exists($key3, $deviceTagsArray)) {
    				    if(strpos($key3, "AUTODIAL_STATUS")> 0){
    				        $val3 = $lineFeatureArray[$val3];
    				    }
						$_SESSION['customTagsUpdated'] = true;
						$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$deviceTagsArray[$key3].'</td><td class="errorTableRows">'.$val3.' '. $errorMessage.' </td></tr>';
					}
				}
			}
		}
	}

	if ($_SESSION['customTagsUpdated']){
		
		if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild and reset after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == false){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == false && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be reset after successful modification</b></td></tr>';
		}
	}else{
		//$changeStringReset = "";
		if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild and reset after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == false){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == false && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be reset after successful modification</b></td></tr>';
		}
	}
	
	if($_SESSION['deviceUpdated'] == false && $_SESSION['customTagsUpdated'] == false && $_POST['rebuildPhoneFiles'] == false && $_POST['resetPhone'] == false){
		$changeString = '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'">Device Update</td><td class="errorTableRows">No Changes </td></tr>';
	}
}

//newcode ends
echo $error.$changeStringReset.$changeString;

?>