<script>
$(function()
{
// 	var formDataUpdated = "false";
// 	$("#deviceName").autocomplete({
// 	    select: function (event , ui) {
// 	    	var formDataUpdated = "false";
// 	    	var dataToSend = $("form#vdmLightForm").serializeArray();
			 
// 	    	if(ui.item.label != $("#tempDeviceName").val()) {
//     	    	$.ajax({
//     	 			type: "POST",
//     	 			url: "Vdm/devices/audioCodes/audioCodes-Validate.php",
//     	 			data: dataToSend,
//     	 			success: function(result) {
//     	 				var noChangesExist = result.search("No Changes");
//     	 				if(noChangesExist == -1 && $("#indexForm").is(":visible")){
//     	 					$("#dialogForChangesAudio").dialog("open");
//     		 			} else {
//     						refreshOnSearchDevice();
//     		 			}
    
//     	 			}
//     	 		});
// 	    	}
// 	    },
// 	});


// 	$(".vdmDeviceChange").change(function() {
// 			navVdmType = "";
//			var vdmType = "<?php //echo $vdmType;?>";
// 			if(vdmType == "audioCodesMP") {
				 
//     	    	var dataToSend = $("form#vdmLightForm").serializeArray();
//     	 		$.ajax({
//     	 			type: "POST",
//     	 			url: "Vdm/devices/audioCodes/audioCodes-Validate.php",
//     	 			data: dataToSend,
//     	 			success: function(result) {
//     	 				var noChangesExist = result.search("No Changes");
//     	 				if(noChangesExist == -1 && $("#indexForm").is(":visible")){
//     		 				formDataUpdated = "true";
//     	 					$("#dialogForChangesAudio").dialog("open");
//     		 			} else {
//     		 				$("#isGetClick").val("false");
//     						loadDeviceName();
//     			 		}
//     	 			}
//     	 		});
// 			}
// 	});
	
// 	$("#dialogForChangesAudio").dialog({
// 		autoOpen: false,
// 		width: 800,
// 		modal: true,
// 		position: { my: "top", at: "top" },
// 		resizable: false,
// 		closeOnEscape: false,
// 		buttons: {
// 			"Discard Changes and Continue": function() {
// 				if(formDataUpdated == "true") {
// 					$("#isGetClick").val("false");
// 					loadDeviceName();
// 				} else {
// 					$("#deviceDetails").html("");
// 					$("#indexForm").hide();
// 					var bck = jQuery.Event("keydown", { keyCode: 20 });
// 					$("#deviceName").trigger( bck );
// 				}
// 				$(this).dialog("close");
// 			},
// 			"Cancel": function() {
// 				$(this).dialog("close");
// 				$("#loading2").hide();
// 				$("#vdmLightForm").show();
// 				var tempDeviceType = $("#tempDeviceType").val();
// 				var tempDeviceName = $("#tempDeviceName").val();
// 				$('[name=vdmType]').val(tempDeviceType);
// 				$("#deviceName").val(tempDeviceName);				
// 			},
// 		},
// 		open: function() {
// 				$("#dialogForChangesAudio").dialog("option", "title", "Warning Message");
// 				$("#dialogForChangesAudio").html("<b>Attempted search for another device will discard changes made on this page. </b><br>");
// 				$("#dialogForChangesAudio").html("<b>Please click on 'Discard Changes and Continue' to proceed with a search without saving changes. Press 'Cancel' to stay on the page</b>");
//         }
// 	});

	
	$("#dialogAudio").html('');	
	$("#AlgoAdvancedOption").hide();	
	$("#loading2").hide();	
	$("#advancedOption").click(function(){
		var buttonData = $("#advancedOption").val();
		if(buttonData == "Show Advanced Options"){
			$("#advancedOption").val("Hide Advanced Options");
			$("#AlgoAdvancedOption").show();
		}else if(buttonData == "Hide Advanced Options"){
			$("#advancedOption").val("Show Advanced Options");
			$("#AlgoAdvancedOption").hide();
		}	    
	}); 

	var validateAlgoDevice = function(){
    	var dataToSend = $("form#vdmLightForm").serializeArray();
		$.ajax({
			type: "POST",
			url: "Vdm/devices/audioCodes/audioCodes-Validate.php",
			data: dataToSend,
			success: function(result) {
				var noChangesExist = result.search("No Changes");
				if(noChangesExist >0){
					$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
				}else{
					$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
				}

				var errorExist = result.search("Error");
				$("#loading2").hide();		
				$("#dialogAudio").dialog("option", "title", "Request Complete");
				$("#dialogAudio").html('<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelist" class="confSettingTable"><tbody><tr><th colspan="2" align="center">Please confirm the settings below and click Complete to process your modifications.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');		    	
				$("#changelist").append(result);
				$("#dialogAudio").append('</tbody></table>');
				$("#dialogAudio").dialog("open");
				if(noChangesExist > 0 || errorExist == 0){
					buttonsShowHide('Complete', 'hide');
					buttonsShowHide('Cancel', 'show');
					buttonsShowHide('More Changes', 'hide');
					buttonsShowHide('Ok', 'hide');
				}else{				
					buttonsShowHide('Complete', 'show');
					buttonsShowHide('Cancel', 'show');
					buttonsShowHide('More Changes', 'hide');
					buttonsShowHide('Ok', 'hide');
				}

			}
		});
	};


	$("#subButtonCancel").click(function(){
		//$("#deviceName").val($('ul.ui-autocomplete:eq(1) li:first a').text(""));
		//$("#deviceName").html(html);
		var autoComplete = [];
		   $("#deviceName").autocomplete({
             source: autoComplete
         });
		$('#vdmType').val("");
		//$('#deviceName').html("");
		$('#deviceName').val("");
		$('#vdmLightDisplayContent').hide();
		$('html, body').animate({scrollTop: '0px'}, 300);
		
	});
	
	var modifyAudioDevice = function(){
		
		var dataToSend = $("form#vdmLightForm").serializeArray();
		pendingProcess.push("Modify Device Management");
		$.ajax({
			url: 'Vdm/devices/audioCodes/complete.php',
            type: 'POST',
            data: dataToSend,
            success: function(data)
            {
            	if(foundServerConErrorOnProcess(data, "Modify Device Management")) {
    				return false;
              	}
            	var isErrorExist = data.search("background-color: #ac5f5d");
                if(isErrorExist > 0){
    				buttonsShowHide('Cancel', 'show');
                }else{
                	buttonsShowHide('Cancel', 'hide');
                 }
				$("#dialogAudio").dialog("option", "title", "Request Complete");
				$("#dialogAudio").html('<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelist" class="confSettingTable"><tbody><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');		    	
				$("#changelist").append(data);
				$("#dialogAudio").append('</tbody></table>');
				$("#dialogAudio").dialog("open");
				buttonsShowHide('More Changes', 'show');
				buttonsShowHide('Return to Main', 'show');
				buttonsShowHide('Complete', 'hide');
// 				buttonsShowHide('Cancel', 'hide');
				$(":button:contains('Complete')").removeAttr("disabled", "disabled").removeClass("ui-state-disabled");

            }
            
		});
	};

	$("#subButton").click(function(){
		$('html, body').animate({scrollTop: '0px'}, 300);
		$("#vdmLightForm").hide();
		$("#loading2").show();	
		$("#dialogAudio").html('');	
		validateAlgoDevice();
	});
	
	$("#dialogAudio").dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		buttons: {
			"Complete": function() {
				modifyAudioDevice();
			},
			"Cancel": function() {
				$(this).dialog("close");
				$("#loading2").hide();
				$("#vdmLightForm").show();
			},
			"More Changes": function() {
            	$(this).dialog("close");
            	$("#loading2").hide();               
	           	reloadPage();
             	$("#vdmLightForm").show();
            },
            "Ok": function() {
            	$(this).dialog("close"); 
 			},
			"Return to Main": function() {
				location.href="main.php";
			},
		},
        open: function() {
			setDialogDayNightMode($(this));
        	buttonsShowHide('Complete', 'show');
        	buttonsShowHide('Cancel', 'show');
        	buttonsShowHide('More Changes', 'hide');
        	buttonsShowHide('Return to Main', 'hide');
        	buttonsShowHide('Ok', 'hide');
        	$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('completeButton');
        	$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');
        	$('.ui-dialog-buttonpane').find('button:contains("More Changes")').addClass('moreChangesButton');
        	$('.ui-dialog-buttonpane').find('button:contains("Return to Main")').addClass('returnToMainButton');
        }
	});		

	var buttonsShowHide = function(b,e){
		if(e == "show"){
			$(".ui-dialog-buttonpane button:contains("+b+")").button().show();
		}else{
		 	$(".ui-dialog-buttonpane button:contains("+b+")").button().hide();
		}
	};

	$(".spanSas").hide();
	
	var showSpanSas = function(sasLineVal){
		var sasLineDone = 0;
		
		$( ".spanSas" ).each(function( index ) {
			if(sasLineDone < sasLineVal){
				
				$(this).show();
				var valToSet = $(this).parent().find('input[type=hidden]').attr('data');
				$(this).parent().find('input[type=hidden]').val(valToSet);
				$(this).parent('td').parent('tr').find('td:eq(2)').find('div').find('.lineFeatureClass').prop("disabled", "disabled");
				
			}else{
				$(this).hide();
				$(this).parent().find('input[type=hidden]').val('');
				$(this).parent('td').parent('tr').find('td:eq(2)').find('div').find('.lineFeatureClass').prop("disabled", false);
				
			}
				sasLineDone++;
		});
	}

	$("#sasLines").change(function(){
		var el = $(this);
		sasLineVal = el.val();
		showSpanSas(sasLineVal);
	});

	showSpanSas($("#sasLines").val());

	var reloadPage = function(){
		$("#subButtonDevice").trigger('click');
	}

	var onMacChange =  function(macAddress) {
		if(macAddress && macAddress.length == 12) {
			var trimmedMac = macAddress.substr(6);
			var sn = parseInt(trimmedMac, 16);
			//$(".serialNumber").val(sn);		
		}
	}

	var onSnChange =  function(sn) {
		macAddress = "";
		if(sn && (sn.length == 8 || sn.length == 7) && sn <= 16777215) {
			var macAddress = "00908F" + parseInt(sn).toString(16).toUpperCase();
			//$("#macAddress").val(macAddress);		
			$(".serialNumber").attr('style', 'border-color :#002c60 !important');	
			$("#subButton").prop("disabled", false);
			$(".serialNumber").tooltip('disable');
			$(".serialNumber").removeAttr( "title" )
			
		}else if(sn && (sn.length > 8 || sn.length < 7)){
			$(".serialNumber").attr('style', 'border-color :#ac5f5d !important');
			$("#serialNumber").attr("title", "Serial Number doesn't match with Mac Address and length should be 8 or 7");
			$("#subButton").prop("disabled", true);
			$(".serialNumber").tooltip();
		}else if(sn && (sn.length == 7 || sn.length == 8) && sn > 16777215){
			$(".serialNumber").attr('style', 'border-color :#ac5f5d !important');
			$("#serialNumber").attr("title", "Serial number value cannot be greater than 16777215");
			$(".serialNumber").tooltip();
			$("#subButton").prop("disabled", true);
		}
	}

// 	$(".macAddress").keyup(function(){
// 		var el = $(this);
// 		var val = el.val();
// 		onMacChange(val);
//    });

	//$(".serialNumber").keyup(function(){
	$(document).on('input', '.serialNumber', function(){
		var el = $(this);
		var val = el.val();
		onSnChange(val);
		if(! isNaN(val))
		{
			if(val.length <= 8)
		    {
				$(this).val(val);
		    }
		    else
			{
		    	$(this).val(val.slice(0,8));
		    }
		}
		else
		{
			$(this).val("");
		}

		var macAddress = $("#macAddress").val();
       if(macAddress.length > 0 && macAddress.length < 12){
    	   $("#macAddress").attr('style', 'border-color :#ac5f5d !important');
    	   $("#subButton").prop("disabled", true);
       }else{
    	   $("#macAddress").attr('style', 'border-color :#002c60 !important');
    	   $("#subButton").prop("disabled", false);	
    	}
   }); 
 
	var checkSnOnLoad = function(){
		var macAddressVal = $(".macAddress").val();
		var serialNumberVal = $(".serialNumber").val();

		if(macAddressVal && macAddressVal.length == 12) {
			var trimmedMac = macAddressVal.substr(6);
			var sn = parseInt(trimmedMac, 16);
			if(serialNumberVal != "" && sn != serialNumberVal){
				$(".serialNumber").attr('style', 'border-color :#ac5f5d !important');
				$("#subButton").prop("disabled", true);
				//$(".serialNumber").tooltip();
			}
		}
		if(serialNumberVal && (serialNumberVal.length == 8 || serialNumberVal.length == 7)) {
			var macAddress = "00908F" + parseInt(serialNumberVal).toString(16).toUpperCase();
			//$("#macAddress").val(macAddress);	
			$(".serialNumber").attr('style', 'border-color :#002c60 !important');	
			//$(".serialNumber").tooltip('disable');
		}else if(sn && (sn.length < 7 || sn.length > 8)){
			$(".serialNumber").attr('style', 'border-color :#ac5f5d !important');
			$("#subButton").prop("disabled", true);
			$(".serialNumber").tooltip();
		}
	};
	
	checkSnOnLoad();

		$(".primaryUserModifyLink").click(function()
				{
				$("#mainBody").html("<div class='col span_11 leftSpace' id='loader' style='margin-left:400px;margin-top:10%'> <img src='/Express/images/ajax-loader.gif'></div>");
				var userId = $(this).attr("id");
				var fname = $(this).attr("data-firstname");
				var lname = $(this).attr("data-lastname");

				var extension = $(this).attr("data-extension");
				var phone = $(this).attr("data-phone");
				if($.trim(phone) != ""){
					userIdValue1 = phone+"x"+extension;
				}else{
					userIdValue1 = userId;
				}
				var userIdValue = userIdValue1+" - "+lname+", "+fname;
				
				$.ajax({
						type: "POST",
						url: "userMod/userMod.php",
						data: { searchVal: userId, userFname : fname, userLname : lname },
						success: function(result)
						{
							$("#mainBody").html(result);
							$("#searchVal").val(userIdValue);
							setTimeout(function() {
								$("#go").trigger("click");
							}, 2000);
							//$(".subBanner").html("User Modify");
							$(".navMenu").removeClass("active");
							$("#userMod").addClass("active");
							$('#helpUrl').attr('data-module', "userMod");
						}

					});
	});

	$("#macAddress").on("input", function()
	{
		var macAddress = $("#macAddress").val();
		var macToLowerCase = macAddress.toLowerCase();
		var lastElmt = macToLowerCase.substr(-1);
		if((lastElmt >= "0" && lastElmt <= "9") || (lastElmt >= "a" && lastElmt <= "f")){
			//do something
	    }else{
	   		$("#macAddress").val(macAddress.slice(0,-1));
	    }
	    
	   var macAddress = $("#macAddress").val();
	   if(macAddress.length > 0 && macAddress.length < 12){
		   $("#macAddress").attr('style', 'border-color :#ac5f5d !important');
		   $("#subButton").prop("disabled", true);
	   }else{
		   $("#macAddress").attr('style', 'border-color :#002c60 !important');
		   $("#subButton").prop("disabled", false);	
		}
	});
	
});
</script>
<!-- Sks  -->

<script>

var activeImageSwap = function()
{	previousActiveMenu	= "modVdm";
	 currentClickedDiv = "userMod";       
  
		//active
  		if(previousActiveMenu != "")
		{ 
  			$("#modVdm").removeClass("activeNav");
  		 	$(".navMenu").removeClass("activeNav");
			var $thisPrev = $("#modVdm").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);
	       
		}
		// inactive tab
		if(currentClickedDiv != ""){
			$("#userMod").addClass("activeNav");
			var $thisPrev = $("#userMod").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);
			 previousActiveMenu = currentClickedDiv;
			
    	}
   
}
$('.primaryUserModifyLink').click(activeImageSwap, activeImageSwap);

</script>