<?php 
$micData =  micVolume($deviceTags);
$pageData =  pageVolume($deviceTags);

function micVolume($deviceTags){
	$micValue = "";
	foreach ($deviceTags["Success"] as $key => $value){
		if(isset($value[0])){
			if($value[0] == "%ALGOPAGEVOL%"){
				$micValue = $value[1];
			}
			
		}
	}
	return $micValue;
}

function pageVolume($deviceTags){
	$pageVolume = "";
	foreach ($deviceTags["Success"] as $key => $value){
		if(isset($value[0])){
			if($value[0] == "%ALGOPAGEVOL%"){
				$pageVolume= $value[1];
			}
			
		}
	}
	return $pageVolume;
}

function doorOpenCode($deviceTags){
	$doorOpenCode = "";
	foreach ($deviceTags["Success"] as $key => $value){
		if(isset($value[0])){
			if($value[0] == "%ALGO8028-OPEN%"){
				$doorOpenCode= $value[1];
			}
			
		}
	}
	return $doorOpenCode;
}

function buzzerDial($deviceTags){
	$buzzerDial= "";
	foreach ($deviceTags["Success"] as $key => $value){
		if(isset($value[0])){
			if($value[0] == "%ALGO8028-SPDIAL1%"){
				$buzzerDial= $value[1];
			}
			
		}
	}
	return $buzzerDial;
}

function DNSServer1($deviceTags){
	$DNSServer1= "";
	foreach ($deviceTags["Success"] as $key => $value){
		if(isset($value[0])){
			if($value[0] == "%ALGO.DNS1%"){
				$DNSServer1= $value[1];
			}
			
		}
	}
	return $DNSServer1;
}

function DNSServer2($deviceTags){
	$DNSServer2= "";
	foreach ($deviceTags["Success"] as $key => $value){
		if(isset($value[0])){
			if($value[0] == "%ALGO.DNS2%"){
				$DNSServer2= $value[1];
			}
			
		}
	}
	return $DNSServer2;
}

function IPGatewayAddress($deviceTags){
	$IPGatewayAddress= "";
	foreach ($deviceTags["Success"] as $key => $value){
		if(isset($value[0])){
			if($value[0] == "%ALGO.NET.GATEWAY%"){
				$IPGatewayAddress= $value[1];
			}
			
		}
	}
	return $IPGatewayAddress;
}

function AlgoStaticIPAddress($deviceTags){
	$AlgoStaticIPAddress= "";
	foreach ($deviceTags["Success"] as $key => $value){
		if(isset($value[0])){
			if($value[0] == "%ALGO.NET.IP%"){
				$AlgoStaticIPAddress= $value[1];
			}
			
		}
	}
	return $AlgoStaticIPAddress;
}

function networkSubnetMask($deviceTags){
	$networkSubnetMask= "";
	foreach ($deviceTags["Success"] as $key => $value){
		if(isset($value[0])){
			if($value[0] == "%ALGO.NET.MASK%"){
				$networkSubnetMask= $value[1];
			}
			
		}
	}
	return $networkSubnetMask;
}

function vlanId($deviceTags){
	$vlanId= "";
	foreach ($deviceTags["Success"] as $key => $value){
		if(isset($value[0])){
			if($value[0] == "%ALGO.VLAN.ID%"){
				$vlanId= $value[1];
			}
			
		}
	}
	return $vlanId;
}

?>