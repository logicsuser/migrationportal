<?php 
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
$vdm = new VdmOperations();

$changeString = "";
$changeStringReset = "";

if(!array_key_exists("rebuildPhoneFiles", $_POST)){
	$_POST['rebuildPhoneFiles'] = false;
}

if(!array_key_exists("resetPhone", $_POST)){
	$_POST['resetPhone'] = false;
}

foreach($_POST as $key=>$val){
	if($key != "customTags"){
		$deviceArray['details'][$key] = $_POST[$key];
	}else{
		if($val <> "Error" || $val <> "Success"){
			$deviceArray[$key] = $_POST[$key];
		}
	}
}

//remove the single quotes from key coming from post
foreach($deviceArray['customTags'] as $key=>$val){
	$keyc = str_replace("'", "", $key); 
	$tagsArray['customTags'][$keyc] = $val;
}

$deviceDetailsArray = array(
		"deviceType" => "Device Type",
		"macAddress" => "Mac Address",
		"deviceName" => "Device Name",
		"ipSettings" => "IP Settings",
		"automaticGainControl" => "Automatic Gain Control",
);

$deviceTagsArray = array(
		"%ALGOPAGEVOL%" => "Page Volume",
		"%ALGORINGVOL%" => "Mic Volume",
		"%ALGO.DNS1%" => "DNS Server 1",
		"%ALGO.DNS2%" => "DNS Server 2",
		"%ALGO.NET.GATEWAY%" => "IP Gateway Address (router)",
		"%ALGO.NET.IP%" => "Algo static IP address",
		"%ALGO.NET.MASK%" => "Network Subnet Mask",
		"%ALGO.VLAN.ID%" => "VLAN ID",
		"%ALGO8028-OPEN%" => "Door Open Code",
		"%ALGO.AGC%" => "Advanced Gain Control",
		"%ALGO.DHCP.USE%" => "IP Settings",
		"%ALGO8028-SPDIAL1%" => "Buzzer Dial",
		"%ALGO8028-OPEN_USER%" => "Door Open Code SIP User"
);

$_SESSION['deviceUpdatedDoor'] = false;
$_SESSION['customTagsUpdatedDoor'] = false;
//newcode starts
$error = "";
$backgroundColor = 'background:ac5f5d;'; $errorMessage = '';

if(isset($deviceArray['details']['deviceName']) && !empty($deviceArray['details']['deviceName'])){
	
	foreach($deviceArray['details'] as $key=>$val){
		foreach($_SESSION['device']['details'] as $key1=>$val1){
			$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
			if($key == $key1){
				$val = trim($val);
				if($val <> $val1){
					if (array_key_exists($key, $deviceDetailsArray)) {
						$_SESSION['deviceUpdatedDoor'] = true;
						$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$deviceDetailsArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
					}
				}
			}
		}
	}
	
	foreach($tagsArray['customTags'] as $key3=>$val3){ 
		foreach($_SESSION['device']['customTags'] as $key4=>$val4){
			$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
			if($key3 == $key4){ 
				$val3 = trim($val3);
				if($val4[0] <> $val3){ 
					if (array_key_exists($key3, $deviceTagsArray)) {
						$_SESSION['customTagsUpdatedDoor'] = true;
						
						$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$deviceTagsArray[$key3].'</td><td class="errorTableRows">'.$val3.' '. $errorMessage.' </td></tr>';
					}
				}
			}
		}
	}
	
	if ($_SESSION['customTagsUpdatedDoor']){
		
		if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild and reset after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == false){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == false && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be reset after successful modification</b></td></tr>';
		}
	}else{
		//$changeStringReset = "";
		if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild and reset after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == false){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == false && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be reset after successful modification</b></td></tr>';
		}
	}
	
	
	if($_SESSION['deviceUpdatedDoor'] == false && $_SESSION['customTagsUpdatedDoor'] == false && $_POST['rebuildPhoneFiles'] == false && $_POST['resetPhone'] == false){
		$changeString .= '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'">Device Update</td><td class="errorTableRows">No Changes </td></tr>';
	}
}

//newcode ends
echo $error.$changeStringReset.$changeString;

?>