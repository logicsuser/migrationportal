<?php 

$result = array();
$vdm = new VdmOperations();

$port = 5;

//order ascending
function compareByName($a, $b) {
	return strcmp($a["order"], $b["order"]);
}

$defaultTags = array(
		"%ALGOPAGEVOL%" => array("0"=>""),
		"%ALGO.DNS1%" => array("0"=>""),
		"%ALGO.DNS2%" => array("0"=>""),
		"%ALGO.NET.GATEWAY%" => array("0"=>""),
		"%ALGO.NET.IP%" => array("0"=>""),
		"%ALGO.NET.MASK%" => array("0"=>""),
		"%ALGO.VLAN.ID%" => array("0"=>""),
		"%ALGO.DHCP.USE%" => array("0"=>"1"),
		"%ALGO.AGC%" => array("0"=>"0"),
);

//get user array using port
function getUserInfoUsingPort($userArray, $port){	
	$userArrayByPort = array_values(array_filter($userArray, function($u) use ($port){
		return $u['order'] == $port;
	}));
	return $userArrayByPort;
}

//getDeviceDetails function
$getDeviceDetails = $vdm->getUserDeviceDetail($sp, $groupId, $deviceName);
if(empty($getDeviceDetails['Error'])){
	
	//put deviceDetails in session
	$deviceDetails = $_SESSION['device']['details'] = $getDeviceDetails['Success'];
	
	$getCustomTags = $vdm->getDeviceCustomTags($sp, $groupId, $deviceName);
	$_SESSION['device']['deviceTags'] = $getCustomTags["Success"];
	
	if(empty($getCustomTags['Error'])){
		if(count($getCustomTags['Success']) > 0){
			$_SESSION['device']['customTags'] = array_merge($defaultTags, $getCustomTags['Success']);
		}else{
			$_SESSION['device']['customTags'] = $defaultTags;
		}
	}
	$customTags = $_SESSION['device']['customTags'];
	
	$getUserDevice = $vdm->getAllUserOfDevice($sp, $groupId, $deviceName);
	if(empty($getUserDevice['Error'])){
		if(count($getUserDevice['Success']) > 0){
			//sort user array by order field
			usort($getUserDevice['Success'], 'compareByName');
			
		}
	}
}
?>