<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
$vdm = new VdmOperations();

$changeString = "";
$changeStringReset = "";
$_SESSION['announcementUpdate'] = false;

if(!array_key_exists("rebuildPhoneFiles", $_POST)){
	$_POST['rebuildPhoneFiles'] = false;
}

if(!array_key_exists("resetPhone", $_POST)){
	$_POST['resetPhone'] = false;
}

foreach($_POST['customTags'] as $key=>$val){
	if(!array_key_exists("'%ALGO.AGC%'", $_POST['customTags'])){
		$_POST['customTags']['%ALGO.AGC%'] = "0";
	}
	if(!array_key_exists("'%ALGO.DHCP.USE%'", $_POST['customTags'])){
		$_POST['customTags']['%ALGO.DHCP.USE%'] = "0";
	}
}

foreach($_POST as $key=>$val){
	if($key != "customTags"){
		$deviceArray['details'][$key] = $_POST[$key];
	}else{
		if($val <> "Error" || $val <> "Success"){
			$deviceArray[$key] = $_POST[$key];
		}
	}
}

//remove the single quotes from key coming from post
foreach($deviceArray['customTags'] as $key=>$val){
	$keyc = str_replace("'", "", $key); 
	$tagsArray['customTags'][$keyc] = $val;
}

//echo "<pre>SESSION"; print_r($_SESSION['device']);
//echo "<pre>POST"; print_r($deviceArray); die;

$deviceDetailsArray = array(
		"deviceType" => "Device Type",
		"macAddress" => "Mac Address",
		"deviceName" => "Device Name",
		"ipSettings" => "IP Settings",
		"automaticGainControl" => "Automatic Gain Control",
);

$deviceTagsArray = array(
		"%ALGOPAGEVOL%" => "Strobe Pattern",
		"%ALGO.DNS1%" => "DNS Server 1",
		"%ALGO.DNS2%" => "DNS Server 2",
		"%ALGO.NET.GATEWAY%" => "IP Gateway Address (router)",
		"%ALGO.NET.IP%" => "Algo static IP address",
		"%ALGO.NET.MASK%" => "Network Subnet Mask",
		"%ALGO.VLAN.ID%" => "VLAN ID",
// 		"%ALGOMCASTMODE%" => "Automatic Gain Control",
		"%ALGO.DHCP.USE%" => "IP Settings",
        "%ALGO.AGC%" => "Automatic Gain Control"
    
);

$strobePattern = array(
		"0" => "Rotate Fast",
		"1" => "Rotate Slow",
		"2" => "Multi-Strobe and Dim holdover",
		"3" => "Multi-Strobe fast dimmed",
		"4" => "Multi-Strobe fast bright",
		"5" => "Multi-strobe slow dimmed",
		"6" => "Multi Strobe slow bright",
		"7" => "Rotating Strobe",
		"8" => "Steady Dim",
		"9" => "Steady Bright",
		"10" => "Pulse",
		"11" => "Side to Side",
		"12" => "Flashing",
		"13" => "Classic strobe fast",
		"14" => "Classic strobe medium",
		"15" => "Classic strobe slow"
);

$_SESSION['deviceUpdated'] = false;
$_SESSION['customTagsUpdated'] = false;

//newcode starts
$error = "";
$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';

if(isset($deviceArray['details']['deviceName']) && !empty($deviceArray['details']['deviceName'])){
	
	foreach($deviceArray['details'] as $key=>$val){
		foreach($_SESSION['device']['details'] as $key1=>$val1){
			$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
			if($key == $key1){
				$val = trim($val);
				if($val <> $val1){
					if (array_key_exists($key, $deviceDetailsArray)) {
						$_SESSION['deviceUpdated'] = true;
						$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$deviceDetailsArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
					}
				}
			}
		}
	}	
	foreach($tagsArray['customTags'] as $key3=>$val3){ 
		foreach($_SESSION['device']['customTags'] as $key4=>$val4){
			$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
			if($key3 == $key4){ 
				$val3 = trim($val3);
				if($val4[0] <> $val3){ 
					if (array_key_exists($key3, $deviceTagsArray)) {
						$_SESSION['customTagsUpdated'] = true;
						
						if($key3 == "%ALGOMCASTMODE%"){
							if($val3 == "1"){$val3 = "On";}else{$val3 = "Off";}
						}
						if($key3 == "%ALGO.DHCP.USE%"){
							if($val3 == "1"){$val3 = "DHCP";}else{$val3 = "Static";}
						}
						if($val3 <> "" && $key3 == "%ALGOPAGEVOL%"){
							$val3 = $strobePattern[$val3];
						}
						if($key3 == "%ALGO.DNS1%"){
							if(!empty($val3)){
								$dns1Valid = $vdm->checkIpValidation($val3);
								if($dns1Valid == 0){$error = "Error"; $errorMessage = " (Not a valid IP Address)";$backgroundColor = 'background:#ac5f5d;';}
							}
						}
						if($key3 == "%ALGO.DNS2%"){
							if(!empty($val3)){
								$dns2Valid = $vdm->checkIpValidation($val3);
								if($dns2Valid == 0){$error = "Error"; $errorMessage = " (Not a valid IP Address)";$backgroundColor = 'background:#ac5f5d;';}
							}
						}
						if($key3 == "%ALGO.NET.GATEWAY%"){
							if(!empty($val3)){
								$gateValid = $vdm->checkIpValidation($val3);
								if($gateValid == 0){$error = "Error"; $errorMessage = " (Not a valid IP Address)";$backgroundColor = 'background:#ac5f5d;';}
							}
						}
						if($key3 == "%ALGO.NET.IP%"){
							if(!empty($val3)){
								$netipValid = $vdm->checkIpValidation($val3);
								if($netipValid== 0){$error = "Error"; $errorMessage = " (Not a valid IP Address)";$backgroundColor = 'background:#ac5f5d;';}
							}
						}
						if($key3 == "%ALGO.NET.MASK%"){
							if(!empty($val3)){
								$netMaskValid = $vdm->checkIpValidation($val3);
								if($netMaskValid == 0){$error = "Error"; $errorMessage = " (Not a valid IP Address)";$backgroundColor = 'background:#ac5f5d;';}
							}
						}
						if($key3 == "%ALGO.VLAN.ID%"){
							if(!is_numeric($val3)){$error = "Error"; $errorMessage = " (Not a valid VLAN ID)";$backgroundColor = 'background:#ac5f5d;';}
							
						}
						if($val3 == ""){$val3 = "None";}
						$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$deviceTagsArray[$key3].'</td><td class="errorTableRows">'.$val3.' '. $errorMessage.' </td></tr>';
					}
				}
			}
		}
	}
	
	
	if ($_SESSION['customTagsUpdated']){
		
		if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild and reset after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == false){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == false && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be reset after successful modification</b></td></tr>';
		}
	}else{
		//$changeStringReset = "";
		if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild and reset after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == false){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == false && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be reset after successful modification</b></td></tr>';
		}
	}
	
	if($_SESSION['deviceUpdated'] == false && $_SESSION['customTagsUpdated'] == false && $_POST['rebuildPhoneFiles'] == false && $_POST['resetPhone'] == false){
		$changeString = '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'">Device Update</td><td class="errorTableRows">No Changes </td></tr>';
	}
}

//newcode ends
echo $error.$changeStringReset.$changeString;

?>