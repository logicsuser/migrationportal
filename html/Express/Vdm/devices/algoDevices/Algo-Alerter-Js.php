<script>
$(function()
{
// 	warning message
// 	var formDataUpdated = "false";
// 	$("#deviceName").autocomplete({
// 	    select: function (event , ui) {
// 	    	var formDataUpdated = "false";
// 	    	var dataToSend = $("form#vdmLightFormDoorPhone").serializeArray();
// 	    	if(ui.item.label != $("#tempDeviceName").val()) {
// 	 		$.ajax({
// 	 			type: "POST",
// 	 			url: "Vdm/devices/algoDevices/Algo-Alerter-Validate.php",
// 	 			data: dataToSend,
// 	 			success: function(result) {
// 	 				var noChangesExist = result.search("No Changes");
// 	 				if(noChangesExist == -1 && $("#indexForm").is(":visible")){
// 	 					$("#dialogAlgoAlerterWarningMsg").dialog("open");
// 	 				} else {
// 						refreshOnSearchDevice();
// 		 			}
// 	 			}
// 	 		});
// 	    }
// 	    },
// 	});


// 	$(".vdmDeviceChange").change(function() {
// 			navVdmType = "";
			//var $vdmType = "<?php //echo $vdmType; ?>";
// 			if($vdmType == "algoAlerter")
// 			{
//     	    	var dataToSend = $("form#vdmLightFormDoorPhone").serializeArray();
//     	 		$.ajax({
//     	 			type: "POST",
//     	 			url: "Vdm/devices/algoDevices/Algo-Alerter-Validate.php",
//     	 			data: dataToSend,
//     	 			success: function(result) {
//     	 				var noChangesExist = result.search("No Changes");
//     	 				if(noChangesExist == -1 && $("#indexForm").is(":visible")){
//     		 				formDataUpdated = "true";
//     	 					$("#dialogAlgoAlerterWarningMsg").dialog("open");
//     	 					} else {
//     		 				$("#isGetClick").val("false");
//     						loadDeviceName();
//     			 		}
//     	 			}
//     	 		});
// 			}
// 	});
	
// 	$("#dialogAlgoAlerterWarningMsg").dialog({
// 		autoOpen: false,
// 		width: 800,
// 		modal: true,
// 		position: { my: "top", at: "top" },
// 		resizable: false,
// 		closeOnEscape: false,
// 		buttons: {
// 			"Discard Changes and Continue": function() {
// 				if(formDataUpdated == "true") {
// 					$("#isGetClick").val("false");
// 					loadDeviceName();
// 				} else {
// 					$("#deviceDetails").html("");
// 					$("#indexForm").hide();
// 					var bck = jQuery.Event("keydown", { keyCode: 20 });
// 					$("#deviceName").trigger( bck );
// 				}
// 				$(this).dialog("close");
// 			},
// 			"Cancel": function() {
// 				$(this).dialog("close");
// 				$("#loading2").hide();
// 				$("#vdmLightForm").show();
				
// 				$("#vdmType select").val($("#tempDeviceType").val());
// 				$("#deviceName select").val($("#deviceName").val());				
// 			},
// 		},
// 		open: function() {
// 			$("#dialogAlgoAlerterWarningMsg").dialog("option", "title", "Warning Message");
// 			$("#dialogAlgoAlerterWarningMsg").html("<b>Attempted search for another device will discard changes made on this page. </b><br>");
// 			$("#dialogAlgoAlerterWarningMsg").html("<b>Please click on 'Discard Changes and Continue' to proceed with a search without saving changes. Press 'Cancel' to stay on the page</b>");
//     }
// 	});

// end	warning message

	$("#AlgoAdvancedOption").hide();

	$("#multiCastCodeMaster").click(function() {
		if($(this).is(':checked')){
			$("#multiCastCodeMasterValue").val("1");
		}else{
			$("#multiCastCodeMasterValue").val("0");
		}
	});

	$("#polycomGroupPaging").click(function() {
		if($(this).is(':checked')){
			$("#polycomGroupPagingValue").val("1");
		}else{
			$("#polycomGroupPagingValue").val("0");
		}
	});

	$("#automaticGainControl").click(function() {
		if($(this).is(':checked')){
			$("#automaticGainControlValue").val("1");
		}else{
			$("#automaticGainControlValue").val("0");
		}
	});

	$("#ipSettings").click(function() {
		if($(this).is(':checked')){
			$("#ipSettingsValue").val("1");
		}else{
			$("#ipSettingsValue").val("0");
		}
	});

		$(".primaryUserModifyLink").click(function()
				{
				$("#vdmMainPageReplace").html("<div class='col span_11 leftSpace' id='loader' style='margin-left:400px;margin-top:10%'> <img src='/Express/images/ajax-loader.gif'></div>");
				var userId = $(this).attr("id");
				var fname = $(this).attr("data-firstname");
				var lname = $(this).attr("data-lastname");

				var extension = $(this).attr("data-extension");
				var phone = $(this).attr("data-phone");
				if($.trim(phone) != ""){
					userIdValue1 = phone+"x"+extension;
				}else{
					userIdValue1 = userId;
				}
				var userIdValue = userIdValue1+" - "+lname+", "+fname;
				
				$.ajax({
						type: "POST",
						url: "userMod/userMod.php",
						data: { searchVal: userId, userFname : fname, userLname : lname },
						success: function(result)
						{
							$("#mainBody").html(result);
							$("#searchVal").val(userIdValue);
							setTimeout(function() {
								$("#go").trigger("click");
							}, 2000);
							//$(".subBanner").html("User Modify");
							$(".navMenu").removeClass("active");
							$("#userMod").addClass("active");
							$('#helpUrl').attr('data-module', "userMod");
							activeImageSwap();
						}

					});
	});
	var activeImageSwap = function(){
		 previousActiveMenu	= "modVdm";
		 currentClickedDiv = "userMod";       
	  
			//active
	  		if(previousActiveMenu != "")
			{ 
	  			$("#modVdm").removeClass("activeNav");
	  		 	$(".navMenu").removeClass("activeNav");
				var $thisPrev = $("#modVdm").find('.ImgHoverIcon');
		        var newSource = $thisPrev.data('alt-src');
		        $thisPrev.data('alt-src', $thisPrev.attr('src'));
		        $thisPrev.attr('src', newSource);
		       
			}
			// inactive tab
			if(currentClickedDiv != ""){
				$("#userMod").addClass("activeNav");
				var $thisPrev = $("#userMod").find('.ImgHoverIcon');
		        var newSource = $thisPrev.data('alt-src');
		        $thisPrev.data('alt-src', $thisPrev.attr('src'));
		        $thisPrev.attr('src', newSource);
				 previousActiveMenu = currentClickedDiv;
				
	    	}
	   
	}
	
});
$("#advancedOption").click(function(){
	var buttonData = $("#advancedOption").val();
	if(buttonData == "Show Advanced Options"){
		$("#AlgoAdvancedOption").show();
		$("#advancedOption").val("Hide Advanced Options");
	}else if(buttonData == "Hide Advanced Options"){
		$("#AlgoAdvancedOption").hide();
		$("#advancedOption").val("Show Advanced Options");
	}
    
    
}); 

$("#subButtonCancel").click(function(){
	var autoComplete = [];
	   $("#deviceName").autocomplete({
      source: autoComplete
  });
	$('#vdmType').val("");
	//$('#deviceName').html("");
	$('#deviceName').val("");
	$('#vdmLightDisplayContent').hide();
	$('html, body').animate({scrollTop: '0px'}, 300);
	
});

//Algo Alerter Validation
var validateAlgoAlerterDevice = function(){
	var dataToSend = $("form#vdmLightFormAlerter").serializeArray();
	$.ajax({
		type: "POST",
		url: "Vdm/devices/algoDevices/Algo-Alerter-Validate.php",
		data: dataToSend,
		success: function(result) {
			var noChangesExist = result.search("No Changes");
			
			if(noChangesExist > 0){
				$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
			}else{
				$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
			}
			$("#dialogAlgoAlerter").dialog("option", "title", "Request Complete");
			$("#dialogAlgoAlerter").html('<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelistAlerter" class="confSettingTable"><tbody><tr><th colspan="2" align="center">Please confirm the settings below and click Complete to process your modifications.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');		    	
			$("#changelistAlerter").append(result);
			$("#dialogAlgoAlerter").append('</tbody></table>');
			$("#dialogAlgoAlerter").dialog("open");
			buttonsShowHide('Complete', 'show');
			buttonsShowHide('Cancel', 'show');
			buttonsShowHide('More Changes', 'hide');
			buttonsShowHide('Ok', 'hide');
		}
	});
};

var modifyAlerterDevice = function(){
	
	var dataToSend = $("form#vdmLightFormAlerter").serializeArray();
	$("#dialogAlgoAlerter").html('Submitting the request....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
	pendingProcess.push("Modify Device Management");
	$.ajax({
		url: 'Vdm/devices/algoDevices/Algo-Alerter-Update.php',
        type: 'POST',
        data: dataToSend,
        success: function(data)
        {
        	if(foundServerConErrorOnProcess(data, "Modify Device Management")) {
				return false;
          	}
        	var isErrorExist = data.search("background-color: #ac5f5d");
            if(isErrorExist > 0){
				buttonsShowHide('Cancel', 'show');
            }else{
            	buttonsShowHide('Cancel', 'hide');
             }
            
            $("#dialogAlgoAlerter").html('');
            $("#dialogAlgoAlerter").dialog("option", "title", "Request Complete");
			$("#dialogAlgoAlerter").html('<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelistAlerter" class="confSettingTable"><tbody><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');		    	
			$("#changelistAlerter").append(data);
			$("#dialogAlgoAlerter").append('</tbody></table>');
			$("#dialogAlgoAlerter").dialog("open");
			buttonsShowHide('More Changes', 'show');
			buttonsShowHide('Return to Main', 'show');
			buttonsShowHide('Complete', 'hide');
// 			buttonsShowHide('Cancel', 'hide');
			$(":button:contains('Complete')").removeAttr("disabled", "disabled").removeClass("ui-state-disabled");
        }
        
	});
};

$("#subButtonAlerter").click(function(){
	validateAlgoAlerterDevice();
});

$("#dialogAlgoAlerter").dialog({
	autoOpen: false,
	width: 800,
	modal: true,
	position: { my: "top", at: "top" },
	resizable: false,
	closeOnEscape: false,
	buttons: {
		"Complete": function() {
			modifyAlerterDevice();
		},
		"Cancel": function() {
			$(this).dialog("close");
		},
        "More Changes": function() {
        	var deviceType = $("#vdmLightFormAlerter .deviceType").val();
            var deviceName = $("#vdmLightFormAlerter .deviceName").val();
            $(this).dialog("close");
            $('html, body').animate({scrollTop: '0px'}, 300);
            reloadDeviceInfo(deviceType, deviceName);
        },
        "Ok": function() {
        	$(this).dialog("close"); 
			},
		"Return to Main": function() {
			location.href="main.php";
		},
	},
    open: function() {
		setDialogDayNightMode($(this));
    	buttonsShowHide('Complete', 'show');
    	buttonsShowHide('Cancel', 'show');
    	buttonsShowHide('More Changes', 'hide');
    	buttonsShowHide('Return to Main', 'hide');
    	buttonsShowHide('Ok', 'hide');
    	$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('completeButton');
    	$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');

    	$('.ui-dialog-buttonpane').find('button:contains("More Changes")').addClass('moreChangesButton');
    	$('.ui-dialog-buttonpane').find('button:contains("Return to Main")').addClass('returnToMainButton');
    }
});		

var buttonsShowHide = function(b,e){
	if(e == "show"){
		$(".ui-dialog-buttonpane button:contains("+b+")").button().show();
	}else{
	 	$(".ui-dialog-buttonpane button:contains("+b+")").button().hide();
	}
};

var reloadDeviceInfo = function(deviceType, deviceName){
	if(deviceType == "Algo_8301" || deviceType == "Algo_8180"){
		deviceType = "algoAlerter";
	}
	$("#vdmType").val(deviceType);
	$("#deviceNameDiv").val(deviceName);
	$("#subButtonDevice").trigger("click");
};

function feedBackDelay(feedbackValue){
	var enableCreate ="";
	var min = parseInt(feedbackValue.min);
    var max = parseInt(feedbackValue.max);
    var tagVal = parseInt(feedbackValue.value);
    enableCreate = (tagVal >= min && tagVal <= max) ? "enable" : "disable";
    if(enableCreate == "enable"){
    	$("#subButtonAlerter").prop("disabled", false);
    	document.getElementById(feedbackValue.id).style.setProperty('border-color', '#002c60', 'important');
    }else{
    	$("#subButtonAlerter").prop("disabled", "disabled");
    	document.getElementById(feedbackValue.id).style.setProperty('border-color', '#ac5f5d', 'important');
    }
    
}

var ipSettingChange = function(ipSettingVal){
	
	if(ipSettingVal == "1"){
		/*$(".algodns1").val('');
		$(".algodns2").val('');
		$(".algonetgateway").val('');
		$(".algonetip").val('');
		$(".algonetmask").val('');*/
		$(".algodns1").prop('readonly', true);
		$(".algodns2").prop('readonly', true);
		$(".algonetgateway").prop('readonly', true);
		$(".algonetip").prop('readonly', true);
		$(".algonetmask").prop('readonly', true);
	}else{
		$(".algodns1").attr('readonly', false);
		$(".algodns2").attr('readonly', false);
		$(".algonetgateway").attr('readonly', false);
		$(".algonetip").attr('readonly', false);
		$(".algonetmask").attr('readonly', false);

	}
}

$("#macAddress").on("input", function()
{
	var macAddress = $("#macAddress").val();
	var macToLowerCase = macAddress.toLowerCase();
	var lastElmt = macToLowerCase.substr(-1);
	if((lastElmt >= "0" && lastElmt <= "9") || (lastElmt >= "a" && lastElmt <= "f")){
		//do something
    }else{
   		$("#macAddress").val(macAddress.slice(0,-1));
    }
    
			   var macAddress = $("#macAddress").val();
			   if(macAddress.length > 0 && macAddress.length < 12){
				   $("#macAddress").attr('style', 'border-color :#ac5f5d !important');
				   $("#subButtonAlerter").prop("disabled", true);
			   }else{
				   $("#macAddress").attr('style', 'border-color :#002c60 !important');
				   $("#subButtonAlerter").prop("disabled", false);	
				}
		});


$(".ipSettingClass").change(function(){
	var el = $(this);
	var ipSettingVal = el.val();
	ipSettingChange(ipSettingVal);
});

$(".ipSettingClass").trigger('change');

</script>

<style type="text/css">
	select:disabled {
    	border-color: #d9d9d9 !important;
	}
	/*#AlgoAdvancedOption{
		border: 2px solid rgb(110, 160, 220);
		border-radius: 4px;
		padding: 16px;
		margin-top: -60px;
	}*/
	.lebelPadding {
    	width: 170px;
	}
	.subButtonAlerter{
	background-color: #72ac5d !important;
    color: #FFF;
    width: 226px;
    height: 40px;
    border-radius: 4px;
    border: none;
}
	
</style>