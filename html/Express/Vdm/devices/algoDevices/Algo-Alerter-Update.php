<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");

include("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");

$cLUObj = new ChangeLogUtility($_POST['deviceName'], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
$vdm = new VdmOperations();

server_fail_over_debuggin_testing(); /* for fail Over testing. */

function getModifiedArray($postArray, $sessionArray){
	$allChangedValArray = array();
	foreach($postArray as $key=>$val){
		if($key != "customTags"){
			$deviceArray['details'][$key] = $_POST[$key];
		}else{
			if($val <> "Error" || $val <> "Success"){
				$deviceArray[$key] = $_POST[$key];
			}
		}
	}
	
	foreach($deviceArray['customTags'] as $key=>$val){
		$keyc = str_replace("'", "", $key);
		$tagsArray['customTags'][$keyc] = $val;
	}
	
	if(count($tagsArray['customTags']) > 0){
		foreach($tagsArray['customTags'] as $key3=>$val3){
			foreach($sessionArray as $key4=>$val4){
				if($key3 == $key4){
					$val3 = trim($val3);
					if($val4[0] <> $val3){
						$allChangedValArray[$key3] = $val3;
					}
				}
			}
		}
	}
	return $allChangedValArray;
}



$deviceArray = array();
foreach($_POST as $key=>$val){
	if($key != "customTags"){
		$deviceArray[$key] = $_POST[$key];
	}else{
		$tagsArray[$key] = $_POST[$key];
	}
}


$deviceArray['spId'] = $sp = $_SESSION['sp'];
$groupId = $deviceArray['groupId'] = $_SESSION['groupId'];
$deviceName = $_POST['deviceName'];
$modifyDevices['devices'] = array();

$modifyTag['add'] = array();
$modifyTag['mod'] = array();
$modifyTag['del'] = array();

$changeStringAlerter = "";
$deviceName = $_POST['deviceName'];
$deviceDetail = array("macAddress" => "Mac Address");
$deviceTagsArray = array(
		"%ALGOPAGEVOL%" => "Page Volume",
		"%ALGORINGVOL%" => "Ring Volume",
		"%ALGORINGTONE%" => "Ring Tone",
		"%ALGOPAGETONE%" => "Page Tone",
		"%ALGORINGTONE_2%" => "Ring Tone 2",
		"%ALGORINGTONE_3%" => "Ring Tone 3",
		"%ALGORINGTONE_4%" => "Ring Tone 4",
		"%ALGORINGTONE_5%" => "Ring Tone 5",
		"%ALGOMCASTMODE%" => "Multicast Mode",
		"%ALGO.POLYCOM.MODE%" => "Polycom Group Paging/PPT",
		"%ALGO.AGC%" => "Advanced Gain Control",
		"%ALGO.JC.DELAY%" => "Feedback Delay",
		"%ALGO.DNS1%" => "DNS Server 1",
		"%ALGO.DNS2%" => "DNS Server 2",
		"%ALGO.NET.GATEWAY%" => "IP Gateway Address(router)",
		"%ALGO.NET.IP%" => "Algo Static IP address",
		"%ALGO.NET.MASK%" => "Network Subnet Mask",
		"%ALGO.VLAN.ID%" => "VLAN ID",
		"%ALGO.DHCP.USE%" => "IP Settings"
);

if(!array_key_exists("rebuildPhoneFiles", $_POST)){
	$_POST['rebuildPhoneFiles'] = "false";
}

if(!array_key_exists("resetPhone", $_POST)){
	$_POST['resetPhone'] = "false";
}

$getModifiedArr = getModifiedArray($_POST, $_SESSION['device']['customTags']);
$arrayToAdd = array();
$arrayToMod = array();
$arrayToDel = array();
if(count($getModifiedArr) > 0){
	foreach ($getModifiedArr as $key => $value){
		if(array_key_exists($key, $_SESSION['device']['deviceTags'])){
			if($value != "0" && empty($value)) {
				$arrayToDel[$key][] = $value;
			} else {
				$arrayToMod[$key][] = $value;
			}
		}else{
			$arrayToAdd[$key][] = $value;
		}
	}
	
}

$modifyDevices['devices']= $vdm->modifyUserDeviceDetail($deviceArray);
if(empty($modifyDevices['devices']['Error'])){
	if(count($arrayToAdd) > 0){
		$sp = $_SESSION['sp'];
		$groupId = $_SESSION['groupId'];
		
		foreach ($arrayToAdd as $key => $value){
			$modifyTag['add'][$key] = $vdm->addDeviceCustomTag($sp, $groupId, $deviceName, $key, $value[0]);
		}
	}
	
	if(count($arrayToMod) > 0){
		
		foreach ($arrayToMod as $tagName => $tagValue){
			$tagUpdateArray['tagName'] = $tagName;
			$tagUpdateArray['tagValue'] = $tagValue[0];
			$tagUpdateArray['spId'] = $_SESSION['sp'];
			$tagUpdateArray['groupId'] = $_SESSION['groupId'];
			$tagUpdateArray['deviceName'] = $deviceName;
			$modifyTag['mod'][$tagName] = $vdm->modfiyCustomTags($tagUpdateArray);
			
		}
	}
	
	if(count($arrayToDel) > 0){
		
		foreach ($arrayToDel as $key => $value){
			$modifyTag['del'][$key] = $vdm->deleteDeviceCustomTag($sp, $groupId, $deviceName, $key);
		}
	}
}

if( !empty($modifyDevices['devices']['Error']) ) {
    $changeStringAlerter .= '<tr><td class="errorTableRows" style="background-color: #ac5f5d">Device Not Updated</td><td class="errorTableRows">'.$modifyDevices["devices"]["Error"].' </td></tr>';
}else{
    foreach($deviceDetail as $key => $val){
    	if(isset($deviceArray[$key]) && $_SESSION['device']['details'][$key] <> $deviceArray[$key]){

            $changeStringAlerter .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">'.$deviceDetail[$key].'</td><td class="errorTableRows">Successfully Updated </td></tr>';
            
            $cLUObj->createChangesArray($module = $deviceDetail[$key], $oldValue = $_SESSION['device']['details'][$key], $newValue = $tagsArray["customTags"]["'".$key."'"]);
    	}

    }

}
    

if($_POST['rebuildPhoneFiles'] == true || $_POST['resetPhone'] == true ){
	$changeStringAlerter .= rebuildResetDevice($_POST, $sp, $groupId);
	
	if ($_POST ['rebuildPhoneFiles'] == "true" && $_POST ['resetPhone'] == "true") {
	    $cLUObj->createChangesArray($module = "Rebuild and Reset", $oldValue = "" , $newValue = "Yes");
	} else if ($_POST ['rebuildPhoneFiles'] == "true" && $_POST ['resetPhone'] == "false") {
	    $cLUObj->createChangesArray($module = "Rebuild", $oldValue = "" , $newValue = "Yes");
	} else if ($_POST ['rebuildPhoneFiles'] == "false" && $_POST ['resetPhone'] == "true") {
	    $cLUObj->createChangesArray($module = "Reset", $oldValue = "" , $newValue = "Yes");
	}
}

if(count($modifyTag['mod']) > 0){
	foreach($modifyTag['mod'] as $key=>$val){
		if(empty($val['Error'])){

			$changeStringAlerter .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">'.$deviceTagsArray[$key].'</td><td class="errorTableRows">Successfully Updated </td></tr>';
		    
			$cLUObj->createChangesArray($module = $deviceTagsArray[$key], $oldValue = $_SESSION['device']['customTags'][$key][0], $newValue = $getModifiedArr[$key]);

		}else{
			$changeStringAlerter .= '<tr><td class="errorTableRows" style="background-color: #ac5f5d">'.$deviceTagsArray[$key].'</td><td class="errorTableRows">'.$val['Error'].' </td></tr>';
		}
	}
}

if(count($modifyTag['add']) > 0){
	foreach($modifyTag['add'] as $key=>$val){
		if(empty($val['Error'])){

			$changeStringAlerter .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">'.$deviceTagsArray[$key].'</td><td class="errorTableRows">Successfully Updated </td></tr>';		    
			$cLUObj->createChangesArray($module = $deviceTagsArray[$key], $oldValue = $_SESSION['device']['customTags'][$key][0], $newValue = $getModifiedArr[$key]);						

		}else{
			$changeStringAlerter .= '<tr><td class="errorTableRows" style="background-color: #ac5f5d">'.$deviceTagsArray[$key].'</td><td class="errorTableRows">'.$val['Error'].' </td></tr>';
		}
	}
}

if(count($modifyTag['del']) > 0){
	foreach($modifyTag['del'] as $key=>$val){
		if(empty($val['Error'])){

			$changeStringAlerter .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">'.$deviceTagsArray[$key].'</td><td class="errorTableRows">Successfully Updated </td></tr>';

			$cLUObj->createChangesArray($module = $deviceTagsArray[$key], $oldValue = $_SESSION['device']['customTags'][$key][0], $newValue = $getModifiedArr[$key]);			

		}else{
			$changeStringAlerter .= '<tr><td class="errorTableRows" style="background-color: #ac5f5d">'.$deviceTagsArray[$key].'</td><td class="errorTableRows">'.$val['Error'].' </td></tr>';
		}
	}
}

/* Logger */
$module = "Device Management Modification";
$cLUObj->changeLogModifyUtility($module, $_POST['deviceName'], $tableName = "deviceMgmtModChanges");


echo $changeStringAlerter;
?>