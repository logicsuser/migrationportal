<?php
require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin ();
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
$vdm = new VdmOperations ();
$ringerName = array (
    "AlgoPageWave2New.wav",
    "AlgoPageWaveNew.wav",
    "bell-na.wav",
    "bell-uk.wav",
    "buzzer.wav",
    "chime.wav",
    "dogs.wav",
    "gong.wav",
    "page-chime.wav",
    "page-notif.wav",
    "warble1-low.wav",
    "warble2-med.wav",
    "warble3-high.wav",
    "warble4-trill.wav"
);

$defaultTags = array (
		"%ALGOPAGEVOL%" => Array(0 => ""),
		"%ALGORINGVOL%" => Array(0 => ""),
		"%ALGORINGTONE%" => Array(0 => ""),
		"%ALGOPAGETONE%" => Array(0 => ""),
		"%ALGORINGTONE_2%" => Array(0 => ""),
		"%ALGORINGTONE_3%" => Array(0 => ""),
		"%ALGORINGTONE_4%" => Array(0 => ""),
		"%ALGORINGTONE_5%" => Array(0 => ""),
		"%ALGOMCASTMODE%" => Array(0 => "0"),
		"%ALGO.POLYCOM.MODE%" => Array(0 => "0"),
		"%ALGO.AGC%" => Array(0 => "0"),
		"%ALGO.JC.DELAY%" => Array(0 => ""),
		"%ALGO.DNS1%" => Array(0 => ""),
		"%ALGO.DNS2%" => Array(0 => ""),
		"%ALGO.NET.GATEWAY%" => Array(0 => ""),
		"%ALGO.NET.IP%" => Array(0 => ""),
		"%ALGO.NET.MASK%" => Array(0 => ""),
		"%ALGO.VLAN.ID%" => Array(0 => ""),
		"%ALGO.DHCP.USE%" => Array(0 => "1")
);

// check if tag exist for algo 8028

function getUserDetail($userArray, $index, $vdm) {
    $html = "";
    if (array_key_exists ( $index, $userArray )) {
        $userData = $userArray [$index];
        
        if (! empty ( $userData ['phoneNumber'] )) {
            $userInfo = $userData ['phoneNumber'];
        } else {
            $userInfo = $userData ['extn'];
        }
        
        $userId = isset ( $userData ['userId'] ) ? $userData ['userId'] : "";
        $firstName = isset ( $userData ['firstName'] ) ? $userData ['firstName'] : "";
        $lastName = isset ( $userData ['lastName'] ) ? $userData ['lastName'] : "";
        
        $checkStatus = $vdm->getUserStatus ( $userData ['userId'] );
        if ($checkStatus == "Registered") {
            $color = "#72ac5d";
        } else {
            $color = "#ac5f5d";
        }
        // $html = '<span style="color:'. $color.'">'.$userInfo.'</span>';
        $html = "<a style='color:" . $color . "' href='#' class='primaryUserModifyLink'
                    id='" . $userId . "' data-phone='" . $userData ['phoneNumber'] . "' data-extension='" . $userData ['extn'] . "' data-lastname='" . $lastName . "' data-firstname='" . $firstName . "' >" . $userInfo . "</a>";
    }
    return $html;
}

$result = array ();

$getDeviceDetails = $vdm->getUserDeviceDetail ( $sp, $groupId, $deviceName );

if (empty ( $getDeviceDetails ['Error'] )) {
	$getCustomTags = $vdm->getDeviceCustomTags ( $sp, $groupId, $deviceName );
	$_SESSION['device']['deviceTags'] = $getCustomTags['Success'];
	$deviceCustomArray = array();
	$mergedArray = array_merge($defaultTags, $getCustomTags['Success']);
	
    $deviceDetails = $_SESSION ["device"] ["details"] = $getDeviceDetails ['Success'];
    	$customTags = $mergedArray;
        $_SESSION ['device']['customTags'] = $customTags;
    $getUserDetails = $vdm->getAllUserOfDevice ( $sp, $groupId, $deviceName );
}

$micVol = "10";
$pagVol = "10";
?>
<style>
.leftSpace {margin-left: 20px;}
td.thPolyD .dropdown-wrap {
    width: 100% !important;
}
td.thPolyD .dropdown-wrap:before {
    top: 0px !important;
}
</style>
<div class="selectContainer" style="padding-top: 0% !important;">


	<form name="vdmLightForm" id="vdmLightFormAlerter" method="POST"
		autocomplete="off">
            <div class="formPadding vdmForm" id="indexForm">
		<div id="vdmLightDisplayContent">
			
		<h2 class="addUserText"><?php if(isset($deviceDetails['deviceType'])){ echo $deviceDetails['deviceType'];}?></h2>
			<div class="row" style="">
				<div class="col-md-6">
				<div class="form-group">
					<label class="labelText">Model:</label>
					<input style="font-size: 15px;" type="text" name="deviceType"class="inputColorReadOnly inputColor deviceType" readonly id="deviceType" size="50" value="<?php if(isset($deviceDetails['deviceType'])){ echo $deviceDetails['deviceType'];}?>">
				</div>
				</div>
				<div class="col-md-6">
				<div class="form-group">
					<label class="labelText">Mac Address:</label>
					<input style="font-size: 15px;" type="text" name="macAddress"
							class="inputColorReadOnly inputColor"
							id="macAddress" size="50" maxlength="12"
							value="<?php if(isset($deviceDetails['macAddress'])){ echo $deviceDetails['macAddress'];}?>">
				
				</div>
				</div>
			</div>


			<div class="row" style="">
				<div class="col-md-6">
				<div class="form-group">
					<label class="labelText">Name:</label>
					<input style="font-size: 15px;" type="text" name="deviceName"
						class="inputColorReadOnly inputColor deviceName" readonly
						id="deviceName" size="50"
						value="<?php if(isset($deviceDetails['deviceName'])){ echo $deviceDetails['deviceName'];}?>">
				</div>
				</div>
				
				<div class="col-md-6">
				<div class="form-group">
					<div class="col-md-6" style="padding-left:0;">
						<label class="labelText">Ring Volume:</label>
						<div class="dropdown-wrap">
						<select name="customTags['%ALGORINGVOL%']" id="micValume"
							class="inputBackground" style="width:100% !important;">
							<option value = "">None</option>
						<?php for($i = 0; $i <= $micVol; $i++){?>
								<option value="<?php echo $i;?>"
								<?php
								if (!empty($customTags ['%ALGORINGVOL%'] [0]) && $customTags ['%ALGORINGVOL%'] [0] == $i) {
									echo "selected";
								}
							?>><?php echo $i;?></option>
						<?php }?>
						
					</select>
					</div>
					</div>
					
					
					<div class="col-md-6" style="padding-right:0;">
						<label class="labelText">Page Volume:</label>
						<div class="dropdown-wrap">
						<select name="customTags['%ALGOPAGEVOL%']" id="pageVolume"
							class="inputBackground" style="width:100% !important;">
							<option value = "">None</option>
						<?php for($i = 0; $i <= $pagVol; $i++){?>
								<option value="
								<?php echo $i;?>"
								<?php
								if (!empty ( $customTags ['%ALGOPAGEVOL%'] [0] ) &&  $customTags ['%ALGOPAGEVOL%'] [0] == $i) {
									echo "selected";
								}
							?>><?php echo $i;?></option>
						<?php }?>
						</select>
						</div>
					</div>
				</div>
				</div>
				
			</div>
		</div>
                </div>
			<div class="row">
				<div class="col-md-12">
				<div class="form-group">
					<div class="viewDetailNew">
						<table id="allUsers" class="table tableLineType scroll algoDevicesTableClass"
							style="width: 100%; margin: 0;" border="0">
							<thead>
								<tr class="tableVdmColor">
									<th class="thDoor">Port</th>
									<th class="thDoor">User</th>
									<th class="thDoor">Value</th>
								</tr>
							</thead>

							<tbody id="tabletr" class="vdmTbody">
								<tr>
									<td class="tdDoor">Ringer 1</td>
									<td class="tdDoor"><?php echo $userDetailOnPort = getUserDetail($getUserDetails['Success'], 0, $vdm);?></td>
									<td class="tdDoor">
									<div class="dropdown-wrap">
									<select name="customTags['%ALGORINGTONE%']"
										id="customTags['%ALGORINGTONE%']"
										<?php if($userDetailOnPort == ""){echo "disabled";}?>>
											<option value="">Select Tone</option>	
									<?php
									foreach ( $ringerName as $key => $value ) {
										echo "<option value='" . $value . "'";
										if ($value == $customTags ['%ALGORINGTONE%'] [0] && $userDetailOnPort != "") {
											echo "selected";
										}
										echo " >" . $value . "</option>";
									}
									?>									
									</select>
									</div>
									</td>

								</tr>
								<tr>
									<td class="tdDoor">Pager 1</td>
									<td class="tdDoor"><?php echo $userDetailOnPort = getUserDetail($getUserDetails['Success'], 1, $vdm);?></td>
									<td class="tdDoor">
									<div class="dropdown-wrap">
									<select name="customTags['%ALGOPAGETONE%']"
										id="customTags['%ALGOPAGETONE%']"
										<?php if($userDetailOnPort == ""){echo "disabled";}?>>
											<option value="">Select Tone</option>	
									<?php
									foreach ( $ringerName as $key => $value ) {
										echo "<option value='" . $value . "'";
										if ($value == $customTags ['%ALGOPAGETONE%'] [0] && $userDetailOnPort != "") {
											echo "selected";
										}
										echo " >" . $value . "</option>";
									}
									?>										
								</select>
								</div>
								</td>
								</tr>
								<tr>
									<td class="tdDoor">Ringer 2</td>
									<td class="tdDoor"><?php echo $userDetailOnPort = getUserDetail($getUserDetails['Success'], 2, $vdm);?></td>
									<td class="tdDoor">
									<div class="dropdown-wrap">
									<select name="customTags['%ALGORINGTONE_2%']"
										id="customTags['%ALGORINGTONE_2%']"
										<?php if($userDetailOnPort == ""){echo "disabled";}?>>
											<option value="">Select Tone</option>										
									<?php
									foreach ( $ringerName as $key => $value ) {
										echo "<option value='" . $value . "'";
										if ($customTags ['%ALGORINGTONE_2%'] [0] == $value && $userDetailOnPort != "") {
											echo "selected";
										}
										echo " >" . $value . "</option>";
									}
									?>									
								</select>
								</div>
								</td>
								</tr>

								<tr>
									<td class="tdDoor">Ringer 3</td>
									<td class="tdDoor"><?php echo $userDetailOnPort = getUserDetail($getUserDetails['Success'], 3, $vdm);?></td>
									<td class="tdDoor">
									<div class="dropdown-wrap">
									<select name="customTags['%ALGORINGTONE_3%']"
										id="customTags['%ALGORINGTONE_3%']"
										<?php if($userDetailOnPort == ""){echo "disabled";}?>>
											<option value="">Select Tone</option>										
									<?php
									foreach ( $ringerName as $key => $value ) {
										echo "<option value='" . $value . "'";
										if ($value == $customTags ['%ALGORINGTONE_3%'] [0] && $userDetailOnPort != "") {
											echo "selected";
										}
										echo " >" . $value . "</option>";
									}
									?>									
								</select>
								</div>
								</td>
								</tr>

								<tr>
									<td class="tdDoor">Ringer 4</td>
									<td class="tdDoor"><?php echo $userDetailOnPort = getUserDetail($getUserDetails['Success'], 4, $vdm);?></td>
									<td class="tdDoor">
									<div class="dropdown-wrap">
									<select name="customTags['%ALGORINGTONE_4%']"
										id="customTags['%ALGORINGTONE_4%']"
										<?php if($userDetailOnPort == ""){echo "disabled";}?>>
											<option value="">Select Tone</option>										
									<?php
									foreach ( $ringerName as $key => $value ) {
										echo "<option value='" . $value . "'";
										if ($value == $customTags ['%ALGORINGTONE_4%'] [0] && $userDetailOnPort != "") {
											echo "selected";
										}
										echo " >" . $value . "</option>";
									}
									?>									
								</select>
								</div>
								</td>
								</tr>

							</tbody>
						</table>

					</div>
				</div>
				</div>
			
		</div>
			
                        <div class="formPadding vdmForm" id="indexForm">
			<?php if($rebuildResetDevice == "true"){?>
				<div class="row">
					<div class="col-md-12">
					<div class="form-group">
						<label class="labelText labelAboveInput" for="">Rebuild and Reset Device :</label>
					</div>
					</div>
					<div class="">
						<div class="col-md-3" style="padding:0">
						<div class="form-group">
							<input type="checkbox" name="rebuildPhoneFiles"
								id="rebuildPhoneFiles" value="true"> <label class="labelText"
								for="rebuildPhoneFiles"><span></span>Rebuild Phone Files</label>
						</div>
						</div>
						<div class="col-md-3" style="padding:0">
						<div class="form-group">
							<input type="checkbox" name="resetPhone" id="resetPhone"
								value="true"> <label class="labelText" for="resetPhone"><span></span>Reset
								the Phone</label>
						</div>
						</div>
						<div class="col-md-6">
						<div class="form-group"></div>
						</div>
					</div>
				</div>
				<?php }else{?>
					<input type="hidden" name="rebuildPhoneFiles"
					id="rebuildPhoneFiles" value="true" /> <input type="hidden"
					name="resetPhone" id="resetPhone" value="true" />
				<?php }?>
			
			<div class="spacer" style="margin-top: 20px">&nbsp</div>
			<?php if (isset($_SESSION["permissions"]["vdmAdvanced"]) && $_SESSION["permissions"]["vdmAdvanced"] == "1") { ?>
			<div style="" class="alignBtn">
				<input type="button" id="advancedOption" class="go" value="Show Advanced Options">
			</div>
			<?php } ?>
			<div class="spacer" style="margin-top: 20px">&nbsp</div>

			<!-- Show Advanced Options Div -->

			<div class="" id="AlgoAdvancedOption" style="display: block;">
					<div class="row">
						<div class="col-md-6">
						<div class="form-group">
							<span class="selectInputRadio"> <label
								class="labelText labelAboveInput"
								style="width: 193px;" for="">Multicast Mode : </label>
							</span> <input type="hidden" name="customTags['%ALGOMCASTMODE%']"
								value="<?php echo $customTags['%ALGOMCASTMODE%'][0];?>"
								id="multiCastCodeMasterValue"> <input type="checkbox"
								name="searchmac" id="multiCastCodeMaster" style="width: 25px"
								<?php if(isset($customTags['%ALGOMCASTMODE%'][0])){if($customTags['%ALGOMCASTMODE%'][0] == "1"){echo "checked";}}else{echo "";}?> />

							<label class="labelText" for="multiCastCodeMaster"><span></span>Master</label>
						</div>
						</div>
						
						<div class="col-md-6">
						<div class="form-group">
							<span class="selectInputRadio"> <label
								class="labelText labelAboveInput"
								style="width: 283px;" for="">Polycom Group Paging/PPT</label></span>
							<input type="hidden" name="customTags['%ALGO.POLYCOM.MODE%']"
								value="<?php echo $customTags['%ALGO.POLYCOM.MODE%'][0];?>"
								id="polycomGroupPagingValue"> <input type="checkbox"
								name="searchmac" id="polycomGroupPaging" style="width: 25px"
								<?php if(isset($customTags['%ALGO.POLYCOM.MODE%'][0])){if($customTags['%ALGO.POLYCOM.MODE%'][0] == "1"){echo "checked";}}else{echo "";}?> />
							<label class="labelText" for="polycomGroupPaging"><span></span>
								Disable </label>
						</div>
						</div>

					</div>

					<div class="row">
						<div class="col-md-6">
						<div class="form-group">
							<span class="selectInputRadio"> <label
								class="labelText labelAboveInput"
								style="width: 193px;" for=""> Automatic Gain Control : </label>
							</span> <input type="hidden" name="customTags['%ALGO.AGC%']"
								value="<?php echo $customTags['%ALGO.AGC%'][0];?>"
								id="automaticGainControlValue"> <input type="checkbox"
								name="searchmac" id="automaticGainControl" value="labelText"
								style="width: 25px"
								<?php if(isset($customTags['%ALGO.AGC%'][0])){if($customTags['%ALGO.AGC%'][0] == "1"){echo "checked";}}else{echo "";}?> />
							<label class="labelText" for="automaticGainControl"><span></span>Master</label>
						</div>
						</div>
						
						<div class="col-md-6">
						<div class="form-group">
							<label class="labelText labelAboveInput" for="deviceName"> FeedBack Delay
								(0-1000ms):</label> <br> <input style="font-size: 15px;margin-top: 10px;"
								type="number" min="0" max="1000" class="inputBox"
								name="customTags['%ALGO.JC.DELAY%']" id="feedBackDelayId"
								size="50" onkeyup="feedBackDelay(this);"
								value="<?php echo $customTags['%ALGO.JC.DELAY%'][0];?>">
						</div>
						</div>
					</div>


					<div class="row">
						<div class="col-md-6">
						<div class="form-group">
							<span class="selectInputRadio">
							 <label class="labelText labelAboveInput" for="ipSettings"
								style="width: 193px ; margin-bottom:10px;"> IP settings : </label></span>
								<div class="dropdown-wrap oneColWidth">
								 <select name="customTags['%ALGO.DHCP.USE%']" id="%ALGO.DHCP.USE%"
								class="ipSettingClass" style="background-color: #eeeeee;">
								<option value="1"
								<?php if($customTags['%ALGO.DHCP.USE%'][0] == "1"){?> selected
									<?php }?>>DHCP</option>
								<option value="0"
									<?php if($customTags['%ALGO.DHCP.USE%'][0] == "0"){?> selected
									<?php }?>>Static</option>
							</select>
							</div>
							
							 <!-- <input type="hidden" name="customTags['%ALGO.DHCP.USE%']" value="<?php echo $customTags['%ALGO.DHCP.USE%'][0];?>" id="ipSettingsValue">
						<input type="checkbox" name="searchmac" id="ipSettings" value="labelText" style="width: 25px" <?php if(isset($customTags['%ALGO.DHCP.USE%'][0])){if($customTags['%ALGO.DHCP.USE%'][0] == "1"){echo "checked";}}else{echo "";}?>/>
						<label class="labelText" for="ipSettings"> <span></span>Static </label> -->
						</div>
						</div>
						
						<div class="col-md-6">&nbsp;</div>
					</div>

					<div class="row">

						<div class="col-md-6">
						<div class="form-group">
							<label class="labelText labelAboveInput" for="deviceName"> DNS
								Server 1 :</label>
							<input class="algodns1" style="font-size: 15px;" type="text"
								name="customTags['%ALGO.DNS1%']"
								onkeyup="ValidateIPaddress(this, 'subButtonAlerter')"
								id="dnsServer1" size="50"
								value="<?php if(isset($customTags['%ALGO.DNS1%'][0])){echo $customTags['%ALGO.DNS1%'][0];}?>">
						</div>
						</div>

						
						<div class="col-md-6">
						<div class="form-group">
							<label class="labelText labelAboveInput" for="deviceName">DNS Server 2 :</label>
							<input class="algodns2" style="font-size: 15px;" type="text"
								name="customTags['%ALGO.DNS2%']"
								onkeyup="ValidateIPaddress(this, 'subButtonAlerter')"
								id="dnsServer2" size="50"
								value="<?php if(isset($customTags['%ALGO.DNS2%'][0])){echo $customTags['%ALGO.DNS2%'][0];}?>">
						</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
						<div class="form-group">
						<label class="labelText labelAboveInput" for="deviceName"> IP
								Gateway Address(router) :</label>
							<input class="algonetgateway" style="font-size: 15px;"
								type="text" name="customTags['%ALGO.NET.GATEWAY%']"
								onkeyup="ValidateIPaddress(this, 'subButtonAlerter')"
								id="ipGatewayAddress" size="50"
								value="<?php if(isset($customTags['%ALGO.NET.GATEWAY%'][0])){echo $customTags['%ALGO.NET.GATEWAY%'][0];}?>">
						</div>
						</div>

						<div class="col-md-6">
						<div class="form-group">
							<label class="labelText labelAboveInput" for="deviceName">Algo
								Static IP address :</label>
							<input class="algonetip" style="font-size: 15px;" type="text"
								name="customTags['%ALGO.NET.IP%']"
								onkeyup="ValidateIPaddress(this, 'subButtonAlerter')"
								id="staticIPAddress" size="50"
								value="<?php if(isset($customTags['%ALGO.NET.IP%'][0])){echo $customTags['%ALGO.NET.IP%'][0];}?>">
						</div>
						</div>
					</div>
				

				<div class="row">
					<div class="col-md-6">
					<div class="form-group">
						<label class="labelText labelAboveInput" for="deviceName">Network
							Subnet Mask :</label>
						<input class="algonetmask" style="font-size: 15px;" type="text"
							name="customTags['%ALGO.NET.MASK%']"
							onkeyup="ValidateIPaddress(this, 'subButtonAlerter')"
							id="networkSubnetMask" size="50"
							value="<?php if(isset($customTags['%ALGO.NET.MASK%'][0])){echo $customTags['%ALGO.NET.MASK%'][0];}?>">
					</div>
					</div>

					<div class="col-md-6">
					<div class="form-group">
						<label class="labelText labelAboveInput" for="deviceName">VLAN ID
							:</label>
						<input style="font-size: 15px;" type="text"
							name="customTags['%ALGO.VLAN.ID%']" id="searchSasUsers" class="sasUserVdm"
							size="50"
							value="<?php if(isset($customTags['%ALGO.VLAN.ID%'][0])){echo $customTags['%ALGO.VLAN.ID%'][0];}?>">
					</div>
					</div>
				</div>
				
			
			

			</div><!-- Show Advanced Options Div End -->

			<div class="spacer" style="margin-top: 20px">&nbsp</div>

			<div style="" class="alignBtn">
				<input type="button" id="subButtonCancel" value="Cancel"
					class="cancelButton"> <span class="spacer">&nbsp;</span>
				<input type="button" id="subButtonAlerter" class="subButtonAlerter subButton"
					value="Save" style="">
			</div>
		
                        </div>
		<div class="clr"></div>
		<div class="clr"></div>            
	</form>
		<input type="hidden" id="vdmTypeHidden" name="vdmTypeHidden" value="algoAlerter">

</div>
<div id="dialogAlgoAlerter" class="dialogClass"></div>
<div id="dialogAlgoAlerterWarningMsg" class="dialogClass"></div>