<style>
.dropdown-wrap {
	font-size: 30px
}

.leftSpace {
	margin-left: 20px;
}
#allUsers .thStrobe{
     padding-left: 2% !important;
}
#allUsers .tdStrobe {    
    padding-left: 2% !important;
}
td.thPolyD .dropdown-wrap {
    width: 100% !important;
}
td.thPolyD .dropdown-wrap:before {
    top: 0px !important;
}
</style>

<link rel="stylesheet" type="text/css"
	href="/Express/Vdm/css/vdm.css">


<div class="selectContainer" style="padding-top: 0% !important;">

		<form name="vdmLightForm" id="vdmLightForm" method="POST"
			autocomplete="off">
                    <div id="vdmStrobeDisplayContent">
                    <div class="vdmForm" id="indexForm">
			<h2 class="addUserText"><?php echo $deviceDetails['deviceType'];?></h2>
			<div id="vdmLightDisplayContent">

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						<label class="labelText labelAboveInput" for="">Model:</label>
							<input style="font-size: 15px;"
							class="inputColorReadOnly inputColor deviceType" type="text"
							name="deviceType" id="deviceType" size="50"
							value="<?php echo $deviceDetails['deviceType'];?>" readonly>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
								<label class="labelText labelAboveInput" for="">Mac Address:</label>
								<br>
									<input style="font-size: 15px;"
									class=" inputColorReadOnly inputColor deviceType" type="text"
									name="macAddress" id="Address"
									value="<?php echo $deviceDetails['macAddress'];?>" size="50" maxlength="12">
						</div>
					</div>
				
						
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="labelText labelAboveInput" for="">Name :</label>
							<input readonly style="font-size: 15px;"
								class="inputColorReadOnly inputColor deviceType" type="text"
								name="deviceName" id="deviceName" size="50"
								value="<?php echo $deviceName;?>">
						</div>
					</div>
					<div class="col-md-6">
					<div class="form-group">
						<label class="labelText labelAboveInput" for="">Strobe Pattern :</label>
						<div class="dropdown-wrap">
							<select name="customTags['%ALGOPAGEVOL%']"
								id="customTags['%ALGOPAGEVOL%']" class="inputBackground"
								style="">
								<option value="" <?php if(empty($customTags['%ALGOPAGEVOL%'][0])){echo "selected";}?>></option>
								<option value="0"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "0"){echo "selected";}?>>Rotate
									Fast</option>
								<option value="1"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "1"){echo "selected";}?>>Rotate
									Slow</option>
								<option value="2"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "2"){echo "selected";}?>>Multi-Strobe
									and Dim holdover</option>
								<option value="3"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "3"){echo "selected";}?>>Multi-Strobe
									fast dimmed</option>
								<option value="4"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "4"){echo "selected";}?>>Multi-Strobe
									fast bright</option>
								<option value="5"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "5"){echo "selected";}?>>Multi-strobe
									slow dimmed</option>
								<option value="6"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "6"){echo "selected";}?>>Multi
									Strobe slow bright</option>
								<option value="7"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "7"){echo "selected";}?>>Rotating
									Strobe</option>
								<option value="8"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "8"){echo "selected";}?>>Steady
									Dim
								
								<option value="9"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "9"){echo "selected";}?>>Steady
									Bright</option>
								<option value="10"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "10"){echo "selected";}?>>Pulse</option>
								<option value="11"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "11"){echo "selected";}?>>Side
									to Side</option>
								<option value="12"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "12"){echo "selected";}?>>Flashing</option>
								<option value="13"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "13"){echo "selected";}?>>Classic
									strobe fast</option>
								<option value="14"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "14"){echo "selected";}?>>Classic
									strobe medium</option>
								<option value="15"
									<?php if($customTags['%ALGOPAGEVOL%'][0] == "15"){echo "selected";}?>>Classic
									strobe slow</option>
							</select>
						</div>
					</div>
					</div>
				</div>
                            
                             </div>
                            </div>
                            
				<div class="row">
						<div class="col-md-12">
						<div class="form-group">
							<div class="autoHeight viewDetailNew">
								<div style="zoom: 1;">
									<table id="allUsers" class="table tableLineType scroll algoDevicesTableClass" style="width: 100%; margin: 0;" border="0">
										<thead>
											<tr class="tableVdmColor" style="">
												<th class="thStrobe">Port</th>
												<th class="thStrobe">User</th>
											</tr>
										</thead>
										<tbody id="tabletr" class="vdmTbody">
											<?php
											for($r = 1; $r <= $port; $r ++) {
												$color = "";
												$userInfo = "";
												$userId = "";
												$firstName = "";
												$lastName = "";
												$userPortInfo = getUserInfoUsingPort ( $getUserDevice ['Success'], $r );
												if (! empty ( $userPortInfo [0] ['userId'] )) {
												    
													if (! empty ( $userPortInfo [0] ['phoneNumber'] )) {
														$userInfo = $userPortInfo [0] ['phoneNumber'];
													} else {
														$userInfo = $userPortInfo [0] ['extn'];
													}
													
													if (! empty ( $userPortInfo [0] ['phoneNumber'] )) {
													    $userPhone = $userPortInfo [0] ['phoneNumber'];
													} else {
													    $userPhone = "";
													}
													
													if (! empty ( $userPortInfo [0] ['extn'] )) {
													    $userExtension = $userPortInfo [0] ['extn'];
													} else {
													    $userExtension = "";
													}
													
													$lastName = isset ( $userPortInfo [0] ['lastName'] ) ? $userPortInfo [0] ['lastName'] : "";
													$firstName = isset ( $userPortInfo [0] ['firstName'] ) ? $userPortInfo [0] ['firstName'] : "";
													$userId = isset ( $userPortInfo [0] ['userId'] ) ? $userPortInfo [0] ['userId'] : "";
													$checkStatus = $vdm->getUserStatus ( $userPortInfo [0] ['userId'] );
													if ($checkStatus == "Registered") {
														$color = "#72ac5d";
													} else {
														$color = "#ac5f5d";
													}
												}
												$userInfoLink = "<a style='color:" . $color . "' href='#' class='primaryUserModifyLink'
			                                               id='" . $userId . "' data-phone='".$userPhone."' data-extension='".$userExtension."' data-lastname='" . $lastName . "' data-firstname='" . $firstName . "' >" . $userInfo . "</a>";
												?>	
													<tr>
												<td class="tdStrobe">SIP ALERT <?php echo $r;?></td>
												<td class="tdStrobe"><span style="color:<?php echo $color;?>"><?php echo $userInfoLink;?></span></td>
											</tr>
											<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
                        
                        <div id="vdmStrobeDisplayContent">
                            <div class="vdmForm" id="indexForm">
				 <?php if($rebuildResetDevice == "true"){?>
				<div class="row ">
					<div class="col-md-12">
						<div class="form-group">
							<label class="labelText labelAboveInput" for="">Rebuild and Reset
								Device :</label>
						</div>
					</div>
					<div class="">
						<div class="col-md-3" style="padding:0">
							<div class="form-group">
								<input type="checkbox" name="rebuildPhoneFiles"
									id="rebuildPhoneFiles" value="true"> <label class="labelText"
									for="rebuildPhoneFiles"><span></span>Rebuild Phone Files</label>
							</div>
						</div>
						<div class="col-md-3" style="padding:0">
							<div class="form-group">
								<input type="checkbox" name="resetPhone" id="resetPhone"
									value="true"> <label class="labelText" for="resetPhone"><span></span>Reset
									the Phone</label>
							</div>
						</div>
						<div class="col-md-6">
						<div class="form-group"></div>
						</div>
					</div>
				</div>
				<?php }else{?>
					<input type="hidden" name="rebuildPhoneFiles"
					id="rebuildPhoneFiles" value="true" /> <input type="hidden"
					name="resetPhone" id="resetPhone" value="true" />
				<?php }?>
				
				
				
				<?php if (isset($_SESSION["permissions"]["vdmAdvanced"]) && $_SESSION["permissions"]["vdmAdvanced"] == "1") { ?>
				<div class="alignBtn">
					<input type="button" id="advancedOption" class="go" value="Show Advanced Options" style="">
				</div>
				<?php } ?>
				

				<!-- Show Advanced Options Div -->
				<div class="" id="AlgoAdvancedOption" style="display: block;">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group"></div>
							<div class="form-group">
								<span class="selectInputRadio">
								<label class="labelText labelAboveInput"> Automatic Gain Control : </label></span>
								<input type="checkbox" name="customTags['%ALGO.AGC%']"
									id="%ALGO.AGC%" value="1" style="width: 25px"
									<?php if($customTags['%ALGO.AGC%'][0] == 1){?> checked
									<?php }?> /> <label class="labelText" for="%ALGO.AGC%"><span></span></label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<span class="selectInputRadio">
								<label class="labelText labelAboveInput" for=""> IP Settings</label></span><br />
							
								<!-- input type="checkbox"
									 name="customTags['%ALGO.DHCP.USE%']" id="%ALGO.DHCP.USE%" value="1" style="width: 25px" <?php if($customTags['%ALGO.DHCP.USE%'][0] == 1){?>checked <?php }?> -->
								<div class="dropdown-wrap">
								<select name="customTags['%ALGO.DHCP.USE%']"
									id="%ALGO.DHCP.USE%" class="ipSettingClass inputBackground">
									<option value="0"
										<?php if($customTags['%ALGO.DHCP.USE%'][0] == "0"){?> selected
										<?php }?>>Static</option>
									<option value="1"
										<?php if($customTags['%ALGO.DHCP.USE%'][0] == "1"){?> selected
										<?php }?>>DHCP</option>
								</select>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="labelText labelAboveInput" for=""> DNS Server 1:</label>
								<br>
								<input style="font-size: 15px;" type="text"
								name="customTags['%ALGO.DNS1%']" id="%ALGO.DNS1%"
								class="algodns1"
								value="<?php echo $customTags['%ALGO.DNS1%'][0];?>" size="50"
								onkeyup="ValidateIPaddress(this, 'subButton')">
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="form-group">
								<label class="labelText labelAboveInput" for=""> DNS Server 2 :</label>
								<br>
								<input style="font-size: 15px;" type="text"
									name="customTags['%ALGO.DNS2%']" id="%ALGO.DNS2%"
									class="algodns2"
									value="<?php echo $customTags['%ALGO.DNS2%'][0];?>" size="50"
									onkeyup="ValidateIPaddress(this, 'subButton')">
							</div>

						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="labelText labelAboveInput" for=""> IP Gateway
									Address (router) :</label> <br>
									<input style="font-size: 15px;" type="text"
									name="customTags['%ALGO.NET.GATEWAY%']" id="%ALGO.NET.GATEWAY%"
									class="algonetgateway"
									value="<?php echo $customTags['%ALGO.NET.GATEWAY%'][0];?>"
									size="50" onkeyup="ValidateIPaddress(this, 'subButton')">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="labelText labelAboveInput" for="">Algo static IP
									address :</label> <br>
									<input style="font-size: 15px;" type="text"
									name="customTags['%ALGO.NET.IP%']" id="%ALGO.NET.IP"
									class="algonetip"
									value="<?php echo $customTags['%ALGO.NET.IP%'][0];?>" size="50"
									onkeyup="ValidateIPaddress(this, 'subButton')">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="labelText labelAboveInput" for=""> Network Subnet
									Mask :</label> <br>
									<input style="font-size: 15px;" type="text"
									name="customTags['%ALGO.NET.MASK%']" id="%ALGO.NET.MASK%"
									class="algonetmask"
									value="<?php echo $customTags['%ALGO.NET.MASK%'][0];?>"
									size="50" onkeyup="ValidateIPaddress(this, 'subButton')">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="labelText labelAboveInput" for="">VLAN ID :</label>
								<br>
								<input style="font-size: 15px;" type="text"
									name="customTags['%ALGO.VLAN.ID%']" id="%ALGO.VLAN.ID%"
									class="algovlanid"
									value="<?php echo $customTags['%ALGO.VLAN.ID%'][0];?>"
									size="50">
							</div>
						</div>
					</div>
				</div><!-- Show Advanced Options Div -->
				<div class="spacer" style="margin-top: 20px">&nbsp;</div>
				<div class="alignBtn">
					<input type="button" id="cancelButton" value="Cancel"
						class="cancelButton"> <span class="spacer">&nbsp;</span>
					<input type="button" id="subButton" class="subButton" value="Save">
				</div>
                            </div>   
                        </div> 
		</form>
		<input type="hidden" id="vdmTypeHidden" name="vdmTypeHidden" value="Algo_8128">
	

	<div class="clr"></div>
</div>

<div id="dialogAlgo" class="dialogClass"></div>
<div id="dialogAlgoWarningMsg" class="dialogClass"></div>

