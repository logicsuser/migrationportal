<style>
.dropdown-wrap {
	font-size: 30px
}

.leftSpace {
	margin-left: 20px;
}
select#tabBundle option {
    background: #fff;
    font-weight: 600;
    color:#333333;
}
#tabBundle{background:#fff;}

.tableVdmColor th{
    padding-left: 2%;
}
#allUsers tbody td{
      word-break: break-all;
   }
   
#vdmLightForm .tableVdmColor th, #vdmLightPlycomPhoneForm .tableVdmColor th{
    padding: 15px;
    font-size:12px;
}

/* .tableLineTypePoly select{ */
/*     margin-right: 35%; */
/* } */

#allUsers tbody tr td {
    border-top: 1px solid #e7e6e6;
}



.tableLineType th {
    width: 12% !important;
    text-align: center !important;
    xpadding-right: 10% !important;
    font-weight: 500 !important;
}


.tableLineType th {
    width: 12% !important;
    text-align: center !important;
    xpadding-right: 10% !important;
    font-weight: 500 !important;
}

.tableLineType .lineType{
    width: 180px !important;
    float: left !important;
}

.tableLineType .regLineKey {
    width: 95% !important;
}

.tableLineType .callsPerLine {
    width: 95% !important;
}

td.thPolyD .dropdown-wrap {
    width: 100% !important;
}
td.thPolyD .dropdown-wrap:before {
    top: 0px !important;
}
</style>
<?php 

require_once("/var/www/html/Express/Vdm/devices/polycom/PolycomDBOperations.php");

$pDBOP = new PolycomDBOperations;
$customProfile = $pDBOP->getAllPhoneProfiles();
$tagBundle = $pDBOP->getAllTagBundles();

$featuresPermission = $_SESSION["permissions"]["devVdmFeatures"];
$softKeyPermissions = $_SESSION["permissions"]["devVdmSoftKeys"];
$codecPermissions = $_SESSION["permissions"]["devVdmCodecs"];
//print_r($featuresPermission);
//print_r($softKeyPermissions);
//print_r($codecPermissions);


//print_r($customProfile);


// $tags = $pDBOP->getAllCustomTagsFromTagBundles(array("ACD", "EnhForward", "Forward"));
// print_r($tags); exit;

$vdm = new VdmOperations ();
$getDeviceDetails = $vdm->getAllUserOfDevice ( $sp, $groupId, $deviceName );
//print_r($getDeviceDetails["Success"]);


if($softKeyPermissions == "1"){
    $styleSoftKey = "style='display:block'";
}else{
    $styleSoftKey = "style='display:none'";
}

if($codecPermissions == "1"){
    $styleCodec = "style='display:block'";
}else{
    $styleCodec = "style='display:none'";
}
$styleFeature="";
if($featuresPermission == "1"){
    $styleFeature = "style='display:block'";
}else{
    $styleFeature = "style='display:none'";
}
if(isset($_SESSION["permissions"]["vdmAdvanced"]) && $_SESSION["permissions"]["vdmAdvanced"] == "1"){
    $permVDMAdvanced = true;
}else{
    $permVDMAdvanced = false;
}
?>
<div class="selectContainer" style="padding-top: 0% !important;">

	<form name="vdmLightForm8028" id="vdmLightPlycomPhoneForm"
		method="POST" autocomplete="off">
            <div class="formPadding vdmForm" id="indexForm">
		<h2 class="addUserText">
		<?php
		echo $deviceDetails['deviceType'];
		?>
		                    
		</h2>
		<!-- a href="javascript:void(0);" id="lcpt">Load Custom Profile Tags</a-->
		<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText labelAboveInput" for="">Device Name:</label>
						<input readonly style="font-size: 15px;"
						class="inputColorReadOnly deviceName" type="text"
						name="deviceName" id="deviceName" size="50"
						value="<?php echo $deviceName;?>"> <input type="hidden"
						name="deviceType" id="deviceType"
						value="<?php echo $deviceType;?>" class="deviceType">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText labelAboveInput" for="">MAC Address:</label><br>
						<input style="font-size: 15px;" class="inputColorReadOnly inputColor" maxlength="12"
						type="text" name="macAddress" id="Address"
						value="<?php echo $deviceDetails['macAddress'];?>" size="50">
					</div>
				</div>
		</div>
		
			<?php //if(isset($primaryUser['phoneNumber']) && isset($primaryUser['lastName']) && isset($primaryUser['firstName'])){?>
				<h2 class="addUserText">
				<?php 
				
				$userId = isset($primaryUser["userId"]) ? $primaryUser["userId"] : "";
				$firstName = isset($primaryUser["firstName"]) ? $primaryUser["firstName"] : "";
				$lastName = isset($primaryUser["lastName"]) ? $primaryUser["lastName"] : "";
				$phoneNumber = isset($primaryUser['phoneNumber']) ? $primaryUser['phoneNumber'] : "";
				$extension = isset($primaryUser['extn']) ? $primaryUser['extn'] : "";
				$userModLink = "";
				if($phoneNumber != ""){
						$userModLink .= $phoneNumber . "|";
				}
				if($lastName != ""){
					$userModLink .= $lastName . ", ";
				}
				if($firstName){
					$userModLink .= $firstName;
				}
				
				$deviceTypeLink = "<a style='color:#2A6496' href='#' class='primaryUserModifyLink' id='" . $userId . "' data-phone='".$phoneNumber."' data-extension='".$extension."' data-lastname='" . 
		                         $lastName . "' data-firstname='" . $firstName . "'>" . $userModLink . "</a>";
								 
				echo $deviceTypeLink;				
				?>
				
				</h2>
			<?php //}?>
			
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
					
					<?php if($deviceDetails['deviceType'] == "Polycom_VVX310"){?>
						<img src="images/phone--310.png" align="center" style="width: 100%;"/>
					<?php }?>
					
					<?php if($deviceDetails['deviceType'] == "Polycom_VVX410"){?>
						<img src="images/phone410.png" align="center" style="width: 100%;"/>
					<?php }?>
					
					<?php if($deviceDetails['deviceType'] == "Polycom_VVX500"){?>
						<img src="images/phone500.png" align="center" style="width: 100%;"/>
					<?php }?>
					
					<?php if($deviceDetails['deviceType'] == "Polycom_VVX600"){?>
						<img src="images/phone600.png" align="center" style="width: 100%;"/>
					<?php }?>
					</div>
				</div>

			<div class="col-md-6">
				<div class="form-group">
							<?php
							
if(isset($license["customProfile"]) && $license["customProfile"] == "true"){
							?>
							<span class="selectInputRadio"> <label
						class="labelText labelAboveInput" for=""> Custom Profile: </label>
					</span><br />
					<div class="dropdown-wrap">  
		                                                <?php 
		                                                if(isset($customTags['%phone-template%'][0]))
		                                                {
		                                                    $ExpCstmPrfeName = $customTags['%phone-template%'][0];
		                                                    echo "<input type='hidden' name='old_exprs_cstmPrfe_name' value='$ExpCstmPrfeName' />";               
		                                                } 
		                                                ?>
								
		                                                <select
							name="customTags['%Express_Custom_Profile_Name%']"
							class="inputBackground" id="customProfile">
								<?php
		                                                    $profileOptions = "";
		                                                    foreach($customProfile as $key => $val) {
		                                                        $profileOptions .= "<option value='".$val[1]."'>".$val[0]."</option>";
		                                                     }
		            					    echo $profileOptions;
		            					?>
								</select>
					</div>
				</div>
							<?php }?>
							<br />
				<br />
				<!--New code found Start-->
				<span class="selectInputRadio"> <label
					class="labelText labelAboveInput" for=""> Device Management Tag
						Bundles: </label>
				</span> <br />
				<div class="">
		                                            <?php
		                                                //$oldTagBundle = explode(":", $customTags["%Express_Tag_Bundle%"][0]);
		                                                //echo "<br />Tage Bunndle - ".$customTags["%Express_Tag_Bundle%"][0];
		                                                $oldTagBundle = "";
		                                                if(isset($customTags["%Express_Tag_Bundle%"][0]) && !empty($customTags["%Express_Tag_Bundle%"][0])){
		                                                    $oldTagBundle = explode(";", $customTags["%Express_Tag_Bundle%"][0]);
		                                                }
		                                                
		                				$bundleOpt = "";
		                				if(!isset($featuresPermission)){
		                				    $style = "style='padding:4px 0;background:transparent'";
		                				}else{
		                				    $style = "style='background: transparent'";
		                				}
		                				
		                         ?>
								<select name="deviceMgtTagBundle[]" id="deviceMgtTagBundle"
                                                                    size="<?php if(!isset($featuresPermission)){echo "9";}else{echo "3";} ?>"
                                                                    multiple="">
		                                                <?php
		                                                    foreach($tagBundle as $keyOpt => $valOpt) 
		                                                    {
		                                                        $selected = in_Array($valOpt["Tag_Bundle_ID"], $oldTagBundle) ? "selected" : "";
		                                                        $bundleOpt .= '<option value="'.$valOpt["Tag_Bundle_ID"].'" '.$selected.' '.$style.'>'.$valOpt["Tag_Bundle_Name"].'</option>';
		                                                    }
		                                                    echo $bundleOpt;
		                                                ?>
		                                                </select>
				</div>
				<div id="logChange">&nbsp;</div>
				<!--New code found End-->

				<br />
				<br />
				<!-- code start for feature -->
				<div <?php echo $styleFeature; ?>>
					<div class="">
						<span class="selectInputRadio"> <label
							class="labelText labelAboveInput" for=""> Features </label>
						</span>
					</div>
					<div class="col-md-3">
						<div class="">
							<span class="selectInputRadio"> <label class="labelText" for="">
									Sync ACD: </label>
							</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="">
							<input type="checkbox" id="FEATURE_SYNC_ACD"
								name="customTags['%FEATURE_SYNC_ACD%']" value="1"
								style="width: 25px"
								<?php if($customTags['%FEATURE_SYNC_ACD%'][0] == 1){?> checked
								<?php }?>> <label class="labelText" for="FEATURE_SYNC_ACD"><span></span></label>
						</div>
					</div>
					<div class="col-md-3">
						<div class="">
							<span class="selectInputRadio"> <label class="labelText" for="">
									Hoteling: </label>
							</span>
						</div>
					</div>
					<div class="col-md-3">
						<div class="">
							<input type="checkbox" id="FEATURE_HOTELING"
								name="customTags['%FEATURE_HOTELING%']" value="1"
								style="width: 25px"
								<?php if($customTags['%FEATURE_HOTELING%'][0] == 1){?> checked
								<?php }?>> <label class="labelText" for="FEATURE_HOTELING"><span></span></label>
						</div>
					</div>
				</div>
			</div>
			<!--  end code for feature-->
			</div>
			
			<div class="row">
				<div <?php echo $styleSoftKey; ?>>
	    			<div class="<?php if(!isset($codecPermissions) && $codecPermissions != "1"){echo "";}else{echo "col-md-6";} ?>">
		    			<span class="selectInputRadio"> <label class="labelText labelAboveInput form-group" for="" style="float:left">
		    					Soft Key Options </label>
		    			</span>
	    				<div class="<?php if(!isset($codecPermissions) && $codecPermissions != "1"){echo "col-md-3";}else{echo "col-md-6";} ?>" style="padding-bottom: 10px;clear:both">
	    						<div class="col-md-9">
	    							<span class="selectInputRadio"><label class="labelText" for="">
	    									Paging: </label> </span>
	    						</div>
	    						<div class="col-md-3">
	    							<input type="checkbox" id="SKPaging"
	    								name="customTags['%SKPaging%']" value="1" style="width: 25px"
	    								<?php if($customTags['%SKPaging%'][0] == 1){?> checked
	    								<?php }?>> <label class="labelText" for="SKPaging"><span></span></label>
	    						</div>
	    				</div>
	    
	    				<div class="<?php if(!isset($codecPermissions) && $codecPermissions != "1"){echo "col-md-3";}else{echo "col-md-6";} ?> " style="padding-bottom: 10px;">
	    						<div class="col-md-9">
	    							<span class="selectInputRadio"><label class="labelText" for="">
	    									Call Retrieve: </label> </span>
	    						</div>
	    						<div class="col-md-3">
	    							<input type="checkbox" id="SKRetrieve"
	    								name="customTags['%SKRetrieve%']" value="1"
	    								style="width: 25px"
	    								<?php if($customTags['%SKRetrieve%'][0] == 1){?> checked
	    								<?php }?>> <label class="labelText" for="SKRetrieve"><span></span></label>
	    						</div>
	    				</div>
	    				
	    		
	    				<div class="<?php if(!isset($codecPermissions) && $codecPermissions != "1"){echo "col-md-3";}else{echo "col-md-6";} ?>" style="padding-bottom: 10px;">
    						<div class="col-md-9">
    							<span class="selectInputRadio"><label class="labelText" for="">
    									Zip Dial: </label> </span>
    						</div>
    						<div class="col-md-3">
    							<input type="checkbox" id="SKZipDial"
    								name="customTags['%SKZipDial%']" value="1" style="width: 25px"
    								<?php if($customTags['%SKZipDial%'][0] == 1){?> checked
    								<?php }?>> <label class="labelText" for="SKZipDial"><span></span></label>
    						</div>
	    				</div>
	    
	    				<div class="<?php if(!isset($codecPermissions) && $codecPermissions != "1"){echo "col-md-3";}else{echo "col-md-6";} ?>" style="padding-bottom: 10px;">
    						<div class="col-md-9">
    							<span class="selectInputRadio"><label class="labelText" for="">
    									Call Forward: </label> </span>
    						</div>
    						<div class="col-md-3" <?php //if(!isset($codecPermissions) && $codecPermissions != "1"){echo "style='text-align:right;'";} ?>>
    							<input type="checkbox" id="SKForward"
    								name="customTags['%SKForward%']" value="1" style="width: 25px"
    								<?php if($customTags['%SKForward%'][0] == 1){?> checked
    								<?php }?>> <label class="labelText" for="SKForward"><span></span></label>
    						</div>
	    				</div>
	    		 		
	    				<div class="<?php if(!isset($codecPermissions) && $codecPermissions != "1"){echo "col-md-3";}else{echo "col-md-6";} ?>" style="padding-bottom: 10px;">
    						<div class="col-md-9">
    							<span class="selectInputRadio"><label class="labelText" for="">
    									Call Pickup: </label> </span>
    						</div>
    						<div class="col-md-3">
    							<input type="checkbox" id="SKPickup"
    								name="customTags['%SKPickup%']" value="1" style="width: 25px"
    								<?php if($customTags['%SKPickup%'][0] == 1){?> checked
    								<?php }?>> <label class="labelText" for="SKPickup"><span></span></label>
    						</div>
	    				</div>
	    
	    				<div class="<?php if(!isset($codecPermissions) && $codecPermissions != "1"){echo "col-md-3";}else{echo "col-md-6";} ?>" style="padding-bottom: 10px;">
    						<div class="col-md-9">
    							<span class="selectInputRadio"><label class="labelText" for="">
    									Do Not Disturb: </label> </span>
    						</div>
    						<div class="col-md-3">
    							<input type="checkbox" id="SKDND" name="customTags['%SKDND%']"
    								value="1" style="width: 25px"
    								<?php if($customTags['%SKDND%'][0] == 1){?> checked <?php }?>>
    							<label class="labelText" for="SKDND"><span></span></label>
    						</div>
	    				</div>
	    			
	    				<div class="<?php if(!isset($codecPermissions) && $codecPermissions != "1"){echo "col-md-3";}else{echo "col-md-6";} ?>" style="padding-bottom: 10px;">
    						<div class="col-md-9">
    							<span class="selectInputRadio"><label class="labelText" for="">
    									Recent Call: </label> 
    							</span>
    						</div>
    						<div class="col-md-3">
    							<input type="checkbox" id="SKRecent"
    								name="customTags['%SKRecent%']" value="1" style="width: 25px"
    								<?php if($customTags['%SKRecent%'][0] == 1){?> checked
    								<?php }?>> <label class="labelText" for="SKRecent"><span></span></label>
    						</div>
	    				</div>
	    
	    				<div class="<?php if(!isset($codecPermissions) && $codecPermissions != "1"){echo "col-md-3";}else{echo "col-md-6";} ?>" style="padding-bottom: 10px;">
    						<div class="col-md-9">
    							<span class="selectInputRadio"><label class="labelText" for="">
    									Conference: </label> </span>
    						</div>
    						<div class="col-md-3" <?php //if(!isset($codecPermissions) && $codecPermissions != "1"){echo "style='text-align:right;'";} ?>>
	    							<input type="checkbox" id="SKConf"
	    								name="customTags['%SKConf%']" value="1" style="width: 25px"
	    								<?php if($customTags['%SKConf%'][0] == 1){?> checked <?php }?>>
	    							<label class="labelText" for="SKConf"><span></span></label>
	    						</div>
	    				</div>
	    			
	    				<div class="<?php if(!isset($codecPermissions) && $codecPermissions != "1"){echo "col-md-3";}else{echo "col-md-6";} ?>" style="padding-bottom: 10px;">
	    						<div class="col-md-9">
	    							<span class="selectInputRadio"><label class="labelText" for="">
	    									Call Park: </label> </span>
	    						</div>
	    						<div class="col-md-3">
	    							<input type="checkbox" id="SKCallPark"
	    								name="customTags['%SKCallPark%']" value="1"
	    								style="width: 25px"
	    								<?php if($customTags['%SKCallPark%'][0] == 1){?> checked
	    								<?php }?>> <label class="labelText" for="SKCallPark"><span></span></label>
		    						</div>
		    				</div>
		    				<div class="col-md-6"></div>
			    	  </div> 
    	  			</div>
				<div <?php echo $styleCodec; ?> >
        			<div class="<?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "";}else{echo "col-md-6";} ?>">
        			<div>
        				<span class="selectInputRadio"> <label class="labelText labelAboveInput" for="" <?php if(!$softKeyPermissions){echo "style='margin-left:27px;'";} ?>>
	        						Voice Codecs </label>
	        				</span>
	        			</div>
	        			<div class="clr"></div>	
	        			<div class="">
        				<div class="col-md-6">
        					<span class="selectInputRadio"> <label class="labelText labelAboveInput" for="" <?php //if(!$softKeyPermissions){echo "style='margin-left:32px;'";} ?>>
        							Codec: </label>
        					</span> <br>
        				</div>
        				<?php 
        				if(isset($softKeyPermissions) && $softKeyPermissions == "1"){ ?>
        					<div class="col-md-6">
	        					<span class="selectInputRadio"> <label class="labelText labelAboveInput" for="">
	        							Preference: </label>
	        					</span> <br>
        					</div>
        				<?php } ?>
        			</div>
        			<div id="codecId" style= "xdisplay:flex; padding:0px !important" class="col-md-12">
            			<div class="<?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "col-md-4";}else{echo "formspace";} ?>" <?php if(!$softKeyPermissions){echo "style=''";} ?> data-class="codecDiv1" id="codecDiv1">
            				<div class="" id="codec1" >
                				<div class="dropdown-wrap <?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "form-group";}else{echo "col-md-6";} ?>" style="<?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "";}else{echo "padding-right:0px !important";} ?>">
                					<input type="text" disabled name="codecTagName1Changed" id = "codecTagName1" value="G729 AB:" style="width: 100% !important;">
                				</div>
            					<?php 
            					   if(!isset($softKeyPermissions) && $softKeyPermissions !="1"){ ?>
            					       <!-- <div class="col span_24"><label class="labelText labelAboveInput">Preference: </label></div> -->
            					       <div class=""><label class="labelText labelAboveInput">Preference: </label></div>
            					<?php } ?>
            
                    			<div class="dropdown-wrap <?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "";}else{echo "col-md-6";} ?>" style="<?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "";}else{echo "padding-right:0px !important";} ?>">
                    				<select name="customTags['%codecPrefG729AB%']" class="inputBackground codecValue"
                    					id="codecTagValue1" style="width:100% !important">
                    					<option value="0">0</option>
                    					<option value="1">1</option>
                    					<option value="2">2</option>
                    					<option value="3">3</option>
                    				</select>
                    			</div>
            				</div>
            			</div>
        		  <!-- second dropdown -->
        		  		<?php if(isset($softKeyPermissions) && $softKeyPermissions == "1"){?><div class="clr" style="clear:both"></div><?php }?>
        				<div class="<?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "col-md-4";}else{echo "formspace";} ?>" <?php if(!$softKeyPermissions){echo "style=''";} ?> data-class="codecDiv2" id="codecDiv2">
            				<div class="" id="codec2">
            					<div class="dropdown-wrap <?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "form-group";}else{echo "col-md-6";} ?>" style="<?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "";}else{echo "padding-right:0px !important";} ?>">
            						<input type="text" disabled name="codecTagName2Changed" value="G711 u-law:" id = "codecTagName2" style="width:100% !important;">
            					</div>
            					<?php 
            					   if(!isset($softKeyPermissions) && $softKeyPermissions !="1"){ ?>
            					       <div class=""><label class="labelText labelAboveInput">&nbsp; </label></div>
            					<?php } ?>
            
            					<div class="dropdown-wrap <?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "";}else{echo "col-md-6";} ?>" style="<?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "";}else{echo "padding-right:0px !important";} ?>">
                    				<select name="customTags['%codecPrefG711Mu%']" class="inputBackground codecValue"
                    					id="codecTagValue2" style="width:100% !important">
                    					<option value="0">0</option>
                    					<option value="1">1</option>
                    					<option value="2">2</option>
                    					<option value="3">3</option>
                    				</select>
            					</div>
            				</div>
            			</div>
        				<?php if(isset($softKeyPermissions) && $softKeyPermissions == "1"){?><div class="clr" style="clear:both"></div><?php }?>
        		  <!-- thired dropdown -->
            			<div class="<?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "col-md-4";}else{echo "formspace";} ?>" <?php if(!$softKeyPermissions){echo "style='padding-right:0px !important '";} ?> data-class="codecDiv3" id="codecDiv3">
                			<div class="" id="codec3">
                				<div class="dropdown-wrap <?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "form-group";}else{echo "col-md-6";} ?>" style="<?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "";}else{echo "padding-right:0px !important";} ?>">
                					<input type="text" disabled name = "codecTagName3Changed" value="G722:" id ="codecTagName3" style="width:100% !important;">
                				</div>
                				<?php 
            					   if(!isset($softKeyPermissions) && $softKeyPermissions !="1"){ ?>
            					       <div class=""><label class="labelText labelAboveInput"> &nbsp;</label></div>
            					<?php } ?>
                
                				<div class="dropdown-wrap <?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "";}else{echo "col-md-6";} ?>" style="<?php if(!isset($softKeyPermissions) && $softKeyPermissions != "1"){echo "";}else{echo "padding-right:0px !important";} ?>">
                    				<select name="customTags['%codecPrefG722%']" class="inputBackground codecValue"
                    					id="codecTagValue3" style="width:100% !important;">
                    					<option value="0">0</option>
                    					<option value="1">1</option>
                    					<option value="2">2</option>
                    					<option value="3">3</option>
                    				</select>
                				</div>
                			</div>
            			</div>
        		  <!-- end dropdown thired -->
        			</div>
        		<div class="clr"></div>
        	</div>
        			</div>
			</div>
		
	<!-- code for softkey -->		
			<div class="clr"></div>	
			<!-- Line configuration -->
               </div>         
			<div class="row">
				<div class="col-md-12" style="padding-right:0px;">
				<div class="form-group">
					<div class="autoHeight viewDetailNew">
						
							<table id="allUsers" class="table tableLineType scroll algoDevicesTableClass polycomDeviceTableClass" style="width: 100%; margin: 0;" border="0">
								<thead>
									<tr class="tableVdmColor">
										<th class="thPolyD thPolyDNw">Port</th>
										<th style="thPolyD thPolyDNw">DN/Extension</th>
										<th style="thPolyD thPolyDNw">Line/Port</th>
										<th class="thPolyD thPolyDNw">Reg. Number</th> 
										<?php 
										if($permVDMAdvanced){ ?>
										    <th style="thPolyD thPolyDNw">Line Type</th>
										    <th style="thPolyD thPolyDNw">Line Key Index</th>
										<?php }
										?>
										<th class="thPolyD thPolyDNw">Number of Lines</th>
										<th class="thPolyD thPolyDNw">Calls Per Line</th>
									</tr>
								</thead>
								<tbody id="tabletr" class="">
							<?php
							
							for($li = 1; $li <= $deviceDetails ["numberOfPorts"]; $li ++) {
								$lineType = "lineType" . $li;
								$callsPerLine = "CallsPerLine-" . $li;
								$regLineKey = "reg" . $li . "lineKeys";
								$linePort = "";
								$userPhoneNumber = "";
								$userIdVal = "";
								$dataFirstName = "";
								$dataLastName = "";
								$orderVal = "0";
								if(count($getDeviceDetails["Success"]) > 0){
								    foreach ($getDeviceDetails["Success"] as $devKey => $devValue){
								        if($devValue["order"] == $li){
								            
								            $orderVal = $devValue["order"];
								            $linePort = $devValue["linePort"];
								            $userIdVal = $devValue["userId"];
								            $dataFirstName = $devValue["firstName"];
								            $dataLastName = $devValue["lastName"];
								            
								            if($devValue["phoneNumber"] != "" ){
								                $userPhoneNumber = $devValue["phoneNumber"] ." x ". $devValue["extn"];
								            }else{
								                
								                $extval = $vdm->getUserDetails($devValue["userId"]);
								             
								                $userPhoneNumber = $extval;
								            }
								            if($devValue["phoneNumber"] != ""){
								                $userPhone = $devValue["phoneNumber"];
								            }else{
								                $userPhone= "";
								            }
								            
								            if($devValue["extn"] != ""){
								                $userExt = $devValue["extn"];
								            }else{
								                $userExt= "";
								            }
								        }
								    }
								}
					/*create new array for Dn extension*/ 		
								 
					/*end */			
								
			?> 				
								<input type="hidden" name="regName_<?php echo $li; ?>" value="<?php  echo 'Reg.'.$li; ?>" /> 
									<input type="hidden" id="reglineKeys_<?php echo $li;?>" value="<?php echo $customTags['%'.$regLineKey.'%'][0];?>" />
												
									<tr class="tblRow_<?php echo $li; ?>">
												<td class="thPolyD"></td>
												 <td class="thPolyD"> 
                                                     <?php
                                                     if($userIdVal != ""){ ?>
                                                         <a href="#" id= "<?php echo $userIdVal; ?>" class="modiyUserLink" data-phone="<?php echo $userPhone; ?>" data-extension="<?php echo $userExt; ?>" data-firstname = "<?php echo $dataFirstName; ?>" data-lastname = "<?php echo $dataLastName; ?>"><?php echo $userPhoneNumber; ?></a>
                                                    <?php } else{
                                                        echo $userPhoneNumber;
                                                     }
                                                     ?>
												</td>
												<td class="thPolyD"><?php echo $linePort; ?></td>
												<td class='tblRowTd_<?php echo $li; ?> thPolyD'><div id="regId_<?php echo $li; ?>"><?php  echo 'Reg.'.$li; ?></div></td>
												<?php if($permVDMAdvanced){  ?>
												<td class='tblRowTd_<?php echo $li; ?> thPolyD'>
												<div class="dropdown-wrap">
												<select name="customTags['%lineType<?php echo $li;?>%']"
																		id="lineType<?php echo $li;?>" class="lineType">
																			<option value=""
																				<?php if($customTags['%'.$lineType.'%'][0] == "Unspecified" || $customTags['%'.$lineType.'%'][0] == ""){echo "selected";}?>>Unspecified</option>

												</select>
												</div>
																		
												</td>
												<td class="thPolyD">
													<?php //echo $orderVal;
															//echo $customTags['%'.lineKey-3-index.'%'][0];
													?>
													<input type="text" name="customTags['%lineKey-<?php echo $li; ?>-index%']" value="<?php if(isset($customTags['%lineKey-'.$li.'-index%'][0])){echo $customTags['%lineKey-'.$li.'-index%'][0];}else{echo "0";} ?>" class="lineKey" style="padding:0px 9px !important">
												</td>										
												<?php 
													}
												?>
									
										<td class='tblRowTd_<?php echo $li; ?> thPolyD'>
											<div class="dropdown-wrap">
												<select name="customTags['%reg<?php echo $li;?>lineKeys%']"
												id="reg<?php echo $li;?>lineKeys" class="regLineKey">

												<option value="1"
													<?php if($customTags['%'.$regLineKey.'%'][0] == "1"){echo "selected";}?>>1</option>
												<option value="2"
													<?php if($customTags['%'.$regLineKey.'%'][0] == "2"){echo "selected";}?>>2</option>
												<option value="3"
													<?php if($customTags['%'.$regLineKey.'%'][0] == "3"){echo "selected";}?>>3</option>
												<option value="4"
													<?php if($customTags['%'.$regLineKey.'%'][0] == "4"){echo "selected";}?>>4</option>
												<option value="5"
													<?php if($customTags['%'.$regLineKey.'%'][0] == "5"){echo "selected";}?>>5</option>
												<option value="6"
													<?php if($customTags['%'.$regLineKey.'%'][0] == "6"){echo "selected";}?>>6</option>
												</select>
											</div>
										</td>
										<td class='tblRowTd_<?php echo $li; ?> thPolyD'>
										<div class="dropdown-wrap">
												<select name="customTags['%CallsPerLine-<?php echo $li;?>%']"
												id="CallsPerLine<?php echo $li;?>" class="callsPerLine">
												<option value="1"
													<?php if($customTags['%'.$callsPerLine.'%'][0] == "1"){echo "selected";}?>>1</option>
												<option value="2"
													<?php if($customTags['%'.$callsPerLine.'%'][0] == "2"){echo "selected";}?>>2</option>
												<option value="3"
													<?php if($customTags['%'.$callsPerLine.'%'][0] == "3"){echo "selected";}?>>3</option>
												<option value="4"
													<?php if($customTags['%'.$callsPerLine.'%'][0] == "4"){echo "selected";}?>>4</option>
												<option value="5"
													<?php if($customTags['%'.$callsPerLine.'%'][0] == "5"){echo "selected";}?>>5</option>
												<option value="6"
													<?php if($customTags['%'.$callsPerLine.'%'][0] == "6"){echo "selected";}?>>6</option>
												<option value="7"
													<?php if($customTags['%'.$callsPerLine.'%'][0] == "7"){echo "selected";}?>>7</option>
												<option value="8"
													<?php if($customTags['%'.$callsPerLine.'%'][0] == "8"){echo "selected";}?>>8</option>
												<option value="9"
													<?php if($customTags['%'.$callsPerLine.'%'][0] == "9"){echo "selected";}?>>9</option>
												<option value="10"
													<?php if($customTags['%'.$callsPerLine.'%'][0] == "10"){echo "selected";}?>>10</option>
												<option value="11"
													<?php if($customTags['%'.$callsPerLine.'%'][0] == "11"){echo "selected";}?>>11</option>
												<option value="12"
													<?php if($customTags['%'.$callsPerLine.'%'][0] == "12"){echo "selected";}?>>12</option>

										</select>
										</div>
										</td>
									</tr>
							<?php }?>
						</tbody>
							</table>
					
					</div>
					</div>
					</div>
					<!-- end line Configuration -->
			</div>

			
		<?php if($rebuildResetDevice == "true"){?>

			<div class="" style="width: 830px;margin: 0 auto;">
				<div class="col-md-12">
						<div class="form-group">
							<label class="labelText labelAboveInput" for="">Rebuild and Reset
								Device :</label>
						</div>
				</div>
				<div class="">
					<div class="col-md-3" style="padding:0">
						<div class="form-group">
							<input type="checkbox" name="rebuildPhoneFiles"
								id="rebuildPhoneFiles" value="true"> <label class="labelText"
								for="rebuildPhoneFiles"><span></span>Rebuild Phone Files</label>
						</div>
					</div>

					<div class="col-md-3" style="padding:0">
						<div class="form-group">
							<input type="checkbox" name="resetPhone" id="resetPhone"
								value="true"> <label class="labelText" for="resetPhone"><span></span>Reset
								the Phone</label>
						</div>
					</div>
					<div class="col-md-6">
					<div class="form-group"></div>
					</div>
				</div>
			</div>
			
				<?php }else{?>
					<input type="hidden" name="rebuildPhoneFiles"
					id="rebuildPhoneFiles" value="true" /> <input type="hidden"
					name="resetPhone" id="resetPhone" value="true" />
				<?php }?>
		<div class="alignBtn" style="clear:both;">
			<input type="button" id="polyButtonCancel" class="cancelButton"
				value="Cancel"> <span class="spacer">&nbsp;</span>
			<input type="button" id="polyButtonSubmit" class="subButton"
				value="Save">
		</div>
		<div class="clr"></div>
                <!-- </div> -->
	</form>
	<input type="hidden" id="vdmTypeHidden" name="vdmTypeHidden" value="polycomVVX310, polycomVVX410, polycomVVX500, polycomVVX600">


</div>
<div id="dialogPolycom" class="dialogClass"></div>


