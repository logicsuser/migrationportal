<?php
require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

if(!array_key_exists("deviceMgtTagBundle", $_POST)){
    $_POST['deviceMgtTagBundle'][0] = "";
}

//echo "1234"; print_r($_POST);
require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
$vdm = new VdmOperations();

require_once("/var/www/html/Express/util/formDataArrayDiff.php");
$formDataArrDiff = new FormDataArrayDiff();

//Code added @ 17-April 2018 to show add/deleted tag under Tag Bundle
require_once("/var/www/html/Express/Vdm/devices/polycom/PolycomDBOperations.php");
$polComDbOp = new PolycomDBOperations;
//End Code 

$changeStringValidate = "";
$changeStringReset = "";
$changeStringDeviceFileReset = "";

//echo "Post Data"; print_r($_POST);
//echo "Session Data"; print_r($_SESSION['device']['customTags']);

if(!array_key_exists("rebuildPhoneFiles", $_POST)){
	$_POST['rebuildPhoneFiles'] = false;
}

if(!array_key_exists("resetPhone", $_POST)){
	$_POST['resetPhone'] = false;
}


/*foreach($_POST as $key=>$val){
	if(isset($_POST['codecTagName1']) && $key == "codecTagName1" && !empty($_POST['codecTagName1'])){
		$_POST['customTags'][$val] = $_POST['codecTagValue1'];
	}
	if(isset($_POST['codecTagName2']) && $key == "codecTagName2" && !empty($_POST['codecTagName2'])){
		$_POST['customTags'][$val] = $_POST['codecTagValue2'];
	}
	if(isset($_POST['codecTagName3']) && $key == "codecTagName3" && !empty($_POST['codecTagName3'])){
		$_POST['customTags'][$val] = $_POST['codecTagValue3'];
	}
}

if(!isset($_POST['customTags']['%codecPrefG711Mu%'])){
	$_POST['customTags']['%codecPrefG711Mu%'] = "0";
}
if(!isset($_POST['customTags']['%codecPrefG722%'])){
	$_POST['customTags']['%codecPrefG722%'] = "0";
}
if(!isset($_POST['customTags']['%codecPrefG729AB%'])){
	$_POST['customTags']['%codecPrefG729AB%'] = "0";
}*/


foreach($_POST['customTags'] as $key=>$val){
	if(!array_key_exists("'%FEATURE_SYNC_ACD%'", $_POST['customTags'])){
		$_POST['customTags']['%FEATURE_SYNC_ACD%'] = "0";
	}
	if(!array_key_exists("'%FEATURE_HOTELING%'", $_POST['customTags'])){
		$_POST['customTags']['%FEATURE_HOTELING%'] = "0";
	}
	if(!array_key_exists("'%SKPaging%'", $_POST['customTags'])){
		$_POST['customTags']['%SKPaging%'] = "0";
	}
	if(!array_key_exists("'%SKZipDial%'", $_POST['customTags'])){
		$_POST['customTags']['%SKZipDial%'] = "0";
	}
	
	if(!array_key_exists("'%SKPickup%'", $_POST['customTags'])){
		$_POST['customTags']['%SKPickup%'] = "0";
	}
	if(!array_key_exists("'%SKRecent%'", $_POST['customTags'])){
		$_POST['customTags']['%SKRecent%'] = "0";
	}
	if(!array_key_exists("'%SKCallPark%'", $_POST['customTags'])){
		$_POST['customTags']['%SKCallPark%'] = "0";
	}
	if(!array_key_exists("'%SKRetrieve%'", $_POST['customTags'])){
		$_POST['customTags']['%SKRetrieve%'] = "0";
	}
	
	if(!array_key_exists("'%SKForward%'", $_POST['customTags'])){
		$_POST['customTags']['%SKForward%'] = "0";
	}
	if(!array_key_exists("'%SKDND%'", $_POST['customTags'])){
		$_POST['customTags']['%SKDND%'] = "0";
	}
	if(!array_key_exists("'%SKConf%'", $_POST['customTags'])){
		$_POST['customTags']['%SKConf%'] = "0";
	}
	
}

foreach($_POST as $key=>$val){
	if($key != "customTags"){
		$deviceArray['details'][$key] = $_POST[$key];
	}else{
		if($val <> "Error" || $val <> "Success"){
			$deviceArray[$key] = $_POST[$key];
		}
	}
}

//remove the single quotes from key coming from post
foreach($deviceArray['customTags'] as $key=>$val){
	$keyc = str_replace("'", "", $key);
	$tagsArray['customTags'][$keyc] = $val;
}

$deviceDetailsArray = array(
		"deviceType" => "Device Type",
		"macAddress" => "Mac Address",
		"deviceName" => "Device Name",
		"ipSettings" => "IP Settings",
		"automaticGainControl" => "Automatic Gain Control",
);

$deviceTagsArray = array(
		"%CallsPerLine-1%" => "Calls Per Line 1",
		"%CallsPerLine-2%" => "Calls Per Line 2",
		"%CallsPerLine-3%" => "Calls Per Line 3",
		"%CallsPerLine-4%" => "Calls Per Line 4",
		"%CallsPerLine-5%" => "Calls Per Line 5",
		"%CallsPerLine-6%" => "Calls Per Line 6",
		"%CallsPerLine-7%" => "Calls Per Line 7",
		"%CallsPerLine-8%" => "Calls Per Line 8",
		"%CallsPerLine-9%" => "Calls Per Line 9",
		"%CallsPerLine-10%" => "Calls Per Line 10",
		"%CallsPerLine-11%" => "Calls Per Line 11",
		"%CallsPerLine-12%" => "Calls Per Line 12",
		"%CallsPerLine-13%" => "Calls Per Line 13",
		"%CallsPerLine-14%" => "Calls Per Line 14",
		"%CallsPerLine-15%" => "Calls Per Line 15",
		"%CallsPerLine-16%" => "Calls Per Line 16",
		"%codecPrefG711Mu%" => "Codec Preference G711Mu",
		"%codecPrefG722%" => "Codec Preference G722",
		"%codecPrefG729AB%" => "Codec Preference G729AB",
		"%FEATURE_HOTELING%" => "Hoteling",
		"%FEATURE_SYNC_ACD%" => "Feature Sync ACD",
		"%reg1lineKeys%" => "Registry Line 1",
		"%reg2lineKeys%" => "Registry Line 2",
		"%reg3lineKeys%" => "Registry Line 3",
		"%reg4lineKeys%" => "Registry Line 4",
		"%reg5lineKeys%" => "Registry Line 5",
		"%reg6lineKeys%" => "Registry Line 6",
		"%reg7lineKeys%" => "Registry Line 7",
		"%reg8lineKeys%" => "Registry Line 8",
		"%reg9lineKeys%" => "Registry Line 9",
		"%reg10lineKeys%" => "Registry Line 10",
		"%reg11lineKeys%" => "Registry Line 11",
		"%reg12lineKeys%" => "Registry Line 12",
		"%reg13lineKeys%" => "Registry Line 13",
		"%reg14lineKeys%" => "Registry Line 14",
		"%reg15lineKeys%" => "Registry Line 15",
		"%reg16lineKeys%" => "Registry Line 16",
		"%SKCallPark%" => "Call Park",
		"%SKConf%" => "Conference",
		"%SKDND%" => "Do Not Disturb",
		"%SKForward%" => "Call Forward",
		"%SKPaging%" => "Paging",
		"%SKPickup%" => "Call Pickup",
		"%SKRecent%" => "Recent Call",
		"%SKRetrieve%" => "Call Retrieve",
		"%SKZipDial%" => "Zip Dial",
		"%lineType1%" => "Line Type 1",
		"%lineType2%" => "Line Type 2",
		"%lineType3%" => "Line Type 3",
		"%lineType4%" => "Line Type 4",
		"%lineType5%" => "Line Type 5",
		"%lineType6%" => "Line Type 6",
		"%lineType7%" => "Line Type 7",
		"%lineType8%" => "Line Type 8",
		"%lineType9%" => "Line Type 9",
		"%lineType10%" => "Line Type 10",
		"%lineType11%" => "Line Type 11",
		"%lineType12%" => "Line Type 12",
		"%lineType13%" => "Line Type 13",
		"%lineType14%" => "Line Type 14",
		"%lineType15%" => "Line Type 15",
		"%lineType16%" => "Line Type 16",
		"%Express_Custom_Profile_Name%" => "Custom Profile",
        "%lineKey-1-index%" => "Line Key Index 1",
        "%lineKey-2-index%" => "Line Key Index 2",
        "%lineKey-3-index%" => "Line Key Index 3",
        "%lineKey-4-index%" => "Line Key Index 4",
        "%lineKey-5-index%" => "Line Key Index 5",
        "%lineKey-6-index%" => "Line Key Index 6",
        "%lineKey-7-index%" => "Line Key Index 7",
        "%lineKey-8-index%" => "Line Key Index 8",
        "%lineKey-9-index%" => "Line Key Index 9",
        "%lineKey-10-index%" => "Line Key Index 10",
        "%lineKey-11-index%" => "Line Key Index 11",
        "%lineKey-12-index%" => "Line Key Index 12",
        "%lineKey-13-index%" => "Line Key Index 13",
        "%lineKey-14-index%" => "Line Key Index 14",
        "%lineKey-15-index%" => "Line Key Index 15",
        "%lineKey-16-index%" => "Line Key Index 16"

);

$_SESSION['deviceUpdatedPolycom'] = false;
$_SESSION['customTagsUpdatedPloycom'] = false;
//newcode starts
$error = "";
$backgroundColor = 'background:ac5f5d;'; $errorMessage = '';

$oldTagBundle = isset($_SESSION['device']['allCustomTags']["%Express_Tag_Bundle%"]) ? explode(";", $_SESSION['device']['allCustomTags']["%Express_Tag_Bundle%"][0]) : array(0 => "");

$newTagBundle = isset($_POST["deviceMgtTagBundle"]) ? $_POST["deviceMgtTagBundle"] : array();
$isTagBundleSame = false;

if (count(array_diff(array_merge($oldTagBundle, $newTagBundle), array_intersect($oldTagBundle, $newTagBundle))) === 0) {
    $isTagBundleSame = true;
}

//Code added @ 17 April 2018 to check custom profile is changed or not
$old_exprs_cstmPrfe_name = $_POST['old_exprs_cstmPrfe_name'];
$custom_profile_name = $tagsArray['customTags']['%Express_Custom_Profile_Name%'];

if($custom_profile_name==$old_exprs_cstmPrfe_name)
    $isCustomProfileSame = true;
else
    $isCustomProfileSame = false;
//End Code
if(isset($deviceArray['details']['deviceName']) && !empty($deviceArray['details']['deviceName'])){
	
	foreach($deviceArray['details'] as $key=>$val){
		foreach($_SESSION['device']['details'] as $key1=>$val1){
			$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
			if($key == $key1){
				$val = trim($val);
				if($val <> $val1){
					if (array_key_exists($key, $deviceDetailsArray)) {
						$_SESSION['deviceUpdatedPolycom'] = true;
						$changeStringReset .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$deviceDetailsArray[$key].'</td><td class="errorTableRows">'.$val.' '. $errorMessage.' </td></tr>';
					}
				}
			}
		}
	}
// 	array_push($tagsArray, array("ertertre" => 1111)); 
	$tagsArray['customTags']["%deviceMgtTagBundle%"] = $newTagBundle;
// 	print_r($tagsArray);exit;
	
	$changeStringValidate = $vdm->checkModifyDataCompare($tagsArray['customTags'], $_SESSION['device']['customTags'], $deviceTagsArray, $_SESSION['device']['allCustomTags'], $_POST['deviceName'], $isCustomProfileSame);
	
	if ($_SESSION['customTagsUpdatedPloycom']){
		
		if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild and reset after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == false){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == false && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be reset after successful modification</b></td></tr>';
		}
	}else{
		//$changeStringReset = "";
		if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild and reset after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == true && $_POST['resetPhone'] == false){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be rebuild after successful modification</b></td></tr>';
		}else if($_POST['rebuildPhoneFiles'] == false && $_POST['resetPhone'] == true){
			$changeStringReset .= '<tr><td colspan = "2" style="padding:4px 0 12px 0;"><b>Device will be reset after successful modification</b></td></tr>';
		}
	}
	
	if($_SESSION['deviceUpdatedPolycom'] == false && $_SESSION['customTagsUpdatedPloycom'] == false && $_POST['rebuildPhoneFiles'] == false && $_POST['resetPhone'] == false && $isTagBundleSame && $isCustomProfileSame){
		$changeStringValidate .= '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'">Device Update</td><td class="errorTableRows">No Changes </td></tr>';
	}	
        
        
        if($isCustomProfileSame && count($newTagBundle)>0)
        {
            if(!$isTagBundleSame) {            
                //Code added @ 17-April 2018 to show add/deleted tag under Tag Bundle
                
                $diffArr =  $formDataArrDiff->diffInTwoArray($newTagBundle, $oldTagBundle);
               
                if(isset($diffArr["removed"]) && $diffArr["removed"][0] != ""){
                    $removedTagBundle  = $polComDbOp->getAllCustomTagsFromTagBundlesWithTagName($diffArr['removed']);
                    $removedTagBundleStr  = $polComDbOp->implodeArrayKeys($removedTagBundle);
                    $changeStringValidate .= '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'"> Device Management removed old custom tags</td><td class="errorTableRows">' .$removedTagBundleStr. '</td></tr>';
                }
                if(isset($diffArr["assigned"]) && $diffArr["assigned"][0] != ""){
                    $assignedTagBundle = $polComDbOp->getAllCustomTagsFromTagBundlesWithTagName($diffArr['assigned']);
                    $assignedTagBundleStr = $polComDbOp->implodeArrayKeys($assignedTagBundle);
                    $changeStringValidate .= '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'"> Device Management assigned new custom tags</td><td class="errorTableRows">' .$assignedTagBundleStr. '</td></tr>';
                }
                
                //End Code
            }
        }
        elseif($isCustomProfileSame==false && count($newTagBundle)>0)
        {
            if(!$isTagBundleSame) {            
                //Code added @ 17-April 2018 to show add/deleted tag under Tag Bundle
                $diffArr =  $formDataArrDiff->diffInTwoArray($newTagBundle, $oldTagBundle);
                if(isset($diffArr["removed"]) && $diffArr["removed"][0] != ""){
                    $removedTagBundle  = $polComDbOp->getAllCustomTagsFromTagBundlesWithTagName($diffArr['removed']);
                    $removedTagBundleStr  = $polComDbOp->implodeArrayKeys($removedTagBundle);
                    $changeStringValidate .= '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'"> Device Management removed old custom tags</td><td class="errorTableRows">' .$removedTagBundleStr. '</td></tr>';
                }
                if(isset($diffArr["assigned"]) && $diffArr["assigned"][0] != ""){
                    $assignedTagBundle = $polComDbOp->getAllCustomTagsFromTagBundlesWithTagName($diffArr['assigned']);
                    $assignedTagBundleStr = $polComDbOp->implodeArrayKeys($assignedTagBundle);
                    $changeStringValidate .= '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'"> Device Management assigned new custom tags</td><td class="errorTableRows">' .$assignedTagBundleStr. '</td></tr>';
                }
            }
        }
        
        if($rebuildResetDevice){
            if($_POST["rebuildPhoneFiles"] == true && $_POST["resetPhone"] == true){
                $changeStringDeviceFileReset .= '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'"> Device Rebuild and Reset</td><td class="errorTableRows">Device will be rebuild and reset after successful modification</td></tr>';
            }
            if($_POST["rebuildPhoneFiles"] == true && $_POST["resetPhone"] == false){
                $changeStringDeviceFileReset .= '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'"> Device Rebuild</td><td class="errorTableRows">Device will be rebuild after successful modification</td></tr>';
            }
            if($_POST["rebuildPhoneFiles"] == false && $_POST["resetPhone"] == true){
                $changeStringDeviceFileReset .= '<tr><td class="errorTableRows t2" style="'.$backgroundColor.'"> Device Reset</td><td class="errorTableRows">Device will be reset after successful modification</td></tr>';
            }
        }
}

//newcode ends
echo $error.$changeStringReset.$changeStringValidate.$changeStringDeviceFileReset;

?>