<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");

$vdm = new VdmOperations();

$customProfileName = $_POST["customProfileName"];
$deviceType = $_POST["deviceType"];

$customProfileTags = $vdm->getTagsCustomProfile($customProfileName, $deviceType);

foreach($customProfileTags as $key=>$val){
	$find = array("%", "-");
	$replace = array("", "");
	$tagName = str_replace($find, $replace, $val['customTagName']);
	$tags[$key]['customTagName'] = $tagName;
	$tags[$key]['customTagValue'] = $val['customTagValue'];
}
echo json_encode($tags);
?>