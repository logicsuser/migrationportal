<?php 
require_once("/var/www/html/Express/config.php");
checkLogin();


class PolycomDBOperations{
    
    function getAllPhoneProfiles(){
        global $db;
        
        $phoneProfileArray = array();
        $stmt = $db->prepare("SELECT * FROM phoneProfilesLookup");
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i =0;
        foreach($row as $key => $val) {
            $phoneProfileArray[$i][] = $val->phoneProfile;
            $phoneProfileArray[$i][] = $val->value;
            $i++;
        }
        
        return $phoneProfileArray;
    }
    
    
    
    
    
    function getAllTagBundles() {
        global $db;
        $tagBundleArray = array();
        
//         $tagBundleArray = array("nhForward","Forward","N-Way","Pickup","TCP","ACD");
        
        $stmt = $db->prepare("SELECT DISTINCT `tagBundle` FROM deviceMgmtTagBundles");
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i =0;
        foreach($row as $key => $val) {
            $tagBundleArray[$i]['Tag_Bundle_ID'] = $val->tagBundle;
            $tagBundleArray[$i]['Tag_Bundle_Name'] = $val->tagBundle;
         
            $i++;
        }
//         print_r($tagBundleArray); exit;
        return $tagBundleArray;
    }
    
    
    
    
    function getAllCustomTagsFromTagBundles($tagBundle)
    {
        global $db;
        
        $customTagArray = array();
        $tagBundle = join("','", $tagBundle);
        
        $stmt = $db->prepare("SELECT `tag`, `tagValue` FROM deviceMgmtTagBundles where `tagBundle` in('$tagBundle')");
        $stmt->execute();
        $customTagArrayRel = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($customTagArrayRel as $key => $value){
            $customTagArray[strval($value->tag)] = strval($value->tagValue);
        }
        
        return $customTagArray;
    }
    
    function getAllCustomTagsFromTagBundlesWithTagName($tagBundle)
    {
        global $db;
        
        $customTagArray = array();
        $tagBundle = join("','", $tagBundle);
        
        //$stmt = $db->prepare("SELECT `Tag_Name`, `Default_Value` FROM deviceMgmtTagBundleDefaults where `Tag_Bundle_ID` in('$tagBundle')");
        $stmt = $db->prepare("SELECT `tag` as `Tag_Name`, `tagValue` as `Default_Value` FROM deviceMgmtTagBundles where `tagBundle` in('$tagBundle')");
        $stmt->execute();
        $customTagArrayRel = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($customTagArrayRel as $key => $value){
            $customTagArray[strval($value->Tag_Name)] = strval($value->Default_Value);
        }
        
        return $customTagArray;
    }

    function implodeArrayKeys($tagNameArr)
    {
        return implode(", ", array_keys($tagNameArr));
    }
    
    function getAllCustomTagsFromTagBundlesOverride($tagBundle)
    {
        global $db;
        
        $Express_Tag_Bundle = implode(":", $tagBundle);
        $customTagArray = "";
        $tagBundle = join("','", $tagBundle);
        
        $stmt = $db->prepare("SELECT `tag` as `Tag_Name`, `tagValue` as `Default_Value` FROM deviceMgmtTagBundles where `tagBundle` in('$tagBundle')");
        $stmt->execute();
        $customTagArray = $stmt->fetchAll(PDO::FETCH_OBJ);
        
        //         $customTagArray = array(
        //             '%FEATURE_SYNC_ACD%' => '0',
        //             '%FEATURE_CALLPARK%' => '1',
        //             '%FEATURE_N_WAY%' => '0',
        
        //             '%SKRetrieve%' => '0',
        //             '%SKZipDial%' => '0',
        //             '%SKForward%' => '0',
        //             '%SKPickup%' => '0',
        //             '%SKDND%' => '0',
        //             '%Express_Tag_Bundle%' => implode(":", $tagBundle)
        //         );
        //$customTagArray[] = (object)array("Tag_Name" => "%Express_Tag_Bundle%", "Default_Value" => $Express_Tag_Bundle);
        return $customTagArray;
    }
    
}



if(isset($_POST["model"]) && $_POST["model"] == "getAllCustomTag") {
    
    $uMDBO = new PolycomDBOperations;
    $allCustomTag = $uMDBO->getAllCustomTagsFromTagBundlesOverride($_POST["selectedBundle"]);
    //print_r($allCustomTag);
    $customTagArrayNew = array();
    $i = 0;
    foreach($allCustomTag as $key => $value) {
        $customTagArrayNew[$i]['tagName'] = $value->Tag_Name;
        $customTagArrayNew[$i]['tagValue'] = $value->Default_Value;
        $i++;
    }
    print_r(json_encode($customTagArrayNew));
}

?>