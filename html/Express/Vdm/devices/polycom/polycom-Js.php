<?php 
if(isset($_SESSION["permissions"]["vdmAdvanced"]) && $_SESSION["permissions"]["vdmAdvanced"] == "1"){
    $permVDMAdvanced = true;
}else{
    $permVDMAdvanced = false;
}
?>
<script>
$(function()
{
	$("#polyButtonSubmit").click(function(){
		validatePolycomDevice();
	});

	var validatePolycomDevice = function(){
		var dataToSend = $("form#vdmLightPlycomPhoneForm").serializeArray();
		$.ajax({
			type: "POST",
			url: "Vdm/devices/polycom/polycom-Validate.php",
			data: dataToSend,
			success: function(result) {
				var noChangesContain = result.search("No Changes");
				var errorContain = result.search("Error");
				
				if(noChangesContain > 0 || errorContain > 0){
					$(":button:contains('Complete')").attr("disabled", "disabled").addClass("ui-state-disabled");
				}else{
					$(":button:contains('Complete')").removeAttr("disabled").removeClass("ui-state-disabled");
				}
				$("#dialogPolycom").dialog("option", "title", "Request Complete");
				$("#dialogPolycom").html('<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelistPolycom" class="confSettingTable"><tbody><tr><th colspan="2" align="center">Please confirm the settings below and click Complete to process your modifications.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');		    	
				$("#changelistPolycom").append(result);
				$("#dialogPolycom").append('</tbody></table>');
				$("#dialogPolycom").dialog("open");
				buttonsShowHide('Complete', 'show');
				buttonsShowHide('Cancel', 'show');
				buttonsShowHide('More Changes', 'hide');
				buttonsShowHide('Ok', 'hide');
			}
		});
	};

var modifyPolycomDevice = function(){
		
		var dataToSend = $("form#vdmLightPlycomPhoneForm").serializeArray();
		$("#dialogPolycom").html('Submitting the request....<img src=\"images/ajax-loader.gif\" height=\"15px\" width=\"95px\">');
		pendingProcess.push("Modify Device Management");
		$.ajax({
			url: 'Vdm/devices/polycom/polycom-Update.php',
	        type: 'POST',
	        data: dataToSend,
	        success: function(data)
	        {
	        	if(foundServerConErrorOnProcess(data, "Modify Device Management")) {
					return false;
              	}
	        	var isErrorExist = data.search("background-color: #ac5f5d");
	            if(isErrorExist > 0){
					buttonsShowHide('Cancel', 'show');
	            }else{
	            	buttonsShowHide('Cancel', 'hide');
	             }
	             
	            $("#dialogPolycom").html('');
	            $("#dialogPolycom").dialog("option", "title", "Request Complete");
				$("#dialogPolycom").html('<table style="width:850px; margin: 0 auto;" cellspacing="0" cellpadding="5" id="changelistPolyUpdate" class="confSettingTable"><tbody><tr><th colspan="2" align="center">The following changes are commplete.</th></tr><tr><td colspan="2"><table width="100%" cellpadding="5" class="legendRGTable"><tbody><tr><td style="background:#72ac5d;" width="5%">&nbsp;</td><td>Value Changed</td></tr><tr><td style="background:#ac5f5d;" width="5%">&nbsp;</td><td>Invalid Value</td></tr></tbody></table></td></tr><tr><td width="50%">&nbsp;</td><td width="50%">&nbsp;</td></tr>');		    	
				$("#changelistPolyUpdate").append(data);
				$("#dialogPolycom").append('</tbody></table>');
				$("#dialogPolycom").dialog("open");
				buttonsShowHide('More Changes', 'show');
				buttonsShowHide('Return to Main', 'show');
				buttonsShowHide('Complete', 'hide');
// 				buttonsShowHide('Cancel', 'hide');
				$(":button:contains('Complete')").removeAttr("disabled", "disabled").removeClass("ui-state-disabled");
	        }
	        
		});
	};

	$("#dialogPolycom").dialog({
		autoOpen: false,
		width: 800,
		modal: true,
		position: { my: "top", at: "top" },
		resizable: false,
		closeOnEscape: false,
		buttons: {
			"Complete": function() {
				modifyPolycomDevice();
			},
			"Cancel": function() {
				$(this).dialog("close");
			},
	        "More Changes": function() {
	        	var deviceType = $("#vdmLightPlycomPhoneForm .deviceType").val();
	            var deviceName = $("#vdmLightPlycomPhoneForm .deviceName").val();
	            if(deviceType == "Polycom_VVX410"){
	            	deviceType = "polycomVVX410";
		        }
	            if(deviceType == "Polycom_VVX310"){
	            	deviceType = "polycomVVX310";
		        }
	            if(deviceType == "Polycom_VVX500"){
	            	deviceType = "polycomVVX500";
		        }
	            if(deviceType == "Polycom_VVX600"){
	            	deviceType = "polycomVVX600";
		        }
	            $(this).dialog("close");
	            $('html, body').animate({scrollTop: '0px'}, 300);
	            reloadDevicePolycom(deviceType, deviceName);
	        },
	        "Ok": function() {
	        	$(this).dialog("close"); 
				},
			"Return to Main": function() {
				location.href="main.php";
			},
		},
	    open: function() {
			setDialogDayNightMode($(this));
	    	buttonsShowHide('Complete', 'show');
	    	buttonsShowHide('Cancel', 'show');
	    	buttonsShowHide('More Changes', 'hide');
	    	buttonsShowHide('Return to Main', 'hide');
	    	buttonsShowHide('Ok', 'hide');
	    	$('.ui-dialog-buttonpane').find('button:contains("Complete")').addClass('completeButton');
	    	$('.ui-dialog-buttonpane').find('button:contains("Cancel")').addClass('cancelButton');

        	$('.ui-dialog-buttonpane').find('button:contains("More Changes")').addClass('moreChangesButton');
        	$('.ui-dialog-buttonpane').find('button:contains("Return to Main")').addClass('returnToMainButton');
	    }
	});

	var reloadDevicePolycom = function(deviceType, deviceName){
		if(deviceType == "Algo_8028"){
			deviceType = "algoDoorPhone";
		}
		$("#vdmType").val(deviceType);
		$("#deviceNameDiv").val(deviceName);
		$("#subButtonDevice").trigger("click");
	};

	var buttonsShowHide = function(b,e){
		if(e == "show"){
			$(".ui-dialog-buttonpane button:contains("+b+")").button().show();
		}else{
		 	$(".ui-dialog-buttonpane button:contains("+b+")").button().hide();
		}
	};

	var loadCustomProfileTags = function(customProfileName, deviceType, deviceName){
		var result = [];
		$.ajax({
			url: 'Vdm/devices/polycom/custom_profile_tags.php',
	        type: 'POST',
	        data: {customProfileName : customProfileName, deviceType : deviceType, deviceName : deviceName},
	        success: function(data)
	        {
	            var obj = jQuery.parseJSON(data);   
	            for(var i in obj){
	                result.push([i, obj [i]]);
	            }
	            
	            for(w = 0; w < result.length; w++){
		            var TagName = result[w][1].customTagName;
		            var TagValue = result[w][1].customTagValue;
					if($("#"+TagName).length > 0){
						$("#"+TagName).val(TagValue);
						if ($('#'+TagName).is(':checkbox')) {
					        if(TagValue == 1){
					        	$('#'+TagName).prop('checked', true);
					        }else{
					        	$('#'+TagName).prop('checked', false);
					        }
					    }
						if(TagName.includes("lineType")){
							checkLineType(TagName, TagValue);
						}
					}
	            }
                
	        }
	        
		});
	};

	$("#customProfile").change(function(){

		var el = $(this);
		var elVal = el.val();
		
		var deviceName = $("#vdmLightPlycomPhoneForm .deviceName").val();
		var deviceType = $("#vdmLightPlycomPhoneForm .deviceType").val();
		loadCustomProfileTags(elVal, 'Polycom_VVX310', deviceName);
                //Code added @ 17 April 2018 if custom profile is changed then unselect tag bumdles
               // $("#deviceMgtTagBundle option").prop("selected", false);
	});

	var checkLineType = function(lineTypeId, lineTypeVal){
		
		if(lineTypeVal == "BLF" || lineTypeVal == "Speed Dial"){ 
			$("#"+lineTypeId).parent("td").parent("tr").find(".regLineKey").val('1');
			$("#"+lineTypeId).parent("td").parent("tr").find(".callsPerLine").val('1');		
		}
	}
	
	$(".lineType").change(function(){ 
		
		var el = $(this);
		var lineTypeVal = el.val();
		var lineTypeId = el.attr("id");
		checkLineType(lineTypeId, lineTypeVal);		
	});

	
	var temp_array = [];
	var codecArray = ["%codecPrefG711Mu%", "%codecPrefG729AB%", "%codecPrefG722%"];
	
	var firstSelected = [];
	var firstSelectedCount = 0;
	var nonArray = 0;
	var arrayTags = {"%codecPrefG711Mu%": "G711 u-law:", "%codecPrefG729AB%": "G729 AB:", "%codecPrefG722%": "G722:"};

	var codecPrefG729AB = "<?php if(isset($customTags['%codecPrefG729AB%']) && $customTags['%codecPrefG729AB%'][0] !== "0" ){ echo $customTags['%codecPrefG729AB%'][0];}else {echo "0"; } ?>";
	var codecPrefG711Mu = "<?php if(isset($customTags['%codecPrefG711Mu%']) && $customTags['%codecPrefG711Mu%'][0] !== "0" ){ echo $customTags['%codecPrefG711Mu%'][0];}else {echo "0"; } ?>";
	var codecPrefG722  = "<?php if(isset($customTags['%codecPrefG722%']) && $customTags['%codecPrefG722%'][0] !== "0" ){ echo $customTags['%codecPrefG722%'][0];}else {echo "0"; } ?>";
	$("#codecTagValue1").val(codecPrefG729AB);
	$("#codecTagValue2").val(codecPrefG711Mu);
	$("#codecTagValue3").val(codecPrefG722);

	/*$(".codec").change(function()
	{
		var el = $(this);
		var elId = el.attr("id");
		var clickedDropdown = [];
		clickedDropdown.push(elId);
		
				var allIds = ["codecTagName1", "codecTagName2", "codecTagName3"];


		var currentValue = $("#"+elId).val();
		var otherSelected = new Array();

		allIds.forEach(function(id){
			if(id != elId ){ //process options of other drop downs
				otherSelected = new Array();
				allIds.forEach(function(id2){
					if(id2 != elId && id2 !=id ){
						otherSelected.push($("#"+id2).val());
						
					}
				});	
				var selectedValue = $("#"+id).val();
				var option = "<option value=''></option>";
				codecArray.forEach(function(codecValue){
					if(codecValue != currentValue && otherSelected.indexOf(codecValue) == -1 ) {
						option += "<option value =" + codecValue +">" + arrayTags[codecValue] +"</option>";	
					}
				});
				$("#"+id).html(option);
				$("#"+id).val(selectedValue);
			} 					
		});	
	});

	$(".codecValue").change(function(){
		var val1 = "";
		val1 = $("#codecTagValue1 option:selected").val();
		var val2 = "";
		val2 = $("#codecTagValue2 option:selected").val();
		var val3 = "";
		val3 = $("#codecTagValue3 option:selected").val();
		var obj = {};
		obj["codecDiv1"] = val1;
		obj["codecDiv2"] = val2;
		obj["codecDiv3"] = val3;
		var sortableData = [];
		for (var t in obj) {
			sortableData.push([t, obj[t]]);
		}

		sortableData.sort(function(a, b) {
		    return a[1] - b[1];
		});

		if(obj.codecDiv1){
			if(obj.codecDiv1 == 0){
				$("#codecDiv1").css("order", "4");
			}else{
				$("#codecDiv1").css("order", obj.codecDiv1);
			}
			
		}
		if(obj.codecDiv2){
			if(obj.codecDiv2 == "0"){
				$("#codecDiv2").css("order", "4");
			}else{
				$("#codecDiv2").css("order", obj.codecDiv2);
			}
			
		}
		if(obj.codecDiv3){
			if(obj.codecDiv3 == "0"){
				$("#codecDiv3").css("order", "4");
			}else{
				$("#codecDiv3").css("order", obj.codecDiv3);
			}
			
		}		

	});

	var selectedField1 = $("#codecTagValue1 option:selected").val();
	var selectedField2 = $("#codecTagValue2 option:selected").val();
	var selectedField3 = $("#codecTagValue3 option:selected").val();
	if(selectedField1){
		if(selectedField1 == "0"){
			$("#codecDiv1").css("order", "4");
		}else{
			$("#codecDiv1").css("order", selectedField1);
		}
	}

	if(selectedField2){
		if(selectedField2 == "0"){
			$("#codecDiv2").css("order", "4");
		}else{
			$("#codecDiv2").css("order", selectedField2);
		}
	}

	if(selectedField3){
		if(selectedField3 == "0"){
			$("#codecDiv3").css("order", "4");
		}else{
			$("#codecDiv3").css("order", selectedField3);
		}
	}
 		$("#codecTagName3").change(function(){
		var val = "";
		val = $("#codecTagName3 option:selected").val();
		if(val == ""){
			$("#codecTagValue3").val(0);
		}
		//alert(val);
	});
	$("#codecTagName2").change(function(){
		var val = "";
		val = $("#codecTagName2 option:selected").val();
		if(val == ""){
			$("#codecTagValue2").val(0);
		}
		//alert(val);
	});
	$("#codecTagName1").change(function(){
		var val = "";
		val = $("#codecTagName1 option:selected").val();
		if(val == ""){
			$("#codecTagValue1").val(0);
		}
		//alert(val);
	});*/

	$("#polyButtonCancel").click(function(){
		var autoComplete = [];
		   $("#deviceName").autocomplete({
          source: autoComplete
      });
		$('#vdmType').val("");
		//$('#deviceName').html("");
		$('#deviceName').val("");
		$('#vdmLightPlycomPhoneForm').hide();
		$('html, body').animate({scrollTop: '0px'}, 300);
		
	});
	//$(".codec").trigger("change");
	//$("#codecTagName1").trigger("change");
	//$("#codecTagName2").trigger("change");
	//$("#codecTagName3").trigger("change");

	var selectedProfileName = "<?php if(isset($customTags['%phone-template%'][0])){echo $customTags['%phone-template%'][0];}else {echo "";} ?>";
	var defaultphonetemplateName = "<?php echo $systemTagList["Success"]["%phone-template%"][0]; ?>";

	if(selectedProfileName){
		$("#customProfile").val(selectedProfileName);
	}else{
		$("#customProfile").val(defaultphonetemplateName);
	}
	
	


		$(".primaryUserModifyLink").click(function() {
                                                $("#vdmMainPageReplace").html("<div class='col span_11 leftSpace' id='loader' style='margin-left:400px;margin-top:10%'> <img src='/Express/images/ajax-loader.gif'></div>");
						var userId = $(this).attr("id");
						var fname = $(this).attr("data-firstname");
						var lname = $(this).attr("data-lastname");

						var extension = $(this).attr("data-extension");
						var phone = $(this).attr("data-phone");
						if($.trim(phone) != ""){
							userIdValue1 = phone+"x"+extension;
						}else{
							userIdValue1 = userId;
						}
						var userIdValue = userIdValue1+" - "+lname+", "+fname;
						
						$.ajax({
								type: "POST",
								url: "userMod/userMod.php",
								data: { searchVal: userId, userFname : fname, userLname : lname },
								success: function(result)
								{
									$("#mainBody").html(result);
									$("#searchVal").val(userIdValue);
									setTimeout(function() {
										$("#go").trigger("click");
									}, 2000);
									//$(".mainBannerModify").html("User Modify");
									$(".navMenu").removeClass("active");
									$("#userMod").addClass("active");
									$('#helpUrl').attr('data-module', "userMod");
								}

							});
			});

		$(".modiyUserLink").click(function() {
			$("#vdmMainPageReplace").html("<div class='col span_11 leftSpace' id='loader' style='margin-left:400px;margin-top:10%'> <img src='/Express/images/ajax-loader.gif'></div>");
			var userId = $(this).attr("id");
			var fname = $(this).attr("data-firstname");
			var lname = $(this).attr("data-lastname");

			var extension = $(this).attr("data-extension");
			var phone = $(this).attr("data-phone");
			if($.trim(phone) != ""){
				userIdValue1 = phone+"x"+extension;
			}else{
				userIdValue1 = userId;
			}
			var userIdValue = userIdValue1+" - "+lname+", "+fname;
			
			$.ajax({
					type: "POST",
					url: "userMod/userMod.php",
					data: { searchVal: userId, userFname : fname, userLname : lname },
					success: function(result)
					{
						$("#mainBody").html(result);
						$("#searchVal").val(userIdValue);
						$("#go").trigger("click");
						//$(".mainBannerModify").html("User Modify");
						$(".navMenu").removeClass("active");
						$("#userMod").addClass("active");
						$('#helpUrl').attr('data-module', "userMod");
					}

				});
});

		

		$("#Address").on("input", function()
		{
			var macAddress = $("#Address").val();
			var macToLowerCase = macAddress.toLowerCase();
			var lastElmt = macToLowerCase.substr(-1);
			if((lastElmt >= "0" && lastElmt <= "9") || (lastElmt >= "a" && lastElmt <= "f")){
				//do something
		    }else{
		   		$("#Address").val(macAddress.slice(0,-1));
		    }
		    
		   var macAddress = $("#Address").val();
		   if(macAddress.length > 0 && macAddress.length < 12){
			   $("#Address").attr('style', 'border-color :#ac5f5d !important');
			   $("#polyButtonSubmit").prop("disabled", true);
		   }else{
			   $("#Address").attr('style', 'border-color :#002c60 !important');
			   $("#polyButtonSubmit").prop("disabled", false);	
			}
		});
});
</script>

<script>

var activeImageSwap = function()
{	previousActiveMenu	= "modVdm";
	 currentClickedDiv = "userMod";       
  
		//active
  		if(previousActiveMenu != "")
		{ 
  		 	$(".navMenu").removeClass("activeNav");
			var $thisPrev = $("#modVdm").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);
	       
		}
		// inactive tab
		if(currentClickedDiv != ""){
			$("#userMod").addClass("activeNav");
			var $thisPrev = $("#userMod").find('.ImgHoverIcon');
	        var newSource = $thisPrev.data('alt-src');
	        $thisPrev.data('alt-src', $thisPrev.attr('src'));
	        $thisPrev.attr('src', newSource);
			 previousActiveMenu = currentClickedDiv;
			
    	}
   
}
$('.primaryUserModifyLink').click(activeImageSwap, activeImageSwap);
$('.modiyUserLink').click(activeImageSwap, activeImageSwap);

//Code added @ 23 April 2018
function dropdownChanges(array, type) {
    $.each(array, function(i, item) {
        //$('#logChange').append(item + ' was ' + type + '<br>');        
        if(type=="removed")
        {
            removedBundle.push(item);
        }        
    });
}
//End Code

var removedBundle = [];
var valArray = $('#deviceMgtTagBundle').val() ? $('#deviceMgtTagBundle').val() : [];
$("#deviceMgtTagBundle").change(function() {
       //Code added @ 23 April 2018        
        var selectedBundle = $(this).val();
        var val = $(this).val();
        var numVals = (val) ? val.length : 0;
        if (numVals != valArray.length) 
        {
            var longerSet, shortSet;
            (numVals > valArray.length) ? longerSet = val : longerSet = valArray;
            (numVals > valArray.length) ? shortSet = valArray : shortSet = val;
            var changes = $.grep(longerSet, function(n) {
                return $.inArray(n, shortSet) == -1;
            });
            dropdownChanges(changes, (numVals > valArray.length) ? 'selected' : 'removed');
        }
        else
        {                
            dropdownChanges( valArray, 'removed');
            dropdownChanges( val, 'selected');
        }
        valArray = (val) ? val : [];
       //End code
       //alert('Added Items - '+selectedBundle);
       //alert('Removed Items - '+removedBundle);

	$.ajax({
		type: 'POST',
		url: 'Vdm/devices/polycom/PolycomDBOperations.php',
		data:{model: 'getAllCustomTag', selectedBundle: selectedBundle},
		success: function(result) {
			var result = JSON.parse(result);
			//console.log(result);
			$.each(result, function(index, value) {
				var status = value.tagValue == 1 ? true : false;
				var tagName = value.tagName.replace("%", "").replace("%", "");
				if($("#"+tagName).length) {
					$("#"+tagName).prop("checked", status);
				}
			}); 	
		}
		});
                
        $.ajax({
		type: 'POST',
		url: 'Vdm/devices/polycom/PolycomDBOperations.php',
		data:{model: 'getAllCustomTag', selectedBundle: removedBundle},
		success: function(result) {
			var result = JSON.parse(result);
			//console.log(result);
			$.each(result, function(index, value) {
				var rStatus = value.tagValue == 1 ? true : false;
				var rTagName = value.tagName.replace("%", "").replace("%", "");
				//alert('Removed Tag Name - ' + value.tagName +'Removed Tag Value - ' + value.tagValue);
                                if($("#"+rTagName).length) {
					$("#"+rTagName).prop("checked", false);
				}
			});
                removedBundle = [];
		}
		});
	
});

//Code added for VDM Registration line
var redrawTableRowsBasedonNoflines  = function(){
             
                var countAllRow = <?php echo $deviceDetails["numberOfPorts"]; ?>;                
                var i=1;                
                var count = 0;
                var tmpValcounter = 0;
                var limitRemain = countAllRow;
                var staticDrdLimit = 6;
                var ddLimit = 6;
                var ddFinalLimit = 6;
                var newCount = 1;
                var newCounter = 1;
                var tmpCounter = 1;
                
                while(newCounter <= countAllRow)
                {                    
                    var nofLineDdwnId = "#reg"+i+"lineKeys";                    
                    var tblRowId      = ".tblRow_"+i;                    
                    var ddVal         = Number($(nofLineDdwnId).val());              
                    
                    //var tblRowId      = ".tblRow_"+a;
                    //$(tblRowId).css("border-bottom", "0px");
                    
                    var regRowId   = "#regId_"+i;                    
                    //$(nofLineDdwnId).attr('name', "customTag['%reg"+newCount+"lineKeys%']");
                    $(regRowId).html('Reg. '+newCount);              
                    newCount = newCount+1;
                    for(j=1;j<ddVal;j++)
                    {  
                        <?php 
                        if($permVDMAdvanced){ ?>
                            var addNewBlankTr = "<tr class='newTr'><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
                        <?php } else{ ?>
                            var addNewBlankTr = "<tr class='newTr'><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
                       <?php }
                        ?>
                        
                        $('#allUsers > tbody > tr').eq(count).after(addNewBlankTr);
                        count = count+1;
                        newCounter = newCounter +1;
                    }
                  i = i+1; 
                  count = count+1;
                  newCounter = newCounter +1;
                }
                
                var rowCount = $('#allUsers tr').length;
                var coutStart = Number(countAllRow) + 1;
                for(j=coutStart; j<=rowCount; j++)
                {
                    $('#allUsers tr').eq(j).hide();
                }          
	 }
      
      var addSerialNumber = function () {
                $('#allUsers tbody tr').each(function(index) {
                    $(this).find('td:nth-child(1)').html(index+1);
                });
                }
                
         
     $(document).ready(function(){
    	 redrawTableRowsBasedonNoflines();
         addSerialNumber();
    });
    
    
    
    $(".regLineKey").on("click", function() {
            
            var countAllRow = <?php echo $deviceDetails["numberOfPorts"]; ?>;
            var dDwnId = $(this).attr("id");
            var tmpArr = dDwnId.split("reg");
            var tmpStr = tmpArr[1];
            var tmpArr = tmpStr.split("lineKeys");
            var i = Number(tmpArr[0]);
            var count = i;
            var tmpCount = i-1;
            var newCounter = i;
            var tmpValcounter = 0;
            
            var dDwnName = $(this).attr("name"); 
            var tpArr = dDwnName.split("reg");
            var tpStr = tpArr[1];
            var tpArr = tpStr.split("lineKeys");
            var newCount = Number(tpArr[0]);
            
            var selItem = $(this).val();            
            var hidCustomTagId = "#reglineKeys_"+i;
            var oldHidCustomTagId = Number($(hidCustomTagId).val());            
            $(hidCustomTagId).val(selItem);
            oldHidCustomTagId = oldHidCustomTagId - 1;
            var ddVal         = selItem - 1;            
            
            for(k=1;k<=oldHidCustomTagId;k++)
            { 
                //If next row is blank then remove otherwise skip remove                
                $(this).closest('tr').next('tr.newTr').remove();   
            }          
             
            for(j=1;j<=ddVal;j++)
            { 
				<?php
				if($permVDMAdvanced){ ?>
				var addNewBlankTr = "<tr class='newTr'><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
				<?php  }else{ ?>
				var addNewBlankTr = "<tr class='newTr'><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>"; 
				<?php }
				?>
                
                $(this).closest('tr').after(addNewBlankTr);
            }
            
            for(x=1; x<=countAllRow; x++)
            {
                $('#allUsers tr').eq(x).show();
            }
            
            var rowCount = $('#allUsers tr').length;
            var coutStart = Number(countAllRow) + 1;
            for(y=coutStart; y<=rowCount; y++)
            {
                $('#allUsers tr').eq(y).hide();
            }
            
            addSerialNumber();
            //redrawTableRowsBasedonNoflines();
            
      });         
    
</script>