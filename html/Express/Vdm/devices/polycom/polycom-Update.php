<?php 

require_once ("/var/www/html/Express/config.php");
require_once("/var/www/lib/broadsoft/login.php");
checkLogin ();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/vdm/vdmOperations.php");
require_once ("/var/www/html/Express/Vdm/devices/polycom/PolycomDBOperations.php");
require_once("/var/www/html/Express/util/formDataArrayDiff.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/html/Express/userMod/UserModifyDBOperations.php");
include("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");

$cLUObj = new ChangeLogUtility($_POST['deviceName'], $_SESSION["groupId"], $_SESSION["loggedInUserName"]);
$userOpDBOperation = new UserModifyDBOperations();
$formDataArrDiff = new FormDataArrayDiff();
require_once("/var/www/html/Express/userMod/UserCustomTagManager.php");


$deviceArray = array();
$modifyTag['tags'] = array();
$changeStringComplete = "";
$deviceDetail = array("macAddress" => "Mac Address");
$deviceProfile = array();
if(!array_key_exists("rebuildPhoneFiles", $_POST)){
	$_POST['rebuildPhoneFiles'] = "false";
}

if(!array_key_exists("resetPhone", $_POST)){
	$_POST['resetPhone'] = "false";
}

const phoneProfileName = "%Express_Custom_Profile_Name%";
const phoneTemplateName = "%phone-template%";
const ExpressTagBundleName = "%Express_Tag_Bundle%";

if(!array_key_exists("deviceMgtTagBundle", $_POST)){
    $_POST['deviceMgtTagBundle'][0] = "";
}
//print_r($_POST);
server_fail_over_debuggin_testing(); /* for fail Over testing. */

$deviceType = $_POST["deviceType"];
$deviceName = $_POST["deviceName"];
require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
$syslevelObj = new sysLevelDeviceOperations();
$setName = $syslevelObj->getTagSetNameByDeviceType($deviceType, $ociVersion);
$systemDefaultTagVal = "";
if($setName["Success"] != "" && !empty($setName["Success"])){
    $defaultSettags = $syslevelObj->getDefaultTagListByTagSet($setName["Success"]);
    $phoneTemplateName = "%phone-template%";
    if(array_key_exists($phoneTemplateName, $defaultSettags["Success"])){
        $systemDefaultTagVal = $defaultSettags["Success"]["%phone-template%"][0];
    }
}

$oldTagBundleArray[] = "";
if(isset($_SESSION['device']['allCustomTags']["%Express_Tag_Bundle%"][0]) && !empty($_SESSION['device']['allCustomTags']["%Express_Tag_Bundle%"][0])){
    $oldTagBundleArray = explode(";", $_SESSION['device']['allCustomTags']["%Express_Tag_Bundle%"][0]);
}

$diffArray =  $formDataArrDiff->diffInTwoArray($_POST['deviceMgtTagBundle'], $oldTagBundleArray);
$bundleChangedCheck = false;
if((isset($diffArray["assigned"]) && $diffArray["assigned"][0] != "") || (isset($diffArray["removed"]) && $diffArray["removed"][0] != "")){
    $bundleChangedCheck = true;
}


$deviceTagsArray = array(
		"%CallsPerLine-1%" => "Calls Per Line 1",
		"%CallsPerLine-2%" => "Calls Per Line 2",
		"%CallsPerLine-3%" => "Calls Per Line 3",
		"%CallsPerLine-4%" => "Calls Per Line 4",
		"%CallsPerLine-5%" => "Calls Per Line 5",
		"%CallsPerLine-6%" => "Calls Per Line 6",
		"%CallsPerLine-7%" => "Calls Per Line 7",
		"%CallsPerLine-8%" => "Calls Per Line 8",
		"%CallsPerLine-9%" => "Calls Per Line 9",
		"%CallsPerLine-10%" => "Calls Per Line 10",
		"%CallsPerLine-11%" => "Calls Per Line 11",
		"%CallsPerLine-12%" => "Calls Per Line 12",
		"%CallsPerLine-13%" => "Calls Per Line 13",
		"%CallsPerLine-14%" => "Calls Per Line 14",
		"%CallsPerLine-15%" => "Calls Per Line 15",
		"%CallsPerLine-16%" => "Calls Per Line 16",
		"%codecPrefG711Mu%" => "Codec Preference G711Mu",
		"%codecPrefG722%" => "Codec Preference G722",
		"%codecPrefG729AB%" => "Codec Preference G729AB",
		"%FEATURE_HOTELING%" => "Hoteling",
		"%FEATURE_SYNC_ACD%" => "Feature Sync ACD",
		"%reg1lineKeys%" => "Registry Line 1",
		"%reg2lineKeys%" => "Registry Line 2",
		"%reg3lineKeys%" => "Registry Line 3",
		"%reg4lineKeys%" => "Registry Line 4",
		"%reg5lineKeys%" => "Registry Line 5",
		"%reg6lineKeys%" => "Registry Line 6",
		"%reg7lineKeys%" => "Registry Line 7",
		"%reg8lineKeys%" => "Registry Line 8",
		"%reg9lineKeys%" => "Registry Line 9",
		"%reg10lineKeys%" => "Registry Line 10",
		"%reg11lineKeys%" => "Registry Line 11",
		"%reg12lineKeys%" => "Registry Line 12",
		"%reg13lineKeys%" => "Registry Line 13",
		"%reg14lineKeys%" => "Registry Line 14",
		"%reg15lineKeys%" => "Registry Line 15",
		"%reg16lineKeys%" => "Registry Line 16",
		"%SKCallPark%" => "Call Park",
		"%SKConf%" => "Conference",
		"%SKDND%" => "Do Not Disturb",
		"%SKForward%" => "Call Forward",
		"%SKPaging%" => "Paging",
		"%SKPickup%" => "Call Pickup",
		"%SKRecent%" => "Recent Call",
		"%SKRetrieve%" => "Call Retrieve",
		"%SKZipDial%" => "Zip Dial",
		"%lineType1%" => "Line Type 1",
		"%lineType2%" => "Line Type 2",
		"%lineType3%" => "Line Type 3",
		"%lineType4%" => "Line Type 4",
		"%lineType5%" => "Line Type 5",
		"%lineType6%" => "Line Type 6",
		"%lineType7%" => "Line Type 7",
		"%lineType8%" => "Line Type 8",
		"%lineType9%" => "Line Type 9",
		"%lineType10%" => "Line Type 10",
		"%lineType11%" => "Line Type 11",
		"%lineType12%" => "Line Type 12",
		"%lineType13%" => "Line Type 13",
		"%lineType14%" => "Line Type 14",
		"%lineType15%" => "Line Type 15",
		"%lineType16%" => "Line Type 16",
		"%Express_Custom_Profile_Name%" => "Custom Profile",
        "%lineKey-1-index%" => "Line Key Index 1",
        "%lineKey-2-index%" => "Line Key Index 2",
        "%lineKey-3-index%" => "Line Key Index 3",
        "%lineKey-4-index%" => "Line Key Index 4",
        "%lineKey-5-index%" => "Line Key Index 5",
        "%lineKey-6-index%" => "Line Key Index 6",
        "%lineKey-7-index%" => "Line Key Index 7",
        "%lineKey-8-index%" => "Line Key Index 8",
        "%lineKey-9-index%" => "Line Key Index 9",
        "%lineKey-10-index%" => "Line Key Index 10",
        "%lineKey-11-index%" => "Line Key Index 11",
        "%lineKey-12-index%" => "Line Key Index 12",
        "%lineKey-13-index%" => "Line Key Index 13",
        "%lineKey-14-index%" => "Line Key Index 14",
        "%lineKey-15-index%" => "Line Key Index 15",
        "%lineKey-16-index%" => "Line Key Index 16"
);

function compareExistingTagsWithBundleTags($existingCustomTag, $tagToBeAddIfBundleChanged){
    $diffrencedArray = array();
    
    foreach ($tagToBeAddIfBundleChanged as $key => $val){
        if(!array_key_exists($key, $existingCustomTag)){
            $diffrencedArray["add"][$key] = $val;
        }else{
            $diffrencedArray["modify"][$key] = $val;
        }
    }
    return $diffrencedArray;
}

function removeDeviceCustomTagsIfTagBundleChanged($deviceType, $deviceName, $tagToDeleteArray) {
    global $db;
    //$userOpDBOperation = new UserModifyDBOperations();
    
    //$existingCustomTag = $userOpDBOperation->getAllCustomTagsFromTagBundles($deviceType, $deviceName);
    $tagManager = new UserCustomTagManager($deviceName,$deviceType);
    //$existingCustomTag = $tagManager->getUserCustomTagList();
    if(count($tagToDeleteArray) > 0){
        foreach ($tagToDeleteArray as $key => $val){
            $resp = $tagManager->deleteCustomTag($key);
        }
    }
}

function addDeviceCustomTagsIfBundleChanged($deviceType, $deviceName, $tagToBeAddIfBundleChanged){
    
    if(count($tagToBeAddIfBundleChanged) > 0){
        foreach ($tagToBeAddIfBundleChanged as $keyToAdd => $valToAdd){
            if (($errMsg = addDeviceCustomTag($deviceName, $keyToAdd, $valToAdd)) != "") {
                return $errMsg;
            }
        }
    }
    
    return "";
}

function addDeviceCustomTag($deviceName, $tagName, $tagValue) {
    global $sessionid, $client;
    
    $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagAddRequest");
    $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
    $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
    $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
    $xmlinput .= "<tagName>" . $tagName . "</tagName>";
    $xmlinput .= "<tagValue>" . $tagValue . "</tagValue>";
    
    $xmlinput .= xmlFooter();
    
    $response = $client->processOCIMessage(array("in0" => $xmlinput));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    
    $errMsg = "";
    if (readError($xml) != "") {
        $errMsg = "Failed to add custom tag to device " . $deviceName . " .";
        $errMsg .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
        if ($xml->command->detail)
        {
            $errMsg .= "%0D%0A" . strval($xml->command->detail);
        }
    }
    return $errMsg;
}
/*
function removeDeviceCustomTagsIfTagBundleChanged($deviceType, $deviceName, $tagToDeleteArray) {
    global $db;
    
    $tagManager = new UserCustomTagManager($deviceName,$deviceType);
    if(count($tagToDeleteArray) > 0){
        foreach ($tagToDeleteArray as $key => $val){
            $resp = $tagManager->deleteCustomTag($key);
        }
    }
}

*/

/*foreach($_POST as $key=>$val){
	if(isset($_POST['codecTagName1']) && $key == "codecTagName1" && !empty($_POST['codecTagName1'])){
		$_POST['customTags'][$val] = $_POST['codecTagValue1'];
	}
	if(isset($_POST['codecTagName2']) && $key == "codecTagName2" && !empty($_POST['codecTagName2'])){
		$_POST['customTags'][$val] = $_POST['codecTagValue2'];
	}
	if(isset($_POST['codecTagName3']) && $key == "codecTagName3" && !empty($_POST['codecTagName3'])){
		$_POST['customTags'][$val] = $_POST['codecTagValue3'];
	}
}

if(!isset($_POST['customTags']['%codecPrefG711Mu%'])){
	$_POST['customTags']['%codecPrefG711Mu%'] = "0";
}
if(!isset($_POST['customTags']['%codecPrefG722%'])){
	$_POST['customTags']['%codecPrefG722%'] = "0";
}
if(!isset($_POST['customTags']['%codecPrefG729AB%'])){
	$_POST['customTags']['%codecPrefG729AB%'] = "0";
}*/

foreach($_POST['customTags'] as $key=>$val){
	if(!array_key_exists("'%FEATURE_SYNC_ACD%'", $_POST['customTags'])){
		$_POST['customTags']['%FEATURE_SYNC_ACD%'] = "0";
	}
	if(!array_key_exists("'%FEATURE_HOTELING%'", $_POST['customTags'])){
		$_POST['customTags']['%FEATURE_HOTELING%'] = "0";
	}
	if(!array_key_exists("'%SKPaging%'", $_POST['customTags'])){
		$_POST['customTags']['%SKPaging%'] = "0";
	}
	if(!array_key_exists("'%SKZipDial%'", $_POST['customTags'])){
		$_POST['customTags']['%SKZipDial%'] = "0";
	}
	
	if(!array_key_exists("'%SKPickup%'", $_POST['customTags'])){
		$_POST['customTags']['%SKPickup%'] = "0";
	}
	if(!array_key_exists("'%SKRecent%'", $_POST['customTags'])){
		$_POST['customTags']['%SKRecent%'] = "0";
	}
	if(!array_key_exists("'%SKCallPark%'", $_POST['customTags'])){
		$_POST['customTags']['%SKCallPark%'] = "0";
	}
	if(!array_key_exists("'%SKRetrieve%'", $_POST['customTags'])){
		$_POST['customTags']['%SKRetrieve%'] = "0";
	}
	
	if(!array_key_exists("'%SKForward%'", $_POST['customTags'])){
		$_POST['customTags']['%SKForward%'] = "0";
	}
	if(!array_key_exists("'%SKDND%'", $_POST['customTags'])){
		$_POST['customTags']['%SKDND%'] = "0";
	}
	if(!array_key_exists("'%SKConf%'", $_POST['customTags'])){
		$_POST['customTags']['%SKConf%'] = "0";
	}
	
}

foreach($_POST as $key=>$val){
	if($key != "customTags"){
		$deviceArray[$key] = $_POST[$key];
	}else{
		$tagsArray[$key] = $_POST[$key];
	}
}

$deviceArray['spId'] = $sp = $_SESSION['sp']; 
$deviceArray['groupId'] = $groupId = $_SESSION['groupId']; 
$deviceName = $_POST['deviceName'];
$vdm = new VdmOperations();

if($_SESSION['deviceUpdatedPolycom']){
    $modifyDevices['devices']= $vdm->modifyUserDeviceDetail($deviceArray);
}

if(empty($modifyDevices['devices']['Error'])){
	$modifyResponse['devices']['Success'] = "Success";	
	$isModifiedPolycom = false;
	$isAddedPolycom = false;
	$isDeletedPolycom = false;
	$customTagAddArray = array();
	$customTagDeleteArray = array();
	$customTagModArray = array();
	$i = 0;
	$j = 0;
	
	$newCustomProfile = $tagsArray['customTags']["'%Express_Custom_Profile_Name%'"];
	$newCustomProfile = str_replace("'", "", $newCustomProfile);
	$oldCustomProfile = $_SESSION['device']['allCustomTags']["%Express_Custom_Profile_Name%"];
        
	$isCustomProfileSame = false; 
	if($newCustomProfile  == $oldCustomProfile[0]){
	    $isCustomProfileSame = true;
	}
	
	// Tag Bundle.
	if(isset($_SESSION['device']['allCustomTags']["%Express_Tag_Bundle%"])){
	    $oldTagBundle = explode(";", $_SESSION['device']['allCustomTags']["%Express_Tag_Bundle%"][0]);
	}else{
	    $oldTagBundle[0] = "";
	}
	//$oldTagBundle = isset($_SESSION['device']['allCustomTags']["%Express_Tag_Bundle%"]) ? explode(";", $_SESSION['device']['allCustomTags']["%Express_Tag_Bundle%"][0]) : array();
	$newTagBundle = isset($_POST["deviceMgtTagBundle"]) ? $_POST["deviceMgtTagBundle"] : array();

	$isTagBundleSame = false;
	if (count(array_diff(array_merge($oldTagBundle, $newTagBundle), array_intersect($oldTagBundle, $newTagBundle))) === 0) {
	    $isTagBundleSame = true;
	}
	
	// Tag Bundle end.
	
        
        
        //Code added @ 19 April 2018 If custom profile has changed but tag bundle is still same then only delete and add only 2 tags Express_Custom_Profile_Name and phoneTemplateName
        if(!$isCustomProfileSame)
        {
                $objDBOP = new PolycomDBOperations();
	        $newCustomTagList = array(phoneProfileName,phoneTemplateName);
		foreach($newCustomTagList as $tagKey => $tagValue)
                {
			$deletedTags = $vdm->deleteDeviceCustomTag($sp, $groupId, $_POST['deviceName'], $tagValue);
                }
		// Add default tag.
		$addDefaultTags = $vdm->addDeviceCustomTag($sp, $groupId, $_POST['deviceName'], "%Express_Custom_Profile_Name%", $newCustomProfile);
                $addDefaultTags1 = $vdm->addDeviceCustomTag($sp, $groupId, $_POST['deviceName'], "%phone-template%", $newCustomProfile);
        }
        //end code
        
	
	$newCustomTagList = array();
	
	foreach($tagsArray['customTags'] as $key1=>$val1){
		//remove single quote from tagname
		$tagName = str_replace("'", "", $key1);
		if($tagName == "%Express_Custom_Profile_Name%") {
		    $existingCustomTagVl = isset($_SESSION['device']['allCustomTags']["%Express_Custom_Profile_Name%"]) ? 
		                          $_SESSION['device']['allCustomTags']["%Express_Custom_Profile_Name%"][0] : "None";
		        if($existingCustomTagVl <> $val1){
    		        $deviceProfile["changed"] = "true";
    		        $deviceProfile["tagName"] = "Express_Custom_Profile_Name";
    		        $deviceProfile["value"] = $val1;
		         }
			continue;
		}
		//loop of session to filter which tags has been changed
		//print_r($_SESSION['device']['allCustomTags']);
		foreach($_SESSION['device']['allCustomTags'] as $key2=>$val2){
			if($tagName == $key2){
				if($val1 <> $val2[0]){
		          
					if(!array_key_exists($tagName, $_SESSION['device']['customTags'])){
						$customTagAddArray[$tagName][] = $val1;	
						$modifyTag['tags'][$i][$tagName] = $vdm->addDeviceCustomTag($_SESSION['sp'], $_SESSION['groupId'], $_POST['deviceName'], $tagName, $val1);
						$modifyTag['tags'][$i]["operationType"] = "add";
						$isAddedPolycom = true;
					}else if(isset($_SESSION['systemTagList'][$tagName]) && $val1 == $_SESSION['systemTagList'][$tagName][0]){
					   
						$customTagDeleteArray[$tagName][] = $val1;	
						$modifyTag['tags'][$i][$tagName] = $vdm->deleteDeviceCustomTag($_SESSION['sp'], $_SESSION['groupId'], $_POST['deviceName'], $tagName);
						$modifyTag['tags'][$i]["operationType"] = "delete";
						$isDeletedPolycom = true;
					}else{
						$customTagModArray[$tagName][] = $val1;
						$tagUpdateArray['tagName'] = $tagName;
						$tagUpdateArray['tagValue'] = $val1;
						$tagUpdateArray['spId'] = $sp = $_SESSION['sp'];
						$tagUpdateArray['groupId'] = $groupId = $_SESSION['groupId'];
						$tagUpdateArray['deviceName'] = $deviceName = $_POST['deviceName'];
						if(!$isTagBundleSame) { 
							// all custom tags were deleted, so there is nothing to modify, only add will happen
							$modifyTag['tags'][$i][$tagName] = $vdm->addDeviceCustomTag($_SESSION['sp'], $_SESSION['groupId'], $_POST['deviceName'], $tagName, $val1);
							$modifyTag['tags'][$i]["operationType"] = "addINnfnfnfn";							
						} else  {
							$modifyTag['tags'][$i][$tagName] = $vdm->modfiyCustomTags($tagUpdateArray);
							$modifyTag['tags'][$i]["operationType"] = "modify";
						}
						$isModifiedPolycom = true;
					}
					$i++;
				}
			}
		}
	}
	
	// add the custom profile tags other than processed from vdm for device
	
	if(!$isTagBundleSame && !empty($newTagBundle)) {
	    
	    $objDBOP = new PolycomDBOperations();
	    $customProfileTags = $objDBOP->getAllCustomTagsFromTagBundles($_POST['deviceMgtTagBundle']);
	    $deletedTags = $vdm->deleteDeviceCustomTag($sp, $groupId, $_POST['deviceName'], ExpressTagBundleName);
	    if(isset($newTagBundle) && $newTagBundle[0] != ""){
	        $vdm->addDeviceCustomTag($sp, $groupId, $_POST['deviceName'], ExpressTagBundleName, implode(";", $newTagBundle));
	    }
	    

		/*foreach ($customProfileTags as $customProfileKey => $customProfileValue){
		    if(!array_key_exists($customProfileKey, $deviceTagsArray)) {

		        $addedTags = $vdm->addDeviceCustomTag($sp, $groupId, $_POST['deviceName'], $customProfileKey, $customProfileValue);
			}
		}*/
	}
	//If Tag bundle change only
	if(!$isTagBundleSame)
	{
	    
	    $tagManager = new UserCustomTagManager($deviceName,$deviceType);
	    $existingCustomTag = $tagManager->getUserCustomTagList();
	    
	    
	    /*require_once("/var/www/lib/broadsoft/adminPortal/util/sasUtil.php");
	     $sasUtil = new SasUtil();
	     
	     if(count($newCustomTagList) > 0){
	     foreach ($newCustomTagList as $key => $value){
	     $isSasUser = $sasUtil->isSASUser($value);
	     if($isSasUser){
	     unset($newCustomTagList[$key]);
	     }
	     }
	     }	*/
	    
	    if(isset($diffArray["removed"]) && count($diffArray["removed"]) > 0){
	       
	        $objDBOP = new PolycomDBOperations();
	        
	       
	        $resultMsg = removeDeviceCustomTagsIfTagBundleChanged($deviceType, $deviceName, $tagToBeDeleteIfBundleChanged);
	        if ($resultMsg != "") {
	            $data = $resultMsg;
	        }
	    }
	    
	    if(isset($diffArray["assigned"]) && count($diffArray["assigned"]) > 0){
	        $objDBOP = new PolycomDBOperations();
	        $tagToBeAddIfBundleChanged = $objDBOP->getAllCustomTagsFromTagBundles($diffArray["assigned"]);
	        
	        $diffrencedArray = compareExistingTagsWithBundleTags($existingCustomTag, $tagToBeAddIfBundleChanged);
	       
	        if(isset($diffrencedArray["add"]) && count($diffrencedArray["add"]) > 0){
	            $resultMsg = addDeviceCustomTagsIfBundleChanged($deviceType, $deviceName, $diffrencedArray["add"]);
	            if ($resultMsg != "") {
	                $data = $resultMsg;
	            }
	        }
	        if(isset($diffrencedArray["modify"]) && count($diffrencedArray["modify"]) > 0){
	            foreach ($diffrencedArray["modify"] as $key => $val){
	                $resultMsg = $tagManager->modifyCustomTag($key, $val);
	                if ($resultMsg != "") {
	                    $data = $resultMsg;
	                }
	            }
	        }
	        
	    }
	    
	    /*foreach ($newCustomTagList as $tagKey => $tagValue){
	    
	    $deletedTags = $vdm->deleteDeviceCustomTag($sp, $groupId, $_POST['deviceName'], $tagValue);
	    }
	    if($systemDefaultTagVal != $newCustomProfile){
	    $addDefaultTags = $vdm->addDeviceCustomTag($sp, $groupId, $_POST['deviceName'], phoneTemplateName, $newCustomProfile);
	    
	    }
	    $addDefaultTags = $vdm->addDeviceCustomTag($sp, $groupId, $_POST['deviceName'], "%Express_Custom_Profile_Name%", $newCustomProfile);
	    */
	    
	}
	
	/*if($isModifiedPolycom || $isAddedPolycom || $isDeletedPolycom){
			require_once("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig/rebuildAndResetDeviceConfig.php");
			$objRebuildAndReset = new RebuildAndResetDeviceConfig();
			$respRebuild = $objRebuildAndReset->GroupCPEConfigRebuildDeviceConfig($sp, $groupId, $deviceName);
			if(empty($respRebuild['Error'])){
				$rebuildAndReset = $objRebuildAndReset->GroupCPEConfigResetDevice($sp, $groupId, $deviceName);
			}
	}*/
	$changeStringComplete .= rebuildResetDevice($_POST, $sp, $groupId);
	
	if ($_POST ['rebuildPhoneFiles'] == "true" && $_POST ['resetPhone'] == "true") {
	    $cLUObj->createChangesArray($module = "Rebuild and Reset", $oldValue = "" , $newValue = "Yes");
	} else if ($_POST ['rebuildPhoneFiles'] == "true" && $_POST ['resetPhone'] == "false") {
	    $cLUObj->createChangesArray($module = "Rebuild", $oldValue = "" , $newValue = "Yes");
	} else if ($_POST ['rebuildPhoneFiles'] == "false" && $_POST ['resetPhone'] == "true") {
	    $cLUObj->createChangesArray($module = "Reset", $oldValue = "" , $newValue = "Yes");
	}
}

if( !empty($modifyDevices["devices"]["Error"])){
    $changeStringComplete .= '<tr><td class="errorTableRows" style="background-color: #ac5f5d">Device Not Updated</td><td class="errorTableRows">'.$modifyDevices["devices"]["Error"].' </td></tr>';
}else{
    foreach($deviceDetail as $key => $val){
        if($_SESSION['device']['details'][$key] <> $deviceArray[$key]){

            $changeStringComplete .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">'.$deviceDetail[$key].'</td><td class="errorTableRows"> Successfully Updated </td></tr>';
            
            $cLUObj->createChangesArray($module = $key, $oldValue = $_SESSION['device']['details'][$key], $newValue = $deviceArray[$key]);

        }
    }
}

// if(count($modifyTag['tags']) == 0){
// 	$changeStringComplete .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">'.$deviceName.'</td><td class="errorTableRows">Device Successfully Updated </td></tr>';
// }


if(!empty($deviceProfile) && $deviceProfile["changed"] == "true" && $deviceProfile["value"] == "None"){
    $changeStringComplete .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">Custom Profile</td><td class="errorTableRows">Successfully Updated </td></tr>';
}

if(!$isTagBundleSame) {

    $changeStringComplete .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">Device Management Tag Bundles</td><td class="errorTableRows">Successfully Updated </td></tr>';
    
    $oldTagBundleData = "";
    $newTagBundleData = "";    
    foreach($oldTagBundle as $key => $value) {
        $oldTagBundleData .= $value. " ";
    }
    foreach($newTagBundle as $key => $value) {
        $newTagBundleData .= $value. " ";
    }
    
    $cLUObj->createChangesArray($module = "Device Management Tag Bundles", $oldValue = str_replace(" ", ",", trim($oldTagBundleData)), $newValue = str_replace(" ", ",", trim($newTagBundleData)));
    
}
if(!$isCustomProfileSame){
    $changeStringComplete .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">Phone Profile</td><td class="errorTableRows">Successfully Updated </td></tr>';
    
    $cLUObj->createChangesArray($module = "Custom Profile", $oldValue = $oldCustomProfile[0], $newValue = $newCustomProfile);

}

foreach($modifyTag['tags'] as $key=>$val){
	foreach($val as $key1=>$val1){
		foreach($_SESSION['device']['allCustomTags'] as $key2=>$val2){
			if($key1 == $key2){
				if($val1 <> $val2[0]){
					if(!empty($val1['Error'])){
						//$modifyDevices['tags']['Error'][$key1] = $val1['Error'];
						$changeStringComplete .= '<tr><td class="errorTableRows" style="background-color: #ac5f5d">'.$deviceTagsArray[$key1].'</td><td class="errorTableRows">'.$val1['Error'].' </td></tr>';
					}else{
						//$modifyDevices['tags']['Success'][$key1] = $val1['Success'];

						$changeStringComplete .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">'.$deviceTagsArray[$key1].'</td><td class="errorTableRows">Successfully Updated </td></tr>';

					   
						$newCustomTags = isset($_POST["customTags"]["'".$key1."'"]) ? $_POST["customTags"]["'".$key1."'"] : $_POST["customTags"][$key1];
						$cLUObj->createChangesArray($module = $deviceTagsArray[$key1], $oldValue = $val2[0], $newValue = $newCustomTags);

					}
				}
			}
		}
	}
}

/* Logger */
$module = "Device Management Modification";
$cLUObj->changeLogModifyUtility($module, $_POST['deviceName'], $tableName = "deviceMgmtModChanges");

echo $changeStringComplete;

?>
