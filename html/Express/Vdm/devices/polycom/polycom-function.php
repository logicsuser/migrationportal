<?php 

$vdm = new VdmOperations();
$_SESSION['device'] = array();
//order ascending
function compareByName($a, $b) {
	return strcmp($a["order"], $b["order"]);
}

//getDeviceDetails function
$getDeviceDetails = $vdm->getUserDeviceDetail($sp, $groupId, $deviceName);
//echo "<pre>"; print_r($getDeviceDetails); die;
if(empty($getDeviceDetails['Error'])){
	
	//put deviceDetails in session
	$deviceDetails = $_SESSION['device']['details'] = $getDeviceDetails['Success'];
	if(isset($deviceDetails ["numberOfPorts"]) && $deviceDetails ["numberOfPorts"] == ""){
		$deviceDetails ["numberOfPorts"] ="12";
	}
	
	//get the custom tags added in express
	$getCustomTags = $vdm->getDeviceCustomTags($sp, $groupId, $deviceName);
	if(empty($getCustomTags['Error'])){
		$customTags = $getCustomTags['Success'];
		$_SESSION['device']['customTags'] = $customTags;
	}
}

$sysOperations = new sysLevelDeviceOperations();
$tagsetName = $sysOperations->getTagSetNameByDeviceType($deviceType, $ociVersion);

if(count($tagsetName["Success"]) > 0){
	$systemTagList = $sysOperations->getDefaultTagListByTagSet($tagsetName["Success"]);
	$_SESSION['systemTagList'] = $systemTagList["Success"];
}

if(count($customTags) > 0 || count($systemTagList["Success"]) > 0){
	$customTags = $vdm->mergeArray($customTags, $systemTagList["Success"]);	
	//echo "<pre>"; print_r($customTags);
	$customTags = $vdm->createPortTags($customTags, $deviceDetails["numberOfPorts"]);
	//echo "<pre>"; print_r($customTags);
	if(!array_key_exists("%Express_Custom_Profile_Name%", $customTags)){
		$customTags['%Express_Custom_Profile_Name%'][0]= "None";
	}
	$_SESSION['device']['allCustomTags'] = $customTags;
}

for ($i=0; $i <=16; $i++){
    if(!array_key_exists("%lineKey-".$i."-index%", $_SESSION['device']['allCustomTags'])){
        $_SESSION['device']['allCustomTags']["%lineKey-".$i."-index%"] = array(0 => 0);
    }
    
}

//get primary user details 
function searchForKey($searchKey, $searchValue, $array) {
	$returnVal = "";
	foreach ($array as $key => $value) {
		
		$result = $value[$searchKey];
		if($result == $searchValue)
		{
			$returnVal = $value;
			break;
		}
	}
	return $returnVal;
}
$getUserDevice = $vdm->getAllUserOfDevice($sp, $groupId, $deviceName);
$userDevice = "";
if (empty($getUserDevice['Error'])) {
	if (count($getUserDevice['Success']) > 0) {
		// sort user array by order field
		usort($getUserDevice['Success'], 'compareByName');
		$userDevice = $getUserDevice['Success'];
	}
}

// get the registration info for primary user
$primaryUser = array();
if (!empty($userDevice)) {
	if (count($userDevice) > 0) {
		$primaryUser = searchForKey("primaryLinePort", "true", $userDevice);
	}
}

?>