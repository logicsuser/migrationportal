<?php
require_once ("/var/www/html/Express/config.php");
require_once ("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
require_once ("/var/www/lib/broadsoft/adminPortal/devices/deviceTypeManagement.php");
$devobj = new DeviceTypeManagement();
$vdmArray = $devobj->getVDMTemplate();

$alDeviceType = array(
    "audioCodesMP",
    "algoAlerter",
    "algoDoorPhone",
    "algoStrobe",
    "polycomVVX310",
    "polycomVVX410",
    "polycomVVX500",
    "polycomVVX600"
);
$deviceNameList = array();

if ($_POST["deviceType"] == "all") {
    $deviceType = $alDeviceType;
} else {
    $deviceType = explode("&&", $_POST["deviceType"]);
}

    //Code added @ 16 July 2019
    $whereCndtn = "";
    if($_SESSION['cluster_support']){
        $selectedCluster = $_SESSION['selectedCluster'];
        $whereCndtn = " AND clusterName ='$selectedCluster' ";
    }
    //End code

foreach ($deviceType as $key => $deviceTypeVal) {
    $deviceList = array();
    $keyName = array_search ($deviceTypeVal, $vdmArray);
    $query = "SELECT deviceType from systemDevices where vdmTemplate = '" . $keyName . "' $whereCndtn ";
    //print_r($query);
    $qwr = $db->query($query);
    while ($r = $qwr->fetch()) {
        $deviceList[] = $r["deviceType"];
    }
    
    $deviceNameListForDevice = deviceGetList($deviceList);
    array_push($deviceNameList, $deviceNameListForDevice);
}
// create Device Name Select Optins
makeSelectOptions($deviceNameList);

function deviceGetList($deviceList)
{
    $returnValue = array();
    foreach ($deviceList as $key => $value) {
        $deviceOps = new DeviceOperations();
        $deviceDetail = $deviceOps->GroupAccessDeviceGetListRequest($_SESSION["sp"], $_SESSION["groupId"], $deviceType = $value);
        if ($deviceDetail["Error"] == "") {
            if ($deviceDetail["Success"] != "") {
                $j = 0;
                foreach ($deviceDetail["Success"] as $k => $v) {
                    $returnValue[$j][$value] = strval($v["deviceName"]);
                    $j ++;
                }
            }
        }
    }
    return $returnValue;
}

/*function makeSelectOptions($deviceNameList)
 {
 $selectBoxOptions = "";
 foreach ($deviceNameList as $key => $value) {
 foreach ($value as $key1 => $val1) {
 foreach ($val1 as $k => $v) {
 $selectBoxOptions .= "<option value='". $k . "||" . $v . "'>" . $v . " </option>";
 }
 }
 }
 
 echo $selectBoxOptions;
 }*/

function makeSelectOptions($deviceNameList){
    $s = 0;
    foreach($deviceNameList as $key=>$val){
        
        foreach($val as $key1=>$val1){
            
            foreach($val1 as $K => $V){
                $deviceType = $K;
                $deviceArray[$s]['deviceType'] = $deviceType;
                $deviceArray[$s]['deviceName'] = $V;
                $s++;
            }
        }
    }
    uasort($deviceArray, function($a, $b) {
        return strnatcasecmp($a['deviceName'], $b['deviceName']);
    });
        $searchDevice = array();
        //$selectBoxOptions = "";
        foreach ($deviceArray as $key => $value) {
            
            $deviceName = $value['deviceName'];
            $deviceType = $value['deviceType'];
            $deviceInfo = $value['deviceType'] . "||" . $value['deviceName'] ;
            
            $searchDevice[] = array("deviceName" => $deviceName,
                "deviceType" => $deviceType,
                "deviceInfo" => $deviceInfo);
            
            //$selectBoxOptions .= "<option value='". $value["deviceType"] . "||" . $value["deviceName"] . "'>" . $value["deviceName"]."</option>";
        }
        echo json_encode($searchDevice);
        //echo $selectBoxOptions;
}
?>