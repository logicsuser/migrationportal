<?php
	//error_reporting(E_ALL);
	ini_set("display_errors", "1");

	//server-specific configuration variables
	require_once("/var/www/html/Express/config.php");
	require_once("/var/www/lib/broadsoft/ServerConnectionUtil.php");
	$serConnUtil = new ServerConnectionUtil();
	$checkToDefaultServer = isset($_SESSION['server_connection_detail']['current_server_connection']) && $_SESSION['server_connection_detail']['current_server_connection'] == "alternate" ? false : true;
	
	function openBroadsoftHeader($sessionid)
	{
		$header = "<BroadsoftDocument protocol=\"OCI\" xmlns=\"C\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
		$header .= "<sessionId xmlns=\"\">" . $sessionid . "</sessionId>";
		return $header;
	}
	
	function closeBroadSoftHeader()
	{
		return "</BroadsoftDocument>\n";
	}
	
	function openCommand($commandType)
	{
		$header .= "<command xsi:type=\"" . $commandType . "\" xmlns=\"\">";
		return $header;
	}
	
	function closeCommand()
	{
		return "</command>\n";
	}

	function xmlHeader($sessionid, $commandType)
	{
	    checkServerConnErrorIsExist();
		
		$header = "<BroadsoftDocument protocol=\"OCI\" xmlns=\"C\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
		$header .= "<sessionId xmlns=\"\">" . $sessionid . "</sessionId>";
//		if (strlen($commandType) > 0)
//		{
			$header .= "<command xsi:type=\"" . $commandType . "\" xmlns=\"\">";
//		}
		return $header;
	}

	
	
	function xmlFooter()
	{
		return "</command>\n</BroadsoftDocument>\n";
	}

	function buildCommandAuthenticationRequest($sessionid, $bwuserid)
	{
		$xmlstr = xmlHeader($sessionid, "AuthenticationRequest");
		$xmlstr .= "<userId>" . $bwuserid . "</userId>" . "\n";
		$xmlstr .= xmlFooter();
		return $xmlstr;
	}

	try
	{                         
                $opts = array(
			'http' => array(
				'user_agent'=>'PHPSoapClient'
		    ),
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false
			)
		);

		$context = stream_context_create($opts);

		$soapClientOptions = array(
			'stream_context' => $context,
			'cache_wsdl' => WSDL_CACHE_NONE,
			"exceptions" => 0,
			"trace" => 1
		);

		libxml_disable_entity_loader(false);
		
		$soapClientOptions["exceptions"] = 1;
		
        try {
            
            $serverConnectionStatus = $serConnUtil->checkServerConnectionStatus();
            
            if($serverConnectionStatus["defaultSerIsCononected"] == "true" && $checkToDefaultServer) {
                $client = new SoapClient("$bwProtocol://" . $bw_ews . ":$bwPort/webservice/services/ProvisioningService?wsdl", $soapClientOptions); //Code added @ 10 May 2018
                $client1 = new SoapClient("$bwProtocol://" . $bw_ews . ":$bwPort/webservice/services/ProvisioningService?wsdl", $soapClientOptions); //Code added @ 10 May 2018
            } else if($serverConnectionStatus["alternateSerIsCononected"] == "true") {
                $bwProtocol = $GLOBALS['serversDetail']->alternateAppServer->bwProtocol;
                $bw_ews = $GLOBALS['serversDetail']->alternateAppServer->ip;
                $bwPort = $GLOBALS['serversDetail']->alternateAppServer->port;
                $bwuserid = $GLOBALS['serversDetail']->alternateAppServer->userId;
                $bwpasswd = $GLOBALS['serversDetail']->alternateAppServer->password;
                $client = $serverConnectionStatus["client"];
            }
            
		    //$client = new SoapClient("http://" . $bw_ews . "/webservice/services/ProvisioningService?wsdl", $soapClientOptions);
            if($client) {
        		$charid = md5(uniqid(rand(), true));
        		$sessionid = substr($charid, 0, 32);
        		$nonce = "";
        		$response = $client->processOCIMessage(array("in0" => buildCommandAuthenticationRequest($sessionid, $bwuserid)));
                        
        		if ($response and $response->processOCIMessageReturn and substr($response->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
        		{
        			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        			if ($xml->command["type"] != "Error")
        			{
        				foreach ($xml->children() as $child)
        				{
        					$temp = $child->getName();
        					if (!strcmp($temp, "sessionId"))
        					{
        						$sessionId = $child;
        					}
        					foreach ($child->children() as $grandson)
        					{
        						$temp = $grandson->getName();
        						if (!strcmp($temp, "nonce"))
        						{
        							$nonce = $grandson;
        						}
        						foreach ($grandson->children() as $greatson)
        						{
        							$temp = $greatson->getName();
        							foreach ($greatson->children() as $ggson)
        							{
        								$temp = $ggson->getName();
        							}
        						}
        					}
        				}
        			}
        		}
        		
                        //$S1 = $bwpasswd;
        		$S1 = sha1($bwpasswd);
        		$S2 = $nonce . ":" . $S1;
        		$enc_pass = md5($S2);
        
        		$xmlinput = xmlHeader($sessionid, "LoginRequest14sp4");
        		$xmlinput .= "<userId>" . $bwuserid . "</userId>";
        		$xmlinput .= "<signedPassword>" . $enc_pass . "</signedPassword>";
        		$xmlinput .= xmlFooter();
        
        		$response = $client->processOCIMessage(array("in0" => $xmlinput));
        
        		if ($response and $response->processOCIMessageReturn and substr($response->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
        		{
        			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        			if ($xml->command["type"] == "Error")
        			{
        			}
        		}
        		else
        		{
        			echo "No response at all from server. <br>";
        		}
        		foreach ($xml->children() as $child)
        		{
        			$temp = $child->getName();
        			if (!strcmp($temp, "sessionId"))
        			{
        				$sessionId = $child;
        			}
        			foreach ($child->children() as $grandson)
        			{
        				$temp = $grandson->getName();
        
        				foreach ($grandson->children() as $greatson)
        				{
        					$temp = $greatson->getName();
        					foreach ($greatson->children() as $ggson)
        					{
        						$temp = $ggson->getName();
        					}
        				}
        			}
        		}
            } else {
                if($_SESSION['superUser'] != "2") {
                    echo '<script type="text/javascript"> window.location.href = "/Express/index.php"; alert("Connection with Feature Server has failed. Please contact Express Administrator."); </script>';				
                }
            }
        } catch (SoapFault $E) {
//             echo $E->faultstring;
        }
	}
	catch (Exception $e)
	{
		$code = $e->getCode();
		$str = $e->getMessage();
		exit(1);
	}

if(!function_exists("http_post_fields")) {

	function http_post_fields($url, $data, $headers=null) {
		$data = http_build_query($data);
		$opts = array('http' => array('method' => 'POST', 'content' => $data));

		if($headers) {
			$opts['http']['header'] = $headers;
		}
		$st = stream_context_create($opts);
		$fp = fopen($url, 'rb', false, $st);

		if(!$fp) {
			return false;
		}
		return stream_get_contents($fp);
	}

}

function checkServerConnErrorIsExist() {
    $serConnUtil = new ServerConnectionUtil();
    $res = $serConnUtil->checkServerConnectionStatus();
    if( $res["statusMessage"] != "") {
        print_r($res["statusMessage"]);     /* print error if any server error exist */
    }
}

?>
