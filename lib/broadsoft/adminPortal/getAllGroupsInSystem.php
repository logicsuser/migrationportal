<?php
/**
 * Created by Karl.
 * Date: 9/24/2016
 */
    $xmlinput = xmlHeader($sessionid, "GroupGetListInSystemRequest");
    $xmlinput .= xmlFooter();

    $response = $client->processOCIMessage(array("in0" => $xmlinput));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

    $a = 0;
    if (isset($xml->command->groupTable->row))
    {
        foreach ($xml->command->groupTable->row as $key => $value)
        {
            $allGroupsInSystem[$a]["groupId"] = strval($value->col[0]); // group ID
            $allGroupsInSystem[$a]["sp"] = strval($value->col[3]);      // organization ID (SP or Enterprise)
            $a++;
        }
    }
?>
