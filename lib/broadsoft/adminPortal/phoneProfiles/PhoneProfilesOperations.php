<?php 
require_once("/var/www/lib/broadsoft/adminPortal/customTagOperations/UserCustomTagOperations.php");

class PhoneProfilesOperations{
    
    public static $restrictedPhoneProfiles = array('SAS');
    const EXPRESS_CUSTOM_PROFILE_NAME = "%Express_Custom_Profile_Name%";
    const EXPRESS_PHONE_PROFILE_NAME = "%phone-template%";
    const PREVIOUS_EXPRESS_CUSTOM_PROFILE_NAME = "%Previous_Express_Custom_Profile_Name%";
    const SAS_PHONE_PROFILE_NAME = "SAS";
    
    function getDevicesFromSystemLevel($groupId, $deviceType, $phoneProfileSearchlevel, $spName) {
        global $sessionid, $client;
        $responseData ["Error"] = array();
        $responseData ["ErrorData"] = array();
        $responseData ["Success"] = array();
        $responseData ["SuccessData"] = array();
        
        $xmlinput = xmlHeader ( $sessionid, "SystemAccessDeviceGetAllRequest" );
        
        if(!empty($groupId) && $groupId != ""){
            $xmlinput .= "<searchCriteriaGroupId>";
            $xmlinput .= "<mode>Contains</mode>";
            $xmlinput .= "<value>".$groupId."</value>";
            $xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
            $xmlinput .= "</searchCriteriaGroupId>";
        }
        
        if(!empty($deviceType) && $deviceType != ""){
            $xmlinput .= "<searchCriteriaExactDeviceType><deviceType>".$deviceType."</deviceType></searchCriteriaExactDeviceType>";
        }
        
        $xmlinput .= xmlFooter ();
        
        $response = $client->processOCIMessage ( array ("in0" => $xmlinput) );
        $xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
       
        if (readErrorXmlGenuine($xml) != "") {
            $responseData ["Error"] = "Error";
            $responseData ["ErrorData"] = strval($xml->command->detail);
        }else{
            $responseData ["Success"] = "Success";
            
            if(count($xml->command->accessDeviceTable->row) > 0){
                $i = 0;
                foreach ($xml->command->accessDeviceTable->row as $key => $value){
                    if($phoneProfileSearchlevel == "enterprise"){
                        if($value->col[0] == trim($spName) ){
                            $responseData ["SuccessData"][$i]["spName"] = strval($value->col[0]);
                            $responseData ["SuccessData"][$i]["groupId"] = strval($value->col[2]);
                            $responseData ["SuccessData"][$i]["deviceName"] = strval($value->col[3]);
                            $responseData ["SuccessData"][$i]["deviceType"] = strval($value->col[4]);
                            $responseData ["SuccessData"][$i]["netAddress"] = strval($value->col[5]);
                            $responseData ["SuccessData"][$i]["macAddress"] = strval($value->col[6]);
                        }
                    }else{
                        $responseData ["SuccessData"][$i]["spName"] = strval($value->col[0]);
                        $responseData ["SuccessData"][$i]["groupId"] = strval($value->col[2]);
                        $responseData ["SuccessData"][$i]["deviceName"] = strval($value->col[3]);
                        $responseData ["SuccessData"][$i]["deviceType"] = strval($value->col[4]);
                        $responseData ["SuccessData"][$i]["netAddress"] = strval($value->col[5]);
                        $responseData ["SuccessData"][$i]["macAddress"] = strval($value->col[6]);
                    }   
                    $i++;
                }
            }
            
            
        }
        return $responseData;
    }
    
    function getDeviceCustomTags($providerid, $groupId, $deviceName)
    {
        global $sessionid, $client;
        $userAssignedTags = array();
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        // get custom tags from back-end
        $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagGetListRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($providerid) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        }
        else
        {
            if(isset($xml->command->deviceCustomTagsTable->row)) {
                foreach ($xml->command->deviceCustomTagsTable->row as $key => $value){
                    $userAssignedTags[strval($value->col[0])] = strval($value->col[1]);
                }
            } else {
                $userAssignedTags["Success"] = array();
            }
            
            $returnResponse["Success"] = $userAssignedTags;
        }
        return $returnResponse;
    }
    
    
    public function SASPhoneProfileCustomTagAddOperation($providerid, $groupId, $deviceType, $deviceName, $customProfile, $ociVersion) {
        global $db;
        require_once ("/var/www/lib/broadsoft/adminPortal/vdm/sysLevelDeviceOperations.php");
        $uCTO = new UserCustomTagOperations();
        $syslevelObj = new sysLevelDeviceOperations();
        $setName = $syslevelObj->getTagSetNameByDeviceType($deviceType, $ociVersion);
        $systemDefaultTagVal = "";
        if($setName["Success"] != "" && !empty($setName["Success"])){
            $defaultSettags = $syslevelObj->getDefaultTagListByTagSet($setName["Success"]);
            $phoneTemplateName = "%phone-template%";
            if(array_key_exists($phoneTemplateName, $defaultSettags["Success"])){
                $systemDefaultTagVal = $defaultSettags["Success"]["%phone-template%"][0];
            }
        }
        if($customProfile != "None" && !empty($customProfile)) {
            
            if($systemDefaultTagVal != $customProfile){
                $response = $uCTO->addDeviceCustomTag($providerid, $groupId, $deviceName, self::EXPRESS_PHONE_PROFILE_NAME, $customProfile);
                if( !empty($response["Error"]) ) {
                    return $response["Error"];
                }
            }
            
            $response = $uCTO->addDeviceCustomTag($providerid, $groupId, $deviceName, self::EXPRESS_CUSTOM_PROFILE_NAME, $customProfile);
            if( !empty($response["Error"]) ) {
                return $response["Error"];
            }
            
        }
    }
    
    public function getPhoneProfileValueByName($phoneProfileName) {
        global $db;
        $params[] = $phoneProfileName;
        $stmt = $db->prepare("SELECT * FROM phoneProfilesLookup where phoneProfile = ?");
        $stmt->execute($params);
        $filtered_users = "";
        if($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $filtered_users = $row;
        }
        
        return $filtered_users;
    }
    
    public function getPhoneProfileCustomTag($providerid, $groupId, $deviceName) {
        $phoneProfileData = array();
        $uCTO = new UserCustomTagOperations();
        $allUserCustomTags = $uCTO->getUserCustomTagsWithValue($providerid, $groupId, $deviceName);
        if( empty($allUserCustomTags["Error"]) ) {
            if( isset($allUserCustomTags["Success"][self::EXPRESS_CUSTOM_PROFILE_NAME]) ) {
                $phoneProfileData[self::EXPRESS_CUSTOM_PROFILE_NAME] = $allUserCustomTags["Success"][self::EXPRESS_CUSTOM_PROFILE_NAME];
                $phoneProfileData[self::PREVIOUS_EXPRESS_CUSTOM_PROFILE_NAME] = $allUserCustomTags["Success"][self::EXPRESS_CUSTOM_PROFILE_NAME];
            }
            if( isset($allUserCustomTags["Success"][self::EXPRESS_PHONE_PROFILE_NAME]) ) {
                $phoneProfileData[self::EXPRESS_PHONE_PROFILE_NAME] = $allUserCustomTags["Success"][self::EXPRESS_PHONE_PROFILE_NAME];
            }
        }
        return $phoneProfileData;
    }
    
    public function getPhoneProfileCustomTagForRemoveFromSAS($providerid, $groupId, $deviceName) {
        $phoneProfileData = array();
        $uCTO = new UserCustomTagOperations();
        $allUserCustomTags = $uCTO->getUserCustomTagsWithValue($providerid, $groupId, $deviceName);
        if( empty($allUserCustomTags["Error"]) ) {
            if( isset($allUserCustomTags["Success"][self::EXPRESS_CUSTOM_PROFILE_NAME]) ) {
                $phoneProfileData[self::EXPRESS_CUSTOM_PROFILE_NAME] = $allUserCustomTags["Success"][self::EXPRESS_CUSTOM_PROFILE_NAME];
            }
            if( isset($allUserCustomTags["Success"][self::PREVIOUS_EXPRESS_CUSTOM_PROFILE_NAME]) ) {
                $phoneProfileData[self::PREVIOUS_EXPRESS_CUSTOM_PROFILE_NAME] = $allUserCustomTags["Success"][self::PREVIOUS_EXPRESS_CUSTOM_PROFILE_NAME];
            }
            if( isset($allUserCustomTags["Success"][self::EXPRESS_PHONE_PROFILE_NAME]) ) {
                $phoneProfileData[self::EXPRESS_PHONE_PROFILE_NAME] = $allUserCustomTags["Success"][self::EXPRESS_PHONE_PROFILE_NAME];
            }
        }
        return $phoneProfileData;
    }
    
    
}

?>