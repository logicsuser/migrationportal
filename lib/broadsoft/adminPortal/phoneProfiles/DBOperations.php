<?php 
class PhoneProfilesDBOperations {
    
    function getCustomProfiles($tableName) {
        
        global $db;
        $query = "select customProfileName from " . $tableName ." order by customProfileName";
        $results = $db->query ( $query );
        $profiles = array ();
        $i = 0;
        while ( $row = $results->fetch () ) {
            if (! in_array ( $profile = $row ["customProfileName"], $profiles )) {
                $profiles [$i ++] = $profile;
            }
        }
        
        return $profiles;
    }
    
    
    function getDeviceType($tableName) {
        
        global $db;
        $query = "select deviceType from " . $tableName ." order by deviceType";
        $results = $db->query ( $query );
        $devieType = array ();
        $i = 0;
        while ( $row = $results->fetch () ) {
            if (! in_array ( $deviceTypeInd = $row ["deviceType"], $devieType )) {
                $devieType [$i ++] = $deviceTypeInd;
            }
        }
        
        return $devieType;
    }
    
    function getSelectedDeviceTypeProfiles($tableName, $deviceType){
        
        global $db;
        // $query = "select deviceType from " . $tableName ." order by deviceType";
        $query = "select customProfileName from " . $tableName ." where deviceType='" . $deviceType . "' order by customProfileName";
       // global $db;
        //$query = "select customProfileName from " . $tableName ." order by customProfileName";
        $results = $db->query ( $query );
        $profiles = array ();
        $i = 0;
        while ( $row = $results->fetch () ) {
            if (! in_array ( $profile = $row ["customProfileName"], $profiles )) {
                $profiles [$i ++] = $profile;
            }
        }
        
        return $profiles;
    }
    
}
?>