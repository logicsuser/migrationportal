<?php

require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
class UserCustomTagOperations{
	
	
	function groupAccessDeviceDeleteCustomTag($providerid, $groupId, $deviceName, $tagName)
	{
	    global $sessionid, $client;
	    $returnResponse["Error"] = "";
	    $returnResponse["Success"] = "";
	    
	    $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceCustomTagDeleteListRequest");
	    $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($providerid) . "</serviceProviderId>";
	    $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
	    $xmlinput .= "<deviceName>". $deviceName."</deviceName>";
	    $xmlinput .= "<tagName>". $tagName ."</tagName>";
	    $xmlinput .= xmlFooter();
	    
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    $errMsg = readErrorXmlGenuine($xml);
	    if($errMsg != "") {
	        $returnResponse["Error"] = $errMsg;
	    }
	    else
	    {
	        $returnResponse["Success"] = $xml;
	    }
	    return $returnResponse;
	}
	
	function addDeviceCustomTag($providerid, $groupId, $deviceName, $tagName, $tagValue)
	{
	    global $sessionid, $client;
	    $returnResponse["Error"] = "";
	    $returnResponse["Success"] = "";
	    
	    $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagAddRequest");
	    $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($providerid) . "</serviceProviderId>";
	    $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
	    $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
	    $xmlinput .= "<tagName>" . $tagName . "</tagName>";
	    if(!empty($tagValue) || $tagValue === "0"){
	    	$xmlinput .= "<tagValue>" . $tagValue . "</tagValue>";
	    }
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    $errMsg = readErrorXmlGenuine($xml);
	    if($errMsg != "") {
	        $returnResponse["Error"] = $errMsg;
	    }
	    else
	    {
	        $returnResponse["Success"] = $xml;
	    }
	    
	    return $returnResponse;
	}
	
	function getUserCustomTags($providerid, $groupId, $deviceName)
	{
	    global $sessionid, $client;
	    $userAssignedTags = array();
	    $returnResponse["Error"] = "";
	    $returnResponse["Success"] = "";
	    // get custom tags from back-end
	    $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagGetListRequest");
	    $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($providerid) . "</serviceProviderId>";
	    $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
	    $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
	    $xmlinput .= xmlFooter();
	    
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    $errMsg = readErrorXmlGenuine($xml);
	    if($errMsg != "") {
	        $returnResponse["Error"] = $errMsg;
	    }
	    else
	    {
	        if(isset($xml->command->deviceCustomTagsTable->row)) {
	            foreach ($xml->command->deviceCustomTagsTable->row as $key => $value){
	                $userAssignedTags["tagName"][] = strval($value->col[0]);
	            }
	        } else {
	            $userAssignedTags["tagName"] = array();
	        }
	        
	        $returnResponse["Success"] = $userAssignedTags["tagName"];
	    }
	    return $returnResponse;
	}
	
	function getUserCustomTagsWithValue($providerid, $groupId, $deviceName)
	{
	    global $sessionid, $client;
	    $userAssignedTags = array();
	    $returnResponse["Error"] = "";
	    $returnResponse["Success"] = "";
	    // get custom tags from back-end
	    $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagGetListRequest");
	    $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($providerid) . "</serviceProviderId>";
	    $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
	    $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
	    $xmlinput .= xmlFooter();
	    
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    $errMsg = readErrorXmlGenuine($xml);
	    if($errMsg != "") {
	        $returnResponse["Error"] = $errMsg;
	    }
	    else
	    {
	        if(isset($xml->command->deviceCustomTagsTable->row)) {
	            foreach ($xml->command->deviceCustomTagsTable->row as $key => $value){
	                $userAssignedTags["tagName"][strval($value->col[0])] = strval($value->col[1]);
	            }
	        } else {
	            $userAssignedTags["tagName"] = array();
	        }
	        
	        $returnResponse["Success"] = $userAssignedTags["tagName"];
	    }
	    return $returnResponse;
	}
}
