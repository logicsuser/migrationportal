<?php
require_once("/var/www/lib/broadsoft/adminPortal/customTagOperations/UserCustomTagOperations.php");

function deleteTagsTagBundleChange($db, $providerid, $groupId, $deviceName, $deleteTags){
    $customTagObj = new UserCustomTagOperations();
    if(count($deleteTags) > 0){
        foreach ($deleteTags as $key => $value){
            $customTagObj->groupAccessDeviceDeleteCustomTag($providerid, $groupId, $deviceName, $key);
        }
    }
    
}

function addExpressCutomTags($db, $providerid, $groupId, $deviceName, $deviceType, $customProfile, $defaultTag) {
	
	$customTagObj = new UserCustomTagOperations();
	
	$customTagObj->addDeviceCustomTag($providerid, $groupId, $deviceName, "%Express_Custom_Profile_Name%", $customProfile);
	if($defaultTag != $customProfile){
	    $customTagObj->addDeviceCustomTag($providerid, $groupId, $deviceName, "%phone-template%", $customProfile);
	}
	
}

function deleteExpressCutomTags($db, $providerid, $groupId, $deviceName, $deviceType, $customProfile, $defaultTag) {
	
	$customTagObj = new UserCustomTagOperations();
	
	$customTagObj->groupAccessDeviceDeleteCustomTag($providerid, $groupId, $deviceName, "%Express_Custom_Profile_Name%");
	$customTagObj->groupAccessDeviceDeleteCustomTag($providerid, $groupId, $deviceName, "%phone-template%");
	
	// finally add custom tag with the custom profile name for the reporting purpose
	if($customProfile != ""){
	    addExpressCutomTags($db, $providerid, $groupId, $deviceName, $deviceType, $customProfile, $defaultTag);
	}
	
}

function addTagBundlesCustomTags($db, $providerid, $groupId, $deviceName, $tagBundlesTags){
    $customTagObj = new UserCustomTagOperations();
    if(count($tagBundlesTags) > 0){
        foreach ($tagBundlesTags as $key => $value){
            $customTagObj->addDeviceCustomTag($providerid, $groupId, $deviceName, $key, $value);
        }
    }
    
}

function addExpressTagBundlesDefaultCutomTags($db, $providerid, $groupId, $deviceName, $tagBundle) { 
    $customTagObj = new UserCustomTagOperations();
    $customTagObj->addDeviceCustomTag($providerid, $groupId, $deviceName, "%Express_Tag_Bundle%", implode(";", $tagBundle));
}













