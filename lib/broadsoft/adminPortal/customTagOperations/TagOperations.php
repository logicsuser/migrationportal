<?php 

class TagOperations {
    
    private $tagBundle = array();
    
    function addCustomTag($deviceName, $tagData) {
        $addedCustomTags = array();
        foreach($tagData as $tagKey => $tagVal) {
            $isTagAdded = addDeviceCustomTag($deviceName, $tagVal["tagName"], $tagVal["tagValue"]);
            if($isTagAdded) {
                $addedCustomTags[$tagKey]['tagName'] = $tagVal["tagName"];
                $addedCustomTags[$tagKey]['status'] = "Success";
            }
            
        }
        
        return $addedCustomTags;
    }
    
    
    function deleteCustomTag($deviceName, $tagData) {
        $deletedCustomTags = array();
        foreach($tagData as $tagKey => $tagVal) {
            $isTagDeleted = groupAccessDeviceDeleteCustomTag($deviceName, $tagName);
            if($isTagDeleted) {
                $deletedCustomTags[$tagKey]['tagName'] = $tagVal["tagName"];
                $deletedCustomTags[$tagKey]['status'] = "Success";
            }
            
        }
        
        return $deletedCustomTags;
    }
    
    
   
    
    function getTagBundleData() {
        $tagBundleData = array("nhForward", "Forward", "N-Way", "Pickup", "TCP");
        $this->tagBundle = $tagBundleData;
        return $this->tagBundle;
    }
}





$gBD = new TagOperations;
$tagBundle = $gBD->getTagBundleData();
// print_r(json_encode($tagBundle));




// $custTagList = json_decode($_POST["custTagList"]);
// $gBD = new TagOperations;

// if(isset($custTagList["addTagList"])) {
//     $returnData = $gBD->addCustomTag($_SESSION["devName"], $custTagList["addTagList"]);
// }

// if(isset($custTagList["deleteTagList"])) {
//     $gBD->deleteCustomTag($_SESSION["devName"], $custTagList["deleteTagList"]);
// }

?>