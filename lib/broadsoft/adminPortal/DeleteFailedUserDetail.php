<?php 
require_once("/var/www/html/Express/config.php");

class DeleteFailedUserDetail {
    
    
    function deleteUser($userId)
    {
        global $sessionid, $client;
        // Deleting User in back-end
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "UserDeleteRequest");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine ( $xml );
        if ($errMsg != "") {
            $returnResponse ["Error"] = $errMsg;
        }else{
            $returnResponse["Success"] = "User has been deleted";
        }
        return $returnResponse;
    }
    
    
    function deviceDeleteRequest($spId, $groupId, $deviceName)
    {
        global $sessionid, $client;
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceDeleteRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine ( $xml );
        if ($errMsg != "") {
            $returnResponse ["Error"] = $errMsg;
        }else{
            $returnResponse["Success"] = "Device has been deleted";
        }
        return $returnResponse;
    }
    
    function deleteSurgeMail($emailAddress, $emailPassword, $surgemailUsername, $surgemailPassword, $surgemailURL)
    {
        $surgemailPost = array ("cmd" => "cmd_user_login",
            "lcmd" => "user_delete",
            "show" => "simple_msg.xml",
            "username" => $surgemailUsername,
            "password" => $surgemailPassword,
            "lusername" => $emailAddress,
            "lpassword" => $emailPassword,
            "uid" => isset($userid) ? $userid : "");
        $result = http_post_fields($surgemailURL, $surgemailPost);
        return $result;
    }
}


?>