<?php
/**
 * Created by Sollogics.
 */
    class GroupCallPickup
    {
        function addGroupCallPickupAddInstance($name) {

            global $sessionid, $client;

            $xmlinput  = xmlHeader($sessionid, "GroupCallPickupAddInstanceRequest");
            $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
            $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
            $xmlinput .= "<name>" . $name . "</name>";
            //$xmlinput .= "<userId></userId>";

            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

            $errMsg =  readErrorXml($xml, $name);
            return $errMsg;
        }


        function showGroupCallPickupInstanceCSV() {

            global $sessionid, $client;

            $xmlinput = xmlHeader($sessionid, "GroupCallPickupGetInstanceListRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
            $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            $errMsg =  readErrorXmlList($xml);
            $groupCallPickupList = array();

            if($errMsg <> "Success"){

                $groupCallPickupList = $errMsg;
            }else{

                foreach ($xml->command->name as $key => $value)
                {
                    $groupCallPickupList[] = strval($value);
                }
                /*	if (isset($groupCallPickupList))
                    {
                        $groupCallPickupList = subval_sort($groupCallPickupList);
                    }*/
            }

            return $groupCallPickupList;
        }

        function showGroupCallPickupInstance() {

            global $sessionid, $client;

            $xmlinput = xmlHeader($sessionid, "GroupCallPickupGetInstanceListRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
            $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            $errMsg =  readErrorXmlList($xml);
            $groupCallPickupList = array();

            if($errMsg <> "Success"){

                $groupCallPickupList = $errMsg;
            }else{

                foreach ($xml->command as $key => $value)
                {
                    $groupCallPickupList[] = $value->name;
                }
                if (isset($groupCallPickupList))
                {
                    $groupCallPickupList = subval_sort($groupCallPickupList);
                }
            }

            return $groupCallPickupList;
        }


        function deleteGroupCallPickupInstance($name) {

            global $sessionid, $client;

            $xmlinput  = xmlHeader($sessionid, "GroupCallPickupDeleteInstanceRequest");
            $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
            $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
            $xmlinput .= "<name>" . $name . "</name>";
            //$xmlinput .= "<userId></userId>";

            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

            $errMsg =  readErrorXml($xml, $name);
            return $errMsg;
        }



    }
?>

