<?php 
class SurgeMailOperationsExpSheet{  
    
    function surgeMailAccountOperationsExpSheet($surgeMailUrl, $surgemailPost){
        
        $resp = "";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL, $surgeMailUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $surgemailPost);
        $resp = curl_exec($ch);
        //print_r($output);
        //$info = curl_getinfo($ch);
        /*if(curl_errno($ch)){
         $resp = curl_error($ch);
         } else if(strpos($output, "User saved") === false) {
         $resp = $output;
         } else {
         $resp = $info["http_code"];
         }*/
        curl_close($ch);
        return $resp;
    }
    
}
?>