<?php
//print_r($_SESSION["adminType"])."wjwwjwjw";
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
class CustomTagManagement{
	
	function getTagList($deviceType){
		global $db;
		$lookupDeviceType = $deviceType;
                //Code added @ 16 July 2019
                $whereCndtn = "";
                if($_SESSION['cluster_support']){
                    $selectedCluster = $_SESSION['selectedCluster'];
                    $whereCndtn = " AND clusterName ='$selectedCluster' ";
                }
                //End code
		
		$lookupQuery = "select customTagSpec from systemDevices where deviceType = '".$deviceType."' $whereCndtn ";
		$result = $db->query($lookupQuery);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$lookupDeviceType= isset($row["customTagSpec"]) ? $row["customTagSpec"] : $deviceType;
		}
		
		$tagArray = array();
		$checkPermissionSets = $this->checkPermissionSets();
		if($checkPermissionSets > 0){
			$andClause = $this->getAdminTypeClause();
		}else{
			$andClause = "";
		}
		
		$query = "select * from customTagSpec where deviceType = '".$lookupDeviceType."'" . $andClause. " ORDER BY shortDescription ASC";
		//print_r($query);
		$result = $db->query($query);
		while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
			$tagArray[] = $row;
		}
		return $tagArray;
	}
	
	function getAdminTypeClause() {
		require_once("/var/www/lib/broadsoft/adminPortal/configUtility.php");
		
		$andClause = "";
		if (isset($adminTypeWeightArray)) {

			if( $adminTypeWeightArray[$_SESSION["adminType"]] == "8") { //weight 8 , District IT
				$andClause = " and adminType in (8,9,10,11,12,13,14,15)";
			}

			if( $adminTypeWeightArray[$_SESSION["adminType"]] == "4") { //weight 4, USPS
				$andClause = " and adminType in (4,5,6,7,12,13,14,15)";
			}


			if( $adminTypeWeightArray[$_SESSION["adminType"]] == "2") { //weight 2, National
				$andClause = " and adminType in (2,3,6,7,10,11,14,15)";
			}


			if( $adminTypeWeightArray[$_SESSION["adminType"]] == "1") { //weight 1, Provisioning User
				$andClause = " and adminType in (1,3,5,7,9,11,13,15)";
			}

		}
		
		return $andClause;
		
	}
	
	function getUserCustomTags($deviceName){
		global $sessionid, $client;
		$userAssignedTags = array();
		
		// get custom tags from back-end
		$xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagGetListRequest");
		$xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errMsg = readErrorXmlGenuine($xml);
		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";
		if($errMsg != "") {
			$returnResponse["Error"] = $errMsg;
		} else {
			if(isset($xml->command->deviceCustomTagsTable->row)) {
				foreach ($xml->command->deviceCustomTagsTable->row as $key => $value){
					$userAssignedTags["tagName"][] = strval($value->col[0]);
				}
			} else {
				$userAssignedTags["tagName"] = array();
			}
			
			$returnResponse["Success"] = $userAssignedTags["tagName"];
		}
		
		return $returnResponse;
	}
	
	function groupAccessDeviceDeleteCustomTag($deviceName, $tagName){
		global $sessionid, $client;
		$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceCustomTagDeleteListRequest");
		$xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<deviceName>". $deviceName."</deviceName>";
		$xmlinput .= "<tagName>". $tagName ."</tagName>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		return $xml;
	}
	
	function addDeviceCustomTag($deviceName, $tagName, $tagValue) {
		global $sessionid, $client;
		
		$xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagAddRequest");
		$xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
		$xmlinput .= "<tagName>" . $tagName . "</tagName>";
		$xmlinput .= "<tagValue>" . $tagValue . "</tagValue>";
		
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		
		return $xml;
	}
        
        function addDeviceCustomTagDuplicate($deviceName, $tagName, $tagValue) {
		global $sessionid, $client;
		
		$xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagAddRequest");
		$xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
		$xmlinput .= "<tagName>" . $tagName . "</tagName>";
                if($tagValue!=""){
                    $xmlinput .= "<tagValue>" . $tagValue . "</tagValue>";
                }
		
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		
		return $xml;
	}
	
	function checkPermissionSets(){
		global $db;
		$total = 0;
		
		//$query = "SELECT count(*) as total FROM permissionsSets";
                $query = "SELECT count(*) as total FROM adminTypesLookup";
                
		$result = $db->query($query);
		if ($output = $result->fetch()) {
			$total = $output["total"];
		}
		return $total;
		
	}
}
