<?php

/**
 * Created by Sologics.
 * Date: 25/07/2017
 */
class DnsHelper
{	
	//function to get the basic information of a group
	public function getGroupExtensionConfigRequestHelper($extensionResponse){
		
		$groupInfoArray = array();
		if(empty($extensionResponse['Error'])){
			$groupInfoArray['minExtensionLength'] = $extensionResponse['minExtensionLength'];
			$groupInfoArray['maxExtensionLength'] = $extensionResponse['maxExtensionLength'];
			$groupInfoArray['defaultExtensionLength'] = $extensionResponse['defaultExtensionLength'];
		}
		return $groupInfoArray;		
	}
}