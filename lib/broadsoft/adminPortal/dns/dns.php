<?php

/**
 * Created by Sologics.
 * Date: 19/06/2017
 */

class Dns
{
	
	//add DN's request in service provider
	public function setDNServiceProviderRequest($postArray){
		global  $sessionid, $client;
		
		$addDnResponse["Error"] = "";
		$addDnResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "ServiceProviderDnAddListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		if(isset($postArray['phoneNumber']) && count($postArray['phoneNumber']) > 0){
			foreach($postArray['phoneNumber'] as $key=>$val){
				$xmlinput .= "<phoneNumber>" . $val . "</phoneNumber>";
			}
		}
		if(!empty($postArray['minPhoneNumber']) && !empty($postArray['maxPhoneNumber'])){			
			for($r = 0; $r < count($postArray['minPhoneNumber']); $r++){
				$xmlinput .= "<dnRange>";
				$xmlinput .= "<minPhoneNumber>" . $postArray['minPhoneNumber'][$r] . "</minPhoneNumber>";
				$xmlinput .= "<maxPhoneNumber>" . $postArray['maxPhoneNumber'][$r] . "</maxPhoneNumber>";
				$xmlinput .= "</dnRange>";
			}
			
		}
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		// echo "<pre>"; print_r($xml); die();
		if (readErrorXmlGenuine($xml) != "") {
			$addDnResponse["Error"] = strval($xml->command->summaryEnglish);
		}else{
			$addDnResponse["Success"] = "DN's added Successfully. Please check your DN's list to see the changes";
			
		}                
		return $addDnResponse;
	}
	
	//get DN's list in service provider
	public function getDNServiceProviderSummaryRequest(){
		global  $sessionid, $client;
		
		$getDnResponse["Error"] = "";
		$getDnResponse["Success"] = "";
		$phoneArray['phone'] = array();
		
		$xmlinput = xmlHeader($sessionid, "ServiceProviderDnGetSummaryListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$getDnResponse["Error"] = strval($xml->command->summaryEnglish);
		}else{
			$x = 0;
			foreach($xml->command->dnSummaryTable->row as $key=>$val){
				if(strval($val->col[1]) == ""){
					$phoneArray['phone'][$x]['phoneNumber'] = strval($val->col[0]);
					$phoneArray['phone'][$x]['groupId'] = strval($val->col[1]);
				}
				$x++;
			}
			array_multisort($phoneArray['phone']);
			$getDnResponse["Success"] = array_values($phoneArray['phone']);			
		}
		return $getDnResponse;
	}
	
	
	//add DN's request in service provider
	public function setGroupDNAssignRequest($postArray){
		global  $sessionid, $client;
		
		$addGroupDNResponse["Error"] = "";
		$addGroupDNResponse["Success"] = "";
		$activateDnsArray = array();
		
		$xmlinput = xmlHeader($sessionid, "GroupDnAssignListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']). "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']) . "</groupId>";
		if(!empty($postArray['phoneNumber'])){
			foreach($postArray['phoneNumber'] as $key=>$val){
				$xmlinput .= "<phoneNumber>" . $val . "</phoneNumber>";
			}
		}
		if(!empty($postArray['minPhoneNumber']) && !empty($postArray['maxPhoneNumber'])){
			$xmlinput .= "<dnRange>";
			$xmlinput .= "<minPhoneNumber>" . $postArray['minPhoneNumber'] . "</minPhoneNumber>";
			$xmlinput .= "<maxPhoneNumber>" . $postArray['maxPhoneNumber'] . "</maxPhoneNumber>";
			$xmlinput .= "</dnRange>";
		}
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			if(!empty($xml->command->detail)){
				$addGroupDNResponse["Error"] = strval($xml->command->detail);
			}else{
				$addGroupDNResponse["Error"] = strval($xml->command->summaryEnglish);
			}
		}else{
			$addGroupDNResponse["Success"] = "Success";			
		}
		return $addGroupDNResponse;
	}
	
	//add DN's request in service provider
	public function getGroupDNListRequest($postArray){
		global  $sessionid, $client;
		
		$addGroupDNResponse["Error"] = "";
		$addGroupDNResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupDnGetListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($postArray['serviceProviderId']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']) . "</groupId>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			if(!empty($xml->command->detail)){
				$addGroupDNResponse["Error"] = strval($xml->command->detail);
			}else{
				$addGroupDNResponse["Error"] = strval($xml->command->summaryEnglish);
			}
		}else{
			$addGroupDNResponse["Success"] = $xml->command->phoneNumber;
			
		}
		return $addGroupDNResponse;
	}
	
	public function getUpdatePostDns($postArray){
		$dnsArray['newAssign'] = array();
		$dnsArray['unAssign'] = array();
		$sessionArray = array();
		$assignedSessionNumbersArray = array();
		
		if(count($_SESSION['groupInfoData']['assignNumbers']) > 0){
			$sessionArray = $_SESSION['groupInfoData']['assignNumbers'];
		}
			
		if(count($sessionArray) > 0){
			$assignedSessionNumbersArray = explode(",", $sessionArray);
			
			if(count($assignedSessionNumbersArray) > 0){
				foreach($assignedSessionNumbersArray as $key=>$val){
					if(!in_array($val, $postArray)){
						$dnsArray['unAssign'][] = $val;
					}
				}
			}
		}
		if(count($postArray) > 0){
			foreach($postArray as $key=>$val){
				if(!in_array($val, $assignedSessionNumbersArray)){
					$dnsArray['newAssign'][] = $val;
				}
			}
		}
		
		return $dnsArray;
	}

	//unAssign DN's request in service provider
	public function unAssignGroupDNAssignRequest($postArray){
		global  $sessionid, $client;
		
		$unAssignGroupDNResponse["Error"] = "";
		$unAssignGroupDNResponse["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupDnUnassignListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']). "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']) . "</groupId>";
		if(!empty($postArray['phoneNumber'])){
			foreach($postArray['phoneNumber'] as $key=>$val){
				$xmlinput .= "<phoneNumber>" . $val . "</phoneNumber>";
			}
		}
		if(!empty($postArray['minPhoneNumber']) && !empty($postArray['maxPhoneNumber'])){
			$xmlinput .= "<dnRange>";
			$xmlinput .= "<minPhoneNumber>" . $postArray['minPhoneNumber'] . "</minPhoneNumber>";
			$xmlinput .= "<maxPhoneNumber>" . $postArray['maxPhoneNumber'] . "</maxPhoneNumber>";
			$xmlinput .= "</dnRange>";
		}
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			if(!empty($xml->command->detail)){
				$unAssignGroupDNResponse["Error"] = strval($xml->command->detail);
			}else{
				$unAssignGroupDNResponse["Error"] = strval($xml->command->summaryEnglish);
			}
		}else{
			$unAssignGroupDNResponse["Success"] = "Success";
			
		}
		return $unAssignGroupDNResponse;
	}

	//add DN's request in service provider
	public function getGroupDNAvailableListRequest($postArray){
		global  $sessionid, $client;
		
		$getAvailableDNResponse["Error"] = "";
		$getAvailableDNResponse["Success"] = "";
		$phone = array();
		
		$xmlinput = xmlHeader($sessionid, "GroupDnGetAvailableListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']) . "</groupId>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			if(!empty($xml->command->detail)){
				$getAvailableDNResponse["Error"] = strval($xml->command->detail);
			}else{
				$getAvailableDNResponse["Error"] = strval($xml->command->summaryEnglish);
			}
		}else{
			if(!empty($_SESSION['groupInfoData']['portalPhoneNo'])){
				$count = count($xml->command->phoneNumber);
				$xml->command->phoneNumber->$count = $_SESSION['groupInfoData']['portalPhoneNo'];
			}
			$a = 0;
			foreach ($xml->command->phoneNumber as $key => $value)
			{
				$phone[$a] = strval($value);
				$a++;
			}
			$getAvailableDNResponse["Success"] = $phone;
		}
		return $getAvailableDNResponse;
	}

	//add DN's request in service provider
	public function activateDNRequest($postArray){
		global  $sessionid, $client;
		
		$activateDNResponse["Error"] = "";
		$activateDNResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupDnActivateListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']). "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']) . "</groupId>";
		if(!empty($postArray['phoneNumber'])){
			foreach($postArray['phoneNumber'] as $key=>$val){
				$xmlinput .= "<phoneNumber>" . $val . "</phoneNumber>";
			}
		}
		if(!empty($postArray['dnrange']) ){
			foreach($postArray['dnrange'] as $key=>$val){
				$xmlinput .= "<dnRange>";
				$xmlinput .= "<minPhoneNumber>" . $val['minPhoneNumber'] . "</minPhoneNumber>";
				$xmlinput .= "<maxPhoneNumber>" . $val['maxPhoneNumber'] . "</maxPhoneNumber>";
				$xmlinput .= "</dnRange>";
			}
		}
		
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			if(!empty($xml->command->detail)){
				$activateDNResponse["Error"] = strval($xml->command->detail);
			}else{
				$activateDNResponse["Error"] = strval($xml->command->summaryEnglish);
			}
		}else{
			$activateDNResponse["Success"] = "Success";
			
		}
		return $activateDNResponse;
	}
	
	public function getGroupExtensionConfigRequest($sp, $groupid){
		global  $sessionid, $client;
		
		$getExtensionResponse["Error"] = "";
		$getExtensionResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupExtensionLengthGetRequest17");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($sp) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($groupid) . "</groupId>";
		
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			if(!empty($xml->command->detail)){
				$getExtensionResponse["Error"] = strval($xml->command->detail);
			}else{
				$getExtensionResponse["Error"] = strval($xml->command->summaryEnglish);
			}
		}else{
			$getExtensionResponse["minExtensionLength"] = strval($xml->command->minExtensionLength);
			$getExtensionResponse["maxExtensionLength"] = strval($xml->command->maxExtensionLength);
			$getExtensionResponse["defaultExtensionLength"] = strval($xml->command->defaultExtensionLength);
			
		}
		return $getExtensionResponse;
	}
	
	public function modifyGroupExtensionConfigRequest($postArray){
		global  $sessionid, $client;
		
		$modifyExtensionResponse["Error"] = "";
		$modifyExtensionResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupExtensionLengthModifyRequest17");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']). "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']) . "</groupId>";
		if(!empty($postArray['minExtensionLength'])){
			$xmlinput .= "<minExtensionLength>" . $postArray['minExtensionLength'] . "</minExtensionLength>";
		}
		if(!empty($postArray['maxExtensionLength'])){
			$xmlinput .= "<maxExtensionLength>" . $postArray['maxExtensionLength'] . "</maxExtensionLength>";
		}
		
		if(!empty($postArray['defaultExtensionLength'])){
			$xmlinput .= "<defaultExtensionLength>" . $postArray['defaultExtensionLength'] . "</defaultExtensionLength>";
		}
		
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			if(!empty($xml->command->detail)){
				$modifyExtensionResponse["Error"] = strval($xml->command->detail);
			}else{
				$modifyExtensionResponse["Error"] = strval($xml->command->summaryEnglish);
			}
		}else{
			$modifyExtensionResponse["Success"] = "Success";
			
		}
		return $modifyExtensionResponse;
	}
	
	//function to deactivate DN's
	
	//add DN's request in service provider
	public function deActivateDNRequest($postArray){
		global  $sessionid, $client;
		
		$activateDNResponse["Error"] = "";
		$activateDNResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupDnDeactivateListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']) . "</groupId>";
		if(!empty($postArray['phoneNumber'])){
			foreach($postArray['phoneNumber'] as $key=>$val){
				$xmlinput .= "<phoneNumber>" . $val . "</phoneNumber>";
			}
		}
		if(!empty($postArray['dnrange']) ){
			foreach($postArray['dnrange'] as $key=>$val){
				$xmlinput .= "<dnRange>";
				$xmlinput .= "<minPhoneNumber>" . $val['minPhoneNumber'] . "</minPhoneNumber>";
				$xmlinput .= "<maxPhoneNumber>" . $val['maxPhoneNumber'] . "</maxPhoneNumber>";
				$xmlinput .= "</dnRange>";
			}
		}
		
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			if(!empty($xml->command->detail)){
				$activateDNResponse["Error"] = strval($xml->command->detail);
			}else{
				$activateDNResponse["Error"] = strval($xml->command->summaryEnglish);
			}
		}else{
			$activateDNResponse["Success"] = "Success";
			
		}
		return $activateDNResponse;
	}
	
	//function to get the status of user number activation
	public function getUserDNActivateListRequest($userId){
		global  $sessionid, $client;
		
		$getDNResponse["Error"] = "";
		$getDNResponse["Success"] = "";
		$list = array();
		
		$xmlinput = xmlHeader($sessionid, "UserDnGetActivationListRequest");
		$xmlinput .= "<userId>" . $userId. "</userId>";		
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml);
		if (readErrorXmlGenuine($xml) != "") {
			if(!empty($xml->command->detail)){
				$getDNResponse["Error"] = strval($xml->command->detail);
			}else{
				$getDNResponse["Error"] = strval($xml->command->summaryEnglish);
			}
		}else{
			$r = 0;
			foreach($xml->command->dnTable->row as $key=>$val){
				$status = strval($val->col[1]);
				
				$list[$r]['phoneNumber'] = strval($val->col[0]);
				if($status == "true"){
					$numberStatus = "Yes";
				}else if($status == "false"){
					$numberStatus = "No";
				}else{
					$numberStatus = "";
				}
				$list[$r]['status'] = $numberStatus;
				$r++;
			}
			$getDNResponse["Success"] = $list;
			
		}
		return $getDNResponse;
	}

	//add DN's request in service provider
	public function deleteDNServiceProviderRequest($postArray){
		global  $sessionid, $client;
		
		$addDnResponse["Error"] = "";
		$addDnResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "ServiceProviderDnDeleteListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		if(isset($postArray['phoneNumber']) && count($postArray['phoneNumber']) > 0){
			foreach($postArray['phoneNumber'] as $key=>$val){
				$xmlinput .= "<phoneNumber>" . $val . "</phoneNumber>";
			}
		}
		if(!empty($postArray['minPhoneNumber']) && !empty($postArray['maxPhoneNumber'])){
			for($r = 0; $r < count($postArray['minPhoneNumber']); $r++){
				$xmlinput .= "<dnRange>";
				$xmlinput .= "<minPhoneNumber>" . $postArray['minPhoneNumber'][$r] . "</minPhoneNumber>";
				$xmlinput .= "<maxPhoneNumber>" . $postArray['maxPhoneNumber'][$r] . "</maxPhoneNumber>";
				$xmlinput .= "</dnRange>";
			}
			
		}
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);  
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
                        //$addDnResponse["Error"] = strval($xml->command->summaryEnglish);
                        //Below line modified by Anshu @ 05 Feb 2018
                        $summaryEnglish = rtrim(strval($xml->command->summaryEnglish), ".");
                        $addDnResponse["Error"]  = $summaryEnglish."; Phone number(s) assigned to users or group services.";
		}else{
			$addDnResponse["Success"] = "DN's deleted Successfully. Please check your DN's list to see the changes";
			
		}
                
		return $addDnResponse;
	}
	
	// get available Dn's of a group
	public function GroupDnGetListRequest($sp, $groupId) {
	    global $sessionid, $client;
	    $returnResponse ["Error"] = "";
	    $returnResponse ["Success"] = "";
	    
	    $xmlinput = xmlHeader($sessionid, "GroupDnGetDetailedAvailableListRequest");
	    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($sp) . "</serviceProviderId>";
	    $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    if(strval($xml->command->attributes()->type) == "Error") {
	        $returnResponse ["Error"] = strval($xml->command->attributes()->type);
	    }
	    else {
	        $i = 0;
	        foreach($xml->command->dnTable->row as $key=>$val) {
	            $vailableDnList[$i]["phoneNumber"] = strval($val->col [0]);
	            $vailableDnList[$i]["department"] = strval($val->col [1]);
	            $vailableDnList[$i]["activated"] = strval($val->col [2]);
	            
	            $i++;
	        }
	        $returnResponse ["Success"] = $vailableDnList;
	    }
	    return $returnResponse;
	    
	}
	
	public function getDNServiceProviderRequest($spId){
	    global  $sessionid, $client;
	    
	    $getDnResponse["Error"] = "";
	    $getDnResponse["Success"] = "";
	    $phoneArray['phone'] = array();
	    
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderDnGetSummaryListRequest");
	    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId) . "</serviceProviderId>";
	    $xmlinput .= xmlFooter();
	    
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	 
            if (readErrorXmlGenuine($xml) != "") {
	        $getDnResponse["Error"] = strval($xml->command->summaryEnglish);
	    }else{
	        $x = 0;
	        foreach($xml->command->dnSummaryTable->row as $key=>$val){
	           /* if(strval($val->col[1]) == ""){
	                $phoneArray['phone'][$x]['phoneNumber'] = strval($val->col[0]);
	                $phoneArray['phone'][$x]['groupId'] = strval($val->col[1]);
	                $phoneArray['phone'][$x]['canDelete'] = strval($val->col[2]);
	            } */
                  //  if(strval($val->col[1]) == ""){
	                $phoneArray['phone'][$x]['phoneNumber'] = strval($val->col[0]);
	                $phoneArray['phone'][$x]['groupId'] = strval($val->col[1]);
	                $phoneArray['phone'][$x]['canDelete'] = strval($val->col[2]);
	         //   }
	            $x++;
	        }
	        array_multisort($phoneArray['phone']);
	        $getDnResponse["Success"] = array_values($phoneArray['phone']);
	    }
	    return $getDnResponse;
	}
	
	
	public function serviceProviderDnAddListRequest($spId, $postArray){
	    global  $sessionid, $client;
	    
	    $addDnResponse["Error"] = "";
	    $addDnResponse["Success"] = "";
	    
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderDnAddListRequest");
	    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId) . "</serviceProviderId>";
	    if(isset($postArray['newlyAddedDns']) && count($postArray['newlyAddedDns']) > 0){
	        foreach($postArray['newlyAddedDns'] as $dnKey => $dnValue) {
	            $xmlinput .= "<phoneNumber>" . $dnValue . "</phoneNumber>";
	        }
	    }
	    if(!empty($postArray['newlyAddedDnsRanges']) ) {
// 	        for($r = 0; $r < count($postArray['minPhoneNumber']); $r++){
	        foreach($postArray['newlyAddedDnsRanges'] as $rangeKey => $rangeValue) {
	            $xmlinput .= "<dnRange>";
	            $xmlinput .= "<minPhoneNumber>" . $rangeValue['minPhoneNumber'] . "</minPhoneNumber>";
	            $xmlinput .= "<maxPhoneNumber>" . $rangeValue['maxPhoneNumber'] . "</maxPhoneNumber>";
	            $xmlinput .= "</dnRange>";
	        }
	        
	    }
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    // echo "<pre>"; print_r($xml); die();
	    if (readErrorXmlGenuine($xml) != "") {
	        $addDnResponse["Error"] = strval($xml->command->summaryEnglish);
	    }else{
	        $addDnResponse["Success"] = "DN's added Successfully. Please check your DN's list to see the changes";
	        
	    }
	    return $addDnResponse;
	}
	
	public function serviceProviderDnDeleteListRequest($postArray, $spId){
	    global  $sessionid, $client;
	    
	    $addDnResponse["Error"] = "";
	    $addDnResponse["Success"] = "";
	    
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderDnDeleteListRequest");
	    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId) . "</serviceProviderId>";
	    if(isset($postArray['numbers']) && count($postArray['numbers']) > 0){
	        foreach($postArray['numbers'] as $key=>$val){
	            $xmlinput .= "<phoneNumber>" . $val . "</phoneNumber>";
	        }
	    }
	    
	    if(!empty($postArray['numbersRanges']) ) {
	        foreach($postArray['numbersRanges'] as $rangeKey => $rangeValue) {
	            $xmlinput .= "<dnRange>";
	            $xmlinput .= "<minPhoneNumber>" . $rangeValue['minPhoneNumber'] . "</minPhoneNumber>";
	            $xmlinput .= "<maxPhoneNumber>" . $rangeValue['maxPhoneNumber'] . "</maxPhoneNumber>";
	            $xmlinput .= "</dnRange>";
	        }
	        
	    }
	    
	    $xmlinput .= xmlFooter();
	    
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    if (readErrorXmlGenuine($xml) != "") {
	        $summaryEnglish = rtrim(strval($xml->command->summaryEnglish), ".");
	        $addDnResponse["Error"]  = $summaryEnglish."; Phone number(s) assigned to users or group services.";
	    }else{
	        $addDnResponse["Success"] = "DN's deleted Successfully. Please check your DN's list to see the changes";
	        
	    }
	    
	    return $addDnResponse;
	}
	
}