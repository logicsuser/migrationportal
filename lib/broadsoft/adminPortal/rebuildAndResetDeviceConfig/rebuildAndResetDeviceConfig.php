<?php

class RebuildAndResetDeviceConfig
{
    function GroupCPEConfigRebuildDeviceConfig($providerId, $groupId, $deviceName, $forceOption = "")
    {
        global $sessionid, $client;
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput  = xmlHeader($sessionid, "GroupCPEConfigRebuildDeviceConfigFileRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($providerId) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        if ($forceOption != "") {
            $xmlinput .= "<force>true</force>";
        }
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        }
        else
        {
            $returnResponse["Success"] = "Success";
        }
        return $returnResponse;
    }
    
    
    function GroupCPEConfigResetDevice($providerId, $groupId, $deviceName)
    {
        global $sessionid, $client;
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput  = xmlHeader($sessionid, "GroupCPEConfigResetDeviceRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($providerId) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        }
        else
        {
            $returnResponse["Success"] = "Success";
        }
        return $returnResponse;
    }
    
   
}


?>

