<?php
require_once ("/var/www/lib/broadsoft/adminPortal/ChangeLogUtility.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/adminLogs/classes/adminLogs.php");

Class AnnouncementChangeLog21 {
	
	private $groupId;
	private $loggedInUserName;
	private $serviceId;
	private $changeLog = array();
	private $postedData;
	
	private $adminId;
	private $loggedInUser;
	private $announcementGroup;
        private $enterpriseId;
        
	public function __construct($serviceId, $postedData, $changeLog, $announcementGroup) {
		
		$this->serviceId = $serviceId;
		$this->changeLog = $changeLog;
		$this->postedData = $postedData;
		
		$this->loggedInUserName = $_SESSION["loggedInUserName"];
		$this->groupId = $_SESSION["groupId"];
		$this->adminId = $_SESSION["adminId"];
		$this->loggedInUser = $_SESSION["loggedInUser"];
		$this->announcementGroup = $announcementGroup;
                $this->enterpriseId = $_SESSION['sp'];
	}
	
	public function changeLogUtil() {
		//Code added @ 30 Jan 2019
                if($this->groupId==""){
                    $this->groupId = $this->announcementGroup;
                }
                //End code
		$changeLogObj = new ChangeLogUtility($this->serviceId, $this->groupId, $this->loggedInUserName);
		$adminLogs = new adminLogs();
		if(empty($this->postedData["announcementId"])){
			
			$module = "Add Announcement";
			$tableName = "announcementAddChanges21";
			$entity = $this->postedData["announcementName"];
			$changeResponse = $changeLogObj->changeLogAddUtility($module, $entity, $tableName, $this->changeLog);
			/*
			 * EX-765 updated date 16-08-2018
			 * Show admin Log details in add/save announcement
			 * get announcement Type using getAnnouncementTypeInfo();
			 * find user Id into user table accourding to userName which name is store in session
			 */
			$log = array(
				'adminUserID' => $this->adminId,
				'eventType' => 'ADD_ANNOUNCEMENT',
				'adminUserName' => $this->loggedInUserName,
				'adminName' => $this->loggedInUser,
				'updatedBy' => $this->adminId,
				'details' => array(
					'Group Name' => $this->announcementGroup,
					'Announcement Name' => $this->postedData["announcementName"],
					'Announcement Type' => $this->postedData["announcementType"],
					'Announcement File' => $this->postedData["filenames"][0],
				)
			);
			
		} else {
			$this->postedData["announcementUploadVal"] = $this->postedData["filenames"][0];
			$formDataArray = array(
				"announcementName" => "Announcement Name",
				"announcementType" => "Announcement Type",
				"announcementUploadVal" => "Announcement File",
			);
			$changeLogObj->createChangesArrayFromArray($this->postedData, $_SESSION['addAnnouncement'], $formDataArray);
			$module = "Modify Announcement";
			$tableName = "announcementModChanges";
			$entity = $this->postedData["announcementName"];
			$changeResponse = $changeLogObj->changeLogModifyUtility($module, $entity, $tableName);
			/*
			 * Admin log show announcement Modify details
			 * Details are show according to old value and new value
			 */
			$details = array('Modified By' => $this->loggedInUser,'Group Name' =>$this->announcementGroup);
			if($this->postedData["announcementName"] != $_SESSION['addAnnouncement']['announcementName']) {
				$details['Announcement Name'] = array($this->postedData["announcementName"],$_SESSION['addAnnouncement']['announcementName']);
			}
			if($this->postedData["announcementType"] != $_SESSION['addAnnouncement']['announcementMediaType']) {
				$details['Announcement Type'] = array($this->postedData["announcementType"],$_SESSION['addAnnouncement']['announcementMediaType']);
			}
			if(isset($this->postedData["announcementUploadVal"]) && !empty($this->postedData["announcementUploadVal"])){
				if($this->postedData["announcementUploadVal"] != $_SESSION['addAnnouncement']['announcementUploadVal']) {
					$details['Announcement File'] = array($this->postedData["announcementUploadVal"],$_SESSION['addAnnouncement']['announcementUploadVal']);
				}
			}
			$log = array(
				'adminUserID' =>$this->adminId,
				'eventType' => 'MODIFY_ANNOUNCEMENT',
				'adminUserName' => $this->loggedInUserName,
				'adminName' => $this->loggedInUser,
				'updatedBy' => $this->adminId,
				'details' => $details
			);
			
		}
		
		$adminLogs->addLogEntry($log);
	}
}

?>