<?php

/**
 * Created by Sollogics.
 * Date: 10/08/2018
 */
class Announcement21 {
	public $sp;
	public $groupId;
	public $announcementFileType;
	public $includeAnnouncementTable;
	public $announcementFileName;
	public $announcementMode;
	private $enterpriseLevel;
	public $fileExt;
	public $supportedExt = array();
	public $checkCriteria = true;
	public $searchLevel = "";
	private $tempCounter = 0;
	
	public function __construct() {
	    $this->enterpriseLevel = ! isset($_SESSION['groupId']) ? true : false;
	    $this->supportedExt = array("WMA", "WAV", "3GP", "MOV");
	}
	
	// get announcement list
	public function getAnnouncementList() {
		global $sessionid, $client;
		
		$announcementResponse ["Error"] = "";
		$announcementResponse ["Success"] = "";
		$xmlinput = xmlHeader ( $sessionid, "GroupAnnouncementFileGetListRequest" );
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars ( $this->sp ) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars ( $this->groupId ) . "</groupId>";
		if (! empty ( $this->announcementFileType )) {
			$xmlinput .= "<announcementFileType>" . $this->announcementFileType . "</announcementFileType>";
		}
		$xmlinput .= "<includeAnnouncementTable>" . $this->includeAnnouncementTable . "</includeAnnouncementTable>";
		
		if($this->checkCriteria) {
		    
		    
		     if (! empty ( $this->announcementFileName )) {
		     $xmlinput .= "<searchCriteriaAnnouncementFileName>";
		     $xmlinput .= "<mode>". htmlspecialchars ( $this->announcementMode) ."</mode>";
		     $xmlinput .= "<value>" . $this->announcementFileName . "</value>";
		     $xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
		     $xmlinput .= "</searchCriteriaAnnouncementFileName>";
		     }
		     
		}
		
		$xmlinput .= xmlFooter ();
		
		$response = $client->processOCIMessage ( array (
				"in0" => $xmlinput 
		) );
		
		$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
// 		echo "<pre>";
		//print_r ( $xmlinput );
// 	print_r ( $xml );
// 		die ();
		//echo "rajesh "; print_r($this->checkCriteria);
		if (readErrorXmlGenuine ( $xml ) != "") {
			$announcementResponse ["Error"] = strval ( $xml->command->summaryEnglish );
		} else {
			
			$i = 0;
			foreach ( $xml->command->announcementTable->row as $key => $val ) {
				$announcementList [$key] [$i] ["announcementName"] = strval ( $val->col [0] );
				$announcementList [$key] [$i] ["announcementSp"] = $this->sp;
				$announcementList [$key] [$i] ["announcementGroup"] = $this->groupId;
				$announcementList [$key] [$i] ["announcementMediaType"] = strval ( $val->col [1] );
				$announcementList [$key] [$i] ["announcementFileSize"] = strval ( $val->col [2] );
				
				$announcementInfoArray ["name"] = strval ( $val->col [0] );
				$announcementInfoArray ["mediaType"] = strval ( $val->col [1] );
				
				$announcementInfo = $this->getAnnouncementInfo ( $announcementInfoArray );
				$announcementList [$key] [$i] ["announcementDescription"] = $announcementInfo ["Success"] ["description"];
				$announcementList [$key] [$i] ["usageTable"] = $announcementInfo ["Success"] ["usageTable"];
				if(count($announcementInfo ["Success"] ["usageTable"]) > 0){	
					$announcementList [$key] [$i] ["announcementInUse"] = "In Use";
				}else{
					$announcementList [$key] [$i] ["announcementInUse"] = "Not In Use";
				}
				
				$i ++;
			}
			$announcementResponse ["Success"] = $announcementList;
			$announcementResponse ["Success"] ["announcementSp"] = $this->sp;
			$announcementResponse ["Success"] ["announcementGroup"] = $this->groupId;
			$tFileSize = strval ( $xml->command->totalFileSize );
			$int = (int)$tFileSize;
			//$totalFileSize = $int / 1048576;
			$totalFileSize = number_format($int / 1000, 2);
			$announcementResponse ["Success"] ["totalFileSize"] = $totalFileSize;
			$announcementResponse ["Success"] ["maxFileSize"] = strval ( $xml->command->maxFileSize );
		}
		//print_r($announcementResponse);
		return $announcementResponse;
	}
	
	// print the column data
	public function parseAnnouncementColumn() {
		$html = '<table id="announcementTable" class="scroll tableAlignMentDesign tablesorter" style="width: 100%; margin: 0;">';
		$html .= '<thead>'; 
		$html .= '<tr>';
		$html .= '<th class="thSno header">Delete</th>';
		if($this->enterpriseLevel) {
		    $html .= '<th class="header thsmall">Enterprise</th>';
		    $html .= '<th class="header thsmall">Group</th>';
		}
		
		$html .= '<th class="header thsmall">Announcement Name</th>';
		$html .= '<th class="header thsmall">In Use</th>';
		$html .= '<th class="header thsmall">Media Type</th>';
		$html .= '<th class="header thsmall">File Size (KB)</th>';
		$html .= '</tr>';
		$html .= '</thead>';
		
		return $html;
	}
	
	public function getMediaTypeVal($mediaType){
		
		if($mediaType == "WAV" || $mediaType == "WMA"){
			$mediaTypeVal = "Audio";
		}else if($mediaType == "MOV" || $mediaType == "3GP"){
			$mediaTypeVal = "Video";
		}
		return $mediaTypeVal;
	}
	public function parseAnnouncementData($data) {
		$html = "";
		$opacity = "";
		//print_r($data); echo"11111111"; exit;
		foreach ( $data as $key => $val ) {
		    $key = $this->tempCounter.$key;
		    
		    $opacity = "style='opacity: 1;'";
			$mediaTypeVal = $this->getMediaTypeVal($val ["announcementMediaType"]);
			
			if($val["addToBroad"] == "No"){
				$inUse = "Not In Use";
			}else{
				$inUse = $val ["announcementInUse"];
			}
						
			$html .= '<tr href="javascript:void(0)" class="announcementLink" data-name="' . $val ["announcementName"] . '" data-mediaTypeVal="' . $val ["announcementMediaType"]. '" data-mediaType="' . $mediaTypeVal. '" data-sp="'. $val["announcementSp"].'" data-group="'. $val["announcementGroup"].'" data-exist="'. $val ["addToBroad"].'">';
			$html .= '<td class="thSno">';
			$html .= '<input type="checkbox" class="checkAnnouncement" data-sp="'. $val["announcementSp"].'" data-mediaTypeVal="' . $val ["announcementMediaType"]. '" data-group="'. $val["announcementGroup"].'" data-mediaType="'. $mediaTypeVal.'" data-inuse="'. $inUse.'" name="' . $val ["announcementName"]. '" id="announcementCheckBox_' . $key . '" value="true" data-exist="'. $val ["addToBroad"].'"';
			if($val ["announcementInUse"] == "In Use"){
				$html .= ' disabled';
				$opacity = "style='opacity: .5;'";
			}	
				//$html .= ' />';
			$html .= ' /><label for="announcementCheckBox_' . $key . '" ' .$opacity.'><span class="annCheckBox"></span></label>';
			$html .= '</td>';
			if($this->enterpriseLevel) {
				$html .= '<td class="thsmall">'. $val["announcementSp"] .'</td>';
			    $html .=  '<td class="thsmall">'. $val["announcementGroup"] .'</td>';
			}			
			$html .= '<td class="thsmall">';
			$html .= '<div style="float:left; width:60%; text-align:left">';
			//$html .= '<a href="javascript:void(0)" class="announcementLink" data-name="' . $val ["announcementName"] . '" data-mediaTypeVal="' . $val ["announcementMediaType"]. '" data-mediaType="' . $mediaTypeVal. '" data-sp="'. $val["announcementSp"].'" data-group="'. $val["announcementGroup"].'" data-exist="'. $val ["addToBroad"].'" >' . $val ["announcementName"] . '</a> ';
			$html .=  $val ["announcementName"];
			$html .= '</div>';
			if($val["addToBroad"] == "No" || $val["addToBroad"] == "") {
			$html .= '<div style="float:left; width:17%; text-align:center">';
			
			/*
			$html .= '<audio controls style="display: block; width:48px; height:22px;"	id="audio" src="announcementUpload/' . htmlspecialchars ( $this->sp ) . "/" . htmlspecialchars ( $this->groupId ) . "/" . $val ["announcementDescription"] . '" controlsList="nodownload">';
			$html .= '<source id="audioOgg" src="announcementUpload/' . htmlspecialchars ( $this->sp ) . "/" . htmlspecialchars ( $this->groupId ) . "/" . $val ["announcementDescription"] . '" type="audio/ogg">';
			$html .= '<source id="audioMpeg" src="announcementUpload/' . htmlspecialchars ( $this->sp ) . "/" . htmlspecialchars ( $this->groupId ) . "/" . $val ["announcementDescription"] . '" type="audio/mpeg">';
			$html .= '</audio>';
			*/
			$html .= '<img class="playAnnouncementMedia" data-mediaTypeVal="' . $val ["announcementMediaType"]. '" data-mediaType="' . $mediaTypeVal. '" src="images/Announcement/iconPlayAnn.png" data-src="announcementUpload/' . htmlspecialchars ( $val["announcementSp"] ) . "/" . htmlspecialchars ( $val["announcementGroup"] ) . "/" . $val ["announcementName"] . '" alt="Ann" width="27" height="25">';
			$html .= '</div>';
			$html .= '<div style="float:left; width:18%; text-align:center">';
			$html .= '<a href="download.php?file=' . $val ["announcementName"] . "." . strtolower($val ["announcementMediaType"]) . '&spName=' . htmlspecialchars ( $val["announcementSp"] ) . '&grpName=' . htmlspecialchars ( $val["announcementGroup"] ) . '" id="downloadAnn" class="downloadAnn"
						style="display: block; padding-left:15px"><img class="downloadMediaFile" src="images/downloadAudio.png" width="24" /></a>';
			$html .= '</div>';
			}
			$html .= '</td>';
			$html .= '<td class="thsmall">' . $inUse . '</td>';
			$html .= '<td class="thsmall">' . $val ["announcementMediaType"] . '</td>';
			$html .= '<td class="thsmall">' . $val ["announcementFileSize"] . ' </td>';
			$html .= '</tr>';
		}
		return $html;
	}
	
	/*
	 public function parseAnnouncementData($data) {
		$html = "";
		foreach ( $data as $key => $val ) {
			
			$mediaTypeVal = $this->getMediaTypeVal($val ["announcementMediaType"]);
			
			if($val["addToBroad"] == "No"){
				$inUse = "Not In Use";
			}else{
				$inUse = $val ["announcementInUse"];
			}
						
			$html .= '<tr>';
			$html .= '<td class="macTable" style="width:4%; padding: 2px 10px; ">';
			$html .= '<input type="checkbox" class="checkAnnouncement" data-sp="'. $val["announcementSp"].'" data-mediaTypeVal="' . $val ["announcementMediaType"]. '" data-group="'. $val["announcementGroup"].'" data-mediaType="'. $mediaTypeVal.'" data-inuse="'. $inUse.'" name="' . $val ["announcementName"]. '" id="announcementCheckBox_' . $key . '" value="true" data-exist="'. $val ["addToBroad"].'"';
			if($val ["announcementInUse"] == "In Use"){
				$html .= ' disabled';
				$opacity = "style='opacity: .5;'";
			}else{$opacity = "style='opacity: 1;'";}
			$html .= ' /><label for="announcementCheckBox_' . $key . '" ' .$opacity.'><span></span></label>';
			$html .= '</td>';
			if($this->enterpriseLevel) {
				$html .= '<td class="macTable" style="width:10%; padding: 2px 10px; ">'. $val["announcementSp"] .'</td>';
			    $html .=  '<td class="macTable" style="width:10%; padding: 2px 10px; ">'. $val["announcementGroup"] .'</td>';
			}			
			$html .= '<td class="macTable" style="width:30%; padding: 2px 10px; ">';
			$html .= '<div style="float:left; width:60%;">';
			$html .= '<a href="javascript:void(0)" class="announcementLink" data-name="' . $val ["announcementName"] . '" data-mediaTypeVal="' . $val ["announcementMediaType"]. '" data-mediaType="' . $mediaTypeVal. '" data-sp="'. $val["announcementSp"].'" data-group="'. $val["announcementGroup"].'" data-exist="'. $val ["addToBroad"].'" >' . $val ["announcementName"] . '</a> ';
			$html .= '</div>';
			if($val["addToBroad"] == "No" || $val["addToBroad"] == "") {
			$html .= '<div style="float:left; width:17%;">';
			
			/*
			$html .= '<audio controls style="display: block; width:48px; height:22px;"	id="audio" src="announcementUpload/' . htmlspecialchars ( $this->sp ) . "/" . htmlspecialchars ( $this->groupId ) . "/" . $val ["announcementDescription"] . '" controlsList="nodownload">';
			$html .= '<source id="audioOgg" src="announcementUpload/' . htmlspecialchars ( $this->sp ) . "/" . htmlspecialchars ( $this->groupId ) . "/" . $val ["announcementDescription"] . '" type="audio/ogg">';
			$html .= '<source id="audioMpeg" src="announcementUpload/' . htmlspecialchars ( $this->sp ) . "/" . htmlspecialchars ( $this->groupId ) . "/" . $val ["announcementDescription"] . '" type="audio/mpeg">';
			$html .= '</audio>';
			*/
			/*$html .= '<img class="playAnnouncementMedia" data-mediaTypeVal="' . $val ["announcementMediaType"]. '" data-mediaType="' . $mediaTypeVal. '" src="images/Announcement/iconPlayAnn.png" data-src="announcementUpload/' . htmlspecialchars ( $val["announcementSp"] ) . "/" . htmlspecialchars ( $val["announcementGroup"] ) . "/" . $val ["announcementName"] . '" alt="Ann" width="27" height="25">';
			$html .= '</div>';
			$html .= '<div style="float:left; width:18%;">';
			$html .= '<a href="download.php?file=' . $val ["announcementName"] . "." . strtolower($val ["announcementMediaType"]) . '&spName=' . htmlspecialchars ( $val["announcementSp"] ) . '&grpName=' . htmlspecialchars ( $val["announcementGroup"] ) . '" id="downloadAnn" class="downloadAnn"
						style="display: block; padding-left:15px"><img src="images/downloadAudio.png" width="24" /></a>';
			$html .= '</div>';
			}
			$html .= '</td>';
			$html .= '<td class="macTable" style="width:10%; padding: 2px 10px; ">' . $inUse . '</td>';
			$html .= '<td class="macTable" style="width:10%; padding: 2px 10px; ">' . $val ["announcementMediaType"] . '</td>';
			$html .= '<td class="macTable" style="width:10%; padding: 2px 10px; ">' . $val ["announcementFileSize"] . ' </td>';
			$html .= '</tr>';
		}
		return $html;
	} 
	 */
	
	public function parseAnnouncementNoData() {
		$html .= '<tr>';
		$html .= '<td colspan="6" class="macTable">No Announcement</td>';
		$html .= '</tr>';
		
		return $html;
	}
	public function showAnnouncementListTable($announcementList) {
		$announcementHtml .= $this->parseAnnouncementColumn ();
		$announcementHtml .= '<tbody>';
		if (empty ( $announcementList ["Error"] )) {
			if (count ( $announcementList ["Success"] ["row"] ) > 0) {
				
				// print the data
				$announcementHtml .= $this->parseAnnouncementData ( $announcementList ["Success"] ["row"] );
			} else {
				
				// print the no data html
				$announcementHtml .= $this->parseAnnouncementNoData ();
			}
		}
		$announcementHtml .= '</tbody>';
		$announcementHtml .= '</table>';
		$announcementHtml .= "TotalFileSize" . $announcementList ["Success"] ["totalFileSize"];
		$announcementHtml .= "MaxFileSize" . $announcementList ["Success"] ["maxFileSize"];
		$announcementHtml .= "CountRecords" . count ( $announcementList ["Success"] ["row"] );
		return $announcementHtml;
	}
	
	//function to show enterprise announcement table
	public function showAnnouncementEnterpriseListTable($announcementList) {
		
		$announcementHtml .= $this->parseAnnouncementColumn ();
		$announcementHtml .= '<tbody>';
		$countRow = 0;
		$totalFileSize = 0;
		$maxFileSize = 0;
		$count = 0;
		foreach($announcementList as $key=>$val){
				if (empty ( $val ["Error"] )) {
				    $this->tempCounter = $key;
					if (isset($val["Success"] ["row"]) && count ( $val["Success"] ["row"] ) > 0) {//print_r($val1);
						
						// print the data
						$announcementHtmlA .= $this->parseAnnouncementData ( $val ["Success"] ["row"] );
						//print_r($announcementHtmlA);
						$countRow++;
					}
				}
				
				$totalFileSize += $val["Success"]["totalFileSize"];
				$maxFileSize += $val["Success"]["maxFileSize"];
				$count += count ( $val ["Success"] ["row"] );
		}
		if($countRow == 0){
			// print the no data html
			$announcementHtml .= $this->parseAnnouncementNoData ();
		}
		$announcementHtml .= $announcementHtmlA;
		$announcementHtml .= '</tbody>';
		$announcementHtml .= '</table>';
		$announcementHtml .= "TotalFileSize" . $totalFileSize;
		$announcementHtml .= "MaxFileSize" . $maxFileSize;
		$announcementHtml .= "CountRecords" . $count;
		return $announcementHtml;
	}
	
	//get the announcementInfo
	public function getAnnouncementInfo($announcementArray) {
		global $sessionid, $client;
		
		$announcementResponse ["Error"] = "";
		$announcementResponse ["Success"] = "";
		$announcementUsageList = array();
		
		$xmlinput = xmlHeader ( $sessionid, "GroupAnnouncementFileGetRequest" );
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars ( $this->sp ) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars ( $this->groupId ) . "</groupId>";
		$xmlinput .= "<announcementFileKey>";
		$xmlinput .= "<name>" . $announcementArray ["name"] . "</name>";
		$xmlinput .= "<mediaFileType>" . $announcementArray ["mediaType"] . "</mediaFileType>";
		$xmlinput .= "</announcementFileKey>";
		
		$xmlinput .= xmlFooter ();
		$response = $client->processOCIMessage ( array (
				"in0" => $xmlinput 
		) );
		$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
// 		print_R($xml);
		if (readErrorXmlGenuine ( $xml ) != "") {
			$announcementResponse ["Error"] = strval ( $xml->command->summaryEnglish );
		} else {
			
			$announcementResponse ["Success"] ["description"] = strval ( $xml->command->description );
			$announcementResponse ["Success"] ["filesize"] = strval ( $xml->command->filesize );
			$announcementResponse ["Success"] ["lastUploaded"] = strval ( $xml->command->lastUploaded );
			
			//code to get the usageAnnouncement row count
			$r = 0;
			if(isset($xml->command->usageTable->row)){
				foreach($xml->command->usageTable->row as $key=>$val){
					$announcementUsageList [$key] [$r] ["serviceName"] = strval ( $val->col [0] );
					$announcementUsageList [$key] [$r] ["instanceName"] = strval ( $val->col [1] );
					$r++;
				}
			}
			//assign usage array to index 
			$announcementResponse ["Success"] ["usageTable"] = $announcementUsageList;
		}
		//print_r($announcementResponse);
		return $announcementResponse;
	}
	
	public function commonHeader($sessionid){
		$header = "<BroadsoftDocument protocol=\"OCI\" xmlns=\"C\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
		$header .= "<sessionId xmlns=\"\">" . $sessionid . "</sessionId>";
		return $header;
	}
	
	public function commonFooter(){
		return "\n</BroadsoftDocument>\n";
	}
	
	//function to delete files
	public function deleteFile($fileName){
		if(file_exists($fileName)){
			@unlink($fileName);
		}
	}
	// function to delete
	public function deleteAnnouncement($deleteArray) {
		global $sessionid, $client;

		$deleteResponse ["Error"] = "";
		$deleteResponse ["Success"] = "";
		$ociFlag = "false";
		
		$xmlinput = $this->commonHeader($sessionid);
		foreach($deleteArray as $key=>$val){
			
			if($val["announcementExist"] == "No"){
				$fileName =  "/var/www/html/announcementUpload/".htmlspecialchars ( $val["announcementSp"] )."/".htmlspecialchars ( $val["announcementGroupId"] )."/".$val["announcementFileName"].".".strtolower($val ["announcementFileType"]);
				$this->deleteFile($fileName);
			}else{
				$ociFlag = "true";
				$xmlinput .= "<command xsi:type=\"GroupAnnouncementFileDeleteListRequest\" xmlns=\"\">";
				$xmlinput .= "<serviceProviderId>" . htmlspecialchars ( $val["announcementSp"] ). "</serviceProviderId>";
				$xmlinput .= "<groupId>" . htmlspecialchars ( $val["announcementGroupId"]). "</groupId>";
				$xmlinput .= "<announcementFileKey>";
				$xmlinput .= "<name>" . $val["announcementFileName"] . "</name>";
				$xmlinput .= "<mediaFileType>" . $val["announcementFileType"] . "</mediaFileType>";
				$xmlinput .= "</announcementFileKey>";
				$xmlinput .= "</command>";
			
				$fileName[$key] =  "/var/www/html/announcementUpload/".htmlspecialchars ( $val["announcementSp"] )."/".htmlspecialchars ( $val["announcementGroupId"] )."/".$val["announcementFileName"].".".strtolower($val ["announcementFileType"]);
			}
		}
			if($ociFlag == "true"){
				$xmlinput .= $this->commonFooter ();
		
				$response = $client->processOCIMessage ( array (
						"in0" => $xmlinput 
				) );
				$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
				
				// print_r($xmlinput); echo"/n";
				if (readErrorXmlGenuine ( $xml ) != "") {
					$deleteResponse ["Error"] = strval ( $xml->command->summaryEnglish );
				} else {
					//print_r($xml->command);
					foreach($fileName as $key1=>$val1){
						if(file_exists($val1)){
							unlink($val1);
						}
					}
					//print_r($fileName);
					$deleteResponse ["Success"] = "Success";
				}
			}
		return $deleteResponse;
	}
	
	
	public function uploadAnnouncement($file, $uploaddir, $announcementName){
		
		$error = false;
		$files = array();
		//print_r($file);
		//$baseName = time().basename($file['name']);
		$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
		$baseName = basename($announcementName).".".$ext;
		
		if(move_uploaded_file($file['tmp_name'], $uploaddir .$baseName))
		{
			$files[] = $uploaddir .$baseName;
			$filesname[] = $baseName;
		}
		else
		{
			$error = true;
		}
		
		$data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $filesname);
		return json_encode($data);
	}
	
	public function saveAnnouncement($announcementArray){
		
		global $sessionid, $client;
		
		$saveResponse ["Error"] = "";
		$saveResponse ["Success"] = "";

		$xmlinput = xmlHeader($sessionid, "GroupAnnouncementFileAddRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars ( $this->sp ). "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars ( $this->groupId ). "</groupId>";
		$xmlinput .= "<announcementFileName>" . $announcementArray["announcementFileName"]. "</announcementFileName>";
		$xmlinput .= "<announcementFile>";
		$xmlinput .= "<description>" . $announcementArray["announcementFileName"]. "</description>";
		$xmlinput .= "<mediaType>" . strtoupper($announcementArray["announcementMediaType"]). "</mediaType>";
		$xmlinput .= "<content>" . base64_encode(file_get_contents($announcementArray["announcementContent"])). "</content>";
		$xmlinput .= "</announcementFile>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine ( $xml ) != "") {
			$saveResponse ["Error"] = strval ( $xml->command->summaryEnglish );
		} else {
			$saveResponse ["Success"] = "Success";
		}
		return $saveResponse;
	}
	
	//function to check media type
	public function checkMediaType($mediaType, $ext){
		
		$returnMedia = "true";
		
		$mediaArray = array(
				"Audio"=> array("wav", "wma"),
				"Video"=> array("3gp", "mov")
		);
		
		if(!in_array($ext, $mediaArray[$mediaType])){
			$returnMedia = "false";
		}
		
		return $returnMedia;
	}
	
	//function to validate add data
	public function checkAddSaveData($postArray, $announcementList){
	    
		$changeString = "";
		
		$backgroundColor = 'background:#72ac5d;width: 50% !important;'; $errorMessage = ''; $error = "";

                if(empty($_SESSION["groupId"])){
                    if(isset($postArray['announcementGroup'])){
                            if($postArray['announcementGroup']==""){
                                $backgroundColor = 'background:#ac5f5d;width: 50% !important;'; $error = "Error"; $errorMessage = " (Announcement Group should not be empty) ";

                            }
                            $changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Announcement Group</td><td class="errorTableRows">'.$postArray["announcementGroup"].' '. $errorMessage.' </td></tr>';
                            $backgroundColor = 'background:#72ac5d;width:50% !important;';
                            $errorMessage = "";                        
                    }                
                }
                
                if(isset($postArray['announcementName'])){
			
			if($postArray['announcementName'] == ""){
				$backgroundColor = 'background:#ac5f5d;width: 50% !important;'; $error = "Error"; $errorMessage = " (Announcement should not be empty) ";
			}else{
                            if($postArray['announcementGroup']==""){ 
                                $backgroundColor = 'background:#ac5f5d;width: 50%!important;'; $error = "Error"; $errorMessage = " (Announcement Group should not be empty) ";
                            }
                            if($postArray['announcementGroup']!=""){
				if(empty($announcementList["Error"])){
					if(count($announcementList["Success"]["row"]) > 0){
						$backgroundColor = 'background:#ac5f5d;width: 50% !important;'; $error = "Error"; $errorMessage = " (Announcement Already Exist)";
					}
				}else{
					$backgroundColor = 'background:#ac5f5d;width: 50% !important;'; $error = "Error"; $errorMessage = $announcementList["Error"];
				}
                            }
				
			}
			$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Announcement Name</td><td class="errorTableRows">'.$postArray["announcementName"].' '. $errorMessage.' </td></tr>';
                        $backgroundColor = 'background:#72ac5d;width: 50% !important;';
                        $errorMessage = ""; 
		}

		if(isset($postArray['announcementMediaType'])){
				
			if($postArray['announcementMediaType'] == ""){
				$backgroundColor = 'background:#ac5f5d;width: 50% !important;'; $error = "Error"; $errorMessage = " (Announcement Type should not be empty) ";
			}
			$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Announcement Type</td><td class="errorTableRows">'.$postArray["announcementMediaType"].' '. $errorMessage.' </td></tr>';
		}
		
		if(isset($postArray['announcementUploadVal'])){
			
			$backgroundColor = 'background:#72ac5d;width: 50%;'; $errorMessage = ''; $error = "";
			
			if($postArray['announcementUploadVal'] == ""){
				$backgroundColor = 'background:#ac5f5d;width: 50% !important;'; $error = "Error"; $errorMessage = "(Announcement File should not be empty) ";
			}else{
				
				$ext = pathinfo($postArray['announcementUploadVal'], PATHINFO_EXTENSION);
				if($postArray['announcementMediaType']  == ""){
				    
				    if(strtolower($ext) == "wav" || strtolower($ext) == "wma"){
				        $postArray['announcementMediaType'] = "Audio";
				    }else if(strtolower($ext) == "3gp" || strtolower($ext) == "mov"){
				        $postArray['announcementMediaType'] = "Video";
				    }
				}
				
				$mediaFlag = $this->checkMediaType($postArray['announcementMediaType'], $ext);
				
				if($mediaFlag == "false"){
				    
					$backgroundColor = 'background:#ac5f5d;width: 50% !important;'; $error = "Error"; $errorMessage = " (Announcement Uploaded Media is not supported) ";
				}
			}
// 			} else if(! in_array($this->fileExt, $this->supportedExt)){
// 			    /* Check Extension */
// 			    $backgroundColor = 'background:#ac5f5d;width: 50%;'; $error = "Error"; $errorMessage = "( ". $this->fileExt ." File Extension is not supported. ) ";
// 			}
			$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Announcement File</td><td class="errorTableRows">'.$postArray["announcementUploadVal"].' '. $errorMessage.' </td></tr>';
		}
		
		return $error.$changeString;
	}
	
	//function to validate modify data
	public function checkModifySaveData($postArray, $announcementList){
		
		$changeString = "";
		$_SESSION["filechanges"] = "false";
		$_SESSION["groupchanges"] = "false";
		$_SESSION["namechanges"] = "false";
		$_SESSION["mediachanges"] = "false";
		
		$backgroundColor = 'background:#72ac5d;width:50% !important;'; $errorMessage = ''; $error = "";
		if(isset($postArray['announcementGroup']) && empty($_SESSION["groupId"])){
			if($_SESSION["addAnnouncement"]["announcementGroup"] <> $postArray['announcementGroup']){	
				$_SESSION["groupchanges"] = "true";
				if($postArray['announcementGroup'] == ""){
					$backgroundColor = 'background:#ac5f5d;width:50% !important;'; $error = "Error"; $errorMessage = " (Group should not be empty) ";
				}else{
					$backgroundColor = 'background:#72ac5d;width:50% !important;'; $errorMessage = ""; $error = "";
				}
				$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">Announcement Group</td><td class="errorTableRows">'.$postArray["announcementGroup"].' '. $errorMessage.' </td></tr>';
			}
		}
		
		if(isset($postArray['announcementName'])){
			if($_SESSION["addAnnouncement"]["announcementName"] <> $postArray['announcementName']){
				$_SESSION["namechanges"] = "true";
				if($postArray['announcementName'] == ""){
					$backgroundColor = 'background:#ac5f5d;width:50% !important;'; $error = "Error"; $errorMessage = " (Announcement should not be empty) ";
				}else{
					if($postArray["announcementId"] <> $postArray["announcementName"]){
						if(empty($announcementList["Error"])){
							if(count($announcementList["Success"]["row"]) > 0 && $postArray["announcementId"] <> $announcementList["Success"]["row"][0]["announcementName"]){
								$backgroundColor = 'background:#ac5f5d;width:50% !important;'; $error = "Error"; $errorMessage = " (Announcement Already Exist)";
							}
						}else{
								$backgroundColor = 'background:#ac5f5d;width:50% !important;'; $error = "Error"; $errorMessage = $announcementList["Error"];
							}
					}				
				}
			$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'width:50% !important;">Announcement Name</td><td class="errorTableRows">'.$postArray["announcementName"].' '. $errorMessage.' </td></tr>';
			}
		}
		if(isset($postArray['announcementMediaType'])){
			if($_SESSION["addAnnouncement"]["announcementMediaType"] <> $postArray['announcementMediaType']){
				$_SESSION["mediachanges"] = "true";
				if($postArray['announcementMediaType'] == ""){
					$backgroundColor = 'background:#ac5f5d;width:50% !important'; $error = "Error"; $errorMessage = " (Announcement Type should not be empty) ";
				}else{
					
					$ext = pathinfo($postArray['announcementUploadVal'], PATHINFO_EXTENSION);
					$mediaFlag = $this->checkMediaType($postArray['announcementMediaType'], $ext);
					
					if($mediaFlag == "false"){
						$backgroundColor = 'background:#ac5f5d;width: 50% !important;'; $error = "Error"; $errorMessage = " (Announcement Uploaded is not matched with media type, not supported) ";
					}else{
						$backgroundColor = 'background:#72ac5d;width:50% !important;'; $errorMessage = ""; $error = "";
					}
					
				}
				$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'width: 50% !important;">Announcement Type</td><td class="errorTableRows">'.$postArray["announcementMediaType"].' '. $errorMessage.' </td></tr>';
			}
		}
		
		if(isset($postArray['announcementUploadVal'])){ 
			if($_SESSION["addAnnouncement"]["announcementUploadVal"] <> $postArray['announcementUploadVal']){
				$_SESSION["filechanges"] = "true";
				if($postArray['announcementUploadVal'] == ""){
					$backgroundColor = 'background:#ac5f5d;width:50% !important'; $error = "Error"; $errorMessage = "(Announcement File should not be empty) ";
				}else{
				
					$ext = pathinfo($postArray['announcementUploadVal'], PATHINFO_EXTENSION);
					$mediaFlag = $this->checkMediaType($postArray['announcementMediaType'], $ext);
					
					if($mediaFlag == "false"){
						$backgroundColor = 'background:#ac5f5d;width: 50%;'; $error = "Error"; $errorMessage = " (Announcement Uploaded Media is not supported) ";
					}else{
						$backgroundColor = 'background:#72ac5d;width:50% !important'; $errorMessage = ""; $error = "";
					}
				}
				$changeString .= '<tr><td class="errorTableRows" style="'.$backgroundColor.' width: 50% !important;">Announcement File</td><td class="errorTableRows">'.$postArray["announcementUploadVal"].' '. $errorMessage.' </td></tr>';
			}
		}
		
		if($_SESSION["filechanges"] == "false" && $_SESSION["groupchanges"] == "false" && $_SESSION["namechanges"] == "false" && $_SESSION["mediachanges"] == "false"){
			$changeString = '<tr><td class="errorTableRows" style="'.$backgroundColor.'width: 50% !important;">Announcement </td><td class="errorTableRows">No Changes </td></tr>';
		}
		
		return $error.$changeString;
	}
	
	function getAnnouncementServerProviderList(){
		global $sessionid;
		global $client;
		
		$sps = array();
		$getSPName = array();
		$xmlinput = xmlHeader($sessionid, "ServiceProviderGetListRequest");
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$a = 0;
		foreach ($xml->command->serviceProviderTable->row as $k => $v)
		{
			$sps[$a] = strval($v->col[0]);
			// $getSPName[$a] = strval($v->col[0].' - '.$v->col[1]);
			if(isset($v->col[1]) && !empty($v->col[1])){
				// $getSPName[$a] = strval($v->col[0].' - '.$v->col[1]);
				$getSPName[$a] = strval($v->col[0].'<span>-</span>'.$v->col[1]); //ex-774
			}else{
				$getSPName[$a] = strval($v->col[0]);
			}
			$a++;
		}
		
		$sps = subval_sort($sps);
		return $sps;
	}
	
	function getAnnouncementSpGroupList($serP){
		
		$allGroups = array();
		global $sessionid;
		global $client;
		
		$xmlinput = xmlHeader($sessionid, "GroupGetListInServiceProviderRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($serP) . "</serviceProviderId>";
		$xmlinput .= xmlFooter();
		if(isset($client)) {
			try {
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				if(isset($response->processOCIMessageReturn) && $response->processOCIMessageReturn != "") {
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
					$a = 0;
					if (isset($xml->command->groupTable->row))
					{
						foreach ($xml->command->groupTable->row as $key => $value)
						{
							$allGroups[$a] = strval($value->col[0]);
							$a++;
						}
					}
				}
				
			} catch (SoapFault $E) {
				// echo $E->faultstring;
			}
		}
		return $allGroups;
	}
	

	//function to modify data
	public function saveModifyAnnouncement($announcementArray){
		
		global $sessionid, $client;
		
		$saveResponse ["Error"] = "";
		$saveResponse ["Success"] = "";
		//$mediaType = $this->getMediaTypeVal($announcementArray["announcementMediaType"]);
		$mediaType = strtoupper($announcementArray["announcementMediaType"]);
		//print_r($announcementArray);die;
		$xmlinput = xmlHeader($sessionid, "GroupAnnouncementFileModifyRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars ( $this->sp ). "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars ( $this->groupId ). "</groupId>";
		$xmlinput .= "<announcementFileKey>";
			$xmlinput .= "<name>" . $announcementArray["announcementName"]. "</name>";
			$xmlinput .= "<mediaFileType>" . $mediaType. "</mediaFileType>";
		$xmlinput .= "</announcementFileKey>";
		
		if(!empty($announcementArray["newAnnouncementFileName"])){
			$xmlinput .= "<newAnnouncementFileName>" . $announcementArray["newAnnouncementFileName"] . "</newAnnouncementFileName>";
		}
		if( isset($announcementArray["fileUploaded"]) && $announcementArray["fileUploaded"] == "Yes") {
		    $xmlinput .= "<announcementFile>";
		    
		    $xmlinput .= "<description>" . $announcementArray["announcementDescription"]. "</description>";
		    $xmlinput .= "<mediaType>" . $mediaType . "</mediaType>";
		    if(isset($announcementArray["fileChanged"]) && !empty($announcementArray["fileChanged"])){
		        $xmlinput .= "<content>" . base64_encode(file_get_contents($announcementArray["announcementContent"])). "</content>";
		    }else{
		        $xmlinput .= "<sourceFileName>" . $announcementArray["announcementUploadVal"] . "</sourceFileName>";
		    }
		    
		    $xmlinput .= "</announcementFile>";
		}
		
// 		$xmlinput .= "<announcementFileName>" . $announcementArray["announcementFileName"]. "</announcementFileName>";
// 		$xmlinput .= "<announcementFile>";
// 		$xmlinput .= "<description>" . $announcementArray["announcementFileName"]. "</description>";
// 		$xmlinput .= "<mediaType>" . $announcementArray["announcementMediaType"]. "</mediaType>";
// 		$xmlinput .= "<content>" . base64_encode(file_get_contents($announcementArray["announcementContent"])). "</content>";
// 		$xmlinput .= "</announcementFile>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine ( $xml ) != "") {
			$saveResponse ["Error"] = strval ( $xml->command->summaryEnglish );
		} else {
			$saveResponse ["Success"] = "Success";
		}
		return $saveResponse;
	}
	
	public function makeAddArray($postArray){
	    $postArray["announcementId"] = isset($postArray["announcementId"]) ? $postArray["announcementId"] : "";
	    $fieldsToLogIn = array("announcementName", "announcementType", "filenames", "announcementId");
	    $logArray = array();
	    foreach($postArray as $key=>$val){
	        if(in_array($key, $fieldsToLogIn)) {
	            if($key == "filenames"){
	                $logArray[$key] = $val[0];
	            }else{
	                $logArray[$key] = $val;
	            }
	        }
	    }
	    
	    //unset($logArray["announcementUploadVal"]);
	    // unset($logArray["checkAnnouncementWIthAutoattendant"]);
	    // unset($logArray["announcementGroup"]);
	    
	    return $logArray;
	} 
	
	public function getListRepository($sp, $allGroupsArray){
// 	    print_r($sp); exit;
// 		print_R($allGroupsArray);
// 		print_R($sp);

		$supportedExt = array("wma", "wav", "3gp", "mov");
		
		$listArray = array();
		foreach($allGroupsArray as $key1=>$val1){
			$dir = "/var/www/html/announcementUpload/".$sp."/".$val1;
			//print_r($dir); exit;
			$files1 = scandir($dir, 1);
			$filesArray = "";
			$listArray[$key1]["Error"] = "";
			$listArray[$key1]["Success"] = "";
			
			$r = 0;
			
			foreach($files1 as $key2 => $val2){
				$ext = pathinfo($val2, PATHINFO_EXTENSION);
				if(in_array($ext, $supportedExt)){
					$nameExplode = explode(".", $val2);
					
					$fileSize = filesize($dir."/".$val2);
					$fileSizeBytes = number_format($fileSize/ 1024, 2);
					
					$filesArray[$r]['announcementName'] = $nameExplode[0];
					$filesArray[$r]['announcementSp'] = $sp;
					$filesArray[$r]['announcementGroup'] = $val1;
					$filesArray[$r]['announcementMediaType'] = strtoupper($ext);
					
					$filesArray[$r]['announcementFileSize'] = $fileSizeBytes;
					$filesArray[$r]['announcementDescription'] = $nameExplode[0];
					$filesArray[$r]['usageTable'] = "";
					$filesArray[$r]['announcementInUse'] = "";
				}
				$r++;
			}
			$listArray[$key1]["Success"]["row"] = $filesArray;
			$listArray[$key1]["Success"]["announcementSp"] = $sp;
			$listArray[$key1]["Success"]["announcementGroup"] = $val1;
		}
		
		return $listArray;
		
	}
	
	public function compareAnnouncementRepository($bwArray, $repoArray, $searchArray){
	    $fileSize = 0;
	    $maxFileSize = 0;
	    if(isset($_SESSION['groupId'])) {
        	    $bwNewArray = $this->makeCompatibleArray($bwArray);
        	    $repoNewArray = $this->makeCompatibleArray($repoArray[0]);
        	    $fileSize = $bwArray['Success']['totalFileSize'];
        	    $maxFileSize = $bwArray['Success']['maxFileSize'];
	    } else if($this->searchLevel == "System") {
    	        $bwNewArray = $this->makeCompatibleArrayForEnt($bwArray);
    	        $repoNewArray = $this->makeCompatibleArrayForSystem($repoArray);
    	        if(count($bwArray)>0){
    	            foreach ($bwArray as $fileSizeKey => $fileSizeVal){
    	                $fileSize += $fileSizeVal["Success"]["totalFileSize"];
    	            }
    	        }
	    }
	    else {
    	        $bwNewArray = $this->makeCompatibleArrayForEnt($bwArray);
    	        $repoNewArray = $this->makeCompatibleArrayForEnt($repoArray);
    	        if(count($bwArray)>0){
    	            foreach ($bwArray as $fileSizeKey => $fileSizeVal){
    	                $fileSize += $fileSizeVal["Success"]["totalFileSize"];
    	            }
    	        }
	    }
	    //echo "fileSize - ";print_r($fileSize);
	   //code to match the announcement exist in bw
		foreach($bwNewArray as $bk => $bv) {
			foreach($bv as $bk1 =>  $bv1){
				if( ! isset($repoNewArray[$bk][$bk1])) {
					$bwNewArray[$bk][$bk1]["addToBroad"] = "Yes";
				}
			}
		}
		$finalArray = $bwNewArray;
		
		//code to match the announcement exist in repository
		foreach($repoNewArray as $rkey => $rval) {
			foreach($rval as $rk =>  $rv){
				if( ! isset($bwNewArray[$rkey][$rk])) {
					$repoNewArray[$rkey][$rk]["addToBroad"] = "No";
				}
			}
		}
		/* Array merge */
		foreach($repoNewArray as $skey => $sval) {
			foreach($sval as $sk =>  $sv){
				if( ! isset($finalArray[$skey][$sk])) {
					$finalArray[$skey][$sk] = $sv;
				}
			}
		}
		
		$finalArray = $this->filterArrayData($finalArray, $searchArray);
		//print_r($bwArray);exit;
		//make the array as compatible as search data
		$finalTableArray = array();
		$t = 0;
		foreach($finalArray as $fKey => $fVal){
			
			$finalTableArray[$t]["Error"] = "";
			$finalTableArray[$t]["Success"] = "";
			$w = 0;
			$rowArray = array();
			foreach($fVal as $fKey1 => $fVal1){
				$rowArray[$w] = $fVal1;
				$w++;
			}
			$finalTableArray[$t]["Success"]["row"] = $rowArray;
			$t++;
		}
		
		//$finalTableArray[0]["Success"]['totalFileSize'] = $bwArray['Success']['totalFileSize'];
		//$finalTableArray[0]["Success"]['maxFileSize'] = $bwArray['Success']['maxFileSize'];
		$finalTableArray[0]["Success"]['totalFileSize'] = $fileSize;
		$finalTableArray[0]["Success"]['maxFileSize'] = $maxFileSize;
		return $finalTableArray;
	}
	
	
	/*New Filter functions*/
	
	public function filterArrayData($arrayData, $searchArray){
	    foreach ($arrayData as $datakey => $dataVal) {
	        foreach ($dataVal as $dataK => $dataV) {
	            $unsetwData = $this->applyFiterWithCriteria($dataV, $searchArray);
	            if(! $unsetwData ) {
	                unset($arrayData[$datakey][$dataK]);
	            }
	        }
	    }
	    return $arrayData;
	}
	
	public function applyFiterWithCriteria($searchVal, $searchArray) {
	    
	    $audioType = array("WAV", "WMA");
	    $videoType = array("3GP", "MOV");
	    $searchArray = array_map('strtoupper', $searchArray);
	    
	    $searchAnn = true;
	    $searchCriteria= true;
	    
	    if ($searchArray["searchCriteria"] == "" && $searchArray["searchAnnouncement"] == "" ) {
	        return true;
	    }
	    
	    /* Otherwise match filter data*/
	    if(($searchArray["searchCriteria"] == "" && isset($searchArray["searchAnnouncement"]) && $searchArray["searchAnnouncement"] != "")) {
	        if(stripos($searchVal["announcementMediaType"], $searchArray["searchAnnouncement"]) !== false ||
	            stripos($searchVal["announcementName"], $searchArray["searchAnnouncement"]) !== false
	            ) {
	                $searchAnn = true;
	            }
	            else {
	                $searchAnn = false;
	            }
	    }
	    
	    if((isset($searchArray["searchCriteria"]) && $searchArray["searchCriteria"] != "") ) {
	        if($searchArray["searchCriteria"] == "AUDIO") {
	            if($searchArray["searchAnnouncement"] == "") {
	                if($searchArray["searchCriteria"] == "AUDIO" && in_array($searchVal["announcementMediaType"], $audioType)){
	                    $searchCriteria= true;
	                }
	                else {
	                    $searchCriteria= false;
	                }
	            } else {
	                if($searchArray["searchCriteria"] == "AUDIO" && in_array($searchVal["announcementMediaType"], $audioType) &&
	                    ( stripos($searchVal["announcementMediaType"], $searchArray["searchAnnouncement"]) !== false ||
	                    stripos($searchVal["announcementName"], $searchArray["searchAnnouncement"]) !== false )
	                    ){
	                        $searchCriteria= true;
	                }
	                else {
	                    $searchCriteria= false;
	                }
	            }
	            
	        }
	        
	        
	        if($searchArray["searchCriteria"] == "VIDEO") {
	            if($searchArray["searchAnnouncement"] == "") {
	                if($searchArray["searchCriteria"] == "VIDEO" && in_array($searchVal["announcementMediaType"], $videoType)){
	                    $searchCriteria= true;
	                }
	                else {
	                    $searchCriteria= false;
	                }
	            } else {
	                if($searchArray["searchCriteria"] == "VIDEO" && in_array($searchVal["announcementMediaType"], $videoType) &&
	                    ( stripos($searchVal["announcementMediaType"], $searchArray["searchAnnouncement"]) !== false ||
	                    stripos($searchVal["announcementName"], $searchArray["searchAnnouncement"]) !== false )
	                    ){
	                        $searchCriteria= true;
	                }
	                else {
	                    $searchCriteria= false;
	                }
	            }
	            
	        }
	        
	        if($searchArray["searchCriteria"] == "ANNOUNCEMENTNAME") {
	            if($searchArray["searchCriteria"] == "ANNOUNCEMENTNAME" && $searchArray["searchAnnouncement"] == "") {
	                return true;
	            }
	            else if($searchArray["searchCriteria"] == "ANNOUNCEMENTNAME" && $searchArray["searchAnnouncement"] != "" &&
	                stripos($searchVal["announcementName"], $searchArray["searchAnnouncement"]) !== false
	                ){
	                    $searchCriteria= true;
	            }
	            else {
	                $searchCriteria= false;
	            }
	        }
	    }
	    
	    return $searchAnn && $searchCriteria;
	}
	
	/*End New Filter functions*/
	
	
	
	
	/*
	 public function makeCompatibleArray($bwArray, $searchArray){
	    print_r($bwArray); exit;
		foreach($bwArray as $key=>$val){
			if(empty($val["Error"])){
				if(count($val["Success"]["row"]) > 0){
					$aaArray = array();
					foreach($val["Success"]["row"] as $key1=>$val1){
					    print_r($val1); exit;
					    $this->getArrayData($val1)($searchArray, $val1);
					}
					$bwNewArray[$val["Success"]["announcementGroup"]] = $aaArray;
				}
				else{
					$bwNewArray[$val["Success"]["announcementGroup"]] = array();
				}
			}
		}
		return $bwNewArray;
	}
	  */
	
	public function makeCompatibleArrayForSystem($bwArrayEnt){
	    
	    foreach($bwArrayEnt as $bwArrayEntKey1 => $bwArrayEntVal1) {
	        foreach($bwArrayEntVal1 as $bwArrKey => $bwArrVal) {
	           if(empty($bwArrVal["Error"])){
	               if(count($bwArrVal["Success"]["row"]) > 0){
	                   $aaArray = array();
	                   foreach($bwArrVal["Success"]["row"] as $key1=>$val1){
	                       $aaArray[$val1["announcementName"]]["announcementName"] = $val1["announcementName"];
	                       $aaArray[$val1["announcementName"]]["announcementSp"] = $val1["announcementSp"];
	                       $aaArray[$val1["announcementName"]]["announcementGroup"] = $val1["announcementGroup"];
	                       $aaArray[$val1["announcementName"]]["announcementMediaType"] = $val1["announcementMediaType"];
	                       $aaArray[$val1["announcementName"]]["announcementFileSize"] = $val1["announcementFileSize"];
	                       $aaArray[$val1["announcementName"]]["announcementDescription"] = $val1["announcementDescription"];
	                       $aaArray[$val1["announcementName"]]["usageTable"] = $val1["usageTable"];
	                       $aaArray[$val1["announcementName"]]["announcementInUse"] = $val1["announcementInUse"];
	                   }
	                   $bwNewArray[$bwArrVal["Success"]["announcementGroup"]] = $aaArray;
	               }
	               else{
	                   $bwNewArray[$bwArrVal["Success"]["announcementGroup"]] = array();
	               }
	           }
	       }
	   }
	    
	    return $bwNewArray;
	}
	
	public function makeCompatibleArrayForEnt($bwArrayEnt){
	    foreach($bwArrayEnt as $bwArrKey => $bwArrVal) {
	        if(empty($bwArrVal["Error"])){
	            if(count($bwArrVal["Success"]["row"]) > 0){
	                $aaArray = array();
	                foreach($bwArrVal["Success"]["row"] as $key1=>$val1){
	                    $aaArray[$val1["announcementName"]]["announcementName"] = $val1["announcementName"];
	                    $aaArray[$val1["announcementName"]]["announcementSp"] = $val1["announcementSp"];
	                    $aaArray[$val1["announcementName"]]["announcementGroup"] = $val1["announcementGroup"];
	                    $aaArray[$val1["announcementName"]]["announcementMediaType"] = $val1["announcementMediaType"];
	                    $aaArray[$val1["announcementName"]]["announcementFileSize"] = $val1["announcementFileSize"];
	                    $aaArray[$val1["announcementName"]]["announcementDescription"] = $val1["announcementDescription"];
	                    $aaArray[$val1["announcementName"]]["usageTable"] = $val1["usageTable"];
	                    $aaArray[$val1["announcementName"]]["announcementInUse"] = $val1["announcementInUse"];
	                }
	                $bwNewArray[$bwArrVal["Success"]["announcementGroup"]] = $aaArray;
	            }
	            else{
	                $bwNewArray[$bwArrVal["Success"]["announcementGroup"]] = array();
	            }
	        }
	    }
	    
	    return $bwNewArray;
	}
	
	
	public function makeCompatibleArray($bwArray){
	    
	       if(empty($bwArray["Error"])){
	           if(count($bwArray["Success"]["row"]) > 0){
	                $aaArray = array();
	                foreach($bwArray["Success"]["row"] as $key1=>$val1){
	                    $aaArray[$val1["announcementName"]]["announcementName"] = $val1["announcementName"];
	                    $aaArray[$val1["announcementName"]]["announcementSp"] = $val1["announcementSp"];
	                    $aaArray[$val1["announcementName"]]["announcementGroup"] = $val1["announcementGroup"];
	                    $aaArray[$val1["announcementName"]]["announcementMediaType"] = $val1["announcementMediaType"];
	                    $aaArray[$val1["announcementName"]]["announcementFileSize"] = $val1["announcementFileSize"];
	                    $aaArray[$val1["announcementName"]]["announcementDescription"] = $val1["announcementDescription"];
	                    $aaArray[$val1["announcementName"]]["usageTable"] = $val1["usageTable"];
	                    $aaArray[$val1["announcementName"]]["announcementInUse"] = $val1["announcementInUse"];
	                }
	                $bwNewArray[$bwArray["Success"]["announcementGroup"]] = $aaArray;
	            }
	            else{
	                $bwNewArray[$bwArray["Success"]["announcementGroup"]] = array();
	            }
	        }
	   
	    return $bwNewArray;
	}
	
	public function getArrayData($val1) {
	    $aaArray[$val1["announcementName"]]["announcementName"] = $val1["announcementName"];
	    $aaArray[$val1["announcementName"]]["announcementSp"] = $val1["announcementSp"];
	    $aaArray[$val1["announcementName"]]["announcementGroup"] = $val1["announcementGroup"];
	    $aaArray[$val1["announcementName"]]["announcementMediaType"] = $val1["announcementMediaType"];
	    $aaArray[$val1["announcementName"]]["announcementFileSize"] = $val1["announcementFileSize"];
	    $aaArray[$val1["announcementName"]]["announcementDescription"] = $val1["announcementDescription"];
	    $aaArray[$val1["announcementName"]]["usageTable"] = $val1["usageTable"];
	    $aaArray[$val1["announcementName"]]["announcementInUse"] = $val1["announcementInUse"];
	
	    return $aaArray;
	}
	/*
	public function getSearchArray($searchArray, $val1){
		
		if($searchArray["searchCriteria"] == "announcementName"){
			if(!empty($searchArray["searchAnnouncement"]) && $searchArray["searchAnnouncement"] == $val1["announcementName"]){
				$aaArray[$val1["announcementName"]]["announcementName"] = $val1["announcementName"];
				$aaArray[$val1["announcementName"]]["announcementSp"] = $val1["announcementSp"];
				$aaArray[$val1["announcementName"]]["announcementGroup"] = $val1["announcementGroup"];
				$aaArray[$val1["announcementName"]]["announcementMediaType"] = $val1["announcementMediaType"];
				$aaArray[$val1["announcementName"]]["announcementFileSize"] = $val1["announcementFileSize"];
				$aaArray[$val1["announcementName"]]["announcementDescription"] = $val1["announcementDescription"];
				$aaArray[$val1["announcementName"]]["usageTable"] = $val1["usageTable"];
				$aaArray[$val1["announcementName"]]["announcementInUse"] = $val1["announcementInUse"];
			}else{
				$aaArray[$val1["announcementName"]]["announcementName"] = $val1["announcementName"];
				$aaArray[$val1["announcementName"]]["announcementSp"] = $val1["announcementSp"];
				$aaArray[$val1["announcementName"]]["announcementGroup"] = $val1["announcementGroup"];
				$aaArray[$val1["announcementName"]]["announcementMediaType"] = $val1["announcementMediaType"];
				$aaArray[$val1["announcementName"]]["announcementFileSize"] = $val1["announcementFileSize"];
				$aaArray[$val1["announcementName"]]["announcementDescription"] = $val1["announcementDescription"];
				$aaArray[$val1["announcementName"]]["usageTable"] = $val1["usageTable"];
				$aaArray[$val1["announcementName"]]["announcementInUse"] = $val1["announcementInUse"];
			}
		}else if($searchArray["searchCriteria"] == "Audio"){
			if(!empty($searchArray["searchAnnouncement"]) && $searchArray["searchAnnouncement"] == $val1["announcementMediaType"]){
				$aaArray[$val1["announcementName"]]["announcementName"] = $val1["announcementName"];
				$aaArray[$val1["announcementName"]]["announcementSp"] = $val1["announcementSp"];
				$aaArray[$val1["announcementName"]]["announcementGroup"] = $val1["announcementGroup"];
				$aaArray[$val1["announcementName"]]["announcementMediaType"] = $val1["announcementMediaType"];
				$aaArray[$val1["announcementName"]]["announcementFileSize"] = $val1["announcementFileSize"];
				$aaArray[$val1["announcementName"]]["announcementDescription"] = $val1["announcementDescription"];
				$aaArray[$val1["announcementName"]]["usageTable"] = $val1["usageTable"];
				$aaArray[$val1["announcementName"]]["announcementInUse"] = $val1["announcementInUse"];
			}else{
				$aaArray[$val1["announcementName"]]["announcementName"] = $val1["announcementName"];
				$aaArray[$val1["announcementName"]]["announcementSp"] = $val1["announcementSp"];
				$aaArray[$val1["announcementName"]]["announcementGroup"] = $val1["announcementGroup"];
				$aaArray[$val1["announcementName"]]["announcementMediaType"] = $val1["announcementMediaType"];
				$aaArray[$val1["announcementName"]]["announcementFileSize"] = $val1["announcementFileSize"];
				$aaArray[$val1["announcementName"]]["announcementDescription"] = $val1["announcementDescription"];
				$aaArray[$val1["announcementName"]]["usageTable"] = $val1["usageTable"];
				$aaArray[$val1["announcementName"]]["announcementInUse"] = $val1["announcementInUse"];
			}
		}else if($searchArray["searchCriteria"] == "Video"){
			if(!empty($searchArray["searchAnnouncement"]) && $searchArray["searchAnnouncement"] == $val1["announcementMediaType"]){
				$aaArray[$val1["announcementName"]]["announcementName"] = $val1["announcementName"];
				$aaArray[$val1["announcementName"]]["announcementSp"] = $val1["announcementSp"];
				$aaArray[$val1["announcementName"]]["announcementGroup"] = $val1["announcementGroup"];
				$aaArray[$val1["announcementName"]]["announcementMediaType"] = $val1["announcementMediaType"];
				$aaArray[$val1["announcementName"]]["announcementFileSize"] = $val1["announcementFileSize"];
				$aaArray[$val1["announcementName"]]["announcementDescription"] = $val1["announcementDescription"];
				$aaArray[$val1["announcementName"]]["usageTable"] = $val1["usageTable"];
				$aaArray[$val1["announcementName"]]["announcementInUse"] = $val1["announcementInUse"];
			}else{
				$aaArray[$val1["announcementName"]]["announcementName"] = $val1["announcementName"];
				$aaArray[$val1["announcementName"]]["announcementSp"] = $val1["announcementSp"];
				$aaArray[$val1["announcementName"]]["announcementGroup"] = $val1["announcementGroup"];
				$aaArray[$val1["announcementName"]]["announcementMediaType"] = $val1["announcementMediaType"];
				$aaArray[$val1["announcementName"]]["announcementFileSize"] = $val1["announcementFileSize"];
				$aaArray[$val1["announcementName"]]["announcementDescription"] = $val1["announcementDescription"];
				$aaArray[$val1["announcementName"]]["usageTable"] = $val1["usageTable"];
				$aaArray[$val1["announcementName"]]["announcementInUse"] = $val1["announcementInUse"];
			}
		}else{
			$aaArray[$val1["announcementName"]]["announcementName"] = $val1["announcementName"];
			$aaArray[$val1["announcementName"]]["announcementSp"] = $val1["announcementSp"];
			$aaArray[$val1["announcementName"]]["announcementGroup"] = $val1["announcementGroup"];
			$aaArray[$val1["announcementName"]]["announcementMediaType"] = $val1["announcementMediaType"];
			$aaArray[$val1["announcementName"]]["announcementFileSize"] = $val1["announcementFileSize"];
			$aaArray[$val1["announcementName"]]["announcementDescription"] = $val1["announcementDescription"];
			$aaArray[$val1["announcementName"]]["usageTable"] = $val1["usageTable"];
			$aaArray[$val1["announcementName"]]["announcementInUse"] = $val1["announcementInUse"];
		}
		return $aaArray;
	}
	*/
	
}