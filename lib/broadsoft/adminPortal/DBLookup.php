<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

/**
 * Created by Karl.
 * Date: 9/11/2016
 */
class DBLookup
{
    private $db;
    private $table;
    private $columns = array();
    private $keyCol = "";
    private $valCol = "";
    private $hasKey = false;

    public function __construct($db, $table, $key = "", $val = "") {
        $this->db = $db;
        $this->table = $table;
        $this->keyCol = $key;
        $this->valCol = $val;

        $this->getColumnNames($table);
    }

    public function setKeyValueColumns($key, $value) {
        $this->keyCol = $key;
        $this->valCol = $value;
    }


    public function get($key) {
        //Code added @ 18 July 2019
        $whereCndtn = " ";
        if($_SESSION['cluster_support']){
           $selectedCluster = $_SESSION['selectedCluster'];
           $whereCndtn = " AND clusterName ='$selectedCluster' ";
        }
        //End code
        $query = "select * from " . $this->table . " where " . $this->keyCol . "='" . $key . "' $whereCndtn ";
        $result = $this->db->query($query);

        while ($row = $result->fetch()) {
            $this->hasKey = true;
            return $row[$this->valCol];
        }

        $this->hasKey = false;
        return "";
    }

    public function set($key, $value) {
        $this->get($key);
        //Code added @ 18 July 2019
        $whereCndtn = " ";
        if($_SESSION['cluster_support']){
           $selectedCluster = $_SESSION['selectedCluster'];
           $whereCndtn = " AND clusterName ='$selectedCluster' ";
        }
        //End code
        if ($this->hasKey) {
            $query  = "update " . $this->table . " set " . $this->valCol . "='" . $value . "' ";
            $query .= "where " . $this->keyCol . "='" . $key . "' $whereCndtn ";
        } else {
            $query  = "insert into " . $this->table . " (" . $this->keyCol . ", " . $this->valCol . ") ";
            $query .= "values ('" . $key . "', '" . $value . "')";
        }
        $this->db->query($query);
    }


    public function delete($key) {
        $query  = "delete from " . $this->table . " ";
        $query .= "where " . $this->keyCol . "='" . $key . "'";

        $this->db->query($query);
    }


    private function getColumnNames($table) {
        $query = "describe " . $table;
        $result = $this->db->query($query);

        while ($row = $result->fetch()) {
            $this->columns[] = $row["Field"];
        }
    }

}
