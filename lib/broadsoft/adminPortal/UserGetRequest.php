<?php

/**
 * Date: 6/27/2017
 */
class UserGetRequest
{
    private $deviceName;
    private $firstName;
    private $lastName;
    private $addressLine1;
    private $addressLine2;
    private $city;
    private $stateOrProvince;
    private $zipOrPostalCode;
    private $emailAddress;
    private $phoneNumber;
    private $callingLineIdLastName;
    private $callingLineIdFirstName;
    private $callingLineIdPhoneNumber;
    private $extension;
    private $timeZone;
    private $networkClassOfService;
    private $department;
    private $linePort;
    private $linePortDomain;

    public function getDeviceName() { return $this->deviceName; }
    public function getFirstName() { return $this->firstName; }
    public function getLastName() { return $this->lastName; }
    public function getAddressLine1() { return $this->addressLine1; }
    public function getAddressLine2() { return $this->addressLine2; }
    public function getCity() { return $this->city; }
    public function getStateOrProvince() { return $this->stateOrProvince; }
    public function getZipOrPostalCode() { return $this->zipOrPostalCode; }
    public function getEmailAddress() { return $this->emailAddress; }
    public function getPhoneNumber() { return $this->phoneNumber; }
    public function getCallingLineIdLastName() { return $this->callingLineIdLastName; }
    public function getCallingLineIdFirstName() { return $this->callingLineIdFirstName; }
    public function getCallingLineIdPhoneNumber() { return $this->callingLineIdPhoneNumber; }
    public function getExtension() { return $this->extension; }
    public function getTimeZone() { return $this->timeZone; }
    public function getNetworkClassOfService() { return $this->networkClassOfService; }
    public function getDepartment() { return $this->department; }
    public function getLinePort() { return $this->linePort; }
    public function getLinePortDomain() { return $this->linePortDomain; }

    public function __construct($userId) {
        global $sessionid, $client, $ociVersion;
        if ($ociVersion == "17")
        {
            $xmlinput = xmlHeader($sessionid, "UserGetRequest17sp4");
        }
        else if ($ociVersion == "20")
        {
            $xmlinput = xmlHeader($sessionid, "UserGetRequest20");
        }
        else
        {
            $xmlinput = xmlHeader($sessionid, "UserGetRequest19");
        }
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $this->deviceName = isset($xml->command->accessDeviceEndpoint->accessDevice->deviceName) ? strval($xml->command->accessDeviceEndpoint->accessDevice->deviceName) : "";
        $this->firstName = strval($xml->command->firstName);
        $this->lastName = strval($xml->command->lastName);
        $this->addressLine1 = strval($xml->command->address->addressLine1);
        $this->addressLine2 = strval($xml->command->address->addressLine2);
        $this->city = strval($xml->command->address->city);
        $this->stateOrProvince = strval($xml->command->address->stateOrProvince);
        $this->zipOrPostalCode = strval($xml->command->address->zipOrPostalCode);
        $this->emailAddress = strval($xml->command->emailAddress);
        $this->phoneNumber = strval($xml->command->phoneNumber);
        $this->callingLineIdLastName = strval($xml->command->callingLineIdLastName);
        $this->callingLineIdFirstName = strval($xml->command->callingLineIdFirstName);
        $this->callingLineIdPhoneNumber = strval($xml->command->callingLineIdPhoneNumber);
        $this->extension = strval($xml->command->extension);
        $this->timeZone = strval($xml->command->timeZone);
        $this->networkClassOfService = isset($xml->command->networkClassOfService) ? strval($xml->command->networkClassOfService) : "";
        $this->department = strval($xml->command->department->name);

        $this->linePort = isset($xml->command->accessDeviceEndpoint->linePort) ? strval($xml->command->accessDeviceEndpoint->linePort) : "";
        $this->linePortDomain = "";
        if ($this->linePort != "") {
            $linePortDomain = explode("@", $this->linePort);
            $this->linePortDomain = $linePortDomain[1];
        }
    }
}
