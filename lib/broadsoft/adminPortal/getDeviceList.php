<?php
	$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->accessDeviceTable->row as $key => $value)
	{
		$devices[$a] = array("deviceName" => strval($value->col[0]), "deviceType" => strval($value->col[1]));
		$a++;
	}
?>
