<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

	$xmlinput = xmlHeader($sessionid, "SystemDeviceTypeGetAvailableListRequest19");
	$xmlinput .= "<allowConference>true</allowConference>";
	$xmlinput .= "<allowMusicOnHold>true</allowMusicOnHold>";
	$xmlinput .= "<onlyConference>false</onlyConference>";
	$xmlinput .= "<onlyVideoCapable>false</onlyVideoCapable>";
	$xmlinput .= "<onlyOptionalIpAddress>false</onlyOptionalIpAddress>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

    $nonObsoleteDevices = array();
    $staticLineOrderingDeviceTypes = array();

	$a = 0;
	foreach ($xml->command->deviceType as $key => $value)
	{
		$nonObsoleteDevices[$a] = strval($value);
		$a++;
	}
?>
