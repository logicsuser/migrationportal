<?php
//print_r($_SESSION["adminType"])."wjwwjwjw";
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
class DeviceConfigCustomTagManagement{
        
        function getCustomTagsListsByDeviceType($deviceType){            
	    global $sessionid, $client;
		$userAssignedTags = array();
		//$xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagGetListRequest");
                $xmlinput  = xmlHeader($sessionid, "GroupDeviceTypeCustomTagGetListRequest");           
		$xmlinput .= "<serviceProviderId>" .  trim(htmlspecialchars($_SESSION["sp"])) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . trim(htmlspecialchars($_SESSION["groupId"])) . "</groupId>";
		$xmlinput .= "<deviceType>" . trim($deviceType) . "</deviceType>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		                
		$errMsg = readErrorXmlGenuine($xml);
		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";                
                
		if($errMsg != "") {
			$returnResponse["Error"] = $errMsg;
		} else {
		    if(isset($xml->command->groupDeviceTypeCustomTagsTable->row)) {
				//foreach ($xml->command->deviceCustomTagsTable->row as $key => $value){
                            $count = 0;                            
			    foreach ($xml->command->groupDeviceTypeCustomTagsTable->row as $key => $value){
				    
					$userAssignedTags[$count]["tagName"] = strval($value->col[0]);
                                        $userAssignedTags[$count]["tagValue"] = strval($value->col[1]);
                                        $count = $count + 1;
				}
			} else {
				
			}
			
			$returnResponse["Success"] = $userAssignedTags;
		}		
		return $returnResponse;
	}
	
	function getCustomTagsList($deviceType){            
	    global $sessionid, $client;
		$userAssignedTags = array();
		//$xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagGetListRequest");
                $xmlinput  = xmlHeader($sessionid, "GroupDeviceTypeCustomTagGetListRequest");           
		$xmlinput .= "<serviceProviderId>" .  trim(htmlspecialchars($_SESSION["sp"])) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . trim(htmlspecialchars($_SESSION["groupId"])) . "</groupId>";
		$xmlinput .= "<deviceType>" . trim($deviceType) . "</deviceType>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		$errMsg = readErrorXmlGenuine($xml);
		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";                
                
		if($errMsg != "") {
			$returnResponse["Error"] = $errMsg;
		} else {
		    if(isset($xml->command->groupDeviceTypeCustomTagsTable->row)) {
				//foreach ($xml->command->deviceCustomTagsTable->row as $key => $value){
			    foreach ($xml->command->groupDeviceTypeCustomTagsTable->row as $key => $value){
				    
					$userAssignedTags["tagName"][] = strval($value->col[0]);
				}
			} else {
				$userAssignedTags["tagName"] = array();
			}
			
			$returnResponse["Success"] = $userAssignedTags["tagName"];
		}		
		return $returnResponse;
	}
	
	function groupAccessDeviceDeleteCustomTag($deviceType, $tagName){
	  global $sessionid, $client;  
	    $deviceType = trim($deviceType);
	    $tagName = trim($tagName);
	  
		//$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceCustomTagDeleteListRequest");
		$xmlinput = xmlHeader($sessionid, "GroupDeviceTypeCustomTagDeleteListRequest");
		$xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<deviceType>".$deviceType."</deviceType>";
		$xmlinput .= "<tagName>". $tagName ."</tagName>";
		$xmlinput .= xmlFooter();
		
                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                return $xml;
	}
	
	function addDeviceCustomTag($deviceType, $tagName, $tagValue) {
		global $sessionid, $client;		
		//$xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagAddRequest");
                $xmlinput  = xmlHeader($sessionid, "GroupDeviceTypeCustomTagAddRequest");                
		$xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<deviceType>" . $deviceType . "</deviceType>";
		$xmlinput .= "<tagName>" . $tagName . "</tagName>";
        //if (!empty($tagValue)) {
		if ($tagValue !="") {  // code added for Ex_905
	            $xmlinput .= "<tagValue>" . $tagValue. "</tagValue>";
	        }		
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		return $xml;
	}
	
        /*update device custom config tag name */
	
	function updateDeviceCustomTag($deviceType, $tagName, $tagValue) {
	    
	       global $sessionid, $client;
	    
	       $deviceCustomTagModifyResponse ["Error"]   = array();
	       $deviceCustomTagModifyResponse ["Success"] = "";
	     
	        $xmlinput = xmlHeader ( $sessionid, "GroupDeviceTypeCustomTagModifyRequest" );  
	        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
	        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION['groupId']) . "</groupId>";
	        $xmlinput .= "<deviceType>" . $deviceType. "</deviceType>";
	        $xmlinput .= "<tagName>" . $tagName . "</tagName>";
	        //if (!empty($tagValue)) {
	        if ($tagValue !="") { // code added for Ex_905
	            $xmlinput .= "<tagValue>" . $tagValue. "</tagValue>";
	        }
	       
	        $xmlinput .= xmlFooter ();
	        $response = $client->processOCIMessage ( array (
	            "in0" => $xmlinput
	        ) );
	        $xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
	        
	        if (readErrorXmlGenuine ( $xml ) != "") {
	            if (isset ( $xml->command->detail )) {
	                $deviceCustomTagModifyResponse ["Error"] = strval ( $xml->command->detail );
	            } else {
	                $deviceCustomTagModifyResponse ["Error"] = strval ( $xml->command->summaryEnglish );
	            }
	        } else {
	            $deviceCustomTagModifyResponse ["Success"] = $xml->command;
	        }
	       
	    
	    return $deviceCustomTagModifyResponse;
	}
        
        function rebuildResetDeviceType($deviceType, $rebuildPhoneFiles="", $resetPhone="") {
		global $sessionid, $client;
                //Rebuild device 
                $rebuildResetResponseArr['rebuildResponse'] = array();
                $rebuildResetResponseArr['resetResponse']  = array();
                
                if($rebuildPhoneFiles=="true"){
                    $xmlinput  = xmlHeader($sessionid, "GroupCPEConfigRebuildConfigFileRequest");
                    $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
                    $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
                    $xmlinput .= "<deviceType>" . $deviceType . "</deviceType>";
                    $xmlinput .= xmlFooter();

                    $response = $client->processOCIMessage(array("in0" => $xmlinput));
                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                    
                    $deviceTypeRebuildResponse = array();
                    if (readErrorXmlGenuine ( $xml ) != "") {
                        if (isset ( $xml->command->detail )) {
                            $deviceTypeRebuildResponse["Error"] = strval ( $xml->command->detail );
                        } else {
                            $deviceTypeRebuildResponse["Error"] = strval ( $xml->command->summaryEnglish );
                        }
                    } else {
                        $deviceTypeRebuildResponse["Success"] = $xml->command;
                    }
                    $rebuildResetResponseArr['rebuildResponse'] = $deviceTypeRebuildResponse;                    
                    
                }if($resetPhone=="true"){
                    $xmlinput  = xmlHeader($sessionid, "GroupCPEConfigResetDeviceTypeRequest");
                    $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
                    $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
                    $xmlinput .= "<deviceType>" . $deviceType . "</deviceType>";
                    $xmlinput .= xmlFooter();

                    $response = $client->processOCIMessage(array("in0" => $xmlinput));
                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                    
                    $deviceTypeResetResponse = array();
                    if (readErrorXmlGenuine ( $xml ) != "") {
                        if (isset ( $xml->command->detail )) {
                            $deviceTypeResetResponse["Error"] = strval ( $xml->command->detail );
                        } else {
                            $deviceTypeResetResponse["Error"] = strval ( $xml->command->summaryEnglish );
                        }
                    } else {
                        $deviceTypeResetResponse["Success"] = $xml->command;
                    }
                    $rebuildResetResponseArr['resetResponse']  = $deviceTypeResetResponse;
                }
                return $rebuildResetResponseArr;
	}
        /*end */
}
