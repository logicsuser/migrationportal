<?php
	$xmlinput = xmlHeader($sessionid, "GroupScheduleDeleteListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<scheduleKey>
				<scheduleName>" . htmlspecialchars($schedName) . "</scheduleName>
				<scheduleType>" . $schedType . "</scheduleType>
			</scheduleKey>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	readError($xml);
?>
