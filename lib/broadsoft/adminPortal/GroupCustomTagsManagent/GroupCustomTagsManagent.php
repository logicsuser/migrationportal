<?php

class GroupCustomTagsManagent
{
    
        function getGroupCustomTagsList($sp, $groupId, $deviceType) 
        {
            global $sessionid, $client;
           
            $returnResponse["Error"] = "";
            $returnResponse["Success"] = "";
            $customTags = array();
            
            $xmlinput  = xmlHeader($sessionid, "GroupDeviceTypeCustomTagGetListRequest");
            $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($sp) . "</serviceProviderId>";
            $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
            $xmlinput .= "<deviceType>" . $deviceType . "</deviceType>";
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            $errMsg = readErrorXmlGenuine($xml);
            if($errMsg != "") {
                $returnResponse["Error"] = $errMsg;
            }
            else
            {
                $i = 0;
                if (isset($xml->command->groupDeviceTypeCustomTagsTable->row)) {
                    foreach ($xml->command->groupDeviceTypeCustomTagsTable->row as $key => $value) {
                        $customTags[$i]["name"] = strval($value->col[0]);
                        $customTags[$i]["value"] = strval($value->col[1]);
                        $i++;
                    }
                } 
                $returnResponse['Success'] = $customTags;
            }
            return $returnResponse;
    }
}


?>
