<?php
	if ($ociVersion == "17")
	{
		$xmlinput = xmlHeader($sessionid, "GroupDnGetAssignmentListRequest");
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "GroupDnGetAssignmentListRequest18");
	}
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
?>
