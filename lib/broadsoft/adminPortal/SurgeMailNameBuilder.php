<?php

/**
 * Child class for ResourceNameBuilder
 * Note: this class must implement function: getFallBackName()
 *
 * Created by Karl.
 * Date: 8/26/2016
 */
class SurgeMailNameBuilder extends ResourceNameBuilder
{

    // constructor
    // $nameStr         - formula string
    // $dn              - DN
    // $extension       - extension
    // $userClid        - User CLID
    // $groupNumber     - group DN
    // $defaultDomain   - default domain
    // $firstName       - user's First Name
    // $lastName        - user's Last Name
    // $forceResolve    - flag indicating whether name should use fallback to be resolved
    // -----------------------------------------------------------------------------------
    public function __construct($nameStr, $dn, $extension, $userClid, $groupNumber, $sysDefaultDomain, $firstName, $lastName, $forceResolve = false) {

        $registeredParams = array(
            ResourceNameBuilder::DN,
            ResourceNameBuilder::EXT,
            ResourceNameBuilder::USR_CLID,
            ResourceNameBuilder::GRP_DN,
            ResourceNameBuilder::USR_ID,
            ResourceNameBuilder::GRP_ID,
            ResourceNameBuilder::SYS_DFLT_DOMAIN,
            //ResourceNameBuilder::DEFAULT_DOMAIN,
            ResourceNameBuilder::SRG_DOMAIN,
            //ResourceNameBuilder::GRP_PROXY_DOMAIN,
            ResourceNameBuilder::FIRST_NAME,
            ResourceNameBuilder::LAST_NAME,
            //ResourceNameBuilder::DEV_NAME,
            //ResourceNameBuilder::MAC_ADDR,
            //ResourceNameBuilder::INT_1,
            //ResourceNameBuilder::INT_2,
            ResourceNameBuilder::TXT
        );

        if ($dn != "")                  { $this->defaults[ResourceNameBuilder::DN]              = $dn; }
        if ($extension != "")           { $this->defaults[ResourceNameBuilder::EXT]             = $extension; }
        if ($userClid != "")            { $this->defaults[ResourceNameBuilder::USR_CLID]        = $userClid; }
        if ($groupNumber != "")         { $this->defaults[ResourceNameBuilder::GRP_DN]          = $groupNumber; }
        if ($sysDefaultDomain != "")    { $this->defaults[ResourceNameBuilder::SYS_DFLT_DOMAIN] = $sysDefaultDomain; }
        if ($firstName != "")           { $this->defaults[ResourceNameBuilder::FIRST_NAME]      = $firstName; }
        if ($lastName != "")            { $this->defaults[ResourceNameBuilder::LAST_NAME]       = $lastName; }

        parent::__construct($nameStr, $registeredParams, $forceResolve);
    }

    public function getFallbackName() {
        $domain = $this->getDomainForFallback();

        $name  = $this->getDefaultAttribute(ResourceNameBuilder::FIRST_NAME);
        $name .= $this->getDefaultAttribute(ResourceNameBuilder::LAST_NAME);
        $name .= "x" . $this->getDefaultAttribute(ResourceNameBuilder::EXT) . "@" . $domain;

        return $name;
    }

}
