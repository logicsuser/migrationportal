<?php

/**
 * Created by PhpStorm.
 * User: Karl
 * Date: 10/11/2016
 * Time: 4:10 PM
 */
class CSVSharedCallAppearance extends CSVManager
{

    public function __construct($postName)
    {
        parent::__construct($postName);

        $this->addAttribute("userId",                                 "Primary User ID",         self::Validate_UserID_Existence());
        $this->addAttribute("deviceName",                             "SCA Device Name",         self::Validate_DeviceExistence());
        $this->addAttribute("linePort",                               "SCA Lineport",            self::Validate_Domain_Existence());
        $this->addAttribute("isActive",                               "Active",                  self::Validate_TrueFalse_Format());
        $this->addAttribute("allowOrigination",                       "Origination",             self::Validate_TrueFalse_Format());
        $this->addAttribute("allowTermination",                       "Termination",             self::Validate_TrueFalse_Format());
        $this->addAttribute("alertAllAppearancesForClickToDialCalls", "Alert All Click To Dial", self::Validate_TrueFalse_Format());
        $this->addAttribute("alertAllAppearancesForGroupPagingCalls", "Alert All Group Paging",  self::Validate_TrueFalse_Format());
        $this->addAttribute("allowSCACallRetrieve",                   "Allow Call Retrieve",     self::Validate_TrueFalse_Format());
        $this->addAttribute("multipleCallArrangementIsActive",        "MCA",                     self::Validate_TrueFalse_Format());
        $this->addAttribute("enableCallParkNotification",             "Call Park Notification",  self::Validate_TrueFalse_Format());
        $this->addAttribute("allowBridgingBetweenLocations",          "Bridging",                self::Validate_TrueFalse_Format());
        $this->addAttribute("bridgeWarningTone",                      "Bridge Warning Tone",     self::Validate_Bridge_Warning_Format());
    }
}
