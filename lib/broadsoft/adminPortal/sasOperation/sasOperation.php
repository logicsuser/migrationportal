<?php


class sasOperation{
	
	function deleteFromCSVFile($deleteArray, $fileName)
	{
		$tempFileName = "../../../SASTestingUser/SASTestUsers_temp.csv";
		$response = "";
		if(file_exists($fileName))
		{ 
			$table = fopen($fileName, 'r');
			$temp_table = fopen($tempFileName, 'w');
			
			while (($data = fgetcsv($table, 1024)) !== FALSE){
// 				if($data[3] == $userId)
				if(in_array($data[3], $deleteArray))
				{ 
					continue;
				}
				fputcsv($temp_table,$data);
			}
			fclose($table);
			fclose($temp_table);
			rename($tempFileName, $fileName);
		}
		else
		{
			$response['Error'] = "CSV File Does Not Exist";
		}
		return $response;
	}
}
