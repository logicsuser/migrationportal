<?php
	unset($_SESSION["hg"]);

	if($ociVersion == "19")
	{
		$xmlinput = xmlHeader($sessionid, "GroupHuntGroupGetInstanceRequest19");
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "GroupHuntGroupGetInstanceRequest20");
	}
	$xmlinput .= "<serviceUserId>" . $hgId . "</serviceUserId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
//print_r($xml);

	$_SESSION["hg"]["hgName"] = strval($xml->command->serviceInstanceProfile->name);
	$_SESSION["hg"]["phoneNumber"] = strval($xml->command->serviceInstanceProfile->phoneNumber);
	$_SESSION["hg"]["extension"] = strval($xml->command->serviceInstanceProfile->extension);
	$_SESSION["hg"]["policy"] = strval($xml->command->policy);
	$_SESSION["hg"]["fwdTimeout"] = strval($xml->command->forwardAfterTimeout);
	$_SESSION["hg"]["fwdSecs"] = strval($xml->command->forwardTimeoutSeconds);
	$_SESSION["hg"]["fwdNum"] = strval($xml->command->forwardToPhoneNumber);
	$_SESSION["hg"]["allowCallWaitingForAgents"] = strval($xml->command->allowCallWaitingForAgents);
	if ($ociVersion !== "17")
	{
		$_SESSION["hg"]["allowMembersToControlGroupBusy"] = strval($xml->command->allowMembersToControlGroupBusy);
		$_SESSION["hg"]["enableGroupBusy"] = strval($xml->command->enableGroupBusy);
	}

	$a = 0;
	foreach ($xml->command->agentUserTable->row as $key => $value)
	{
		$_SESSION["hg"]["users"][$a]["id"] = strval($value->col[0]);
		$_SESSION["hg"]["users"][$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
		$a++;
	}

	//remove sorting for agents; need to be in exact order
//	if (isset($_SESSION["hg"]["users"]))
//	{
//		$_SESSION["hg"]["users"] = subval_sort($_SESSION["hg"]["users"], "name");
//	}

	$xmlinput = xmlHeader($sessionid, "GroupHuntGroupGetAvailableUserListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->userTable->row as $key => $value)
	{
		if ((isset($_SESSION["hg"]["users"]) and !searchArray($_SESSION["hg"]["users"], "id", strval($value->col[0]))) or !isset($_SESSION["hg"]["users"]))
		{
			$_SESSION["hg"]["availableUsers"][$a]["id"] = strval($value->col[0]);
			$_SESSION["hg"]["availableUsers"][$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
			$a++;
		}
	}

	if (isset($_SESSION["hg"]["availableUsers"]))
	{
		$_SESSION["hg"]["availableUsers"] = subval_sort($_SESSION["hg"]["availableUsers"], "name");
	}
	
	$phoneNumberStatus = "No";
	$userPhoneNumber = "";
	if(isset($_SESSION["hg"]["phoneNumber"]) && !empty($_SESSION["hg"]["phoneNumber"])){
		require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
		
		$huntDn = new Dns ();
		$numberActivateResponse = $huntDn->getUserDNActivateListRequest ( $hgId);

		if (empty ( $numberActivateResponse ['Error'] )) {
			if (! empty ( $numberActivateResponse ['Success'] [0] ['phoneNumber'] )) {
				$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
			}
			if (! empty ( $numberActivateResponse ['Success'] [0] ['status'] )) {
				$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
			}
		}
	}
	$_SESSION["hg"]["huntActivateNumber"] = $phoneNumberStatus;
?>
