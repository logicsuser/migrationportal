<?php

/**
 * Child class for ResourceNameBuilder
 * Note: this class must implement function: getFallBackName()
 *
 * Created by kdalka.
 * Date: 12/21/2016
 */
class VoicePasswordNameBuilder extends ResourceNameBuilder
{
    /**
     * VoicePasswordNameBuilder constructor.
     * @param $nameStr
     * @param $dn
     * @param $extension
     * @param $userClid
     * @param $groupNumber
     * @param bool $forceResolve
     */
    public function __construct($nameStr, $dn, $extension, $userClid, $groupNumber, $forceResolve = false) {

        $registeredParams = array(
            ResourceNameBuilder::DN,
            ResourceNameBuilder::EXT,
            ResourceNameBuilder::USR_CLID,
            ResourceNameBuilder::GRP_DN,
            //ResourceNameBuilder::USR_ID,
            ResourceNameBuilder::GRP_ID,
            //ResourceNameBuilder::SYS_DFLT_DOMAIN,
            //ResourceNameBuilder::DEFAULT_DOMAIN,
            //ResourceNameBuilder::SRG_DOMAIN,
            //ResourceNameBuilder::GRP_PROXY_DOMAIN,
            //ResourceNameBuilder::FIRST_NAME,
            //ResourceNameBuilder::LAST_NAME,
            //ResourceNameBuilder::DEV_NAME,
            ResourceNameBuilder::MAC_ADDR,
            //ResourceNameBuilder::INT_1,
            //ResourceNameBuilder::INT_2,
            ResourceNameBuilder::TXT
        );

        if ($dn != "")          { $this->defaults[ResourceNameBuilder::DN]       = $dn; }
        if ($extension != "")   { $this->defaults[ResourceNameBuilder::EXT]      = $extension; }
        if ($userClid != "")    { $this->defaults[ResourceNameBuilder::USR_CLID] = $userClid; }
        if ($groupNumber != "") { $this->defaults[ResourceNameBuilder::GRP_DN]   = $groupNumber; }

        parent::__construct($nameStr, $registeredParams, $forceResolve);
    }

    /**
     * @return string
     */
    public function getFallbackName()
    {
        $extensionLength = strlen($this->defaults[ResourceNameBuilder::EXT]);

        if (($name = $this->defaults[ResourceNameBuilder::DN]) != "") {
            return substr($name, 0, $extensionLength);
        }

        if (($name = $this->defaults[ResourceNameBuilder::USR_CLID]) != "") {
            return substr($name, 0, $extensionLength);
        }

        if (($name = $this->defaults[ResourceNameBuilder::GRP_DN]) != "") {
            return substr($name, 0, $extensionLength);
        }

        return $this->defaults[ResourceNameBuilder::EXT];
    }
}
