<?php
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
class GroupCallPickupUsers {
	private $callPickupGroupName;

	public function __construct($callPickupGroupName) {
		$this->callPickupGroupName = $callPickupGroupName;
	}

	public function getUsersInCallPickupGroup() {
		global $sessionid, $client;
		$xmlinput = xmlHeader($sessionid, "GroupCallPickupGetInstanceRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<name>" .$this->callPickupGroupName. "</name>";
		$xmlinput .= xmlFooter();
		$groupCallPickupResponse = $client->processOCIMessage(array("in0" => $xmlinput));
		$groupCallPickupResponseXML = new SimpleXMLElement($groupCallPickupResponse->processOCIMessageReturn, LIBXML_NOWARNING);

		$groupCallPickupUsers = array();
		if(!empty($groupCallPickupResponseXML->command->userTable->row)){
			foreach ($groupCallPickupResponseXML->command->userTable->row as $keyy => $val)
			{
				$groupCallPickupUsers[]= strval($val->col[0]);
			}
		}
		return $groupCallPickupUsers;
	}

	public function modifyUserCallPickupGroup($users )	{
		global $sessionid, $client;
		$xmlinput = xmlHeader($sessionid, "GroupCallPickupModifyInstanceRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<name>" .$this->callPickupGroupName. "</name>";

		if(count($users) > 0) {
			$xmlinput .= "<userIdList>";
				foreach($users as $key => $value){
					$xmlinput .= "<userId>" .$value. "</userId>";
				}
			$xmlinput .= "</userIdList>";
		} else {
			$xmlinput .= "<userIdList xsi:nil=\"true\" />";
		}

		$xmlinput .= xmlFooter();

		$groupCallPickupModifiedResponse = $client->processOCIMessage(array("in0" => $xmlinput));
		$groupCallPickupModifiedResponseXML = new SimpleXMLElement($groupCallPickupModifiedResponse->processOCIMessageReturn, LIBXML_NOWARNING);

		$errMsg =  readErrorXml($groupCallPickupModifiedResponseXML, $this->callPickupGroupName);
		return $errMsg;

		//echo "<pre>"; print_r($groupCallPickupModifiedResponseXML); die;
		//$data = "";
		/*if (count($groupCallPickupModifiedResponseXML->command->attributes()) > 0)
		{
			foreach ($groupCallPickupModifiedResponseXML->command->attributes() as $a => $b)
			{
				if ($b == "Error")
				{
					if(isset($_POST["groupCallPickupList"]) && !empty($_POST["groupCallPickupList"])){
						$data = $_POST["groupCallPickupList"] . " was attempted to be assigned but failed.";
					}
					$data .= "%0D%0A%0D%0A" . strval($groupCallPickupModifiedResponseXML->command->summaryEnglish);
					if ($groupCallPickupModifiedResponseXML->command->detail)
					{
						$data .= "%0D%0A" . strval($groupCallPickupModifiedResponseXML->command->detail);
					}
				}
			}
		}*/
	}
	public function findCallPickupGroupByUsername(){
		global $sessionid, $client;
		$xmlinput = xmlHeader($sessionid, "UserCallPickupGetRequest");
		$xmlinput .= "<userId>" . $this->callPickupGroupName . "</userId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errMsg =  readErrorXml($xml, $this->callPickupGroupName);
		if($errMsg == "Success"){
			return strval($xml->command->name[0]);
		}else{
			return $errMsg;
		}
		//print_r($xml);
		return $xml;
	}

}