<?php

class ServicePacksController{
    function serviceProviderServicePackAddRequest($serviceArray){
        global  $sessionid, $client;
        $servicePackAddResp["Error"] = array();
        $servicePackAddResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackAddRequest");
        $xmlinput .= "<serviceProviderId>".$serviceArray["serviceProviderId"]."</serviceProviderId>";
        $xmlinput .= "<servicePackName>".$serviceArray["servicePackName"]."</servicePackName>";
        if(!empty($serviceArray["servicePackDescription"])){
            $xmlinput .= "<servicePackDescription>".$serviceArray["servicePackDescription"]."</servicePackDescription>";
        }
        if(!empty($serviceArray["isAvailableForUse"])){
            $xmlinput .= "<isAvailableForUse>".$serviceArray["isAvailableForUse"]."</isAvailableForUse>";
        }
        
        if(!empty($serviceArray["servicePackQuantity"])){
            $xmlinput .= "<servicePackQuantity>";
            if(is_numeric($serviceArray["servicePackQuantity"])){
                $xmlinput .= "<quantity>".$serviceArray["servicePackQuantity"]."</quantity>";
            }else{
                $xmlinput .= "<unlimited>true</unlimited>";
            }
            $xmlinput .= "</servicePackQuantity>";
        }
        
        if(count($serviceArray["serviceName"]) > 0){
            foreach($serviceArray["serviceName"] as $key => $value){
                $xmlinput .= "<serviceName>".$value."</serviceName>";
            }
        }
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        if (readErrorXmlGenuine($xml) != "") {
            $servicePackAddResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $servicePackAddResp["Success"] = "Success";
        }
        
        return $servicePackAddResp;
    }
    
    function serviceProviderServicePackModifyRequest($serviceArray){
        global  $sessionid, $client;
        $servicePackModResp["Error"] = array();
        $servicePackModResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackModifyRequest");
        $xmlinput .= "<serviceProviderId>".$serviceArray["serviceProviderId"]."</serviceProviderId>";
        $xmlinput .= "<servicePackName>".$serviceArray["servicePackName"]."</servicePackName>";
        if(isset($serviceArray["newServicePackName"])){
            $xmlinput .= "<newServicePackName>".$serviceArray["newServicePackName"]."</newServicePackName>";
        }
        
        if(isset($serviceArray["servicePackDescription"])){
            if(!empty($serviceArray["servicePackDescription"])){
                $xmlinput .= "<servicePackDescription>".$serviceArray["servicePackDescription"]."</servicePackDescription>";
            }else{
                $xmlinput .= "<servicePackDescription xsi:nil='true'></servicePackDescription>";
            }
            
        }
        if(!empty($serviceArray["isAvailableForUse"])){
            $xmlinput .= "<isAvailableForUse>".$serviceArray["isAvailableForUse"]."</isAvailableForUse>";
        }
        
        if(!empty($serviceArray["servicePackQuantity"])){
            $xmlinput .= "<servicePackQuantity>";
            if(is_numeric($serviceArray["servicePackQuantity"])){
                $xmlinput .= "<quantity>".$serviceArray["servicePackQuantity"]."</quantity>";
            }else{
                $xmlinput .= "<unlimited>true</unlimited>";
            }
            $xmlinput .= "</servicePackQuantity>";
        }
        
        $xmlinput .= xmlFooter();
        //print_r($xmlinput);
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $servicePackModResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $servicePackModResp["Success"] = "Success";
        }
        
        return $servicePackModResp;
    }
    
    function serviceProviderServicePackDeleteRequest($spId, $spName){
        global  $sessionid, $client;
        $servicePackDelResp["Error"] = array();
        $servicePackDelResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackDeleteRequest");
        $xmlinput .= "<serviceProviderId>".$spId."</serviceProviderId>";
        $xmlinput .= "<servicePackName>".$spName."</servicePackName>";
        
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $servicePackDelResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $servicePackDelResp["Success"] = "Success";
        }
        
        return $servicePackDelResp;
    }
    
    //Return detail of a perticular service pack
    function serviceProviderServicePackGetDetailListRequest($spId, $spName){
        global  $sessionid, $client;
        $servicePackGetDetailResp["Error"] = array();
        $servicePackGetDetailResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
        $xmlinput .= "<serviceProviderId>".$spId."</serviceProviderId>";
        $xmlinput .= "<servicePackName>".$spName."</servicePackName>";
        
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $servicePackGetDetailResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $servicePackGetDetailResp["Success"]["servicePackName"] = strval($xml->command->servicePackName);
            $servicePackGetDetailResp["Success"]["servicePackDescription"] = strval($xml->command->servicePackDescription);
            $servicePackGetDetailResp["Success"]["isAvailableForUse"] = strval($xml->command->isAvailableForUse);
            if(isset($xml->command->servicePackQuantity->unlimited)){
                $servicePackGetDetailResp["Success"]["servicePackQuantity"]["unlimited"] = "unlimited";
            }else{
                $servicePackGetDetailResp["Success"]["servicePackQuantity"]["quantity"] = strval($xml->command->servicePackQuantity->quantity);
            }
            
            if(isset($xml->command->assignedQuantity->unlimited)){
                $servicePackGetDetailResp["Success"]["assignedQuantity"] = "unlimited";
            }else{
                $servicePackGetDetailResp["Success"]["assignedQuantity"] = strval($xml->command->assignedQuantity->quantity);
            }
            
            if(isset($xml->command->allowedQuantity->unlimited)){
                $servicePackGetDetailResp["Success"]["allowedQuantity"] = "unlimited";
            }else{
                $servicePackGetDetailResp["Success"]["allowedQuantity"] = strval($xml->command->allowedQuantity->quantity);
            }
            
            if(isset($xml->command->userServiceTable->row)){
                $i=0;
                foreach ($xml->command->userServiceTable->row as $key => $val){
                    $servicePackGetDetailResp["Success"]["Services"][$i]["serviceName"] = strval($val->col[0]);
                    $servicePackGetDetailResp["Success"]["Services"][$i]["authorized"] = strval($val->col[1]);
                    $servicePackGetDetailResp["Success"]["Services"][$i]["allocated"] = strval($val->col[2]);
                    $servicePackGetDetailResp["Success"]["Services"][$i]["available"] = strval($val->col[3]);
                    $i++;
                }
            }
            
            //$servicePackGetDetailResp["Success"] = "Success";
        }
        
        return $servicePackGetDetailResp;
    }
    
    function serviceProviderServicePackGetListRequest($spId){
        global  $sessionid, $client;
        $servicePackGetAllResp["Error"] = array();
        $servicePackGetAllResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetListRequest");
        $xmlinput .= "<serviceProviderId>".$spId."</serviceProviderId>";
        
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $servicePackGetAllResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            if(count($xml->command->servicePackName) > 0){
                foreach ($xml->command->servicePackName as $key => $val){
                    $servicePackGetAllResp["Success"][] = strval($val);
                }
            }
            //$servicePackGetAllResp["Success"] = array($xml->command->servicePackName);
        }
        return $servicePackGetAllResp;
    }
    
    function serviceProviderServicePackGetUtilizationListRequest($spId){
        global  $sessionid, $client;
        $servicePackGetAllResp["Error"] = array();
        $servicePackGetAllResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetUtilizationListRequest");
        $xmlinput .= "<serviceProviderId>".$spId."</serviceProviderId>";
        
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $servicePackGetAllResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            if(count($xml->command->servicePackName) > 0){
                
                $k = 0;
                //print_r($xml->command->serviceUtilizationTable[0]);
                foreach ($xml->command->servicePackName as $key => $val){
                    $arrayToProceed = $xml->command->serviceUtilizationTable[$k];
                    $i = 0;
                    if(count($arrayToProceed->row) > 0){
                        foreach ($arrayToProceed->row as $nKey => $nVal){
                            //print_r(strval($val));
                            $servicePackGetAllResp["Success"][strval($val)][$i]["group"] = strval($nVal->col[0]);
                            $servicePackGetAllResp["Success"][strval($val)][$i]["totalPacks"] = strval($nVal->col[1]);
                            $servicePackGetAllResp["Success"][strval($val)][$i]["assigned"] = strval($nVal->col[2]);
                            $i++;
                        }
                    }else{
                        $servicePackGetAllResp["Success"][strval($val)] = "";
                        $servicePackGetAllResp["Success"][strval($val)] = "";
                        $servicePackGetAllResp["Success"][strval($val)] = "";
                    }
                    
                    $k++;
                }
                
            }
        }
        return $servicePackGetAllResp;
    }
    
    function serviceProviderServiceGetAuthorizationListRequest($spId){
        global  $sessionid, $client;
        $servicePackGetAllServices["Error"] = array();
        $servicePackGetAllServices["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServiceGetAuthorizationListRequest");
        $xmlinput .= "<serviceProviderId>".$spId."</serviceProviderId>";
        
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);exit;
        if (readErrorXmlGenuine($xml) != "") {
            $servicePackGetAllServices["Error"] = strval($xml->command->summaryEnglish);
        }else{
            if(count($xml->command->userServicesAuthorizationTable->row) > 0){
                foreach ($xml->command->userServicesAuthorizationTable->row as $key => $value){
                    if($value->col[1] != "false"){
                        $servicePackGetAllServices["Success"]["userServices"][] = strval($value->col[0]);
                    }
                }
                
                
            }
        }
        return $servicePackGetAllServices;
    }
}

?>