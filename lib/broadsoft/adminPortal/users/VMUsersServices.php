<?php 
class VMUsersServices{
    
    function modifyGroupWideUsersVoiceMessaging($userId, $postArray){
        global $sessionid, $client;
        $vmArray["Error"]="";
        $vmArray["Success"]="";
        $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
        $xmlinput .= "<userId>".$userId."</userId>";
        $xmlinput .= "<isActive>".$postArray["isActive"]."</isActive>";
        if(isset($postArray['processing'])){
            $xmlinput .= "<processing>".$postArray["processing"]."</processing>";
        }
        /*if(isset($postArray['usePhoneMessageWaitingIndicator'])){
            $xmlinput .= "<usePhoneMessageWaitingIndicator>".$postArray["usePhoneMessageWaitingIndicator"]."</usePhoneMessageWaitingIndicator>";
        }*/
        if(isset($postArray['sendCarbonCopyVoiceMessage'])){
            $xmlinput .= "<sendCarbonCopyVoiceMessage>".$postArray["sendCarbonCopyVoiceMessage"]."</sendCarbonCopyVoiceMessage>";
        }
        if(isset($postArray['voiceMessageCarbonCopyEmailAddress']) ){
            if($postArray['voiceMessageCarbonCopyEmailAddress'] != "") {
                $xmlinput .= "<voiceMessageCarbonCopyEmailAddress>".$postArray["voiceMessageCarbonCopyEmailAddress"]."</voiceMessageCarbonCopyEmailAddress>";
            } else  {
                $xmlinput .= "<voiceMessageCarbonCopyEmailAddress xsi:nil='true'></voiceMessageCarbonCopyEmailAddress>";
            }
        }
        if(isset($postArray['transferOnZeroToPhoneNumber'])){
            $xmlinput .= "<transferOnZeroToPhoneNumber>".$postArray["transferOnZeroToPhoneNumber"]."</transferOnZeroToPhoneNumber>";
        }
        if(isset($postArray['transferPhoneNumber']) ){
            if($postArray['transferPhoneNumber'] != "") {
            $xmlinput .= "<transferPhoneNumber>".$postArray["transferPhoneNumber"]."</transferPhoneNumber>";
            } else  {
                $xmlinput .= "<transferPhoneNumber  xsi:nil='true'></transferPhoneNumber>";
            }
        }
        if(isset($postArray['alwaysRedirectToVoiceMail'])){
            $xmlinput .= "<alwaysRedirectToVoiceMail>".$postArray["alwaysRedirectToVoiceMail"]."</alwaysRedirectToVoiceMail>";
        }
        if(isset($postArray['busyRedirectToVoiceMail'])){
            $xmlinput .= "<busyRedirectToVoiceMail>".$postArray["busyRedirectToVoiceMail"]."</busyRedirectToVoiceMail>";
        }
        if(isset($postArray['noAnswerRedirectToVoiceMail'])){
            $xmlinput .= "<noAnswerRedirectToVoiceMail>".$postArray["noAnswerRedirectToVoiceMail"]."</noAnswerRedirectToVoiceMail>";
        }
        $xmlinput .= xmlFooter();
        $responseCall = $client->processOCIMessage(array("in0" => $xmlinput));        
        $xmlCall = new SimpleXMLElement($responseCall->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xmlCall) != "") {
            if(!empty($xmlCall->command->summary)){
                $vmArray["Error"] = strval($xmlCall->command->summary);
            }
        }else{
            $vmArray["Success"] = $xmlCall->command;
            
        }
        return $vmArray;
    }
    
    function modifyUsersVoiceMessagingAdvancedServices($userId, $postArray){
        global  $sessionid, $client;
        
        $vmAdvancedResp["Error"] = "";
        $vmAdvancedResp["Success"] = "";
        $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyAdvancedVoiceManagementRequest");
        $xmlinput .= "<userId>" . $userId. "</userId>";
        
        if(!empty($postArray["mailBoxLimit"]) &&  $postArray["mailBoxLimit"] == "Use Group Default"){
            $xmlinput .= "<useGroupDefaultMailServerFullMailboxLimit>true</useGroupDefaultMailServerFullMailboxLimit>";
        }else{
            $xmlinput .= "<groupMailServerFullMailboxLimit>" .$postArray["mailBoxLimit"]. "</groupMailServerFullMailboxLimit>";
        }
       
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        if (readErrorXmlGenuine($xml) != "") {
            $vmAdvancedResp["Error"] = $xml->command->summaryEnglish;
        }else{
            $vmAdvancedResp["Success"] = "Success";
            
        }
        return $vmAdvancedResp;
    }
    
    function modifyUsersVoiceMessagingAdvancedGetRequest($userId){
        global  $sessionid, $client;
        
        $vmAdvancedGetResp["Error"] = "";
        $vmAdvancedGetResp["Success"] = "";
        $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetAdvancedVoiceManagementRequest14sp3");
        $xmlinput .= "<userId>" . $userId. "</userId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        if (readErrorXmlGenuine($xml) != "") {
            $vmAdvancedGetResp["Error"] = $xml->command->summaryEnglish;
        }else{
            if(isset($xml->command->groupMailServerFullMailboxLimit) && !empty($xml->command->groupMailServerFullMailboxLimit)){
                $vmAdvancedGetResp["Success"]= $xml->command->groupMailServerFullMailboxLimit;
            }else{
                $vmAdvancedGetResp["Success"] = $xml->command->useGroupDefaultMailServerFullMailboxLimit;
            } 
        }
        return $vmAdvancedGetResp;
    }
    
    
    
    
}
?>