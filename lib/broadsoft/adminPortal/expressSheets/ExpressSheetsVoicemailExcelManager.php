<?php
/**
 * Created by Dipin Krishna.
 * Date: 01/01/18
 * Time: 7:50 AM
 */
class ExpressSheetsVoicemailExcelManager extends ExpressSheetsExcelManager
{

	public function __construct($postName, $parseFile)
	{
		parent::__construct($postName, $parseFile);

		$this->addAttribute("action",                       "Action",                       self::Validate_Action());
		$this->addAttribute("user",                         "User",                         self::Validate_UserID_Existence(), 17);
		$this->addAttribute("vm_service",                   "VM Service",                   self::Validate_OnOff_Format());
		$this->addAttribute("enable_vm",                    "Enable VM",                    self::Validate_OnOff_Format());
		$this->addAttribute("use_unified_messaging",        "Use Unified Messaging",        self::Validate_YesNo_Format());
		$this->addAttribute("forward_to_email",             "Forward to email",             self::Validate_Email_Format());
		$this->addAttribute("new_message_notification",     "New Message Notification",     self::Validate_YesNo_Format());
		$this->addAttribute("new_message_email_address",    "New Message Email Address",    self::Validate_Email_Format());
		$this->addAttribute("use_phone_MWI",                "Use Phone MWI",                self::Validate_YesNo_Format());
		$this->addAttribute("email_message_carbon_copy",    "Email Message Carbon Copy",    self::Validate_YesNo_Format());
		$this->addAttribute("carbon_copy_address",          "Carbon Copy Address",          self::Validate_Email_Format());
		$this->addAttribute("send_all_calls_to_vm",         "Send All Calls to VM",         self::Validate_YesNo_Format());
		$this->addAttribute("send_busy_calls_to_vm",        "Send Busy Calls to VM",        self::Validate_YesNo_Format());
		$this->addAttribute("send_unanswered_calls_to_vm",  "Send Unanswered Calls to VM",  self::Validate_YesNo_Format());
		$this->addAttribute("transfer_on_0",                "Transfer on '0'",              self::Validate_YesNo_Format());
		$this->addAttribute("transfer_on_0_DN",             "Transfer on '0' DN",           self::Validate_None());
		$this->addAttribute("mailbox_limit",                "Mailbox Limit",                self::Validate_None());

		$this->getAllPhoneNumberWithExtension();
	}

	protected function parseRow($row, $index)
	{

		if(count($row) >= 5 && (strtolower($row[0]) == "add" || strtolower($row[0]) == "delete" || strtolower($row[0]) == "modify")) {

			if (is_array($row) && array_filter($row)) {

				$userId = $row[17];
				$userName = $row[1];
				$user_id_name = $this->postName . "[" . $index . "][userId]";
				$html = "<input value='$userId' name='$user_id_name' type='hidden'/>";

				if(strtoupper($row[4]) == 'NO') { //use_unified_messaging
					$this->addRequiredAttribute('forward_to_email');
				}
				if(strtoupper($row[6]) == 'YES') { //new_message_notification
					$this->addRequiredAttribute('new_message_email_address');
				}
				if(strtoupper($row[9]) == 'YES') { //email_message_carbon_copy
					$this->addRequiredAttribute('carbon_copy_address');
				}
				if(strtoupper($row[14]) == 'YES') { //transfer_on_0
					$this->addRequiredAttribute('transfer_on_0_DN');
				}

				$error_html = "";
				if(strtoupper($row[2]) == 'OFF') {
					$assignedServices = new UserServiceGetAssignmentList($userId);
					if ($assignedServices->hasService("Voice Messaging User")) {
						$servicePacks = $assignedServices->getservicePacks();
						//$html .= "<tr><td colspan='18'>" . print_r($servicePacks, true) . "</td></tr>";
						foreach ($servicePacks as $servicePack => $userServices) {
							if(in_array("Voice Messaging User", $userServices) && count($userServices) > 1) {
								//$error_html = "<tr><td colspan='18' class='error'>$userName: VM User Service cannot be un-assigned because it is assigned through '$servicePack' Service Pack, containing multiple services</td></tr>";
								//$this->error = true;
								//break;
							}
						}
					}
				}

				return $html . parent::parseRow($row, $index) . $error_html;

			}
		}

		return null;

	}
}