<?php
/**
 * Created by Dipin Krishna.
 * Date: 10/18/17
 * Time: 10:46 AM
 */
class ExpressSheetsBLFExcelManager extends ExpressSheetsExcelManager
{

	public function __construct($postName, $parseFile)
	{
		parent::__construct($postName, $parseFile);

		$this->addAttribute("action",           "Action",          self::Validate_Action());
		$this->addAttribute("primaryUser",      "User Info",            self::Validate_UserID_Existence(), 4);
		$this->addAttribute("line",             "Line",            self::Validate_NonZero_Number());
		$this->addAttribute("blfUser",          "BLF User",        self::Validate_UserID_Existence(), 5);

		$this->getAllPhoneNumberWithExtension();
	}

	protected function parseRow($row, $index)
	{

		if(count($row) >= 5 && (strtolower($row[0]) == "add" || strtolower($row[0]) == "delete" || strtolower($row[0]) == "modify")) {

			if (is_array($row) && array_filter($row)) {

				$user_id_name = $this->postName . "[" . $index . "][userId]";
				$blf_user_id_name = $this->postName . "[" . $index . "][blfUserId]";
				$html = "<input value='{$row[4]}' name='$user_id_name' type='hidden'/>";
				$html .= "<input value='{$row[5]}' name='$blf_user_id_name' type='hidden'/>";
				return $html . parent::parseRow($row, $index);

			}
		}

		return null;

	}
}