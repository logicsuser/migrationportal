<?php
/**
 * Created by Dipin Krishna.
 * Date: 10/18/17
 * Time: 10:42 AM
 */

class ExpressSheetsExcelManager
{
	protected $keys = array();
	protected $headTitles = array();
	private $validators = array();
	private $validate_column = array();
	private $bw_DeviceTypesList = array();  // not always populated
	private $userList = array();            // not always populated
	private $devicesList = array();         // not always populated
	private $groupDomainList = array();     // not always populated
	private $existingPhoneNumberList = array(); // not always populated
	protected $phoneNumberWithExtensionList = array(); // not always populated
	protected $requiredAttributes = array();

	private $numColumns = 0;
	private $responseTable;
	private $msg = "trace:";
	protected $error = false;
	protected $postName = "";

	protected $header_row;
	protected $fileUploadParsed = false;
	protected $rows;

	// mimicking protected constants
	protected static function Validate_BW_Device_Type()
	{
		return "Validate_BW_Device_Type";
	}

	protected static function Validate_BW_Tag_Name()
	{
		return "Validate_BW_Tag_Name";
	}

	protected static function Validate_UserID_Format()
	{
		return "Validate_UserID_Format";
	}

	protected static function Validate_TrueFalse_Format()
	{
		return "Validate_TrueFalse_Format";
	}

	protected static function Validate_YesNo_Format()
	{
		return "Validate_YesNo_Format";
	}

	protected static function Validate_OnOff_Format()
	{
		return "Validate_OnOff_Format";
	}

	protected static function Validate_Email_Format()
	{
		return "Validate_Email_Format";
	}

	protected static function Validate_NonZero_Number()
	{
		return "Validate_NonZero_Number";
	}

	protected static function Validate_UserID_Existence()
	{
		return "Validate_UserID_Existence";
	}

	protected static function Validate_DeviceExistence()
	{
		return "Validate_Device_Existence";
	}

	protected static function Validate_Bridge_Warning_Format()
	{
		return "Validate_Bridge_Warning_Format";
	}

	protected static function Validate_Domain_Existence()
	{
		return "Validate_Domain_Existence";
	}

	protected static function Validate_Extension()
	{
		return "Validate_Extension";
	}

	protected static function Validate_Existing_Phone_Number()
	{
		return "Validate_Existing_Phone_Number";
	}

	protected static function Validate_PhoneNumber_Extension_Existence()
	{
		return "Validate_PhoneNumber_Extension_Existence";
	}

	protected static function Validate_Action()
	{
		return "Validate_Action";
	}

	protected static function Validate_Phone_Profile()
	{
		return "Validate_Phone_Profile";
	}

	protected static function Validate_Tag_Bundles()
	{
		return "Validate_Tag_Bundles";
	}

	protected static function Validate_NonEmpty()
	{
		return "Validate_NonEmpty";
	}

	protected static function Validate_None()
	{
		return "Validate_None";
	}

	protected static function Validate_EmptyOrNonZeroNumber()
	{
		return "Validate_EmptyOrNonZeroNumber";
	}

	/**
	 * ExpressSheetsExcelManager constructor.
	 */
	public function __construct($postName, $parseFile = false)
	{
		$this->postName = $postName;
		if($parseFile) {
			$this->rows = $this->parseFileUploaded();
		}
	}

	/**
	 * @return bool
	 */
	public function hasError()
	{
		return $this->error;
	}

	/**
	 * @return string
	 */
	public function trace()
	{
		return $this->msg;
	}


	/**
	 * Create HTML table header as in the example below:
	 * <th style="width:12%">Caller ID</th>
	 */
	public function createTableHeader()
	{
		$header = "";
		$margin = $this->numColumns == 0 ? 100 : (int)(100 / $this->numColumns);

		for ($i = 0; $i < $this->numColumns; $i++) {
			$header .= "<th style=\"width:" . $margin . "%\">" . $this->headTitles[$i] . "</th>";
		}

		return $header;
	}


	/**
	 * @return string
	 */
	public function parse()
	{

		if(!$this->fileUploadParsed) {
			$this->rows = $this->parseFileUploaded();
		}

		$this->error = count($this->rows) == 0;
		$this->responseTable = "";

		$this->msg .= " parse:" . count($this->rows) . " rows";
		$i = 0;
		foreach ($this->rows as $row) {
			if($parsed_row = $this->parseRow($row, $i)) {
				$this->responseTable .= "<tr class='excelRow'>";
				$this->responseTable .= $parsed_row;
				$this->responseTable .= "</tr>";
				$i++;
			}
		}

		return $this->responseTable;
	}

	function parseFileUploaded()
	{

		$exp = array();

		if (file_exists($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

			$tmp_file_name = tempnam(sys_get_temp_dir(), 'BULK_');

			$file_type = $_FILES['file']['type'];

			$path = $_FILES['file']['name'];
			$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

			move_uploaded_file(
				$_FILES['file']['tmp_name'], $tmp_file_name
			);

			if (file_exists($tmp_file_name) && ($file_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $ext == "xlsx" || $ext == "xlsm")) {

				try {
					/** PhpSpreadsheet */

					require_once $_SERVER['DOCUMENT_ROOT'] . "/../vendor/phpoffice/phpspreadsheet/src/Bootstrap.php";

					$objReader = PhpOffice\PhpSpreadsheet\IOFactory::createReader("Xlsx");
					//Load the file
					$objReader = $objReader->load($tmp_file_name);
					//Get number of Sheets
					$sheetCount = $objReader->getSheetCount();
					//Set the Last Sheet as Active
					$objReader->setActiveSheetIndex($sheetCount - 1);
					//Read the data into an array
					$sheet = $objReader->getActiveSheet();
					$maxCell = $sheet->getHighestRowAndColumn();

					$max_col = $maxCell['column'];

					for ($row = 1; $row <= $maxCell['row']; $row++) {
						ini_set('max_execution_time', 300);
						$sheet_row = $sheet->rangeToArray("A$row:$max_col$row", null, true, false);
						if ($sheet_row && count($sheet_row) > 0) {
							if (!array_filter($sheet_row[0])) {
								break;
							}
							$exp[] = $sheet_row[0];
						}
					}

				} catch (Exception $e) {
					error_log($e->getMessage());
				}

				//Remove the Header
				if (count($exp) > 0) {
					$this->header_row = $exp[0];
					array_shift($exp);
				}

			}
		}

		$this->fileUploadParsed = true;
		return $exp;
	}


	//-----------------------------------------------------------------------------------------------------------------
	protected function addAttribute($key, $headTitle, $validation, $validate_column = null)
	{
		$this->keys[] = $key;
		$this->headTitles[] = $headTitle;
		$this->validators[] = $validation;
		if($validate_column) {
			$this->validate_column[$key] = $validate_column;
		}

		$this->numColumns++;

		// get data from back-end
		switch ($validation) {
			case self::Validate_BW_Device_Type():
				$this->getBwDeviceTypes();
				break;
			case self::Validate_UserID_Existence():
				$this->getAllUsers();
				break;
			case self::Validate_DeviceExistence():
				$this->getAllDevices();
				break;
			case self::Validate_Domain_Existence():
				$this->getAllGroupDomains();
				break;
			case self::Validate_Existing_Phone_Number():
				$this->getExistingPhoneNumbers();
				break;
			case self::Validate_PhoneNumber_Extension_Existence():
				$this->getAllPhoneNumberWithExtension();
				break;
			case self::Validate_BW_Tag_Name():
			case self::Validate_TrueFalse_Format():
			case self::Validate_YesNo_Format():
			case self::Validate_OnOff_Format():
			case self::Validate_Email_Format():
			case self::Validate_NonZero_Number():
			case self::Validate_Bridge_Warning_Format():
			case self::Validate_Extension():
			case self::Validate_Action():
			case self::Validate_NonEmpty():
			case self::Validate_None():
			case self::Validate_EmptyOrNonZeroNumber():
			case self::Validate_Phone_Profile():
			case self::Validate_Tag_Bundles():
			default:
		}
	}

	protected function addRequiredAttribute($key)
	{
		$this->requiredAttributes[] = $key;
	}

	/**
	 * /* Create HTML Table with rows as in the example below:
	 * <td class="bad">
	 * <input class="noBorder" title="MAC address is not 12 characters" type="text" name="customTags[0][macAddress]" id="customTags[0][macAddress]" value="3c27a21123b" style="width:98%;" readonly="readonly">
	 * </td>
	 *
	 * @param $row
	 * @return string
	 */
	//-----------------------------------------------------------------------------------------------------------------
	protected function parseRow($attr, $index)
	{

		$html = "";

		for ($i = 0; $i < $this->numColumns; $i++) {
			$key = $this->keys[$i];
			$value = $attr[$i];
			$value .= '';
			$error = '';

			// Validate each attribute
			$error = $this->validateColumn($attr, $i);

			$class = $error == "" ? "good" : "bad";

			if ($error != "") {
				$this->error = true;
			}
			
			//write to data array
			//$this->dataArray[$index][$key] = $value;

			// Create input name and id
			$name = $this->postName . "[" . $index . "][" . $key . "]";

			$html .= "<td class=\"" . $class . "\">";
			$html .= "<input class=\"noBorder\" title=\"" . $error . "\" type=\"text\"";
			$html .= " name=\"" . $name . "\" id=\"" . $name . "\"";
			$html .= " value=\"" . $value . "\" style=\"width:98%\" readonly=\"readonly\">";
			$html .= "</td>";
		}

		return $html;
	}

	protected function validateColumn($attr, $column) {
	    
		$key = $this->keys[$column];
		$value = $attr[$column] . '';
		
		if(isset($this->validate_column[$key])) {
			$error = $this->validateAttribute($attr[$this->validate_column[$key]], $this->validators[$column]);
		} else {
			$error = $this->validateAttribute($value, $this->validators[$column]);
		}

		if(in_array($key, $this->requiredAttributes)) {
			$error = $this->ValidateNonEmpty($value);
		}

		if($column == 2) {
		    $userId = $attr[17];
		    $error = $this->validateUserHaveVMSErvice($userId);
		}
		
		return $error;
	}


	//-----------------------------------------------------------------------------------------------------------------
	private function validateAttribute($attr, $validator)
	{
		switch ($validator) {
			case self::Validate_BW_Device_Type():
				return $this->validateDeviceType($attr);
			case self::Validate_BW_Tag_Name():
				return self::validateBwTagName($attr);
			case self::Validate_UserID_Format():
				return self::validateUserIdFormat($attr);
			case self::Validate_TrueFalse_Format():
				return self::validateTrueFalseFormat($attr);
			case self::Validate_YesNo_Format():
				return self::validateYesNoFormat($attr);
			case self::Validate_OnOff_Format():
				return self::validateOnOffFormat($attr);
			case self::Validate_Email_Format():
				return self::validateEmailFormat($attr);
			case self::Validate_NonZero_Number():
				return self::validateNonZeroNumber($attr);
			case self::Validate_UserID_Existence():
				return $this->validateUserIdExistence($attr);
			case self::Validate_DeviceExistence():
				return $this->validateDeviceExistence($attr);
			case self::Validate_Bridge_Warning_Format():
				return self::validateBridgeWarningFormat($attr);
			case self::Validate_Domain_Existence():
				return $this->validateDomainExistence($attr);
			case self::Validate_Extension():
				return self::validateExtension($attr);
			case self::Validate_Existing_Phone_Number():
				return self::ValidateExistingPhoneNumber($attr);
			case self::Validate_PhoneNumber_Extension_Existence():
				return self::ValidatePhoneNumberExtensionExistence($attr);
			case self::Validate_Action():
				return self::ValidateAction($attr);
			case self::Validate_Phone_Profile():
				return self::ValidatePhoneProfile($attr);
			case self::Validate_Tag_Bundles():
				return self::ValidateTagBundles($attr);
			case self::Validate_NonEmpty():
				return self::ValidateNonEmpty($attr);
			case self::Validate_EmptyOrNonZeroNumber():
				return self::validateEmptyOrNonZeroNumber($attr);

			default:
				return "";
		}
	}


	//-----------------------------------------------------------------------------------------------------------------
	private function validateDeviceType($deviceType)
	{
		return in_array($deviceType, $this->bw_DeviceTypesList) ? "" : $deviceType . " is not a valid Device Type";
	}


	//-----------------------------------------------------------------------------------------------------------------
	private function validateUserIdExistence($userId)
	{
		return in_array($userId, $this->userList) ? "" : "User does not exist";
	}


	private function validateUserHaveVMSErvice($userId)
	{
	    global $sessionid, $client, $db, $ociVersion;
	    require_once $_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/UserServiceGetAssignmentList.php";
	    
	    //if(strtoupper($row[2]) == 'ON') {
	        $assignedServices = new UserServiceGetAssignmentList($userId);
	        if ( !$assignedServices->hasService("Voice Messaging User") ) {
	            $servicePacks = array();
	            $userServices = array();
	            require_once("/var/www/lib/broadsoft/adminPortal/getServicePacks.php");
	            $responseVM = $this->getVMEnabledServicePack($servicePacks, $userServices);
	            if( !isset($responseVM["VMOnlyServicePack"]) && !isset($responseVM["VMService"]) ) {
	                return "VM services not assigned to User";
	            }
	        }
	    //}
	    return "";
	}
	
	//-----------------------------------------------------------------------------------------------------------------
	private function validateDeviceExistence($device)
	{
		return in_array($device, $this->devicesList) ? "" : "Device does not exist";
	}


	//-----------------------------------------------------------------------------------------------------------------
	private function validateDomainExistence($value)
	{
		$exp = explode("@", $value);

		return isset($exp[1]) && in_array($exp[1], $this->groupDomainList) ? "" : "Domain does not exist";
	}


	//-----------------------------------------------------------------------------------------------------------------
	private static function validateBwTagName($tagName)
	{
		if (strlen($tagName) > 2) {
			if ($tagName[0] == "%" && substr($tagName, -1) == "%") {
				return "";
			}
		}

		return "Invalid tag format, expected: '%NAME%'";
	}


	//-----------------------------------------------------------------------------------------------------------------
	private static function validateUserIdFormat($userId)
	{
		if (!filter_var($userId, FILTER_VALIDATE_EMAIL)) {
			return "Invalid User ID format. Expected: '<userPortion>@<domain>";
		}

		$uidSegments = explode("@", $userId);
		$len = strlen($uidSegments[0]);
		if ($len < 6 || $len > 80) {
			return "Invalid User ID format. User portion of the ID must be between 6-80 chars.";
		}

		$len = strlen($uidSegments[1]);
		if ($len < 2 || $len > 80) {
			return "Invalid User ID format. Domain portion of the ID must be between 2-80 chars.";
		}

		return "";
	}


	//-----------------------------------------------------------------------------------------------------------------
	private static function validateTrueFalseFormat($value)
	{
		return strtoupper($value) != "TRUE" && strtoupper($value) != "FALSE" ? "Expected 'true' or 'false'" : "";
	}

	//-----------------------------------------------------------------------------------------------------------------
	private static function validateYesNoFormat($value)
	{
		return strtoupper($value) != "YES" && strtoupper($value) != "NO" ? "Expected 'Yes' or 'No'" : "";
	}

	//-----------------------------------------------------------------------------------------------------------------
	private static function validateOnOffFormat($value)
	{
		return strtoupper($value) != "ON" && strtoupper($value) != "OFF" ? "Expected 'On' or 'Off'" : "";
	}

	//-----------------------------------------------------------------------------------------------------------------
	private static function validateEmailFormat($value)
	{
		return $value && !filter_var($value, FILTER_VALIDATE_EMAIL) ? "Invalid Email '$value'" : "";
	}


	//-----------------------------------------------------------------------------------------------------------------
	private static function validateNonZeroNumber($value)
	{
		return ctype_digit($value . '') && (int)$value > 0 ? "" : "Expected positive integer";
	}

	private static function validateEmptyOrNonZeroNumber($value)
	{
		return $value == "" ? '' : (ctype_digit($value . '') && (int)$value > 0 ? "" : "Expected positive integer");
	}


	//-----------------------------------------------------------------------------------------------------------------
	private static function validateBridgeWarningFormat($value)
	{
		return strtoupper($value) == "BARGE-IN" ||
		strtoupper($value) == "BARGE-IN AND REPEAT" ||
		strtoupper($value) == "NONE" ? "" : "Invalid Bridge Warning Tone attribute";
	}


	//-----------------------------------------------------------------------------------------------------------------
	private static function validateExtension($value)
	{
		$errorMsg = "";
		if (!is_numeric($value)) {
			$errorMsg = "Extension must be numeric.";
		}
		//min length and max length
		$minLen = intval($_SESSION['groupExtensionLength']['min']);
		$maxLen = intval($_SESSION['groupExtensionLength']['max']);

		if (strlen($value) < $minLen or strlen($value) > $maxLen) {
			if ($minLen == $maxLen) {
				$errorMsg = "Extension must be " . $minLen . " digits.";
			} else {
				$errorMsg = "Extension must be between " . $minLen . " and " . $maxLen . " digits.";
			}
		}

		return $errorMsg;
	}

	//-----------------------------------------------------------------------------------------------------------------
	private function ValidateExistingPhoneNumber($value)
	{
		$errorMsg = "";
		if (!is_numeric($value)) {
			$errorMsg = "Phone Number must be numeric.";
		} else if ($value !== "" and !in_array("+1-" . $value, $this->existingPhoneNumberList)) {
			$errorMsg = "Phone Number not in use.";
		}

		return $errorMsg;
	}

	//-----------------------------------------------------------------------------------------------------------------
	private function ValidatePhoneNumberExtensionExistence($value)
	{
		$errorMsg = "";
		if (!in_array($value, array_keys($this->phoneNumberWithExtensionList))) {
			$errorMsg = "No user found with this phone number and extension.";
		}

		return $errorMsg;
	}

	//-----------------------------------------------------------------------------------------------------------------
	private function ValidateAction($value)
	{
		$errorMsg = "";
		if (!in_array(strtolower($value), array('add', 'modify', 'delete', ''))) {
			$errorMsg = "Invalid Action.";
		}

		return $errorMsg;
	}

	//-----------------------------------------------------------------------------------------------------------------
	private function ValidateNonEmpty($value)
	{
		$errorMsg = "";
		if (strlen($value) <= 0 ) {
			$errorMsg = "This field is required.";
		}

		return $errorMsg;
	}

	//-----------------------------------------------------------------------------------------------------------------
	private function ValidatePhoneProfile($value)
	{
		$errorMsg = "";

		if($value != "") {
			require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/expressSheetFunctions.php");
			$hasPhoneProfileValue = getPhoneProfileValue($value);
			if (!$hasPhoneProfileValue) {
				$errorMsg = "This is not a valid Phone Profile.";
			}
		}

		return $errorMsg;
	}

	//-----------------------------------------------------------------------------------------------------------------
	private function ValidateTagBundles($value)
	{
		$errorMsg = "";

		if($value != "") {

			$tagBundles = explode(";", $value);

			foreach ($tagBundles as $tagBundle) {
				require_once($_SERVER['DOCUMENT_ROOT'] . "/Express/expressSheets/expressSheetFunctions.php");
				$tags = getAllTagsforBundle($tagBundle);
				if (count($tags) <= 0) {
					$errorMsg .= "$tagBundle is not a valid Tag Bundle. ";
				}
			}
		}

		return $errorMsg;
	}



	//-----------------------------------------------------------------------------------------------------------------
	private function getBwDeviceTypes()
	{
		global $nonObsoleteDevices;

		require_once("/var/www/lib/broadsoft/login.php");
		require_once("/var/www/html/Express/config.php");
		checkLogin();

		if (count($this->bw_DeviceTypesList) == 0) {
			require_once("/var/www/lib/broadsoft/adminPortal/getDevices.php");
			$this->bw_DeviceTypesList = $nonObsoleteDevices;
		}
	}


	//-----------------------------------------------------------------------------------------------------------------
	private function getAllUsers()
	{
		global $users;

		// Populate user list
		if (count($this->userList) == 0) {
			$i = 0;
			foreach ($users as $key => $user) {
				$this->userList[$i++] = $user["id"];
			}
		}
	}


	//-----------------------------------------------------------------------------------------------------------------
	private function getAllDevices()
	{
		global $devices;

		// Populate devices list
		if (count($this->devicesList) == 0) {
			$this->devicesList = $devices;
		}
	}


	//-----------------------------------------------------------------------------------------------------------------
	private function getAllGroupDomains()
	{
		global $groupDomains;
		// Populate group domains
		if (count($this->groupDomainList) == 0) {
			$this->groupDomainList = $groupDomains;
		}
	}

	//-----------------------------------------------------------------------------------------------------------------
	private function getExistingPhoneNumbers()
	{
		global $users;

		// Populate existingPhoneNumber list
		if (count($this->existingPhoneNumberList) == 0) {
			$i = 0;
			foreach ($users as $key => $user) {
				$this->existingPhoneNumberList[$i++] = $user["phoneNumber"];
			}
		}
	}

	//-----------------------------------------------------------------------------------------------------------------
	protected function getAllPhoneNumberWithExtension()
	{
		global $users;

		// Populate phoneNumberWithExtension List
		if (count($this->phoneNumberWithExtensionList) == 0) {
			$i = 0;
			foreach ($users as $key => $user) {
				$this->phoneNumberWithExtensionList[$user["phoneNumber"] . "x" . $user["extension"]] = $user["id"];
			}
		}

	}
	
	function checkUserScaService($uid){
		
		require_once ("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");
		//sca services check flag
		$scaArray = array(
				"Shared Call Appearance",
				"Shared Call Appearance 10",
				"Shared Call Appearance 15",
				"Shared Call Appearance 20",
				"Shared Call Appearance 25",
				"Shared Call Appearance 30",
				"Shared Call Appearance 35",
				"Shared Call Appearance 5",
		);
		$respServices = getUserAssignedUserServices($uid);

		if($respServices && is_array($respServices))
		{
			$scaServiceCountCheck = array_intersect($scaArray, $respServices);
			if(count($scaServiceCountCheck) > 0){
				return "Yes";
			}
		}

		return "No";
	}
	
	function checkUserScaServiceAssign($uid, $scaId, $rowData){
		require_once ("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");   //Code added @ 10 July 2018
		require_once ("/var/www/lib/broadsoft/adminPortal/services/userServices.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/UserGetRequest.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/getDataByMac.php");
		$scaArray = array(
		    "Shared Call Appearance",
		    "Shared Call Appearance 10",
		    "Shared Call Appearance 15",
		    "Shared Call Appearance 20",
		    "Shared Call Appearance 25",
		    "Shared Call Appearance 30",
		    "Shared Call Appearance 35",
		    "Shared Call Appearance 5",
		);
		     
		$data = "";
		$servicePackAssigned = "";
		$grpServiceList = new Services();
		$userServices = new userServices();
		if($rowData[0] == "Modify" || $rowData[0] == "Add") {
		    $respServices = getUserAssignedUserServices($scaId);
		    $scaServiceCountCheck = array_intersect($scaArray, $respServices);
		    $userservicePackArray = array();
		    $userSessionservicePackArray = $userServices->getUserServicesAll($scaId, $_SESSION["sp"]);
		    $servicesSessionArray = $grpServiceList->getServicePackServicesListUsers($userSessionservicePackArray["Success"]["ServicePack"]["assigned"]);
		    $scaSessionServiceArray = $grpServiceList->checkScaUserServiceInServicepack($servicesSessionArray);
		    foreach($scaSessionServiceArray as $ssa=>$ssv){
		        $pos = strpos($ssv->col[0], "Shared Call Appearance");
		        if($pos === false){
		        }else{
		            $servicePackAssigned = $userSessionservicePackArray["Success"]["ServicePack"]["assigned"][$ssa];
		        }
		    }
		    
		    if($servicePackAssigned == ""){
		        $grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]);
		        $servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
		        $servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray);
		        $scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);
		        $oneServicePack = "false";
		        $multipleServicePack = "false";
		        $noServicePack = "false";
		        $r = 0;
		        foreach($scaServiceArray as $ssa=>$ssv){
		            $pos = strpos($ssv->col[0], "Shared Call Appearance");
		            if($pos === false){
		                
		            }else{
		                if($ssv->count == 1){
		                    $oneServicePack = "true";
		                    $r++;
		                    //$countMore = "0";
		                }else if($ssv->count > 1){
		                    $multipleServicePack = "true";
		                    //$countMore = "true";
		                }else{
		                    $noServicePack = "true";
		                    //$countMore = "0";
		                }
		            }
		            
		        }
		        if($oneServicePack == "false" || $r != "1"){
		            $error = 1;
		            $bg = INVALID;
		            $data = "Express cannot unambiguously assign Service Pack with SCA service to users(s) : ";
		        }
		    }
		} else if($rowData[0] == "Delete") {
		    if( $this->isMultipleServicePackExistWithScaServiceOnUser($scaId) > 1)
		    {
		        $data = "Express cannot unambiguously unassign Service Pack with SCA service to users(s) : ";
		    }
		}
		//code ends
		return $data;
	}
	
	public function isMultipleServicePackExistWithScaServiceOnUser($scaUserId) {
	    require_once ("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");
	    require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
	    require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");   //Code added @ 10 July 2018
	    require_once ("/var/www/lib/broadsoft/adminPortal/services/userServices.php");
	    require_once ("/var/www/lib/broadsoft/adminPortal/UserGetRequest.php");
	    require_once ("/var/www/lib/broadsoft/adminPortal/getDataByMac.php");
	    $scaArray = array(
	        "Shared Call Appearance",
	        "Shared Call Appearance 10",
	        "Shared Call Appearance 15",
	        "Shared Call Appearance 20",
	        "Shared Call Appearance 25",
	        "Shared Call Appearance 30",
	        "Shared Call Appearance 35",
	        "Shared Call Appearance 5",
	    );
	    $userservicePackArray = array();
	    $userServices = new userServices();
	    $userSessionservicePackArray = $userServices->getUserServicesAll($scaUserId, $_SESSION["sp"]);
	    $userServicePackArr      = $userSessionservicePackArray["Success"]["ServicePack"]["assigned"];
	    $scaServicePackArr       = $userServices->getServicePacksListHaveScaServices($userServicePackArr);
	    $scaServicePackCustomArr = $userServices->getServicePacksHasOneScaServiceOnly($scaServicePackArr);
	    $servicePackSCACounter = 0;
	    foreach($scaServicePackCustomArr as $keyTmpN => $valTmpN)
	    {
	        if($valTmpN['hasOneSCAServiceOnly']=="true")
	        {
	            $servicePackSCACounter = $servicePackSCACounter + 1;
	        }
	    }
	    return $servicePackSCACounter;
	}
	
	
	function assignScaUserExpressSheet($userId){
		require_once ("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");   //Code added @ 10 July 2018
		require_once ("/var/www/lib/broadsoft/adminPortal/services/userServices.php");
		$grpServiceList = new Services();
		$userServices = new userServices();
		
		$changeSCAAssignLog = array();
		$grpServiceListResponse = $grpServiceList->GroupServiceGetAuthorizationListRequest($_SESSION["groupId"]); 
		$servicePackArray = $grpServiceList->getservicePacksHaveSCAService($grpServiceListResponse["Success"]["servicePacksAuthorizationTable"]);
		$servicesArray = $grpServiceList->getServicePackServicesListServiceProvider($servicePackArray);
		$scaServiceArray = $grpServiceList->checkScaServiceInServicepack($servicesArray);
		foreach($scaServiceArray as $ssa=>$ssv){
			$pos = strpos($ssv->col[0], "Shared Call Appearance");
			if($pos === false){
			}else{
				if($ssv->count == "1"){
					$servicePackToBeAssigned = $servicePackArray[$ssa][0];
				}
			}
		}
		$responseServicePackUser = "";
		if(!empty($userId)){ 
// 			foreach($value as $scaUserkey=>$scaUserVal){
				/*Check User Service Pack checked or not..*/
				$userSessionservicePackArray = $userServices->getUserServicesAll($userId, $_SESSION["sp"]);
				$servicesSessionArray = $grpServiceList->getServicePackServicesListUsers($userSessionservicePackArray["Success"]["ServicePack"]["assigned"]);
				$scaSessionServiceArray = $grpServiceList->checkScaUserServiceInServicepack($servicesSessionArray);
				//code ends
				if(count($scaSessionServiceArray) == 0){
					$responseServicePackUser = $grpServiceList->assignServicePackUser($userId, $servicePackToBeAssigned);
				}
// 			}
		}	
		return $responseServicePackUser;
	}

	public function validateHeader() {

		$header_error = '';
		$header_col_num = 0;
		foreach ($this->headTitles as $headTitle) {
			$column_header = $this->header_row[$header_col_num];
			$column_header = str_replace("\n", " ", $column_header);
			if($headTitle != $column_header) {
				//echo "$headTitle != " . $this->header_row[$header_col] . "<br/>";
				$header_error .= "Column $header_col_num should be \"$headTitle\", but your sheet has \"" . $column_header . "\"<br/>";

			}
			$header_col_num++;
		}
		return $header_error;
	}
	
	public function getVMEnabledServicePack($servicePacks, $userServices) {
	    global $sessionid, $client;
	    
	    require_once("/var/www/lib/broadsoft/adminPortal/services/Services.php");
	    $response = array();
	    $serviceObj = new Services();
	    /* Get VM service pack with VM user service only */
	    foreach ($servicePacks as $servicePack) {
	        $services = $serviceObj->getServicesListOfServicePackBasedOnEnterprise($servicePack, $_SESSION['sp']);
	        if (in_array("Voice Messaging User", $services)) {
	            if (count($services) == 1) {
	                $response["VMOnlyServicePack"] = $servicePack;
	                break;
	            }
	        }
	    }
	    
	    /* If VM only service pack is not available then check VM user service */
	    if( !(isset($response["VMOnlyServicePack"])) ) {
	        if (in_array("Voice Messaging User", $userServices)) {
	            $response["VMService"] = "Voice Messaging User";
	        }
	    }
	    
	    return $response;
	}
}

?>