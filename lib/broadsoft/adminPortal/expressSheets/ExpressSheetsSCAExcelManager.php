<?php
/**
 * Created by Dipin Krishna.
 * Date: 10/18/17
 * Time: 10:46 AM
 */
class ExpressSheetsSCAExcelManager extends ExpressSheetsExcelManager
{

	private $primaryUserIDColumn = 17;
	private $scaUserIDColumn = 18;

	public function __construct($postName, $parseFile)
	{
		parent::__construct($postName, $parseFile);

		$this->addAttribute("action",                                 "Action",                  self::Validate_Action());
		$this->addAttribute("primaryUser",                            "Primary User",            self::Validate_UserID_Existence(), 17);
		$this->addAttribute("scaPort",                                "SCA Port",                self::Validate_EmptyOrNonZeroNumber());
		$this->addAttribute("scaUser",                                "SCA User",                self::Validate_UserID_Existence(), 18);
		$this->addAttribute("isActive",                               "Active",                  self::Validate_TrueFalse_Format());
		$this->addAttribute("allowOrigination",                       "Origination",             self::Validate_TrueFalse_Format());
		$this->addAttribute("allowTermination",                       "Termination",             self::Validate_TrueFalse_Format());
		$this->addAttribute("alertAllAppearancesForClickToDialCalls", "Alert All Click To Dial", self::Validate_TrueFalse_Format());
		$this->addAttribute("alertAllAppearancesForGroupPagingCalls", "Alert All Group Paging",  self::Validate_TrueFalse_Format());
		$this->addAttribute("allowSCACallRetrieve",                   "Allow Call Retrieve",     self::Validate_TrueFalse_Format());
		$this->addAttribute("multipleCallArrangementIsActive",        "MCA",                     self::Validate_TrueFalse_Format());
		$this->addAttribute("enableCallParkNotification",             "Call Park Notification",  self::Validate_TrueFalse_Format());
		$this->addAttribute("allowBridgingBetweenLocations",          "Bridging",                self::Validate_TrueFalse_Format());
		$this->addAttribute("bridgeWarningTone",                      "Bridge Warning Tone",     self::Validate_Bridge_Warning_Format());
		//$this->addAttribute("phoneProfile",                           "Phone Profile",           self::Validate_Phone_Profile());
		//$this->addAttribute("tagBundles",                             "Tag Bundles",             self::Validate_Tag_Bundles());
		$this->addAttribute("numOfLineApperances",                    "Num of Line Appearances", self::Validate_EmptyOrNonZeroNumber());
		$this->addAttribute("callsPerLineKey",                        "Calls Per LineKey",       self::Validate_EmptyOrNonZeroNumber());
		$this->addAttribute("polycomLineRegNumber",                   "Polycom Line Reg. Number",self::Validate_EmptyOrNonZeroNumber());
	}

	protected function parseRow($row, $index)
	{
		if(isset($row[0]) && (strtolower($row[0]) == "add" || strtolower($row[0]) == "delete" || strtolower($row[0]) == "modify")) {

			if (is_array($row) && array_filter($row)) {
				
				$user_id_name = $this->postName . "[" . $index . "][userId]";
				$sca_user_id_name = $this->postName . "[" . $index . "][scaUserId]";
				$html = "<input value='{$row[17]}' name='$user_id_name' type='hidden'/>";
				$html .= "<input value='{$row[18]}' name='$sca_user_id_name' type='hidden'/>";
				return $html . parent::parseRow($row, $index);
			}
		}

		return null;

	}

	protected function validateColumn($attr, $column)
	{
	    //echo "attr ";print_r($attr);
	    //echo "col ";print_r($column);
		$key = $this->keys[$column];
		//Primary User and SCA USer cannot be the same
		if($key == 'scaUser') {
			$scaUser = $attr[$column];
			if($primaryUserIndex = array_search('primaryUser', $this->keys)) {
				$primaryUser = $attr[$primaryUserIndex];
				if($scaUser == $primaryUser) {
					return "Primary and SCA User cannot be the same.";
				}
			}
            
			$explodeUserId = $attr[$this->scaUserIDColumn];
			$scaServiceExist = $this->checkUserScaServiceAssign($attr[$this->primaryUserIDColumn], $explodeUserId, $attr);
			if(strpos($scaServiceExist, "unambiguously") > -1){
			    return $scaServiceExist." ". $scaUser;
			}

			/*   Old Code (Commenting becuase no longer used to check Sca Service pack, Though we assign service pack to Sca user if no sca Service Pack exist.)
			$scaServiceExist = $this->checkUserScaService($explodeUserId);
			if($scaServiceExist == "No"){
				return $scaUser . " does not have any SCA User Service assigned";
			}
			*/

		}
		
		if($key == "primaryUser"){
		        $userIdForDeviceCheck = $attr[$this->primaryUserIDColumn];
		        require_once ("/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php");
		        require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
		        $usobj = new UserOperations();
		        $existanceCheck = $usobj->checkUserHasDevice($userIdForDeviceCheck);
		        
		        if($existanceCheck["Success"]["deviceName"] == "") {
		            return "User has no primary device.";
		        }
		}
		
		if($key == 'numOfLineApperances' || $key == 'callsPerLineKey') {
			$value = $attr[$column];
			if($polycomLineRegNumberIndex = array_search('polycomLineRegNumber', $this->keys)) {
				$polycomLineRegNumberValue = $attr[$polycomLineRegNumberIndex];

				if($value > 0 && ($polycomLineRegNumberValue == "" || $polycomLineRegNumberValue <= 0)) {
					return "You must select a valid value for 'Polycom Line Reg. Number' if you want to update this field.";
				}
			}
		}

		return parent::validateColumn($attr, $column);

	}

}