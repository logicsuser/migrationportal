<?php
/**
 * Created by Dipin Krishna.
 * Date: 10/16/17
 * Time: 8:35 AM
 */

require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/login.php");

require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/commonUtility.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/groupManage/groupModify/groupModify.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/dns/dns.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/dns/dnsHelper.php");
require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getServicePacks.php");
require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getNetworkClassesOfService.php");
require($_SERVER['DOCUMENT_ROOT'] . "/../lib/broadsoft/adminPortal/getAllCustomContactDirectories.php");

$groupModify = new GroupModify($groupId);
$groupInfoData = $groupModify->getGroupInfo();

//dn class
$dns = new Dns();
$extensionResponse = $dns->getGroupExtensionConfigRequest($sp, $groupId);
//class for DNS helper
$dnsHelper = new DnsHelper();
$extensionInfo = $dnsHelper->getGroupExtensionConfigRequestHelper($extensionResponse);

$groupInfoData = array_merge_recursive($groupInfoData, $extensionInfo);

?>