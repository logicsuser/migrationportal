<?php
class UserServicesOperations {
	
	
	function _construct() {
	}
	
	function getUserDeviceDetail($userInfo)
	{
		global $sessionid, $client;
		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetRequest18sp1");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($userInfo["serviceProviderId"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($userInfo["groupId"]) . "</groupId>";
		$xmlinput .= "<deviceName>" . $userInfo["deviceName"] . "</deviceName>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errMsg = readErrorXmlGenuine($xml);
		if($errMsg != "") {
			$returnResponse["Error"] = $errMsg;
		}
		else
		{
			$returnResponse["Success"] = strval($xml->command->deviceType);
		}
		return $returnResponse;
	}
		
	function getUserDetail($userId)
	{
		global $sessionid, $client;
		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";
		
		$xmlinput .= openBroadsoftHeader($sessionid, "UserGetRequest19");
		$xmlinput .= self::getUserGetCommand($userId);
		$xmlinput .= closeBroadSoftHeader();
		print_r($xmlinput);
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		print_r($response);
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errMsg = readErrorXmlGenuine($xml);
		if($errMsg != "") {
			$returnResponse["Error"] = $errMsg;
		}
		else
		{
			foreach ($xml->command->attributes() as $a => $b)
			{
				if ($b == "Error")
				{
					$userInfo["error"] = "true";
				}
			}
			
			$userInfo["serviceProviderId"] = strval($xml->command->serviceProviderId);
			$userInfo["groupId"] = strval($xml->command->groupId);
			$userInfo["deviceName"] = isset($xml->command->accessDeviceEndpoint->accessDevice->deviceName) ? strval($xml->command->accessDeviceEndpoint->accessDevice->deviceName) : "";
			$userInfo["userId"] = $userId;
			$userInfo["lastName"] = strval($xml->command->lastName);
			$userInfo["firstName"] = strval($xml->command->firstName);
			$userInfo["callingLineIdPhoneNumber"] = strval($xml->command->callingLineIdPhoneNumber);
			$userInfo["extension"] = strval($xml->command->extension);
			$userDevice = $this->getUserDeviceDetail($userInfo);
			if(count($userDevice['Success']) > 0)
			{
				$userInfo["deviceType"] =$userDevice['Success'];
			}
			$returnResponse["Success"] = $userInfo;
		}
		//print_r($returnResponse); exit;
		return $returnResponse;
	}
	
	public static function getUserGetCommand($userId) {
		$xmlinput = openCommand( "UserGetRequest19");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	public static function getUserGetCommandResponse($commandResponse) {
		//$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errMsg = "";//readErrorXmlGenuine($commandResponse);
		if($errMsg != "") {
			$returnResponse["Error"] = $errMsg;
		}
		else
		{
			foreach ($commandResponse->attributes() as $a => $b)
			{
				if ($b == "Error")
				{
					$userInfo["error"] = "true";
				}
			}
			
			$userInfo["serviceProviderId"] = strval($commandResponse->serviceProviderId);
			$userInfo["groupId"] = strval($commandResponse->groupId);
			$userInfo["deviceName"] = isset($commandResponse->accessDeviceEndpoint->accessDevice->deviceName) ? strval($commandResponse->accessDeviceEndpoint->accessDevice->deviceName) : "";
			$userInfo["userId"] = $userId;
			$userInfo["lastName"] = strval($commandResponse->lastName);
			$userInfo["firstName"] = strval($commandResponse->firstName);
			$userInfo["callingLineIdPhoneNumber"] = strval($commandResponse->callingLineIdPhoneNumber);
			$userInfo["extension"] = strval($commandResponse->extension);
			/* $userDevice = $this->getUserDeviceDetail($userInfo);
			if(count($userDevice['Success']) > 0)
			{
				$userInfo["deviceType"] =$userDevice['Success'];
			} */
			$returnResponse["Success"] = $userInfo;
		}
		//print_r($returnResponse); exit;
		return $returnResponse;
	}
	
	
}