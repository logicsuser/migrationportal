<?php
	$xmlinput = xmlHeader($sessionid, "GroupThirdPartyVoiceMailSupportGetRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$thirdPartyVoiceMailSupport = strval($xml->command->isActive); //must be true for groups with Third Party Voice Mail Support
?>
