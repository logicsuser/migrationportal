<?php
	$xmlinput = xmlHeader($sessionid, "GroupDnGetListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->phoneNumber as $key => $value)
	{
		$allNumbers[$a] = strval($value);
		$a++;
	}
?>
