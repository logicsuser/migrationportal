<?php

/**
 * Created by Karl.
 * Date: 8/20/2016
 */
abstract class ResourceNameBuilder
{
    protected abstract function getFallbackName();
    
    // public constants
    const DN = "DN";
    const EXT = "EXT";
    const USR_CLID = "USERCLID";
    const GRP_DN = "GROUPDN";
    const USR_ID = "USERID";
    const GRP_ID = "GROUPID";
    const SYS_DFLT_DOMAIN = "SYSTEMDEFAULTDOMAIN";
    const DEFAULT_DOMAIN = "DEFAULTDOMAIN";
    const GRP_PROXY_DOMAIN = "GROUPPROXYDOMAIN";
    const SRG_DOMAIN = "SURGEMAILDOMAIN";
    const FIRST_NAME = "FIRSTNAME";
    const LAST_NAME = "LASTNAME";
    const DEV_NAME = "DEVICENAME";
    const DEV_TYPE = "DEVICETYPE";
    const MAC_ADDR = "MAC";
    const INT_1 = "N";
    const INT_2 = "NN";
    const TXT = "TXT";
    const GRP_CLID= "GROUPCLID";
    const GRP_NAME= "GROUPNAME";
    const MAC= "MAC";
    const SN= "SN";
    const DEV_IP= "DEVICEIP";
    const SITE_NAME= "SiteName";
    const ENT = "ENTERPRISEID";
    
    protected $forceResolve;
    protected $defaults;
    protected $msg="";
    
    private $parameter = "PARAMETER";
    private $paramValue = "PARAMVALUE";
    
    private $syntaxList;
    private $parseTable;
    private $isError = false;
    private $currentAttributeIndex = 0;
    
    // implement appropriate attributes in child classes
    /*
     $registeredParams = array(
     ResourceNameBuilder::DN,
     ResourceNameBuilder::EXT,
     ResourceNameBuilder::USR_CLID,
     ResourceNameBuilder::GRP_DN,
     ResourceNameBuilder::USR_ID,
     ResourceNameBuilder::GRP_ID,
     ResourceNameBuilder::SYS_DFLT_DOMAIN,
     ResourceNameBuilder::DEFAULT_DOMAIN,
     ResourceNameBuilder::GRP_PROXY_DOMAIN,
     ResourceNameBuilder::SRG_DOMAIN,
     ResourceNameBuilder::FIRST_NAME,
     ResourceNameBuilder::LAST_NAME,
     ResourceNameBuilder::DEV_NAME,
     ResourceNameBuilder::MAC_ADDR,
     ResourceNameBuilder::INT_1,
     ResourceNameBuilder::INT_2,
     ResourceNameBuilder::TXT
     );
     */
    //TODO: More validations: look for forbidden characters in free-form text components
    //TODO: More validations: implement builder specific validations, eg. user ID format must be: <someName>@<someDomain>
    
    // constructor
    // $nameStr          - formula string
    // $registrationList - list of allowable attributes in inherited class
    // $forceResolve     - flag indicating whether name should use fallback to be resolved
    // ----------------------------------------------------------------------------------------------------------------
    public function __construct($nameStr, $registrationList, $forceResolve)
    {
        $this->forceResolve = $forceResolve;
        
        
        for ($i = 0; $i < count($registrationList); $i++) {
            $this->syntaxList[$i] = $registrationList[$i];
        }
        
        $this->parse($nameStr);
        $this->validateRegisteredParameters();
        $this->resolveDefaults();
    }
    
    
    // get validation status
    // ----------------------------------------------------------------------------------------------------------------
    public function validate() {
        return ! $this->isError;
    }
    
    
    // get all parsed attributes keys, so they can be resolved
    // function will return attribute key, eg. ResourceNameBuilder::DN
    // or -1 if all attributes are read
    // ----------------------------------------------------------------------------------------------------------------
    public function getRequiredAttributeKey() {
        if ($this->currentAttributeIndex == -1) { return "";}
        
        foreach ($this->parseTable as $index=>$value) {
            if ($index < $this->currentAttributeIndex) {
                continue;
            }
            $this->currentAttributeIndex = $index + 1;
            if ($value[$this->parameter] != "" && ! isset($this->defaults[$value[$this->parameter]])) {
                return $value[$this->parameter];
            }
        }
        
        $this->currentAttributeIndex = -1;
        return "";
    }
    
    
    // resolve parsed attribute
    // input: - attribute key/type, eg. ResourceNameBuilder::DN
    //        - attribute value
    // ----------------------------------------------------------------------------------------------------------------
    public function setRequiredAttribute($type, $val) {
        if ($type == "") {
            return;
        }
        
        // set integer attribute in a proper format
        if ($type == ResourceNameBuilder::INT_1 || $type == ResourceNameBuilder::INT_2) {
            $val = $type == ResourceNameBuilder::INT_2 ? sprintf("%02d", (int)$val) : (int)$val;
        }
        
        // This is XO modification!
        // alter value for device type
        if ($type == ResourceNameBuilder::DEV_TYPE) {
            $val = $this->alterDeviceType($val);
        }
        
        foreach ($this->parseTable as $index=>$value) {
            if ($value[$this->parameter] == $type) {
                // adjust value if parameter has length attribute
                $this->parseTable[$index][$this->paramValue] = $this->adjustAttributeValue($val, $value["attribute"]);
            }
        }
    }
    
    
    // this function is used just for debugging
    // ----------------------------------------------------------------------------------------------------------------
    public function getParseTable() {
        $str = "";
        foreach ($this->parseTable as $key=>$value) {
            if ($value[$this->parameter] != "") {
                $str .= "param:" . $value[$this->parameter] . ":" . $value[$this->paramValue] . ":" . $value["attribute"] . " ";
            } else {
                $str .= "text:" . $value[ResourceNameBuilder::TXT] . " ";
            }
        }
        
        //return $str;
        return $str . "***MSG*** " . $this->msg;
    }
    
    
    // get resolved name
    // ----------------------------------------------------------------------------------------------------------------
    public function getName() {
        if ($this->isError || ! $this->isResolved  () ) {
            return $this->forceResolve ? $this->getFallbackName() : "";
        }
        
        $name = "";
        
        foreach ($this->parseTable as $key=>$value) {
            $name .= $value[$this->paramValue];
            $name .= $value[ResourceNameBuilder::TXT];
        }
        
        return $name;
    }
    
    
    // get resolved name without domain
    // ----------------------------------------------------------------------------------------------------------------
    public function getNameWithoutDomain() {
        $name = $this->getName();
        if ($name == "") { return ""; }
        
        $nameSplit = explode("@", $name);
        return $nameSplit[0];
    }
    
    
    // get resolved fallback name without domain
    // ----------------------------------------------------------------------------------------------------------------
    public function getFallbackNameWithoutDomain() {
        $name = $this->getFallbackName();
        if ($name == "") { return ""; }
        
        $nameSplit = explode("@", $name);
        return $nameSplit[0];
    }
    
    
    
    
    // return requested attribute from parse table or empty string if not found
    // input: attribute name eg. ResourceNameBuilder::DN
    // ------------------------------------------------------------------------
    protected function getAttribute($attribute) {
        foreach ($this->parseTable as $index=>$value) {
            if ($value[$this->parameter] == $attribute) {
                return isset($value[$this->paramValue]) ? $value[$this->paramValue] : "";
            }
        }
        
        return "";
    }
    
    
    protected function getDefaultAttribute($attribute) {
        foreach ($this->defaults as $key=>$value) {
            if ($key == $attribute) {
                return $value;
            }
        }
        
        return "";
    }
    
    
    // get domain for fallback
    // Attempt to get a domain in the following order:
    // - group domain
    // - alternate domain
    // - default system domain
    // -----------------------------------------------
    protected function getDomainForFallback() {
        
        $domain = $this->getAttribute(ResourceNameBuilder::DEFAULT_DOMAIN);
        if ($domain != "") { return $domain; }
        
        return $this->getDefaultAttribute(ResourceNameBuilder::SYS_DFLT_DOMAIN);
    }
    
    
    // main parser
    //--------------------------------
    private function parse($nameStr) {
        if ($nameStr == "") {
            $this->isError = true;
            return;
        }
        
        $state = "text";
        $index = 0;
        while ($nameStr != "") {
            $splitChar = $state == "text" ? "<" : ">";
            $type = $state == "text" ? ResourceNameBuilder::TXT : $this->parameter;
            $pos = strpos($nameStr, $splitChar);
            
            if ($pos === false) {
                $val = $state != "text" ? strtoupper($nameStr) : $nameStr;
                //$this->parseTable[$index][$type] = $val;
                $this->parse2($val, $type, $index);
                break;
            }
            if ($pos > 0) {
                $val = $state != "text" ? strtoupper(substr($nameStr, 0, $pos)) : substr($nameStr, 0, $pos);
                //$this->parseTable[$index++][$type] = $val;
                $this->parse2($val, $type, $index++);
            }
            $nameStr = substr($nameStr, $pos+1);
            $state = ($state == "text") ? "parameter" : "text";
            
            if ($index == 10) {
                break;
            }
        }
    }
    
    // parse for parameter in a parameter
    // Example: the attribute of sentence 'groupId(6)' is value 6, parameter name is 'groupId'
    // ---------------------------------------------------------------------------------------
    private function parse2($param, $type, $index) {
        // text types don't have attributes; it's all text
        if ($type == ResourceNameBuilder::TXT) {
            $this->parseTable[$index][$type] = $param;
            return;
        }
        
        // if there is no opening parenthesis or opening parenthesis is at first position, assume there is no embedded attribute
        $pos = strpos($param, "(");
        if ($pos === false || $pos == 0) {
            $this->parseTable[$index][$type] = $param;
            return;
        }
        
        $val = substr($param, 0, $pos);
        $remainingStr = substr($param, $pos+1);
        
        // if there is no closing parenthesis, assume there is no embedded attribute
        $pos = strpos($remainingStr, ")");
        if ($pos == false) {
            $this->parseTable[$index][$type] = $param;
            return;
        }
        
        $attr = substr($remainingStr, 0, $pos);
        $this->parseTable[$index][$type] = $val;
        $this->parseTable[$index]["attribute"] = $attr;
    }
    
    
    private function resolveDefaults() {
        $i = 0;
        foreach ($this->parseTable as $index=>$value) {
            if (isset($value[$this->parameter]) && isset($this->defaults[$value[$this->parameter]]) ) {
                // adjust value if parameter has length attribute
                $this->parseTable[$index][$this->paramValue] =
                $this->adjustAttributeValue($this->defaults[$value[$this->parameter]], $value["attribute"]);
            }
            $i++;
        }
    }
    
    private function adjustAttributeValue($value, $lenStr) {
        if ($lenStr == "" || (int)$lenStr == 0) {
            return $value;
        }
        
        $len = (int)$lenStr;
        return strlen($value) > $len ? substr($value, 0, $len) : "";
    }
    
    
    private function isResolved() {
        foreach ($this->parseTable as $index=>$value) {
            if (isset($value[$this->parameter])) {
                if (! isset($value[$this->paramValue]) || $value[$this->paramValue] == "") {
                    $this->msg .= " Not resolved" . $value[$this->parameter] . " ";
                    return false;
                }
            }
        }
        
        return true;
    }
    
    
    // validate whether parsed parameters are allowed
    // set error flag if parsed parameter is not allowed
    // validation is done against registered parameters
    // -------------------------------------------------
    private function validateRegisteredParameters() {
        foreach ($this->parseTable as $key=>$value) {
            if ($value[$this->parameter] != "") {
                if (! in_array($value[ $this->parameter], $this->syntaxList)) {
                    $this->isError = true;
                    break;
                }
            }
        }
    }
    
    // XO Change - remove from GA
    // -------------------------------------------------
    private function alterDeviceType($devType) {
        $exclusions = array("Polycom_", "AudioCodes-");
        
        for ($i = 0; $i < count($exclusions); $i++) {
            $len = strlen($exclusions[$i]);
            if (substr($devType, 0, $len) == $exclusions[$i]) {
                return substr($devType, $len);
            }
        }
        return $devType;
    }
    
}// class
