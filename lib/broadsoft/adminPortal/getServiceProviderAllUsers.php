<?php
	$xmlinput = xmlHeader($sessionid, "UserGetListInServiceProviderRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	//$xmlinput .= "<GroupId>" . htmlspecialchars($_SESSION["groupId"]) . "</GroupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
//print_r($xml);

	$userCheck = $xml;
	$a = 0;
	if (isset($xml->command->userTable->row))
	{
		foreach ($xml->command->userTable->row as $key => $value)
		{
			$chkUserId[$a] = strval($value->col[0]);
			$users[$a]["id"] = strval($value->col[0]);
			$users[$a]["name"] = strval($value->col[2]) . ", " . strval($value->col[3]);
			$users[$a]["phoneNumber"] = strval($value->col[5]);
            $users[$a]["extension"] = strval($value->col[11]);
			$a++;
			$ext = strval($value->col[11]);
			$userPh[$ext]["name"] = strval($value->col[2]) . ", " . strval($value->col[3]);
		}
	}

	if (isset($users))
	{
		$users = subval_sort($users, "name");
	}
?>
