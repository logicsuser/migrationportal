<?php
	unset($_SESSION["callCenterInfo"]);
	
	if ($ociVersion == "19")
	{
		$xmlinput = xmlHeader($sessionid, "GroupCallCenterGetInstanceRequest19");		
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "GroupCallCenterGetInstanceRequest19sp1");
	}
	$xmlinput .= "<serviceUserId>" . $_POST["ccId"] . "</serviceUserId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$_SESSION["callCenterInfo"]["info"]["name"] = strval($xml->command->serviceInstanceProfile->name);
	$_SESSION["callCenterInfo"]["info"]["phoneNumber"] = strval($xml->command->serviceInstanceProfile->phoneNumber);
	$_SESSION["callCenterInfo"]["info"]["type"] = strval($xml->command->type);
	$_SESSION["callCenterInfo"]["info"]["policy"] = strval($xml->command->policy);
	$_SESSION["callCenterInfo"]["info"]["enableVideo"] = strval($xml->command->enableVideo);
	$_SESSION["callCenterInfo"]["info"]["queueLength"] = strval($xml->command->queueLength);
	$_SESSION["callCenterInfo"]["info"]["enableReporting"] = strval($xml->command->enableReporting);
	$_SESSION["callCenterInfo"]["info"]["allowCallerToDialEscapeDigit"] = strval($xml->command->allowCallerToDialEscapeDigit);
	$_SESSION["callCenterInfo"]["info"]["escapeDigit"] = strval($xml->command->escapeDigit);
//	$_SESSION["callCenterInfo"]["info"]["resetCallStatisticsUponEntryInQueue"] = strval($xml->command->resetCallStatisticsUponEntryInQueue);
//	$_SESSION["callCenterInfo"]["info"]["allowAgentLogoff"] = strval($xml->command->allowAgentLogoff);
//	$_SESSION["callCenterInfo"]["info"]["allowCallWaitingForAgents"] = strval($xml->command->allowCallWaitingForAgents);
//	$_SESSION["callCenterInfo"]["info"]["allowCallsToAgentsInWrapUp"] = strval($xml->command->allowCallsToAgentsInWrapUp);
//	$_SESSION["callCenterInfo"]["info"]["overrideAgentWrapUpTime"] = strval($xml->command->overrideAgentWrapUpTime);
//	$_SESSION["callCenterInfo"]["info"]["wrapUpSeconds"] = strval($xml->command->wrapUpSeconds);
//	$_SESSION["callCenterInfo"]["info"]["forceDeliveryOfCalls"] = strval($xml->command->forceDeliveryOfCalls);
//	$_SESSION["callCenterInfo"]["info"]["enableAutomaticStateChangeForAgents"] = strval($xml->command->enableAutomaticStateChangeForAgents);
//	$_SESSION["callCenterInfo"]["info"]["agentStateAfterCall"] = strval($xml->command->agentStateAfterCall);
//	$_SESSION["callCenterInfo"]["info"]["externalPreferredAudioCodec"] = strval($xml->command->externalPreferredAudioCodec);
//	$_SESSION["callCenterInfo"]["info"]["internalPreferredAudioCodec"] = strval($xml->command->internalPreferredAudioCodec);
//	$_SESSION["callCenterInfo"]["info"]["playRingingWhenOfferingCall"] = strval($xml->command->playRingingWhenOfferingCall);

	$xmlinput = xmlHeader($sessionid, "GroupCallCenterGetAgentListRequest");
	$xmlinput .= "<serviceUserId>" . $_POST["ccId"] . "</serviceUserId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->agentTable->row as $key => $value)
	{
		$_SESSION["callCenterInfo"]["agents"][$a]["userId"] = strval($value->col[0]);
		$_SESSION["callCenterInfo"]["agents"][$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
		$a++;
	}

	if (isset($_SESSION["callCenterInfo"]["agents"]))
	{
		$_SESSION["callCenterInfo"]["agents"] = subval_sort($_SESSION["callCenterInfo"]["agents"], "name");
	}

	$xmlinput = xmlHeader($sessionid, "GroupCallCenterGetAvailableAgentListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<callCenterType>" . $_SESSION["callCenterInfo"]["info"]["type"] . "</callCenterType>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->userTable->row as $key => $value)
	{
		if ((isset($_SESSION["callCenterInfo"]["agents"]) and !searchArray($_SESSION["callCenterInfo"]["agents"], "userId", strval($value->col[0]))) or !isset($_SESSION["callCenterInfo"]["agents"]))
		{
			$_SESSION["callCenterInfo"]["availableAgents"][$a]["id"] = strval($value->col[0]);
			$_SESSION["callCenterInfo"]["availableAgents"][$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
			$a++;
		}
	}

	if (isset($_SESSION["callCenterInfo"]["availableAgents"]))
	{
		$_SESSION["callCenterInfo"]["availableAgents"] = subval_sort($_SESSION["callCenterInfo"]["availableAgents"], "name");
	}

	//Disposition Codes (Group)

	//check if service provider is an enterprise before determining which commands to run
	if ($_SESSION["enterprise"] == "true")
	{
		$xmlinput = xmlHeader($sessionid, "EnterpriseCallCenterCallDispositionCodeGetListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "GroupCallCenterCallDispositionCodeGetListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	}
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->dispositionCodesTable->row as $key => $value)
	{
		$_SESSION["callCenterInfo"]["info"]["codesGroup"][$a]["code"] = strval($value->col[1]);
		$_SESSION["callCenterInfo"]["info"]["codesGroup"][$a]["description"] = strval($value->col[2]);
		$a++;
	}

	//Disposition Codes (ACD)
	$xmlinput = xmlHeader($sessionid, "GroupCallCenterQueueCallDispositionCodeGetListRequest");
	$xmlinput .= "<serviceUserId>" . $_POST["ccId"] . "</serviceUserId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	if (isset($xml->command->dispositionCodesTable->row))
	{
		foreach ($xml->command->dispositionCodesTable->row as $key => $value)
		{
			if (strval($value->col[3]) == "Queue")
			{
				$_SESSION["callCenterInfo"]["info"]["codesACD"][$a]["code"] = strval($value->col[1]);
				$_SESSION["callCenterInfo"]["info"]["codesACD"][$a]["description"] = strval($value->col[2]);
				$a++;
			}
		}
	}
	
	$phoneNumberStatus = "No";
	$userPhoneNumber = "";
	if(isset($_SESSION["callCenterInfo"]["info"]["phoneNumber"]) && !empty($_SESSION["callCenterInfo"]["info"]["phoneNumber"])){
		require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
		
		$ccDn = new Dns (); //echo $_POST["ccId"];
		$numberActivateResponse = $ccDn->getUserDNActivateListRequest ( $_POST["ccId"] );
		//echo "<pre>"; print_r($numberActivateResponse); die;
		if (empty ( $numberActivateResponse ['Error'] )) {
			if (! empty ( $numberActivateResponse ['Success'] [0] ['phoneNumber'] )) {
				$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
			}
			if (! empty ( $numberActivateResponse ['Success'] [0] ['status'] )) {
				$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
			}
		}
	}
	$_SESSION["callCenterInfo"]["info"]["ccActivateNumber"] = $phoneNumberStatus;
	
?>
