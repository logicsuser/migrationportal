<?php

/**
 * Created by Karl.
 * Date: 1/2/2017
 */
class ServerConfiguration
{
    private $db;
    private $id = "";
    private $type = "";
    private $instance;
    private $active = "";
    private $ip = "";
    private $userName = "";
    private $password = "";
    private $version = "";
    private $protocol = "";
    private $port = "";
    private $label = "";
    private $password_salt = "";
    private static function serverTable() { return "serversConfig"; }
    private static function colID() { return "id"; }
    private static function colActive() { return "active"; }
    private static function colIP() { return "ip"; }
    private static function colUserName() { return "userName"; }
    private static function colPassword() { return "password"; }
    private static function colOciRelease() { return "ociRelease"; }
    private static function colProtocol() { return "protocol"; }
    private static function colPort() { return "port"; }
    private static function colLabel() { return "label"; }
    private static function colPasswordSalt() { return "password_salt"; }
    private static function colClusterName() { return "clusterName"; }

    /**
     * ServerConfiguration constructor.
     * @param $db
     * @param $serverType
     * @param $serverInstance
     */
    public function __construct($db, $serverType, $serverInstance, $selectedCluster='')
    {
        $this->db = $db;
        $this->type = $serverType;
        $this->instance = $serverInstance;

	require_once("/var/www/html/Express/functions.php");
        //Code added @ 13 June 2019
        $whereCndtn = " and 1=1 ";        
        if($selectedCluster !="" && $serverType == "bwAppServer"){
            $selectedCluster = trim($selectedCluster);
            $whereCndtn = " and clusterName = '$selectedCluster' ";
        }
        //End code
        $query  = "select clusterName, id, active, ip, userName, password, ociRelease, protocol, port, password_salt from " . self::serverTable();
        $query .= " where type='" . $serverType . "' and instance='" . $serverInstance . "' $whereCndtn ";
        $query .= " order by id asc";

        $result = $this->db->query($query);
        $row = $result->fetch();

        $this->id = $row[self::colID()];
        $this->active = $row[self::colActive()];
        $this->ip = $row[self::colIP()];
        $this->userName = $row[self::colUserName()];
        $this->version = $row[self::colOciRelease()];
        $this->protocol = $row[self::colProtocol()];
        $this->port     = $row[self::colPort()];
        $this->clusterName     = $row[self::colClusterName()];        
        //$this->label = $row[self::colLabel()];
        if(isset($row[self::colPasswordSalt()]) && $row[self::colPasswordSalt()]) {
	        $this->password = decryptString($row[self::colPassword()], $row[self::colUserName()] . "_" . $row[self::colPasswordSalt()]);
        } else {
	        $this->password = $row[self::colPassword()];
        }
    }

    /**
     * @param $ip
     * @param $userName
     * @param $password
     * @param $ociRelease
     * @param $label
     * @param $active
     */
    public function updateConfig($ip, $userName, $password, $ociRelease, $active, $protocol='', $port='') {

        
	    require_once("/var/www/html/Express/functions.php");

    	$password_salt = sha1(uniqid(bin2hex(openssl_random_pseudo_bytes(16)), true));
	    $password = encryptString($password, $userName . "_" . $password_salt);

        $query = "UPDATE " . self::serverTable() . " set " .
            self::colActive()    . "='" . $active . "', " .
            self::colIP()        . "='" . $ip . "', " .
            self::colUserName()  . "='" . $userName . "', " .
            self::colPassword()  . "='" . $password . "', " .
            self::colOciRelease() . "='" . $ociRelease . "', " .
            self::colProtocol()  . "='" . $protocol . "', " .
            self::colPort()      . "='" . $port . "', " .
            //self::colLabel()     . "='" . $label . "', " .
            self::colPasswordSalt()     . "='" . $password_salt . "' " .
            "where " . self::colID() . "='" . $this->id . "'";

        $this->db->query($query);
    }


    public function updateClusterConfig($id, $clusterName, $ip, $userName, $password, $ociRelease, $active, $protocol='', $port='') {
        
        require_once("/var/www/html/Express/functions.php");
        
        $password_salt = sha1(uniqid(bin2hex(openssl_random_pseudo_bytes(16)), true));
        $password = encryptString($password, $userName . "_" . $password_salt);
        
        $query = "UPDATE " . self::serverTable() . " set " .
            self::colActive()    . "='" . $active . "', " .
            self::colClusterName()    . "='" . $clusterName . "', " .
            self::colIP()        . "='" . $ip . "', " .
            self::colUserName()  . "='" . $userName . "', " .
            self::colPassword()  . "='" . $password . "', " .
            self::colOciRelease() . "='" . $ociRelease . "', " .
            self::colProtocol()  . "='" . $protocol . "', " .
            self::colPort()      . "='" . $port . "', " .
            //self::colLabel()     . "='" . $label . "', " .
        self::colPasswordSalt()     . "='" . $password_salt . "' " .
        "where " . self::colID() . "='" . $id . "'";
        
        $this->db->query($query);
    }
    
    public function updateClusterXspConfig($id, $clusterName, $ip, $active, $protocol='', $port='') {
        
        require_once("/var/www/html/Express/functions.php");
        
        $query = "UPDATE " . self::serverTable() . " set " .
            self::colActive()    . "='" . $active . "', " .
            self::colIP()        . "='" . $ip . "', " .
            self::colProtocol()  . "='" . $protocol . "', " .
            self::colPort()      . "='" . $port . "' " .        
            "where " . self::colID() . "='" . $id . "'";
            
        $this->db->query($query);
    }
    
    public function getClusterInfo()
    {
        require_once("/var/www/html/Express/functions.php");
        
        $this->instance = $serverInstance;
        
        $query  = "select * from " . self::serverTable();
        $query .= " where type='" . $this->type . "' order by id asc";
        
        $result = $this->db->query($query);
        $clusters = array();
        while ($row = $result->fetch()) {
            if(isset($row[self::colPasswordSalt()]) && $row[self::colPasswordSalt()]) {
                $row['password'] = decryptString($row[self::colPassword()], $row[self::colUserName()] . "_" . $row[self::colPasswordSalt()]);
            } else {
                $row['password'] = $row[self::colPassword()];
            }
            $clusters[$row['clusterName']][$row['instance']] = $row;
        }
               
        return $clusters;
    }


    public function deleteCluster($primaryRowId, $secondayRowId, $clusterName)
    {
//         require_once("/var/www/html/Express/functions.php");
        $query  = "delete from " . self::serverTable();
        $query .= " where type in ('bwAppServer','xspServer')";
        $query .= " and clusterName = '" . $clusterName . "'";
        //SELECT clusterName FROM serversconfig WHERE clusterName = "AveriStar" and id not in ('1', '2');
        
        $result = $this->db->query($query);
        return $result;
    }
    
    public function getClusterPassword($id)
    {
        $password = "";
        require_once("/var/www/html/Express/functions.php");
        
        $query  = "select id, userName, password, password_salt from " . self::serverTable();
        $query .= " where type='" . $this->type . "' and instance='" . $this->instance . "' and id='" . $id . "'";        
        
        $result = $this->db->query($query);
        $row = $result->fetch();        
        if(isset($row[self::colPasswordSalt()]) && $row[self::colPasswordSalt()]) {
            $password = decryptString($row[self::colPassword()], $row[self::colUserName()] . "_" . $row[self::colPasswordSalt()]);
        } else {
            $password = $row[self::colPassword()];
        }

        return $password;
    }
    
    public function addNewCluster($clusterName, $ip, $userName, $password, $ociRelease, $active, $protocol='', $port='') {
        $password_salt = sha1(uniqid(bin2hex(openssl_random_pseudo_bytes(16)), true));
        $password = encryptString($password, $userName . "_" . $password_salt);
        
        $query  = "insert into ". self::serverTable();
        $query  .= "(". self::colActive() . "," .
                    self::colClusterName() . "," .
                    self::colIP() . "," .
                    self::colUserName() . "," .
                    self::colPassword() . "," .
                    self::colPasswordSalt() . "," .
                    self::colOciRelease() . "," .
                    self::colProtocol() . "," .
                    self::colPort() . "," .
                    "instance, " .
                    "type" .
            ")";
        
         $query  .= "VALUES ('".
             $active ."','".
             $clusterName ."','".
            $ip ."','".
             $userName ."','".
             $password ."','".
             $password_salt ."','".
             $ociRelease ."','".
             $protocol ."','".
             $port ."','".
             $this->instance. "','".
             $this->type .
            "')";
             
        $this->db->query($query);     
    }
    
    
    public function addNewClusterXspServer($clusterName, $ip, $active, $protocol='', $port='') {
        
        $query  = "insert into ". self::serverTable();
        $query  .= "(". self::colActive() . "," .
            self::colClusterName() . "," .
            self::colIP() . "," .
            self::colProtocol() . "," .
            self::colPort() . "," .
            "instance, " .
            "type" .
            ")";
            
            $query  .= "VALUES ('".
                $active ."','".
                $clusterName ."','".
                $ip ."','".
                $protocol ."','".
                $port ."','".
                $this->instance. "','".
                $this->type .
                "')";
                
                $this->db->query($query);
    }
    
    /**
     * @return string
     */
    public function getID() {
        return $this->id;
    }
    
    public function getclusterName() {
        return $this->clusterName;
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getInstance() {
        return $this->instance;
    }

    /**
     * @return string
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * @return string
     */
    public function getIP() {
        return $this->ip;
    }

    /**
     * @return string
     */
    public function getUserName() {
        return $this->userName;
    }

    /**
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getOCIRelease() {
        return $this->version;
    }

    /**
     * @return string
     */
    public function getProtocol() {
        return $this->protocol;
    }
    
    public function getPort() {
        return $this->port;
    }
    

    /**
     * @return string
     */
    public function getLabel() {
        return $this->label;
    }
    
}
