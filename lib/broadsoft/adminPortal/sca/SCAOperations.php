<?php

class SCAOperations {

    /*
     * This method adds a device to shared call appearance of a user
     */
    function addDeviceToUserAsSCA($userId, $deviceName, $linePort, $systemLinePortFlag, $scaUtilityData) {
        global $sessionid, $client;
        $allGroups = Array();
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "UserSharedCallAppearanceAddEndpointRequest14sp2");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= "<accessDeviceEndpoint>";
            $xmlinput .= "<accessDevice>";
                $xmlinput .= "<deviceLevel>Group</deviceLevel>";
                $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
            $xmlinput .= "</accessDevice>";
            $xmlinput .= "<linePort>" . $linePort . "</linePort>";
            if($systemLinePortFlag == "true"){
                if($scaUtilityData !=""){
                    $xmlinput .= "<portNumber>" . $scaUtilityData . "</portNumber>";
                }
                
            }
        $xmlinput .= "</accessDeviceEndpoint>";
        $xmlinput .= "<isActive>true</isActive><allowOrigination>true</allowOrigination><allowTermination>true</allowTermination>";
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array(
            "in0" => $xmlinput
        ));
       // print_r($response); echo"response";
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        $errMsg = readErrorXmlGenuine($xml);
        if ($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        } else {
            $returnResponse["Success"] = $xml;
        }
        return $returnResponse;
    }
    
    
    /*
     * This method deletes a list of devices configured as Shared Appearance for the user
     */
    function deleteDevicesFromSCA($userId, $deviceName, $linePort) {
        global $sessionid, $client;
        $allGroups = Array();
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "UserSharedCallAppearanceDeleteEndpointListRequest14");
        $xmlinput .= "<userId>" . $userId . "</userId>";
            $xmlinput .= "<accessDeviceEndpoint>";
            $xmlinput .= "<accessDevice>";
            $xmlinput .= "<deviceLevel>Group</deviceLevel>";
            $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
            $xmlinput .= "</accessDevice>";
            $xmlinput .= "<linePort>" . $linePort . "</linePort>";
            $xmlinput .= "</accessDeviceEndpoint>";
        
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array(
            "in0" => $xmlinput
        ));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //echo "<pre>"; print_r($xml); die;
        $errMsg = readErrorXmlGenuine($xml);
        if ($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        } else {
            $returnResponse["Success"] = $xml;
        }
        return $returnResponse;
    }
}

?>
