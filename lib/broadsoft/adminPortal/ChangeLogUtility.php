<?php
/**
 * Created by Sollogics.
 * Date: 05/15/2018
 */
 
class ChangeLogUtility
{		
        public  $db = "";
        public  $date = "";
        public  $serviceId = "";
        public  $enterpriseId = "";
        public  $groupId = "";
        public  $loggedInUserName = "";
        public  $modTableName = "";
        public  $changesArr = array();
        public  $entityName = "";
        public  $module = "";
        public  $field = "";
        public  $deviceName = "";
        public  $deviceType = "";
        private $lastId = "";
        

        public function __construct($serviceId, $groupId, $loggedInUserName, $enterpriseId='') {  
//          global $db;
//          $this->db = $db;
            $this->db = $this->expressProvLogsDBConnection();
            $this->date = date("Y-m-d H:i:s");            
            $this->serviceId         = $serviceId;
            $this->groupId           = $groupId;
            $this->loggedInUserName  = $loggedInUserName;
            $this->enterpriseId      = trim($_SESSION["sp"]);
            if($enterpriseId!=""){
                $this->enterpriseId  = $enterpriseId;
            }
        }
        
        public function expressProvLogsDBConnection() {
            require_once("/var/www/lib/broadsoft/adminPortal/ExpressProvLogsDB.php");
            $expProvDB = new ExpressProvLogsDB();
            return $expProvDB->expressProvLogsDb;
        }
        
	public function changeLog() {
                
	        $insValues = array($this->loggedInUserName, $this->date, $this->module, $this->enterpriseId, $this->groupId, addslashes($this->entityName));
                //Code added @ 22 May 2019
                $columnNames        = "";
                $columnValForPPStmt = "";
                $columnVal          = array();
                if(!empty($_SESSION['changeLogAddlnlParamArr'])){
                    $changeLogAddlnlParamArr = $_SESSION['changeLogAddlnlParamArr'];
                    $columnNames             = $changeLogAddlnlParamArr['coulmnNames'];
                    $columnValForPPStmt      = $changeLogAddlnlParamArr['columnValForPPStmt'];
                    $columnVal               = $changeLogAddlnlParamArr['columnVal'];
                }       
                if(!empty($columnVal)){
                    $insValues = array_merge($insValues, $columnVal);
                }
                //End code
                
                $insert_sql   = "INSERT INTO changeLog (userName, date, module, enterpriseId, groupId, entityName $columnNames)" . "VALUES " . "(?, ?, ?, ?, ?, ? $columnValForPPStmt)";
                $insert_stmt  = $this->db->prepare($insert_sql);
                $insert_stmt->execute($insValues);
                $this->lastId = $this->db->lastInsertId();            
		/*$query        = "INSERT into changeLog (userName, date, module, groupId, entityName)";
		$query       .= " VALUES ('" . $this->loggedInUserName. "', '" . $this->date . "', '" . $this->module. "' , '" . $this->groupId. "', '" . addslashes($this->entityName) . "')";
		$sth          = $this->db->query($query);
		$lastId       = $this->db->lastInsertId();
		$this->lastId = $lastId;*/
		
		return $this->lastId;
	}
	public function tableModChanges() {
		$changeString = "";
		foreach ( $this->changesArr as $key => $value )
                {                           
                        $insValues    = array($this->lastId, $this->serviceId, addslashes ( $this->entityName ), $key, addslashes($value ["oldValue"]), addslashes($value ["newValue"]));
                        
                        $insert_sql   = "INSERT into " . $this->modTableName . " (id, serviceId, entityName, field, oldValue, newValue)";
			$insert_sql  .= " VALUES (?,?,?,?,?,?)";
                        $insert_stmt  = $this->db->prepare($insert_sql);
                        $insert_stmt->execute($insValues);
                        $changeString = "<li>" . $this->entityName . " has changed from " . $value ["oldValue"] . " to " . $value ["newValue"] . ".</li>";
                        
			/*$insert = "INSERT into " . $this->modTableName . " (id, serviceId, entityName, field, oldValue, newValue)";
			$values = " VALUES ('" . $this->lastId . "', '" . $this->serviceId . "', '" . addslashes ( $this->entityName ) . "', '" . $key . "', '" . addslashes ( $value ["oldValue"] ) . "', '" . addslashes ( $value ["newValue"] ) . "')";
			$query = $insert . $values;
			$sth = $this->db->query ( $query );
			$changeString = "<li>" . $this->entityName . " has changed from " . $value ["oldValue"] . " to " . $value ["newValue"] . ".</li>";*/
		}
		return $changeString;
	}
	
	public function tableUserCustomTagModChanges() {
	    $changeString = "";
	    foreach ( $this->changesArr as $key => $value )
	    {
	        $insValues    = array($this->lastId, $this->serviceId, addslashes ( $this->entityName ), $this->deviceType, $this->deviceName, $key, addslashes($value ["oldValue"]), addslashes($value ["newValue"]));
	        
	        $insert_sql   = "INSERT into " . $this->modTableName . " (id, serviceId, entityName, deviceType, deviceName, tagName, oldValue, newValue)";
	        $insert_sql  .= " VALUES (?,?,?,?,?,?,?,?)";
	        $insert_stmt  = $this->db->prepare($insert_sql);
	        $insert_stmt->execute($insValues);
	        $changeString = "<li>" . $this->entityName . " has changed from " . $value ["oldValue"] . " to " . $value ["newValue"] . ".</li>";
	        
	    }
	    return $changeString;
	}
	public function tableDelChanges($userId) {
		$changeString = "";
                
                $insValues    = array($this->lastId, $this->serviceId, $this->entityName);                        
		$insert_sql   = "INSERT into " . $this->modTableName . " (id, serviceId, entityName)";
		$insert_sql  .= " VALUES (?,?,?)";
                $insert_stmt  = $this->db->prepare($insert_sql);
                $insert_stmt->execute($insValues);
		
		$changeString = "<li>".$userId." has deleted</li>";
		return $changeString;
	}
	
	
	/* function used for add record into tables */
	public function tableAddChanges($data) {
		
	   global $db;	 
	   $changeString = "";

           //get the array for all keys
	   $valuesArr       = array();
	   $valuesPrPrArr   = array();
	   
	   $columnKeysArray = array_keys($data);
	   $columnKeysStr   = implode("`,`", $columnKeysArray);
	   
	   $i = 0;
	   foreach($data as $key=>$val)
           {
                    $valuesPrPrArr[$i] = "?";
                    $valuesArr[$i]     = $val;
                    $i++;
	   }
           
            $sql  = "INSERT INTO " . $this->modTableName;
            $sql .= "(`".$columnKeysStr."`) ";
            $sql .= "VALUES(".implode(",", $valuesPrPrArr).")";
            $insert_stmt  = $this->db->prepare($sql);
            $insert_stmt->execute($valuesArr);
            $changeString   = "<li>Record Inserted Successfully</li>";
            /*$ins = $db->query($sql);/*
            if($ins > 0){
                $changeString   = "<li>Record Inserted Successfully</li>";
            }*/       
            return $changeString;
	}
	
	public function changeLogModifyUtility($module, $entity, $tabelName) {
		$this->module = $module;
		$this->entityName = $entity;
		$this->modTableName = $tabelName;
		/* Store data on changeLog table */
		$lastId = $this->changeLog();		
		/* Store data on modify table. */
		if($lastId) {
			$this->tableModChanges();
		}
	}
	
	public function changeLogAddUtility($module, $entity, $tabelName, $changeLog) {
        
		/* Store data on changeLog table */
		$this->module = $module;
		$this->entityName = $entity;
		$this->modTableName = $tabelName;
		$lastId = $this->changeLog();
		$this->lastId = $lastId;
		/* Store data on modify table. */
		
		if($lastId) {
			$lastIdArr["id"] = $lastId;
			$changeLog = array_merge($lastIdArr, $changeLog);
			return $addResponse = $this->tableAddChanges($changeLog);
			
		}
	}

	public function changeLogFailOverServerAddUtility($module, $entity, $tabelName, $changeLog) {
	    
	    /* Store data on changeLog table */
	    $this->module = $module;
	    $this->entityName = $entity;
	    $this->modTableName = $tabelName;
	    $lastId = $this->changeLog();
	    $this->lastId = $lastId;
	    /* Store data on modify table. */
	    
	    if($lastId) {
	        return $addResponse = $this->tableAddFailOverServerChanges($changeLog);
	    }
	}
	
	public function tableAddFailOverServerChanges($logData) {
	    
	    global $db;
	    
	        $changeString = ""; 	        
	        foreach ( $logData as $key => $value )
	        {
	            $insValues    = array($this->lastId, $value['connection_status'], $value['status_message'], $value['server_detail'] );
	            
	            $insert_sql   = "INSERT into " . $this->modTableName . " (id, connection_status, status_message, server_detail)";
	            $insert_sql  .= " VALUES (?,?,?,?)";
	            $insert_stmt  = $this->db->prepare($insert_sql);
	            $insert_stmt->execute($insValues);
	            $changeString = "<li> Server Record Inserted Successfully</li>";
	            
	        }
	        return $changeString;
	    	    
	}
	
	public function createChangesArray($module, $oldValue, $newValue) {
	    $this->changesArr[$module]["oldValue"] = $oldValue;
	    $this->changesArr[$module]["newValue"] = $newValue;
	}
	
	/* Used for when post array has array*/
	public function createChangesArrayFromArray($postArray, $sessionDeviceInfo, $formDataArray) {
	   
	    foreach($postArray as $key => $value) {
	        if(isset($sessionDeviceInfo[$key]) && trim($value) <> $sessionDeviceInfo[$key]) {
	            if(isset($formDataArray[$key])) {
	                $this->createChangesArray($formDataArray[$key], $oldValue = $sessionDeviceInfo[$key], $newValue = trim($value));
	            }
	        }
	    }
	}
	
	public function changeLogDeleteUtility($module, $entity, $tabelName, $changeLog) {
		
		$this->module = $module;
		$this->entityName = $entity;
		$this->modTableName = $tabelName;
		/* Store data on changeLog table */
		$lastId = $this->changeLog();		
		/* Store data on modify table. */
		if($lastId) {
			$this->tableDelChanges();
		}
	}
        
	
	public function counterPathChangesToTable($tabelName, $changeLog, $lastId) {
	    $this->modTableName = $tabelName;
	    
	    /* Store soft phone details table. */
	        $lastIdArr["id"] = $lastId;
	        $changeLog = array_merge($lastIdArr, $changeLog);
	        return $addResponse = $this->addCounterPathChangesToTable($changeLog);
	}
	
	
	public function addCounterPathChangesToTable($changeLog) {
	    $changeString = "";
    	  $insValues =  array(
    	      $changeLog['id'],
    	      $changeLog['counterPathAcctGroupname'],
    	      $changeLog['counterPathAcctProfilename'],
    	      $changeLog['counterPathAcctUsername'],
    	      $changeLog['counterPathAcctEmailAddr'],
    	      $changeLog['counterPathAcctAuthName'],
    	      $changeLog['counterPathAcctSipDomain'],
    	      $changeLog['counterPathAcctSipProxy'],
    	      $changeLog['counterPathAcctDeviceType'],
    	      $changeLog['counterPathAcctDeviceName'],
    	      $changeLog['counterPathAcctLinePortDomain']
    	  );
	  
	        $insert_sql   = "INSERT into " . $this->modTableName . " (id, counterPathAcctGroupname, counterPathAcctProfilename, counterPathAcctUsername,counterPathAcctEmailAddr, counterPathAcctAuthName, counterPathAcctSipDomain, counterPathAcctSipProxy, counterPathAcctDeviceType, counterPathAcctDeviceName, counterPathAcctLinePortDomain)";
	        $insert_sql  .= " VALUES (?,?,?,?,?,?,?,?,?,?,?)";
	        $insert_stmt  = $this->db->prepare($insert_sql);
	        $insert_stmt->execute($insValues);
	        $changeString = "<li>" . $this->entityName . " has added. </li>";
	        
	    return $changeString;
	}
	
	public function softPhoneChangesLog($tabelName, $changeLog, $lastId) {
	    $this->modTableName = $tabelName;
	        $lastIdArr["id"] = $lastId;
	        $changeLog = array_merge($lastIdArr, $changeLog);
	        return $addResponse = $this->softPhoneChangesToTable($changeLog);
	}
	
	
	public function softPhoneChangesToTable($changeLog) {
	    $changeString = "";
	    $insValues =  array(
	        $changeLog['id'],
	        $changeLog['softPhoneAcctDeviceType'],
	        $changeLog['softPhoneAcctDeviceName'],
	        $changeLog['softPhoneAcctLinePortDomain']
	    );
	    
	    $insert_sql   = "INSERT into " . $this->modTableName . " (id, softPhoneAcctDeviceType, softPhoneAcctDeviceName, softPhoneAcctLinePortDomain)";
	    $insert_sql  .= " VALUES (?,?,?,?)";
	    $insert_stmt  = $this->db->prepare($insert_sql);
	    $insert_stmt->execute($insValues);
	    $changeString = "<li>" . $this->entityName . " has added. </li>";
	    
	    return $changeString;
	}
        
        //function named changeLogDelUtility added @ 30 Jan 2019 due to column entityName not exists in some table 
        public function changeLogDelUtility($module, $entity, $tabelName) {
		
		$this->module = $module;
		$this->entityName = $entity;
		$this->modTableName = $tabelName;
		/* Store data on changeLog table */
		$lastId = $this->changeLog();	
		/* Store data on modify table. */
		if($lastId) {
			$this->tableDeleteChanges();
		}
	}
        
        //function named changeLogDelUtility added @ 30 Jan 2019 due to column entityName not exists in some table 
        public function tableDeleteChanges() {
		$changeString = "";
                
                $insValues    = array($this->lastId, $this->serviceId);                        
		$insert_sql   = "INSERT into " . $this->modTableName . " (id, serviceId)";
		$insert_sql  .= " VALUES (?,?)";
                $insert_stmt  = $this->db->prepare($insert_sql);
                $insert_stmt->execute($insValues);
		
		$changeString = "<li>".$userId." has deleted</li>";
		return $changeString;
	}
}