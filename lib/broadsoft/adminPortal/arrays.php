<?php
	$ccPolicies = array("Circular", "Regular", "Simultaneous", "Uniform", "Weighted");
//	$tF = array("true", "false");
	$escapeDigits = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "#", "*");

	$callForwardsFields = array(
		"cfa" => "Call Forwarding Always",
		"cfb" => "Call Forwarding Busy",
		"cfn" => "Call Forwarding No Answer",
		"cfr" => "Call Forwarding Not Reachable"
	);

	$restrictedNumbers = array(
		"+" => "Any number starting with +",
		"011" => "All 011 dialing",
		"1010" => "Casual dialing",
		"1242" => "Bahamas forwarding",
		"1246" => "Barbados forwarding",
		"1264" => "Anguilla forwarding",
		"1268" => "Antigua/Barbuda forwarding",
		"1284" => "British Virgin Islands forwarding",
		"1340" => "US Virgin Islands forwarding",
		"1345" => "Cayman Islands forwarding",
		"1441" => "Bermuda forwarding",
		"1473" => "Grenada forwarding",
		"1649" => "Turks & Caicos Islands forwarding",
		"1664" => "Montserrat forwarding",
		"1670" => "Northern Mariana Islands forwarding",
		"1671" => "Guam forwarding",
		"1684" => "American Samoa forwarding",
		"1758" => "St. Lucia forwarding",
		"1767" => "Dominica forwarding",
		"1784" => "St. Vincent & Grenadines forwarding",
		"1787" => "Puerto Rico forwarding",
		"1809" => "Dominican Republic forwarding",
		"1829" => "Dominican Republic forwarding",
		"1849" => "Dominican Republic forwarding",
		"1868" => "Trinidad & Tobago forwarding",
		"1869" => "St. Kitts & Nevis forwarding",
		"1876" => "Jamaica forwarding",
		"1900" => "1-900 dialing",
		"1939" => "Puerto Rico forwarding",
		"1976" => "1-976 dialing"
	);
	
	$vmArray = array("isActive", "alwaysRedirectToVoiceMail", "busyRedirectToVoiceMail", "noAnswerRedirectToVoiceMail", "processing", "sendCarbonCopyVoiceMessage", "voiceMessageCarbonCopyEmailAddress", "transferOnZeroToPhoneNumber", "transferPhoneNumber", "mailBoxLimit");
	
	$formDataArr = array(
	    "aaId" => "Auto Attendant Id",
	    "aaType" => "Auto Attendant Type",
	    "aaName" => "Auto Attendant name",
	    "callingLineIdLastName" => "Calling Line Id Last Name",
	    "callingLineIdFirstName" => "Calling Line Id First Name",
	    "department" => "department",
	    "eVsupport" => "Enable Video Support",
	    "timeZone" => "timeZone",
	    "businessHoursSchedule" => "Business Hours Schedule",
	    "holidaySchedule" => "Holiday Schedule",
	    "extensionDialing" => "Scope of Extension Dialing",
	    "nameDialing" => "Scope of Name Dialing",
	    "nameDialingEntries" => "Name Dialing Entries",
	    "phoneNumberAdd" => "Phone Number",
	    "extensionAdd" => "Extension",
	    "firstDigitTimeoutSeconds" => "Transfer to operator after",
	);
	
	$ocpArray = array(
	    "Y" => "Allow",
	    "N" => "Disallow",
	    "A" => "Authorization Code Required",
	    "T1" => "Transfer To First Transfer Number",
	    "T2" => "Transfer To Second Transfer Number",
	    "T3" => "Transfer To Third Transfer Number"
	);
	
	
?>
