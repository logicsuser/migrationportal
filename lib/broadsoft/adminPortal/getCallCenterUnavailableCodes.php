<?php
	//check if service provider is an enterprise before determining which commands to run
	if ($_SESSION["enterprise"] == "true")
	{
		$xmlinput = xmlHeader($sessionid, "EnterpriseCallCenterAgentUnavailableCodeGetListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "GroupCallCenterAgentUnavailableCodeGetListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	}
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	if (isset($xml->command->unavailableCodesTable->row))
	{
		foreach ($xml->command->unavailableCodesTable->row as $key => $value)
		{
			$_SESSION["callCenterInfo"]["info"]["codesUnavailable"][$a]["code"] = strval($value->col[1]);
			$_SESSION["callCenterInfo"]["info"]["codesUnavailable"][$a]["description"] = strval($value->col[2]);
			$a++;
		}
	}

	if ($_SESSION["enterprise"] == "true")
	{		
		$xmlinput = xmlHeader($sessionid, "EnterpriseCallCenterAgentUnavailableCodeSettingsGetRequest17sp4");		
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "GroupCallCenterAgentUnavailableCodeSettingsGetRequest17sp4");		
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	}
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$_SESSION["callCenterInfo"]["info"]["enableAgentUnavailableCodes"] = strval($xml->command->enableAgentUnavailableCodes);
	$_SESSION["callCenterInfo"]["info"]["defaultAgentUnavailableCodeOnDND"] = strval($xml->command->defaultAgentUnavailableCodeOnDND);
	$_SESSION["callCenterInfo"]["info"]["defaultAgentUnavailableCodeOnPersonalCalls"] = strval($xml->command->defaultAgentUnavailableCodeOnPersonalCalls);
	$_SESSION["callCenterInfo"]["info"]["defaultAgentUnavailableCodeOnConsecutiveBounces"] = strval($xml->command->defaultAgentUnavailableCodeOnConsecutiveBounces);
	if ($ociVersion == "17")
	{
		$_SESSION["callCenterInfo"]["info"]["defaultAgentUnavailableCodeOnNotReachable"] = "";
	}
	else
	{
		$_SESSION["callCenterInfo"]["info"]["defaultAgentUnavailableCodeOnNotReachable"] = strval($xml->command->defaultAgentUnavailableCodeOnNotReachable);
	}
	$_SESSION["callCenterInfo"]["info"]["forceUseOfAgentUnavailableCodes"] = strval($xml->command->forceUseOfAgentUnavailableCodes);
	$_SESSION["callCenterInfo"]["info"]["defaultAgentUnavailableCode"] = strval($xml->command->defaultAgentUnavailableCode);
?>
