<?php 

class UserPolycomPhoneServicesOperation{
	
	
    function _construct(){
 	}
	
 	
 	public static function getPolycomPhoneServicesDetails($userId, $deviceName) {
 		global $sessionid, $client;
 		
 		$xmlinput = openBroadsoftHeader($sessionid);
 		$xmlinput .= self::getUserPolycomPhoneServicesGetCommand($userId, $deviceName);
 		$xmlinput .= closeBroadSoftHeader(); 
 		
 		$response = $client->processOCIMessage(array("in0" => $xmlinput));
 		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                		
 		$returnResponse = self::getParsedPolycomPhoneServicesGetResponse($xml->command);
 		
 		return $returnResponse;
 	}
 	
    public static function  getUserPolycomPhoneServicesGetCommand($userId, $deviceName) {
		$xmlinput  = openCommand( "UserPolycomPhoneServicesGetRequest");
		$xmlinput .= "<userId>" . htmlspecialchars($userId) . "</userId>";
                //$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>"; //Code commented @ 19 Sep 2018
                $xmlinput .= "<accessDevice>
                              <deviceLevel>Group</deviceLevel>
                              <deviceName>".$deviceName."</deviceName>
                              </accessDevice>";
		$xmlinput .= closeCommand();
		return $xmlinput;
	}
	
	
	public static function getParsedPolycomPhoneServicesGetResponse($commandResponse) {
		
		$returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
        $returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserPolycomPhoneServicesOperation::parsePolycomPhoneServicesGetCommandResponse ( $commandResponse );
        
		return $returnResponse;
	}
	
	private static function parsePolycomPhoneServicesGetCommandResponse($commandResponse) {
		
		
		$polycomService["integration"] = strval($commandResponse->integratePhoneDirectoryWithBroadWorks);
		$polycomService["ccd"] = strval($commandResponse->groupCustomContactDirectory);
		return $polycomService;
	}         
}
?>