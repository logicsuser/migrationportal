<?php 

class UserServiceOperation{
	
	public $userId;
	
	
	const MUSICONHOLD = "Music On Hold";
	const MUSICONHOLDUSER = "Music On Hold User";
	const ALTERNATENUMBERS = "Alternate Numbers";
	const ANONYMOUSCALLREJECTION = "Anonymous Call Rejection";
	const ATTENDANTCONSOLE = "Attendant Console";
	const AUTHENTICATION = "Authentication";
	const AUTOMATICCALLBACK = "Automatic Callback";
	const HOLDRETRIEVE = "Automatic Hold/Retrieve";
	const BARGEINEXEMPT = "Barge-in Exempt";
	const BASICCALLLOGS = "Basic Call Logs";
	const BBCM = "BroadTouch Business Communicator Mobile";
	const BBCMAUDIO = "BroadTouch Business Communicator Mobile - Audio";
	const BROADWORKSANYWHERE = "BroadWorks Anywhere";
	const BROASWORKSMOBILITY = "BroadWorks Mobility";
	const busyLampField = "Busy Lamp Field";
	const CALLCENTERBASIC = "Call Center - Basic";
	const CALLCENTERPREMIUM = "Call Center - Premium";
	const CALLCENTERMONITORING = "Call Center Monitoring";
	const CALLCENTERSTANDARD = "Call Center - Standard";
	const cfa = "Call Forwarding Always";
	const cfb = "Call Forwarding Busy";
	const cfn = "Call Forwarding No Answer";
	const cfr = "Call Forwarding Not Reachable";
	const CALLFORWARDINGSELECTIVE = "Call Forwarding Selective";
	const CALLMEKNOW = "Call Me Now";
	const CALLNOTIFY = "Call Notify";
	const CALLRECORDING = "Call Recording";
	const CALLRETURN = "Call Return";
	const CALLTRANSFER = "Call Transfer";
	const CALLWAITING = "Call Waiting";
	const CLIDBLOCKINGOVERRIDE = "Calling Line ID Blocking Override";
	const CLIDDELIVERYBLOCKING = "Calling Line ID Delivery Blocking";
	const CALLINGNAMEDELIVERY = "Calling Name Delivery";
	const CALLINGNAMERETRIVAL = "Calling Name Retrieval";
	const CALLINGNUMBERDELIVERY = "Calling Number Delivery";
	const CALLINGPARTYCATEGORY = "Calling Party Category";
	const CHARGENUMBER = "Charge Number";
	const CLASSMARK = "Classmark";
	const CLIENTCALLCONTROL = "Client Call Control";
	const CLIENTLICENSE17 = "Client License 17";
	const CLIENTLICENSE18 = "Client License 18";
	const CLIENTLICENSE19 = "Client License 19";
	const CLIENTLICENSE3 = "Client License 3";
	const CLIENTLICENSE4 = "Client License 4";
	const COMMPILOTCALLMANAGER = "CommPilot Call Manager";
	const COMMPILOTEXPRESS = "CommPilot Express";
	const BARRINGUSERCTRL = "Communication Barring User-Control";
	const CLIP = "Connected Line Identification Presentation";
	const CLIR = "Connected Line Identification Restriction";
	const CUSTOMRIGHTBACK = "Custom Ringback User";
	const CUSTOMCALLWAITING = "Custom Ringback User - Call Waiting";
	const CUSTOMRINGBACKUSERVIDEO = "Custom Ringback User - Video";
	const CUSTOMERORIGINATEDTRACE = "Customer Originated Trace";
	const DIRECTEDCALLPICKUP = "Directed Call Pickup";
	const DIRECTEDCALLPICKUPBARGEIN = "Directed Call Pickup with Barge-in";
	const DIVERSIONINHIBITOR = "Diversion Inhibitor";
	const DND = "Do Not Disturb";
	const ENHANCEDCALLLOGS = "Enhanced Call Logs";
	const EXTERNALCLID = "External Calling Line ID Delivery";
	const EXTERNALCUSTOMRINGBACK = "External Custom Ringback";
	const FAXMESSAGING = "Fax Messaging";
	const FLASHCALLHOLD = "Flash Call Hold";
	const GROUPNIGHTFORWARDING = "Group Night Forwarding";
	const HOTELINGGUEST = "Hoteling Guest";
	const HOTELINGHOST = "Hoteling Host";
	const INCALLSERVICEACTIVATION = "In-Call Service Activation";
	const INTEGRATEDIMP = "Integrated IMP";
	const INTERCEPTUSER = "Intercept User";
	const INTERNALCLID = "Internal Calling Line ID Delivery";
	const LASTNUMBERREDIAL = "Last Number Redial";
	const LOCATIONBASEDCALLINGRESTRICTIONS = "Location-Based Calling Restrictions";
	const MWIDELIVERYMOBILEENDPOINT = "MWI Delivery to Mobile Endpoint";
	const MALICIOUSCALLTRACE = "Malicious Call Trace";
	const MULTIPLECALLARRANGEMENT = "Multiple Call Arrangement";
	const NWAYCALL = "N-Way Call";
	const OUTLOOKINTEGRATION = "Outlook Integration";
	const PHYSICALLOCATION = "Physical Location";
	const POLYCOMOHONESERVICE = "Polycom Phone Services";
	const PREALERTINGANNOUNCEMENT = "Pre-alerting Announcement";
	const PREFERREDCARRIERUSER = "Preferred Carrier User";
	const PREPAID = "Prepaid";
	const PRIORITYALERT = "Priority Alert";
	const PRIVACY = "Privacy";
	const PUSHTOTALK = "Push to Talk";
	const REMOTEOFFICE = "Remote Office";
	const SELECTIVECALLACCEPTANCE = "Selective Call Acceptance";
	const SCR = "Selective Call Rejection";
	const SEQUENTIALRING = "Sequential Ring";
	const SERVICESCRIPTSUSER = "Service Scripts User";
	const SCA = "Shared Call Appearance";
	const SCA10 = "Shared Call Appearance 10";
	const SCA20 = "Shared Call Appearance 20";
	const SCA15 = "Shared Call Appearance 15";
	const SCA25 = "Shared Call Appearance 25";
	const SCA30 = "Shared Call Appearance 30";
	const SCA35 = "Shared Call Appearance 35";
	const SCA5 = "Shared Call Appearance 5";
	const SIMULTANEOUSRINGPERSONAL = "Simultaneous Ring Personal";
	const SPEEDDIAL100 = "Speed Dial 100";
	const SPEEDDIAL8 = "Speed Dial 8";
	const THIRDPARTYMWICONTROL = "Third-Party MWI Control";
	const THIRDPARTYVOICEMAILSUPPORT = "Third-Party Voice Mail Support";
	const THREEWAYCALL = "Three-Way Call";
	const TWOSTAGEDIALING = "Two-Stage Dialing";
	const VIDEOADDON = "Video Add-On";
	const VIDEOONHOLDUSER = "Video On Hold User";
	const VIRTUALONNETExt = "Virtual On-Net Enterprise Extensions";
	const voiceMessagingUser = "Voice Messaging User";
	const VOICEMESSAGINGUSERVIDEO = "Voice Messaging User - Video";
	const VOICEPORTALCALLING = "Voice Portal Calling";
	const ZONECALLINGRESTRICTIONS = "Zone Calling Restrictions";
	
	public function _construct(){
	}
	
	public STATIC function getUserServiceCommand($userId) {
		
		$xmlinput = openCommand( "UserAssignedServicesGetListRequest");
		$xmlinput .= "<userId>" . $userId. "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	public static function getParsedServiceGetResponse($commandResponse) {
		
		$returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
		$returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserServiceOperation::parseServiceGetCommandResponse( $commandResponse );
		
		return $returnResponse;
	}
	
	private static function parseServiceGetCommandResponse($commandResponse) {
		
		$u = 0;
		$serviceResponse["user"] = array();
		$serviceResponse["group"] = array();
		
		if(count($commandResponse->userServiceEntry) > 0){
			foreach($commandResponse->userServiceEntry as $key=>$val){
				$serviceResponse["user"][$u] = strval($val->serviceName);
				$u++;
			}
		}
		
		$g = 0;
		if(count($commandResponse->groupServiceEntry) > 0){
			foreach($commandResponse->groupServiceEntry as $key=>$val){
				$serviceResponse["group"][$g] = strval($val->serviceName);
				$g++;
			}
		}
		
		return $serviceResponse;
	}


}
?>