<?php 

class GroupAccessDeviceGetOperation {
    function _construct(){
 	}

 	public static function getGroupAccessDeviceDetails($sp,$groupId,$deviceName) {
 	     global $sessionid, $client;
 		
 		$xmlinput = openBroadsoftHeader($sessionid);
 		$xmlinput .= self::getGroupAccessDeviceGetCommand($sp, $groupId, $deviceName);
 		$xmlinput .= closeBroadSoftHeader(); 
 		
 		$response = $client->processOCIMessage(array("in0" => $xmlinput));
 		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
 		
 		$returnResponse = self::getParsedGroupAccessDeviceGetResponse($xml->command);
 		
 		return $returnResponse;
 	}
 	
 	public static function  getGroupAccessDeviceGetCommand($sp, $groupId, $deviceName) {
 		global $ociVersion;
 		
 		$xmlinput  = openCommand( "GroupAccessDeviceGetRequest18sp1");
 		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($sp) . "</serviceProviderId>";
 		$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
		$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
 	}
	
 	public static function getParsedGroupAccessDeviceGetResponse($commandResponse) {
		
	   $returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
	   $returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: GroupAccessDeviceGetOperation::parseGroupAccessDeviceList($commandResponse);
	   return $returnResponse;
	}
	
	private static function parseGroupAccessDeviceList($commandResponse) {
		
		$groupAccessResponse["deviceType"]= "";
		$groupAccessResponse["macAddress"]= "";
		
		if(isset($commandResponse->deviceType)){
			$groupAccessResponse["deviceType"] = strval($commandResponse->deviceType);
		}
		if(isset($commandResponse->macAddress)){
			$groupAccessResponse["macAddress"] = strval($commandResponse->macAddress);
		}
		return $groupAccessResponse;
	}
}
?>