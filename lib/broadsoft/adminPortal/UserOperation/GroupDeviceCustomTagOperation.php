<?php 

class GroupDeviceCustomTagOperation {
    function _construct(){
 	}

 	function getGroupAccessCustomTagUserListDetails($sp,$groupId,$deviceName) {
 	    global $sessionid, $client;
 		
 		$xmlinput .= openBroadsoftHeader($sessionid);
 		$xmlinput .= self::getGroupAccessCustomTagGetUserListRequestCommand($sp,$groupId,$deviceName);
 		$xmlinput .= closeBroadSoftHeader(); 
 		
 		$response = $client->processOCIMessage(array("in0" => $xmlinput));
 		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
 		
 		$returnResponse = self::getParsedGroupAccessCustomTagGetUserListRequestGetResponse($xml->command);
 		
 		return $returnResponse;
 	}
 	
 	public static function  getGroupAccessCustomTagGetUserListRequestCommand($sp,$groupId,$deviceName) {
		$xmlinput  = openCommand( "GroupAccessDeviceCustomTagGetListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($sp) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
		$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	public static function getParsedGroupAccessCustomTagGetUserListRequestGetResponse($commandResponse) {
		
			$returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
			$returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: GroupDeviceCustomTagOperation::parseGroupAccessCustomTagGetUserListResponse($commandResponse);
		return $returnResponse;
	}
	
	private static function  parseGroupAccessCustomTagGetUserListResponse($commandResponse) {
		$dnResponse = strval($commandResponse->isActive);
		return $dnResponse;
	}
}
?>