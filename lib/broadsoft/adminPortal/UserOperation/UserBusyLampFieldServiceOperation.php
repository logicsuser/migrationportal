<?php 

class UserBusyLampFieldServiceOperation{
	
	
    function _construct(){
 	}
	
 	
 	function getBusyLampFieldServiceDetails($userId) {
 		global $sessionid, $client;
 		
 		$xmlinput = openBroadsoftHeader($sessionid);
 		$xmlinput .= self::getUserBusyLampFieldServiceGetCommand($userId);
 		$xmlinput .= closeBroadSoftHeader(); 
 		
 		$response = $client->processOCIMessage(array("in0" => $xmlinput));
 		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
 		
 		$returnResponse = self::getParsedBusyLampFieldServiceGetResponse($xml->command);
 		
 		return $returnResponse;
 	}
 	
	public static function  getUserBusyLampFieldServiceGetCommand($userId) {
		$xmlinput = openCommand( "UserBusyLampFieldGetRequest16sp2");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	public static function getParsedBusyLampFieldServiceGetResponse($commandResponse) {
		
        $returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
        $returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserBusyLampFieldServiceOperation::parseBusyLampFieldServiceGetCommandResponse ( $commandResponse );
		return $returnResponse;
	}
	
	private static function  parseBusyLampFieldServiceGetCommandResponse($commandResponse) {
		$blfResponse["enableCallParkNotification"] = strval($commandResponse->enableCallParkNotification);
		$blfResponse["monitoredUserTableCol"] = $commandResponse->monitoredUserTable->colHeading;
		$blfResponse["monitoredUserTableRow"] = $commandResponse->monitoredUserTable->row;
		return $blfResponse;
	}
}
?>