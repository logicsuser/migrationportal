<?php 

class GroupDeviceGetUserOperation {
    function _construct(){
 	}

 	public static function getGroupAccessDeviceUserListDetails($sp,$groupId,$deviceName) {
 	     global $sessionid, $client;
 		
 		$xmlinput = openBroadsoftHeader($sessionid);
 		$xmlinput .= self::getGroupAccessDeviceUserListGetCommand($sp,$groupId,$deviceName);
 		$xmlinput .= closeBroadSoftHeader(); 
 		
 		$response = $client->processOCIMessage(array("in0" => $xmlinput));
 		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
 		
 		$returnResponse = self::getParsedGroupAccessDeviceUserListGetResponse($xml->command);
 		
 		return $returnResponse;
 	}
 	
 	public static function  getGroupAccessDeviceUserListGetCommand($sp, $groupId, $deviceName) {
		$xmlinput  = openCommand( "GroupAccessDeviceGetUserListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($sp) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
		$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
 	}
	
 	public static function getParsedGroupAccessDeviceUserListGetResponse($commandResponse) {
		
	   $returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
	   $returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: GroupDeviceGetUserOperation::parseGroupAccessDeviceUserList($commandResponse);
	   return $returnResponse;
	}
	
	private static function  parseGroupAccessDeviceUserList($commandResponse) {
		$deviceArray["deviceType"] = strval($commandResponse->deviceType);
		$deviceArray["protocol"] = strval($commandResponse->protocol);
		$deviceArray["numberOfPorts"] = strval($commandResponse->numberOfPorts->unlimited);
		$deviceArray["numberOfAssignedPorts"] = strval($commandResponse->numberOfAssignedPorts);
		$deviceArray["status"] = strval($commandResponse->status);
		$deviceArray["configurationMode"] = strval($commandResponse->configurationMode);
		$deviceArray["transportProtocol"] = strval($commandResponse->transportProtocol);
		$deviceArray["useCustomUserNamePassword"] = strval($commandResponse->useCustomUserNamePassword);
		$deviceArray["userName"] = strval($commandResponse->userName);
		$deviceArray["row"] = $commandResponse->deviceUserTable->row;
		return $deviceArray;
	}
}
?>