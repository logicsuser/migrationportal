<?php 

class UserEndPointDeviceNameOperation{
	
	
    function _construct(){
 	}
	
 	
 	function getUserEndPointDeviceNameDetails($userId) {
 		global $sessionid, $client;
 		
 		$xmlinput = openBroadsoftHeader($sessionid, "UserGetRequest19");
 		$xmlinput .= self::getUserEndPointDeviceNameGetCommand($userId);
 		$xmlinput .= closeBroadSoftHeader(); 
 		
 		$response = $client->processOCIMessage(array("in0" => $xmlinput));
 		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
 		
 		$returnResponse = self::getParsedEndPointDeviceNameGetResponse($xml->command);
 		
 		return $returnResponse;
 	}
 	
 	public static function  getUserEndPointDeviceNameGetCommand($userId) {
 		global $ociVersion;
 		
 		if ($ociVersion == "17"){
 			$xmlinput = openCommand( "UserGetRequest17sp4" );
 		}else if ($ociVersion == "20"){
 			$xmlinput = openCommand( "UserGetRequest20" );
 		}else{
 			$xmlinput = openCommand( "UserGetRequest19" );
 		}
 		
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
 	
	public static function getParsedEndPointDeviceNameGetResponse($commandResponse) {
		$returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
		$returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserEndPointDeviceNameOperation::parseEndPointDeviceNameGetCommandResponse( $commandResponse );
		return $returnResponse;
	}
	
	private static function  parseEndPointDeviceNameGetCommandResponse($commandResponse) { 
		$endPointResponse["serviceProviderId"] = strval($commandResponse->serviceProviderId);
		$endPointResponse["groupId"] = strval($commandResponse->groupId);
		$endPointResponse["lastName"] = strval($commandResponse->lastName);
		$endPointResponse["firstName"] = strval($commandResponse->firstName);
		$endPointResponse["callingLineIdLastName"] = strval($commandResponse->callingLineIdLastName);
		$endPointResponse["callingLineIdFirstName"] = strval($commandResponse->callingLineIdFirstName);
		$endPointResponse["hiraganaLastName"] = strval($commandResponse->hiraganaLastName);
		$endPointResponse["hiraganaFirstName"] = strval($commandResponse->hiraganaFirstName);
		$endPointResponse["phoneNumber"] = strval($commandResponse->phoneNumber);
		$endPointResponse["extension"] = strval($commandResponse->extension);
		$endPointResponse["language"] = strval($commandResponse->language);
		$endPointResponse["timeZone"] = strval($commandResponse->timeZone);
		$endPointResponse["timeZoneDisplayName"] = strval($commandResponse->timeZoneDisplayName);
		$endPointResponse["defaultAlias"] = strval($commandResponse->defaultAlias);
		$endPointResponse["emailAddress"] = strval($commandResponse->emailAddress);
		$endPointResponse["countryCode"] = strval($commandResponse->countryCode);
		$endPointResponse["networkClassOfService"] = strval($commandResponse->networkClassOfService);
		$endPointResponse["address"] = $commandResponse->address;

		$endPointResponse["linePort"] = strval($commandResponse->accessDeviceEndpoint->linePort);
		$endPointResponse["staticRegistrationCapable"] = strval($commandResponse->accessDeviceEndpoint->staticRegistrationCapable);
		$endPointResponse["useDomain"] = strval($commandResponse->accessDeviceEndpoint->useDomain);
		
		
		$endPointResponse["portNumber"] = "";
		if(isset($commandResponse->accessDeviceEndpoint->portNumber)){
			$endPointResponse["portNumber"] = strval($commandResponse->accessDeviceEndpoint->portNumber);
		}
		
		$deviceLevel = "";
		$deviceName = "";
		
		if(isset($commandResponse->accessDeviceEndpoint->accessDevice->deviceLevel)){
			$deviceLevel = strval($commandResponse->accessDeviceEndpoint->accessDevice->deviceLevel);
		}
		if(isset($commandResponse->accessDeviceEndpoint->accessDevice->deviceName)){
			$deviceName = strval($commandResponse->accessDeviceEndpoint->accessDevice->deviceName);
		}
		$endPointResponse["deviceLevel"] = $deviceLevel;
		$endPointResponse["deviceName"] = $deviceName;
		return $endPointResponse;
	}
}
?>