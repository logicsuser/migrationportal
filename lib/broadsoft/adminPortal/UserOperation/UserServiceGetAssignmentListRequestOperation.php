<?php 

class UserServiceGetAssignmentListRequestOperation{
	
	
    function _construct(){
 	}
	
 	
 	function getUserServiceGetAssignmentListRequestDetails($userId) {
 		global $sessionid, $client;
 		
 		$xmlinput = openBroadsoftHeader($sessionid, "UserServiceGetAssignmentListRequest");
 		$xmlinput .= self::getUserServiceGetAssignmentListRequestGetCommand($userId);
 		$xmlinput .= closeBroadSoftHeader(); 
 		
 		$response = $client->processOCIMessage(array("in0" => $xmlinput));
 		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
 		
 		$returnResponse = self::getParsedUserServiceGetAssignmentListRequestGetResponse($xml->command);
 		
 		return $returnResponse;
//  		return $xml;
 	}
 	
 	public static function  getUserServiceGetAssignmentListRequestGetCommand($userId) {
		$xmlinput = openCommand( "UserServiceGetAssignmentListRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	public static function getParsedUserServiceGetAssignmentListRequestGetResponse($commandResponse) {
		
		$returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
		$returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserServiceGetAssignmentListRequestOperation::parseUserServiceGetAssignmentListRequestGetCommandResponse( $commandResponse );
		return $returnResponse;
	}
	
	private static function  parseUserServiceGetAssignmentListRequestGetCommandResponse($commandResponse) {
		
		$serviceResponse = array();
		$r = 0;
		$s = 0;
		
		if(isset($commandResponse->servicePacksAssignmentTable->row) && count($commandResponse->servicePacksAssignmentTable->row) > 0){
			foreach($commandResponse->servicePacksAssignmentTable->row as $key=>$val){
				$serviceResponse["servicePacksAssignmentTableRow"][$r] = $commandResponse->servicePacksAssignmentTable->row[$r];
				$r++;
			}
		}
		
		if(isset($commandResponse->userServiceAssignmentTable->row) && count($commandResponse->userServiceAssignmentTable->row) > 0){
			foreach($commandResponse->userServiceAssignmentTable->row as $key=>$val){
				$serviceResponse["userServiceAssignmentTableRow"][$s] = $commandResponse->userServiceAssignmentTableRow->row[$s];
				$s++;
			}
		}
		return $serviceResponse;
	}
}
?>