<?php 

class UserRegistrationOperation{
	
	
    function _construct(){
 	}
	
 	
 	function getRegistrationDetails($userId) {
 		global $sessionid, $client;
 		
 		$xmlinput = openBroadsoftHeader($sessionid, "UserGetRegistrationListRequest");
 		$xmlinput .= self::getUserRegistrationGetCommand($userId);
 		$xmlinput .= closeBroadSoftHeader(); 
 		
 		$response = $client->processOCIMessage(array("in0" => $xmlinput));
 		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
 		
 		$returnResponse = self::getParsedRegistrationGetResponse($xml->command);
 		
 		return $xml;
 	}
 	
	public static function  getUserRegistrationGetCommand($userId) {
		$xmlinput = openCommand( "UserGetRegistrationListRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	public static function getParsedRegistrationGetResponse($commandResponse) {
		
			$returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
			$returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserRegistrationOperation::parseRegistrationGetCommandResponse( $commandResponse );
                        return $returnResponse;
	}
	
	private static function  parseRegistrationGetCommandResponse($commandResponse) {
		$registerResponse["colHeading"] = $commandResponse->registrationTable->colHeading;
		$registerResponse["row"] = $commandResponse->registrationTable->row;
		return $registerResponse;
	}
}
?>