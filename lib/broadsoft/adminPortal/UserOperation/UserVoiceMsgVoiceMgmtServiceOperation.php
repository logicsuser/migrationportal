<?php 

class UserVoiceMsgVoiceMgmtServiceOperation{	
	
    function _construct(){
 	}
	
 	
 	function getVoiceMsgVoiceMgmtServiceDetails($userId) {
 		global $sessionid, $client;
 		
 		$xmlinput .= openBroadsoftHeader($sessionid);
 		$xmlinput .= self::getUserVoiceMsgVoiceMgmtServiceGetCommand($userId);
 		$xmlinput .= closeBroadSoftHeader(); 
 		
 		$response = $client->processOCIMessage(array("in0" => $xmlinput));
 		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
 		
 		$returnResponse = self::getParsedVoiceMsgVoiceMgmtServiceGetResponse($xml->command);
 		
 		return $returnResponse;
 	}
 	
        public static function  getUserVoiceMsgVoiceMgmtServiceGetCommand($userId) {
		$xmlinput = openCommand( "UserVoiceMessagingUserGetVoiceManagementRequest17");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	
	public static function getParsedVoiceMsgVoiceMgmtServiceGetResponse($commandResponse) {
		
                $returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
                $returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserVoiceMsgVoiceMgmtServiceOperation::parseVoiceMsgVoiceMgmtServiceGetCommandResponse ( $commandResponse );
		return $returnResponse;
	}
	
	private static function  parseVoiceMsgVoiceMgmtServiceGetCommandResponse($commandResponse) {
		$vMVmSResponse["isActive"] = strval($commandResponse->isActive);
		$vMVmSResponse["alwaysRedirectToVoiceMail"] = strval($commandResponse->alwaysRedirectToVoiceMail);
		$vMVmSResponse["busyRedirectToVoiceMail"] = strval($commandResponse->busyRedirectToVoiceMail);
		$vMVmSResponse["noAnswerRedirectToVoiceMail"] = strval($commandResponse->noAnswerRedirectToVoiceMail);
		$vMVmSResponse["serverSelection"] = strval($commandResponse->serverSelection);
		$vMVmSResponse["userServer"] = strval($commandResponse->userServer);
		$vMVmSResponse["mailboxIdType"] = strval($commandResponse->mailboxIdType);
		$vMVmSResponse["mailboxURL"] = strval($commandResponse->mailboxURL);
		$vMVmSResponse["noAnswerNumberOfRings"] = strval($commandResponse->noAnswerNumberOfRings);
		$vMVmSResponse["processing"] = strval($commandResponse->processing);
		$vMVmSResponse["voiceMessageDeliveryEmailAddress"] = strval($commandResponse->voiceMessageDeliveryEmailAddress);
		$vMVmSResponse["usePhoneMessageWaitingIndicator"] = strval($commandResponse->usePhoneMessageWaitingIndicator);
		$vMVmSResponse["sendVoiceMessageNotifyEmail"] = strval($commandResponse->sendVoiceMessageNotifyEmail);
		$vMVmSResponse["voiceMessageNotifyEmailAddress"] = strval($commandResponse->voiceMessageNotifyEmailAddress);
		$vMVmSResponse["sendCarbonCopyVoiceMessage"] = strval($commandResponse->sendCarbonCopyVoiceMessage);
		$vMVmSResponse["voiceMessageCarbonCopyEmailAddress"] = strval($commandResponse->voiceMessageCarbonCopyEmailAddress);
		$vMVmSResponse["transferOnZeroToPhoneNumber"] = strval($commandResponse->transferOnZeroToPhoneNumber);
		$vMVmSResponse["transferPhoneNumber"] = strval($commandResponse->transferPhoneNumber);
		return $vMVmSResponse;
	}         
}
?>