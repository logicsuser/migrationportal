<?php 

class UserRemoteOfficeOperation{
	
	
    function _construct(){
 	}
	
 	
 	function getUserRemoteOfficeDetails($userId) {
 		global $sessionid, $client;
 		
 		$xmlinput .= openBroadsoftHeader($sessionid);
 		$xmlinput .= self::getUserRemoteOfficeGetCommand($userId);
 		$xmlinput .= closeBroadSoftHeader(); 
 		
 		$response = $client->processOCIMessage(array("in0" => $xmlinput));
 		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
 		
 		$returnResponse = self::getParsedRemoteOfficeGetResponse($xml->command);
 		
 		return $returnResponse;
 	}
 	
	public static function  getUserRemoteOfficeGetCommand($userId) {
		$xmlinput = openCommand( "UserRemoteOfficeGetRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	public static function getParsedRemoteOfficeGetResponse($commandResponse) {
		
                $returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
                $returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserRemoteOfficeOperation::parseRemoteOfficeGetCommandResponse ( $commandResponse );
		return $returnResponse;
	}
	
	private static function  parseRemoteOfficeGetCommandResponse($commandResponse) {
		$remoteResponse["isActive"] = strval($commandResponse->isActive);
		$remoteResponse["remoteOfficePhoneNumber"] = strval($commandResponse->remoteOfficePhoneNumber);
		return $remoteResponse;
	}
}
?>