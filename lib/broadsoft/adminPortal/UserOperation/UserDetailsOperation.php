<?php 

class UserDetailsOperation{
	
	public $userDetails = array();
	public $sp;
	public $groupId;
	public $users_BLF_Monitored = array();
	public $users_SCA_Appearances = array();
	public $department;
	
	public function __construct($providerId = null, $groupId = null){
		if($providerId) {
			$this->sp = $providerId;
		}
		if($groupId) {
			$this->groupId = $groupId;
		}
	}
	
	private function getUserServiceDetailsExecuteCommand($commandInput){
		global $client;
		$response = $client->processOCIMessage(array("in0" => $commandInput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		return $xml;
	}
	
	public function getAllUserDetailsForExpress($providerId, $groupId, $usersList = null) {
		
		$this->sp = $providerId;
		$this->groupId = $groupId;
		
		$userDataResponse = $this->getParsedUserDetails($providerId, $groupId, $usersList);
		
		//print_r($userDataResponse);
		//echo "<br/>------"; 
		return $this->getAllUserListResponse($userDataResponse);
	}
	
	private function getAllUserListResponse($userDataResponse){
		
		global $sessionid;
		global $client;
		$users = array();

		foreach($userDataResponse as $key=>$val){
			
			//user personal details
			$userId = $users[$key]["userId"] = $val["UserDetails"]["userId"];
			$users[$key]["firstName"] = $val["UserDetails"]["firstName"];
			$users[$key]["lastName"] = $val["UserDetails"]["lastName"];
			$users[$key]["extension"] = $val["UserDetails"]["extension"];
			$users[$key]["phoneNumber"] = $val["UserDetails"]["phoneNumber"];			
			$users[$key]["department"] = $val["UserDetails"]["department"];
                        
            $users[$key]["simRing"] = $val["UserDetails"]["simRing"];
            $users[$key]["clidBlock"] = $val["UserDetails"]["clidBlock"];

			//Activation Status
			/*
			if($val["UserDetails"]["activated"] == 'true') {
				$users[$key]["activated"] = 'Yes';
			} else if($val["UserDetails"]["activated"] == 'false') {
				$users[$key]["activated"] = 'No';
			} else {
				$users[$key]["activated"] = '';
			}
			*/
			$users[$key]["activated"] = '';
			if($activationStatus = $this->getActivationStatus($userId)) {
				$users[$key]["activated"] = $activationStatus['activated'];
				if(isset($activationStatus['phoneNumber'])) {
					$users[$key]["phoneNumber"] = $activationStatus['phoneNumber'];
				}
			}

			//registration details
			$b = 0;
			$deviceName = "";
			$userid = '';
			foreach ($val["registration"]["Success"]["row"] as $key1 => $registrationValue)
			{
				$deviceName = $registrationValue->col[1];
				$expiration = $registrationValue->col[4];
				$epType = $registrationValue->col[6];
				$deviceLevel = strval($registrationValue->col[0]);
				$publicIp = strval($registrationValue->col[7]);
				$agentType = strval($registrationValue->col[11]);

				$users[$key]['registerStatus'] = "Registered";
				$users[$key]["registration"][$b]["deviceName"] = $deviceName;
				$users[$key]["registration"][$b]["deviceLevel"] = $deviceLevel;
				$users[$key]["registration"][$b]["expiration"] = $expiration;
				$users[$key]["registration"][$b]["epType"] = $epType;
				$users[$key]["registration"][$b]["publicIp"] = $publicIp;
				$users[$key]["registration"][$b]["agentType"] = $agentType;
				
				$groupAccessDeviceResponse = GroupAccessDeviceGetOperation::getGroupAccessDeviceDetails($this->sp, $this->groupId, $deviceName);
				$users[$key]["registration"][$b]["deviceType"] = $groupAccessDeviceResponse["Success"]["deviceType"];
				$users[$key]["registration"][$b]["macAddress"] = $groupAccessDeviceResponse["Success"]["macAddress"];
				
				$polycomPhoneServicesResponse = UserPolycomPhoneServicesOperation::getPolycomPhoneServicesDetails($userId, $deviceName);
				
                               
//$users[$key]["registration"][$b]["integration"] = $polycomPhoneServicesResponse["Success"]["integratePhoneDirectoryWithBroadWorks"];
                                $users[$key]["registration"][$b]["integration"] = $polycomPhoneServicesResponse["Success"]["integration"] == true ? "Yes" : "No" ;
				$users[$key]["registration"][$b]["ccd"] = $polycomPhoneServicesResponse["Success"]["groupCustomContactDirectory"];

				//TODO: UGLY HACK - Need to fix this.
				foreach ($users[$key]["registration"][$b] as $item => $value) {
					$users[$key][$item] = $value;
				}
				//TODO: UGLY HACK - Need to fix this. - End

				$b++;
			}
			
			// Retrieve device info and Polycom phone service info for unregistered users
			if ($deviceName == ""){
				
				$users[$key]["deviceName"] = "";
				$users[$key]["deviceType"] = "";
				$users[$key]["portNumber"] = "";
				$users[$key]["macAddress"] = "";
				//$users[$key]["integration"] = "";
                                $users[$key]["integration"] = "No";
				$users[$key]["ccd"] = "";
				$users[$key]["epType"] = "";
				
				$userEndPointResponse = UserEndPointDeviceNameOperation::getUserEndPointDeviceNameDetails($userId);
				$deviceName = $userEndPointResponse["Success"]["deviceName"];
				$portNumber = $userEndPointResponse["Success"]["portNumber"];
				
				if ($deviceName != "")
				{
					$users[$key]["deviceName"] = $deviceName;
					$users[$key]["portNumber"] = $portNumber;
					$users[$key]['registerStatus'] = "Non-Registered";
					
					$groupAccessDeviceResponse = GroupAccessDeviceGetOperation::getGroupAccessDeviceDetails($this->sp, $this->groupId, $deviceName);
					$users[$key]["deviceType"] = $groupAccessDeviceResponse["Success"]["deviceType"];
					$users[$key]["macAddress"] = $groupAccessDeviceResponse["Success"]["macAddress"];
					
					$polycomPhoneServicesResponse = UserPolycomPhoneServicesOperation::getPolycomPhoneServicesDetails($userId, $deviceName);
					  
                                      //  $users[$key]["integration"] = $polycomPhoneServicesResponse["Success"]["integratePhoneDirectoryWithBroadWorks"];
                                        $users[$key]["integration"] = $polycomPhoneServicesResponse["Success"]["integration"] == true ? "Yes" : "No" ;
					$users[$key]["ccd"] = $polycomPhoneServicesResponse["Success"]["groupCustomContactDirectory"];
                                        $users[$key]["customContactDirectory"] = $polycomPhoneServicesResponse["Success"]["ccd"];
					$devDetailsResponse = GroupDeviceGetUserOperation::getGroupAccessDeviceUserListDetails($this->sp, $this->groupId, $deviceName);
					$users[$key]["epType"] = strval($devDetailsResponse["Success"]["row"]->col[6]);
				}
			}
			
			//call forwarding
			
			$userServiceArray = array();
			if(empty($val["Service"]["Error"])){
				$userServiceArray = $val["Service"]["Success"]["user"];
				
				$usv = 0;
				foreach($userServiceArray as $keyu => $userServiceValue){ 
					if ($userServiceArray[$usv] == "Voice Messaging User") {
						$users[$key]["voiceMessagingUser"] = "true";
					}
                                        
					if ($userServiceArray[$usv] == "Busy Lamp Field") {
						$users[$key]["busyLampField"] = "true";
					}
					if ($userServiceArray[$usv] == "Call Forwarding Always") {
						$users[$key]["cfa"] = "true";
						$users[$key]["callControlTab"] = "true";
					}
					if ($userServiceArray[$usv] == "Call Forwarding Busy") {
						$users[$key]["cfb"] = "true";
						$users[$key]["callControlTab"] = "true";
					}
					if ($userServiceArray[$usv] == "Call Forwarding No Answer") {
						$users[$key]["cfn"] = "true";
						$users[$key]["callControlTab"] = "true";
					}
					if ($userServiceArray[$usv] == "Call Forwarding Not Reachable") {
						$users[$key]["cfr"] = "true";
						$users[$key]["callControlTab"] = "true";
					}
					$usv++;
				}
			}else{
				$users[$key]["userServices"] = $val["Service"]["Error"];
			}
			
			$users[$key]["userServices"] = $userServiceArray;
			
			
			$thirdParty = $val["ThirdPartyVM"]["Success"]["isActive"];
			$voiceManagement = $val["VmsgAdvceVMService"]["Success"];
			
			if ($thirdParty == "true" and $voiceManagement == "false")
			{
				$users[$key]["thirdPartyVoiceMail"] = "true";
			}
			
			//vm section
			$users[$key]['vm']["isActive"] = $val["vm"]["Success"]["isActive"];
			$users[$key]['vm']["alwaysRedirectToVoiceMail"] = $val["vm"]["Success"]["alwaysRedirectToVoiceMail"];
			$users[$key]['vm']["busyRedirectToVoiceMail"] = $val["vm"]["Success"]["busyRedirectToVoiceMail"];
			$users[$key]['vm']["noAnswerRedirectToVoiceMail"] = $val["vm"]["Success"]["noAnswerRedirectToVoiceMail"];
			if (isset($users[$key]["thirdPartyVoiceMail"]) and $users[$key]["thirdPartyVoiceMail"] == "true")
			{
				$users[$key]['vm']["serverSelection"] = $val["vm"]["Success"]["serverSelection"];
				$users[$key]['vm']["userServer"] = $val["vm"]["Success"]["userServer"];
				$users[$key]['vm']["mailboxIdType"] = $val["vm"]["Success"]["mailboxIdType"];
				$users[$key]['vm']["mailboxURL"] = $val["vm"]["Success"]["mailboxURL"];
				$users[$key]['vm']["noAnswerNumberOfRings"] = $val["vm"]["Success"]["noAnswerNumberOfRings"];
			}
			else
			{
				$users[$key]['vm']["processing"] = $val["vm"]["Success"]["processing"];
				$users[$key]['vm']["voiceMessageDeliveryEmailAddress"] = $val["vm"]["Success"]["voiceMessageDeliveryEmailAddress"];
				$users[$key]['vm']["usePhoneMessageWaitingIndicator"] = $val["vm"]["Success"]["usePhoneMessageWaitingIndicator"];
				$users[$key]['vm']["sendVoiceMessageNotifyEmail"] = $val["vm"]["Success"]["sendVoiceMessageNotifyEmail"];
				$users[$key]['vm']["voiceMessageNotifyEmailAddress"] = $val["vm"]["Success"]["voiceMessageNotifyEmailAddress"];
				$users[$key]['vm']["sendCarbonCopyVoiceMessage"] = $val["vm"]["Success"]["sendCarbonCopyVoiceMessage"];
				$users[$key]['vm']["voiceMessageCarbonCopyEmailAddress"] = $val["vm"]["Success"]["voiceMessageCarbonCopyEmailAddress"];
				$users[$key]['vm']["transferOnZeroToPhoneNumber"] = $val["vm"]["Success"]["transferOnZeroToPhoneNumber"];
				$users[$key]['vm']["transferPhoneNumber"] = $val["vm"]["Success"]["transferPhoneNumber"];
			}
			//end vm section
			
			//dnd
			if(empty($val["Dnd"]["Error"])){
				$users[$key]["DnD"] = $val["Dnd"]["Success"];
			}else{
				$users[$key]["DnD"] = $val["Dnd"]["Error"];
			}
			
			//call forwarding
			if(empty($val["CallForwarding"]["Error"])){
				$users[$key]["Fwd"]["active"] = $val["CallForwarding"]["Success"]["isActive"];
				$users[$key]["Fwd"]["FwdTo"] = $val["CallForwarding"]["Success"]["forwardToPhoneNumber"];
			}else{
				$users[$key]["Fwd"]["active"] = $val["Dnd"]["Error"];
				$users[$key]["Fwd"]["FwdTo"] = "";
			}
			
			//cfa active user details
			if (empty($val["cfaActive"]["Error"])) {
				$users[$key]["cfaActive"] = strval($val["cfaActive"]["Success"]["isActive"]);
			}else{
				$users[$key]["cfaActive"] = $val["cfaActive"]["Error"];
			}
			
			//cfb active user details
			if (empty($val["cfbActive"]["Error"])) {
				$users[$key]["cfbActive"] = strval($val["cfbActive"]["Success"]["isActive"]);
			}else{
				$users[$key]["cfbActive"] = $val["cfbActive"]["Error"];
			}
			
			//cfn active user details
			if (empty($val["cfnActive"]["Error"])) {
				$users[$key]["cfnActive"] = strval($val["cfnActive"]["Success"]["isActive"]);
			}else{
				$users[$key]["cfnActive"] = $val["cfnActive"]["Error"];
			}
			
			//cfr active user details
			if (empty($val["cfrActive"]["Error"])) {
				$users[$key]["cfrActive"] = strval($val["cfrActive"]["Success"]["isActive"]);
			}else{
				$users[$key]["cfrActive"] = $val["cfrActive"]["Error"];
			}
			
			//blf users
			if(empty($val["blf"]["Error"])){
				//$users[$key]["hasBLFUsers"] = $val["blf"]["Success"];
				$users[$key]["hasBLFUsers"] = "false";
				
				if (isset($val["blf"]["Success"]["monitoredUserTableRow"]) && count($val["blf"]["Success"]["monitoredUserTableRow"]) > 0) {
					$users[$key]["hasBLFUsers"] = "true";
					
					$a = 0;
					foreach ($val["blf"]["Success"]["monitoredUserTableRow"] as $key1 => $value) {
						$users_BLF_Monitored[] = strval($value->col[0]);
						$users[$key]["monitoredUsers"][$a]["id"] = strval($value->col[0]);
						$users[$key]["monitoredUsers"][$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
						$a++;
					}
				}
			}else{
				$users[$key]["hasBLFUsers"] = $val["blf"]["Error"];
			}
			
			//shared call appearance
			$users[$key]["sharedCallAppearances"] = "false";
			if(!empty($deviceName)){
				$devDetailsResponse = GroupDeviceGetUserOperation::getGroupAccessDeviceUserListDetails($this->sp, $this->groupId, $deviceName);
				
				if (isset($devDetailsResponse["Success"]["row"]) && count($devDetailsResponse["Success"]["row"]) > 1) {
					$users[$key]["sharedCallAppearances"] = "true";
					
					$a = 0;
					foreach ($devDetailsResponse["Success"]["row"] as $scakey => $scavalue) {
						if (strval($scavalue->col[6]) == "Shared Call Appearance") {
							$users_SCA_Appearances[] = strval($scavalue->col[4]);
						}
						$users[$key]["sharedCallAppearanceUsers"][$a]["id"] = strval($scavalue->col[4]);
						$users[$key]["sharedCallAppearanceUsers"][$a]["name"] = strval($scavalue->col[1]) . ", " . strval($scavalue->col[2]);
						$users[$key]["sharedCallAppearanceUsers"][$a]["phoneNumber"] = strval($scavalue->col[3]);
						$users[$key]["sharedCallAppearanceUsers"][$a]["extension"] = strval($scavalue->col[9]);
						$users[$key]["sharedCallAppearanceUsers"][$a]["department"] = strval($scavalue->col[10]);
						$users[$key]["sharedCallAppearanceUsers"][$a]["linePort"] = strval($scavalue->col[0]);
						$users[$key]["sharedCallAppearanceUsers"][$a]["port"] = strval($scavalue->col[7]);
						$users[$key]["sharedCallAppearanceUsers"][$a]["endpointType"] = strval($scavalue->col[6]);
						$users[$key]["sharedCallAppearanceUsers"][$a]["primary"] = strval($scavalue->col[8]);
						
						$a++;
					}
				}
			}
			
			// get Custom Profile & custom tags associated with this user
			$users[$key]["customProfile"] = "None";
			$users[$key]["customTags"] = array();
			$users[$key]["customTagsShortDescriptions"] = array();
			if ($users[$key]["deviceName"] != "") {
				$customTags = new UserCustomTagManager($users[$key]["deviceName"], $users[$key]["deviceType"]);
				$users[$key]["customProfile"] = $customTags->getCustomTagValue(ExpressCustomProfileName);
				if ($users[$key]["customProfile"] == "") {
					$users[$key]["customProfile"] = "None";
				}
				
				// get custom tags from back-end
				if (isset($customTags)) {
					$users[$key]["customTags"] = $customTags->getUserCustomTagList();
					
					if(isset($users[$key]["customTags"]) && isset($users[$key]["customTags"]) > 0) {
						foreach ($users[$key]["customTags"] as $name => $value) {
							$users[$key]["customTagsShortDescriptions"][] = $customTags->getShortDescription($name);
						}
						natsort($users[$key]["customTagsShortDescriptions"]);
					}
				}
			}
			
			$users[$key]["numberOfTags"] = 0;
			
			//remote user details
			if (empty($val["remoteOffice"]["Error"])) {
				$users[$key]["remote"]["active"] = "";
				$users[$key]["remote"]["number"] = "";
			}else{
				$users[$key]["remote"]["active"] = strval($val["remoteOffice"]["Success"]["isActive"]);
				$users[$key]["remote"]["number"] = strval($val["remoteOffice"]["Success"]["remoteOfficePhoneNumber"]);
			}
			
			//user service get assignment get list request
			if(empty($val["servicePack"]["Error"])){
				$serviceCount = 0;
				foreach ($val["servicePack"]["Success"]["servicePacksAssignmentTableRow"] as $keys => $servicePackValue){
					if ($servicePackValue->col[1] == "true")
					{
						$users[$key]["servicePack"][$serviceCount] = strval($servicePackValue->col[0]);
						$serviceCount++;
					}
				}
			}
			//ends user service get assignment get list request

		}
		$this->users_BLF_Monitored = array_unique($users_BLF_Monitored);
		$this->users_SCA_Appearances = array_unique($users_SCA_Appearances);
		
		return $users;
	}
	
	public function getParsedUserDetails($providerId, $groupId, $usersList = null) {
		$userDataCommand = $this->getUserDetails($providerId, $groupId, $usersList);
		return $this->parseBatchResponseForServices($userDataCommand);
	}
	
	public function getUserDetails($providerId, $groupId, $basicUsersList = null){
		global $sessionid;

		$usersList = array();

		if($basicUsersList) {
			foreach ($basicUsersList as $user) {
				$usersListResponse = $this->userWithUserIdInGroup($providerId, $groupId, $user['userId']);
				if(empty($usersList["Error"]) && isset($usersListResponse["Success"]) && isset($usersListResponse["Success"][0])){
					$usersList[] = $usersListResponse["Success"][0];
				}
			}

		} else {
			$userObj = new UserOperations();
			$userObj->department = $this->department;
			$usersListResponse = $userObj->userListOfGroup($providerId, $groupId);
			if(empty($usersList["Error"])){
				$usersList = $usersListResponse["Success"];
			}
		}

		$userCommandResponse = array();
		$a = 0;

		if($usersList){
			foreach($usersList as $key=>$val){
				$userId = $val->col[0];
				$userDataCommand = self::prepareBatchCommandsForServices ( $userId );
				$userCommandResponse[$a] = $this->getUserServiceDetailsExecuteCommand($userDataCommand);
				$this->userDetails[$a] = $this->getUserInfoResponse($val);
				
				$a++;
			}
		}

		return $userCommandResponse;
	}

	public function getSingleUserDetails($userId){
		global $sessionid;

		$usersList = $this->userWithUserIdInGroup($this->sp, $this->groupId, $userId);

		$userCommandResponse = array();
		$a = 0;

		if(empty($usersList["Error"])){
			foreach($usersList["Success"] as $key=>$val){
				$userId = $val->col[0];
				$userDataCommand = self::prepareBatchCommandsForServices ( $userId );
				$userCommandResponse[$a] = $this->getUserServiceDetailsExecuteCommand($userDataCommand);
				$this->userDetails[$a] = $this->getUserInfoResponse($val);

				$a++;
			}
		}
		$userDetails = $this->parseBatchResponseForServices($userCommandResponse);
		$response = $this->getAllUserListResponse($userDetails);

		return $response;
	}

	function userWithUserIdInGroup($providerId, $groupId, $userId){
		global $sessionid, $client;
		$returndata = array();
		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";

		$xmlinput = xmlHeader($sessionid, "UserGetListInGroupRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($providerId) . "</serviceProviderId>";
		$xmlinput .= "<GroupId>" . htmlspecialchars($groupId) . "</GroupId>";

		$xmlinput .= "<searchCriteriaUserId>";
		$xmlinput .= "<mode>Equal To</mode>";
		$xmlinput .= "<value>" . htmlspecialchars($userId) . "</value>";
		$xmlinput .= "<isCaseInsensitive>false</isCaseInsensitive>";
		$xmlinput .= "</searchCriteriaUserId>";

		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		$errMsg = readErrorXmlGenuine($xml);
		if($errMsg != "") {
			$returnResponse["Error"] = $errMsg;
		}
		else
		{
			foreach ($xml->command->userTable->row as $key => $value)
			{
				$returndata[] = $value;
			}
			$returnResponse["Success"] = $returndata;
		}
		return $returnResponse;
	}

	/**
	 * @param userId
	 */
	 private function prepareBatchCommandsForServices($userId) {
	 	
	 	global $sessionid;
		$userDataCommand = openBroadsoftHeader($sessionid);
		$userDataCommand .= UserDndOperation::getUserDndGetCommand($userId);
		$userDataCommand .= UserServiceOperation::getUserServiceCommand($userId);
		$userDataCommand .= UserCallForwardingOperation::getUserCallForwardingCommand($userId);
		$userDataCommand .= UserBusyLampFieldServiceOperation::getUserBusyLampFieldServiceGetCommand($userId);
		$userDataCommand .= UserRegistrationOperation::getUserRegistrationGetCommand($userId);
        $userDataCommand .= UserThirdPartyVoiceMailOperation::getUserThirdPartyVoiceMailGetCommand($userId);
		$userDataCommand .= UserVoiceMsgVoiceMgmtServiceOperation::getUserVoiceMsgVoiceMgmtServiceGetCommand($userId);
		$userDataCommand .= UserVoiceMsgAdvanceVoiceMgmtServiceOperation::getUserVoiceMsgAdvanceVoiceMgmtServiceGetCommand($userId);
		$userDataCommand .= UserRemoteOfficeOperation::getUserRemoteOfficeGetCommand($userId);
		$userDataCommand .= UserCallForwardingOperation::getUserCallForwardingCommand($userId);
		$userDataCommand .= UserCallForwardingOperation::getUserCallForwardingBusCommand($userId);
		$userDataCommand .= UserCallForwardingOperation::getUserCallForwardingNoAnswerCommand($userId);
		$userDataCommand .= UserServiceGetAssignmentListRequestOperation::getUserServiceGetAssignmentListRequestGetCommand($userId);
		$userDataCommand .= UserCallForwardingOperation::getUserCallForwardingNotReachableCommand($userId);
		$userDataCommand .= UserVoiceMsgVoiceMgmtServiceOperation::getUserVoiceMsgVoiceMgmtServiceGetCommand($userId);
		$userDataCommand .= closeBroadSoftHeader();
		return $userDataCommand;
	}

	
	public function parseBatchResponseForServices($commandResponse) {
		$userDataOutput = array ();
		
		foreach ( $commandResponse as $key => $val ) {
				$userDataOutput [$key] ["UserDetails"] = $this->userDetails[$key];
				$userDataOutput [$key] ["Dnd"] = UserDndOperation::getParsedDndGetResponse( $commandResponse [$key]->command [0] );
				$userDataOutput [$key] ["Service"] = UserServiceOperation::getParsedServiceGetResponse( $commandResponse [$key]->command [1] );
				$userDataOutput [$key] ["CallForwarding"] = UserCallForwardingOperation::getParsedCallForwardingGetResponse( $commandResponse [$key]->command [2] );
				$userDataOutput [$key] ["blf"] = UserBusyLampFieldServiceOperation::getParsedBusyLampFieldServiceGetResponse( $commandResponse [$key]->command [3] );
				$userDataOutput [$key] ["registration"] = UserRegistrationOperation::getParsedRegistrationGetResponse( $commandResponse [$key]->command [4] );
				$userDataOutput [$key] ["ThirdPartyVM"] = UserThirdPartyVoiceMailOperation::getParsedThirdPartyVoiceMailGetResponse($commandResponse [$key]->command [5]);
				$userDataOutput [$key] ["VmsgVMService"] = UserVoiceMsgVoiceMgmtServiceOperation::getParsedVoiceMsgVoiceMgmtServiceGetResponse($commandResponse [$key]->command [6]);
				$userDataOutput [$key] ["VmsgAdvceVMService"] = UserVoiceMsgAdvanceVoiceMgmtServiceOperation::getParsedVoiceMsgAdvanceVoiceMgmtServiceGetResponse($commandResponse [$key]->command [7]);
				$userDataOutput [$key] ["remoteOffice"] = UserRemoteOfficeOperation::getParsedRemoteOfficeGetResponse($commandResponse [$key]->command [8]);
				$userDataOutput [$key] ["cfaActive"] = UserCallForwardingOperation::getParsedCallForwardingGetResponse($commandResponse [$key]->command [9]);
				$userDataOutput [$key] ["cfbActive"] = UserCallForwardingOperation::getParsedCallForwardingBusGetResponse($commandResponse [$key]->command [10]);
				$userDataOutput [$key] ["cfnActive"] = UserCallForwardingOperation::getParsedCallForwardingNoAnswerGetResponse($commandResponse [$key]->command [11]);
				$userDataOutput [$key] ["cfrActive"] = UserCallForwardingOperation::getParsedCallForwardingNoAnswerGetResponse($commandResponse [$key]->command [13]);
				$userDataOutput [$key] ["servicePack"] = UserServiceGetAssignmentListRequestOperation::getParsedUserServiceGetAssignmentListRequestGetResponse($commandResponse [$key]->command [12]);
				$userDataOutput [$key] ["vm"] = UserVoiceMsgVoiceMgmtServiceOperation::getParsedVoiceMsgVoiceMgmtServiceGetResponse($commandResponse [$key]->command [14]);
		}
		return $userDataOutput;
	}
	
	public function getUserInfoResponse($usersArray){
            
                require_once '/var/www/lib/broadsoft/adminPortal/simRingCriteriaManagement.php';
                $objSimRingMgmt   = new SimRingCriteriaManagement();
                require_once '/var/www/lib/broadsoft/adminPortal/services/Services.php';
                $objClidBlock = new Services();
            
		global $ociVersion;
		$usersInfo = array();
		
		$userInfo["userId"]    = strval($usersArray->col[0]);
		$userInfo["firstName"] = strval($usersArray->col[2]);
		$userInfo["lastName"]  = strval($usersArray->col[1]);
                
                $simRingListData  = $objSimRingMgmt->getSimRingCrietriaList($userInfo["userId"]);
                               
                $simRingService   = $simRingListData['Success']["activeStatus"];
                if ($simRingService == "true") {
                    $simRingService = "On";
                } else {
                    $simRingService = "Off";
                }
                
                $userInfo["simRing"]   = $simRingService; 
                $clidBlockService = $objClidBlock->userCallingLineIDDeliveryBlockingGetRequest($userInfo["userId"]);
                if(isset($clidBlockService["Success"]["isActive"]) && $clidBlockService["Success"]["isActive"] == "true"){
                    $userInfo["clidBlock"] = "Yes";
                }else{
                    $userInfo["clidBlock"] = "No";
                }
                
		if ($ociVersion == "17"){
			$userInfo["extension"] = "N/A";
		}else{
			$userInfo["extension"] = strval($usersArray->col[10]);
		}
		$userInfo["phoneNumber"] = strval($usersArray->col[4]);		
		$userInfo["department"] = strval($usersArray->col[3]);
		$userInfo["activated"] = strval($usersArray->col[5]);
		return $userInfo;
	}

	public function getActivationStatus($userId) {

		require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
		$userDn = new Dns();

		$user = array();

		$numberActivateResponse = $userDn->getUserDNActivateListRequest($userId);
		$phoneNumberStatus = "";
		if (empty ($numberActivateResponse ['Error'])) {
			if (count($numberActivateResponse ['Success']) > 0) {
				if (!empty ($numberActivateResponse ['Success'] [0] ['phoneNumber'])) {
					$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
					$user['phoneNumber'] = $userPhoneNumber;
				}
				if (!empty ($numberActivateResponse ['Success'] [0] ['status'])) {
					$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
				}
			}
		}

		$user['activated'] = $phoneNumberStatus;

		return $user;
	}

}
?>