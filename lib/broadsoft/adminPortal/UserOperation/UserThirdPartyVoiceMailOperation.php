<?php 

class UserThirdPartyVoiceMailOperation{
	
	
    function _construct(){
 	}
	
 	
 	function getThirdPartyVoiceMailDetails($userId) {
 		global $sessionid, $client;
 		
 		$xmlinput = openBroadsoftHeader($sessionid);
 		$xmlinput .= self::getUserThirdPartyVoiceMailGetCommand($userId);
 		$xmlinput .= closeBroadSoftHeader(); 
 		
 		$response = $client->processOCIMessage(array("in0" => $xmlinput));
 		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
 		
 		$returnResponse = self::getParsedThirdPartyVoiceMailGetResponse($xml->command);
 		
 		return $xml;
 	}
 	
    public static function  getUserThirdPartyVoiceMailGetCommand($userId) {
		$xmlinput = openCommand( "UserThirdPartyVoiceMailSupportGetRequest17");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	
	public static function getParsedThirdPartyVoiceMailGetResponse($commandResponse) {
		
                $returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
                $returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserThirdPartyVoiceMailOperation::parseThirdPartyVoiceMailGetCommandResponse ( $commandResponse );
		return $returnResponse;
	}
	
	private static function  parseThirdPartyVoiceMailGetCommandResponse($commandResponse) {
		$tPVMResponse["isActive"] = strval($commandResponse->isActive);
		$tPVMResponse["busyRedirectToVoiceMail"] = strval($commandResponse->busyRedirectToVoiceMail);
		$tPVMResponse["noAnswerRedirectToVoiceMail"] = strval($commandResponse->noAnswerRedirectToVoiceMail);
		$tPVMResponse["serverSelection"] = strval($commandResponse->serverSelection);
		$tPVMResponse["mailboxIdType"] = strval($commandResponse->mailboxIdType);
		$tPVMResponse["noAnswerNumberOfRings"] = strval($commandResponse->noAnswerNumberOfRings);
		$tPVMResponse["alwaysRedirectToVoiceMail"] = strval($commandResponse->alwaysRedirectToVoiceMail);
		$tPVMResponse["outOfPrimaryZoneRedirectToVoiceMail"] = strval($commandResponse->outOfPrimaryZoneRedirectToVoiceMail);
		return $tPVMResponse;
	}         
}
?>