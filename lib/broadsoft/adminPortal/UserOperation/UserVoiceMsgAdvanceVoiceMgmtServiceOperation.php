<?php 

class UserVoiceMsgAdvanceVoiceMgmtServiceOperation{
	
    function _construct(){
 	}
	
 	
 	function getVoiceMsgAdvanceVoiceMgmtServiceDetails($userId) {
 		global $sessionid, $client;
 		
 		$xmlinput .= openBroadsoftHeader($sessionid);
 		$xmlinput .= self::getUserVoiceMsgAdvanceVoiceMgmtServiceGetCommand($userId);
 		$xmlinput .= closeBroadSoftHeader(); 
 		
 		$response = $client->processOCIMessage(array("in0" => $xmlinput));
 		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
 		
 		$returnResponse = self::getParsedVoiceMsgAdvanceVoiceMgmtServiceGetResponse($xml->command);
 		
 		return $returnResponse;
 	}
 	
        public static function  getUserVoiceMsgAdvanceVoiceMgmtServiceGetCommand($userId) {
		$xmlinput = openCommand( "UserVoiceMessagingUserGetAdvancedVoiceManagementRequest14sp3");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	
	public static function getParsedVoiceMsgAdvanceVoiceMgmtServiceGetResponse($commandResponse) {
		
                $returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
                $returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserVoiceMsgAdvanceVoiceMgmtServiceOperation::parseVoiceMsgAdvanceVoiceMgmtServiceGetCommandResponse ( $commandResponse );
		return $returnResponse;
	}
	
	private static function  parseVoiceMsgAdvanceVoiceMgmtServiceGetCommandResponse($commandResponse) {
		$vMAVmSResponse = strval($commandResponse->isActive);
		return $vMAVmSResponse;
	}         
}
?>