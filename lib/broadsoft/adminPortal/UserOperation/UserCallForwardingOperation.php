<?php 

class UserCallForwardingOperation{
	
	public $userId;
	
// 	public function __construct($userId){
// 		$this->userId = $userId;
// 	}

	
	function getCallForwardingDetails($userId) {
		global $sessionid, $client;
		
		$xmlinput .= openBroadsoftHeader($sessionid, "UserCallForwardingAlwaysGetRequest");
		$xmlinput .= self::getUserDndGetCommand($userId);
		$xmlinput .= closeBroadSoftHeader();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		$returnResponse = getParsedDndGetResponse($xml->command);
		
		return $returnResponse;
	}
	
	public static function getUserCallForwardingCommand($userId) {
		$xmlinput = openCommand( "UserCallForwardingAlwaysGetRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
// 	public static function getUserCallForwardingCommandResponse($commandResponse) {
// 		$cfResponse = strval($commandResponse->isActive);
// 		return $cfResponse;
// 	}

	
	public static function getParsedCallForwardingGetResponse($commandResponse) {
			$returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
			$returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserCallForwardingOperation::parseCallForwardGetCommandResponse( $commandResponse );
		return $returnResponse;
	}
	
	private static function  parseCallForwardGetCommandResponse($commandResponse) {
		$cfResponse["isActive"] = strval($commandResponse->isActive);
		$cfResponse["forwardToPhoneNumber"] = strval($commandResponse->forwardToPhoneNumber);
		return $cfResponse;
	}
	
	//call forwarding bus command
	public static function getUserCallForwardingBusCommand($userId) {
		$xmlinput = openCommand( "UserCallForwardingBusyGetRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	//call forwarding bus command response
	public static function getParsedCallForwardingBusGetResponse($commandResponse) {
		$returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
		$returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserCallForwardingOperation::parseCallForwardingBusGetCommandResponse( $commandResponse );
		return $returnResponse;
	}
	
	//give the output for command response
	private static function  parseCallForwardingBusGetCommandResponse($commandResponse) {
		$cfbAResponse["isActive"] = strval($commandResponse->isActive);
		return $cfbAResponse;
	}
	
	//call forwarding bus command
	public static function getUserCallForwardingNoAnswerCommand($userId) {
		$xmlinput = openCommand( "UserCallForwardingNoAnswerGetRequest13mp16");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	//call forwarding bus command response
	public static function getParsedCallForwardingNoAnswerGetResponse($commandResponse) {
		$returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
		$returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserCallForwardingOperation::parseCallForwardingNoAnswerGetCommandResponse( $commandResponse );
		return $returnResponse;
	}
	
	//give the output for command response
	private static function  parseCallForwardingNoAnswerGetCommandResponse($commandResponse) {
		$cfnAResponse["isActive"] = strval($commandResponse->isActive);
		return $cfnAResponse;
	}
	
	//call forwarding not reachable command
	public static function getUserCallForwardingNotReachableCommand($userId) {
		$xmlinput = openCommand( "UserCallForwardingNotReachableGetRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	//call forwarding not reachable response
	public static function getParsedCallForwardingNotReachableGetResponse($commandResponse) {
		$returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
		$returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserCallForwardingOperation::parseCallForwardingNoAnswerGetCommandResponse( $commandResponse );
		return $returnResponse;
	}
	
	//give the output not reachable response
	private static function  parseCallForwardingNotReachableGetCommandResponse($commandResponse) {
		$cfrAResponse["isActive"] = strval($commandResponse->isActive);
		return $cfrAResponse;
	}
}
?>