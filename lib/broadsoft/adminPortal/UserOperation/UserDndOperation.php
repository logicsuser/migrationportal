<?php 

class UserDndOperation{
	
	
    function _construct(){
 	}
	
 	
 	function getDndDetails($userId) {
 		global $sessionid, $client;
 		
 		$xmlinput = openBroadsoftHeader($sessionid, "UserDoNotDisturbGetRequest");
 		$xmlinput .= self::getUserDndGetCommand($userId);
 		$xmlinput .= closeBroadSoftHeader(); 
 		
 		$response = $client->processOCIMessage(array("in0" => $xmlinput));
 		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
 		
 		$returnResponse = getParsedDndGetResponse($xml->command);
 		
 		return $returnResponse;
 	}
 	
	public static function  getUserDndGetCommand($userId) {
		$xmlinput = openCommand( "UserDoNotDisturbGetRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= closeCommand();
		
		return $xmlinput;
	}
	
	public static function getParsedDndGetResponse($commandResponse) {
		
			$returnResponse["Error"] = readErrorFromCommand ( $commandResponse );
			$returnResponse["Success"] = !empty($returnResponse["Error"]) ? null: UserDndOperation::parseDndGetCommandResponse ( $commandResponse );
		return $returnResponse;
	}
	
	private static function  parseDndGetCommandResponse($commandResponse) {
		$dnResponse = strval($commandResponse->isActive);
		return $dnResponse;
	}
}
?>