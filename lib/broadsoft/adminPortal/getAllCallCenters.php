<?php
	$xmlinput = xmlHeader($sessionid, "GroupCallCenterGetInstanceListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->callCenterTable->row as $key => $value)
	{
		$callCenters[$a]["id"] = strval($value->col[0]);
		$callCenters[$a]["name"] = strval($value->col[1]);
		$a++;
	}

	if (isset($callCenters))
	{
		$callCenters = subval_sort($callCenters, "name");
	}
?>
