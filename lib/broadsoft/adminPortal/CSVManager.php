<?php

/**
 * Created by Karl.
 * Date: 9/18/2016
 */
class CSVManager
{
    private $keys = array();
    private $headTitles = array();
    private $validators = array();
    private $bw_DeviceTypesList = array();  // not always populated
    private $userList = array();            // not always populated
    private $devicesList = array();         // not always populated
    private $groupDomainList = array();     // not always populated

    private $numColumns = 0;
    private $responseTable;
    private $msg = "trace:";
    private $error = false;
    private $postName = "";

    public $is_file_upload = false;

    // mimicking protected constants
    protected static function Validate_BW_Device_Type() { return "Validate_BW_Device_Type"; }
    protected static function Validate_BW_Tag_Name() { return "Validate_BW_Tag_Name"; }
    protected static function Validate_UserID_Format() { return "Validate_UserID_Format"; }
    protected static function Validate_TrueFalse_Format() { return "Validate_TrueFalse_Format"; }
    protected static function Validate_NonZero_Number() { return "Validate_NonZero_Number"; }
    protected static function Validate_UserID_Existence() { return "Validate_UserID_Existence"; }
    protected static function Validate_DeviceExistence() { return "Validate_Device_Existence"; }
    protected static function Validate_Bridge_Warning_Format() { return "Validate_Bridge_Warning_Format"; }
    protected static function Validate_Domain_Existence() { return "Validate_Domain_Existence"; }
    protected static function Validate_None() { return "Validate_None"; }

    /**
     * CSVManager constructor.
     */
    public function __construct($postName) {
        $this->postName = $postName;
    }

    /**
     * @return bool
     */
    public function hasError() {
        return $this->error;
    }

    /**
     * @return string
     */
    public function trace() {
        return $this->msg;
    }


    /**
     * Create HTML table header as in the example below:
     * <th style="width:12%">Caller ID</th>
     */
    public function createTableHeader() {
        $header = "";
        $margin = $this->numColumns == 0 ? 100 : (int)(100 / $this->numColumns);

        for ($i = 0; $i < $this->numColumns; $i++) {
            $header .= "<th style=\"width:" . $margin . "%\">" . $this->headTitles[$i] . "</th>";
        }

        return $header;
    }


    /**
     * @return string
     */
	public function parse()
	{

		if ($this->is_file_upload) {
			$rows = $this->parseFileUploaded();
		} else {
			$rows = explode("\r\n", $_POST["data"]);
			unset($rows[count($rows) - 1]); //remove blank value from end of array
			unset($rows[0]); //remove header row
		}

		$this->error = count($rows) == 0;
		$this->responseTable = "";

		$this->msg .= " parse:" . count($rows) . " rows";
		$i = 0;
		foreach ($rows as $row) {
			$this->responseTable .= "<tr>";
			$this->responseTable .= $this->parseRow($row, $i);
			$this->responseTable .= "</tr>";
			$i++;
		}

		return $this->responseTable;
	}

	function parseFileUploaded()
	{

		$exp = array();

		if (file_exists($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

			$tmp_file_name = tempnam(sys_get_temp_dir(), 'BULK_');

			$file_type = $_FILES['file']['type'];

			$path = $_FILES['file']['name'];
			$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

			//print_r($_FILES['file']);

			move_uploaded_file(
				$_FILES['file']['tmp_name'], $tmp_file_name
			);

			if (file_exists($tmp_file_name) && ($file_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $ext == "xlsx")) {

				try {
					/** PHPExcel */
					require_once("/var/www/phpToExcel/Classes/PHPExcel.php");
					require_once("/var/www/phpToExcel/Classes/PHPExcel/IOFactory.php");

					$objReader = PHPExcel_IOFactory::createReader('Excel2007');
					//Load the file
					$objPHPExcel = $objReader->load($tmp_file_name);
					//Get number of Sheets
					$sheetCount = $objPHPExcel->getSheetCount();
					//Set the Last Sheet as Active
					$objPHPExcel->setActiveSheetIndex($sheetCount - 1);
					//Read the data into an array
					$sheet = $objPHPExcel->getActiveSheet();
					$maxCell = $sheet->getHighestRowAndColumn();

					//$max_col = PHPExcel_Cell::stringFromColumnIndex(count($fields) - 1);
					$max_col = $maxCell['column'];

					for ($row = 1; $row <= $maxCell['row']; $row++) {
						$sheet_row = $sheet->rangeToArray("A$row:$max_col$row", null, true, false);
						if ($sheet_row && count($sheet_row) > 0) {
							if(!array_filter($sheet_row[0])) {
								break;
							}
							$exp[] = $sheet_row[0];
						}
					}

				} catch (Exception $e) {
					error_log($e->getMessage());
				}

				//Remove the Header
				if (count($exp) > 0) {
					array_shift($exp);
				}

			} else if (file_exists($tmp_file_name) && ($file_type == "text/csv" || $ext == "csv")) {

				try {
					/** PHPExcel */
					require_once("/var/www/phpToExcel/Classes/PHPExcel.php");
					require_once("/var/www/phpToExcel/Classes/PHPExcel/IOFactory.php");

					$objReader = new PHPExcel_Reader_CSV();
					$objPHPExcel = $objReader->load($tmp_file_name);
					$objReader->setSheetIndex(0);

					//Read the data into an array
					$exp = $objPHPExcel->getActiveSheet()->toArray(null, false, true, false);

					//Remove the Header
					if (count($exp) > 0) {
						array_shift($exp);
					}
					//echo "<pre>";
					//print_r($sheetData);
					//echo "</pre>";
				} catch (Exception $e) {
					error_log($e->getMessage());
				}
			}
		}

		return $exp;
	}


    //-----------------------------------------------------------------------------------------------------------------
    protected function addAttribute($key, $headTitle, $validation) {
        $this->keys[] = $key;
        $this->headTitles[] = $headTitle;
        $this->validators[] = $validation;

        $this->numColumns++;

        // get data from back-end
        switch ($validation) {
            case self::Validate_BW_Device_Type(): $this->getBwDeviceTypes(); break;
            case self::Validate_UserID_Existence(): $this->getAllUsers(); break;
            case self::Validate_DeviceExistence(): $this->getAllDevices(); break;
            case self::Validate_Domain_Existence(): $this->getAllGroupDomains(); break;
            case self::Validate_BW_Tag_Name():
            case self::Validate_TrueFalse_Format():
            case self::Validate_NonZero_Number():
            case self::Validate_Bridge_Warning_Format():
            case self::Validate_None():
            default:
        }
    }


    /**
    /* Create HTML Table with rows as in the example below:
     * <td class="bad">
     * <input class="noBorder" title="MAC address is not 12 characters" type="text" name="customTags[0][macAddress]" id="customTags[0][macAddress]" value="3c27a21123b" style="width:98%;" readonly="readonly">
     * </td>
     *
     * @param $row
     * @return string
     */
    //-----------------------------------------------------------------------------------------------------------------
    private function parseRow($row, $index) {

	    if($this->is_file_upload) {
	    	$attr = $row;
	    } else {
	    	$attr = explode(",", $row);
	    }

        $html = "";

        for ($i = 0; $i < $this->numColumns; $i++) {
            $key = $this->keys[$i];
            $value = $attr[$i];
	        $value .= '';

            // Validate each attribute
            $error = $this->validateAttribute($value, $this->validators[$i]);
            $class = $error == "" ? "good" : "bad";

            if ($error != "") {
                $this->error = true;
            }

            //write to data array
            //$this->dataArray[$index][$key] = $value;

            // Create input name and id
            $name = $this->postName . "[" . $index . "][" . $key . "]";

            $html .= "<td class=\"" . $class . "\">";
            $html .= "<input class=\"noBorder\" title=\"" . $error . "\" type=\"text\"";
            $html .= " name=\"" . $name . "\" id=\"" . $name . "\"";
            $html .= " value=\"" . $value . "\" style=\"width:98%\" readonly=\"readonly\">";
            $html .= "</td>";
        }

        return $html;
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function validateAttribute($attr, $validator) {
        switch ($validator) {
            case self::Validate_BW_Device_Type():           return $this->validateDeviceType($attr);
            case self::Validate_BW_Tag_Name():              return self::validateBwTagName($attr);
            case self::Validate_UserID_Format():            return self::validateUserIdFormat($attr);
            case self::Validate_TrueFalse_Format():         return self::validateTrueFalseFormat($attr);
            case self::Validate_NonZero_Number():           return self::validateNonZeroNumber($attr);
            case self::Validate_UserID_Existence():         return $this->validateUserIdExistence($attr);
            case self::Validate_DeviceExistence():          return $this->validateDeviceExistence($attr);
            case self::Validate_Bridge_Warning_Format():    return self::validateBridgeWarningFormat($attr);
            case self::Validate_Domain_Existence():         return $this->validateDomainExistence($attr);
            default: return "";
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function validateDeviceType($deviceType) {
        return in_array($deviceType, $this->bw_DeviceTypesList) ? "" : $deviceType . " is not a valid Device Type";
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function validateUserIdExistence($userId) {
        return in_array($userId, $this->userList) ? "" : "User does not exist";
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function validateDeviceExistence($device) {
        return in_array($device, $this->devicesList) ? "" : "Device does not exist";
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function validateDomainExistence($value) {
        $exp = explode("@", $value);

        return isset($exp[1]) && in_array($exp[1], $this->groupDomainList) ? "" : "Domain does not exist";
    }


    //-----------------------------------------------------------------------------------------------------------------
    private static function validateBwTagName($tagName) {
        if (strlen($tagName) > 2) {
            if ($tagName[0] == "%" && substr($tagName, -1) == "%") {
                return "";
            }
        }

        return "Invalid tag format, expected: '%NAME%'";
    }


    //-----------------------------------------------------------------------------------------------------------------
    private static function validateUserIdFormat($userId) {
        if (! filter_var($userId, FILTER_VALIDATE_EMAIL)) {
            return "Invalid User ID format. Expected: '<userPortion>@<domain>";
        }

        $uidSegments = explode("@", $userId);
        $len = strlen($uidSegments[0]);
        if ($len < 6 || $len > 80) {
            return "Invalid User ID format. User portion of the ID must be between 6-80 chars.";
        }

        $len = strlen($uidSegments[1]);
        if ($len < 2 || $len > 80) {
            return "Invalid User ID format. Domain portion of the ID must be between 2-80 chars.";
        }

        return "";
    }


    //-----------------------------------------------------------------------------------------------------------------
    private static function validateTrueFalseFormat($value) {
        return strtoupper($value) != "TRUE" && strtoupper($value) != "FALSE" ? "Expected 'true' or 'false'" : "";
    }


    //-----------------------------------------------------------------------------------------------------------------
    private static function validateNonZeroNumber($value) {
        return ctype_digit($value . '') && (int)$value > 0 ? "" : "Expected positive integer";
    }


    //-----------------------------------------------------------------------------------------------------------------
    private static function validateBridgeWarningFormat($value) {
        return strtoupper($value) == "BARGE-IN" ||
            strtoupper($value) == "BARGE-IN AND REPEAT" ||
            strtoupper($value) == "NONE" ? "" : "Invalid Bridge Warning Tone attribute";
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function getBwDeviceTypes() {
        global $nonObsoleteDevices;

        require_once("/var/www/lib/broadsoft/login.php");
        require_once("/var/www/html/Express/config.php");
        checkLogin();

        if (count($this->bw_DeviceTypesList) == 0) {
            require_once("/var/www/lib/broadsoft/adminPortal/getDevices.php");
            $this->bw_DeviceTypesList = $nonObsoleteDevices;
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function getAllUsers() {
        global $users;

        // Populate user list
        if (count($this->userList) == 0) {
            $i = 0;
            foreach ($users as $key => $user) {
                $this->userList[$i++] = $user["id"];
            }
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function getAllDevices() {
        global $devices;

        // Populate devices list
        if (count($this->devicesList) == 0) {
            $this->devicesList = $devices;
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function getAllGroupDomains() {
        global $groupDomains;

        // Populate group domains
        if (count($this->groupDomainList) == 0) {
            $this->groupDomainList = $groupDomains;
        }
    }
}
