<?php

/**
 * Created by Sologics.
 * Date: 12/05/2017
 */
class GroupModify
{
	public $groupId;
	public $groupInfo = array();
	public $groupInfoResponse = array();
	public $groupRoutingResponse= array();
	public $routingResponse= array();
	
	public function __construct($groupId) {
		global  $sessionid, $client;
		$this->groupId = $groupId;
		$groupInfoResponse["Error"] = "";
		$groupInfoResponse["Success"] = "";
		
		if(!empty($groupId)){
			$xmlinput = xmlHeader($sessionid, "GroupGetRequest14sp7");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId). "</groupId>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			//echo "<pre>"; print_r($xml); die;
			if (readErrorXmlGenuine($xml) != "") {
				$groupInfoResponse["Error"] = $xml->command->summaryEnglish;
			}else{
				$groupInfoResponse["Success"] = $xml->command;
				
			}
		}
		$this->groupInfo = $groupInfoResponse;
	}
	
	public  function getGroupInfo(){
		global $securityDomainPattern;
		$groupInfoData = $this->groupInfo;
		$groupInfoArray = array();
		if(!empty($groupInfoData["Success"])){
			$groupInfoArray['groupInfoData']['groupName'] = strval($groupInfoData['Success']->groupName);
			$groupInfoArray['groupInfoData']['groupId'] = $this->groupId;
			$groupInfoArray['groupInfoData']['defaultDomain'] = strval($groupInfoData['Success']->defaultDomain);
			$groupInfoArray['groupInfoData']['userCount'] = strval($groupInfoData['Success']->userCount);
			$groupInfoArray['groupInfoData']['userLimit'] = strval($groupInfoData['Success']->userLimit);
			$groupInfoArray['groupInfoData']['callingLineIdName'] = strval($groupInfoData['Success']->callingLineIdName);
			$groupInfoArray['groupInfoData']['callingLineIdDisplayPhoneNumber'] = "+1-".strval($groupInfoData['Success']->callingLineIdDisplayPhoneNumber);
			$groupInfoArray['groupInfoData']['timeZone'] = strval($groupInfoData['Success']->timeZone);
			$groupInfoArray['groupInfoData']['timeZoneDisplayName'] = strval($groupInfoData['Success']->timeZoneDisplayName);
			$groupInfoArray['groupInfoData']['locationDialingCode'] = strval($groupInfoData['Success']->locationDialingCode);
			$groupInfoArray['groupInfoData']['contactName'] = strval($groupInfoData['Success']->contact->contactName);
			$groupInfoArray['groupInfoData']['contactNumber'] = strval($groupInfoData['Success']->contact->contactNumber);
			$groupInfoArray['groupInfoData']['contactEmail'] = strval($groupInfoData['Success']->contact->contactEmail);
			$groupInfoArray['groupInfoData']['addressLine1'] = strval($groupInfoData['Success']->address->addressLine1);
			$groupInfoArray['groupInfoData']['addressLine2'] = strval($groupInfoData['Success']->address->addressLine2);
			$groupInfoArray['groupInfoData']['city'] = strval($groupInfoData['Success']->address->city);
			$groupInfoArray['groupInfoData']['stateOrProvince'] = strval($groupInfoData['Success']->address->stateOrProvince);
			$groupInfoArray['groupInfoData']['zipOrPostalCode'] = strval($groupInfoData['Success']->address->zipOrPostalCode);
			$groupInfoArray['groupInfoData']['country'] = strval($groupInfoData['Success']->address->country);
			
			$assignDomainList = $this->getDefaultDomainAssignList();
			if(empty($assignDomainList['Error'])){
				$groupInfoArray['groupInfoData']['assignDomainList'] = $assignDomainList['Success'];
			}
			
			$dnList = $this->getGroupDnGetListRequest();
			if(empty($dnList['Error'])){
				$groupInfoArray['groupInfoData']['callingLineIdDisplayPhoneNumberList'] = $dnList['Success'];
			}
			
			$routingProfile = $this->getGroupRoutingProfile();
			if(empty($routingProfile['Error'])){
				$groupInfoArray['groupInfoData']['routingProfile'] = strval($routingProfile['Success']->routingProfile);
			}
			
			$dnsList = $this->getGroupDnGetListRequest();
			//echo "<pre>"; print_r($dnsList);
			if(empty($dnsList['Error'])){
				if(!empty($dnsList['Success']) && count($dnsList['Success']) > 0){
					$groupInfoArray['groupInfoData']['assignNumbers'] = implode(",", $dnsList['Success']);
				}else{
					$groupInfoArray['groupInfoData']['assignNumbers'] = array();
				}
			}
			
			$callProcessingPolicy = $this->getGroupCallProcessingPolicy();
			if(empty($callProcessingPolicy['Error'])){
				$groupInfoArray['groupInfoData']['useGroupCLIDSetting'] = strval($callProcessingPolicy['Success']->useGroupCLIDSetting);
				$groupInfoArray['groupInfoData']['useGroupMediaSetting'] = strval($callProcessingPolicy['Success']->useGroupMediaSetting);
				$groupInfoArray['groupInfoData']['useGroupCallLimitsSetting'] = strval($callProcessingPolicy['Success']->useGroupCallLimitsSetting);
				$groupInfoArray['groupInfoData']['useGroupTranslationRoutingSetting'] = strval($callProcessingPolicy['Success']->useGroupTranslationRoutingSetting);
				$groupInfoArray['groupInfoData']['useGroupDCLIDSetting'] = strval($callProcessingPolicy['Success']->useGroupDCLIDSetting);
				$groupInfoArray['groupInfoData']['useMaxSimultaneousCalls'] = strval($callProcessingPolicy['Success']->useMaxSimultaneousCalls);
				$groupInfoArray['groupInfoData']['maxSimultaneousCalls'] = strval($callProcessingPolicy['Success']->maxSimultaneousCalls);
				$groupInfoArray['groupInfoData']['useMaxSimultaneousVideoCalls'] = strval($callProcessingPolicy['Success']->useMaxSimultaneousVideoCalls);
				$groupInfoArray['groupInfoData']['maxSimultaneousVideoCalls'] = strval($callProcessingPolicy['Success']->maxSimultaneousVideoCalls);
				$groupInfoArray['groupInfoData']['useMaxCallTimeForAnsweredCalls'] = strval($callProcessingPolicy['Success']->useMaxCallTimeForAnsweredCalls);
				$groupInfoArray['groupInfoData']['maxCallTimeForAnsweredCallsMinutes'] = strval($callProcessingPolicy['Success']->maxCallTimeForAnsweredCallsMinutes);
				$groupInfoArray['groupInfoData']['useMaxCallTimeForUnansweredCalls'] = strval($callProcessingPolicy['Success']->useMaxCallTimeForUnansweredCalls);
				$groupInfoArray['groupInfoData']['maxCallTimeForUnansweredCallsMinutes'] = strval($callProcessingPolicy['Success']->maxCallTimeForUnansweredCallsMinutes);
				$groupInfoArray['groupInfoData']['mediaPolicySelection'] = strval($callProcessingPolicy['Success']->mediaPolicySelection);
				$groupInfoArray['groupInfoData']['networkUsageSelection'] = strval($callProcessingPolicy['Success']->networkUsageSelection);
				$groupInfoArray['groupInfoData']['enforceGroupCallingLineIdentityRestriction'] = strval($callProcessingPolicy['Success']->enforceGroupCallingLineIdentityRestriction);
				$groupInfoArray['groupInfoData']['allowEnterpriseGroupCallTypingForPrivateDialingPlan'] = strval($callProcessingPolicy['Success']->allowEnterpriseGroupCallTypingForPrivateDialingPlan);
				$groupInfoArray['groupInfoData']['overrideCLIDRestrictionForPrivateCallCategory'] = strval($callProcessingPolicy['Success']->overrideCLIDRestrictionForPrivateCallCategory);
				$groupInfoArray['groupInfoData']['useEnterpriseCLIDForPrivateCallCategory'] = strval($callProcessingPolicy['Success']->useEnterpriseCLIDForPrivateCallCategory);
				$groupInfoArray['groupInfoData']['enableEnterpriseExtensionDialing'] = strval($callProcessingPolicy['Success']->enableEnterpriseExtensionDialing);
				$groupInfoArray['groupInfoData']['useMaxConcurrentRedirectedCalls'] = strval($callProcessingPolicy['Success']->useMaxConcurrentRedirectedCalls);
				$groupInfoArray['groupInfoData']['maxConcurrentRedirectedCalls'] = strval($callProcessingPolicy['Success']->maxConcurrentRedirectedCalls);
				$groupInfoArray['groupInfoData']['useMaxFindMeFollowMeDepth'] = strval($callProcessingPolicy['Success']->useMaxFindMeFollowMeDepth);
				$groupInfoArray['groupInfoData']['maxFindMeFollowMeDepth'] = strval($callProcessingPolicy['Success']->maxFindMeFollowMeDepth);
				$groupInfoArray['groupInfoData']['maxRedirectionDepth'] = strval($callProcessingPolicy['Success']->maxRedirectionDepth);
				$groupInfoArray['groupInfoData']['useMaxConcurrentFindMeFollowMeInvocations'] = strval($callProcessingPolicy['Success']->useMaxConcurrentFindMeFollowMeInvocations);
				$groupInfoArray['groupInfoData']['maxConcurrentFindMeFollowMeInvocations'] = strval($callProcessingPolicy['Success']->maxConcurrentFindMeFollowMeInvocations);
				$groupInfoArray['groupInfoData']['clidPolicy'] = strval($callProcessingPolicy['Success']->clidPolicy);
				$groupInfoArray['groupInfoData']['emergencyClidPolicy'] = strval($callProcessingPolicy['Success']->emergencyClidPolicy);
				$groupInfoArray['groupInfoData']['allowAlternateNumbersForRedirectingIdentity'] = strval($callProcessingPolicy['Success']->allowAlternateNumbersForRedirectingIdentity);
				$groupInfoArray['groupInfoData']['useGroupName'] = strval($callProcessingPolicy['Success']->useGroupName);
				$groupInfoArray['groupInfoData']['blockCallingNameForExternalCalls'] = strval($callProcessingPolicy['Success']->blockCallingNameForExternalCalls);
				$groupInfoArray['groupInfoData']['enableDialableCallerID'] = strval($callProcessingPolicy['Success']->enableDialableCallerID);
				$groupInfoArray['groupInfoData']['allowConfigurableCLIDForRedirectingIdentity'] = strval($callProcessingPolicy['Success']->allowConfigurableCLIDForRedirectingIdentity);
				$groupInfoArray['groupInfoData']['allowDepartmentCLIDNameOverride'] = strval($callProcessingPolicy['Success']->allowDepartmentCLIDNameOverride);
			}
			
			$allDomains = "";
			$allAssignedDomain = "";
			
			$all = $this->getServiceProviderDomainGetAssignedListRequest();
			if(empty($assigned['Error'])){
				$groupInfoArray['groupInfoData']['allDomain'] = $allDomains = $all['Success'];
			}
			

			$assigned = $this->getGroupDomainGetAssignedListRequest();
			if(empty($assigned['Error'])){
				$groupInfoArray['groupInfoData']['assignedDomain'] = $allAssignedDomain = $assigned['Success'];
			}
			
			$groupInfoArray['groupInfoData']['available'] = array_values(array_diff($allDomains, $allAssignedDomain));
			
			$groupInfoArray['groupInfoData']['domainAvailable'] = $this->getAvailableDomain($groupInfoArray['groupInfoData']['available'], $securityDomainPattern);
			$groupInfoArray['groupInfoData']['domainAssigned'] = $this->getAvailableDomain($allAssignedDomain, $securityDomainPattern);
			$groupInfoArray['groupInfoData']['securityDomainAvailable'] = $this->getSecurityDomain($groupInfoArray['groupInfoData']['available'], $securityDomainPattern);
			$groupInfoArray['groupInfoData']['securityDomainAssigned'] = $this->getSecurityDomain($allAssignedDomain, $securityDomainPattern);
			
			//voice portal response
			$voicePortalResponse = $this->getVoicePortalGroupResponse();
			if(empty($voicePortalResponse['Error'])){
				//echo "<pre>"; print_r($voicePortalResponse); die;
				$groupInfoArray['groupInfoData']['portalPhoneNo'] = $voicePortalResponse['Success']['portalPhoneNo'];
				$groupInfoArray['groupInfoData']['portalExt'] = $voicePortalResponse['Success']['portalExt'];
				$groupInfoArray['groupInfoData']['portalLanguage'] = $voicePortalResponse['Success']['portalLanguage'];
				$groupInfoArray['groupInfoData']['portalTimeZone'] = $voicePortalResponse['Success']['portalTimeZone'];
				$groupInfoArray['groupInfoData']['portalTimeZoneDisplayName'] = $voicePortalResponse['Success']['portalTimeZoneDisplayName'];
				$groupInfoArray['groupInfoData']['callingLineIdLastName'] = $voicePortalResponse['Success']['callingLineIdLastName'];
				$groupInfoArray['groupInfoData']['callingLineIdFirstName'] = $voicePortalResponse['Success']['callingLineIdFirstName'];
				$groupInfoArray['groupInfoData']['name'] = $voicePortalResponse['Success']['name'];
				$groupInfoArray['groupInfoData']['serviceUserId'] = $voicePortalResponse['Success']['serviceUserId'];
                                $groupInfoArray['groupInfoData']['voicePortalActiveStatus'] = $voicePortalResponse['Success']['voicePortalActiveStatus'];
			}					
			
		}else{
			$groupInfoArray['groupInfoData']['Error'] = strval($groupInfoData['Error']);
		}
		$_SESSION['groupInfoData'] = $groupInfoArray['groupInfoData'];
		return $groupInfoArray['groupInfoData'];
	}
	
	
	
	public function getGroupList() {
		global  $sessionid, $client;
		
		$xmlinput = xmlHeader($sessionid, "GroupGetListInServiceProviderRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		$allGroups = array();
		$a = 0;
		if (isset($xml->command->groupTable->row) && !empty($xml->command->groupTable->row))
		{
			foreach ($xml->command->groupTable->row as $key => $value)
			{
				$allGroups[$a] = strval($value->col[0])." ";
				$a++;
			}
		}
		return $allGroups;
	}
	
	public function groupModifyRequest($postArray) {
		global $sessionid, $client;
		$groupModifyResponse["Error"] = "";
		$groupModifyResponse["Success"] = "";
		if(!empty($postArray)){
			$xmlinput = xmlHeader($sessionid, "GroupModifyRequest");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']). "</groupId>";
			$xmlinput .= "<defaultDomain>" . $postArray['defaultDomain']. "</defaultDomain>";
			$xmlinput .= "<userLimit>" . $postArray['userLimit']. "</userLimit>";
			if(!empty($postArray['groupName'])){
				$xmlinput .= "<groupName>" . $postArray['groupName']. "</groupName>";
			}else{
				$xmlinput .= "<groupName xsi:nil='true'></groupName>";
			}
			if(!empty($postArray['callingLineIdName'])){
				$xmlinput .= "<callingLineIdName>" . $postArray['callingLineIdName']. "</callingLineIdName>";
			}else{
				$xmlinput .= "<callingLineIdName xsi:nil='true'></callingLineIdName>";
			}
			if(!empty($postArray['callingLineIdDisplayPhoneNumber'])){
				$xmlinput .= "<callingLineIdPhoneNumber>" . $postArray['callingLineIdDisplayPhoneNumber']. "</callingLineIdPhoneNumber>";
			}else{
				$xmlinput .= "<callingLineIdPhoneNumber xsi:nil='true'></callingLineIdPhoneNumber>";
			}
			$xmlinput .= "<timeZone>" . $postArray['timeZone']. "</timeZone>";
			if(!empty($postArray['locationDialingCode'])){
				$xmlinput .= "<locationDialingCode>" . $postArray['locationDialingCode']. "</locationDialingCode>";
			}else{
				$xmlinput .= "<locationDialingCode xsi:nil='true'></locationDialingCode>";
			}
			//if(!empty($postArray['contactName']) || !empty($postArray['contactEmail']) || !empty($postArray['contactNumber'])){
			$xmlinput .= "<contact>";
			if(!empty($postArray['contactName'])){
				$xmlinput .= "<contactName>" . $postArray['contactName']. "</contactName>";
			}else{
				$xmlinput .= "<contactName xsi:nil='true'></contactName>";
			}
			
			if(!empty($postArray['contactNumber'])){
				$xmlinput .= "<contactNumber>" . $postArray['contactNumber']. "</contactNumber>";
			}else{
				$xmlinput .= "<contactNumber xsi:nil='true'></contactNumber>";
			}
			
			if(!empty($postArray['contactEmail'])){
				$xmlinput .= "<contactEmail>" . $postArray['contactEmail']. "</contactEmail>";
			}else{
				$xmlinput .= "<contactEmail xsi:nil='true'></contactEmail>";
			}
			
			$xmlinput .= "</contact>";
			
			//}
			//if(!empty($postArray['addressLine1']) || !empty($postArray['addressLine2']) || !empty($postArray['city']) || !empty($postArray['stateOrProvince']) || !empty($postArray['zipOrPostalCode']) || !empty($postArray['country'])){
			$xmlinput .= "<address>";
			if(!empty($postArray['addressLine1'])){
				$xmlinput .= "<addressLine1>" . $postArray['addressLine1']. "</addressLine1>";
			}else{
				$xmlinput .= "<addressLine1 xsi:nil='true'></addressLine1>";
			}
			if(!empty($postArray['addressLine2'])){
				$xmlinput .= "<addressLine2>" . $postArray['addressLine2']. "</addressLine2>";
			}else{
				$xmlinput .= "<addressLine2 xsi:nil='true'></addressLine2>";
			}
			if(!empty($postArray['city'])){
				$xmlinput .= "<city>" . $postArray['city']. "</city>";
			}else{
				$xmlinput .= "<city xsi:nil='true'></city>";
			}
			if(!empty($postArray['stateOrProvince'])){
				$xmlinput .= "<stateOrProvince>" . $postArray['stateOrProvince']. "</stateOrProvince>";
			}else{
				$xmlinput .= "<stateOrProvince xsi:nil='true'></stateOrProvince>";
			}
			if(!empty($postArray['zipOrPostalCode'])){
				$xmlinput .= "<zipOrPostalCode>" . $postArray['zipOrPostalCode']. "</zipOrPostalCode>";
			}else{
				$xmlinput .= "<zipOrPostalCode xsi:nil='true'></zipOrPostalCode>";
			}
			if(!empty($postArray['country'])){
				$xmlinput .= "<country>" . $postArray['country']. "</country>";
			}else{
				$xmlinput .= "<country xsi:nil='true'></country>";
			}
			$xmlinput .= "</address>";
			//}
			
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			//echo "<pre>"; print_r($xml);
			if (readErrorXmlGenuine($xml) != "") {
				//$groupModifyResponse["Error"] = $xml->command->summaryEnglish;
				if(isset($xml->command->detail)){
					$groupModifyResponse["Error"]['Detail'] = strval($xml->command->detail);
				}else{
					$groupModifyResponse["Error"]['Detail'] = strval($xml->command->summaryEnglish);
				}
			}else{
				$groupModifyResponse["Success"] = $xml->command;
				
			}
		}
		return $groupModifyResponse;
	}
	
	public function modifyRoutingProfile($postArray){
		global  $sessionid, $client;
		$routingResponse['Error'] = "";
		$routingResponse['Success'] = "";
		
		$xmlinputRouting = xmlHeader($sessionid, "GroupRoutingProfileModifyRequest");
		$xmlinputRouting.= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinputRouting.= "<groupId>" . htmlspecialchars($postArray['groupId']). "</groupId>";
		if(!empty($postArray['routingProfile'])){
			$xmlinputRouting.= "<routingProfile>" . $postArray['routingProfile']. "</routingProfile>";
		}else{
			$xmlinputRouting.= "<routingProfile xsi:nil='true'></routingProfile>";
		}
		$xmlinputRouting.= xmlFooter();
		
		$responseRouting = $client->processOCIMessage(array("in0" => $xmlinputRouting));
		$xmlRouting = new SimpleXMLElement($responseRouting->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xmlRouting) != "") {
			//$routingResponse["Error"] = $xmlRouting->command->summaryEnglish;
			$routingResponse["Error"]['Detail'] = strval($xmlRouting->command->detail);
		}else{
			$routingResponse["Success"] = $xmlRouting->command;
			
		}
		return $routingResponse;
	}
	
	public function modifyAssignDomain($postArray){
		global  $sessionid, $client;
		$assignedDomains = array();
		$securityAssignedDomains = array();
		
		$assignDomainResponse['Error'] = "";
		$assignDomainResponse['Success'] = "";
		$newArray = array();
		
		if(!empty($postArray['domainAssignList'])){
			$assignedDomains = explode(";", substr($postArray['domainAssignList'], 0, -1));
		}
		if(!empty($postArray['securityDomainAssignList'])){
			$securityAssignedDomains = explode(";", substr($postArray['securityDomainAssignList'], 0, -1));
		}
		$domainListArray = array_merge($assignedDomains, $securityAssignedDomains);
		if(count($domainListArray) > 0){
			
			foreach($domainListArray as $key=>$val){
				if(!in_array($val, $_SESSION['groupInfoData']['assignedDomain']) && $val <> $postArray['defaultDomain']){
					$newArray['assignDomain'][] = $val;
				}
			}
			
			foreach($_SESSION['groupInfoData']['assignedDomain'] as $key=>$val){
				if(!in_array($val, $domainListArray) && $val <> $postArray['defaultDomain']){
					$newArray['unAssignDomain'][] = $val;
				}
			}
			
			
		}
		return $newArray;
	}
	
	public function updateUnAssignDomain($assignArray, $postArray){
		global  $sessionid, $client;
		
		$assignDomainResponse['Error'] = "";
		$assignDomainResponse['Success'] = "";
		$newArray = array();
		
		if(count($assignArray) > 0){
			$xmlinputUnAssignDomain  = xmlHeader($sessionid, "GroupDomainUnassignListRequest");
			$xmlinputUnAssignDomain.= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
			$xmlinputUnAssignDomain.= "<groupId>" . htmlspecialchars($postArray['groupId']). "</groupId>";
			foreach($assignArray as $key=>$val){
				if(!empty($val)){
					$xmlinputUnAssignDomain.= "<domain>".$val."</domain>";
				}
			}
			$xmlinputUnAssignDomain.= xmlFooter();
			$responseUnAssignDomain = $client->processOCIMessage(array("in0" => $xmlinputUnAssignDomain));
			$xmlUnAssignDomain = new SimpleXMLElement($responseUnAssignDomain->processOCIMessageReturn, LIBXML_NOWARNING);
			if (readErrorXmlGenuine($xmlUnAssignDomain) != "") {
				$assignDomainResponse["Error"] = $xmlUnAssignDomain->command->summaryEnglish;
				$assignDomainResponse["Error"]['Detail'] = $xmlUnAssignDomain->command->detail;
			}else{
				$assignDomainResponse["Success"] = $xmlUnAssignDomain->command;
			}
			
		}
		return $assignDomainResponse;
	}
	
	public function updateAssignDomain($assignArray, $postArray){
		global  $sessionid, $client;
		
		$assignDomainResponse['Error'] = "";
		$assignDomainResponse['Success'] = "";
		$newArray = array();
		
		if(count($assignArray) > 0){
			$xmlinputDomain  = xmlHeader($sessionid, "GroupDomainAssignListRequest");
			$xmlinputDomain.= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
			$xmlinputDomain.= "<groupId>" . htmlspecialchars($postArray['groupId']). "</groupId>";
			foreach($assignArray as $key=>$val){
				if(!empty($val)){
					$xmlinputDomain .= "<domain>".$val."</domain>";
				}
			}
			$xmlinputDomain.= xmlFooter();
			//echo $xmlinputDomain;
			$responseDomain = $client->processOCIMessage(array("in0" => $xmlinputDomain));
			$xmlDomain = new SimpleXMLElement($responseDomain->processOCIMessageReturn, LIBXML_NOWARNING);
			if (readErrorXmlGenuine($xmlDomain) != "") {
				$assignDomainResponse["Error"] = $xmlDomain->command->summaryEnglish;
				$assignDomainResponse["Error"]['Detail'] = $xmlDomain->command->detail;
			}else{
				$assignDomainResponse["Success"] = $xmlDomain->command;
			}
		}
		return $assignDomainResponse;
	}
	
	public function modifyCallProcessingPolicy($postArray){
		global  $sessionid, $client;
		$callProcessingModifyResponse["Error"] = "";
		$callProcessingModifyResponse["Success"] = "";
		
		//call Processing
		$xmlinputCall  = xmlHeader($sessionid, "GroupCallProcessingModifyPolicyRequest15sp2");
		$xmlinputCall .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinputCall .= "<groupId>" . htmlspecialchars($postArray['groupId']) . "</groupId>";
		if(!isset($postArray['useGroupName'])){
			$postArray['useGroupName'] = "false";
		}
		if(!isset($postArray['allowDepartmentCLIDNameOverride'])){
			$postArray['allowDepartmentCLIDNameOverride'] = "false";
		}
		if(!isset($postArray['allowAlternateNumbersForRedirectingIdentity'])){
			$postArray['allowAlternateNumbersForRedirectingIdentity'] = "false";
		}
		if(!isset($postArray['allowConfigurableCLIDForRedirectingIdentity'])){
			$postArray['allowConfigurableCLIDForRedirectingIdentity'] = "false";
		}
		if(!isset($postArray['blockCallingNameForExternalCalls'])){
			$postArray['blockCallingNameForExternalCalls'] = "false";
		}
		if(!isset($postArray['useMaxSimultaneousCalls'])){
			$postArray['useMaxSimultaneousCalls'] = "false";
		}
		if(!isset($postArray['useMaxSimultaneousVideoCalls'])){
			$postArray['useMaxSimultaneousVideoCalls'] = "false";
		}
		if(!isset($postArray['useMaxCallTimeForAnsweredCalls'])){
			$postArray['useMaxCallTimeForAnsweredCalls'] = "false";
		}
		if(!isset($postArray['useMaxCallTimeForUnansweredCalls'])){
			$postArray['useMaxCallTimeForUnansweredCalls'] = "false";
		}
		if(!isset($postArray['useMaxConcurrentRedirectedCalls'])){
			$postArray['useMaxConcurrentRedirectedCalls'] = "false";
		}
		if(!isset($postArray['useMaxConcurrentFindMeFollowMeInvocations'])){
			$postArray['useMaxConcurrentFindMeFollowMeInvocations'] = "false";
		}
		if(!isset($postArray['useMaxFindMeFollowMeDepth'])){
			$postArray['useMaxFindMeFollowMeDepth'] = "false";
		}
		if(!isset($postArray['enforceGroupCallingLineIdentityRestriction'])){
			$postArray['enforceGroupCallingLineIdentityRestriction'] = "false";
		}
		if(!isset($postArray['allowEnterpriseGroupCallTypingForPrivateDialingPlan'])){
			$postArray['allowEnterpriseGroupCallTypingForPrivateDialingPlan'] = "false";
		}
		if(!isset($postArray['allowEnterpriseGroupCallTypingForPublicDialingPlan'])){
			$postArray['allowEnterpriseGroupCallTypingForPublicDialingPlan'] = "false";
		}
		if(!isset($postArray['overrideCLIDRestrictionForPrivateCallCategory'])){
			$postArray['overrideCLIDRestrictionForPrivateCallCategory'] = "false";
		}
		
		if(isset($postArray['useGroupName'])){
			$useGroupName= "true";
			if(isset($postArray['allowDepartmentCLIDNameOverride'])){
				$allowDepartmentCLIDNameOverride= "true";
			}else{
				$allowDepartmentCLIDNameOverride= "false";
			}
			
		}else{
			$useGroupName= "false";
			$allowDepartmentCLIDNameOverride= "false";
		}
		
		$xmlinputCall .= "<useGroupCLIDSetting>" . $postArray['useGroupCLIDSetting']. "</useGroupCLIDSetting>";
		$xmlinputCall .= "<useGroupMediaSetting>" . $postArray['useGroupMediaSetting']. "</useGroupMediaSetting>";
		$xmlinputCall .= "<useGroupCallLimitsSetting>" . $postArray['useGroupCallLimitsSetting']. "</useGroupCallLimitsSetting>";
		$xmlinputCall .= "<useGroupTranslationRoutingSetting>" . $postArray['useGroupTranslationRoutingSetting']. "</useGroupTranslationRoutingSetting>";
		$xmlinputCall .= "<useGroupDCLIDSetting>" . $postArray['useGroupDCLIDSetting'] . "</useGroupDCLIDSetting>";
		$xmlinputCall .= "<useMaxSimultaneousCalls>" . $postArray['useMaxSimultaneousCalls']. "</useMaxSimultaneousCalls>";
		$xmlinputCall .= "<maxSimultaneousCalls>" . $postArray['maxSimultaneousCalls']. "</maxSimultaneousCalls>";
		$xmlinputCall .= "<useMaxSimultaneousVideoCalls>" . $postArray['useMaxSimultaneousVideoCalls']. "</useMaxSimultaneousVideoCalls>";
		$xmlinputCall .= "<maxSimultaneousVideoCalls>" . $postArray['maxSimultaneousVideoCalls']. "</maxSimultaneousVideoCalls>";
		$xmlinputCall .= "<useMaxCallTimeForAnsweredCalls>" . $postArray['useMaxCallTimeForAnsweredCalls']. "</useMaxCallTimeForAnsweredCalls>";
		$xmlinputCall .= "<maxCallTimeForAnsweredCallsMinutes>" . $postArray['maxCallTimeForAnsweredCallsMinutes']. "</maxCallTimeForAnsweredCallsMinutes>";
		$xmlinputCall .= "<useMaxCallTimeForUnansweredCalls>" . $postArray['useMaxCallTimeForUnansweredCalls']. "</useMaxCallTimeForUnansweredCalls>";
		$xmlinputCall .= "<maxCallTimeForUnansweredCallsMinutes>" . $postArray['maxCallTimeForUnansweredCallsMinutes']. "</maxCallTimeForUnansweredCallsMinutes>";
		
		$xmlinputCall .= "<mediaPolicySelection>" . $postArray['mediaPolicySelection'] . "</mediaPolicySelection>";
		
		if(isset($postArray['supportedMediaSetName'])){
			$xmlinputCall .= "<supportedMediaSetName xsi:nil='true' >" . $postArray['supportedMediaSetName'] . "</supportedMediaSetName>";
		}else{
			$xmlinputCall .= "<supportedMediaSetName xsi:nil='true' ></supportedMediaSetName>";
		}
		
		$xmlinputCall .= "<networkUsageSelection>" . $postArray['networkUsageSelection']. "</networkUsageSelection>";
		$xmlinputCall .= "<enforceGroupCallingLineIdentityRestriction>" . $postArray['enforceGroupCallingLineIdentityRestriction']. "</enforceGroupCallingLineIdentityRestriction>";
		
		$xmlinputCall .= "<allowEnterpriseGroupCallTypingForPrivateDialingPlan>" . $postArray['allowEnterpriseGroupCallTypingForPrivateDialingPlan']. "</allowEnterpriseGroupCallTypingForPrivateDialingPlan>";
		$xmlinputCall .= "<allowEnterpriseGroupCallTypingForPublicDialingPlan>" . $postArray['allowEnterpriseGroupCallTypingForPublicDialingPlan'] . "</allowEnterpriseGroupCallTypingForPublicDialingPlan>";
		$xmlinputCall .= "<overrideCLIDRestrictionForPrivateCallCategory>" . $postArray['overrideCLIDRestrictionForPrivateCallCategory']. "</overrideCLIDRestrictionForPrivateCallCategory>";
		//$xmlinputCall .= "<useEnterpriseCLIDForPrivateCallCategory>" . $postArray['useEnterpriseCLIDForPrivateCallCategory']. "</useEnterpriseCLIDForPrivateCallCategory>";
		//$xmlinputCall .= "<enableEnterpriseExtensionDialing>" . $postArray['enableEnterpriseExtensionDialing']. "</enableEnterpriseExtensionDialing>";
		$xmlinputCall .= "<useMaxConcurrentRedirectedCalls>" . $postArray['useMaxConcurrentRedirectedCalls'] . "</useMaxConcurrentRedirectedCalls>";
		
		if(isset($postArray['maxConcurrentRedirectedCalls'])){
			$xmlinputCall .= "<maxConcurrentRedirectedCalls>" . $postArray['maxConcurrentRedirectedCalls'] . "</maxConcurrentRedirectedCalls>";
		}else{
			$xmlinputCall .= "<maxConcurrentRedirectedCalls></maxConcurrentRedirectedCalls>";
		}
		
		$xmlinputCall .= "<useMaxFindMeFollowMeDepth>" . $postArray['useMaxFindMeFollowMeDepth']. "</useMaxFindMeFollowMeDepth>";
		
		$xmlinputCall .= "<maxFindMeFollowMeDepth>" . $postArray['maxFindMeFollowMeDepth']. "</maxFindMeFollowMeDepth>";
		$xmlinputCall .= "<maxRedirectionDepth>" . $postArray['maxRedirectionDepth']. "</maxRedirectionDepth>";
		$xmlinputCall .= "<useMaxConcurrentFindMeFollowMeInvocations>" . $postArray['useMaxConcurrentFindMeFollowMeInvocations']. "</useMaxConcurrentFindMeFollowMeInvocations>";
		
		
		if(isset($postArray['maxConcurrentFindMeFollowMeInvocations'])){
			$xmlinputCall .= "<maxConcurrentFindMeFollowMeInvocations>" . $postArray['maxConcurrentFindMeFollowMeInvocations'] . "</maxConcurrentFindMeFollowMeInvocations>";
		}else{
			$xmlinputCall .= "<maxConcurrentFindMeFollowMeInvocations></maxConcurrentFindMeFollowMeInvocations>";
		}
		
		$xmlinputCall .= "<clidPolicy>" . $postArray['clidPolicy']. "</clidPolicy>";
		$xmlinputCall .= "<emergencyClidPolicy>" . $postArray['emergencyClidPolicy']. "</emergencyClidPolicy>";
		$xmlinputCall .= "<allowAlternateNumbersForRedirectingIdentity>" . $postArray['allowAlternateNumbersForRedirectingIdentity']. "</allowAlternateNumbersForRedirectingIdentity>";
		$xmlinputCall .= "<useGroupName>" . $postArray['useGroupName'] . "</useGroupName>";
		
		$xmlinputCall .= "<blockCallingNameForExternalCalls>" . $postArray['blockCallingNameForExternalCalls']. "</blockCallingNameForExternalCalls>";
		$xmlinputCall .= "<enableDialableCallerID>" . $postArray['enableDialableCallerID']. "</enableDialableCallerID>";
		$xmlinputCall .= "<allowConfigurableCLIDForRedirectingIdentity>" . $postArray['allowConfigurableCLIDForRedirectingIdentity']. "</allowConfigurableCLIDForRedirectingIdentity>";
		$xmlinputCall .= "<allowDepartmentCLIDNameOverride>" . $postArray['allowDepartmentCLIDNameOverride']. "</allowDepartmentCLIDNameOverride>";
		
		$xmlinputCall .= xmlFooter();
		
		$responseCall = $client->processOCIMessage(array("in0" => $xmlinputCall));
		$xmlCall = new SimpleXMLElement($responseCall->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xmlCall) != "") {
			$callProcessingModifyResponse["Error"] = $xmlCall->command->summaryEnglish;
			$callProcessingModifyResponse["Error"]['Detail'] = strval($xmlCall->command->detail);
		}else{
			$callProcessingModifyResponse["Success"] = $xmlCall->command;
			
		}
		return $callProcessingModifyResponse;
		
	}
	
	public function getGroupRoutingProfile(){
		global  $sessionid, $client;
		$groupRoutingInfoResponse["Error"] = "";
		$groupRoutingInfoResponse["Success"] = "";
		
		if(!empty($this->groupId)){
			$xmlinput = xmlHeader($sessionid, "GroupRoutingProfileGetRequest");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId) . "</groupId>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if (readErrorXmlGenuine($xml) != "") {
				$groupRoutingInfoResponse["Error"] = $xml->command->summaryEnglish;
			}else{
				$groupRoutingInfoResponse["Success"] = $xml->command;
				
			}
		}
		return $groupRoutingInfoResponse;
	}
	
	public function getGroupCallProcessingPolicy(){
		global  $sessionid, $client;
		$groupCallProcessingResponse["Error"] = "";
		$groupCallProcessingResponse["Success"] = "";
		
		if(!empty($this->groupId)){
			$xmlinput = xmlHeader($sessionid, "GroupCallProcessingGetPolicyRequest18");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId). "</groupId>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if (readErrorXmlGenuine($xml) != "") {
				$groupCallProcessingResponse["Error"] = $xml->command->summaryEnglish;
			}else{
				$groupCallProcessingResponse["Success"] = $xml->command;
				
			}
		}
		return $groupCallProcessingResponse;
	}
	
	public function getStateList(){
		global  $sessionid, $client;
		$stateListResponse["Error"] = "";
		$stateListResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "SystemStateOrProvinceGetListRequest");
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$stateListResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$a = 0;
			foreach ($xml->command->stateOrProvinceTable->row as $key => $value)
			{
				$states[$a] = strval($value->col[0]);
				$a++;
			}
			$stateListResponse["Success"] = $states;
		}
		return $stateListResponse;
	}
	
	public function getRoutingProfileList(){
		global  $sessionid, $client;
		$routingInfoResponse["Error"] = "";
		$routingInfoResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "SystemRoutingProfileGetListRequest");
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$routingInfoResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$routingInfoResponse["Success"] = $xml->command;
		}
		
		return $routingInfoResponse;
	}
	
	public function getTimeZoneList($ociVersion){
		global $sessionid, $client;
		$timeZoneResponse["Error"] = "";
		$timeZoneResponse["Success"] = "";
		
		if ($ociVersion == "20")
		{
			$xmlinput = xmlHeader($sessionid, "SystemTimeZoneGetListRequest20");
		}
		else
		{
			$xmlinput = xmlHeader($sessionid, "SystemTimeZoneGetListRequest");
		}
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$timeZoneResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			//echo "<pre>"; print_r($xml->command);
			$a = 0;
			foreach ($xml->command->timeZoneTable->row as $key => $value)
			{
				$timeZones[$a]['name'] = strval($value->col[0]);
				$timeZones[$a]['displayName'] = strval($value->col[1]);
				$a++;
			}
			$timeZoneResponse["Success"] = $timeZones;
		}
		
		return $timeZoneResponse;
	}
	
	public function getDeleteGroup($postArray){
		global  $sessionid, $client;
		$deleteGroup["Error"] = "";
		$deleteGroup["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupDeleteRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']). "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			$deleteGroup["Error"] = $xml->command->summaryEnglish;
		}else{
			$deleteGroup["Success"] = $xml->command;
		}
		
		return $deleteGroup;
	}
	
	public function getServiceProviderDomainGetAssignedListRequest() {
		global  $sessionid, $client;
		
		$xmlinput = xmlHeader($sessionid, "ServiceProviderDomainGetAssignedListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			$domainListResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$a = 0;
			foreach ($xml->command->domain as $key => $value)
			{
				$domain[$a] = strval($value);
				$a++;
			}
			$domainListResponse["Success"] = $domain;
			
		}
		return $domainListResponse;
	}
	
	public function getSecurityDomain($array, $securityDomainPattern) {
		global  $sessionid, $client;
		$securityDomain = array();
		if(count($array) > 0){
			foreach ($array as $domain) {
				if ($securityDomainPattern <> "" && strpos($domain, $securityDomainPattern) !== false ) {
					$securityDomain[] = $domain;
				}
			}
		}
		return $securityDomain;
	}
	
	public function getAvailableDomain($array, $securityDomainPattern) {
		global  $sessionid, $client;
		$availableDomain = array();
		if(count($array) > 0){
			foreach ($array as $domain) {
				if ($securityDomainPattern <> "" && strpos($domain, $securityDomainPattern) === false ) {
					$availableDomain[] = $domain;
				}
			}
		}
		return $availableDomain;
	}
	
	public function getGroupDomainGetAssignedListRequest() {
		global  $sessionid, $client;
		
		$xmlinput = xmlHeader($sessionid, "GroupDomainGetAssignedListRequest");
		$xmlinput .= "<serviceProviderId>".htmlspecialchars($_SESSION['sp'])."</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId). "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		
		if (readErrorXmlGenuine($xml) != "") {
			$domainListResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$a = 0;
			foreach ($xml->command->domain as $key => $value)
			{
				$domain[$a] = strval($value);
				$a++;
			}
			$domainListResponse["Success"] = $domain;
			
		}
		
		return $domainListResponse;
	}
	
	public function getDefaultDomainAssignList(){
		global  $sessionid, $client;
		
		$xmlinput = xmlHeader($sessionid, "GroupDomainGetAssignedListRequest");
		$xmlinput .= "<serviceProviderId>".htmlspecialchars($_SESSION['sp'])."</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId). "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			$domainListResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$a = 0;
			foreach ($xml->command->domain as $key => $value)
			{
				$domain[$a] = strval($value);
				$a++;
			}
			$domainListResponse["Success"] = $domain;
			
		}
		
		return $domainListResponse;
	}
	
	public function getGroupDnGetListRequest(){
		global  $sessionid, $client;
		$dnListResponse["Error"] = "";
		$dnListResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupDnGetListRequest");
		$xmlinput .= "<serviceProviderId>".htmlspecialchars($_SESSION['sp'])."</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId). "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml);
		if (readErrorXmlGenuine($xml) != "") {
			$dnListResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$a = 0;
			if(count($xml->command->phoneNumber) > 0){
				foreach ($xml->command->phoneNumber as $key => $value)
				{
					$phoneNumber[$a] = "+1-".strval($value);
					$a++;
				}
				$dnListResponse["Success"] = $phoneNumber;
			}
		}
		return $dnListResponse;
	}
	
	public function getVoicePortalGroupResponse(){
		global  $sessionid, $client;
		$voicePortalResponse["Error"] = "";
		$voicePortalResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupVoiceMessagingGroupGetVoicePortalRequest14");
		$xmlinput .= "<serviceProviderId>".htmlspecialchars($_SESSION['sp'])."</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId). "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			$voicePortalResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$voicePortalResponse["Success"]['serviceUserId'] = strval($xml->command->serviceUserId);
                        $voicePortalResponse["Success"]['voicePortalActiveStatus'] = strval($xml->command->isActive); //Code added @ 05 June 2019
			$voicePortalResponse["Success"]['name'] = strval($xml->command->serviceInstanceProfile->name);
			$voicePortalResponse["Success"]['callingLineIdLastName'] = strval($xml->command->serviceInstanceProfile->callingLineIdLastName);
			$voicePortalResponse["Success"]['callingLineIdFirstName'] = strval($xml->command->serviceInstanceProfile->callingLineIdFirstName);
			$voicePortalResponse["Success"]['portalPhoneNo'] = strval($xml->command->serviceInstanceProfile->phoneNumber);
			$voicePortalResponse["Success"]['portalExt'] = strval($xml->command->serviceInstanceProfile->extension);
			$voicePortalResponse["Success"]['portalLanguage'] = strval($xml->command->serviceInstanceProfile->language);
			$voicePortalResponse["Success"]['portalTimeZone'] = strval($xml->command->serviceInstanceProfile->timeZone);
			$voicePortalResponse["Success"]['portalTimeZoneDisplayName'] = strval($xml->command->serviceInstanceProfile->timeZoneDisplayName);
		}
		return $voicePortalResponse;
	}
}
