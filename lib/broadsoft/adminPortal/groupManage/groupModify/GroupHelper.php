<?php

/**
 * Created by Sologics.
 * Date: 25/07/2017
 */
class GroupHelper
{	
	//function to get the basic information of a group
	public function getGroupBasicInfoHelper($groupInfoData, $groupId){
		
		$groupInfoArray = array();
		if(empty($groupInfoData["Error"])){
			$groupInfoArray['groupName'] = strval($groupInfoData['Success']->groupName);
			$groupInfoArray['groupId'] = $groupId;
			$groupInfoArray['defaultDomain'] = strval($groupInfoData['Success']->defaultDomain);
			$groupInfoArray['userCount'] = strval($groupInfoData['Success']->userCount);
			$groupInfoArray['userLimit'] = strval($groupInfoData['Success']->userLimit);
			$groupInfoArray['callingLineIdName'] = strval($groupInfoData['Success']->callingLineIdName);
			$groupInfoArray['callingLineIdDisplayPhoneNumber'] = "+1-".strval($groupInfoData['Success']->callingLineIdDisplayPhoneNumber);
			$groupInfoArray['timeZone'] = strval($groupInfoData['Success']->timeZone);
			$groupInfoArray['timeZoneDisplayName'] = strval($groupInfoData['Success']->timeZoneDisplayName);
			$groupInfoArray['locationDialingCode'] = strval($groupInfoData['Success']->locationDialingCode);
			$groupInfoArray['contactName'] = strval($groupInfoData['Success']->contact->contactName);
			$groupInfoArray['contactNumber'] = strval($groupInfoData['Success']->contact->contactNumber);
			$groupInfoArray['contactEmail'] = strval($groupInfoData['Success']->contact->contactEmail);
			$groupInfoArray['addressLine1'] = strval($groupInfoData['Success']->address->addressLine1);
			$groupInfoArray['addressLine2'] = strval($groupInfoData['Success']->address->addressLine2);
			$groupInfoArray['city'] = strval($groupInfoData['Success']->address->city);
			$groupInfoArray['stateOrProvince'] = strval($groupInfoData['Success']->address->stateOrProvince);
			$groupInfoArray['zipOrPostalCode'] = strval($groupInfoData['Success']->address->zipOrPostalCode);
			$groupInfoArray['country'] = strval($groupInfoData['Success']->address->country);
		}		
		return $groupInfoArray;		
	}
	
	public function NullValueArray(){
		
	}
	
	//function to get the assign domain list take the parameter and return the array of assign domain
	public function getAssignDomainListHelper($assignDomainList){
		
		$groupInfoArray = array();
		if(empty($assignDomainList['Error'])){
			$groupInfoArray['assignDomainList'] = $assignDomainList['Success'];
		}else{
			$groupInfoArray['assignDomainList'] = array();
		}
		return $groupInfoArray;	
	}

	//function to get the assign dn list take the parameter and return the array of assign dn
	public function getGroupDnGetListRequestHelper($dnList){
		
		$groupInfoArray = array();
		if(empty($dnList['Error'])){
			$groupInfoArray['callingLineIdDisplayPhoneNumberList'] = $dnList['Success'];
			$groupInfoArray['assignNumbers'] = implode(",", $dnList['Success']);
		}
		return $groupInfoArray;	
	}

	//function to get the route list take the parameter and return the array of route
	public function getGroupRoutingProfileHelper($routeList){
		
		$groupInfoArray = array();
		if(empty($routeList['Error'])){
			$groupInfoArray['routingProfile'] = strval($routeList['Success']->routingProfile);
		}
		return $groupInfoArray;	
	}

	//function to get the call processing information, it takes the parameter and return the array of route
	public function getGroupCallProcessingPolicyHelper($callProcessingPolicy){
		$groupInfoArray = array();
		if(empty($callProcessingPolicy['Error'])){
			$groupInfoArray['useGroupCLIDSetting'] = strval($callProcessingPolicy['Success']->useGroupCLIDSetting);
			$groupInfoArray['useGroupMediaSetting'] = strval($callProcessingPolicy['Success']->useGroupMediaSetting);
			$groupInfoArray['useGroupCallLimitsSetting'] = strval($callProcessingPolicy['Success']->useGroupCallLimitsSetting);
			$groupInfoArray['useGroupTranslationRoutingSetting'] = strval($callProcessingPolicy['Success']->useGroupTranslationRoutingSetting);
			$groupInfoArray['useGroupDCLIDSetting'] = strval($callProcessingPolicy['Success']->useGroupDCLIDSetting);
			$groupInfoArray['useMaxSimultaneousCalls'] = strval($callProcessingPolicy['Success']->useMaxSimultaneousCalls);
			$groupInfoArray['maxSimultaneousCalls'] = strval($callProcessingPolicy['Success']->maxSimultaneousCalls);
			$groupInfoArray['useMaxSimultaneousVideoCalls'] = strval($callProcessingPolicy['Success']->useMaxSimultaneousVideoCalls);
			$groupInfoArray['maxSimultaneousVideoCalls'] = strval($callProcessingPolicy['Success']->maxSimultaneousVideoCalls);
			$groupInfoArray['useMaxCallTimeForAnsweredCalls'] = strval($callProcessingPolicy['Success']->useMaxCallTimeForAnsweredCalls);
			$groupInfoArray['maxCallTimeForAnsweredCallsMinutes'] = strval($callProcessingPolicy['Success']->maxCallTimeForAnsweredCallsMinutes);
			$groupInfoArray['useMaxCallTimeForUnansweredCalls'] = strval($callProcessingPolicy['Success']->useMaxCallTimeForUnansweredCalls);
			$groupInfoArray['maxCallTimeForUnansweredCallsMinutes'] = strval($callProcessingPolicy['Success']->maxCallTimeForUnansweredCallsMinutes);
			$groupInfoArray['mediaPolicySelection'] = strval($callProcessingPolicy['Success']->mediaPolicySelection);
			$groupInfoArray['networkUsageSelection'] = strval($callProcessingPolicy['Success']->networkUsageSelection);
			$groupInfoArray['enforceGroupCallingLineIdentityRestriction'] = strval($callProcessingPolicy['Success']->enforceGroupCallingLineIdentityRestriction);
			$groupInfoArray['allowEnterpriseGroupCallTypingForPrivateDialingPlan'] = strval($callProcessingPolicy['Success']->allowEnterpriseGroupCallTypingForPrivateDialingPlan);
			$groupInfoArray['overrideCLIDRestrictionForPrivateCallCategory'] = strval($callProcessingPolicy['Success']->overrideCLIDRestrictionForPrivateCallCategory);
			$groupInfoArray['useEnterpriseCLIDForPrivateCallCategory'] = strval($callProcessingPolicy['Success']->useEnterpriseCLIDForPrivateCallCategory);
			$groupInfoArray['enableEnterpriseExtensionDialing'] = strval($callProcessingPolicy['Success']->enableEnterpriseExtensionDialing);
			$groupInfoArray['useMaxConcurrentRedirectedCalls'] = strval($callProcessingPolicy['Success']->useMaxConcurrentRedirectedCalls);
			$groupInfoArray['maxConcurrentRedirectedCalls'] = strval($callProcessingPolicy['Success']->maxConcurrentRedirectedCalls);
			$groupInfoArray['useMaxFindMeFollowMeDepth'] = strval($callProcessingPolicy['Success']->useMaxFindMeFollowMeDepth);
			$groupInfoArray['maxFindMeFollowMeDepth'] = strval($callProcessingPolicy['Success']->maxFindMeFollowMeDepth);
			$groupInfoArray['maxRedirectionDepth'] = strval($callProcessingPolicy['Success']->maxRedirectionDepth);
			$groupInfoArray['useMaxConcurrentFindMeFollowMeInvocations'] = strval($callProcessingPolicy['Success']->useMaxConcurrentFindMeFollowMeInvocations);
			$groupInfoArray['maxConcurrentFindMeFollowMeInvocations'] = strval($callProcessingPolicy['Success']->maxConcurrentFindMeFollowMeInvocations);
			$groupInfoArray['clidPolicy'] = strval($callProcessingPolicy['Success']->clidPolicy);
			$groupInfoArray['emergencyClidPolicy'] = strval($callProcessingPolicy['Success']->emergencyClidPolicy);
			$groupInfoArray['allowAlternateNumbersForRedirectingIdentity'] = strval($callProcessingPolicy['Success']->allowAlternateNumbersForRedirectingIdentity);
			$groupInfoArray['useGroupName'] = strval($callProcessingPolicy['Success']->useGroupName);
			$groupInfoArray['blockCallingNameForExternalCalls'] = strval($callProcessingPolicy['Success']->blockCallingNameForExternalCalls);
			$groupInfoArray['enableDialableCallerID'] = strval($callProcessingPolicy['Success']->enableDialableCallerID);
			$groupInfoArray['allowConfigurableCLIDForRedirectingIdentity'] = strval($callProcessingPolicy['Success']->allowConfigurableCLIDForRedirectingIdentity);
			$groupInfoArray['allowDepartmentCLIDNameOverride'] = strval($callProcessingPolicy['Success']->allowDepartmentCLIDNameOverride);
		}
		
		return $groupInfoArray;	
	}
	
	//function to get the route list take the parameter and return the array of route
	public function getServiceProviderDomainGetAssignedListRequestHelper($spDomainList){
		
		$groupInfoArray = array();
		$allDomains = "";
		$allAssignedDomain = "";
		
		if(empty($spDomainList['Error'])){
			$groupInfoArray['allDomain'] = $spDomainList['Success'];
		}
		return $groupInfoArray;
	}
	
	//function to get the route list take the parameter and return the array of route
	public function getGroupDomainGetAssignedListRequestHelper($assigned){
		
		$groupInfoArray = array();
		if(empty($assigned['Error'])){
			$groupInfoArray['assignedDomain'] = $assigned['Success'];
		}
		return $groupInfoArray;
	}	

	//function to get the route list take the parameter and return the array of route
	public function getVoicePortalGroupResponseHelper($voicePortalResponse){
		
		$groupInfoArray = array();
		if(empty($voicePortalResponse['Error'])){
			//echo "<pre>"; print_r($voicePortalResponse); die;
			$groupInfoArray['portalPhoneNo'] = $voicePortalResponse['Success']['portalPhoneNo'];
			$groupInfoArray['portalExt'] = $voicePortalResponse['Success']['portalExt'];
			$groupInfoArray['portalLanguage'] = $voicePortalResponse['Success']['portalLanguage'];
			$groupInfoArray['portalTimeZone'] = $voicePortalResponse['Success']['portalTimeZone'];
			$groupInfoArray['portalTimeZoneDisplayName'] = $voicePortalResponse['Success']['portalTimeZoneDisplayName'];
			$groupInfoArray['callingLineIdLastName'] = $voicePortalResponse['Success']['callingLineIdLastName'];
			$groupInfoArray['callingLineIdFirstName'] = $voicePortalResponse['Success']['callingLineIdFirstName'];
			$groupInfoArray['name'] = $voicePortalResponse['Success']['name'];
			$groupInfoArray['serviceUserId'] = $voicePortalResponse['Success']['serviceUserId'];
                        $groupInfoArray['voicePortalActiveStatus'] = $voicePortalResponse['Success']['voicePortalActiveStatus'];
		}	
		return $groupInfoArray;
	}	

	//function to get the route list take the parameter and return the array of route
	public function getAvailableDomainHelper($domainAvailable, $field){
		
		$groupInfoArray = array();
		if(count($domainAvailable) > 0){
			$groupInfoArray[$field] = $domainAvailable;
		}
		return $groupInfoArray;
	}	

	//function to get the route list take the parameter and return the array of route
	public function getSecurityDomainHelper($domainAvailable, $field){
		
		$groupInfoArray = array();
		if(count($domainAvailable) > 0){
			$groupInfoArray[$field] = $domainAvailable;
		}else{
			$groupInfoArray[$field] = array();
		}
		return $groupInfoArray;
	}
        
        //Code added @ 21 Aug 2018
        public function getGroupOutgoingCallingPlanHelper($serviceProiverId, $groupId){
                
                require_once ("/var/www/lib/broadsoft/adminPortal/ocpFeatures/OCPOperations.php"); 
                $objOCP         = new OCPOperations();
                $ocpData        = $objOCP->groupCallingPlanGetOriginatingRequest(htmlspecialchars($_SESSION['sp']), $groupId);
                $groupInfoArray = array();
                
                global  $sessionid, $client;
                $ocpResponse["Error"] = array();
                $ocpResponse["Success"] = array();

                $xmlinput  = xmlHeader($sessionid, "GroupOutgoingCallingPlanOriginatingGetListRequest");
                $xmlinput .= "<serviceProviderId>". htmlspecialchars($serviceProiverId) ."</serviceProviderId>";        
                $xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
                $xmlinput .= xmlFooter();
                $response  = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);        

                if (readErrorXmlGenuine($xml) != "") {
                    $ocpResponse["Error"] = strval($xml->command->summaryEnglish);
                 }else{

                    $ocpResponse["Success"]["groupPermissions"] = (array) $xml->command->groupPermissions;

                    $groupInfoArray["groupPermissionsGroup"]                     = $ocpResponse["Success"]["groupPermissions"]["group"];
                    $groupInfoArray["groupPermissionsLocal"]                     = $ocpResponse["Success"]["groupPermissions"]["local"];
                    $groupInfoArray["groupPermissionsTollfree"]                  = $ocpResponse["Success"]["groupPermissions"]["tollFree"];
                    $groupInfoArray["groupPermissionsToll"]                      = $ocpResponse["Success"]["groupPermissions"]["toll"];

                    $groupInfoArray["groupPermissionsInternational"]             = $ocpResponse["Success"]["groupPermissions"]["international"];
                    $groupInfoArray["groupPermissionsOperatorAssisted"]          = $ocpResponse["Success"]["groupPermissions"]["operatorAssisted"];
                    $groupInfoArray["groupPermissionsOperatorChargDirAssisted"]  = $ocpResponse["Success"]["groupPermissions"]["chargeableDirectoryAssisted"];
                    $groupInfoArray["groupPermissionsSpecialService1"]           = $ocpResponse["Success"]["groupPermissions"]["specialServicesI"];

                    $groupInfoArray["groupPermissionsSpecialService2"]           = $ocpResponse["Success"]["groupPermissions"]["specialServicesII"];
                    $groupInfoArray["groupPermissionsPremiumServices1"]          = $ocpResponse["Success"]["groupPermissions"]["premiumServicesI"];
                    $groupInfoArray["groupPermissionsPremiumServices2"]          = $ocpResponse["Success"]["groupPermissions"]["premiumServicesII"];
                    $groupInfoArray["groupPermissionsCasual"]                    = $ocpResponse["Success"]["groupPermissions"]["casual"];

                    $groupInfoArray["groupPermissionsUrlDialing"]                = $ocpResponse["Success"]["groupPermissions"]["urlDialing"];
                    $groupInfoArray["groupPermissionsUnknown"]                   = $ocpResponse["Success"]["groupPermissions"]["unknown"];
                 }                 
                 return $groupInfoArray;                
	}
        //End code


}