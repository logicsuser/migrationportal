<?php

/**
 * Created by Sologics.
 * Date: 12/05/2017
 */
class GroupOperation
{
	public $groupId;
	public $groupInfo = array();
	public $groupInfoResponse = array();
	public $groupRoutingResponse= array();
	public $routingResponse= array();
	
	public function __construct($sp, $groupId) {
		global  $sessionid, $client;
		$this->groupId = $groupId;
		$this->sp = $sp;
	}
	
	
	//function to get the basic information of the group
	public function getGroupBasicInfo(){
		global  $sessionid, $client;
		$groupInfoResponse["Error"] = "";
		$groupInfoResponse["Success"] = "";
		
		if(!empty($this->groupId)){
			$xmlinput = xmlHeader($sessionid, "GroupGetRequest14sp7");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($this->sp) . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId). "</groupId>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			
			if (readErrorXmlGenuine($xml) != "") {
				$groupInfoResponse["Error"] = $xml->command->summaryEnglish;
			}else{
				$groupInfoResponse["Success"] = $xml->command;
				
			}
		}
		return $groupInfoResponse;
		
	}
	
	public function getAvailableDomains($allDomains, $allAssignedDomain){
		
		$groupInfoArray = array();
		if(count($allDomains) > 0 && count($allAssignedDomain) > 0){
			$groupInfoArray['groupInfoData']['available'] = array_values(array_diff($allDomains['Success'], $allAssignedDomain['Success']));
		}
		return $groupInfoArray;
	}
	
	//function to get group list with OCI command for the service provider
	public function getGroupList() {
		global  $sessionid, $client;
		
		$xmlinput = xmlHeader($sessionid, "GroupGetListInServiceProviderRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($this->sp) . "</serviceProviderId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		$allGroups = array();
		$a = 0;
		if (isset($xml->command->groupTable->row) && !empty($xml->command->groupTable->row))
		{
			foreach ($xml->command->groupTable->row as $key => $value)
			{
				if(strval($value->col[1]) != ""){
					$allGroups[$a] = strval($value->col[0])." <span class='search_separator'>-</span> ".strval($value->col[1])." ";
					$a++;
				}else{
					$allGroups[$a] = strval($value->col[0])." ";
					$a++;
				}
				
			}
		}
		return $allGroups;
	}

	//function to update basic info of the group
	public function groupBasicInfoOperation($postArray){
		
		global $sessionid, $client;
		$groupModifyResponse["Error"] = "";
		$groupModifyResponse["Success"] = "";
		
		if(!empty($postArray)){
		
			$xmlinput = xmlHeader($sessionid, "GroupModifyRequest");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($this->sp) . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId). "</groupId>";
			$xmlinput .= "<defaultDomain>" . $postArray['defaultDomain']. "</defaultDomain>";
			$xmlinput .= "<userLimit>" . $postArray['userLimit']. "</userLimit>";
			
			if(!empty($postArray['groupName'])){
				$xmlinput .= "<groupName>" . htmlspecialchars($postArray['groupName']). "</groupName>";
			}else{
				$xmlinput .= "<groupName xsi:nil='true'></groupName>";
			}
			
			if(!empty($postArray['callingLineIdName'])){
				$xmlinput .= "<callingLineIdName>" . htmlspecialchars($postArray['callingLineIdName']). "</callingLineIdName>";
			}else{
				$xmlinput .= "<callingLineIdName xsi:nil='true'></callingLineIdName>";
			}
			
			if(!empty($postArray['callingLineIdDisplayPhoneNumber'])){
				$xmlinput .= "<callingLineIdPhoneNumber>" . $postArray['callingLineIdDisplayPhoneNumber']. "</callingLineIdPhoneNumber>";
			}else{
				$xmlinput .= "<callingLineIdPhoneNumber xsi:nil='true'></callingLineIdPhoneNumber>";
			}
			
			$xmlinput .= "<timeZone>" . $postArray['timeZone']. "</timeZone>";
			if(!empty($postArray['locationDialingCode'])){
				$xmlinput .= "<locationDialingCode>" . $postArray['locationDialingCode']. "</locationDialingCode>";
			}else{
				$xmlinput .= "<locationDialingCode xsi:nil='true'></locationDialingCode>";
			}			
			
			$xmlinput .= "<contact>";
			if(!empty($postArray['contactName'])){
				$xmlinput .= "<contactName>" . $postArray['contactName']. "</contactName>";
			}else{
				$xmlinput .= "<contactName xsi:nil='true'></contactName>";
			}
			
			if(!empty($postArray['contactNumber'])){
				$xmlinput .= "<contactNumber>" . $postArray['contactNumber']. "</contactNumber>";
			}else{
				$xmlinput .= "<contactNumber xsi:nil='true'></contactNumber>";
			}
			
			if(!empty($postArray['contactEmail'])){
				$xmlinput .= "<contactEmail>" . $postArray['contactEmail']. "</contactEmail>";
			}else{
				$xmlinput .= "<contactEmail xsi:nil='true'></contactEmail>";
			}
			
			$xmlinput .= "</contact>";
			
			$xmlinput .= "<address>";
			
			if(!empty($postArray['addressLine1'])){
				$xmlinput .= "<addressLine1>" . $postArray['addressLine1']. "</addressLine1>";
			}else{
				$xmlinput .= "<addressLine1 xsi:nil='true'></addressLine1>";
			}
			
			if(!empty($postArray['addressLine2'])){
				$xmlinput .= "<addressLine2>" . $postArray['addressLine2']. "</addressLine2>";
			}else{
				$xmlinput .= "<addressLine2 xsi:nil='true'></addressLine2>";
			}
			
			if(!empty($postArray['city'])){
				$xmlinput .= "<city>" . $postArray['city']. "</city>";
			}else{
				$xmlinput .= "<city xsi:nil='true'></city>";
			}
			
			if(!empty($postArray['stateOrProvince'])){
				$xmlinput .= "<stateOrProvince>" . $postArray['stateOrProvince']. "</stateOrProvince>";
			}else{
				$xmlinput .= "<stateOrProvince xsi:nil='true'></stateOrProvince>";
			}
			
			if(!empty($postArray['zipOrPostalCode'])){
				$xmlinput .= "<zipOrPostalCode>" . $postArray['zipOrPostalCode']. "</zipOrPostalCode>";
			}else{
				$xmlinput .= "<zipOrPostalCode xsi:nil='true'></zipOrPostalCode>";
			}
			
			if(!empty($postArray['country'])){
				$xmlinput .= "<country>" . $postArray['country']. "</country>";
			}else{
				$xmlinput .= "<country xsi:nil='true'></country>";
			}
			
			$xmlinput .= "</address>";
			
			$xmlinput .= xmlFooter();
			
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if (readErrorXmlGenuine($xml) != "") {
				
				if(isset($xml->command->detail)){
					$groupModifyResponse["Error"]['Detail'] = strval($xml->command->detail);
				}else{
					$groupModifyResponse["Error"]['Detail'] = strval($xml->command->summaryEnglish);
				
				}
			}else{
				$groupModifyResponse["Success"] = $xml->command;	
			}
		}
		return $groupModifyResponse;
	}

	//function to update routing profile of the group
	public function groupRoutingProfileOperation($postArray){
		
		global  $sessionid, $client;
		$routingResponse['Error'] = "";
		$routingResponse['Success'] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupRoutingProfileModifyRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($this->sp) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId). "</groupId>";
		
		if(!empty($postArray['routingProfile'])){
			$xmlinput .= "<routingProfile>" . $postArray['routingProfile']. "</routingProfile>";
		}else{
			$xmlinput .= "<routingProfile xsi:nil='true'></routingProfile>";
		}
		
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			$routing["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			$routing["Success"] = $xml->command;
			
		}
		return $routingResponse;
	}
	
	public function modifyAssignDomain($postArray){
		global  $sessionid, $client;
		$assignedDomains = array();
		$securityAssignedDomains = array();
		
		$assignDomainResponse['Error'] = "";
		$assignDomainResponse['Success'] = "";
		$newArray = array();
		
		if(!empty($postArray['domainAssignList'])){
			$assignedDomains = explode(";", substr($postArray['domainAssignList'], 0, -1));
		}
		if(!empty($postArray['securityDomainAssignList'])){
			$securityAssignedDomains = explode(";", substr($postArray['securityDomainAssignList'], 0, -1));
		}
		$domainListArray = array_merge($assignedDomains, $securityAssignedDomains);
		if(count($domainListArray) > 0){
			
			foreach($domainListArray as $key=>$val){
				if(!in_array($val, $_SESSION['groupInfoData']['assignedDomain']) && $val <> $postArray['defaultDomain']){
					$newArray['assignDomain'][] = $val;
				}
			}
			
			foreach($_SESSION['groupInfoData']['assignedDomain'] as $key=>$val){
				if(!in_array($val, $domainListArray) && $val <> $postArray['defaultDomain']){
					$newArray['unAssignDomain'][] = $val;
				}
			}
			
			
		}
		return $newArray;
	}
	
	//function to update unassign domain
	public function updateUnAssignDomain($assignArray, $postArray){
		global  $sessionid, $client;
		
		$assignDomainResponse['Error'] = "";
		$assignDomainResponse['Success'] = "";
		$newArray = array();
		
		if(count($assignArray) > 0){
			
			$xmlinputUnAssignDomain  = xmlHeader($sessionid, "GroupDomainUnassignListRequest");
			$xmlinputUnAssignDomain.= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
			$xmlinputUnAssignDomain.= "<groupId>" . htmlspecialchars($postArray['groupId']). "</groupId>";
			foreach($assignArray as $key=>$val){
				if(!empty($val)){
					$xmlinputUnAssignDomain.= "<domain>".$val."</domain>";
				}
			}
			$xmlinputUnAssignDomain.= xmlFooter();
			
			$responseUnAssignDomain = $client->processOCIMessage(array("in0" => $xmlinputUnAssignDomain));
			$xmlUnAssignDomain = new SimpleXMLElement($responseUnAssignDomain->processOCIMessageReturn, LIBXML_NOWARNING);
			
			if (readErrorXmlGenuine($xmlUnAssignDomain) != "") {
				$assignDomainResponse["Error"] = $xmlUnAssignDomain->command->summaryEnglish;
				$assignDomainResponse["Error"]['Detail'] = $xmlUnAssignDomain->command->detail;
			}else{
				$assignDomainResponse["Success"] = $xmlUnAssignDomain->command;
			}
			
		}
		return $assignDomainResponse;
	}
	
	//function to assign domain
	public function updateAssignDomain($assignArray, $postArray){
		global  $sessionid, $client;
		
		$assignDomainResponse['Error'] = "";
		$assignDomainResponse['Success'] = "";
		$newArray = array();
		
		if(count($assignArray) > 0){
			
			$xmlinputDomain  = xmlHeader($sessionid, "GroupDomainAssignListRequest");
			$xmlinputDomain.= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
			$xmlinputDomain.= "<groupId>" . htmlspecialchars($postArray['groupId']). "</groupId>";
			
			foreach($assignArray as $key=>$val){
				if(!empty($val)){
					$xmlinputDomain .= "<domain>".$val."</domain>";
				}
			}
			
			$xmlinputDomain.= xmlFooter();
			$responseDomain = $client->processOCIMessage(array("in0" => $xmlinputDomain));
			
			$xmlDomain = new SimpleXMLElement($responseDomain->processOCIMessageReturn, LIBXML_NOWARNING);
			
			if (readErrorXmlGenuine($xmlDomain) != "") {
				$assignDomainResponse["Error"] = $xmlDomain->command->summaryEnglish;
				$assignDomainResponse["Error"]['Detail'] = $xmlDomain->command->detail;
			}else{
				$assignDomainResponse["Success"] = $xmlDomain->command;
			}
		}
		return $assignDomainResponse;
	}
	
	//function to update call processing policy
	public function groupCallProcessingOperation($postArray){
	//public function modifyCallProcessingPolicy($postArray){
		global  $sessionid, $client;
		$callProcessingModifyResponse["Error"] = "";
		$callProcessingModifyResponse["Success"] = "";
		
		//call Processing
		$xmlinputCall  = xmlHeader($sessionid, "GroupCallProcessingModifyPolicyRequest15sp2");
		$xmlinputCall .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinputCall .= "<groupId>" . htmlspecialchars($postArray['groupId']). "</groupId>";
		if(!isset($postArray['useGroupName'])){
			$postArray['useGroupName'] = "false";
		}
		if(!isset($postArray['allowDepartmentCLIDNameOverride'])){
			$postArray['allowDepartmentCLIDNameOverride'] = "false";
		}
		if(!isset($postArray['allowAlternateNumbersForRedirectingIdentity'])){
			$postArray['allowAlternateNumbersForRedirectingIdentity'] = "false";
		}
		if(!isset($postArray['allowConfigurableCLIDForRedirectingIdentity'])){
			$postArray['allowConfigurableCLIDForRedirectingIdentity'] = "false";
		}
		if(!isset($postArray['blockCallingNameForExternalCalls'])){
			$postArray['blockCallingNameForExternalCalls'] = "false";
		}
		if(!isset($postArray['useMaxSimultaneousCalls'])){
			$postArray['useMaxSimultaneousCalls'] = "false";
		}
		if(!isset($postArray['useMaxSimultaneousVideoCalls'])){
			$postArray['useMaxSimultaneousVideoCalls'] = "false";
		}
		if(!isset($postArray['useMaxCallTimeForAnsweredCalls'])){
			$postArray['useMaxCallTimeForAnsweredCalls'] = "false";
		}
		if(!isset($postArray['useMaxCallTimeForUnansweredCalls'])){
			$postArray['useMaxCallTimeForUnansweredCalls'] = "false";
		}
		if(!isset($postArray['useMaxConcurrentRedirectedCalls'])){
			$postArray['useMaxConcurrentRedirectedCalls'] = "false";
		}
		if(!isset($postArray['useMaxConcurrentFindMeFollowMeInvocations'])){
			$postArray['useMaxConcurrentFindMeFollowMeInvocations'] = "false";
		}
		if(!isset($postArray['useMaxFindMeFollowMeDepth'])){
			$postArray['useMaxFindMeFollowMeDepth'] = "false";
		}
		if(!isset($postArray['enforceGroupCallingLineIdentityRestriction'])){
			$postArray['enforceGroupCallingLineIdentityRestriction'] = "false";
		}
		if(!isset($postArray['allowEnterpriseGroupCallTypingForPrivateDialingPlan'])){
			$postArray['allowEnterpriseGroupCallTypingForPrivateDialingPlan'] = "false";
		}
		if(!isset($postArray['allowEnterpriseGroupCallTypingForPublicDialingPlan'])){
			$postArray['allowEnterpriseGroupCallTypingForPublicDialingPlan'] = "false";
		}
		if(!isset($postArray['overrideCLIDRestrictionForPrivateCallCategory'])){
			$postArray['overrideCLIDRestrictionForPrivateCallCategory'] = "false";
		}
		
		if(isset($postArray['useGroupName'])){
			$useGroupName= "true";
			if(isset($postArray['allowDepartmentCLIDNameOverride'])){
				$allowDepartmentCLIDNameOverride= "true";
			}else{
				$allowDepartmentCLIDNameOverride= "false";
			}
			
		}else{
			$useGroupName= "false";
			$allowDepartmentCLIDNameOverride= "false";
		}
		
		$xmlinputCall .= "<useGroupCLIDSetting>" . $postArray['useGroupCLIDSetting']. "</useGroupCLIDSetting>";
		$xmlinputCall .= "<useGroupMediaSetting>" . $postArray['useGroupMediaSetting']. "</useGroupMediaSetting>";
		$xmlinputCall .= "<useGroupCallLimitsSetting>" . $postArray['useGroupCallLimitsSetting']. "</useGroupCallLimitsSetting>";
		$xmlinputCall .= "<useGroupTranslationRoutingSetting>" . $postArray['useGroupTranslationRoutingSetting']. "</useGroupTranslationRoutingSetting>";
		$xmlinputCall .= "<useGroupDCLIDSetting>" . $postArray['useGroupDCLIDSetting'] . "</useGroupDCLIDSetting>";
		$xmlinputCall .= "<useMaxSimultaneousCalls>" . $postArray['useMaxSimultaneousCalls']. "</useMaxSimultaneousCalls>";
		$xmlinputCall .= "<maxSimultaneousCalls>" . $postArray['maxSimultaneousCalls']. "</maxSimultaneousCalls>";
		$xmlinputCall .= "<useMaxSimultaneousVideoCalls>" . $postArray['useMaxSimultaneousVideoCalls']. "</useMaxSimultaneousVideoCalls>";
		$xmlinputCall .= "<maxSimultaneousVideoCalls>" . $postArray['maxSimultaneousVideoCalls']. "</maxSimultaneousVideoCalls>";
		$xmlinputCall .= "<useMaxCallTimeForAnsweredCalls>" . $postArray['useMaxCallTimeForAnsweredCalls']. "</useMaxCallTimeForAnsweredCalls>";
		$xmlinputCall .= "<maxCallTimeForAnsweredCallsMinutes>" . $postArray['maxCallTimeForAnsweredCallsMinutes']. "</maxCallTimeForAnsweredCallsMinutes>";
		$xmlinputCall .= "<useMaxCallTimeForUnansweredCalls>" . $postArray['useMaxCallTimeForUnansweredCalls']. "</useMaxCallTimeForUnansweredCalls>";
		$xmlinputCall .= "<maxCallTimeForUnansweredCallsMinutes>" . $postArray['maxCallTimeForUnansweredCallsMinutes']. "</maxCallTimeForUnansweredCallsMinutes>";
		
		$xmlinputCall .= "<mediaPolicySelection>" . $postArray['mediaPolicySelection'] . "</mediaPolicySelection>";
		
		if(isset($postArray['supportedMediaSetName'])){
			$xmlinputCall .= "<supportedMediaSetName xsi:nil='true' >" . $postArray['supportedMediaSetName'] . "</supportedMediaSetName>";
		}else{
			$xmlinputCall .= "<supportedMediaSetName xsi:nil='true' ></supportedMediaSetName>";
		}
		
		$xmlinputCall .= "<networkUsageSelection>" . $postArray['networkUsageSelection']. "</networkUsageSelection>";
		$xmlinputCall .= "<enforceGroupCallingLineIdentityRestriction>" . $postArray['enforceGroupCallingLineIdentityRestriction']. "</enforceGroupCallingLineIdentityRestriction>";
		
		$xmlinputCall .= "<allowEnterpriseGroupCallTypingForPrivateDialingPlan>" . $postArray['allowEnterpriseGroupCallTypingForPrivateDialingPlan']. "</allowEnterpriseGroupCallTypingForPrivateDialingPlan>";
		$xmlinputCall .= "<allowEnterpriseGroupCallTypingForPublicDialingPlan>" . $postArray['allowEnterpriseGroupCallTypingForPublicDialingPlan'] . "</allowEnterpriseGroupCallTypingForPublicDialingPlan>";
		$xmlinputCall .= "<overrideCLIDRestrictionForPrivateCallCategory>" . $postArray['overrideCLIDRestrictionForPrivateCallCategory']. "</overrideCLIDRestrictionForPrivateCallCategory>";
		//$xmlinputCall .= "<useEnterpriseCLIDForPrivateCallCategory>" . $postArray['useEnterpriseCLIDForPrivateCallCategory']. "</useEnterpriseCLIDForPrivateCallCategory>";
		//$xmlinputCall .= "<enableEnterpriseExtensionDialing>" . $postArray['enableEnterpriseExtensionDialing']. "</enableEnterpriseExtensionDialing>";
		$xmlinputCall .= "<useMaxConcurrentRedirectedCalls>" . $postArray['useMaxConcurrentRedirectedCalls'] . "</useMaxConcurrentRedirectedCalls>";
		
		if(isset($postArray['maxConcurrentRedirectedCalls'])){
			$xmlinputCall .= "<maxConcurrentRedirectedCalls>" . $postArray['maxConcurrentRedirectedCalls'] . "</maxConcurrentRedirectedCalls>";
		}else{
			$xmlinputCall .= "<maxConcurrentRedirectedCalls></maxConcurrentRedirectedCalls>";
		}
		
		$xmlinputCall .= "<useMaxFindMeFollowMeDepth>" . $postArray['useMaxFindMeFollowMeDepth']. "</useMaxFindMeFollowMeDepth>";
		
		$xmlinputCall .= "<maxFindMeFollowMeDepth>" . $postArray['maxFindMeFollowMeDepth']. "</maxFindMeFollowMeDepth>";
		$xmlinputCall .= "<maxRedirectionDepth>" . $postArray['maxRedirectionDepth']. "</maxRedirectionDepth>";
		$xmlinputCall .= "<useMaxConcurrentFindMeFollowMeInvocations>" . $postArray['useMaxConcurrentFindMeFollowMeInvocations']. "</useMaxConcurrentFindMeFollowMeInvocations>";
		
		
		if(isset($postArray['maxConcurrentFindMeFollowMeInvocations'])){
			$xmlinputCall .= "<maxConcurrentFindMeFollowMeInvocations>" . $postArray['maxConcurrentFindMeFollowMeInvocations'] . "</maxConcurrentFindMeFollowMeInvocations>";
		}else{
			$xmlinputCall .= "<maxConcurrentFindMeFollowMeInvocations></maxConcurrentFindMeFollowMeInvocations>";
		}
		
		if(isset($postArray['clidPolicy'])){
			$xmlinputCall .= "<clidPolicy>" . $postArray['clidPolicy']. "</clidPolicy>";
		}
		if(isset($postArray['emergencyClidPolicy'])){
			$xmlinputCall .= "<emergencyClidPolicy>" . $postArray['emergencyClidPolicy']. "</emergencyClidPolicy>";
		}
		$xmlinputCall .= "<allowAlternateNumbersForRedirectingIdentity>" . $postArray['allowAlternateNumbersForRedirectingIdentity']. "</allowAlternateNumbersForRedirectingIdentity>";
		$xmlinputCall .= "<useGroupName>" . $postArray['useGroupName'] . "</useGroupName>";
		
		$xmlinputCall .= "<blockCallingNameForExternalCalls>" . $postArray['blockCallingNameForExternalCalls']. "</blockCallingNameForExternalCalls>";
		$xmlinputCall .= "<enableDialableCallerID>" . $postArray['enableDialableCallerID']. "</enableDialableCallerID>";
		$xmlinputCall .= "<allowConfigurableCLIDForRedirectingIdentity>" . $postArray['allowConfigurableCLIDForRedirectingIdentity']. "</allowConfigurableCLIDForRedirectingIdentity>";
		$xmlinputCall .= "<allowDepartmentCLIDNameOverride>" . $postArray['allowDepartmentCLIDNameOverride']. "</allowDepartmentCLIDNameOverride>";
		
		$xmlinputCall .= xmlFooter();
		
		$responseCall = $client->processOCIMessage(array("in0" => $xmlinputCall));
		$xmlCall = new SimpleXMLElement($responseCall->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xmlCall); die;
		if (readErrorXmlGenuine($xmlCall) != "") {
			if(empty($xmlCall->command->detail)){
				$callProcessingModifyResponse["Error"] = strval($xmlCall->command->summaryEnglish);
			}else{
				$callProcessingModifyResponse["Error"]['Detail'] = strval($xmlCall->command->detail);
			}
		}else{
			$callProcessingModifyResponse["Success"] = $xmlCall->command;
			
		}
		return $callProcessingModifyResponse;
		
	}
	
	public function getGroupRoutingProfile(){
		global  $sessionid, $client;
		$groupRoutingInfoResponse["Error"] = "";
		$groupRoutingInfoResponse["Success"] = "";
		
		if(!empty($this->groupId)){
			$xmlinput = xmlHeader($sessionid, "GroupRoutingProfileGetRequest");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId) . "</groupId>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if (readErrorXmlGenuine($xml) != "") {
				$groupRoutingInfoResponse["Error"] = $xml->command->summaryEnglish;
			}else{
				$groupRoutingInfoResponse["Success"] = $xml->command;
				
			}
		}
		return $groupRoutingInfoResponse;
	}
	
	public function getGroupCallProcessingPolicy(){
		global  $sessionid, $client;
		$groupCallProcessingResponse["Error"] = "";
		$groupCallProcessingResponse["Success"] = "";
		
		if(!empty($this->groupId)){
			$xmlinput = xmlHeader($sessionid, "GroupCallProcessingGetPolicyRequest18");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($this->sp) . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId) . "</groupId>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if (readErrorXmlGenuine($xml) != "") {
				$groupCallProcessingResponse["Error"] = $xml->command->summaryEnglish;
			}else{
				$groupCallProcessingResponse["Success"] = $xml->command;
				
			}
		}
		return $groupCallProcessingResponse;
	}
	
	public function getStateList(){
		global  $sessionid, $client;
		$stateListResponse["Error"] = "";
		$stateListResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "SystemStateOrProvinceGetListRequest");
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$stateListResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$a = 0;
			foreach ($xml->command->stateOrProvinceTable->row as $key => $value)
			{
				$states[$a] = strval($value->col[0]);
				$a++;
			}
			$stateListResponse["Success"] = $states;
		}
		return $stateListResponse;
	}
	
	public function getRoutingProfileList(){
		global  $sessionid, $client;
		$routingInfoResponse["Error"] = "";
		$routingInfoResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "SystemRoutingProfileGetListRequest");
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$routingInfoResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$routingInfoResponse["Success"] = $xml->command;
		}
		
		return $routingInfoResponse;
	}
	
	public function getTimeZoneList($ociVersion){
		global $sessionid, $client;
		$timeZoneResponse["Error"] = "";
		$timeZoneResponse["Success"] = "";
        
        $versionArray = array("20", "21", "22");
                
		if (in_array($ociVersion, $versionArray))

		{
			$xmlinput = xmlHeader($sessionid, "SystemTimeZoneGetListRequest20");
		}
		else
		{
			$xmlinput = xmlHeader($sessionid, "SystemTimeZoneGetListRequest");
		}
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$timeZoneResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			//echo "<pre>"; print_r($xml->command);
			$a = 0;
			foreach ($xml->command->timeZoneTable->row as $key => $value)
			{
				$timeZones[$a]['name'] = strval($value->col[0]);
				$timeZones[$a]['displayName'] = strval($value->col[1]);
				$a++;
			}
			$timeZoneResponse["Success"] = $timeZones;
		}
		
		return $timeZoneResponse;
	}
	
	public function getDeleteGroup($postArray){
		global  $sessionid, $client;
		$deleteGroup["Error"] = "";
		$deleteGroup["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupDeleteRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']). "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			$deleteGroup["Error"] = $xml->command->summaryEnglish;
		}else{
			$deleteGroup["Success"] = $xml->command;
		}
		
		return $deleteGroup;
	}
	
	public function getServiceProviderDomainGetAssignedListRequest() {
		global  $sessionid, $client;
		
		$xmlinput = xmlHeader($sessionid, "ServiceProviderDomainGetAssignedListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($this->sp) . "</serviceProviderId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			$domainListResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$a = 0;
			foreach ($xml->command->domain as $key => $value)
			{
				$domain[$a] = strval($value);
				$a++;
			}
			$domainListResponse["Success"] = $domain;
			
		}
		return $domainListResponse;
	}
	
	public function getSecurityDomain($array, $securityDomainPattern) {
		global  $sessionid, $client;
		$securityDomain = array();
		
		foreach ($array as $domain) {
		    if (!empty($securityDomainPattern) && strpos($domain, $securityDomainPattern) !== false ) {
				$securityDomain[] = $domain;
			}
		}
		
		return $securityDomain;
	}
	
	public function getAvailableDomain($array, $securityDomainPattern) {
		global  $sessionid, $client;
		$availableDomain = array();
		
		foreach ($array as $domain) {
		    if (!empty($securityDomainPattern) && strpos($domain, $securityDomainPattern) === false ) {
				$availableDomain[] = $domain;
			}
		}
		
		return $availableDomain;
	}
        
    public function getAvailableDomainsBasedOnSPattern($array, $securityDomainPattern) {		
	$availableDomain = array();
	if(!empty($securityDomainPattern)){
                    foreach ($array as $domain) {
                        if (!empty($securityDomainPattern) && strpos($domain, $securityDomainPattern) === false ) {
                                    $availableDomain[] = $domain;
                            }
                    }
            }else{
                 foreach ($array as $domain) {
                        $availableDomain[] = $domain;
                 }
            }		
	return $availableDomain;
	}
	
	public function getGroupDomainGetAssignedListRequest() {
		global  $sessionid, $client;
		
		$xmlinput = xmlHeader($sessionid, "GroupDomainGetAssignedListRequest");
		$xmlinput .= "<serviceProviderId>".htmlspecialchars($_SESSION['sp'])."</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId) . "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		
		if (readErrorXmlGenuine($xml) != "") {
			$domainListResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$a = 0;
			foreach ($xml->command->domain as $key => $value)
			{
				$domain[$a] = strval($value);
				$a++;
			}
			$domainListResponse["Success"] = $domain;
			
		}
		
		return $domainListResponse;
	}
	
	public function getDefaultDomainAssignList(){
		global  $sessionid, $client;
		
		$xmlinput = xmlHeader($sessionid, "GroupDomainGetAssignedListRequest");
		$xmlinput .= "<serviceProviderId>".htmlspecialchars($this->sp)."</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId). "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			$domainListResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$a = 0;
			foreach ($xml->command->domain as $key => $value)
			{
				$domain[$a] = strval($value);
				$a++;
			}
			$domainListResponse["Success"] = $domain;
			
		}
		
		return $domainListResponse;
	}
	
	public function getGroupDnGetListRequest(){
		global  $sessionid, $client;
		$dnListResponse["Error"] = "";
		$dnListResponse["Success"] = "";
		$phoneNumber = array();
		
		$xmlinput = xmlHeader($sessionid, "GroupDnGetListRequest");
		$xmlinput .= "<serviceProviderId>".htmlspecialchars($this->sp)."</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId). "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml);
		if (readErrorXmlGenuine($xml) != "") {
			$dnListResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$dnListResponse["Success"] = $phoneNumber;
			$a = 0;
			if(count($xml->command->phoneNumber) > 0){
				foreach ($xml->command->phoneNumber as $key => $value)
				{
					$phoneNumber[$a] = "+1-".strval($value);
					$a++;
				}
				$dnListResponse["Success"] = $phoneNumber;
			}
		}
		//echo "<pre>"; print_r($dnListResponse); die;
		return $dnListResponse;
	}
	
	public function getVoicePortalGroupResponse(){
		global  $sessionid, $client;
		$voicePortalResponse["Error"] = "";
		$voicePortalResponse["Success"] = "";
		
		//$xmlinput = xmlHeader($sessionid, "GroupVoiceMessagingGroupGetVoicePortalRequest14");
                $xmlinput = xmlHeader($sessionid, "GroupVoiceMessagingGroupGetVoicePortalRequest19sp1");
		$xmlinput .= "<serviceProviderId>".htmlspecialchars($this->sp)."</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId). "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$voicePortalResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$voicePortalResponse["Success"]['serviceUserId'] = strval($xml->command->serviceUserId);
                        $voicePortalResponse["Success"]['voicePortalActiveStatus']   = strval($xml->command->isActive); //Code added @ 06 June 2019
			$voicePortalResponse["Success"]['name'] = strval($xml->command->serviceInstanceProfile->name);
			$voicePortalResponse["Success"]['callingLineIdLastName'] = strval($xml->command->serviceInstanceProfile->callingLineIdLastName);
			$voicePortalResponse["Success"]['callingLineIdFirstName'] = strval($xml->command->serviceInstanceProfile->callingLineIdFirstName);
			$voicePortalResponse["Success"]['portalPhoneNo'] = strval($xml->command->serviceInstanceProfile->phoneNumber);
			$voicePortalResponse["Success"]['portalExt'] = strval($xml->command->serviceInstanceProfile->extension);
			$voicePortalResponse["Success"]['portalLanguage'] = strval($xml->command->serviceInstanceProfile->language);
			$voicePortalResponse["Success"]['portalTimeZone'] = strval($xml->command->serviceInstanceProfile->timeZone);
			$voicePortalResponse["Success"]['portalTimeZoneDisplayName'] = strval($xml->command->serviceInstanceProfile->timeZoneDisplayName);
		}
		return $voicePortalResponse;
	}

	public function groupErrorMsgArray(){
		$formDataArray = array(
				"defaultDomain" => "Default Domain",
				"userLimit" => "User Limit",
				"userCount" => "User Count",
				"groupName" => "Group Name",
				"callingLineIdName" => "Calling Line ID Name",
				"callingLineIdDisplayPhoneNumber" => "Calling Line ID Group	Number",
				"timeZone" => "Time Zone",
				"timeZoneDisplayName" => "Time Zone Display Name",
				"locationDialingCode" => "Location Dialing Code",
				"contactName" => "Contact Name",
				"contactEmail" => "Contact Email",
				"contactNumber" => "Contact Number",
				"addressLine1" => "Address Line1",
				"addressLine2" => "Address Line2",
				"city" => "City",
				"stateOrProvince" => "State / Province",
				"zipOrPostalCode" => "Zip / Postal Code",
				"country" => "Country",
				"routingProfile" => "Routing Profile",
				"useGroupName" => "Use group name for Calling Line Identity",
				"allowDepartmentCLIDNameOverride" => "Allow Department Name Override",
				"useGroupCLIDSetting" => "Use Group Calling Line Id Policy",
				"clidPolicy" => "Non-Emergency Calls",
				"emergencyClidPolicy" => "Emergency Calls",
				"allowAlternateNumbersForRedirectingIdentity" => "Allow Alternate Numbers for Redirecting Identity",
				"allowConfigurableCLIDForRedirectingIdentity" => "Allow Configurable CLID for Redirecting Identity",
				"blockCallingNameForExternalCalls" => "Block Calling Name for External Calls",
				"useGroupMediaSetting" => "Group Media Setting",
				"mediaPolicySelection" => "Media Policy",
				"supportedMediaSetName" => "Use Supported Media",
				"useGroupCallLimitsSetting" => "Call Limits Policy",
				"useMaxSimultaneousCalls" => "Enable Maximum Number of Concurrent Calls",
				"maxSimultaneousCalls" => "Number of Concurrent Calls",
				"useMaxSimultaneousVideoCalls" => "Enable Maximum Number of Concurrent Video Calls",
				"maxSimultaneousVideoCalls" => "Number of Concurrent Video Calls",
				"useMaxCallTimeForAnsweredCalls" => "Enable Maximum Duration for Answered Calls",
				"maxCallTimeForAnsweredCallsMinutes" => "Duration for Answered Calls",
				"useMaxCallTimeForUnansweredCalls" => "Enable Maximum Duration for Unanswered Calls",
				"maxCallTimeForUnansweredCallsMinutes" => "Duration for Unanswered Calls",
				"useMaxConcurrentRedirectedCalls" => "Enable Maximum Number of Concurrent Redirected Calls",
				"maxConcurrentRedirectedCalls" => "Number of Concurrent Redirected Calls",
				"useMaxConcurrentFindMeFollowMeInvocations" => "Enable Maximum Number of Concurrent Find Me/Follow Me Invocations",
				"maxConcurrentFindMeFollowMeInvocations" => "Number of Concurrent Find Me/Follow Me Invocations",
				"useMaxFindMeFollowMeDepth" => "Enable Maximum Find Me/Follow Me Depth ",
				"maxFindMeFollowMeDepth" => "Find Me/Follow Me Depth",
				"maxRedirectionDepth" => "Maximum Redirection Depth",
				"useGroupTranslationRoutingSetting" => "Translation and Routing",
				"networkUsageSelection" => "Network Usage",
				"enforceGroupCallingLineIdentityRestriction" => "Enforce Group Calling Line Identity Restriction",
				"allowEnterpriseGroupCallTypingForPrivateDialingPlan" => "Allow Group Call Typing For Private Dialing Plan",
				"allowEnterpriseGroupCallTypingForPublicDialingPlan" => "Allow Group Call Typing For Public Dialing Plan",
				"overrideCLIDRestrictionForPrivateCallCategory" => "Override CLID Restriction For Private Call Category",
				"useGroupDCLIDSetting" => "Incoming Caller ID",
				"enableDialableCallerID" => "Dialable Caller ID",
				"domainAssigned" => "Domain Assign List",
                                "voicePortalActiveStatus" => "Voice Portal", //Code added @ 06 June 2019
				"securityDomainAssigned" => "Security Domain Assign List",
				"portalPhoneNo" => "Voice Portal Phone Number",
				"portalExt" => "Voice Portal Extension",
				"portalTimeZone" => "Voice Portal Time Zone",
				"portalLanguage" => "Voice Portal Language",
                                "extensionLength" => "Extension Length",
				"minExtensionLength" => "Min Extension Length",
				"maxExtensionLength" => "Max Extension length",
				"defaultExtensionLength" => "Default Extension Length",
				"isServicePackAuthAssignUpdated" => "New Service Pack Authorized",
				"isServicePackAuthUnAssignUpdated" => "Service Pack UnAuthorized",
				"isServicePackAsgnAssignUpdated" => "New Service Pack Assigned",
				"isServicePackAsgnUnassignUpdated" => "Service Pack Unassigned",
				"isGroupServiceAuthUpdated" => "Group Service Authorized",
				"isGroupServiceUnAuthUpdated" => "Group Service Unauthorized",
				"isGroupServiceAssignUpdated" => "Group Service Assigned",
				"isGroupServiceUnassignUpdated" => "Group Service Unassigned",
				"isUserServiceAuthUpdated" => "New User Service Authorized",
				"isUserServiceUnAuthUpdated" => "User Service UnAuthorized",
				"isDnsAssignUpdated" => "New DN Assigned",
				"isDnsUnAssignUpdated" => "DN Unassigned",
				"isNcsAssigned" => "New Network Class Services Assigned",
				"isNcsUnAssigned" => "Network Class Services Unassigned",
				"isNcsDefaultAssigned" => "New Default Network Class Services Assigned",
				"isNcsDefaultUnAssigned" => "Default Network Class Services Unassigned",
				"domainUnAssignList" => "Domain Unassign List ",
				"securityDomainUnAssignList" => "Security Domain Unassign List",
                                "groupPermissionsGroup" => "Calls within the business group",
                                "groupPermissionsLocal" => "Calls within the local calling area",
                                "groupPermissionsTollfree" => "Calls made to toll free numbers",
                                "groupPermissionsToll" => "Local toll calls",
                                "groupPermissionsInternational" => "International calls",
                                "groupPermissionsOperatorAssisted" => "Calls made with the chargeable assistance of an operator",
                                "groupPermissionsOperatorChargDirAssisted" => "Directory assistance calls",
                                "groupPermissionsSpecialService1" => "Special Services I (700 Number) calls",
                                "groupPermissionsSpecialService2" => "Special Services II",
                                "groupPermissionsPremiumServices1" => "Premium Services I (900 Number) calls",
                                "groupPermissionsPremiumServices2" => "Premium Services II (976 Number) calls",
                                "groupPermissionsCasual" => "1010XXX chargeable calls. Example: 1010321",
                                "groupPermissionsUrlDialing" => "Calls from internet",
                                "groupPermissionsUnknown" => "Unknown call type"
		);
		
		return $formDataArray;
	}
	
	//function to initialize session to check the changes has been done for the particular tab or not
	public function setTabSessionArray(){
		
		$_SESSION['isBasicGroupInfoUpdated'] = false;
		$_SESSION['isRoutingInfoUpdated'] = false;
		$_SESSION['isAssignDomainUpdated'] = false;
		$_SESSION['isCallProcessingUpdated'] = false;
		$_SESSION['isDnsAssignUpdated'] = false;
		$_SESSION['isDnsUnAssignUpdated'] = false;
		$_SESSION['isServicePackAuthAssignUpdated'] = false;
		$_SESSION['isServicePackAuthUnAssignUpdated'] = false;
		$_SESSION['isServicePackAsgnAssignUpdated'] = false;
		$_SESSION['isServicePackAsgnUnassignUpdated'] = false;
		$_SESSION['isGroupServiceAuthUpdated'] = false;
		$_SESSION['isGroupServiceUnAuthUpdated'] = false;
		$_SESSION['isGroupServiceAssignUpdated'] = false;
		$_SESSION['isGroupServiceUnassignUpdated'] = false;
		$_SESSION['isUserServiceAuthUpdated'] = false;
		$_SESSION['isUserServiceUnAuthUpdated'] = false;
		$_SESSION['isNcsAssigned'] = false;
		$_SESSION['isNcsDefaultAssigned'] = false;
		$_SESSION['isNcsUnAssigned'] = false;
		$_SESSION['isNcsDefaultUnAssigned'] = false;
		$_SESSION['isVoicePortalUpdated'] = false;
		$_SESSION['isExtensionLengthUpdated'] = false;
                $_SESSION['isOutgoingCallPlanUpdated'] = false;
                
	}
	
	//function to format post data to compare with session to check which fileds has been changed
	public function formatPostData($postArray){
		
		foreach($postArray as $key=>$val){
			if($val['name'] == "assignNumbers[]"){
// 				$postData['assignNumbers'][] = $val['value'];         /*Not using anymore*/
			}elseif(strpos($val['name'], 'servicePackAuth[]') !== false){
				$search= array("['", "']", "servicePackAuth[]");
				$replace = array("", "", "");
				$postData['servicePackAuth'][] = str_replace($search, $replace, $val['name']);
			}elseif(strpos($val['name'], 'servicePackAssign[]') !== false){
				$search= array("['", "']", "servicePackAssign[]");
				$replace = array("", "", "");
				$postData['servicePackAssign'][] = str_replace($search, $replace, $val['name']);
			}elseif(strpos($val['name'], 'groupServiceAuth[]') !== false){
				$search = array("['", "']", "groupServiceAuth[]");
				$replace = array("", "", "");
				$postData['groupServiceAuth'][] = str_replace($search, $replace, $val['name']);
			}elseif(strpos($val['name'], 'groupServiceAssign[]') !== false){
				$search = array("['", "']", "groupServiceAssign[]");
				$replace = array("", "", "");
				$postData['groupServiceAssign'][] = str_replace($search, $replace, $val['name']);
			}elseif(strpos($val['name'], 'userServiceAuth[]') !== false){
				$search = array("['", "']", "userServiceAuth[]");
				$replace = array("", "", "");
				$postData['userServiceAuth'][] = str_replace($search, $replace, $val['name']);
			}elseif($val['name'] == "assignedNCS[]"){
				$postData['assignedNCS'][] = $val['value'];
			}elseif($val['name'] == "assignedDefault[]"){
				$postData['assignedDefault'][] = $val['value'];
			}else{
				$postData[$val['name']] = $val['value'];
			}
		}
		if(!array_key_exists("servicePackAuth", $postData)){
			$postData['servicePackAuth'] = array();
		}
		if(!array_key_exists("groupServiceAuth", $postData)){
			$postData['groupServiceAuth'] = array();
		}
		if(!array_key_exists("groupServiceAssign", $postData)){
			$postData['groupServiceAssign'] = array();
		}
		if(!array_key_exists("userServiceAuth", $postData)){
			$postData['userServiceAuth'] = array();
		}
		if(!array_key_exists("assignNumbers", $postData)){
			$postData['assignNumbers'] = array();
		}
		
		if(!isset($postData['useGroupName'])){
			$postData['useGroupName'] = "false";
		}
		if(!isset($postData['allowDepartmentCLIDNameOverride'])){
			$postData['allowDepartmentCLIDNameOverride'] = "false";
		}
		if(!isset($postData['allowAlternateNumbersForRedirectingIdentity'])){
			$postData['allowAlternateNumbersForRedirectingIdentity'] = "false";
		}
		if(!isset($postData['allowConfigurableCLIDForRedirectingIdentity'])){
			$postData['allowConfigurableCLIDForRedirectingIdentity'] = "false";
		}
		if(!isset($postData['blockCallingNameForExternalCalls'])){
			$postData['blockCallingNameForExternalCalls'] = "false";
		}
		if(!isset($postData['useMaxSimultaneousCalls'])){
			$postData['useMaxSimultaneousCalls'] = "false";
		}
		if(!isset($postData['useMaxSimultaneousVideoCalls'])){
			$postData['useMaxSimultaneousVideoCalls'] = "false";
		}
		if(!isset($postData['useMaxCallTimeForAnsweredCalls'])){
			$postData['useMaxCallTimeForAnsweredCalls'] = "false";
		}
		if(!isset($postData['useMaxCallTimeForUnansweredCalls'])){
			$postData['useMaxCallTimeForUnansweredCalls'] = "false";
		}
		if(!isset($postData['useMaxConcurrentRedirectedCalls'])){
			$postData['useMaxConcurrentRedirectedCalls'] = "false";
		}
		if(!isset($postData['useMaxConcurrentFindMeFollowMeInvocations'])){
			$postData['useMaxConcurrentFindMeFollowMeInvocations'] = "false";
		}
		if(!isset($postData['useMaxFindMeFollowMeDepth'])){
			$postData['useMaxFindMeFollowMeDepth'] = "false";
		}
		if(!isset($postData['enforceGroupCallingLineIdentityRestriction'])){
			$postData['enforceGroupCallingLineIdentityRestriction'] = "false";
		}
		if(!isset($postData['allowEnterpriseGroupCallTypingForPrivateDialingPlan'])){
			$postData['allowEnterpriseGroupCallTypingForPrivateDialingPlan'] = "false";
		}
		if(!isset($postData['allowEnterpriseGroupCallTypingForPublicDialingPlan'])){
			$postData['allowEnterpriseGroupCallTypingForPublicDialingPlan'] = "false";
		}
		if(!isset($postData['overrideCLIDRestrictionForPrivateCallCategory'])){
			$postData['overrideCLIDRestrictionForPrivateCallCategory'] = "false";
		}
		return $postData;
	}
	
	//function to make array for different tabs
	function tabsInputArray(){
		
		$groups = array();
		//basic info input text array to check and show the message for which tab it has been updated
		$groups['basicInfoArray'] = array("callingLineIdName", "callingLineIdDisplayPhoneNumber", "locationDialingCode", "groupName", "timeZone",
				"contactName", "contactNumber", "contactEmail", "addressLine1", "addressLine2", "zipOrPostalCode", "stateOrProvince",
				"city", "country", "defaultDomain", "userLimit");
                
                //Code added @ 20 Aug 2018
                $groups['ocpSettingsArray'] = array("groupPermissionsGroup", "groupPermissionsLocal", "groupPermissionsTollfree", "groupPermissionsToll", "groupPermissionsInternational", "groupPermissionsOperatorAssisted", "groupPermissionsOperatorChargDirAssisted", "groupPermissionsSpecialService1", "groupPermissionsSpecialService2", "groupPermissionsPremiumServices1", "groupPermissionsPremiumServices2", "groupPermissionsCasual", "groupPermissionsUrlDialing", "groupPermissionsUnknown");
                //End code
		
		//extension min and max input array
		$groups['extensionLengthArray'] = array("minExtensionLength", "maxExtensionLength", "defaultExtensionLength");
		
		//routing profile input array
		$groups['routingProfileArray'] = array("routingProfile");
		
		//domain input array for simpel and security domain
		$groups['assignDomainArray'] = array("domainAssigned", "securityDomainAssigned");
		
		//numbers input array for DN's
		$groups['assignDnsArray'] = array("assignNumbers");
		
		//voice portal input array
		$groups['voicePortalArray'] = array("voicePortalActiveStatus", "portalLanguage", "portalTimeZone", "portalPhoneNo", "portalExt");
		
		//service pack input array
		$groups['assignDnsArray'] = array("servicePackAuth", "servicePackAssign");
		
		//call processing tab input array
		$groups['callProcessingArray'] = array("useGroupCLIDSetting", "useGroupMediaSetting", "useGroupCallLimitsSetting", "useGroupTranslationRoutingSetting", "useGroupDCLIDSetting",
				"useMaxSimultaneousCalls", "useMaxSimultaneousVideoCalls", "useMaxCallTimeForAnsweredCalls", "useMaxCallTimeForUnansweredCalls",
				"enforceGroupCallingLineIdentityRestriction", "allowEnterpriseGroupCallTypingForPrivateDialingPlan", "allowEnterpriseGroupCallTypingForPublicDialingPlan",
				"overrideCLIDRestrictionForPrivateCallCategory", "useEnterpriseCLIDForPrivateCallCategory", "useMaxFindMeFollowMeDepth", "useMaxConcurrentFindMeFollowMeInvocations", "allowAlternateNumbersForRedirectingIdentity", "blockCallingNameForExternalCalls",
				"enableDialableCallerID", "useGroupName", "allowDepartmentCLIDNameOverride", "useGroupCLIDSetting", "useGroupMediaSetting", "useGroupCallLimitsSetting",
				"useGroupTranslationRoutingSetting", "useGroupDCLIDSetting", "useMaxSimultaneousCalls", "maxSimultaneousCalls", "useMaxSimultaneousVideoCalls",
				"maxSimultaneousVideoCalls", "useMaxCallTimeForAnsweredCalls", "maxCallTimeForAnsweredCallsMinutes", "useMaxCallTimeForUnansweredCalls",
				"maxCallTimeForUnansweredCallsMinutes", "mediaPolicySelection", "supportedMediaSetName", "networkUsageSelection", "allowEnterpriseGroupCallTypingForPrivateDialingPlan", "allowEnterpriseGroupCallTypingForPublicDialingPlan", "overrideCLIDRestrictionForPrivateCallCategory",
				"useEnterpriseCLIDForPrivateCallCategory", "enableEnterpriseExtensionDialing", "useMaxConcurrentRedirectedCalls", "maxConcurrentRedirectedCalls",
				"useMaxFindMeFollowMeDepth", "maxFindMeFollowMeDepth", "maxRedirectionDepth", "useMaxConcurrentFindMeFollowMeInvocations", "maxConcurrentFindMeFollowMeInvocations",
				"clidPolicy", "emergencyClidPolicy", "useGroupName", "enableDialableCallerID",
				"allowConfigurableCLIDForRedirectingIdentity", "allowDepartmentCLIDNameOverride");
		
		return $groups;		
	}
	
	
	//function to update routing profile of the group
	public function groupRoutingProfileAddOperation($postArray){
		
		global  $sessionid, $client;
		$routingResponse['Error'] = "";
		$routingResponse['Success'] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupRoutingProfileModifyRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($this->sp) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']). "</groupId>";
		
		if(!empty($postArray['routingProfile'])){
			$xmlinput .= "<routingProfile>" . $postArray['routingProfile']. "</routingProfile>";
		}else{
			$xmlinput .= "<routingProfile xsi:nil='true'></routingProfile>";
		}
		
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			$routing["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			$routing["Success"] = $xml->command;
			
		}
		return $routingResponse;
	}
	
	function getAllUsersInGroup($postArray){
		global  $sessionid, $client;
		$usersList = array();
		$xmlinput = xmlHeader($sessionid, "UserGetListInGroupRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($this->sp) . "</serviceProviderId>";
		$xmlinput .= "<GroupId>" . htmlspecialchars($postArray['groupId']). "</GroupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (isset($xml->command->userTable->row))
		{
			foreach ($xml->command->userTable->row as $key => $value)
			{
				$usersList[] = strval($value->col[0]);
			}
		}
		return $usersList;
	}
	
}
