<?php

/**
 * Created by Sologics.
 * Date: 12/05/2017
 */
class GroupAdd
{
	public $groupId;
	public $groupInfo = array();
	public $groupAddResponse = array();
	
	public function groupAddRequest($postArray) {
		global $sessionid, $client;
		$groupAddResponse["Error"] = "";
		$groupAddResponse["Success"] = "";
		if(!empty($postArray)){
			$xmlinput = xmlHeader($sessionid, "GroupAddRequest");
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']). "</groupId>";
			
			if(!empty($postArray['defaultDomain'])){
				$xmlinput .= "<defaultDomain>" . $postArray['defaultDomain']. "</defaultDomain>";
			}
			
			if(!empty($postArray['userLimit'])){
				$xmlinput .= "<userLimit>" . $postArray['userLimit']. "</userLimit>";
			}else{
				$xmlinput .= "<userLimit>999999</userLimit>";
			}
			
			if(!empty($postArray['groupName'])){
			    $xmlinput .= "<groupName>" . htmlspecialchars($postArray['groupName']). "</groupName>";
			}
			
			if(!empty($postArray['callingLineIdName'])){
				$xmlinput .= "<callingLineIdName>" . $postArray['callingLineIdName']. "</callingLineIdName>";
			}	
			
			if(!empty($postArray['timeZone'])){
				$xmlinput .= "<timeZone>" . $postArray['timeZone']. "</timeZone>";
			}
			
			if(!empty($postArray['locationDialingCode'])){
				$xmlinput .= "<locationDialingCode>" . $postArray['locationDialingCode']. "</locationDialingCode>";
			}
			
			$xmlinput .= "<contact>";
			if(!empty($postArray['contactName'])){
				$xmlinput .= "<contactName>" . $postArray['contactName']. "</contactName>";
			}else{
				$xmlinput .= "<contactName xsi:nil='true'></contactName>";
			}
			
			if(!empty($postArray['contactNumber'])){
				$xmlinput .= "<contactNumber>" . $postArray['contactNumber']. "</contactNumber>";
			}else{
				$xmlinput .= "<contactNumber xsi:nil='true'></contactNumber>";
			}
			
			if(!empty($postArray['contactEmail'])){
				$xmlinput .= "<contactEmail>" . $postArray['contactEmail']. "</contactEmail>";
			}else{
				$xmlinput .= "<contactEmail xsi:nil='true'></contactEmail>";
			}
			
			$xmlinput .= "</contact>";
			
			$xmlinput .= "<address>";
			if(!empty($postArray['addressLine1'])){
				$xmlinput .= "<addressLine1>" . $postArray['addressLine1']. "</addressLine1>";
			}else{
				$xmlinput .= "<addressLine1 xsi:nil='true'></addressLine1>";
			}
			if(!empty($postArray['addressLine2'])){
				$xmlinput .= "<addressLine2>" . $postArray['addressLine2']. "</addressLine2>";
			}else{
				$xmlinput .= "<addressLine2 xsi:nil='true'></addressLine2>";
			}
			if(!empty($postArray['city'])){
				$xmlinput .= "<city>" . $postArray['city']. "</city>";
			}else{
				$xmlinput .= "<city xsi:nil='true'></city>";
			}
			if(!empty($postArray['stateOrProvince'])){
				$xmlinput .= "<stateOrProvince>" . $postArray['stateOrProvince']. "</stateOrProvince>";
			}else{
				$xmlinput .= "<stateOrProvince xsi:nil='true'></stateOrProvince>";
			}
			if(!empty($postArray['zipOrPostalCode'])){
				$xmlinput .= "<zipOrPostalCode>" . $postArray['zipOrPostalCode']. "</zipOrPostalCode>";
			}else{
				$xmlinput .= "<zipOrPostalCode xsi:nil='true'></zipOrPostalCode>";
			}
			if(!empty($postArray['country'])){
				$xmlinput .= "<country>" . $postArray['country']. "</country>";
			}else{
				$xmlinput .= "<country xsi:nil='true'></country>";
			}
			$xmlinput .= "</address>";
			
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			//echo "<pre>"; print_r($xml);
			if (readErrorXmlGenuine($xml) != "") {
				//$groupAddResponse["Error"] = $xml->command->summaryEnglish;
				if(isset($xml->command->detail)){
					$groupAddResponse["Error"]['Detail'] = strval($xml->command->detail);
				}else{
					$groupAddResponse["Error"]['Detail'] = strval($xml->command->summaryEnglish);
				}
			}else{
				$groupAddResponse["Success"] = $xml->command;
				$groupAddResponse["groupId"] = $_POST['groupId'];
				if(!empty($_POST['groupId'])){
				    $groupAddResponse["groupName"] = $_POST['groupName'];
				}
				
			}
		}
		return $groupAddResponse;
	}

}
