<?php
	$xmlinput = xmlHeader($sessionid, "GroupHuntGroupGetInstanceListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	if (isset($xml->command->huntGroupTable->row))
	{
		foreach ($xml->command->huntGroupTable->row as $key => $value)
		{
			$huntgroups[$a]["id"] = strval($value->col[0]);
			$huntgroups[$a]["name"] = strval($value->col[1]);
			$a++;
		}
	}

	if (isset($huntgroups))
	{
		$huntgroups = subval_sort($huntgroups, "name");
	}
?>
