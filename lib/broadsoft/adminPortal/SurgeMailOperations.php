<?php 
class SurgeMailOperations{  
    
    function surgeMailAccountOperations($surgeMailUrl, $surgemailPost){
        
        $resp = "";
        //Code added @ 22 July 2019
        if(empty($surgeMailUrl) || empty($surgemailPost)){
            $respnse = "false";
            return $respnse;            
        }
        //End code
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $surgeMailUrl);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15); //Code added @ 22 July 2019 timeout after 20 seconds 
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20); //Code added @ 22 July 2019 timeout after 20 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $surgemailPost);
        $output = curl_exec($ch);
        //print_r($output);
        $info = curl_getinfo($ch);        
        if(curl_errno($ch)){
            $resp = curl_error($ch);
        } else if(strpos($output, "User saved") === false) {
            $resp = $output;
        } else {
            $resp = $info["http_code"];
        }
        curl_close($ch);
        return $resp;
    }
    
}
?>