<?php
	if (isset($_POST["editSched"]))
	{
		//modify existing schedule
		$xmlinput = xmlHeader($sessionid, "GroupScheduleModifyRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<scheduleKey>
					<scheduleName>" . htmlspecialchars($_POST["editSched"]) . "</scheduleName>
					<scheduleType>" . $_POST["editSchedType"] . "</scheduleType>
				</scheduleKey>";
		$xmlinput .= "<newScheduleName>" . htmlspecialchars($schedName) . "</newScheduleName>";
	}
	else
	{
		//create new schedule
		$xmlinput = xmlHeader($sessionid, "GroupScheduleAddRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<scheduleName>" . htmlspecialchars($schedName) . "</scheduleName>";
		$xmlinput .= "<scheduleType>" . $schedType . "</scheduleType>";
	}
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
?>
