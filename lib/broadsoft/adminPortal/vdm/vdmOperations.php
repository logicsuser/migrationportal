<?php 
class VdmOperations{
	function getUserDeviceDetail($spId, $groupId, $deviceName)
	{
		global $sessionid, $client;
		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetRequest18sp1");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
		$xmlinput .= "<deviceName>" . $deviceName. "</deviceName>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//print_r($xml); exit;
		$errMsg = readErrorXmlGenuine($xml);
		if($errMsg != "") {
			$returnResponse["Error"] = $errMsg;
		}
		else
		{
			$returnResponse["Success"]["deviceName"] = $deviceName;
			$returnResponse["Success"]["deviceType"] = strval($xml->command->deviceType);
			$returnResponse["Success"]["protocol"] = strval($xml->command->protocol);
			$returnResponse["Success"]["macAddress"] = strval($xml->command->macAddress);
			$returnResponse["Success"]["numberOfAssignedPorts"] = strval($xml->command->numberOfAssignedPorts);
			$returnResponse["Success"]["status"] = strval($xml->command->status);
			$returnResponse["Success"]["configurationMode"] = strval($xml->command->configurationMode);
			$returnResponse["Success"]["transportProtocol"] = strval($xml->command->transportProtocol);
			$returnResponse["Success"]["useCustomUserNamePassword"] = strval($xml->command->useCustomUserNamePassword);
			$returnResponse["Success"]["userName"] = strval($xml->command->userName);
			$returnResponse["Success"]["netAddress"] = strval($xml->command->netAddress);
			$returnResponse["Success"]["physicalLocation"] = strval($xml->command->physicalLocation);
			$returnResponse["Success"]["serialNumber"] = strval($xml->command->serialNumber);
			$returnResponse["Success"]["description"] = strval($xml->command->description);
			$returnResponse["Success"]["numberOfPorts"] = strval($xml->command->numberOfPorts->quantity);
			
		}
		return $returnResponse;
	}
	
	function getDeviceCustomTags($providerid, $groupId, $deviceName)
	{
		global $sessionid, $client;
		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";

		// get custom tags from back-end
		$xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagGetListRequest");
		$xmlinput .= "<serviceProviderId>" .  htmlspecialchars($providerid) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
		$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		$errMsg = readErrorXmlGenuine($xml);
		if($errMsg != "") {
			$returnResponse["Error"] = $errMsg;
		}
		else
		{
		    $tagResponse = array();
			if(isset($xml->command->deviceCustomTagsTable->row)) {
				$i = 0;
				foreach ($xml->command->deviceCustomTagsTable->row as $key => $value){
					$tagName = strval($value->col[0]);
					
					$tagResponse[$tagName][] = strval($value->col[1]);
					$i++;
				}
			}
			$returnResponse["Success"] = $tagResponse;
		}
		return $returnResponse;
	}
	
	function getAllUserOfDevice($serviceProviderId, $groupId, $deviceName)
	{
		global $sessionid, $client;
		$allUserOfDevice = array();
		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetUserListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProviderId) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
		$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
		$xmlinput .= "<responseSizeLimit>1000</responseSizeLimit>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		$errMsg = readErrorXmlGenuine($xml);
		if($errMsg != "") {
			$returnResponse["Error"] = $errMsg;
		} else {
			if(isset($xml->command->deviceUserTable->row))
			{
				$c = 0;
				foreach ($xml->command->deviceUserTable->row as $kk => $vv)
				{
					$userInfo['linePort'] =  strval($vv->col[0]);
					$userInfo['lastName'] =  strval($vv->col[1]);
					$userInfo['firstName']= strval($vv->col[2]);
					$userInfo['phoneNumber'] =  strval($vv->col[3]);
					$userInfo['order'] =  strval($vv->col[7]);
					$userInfo['primaryLinePort'] =  strval($vv->col[8]);
					$userInfo['userId'] =  strval($vv->col[4]);
					$userInfo['extn']= strval($vv->col[9]);
					$userInfo['endPointType']= strval($vv->col[6]);
					$allUserOfDevice[$c] = $userInfo;
					$c++;
				}
			}
			$returnResponse["Success"] = $allUserOfDevice;
		}
		
		return $returnResponse;
	}
	
	function getUserDetails($userId){
	    global $sessionid, $client;
	    $returnResponse["Error"] = "";
	    $returnResponse["Success"] = "";
	    $xmlinput = xmlHeader($sessionid, "UserGetRequest19");
	    $xmlinput .= "<userId>" . $userId . "</userId>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    return strval($xml->command->extension);
	}
	
	//modify device request
	function modifyUserDeviceDetail($postArray)
	{
		global $sessionid, $client;
		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyRequest14");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($postArray['spId']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']) . "</groupId>";
		$xmlinput .= "<deviceName>" . $postArray['deviceName']. "</deviceName>";
		if(!empty($postArray['protocol'])){
			$xmlinput .= "<protocol>" . $postArray['protocol']. "</protocol>";
		}
		
		if(!empty($postArray['netAddress'])){
			$xmlinput .= "<netAddress>" . $postArray['netAddress']. "</netAddress>";
		}else{
			$xmlinput .= "<netAddress xsi:nil='true'></netAddress>";
		}
		
		if(!empty($postArray['port'])){
			$xmlinput .= "<port>" . $postArray['port']. "</port>";
		}else{
			$xmlinput .= "<port xsi:nil='true'></port>";
		}
		
		if(!empty($postArray['outboundProxyServerNetAddress'])){
			$xmlinput .= "<outboundProxyServerNetAddress>" . $postArray['outboundProxyServerNetAddress']. "</outboundProxyServerNetAddress>";
		}else{
			$xmlinput .= "<outboundProxyServerNetAddress xsi:nil='true'></outboundProxyServerNetAddress>";
		}
		
		if(!empty($postArray['stunServerNetAddress'])){
			$xmlinput .= "<stunServerNetAddress>" . $postArray['stunServerNetAddress']. "</stunServerNetAddress>";
		}else{
			$xmlinput .= "<stunServerNetAddress xsi:nil='true'></stunServerNetAddress>";
		}
		
		if(!empty($postArray['macAddress'])){
			$xmlinput .= "<macAddress>" . $postArray['macAddress']. "</macAddress>";
		}else{
			$xmlinput .= "<macAddress xsi:nil='true'></macAddress>";
		}
		
		if(!empty($postArray['serialNumber'])){
			$xmlinput .= "<serialNumber>" . $postArray['serialNumber']. "</serialNumber>";
		}else{
			$xmlinput .= "<serialNumber xsi:nil='true'></serialNumber>";
		}
		
		if(!empty($postArray['description'])){
			$xmlinput .= "<description>" . $postArray['description']. "</description>";
		}else{
			$xmlinput .= "<description xsi:nil='true'></description>";
		}
		
		if(!empty($postArray['configurationMode'])){
			$xmlinput .= "<configurationMode>" . $postArray['configurationMode']. "</configurationMode>";
		}else{
			$xmlinput .= "<configurationMode xsi:nil='true'></configurationMode>";
		}
		
		if(!empty($postArray['configurationFile'])){
			$xmlinput .= "<configurationFile>" . $postArray['configurationFile']. "</configurationFile>";
		}
		
		if(!empty($postArray['physicalLocation'])){
			$xmlinput .= "<physicalLocation>" . $postArray['physicalLocation']. "</physicalLocation>";
		}else{
			$xmlinput .= "<physicalLocation xsi:nil='true'></physicalLocation>";
		}
		
		if(!empty($postArray['transportProtocol'])){
			$xmlinput .= "<transportProtocol>" . $postArray['transportProtocol']. "</transportProtocol>";
		}
		
		/*if(!empty($postArray['mobilityManagerProvisioningURL'])){
			$xmlinput .= "<mobilityManagerProvisioningURL>" . $postArray['mobilityManagerProvisioningURL']. "</mobilityManagerProvisioningURL>";
		}
		
		if(!empty($postArray['mobilityManagerProvisioningUserName'])){
			$xmlinput .= "<mobilityManagerProvisioningUserName>" . $postArray['mobilityManagerProvisioningUserName']. "</mobilityManagerProvisioningUserName>";
		}
		
		if(!empty($postArray['mobilityManagerProvisioningPassword'])){
			$xmlinput .= "<mobilityManagerProvisioningPassword>" . $postArray['mobilityManagerProvisioningPassword']. "</mobilityManagerProvisioningPassword>";
		}
		
		
		
		if(!empty($postArray['useCustomUserNamePassword'])){
			$xmlinput .= "<useCustomUserNamePassword>" . $postArray['useCustomUserNamePassword']. "</useCustomUserNamePassword>";
		}*/
		
		/*if(!empty($postArray['accessDeviceCredentials'])){
			$xmlinput .= "<accessDeviceCredentials>" . $postArray['accessDeviceCredentials']. "</accessDeviceCredentials>";
		}else{
			$xmlinput .= "<accessDeviceCredentials xsi:nil='true'></accessDeviceCredentials>";
		}*/
 				
		$xmlinput .= xmlFooter(); 
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		$errMsg = readErrorXmlGenuine($xml);
		if($errMsg != "") {
			$returnResponse["Error"] = $errMsg;
		}
		else
		{
			$returnResponse["Success"] = "Success";
			//$getDeviceDetails = $this->getUserDeviceDetail($postArray['spId'], $postArray['groupId'], $postArray['deviceName']);
			//$_SESSION['device']['details'] = $getDeviceDetails['Success'];			
		}
		return $returnResponse;
	}
	
	//modify custom tags 
	public function modfiyCustomTags($postArray){
		global $sessionid, $client;
		
		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";
	
		// update custom tags from back-end
		$xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagModifyRequest");
		$xmlinput .= "<serviceProviderId>" .  htmlspecialchars($postArray['spId']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']) . "</groupId>";
		$xmlinput .= "<deviceName>" . $postArray['deviceName'] . "</deviceName>";
		$xmlinput .= "<tagName>" . $postArray['tagName'] . "</tagName>";
		if(!empty($postArray['tagValue']) || $postArray['tagValue'] === "0"){
			$xmlinput .= "<tagValue>" . $postArray['tagValue'] . "</tagValue>";
		}else if(empty($postArray['tagValue'])){
		    $xmlinput .= "<tagValue xsi:nil='true'></tagValue>";
		}
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errMsg = readErrorXmlGenuine($xml);
		if($errMsg != "") {
			$returnResponse["Error"] = $errMsg;
		}
		else
		{
			$returnResponse["Success"] = "Success";
			if(!empty($postArray['tagValue']) || $postArray['tagValue'] === "0"){
				$returnResponse["Success"] = $postArray['tagValue'];
			}
			
			//$getCustomTags = $this->getDeviceCustomTags($postArray['spId'], $postArray['groupId'], $postArray['deviceName']);
			//$_SESSION['device']['customTags'] = $getCustomTags['Success'];
		}
		return $returnResponse;
	}
	
	public function getUserStatus($userId){
		
		global $sessionid, $client;
		$returnResponse["Error"] = "";
		$returnResponse["Success"] = "";
		$returnStatus = "";
		
		$xmlinput = xmlHeader($sessionid, "UserGetRegistrationListRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$errMsg = readErrorXmlGenuine($xml);
		if($errMsg != "") {
			$returnStatus = "";
		} else {
			if(isset($xml->command->registrationTable->row) && count($xml->command->registrationTable->row) > 0){
				$returnStatus = "Registered";
			}
		}
		
		return $returnStatus;
	}
	
	public function getUserStatusDetail($userId){
	    
	    global $sessionid, $client;
	    $returnResponse["Error"] = "";
	    $returnResponse["Success"] = "";
	    $returnStatus = array();
	    
	    $xmlinput = xmlHeader($sessionid, "UserGetRegistrationListRequest");
	    $xmlinput .= "<userId>" . $userId . "</userId>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    $errMsg = readErrorXmlGenuine($xml);
	    if($errMsg != "") {
	        $returnStatus = array();
	    } else {
	        if(isset($xml->command->registrationTable->row) && count($xml->command->registrationTable->row) > 0){
	            //$returnStatus = $xml->command->registrationTable->row->col;
	            foreach ($xml->command->registrationTable->row as $kk => $vv)
	            {
	                $returnStatus['order'] =  strval($vv->col[2]);
	                $returnStatus['publicIPAddress'] =  strval($vv->col[7]);
	                $returnStatus['privateIPAddress'] =  strval($vv->col[9]);
	            }
	        }
	    }
// 	    print_r($returnStatus); exit;
	    return $returnStatus;
	}
	
	public function checkIpValidation($ip){
		
		$result = 0;
		if (filter_var($ip, FILTER_VALIDATE_IP)) {
			$result = 1;
		} else {
			$result = 0;
		}
		return $result;
	}
	
	public function getTagsCustomProfile($customProfileName, $devicetype){
		global $db;
	
		$query = "SELECT `customTagName`, `customTagValue` FROM `customProfiles` WHERE `customProfileName` = '" . $customProfileName . "' AND `deviceType` = '".$devicetype."' ORDER BY `customTagName`";
		$results = $db->query ( $query );
		$tags = array ();
		$result = array ();
		
		$i = 0;
		while($row = $results->fetch ()){
			$result[$i]["customTagName"] = $row[0];
			$result[$i]["customTagValue"] = $row[1];
			$i++;
		}
		
		return $result;
	}
	
	function mergeArray($customTags, $systemTagList){
		/*foreach ($customTags as $key => $value){
			if(array_key_exists($key, $systemTagList)){
				if(empty($value[0])){
					$customTags[$key][0] = $systemTagList[$key][0];
				}
			}
		}*/
		$arr = array_merge($systemTagList, $customTags);
		return $arr;
	}
	
	function checkModifyDataCompare($postArray, $sessionDeviceTagsArray, $deviceTagsArray, $allTagsArray, $deviceName, $isCustomProfileSame){
	    
	    if($isCustomProfileSame){
	        unset($postArray["%Express_Custom_Profile_Name%"]);
	    }
	    
		$changeStringValidate = "";
		foreach($postArray as $key3=>$val3){
			foreach($allTagsArray as $key4=>$val4){
				$backgroundColor = 'background:#72ac5d;'; $errorMessage = '';
				if($key3 == $key4){
					$val3 = trim($val3);
					if($val4[0] <> $val3){
						if (array_key_exists($key3, $deviceTagsArray)) {
							$_SESSION['customTagsUpdatedPloycom'] = true;
							$changeStringValidate .= '<tr><td class="errorTableRows" style="'.$backgroundColor.'">'.$deviceTagsArray[$key3].'</td><td class="errorTableRows">'.$val3.' '. $errorMessage.' </td></tr>';
						}
// 						if($key3 == "%Express_Custom_Profile_Name%"){
// 								require_once ("/var/www/lib/broadsoft/adminPortal/customTagManagement.php");
// 								require_once("/var/www/lib/broadsoft/adminPortal/util/sasUtil.php");
// 								$sasUtil = new SasUtil();
// 								$customTagManager = new CustomTagManagement();
// 								$response = $customTagManager->getUserCustomTags($deviceName);
// 								$existingCustomTagList= $response["Success"];
								
// 								if(count($existingCustomTagList) > 0){
// 									foreach ($existingCustomTagList as $key => $value){
// 										$isSasUser = $sasUtil->isSASUser($value);
// 										if($isSasUser){
// 											unset($existingCustomTagList[$key]);
// 										}
// 									}
									
// 								}
								
// 								$value1=implode(", ", $existingCustomTagList);
// 								$changeStringValidate.= "<tr><td class='errorTableRows' style='background: #00AC3E'> Custom Tag will be deleted</td><td class=\"errorTableRows\">" . $value1. "</td></tr>";
// 								//print_r($data);
// 								//$changes++;
// 								//$bg = CHANGED;
// 						}
					}
				}
				
			}
		}
		
		// when tag Bundle will change.
		$oldTagBundle = isset($_SESSION['device']['allCustomTags']["%Express_Tag_Bundle%"]) ? explode(";", $_SESSION['device']['allCustomTags']["%Express_Tag_Bundle%"][0]) : array();
		$newTagBundle = $postArray["%deviceMgtTagBundle%"];
		$isTagBundleSame = false;
		
		if (count(array_diff(array_merge($oldTagBundle, $newTagBundle), array_intersect($oldTagBundle, $newTagBundle))) === 0) {
		    $isTagBundleSame = true;
		}
		
		
		/*if(!$isTagBundleSame) {
		    require_once ("/var/www/lib/broadsoft/adminPortal/customTagManagement.php");
		    require_once("/var/www/lib/broadsoft/adminPortal/util/sasUtil.php");
		    $sasUtil = new SasUtil();
		    $customTagManager = new CustomTagManagement();
		    $response = $customTagManager->getUserCustomTags($deviceName);
		    $existingCustomTagList= $response["Success"];
		    
		    if(count($existingCustomTagList) > 0){
		        foreach ($existingCustomTagList as $key => $value){
		            $isSasUser = $sasUtil->isSASUser($value);
		            if($isSasUser){
		                unset($existingCustomTagList[$key]);
		            }
		        }
		        
		    }
		    
		    $value1=implode(", ", $existingCustomTagList);
		    $changeStringValidate.= "<tr><td class='errorTableRows' style='background: #00AC3E'> Custom Tag will be deleted</td><td class=\"errorTableRows\">" . $value1. "</td></tr>";
		    
		}*/
		
		
		
		if(($postArray["%codecPrefG729AB%"] == "1" && $postArray["%codecPrefG711Mu%"] == "1") || ($postArray["%codecPrefG729AB%"] == "1" && $postArray["%codecPrefG722%"] == 1) || ($postArray["%codecPrefG711Mu%"] == 1 && $postArray["%codecPrefG722%"] == 1)){
			//$_SESSION['customTagsUpdatedPloycom'] = false;
			$changeStringValidate .= 'Error<tr><td class="errorTableRows" style="background-color:#ac5f5d;">Codec Preferences</td><td class="errorTableRows">Preferences value 1 cannot be selected more than Once</td></tr>';
		}
		
		if($postArray["%codecPrefG729AB%"] != "0" || $postArray["%codecPrefG722%"] != "0" || $postArray["%codecPrefG711Mu%"] != "0"){
			if($postArray["%codecPrefG729AB%"] != "1" && $postArray["%codecPrefG722%"] != "1" && $postArray["%codecPrefG711Mu%"] != "1"){
				$changeStringValidate .= 'Error<tr><td class="errorTableRows" style="background-color:#ac5f5d;">Codec Preferences</td><td class="errorTableRows">One codec must be set to preference 1</td></tr>';
			}
		}
		
		return $changeStringValidate;
	}
	
	function addDeviceCustomTag($sp, $groupId, $deviceName, $tagName, $tagValue) {
		global $sessionid, $client;
		$errMsg["Success"] = "";
		$errMsg["Error"] = "";
		$xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagAddRequest");
		$xmlinput .= "<serviceProviderId>" .  htmlspecialchars($sp) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
		$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
		$xmlinput .= "<tagName>" . $tagName . "</tagName>";
		if($tagValue != ""){
		    $xmlinput .= "<tagValue>" . $tagValue . "</tagValue>";
		}
		
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		//$errMsg = "";
		if (readError($xml) != "") {
			$errMsg["Error"] = "Failed to add custom tag to device " . $deviceName . " .";
			$errMsg["Error"].= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
			if ($xml->command->detail)
			{
				$errMsg["Error"].= "%0D%0A" . strval($xml->command->detail);
			}
		}else{
			$errMsg["Success"] = "Success";
		}
		return $errMsg;
	}
	
	function deleteDeviceCustomTag($sp, $groupId, $deviceName, $tagName) {
		global $sessionid, $client;
		
		$xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagDeleteListRequest");
		$xmlinput .= "<serviceProviderId>" .  htmlspecialchars($sp) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
		$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
		$xmlinput .= "<tagName>" . $tagName . "</tagName>";
		
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		$errMsg = "";
		if (readError($xml) != "") {
			$errMsg = "Failed to delete custom tag to device " . $deviceName . " .";
			$errMsg .= "%0D%0A%0D%0A" . strval($xml->command->summaryEnglish);
			if ($xml->command->detail)
			{
				$errMsg .= "%0D%0A" . strval($xml->command->detail);
			}
		}
		return $errMsg;
	}
	
	function createPortTags($tags, $nPorts){
		//echo "<pre>"; print_r($tags);
		$lineTags = array();
		
		for($i = 1; $i <= $nPorts; $i++){					
			$lineType = "%lineType" . $i."%";
			$lineTags[$lineType][0] = "";
			
			$callsPerLine = "%CallsPerLine-" . $i."%";
			$regLineKey = "%reg" . $i . "lineKeys%";		
			
			$CallsPerLinetags[$callsPerLine][0] = "1";
			$regLineTags[$regLineKey][0] = "1";
		}
		
		$defaultCustomTagsArray = array(
				"%FEATURE_SYNC_ACD%" => "0",
				"%FEATURE_HOTELING%" => "0",
				"%SKPaging%" => "0",
				"%SKZipDial%" => "0",
				"%SKPickup%" => "0",
				"%SKRecent%" => "0",
				"%SKCallPark%" => "0",
				"%SKRetrieve%" => "0",
				"%SKForward%" => "0",
				"%SKDND%" => "0",
				"%SKConf%" => "0",	
		);
		
		$array1 = array_merge($lineTags,$CallsPerLinetags);
		$array2 = array_merge($array1, $regLineTags);
		$final_array = array_merge($array2, $defaultCustomTagsArray);
		
		foreach($final_array as $key=>$val){

			if(!array_key_exists($key, $tags)){
				$tags[$key] = $val;
			}
		}
		
		
		return $tags;
		 
	}
	
}
?>