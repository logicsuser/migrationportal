<?php 
class sysLevelDeviceOperations{
	function getTagSetNameByDeviceType($deviceType, $ociVersion){
		$tagSetName["Error"] = "";
		$tagSetName["Success"] = "";
		global $sessionid, $client;
		if ($ociVersion == "20")
		{
			$xmlinput = xmlHeader($sessionid, "SystemSIPDeviceTypeGetRequest20");
		}
		else
		{
			$xmlinput = xmlHeader($sessionid, "SystemSIPDeviceTypeGetRequest19sp1");
		}
		
		$xmlinput .= "<deviceType>".$deviceType."</deviceType>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$tagSetName["Error"] = strval($xml->command->summary);
		}else{
			if(isset($xml->command->cpeDeviceOptions->deviceManagementDeviceTypeOptions->tagSet) && !empty($xml->command->cpeDeviceOptions->deviceManagementDeviceTypeOptions->tagSet)){
				$tagSetName["Success"] = strval($xml->command->cpeDeviceOptions->deviceManagementDeviceTypeOptions->tagSet);
			}else{
				$tagSetName["Success"] = "";
			}
		}
		
		return $tagSetName;
	}
	
	function getDefaultTagListByTagSet($tagSetName){
		$tagSetTagList["Error"] = "";
		$tagSetTagList["Success"] = "";
		global $sessionid, $client;
		
		$xmlinput = xmlHeader($sessionid, "SystemDeviceManagementTagGetListRequest");
		//$xmlinput .= "<systemDefaultTagSet>true</systemDefaultTagSet>";
		$xmlinput .= "<tagSetName>".$tagSetName."</tagSetName>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			$tagSetTagList["Error"] = strval($xml->command->detail);
		}else{
			$tagResponse = array();
			if(count($xml->command->tagsTable->row) > 0){
				$i = 0;
				foreach ($xml->command->tagsTable->row as $key => $value){
					$tagName = strval($value->col[0]);
					$tagResponse[$tagName][] = strval($value->col[1]);
					$i++;
				}
			}
			$tagSetTagList["Success"] = $tagResponse;
		}
		
		return $tagSetTagList;
	}
	
	function getSysytemConfigDeviceTypeRequest($deviceType, $ociVersion){
		
		global $sessionid, $client;
		
		$sipSystemConfigArray["Error"] = "";
		$sipSystemConfigArray["Success"] = "";
		
		if ($ociVersion == "20")
		{
			$xmlinput = xmlHeader($sessionid, "SystemSIPDeviceTypeGetRequest20");
		}
		else
		{
			$xmlinput = xmlHeader($sessionid, "SystemSIPDeviceTypeGetRequest19sp1");
		}
		
		$xmlinput .= "<deviceType>".$deviceType."</deviceType>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$sipSystemConfigArray["Error"] = strval($xml->command->summary);
		}else{
			$sipSystemConfigArray["Success"] = $xml->command;
		}
		
		return $sipSystemConfigArray;
	}
	
	
	function getSysytemDeviceTypeServiceRequest($deviceType, $ociVersion){
		
		global $sessionid, $client;
		
		$sipSystemDeviceArray["Error"] = "";
		$sipSystemDeviceArray["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "SystemSIPDeviceTypeServiceGetRequest");
		
		$xmlinput .= "<deviceType>".$deviceType."</deviceType>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$sipSystemDeviceArray["Error"] = strval($xml->command->summary);
		}else{
			$sipSystemDeviceArray["Success"] = $xml->command;
		}
		
		return $sipSystemDeviceArray;
	}
}
?>