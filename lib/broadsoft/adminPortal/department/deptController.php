<?php
class DeptController{
	public $groupId;
	function getDepartmentList($groupId){
		global $sessionid, $client;
		$deptArr["Error"]="";
		$deptArr["Success"]="";
		$xmlinput = xmlHeader($sessionid, "GroupDepartmentGetListRequest18");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		$xmlinput .= "<includeEnterpriseDepartments>false</includeEnterpriseDepartments>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$deptArr["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			if(isset($xml->command->departmentTable->row) && count($xml->command->departmentTable->row) > 0){
				$i=0;
				foreach ($xml->command->departmentTable->row as $key => $value){
					$deptArr["Success"]["department"][$i]["name"] = strval($value->col[1]);
					$deptArr["Success"]["department"][$i]["fpName"] = strval($value->col[2]);
					$deptArr["Success"]["department"][$i]["callingLineIdNameDepartment"] = strval($value->col[3]);
					$deptArr["Success"]["department"][$i]["callingLineIdPhoneNumberDepartment"] = strval($value->col[4]);
					$i++;
				}
			}
		}
		return $deptArr;
	}
	
	function addDepartmentToGroup($groupId, $postArray){
		global $sessionid, $client;
		$deptAddResp["Error"]="";
		$deptAddResp["Success"]="";
		$xmlinput = xmlHeader($sessionid, "GroupDepartmentAddRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		$xmlinput .= "<departmentName>".$postArray["departmentName"]."</departmentName>";
		if(!empty($postArray["parentDepartmentKey"])){
			$xmlinput .= "<parentDepartmentKey xsi:type='GroupDepartmentKey'>";
				//$xmlinput .= "<GroupDepartmentKey>";
				$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
				$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
					$xmlinput .= "<name>".$postArray["parentDepartmentKey"]."</name>";
				//$xmlinput .= "</GroupDepartmentKey>";
			$xmlinput .= "</parentDepartmentKey>";
		}
		if(!empty($postArray["callingLineIdNameDepartment"])){
			$xmlinput .= "<callingLineIdName>".$postArray["callingLineIdNameDepartment"]."</callingLineIdName>";
		}
		if(!empty($postArray["callingLineIdPhoneNumberDepartment"])){
			$xmlinput .= "<callingLineIdPhoneNumber>".$postArray["callingLineIdPhoneNumberDepartment"]."</callingLineIdPhoneNumber>";
		}
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$deptAddResp["Error"]['Detail'] = strval($xml->command->summary);
		}else{
			$deptAddResp["Success"]['Detail'] = "Success";
		}
		return $deptAddResp;
	}
	
	function deleteDeptFromGroup($groupId, $departmentName){
		global $sessionid, $client;
		$deptDelResp["Error"] = "";
		$deptDelResp["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupDepartmentDeleteRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		$xmlinput .= "<departmentName>".$departmentName."</departmentName>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$deptDelResp["Error"]['Detail'] = strval($xml->command->summary);
		}else{
			$deptDelResp["Success"]['Detail'] = "Success";
		}
		return $deptDelResp;
	}
	
	function getDeptFromGroup($groupId, $departmentName){
		global $sessionid, $client;
		$deptGetResp["Error"] = "";
		$deptGetResp["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupDepartmentGetRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		$xmlinput .= "<departmentName>".$departmentName."</departmentName>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$deptGetResp["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			if(isset($xml->command->parentDepartmentKey)){
				$deptGetResp["Success"]["parentDepartmentKey"] = strval($xml->command->parentDepartmentKey->name);
			}
			if(isset($xml->command->callingLineIdName)){
				$deptGetResp["Success"]["callingLineIdNameDepartment"] = strval($xml->command->callingLineIdName);
			}
			if(isset($xml->command->callingLineIdPhoneNumber)){
				$deptGetResp["Success"]["callingLineIdPhoneNumberDepartment"] = "+1-".strval($xml->command->callingLineIdPhoneNumber);
			}
		}
		return $deptGetResp;
	}
	
	function modifyDepartmentInGroup($groupId, $deptName, $postArray){
		global $sessionid, $client;
		$deptModResp["Error"] = "";
		$deptModResp["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupDepartmentModifyRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		$xmlinput .= "<departmentName>".$deptName."</departmentName>";

		if(isset($postArray['departmentName'])){
			$xmlinput .= "<newDepartmentName>".$postArray['departmentName']."</newDepartmentName>";
		}
		if(!empty($postArray['parentDepartmentKey']) && $postArray['parentDepartmentKey'] == "Nill"){
			$xmlinput .= "<newParentDepartmentKey xsi:nil='true' xsi:type='GroupDepartmentKey'>";
			$xmlinput .= "</newParentDepartmentKey>";
		}else if (!empty($postArray['parentDepartmentKey'])){
			$xmlinput .= "<newParentDepartmentKey xsi:type='GroupDepartmentKey'>";
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
			$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
			$xmlinput .= "<name>".$postArray['parentDepartmentKey']."</name>";
			$xmlinput .= "</newParentDepartmentKey>";
		}

		if(!empty($postArray['callingLineIdNameDepartment']) && $postArray['callingLineIdNameDepartment'] == "Nill"){
			$xmlinput .= "<callingLineIdName xsi:nil='true'></callingLineIdName>";
		}else if(!empty($postArray['callingLineIdNameDepartment'])){
			$xmlinput .= "<callingLineIdName>".$postArray['callingLineIdNameDepartment']."</callingLineIdName>";
		}

		if(!empty($postArray['callingLineIdPhoneNumberDepartment']) && $postArray['callingLineIdPhoneNumberDepartment'] == "Nill"){
			$xmlinput .= "<callingLineIdPhoneNumber xsi:nil='true'></callingLineIdPhoneNumber>";
		}else if(!empty($postArray['callingLineIdPhoneNumberDepartment'])){
			$xmlinput .= "<callingLineIdPhoneNumber>".$postArray['callingLineIdPhoneNumberDepartment']."</callingLineIdPhoneNumber>";
		}
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			$deptModResp["Error"]['Detail'] = strval($xml->command->detail);
		}else{
				$deptModResp["Success"]="Success";
		}
		return $deptModResp;
	}
	
	function GroupDepartmentGetAvailableParentListRequest($spName, $groupId, $deptName){
		global $sessionid, $client;
		$availParentList["Error"]="";
		$availParentList["Success"]="";
		$xmlinput = xmlHeader($sessionid, "GroupDepartmentGetAvailableParentListRequest");
		$xmlinput .= "<serviceProviderId>". htmlspecialchars($spName) ."</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		$xmlinput .= "<departmentName>".$deptName."</departmentName>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$availParentList["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			if(isset($xml->command->departmentKey) && count($xml->command->departmentKey) > 0){
				$i=0;
				foreach ($xml->command->departmentKey as $key => $value){
					$availParentList["Success"]["department"][$i]["name"] = strval($value->name);
					$availParentList["Success"]["department"][$i]["fpName"] = strval($xml->command->fullPathName[$i]);
					$i++;
				}
			}
		}
		return $availParentList;
	}
	
	function formDataArray(){
		$formDataArray = array(
				"departmentName" => "Department Name",
				"parentDepartmentKey" => "Parent Department Key",
				"callingLineIdNameDepartment" => "Calling Line Id Name Department",
				"callingLineIdPhoneNumberDepartment" => "Calling Line Id Phone Number Department"
		);
		return $formDataArray;
	}
	
	function ifDataNotExistInSession(){
		if(!array_key_exists("callingLineIdPhoneNumberDepartment", $_SESSION['groupDepartment'])){
			$_SESSION['groupDepartment']['callingLineIdPhoneNumberDepartment'] = "";
		}
		if(!array_key_exists("parentDepartmentKey", $_SESSION['groupDepartment'])){
			$_SESSION['groupDepartment']['parentDepartmentKey'] = "";
		}
		if(!array_key_exists("callingLineIdNameDepartment", $_SESSION['groupDepartment'])){
			$_SESSION['groupDepartment']['callingLineIdNameDepartment'] = "";
		}
		return $_SESSION['groupDepartment'];
	}
	
}