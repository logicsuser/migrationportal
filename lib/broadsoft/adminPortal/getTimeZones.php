<?php
	if ($ociVersion == "20")
	{
		$xmlinput = xmlHeader($sessionid, "SystemTimeZoneGetListRequest20");
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "SystemTimeZoneGetListRequest");
	}
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->timeZoneTable->row as $key => $value)
	{
		$timeZones[$a] = strval($value->col[0]);
		$a++;
	}
?>
