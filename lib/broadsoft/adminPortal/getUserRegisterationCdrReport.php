<?php

/**
 * Created by Karl.
 * Date: 8/16/2016
 */
class GetUserRegistrationCdrReport
{
		public $message;
		public $mod;
		public $users = array();

		private $from;
		private $to;
		private $date;
		private $reportHeaders = array("responsibleParty","411","911","LD","Local","Intl","TollFree");
		private $tollFreeReports = array("800", "866", "877", "855", "844", "888");

                const SKY_BLUE = "dbe5f1";
                const DARK_BLUE = "254061";
                const BLUE = "0070c0";
                const Light_Grey = "D6D6D6";
		public function __construct() {
		 
		}

		public function getColumnNames() {
					$coulmnArray = array("Device Name","DeviceType","Registration Status","IP","Expiry","Agent Type");
			return $this->message = $coulmnArray;
		}

		public function getColumnNamesCDR() {
			$coulmnArray = array("Phone Number","411","911","LD","Local","Int'l","TollFree");
			return $this->message = $coulmnArray;
		}

		public function getUserData($users) {
		$message = '';
			if (isset($users)){
				$r = 0;
				for ($a = 0; $a < count($users); $a++){
					if (isset($users[$a]["registration"]) and count($users[$a]["registration"]) > 0){

						for ($b = 0; $b < count($users[$a]["registration"]); $b++){
							$userArray[$r]['agentType'] = $users[$a]["registration"][$b]["agentType"];
							$userArray[$r]['deviceName'] = $users[$a]["registration"][$b]["deviceName"];
							$userArray[$r]['expiration'] = $users[$a]["registration"][$b]["expiration"];
							$userArray[$r]['epType'] = $users[$a]["registration"][$b]["epType"];
							$userArray[$r]['publicIp'] = $users[$a]["registration"][$b]["publicIp"];
							$userArray[$r]['registerStatus'] = "Registered";
							$userArray[$r]['deviceType'] = $users[$a]["registration"][$b]["deviceType"];
							$userArray[$r]['userId'] = $users[$a]["registration"][$b]["userId"];
						}
					}else{
                                              if($users[$a]["deviceName"]!="")
                                              {
                                                    $userArray[$r]['agentType'] = "";
                                                    $userArray[$r]['deviceName'] = $users[$a]["deviceName"];
                                                    $userArray[$r]['expiration'] = "";
                                                    $userArray[$r]['epType'] = "";
                                                    $userArray[$r]['publicIp'] = "";
                                                    $userArray[$r]['deviceType'] = $users[$a]["deviceType"];
                                                    $userArray[$r]['registerStatus'] = "Non-Registered";
                                                    $userArray[$r]['userId'] = $users[$a]["userId"];
                                              }
					}
					$r++;
				}
			}
		return $userArray;
    }

		public function excelCreator($objPHPExcel){
                                  
                    $creator = $objPHPExcel->getProperties()
                        ->setCreator("Express")
                        ->setLastModifiedBy("Express")
                        ->setTitle("An Express Document")
                        ->setSubject("An Express Document")
                        ->setDescription("An Express generated document.")
                        ->setKeywords("Averistar Express")
                        ->setCategory("Report");
                    return $creator;
                }

		public function generateRegistrationXLS($objPHPExcel, $coulmnNamesRegistration, $userArray, $headingRowForRegistration){
		$objPHPExcel->setActiveSheetIndex(0);
		$message = "";

		$objPHPExcel->getActiveSheet()->setCellValue('A'.$headingRowForRegistration, 'Current Registration Status: ');
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$headingRowForRegistration.':J'.$headingRowForRegistration);
		$objPHPExcel->getActiveSheet()->getRowDimension($headingRowForRegistration)->setRowHeight(40);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration.':J'.$headingRowForRegistration)->getAlignment()->setVertical(PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration)->getFont()->setSize(16);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration)->getFont()->getColor()->setARGB(self::BLUE);

		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration.':J'.$headingRowForRegistration)->getFill()->setFillType(PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration.':J'.$headingRowForRegistration)->getFill()->getStartColor()->setARGB(self::SKY_BLUE);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForRegistration)->getAlignment()->setShrinkToFit(true);

		$rowForColumnRegistration = $headingRowForRegistration + 1;
		$columnRowForRegistrationIndex = array("A", "B", "C", "D", "E", "F", "G", "H", "I");
		$countColumnForRegistrationIndex = count($columnRowForRegistrationIndex);
		$lastColumnRowForRegistration = $columnRowForRegistrationIndex[$countColumnForRegistrationIndex-1];

		//print column heading for the registration
		foreach($coulmnNamesRegistration as $key=>$val){
			//set value in column heading
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$rowForColumnRegistration, 'Device');
			$objPHPExcel->getActiveSheet()->mergeCells("A".$rowForColumnRegistration.":B".$rowForColumnRegistration);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$rowForColumnRegistration, 'Phone Type');
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$rowForColumnRegistration, 'Registration Status');
			$objPHPExcel->getActiveSheet()->mergeCells("D".$rowForColumnRegistration.":E".$rowForColumnRegistration);
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$rowForColumnRegistration, 'IP');
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$rowForColumnRegistration, 'Expiry');
			$objPHPExcel->getActiveSheet()->mergeCells("G".$rowForColumnRegistration.":H".$rowForColumnRegistration);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$rowForColumnRegistration, 'Agent Type');
			$objPHPExcel->getActiveSheet()->mergeCells("I".$rowForColumnRegistration.":J".$rowForColumnRegistration);
			//auto size cell
			//fill color in cells make it solid first and next fill the color
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rowForColumnRegistration.':'.'J'.$rowForColumnRegistration)->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rowForColumnRegistration.':'.'J'.$rowForColumnRegistration)->getFont()->getColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
                        
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$rowForColumnRegistration.':'.'J'.$rowForColumnRegistration)->getFill()->setFillType(PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rowForColumnRegistration.':'.'J'.$rowForColumnRegistration)->getFill()->getStartColor()->setARGB(self::BLUE);
                        
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$rowForColumnRegistration.':'.'J'.$rowForColumnRegistration)->getBorders()->getTop()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$rowForColumnRegistration.':'.'J'.$rowForColumnRegistration)->getBorders()->getBottom()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$rowForColumnRegistration.':'.'J'.$rowForColumnRegistration)->getBorders()->getLeft()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                        $objPHPExcel->getActiveSheet()->getStyle('A'.$rowForColumnRegistration.':'.'J'.$rowForColumnRegistration)->getBorders()->getRight()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
		}

		// Set style for header row using alternative method
		$objPHPExcel->getActiveSheet()->getStyle($columnRowForRegistrationIndex[0].$rowForColumnRegistration.':'.$lastColumnRowForRegistration.$rowForColumnRegistration)->applyFromArray(
				array(
					'font'    => array(
						'bold'      => true
					),
					'alignment' => array(
						'horizontal' => PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
					    'allborders' => array(
					        'style' => PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					        'color' => array('rgb' => '000000')
					    )
					),
					'fill' => array(
						'type'       => PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
							'rotation'   => 90,
						'startcolor' => array(
							'argb' => '467ab8'
						),
						'endcolor'   => array(
							'argb' => '467ab8'
						)
					)
				)
		);

		// Unprotect a cell
		$objPHPExcel->getActiveSheet()->getStyle('B'.$rowForColumnRegistration)->getProtection()->setLocked(PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);

		//dynamic registration row for the user
		$userRowStartted = $dynamicRowForColumnRegistration = $rowForColumnRegistration + 1;

		foreach($userArray as $key1=>$val1){

			$objPHPExcel->getActiveSheet()->setCellValue('A'.$dynamicRowForColumnRegistration, $val1['deviceName']);
			$objPHPExcel->getActiveSheet()->mergeCells("A".$dynamicRowForColumnRegistration.":B".$dynamicRowForColumnRegistration);
			
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$dynamicRowForColumnRegistration, $val1['deviceType']);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$dynamicRowForColumnRegistration, $val1['registerStatus']);
			$objPHPExcel->getActiveSheet()->mergeCells("D".$dynamicRowForColumnRegistration.":E".$dynamicRowForColumnRegistration);
			
			$objPHPExcel->getActiveSheet()->setCellValue('F'.$dynamicRowForColumnRegistration, $val1['publicIp']);			
			$objPHPExcel->getActiveSheet()->setCellValue('G'.$dynamicRowForColumnRegistration, $val1['expiration']);
			$objPHPExcel->getActiveSheet()->mergeCells("G".$dynamicRowForColumnRegistration.":H".$dynamicRowForColumnRegistration);
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$dynamicRowForColumnRegistration, $val1['agentType']);
			$objPHPExcel->getActiveSheet()->mergeCells("I".$dynamicRowForColumnRegistration.":J".$dynamicRowForColumnRegistration);
			$dynamicRowForColumnRegistration++;

		}
		$lastRowRegistration = $dynamicRowForColumnRegistration - 1;
		$objPHPExcel->getActiveSheet()->getStyle('A'.$userRowStartted.':J'.$lastRowRegistration)->getFill()->setFillType(PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$userRowStartted.':J'.$lastRowRegistration)->getFill()->getStartColor()->setARGB('f2f2f2');
		$styleArrayRegData = array(
			  'borders' => array(
				  'allborders' => array(
					  'style' => PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					  'color' => array('rgb' => 'e5e3e3')
				  )
			  ),
		    'alignment' => array(
		        'horizontal' => PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
		    )
		  );
		$objPHPExcel->getActiveSheet()->getStyle('A'.$userRowStartted.':J'.$lastRowRegistration)->applyFromArray($styleArrayRegData);

	}

		public function generateCdrXLS($objPHPExcel, $coulmnNamesCDR, $cdrReportArray, $headingRowForCDR, $from, $to){

		// Create a first sheet, representing sales data
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$headingRowForCDR, 'Site Summary Report');
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$headingRowForCDR.':J'.$headingRowForCDR);

		$objPHPExcel->getActiveSheet()->setCellValue('J'.$headingRowForCDR, '#12566');
		$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(60);
		$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->setHorizontal(PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getAlignment()->setVertical(PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForCDR)->getFont()->setSize(26);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForCDR)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForCDR)->getFont()->getColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForCDR)->getFont()->getColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$headingRowForCDR)->getFont()->getColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForCDR.':I'.$headingRowForCDR)->getFill()->setFillType(PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForCDR.':I'.$headingRowForCDR)->getFill()->getStartColor()->setARGB('0a4277');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingRowForCDR)->getAlignment()->setShrinkToFit(true);
		
		// code for enterprise and group
		$headingSecondRowForCDR = $headingRowForCDR + 1;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$headingSecondRowForCDR, 'Enterprise:');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$headingSecondRowForCDR, htmlspecialchars($_SESSION["sp"]));
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$headingSecondRowForCDR, 'Date: ');
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$headingSecondRowForCDR, date('m/d/Y h:i:s'));
		$objPHPExcel->getActiveSheet()->mergeCells('I'.$headingSecondRowForCDR.':J'.$headingSecondRowForCDR);
		
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$headingSecondRowForCDR, '#12566');
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingSecondRowForCDR)->getFont()->setSize(16);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingSecondRowForCDR)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$headingSecondRowForCDR)->getFont()->setSize(16);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$headingSecondRowForCDR)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$headingSecondRowForCDR)->getFont()->setSize(16);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$headingSecondRowForCDR)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$headingSecondRowForCDR)->getFont()->setSize(16);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$headingSecondRowForCDR)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingSecondRowForCDR)->getFont()->getColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$headingSecondRowForCDR)->getFont()->getColor()->setARGB(self::SKY_BLUE);
		$objPHPExcel->getActiveSheet()->getStyle('D'.$headingSecondRowForCDR)->getFont()->getColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$headingSecondRowForCDR)->getFont()->getColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$objPHPExcel->getActiveSheet()->getStyle('I'.$headingSecondRowForCDR)->getFont()->getColor()->setARGB(self::SKY_BLUE);
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingSecondRowForCDR.':I'.$headingSecondRowForCDR)->getFill()->setFillType(PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingSecondRowForCDR.':I'.$headingSecondRowForCDR)->getFill()->getStartColor()->setARGB('467ab8');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingSecondRowForCDR)->getAlignment()->setShrinkToFit(true);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$headingSecondRowForCDR)->getAlignment()->setShrinkToFit(true);
		// End code for enterprise and group.
		
		//code start for third row
		$headingThirdRowForCDR = $headingSecondRowForCDR + 1;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$headingThirdRowForCDR, 'Group ID:');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$headingThirdRowForCDR, htmlspecialchars($_SESSION["groupId"]));
		$objPHPExcel->getActiveSheet()->mergeCells('C'.$headingThirdRowForCDR.':J'.$headingThirdRowForCDR);

		$objPHPExcel->getActiveSheet()->setCellValue('F'.$headingThirdRowForCDR, '#12566');

		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingThirdRowForCDR)->getFont()->setSize(16);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingThirdRowForCDR)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$headingThirdRowForCDR)->getFont()->setSize(16);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$headingThirdRowForCDR)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingThirdRowForCDR)->getFont()->getColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$headingThirdRowForCDR)->getFont()->getColor()->setARGB(self::SKY_BLUE);
		$objPHPExcel->getActiveSheet()->getStyle('J'.$headingThirdRowForCDR)->getFont()->getColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingThirdRowForCDR.':I'.$headingThirdRowForCDR)->getFill()->setFillType(PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingThirdRowForCDR.':I'.$headingThirdRowForCDR)->getFill()->getStartColor()->setARGB('467ab8');
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingThirdRowForCDR)->getAlignment()->setShrinkToFit(true);

		//code ends for third row

// 		//code start for fourth row (empty row)
		$headingFourthRowForCDR = $headingThirdRowForCDR + 1;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$headingFourthRowForCDR, ' ');
	        $objPHPExcel->getActiveSheet()->mergeCells('A'.$headingFourthRowForCDR.':J'.$headingFourthRowForCDR);
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingFourthRowForCDR)->getFont()->getColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingFourthRowForCDR.':J'.$headingFourthRowForCDR)->getFill()->getStartColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingFourthRowForCDR)->getAlignment()->setShrinkToFit(true);
		
		//code ends for fourth row
			
		//code start for fifth row
		$headingFifthRowForCDR = $headingFourthRowForCDR + 1;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$headingFifthRowForCDR, 'Call Detail for:');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$headingFifthRowForCDR, date('m/d/Y', strtotime($from)).' - '.date('m/d/Y', strtotime($to)));

		$objPHPExcel->getActiveSheet()->getRowDimension($headingFifthRowForCDR)->setRowHeight(40);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingFifthRowForCDR.':J'.$headingFifthRowForCDR)->getAlignment()->setVertical(PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingFifthRowForCDR)->getFont()->setSize(16);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingFifthRowForCDR)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$headingFifthRowForCDR)->getFont()->setSize(14);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$headingFifthRowForCDR)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingFifthRowForCDR)->getFont()->getColor()->setARGB(self::BLUE);
		$objPHPExcel->getActiveSheet()->getStyle('B'.$headingFifthRowForCDR)->getFont()->getColor()->setARGB(self::DARK_BLUE);

		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingFifthRowForCDR.':J'.$headingFifthRowForCDR)->getFill()->setFillType(PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingFifthRowForCDR.':J'.$headingFifthRowForCDR)->getFill()->getStartColor()->setARGB(self::SKY_BLUE);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$headingFifthRowForCDR)->getAlignment()->setShrinkToFit(true);

		//code ends for fifth row

		$rowForColumnCDR = $headingFifthRowForCDR + 1;
		$columnRowForCDRIndex = array("A", "B", "C", "D", "E", "F", "G");
		$countColumnForCDRIndex = count($columnRowForCDRIndex);
		$lastColumnRowForCDR = $columnRowForCDRIndex[$countColumnForCDRIndex-1];
		//print column heading for the registration
		foreach($coulmnNamesCDR as $key=>$val){
			//set value in column heading
			$objPHPExcel->getActiveSheet()->setCellValue($columnRowForCDRIndex[$key].$rowForColumnCDR, $val);
			//auto size cell
			//fill color in cells make it solid first and next fill the color
			$objPHPExcel->getActiveSheet()->getStyle($columnRowForCDRIndex[$key].$rowForColumnCDR)->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->getStyle($columnRowForCDRIndex[$key].$rowForColumnCDR)->getFont()->getColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
                        
                        $objPHPExcel->getActiveSheet()->getStyle($columnRowForCDRIndex[$key].$rowForColumnCDR)->getFill()->setFillType(PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
			$objPHPExcel->getActiveSheet()->getStyle($columnRowForCDRIndex[$key].$rowForColumnCDR)->getFill()->getStartColor()->setARGB(self::BLUE);
                        
                        
                        $objPHPExcel->getActiveSheet()->getStyle($columnRowForCDRIndex[$key].$rowForColumnCDR)->getBorders()->getTop()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                        $objPHPExcel->getActiveSheet()->getStyle($columnRowForCDRIndex[$key].$rowForColumnCDR)->getBorders()->getBottom()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                        $objPHPExcel->getActiveSheet()->getStyle($columnRowForCDRIndex[$key].$rowForColumnCDR)->getBorders()->getLeft()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                        $objPHPExcel->getActiveSheet()->getStyle($columnRowForCDRIndex[$key].$rowForColumnCDR)->getBorders()->getRight()->setBorderStyle(PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
                        
		}
		
		// Gray color from H9 to J9.
		$objPHPExcel->getActiveSheet()->mergeCells('H'.$rowForColumnCDR.':J'.$rowForColumnCDR);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$rowForColumnCDR.':J'.$rowForColumnCDR)->getFill()->setFillType(PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('H'.$rowForColumnCDR.':J'.$rowForColumnCDR)->getFill()->getStartColor()->setARGB(self::Light_Grey);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(26);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);

		// Set style for header row using alternative method
		$objPHPExcel->getActiveSheet()->getStyle($columnRowForCDRIndex[0].$rowForColumnCDR.':'.$lastColumnRowForCDR.$rowForColumnCDR)->applyFromArray(
				array(
					'font'    => array(
						'bold'      => true
					),
					'alignment' => array(
						'horizontal' => PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
					),
					'borders' => array(
					    'allborders' => array(
					        'style' => PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					        'color' => array('argb' => '000000')
					    )
					),
					'fill' => array(
						'type'       => PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
							'rotation'   => 90,
						'startcolor' => array(
							'argb' => '467ab8'
						),
						'endcolor'   => array(
							'argb' => '467ab8'
						)
					)
				)
		);

		// Unprotect a cell
		$objPHPExcel->getActiveSheet()->getStyle('B'.$rowForColumnCDR)->getProtection()->setLocked(PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_UNPROTECTED);

		//dynamic registration row for the user
		$cdrRowStartted = $dynamicRowForColumnCDR = $rowForColumnCDR + 1;

                if(!empty($cdrReportArray)){
                    foreach($cdrReportArray as $key1=>$val1){
                            $objPHPExcel->getActiveSheet()->setCellValue('A'.$dynamicRowForColumnCDR, $val1[0]);
                            $objPHPExcel->getActiveSheet()->setCellValue('B'.$dynamicRowForColumnCDR, $val1[1]);
                            $objPHPExcel->getActiveSheet()->setCellValue('C'.$dynamicRowForColumnCDR, $val1[2]);
                            $objPHPExcel->getActiveSheet()->setCellValue('D'.$dynamicRowForColumnCDR, $val1[3]);
                            $objPHPExcel->getActiveSheet()->setCellValue('E'.$dynamicRowForColumnCDR, $val1[4]);
                            $objPHPExcel->getActiveSheet()->setCellValue('F'.$dynamicRowForColumnCDR, $val1[5]);
                            $objPHPExcel->getActiveSheet()->setCellValue('G'.$dynamicRowForColumnCDR, $val1[6]);
                            $dynamicRowForColumnCDR++;
                    }
                }

		$lastRowCDR = $dynamicRowForColumnCDR - 1;
                //$styleArray = array();
		$objPHPExcel->getActiveSheet()->getStyle('A'.$cdrRowStartted.':G'.$lastRowCDR)->getFill()->setFillType(PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$cdrRowStartted.':G'.$lastRowCDR)->getFill()->getStartColor()->setARGB('f2f2f2');
		$objPHPExcel->getActiveSheet()
			->getStyle('B'.$cdrRowStartted.':G'.$lastRowCDR)
			->getAlignment()
			->setHorizontal(PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()
			->getStyle('A'.$cdrRowStartted.':A'.$lastRowCDR)
			->getAlignment()
			->setHorizontal(PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
		$styleArray = array(
			  'borders' => array(
				  'allborders' => array(
					  'style' => PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
					  'color' => array('argb' => 'e5e3e3')
				  )
			  ),
		    'alignment' => array(
		        'horizontal' => PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
		    )
		  );
		
		//code start for fourth row (empty row)
		$lastRowCDR = $lastRowCDR + 1;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$lastRowCDR, ' ');
		$objPHPExcel->getActiveSheet()->mergeCells('A'.$lastRowCDR.':J'.$lastRowCDR);
		$objPHPExcel->getActiveSheet()->getRowDimension($lastRowCDR)->setRowHeight(30);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$lastRowCDR)->getFont()->getColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$lastRowCDR.':J'.$lastRowCDR)->getFill()->getStartColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
		$objPHPExcel->getActiveSheet()->getStyle('A'.$lastRowCDR)->getAlignment()->setShrinkToFit(true);
		
		//code ends for fourth row
		
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$cdrRowStartted.':G'.$lastRowCDR)->applyFromArray($styleArray);

		return count($cdrReportArray);
	}

		public function generateSiteReportXLS($users, $filename){
			
			//making excel write object
                        /*
                        $objPHPExcel = new PHPExcel();
			header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			header("Content-Disposition: attachment; filename=\"$filename\"");
			header("Cache-Control: max-age=0");
                        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                        $this->excelCreator($objPHPExcel);
                        */
                        
                        $coulmnNamesRegistration = $this->getColumnNamesCDR();
                        
                        
			$objPHPExcel = new PhpOffice\PhpSpreadsheet\Spreadsheet();
                        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                        header("Content-Disposition: attachment; filename=\"$filename\"");
                        header("Cache-Control: max-age=0");
                        $objWriter = PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, 'Xlsx');
                        $this->excelCreator($objPHPExcel);
			//code for cdr excel
			$headingRowForCDR = 1;                        
			$coulmnNamesRegistration = $this->getColumnNamesCDR();
                        
			$cdrReportArray = $this->getCdrDataAsArray($_POST["fDate"], $_POST["tDate"]);
                                                
			$from = $this->dateConversion($_POST["fDate"], "-30");
			$to = $this->dateConversion($_POST["tDate"], "+1");
                        
			$cdrExcelRowCount = $this->generateCdrXLS($objPHPExcel, $coulmnNamesRegistration, $cdrReportArray, $headingRowForCDR, $from, $to);
			//code ends for cd excel

			//code for registration excel
			//get column name for registration
			$headingRowForRegistration = $cdrExcelRowCount + 8;

			$coulmnNamesRegistration = $this->getColumnNames();                        
                        
			//get user data to print in excel
			$userArray = $this->getUserData($users);
			//get output in excel
			$registrationExcel = $this->generateRegistrationXLS($objPHPExcel, $coulmnNamesRegistration, $userArray, $headingRowForRegistration);
			//code registration excel ends
                        
			//footer function for excel to save the data
			$this->generateExcelFooter($objPHPExcel, $objWriter, $filename);
                        //die();

		}

		//code to save excel
		public function generateExcelFooter($objPHPExcel, $objWriter, $filename){

			// Set page orientation and size
			//$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
			//$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

                        $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
                        $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);

			// Rename first worksheet
			//echo date('H:i:s') , " Rename first worksheet" , EOL;
			$objPHPExcel->getActiveSheet()->setTitle('Summary Report');

			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);

			// Save Excel 2007 file
			$callStartTime = microtime(true);
			ob_clean();
			$objWriter->save("php://output");
			$callEndTime = microtime(true);
			$callTime = $callEndTime - $callStartTime;

		}

		private function dateConversion($date, $addDaysIfDateEmpty){

			if (strlen($date) > 0){
				$formattedDate = trim($date);
				$expFrom = explode("/", $formattedDate);
				$y = $expFrom[2];
				$m = $expFrom[0];
				$d = $expFrom[1];
				$formattedDate = $y . "-" . $m . "-" . $d;
				return $formattedDate;
			}else{
				$formattedDate1 = date("Y-m-d", strtotime($addDaysIfDateEmpty." days")); //limit CDRs to the past 30 days
				return $formattedDate1;
			}
		}

		function getCDRSummary($fromDate, $toDate){
			global $billDB;
                       
                        $whereCondtn = "AND 1=1";
                        if($fromDate != "" && $toDate != ""){
                            $from = $this->dateConversion($fromDate, "-30");
                            $to = $this->dateConversion($toDate, "+1");
                            $whereCondtn = " AND startDate between date('".$from."') AND date('".$to."') ";
                        }

			$query = "SELECT * from broadsoft_cdrs where customerId='" . $_SESSION["groupId"] . "'  
					  AND serviceProvider = '".$_SESSION["sp"]."'
					  AND direction='Originating' $whereCondtn order by responsibleParty desc";


			$_SESSION["query"] = $query;
			openssl_error_string();
			$data = $billDB->query($query);
                        $result = array();
                        if(!empty($data))
                        {
                            // Phone Number, 411, 911, LD, Local, Intl                            
                            while ($row = $data->fetch())
                            {
                                    if(!isset($result[$row["responsibleParty"]])) {
                                            $result[$row["responsibleParty"]]["responsibleParty"] = $row["responsibleParty"];
                                            $result[$row["responsibleParty"]]["411"] = 0;
                                            $result[$row["responsibleParty"]]["911"] = 0;
                                            $result[$row["responsibleParty"]]["LD"] = 0;
                                            $result[$row["responsibleParty"]]["Local"] = 0;
                                            $result[$row["responsibleParty"]]["Intl"] = 0;
                                            $result[$row["responsibleParty"]]["TollFree"] = 0;

                                    }
                                    if ($row["calledNumber"] === "411")	{//All 411 calls
                                            $result[$row["responsibleParty"]]["411"]++;
                                    } else if ($row["calledNumber"] === "911") { //911 calls
                                            $result[$row["responsibleParty"]]["911"]++;
                                    }else if (substr($row["calledNumber"], 0, 3) === "011") {// international call
                                            $result[$row["responsibleParty"]]["Intl"]++;
                                    }else if($this->isTollFree($row["calledNumber"]) ){//Toll Free
                                            $result[$row["responsibleParty"]]["TollFree"]++;
                                    }else if($this->isLDCall($row["responsibleParty"], $row["calledNumber"])){//LD
                                            $result[$row["responsibleParty"]]["LD"]++;
                                    }else if($this->isLocalCall($row["responsibleParty"], $row["calledNumber"]) ){//Local
                                            $result[$row["responsibleParty"]]["Local"]++;
                                    }
                            }
                        }
			return $result;
		}

		private function isLDCall($respNumber, $calledNumber) {
			$respAreaCode = $this->getAreaCode($respNumber);
			$calledAreaCode = $this->getAreaCode($calledNumber);

			return substr($calledNumber, 0, 1) === "1" and strlen($calledNumber) == 11 and ($respAreaCode != $calledAreaCode);
		}

		private function isLocalCall($respNumber, $calledNumber) {
			$respAreaCode = $this->getAreaCode($respNumber);
			$calledAreaCode = $this->getAreaCode($calledNumber);
			return ($respAreaCode == $calledAreaCode) || (strlen($calledNumber) < 10);
		}

		private function isTollFree($number) {
			$respLength = strlen($number);
			$respNumber = $number;
			if(substr($number, 0, 1) == "1") {
				$respNumber = substr($number, 1);
			}
			if(substr($number, 0, 2) == "+1") {
				$respNumber = substr($number, 2);
			}

			$areaCode = substr($respNumber, 0, 3);
			return in_array($areaCode, $this->tollFreeReports);

		}
		function getAreaCode($number) {
			$respLength = strlen($number);
			$respNumber = $number;
			if(substr($number, 0, 1) == "1") {
				$respNumber = substr($number, 1);
			}
			if(substr($number, 0, 2) == "+1") {
				$respNumber = substr($number, 2);
			}
			$areaCode = substr($respNumber, 0, 3);
			return $areaCode;
		}

		function getReportHeaders() {
			return $this->reportHeaders;
		}

		public function getCdrDataAsArray($fromDate, $toDate){
                    
			$cdrResult = $this->getCDRSummary($fromDate, $toDate);
                        $cdrResult = array();
			$cdrArray = array();
			if(count($cdrResult) > 0){
				foreach($cdrResult as $key=>$val){
					foreach($val as $key1=>$val1){
						$cdrArray[$key][] = $val1;
					}
				}
				return $cdrArray;
			}
		}

}
