<?php
	$xmlinput = xmlHeader($sessionid, "GroupScheduleGetListRequest17sp1");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->scheduleGlobalKey as $key => $value)
	{
		$schedules[$a]["name"] = strval($value->scheduleKey->scheduleName);
		$schedules[$a]["type"] = strval($value->scheduleKey->scheduleType);
        $schedules[$a]["level"] = strval($value->scheduleLevel);

        $_SESSION["schedules"][$a]["name"] = strval($value->scheduleKey->scheduleName);
        $_SESSION["schedules"][$a]["type"] = strval($value->scheduleKey->scheduleType);
        $_SESSION["schedules"][$a]["level"] = strval($value->scheduleLevel);
		$a++;
	}

	if (isset($schedules))
	{
		$schedules = subval_sort($schedules, "name");
	}
?>
