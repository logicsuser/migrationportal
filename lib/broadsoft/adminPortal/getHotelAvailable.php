<?php
	$xmlinput = xmlHeader($sessionid, "UserHotelingGuestGetAvailableUserListRequest");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	if (isset($xml->command->hostUserTable->row))
	{
		foreach ($xml->command->hostUserTable->row as $key => $value)
		{
			$hotelUsers[$a]["userId"] = strval($value->col[0]);
			$hotelUsers[$a]["userName"] = strval($value->col[1]) . ", " . strval($value->col[2]);
			$a++;
		}
	}

	if (isset($hotelUsers))
	{
		$hotelUsers = subval_sort($hotelUsers, "userName");
	}
?>
