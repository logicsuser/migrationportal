<?php
/*
 * Get Device Name from User Endpoint
 */

function getUserEndpointDeviceName($ociVersion, $sessionid, $userId, $client)
{
    $deviceName = "";
    $portNumber = "";
    if ($ociVersion == "17") {
        $xmlinput = xmlHeader($sessionid, "UserGetRequest17sp4");
    }
    else if ($ociVersion == "20") {
        $xmlinput = xmlHeader($sessionid, "UserGetRequest20");
    }
    else {
        $xmlinput = xmlHeader($sessionid, "UserGetRequest19");
    }
    
    $xmlinput .= "<userId>" . $userId . "</userId>";
    $xmlinput .= xmlFooter();
    $response = $client->processOCIMessage(array("in0" => $xmlinput));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

    if(isset($xml->command->accessDeviceEndpoint->accessDevice->deviceName)){
        $deviceName = $xml->command->accessDeviceEndpoint->accessDevice->deviceName;
    }
    
    if(isset($xml->command->accessDeviceEndpoint->portNumber)){
        $portNumber = $xml->command->accessDeviceEndpoint->portNumber;
       
    }
    
    return $deviceName."---".$portNumber;
}

?>

