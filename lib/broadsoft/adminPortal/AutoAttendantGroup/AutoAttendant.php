<?php

/**
 * Created by Sologics.
 * Date: 27/07/2017
 */
class AutoAttendantGroup
{	
	public $groupId, $sp;
	
	public function __construct($sp, $groupId) {
		$this->groupId = $groupId;
		$this->sp = $sp;
	}
	
	public function addAutoAttendantGroupRequest($postArray){
		global $sessionid, $client;
		
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantAddInstanceRequest19");
		$xmlinput .= "<serviceProviderId>". htmlspecialchars($this->sp) ."</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($this->groupId) ."</groupId>";
		$xmlinput .= "<serviceUserId>".$postArray['serviceUserId']."</serviceUserId>";
		$xmlinput .= "<serviceInstanceProfile>";
		$xmlinput .= "<name>".$postArray['name']."</name>";
		$xmlinput .= "<callingLineIdLastName>".$postArray['callingLineIdLastName']."</callingLineIdLastName>";
		$xmlinput .= "<callingLineIdFirstName>".$postArray['callingLineIdFirstName']."</callingLineIdFirstName>";
		$xmlinput .= "<phoneNumber>".$postArray['phoneNumber']."</phoneNumber>";
		$xmlinput .= "<extension>".$postArray['extension']."</extension>";
		if(isset($postArray['department']) && !empty($postArray['department'])){
			$xmlinput .= "<department>".$postArray['department']."</department>";
		}
		if(isset($postArray['language']) && !empty($postArray['language'])){
			$xmlinput .= "<language>".$postArray['language']."</language>";
		}
		if(isset($postArray['timeZone']) && !empty($postArray['timeZone'])){
			$xmlinput .= "<timeZone>".$postArray['timeZone']."</timeZone>";
		}
		if(isset($postArray['alias']) && !empty($postArray['alias'])){
			$xmlinput .= "<alias>".$postArray['alias']."</alias>";
		}
		if(isset($postArray['publicUserIdentity']) && !empty($postArray['publicUserIdentity'])){
			$xmlinput .= "<publicUserIdentity>".$postArray['publicUserIdentity']."</publicUserIdentity>";
		}
		$xmlinput .= "</serviceInstanceProfile>";
		$xmlinput .= "<type>".$postArray['type']."</type>";
		$xmlinput .= "<enableVideo>".$postArray['enableVideo']."</enableVideo>";
		
		if((isset($postArray['businessHouresType']) && !empty($postArray['businessHouresType'])) && (isset($postArray['businessHoursName']) && !empty($postArray['businessHoursName']))){
			$xmlinput .= "<businessHours>";
			$xmlinput .= "<type>".$postArray['businessHouresType']."</type>";
			$xmlinput .= "<name>".$postArray['businessHoursName']."</name>";
			$xmlinput .= "</businessHours>";
		}
		if((isset($postArray['holidayScheduleType']) && !empty($postArray['holidayScheduleType'])) && (isset($postArray['holidayScheduleName']) && !empty($postArray['holidayScheduleName']))){
			$xmlinput .= "<holidaySchedule>";
			$xmlinput .= "<type>".$postArray['holidayScheduleType']."</type>";
			$xmlinput .= "<name>".$postArray['holidayScheduleName']."</name>";
			$xmlinput .= "</holidaySchedule>";
		}
		
		$xmlinput .= "<extensionDialingScope>".$postArray['extensionDialingScope']."</extensionDialingScope>";
		$xmlinput .= "<nameDialingScope>".$postArray['nameDialingScope']."</nameDialingScope>";
		$xmlinput .= "<nameDialingEntries>".$postArray['nameDialingEntries']."</nameDialingEntries>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		return $xml;
	}
	
	//function to get list of all attendants
	public function getAllAutoAttendants($ociVersion){ 
		global $sessionid, $client;
		$autoAttendants = array();
		
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantGetInstanceListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($this->sp) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId) . "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		$a = 0;
		if (isset($xml->command->autoAttendantTable->row))
		{
			foreach ($xml->command->autoAttendantTable->row as $key => $value)
			{
				if (($ociVersion == "17" and strval($value->col[7]) !== "Standard") or $ociVersion !== "17")
				{
					$autoAttendants[$a]["id"] = strval($value->col[0]);
					$autoAttendants[$a]["name"] = strval($value->col[1]);
					$a++;
				}
			}
		}
		
		if (isset($autoAttendants))
		{
			$autoAttendants = subval_sort($autoAttendants, "name");
		}
		return $autoAttendants;
	}
	
	public function getAutoAttendantInfo($aaId, $ociVersion){
		global $sessionid, $client;
		//unset($_SESSION["aa"]);
		
		$aaInfo = array();
		
		 
		if ($ociVersion == "19")
		{
			$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantGetInstanceRequest19");
		}
		else
		{
			$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantGetInstanceRequest20");			
		}
		$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		$aaInfo["aa"]["aaName"] = strval($xml->command->serviceInstanceProfile->name);
		$aaInfo["aa"]["callingLineIdLastName"] = strval($xml->command->serviceInstanceProfile->callingLineIdLastName);
		$aaInfo["aa"]["callingLineIdFirstName"] = strval($xml->command->serviceInstanceProfile->callingLineIdFirstName);
		$aaInfo["aa"]["phoneNumber"] = strval($xml->command->serviceInstanceProfile->phoneNumber);
		$aaInfo["aa"]["extension"] = strval($xml->command->serviceInstanceProfile->extension);
		$aaInfo["aa"]["timeZone"] = strval($xml->command->serviceInstanceProfile->timeZone);
		if ($ociVersion == "17")
		{
			$aaInfo["aa"]["type"] = "Basic";
		}
		else
		{
			$aaInfo["aa"]["type"] = strval($xml->command->type);
		}
		$aaInfo["aa"]["businessHoursSchedule"] = strval($xml->command->businessHours->name);
		$aaInfo["aa"]["holidaySchedule"] = strval($xml->command->holidaySchedule->name);
		$aaInfo["aa"]["nameDialingEntries"] = strval($xml->command->nameDialingEntries);
		
		$aaInfo["aa"]["bh"]["announcementSelection"] = strval($xml->command->businessHoursMenu->announcementSelection);
		//	$_SESSION["aa"]["bh"]["audioFileDescription"] = strval($xml->command->businessHoursMenu->audioFileDescription);
		$aaInfo["aa"]["bh"]["enableLevelExtensionDialing"] = strval($xml->command->businessHoursMenu->enableFirstMenuLevelExtensionDialing);
		foreach ($xml->command->businessHoursMenu->keyConfiguration as $key => $value)
		{
			$aaInfo["aa"]["bh"]["keys"][strval($value->key)]["desc"] = strval($value->entry->description);
			$aaInfo["aa"]["bh"]["keys"][strval($value->key)]["action"] = strval($value->entry->action);
			$aaInfo["aa"]["bh"]["keys"][strval($value->key)]["phoneNumber"] = strval($value->entry->phoneNumber);
			if ($ociVersion == "17")
			{
				$aaInfo["aa"]["bh"]["keys"][strval($value->key)]["submenuId"] = "";
			}
			else
			{
				$aaInfo["aa"]["bh"]["keys"][strval($value->key)]["submenuId"] = strval($value->entry->submenuId);
			}
		}
		
		$aaInfo["aa"]["ah"]["announcementSelection"] = strval($xml->command->afterHoursMenu->announcementSelection);
		//	$_SESSION["aa"]["ah"]["audioFileDescription"] = strval($xml->command->afterHoursMenu->audioFileDescription);
		$aaInfo["aa"]["ah"]["enableLevelExtensionDialing"] = strval($xml->command->afterHoursMenu->enableFirstMenuLevelExtensionDialing);
		foreach ($xml->command->afterHoursMenu->keyConfiguration as $key => $value)
		{
			$aaInfo["aa"]["ah"]["keys"][strval($value->key)]["desc"] = strval($value->entry->description);
			$aaInfo["aa"]["ah"]["keys"][strval($value->key)]["action"] = strval($value->entry->action);
			$aaInfo["aa"]["ah"]["keys"][strval($value->key)]["phoneNumber"] = strval($value->entry->phoneNumber);
			if ($ociVersion == "17")
			{
				$aaInfo["aa"]["ah"]["keys"][strval($value->key)]["submenuId"] = "";
			}
			else
			{
				$aaInfo["aa"]["ah"]["keys"][strval($value->key)]["submenuId"] = strval($value->entry->submenuId);
			}
		}
		
		if ($aaInfo["aa"]["type"] == "Standard") //BroadSoft version 19 or later
		{
			$aaInfo["aa"]["h"]["announcementSelection"] = strval($xml->command->holidayMenu->announcementSelection);
			//		$_SESSION["aa"]["h"]["audioFileDescription"] = strval($xml->command->holidayMenu->audioFileDescription);
			$aaInfo["aa"]["h"]["enableLevelExtensionDialing"] = strval($xml->command->holidayMenu->enableFirstMenuLevelExtensionDialing);
			foreach ($xml->command->holidayMenu->keyConfiguration as $key => $value)
			{
				$aaInfo["aa"]["h"]["keys"][strval($value->key)]["desc"] = strval($value->entry->description);
				$aaInfo["aa"]["h"]["keys"][strval($value->key)]["action"] = strval($value->entry->action);
				$aaInfo["aa"]["h"]["keys"][strval($value->key)]["phoneNumber"] = strval($value->entry->phoneNumber);
				$aaInfo["aa"]["h"]["keys"][strval($value->key)]["submenuId"] = strval($value->entry->submenuId);
			}
			
			$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuGetListRequest");
			$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			
			$a = 0;
			foreach ($xml->command->submenuTable->row as $key => $value)
			{
				$aaInfo["aa"]["submenu" . $a]["id"] = strval($value->col[0]);
				
				if ($ociVersion == "20")
				{
					$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuGetRequest20");
				}
				else
				{
					$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuGetRequest");
				}
				$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
				$xmlinput .= "<submenuId>" . htmlspecialchars(strval($value->col[0])) . "</submenuId>";
				$xmlinput .= xmlFooter();
				$response = $client->processOCIMessage(array("in0" => $xmlinput));
				$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
				
				$aaInfo["aa"]["submenu" . $a]["enableLevelExtensionDialing"] = strval($xml->command->enableLevelExtensionDialing);
				foreach ($xml->command->keyConfiguration as $k => $v)
				{
					$aaInfo["aa"]["submenu" . $a]["keys"][strval($v->key)]["desc"] = strval($v->entry->description);
					$aaInfo["aa"]["submenu" . $a]["keys"][strval($v->key)]["action"] = strval($v->entry->action);
					$aaInfo["aa"]["submenu" . $a]["keys"][strval($v->key)]["phoneNumber"] = strval($v->entry->phoneNumber);
					$aaInfo["aa"]["submenu" . $a]["keys"][strval($v->key)]["submenuId"] = strval($v->entry->submenuId);
				}
				$a++;
			}
		}
		return $aaInfo;
	}

	
	//function to get list of all attendants
	public function getAllSchedules(){
		global $sessionid, $client;
		$schedules = array();
		
		$xmlinput = xmlHeader($sessionid, "GroupScheduleGetListRequest17sp1");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($this->sp) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($this->groupId) . "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		$a = 0;
		foreach ($xml->command->scheduleGlobalKey as $key => $value)
		{
			$schedules[$a]["name"] = strval($value->scheduleKey->scheduleName);
			$schedules[$a]["type"] = strval($value->scheduleKey->scheduleType);
			$schedules[$a]["level"] = strval($value->scheduleLevel);
						$a++;
		}
		
		if (isset($schedules))
		{
			$schedules = subval_sort($schedules, "name");
		}
		return $schedules;
	}
}
