<?php
	if ($ccdName !== "")
	{
		$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryGetRequest17");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<name>" . htmlspecialchars($ccdName) . "</name>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		$a = 0;
		foreach ($xml->command->userTable->row as $key => $value)
		{
			$users[$a]["id"] = strval($value->col[0]);
			$users[$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
			$a++;
		}

		if (isset($users))
		{
			$users = subval_sort($users, "name");
		}
	}

	$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryGetAvailableUserListRequest17");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->userTable->row as $key => $value)
	{
		if ((isset($users) and !searchArray($users, "id", strval($value->col[0]))) or !isset($users))
		{
			$availableUsers[$a]["id"] = strval($value->col[0]);
			$availableUsers[$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
			$a++;
		}
	}

	if (isset($availableUsers))
	{
		$availableUsers = subval_sort($availableUsers, "name");
	}
?>
