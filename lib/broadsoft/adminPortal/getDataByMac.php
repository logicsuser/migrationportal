<?php 

class getDataByMac
{
    function getDeviceListOfGroup($serviceProviderId, $groupId, $macSearchValue)
    {
        global $sessionid, $client;
        $returnArray = Array();
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetListRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProviderId) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
        if (isSet($macSearchValue) && trim($macSearchValue) != "") {
            $xmlinput .= "<searchCriteriaDeviceMACAddress>
                            				<mode>Contains</mode>
                            				<value>" . $macSearchValue . "</value>
                            				<isCaseInsensitive>true</isCaseInsensitive>
                            			</searchCriteriaDeviceMACAddress>";
        }
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array(
            "in0" => $xmlinput
        ));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        } else {
            if (isset($xml->command->accessDeviceTable->row)) {
                $b = 0;
                foreach ($xml->command->accessDeviceTable->row as $k => $v) {
                    $returnArray[$b] = Array(
                        'spId' => $serviceProviderId,
                        'grpId' => $groupId,
                        'deviceType' => strval($v->col[1]),
                        'mac' => strval($v->col[4]),
                        'deviceName' => strval($v->col[0])
                        );
                    $b ++;
                }
            }
            $returnResponse["Success"] = $returnArray;
        }
        return $returnResponse;
    }
    
    function getAllEnterprise()
    {
        global $sessionid, $client;
        $sps = array();
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderGetListRequest");
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        } else {
            $a = 0;
            foreach ($xml->command->serviceProviderTable->row as $k => $v)
            {
                $sps[$a] = strval($v->col[0]);
                $a++;
            }
            
            $sps = subval_sort($sps);
            $returnResponse["Success"] = $sps;
        }
        return $returnResponse;
    }
    
    function getGroupListOfProvider($provider)
    {
        global $sessionid, $client;
        $allGroups = Array();
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "GroupGetListInServiceProviderRequest");
        $xmlinput .= "<serviceProviderId>". htmlspecialchars($provider)."</serviceProviderId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        } else {
            $a = 0;
            if (isset($xml->command->groupTable->row))
            {
                foreach ($xml->command->groupTable->row as $key => $value)
                {
                    $allGroups[$a]['spId'] = $provider;
                    $allGroups[$a]['grpId'] = strval($value->col[0]);
                    $a++;
                }
            }
            
            $returnResponse["Success"] = $allGroups;
        }
        return $returnResponse;
    }
    
    function getDeviceListOfEnterprise($serviceProviderId, $macSearchValue)
    {
        global $sessionid, $client;
        $returnArray = array();
        $returnResponse["Success"] = "";
        $enterpriseList = $this->getGroupListOfProvider($serviceProviderId);
        if(count($enterpriseList['Success']) > 0)
        {
            foreach($enterpriseList['Success'] as $entKey => $entValue)
            {
                $deviceList = $this->getDeviceListOfGroup($serviceProviderId, strval($entValue['grpId']), $macSearchValue);
                if(count($deviceList['Success']) > 0)
                {
                    foreach($deviceList['Success'] as $k => $v)
                    {
                        $returnArray[] = $v;
                    }
                }
            }
            $returnResponse["Success"] = $returnArray;
        }
        
        return $returnResponse;
    }
    
    function getDeviceListOfSystem($macSearchValue)
    {
        global $sessionid, $client;
        $dataResponse = array();
        $deviceData = array();
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "SystemAccessDeviceGetAllRequest");
        if (isSet($macSearchValue) && trim($macSearchValue) != "") {
            $xmlinput .= "<searchCriteriaDeviceMACAddress>
                            				<mode>Contains</mode>
                            				<value>" . $macSearchValue . "</value>
                            				<isCaseInsensitive>true</isCaseInsensitive>
                            			</searchCriteriaDeviceMACAddress>";
        }
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array(
            "in0" => $xmlinput
        ));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        } else {
            if (isset($xml->command->accessDeviceTable->row)) {
                $b = 0;
                foreach ($xml->command->accessDeviceTable->row as $k => $v) {
                    $deviceData[$b] = Array(
                        'spId' => strval($v->col[0]),
                        'grpId' => strval($v->col[2]),
                        'deviceType' => strval($v->col[4]),
                        'mac' => strval($v->col[6]),
                        'deviceName' => strval($v->col[3])
                        );
                    $b ++;
                }
            }
            $returnResponse["Success"] = $deviceData;
        }
        
        return $returnResponse;
    }
    
    function getAllUserOfDevice($serviceProviderId, $groupId, $deviceName)
    {
        global $sessionid, $client;
        $allUserOfDevice = array();
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetUserListRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProviderId) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        } else {
            if(isset($xml->command->deviceUserTable->row))
            {
                $c = 0;
                foreach ($xml->command->deviceUserTable->row as $kk => $vv)
                {
                    $userInfo['phoneNumber'] =  strval($vv->col[3]);
                    $userInfo['lastName'] =  strval($vv->col[1]);
                    $userInfo['firstName']= strval($vv->col[2]);
                    $userInfo['extn']= strval($vv->col[9]);
                    $allUserOfDevice[$c] = $userInfo;
                    $c++;
                }
            }
            $returnResponse["Success"] = $allUserOfDevice;
        }
        
        return $returnResponse;
    }
}


?>