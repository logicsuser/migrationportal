<?php

/**
 * Created by Sologics.
 * Date: 17/07/2017
 */

class Announcement
{
	//get announcement list
	public function getAnnouncement($db, $where){
		
		$qry = "SELECT * FROM announcements";
		if(count($where) > 0){
			$qry .= " WHERE ";
			if(!empty($where['announcement_type'])){
				$qry .= " `announcement_type_id` = '".$where['announcement_type']."'";
			}
			if(!empty($where['announcement_id'])){
				$qry .= " `announcement_id` = '".$where['announcement_id']."'";
			}
			if(!empty($where['sp']) && empty($where['announcement_id'])){
			    $qry .= " `sp` = '".$where['sp']."'";
			}
			if(!empty($where['group']) && empty($where['announcement_id'])){
			    $qry .= "and  `group` = '".$where['group']."'";
			}
		}
		$qry .= " ORDER BY announcement_name";  
		//echo $qry;
		$query = $db->query($qry);
		
		$r = 0;
		$result = "";
		while($row = $query->fetch()){
			$result[$r]['announcement_id'] = $row['announcement_id'];
			$result[$r]['announcement_name'] = $row['announcement_name'];
			$result[$r]['announcement_type_id'] = $row['announcement_type_id'];
			$result[$r]['announcement_file'] = $row['announcement_file'];
			$r++;
		}
		
		return $result;
	}	
	
	public function getAnnouncementSubMenu($db, $where){
	    
	    $qry = "SELECT * FROM auto_attendant_announcements";
	    if(count($where) > 0){
	        $qry .= " WHERE ";
	        if(!empty($where['auto_attendant_id'])){
	            $qry .= " `auto_attendant_id` = '".$where['auto_attendant_id']."'";
	        }
	    }
	   // $qry .= " ORDER BY announcement_name";
	    //echo $qry;
	    $query = $db->query($qry);
	    
	    $r = 0;
	    $result = array();
	    while($row = $query->fetch()){
	        $result[$r] = $row['subMenuId'];
	        $r++;
	    }
	    
	    return $result;
	}
	//get announcement type list
	public function getAnnouncementType($db){
		
		$qry = "SELECT * FROM announcement_types";		
		$query = $db->query($qry);
		
		$r = 0;
		$result = "";
		
		while($row = $query->fetch()){
			$result[$r]['announcement_type_id'] = $row['announcement_type_id'];
			$result[$r]['name'] = $row['name'];
			$r++;
		}
		
		return $result;
	}
	
	function insertAndUpdateSubmenu($db, $subData, $aaId){
	   //echo "subdata"; print_r($subData);
	    $arraySubData = array();
	    //echo "count";echo count($subData);
	    if(count($subData) > 0 && !empty($subData)){
	        foreach ($subData as $key => $value){
	            if($value != ""){
	                $arraySubData[] = $value;
	            }
	            
	        }
	    }
	    //echo "arraySubData";print_r($arraySubData);
	    $subData = implode(",", $arraySubData);
	    //echo "finalsubdata";print_r($subData);
	    $qry = "UPDATE auto_attendant_announcements SET subMenuId='$subData' where auto_attendant_id = '$aaId'";
	    //print_r($qry);die;
	    $query = $db->query($qry);
	    
	    return $query;
	}
	
	//get announcement type list
	public function getAnnouncementTypeInfo($db, $id){
		$qry = "SELECT * FROM announcement_types WHERE `announcement_type_id` = '".$id."'";
		$query = $db->query($qry);		
		$row = $query->fetch();
		return $row;
	}	
	
	//get announcement type list
	public function checkAnnoucementExist($db, $announcementName, $where){
		
		$row = array();
		$count = "";
		
		$qry = "SELECT * FROM announcements WHERE `announcement_name` = '".$announcementName."' AND `sp` = '".$where["sp"]."' AND `group` = '".$where["groupId"]."'";
		$query = $db->query($qry);		
		$row = $query->fetch();

		if(!empty($row['announcement_id'])){
			$count = "Exist";
		}
		return $count;
	}	

	//save announcement
	public function saveAnnouncement( $db, $postArray, $uploaddir, $sp, $groupId){
		
		$data = "";
		$data = array('success' =>"");
		
		if(empty($postArray['announcementId'])){ //echo "Insert"; die;
			$announcementName = trim($postArray['announcementName']);
			$qry = "INSERT into announcements SET `announcement_name` = '".addslashes($announcementName)."'";
			
			if(!empty($postArray['announcementType']) && isset($postArray['announcementType'])){
				$qry .= ", `announcement_type_id` = '".$postArray['announcementType']."'";
			}else{
				$qry .= ", `announcement_type_id` = ''";
			}
			
			if(!empty($postArray['filenames'][0]) && isset($postArray['filenames'])){
				$qry .= ", `announcement_file` = '".addslashes($postArray['filenames'][0])."'";
			}else{
				$qry .= ", `announcement_file` = ''";
			}
			if(!empty($sp) && isset($sp)){
			    $qry .= ", `sp` = '".addslashes($sp)."'";
			}else{
			    $qry .= ", `sp` = ''";
			}
			if(!empty($groupId) && isset($groupId)){
			    $qry .= ", `group` = '".addslashes($groupId)."'";
			}else{
			    $qry .= ", `group` = ''";
			}
			
			//echo $qry;
			$tpl = $db->query($qry);
			$lastId = $db->lastInsertId();
			if($lastId > 0){
				$data = array('success' => '<span class = \'labelTextGrey\'>Announcement was submitted successfully</span>', 'announcementId' => $lastId);
			}
			
		}else{ //echo "Update"; die;
			$announcementName = trim($postArray['announcementName']);
			$qry = "UPDATE announcements SET `announcement_name` = '".addslashes($announcementName)."'";
			
			if(!empty($postArray['announcementType']) && isset($postArray['announcementType'])){
				$qry .= ", `announcement_type_id` = '".$postArray['announcementType']."'";
			}else{
				$qry .= ", `announcement_type_id` = ''";
			}
			
			if(!empty($postArray['filenames'][0]) && isset($postArray['filenames'])){
				$qry .= ", `announcement_file` = '".addslashes($postArray['filenames'][0])."'";
			}else{
				$qry .= ", `announcement_file` = '".addslashes($postArray['announcementUploadVal'])."'";
			}
			$qry .= " WHERE `announcement_id` = '".$postArray['announcementId']."'"; 
			//echo $qry; die;
			$tpl = $db->query($qry);
			$data = array('success' => '<span class = \'labelTextGrey\'>Announcement has updated successfully</span>', 'announcementId' => $postArray['announcementId']);
		}
		return json_encode($data);
		
	}
	
	public function uploadAnnouncement($file, $uploaddir){
		
		$error = false;
		$files = array();
		
		$baseName = time().basename($file['name']);
		
		if(move_uploaded_file($file['tmp_name'], $uploaddir .$baseName))
		{
			$files[] = $uploaddir .$baseName;
			$filesname[] = $baseName;
		}
		else
		{
			$error = true;
		}
		
		$data = ($error) ? array('error' => 'There was an error uploading your files') : array('files' => $filesname);
		return json_encode($data);
				
	}

	//function to delete the announcement
	//get announcement list
	public function deleteAnnouncement($db, $where, $uploaddir, $ociVersion){
		
		$result['Error'] = "";
		$result['Success'] = "";
		error_reporting(0);
		$errorLast = array();
		$qry = "SELECT * FROM announcements WHERE `announcement_id` = '".$where['announcement_id']."'";
		$query = $db->query($qry);
		$row = $query->fetch();		
		
		if(!empty($row['announcement_id'])){
			if(!empty($row['announcement_file'])){
				
				$deletAutoAttendant = $this->updateAutoAttendantAfterAnnouncementDelete($db, $where['announcement_id'], $row['announcement_type_id'], $ociVersion);
				
				if($deletAutoAttendant['Error'] == ""){
					$delQry = "DELETE FROM announcements WHERE `announcement_id` = '".$where['announcement_id']."'";
					$delQuery = $db->query($delQry);
					if($delQuery){
						$result['Success']= "Deleted";						
						$filePath = $uploaddir.$row['announcement_file'];
						$unlinkSuccess = unlink($filePath);
						$errorLast = error_get_last(); 
						if(!isset($errorLast) && $unlinkSuccess == 1){
							$result['Success'] = "Success";
							//delete autoattendant
						}else{
							//$result['Error'] = "<b>Error: Announcement Deleted Successfully. Unable to locate the file </b>".$row['announcement_file'];
							$result['Error'] = "<span class = \'labelTextGrey\'>Error: Announcement Deleted Successfully.</span>";
						}
					}else{
						$result['Error'] = "Please try again";
					}				
				}else{
					$result['Error'] = $deletAutoAttendant['Error'];
				}
			}
			
		}else{
			$result['Error'] = "Announcement doesn't exist";
		}
		
		return $result;
	}	
	
	public function updateAutoAttendantAfterAnnouncementDelete($db, $announcementId, $type, $ociVersion){
	    
		$result['Error'] = "";
		$result['Success'] = "";
		$typeVal = $type;
		$typeArray = array("1"=>"bhAnnouncementId", "2"=>"ahAnnouncementId", "3"=>"hAnnouncementId", "4"=>"vpAnnouncementId");
		
		$qry = "SELECT * FROM `auto_attendant_announcements` WHERE `".$typeArray[$type]."` = '".$announcementId."'";
		$query = $db->query($qry);
		$row = $query->fetch();
		
		$type = $typeArray[$type];
		if(!empty($row[$type]) > 0){
			$aaId = $row['auto_attendant_id'];
			$updateBroadsoft = $this->modifyAutoAttendantGreetingsAfterDeleteAnnouncement($ociVersion, $typeVal, $aaId);
			
			$qry = "UPDATE `auto_attendant_announcements` SET `".$type."` = '0' WHERE `".$type."` = '".$announcementId."'";
			$query = $db->query($qry);
			$errorLast = error_get_last();
			if(!isset($errorLast)){
				$result['Success'] = "Success";
			}else{
				$result['Error'] = "Error : ".$errorLast['message'];
			}
		}
		
		return $result;
	}
	
	//get announcement list
	public function getAutoattendantAnnouncementList($db, $where){
		
		$qry = "SELECT * FROM announcements as a JOIN announcement_types as at ON a.announcement_type_id = at.announcement_type_id WHERE at.announcement_type_id = '".$where['announcement_type_id']."'";
		$qry .= " AND a.sp = '".$where["sp"]."' AND a.group = '".$where["groupId"]."'";
		
		$query = $db->query($qry);
		
		$r = 0;
		$result = "";
		while($row = $query->fetch()){
			$result[$r]['announcement_id'] = $row['announcement_id'];
			$result[$r]['announcement_name'] = $row['announcement_name'];
			$result[$r]['announcement_type_id'] = $row['announcement_type_id'];
			$result[$r]['announcement_file'] = $row['announcement_file'];
			$r++;
		}
		
		return $result;
	}
	
	//get announcement list
	public function getAutoattendantAnnouncementInfo($db, $auto_attendant_id, $field){
		
		//echo "<pre>"; print_r($where);
		$row['bhmGreeting'] = "";
		$row['ahmGreeting'] = "";
		$row['bhgGreeting'] = "";
		
		$qry = "SELECT * FROM auto_attendant_announcements WHERE auto_attendant_id = '".$auto_attendant_id."'";
		$query = $db->query($qry);
		$row = $query->fetch();
		if(count($row) > 0){
			if(!empty($field)){
				return $row[$field];
			}else{
				return $row;
			}
		}
		
	}	

	//save announcement
	public function saveAutoattendantAnnouncement($db, $postArray){
				
		//check the announcement related with autoattendant exist	
		$checkAutoattendant = $this->getAutoattendantAnnouncementInfo($db, $postArray['auto_attendant_id'], '');
		//echo "<pre>"; print_r($checkAutoattendant); die;
		
		if(empty($checkAutoattendant['auto_attendant_id'])){
			$qry = "INSERT into auto_attendant_announcements SET `auto_attendant_id` = '".$postArray['auto_attendant_id']."', `bhAnnouncementId` = '".$postArray['bhAnnouncementId']."', `ahAnnouncementId` = '".$postArray['ahAnnouncementId']."', `hAnnouncementId` = '".$postArray['hAnnouncementId']."', `vpAnnouncementId` = '".$postArray['vpAnnouncementId']."'";			
			$tpl = $db->query($qry);
			$lastId = $db->lastInsertId();	
			return "Insert";
		}else{
			$qry = "UPDATE auto_attendant_announcements SET `bhAnnouncementId` = '".addslashes($postArray['bhAnnouncementId'])."', `ahAnnouncementId` = '".addslashes($postArray['ahAnnouncementId'])."', `hAnnouncementId` = '".$postArray['hAnnouncementId']."', `vpAnnouncementId` = '".$postArray['vpAnnouncementId']."'";
			$qry .= " WHERE `auto_attendant_id` = '".$postArray['auto_attendant_id']."'";
			
			$tpl = $db->query($qry);
			return "Update";
		}
	}
	
	public function audioVideoXml($filename, $fpath){

		$header = "";
		$filePath = $fpath.$filename;
		
		if(file_exists($filePath)){
			
				$header .= "<audioFile>";
				$header .= "<description>".$filename."</description>";
				$header .= "<mediaType>WAV</mediaType>";
				if(file_exists($filePath)){
					$header .= "<content>".base64_encode(file_get_contents($filePath))."</content>";
				}
				$header .= "</audioFile>";
			
		}
		return $header;
	}
	
	//check if the announcement is attached with some autoattendant or not
	public function checkAnnouncementWithAutoattendant($db, $announcementId){
		
		$row = array();
		
		$qry = "SELECT * FROM auto_attendant_announcements WHERE bhAnnouncementId = '".$announcementId."' ";
		$qry .= " || ahAnnouncementId = '".$announcementId."'";
		$qry .= " || hAnnouncementId = '".$announcementId."'";
		$qry .= " || vpAnnouncementId = '".$announcementId."'";
		//echo $qry;
		$query = $db->query($qry);
		$row = $query->fetch();
		if(!empty($row)){
			$row = count($row);	
		}
		return $row;
		
	}	
	
	public function voicePortalCustomAnnouncementModifyRequest($aaId, $filename, $fPath){
		
		global $sessionid, $client;
		$filePath = $fPath.$filename;
		
		$voicePortalResponse["Success"] = "";
		$voicePortalResponse["Error"] = "";
		
		$xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoicePortalRequest16");
		$xmlinput .= "<userId>".$aaId."</userId>";
		$xmlinput .= "<personalizedNameAudioFile>";		
		$xmlinput .= "<description>".$filename."</description>";
		$xmlinput .= "<mediaType>WAV</mediaType>";
		if(file_exists($filePath)){
			$xmlinput .= "<content>".base64_encode(file_get_contents($filePath))."</content>";
		}
		$xmlinput .= "</personalizedNameAudioFile>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$voicePortalResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$voicePortalResponse["Success"] = "Success";
			
		}
		return $voicePortalResponse;
		
	}
	
	public function modifyAutoAttendantGreetingsAfterDeleteAnnouncement($ociVersion, $type, $aaId){
		
		global $sessionid, $client;
		$modifyResponse["Success"] = "";
		$modifyResponse["Error"] = "";
		//echo $type; die;
		if ($ociVersion == "20")
		{
			$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantModifyInstanceRequest20");
		}
		else
		{
			$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantModifyInstanceRequest17sp1");
		}
		$xmlinput .= "<serviceUserId>".$aaId."</serviceUserId>";
		
		if($type == "1"){
			$xmlinput .= "<businessHoursMenu>";
			$xmlinput .= "<announcementSelection>Default</announcementSelection>";
			$xmlinput .= "</businessHoursMenu>";
		}
		if($type == "2"){
			$xmlinput .= "<afterHoursMenu>";
			$xmlinput .= "<announcementSelection>Default</announcementSelection>";
			$xmlinput .= "</afterHoursMenu>";
		}
		if($type == "3"){
			$xmlinput .= "<holidayMenu>";
			$xmlinput .= "<announcementSelection>Default</announcementSelection>";
			$xmlinput .= "</holidayMenu>";
		}
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			$modifyResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$modifyResponse["Success"] = "Success";
			
		}
		return $modifyResponse;
	}
	
	//get announcement list
	public function getAnnouncementByFile($db, $announcement_file){
		
		$qry = "SELECT * FROM announcements";
		if(!empty($announcement_file)){
			$qry .= " WHERE ";
			$qry .= " `announcement_file` = '".$announcement_file."'";
		}
		//echo $qry;
		$query = $db->query($qry);
		
		$r = 0;
		$result = array();
		while($row = $query->fetch()){
			$result[$r]['announcement_id'] = $row['announcement_id'];
			$r++;
		}
		
		return $result;
	}	
	
	//get announcement list
	public function getAnnouncementForPlayAnnouncement($db, $where){
		
		$qry = "SELECT a.* FROM announcements as a JOIN announcement_types as at ON a.announcement_type_id = at.announcement_type_id";
		$qry .= " WHERE at.name = 'Announcements' AND a.sp = '".$where["sp"]."' AND a.group = '".$where["groupId"]."'";
		//echo $qry;
		$query = $db->query($qry);
		
		$r = 0;
		$result = array();
		while($row = $query->fetch()){
			$result[$r]['announcement_id'] = $row['announcement_id'];
			$result[$r]['announcement_name'] = $row['announcement_name'];
			$r++;
		}
		
		return $result;
	}	
	
	public function deleteAnnouncementSubmenuType($db, $where, $uploaddir, $ociVersion){
		
		$result['Error'] = "";
		$result['Success'] = "";
		error_reporting(0);
		$errorLast = array();
		$qry = "SELECT * FROM announcements WHERE `announcement_id` = '".$where['announcement_id']."'";
		$query = $db->query($qry);
		$row = $query->fetch();
		
		if(!empty($row['announcement_id'])){
			if(!empty($row['announcement_file'])){
				
				$delQry = "DELETE FROM announcements WHERE `announcement_id` = '".$where['announcement_id']."'";
				$delQuery = $db->query($delQry);
				if($delQuery){
					$result['Success']= "Deleted";
					$filePath = $uploaddir.$row['announcement_file'];
					$unlinkSuccess = unlink($filePath);
					$errorLast = error_get_last();
					if(!isset($errorLast) && $unlinkSuccess == 1){
						$result['Success'] = "Success";
						//delete autoattendant
					}else{
						//$result['Error'] = "<b>Error: Announcement Deleted Successfully. Unable to locate the file </b>".$row['announcement_file'];
						$result['Error'] = "<b>Error: Announcement Deleted Successfully.";
					}
				}else{
					$result['Error'] = "Please try again";
				}
			}
			
		}else{
			$result['Error'] = "Announcement doesn't exist";
		}
		
		return $result;
	}	
	
	
	public function makeAddArray($postArray){
		
		$logArray = array();
		foreach($postArray as $key=>$val){
			if($key == "filenames"){
				$logArray[$key] = $val[0];
			}else{
				$logArray[$key] = $val;
			}
		}
		
		unset($logArray["announcementUploadVal"]);
		unset($logArray["checkAnnouncementWIthAutoattendant"]);
		unset($logArray["annGroupName"]);
		
		return $logArray;
	}
	
	public function deleteAnnouncementTypeAnnouncement($db, $where, $uploaddir, $ociVersion){
		
		$result['Error'] = "";
		$result['Success'] = "";
		error_reporting(0);
		$errorLast = array();
		$qry = "SELECT * FROM announcements WHERE `announcement_id` = '".$where['announcement_id']."'";
		$query = $db->query($qry);
		$row = $query->fetch();
		
		if(!empty($row['announcement_id'])){
			if(!empty($row['announcement_file'])){
				
				$delQry = "DELETE FROM announcements WHERE `announcement_id` = '".$where['announcement_id']."'";
				$delQuery = $db->query($delQry);
				if($delQuery){
					$result['Success']= "Deleted";
					$filePath = $uploaddir.$row['announcement_file'];
					$unlinkSuccess = unlink($filePath);
					$errorLast = error_get_last();
					if(!isset($errorLast) && $unlinkSuccess == 1){
						$result['Success'] = "Success";
						//delete autoattendant
					}else{
						//$result['Error'] = "<b>Error: Announcement Deleted Successfully. Unable to locate the file </b>".$row['announcement_file'];
						$result['Error'] = "<b>Error: Announcement Deleted Successfully.";
					}
				}else{
					$result['Error'] = "Please try again";
				}
			}
			
		}else{
			$result['Error'] = "Announcement doesn't exist";
		}
		
		return $result;
	}
	
}