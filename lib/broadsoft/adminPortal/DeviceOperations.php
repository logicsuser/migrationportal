<?php
class DeviceOperations {
	const DEVICE_NAME = "DeviceName";
	const DEVICE_TYPE = "DeviceType";
	const MAC = "MacAddress";
	const IPADD = "IpAddress";
	const ALL = "All";
	function GroupAccessDeviceGetListRequest($providerId, $groupId, $deviceType) {
		global $sessionid, $client;
		$returnResponse = array ();
		$returnResponse ["Error"] = "";
		$returnResponse ["Success"] = "";
		$deviceInfo = array ();
		
		$xmlinput = xmlHeader ( $sessionid, "GroupAccessDeviceGetListRequest" );
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars ( $providerId ) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars ( $groupId ) . "</groupId>";
		$xmlinput .= "<searchCriteriaExactDeviceType>";
		$xmlinput .= "<deviceType>" . $deviceType . "</deviceType>";
		$xmlinput .= "</searchCriteriaExactDeviceType>";
		$xmlinput .= xmlFooter ();
		$response = $client->processOCIMessage ( array (
				"in0" => $xmlinput 
		) );
		$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
		$errMsg = readErrorXmlGenuine ( $xml );
		if ($errMsg != "") {
			$returnResponse ["Error"] = $errMsg;
		} else {
			$rownum = 0;
			if (isset ( $xml->command->accessDeviceTable->row )) {
				foreach ( $xml->command->accessDeviceTable->row as $key => $value ) {
					$deviceInfo [$rownum] ["deviceName"] = strval ( $value->col [0] );
					$deviceInfo [$rownum] ["deviceType"] = strval ( $value->col [1] );
					$deviceInfo [$rownum] ["availablePorts"] = strval ( $value->col [2] );
					$deviceInfo [$rownum] ["netAddress"] = strval ( $value->col [3] );
					$deviceInfo [$rownum] ["macAddress"] = strval ( $value->col [4] );
					$deviceInfo [$rownum] ["status"] = strval ( $value->col [5] );
					$deviceInfo [$rownum] ["version"] = strval ( $value->col [6] );
					$rownum ++;
				}
			}
			$returnResponse ["Success"] = $deviceInfo;
		}
		return $returnResponse;
	}
	function searchDeviceGroupLevel($providerId, $groupId, $searchValue, $searchCriteria) {
		global $sessionid, $client;
		$returnResponse ["Error"] = "";
		$returnResponse ["Success"] = "";
		$deviceList = array ();
		
		$xmlinput = xmlHeader ( $sessionid, "GroupAccessDeviceGetListRequest" );
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars ( $providerId ) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars ( $groupId ) . "</groupId>";
		if ($searchCriteria == self::DEVICE_NAME) {
			$xmlinput .= "<searchCriteriaDeviceName>";
			$xmlinput .= "<mode>Contains</mode>";
			$xmlinput .= "<value>" . $searchValue . "</value>";
			$xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
			$xmlinput .= "</searchCriteriaDeviceName>";
		} else if ($searchCriteria == self::DEVICE_TYPE) {
			$xmlinput .= "<searchCriteriaExactDeviceType>";
			$xmlinput .= "<deviceType>" . $searchValue . "</deviceType>";
			$xmlinput .= "</searchCriteriaExactDeviceType>";
		} else if ($searchCriteria == self::MAC) {
			$xmlinput .= "<searchCriteriaDeviceMACAddress>";
			$xmlinput .= "<mode>Contains</mode>";
			$xmlinput .= "<value>" . $searchValue . "</value>";
			$xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
			$xmlinput .= "</searchCriteriaDeviceMACAddress>";
		} else if ($searchCriteria == self::IPADD) {
			$xmlinput .= "<searchCriteriaDeviceNetAddress>";
			$xmlinput .= "<mode>Contains</mode>";
			$xmlinput .= "<value>" . $searchValue . "</value>";
			$xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
			$xmlinput .= "</searchCriteriaDeviceNetAddress>";
		} else if ($searchCriteria == self::ALL) {
			// Search All Device Name
		} else {
			$returnResponse ["Error"] = "Invalid Search Criteria";
			return $returnResponse;
		}
		$xmlinput .= xmlFooter ();
		$response = $client->processOCIMessage ( array (
				"in0" => $xmlinput 
		) );
		$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
		$errMsg = readErrorXmlGenuine ( $xml );
		if ($errMsg != "") {
			$returnResponse ["Error"] = $errMsg;
		} else {
			$rownum = 0;
			if (isset ( $xml->command->accessDeviceTable->row )) {
				foreach ( $xml->command->accessDeviceTable->row as $key => $value ) {
					$deviceList [$rownum] ["providerId"] = $providerId;
					$deviceList [$rownum] ["groupId"] = $groupId;
					$deviceList [$rownum] ["deviceName"] = strval ( $value->col [0] );
					$deviceList [$rownum] ["deviceType"] = strval ( $value->col [1] );
					$deviceList [$rownum] ["availablePorts"] = strval ( $value->col [2] );
					$deviceList [$rownum] ["netAddress"] = strval ( $value->col [3] );
					$deviceList [$rownum] ["macAddress"] = strval ( $value->col [4] );
					$deviceList [$rownum] ["status"] = strval ( $value->col [5] );
					$deviceList [$rownum] ["version"] = strval ( $value->col [6] );
					$deviceList [$rownum] ["All"] = strval ( $value->col [0] ) .",". strval ( $value->col [1] ) .",". 
					                                strval ( $value->col [3] ) .",". strval ( $value->col [4] );
					$rownum ++;
				}
			}
			
			$returnResponse ["Success"] = $deviceList;
		}
		return $returnResponse;
	}
	function searchDevicesSystemLevel($searchValue, $searchCriteria) {
		global $sessionid, $client;
		$returnResponse ["Error"] = "";
		$returnResponse ["Success"] = "";
		$deviceList = array ();
		$xmlinput = xmlHeader ( $sessionid, "SystemAccessDeviceGetAllRequest" );
		if ($searchCriteria == "DeviceName") {
			$xmlinput .= "<searchCriteriaDeviceName>";
			$xmlinput .= "<mode>Contains</mode>";
			$xmlinput .= "<value>" . $searchValue . "</value>";
			$xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
			$xmlinput .= "</searchCriteriaDeviceName>";
		} else if ($searchCriteria == "DeviceType") {
			$xmlinput .= "<searchCriteriaExactDeviceType>";
			$xmlinput .= "<deviceType>" . $searchValue . "</deviceType>";
			$xmlinput .= "</searchCriteriaExactDeviceType>";
		} else if ($searchCriteria == "MacAddress") {
			$xmlinput .= "<searchCriteriaDeviceMACAddress>";
			$xmlinput .= "<mode>Contains</mode>";
			$xmlinput .= "<value>" . $searchValue . "</value>";
			$xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
			$xmlinput .= "</searchCriteriaDeviceMACAddress>";
		} else if ($searchCriteria == "IpAddress") {
			$xmlinput .= "<searchCriteriaDeviceNetAddress>";
			$xmlinput .= "<mode>Contains</mode>";
			$xmlinput .= "<value>" . $searchValue . "</value>";
			$xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
			$xmlinput .= "</searchCriteriaDeviceNetAddress>";
		}else if($searchCriteria == self::ALL){
		    // Search All Device Name
		}else {
			$returnResponse ["Error"] = "Invalid SeaRCH CRITERIA";
			return $returnResponse;
		}
		
		$xmlinput .= xmlFooter ();
		$response = $client->processOCIMessage ( array (
				"in0" => $xmlinput 
		) );
		$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
		$errMsg = readErrorXmlGenuine ( $xml );
		if ($errMsg != "") {
			$returnResponse ["Error"] = $errMsg;
		} else {
			$rownum = 0;
			if (isset ( $xml->command->accessDeviceTable->row )) {
				foreach ( $xml->command->accessDeviceTable->row as $key => $value ) {
					$deviceList [$rownum] ["providerId"] = strval ( $value->col [0] );
					$deviceList [$rownum] ["groupId"] = strval ( $value->col [2] );
					$deviceList [$rownum] ["deviceName"] = strval ( $value->col [3] );
					$deviceList [$rownum] ["deviceType"] = strval ( $value->col [4] );
					$deviceList [$rownum] ["netAddress"] = strval ( $value->col [5] );
					$deviceList [$rownum] ["macAddress"] = strval ( $value->col [6] );
					$deviceList [$rownum] ["status"] = strval ( $value->col [7] );
					$deviceList [$rownum] ["All"] = strval ( $value->col [3] ) . "," . strval ( $value->col [4] ) . "," . 
									                strval ( $value->col [5] ) . "," .strval ( $value->col [6] );
					$rownum ++;
				}
			}
			$returnResponse ["Success"] = $deviceList;
		}
		return $returnResponse;
	}
	function getAllUsersforDevice($serviceProviderId, $groupId, $deviceName) {
		global $sessionid, $client;
		$allUserOfDevice = array ();
		$returnResponse ["Error"] = "";
		$returnResponse ["Success"] = "";
		
		$xmlinput = xmlHeader ( $sessionid, "GroupAccessDeviceGetUserListRequest" );
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProviderId) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
		$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
		$xmlinput .= xmlFooter ();
		$response = $client->processOCIMessage ( array (
				"in0" => $xmlinput 
		) );
		
		$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );		
                //print_r($xml);
		$errMsg = readErrorXmlGenuine ( $xml );
		if ($errMsg != "") {
			$returnResponse ["Error"] = $errMsg;
		} else {
			if (isset ( $xml->command->deviceUserTable->row )) {
				$c = 0;
				foreach ( $xml->command->deviceUserTable->row as $kk => $vv ) {
					$userInfo ['linePort'] = strval ( $vv->col [0] );
					$userInfo ['lastName'] = strval ( $vv->col [1] );
					$userInfo ['firstName'] = strval ( $vv->col [2] );
					$userInfo ['phoneNumber'] = strval ( $vv->col [3] );
					$userInfo ['userId'] = strval ( $vv->col [4] );
					$userInfo ['userType'] = strval ( $vv->col [5] );
					$userInfo ['endpointType'] = strval ( $vv->col [6] );
					$userInfo ['extn'] = strval ( $vv->col [9] );
                                        //Code added @ 24 July 2018
                                        if(trim(strval($vv->col[3]))=="" && trim(strval($vv->col[9]))=="")
                                        {
                                            $tmpStr = strval($vv->col[0]);
                                            $tmpArr = explode($deviceName, $tmpStr);
                                            $tmpNewArr = explode(".", $tmpArr[1]);
                                            $userInfo ['extn'] = $tmpNewArr[1];
                                        }
                                        //End code                                        
					$allUserOfDevice [$c] = $userInfo;
					$c ++;
				}
			}
			$returnResponse ["Success"] = $allUserOfDevice;
		}
		
		return $returnResponse;
	}
	function getAllDeviceTypeListOfSystem() {
		global $sessionid, $client;
		$dataResponse = array ();
		$deviceData = array ();
		$returnResponse ["Error"] = "";
		$returnResponse ["Success"] = "";
		
		$xmlinput = xmlHeader ( $sessionid, "SystemDeviceTypeGetAvailableListRequest19" );
		$xmlinput .= "<allowConference>true</allowConference>";
		$xmlinput .= "<allowMusicOnHold>true</allowMusicOnHold>";
		$xmlinput .= "<onlyConference>false</onlyConference>";
		$xmlinput .= "<onlyVideoCapable>false</onlyVideoCapable>";
		$xmlinput .= "<onlyOptionalIpAddress>false</onlyOptionalIpAddress>";
		$xmlinput .= xmlFooter ();
		$response = $client->processOCIMessage ( array (
				"in0" => $xmlinput 
		) );
		$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
		$errMsg = readErrorXmlGenuine ( $xml );
		if ($errMsg != "") {
			$returnResponse ["Error"] = $errMsg;
		} else {
			if (isset ( $xml->command->deviceType )) {
				$count = 0;
				foreach ( $xml->command->deviceType as $deviceKey => $deviceTypeName ) {
					$deviceData [$count] = strval ( $deviceTypeName );
					$count ++;
				}
			}
			$returnResponse ["Success"] = $deviceData;
		}
		return $returnResponse;
	}
	public function deviceAddRequest($postArray) {
		global $sessionid, $client;
		$deviceAddResponse ["Error"] = array();
		$deviceAddResponse ["Success"] = "";
		if (! empty ( $postArray )) {
			$xmlinput = xmlHeader ( $sessionid, "GroupAccessDeviceAddRequest14" );
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($postArray ['sp']) . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . htmlspecialchars($postArray ['groupId']) . "</groupId>";
			$xmlinput .= "<deviceName>" . $postArray ['deviceName'] . "</deviceName>";
			$xmlinput .= "<deviceType>" . $postArray ['deviceType'] . "</deviceType>";
			
			if (! empty ( $postArray ['protocol'] )) {
				$xmlinput .= "<protocol>" . $postArray ['protocol'] . "</protocol>";
			}
			
			if (! empty ( $postArray ['netAddress'] )) {
				$xmlinput .= "<netAddress>" . $postArray ['netAddress'] . "</netAddress>";
			}
			
			if (! empty ( $postArray ['port'] )) {
				$xmlinput .= "<port>" . $postArray ['port'] . "</port>";
			}
			
			if (! empty ( $postArray ['outboundProxyServerNetAddress'] )) {
				$xmlinput .= "<outboundProxyServerNetAddress>" . $postArray ['outboundProxyServerNetAddress'] . "</outboundProxyServerNetAddress>";
			}
			
			if (! empty ( $postArray ['stunServerNetAddress'] )) {
				$xmlinput .= "<stunServerNetAddress>" . $postArray ['stunServerNetAddress'] . "</stunServerNetAddress>";
			}
			
			if (! empty ( $postArray ['macAddress'] )) {
				$xmlinput .= "<macAddress>" . $postArray ['macAddress'] . "</macAddress>";
			}
			
			if (! empty ( $postArray ['serialNumber'] )) {
				$xmlinput .= "<serialNumber>" . $postArray ['serialNumber'] . "</serialNumber>";
			}
			
			if (! empty ( $postArray ['description'] )) {
				$xmlinput .= "<description>" . $postArray ['description'] . "</description>";
			}
			
			if (! empty ( $postArray ['physicalLocation'] )) {
				$xmlinput .= "<physicalLocation>" . $postArray ['physicalLocation'] . "</physicalLocation>";
			}
			
			if (! empty ( $postArray ['transportProtocol'] )) {
				$xmlinput .= "<transportProtocol>" . $postArray ['transportProtocol'] . "</transportProtocol>";
			}
			
			if(!empty($postArray['deviceAccessUserName'])){
				$deviceAccessPassword = $_SESSION["groupId"];
				$xmlinput .= "<useCustomUserNamePassword>true</useCustomUserNamePassword>";
				$xmlinput .= "<accessDeviceCredentials>
									<userName>" . $postArray['deviceAccessUserName'] . "</userName>
									<password>" . $postArray['deviceAccessPassword'] . "</password>
								</accessDeviceCredentials>";
			}
			
			$xmlinput .= xmlFooter ();
			$response = $client->processOCIMessage ( array (
					"in0" => $xmlinput 
			) );
			$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
			if (readErrorXmlGenuine ( $xml ) != "") {
				if (isset ( $xml->command->detail )) {
					$deviceAddResponse ["Error"] = strval ( $xml->command->detail );
				} else {
					$deviceAddResponse ["Error"] = strval ( $xml->command->summaryEnglish );
				}
			} else {
				$deviceAddResponse ["Success"] = $postArray ['deviceName'];
			}
		}
		return $deviceAddResponse;
	}
	function availableUsers($users) {
		$userList = array ();
		if (empty ( $users ["Error"] )) {
			if (is_array ( $users ["Success"] ) && ! empty ( $users ["Success"] )) {
				$users = $users ["Success"];
				for($a = 0; $a < count ( $users ); $a ++) {
					$match = false;
					if (isset ( $_SESSION ["userInfo"] ["sharedCallAppearanceUsers"] )) {
						for($b = 0; $b < count ( $_SESSION ["userInfo"] ["sharedCallAppearanceUsers"] ); $b ++) {
							if ($users [$a] ["userId"] == $_SESSION ["userInfo"] ["sharedCallAppearanceUsers"] [$b] ["id"]) {
								$match = true;
							}
						}
					}
					
					if ($match == false) {
						$userList = $users;
					}
				}
			}
		} else {
			// error msg
		}
		return $userList;
	}
	function scaUsers($users) {
		if (isset ( $_SESSION ["userInfo"] ["sharedCallAppearanceUsers"] )) {
			for($a = 0; $a < count ( $_SESSION ["userInfo"] ["sharedCallAppearanceUsers"] ); $a ++) {
				if ($_SESSION ["userInfo"] ["sharedCallAppearanceUsers"] [$a] ["endpointType"] == "Shared Call Appearance") {
					$id = $_SESSION ["userInfo"] ["sharedCallAppearanceUsers"] [$a] ["id"] . ":" . $_SESSION ["userInfo"] ["sharedCallAppearanceUsers"] [$a] ["name"] . ":" . $_SESSION ["userInfo"] ["sharedCallAppearanceUsers"] [$a] ["phoneNumber"];
					echo "<li class=\"ui-state-highlight\" id=\"" . $id . "\">" . $_SESSION ["userInfo"] ["sharedCallAppearanceUsers"] [$a] ["name"] . "</li>";
				}
			}
		}
	}
	
	// get device detail
	public function getDeviceDetailRequest($postArray) {
		global $sessionid, $client;
		$returnResponse = array ();
		$returnResponse ["Error"] = "";
		$returnResponse ["Success"] = "";
		$deviceInfo = array ();
		
		$xmlinput = xmlHeader ( $sessionid, "GroupAccessDeviceGetRequest18sp1" );
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars ( $postArray ["sp"] ) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars ( $postArray ["groupId"] ) . "</groupId>";
		$xmlinput .= "<deviceName>" . htmlspecialchars ( $postArray ["deviceName"] ) . "</deviceName>";
		
		$xmlinput .= xmlFooter ();
		$response = $client->processOCIMessage ( array (
				"in0" => $xmlinput 
		) );
		$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
		$errMsg = readErrorXmlGenuine ( $xml );
		if ($errMsg != "") {
			$returnResponse ["Error"] = $errMsg;
		} else {
			$deviceInfo ["deviceName"] = strval ( $xml->command->deviceName );
			$deviceInfo ["deviceType"] = strval ( $xml->command->deviceType );
			$deviceInfo ["protocol"] = strval ( $xml->command->protocol );
			$deviceInfo ["netAddress"] = strval ( $xml->command->netAddress );
			$deviceInfo ["port"] = strval ( $xml->command->port );
			$deviceInfo ["macAddress"] = strval ( $xml->command->macAddress );
			$deviceInfo ["serialNumber"] = strval ( $xml->command->serialNumber );
			$deviceInfo ["numberOfPorts"] = strval ( $xml->command->numberOfPorts->quantity );
			$deviceInfo ["numberOfAssignedPorts"] = strval ( $xml->command->numberOfAssignedPorts );
			$deviceInfo ["status"] = strval ( $xml->command->status );
			$deviceInfo ["transportProtocol"] = strval ( $xml->command->transportProtocol );
			$deviceInfo ["useCustomUserNamePassword"] = strval ( $xml->command->useCustomUserNamePassword );
			$deviceInfo ["description"] = strval ( $xml->command->description );
			$deviceInfo ["outboundProxyServerNetAddress"] = strval ( $xml->command->outboundProxyServerNetAddress );
			$deviceInfo ["stunServerNetAddress"] = strval ( $xml->command->stunServerNetAddress );
			$deviceInfo ["physicalLocation"] = strval ( $xml->command->physicalLocation );
			$returnResponse ["Success"] = $deviceInfo;
		}
		return $returnResponse;
	}
	// function to modify the device name
	public function deviceModifyRequest($postArray) {
		global $sessionid, $client;
		
		$deviceModifyResponse ["Error"] = array();
		$deviceModifyResponse ["Success"] = "";
		
		if (! empty ( $postArray )) {
			$xmlinput = xmlHeader ( $sessionid, "GroupAccessDeviceModifyRequest14" );
			$xmlinput .= "<serviceProviderId>" . htmlspecialchars($postArray ['sp']) . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . htmlspecialchars($postArray ['groupId']) . "</groupId>";
			$xmlinput .= "<deviceName>" . $postArray ['modifiedDevice'] . "</deviceName>";
			// $xmlinput .= "<deviceType>" . $postArray['deviceType']. "</deviceType>";
			
			if (! empty ( $postArray ['protocol'] )) {
				$xmlinput .= "<protocol>" . $postArray ['protocol'] . "</protocol>";
			}
			
			if (! empty ( $postArray ['netAddress'] )) {
				$xmlinput .= "<netAddress>" . $postArray ['netAddress'] . "</netAddress>";
			}
			
			if (! empty ( $postArray ['port'] )) {
				$xmlinput .= "<port>" . $postArray ['port'] . "</port>";
			}
			
			if (! empty ( $postArray ['outboundProxyServerNetAddress'] )) {
				$xmlinput .= "<outboundProxyServerNetAddress>" . $postArray ['outboundProxyServerNetAddress'] . "</outboundProxyServerNetAddress>";
			}
			
			if (! empty ( $postArray ['stunServerNetAddress'] )) {
				$xmlinput .= "<stunServerNetAddress>" . $postArray ['stunServerNetAddress'] . "</stunServerNetAddress>";
			}
			
			if (! empty ( $postArray ['macAddress'] )) {
				$xmlinput .= "<macAddress>" . $postArray ['macAddress'] . "</macAddress>";
			}else{
			    $xmlinput .= "<macAddress xsi:nil='true'>" . $postArray ['macAddress'] . "</macAddress>";
			}
			
			if (! empty ( $postArray ['serialNumber'] )) {
				$xmlinput .= "<serialNumber>" . $postArray ['serialNumber'] . "</serialNumber>";
			}
			
			if (! empty ( $postArray ['description'] )) {
				$xmlinput .= "<description>" . $postArray ['description'] . "</description>";
			}
			
			if (! empty ( $postArray ['physicalLocation'] )) {
				$xmlinput .= "<physicalLocation>" . $postArray ['physicalLocation'] . "</physicalLocation>";
			}
			
			if (! empty ( $postArray ['transportProtocol'] )) {
				$xmlinput .= "<transportProtocol>" . $postArray ['transportProtocol'] . "</transportProtocol>";
			}
			
			if(!empty($postArray['deviceAccessUserName'])){
			    //$deviceAccessPassword = $_SESSION["groupId"];
			    $xmlinput .= "<useCustomUserNamePassword>true</useCustomUserNamePassword>";
			    $xmlinput .= "<accessDeviceCredentials>
									<userName>" . $postArray['deviceAccessUserName'] . "</userName>
									<password>" . $postArray['deviceAccessPassword'] . "</password>
								</accessDeviceCredentials>";
			}
			
			$xmlinput .= xmlFooter ();
			$response = $client->processOCIMessage ( array (
					"in0" => $xmlinput 
			) );
			$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
			
			
			if (readErrorXmlGenuine ( $xml ) != "") {
				if (isset ( $xml->command->detail )) {
					$deviceModifyResponse ["Error"] = strval ( $xml->command->detail );
				} else {
					$deviceModifyResponse ["Error"] = strval ( $xml->command->summaryEnglish );
				}
			} else {
				$deviceModifyResponse ["Success"] = $xml->command;
			}
		}
		return $deviceModifyResponse;
	}
	public function getDeleteDevice($scaDeviceId, $providerId, $groupId) {
		global $sessionid, $client;
		$deleteSca ["Error"] = "";
		$deleteSca ["Success"] = "";
		
		$xmlinput = xmlHeader ( $sessionid, "GroupAccessDeviceDeleteRequest" );
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($providerId) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
		$xmlinput .= "<deviceName>" . $scaDeviceId . "</deviceName>";
		$xmlinput .= xmlFooter ();
		$response = $client->processOCIMessage ( array (
				"in0" => $xmlinput 
		) );
		$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
		if (readErrorXmlGenuine ( $xml ) != "") {
			$deleteSca ["Error"] = $xml->command->summaryEnglish;
		} else {
			$deleteSca ["Success"] = $xml->command;
		}
		
		return $deleteSca;
	}
	public function getCustomProfiles($deviceType) {
		global $db;
		$profiles = "";
		if ($deviceType != "") {
			$query = "SELECT `customProfileName` FROM `customProfiles` WHERE `deviceType` = '" . $deviceType . "' ORDER BY `customProfileName`";
			$results = $db->query ( $query );
			$profiles = array ();
			$i = 0;
			/*while ( $row = $results->fetch () ) {
				if (! in_array ( $profile = $row ['customProfileName'], $profiles )) {
					$profiles [$i ++] = $profile;
				}
			}*/
			
			if($results->fetch() != ""){
			    $profiles = $this->getAllPhoneProfiles();
			}
		}
		
		return $profiles;
	}
	
	public function getAllTagBundlesData($deviceType) {
	    global $db;
	    $bundles = "";
	    if ($deviceType != "") {
	        $query = "SELECT `customProfileName` FROM `customProfiles` WHERE `deviceType` = '" . $deviceType . "' ORDER BY `customProfileName`";
	        $results = $db->query ( $query );
	        $bundles = array ();
	        $i = 0;
	        /*while ( $row = $results->fetch () ) {
	         if (! in_array ( $profile = $row ['customProfileName'], $profiles )) {
	         $profiles [$i ++] = $profile;
	         }
	         }*/
	        
	        if($results->fetch() != ""){
	            $bundles = $this->getAllTagBundles();
	        }
	    }
	    
	    return $bundles;
	}
	
	function getAllPhoneProfiles(){
	    global $db;
	    
	    $phoneProfileArray = array();
	    $stmt = $db->prepare("SELECT * FROM phoneProfilesLookup");
	    $stmt->execute();
	    $row = $stmt->fetchAll(PDO::FETCH_OBJ);
	    $i =0;
	    foreach($row as $key => $val) {
	        $phoneProfileArray[$i][] = $val->phoneProfile;
	        $phoneProfileArray[$i][] = $val->value;
	        $i++;
	    }
	    
	    return $phoneProfileArray;
	}
	
	
	function getAllTagBundles() {
	    global $db;
	    $tagBundleArray = array();
	    
	    $stmt = $db->prepare("SELECT `tagBundle` FROM deviceMgmtTagBundles");
	    $stmt->execute();
	    $row = $stmt->fetchAll(PDO::FETCH_OBJ);
	    $i =0;
	    foreach($row as $key => $val) {
	        if(!in_array($val->tagBundle, $tagBundleArray)){
	            $tagBundleArray[$i] = $val->tagBundle;
	            
	            $i++;
	        }
	        
	    }
	    return $tagBundleArray;
	}
	
	public function filterUsers($scaUsers){
		
		$usersArray["assignUser"] = array(); 
		$usersArray["unAssignUser"] = array(); 
		
		$sessionUserList = $_SESSION["sca"]["deviceInfo"]["scaUsers"];
		$sessionUsers = explode(";", $sessionUserList);
		
		$explodePostUsers = explode(";", $scaUsers);
		
		foreach($explodePostUsers as $key=>$val){
			if(!in_array($val, $sessionUsers)){
				$usersArray['assignUser'][] = $val;
			}
		}
		
		foreach($sessionUsers as $key=>$val){
			if(!in_array($val, $explodePostUsers)){
				$usersArray['unAssignUser'][] = $val;
			}
		}
		return $usersArray;
	}
        
        //code added @ 23 July 2018
        public function newFilterUsers($scaUsers){
		
		$usersArray["assignUser"] = array(); 
		$usersArray["unAssignUser"] = array(); 
		
		$sessionUserList = $_SESSION["sca"]["deviceInfo"]["scaUsersDup"];
		$sessionUsers = explode(";", $sessionUserList);
		
		$explodePostUsers = explode(";", $scaUsers);
		
		foreach($explodePostUsers as $key=>$val){
			if(!in_array($val, $sessionUsers)){
				$usersArray['assignUser'][] = $val;
			}
		}
		
		foreach($sessionUsers as $key=>$val){
			if(!in_array($val, $explodePostUsers)){
				$usersArray['unAssignUser'][] = $val;
			}
		}
		return $usersArray;
	}
        //End code
	
	function GroupAccessDeviceExistanceCheck($providerId, $groupId, $deviceName) {
	    global $sessionid, $client;
	    $returnResponse = array ();
	    $returnResponse ["Error"] = "";
	    $returnResponse ["Success"] = "";
	    $deviceInfo = array ();
	    
	    $xmlinput = xmlHeader ( $sessionid, "GroupAccessDeviceGetListRequest" );
	    $xmlinput .= "<serviceProviderId>" . htmlspecialchars ( $providerId ) . "</serviceProviderId>";
	    $xmlinput .= "<groupId>" . htmlspecialchars ( $groupId ) . "</groupId>";
	    $xmlinput .= "<searchCriteriaDeviceName>";
	    $xmlinput .= "<mode>Equal To</mode>";
	    $xmlinput .= "<value>".$deviceName."</value>";
	    $xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
	    
	    $xmlinput .= "</searchCriteriaDeviceName>";
	    $xmlinput .= xmlFooter ();
	    $response = $client->processOCIMessage ( array (
	        "in0" => $xmlinput
	    ) );
	    $xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
	    //print_r($xml);
	    $errMsg = readErrorXmlGenuine ( $xml );
	    if ($errMsg != "") {
	        $returnResponse ["Error"] = $errMsg;
	    } else {
	        $rownum = 0;
	        if (isset ( $xml->command->accessDeviceTable->row )) {
	            foreach ( $xml->command->accessDeviceTable->row as $key => $value ) {
	                $deviceInfo [$rownum] ["deviceName"] = strval ( $value->col [0] );
	                $rownum ++;
	            }
	        }
	        $returnResponse ["Success"] = $deviceInfo;
	    }
	    return $returnResponse;
	}
	
	function getDeviceFileUrl($deviceName){
	    global  $sessionid, $client;
	    $ocpResponse["Error"] = array();
	    $ocpResponse["Success"] = array();
	    
	    $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceFileGetListRequest14sp8");
	    $xmlinput .= "<serviceProviderId>".$_SESSION['sp']."</serviceProviderId>";
	    $xmlinput .= "<groupId>".$_SESSION['groupId']."</groupId>";
	    $xmlinput .= "<deviceName>".$deviceName."</deviceName>";
	    
	    
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    
	    $errMsg = readErrorXmlGenuine ( $xml );
	    if ($errMsg != "") {
	        $ocpResponse ["Error"] = $errMsg;
	    } else {
	        $ocpResponse ["Success"]= strval($xml->command->deviceFilesTable->row->col[2]);
	    }
	    
	    return $ocpResponse;
	}
	
	
	function getDeviceTypesProfileDetail($deviceType){
	    global  $sessionid, $client;
	    $deviceTypeDetails["Error"] = "";
	    $deviceTypeDetails["Success"] = "";
	    if($this->ociVersion == "20"){
                $xmlinput = xmlHeader($sessionid, "SystemSIPDeviceTypeGetRequest20");
            }else if($this->ociVersion == "21"){
                $xmlinput = xmlHeader($sessionid, "SystemSIPDeviceTypeGetRequest21sp1V2");
            }else{
                $xmlinput = xmlHeader($sessionid, "SystemSIPDeviceTypeGetRequest22");
         }
	        $xmlinput .= "<deviceType>".htmlspecialchars($deviceType)."</deviceType>";
	        $xmlinput .= xmlFooter();
	        $response = $client->processOCIMessage(array("in0" => $xmlinput));
	        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
// 	        echo $deviceType; print_r($xml); exit;
	        if (readErrorXmlGenuine ( $xml ) != "") {
	            $deviceTypeDetails["Error"] = strval ( $xml->command->summaryEnglish );
	        }else{
	            $deviceTypeDetails["Success"]["isObsolete"] = strval($xml->command->isObsolete);
	            if(isset($xml->command->numberOfPorts->unlimited) && $xml->command->numberOfPorts->unlimited == "true"){
	                $deviceTypeDetails["Success"]["numberOfPorts"] = 16;// if value is unlimited then it will set to 16
	            }else{
	                $deviceTypeDetails["Success"]["numberOfPorts"] = strval($xml->command->numberOfPorts->quantity);
	            }
	            $deviceTypeDetails["Success"]["profile"] = strval($xml->command->profile);
	            $deviceTypeDetails["Success"]["staticLineOrdering"] = strval($xml->command->staticLineOrdering);
	        }
	    //print_r($deviceTypeDetails);
	    return $deviceTypeDetails;
	}
	
	public function systemAccessDeviceGetAllRequest_for_QuickFilter($appliedFilter) {
	    global $sessionid, $client;
	    $returnResponse ["Error"] = "";
	    $returnResponse ["Success"] = "";
	    $deviceList = array ();
	    $xmlinput = xmlHeader ( $sessionid, "SystemAccessDeviceGetAllRequest" );
	    $xmlinput .= $this->createInputTags($appliedFilter);
	    $xmlinput .= xmlFooter ();
	    $response = $client->processOCIMessage ( array (
	        "in0" => $xmlinput
	    ) );
	    $xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
	    $errMsg = readErrorXmlGenuine ( $xml );
	    if ($errMsg != "") {
	        $errMsg = str_replace("%0D%0A", "", trim($errMsg));
	        $returnResponse ["Error"] = $errMsg;
	    } else {
	        $rownum = 0;
	        if (isset ( $xml->command->accessDeviceTable->row )) {
	            foreach ( $xml->command->accessDeviceTable->row as $key => $value ) {
	                $deviceList [$rownum] ["providerId"] = strval ( $value->col [0] );
	                $deviceList [$rownum] ["isProvider"] = strval ( $value->col [1] );
	                $deviceList [$rownum] ["groupId"] = strval ( $value->col [2] );
	                $deviceList [$rownum] ["deviceName"] = strval ( $value->col [3] );
	                $deviceList [$rownum] ["deviceType"] = strval ( $value->col [4] );
	                $deviceList [$rownum] ["netAddress"] = strval ( $value->col [5] );
	                $deviceList [$rownum] ["macAddress"] = strval ( $value->col [6] );
	                $deviceList [$rownum] ["status"] = strval ( $value->col [7] );
	                $deviceList [$rownum] ["All"] = strval ( $value->col [3] ) . "," . strval ( $value->col [4] ) . "," .
	   	                strval ( $value->col [5] ) . "," .strval ( $value->col [6] );
	   	                $rownum ++;
	            }
	        }
	        $returnResponse ["Success"] = $deviceList;
	    }
	    return $returnResponse;
	    
	}
	
	public function createInputTags($appliedFilter) {
	    $xmlinput = "";
	    
	    if( $appliedFilter->responseSizeLimit != "" ) {
	        $xmlinput .= "<responseSizeLimit>";
	        $xmlinput .= $appliedFilter->responseSizeLimit;
	        $xmlinput .= "</responseSizeLimit>";
	    }
	
	        if( isset($appliedFilter->allFilters["deviceName"]) ) {
	            foreach ( $appliedFilter->allFilters["deviceName"] as $itemKey => $item ) {
	                $xmlinput .= "<searchCriteriaDeviceName>";
	                $xmlinput .= "<mode>{$item['filterItemMode']}</mode>";
	                $xmlinput .= "<value>{$item['filterItemValue']}</value>";
	                $xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
	                $xmlinput .= "</searchCriteriaDeviceName>";
	            }
	        }
	        
	        if( isset($appliedFilter->allFilters["macAddress"]) ) {
	            foreach ( $appliedFilter->allFilters["macAddress"] as $itemKey => $item ) {
	                $xmlinput .= "<searchCriteriaDeviceMACAddress>";
	                $xmlinput .= "<mode>{$item['filterItemMode']}</mode>";
	                $xmlinput .= "<value>{$item['filterItemValue']}</value>";
	                $xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
	                $xmlinput .= "</searchCriteriaDeviceMACAddress>";
	            }
	        }
	        if( isset($appliedFilter->allFilters["ipAddress"]) ) {
	            foreach ( $appliedFilter->allFilters["ipAddress"] as $itemKey => $item ) {
	                $xmlinput .= "<searchCriteriaDeviceNetAddress>";
	                $xmlinput .= "<mode>{$item['filterItemMode']}</mode>";
	                $xmlinput .= "<value>{$item['filterItemValue']}</value>";
	                $xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
	                $xmlinput .= "</searchCriteriaDeviceNetAddress>";
	            }
	        }
	        if( $appliedFilter->allFilters["groupId"] ) {
	            foreach ( $appliedFilter->allFilters["groupId"] as $itemKey => $item ) {
	                $xmlinput .= "<searchCriteriaGroupId>";
	                $xmlinput .= "<mode>{$item['filterItemMode']}</mode>";
	                $xmlinput .= "<value>{$item['filterItemValue']}</value>";
	                $xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
	                $xmlinput .= "</searchCriteriaGroupId>";
	            }
	        }
	        
	    
	    if( $appliedFilter->selectedDeviceType != "" ) {
	            $xmlinput .= "<searchCriteriaExactDeviceType>";
	            $xmlinput .= "<deviceType>{$appliedFilter->selectedDeviceType}</deviceType>";
	            $xmlinput .= "</searchCriteriaExactDeviceType>";
	    }
	    
	    if( $appliedFilter->restrictToEnterprise != "" ) {
	        $xmlinput .= "<searchCriteriaExactDeviceServiceProvider>";
	        $xmlinput .= "<serviceProviderId>{$appliedFilter->restrictToEnterprise}</serviceProviderId>";
	        $xmlinput .= "</searchCriteriaExactDeviceServiceProvider>";
	    }
	    
	    return $xmlinput;
	}
	
	public function systemAccessDeviceTypeGetListRequest() {
	    global $sessionid, $client;
	    $returnResponse ["Error"] = "";
	    $returnResponse ["Success"] = "";
	    $deviceList = array ();
	    $xmlinput = xmlHeader ( $sessionid, "SystemAccessDeviceTypeGetListRequest" );
	    $xmlinput .= xmlFooter ();
	    $response = $client->processOCIMessage ( array (
	        "in0" => $xmlinput
	    ) );
	    $xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
	    $errMsg = readErrorXmlGenuine ( $xml );
	    if ($errMsg != "") {
	        $returnResponse ["Error"] = $errMsg;
	    } else {
	        $returnResponse["Success"] = $xml->command->deviceType;;
	        
	    }
	    return $returnResponse;
	}
	
	public function getEnterpriseDeviceCount($enterpriseId) {
	    global $sessionid, $client;
	    $userCount = 0;
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderAccessDeviceGetListRequest");
	    $xmlinput .= "<serviceProviderId>{$enterpriseId}</serviceProviderId>";
	    $xmlinput .= "<responseSizeLimit>1</responseSizeLimit>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    $errMsg = readErrorXmlGenuine($xml);
	    if ($errMsg != "") {
	        $errorMsg = str_replace("%0D%0A", "", trim($errMsg));
	        preg_match('/Please narrow your search, there are ([0-9]+) matching records/', $errorMsg, $output_array);
	        if ($output_array && isset($output_array[1])) {
	            $userCount = $output_array[1];
	        }
	    } else {
	        $returndata = array();
	        foreach ($xml->command->accessDeviceTable->row as $key => $value) {
	            $userCount++;
	        }
	    }
	    return $userCount;
	}
	
	public function getSystemDeviceCount() {
	    global $sessionid, $client;
	    $userCount = 0;
	    $xmlinput = xmlHeader($sessionid, "SystemAccessDeviceGetAllRequest");
	    $xmlinput .= "<responseSizeLimit>1</responseSizeLimit>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    $errMsg = readErrorXmlGenuine($xml);
	    if ($errMsg != "") {
	        $errorMsg = str_replace("%0D%0A", "", trim($errMsg));
	        preg_match('/Please narrow your search, there are ([0-9]+) matching records/', $errorMsg, $output_array);
	        if ($output_array && isset($output_array[1])) {
	            $userCount = $output_array[1];
	        }
	    } else {
	        $returndata = array();
	        foreach ($xml->command->accessDeviceTable->row as $key => $value) {
	            $userCount++;
	        }
	    }
	    return $userCount;
	}
	
}

?>
