<?php 
class PreventPasswordOperation{  
    
    public  $usedPastPassArray = array();
    public  $passwordReusePrevention = "";


    public function __construct($userName, $encPassword) {  
            global $db;
            $this->db = $db;
            $this->userName = $userName;
            $this->password = $encPassword;
        }
    //This function needs to call
    public function passwordReuseValidationBasedOnPreventPassConfig(){
        $getPreventPsswordConfigVal = $this->getPreventPsswordConfigVal();
        $passwordList = $this->getLastUsedPassword();
        if( in_array($this->password, $passwordList)) {
            return true; 
        } else {
            return false;
        }
    }
    
    private function getLastUsedPassword(){ 
        
        $passwRsePrvtn = $this->passwordReusePrevention;
        $stmt = $this->db->prepare("select admnPshsry.password, usr.id, usr.userName  from users as usr inner join adminPasswordHistory as admnPshsry on usr.id=admnPshsry.userId  where userName=? ORDER BY admnPshsry.modifiedOn desc limit 0, $passwRsePrvtn");
	if ($stmt->execute(array($this->userName))) 
        {
                while($rs = $stmt->fetch(PDO::FETCH_OBJ))
                {  
                    $this->usedPastPassArray['password'][] = $rs->password;
                }
		//return $rows;
	}        
	return $this->usedPastPassArray['password'];
    }
    
    private function getPreventPsswordConfigVal() {
        
        $qryPassReuseCnfgVal = "select passwordReusePrevention from systemConfig ";
        $update_stmt = $this->db->prepare($qryPassReuseCnfgVal);
        $update_stmt->execute();
        if ($rs = $update_stmt->fetch()) 
        {
            $this->passwordReusePrevention   = $rs["passwordReusePrevention"]; 
        }        
        return $this->passwordReusePrevention;        
    }
}
?>