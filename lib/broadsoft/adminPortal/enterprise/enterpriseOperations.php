<?php 
class EnterpriseOperations{
    
    public function addEnterprise($entArray){
        global  $sessionid, $client;
        $enterpriseAddResp["Error"] = array();
        $enterpriseAddResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderAddRequest13mp2");
        $xmlinput .= "<isEnterprise>true</isEnterprise>";
        
        $xmlinput .= "<serviceProviderId>".$entArray["serviceProviderId"]."</serviceProviderId>";
        $xmlinput .= "<defaultDomain>".$entArray["defaultDomain"]."</defaultDomain>";
        if(isset($entArray["serviceProviderName"]) && $entArray["serviceProviderName"] !=""){
            $xmlinput .= "<serviceProviderName>".$entArray["serviceProviderName"]."</serviceProviderName>";
        }
        
        if(isset($entArray["supportEmail"]) && $entArray["supportEmail"] !=""){
            $xmlinput .= "<supportEmail>".$entArray["supportEmail"]."</supportEmail>";
        }
        if((isset($entArray["contactName"]) && !empty($entArray["contactName"])) || (isset($entArray["contactNumber"]) && !empty($entArray["contactNumber"])) || (isset($entArray["contactEmail"]) && !empty($entArray["contactEmail"]))){
            $xmlinput .= "<contact>";
            if((isset($entArray["contactName"]) && !empty($entArray["contactName"]))){
                $xmlinput .= "<contactName>".$entArray["contactName"]."</contactName>";
            }
            if((isset($entArray["contactNumber"]) && !empty($entArray["contactNumber"]))){
                $xmlinput .= "<contactNumber>".$entArray["contactNumber"]."</contactNumber>";
            }
            if((isset($entArray["contactEmail"]) && !empty($entArray["contactEmail"]))){
                $xmlinput .= "<contactEmail>".$entArray["contactEmail"]."</contactEmail>";
            }
            $xmlinput .= "</contact>";
        }
        
        if((isset($entArray["addressLine1"]) && !empty($entArray["addressLine1"])) || (isset($entArray["addressLine2"]) && !empty($entArray["addressLine2"])) ||
            (isset($entArray["city"]) && !empty($entArray["city"])) || (isset($entArray["stateOrProvince"]) && !empty($entArray["stateOrProvince"])) ||
            (isset($entArray["zipOrPostalCode"]) && !empty($entArray["zipOrPostalCode"])) || (isset($entArray["country"]) && !empty($entArray["country"]))){
            $xmlinput .= "<address>";
            if((isset($entArray["addressLine1"]) && !empty($entArray["addressLine1"]))){
                $xmlinput .= "<addressLine1>".$entArray["addressLine1"]."</addressLine1>";
            }
            if((isset($entArray["addressLine2"]) && !empty($entArray["addressLine2"]))){
                $xmlinput .= "<addressLine2>".$entArray["addressLine2"]."</addressLine2>";
            }
            if((isset($entArray["city"]) && !empty($entArray["city"]))){
                $xmlinput .= "<city>".$entArray["city"]."</city>";
            }
            if((isset($entArray["stateOrProvince"]) && !empty($entArray["stateOrProvince"]))){
                $xmlinput .= "<stateOrProvince>".$entArray["stateOrProvince"]."</stateOrProvince>";
            }
            if((isset($entArray["zipOrPostalCode"]) && !empty($entArray["zipOrPostalCode"]))){
                $xmlinput .= "<zipOrPostalCode>".$entArray["zipOrPostalCode"]."</zipOrPostalCode>";
            }
            if((isset($entArray["country"]) && !empty($entArray["country"]))){
                $xmlinput .= "<country>".$entArray["country"]."</country>";
            }
            $xmlinput .= "</address>";
        }
        
        
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $enterpriseAddResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $enterpriseAddResp["Success"] = "Success";
        }
        return $enterpriseAddResp;
    }
    
    public function modifyEnterprise($entArray){
        global  $sessionid, $client;
        $enterpriseModifyResp["Error"] = array();
        $enterpriseModifyResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderModifyRequest");
        /*if(isset($entArray["isEnterprise"]) && $entArray["isEnterprise"] !=""){
            $xmlinput .= "<isEnterprise>true</isEnterprise>";
        }
        if(isset($entArray["useCustomRoutingProfile"]) && $entArray["useCustomRoutingProfile"] =="true"){
            $xmlinput .= "<useCustomRoutingProfile>true</useCustomRoutingProfile>";
        }else{
            $xmlinput .= "<useCustomRoutingProfile>false</useCustomRoutingProfile>";
        }*/
        $xmlinput .= "<serviceProviderId>".$entArray["serviceProviderId"]."</serviceProviderId>";
        if(isset($entArray["defaultDomain"]) && !empty($entArray["defaultDomain"])){
            $xmlinput .= "<defaultDomain>".$entArray["defaultDomain"]."</defaultDomain>";
        }
        
        if(isset($entArray["serviceProviderName"]) && $entArray["serviceProviderName"] !=""){
            $xmlinput .= "<serviceProviderName>".$entArray["serviceProviderName"]."</serviceProviderName>";
        }else if(isset($entArray["serviceProviderName"]) && $entArray["serviceProviderName"] ==""){
            $xmlinput .= "<serviceProviderName xsi:nil='true'></serviceProviderName>";
        }
        
        if(isset($entArray["supportEmail"]) && $entArray["supportEmail"] !=""){
            $xmlinput .= "<supportEmail>".$entArray["supportEmail"]."</supportEmail>";
        }else if(isset($entArray["supportEmail"]) && $entArray["supportEmail"] ==""){
            $xmlinput .= "<supportEmail xsi:nil='true'></supportEmail>";
        }
        if(isset($entArray["contactName"]) || isset($entArray["contactNumber"]) || isset($entArray["contactEmail"])){
            $xmlinput .= "<contact>";
            if(isset($entArray["contactName"]) && !empty($entArray["contactName"])){
                $xmlinput .= "<contactName>".$entArray["contactName"]."</contactName>";
            }else if(isset($entArray["contactName"]) && empty($entArray["contactName"])){
                $xmlinput .= "<contactName xsi:nil='true'></contactName>";
            }
            if(isset($entArray["contactNumber"]) && !empty($entArray["contactNumber"])){
                $xmlinput .= "<contactNumber>".$entArray["contactNumber"]."</contactNumber>";
            }else if(isset($entArray["contactNumber"]) && empty($entArray["contactNumber"])){
                $xmlinput .= "<contactNumber xsi:nil='true'></contactNumber>";
            }
            if(isset($entArray["contactEmail"]) && !empty($entArray["contactEmail"])){
                $xmlinput .= "<contactEmail>".$entArray["contactEmail"]."</contactEmail>";
            }else if(isset($entArray["contactEmail"]) && empty($entArray["contactEmail"])){
                $xmlinput .= "<contactEmail xsi:nil='true'></contactEmail>";
            }
            $xmlinput .= "</contact>";
        }
        
        if(isset($entArray["addressLine1"]) || isset($entArray["addressLine2"]) ||
            isset($entArray["city"]) || isset($entArray["stateOrProvince"]) ||
            isset($entArray["zipOrPostalCode"]) || isset($entArray["country"])){
                $xmlinput .= "<address>";
                if(isset($entArray["addressLine1"]) && !empty($entArray["addressLine1"])){
                    $xmlinput .= "<addressLine1>".$entArray["addressLine1"]."</addressLine1>";
                }else if(isset($entArray["addressLine1"]) && empty($entArray["addressLine1"])){
                    $xmlinput .= "<addressLine1 xsi:nil='true'></addressLine1>";
                }
                if(isset($entArray["addressLine2"]) && !empty($entArray["addressLine2"])){
                    $xmlinput .= "<addressLine2>".$entArray["addressLine2"]."</addressLine2>";
                }else if(isset($entArray["addressLine2"]) && empty($entArray["addressLine2"])){
                    $xmlinput .= "<addressLine2 xsi:nil='true'></addressLine2>";
                }
                if(isset($entArray["city"]) && !empty($entArray["city"])){
                    $xmlinput .= "<city>".$entArray["city"]."</city>";
                }else if(isset($entArray["city"]) && empty($entArray["city"])){
                    $xmlinput .= "<city xsi:nil='true'></city>";
                }
                if(isset($entArray["stateOrProvince"]) && !empty($entArray["stateOrProvince"])){
                    $xmlinput .= "<stateOrProvince>".$entArray["stateOrProvince"]."</stateOrProvince>";
                }else if(isset($entArray["stateOrProvince"]) && empty($entArray["stateOrProvince"])){
                    $xmlinput .= "<stateOrProvince xsi:nil='true'></stateOrProvince>";
                }
                if(isset($entArray["zipOrPostalCode"]) && !empty($entArray["zipOrPostalCode"])){
                    $xmlinput .= "<zipOrPostalCode>".$entArray["zipOrPostalCode"]."</zipOrPostalCode>";
                }else if(isset($entArray["zipOrPostalCode"]) && empty($entArray["zipOrPostalCode"])){
                    $xmlinput .= "<zipOrPostalCode xsi:nil='true'></zipOrPostalCode>";
                }
                if(isset($entArray["country"]) && !empty($entArray["country"])){
                    $xmlinput .= "<country>".$entArray["country"]."</country>";
                }else if(isset($entArray["country"]) && empty($entArray["country"])){
                    $xmlinput .= "<country xsi:nil='true'></country>";
                }
                $xmlinput .= "</address>";
        }
        
        
        $xmlinput .= xmlFooter();
        //print_r($xmlinput);
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $enterpriseModifyResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $enterpriseModifyResp["Success"] = "Success";
        }
        return $enterpriseModifyResp;
        
    }
    
    public function getAllEnterprise(){
        global  $sessionid, $client;
        $entResp["Error"] = array();
        $entResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderGetListRequest");
        $xmlinput .= "<isEnterprise>true</isEnterprise>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        if (readErrorXmlGenuine($xml) != "") {
            $entResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            if(count($xml->command->serviceProviderTable->row) > 0){
                $i = 0;
                foreach ($xml->command->serviceProviderTable->row as $key => $value){
                    $entResp["Success"][$i]["enterpriseId"] = strval($value->col[0]);
                    $entResp["Success"][$i]["enterpriseName"] = strval($value->col[1]);
                    $i++;
                }
            }
        }
        return $entResp;
    }
    
    public function getEnterpriseInfo($serviceProviderId, $ociVersion){
        global  $sessionid, $client;
        $enterpriseInfo["Error"] = array();
        $enterpriseInfo["Success"] = array();
        if($ociVersion == "20" || $ociVersion == "21"){
            $xmlinput = xmlHeader($sessionid, "ServiceProviderGetRequest17sp1");
        }else if($ociVersion == "22"){
            $xmlinput = xmlHeader($sessionid, "ServiceProviderGetRequest22");
        }
        
        $xmlinput .= "<serviceProviderId>".$serviceProviderId."</serviceProviderId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $enterpriseInfo["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $enterpriseInfo["Success"]["enterpriseId"] = $serviceProviderId;
            $enterpriseInfo["Success"]["defaultDomain"] = strval($xml->command->defaultDomain);
            $enterpriseInfo["Success"]["serviceProviderName"] = strval($xml->command->serviceProviderName);
            $enterpriseInfo["Success"]["supportEmail"] = strval($xml->command->supportEmail);
            $enterpriseInfo["Success"]["contactName"] = strval($xml->command->contact->contactName);
            $enterpriseInfo["Success"]["contactNumber"] = strval($xml->command->contact->contactNumber);
            $enterpriseInfo["Success"]["contactEmail"] = strval($xml->command->contact->contactEmail);
            
            $enterpriseInfo["Success"]["addressLine1"] = strval($xml->command->address->addressLine1);
            $enterpriseInfo["Success"]["addressLine2"] = strval($xml->command->address->addressLine2);
            $enterpriseInfo["Success"]["city"] = strval($xml->command->address->city);
            $enterpriseInfo["Success"]["stateOrProvince"] = strval($xml->command->address->stateOrProvince);
            $enterpriseInfo["Success"]["zipOrPostalCode"] = strval($xml->command->address->zipOrPostalCode);
            $enterpriseInfo["Success"]["country"] = strval($xml->command->address->country);
        }
        
        return $enterpriseInfo;
    }
    
    public function deleteEnterprise($serviceProviderId){
        global  $sessionid, $client;
        $entDeleteResp["Error"] = array();
        $entDeleteResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderDeleteRequest");
        $xmlinput .= "<serviceProviderId>".$serviceProviderId."</serviceProviderId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $entDeleteResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $entDeleteResp["Success"] = "Success";
        }
        return $entDeleteResp;
    }
    
    public function getAllSystemDomain(){
        global  $sessionid, $client;
        $systemDomains["Error"] = array();
        $systemDomains["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "SystemDomainGetListRequest");
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $systemDomains["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $systemDomains["Success"]["systemDefaultDomain"] = strval($xml->command->systemDefaultDomain);
            foreach ($xml->command->domain as $key => $value){
                $systemDomains["Success"]["domain"][] = strval($value);
            }
            
        }
        return $systemDomains;
    }
    
    public function getAllStateProvince(){
        global  $sessionid, $client;
        $stateList["Error"] = array();
        $stateList["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "SystemStateOrProvinceGetListRequest");
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        if (readErrorXmlGenuine($xml) != "") {
            $stateList["Error"] = strval($xml->command->summaryEnglish);
        }else{
            foreach ($xml->command->stateOrProvinceTable->row as $key => $value){
                $stateList["Success"][] = strval($value->col[0]);
            }  
        }
        return $stateList;
    }
    
    public function systemDomainGetListRequest(){
        global  $sessionid, $client;
        $systemDomainsList["Error"] = array();
        $systemDomainsList["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "SystemDomainGetListRequest");
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        if (readErrorXmlGenuine($xml) != "") {
            $systemDomainsList["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $systemDomainsList["Success"]["defaultDomain"] = strval($xml->command->systemDefaultDomain);
            foreach ($xml->command->domain as $key => $value){
                $systemDomainsList["Success"]["domains"][] = strval($value);
            }
        }
        return $systemDomainsList;
    }
    
    public function serviceProviderDomainGetAssignedListRequest($serviceProviderId){
        global  $sessionid, $client;
        $spDomainsList["Error"] = array();
        $spDomainsList["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderDomainGetAssignedListRequest");
        $xmlinput .= "<serviceProviderId>".$serviceProviderId."</serviceProviderId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        //die;
        if (readErrorXmlGenuine($xml) != "") {
            $spDomainsList["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $spDomainsList["Success"]["defaultDomain"] = strval($xml->command->serviceProviderDefaultDomain);
            foreach ($xml->command->domain as $key => $value){
                $spDomainsList["Success"]["domains"][] = strval($value);
            }
        }
        return $spDomainsList;
    }
    
    public function getAvailableDomainsforEnterprise($allDomains, $allAssignedDomain){
        
        $array = array();
        if(count($allDomains) > 0 && count($allAssignedDomain) > 0){
            $array['available'] = array_values(array_diff($allDomains['Success']["domains"], $allAssignedDomain['Success']["domains"]));
        }
        return $array;
    }
    
    public function getAvailableDomainsBasedOnSPattern($array, $securityDomainPattern) {
        $availableDomain = array();
        if(!empty($securityDomainPattern)){
            foreach ($array as $domain) {
                if (!empty($securityDomainPattern) && strpos($domain, $securityDomainPattern) === false ) {
                    $availableDomain[] = $domain;
                }
            }
        }else{
            foreach ($array as $domain) {
                $availableDomain[] = $domain;
            }
        }
        return $availableDomain;
    }
    
    public function getSecurityDomain($array, $securityDomainPattern) {
        global  $sessionid, $client;
        $securityDomain = array();
        
        foreach ($array as $domain) {
            if (!empty($securityDomainPattern) && strpos($domain, $securityDomainPattern) !== false ) {
                $securityDomain[] = $domain;
            }
        }
        
        return $securityDomain;
    }
    
    public function serviceProviderServiceGetAuthorizationListRequest($spId){
        global  $sessionid, $client;
        $servicePackGetAllServices["Error"] = array();
        $servicePackGetAllServices["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServiceGetAuthorizationListRequest");
        $xmlinput .= "<serviceProviderId>".$spId."</serviceProviderId>";
        
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);exit;
        if (readErrorXmlGenuine($xml) != "") {
            $servicePackGetAllServices["Error"] = strval($xml->command->summaryEnglish);
        }else{
            if(count($xml->command->userServicesAuthorizationTable->row) > 0){
                //$key1 = 0;
                foreach ($xml->command->userServicesAuthorizationTable->row as $key => $value){
                    $serviceName = strval($value->col[0]);
                    $servicePackGetAllServices["Success"]["userServices"][ $serviceName]["serviceName"] = strval($value->col[0]);
                    $servicePackGetAllServices["Success"]["userServices"][ $serviceName]["authorized"] = strval($value->col[1]);
                    $servicePackGetAllServices["Success"]["userServices"][ $serviceName]["assigned"] = strval($value->col[2]);
                    $servicePackGetAllServices["Success"]["userServices"][ $serviceName]["limited"] = strval($value->col[3]);
                    $servicePackGetAllServices["Success"]["userServices"][ $serviceName]["quantity"] = strval($value->col[4]);
                    $servicePackGetAllServices["Success"]["userServices"][ $serviceName]["allocated"] = strval($value->col[5]);
                    $servicePackGetAllServices["Success"]["userServices"][ $serviceName]["licensed"] = strval($value->col[6]);
                    $servicePackGetAllServices["Success"]["userServices"][ $serviceName]["servicePackAllocation"] = strval($value->col[7]);
                    $servicePackGetAllServices["Success"]["userServices"][ $serviceName]["userAssignable"] = strval($value->col[8]);
                    $servicePackGetAllServices["Success"]["userServices"][ $serviceName]["servicePackAssignable"] = strval($value->col[9]);
                
                    //$key1++;
                }                
            }
            
            if(count($xml->command->groupServicesAuthorizationTable->row) > 0){
//                 $key2 = 0;
                foreach ($xml->command->groupServicesAuthorizationTable->row as $key => $value){
                    $serviceName = strval($value->col[0]);
                    $servicePackGetAllServices["Success"]["groupServices"][ $serviceName]["serviceName"] = strval($value->col[0]);
                    $servicePackGetAllServices["Success"]["groupServices"][ $serviceName]["authorized"] = strval($value->col[1]);
                    $servicePackGetAllServices["Success"]["groupServices"][ $serviceName]["assigned"] = strval($value->col[2]);
                    $servicePackGetAllServices["Success"]["groupServices"][ $serviceName]["limited"] = strval($value->col[3]);
                    $servicePackGetAllServices["Success"]["groupServices"][ $serviceName]["quantity"] = strval($value->col[4]);
                    $servicePackGetAllServices["Success"]["groupServices"][ $serviceName]["allocated"] = strval($value->col[5]);
                    $servicePackGetAllServices["Success"]["groupServices"][ $serviceName]["licensed"] = strval($value->col[6]);
                    $servicePackGetAllServices["Success"]["groupServices"][ $serviceName]["servicePackAllocation"] = strval($value->col[7]);
                
//                     $key2++;
                }
            }
        }
        return $servicePackGetAllServices;
    }
    
    public function serviceProviderServiceModifyAuthorizationListRequest($spId, $modifiedEntServices) {
        global  $sessionid, $client;
        $servicePackGetAllServices["Error"] = array();
        $servicePackGetAllServices["Success"] = array();
        $serviceRequestTags = array("groupServices" => "groupServiceAuthorization", "userServices" => "userServiceAuthorization");
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServiceModifyAuthorizationListRequest");
        $xmlinput .= "<serviceProviderId>".$spId."</serviceProviderId>";
        
        foreach($modifiedEntServices as $serviceKey => $serviceValue) {
            
            foreach($serviceValue as $grpKey => $grpValue) {
                $xmlinput .= "<" . $serviceRequestTags[$serviceKey] . ">";
                $xmlinput .= "<serviceName>" . $grpKey . "</serviceName>";
                if( $grpValue['authorized'] == "false" && $_SESSION[$serviceKey][$grpKey]['allocated'] <= 0) {
                    $xmlinput .= "<unauthorized>" . "true" . "</unauthorized>";
                } else {
                    $xmlinput .= "<authorizedQuantity>";
                    if( $grpValue['limited'] == "false" || (!isset($grpValue['limited']) && !isset($grpValue['quantity'])) ) {
                        $xmlinput .= "<unlimited>" . "true" . "</unlimited>";
                    } else if( isset($grpValue['quantity']) && $grpValue['quantity'] != "" ) {
                        $xmlinput .= "<quantity>" . $grpValue['quantity'] . "</quantity>";
                    }
                    $xmlinput .= "</authorizedQuantity>";
                }
                
                $xmlinput .= "</" . $serviceRequestTags[$serviceKey] . ">";
            }
        }
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        if (readErrorXmlGenuine($xml) != "") {
            $servicePackGetAllServices["Error"] = strval($xml->command->summaryEnglish);
        } else {
            $servicePackGetAllServices["Success"] = "Success";
        }
        
        return $servicePackGetAllServices;
    }
    

    public function enterpriseDomainAssignRequest($serviceProviderId, $domainArray){
        global  $sessionid, $client;
        $systemDomainsResp["Error"] = array();
        $systemDomainsResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderDomainAssignListRequest");
        $xmlinput .= "<serviceProviderId>".$serviceProviderId."</serviceProviderId>";
        foreach ($domainArray as $key => $value){
            $xmlinput .= "<domain>".$value."</domain>";
        }
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $systemDomainsResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $systemDomainsResp["Success"] = "Success";
        }
        return $systemDomainsResp;
    }
    
    public function enterpriseDomainUnAssignRequest($serviceProviderId, $domainArray){
        global  $sessionid, $client;
        $systemDomainsRespUnAssign["Error"] = array();
        $systemDomainsRespUnAssign["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderDomainUnassignListRequest");
        $xmlinput .= "<serviceProviderId>".$serviceProviderId."</serviceProviderId>";
        foreach ($domainArray as $key => $value){
            $xmlinput .= "<domain>".$value."</domain>";
        }
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $systemDomainsRespUnAssign["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $systemDomainsRespUnAssign["Success"] = "Success";
        }
        return $systemDomainsRespUnAssign;
    }
    
    function enterpriseCallProcessingGetRequest($serviceProviderId, $ociVersion){
        global  $sessionid, $client;
        $cpPolicy["Error"] = array();
        $cpPolicy["Success"] = array();
        if($ociVersion == "20"){
            $xmlinput = xmlHeader($sessionid, "ServiceProviderCallProcessingGetPolicyRequest19sp1");
        }else if($ociVersion == "21"){
            $xmlinput = xmlHeader($sessionid, "ServiceProviderCallProcessingGetPolicyRequest21sp1");
        }else{
            $xmlinput = xmlHeader($sessionid, "ServiceProviderCallProcessingGetPolicyRequest22");
        }
        $xmlinput .= "<serviceProviderId>".$serviceProviderId."</serviceProviderId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        //die;
        if (readErrorXmlGenuine($xml) != "") {
            $cpPolicy["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $cpPolicy["Success"]["useServiceProviderDCLIDSetting"] = strval($xml->command->useServiceProviderDCLIDSetting);
            $cpPolicy["Success"]["useMaxSimultaneousCalls"] = strval($xml->command->useMaxSimultaneousCalls);
            $cpPolicy["Success"]["maxSimultaneousCalls"] = strval($xml->command->maxSimultaneousCalls);
            $cpPolicy["Success"]["useMaxSimultaneousVideoCalls"] = strval($xml->command->useMaxSimultaneousVideoCalls);
            $cpPolicy["Success"]["maxSimultaneousVideoCalls"] = strval($xml->command->maxSimultaneousVideoCalls);
            $cpPolicy["Success"]["useMaxCallTimeForAnsweredCalls"] = strval($xml->command->useMaxCallTimeForAnsweredCalls);
            $cpPolicy["Success"]["maxCallTimeForAnsweredCallsMinutes"] = strval($xml->command->maxCallTimeForAnsweredCallsMinutes);
            $cpPolicy["Success"]["useMaxCallTimeForUnansweredCalls"] = strval($xml->command->useMaxCallTimeForUnansweredCalls);
            $cpPolicy["Success"]["maxCallTimeForUnansweredCallsMinutes"] = strval($xml->command->maxCallTimeForUnansweredCallsMinutes);
            $cpPolicy["Success"]["mediaPolicySelection"] = strval($xml->command->mediaPolicySelection);
            $cpPolicy["Success"]["networkUsageSelection"] = strval($xml->command->networkUsageSelection);
            $cpPolicy["Success"]["enforceGroupCallingLineIdentityRestriction"] = strval($xml->command->enforceGroupCallingLineIdentityRestriction);
            $cpPolicy["Success"]["allowEnterpriseGroupCallTypingForPrivateDialingPlan"] = strval($xml->command->allowEnterpriseGroupCallTypingForPrivateDialingPlan);
            $cpPolicy["Success"]["allowEnterpriseGroupCallTypingForPublicDialingPlan"] = strval($xml->command->allowEnterpriseGroupCallTypingForPublicDialingPlan);
            $cpPolicy["Success"]["overrideCLIDRestrictionForPrivateCallCategory"] = strval($xml->command->overrideCLIDRestrictionForPrivateCallCategory);
            $cpPolicy["Success"]["useEnterpriseCLIDForPrivateCallCategory"] = strval($xml->command->useEnterpriseCLIDForPrivateCallCategory);
            $cpPolicy["Success"]["enableEnterpriseExtensionDialing"] = strval($xml->command->enableEnterpriseExtensionDialing);
            $cpPolicy["Success"]["enforceEnterpriseCallingLineIdentityRestriction"] = strval($xml->command->enforceEnterpriseCallingLineIdentityRestriction);
            $cpPolicy["Success"]["useSettingLevel"] = strval($xml->command->useSettingLevel);
            $cpPolicy["Success"]["useMaxConcurrentRedirectedCalls"] = strval($xml->command->useMaxConcurrentRedirectedCalls);
            
            $cpPolicy["Success"]["maxConcurrentRedirectedCalls"] = strval($xml->command->maxConcurrentRedirectedCalls);
            $cpPolicy["Success"]["useMaxFindMeFollowMeDepth"] = strval($xml->command->useMaxFindMeFollowMeDepth);
            $cpPolicy["Success"]["maxFindMeFollowMeDepth"] = strval($xml->command->maxFindMeFollowMeDepth);
            $cpPolicy["Success"]["maxRedirectionDepth"] = strval($xml->command->maxRedirectionDepth);
            $cpPolicy["Success"]["useMaxConcurrentFindMeFollowMeInvocations"] = strval($xml->command->useMaxConcurrentFindMeFollowMeInvocations);
            $cpPolicy["Success"]["maxConcurrentFindMeFollowMeInvocations"] = strval($xml->command->maxConcurrentFindMeFollowMeInvocations);
            $cpPolicy["Success"]["clidPolicy"] = strval($xml->command->clidPolicy);
            $cpPolicy["Success"]["emergencyClidPolicy"] = strval($xml->command->emergencyClidPolicy);
            $cpPolicy["Success"]["allowAlternateNumbersForRedirectingIdentity"] = strval($xml->command->allowAlternateNumbersForRedirectingIdentity);
            $cpPolicy["Success"]["blockCallingNameForExternalCalls"] = strval($xml->command->blockCallingNameForExternalCalls);
            $cpPolicy["Success"]["enableDialableCallerID"] = strval($xml->command->enableDialableCallerID);
            $cpPolicy["Success"]["allowConfigurableCLIDForRedirectingIdentity"] = strval($xml->command->allowConfigurableCLIDForRedirectingIdentity);
            $cpPolicy["Success"]["enterpriseCallsCLIDPolicy"] = strval($xml->command->enterpriseCallsCLIDPolicy);
            $cpPolicy["Success"]["groupCallsCLIDPolicy"] = strval($xml->command->groupCallsCLIDPolicy);
            $cpPolicy["Success"]["enablePhoneListLookup"] = strval($xml->command->enablePhoneListLookup);
            $cpPolicy["Success"]["conferenceURI"] = strval($xml->command->conferenceURI);
            
            if($ociVersion == "22"){
                $cpPolicy["Success"]["useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable"] = strval($xml->command->useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable);
                $cpPolicy["Success"]["useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable"] = strval($xml->command->useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable);
            }
            
            
        }
        return $cpPolicy;
    }
    
    
    function modifyEnterpriseCallProcessingPolicy($entArray){
        global  $sessionid, $client;
        $enterpriseModifyResp["Error"] = array();
        $enterpriseModifyResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderCallProcessingModifyPolicyRequest15");
        
        $xmlinput .= "<serviceProviderId>".$entArray["serviceProviderId"]."</serviceProviderId>";
        
        if(isset($entArray["useServiceProviderDCLIDSetting"]) && !empty($entArray["useServiceProviderDCLIDSetting"])){
            $xmlinput .= "<useServiceProviderDCLIDSetting>".$entArray["useServiceProviderDCLIDSetting"]."</useServiceProviderDCLIDSetting>";
        }
        
        if(isset($entArray["useMaxSimultaneousCalls"]) && !empty($entArray["useMaxSimultaneousCalls"])){
            $xmlinput .= "<useMaxSimultaneousCalls>".$entArray["useMaxSimultaneousCalls"]."</useMaxSimultaneousCalls>";
        }
        
        if(isset($entArray["maxSimultaneousCalls"]) && !empty($entArray["maxSimultaneousCalls"])){
            $xmlinput .= "<maxSimultaneousCalls>".$entArray["maxSimultaneousCalls"]."</maxSimultaneousCalls>";
        }
        
        if(isset($entArray["useMaxSimultaneousVideoCalls"]) && !empty($entArray["useMaxSimultaneousVideoCalls"])){
            $xmlinput .= "<useMaxSimultaneousVideoCalls>".$entArray["useMaxSimultaneousVideoCalls"]."</useMaxSimultaneousVideoCalls>";
        }
        
        if(isset($entArray["maxSimultaneousVideoCalls"]) && !empty($entArray["maxSimultaneousVideoCalls"])){
            $xmlinput .= "<maxSimultaneousVideoCalls>".$entArray["maxSimultaneousVideoCalls"]."</maxSimultaneousVideoCalls>";
        }
        
        if(isset($entArray["useMaxCallTimeForAnsweredCalls"]) && !empty($entArray["useMaxCallTimeForAnsweredCalls"])){
            $xmlinput .= "<useMaxCallTimeForAnsweredCalls>".$entArray["useMaxCallTimeForAnsweredCalls"]."</useMaxCallTimeForAnsweredCalls>";
        }
        
        if(isset($entArray["maxCallTimeForAnsweredCallsMinutes"]) && !empty($entArray["maxCallTimeForAnsweredCallsMinutes"])){
            $xmlinput .= "<maxCallTimeForAnsweredCallsMinutes>".$entArray["maxCallTimeForAnsweredCallsMinutes"]."</maxCallTimeForAnsweredCallsMinutes>";
        }
        
        if(isset($entArray["useMaxCallTimeForUnansweredCalls"]) && !empty($entArray["useMaxCallTimeForUnansweredCalls"])){
            $xmlinput .= "<useMaxCallTimeForUnansweredCalls>".$entArray["useMaxCallTimeForUnansweredCalls"]."</useMaxCallTimeForUnansweredCalls>";
        }
        
        if(isset($entArray["maxCallTimeForUnansweredCallsMinutes"]) && !empty($entArray["maxCallTimeForUnansweredCallsMinutes"])){
            $xmlinput .= "<maxCallTimeForUnansweredCallsMinutes>".$entArray["maxCallTimeForUnansweredCallsMinutes"]."</maxCallTimeForUnansweredCallsMinutes>";
        }
        
        if(isset($entArray["mediaPolicySelection"]) && !empty($entArray["mediaPolicySelection"])){
            $xmlinput .= "<mediaPolicySelection>".$entArray["mediaPolicySelection"]."</mediaPolicySelection>";
        }
        
        if(isset($entArray["networkUsageSelection"]) && !empty($entArray["networkUsageSelection"])){
            $xmlinput .= "<networkUsageSelection>".$entArray["networkUsageSelection"]."</networkUsageSelection>";
        }
        
        if(isset($entArray["enforceGroupCallingLineIdentityRestriction"]) && !empty($entArray["enforceGroupCallingLineIdentityRestriction"])){
            $xmlinput .= "<enforceGroupCallingLineIdentityRestriction>".$entArray["enforceGroupCallingLineIdentityRestriction"]."</enforceGroupCallingLineIdentityRestriction>";
        }
        
        if(isset($entArray["allowEnterpriseGroupCallTypingForPrivateDialingPlan"]) && !empty($entArray["allowEnterpriseGroupCallTypingForPrivateDialingPlan"])){
            $xmlinput .= "<allowEnterpriseGroupCallTypingForPrivateDialingPlan>".$entArray["allowEnterpriseGroupCallTypingForPrivateDialingPlan"]."</allowEnterpriseGroupCallTypingForPrivateDialingPlan>";
        }
        
        if(isset($entArray["allowEnterpriseGroupCallTypingForPublicDialingPlan"]) && !empty($entArray["allowEnterpriseGroupCallTypingForPublicDialingPlan"])){
            $xmlinput .= "<allowEnterpriseGroupCallTypingForPublicDialingPlan>".$entArray["allowEnterpriseGroupCallTypingForPublicDialingPlan"]."</allowEnterpriseGroupCallTypingForPublicDialingPlan>";
        }
        
        if(isset($entArray["overrideCLIDRestrictionForPrivateCallCategory"]) && !empty($entArray["overrideCLIDRestrictionForPrivateCallCategory"])){
            $xmlinput .= "<overrideCLIDRestrictionForPrivateCallCategory>".$entArray["overrideCLIDRestrictionForPrivateCallCategory"]."</overrideCLIDRestrictionForPrivateCallCategory>";
        }
        
        if(isset($entArray["useEnterpriseCLIDForPrivateCallCategory"]) && !empty($entArray["useEnterpriseCLIDForPrivateCallCategory"])){
            $xmlinput .= "<useEnterpriseCLIDForPrivateCallCategory>".$entArray["useEnterpriseCLIDForPrivateCallCategory"]."</useEnterpriseCLIDForPrivateCallCategory>";
        }
        
        if(isset($entArray["enableEnterpriseExtensionDialing"]) && !empty($entArray["enableEnterpriseExtensionDialing"])){
            $xmlinput .= "<enableEnterpriseExtensionDialing>".$entArray["enableEnterpriseExtensionDialing"]."</enableEnterpriseExtensionDialing>";
        }
        
        if(isset($entArray["enforceEnterpriseCallingLineIdentityRestriction"]) && !empty($entArray["enforceEnterpriseCallingLineIdentityRestriction"])){
            $xmlinput .= "<enforceEnterpriseCallingLineIdentityRestriction>".$entArray["enforceEnterpriseCallingLineIdentityRestriction"]."</enforceEnterpriseCallingLineIdentityRestriction>";
        }
        
        if(isset($entArray["useSettingLevel"]) && !empty($entArray["useSettingLevel"])){
            $xmlinput .= "<useSettingLevel>".$entArray["useSettingLevel"]."</useSettingLevel>";
        }
        
        if(isset($entArray["conferenceURI"]) && !empty($entArray["conferenceURI"])){
            $xmlinput .= "<conferenceURI>".$entArray["conferenceURI"]."</conferenceURI>";
        }else if(isset($entArray["conferenceURI"]) && $entArray["conferenceURI"]==""){
            $xmlinput .= "<conferenceURI xsi:nil='true'></conferenceURI>";
        }
        
        if(isset($entArray["useMaxConcurrentRedirectedCalls"]) && !empty($entArray["useMaxConcurrentRedirectedCalls"])){
            $xmlinput .= "<useMaxConcurrentRedirectedCalls>".$entArray["useMaxConcurrentRedirectedCalls"]."</useMaxConcurrentRedirectedCalls>";
        }
        
        if(isset($entArray["maxConcurrentRedirectedCalls"]) && !empty($entArray["maxConcurrentRedirectedCalls"])){
            $xmlinput .= "<maxConcurrentRedirectedCalls>".$entArray["maxConcurrentRedirectedCalls"]."</maxConcurrentRedirectedCalls>";
        }
        
        if(isset($entArray["useMaxFindMeFollowMeDepth"]) && !empty($entArray["useMaxFindMeFollowMeDepth"])){
            $xmlinput .= "<useMaxFindMeFollowMeDepth>".$entArray["useMaxFindMeFollowMeDepth"]."</useMaxFindMeFollowMeDepth>";
        }
        
        if(isset($entArray["maxFindMeFollowMeDepth"]) && !empty($entArray["maxFindMeFollowMeDepth"])){
            $xmlinput .= "<maxFindMeFollowMeDepth>".$entArray["maxFindMeFollowMeDepth"]."</maxFindMeFollowMeDepth>";
        }
        
        if(isset($entArray["maxRedirectionDepth"]) && !empty($entArray["maxRedirectionDepth"])){
            $xmlinput .= "<maxRedirectionDepth>".$entArray["maxRedirectionDepth"]."</maxRedirectionDepth>";
        }
        
        if(isset($entArray["useMaxConcurrentFindMeFollowMeInvocations"]) && !empty($entArray["useMaxConcurrentFindMeFollowMeInvocations"])){
            $xmlinput .= "<useMaxConcurrentFindMeFollowMeInvocations>".$entArray["useMaxConcurrentFindMeFollowMeInvocations"]."</useMaxConcurrentFindMeFollowMeInvocations>";
        }
        
        if(isset($entArray["maxConcurrentFindMeFollowMeInvocations"]) && !empty($entArray["maxConcurrentFindMeFollowMeInvocations"])){
            $xmlinput .= "<maxConcurrentFindMeFollowMeInvocations>".$entArray["maxConcurrentFindMeFollowMeInvocations"]."</maxConcurrentFindMeFollowMeInvocations>";
        }
        
        if(isset($entArray["clidPolicy"]) && !empty($entArray["clidPolicy"])){
            $xmlinput .= "<clidPolicy>".$entArray["clidPolicy"]."</clidPolicy>";
        }
        
        if(isset($entArray["emergencyClidPolicy"]) && !empty($entArray["emergencyClidPolicy"])){
            $xmlinput .= "<emergencyClidPolicy>".$entArray["emergencyClidPolicy"]."</emergencyClidPolicy>";
        }
        
        if(isset($entArray["allowAlternateNumbersForRedirectingIdentity"]) && !empty($entArray["allowAlternateNumbersForRedirectingIdentity"])){
            $xmlinput .= "<allowAlternateNumbersForRedirectingIdentity>".$entArray["allowAlternateNumbersForRedirectingIdentity"]."</allowAlternateNumbersForRedirectingIdentity>";
        }
        
        if(isset($entArray["enableDialableCallerID"]) && !empty($entArray["enableDialableCallerID"])){
            $xmlinput .= "<enableDialableCallerID>".$entArray["enableDialableCallerID"]."</enableDialableCallerID>";
        }
        
        if(isset($entArray["blockCallingNameForExternalCalls"]) && !empty($entArray["blockCallingNameForExternalCalls"])){
            $xmlinput .= "<blockCallingNameForExternalCalls>".$entArray["blockCallingNameForExternalCalls"]."</blockCallingNameForExternalCalls>";
        }
        
        if(isset($entArray["allowConfigurableCLIDForRedirectingIdentity"]) && !empty($entArray["allowConfigurableCLIDForRedirectingIdentity"])){
            $xmlinput .= "<allowConfigurableCLIDForRedirectingIdentity>".$entArray["allowConfigurableCLIDForRedirectingIdentity"]."</allowConfigurableCLIDForRedirectingIdentity>";
        }
        
        if(isset($entArray["enterpriseCallsCLIDPolicy"]) && !empty($entArray["enterpriseCallsCLIDPolicy"])){
            $xmlinput .= "<enterpriseCallsCLIDPolicy>".$entArray["enterpriseCallsCLIDPolicy"]."</enterpriseCallsCLIDPolicy>";
        }
        
        if(isset($entArray["groupCallsCLIDPolicy"]) && !empty($entArray["groupCallsCLIDPolicy"])){
            $xmlinput .= "<groupCallsCLIDPolicy>".$entArray["groupCallsCLIDPolicy"]."</groupCallsCLIDPolicy>";
        }
        
        if(isset($entArray["enablePhoneListLookup"]) && !empty($entArray["enablePhoneListLookup"])){
            $xmlinput .= "<enablePhoneListLookup>".$entArray["enablePhoneListLookup"]."</enablePhoneListLookup>";
        }
        
        if(isset($entArray["useMaxConcurrentTerminatingAlertingRequests"]) && !empty($entArray["useMaxConcurrentTerminatingAlertingRequests"])){
            $xmlinput .= "<useMaxConcurrentTerminatingAlertingRequests>".$entArray["useMaxConcurrentTerminatingAlertingRequests"]."</useMaxConcurrentTerminatingAlertingRequests>";
        }
        
        if(isset($entArray["maxConcurrentTerminatingAlertingRequests"]) && !empty($entArray["maxConcurrentTerminatingAlertingRequests"])){
            $xmlinput .= "<maxConcurrentTerminatingAlertingRequests>".$entArray["maxConcurrentTerminatingAlertingRequests"]."</maxConcurrentTerminatingAlertingRequests>";
        }
        
        if(isset($entArray["includeRedirectionsInMaximumNumberOfConcurrentCalls"]) && !empty($entArray["includeRedirectionsInMaximumNumberOfConcurrentCalls"])){
            $xmlinput .= "<includeRedirectionsInMaximumNumberOfConcurrentCalls>".$entArray["includeRedirectionsInMaximumNumberOfConcurrentCalls"]."</includeRedirectionsInMaximumNumberOfConcurrentCalls>";
        }
        
        if(isset($entArray["useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable"]) && !empty($entArray["useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable"])){
            $xmlinput .= "<useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable>".$entArray["useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable"]."</useUserPhoneNumberForGroupCallsWhenInternalCLIDUnavailable>";
        }
        
        if(isset($entArray["useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable"]) && !empty($entArray["useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable"])){
            $xmlinput .= "<useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable>".$entArray["useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable"]."</useUserPhoneNumberForEnterpriseCallsWhenInternalCLIDUnavailable>";
        }
        
        if(isset($entArray["routeOverrideDomain"]) && !empty($entArray["routeOverrideDomain"])){
            $xmlinput .= "<routeOverrideDomain>".$entArray["routeOverrideDomain"]."</routeOverrideDomain>";
        }
        
        if(isset($entArray["routeOverridePrefix"]) && !empty($entArray["routeOverridePrefix"])){
            $xmlinput .= "<routeOverridePrefix>".$entArray["routeOverridePrefix"]."</routeOverridePrefix>";
        }
        
        $xmlinput .= xmlFooter();
        //print_r($xmlinput);
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $enterpriseModifyResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $enterpriseModifyResp["Success"] = "Success";
        }
        return $enterpriseModifyResp;
        
    }
    

    public function getEnterpriseVoiceMessagingRequest($serviceProviderId){
        global  $sessionid, $client;
        $voiceMessagingResp["Error"] = array();
        $voiceMessagingResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderVoiceMessagingGroupGetRequest");
        $xmlinput .= "<serviceProviderId>".$serviceProviderId."</serviceProviderId>";
        
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $voiceMessagingResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $voiceMessagingResp["Success"]["deliveryFromAddress"] = strval($xml->command->deliveryFromAddress);
            $voiceMessagingResp["Success"]["notificationFromAddress"] = strval($xml->command->notificationFromAddress);
            $voiceMessagingResp["Success"]["voicePortalLockoutFromAddress"] = strval($xml->command->voicePortalLockoutFromAddress);
            $voiceMessagingResp["Success"]["useSystemDefaultDeliveryFromAddress"] = strval($xml->command->useSystemDefaultDeliveryFromAddress);
            $voiceMessagingResp["Success"]["useSystemDefaultNotificationFromAddress"] = strval($xml->command->useSystemDefaultNotificationFromAddress);
            $voiceMessagingResp["Success"]["useSystemDefaultVoicePortalLockoutFromAddress"] = strval($xml->command->useSystemDefaultVoicePortalLockoutFromAddress);
        }
        return $voiceMessagingResp;
    }
    
    public function enterpriseVoiceMessagingModifyRequest($entArray){
        global  $sessionid, $client;
        $voiceMessagingModResp["Error"] = array();
        $voiceMessagingModResp["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderVoiceMessagingGroupModifyRequest");
        $xmlinput .= "<serviceProviderId>".$entArray["serviceProviderId"]."</serviceProviderId>";
        
        if(isset($entArray["deliveryFromAddress"]) && $entArray["deliveryFromAddress"] !=""){
            $xmlinput .= "<deliveryFromAddress>".$entArray["deliveryFromAddress"]."</deliveryFromAddress>";
        }else if(isset($entArray["deliveryFromAddress"]) && $entArray["deliveryFromAddress"] ==""){
            $xmlinput .= "<deliveryFromAddress xsi:nil='true'></deliveryFromAddress>";
        }
        
        if(isset($entArray["notificationFromAddress"]) && $entArray["notificationFromAddress"] !=""){
            $xmlinput .= "<notificationFromAddress>".$entArray["notificationFromAddress"]."</notificationFromAddress>";
        }else if(isset($entArray["notificationFromAddress"]) && $entArray["notificationFromAddress"] ==""){
            $xmlinput .= "<notificationFromAddress xsi:nil='true'></notificationFromAddress>";
        }
        
        if(isset($entArray["voicePortalLockoutFromAddress"]) && $entArray["voicePortalLockoutFromAddress"] !=""){
            $xmlinput .= "<voicePortalLockoutFromAddress>".$entArray["voicePortalLockoutFromAddress"]."</voicePortalLockoutFromAddress>";
        }else if(isset($entArray["voicePortalLockoutFromAddress"]) && $entArray["voicePortalLockoutFromAddress"] ==""){
            $xmlinput .= "<voicePortalLockoutFromAddress xsi:nil='true'></voicePortalLockoutFromAddress>";
        }
        if(isset($entArray["useSystemDefaultDeliveryFromAddress"]) && $entArray["useSystemDefaultDeliveryFromAddress"] !=""){
            $xmlinput .= "<useSystemDefaultDeliveryFromAddress>".$entArray["useSystemDefaultDeliveryFromAddress"]."</useSystemDefaultDeliveryFromAddress>";
        }
        
        if(isset($entArray["useSystemDefaultNotificationFromAddress"]) && $entArray["useSystemDefaultNotificationFromAddress"] !=""){
            $xmlinput .= "<useSystemDefaultNotificationFromAddress>".$entArray["useSystemDefaultNotificationFromAddress"]."</useSystemDefaultNotificationFromAddress>";
        }
        
        if(isset($entArray["useSystemDefaultVoicePortalLockoutFromAddress"]) && $entArray["useSystemDefaultVoicePortalLockoutFromAddress"] !=""){
            $xmlinput .= "<useSystemDefaultVoicePortalLockoutFromAddress>".$entArray["useSystemDefaultVoicePortalLockoutFromAddress"]."</useSystemDefaultVoicePortalLockoutFromAddress>";
        }
        
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $voiceMessagingModResp["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $voiceMessagingModResp["Success"] = "Success";
        }
        return $voiceMessagingModResp;
    }
    
    function enterprieNCSRequest($serviceProvideId){
        global $sessionid, $client;
        $ncsAvailArr = array();
        $ncsAvailArr["Error"] = array();
        $ncsAvailArr["Success"] = array();
        $xmlinput = xmlHeader($sessionid, "ServiceProviderNetworkClassOfServiceGetAssignedListRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $ncsAvailArr["Error"]['Detail'] = strval($xml->command->detail);
        }else{
            if(isset($xml->command->networkClassOfServiceTable->row)){
                foreach ($xml->command->networkClassOfServiceTable->row as $key => $val){
                    $ncsAvailArr["Success"]['ncsAvailArr'][strval($val->col[0])] = strval($val->col[2]);
                }
            }
        }
        return $ncsAvailArr;
    }
    
    function systemNCSGetRequest(){
        global $sessionid, $client;
        $ncsAvailArr = array();
        $ncsAvailArr["Error"] = array();
        $ncsAvailArr["Success"] = array();
        $xmlinput = xmlHeader($sessionid, "SystemNetworkClassOfServiceGetListRequest");
        //$xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $ncsAvailArr["Error"]['Detail'] = strval($xml->command->detail);
        }else{
            if(isset($xml->command->networkClassOfServiceTable->row)){
                foreach ($xml->command->networkClassOfServiceTable->row as $key => $val){
                    $ncsAvailArr["Success"]['systemAvailArr'][] = strval($val->col[0]);
                }
            }
        }
        return $ncsAvailArr;
    }
    
    public function compareSystemAndEnterpriseNCSData($systemArray, $enterpriseArray){
        $compArray = array();
        if(count($systemArray) > 0){
            foreach ($systemArray as $key => $value){
                if(!array_key_exists($value, $enterpriseArray)){
                    $compArray["systemData"][] = $value;
                }
            }
        }
        foreach ($enterpriseArray as $key1 => $value1){
            if($value1 == "true"){
                $compArray["defaultData"][] = $key1;
            }
            $compArray["entData"][] = $key1;
        }
        return $compArray;
        
    }
    
    function enterpriseNCSAssignRequest($serviceProvideId, $entArray){
        global $sessionid, $client;
        $assignResp["Error"] = array();
        $assignResp["Success"] = array();
        $xmlinput = xmlHeader($sessionid, "ServiceProviderNetworkClassOfServiceAssignListRequest21");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
        if(isset($entArray["assigned"]) && count($entArray["assigned"]) > 0){
            foreach ($entArray["assigned"] as $key => $value){
                $xmlinput .= "<networkClassOfService>" . $value . "</networkClassOfService>";
            }
        }
        
        $xmlinput .= "<defaultNetworkClassOfService>";
        if(isset($entArray["default"]) && count($entArray["default"]) > 0){
            foreach ($entArray["default"] as $key => $value){
                $xmlinput .= "<networkClassOfServiceName>" . $value . "</networkClassOfServiceName>";
            }
        }else{
            $xmlinput .= "<useExisting>true</useExisting>";
        }
        $xmlinput .= "</defaultNetworkClassOfService>";
        
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $assignResp["Error"]['Detail'] = strval($xml->command->detail);
        }else{
            $assignResp["Success"] = "Success";
        }
        return $assignResp;
    }
    
    function enterpriseNCSUnAssignRequest($serviceProvideId, $entArray){
        global $sessionid, $client;
        $unAssignResp["Error"] = array();
        $unAssignResp["Success"] = array();
        $xmlinput = xmlHeader($sessionid, "ServiceProviderNetworkClassOfServiceUnassignListRequest21");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
        if(isset($entArray["unassigned"]) && count($entArray["unassigned"]) > 0){
            foreach ($entArray["unassigned"] as $key => $value){
                $xmlinput .= "<networkClassOfService>" . $value . "</networkClassOfService>";
            }
        }
        
        $xmlinput .= "<defaultNetworkClassOfService>";
        if(isset($entArray["default"]) && count($entArray["default"]) > 0){
            foreach ($entArray["default"] as $key => $value){
                $xmlinput .= "<networkClassOfServiceName>" . $value . "</networkClassOfServiceName>";
            }
        }else{
            $xmlinput .= "<useExisting>true</useExisting>";
        }
        $xmlinput .= "</defaultNetworkClassOfService>";
        
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
       
        if (readErrorXmlGenuine($xml) != "") {
            $unAssignResp["Error"] = strval($xml->command->detail);
         }else{
             $unAssignResp["Success"] = "Success";
         }
         return $unAssignResp;
    }
    
    function enterpriseRoutingProfileGetRequest($serviceProvideId){
        global $sessionid, $client;
        $routingProfileResp["Error"] = array();
        $routingProfileResp["Success"] = array();
        $xmlinput = xmlHeader($sessionid, "ServiceProviderRoutingProfileGetRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
        
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $routingProfileResp["Error"] = strval($xml->command->detail);
        }else{
            $routingProfileResp["Success"] = strval($xml->command->routingProfile);
        }
        return $routingProfileResp;
    }
    
    function systemRoutingProfileGetRequest(){
        global $sessionid, $client;
        $routingProfileList["Error"] = array();
        $routingProfileList["Success"] = array();
        $xmlinput = xmlHeader($sessionid, "SystemRoutingProfileGetListRequest");
        
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $routingProfileList["Error"] = strval($xml->command->detail);
         }else{
             if(count($xml->command->routingProfile) > 0){
                 foreach ($xml->command->routingProfile as $key => $value){
                     $routingProfileList["Success"][] = strval($value);
                 }
             }
         }
         return $routingProfileList;
    }
    
    function enterpriseRoutingProfileModifyRequest($serviceProvideId, $entArray){
        global $sessionid, $client;
        $routingResponse["Error"] = array();
        $routingResponse["Success"] = array();
        $xmlinput = xmlHeader($sessionid, "ServiceProviderRoutingProfileModifyRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
        if(!empty($entArray["routingProfile"])){
            $xmlinput .= "<routingProfile>" . $entArray["routingProfile"] . "</routingProfile>";
        }else{
            $xmlinput .= "<routingProfile xsi:nil='true'></routingProfile>";
        }
        
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $routingResponse["Error"] = strval($xml->command->detail);
        }else{
            $routingResponse["Success"] = "Success";
        }
        return $routingResponse;
    }
    
    function enterprisePasswordRulesGetRequest($serviceProvideId, $ociVersion){
        global $sessionid, $client;
        $getResponse["Error"] = array();
        $getResponse["Success"] = array();
        if($ociVersion == "20" || $ociVersion == "21"){
            $xmlinput = xmlHeader($sessionid, "ServiceProviderPasswordRulesGetRequest16");
        }else if($ociVersion == "22"){
            $xmlinput = xmlHeader($sessionid, "ServiceProviderPasswordRulesGetRequest22");
        }
        
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
        
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $getResponse["Error"] = strval($xml->command->detail);
        }else{
            $getResponse["Success"]["rulesApplyTo"] = strval($xml->command->rulesApplyTo);
            $getResponse["Success"]["allowWebAddExternalAuthenticationUsers"] = strval($xml->command->allowWebAddExternalAuthenticationUsers);
            $getResponse["Success"]["disallowUserId"] = strval($xml->command->disallowUserId);
            $getResponse["Success"]["disallowOldPassword"] = strval($xml->command->disallowOldPassword);
            $getResponse["Success"]["disallowReversedOldPassword"] = strval($xml->command->disallowReversedOldPassword);
            $getResponse["Success"]["restrictMinDigits"] = strval($xml->command->restrictMinDigits);
            $getResponse["Success"]["minDigits"] = strval($xml->command->minDigits);
            $getResponse["Success"]["restrictMinUpperCaseLetters"] = strval($xml->command->restrictMinUpperCaseLetters);
            $getResponse["Success"]["minUpperCaseLetters"] = strval($xml->command->minUpperCaseLetters);
            $getResponse["Success"]["restrictMinLowerCaseLetters"] = strval($xml->command->restrictMinLowerCaseLetters);
            $getResponse["Success"]["minLowerCaseLetters"] = strval($xml->command->minLowerCaseLetters);
            $getResponse["Success"]["restrictMinNonAlphanumericCharacters"] = strval($xml->command->restrictMinNonAlphanumericCharacters);
            
            $getResponse["Success"]["minNonAlphanumericCharacters"] = strval($xml->command->minNonAlphanumericCharacters);
            $getResponse["Success"]["minLength"] = strval($xml->command->minLength);
            $getResponse["Success"]["maxFailedLoginAttempts"] = strval($xml->command->maxFailedLoginAttempts);
            $getResponse["Success"]["passwordExpiresDays"] = strval($xml->command->passwordExpiresDays);
            $getResponse["Success"]["sendLoginDisabledNotifyEmail"] = strval($xml->command->sendLoginDisabledNotifyEmail);
            $getResponse["Success"]["disallowRulesModification"] = strval($xml->command->disallowRulesModification);
            
            $getResponse["Success"]["disallowPreviousPasswords"] = strval($xml->command->disallowPreviousPasswords);
            $getResponse["Success"]["numberOfPreviousPasswords"] = strval($xml->command->numberOfPreviousPasswords);
            if($ociVersion == "22"){
                $getResponse["Success"]["forcePasswordChangeAfterReset"] = strval($xml->command->forcePasswordChangeAfterReset);
            }
            
            $getResponse["Success"]["loginDisabledNotifyEmailAddress"] = strval($xml->command->loginDisabledNotifyEmailAddress);
        }
        return $getResponse;
    }
    
    function enterpriseSIPAuthenticationPasswordGetRequest($serviceProvideId){
        global $sessionid, $client;
        $getSIPResponse["Error"] = array();
        $getSIPResponse["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderSIPAuthenticationPasswordRulesGetRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
        
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $getSIPResponse["Error"] = strval($xml->command->detail);
        }else{
            $getSIPResponse["Success"]["useServiceProviderSettings"] = strval($xml->command->useServiceProviderSettings);
            $getSIPResponse["Success"]["disallowAuthenticationName"] = strval($xml->command->disallowAuthenticationName);
            //$getResponse["Success"]["disallowUserId"] = strval($xml->command->disallowUserId);
            $getSIPResponse["Success"]["disallowOldPassword"] = strval($xml->command->disallowOldPassword);
            $getSIPResponse["Success"]["disallowReversedOldPassword"] = strval($xml->command->disallowReversedOldPassword);
            $getSIPResponse["Success"]["restrictMinDigits"] = strval($xml->command->restrictMinDigits);
            $getSIPResponse["Success"]["minDigits"] = strval($xml->command->minDigits);
            $getSIPResponse["Success"]["restrictMinUpperCaseLetters"] = strval($xml->command->restrictMinUpperCaseLetters);
            $getSIPResponse["Success"]["minUpperCaseLetters"] = strval($xml->command->minUpperCaseLetters);
            $getSIPResponse["Success"]["restrictMinLowerCaseLetters"] = strval($xml->command->restrictMinLowerCaseLetters);
            $getSIPResponse["Success"]["minLowerCaseLetters"] = strval($xml->command->minLowerCaseLetters);
            $getSIPResponse["Success"]["restrictMinNonAlphanumericCharacters"] = strval($xml->command->restrictMinNonAlphanumericCharacters);
            
            $getSIPResponse["Success"]["minNonAlphanumericCharacters"] = strval($xml->command->minNonAlphanumericCharacters);
            $getSIPResponse["Success"]["minLength"] = strval($xml->command->minLength);
            $getSIPResponse["Success"]["sendPermanentLockoutNotification"] = strval($xml->command->sendPermanentLockoutNotification);
            $getSIPResponse["Success"]["permanentLockoutNotifyEmailAddress"] = strval($xml->command->permanentLockoutNotifyEmailAddress);
            
            $getSIPResponse["Success"]["endpointAuthenticationLockoutType"] = strval($xml->command->endpointAuthenticationLockoutType);
            $getSIPResponse["Success"]["endpointTemporaryLockoutThreshold"] = strval($xml->command->endpointTemporaryLockoutThreshold);
            $getSIPResponse["Success"]["endpointWaitAlgorithm"] = strval($xml->command->endpointWaitAlgorithm);
            
            $getSIPResponse["Success"]["endpointLockoutFixedMinutes"] = strval($xml->command->endpointLockoutFixedMinutes);
            $getSIPResponse["Success"]["endpointPermanentLockoutThreshold"] = strval($xml->command->endpointPermanentLockoutThreshold);
            $getSIPResponse["Success"]["trunkGroupTemporaryLockoutThreshold"] = strval($xml->command->trunkGroupTemporaryLockoutThreshold);
            $getSIPResponse["Success"]["trunkGroupAuthenticationLockoutType"] = strval($xml->command->trunkGroupAuthenticationLockoutType);
            
            $getSIPResponse["Success"]["trunkGroupWaitAlgorithm"] = strval($xml->command->trunkGroupWaitAlgorithm);
            $getSIPResponse["Success"]["trunkGroupLockoutFixedMinutes"] = strval($xml->command->trunkGroupLockoutFixedMinutes);
            $getSIPResponse["Success"]["trunkGroupPermanentLockoutThreshold"] = strval($xml->command->trunkGroupPermanentLockoutThreshold);
        }
        return $getSIPResponse;
    }
    
    function enterpriseDeviceAuthenticationPasswordRulesGetRequest($serviceProvideId){
        global $sessionid, $client;
        $getDeviceResponse["Error"] = array();
        $getDeviceResponse["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "ServiceProviderDeviceProfileAuthenticationPasswordRulesGetRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
        
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $getDeviceResponse["Error"] = strval($xml->command->detail);
        }else{
            $getDeviceResponse["Success"]["useServiceProviderSettings"] = strval($xml->command->useServiceProviderSettings);
            $getDeviceResponse["Success"]["disallowAuthenticationName"] = strval($xml->command->disallowAuthenticationName);
            $getDeviceResponse["Success"]["disallowOldPassword"] = strval($xml->command->disallowOldPassword);
            $getDeviceResponse["Success"]["disallowReversedOldPassword"] = strval($xml->command->disallowReversedOldPassword);
            $getDeviceResponse["Success"]["restrictMinDigits"] = strval($xml->command->restrictMinDigits);
            $getDeviceResponse["Success"]["minDigits"] = strval($xml->command->minDigits);
            $getDeviceResponse["Success"]["restrictMinUpperCaseLetters"] = strval($xml->command->restrictMinUpperCaseLetters);
            $getDeviceResponse["Success"]["minUpperCaseLetters"] = strval($xml->command->minUpperCaseLetters);
            $getDeviceResponse["Success"]["restrictMinLowerCaseLetters"] = strval($xml->command->restrictMinLowerCaseLetters);
            $getDeviceResponse["Success"]["minLowerCaseLetters"] = strval($xml->command->minLowerCaseLetters);
            $getDeviceResponse["Success"]["restrictMinNonAlphanumericCharacters"] = strval($xml->command->restrictMinNonAlphanumericCharacters);
            
            $getDeviceResponse["Success"]["minNonAlphanumericCharacters"] = strval($xml->command->minNonAlphanumericCharacters);
            $getDeviceResponse["Success"]["minLength"] = strval($xml->command->minLength);
            $getDeviceResponse["Success"]["sendPermanentLockoutNotification"] = strval($xml->command->sendPermanentLockoutNotification);
            $getDeviceResponse["Success"]["permanentLockoutNotifyEmailAddress"] = strval($xml->command->permanentLockoutNotifyEmailAddress);
            
            $getDeviceResponse["Success"]["deviceProfileAuthenticationLockoutType"] = strval($xml->command->deviceProfileAuthenticationLockoutType);
            $getDeviceResponse["Success"]["deviceProfileTemporaryLockoutThreshold"] = strval($xml->command->deviceProfileTemporaryLockoutThreshold);
            $getDeviceResponse["Success"]["deviceProfileWaitAlgorithm"] = strval($xml->command->deviceProfileWaitAlgorithm);
            $getDeviceResponse["Success"]["deviceProfileLockoutFixedMinutes"] = strval($xml->command->deviceProfileLockoutFixedMinutes);
            $getDeviceResponse["Success"]["deviceProfilePermanentLockoutThreshold"] = strval($xml->command->deviceProfilePermanentLockoutThreshold);
            
        }
        return $getDeviceResponse;
    }
    
    function enterprisePasswordRulesModifyRequest($serviceProvideId, $entArray){
        //print_r($entArray);
        global $sessionid, $client;
        $passwordResp["Error"] = array();
        $passwordResp["Success"] = array();
        $xmlinput = xmlHeader($sessionid, "ServiceProviderPasswordRulesModifyRequest14sp3");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
        if(!empty($entArray["rulesApplyTo"])){
            $xmlinput .= "<rulesApplyTo>" . $entArray["rulesApplyTo"] . "</rulesApplyTo>";
        }
        if(!empty($entArray["allowWebAddExternalAuthenticationUsers"])){
            $xmlinput .= "<allowWebAddExternalAuthenticationUsers>" . $entArray["allowWebAddExternalAuthenticationUsers"] . "</allowWebAddExternalAuthenticationUsers>";
        }
        if(!empty($entArray["disallowUserId"])){
            $xmlinput .= "<disallowUserId>" . $entArray["disallowUserId"] . "</disallowUserId>";
        }
        if(!empty($entArray["disallowOldPassword"])){
            $xmlinput .= "<disallowOldPassword>" . $entArray["disallowOldPassword"] . "</disallowOldPassword>";
        }
        if(!empty($entArray["disallowReversedOldPassword"])){
            $xmlinput .= "<disallowReversedOldPassword>" . $entArray["disallowReversedOldPassword"] . "</disallowReversedOldPassword>";
        }
        if(!empty($entArray["restrictMinDigits"])){
            $xmlinput .= "<restrictMinDigits>" . $entArray["restrictMinDigits"] . "</restrictMinDigits>";
        }
        if(!empty($entArray["minDigits"])){
            $xmlinput .= "<minDigits>" . $entArray["minDigits"] . "</minDigits>";
        }
        if(!empty($entArray["restrictMinUpperCaseLetters"])){
            $xmlinput .= "<restrictMinUpperCaseLetters>" . $entArray["restrictMinUpperCaseLetters"] . "</restrictMinUpperCaseLetters>";
        }
        if(!empty($entArray["minUpperCaseLetters"])){
            $xmlinput .= "<minUpperCaseLetters>" . $entArray["minUpperCaseLetters"] . "</minUpperCaseLetters>";
        }
        if(!empty($entArray["restrictMinLowerCaseLetters"])){
            $xmlinput .= "<restrictMinLowerCaseLetters>" . $entArray["restrictMinLowerCaseLetters"] . "</restrictMinLowerCaseLetters>";
        }
        
        if(!empty($entArray["minLowerCaseLetters"])){
            $xmlinput .= "<minLowerCaseLetters>" . $entArray["minLowerCaseLetters"] . "</minLowerCaseLetters>";
        }
        if(!empty($entArray["restrictMinNonAlphanumericCharacters"])){
            $xmlinput .= "<restrictMinNonAlphanumericCharacters>" . $entArray["restrictMinNonAlphanumericCharacters"] . "</restrictMinNonAlphanumericCharacters>";
        }
        if(!empty($entArray["minNonAlphanumericCharacters"])){
            $xmlinput .= "<minNonAlphanumericCharacters>" . $entArray["minNonAlphanumericCharacters"] . "</minNonAlphanumericCharacters>";
        }
        if(!empty($entArray["minLength"])){
            $xmlinput .= "<minLength>" . $entArray["minLength"] . "</minLength>";
        }
        if($entArray["maxFailedLoginAttempts"] != ""){
            $xmlinput .= "<maxFailedLoginAttempts>" . $entArray["maxFailedLoginAttempts"] . "</maxFailedLoginAttempts>";
        }
        if( $entArray["passwordExpiresDays"] != "" ){
            $xmlinput .= "<passwordExpiresDays>" . $entArray["passwordExpiresDays"] . "</passwordExpiresDays>";
        }
        if(!empty($entArray["sendLoginDisabledNotifyEmail"])){
            $xmlinput .= "<sendLoginDisabledNotifyEmail>" . $entArray["sendLoginDisabledNotifyEmail"] . "</sendLoginDisabledNotifyEmail>";
        }
        if(!empty($entArray["loginDisabledNotifyEmailAddress"])){
            $xmlinput .= "<loginDisabledNotifyEmailAddress>" . $entArray["loginDisabledNotifyEmailAddress"] . "</loginDisabledNotifyEmailAddress>";
        }
        if(!empty($entArray["disallowPreviousPasswords"])){
            $xmlinput .= "<disallowPreviousPasswords>" . $entArray["disallowPreviousPasswords"] . "</disallowPreviousPasswords>";
        }
        if(!empty($entArray["numberOfPreviousPasswords"])){
            $xmlinput .= "<numberOfPreviousPasswords>" . $entArray["numberOfPreviousPasswords"] . "</numberOfPreviousPasswords>";
        }
        if(!empty($entArray["forcePasswordChangeAfterReset"])){
            $xmlinput .= "<forcePasswordChangeAfterReset>" . $entArray["forcePasswordChangeAfterReset"] . "</forcePasswordChangeAfterReset>";
        }
        
        
        $xmlinput .= xmlFooter();
        //print_r($xmlinput);
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $passwordResp["Error"] = strval($xml->command->detail);
        }else{
            $passwordResp["Success"] = "Success";
        }
        return $passwordResp;
    }
    
    function enterpriseDevicePasswordRulesModifyRequest($serviceProvideId, $entArray){
        global $sessionid, $client;
        $passwordResp["Error"] = array();
        $passwordResp["Success"] = array();
        $xmlinput = xmlHeader($sessionid, "ServiceProviderDeviceProfileAuthenticationPasswordRulesModifyRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
        if(!empty($entArray["useServiceProviderSettings"])){
            $xmlinput .= "<useServiceProviderSettings>" . $entArray["useServiceProviderSettings"] . "</useServiceProviderSettings>";
        }
        if(!empty($entArray["disallowAuthenticationName"])){
            $xmlinput .= "<disallowAuthenticationName>" . $entArray["disallowAuthenticationName"] . "</disallowAuthenticationName>";
        }
        
        if(!empty($entArray["disallowOldPassword"])){
            $xmlinput .= "<disallowOldPassword>" . $entArray["disallowOldPassword"] . "</disallowOldPassword>";
        }
        if(!empty($entArray["disallowReversedOldPassword"])){
            $xmlinput .= "<disallowReversedOldPassword>" . $entArray["disallowReversedOldPassword"] . "</disallowReversedOldPassword>";
        }
        if(!empty($entArray["restrictMinDigits"])){
            $xmlinput .= "<restrictMinDigits>" . $entArray["restrictMinDigits"] . "</restrictMinDigits>";
        }
        if(!empty($entArray["minDigits"])){
            $xmlinput .= "<minDigits>" . $entArray["minDigits"] . "</minDigits>";
        }
        if(!empty($entArray["restrictMinUpperCaseLetters"])){
            $xmlinput .= "<restrictMinUpperCaseLetters>" . $entArray["restrictMinUpperCaseLetters"] . "</restrictMinUpperCaseLetters>";
        }
        if(!empty($entArray["minUpperCaseLetters"])){
            $xmlinput .= "<minUpperCaseLetters>" . $entArray["minUpperCaseLetters"] . "</minUpperCaseLetters>";
        }
        if(!empty($entArray["restrictMinLowerCaseLetters"])){
            $xmlinput .= "<restrictMinLowerCaseLetters>" . $entArray["restrictMinLowerCaseLetters"] . "</restrictMinLowerCaseLetters>";
        }
        
        if(!empty($entArray["minLowerCaseLetters"])){
            $xmlinput .= "<minLowerCaseLetters>" . $entArray["minLowerCaseLetters"] . "</minLowerCaseLetters>";
        }
        if(!empty($entArray["restrictMinNonAlphanumericCharacters"])){
            $xmlinput .= "<restrictMinNonAlphanumericCharacters>" . $entArray["restrictMinNonAlphanumericCharacters"] . "</restrictMinNonAlphanumericCharacters>";
        }
        if(!empty($entArray["minNonAlphanumericCharacters"])){
            $xmlinput .= "<minNonAlphanumericCharacters>" . $entArray["minNonAlphanumericCharacters"] . "</minNonAlphanumericCharacters>";
        }
        if(!empty($entArray["minLength"])){
            $xmlinput .= "<minLength>" . $entArray["minLength"] . "</minLength>";
        }
        
        if(!empty($entArray["sendPermanentLockoutNotification"])){
            $xmlinput .= "<sendPermanentLockoutNotification>" . $entArray["sendPermanentLockoutNotification"] . "</sendPermanentLockoutNotification>";
        }
        if(!empty($entArray["permanentLockoutNotifyEmailAddress"])){
            $xmlinput .= "<permanentLockoutNotifyEmailAddress>" . $entArray["permanentLockoutNotifyEmailAddress"] . "</permanentLockoutNotifyEmailAddress>";
        }
        if(!empty($entArray["deviceProfileAuthenticationLockoutType"])){
            $xmlinput .= "<deviceProfileAuthenticationLockoutType>" . $entArray["deviceProfileAuthenticationLockoutType"] . "</deviceProfileAuthenticationLockoutType>";
        }
        if(!empty($entArray["deviceProfileTemporaryLockoutThreshold"])){
            $xmlinput .= "<deviceProfileTemporaryLockoutThreshold>" . $entArray["deviceProfileTemporaryLockoutThreshold"] . "</deviceProfileTemporaryLockoutThreshold>";
        }
        if(!empty($entArray["deviceProfileWaitAlgorithm"])){
            $xmlinput .= "<deviceProfileWaitAlgorithm>" . $entArray["deviceProfileWaitAlgorithm"] . "</deviceProfileWaitAlgorithm>";
        }
        if(!empty($entArray["deviceProfileLockoutFixedMinutes"])){
            $xmlinput .= "<deviceProfileLockoutFixedMinutes>" . $entArray["deviceProfileLockoutFixedMinutes"] . "</deviceProfileLockoutFixedMinutes>";
        }
        if(!empty($entArray["deviceProfilePermanentLockoutThreshold"])){
            $xmlinput .= "<deviceProfilePermanentLockoutThreshold>" . $entArray["deviceProfilePermanentLockoutThreshold"] . "</deviceProfilePermanentLockoutThreshold>";
        }
        
        
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        if (readErrorXmlGenuine($xml) != "") {
            $passwordResp["Error"] = strval($xml->command->detail);
        }else{
            $passwordResp["Success"] = "Success";
        }
        return $passwordResp;
    }
    
    function enterpriseSIPPasswordRulesModifyRequest($serviceProvideId, $entArray){
        global $sessionid, $client;
        $passwordResp["Error"] = array();
        $passwordResp["Success"] = array();
        $xmlinput = xmlHeader($sessionid, "ServiceProviderSIPAuthenticationPasswordRulesModifyRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
        if(!empty($entArray["useServiceProviderSettings"])){
            $xmlinput .= "<useServiceProviderSettings>" . $entArray["useServiceProviderSettings"] . "</useServiceProviderSettings>";
        }
        if(!empty($entArray["disallowAuthenticationName"])){
            $xmlinput .= "<disallowAuthenticationName>" . $entArray["disallowAuthenticationName"] . "</disallowAuthenticationName>";
        }
        if(!empty($entArray["disallowOldPassword"])){
            $xmlinput .= "<disallowOldPassword>" . $entArray["disallowOldPassword"] . "</disallowOldPassword>";
        }
        
        if(!empty($entArray["disallowReversedOldPassword"])){
            $xmlinput .= "<disallowReversedOldPassword>" . $entArray["disallowReversedOldPassword"] . "</disallowReversedOldPassword>";
        }
        if(!empty($entArray["restrictMinDigits"])){
            $xmlinput .= "<restrictMinDigits>" . $entArray["restrictMinDigits"] . "</restrictMinDigits>";
        }
        if(!empty($entArray["minDigits"])){
            $xmlinput .= "<minDigits>" . $entArray["minDigits"] . "</minDigits>";
        }
        if(!empty($entArray["restrictMinUpperCaseLetters"])){
            $xmlinput .= "<restrictMinUpperCaseLetters>" . $entArray["restrictMinUpperCaseLetters"] . "</restrictMinUpperCaseLetters>";
        }
        if(!empty($entArray["minUpperCaseLetters"])){
            $xmlinput .= "<minUpperCaseLetters>" . $entArray["minUpperCaseLetters"] . "</minUpperCaseLetters>";
        }
        if(!empty($entArray["restrictMinLowerCaseLetters"])){
            $xmlinput .= "<restrictMinLowerCaseLetters>" . $entArray["restrictMinLowerCaseLetters"] . "</restrictMinLowerCaseLetters>";
        }
        
        if(!empty($entArray["minLowerCaseLetters"])){
            $xmlinput .= "<minLowerCaseLetters>" . $entArray["minLowerCaseLetters"] . "</minLowerCaseLetters>";
        }
        if(!empty($entArray["restrictMinNonAlphanumericCharacters"])){
            $xmlinput .= "<restrictMinNonAlphanumericCharacters>" . $entArray["restrictMinNonAlphanumericCharacters"] . "</restrictMinNonAlphanumericCharacters>";
        }
        if(!empty($entArray["minNonAlphanumericCharacters"])){
            $xmlinput .= "<minNonAlphanumericCharacters>" . $entArray["minNonAlphanumericCharacters"] . "</minNonAlphanumericCharacters>";
        }
        if(!empty($entArray["minLength"])){
            $xmlinput .= "<minLength>" . $entArray["minLength"] . "</minLength>";
        }
        
        if(!empty($entArray["sendPermanentLockoutNotification"])){
            $xmlinput .= "<sendPermanentLockoutNotification>" . $entArray["sendPermanentLockoutNotification"] . "</sendPermanentLockoutNotification>";
        }
        if(!empty($entArray["permanentLockoutNotifyEmailAddress"])){
            $xmlinput .= "<permanentLockoutNotifyEmailAddress>" . $entArray["permanentLockoutNotifyEmailAddress"] . "</permanentLockoutNotifyEmailAddress>";
        }
        if(!empty($entArray["endpointAuthenticationLockoutType"])){
            $xmlinput .= "<endpointAuthenticationLockoutType>" . $entArray["endpointAuthenticationLockoutType"] . "</endpointAuthenticationLockoutType>";
        }
        if(!empty($entArray["endpointTemporaryLockoutThreshold"])){
            $xmlinput .= "<endpointTemporaryLockoutThreshold>" . $entArray["endpointTemporaryLockoutThreshold"] . "</endpointTemporaryLockoutThreshold>";
        }
        if(!empty($entArray["endpointWaitAlgorithm"])){
            $xmlinput .= "<endpointWaitAlgorithm>" . $entArray["endpointWaitAlgorithm"] . "</endpointWaitAlgorithm>";
        }
        
        if(!empty($entArray["endpointLockoutFixedMinutes"])){
            $xmlinput .= "<endpointLockoutFixedMinutes>" . $entArray["endpointLockoutFixedMinutes"] . "</endpointLockoutFixedMinutes>";
        }
        
        if(!empty($entArray["endpointPermanentLockoutThreshold"])){
            $xmlinput .= "<endpointPermanentLockoutThreshold>" . $entArray["endpointPermanentLockoutThreshold"] . "</endpointPermanentLockoutThreshold>";
        }
        
        if(!empty($entArray["trunkGroupAuthenticationLockoutType"])){
            $xmlinput .= "<trunkGroupAuthenticationLockoutType>" . $entArray["trunkGroupAuthenticationLockoutType"] . "</trunkGroupAuthenticationLockoutType>";
        }
        
        if(!empty($entArray["trunkGroupTemporaryLockoutThreshold"])){
            $xmlinput .= "<trunkGroupTemporaryLockoutThreshold>" . $entArray["trunkGroupTemporaryLockoutThreshold"] . "</trunkGroupTemporaryLockoutThreshold>";
        }
        if(!empty($entArray["trunkGroupWaitAlgorithm"])){
            $xmlinput .= "<trunkGroupWaitAlgorithm>" . $entArray["trunkGroupWaitAlgorithm"] . "</trunkGroupWaitAlgorithm>";
        }
        if(!empty($entArray["trunkGroupLockoutFixedMinutes"])){
            $xmlinput .= "<trunkGroupLockoutFixedMinutes>" . $entArray["trunkGroupLockoutFixedMinutes"] . "</trunkGroupLockoutFixedMinutes>";
        }
        if(!empty($entArray["trunkGroupPermanentLockoutThreshold"])){
            $xmlinput .= "<trunkGroupPermanentLockoutThreshold>" . $entArray["trunkGroupPermanentLockoutThreshold"] . "</trunkGroupPermanentLockoutThreshold>";
        }
        
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $passwordResp["Error"] = strval($xml->command->detail);
        }else{
            $passwordResp["Success"] = "Success";
        }
        return $passwordResp;
    }

	public function ServiceProviderServicePackGetServiceUsageListRequest($serviceProviderId, $serviceName) {
			
			global  $sessionid, $client;
			$res["Error"] = array();
			$res["Success"] = array();
			
			$xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetServiceUsageListRequest");
			$xmlinput .= "<serviceProviderId>".$serviceProviderId."</serviceProviderId>";
			$xmlinput .= "<serviceName>".$serviceName."</serviceName>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if (readErrorXmlGenuine($xml) != "") {
				$res["Error"] = strval($xml->command->summaryEnglish);
			} else {
				$serviceUsageTable = $xml->command->serviceUsageTable;
				$tempmArray = array();
				
				$count = 0;
				foreach($serviceUsageTable->row as $key => $value) {
					$tempmArray[$count][strval($serviceUsageTable->colHeading[0])] = strval($value->col[0]);
					$tempmArray[$count][strval($serviceUsageTable->colHeading[1])] = strval($value->col[1]);
					$tempmArray[$count][strval($serviceUsageTable->colHeading[2])] = strval($value->col[2]);
					
					$count++;
				}
				
				$res["Success"] = $tempmArray;
			}
			
			return $res;
	}
	
	public function serviceProviderAdminGetListRequest($spId) {
	    global  $sessionid, $client;
	    $res["Error"] = array();
	    $res["Success"] = array();
	    $adminData = array();
	    
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderAdminGetListRequest14");
	    $xmlinput .= "<serviceProviderId>".$spId."</serviceProviderId>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    
	    if (readErrorXmlGenuine($xml) != "") {
	        $res["Error"] = strval($xml->command->summaryEnglish);
	    } else {
	        $i = 0;
	        foreach($xml->command->serviceProviderAdminTable->row as $adminKey => $adminVal) {
	            $adminData[$i]['administratorId'] = strval($adminVal->col[0]);
	            $adminData[$i]['lastName'] = strval($adminVal->col[1]);
	            $adminData[$i]['firstName'] = strval($adminVal->col[2]);
	            $adminData[$i]['administratorType'] = strval($adminVal->col[3]);
	            $adminData[$i]['language'] = strval($adminVal->col[4]);
	            
	            $i++;
	        }
	        $res["Success"] = $adminData;
	    }
	    
	    return $res;
	}
	
	public function serviceProviderAdminAddRequest($spId, $adminData) {
	    global  $sessionid, $client;
	    $res["Error"] = array();
	    $res["Success"] = array();
	    
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderAdminAddRequest14");
	    $xmlinput .= "<serviceProviderId>".$spId."</serviceProviderId>";
	    $xmlinput .= "<userId>".$adminData['administratorId']."</userId>";
	    if( isset($adminData['firstName']) ) {
	        $xmlinput .= "<firstName>".$adminData['firstName']."</firstName>";
	    }
	    if( isset($adminData['lastName']) ) {
	        $xmlinput .= "<lastName>".$adminData['lastName']."</lastName>";
	    }
	    if( isset($adminData['password']) && $adminData['password'] != "" ) {
	        $xmlinput .= "<password>".$adminData['password']."</password>";
	    }
	    if( isset($adminData['language']) && $adminData['language'] != "" ) {
	        $xmlinput .= "<language>".$adminData['language']."</language>";
	    }
	    $xmlinput .= "<administratorType>".$adminData['administratorType']."</administratorType>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    
	    if (readErrorXmlGenuine($xml) != "") {
	        $res["Error"] = strval($xml->command->summaryEnglish);
	    } else {
	        $res["Success"] = "Success";
	    }
	    
	    return $res;
	}
	
	public function serviceProviderAdminModifyRequest($spId, $adminData) {
	    global  $sessionid, $client;
	    $res["Error"] = array();
	    $res["Success"] = array();
	    
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderAdminModifyRequest");
	    $xmlinput .= "<userId>".$adminData['administratorId']."</userId>";
	    if( isset($adminData['firstName']) ) {
	        $xmlinput .= "<firstName>".htmlspecialchars($adminData['firstName'])."</firstName>";
	    }
	    if( isset($adminData['lastName']) ) {
	        $xmlinput .= "<lastName>".htmlspecialchars($adminData['lastName'])."</lastName>";
	    }
	    if( isset($adminData['password']) && $adminData['password'] != "" ) {
	        $xmlinput .= "<password>".htmlspecialchars($adminData['password'])."</password>";
	    }
	    if( isset($adminData['language']) && $adminData['language'] != "" ) {
	        $xmlinput .= "<language>".htmlspecialchars($adminData['language'])."</language>";
	    }
	    $xmlinput .= xmlFooter();

	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    
	    if (readErrorXmlGenuine($xml) != "") {
	        $res["Error"] = strval($xml->command->summaryEnglish);
	    } else {
	        $res["Success"] = "Success";
	    }
	    
	    return $res;
	}
	
	public function serviceProviderAdminDeleteRequest($administratorId) {
	    global  $sessionid, $client;
	    $res["Error"] = array();
	    $res["Success"] = array();
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderAdminDeleteRequest");
	    $xmlinput .= "<userId>".$administratorId."</userId>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    
	    if (readErrorXmlGenuine($xml) != "") {
	        $res["Error"] = strval($xml->command->summaryEnglish);
	    } else {
	        $res["Success"] = "Success";
	    }
	    
	    return $res;
	}
	
	public function ServiceProviderDialPlanPolicyGetAccessCodeListRequest($serviceProvideID){
	    global  $sessionid, $client;
	    $res["Error"] = array();
	    $res["Success"] = array();
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderDialPlanPolicyGetAccessCodeListRequest");
	    $xmlinput .= "<serviceProviderId>".htmlspecialchars($serviceProvideID)."</serviceProviderId>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    //print_r($xml);
	    if (readErrorXmlGenuine($xml) != "") {
	        $res["Error"] = strval($xml->command->summaryEnglish);
	    } else {
	        if(count($xml->command->accessCodeTable->row) > 0){
	            $i = 0;
	            foreach ($xml->command->accessCodeTable->row as $key => $value){
	                $res["Success"][$i]["accessCode"] = strval($value->col[0]);
	                $res["Success"][$i]["enableSecondaryDialTone"] = strval($value->col[1]);
	                $res["Success"][$i]["description"] = strval($value->col[2]);
	                $i++;
	            }
	        }
	    }
	    
	    return $res;
	}
	
	public function serviceProviderDialPlanPolicyGetAccessCodeRequest($serviceProvideID, $accessCode){
	    global  $sessionid, $client;
	    $res["Error"] = array();
	    $res["Success"] = array();
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderDialPlanPolicyGetAccessCodeRequest");
	    $xmlinput .= "<serviceProviderId>".htmlspecialchars($serviceProvideID)."</serviceProviderId>";
	    $xmlinput .= "<accessCode>".htmlspecialchars($accessCode)."</accessCode>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    //print_r($xml);
	    if (readErrorXmlGenuine($xml) != "") {
	        $res["Error"] = strval($xml->command->summaryEnglish);
	    } else {
	        
	        $res["Success"]["accessCode"] = $accessCode;
	        $res["Success"]["enableSecondaryDialTone"] = strval($xml->command->enableSecondaryDialTone);
	        $res["Success"]["description"] = strval($xml->command->description);
	        $res["Success"]["includeCodeForNetworkTranslationsAndRouting"] = strval($xml->command->includeCodeForNetworkTranslationsAndRouting);
	        $res["Success"]["includeCodeForScreeningServices"] = strval($xml->command->includeCodeForScreeningServices);
	             
	    }
	    
	    return $res;
	}
	
	public function ServiceProviderDialPlanPolicyAddAccessCodeRequest($spId, $dialPlanData) {
	    global  $sessionid, $client;
	    $res["Error"] = array();
	    $res["Success"] = array();
	    
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderDialPlanPolicyAddAccessCodeRequest");
	    $xmlinput .= "<serviceProviderId>".htmlspecialchars($spId)."</serviceProviderId>";
	    $xmlinput .= "<accessCode>".$dialPlanData["accessCode"]."</accessCode>";
	    $xmlinput .= "<includeCodeForNetworkTranslationsAndRouting>".$dialPlanData["includeCodeForNetworkTranslationsAndRouting"]."</includeCodeForNetworkTranslationsAndRouting>";
	    $xmlinput .= "<includeCodeForScreeningServices>".$dialPlanData["includeCodeForScreeningServices"]."</includeCodeForScreeningServices>";
	    $xmlinput .= "<enableSecondaryDialTone>".$dialPlanData["enableSecondaryDialTone"]."</enableSecondaryDialTone>";
	    
	    if( isset($dialPlanData['description']) && !empty($dialPlanData['description'])) {
	        $xmlinput .= "<description>".htmlspecialchars($dialPlanData['description'])."</description>";
	    }
	    $xmlinput .= xmlFooter();
	    //print_r($xmlinput);
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    //print_r($xml);
	    if (readErrorXmlGenuine($xml) != "") {
	        $res["Error"] = strval($xml->command->summaryEnglish);
	    } else {
	        $res["Success"] = "Success";
	    }
	    
	    return $res;
	}
	
	public function serviceProviderDialPlanPolicyGetRequest($serviceProvideID){
	    global  $sessionid, $client;
	    $res["Error"] = array();
	    $res["Success"] = array();
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderDialPlanPolicyGetRequest17");
	    $xmlinput .= "<serviceProviderId>".htmlspecialchars($serviceProvideID)."</serviceProviderId>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    //print_r($xml);
	    if (readErrorXmlGenuine($xml) != "") {
	        $res["Error"] = strval($xml->command->summaryEnglish);
	    } else {
	        $res["Success"]["requiresAccessCodeForPublicCalls"] = strval($xml->command->requiresAccessCodeForPublicCalls);
	        $res["Success"]["allowE164PublicCalls"] = strval($xml->command->allowE164PublicCalls);
	        $res["Success"]["preferE164NumberFormatForCallbackServices"] = strval($xml->command->preferE164NumberFormatForCallbackServices);
	        $res["Success"]["publicDigitMap"] = strval($xml->command->publicDigitMap);
	        $res["Success"]["privateDigitMap"] = strval($xml->command->privateDigitMap);
	    }
	    
	    return $res;
	}
	
	public function serviceProviderDialPlanPolicyModifyRequest($spId, $dialPlanData) {
	    global  $sessionid, $client;
	    $res["Error"] = array();
	    $res["Success"] = array();
	    
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderDialPlanPolicyModifyRequest");
	    $xmlinput .= "<serviceProviderId>".htmlspecialchars($spId)."</serviceProviderId>";
	    if( isset($dialPlanData['requiresAccessCodeForPublicCalls']) && !empty($dialPlanData['requiresAccessCodeForPublicCalls'])) {
	        $xmlinput .= "<requiresAccessCodeForPublicCalls>".$dialPlanData["requiresAccessCodeForPublicCalls"]."</requiresAccessCodeForPublicCalls>";
	    }
	    if( isset($dialPlanData['allowE164PublicCalls']) && !empty($dialPlanData['allowE164PublicCalls'])) {
	       $xmlinput .= "<allowE164PublicCalls>".$dialPlanData["allowE164PublicCalls"]."</allowE164PublicCalls>";
	    }
	    if( isset($dialPlanData['preferE164NumberFormatForCallbackServices']) && !empty($dialPlanData['preferE164NumberFormatForCallbackServices'])) {
	       $xmlinput .= "<preferE164NumberFormatForCallbackServices>".$dialPlanData["preferE164NumberFormatForCallbackServices"]."</preferE164NumberFormatForCallbackServices>";
	    }
	    if( isset($dialPlanData['publicDigitMap']) && !empty($dialPlanData['publicDigitMap'])) {
	        $xmlinput .= "<publicDigitMap>".$dialPlanData["publicDigitMap"]."</publicDigitMap>";
	    }else if(isset($dialPlanData['publicDigitMap']) && empty($dialPlanData['publicDigitMap'])){
	        $xmlinput .= "<publicDigitMap xsi:nil='true'></publicDigitMap>";
	    }
	    
	    if( isset($dialPlanData['privateDigitMap']) && !empty($dialPlanData['privateDigitMap'])) {
	        $xmlinput .= "<privateDigitMap>".$dialPlanData["privateDigitMap"]."</privateDigitMap>";
	    }else if(isset($dialPlanData['privateDigitMap']) && empty($dialPlanData['privateDigitMap'])){
	        $xmlinput .= "<privateDigitMap xsi:nil='true'></privateDigitMap>";
	    }
	   
	    $xmlinput .= xmlFooter();
	    //print_r($xmlinput);
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    //print_r($xml);
	    if (readErrorXmlGenuine($xml) != "") {
	        $res["Error"] = strval($xml->command->summaryEnglish);
	    } else {
	        $res["Success"] = "Success";
	    }
	    
	    return $res;
	}
	
	public function serviceProviderDialPlanPolicyDeleteAccessCodeRequest($serviceProviderId, $accessCode) {
	    global  $sessionid, $client;
	    $res["Error"] = array();
	    $res["Success"] = array();
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderDialPlanPolicyDeleteAccessCodeRequest");
	    $xmlinput .= "<serviceProviderId>".$serviceProviderId."</serviceProviderId>";
	    $xmlinput .= "<accessCode>".$accessCode."</accessCode>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    
	    if (readErrorXmlGenuine($xml) != "") {
	        $res["Error"] = strval($xml->command->summaryEnglish);
	    } else {
	        $res["Success"] = "Success";
	    }
	    
	    return $res;
	}
	
	public function serviceProviderDialPlanPolicyModifyAccessCodeRequest($spId, $accessCode, $dialPlanData) {
	    global  $sessionid, $client;
	    $res["Error"] = array();
	    $res["Success"] = array();
	    
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderDialPlanPolicyModifyAccessCodeRequest");
	    $xmlinput .= "<serviceProviderId>".htmlspecialchars($spId)."</serviceProviderId>";
	    $xmlinput .= "<accessCode>".htmlspecialchars($accessCode)."</accessCode>";
	    if(isset($dialPlanData["includeCodeForNetworkTranslationsAndRouting"])){
	        $xmlinput .= "<includeCodeForNetworkTranslationsAndRouting>".$dialPlanData["includeCodeForNetworkTranslationsAndRouting"]."</includeCodeForNetworkTranslationsAndRouting>";
	    }
	    if(isset($dialPlanData["includeCodeForScreeningServices"])){
	        $xmlinput .= "<includeCodeForScreeningServices>".$dialPlanData["includeCodeForScreeningServices"]."</includeCodeForScreeningServices>";
	    }
	    if(isset($dialPlanData["enableSecondaryDialTone"])){
	        $xmlinput .= "<enableSecondaryDialTone>".$dialPlanData["enableSecondaryDialTone"]."</enableSecondaryDialTone>";
	    }
	    
	    if( isset($dialPlanData['description']) && !empty($dialPlanData['description'])) {
	        $xmlinput .= "<description>".htmlspecialchars($dialPlanData['description'])."</description>";
	    }else if(isset($dialPlanData['description']) && empty($dialPlanData['description'])){
	        $xmlinput .= "<description xsi:nil='true'></description>";
	    }
	    $xmlinput .= xmlFooter();
	    //print_r($xmlinput);
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    //print_r($xml);
	    if (readErrorXmlGenuine($xml) != "") {
	        $res["Error"] = strval($xml->command->summaryEnglish);
	    } else {
	        $res["Success"] = "Success";
	    }
	    
	    return $res;
	}
        
        //Code added @07 June 2019
        public function getEnterpriseVoicePortalRequest($serviceProviderId){
                global  $sessionid, $client;
                $voicePortalResp["Error"] = array();
                $voicePortalResp["Success"] = array();

                $xmlinput = xmlHeader($sessionid, "ServiceProviderVoiceMessagingGroupGetVoicePortalRequest");
                $xmlinput .= "<serviceProviderId>".$serviceProviderId."</serviceProviderId>";

                $xmlinput .= xmlFooter();
                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                //echo "<pre>XML VOICE PORTAL - ";
                //print_r($xml);
                if (readErrorXmlGenuine($xml) != "") {
                    $voicePortalResp["Error"] = strval($xml->command->summaryEnglish);
                }else{
                    $voicePortalResp["Success"]["voicePortalScope"] = strval($xml->command->voicePortalScope);
                }
                return $voicePortalResp;
         }
         public function serviceProviderVoicePortalModifyRequest($voicePortalArr){
                global  $sessionid, $client;
                $voicePortalResp["Error"] = array();
                $voicePortalResp["Success"] = array();
                
                $serviceProviderId = $voicePortalArr['serviceProviderId'];
                $voicePortalScope  = $voicePortalArr['voicePortalScope'];
                if($voicePortalScope == "Enterprise"){
                    $voicePortalScope = "Service Provider";
                }

                $xmlinput = xmlHeader($sessionid, "ServiceProviderVoiceMessagingGroupModifyVoicePortalRequest");
                $xmlinput .= "<serviceProviderId>".$serviceProviderId."</serviceProviderId>";
                $xmlinput .= "<voicePortalScope>".$voicePortalScope."</voicePortalScope>";
                $xmlinput .= xmlFooter();
                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                
                if (readErrorXmlGenuine($xml) != "") {
                    $voicePortalResp["Error"] = strval($xml->command->summaryEnglish);
                }else{
                    $voicePortalResp["Success"]["voicePortalScope"] = strval($xml->command->voicePortalScope);
                }
                return $voicePortalResp;
         }
          public function getContextForVoicePortalRequest($ociVersion){
                global  $sessionid, $client;
                $voicePortalResp["Error"] = array();
                $voicePortalResp["Success"] = array();
                
                if ($ociVersion == "20"){
			$xmlinput = xmlHeader($sessionid, "SystemVoiceMessagingGroupGetRequest20");
		}else{
			$xmlinput = xmlHeader($sessionid, "SystemVoiceMessagingGroupGetRequest21");
		}
                $xmlinput .= xmlFooter();
                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                //echo "<pre>XML VOICE PORTAL CONTEXT - ";
                //print_r($xml);
                if (readErrorXmlGenuine($xml) != "") {
                    $voicePortalResp["Error"] = strval($xml->command->summaryEnglish);
                }else{
                    $voicePortalResp["Success"]["voicePortalScope"] = strval($xml->command->voicePortalScope);
                }
                return $voicePortalResp;
         }
        //End code
        
         public function addDomainsToSystem($domainName) {
             global  $sessionid, $client;
             $res["Error"] = array();
             $res["Success"] = array();
             $xmlinput = xmlHeader($sessionid, "SystemDomainAddRequest");
             $xmlinput .= "<domain>".$domainName."</domain>";
             $xmlinput .= xmlFooter();
             $response = $client->processOCIMessage(array("in0" => $xmlinput));
             $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
             
             if (readErrorXmlGenuine($xml) != "") {
                 $res["Error"] = strval($xml->command->summaryEnglish);
             } else {
                 $res["Success"] = "Success";
             }
             
             return $res;
         }
         
         public function serviceProviderServiceModifyAuthorizationListRequestClone($spId, $serviceType, $modArray) {
             global  $sessionid, $client;
             $resp["Error"] = array();
             $resp["Success"] = array();
             
             $xmlinput = xmlHeader($sessionid, "ServiceProviderServiceModifyAuthorizationListRequest");
             $xmlinput .= "<serviceProviderId>".$spId."</serviceProviderId>";
             
             foreach($modArray as $grpKey => $grpValue) {
                $xmlinput .= "<" .$serviceType. ">";
                 $xmlinput .= "<serviceName>" . $grpKey . "</serviceName>";
                     $xmlinput .= "<authorizedQuantity>";
                     if( $grpValue['limited'] == "Unlimited" ) {
                         $xmlinput .= "<unlimited>" . "true" . "</unlimited>";
                     } else if( isset($grpValue['quantity']) && $grpValue['quantity'] != "" ) {
                         $xmlinput .= "<quantity>" . $grpValue['quantity'] . "</quantity>";
                     }
                     $xmlinput .= "</authorizedQuantity>";
                 $xmlinput .= "</" . $serviceType . ">";
             }
             $xmlinput .= xmlFooter();
             //print_r($xmlinput);
             $response = $client->processOCIMessage(array("in0" => $xmlinput));
             $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
             if (readErrorXmlGenuine($xml) != "") {
                 $resp["Error"] = strval($xml->command->summaryEnglish);
             } else {
                 $resp["Success"] = "Success";
             }
             
             return $resp;
         }
         
         function enterprisePasscodeRulesGetRequest($serviceProvideId){
             global $sessionid, $client;
             $getResponse["Error"] = array();
             $getResponse["Success"] = array();
             
             $xmlinput = xmlHeader($sessionid, "ServiceProviderPortalPasscodeRulesGetRequest19");
             $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
             
             $xmlinput .= xmlFooter();
             
             $response = $client->processOCIMessage(array("in0" => $xmlinput));
             $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
             //print_r($xml);
             if (readErrorXmlGenuine($xml) != "") {
                 $getResponse["Error"] = strval($xml->command->detail);
             }else{
                 $getResponse["Success"]["disallowRepeatedDigits"] = strval($xml->command->disallowRepeatedDigits);
                 $getResponse["Success"]["numberOfRepeatedDigits"] = strval($xml->command->numberOfRepeatedDigits);
                 $getResponse["Success"]["disallowRepeatedPatterns"] = strval($xml->command->disallowRepeatedPatterns);
                 $getResponse["Success"]["disallowContiguousSequences"] = strval($xml->command->disallowContiguousSequences);
                 $getResponse["Success"]["numberOfAscendingDigits"] = strval($xml->command->numberOfAscendingDigits);
                 $getResponse["Success"]["numberOfDescendingDigits"] = strval($xml->command->numberOfDescendingDigits);
                 $getResponse["Success"]["disallowUserNumber"] = strval($xml->command->disallowUserNumber);
                 $getResponse["Success"]["disallowReversedUserNumber"] = strval($xml->command->disallowReversedUserNumber);
                 $getResponse["Success"]["disallowOldPasscode"] = strval($xml->command->disallowOldPasscode);
                 $getResponse["Success"]["numberOfPreviousPasscodes"] = strval($xml->command->numberOfPreviousPasscodes);
                 $getResponse["Success"]["disallowReversedOldPasscode"] = strval($xml->command->disallowReversedOldPasscode);
                 $getResponse["Success"]["minCodeLength"] = strval($xml->command->minCodeLength);
                 
                 $getResponse["Success"]["maxCodeLength"] = strval($xml->command->maxCodeLength);
                 $getResponse["Success"]["disableLoginAfterMaxFailedLoginAttempts"] = strval($xml->command->disableLoginAfterMaxFailedLoginAttempts);
                 $getResponse["Success"]["maxFailedLoginAttempts"] = strval($xml->command->maxFailedLoginAttempts);
                 $getResponse["Success"]["expirePassword"] = strval($xml->command->expirePassword);
                 $getResponse["Success"]["passcodeExpiresDays"] = strval($xml->command->passcodeExpiresDays);
                 
                 $getResponse["Success"]["sendLoginDisabledNotifyEmail"] = strval($xml->command->sendLoginDisabledNotifyEmail);
                 $getResponse["Success"]["loginDisabledNotifyEmailAddress"] = strval($xml->command->loginDisabledNotifyEmailAddress);
                 
             }
             return $getResponse;
         }
         
         function enterprisePasscodeRulesModifyRequest($serviceProvideId, $entArray){
             //print_r($entArray);
             global $sessionid, $client;
             $passwordResp["Error"] = array();
             $passwordResp["Success"] = array();
             $xmlinput = xmlHeader($sessionid, "ServiceProviderPortalPasscodeRulesModifyRequest");
             $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvideId) . "</serviceProviderId>";
             
             if(!empty($entArray["disallowRepeatedDigits"])){
                 $xmlinput .= "<disallowRepeatedDigits>" . $entArray["disallowRepeatedDigits"] . "</disallowRepeatedDigits>";
             }
             if(!empty($entArray["numberOfRepeatedDigits"])){
                 $xmlinput .= "<numberOfRepeatedDigits>" . $entArray["numberOfRepeatedDigits"] . "</numberOfRepeatedDigits>";
             }
             if(!empty($entArray["disallowRepeatedPatterns"])){
                 $xmlinput .= "<disallowRepeatedPatterns>" . $entArray["disallowRepeatedPatterns"] . "</disallowRepeatedPatterns>";
             }
             if(!empty($entArray["disallowContiguousSequences"])){
                 $xmlinput .= "<disallowContiguousSequences>" . $entArray["disallowContiguousSequences"] . "</disallowContiguousSequences>";
             }
             if(!empty($entArray["numberOfAscendingDigits"])){
                 $xmlinput .= "<numberOfAscendingDigits>" . $entArray["numberOfAscendingDigits"] . "</numberOfAscendingDigits>";
             }
             if(!empty($entArray["numberOfDescendingDigits"])){
                 $xmlinput .= "<numberOfDescendingDigits>" . $entArray["numberOfDescendingDigits"] . "</numberOfDescendingDigits>";
             }
             if(!empty($entArray["disallowUserNumber"])){
                 $xmlinput .= "<disallowUserNumber>" . $entArray["disallowUserNumber"] . "</disallowUserNumber>";
             }
             if(!empty($entArray["disallowReversedUserNumber"])){
                 $xmlinput .= "<disallowReversedUserNumber>" . $entArray["disallowReversedUserNumber"] . "</disallowReversedUserNumber>";
             }
             if(!empty($entArray["disallowOldPasscode"])){
                 $xmlinput .= "<disallowOldPasscode>" . $entArray["disallowOldPasscode"] . "</disallowOldPasscode>";
             }
             
             if(!empty($entArray["numberOfPreviousPasscodes"])){
                 $xmlinput .= "<numberOfPreviousPasscodes>" . $entArray["numberOfPreviousPasscodes"] . "</numberOfPreviousPasscodes>";
             }
             if(!empty($entArray["disallowReversedOldPasscode"])){
                 $xmlinput .= "<disallowReversedOldPasscode>" . $entArray["disallowReversedOldPasscode"] . "</disallowReversedOldPasscode>";
             }
             if(!empty($entArray["minCodeLength"])){
                 $xmlinput .= "<minCodeLength>" . $entArray["minCodeLength"] . "</minCodeLength>";
             }
             if(!empty($entArray["maxCodeLength"])){
                 $xmlinput .= "<maxCodeLength>" . $entArray["maxCodeLength"] . "</maxCodeLength>";
             }
             if($entArray["disableLoginAfterMaxFailedLoginAttempts"] != ""){
                 $xmlinput .= "<disableLoginAfterMaxFailedLoginAttempts>" . $entArray["disableLoginAfterMaxFailedLoginAttempts"] . "</disableLoginAfterMaxFailedLoginAttempts>";
             }
             if( $entArray["maxFailedLoginAttempts"] != "" ){
                 $xmlinput .= "<maxFailedLoginAttempts>" . $entArray["maxFailedLoginAttempts"] . "</maxFailedLoginAttempts>";
             }
             if(!empty($entArray["expirePassword"])){
                 $xmlinput .= "<expirePassword>" . $entArray["expirePassword"] . "</expirePassword>";
             }
             if(!empty($entArray["passcodeExpiresDays"])){
                 $xmlinput .= "<passcodeExpiresDays>" . $entArray["passcodeExpiresDays"] . "</passcodeExpiresDays>";
             }
             if(!empty($entArray["sendLoginDisabledNotifyEmail"])){
                 $xmlinput .= "<sendLoginDisabledNotifyEmail>" . $entArray["sendLoginDisabledNotifyEmail"] . "</sendLoginDisabledNotifyEmail>";
             }
             if(!empty($entArray["loginDisabledNotifyEmailAddress"])){
                 $xmlinput .= "<loginDisabledNotifyEmailAddress>" . $entArray["loginDisabledNotifyEmailAddress"] . "</loginDisabledNotifyEmailAddress>";
             }
             
             
             $xmlinput .= xmlFooter();
             //print_r($xmlinput);
             $response = $client->processOCIMessage(array("in0" => $xmlinput));
             $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
             //print_r($xml);
             if (readErrorXmlGenuine($xml) != "") {
                 $passwordResp["Error"] = strval($xml->command->detail);
             }else{
                 $passwordResp["Success"] = "Success";
             }
             return $passwordResp;
         }
}
?>