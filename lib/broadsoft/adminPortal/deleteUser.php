<?php

    function getNumberOfAssignedPorts($device) {
        global $ociVersion, $sessionid, $client;

        $ociCmd = "GroupAccessDeviceGetRequest18sp1";
        $xmlinput = xmlHeader($sessionid, $ociCmd);
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $device . "</deviceName>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        return strval($xml->command->numberOfAssignedPorts);
    }

    function deleteDeviceOnUserRemoval($deviceType) {
        global $db, $deleteAnalogUserDevice;

	$sipGatewayLookup = new DBLookup($db, "systemDevices", "deviceType", "phoneType");
	$deviceIsAnalog = $sipGatewayLookup->get($deviceType) == "Analog";

	// short-circuit for Audio-Codes
	$deviceIsAudioCodes = substr($deviceType, 0, strlen("AudioCodes-")) == "AudioCodes-";

	if ($deviceIsAnalog || $deviceIsAudioCodes) {
		return $deleteAnalogUserDevice == "true";
	}

	// device is not for analog user
	return true;
    }

    include("/var/www/lib/broadsoft/adminPortal/getUserAssignedUserServices.php");
    include("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
    require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
    require_once("/var/www/lib/broadsoft/adminPortal/DeviceOperations.php");
    $deviceObjSoftPhone = new DeviceOperations();
    require_once("/var/www/lib/broadsoft/adminPortal/counterPathStretto/CPSOperations.php");
    $cpsobj = new CPSOperations();
    // Check for Voice Messaging User service assignment
    $userServices = getUserAssignedUserServices($userId);

    //delete Voice Mailbox account from SurgeMail Server
    if ($policiesSupportDeleteVMOnUserDel == "true" && in_array("Voice Messaging User", $userServices)) {
        $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetAdvancedVoiceManagementRequest14sp3");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        readError($xml);

        $groupMailServerUserId = isset($xml->command->groupMailServerUserId) ? strval($xml->command->groupMailServerUserId) : "";
        if ($groupMailServerUserId != "") {
            $mailboxPassword = setEmailPassword();

            $surgemailPost = array ("cmd" => "cmd_user_login",
                "lcmd" => "user_delete",
                "show" => "simple_msg.xml",
                "username" => $surgemailUsername,
                "password" => $surgemailPassword,
                "lusername" => $groupMailServerUserId,
                "lpassword" => $mailboxPassword,
                "uid" => isset($userid) ? $userid : "");
            $result = http_post_fields($surgemailURL, $surgemailPost);
        }
    }

	//delete user
	$xmlinput = xmlHeader($sessionid, "UserDeleteRequest");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	readError($xml);

	//delete device
    if (isset($_SESSION["userInfo"]["deviceName"]))
    {
        $deviceName = $_SESSION["userInfo"]["deviceName"];
        if ($deviceName != "" && getNumberOfAssignedPorts($deviceName) == 0 && deleteDeviceOnUserRemoval($_SESSION["userInfo"]["deviceType"]))
        {
            $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceDeleteRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
            $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
            $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            readError($xml);
        }
    }
    if(count($_SESSION["counterPathDevices"]) > 0){
        foreach ($_SESSION["counterPathDevices"] as $dKey => $dVal){
            $deviceDeleteResp[$dVal["deviceName"]] = $deviceObjSoftPhone->getDeleteDevice($dVal["deviceName"], $_SESSION["sp"], $_SESSION["groupId"]);
            if(isset($dVal["accountUserName"]) && $dVal["accountUserName"] !=""){
                $arrayToProcess["cpsArray"]["userName"] = $dVal["accountUserName"];
                $cpsResp[$dVal["accountUserName"]] = $cpsobj->counterPathAccountDelete($arrayToProcess);
            }
        }
    }
    if(isset($deviceDeleteResp) && !empty($deviceDeleteResp)){
        foreach ($deviceDeleteResp as $respKey => $respVal){
            if($respVal["Error"] !=""){
                print_r($respKey);echo ": ";print_r($respVal["Error"]); echo "<br />";
            }
        }
    }
    if(isset($cpsResp) && !empty($cpsResp)){
        foreach ($cpsResp as $cpKey => $cpVal){
            if($cpVal == "false"){
                echo $cpKey." : Counterpath account not deleted successfully. <br />";
            }
        }
    }
?>
