<?php
class AaOperation{
	
	function getDepartmentList($sp, $groupId){
		global $sessionid, $client;
		$deptArr["Error"]="";
		$deptArr["Success"]="";
		$xmlinput = xmlHeader($sessionid, "GroupDepartmentGetListRequest18");
		$xmlinput .= "<serviceProviderId>". htmlspecialchars($sp) ."</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId)."</groupId>";
		$xmlinput .= "<includeEnterpriseDepartments>true</includeEnterpriseDepartments>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$deptArr["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			if(isset($xml->command->departmentTable->row) && count($xml->command->departmentTable->row) > 0){
				$i=0;
				foreach ($xml->command->departmentTable->row as $key => $value){
					$deptArr["Success"]["department"][$i]["name"] = strval($value->col[1]);
					$deptArr["Success"]["department"][$i]["fpName"] = strval($value->col[2]);
					$deptArr["Success"]["department"][$i]["callingLineIdNameDepartment"] = strval($value->col[3]);
					$deptArr["Success"]["department"][$i]["callingLineIdPhoneNumberDepartment"] = strval($value->col[4]);
					$i++;
				}
			}
		}
		return $deptArr;
	}
	
	function getTimeZoneList($ociVersion){
		global $sessionid, $client;
		$timeZoneResponse["Error"] = "";
		$timeZoneResponse["Success"] = "";
		
		if ($ociVersion == "20")
		{
			$xmlinput = xmlHeader($sessionid, "SystemTimeZoneGetListRequest20");
		}
		else
		{
			$xmlinput = xmlHeader($sessionid, "SystemTimeZoneGetListRequest");
		}
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$timeZoneResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			//echo "<pre>"; print_r($xml->command);
			$a = 0;
			foreach ($xml->command->timeZoneTable->row as $key => $value)
			{
				$timeZones[$a]['name'] = strval($value->col[0]);
				$timeZones[$a]['displayName'] = strval($value->col[1]);
				$a++;
			}
			$timeZoneResponse["Success"] = $timeZones;
		}
		
		return $timeZoneResponse;
	}
	
	function getAllScheduleData($sp, $groupId){
		global $sessionid, $client;
		$allSchedule["Error"] = "";
		$allSchedule["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupScheduleGetListRequest17sp1");
		$xmlinput .= "<serviceProviderId>". htmlspecialchars($sp) ."</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$allSchedule["Error"] = $xml->command->summaryEnglish;
		}else{
			if(count($xml->command->scheduleGlobalKey) > 0 && !empty($xml->command->scheduleGlobalKey)){
				$i = 0;$j = 0;
				foreach ($xml->command->scheduleGlobalKey as $key => $value){
					if(strval($value->scheduleKey->scheduleType) == "Time"){
						$allSchedule["Success"]["timeSchedule"][$i][]= strval($value->scheduleKey->scheduleName);
						$allSchedule["Success"]["timeSchedule"][$i][]= strval($value->scheduleKey->scheduleType);
						$allSchedule["Success"]["timeSchedule"][$i][]= strval($value->scheduleLevel);
						$i++;
					}else{
						$allSchedule["Success"]["holiday"][$j][]= strval($value->scheduleKey->scheduleName);
						$allSchedule["Success"]["holiday"][$j][]= strval($value->scheduleKey->scheduleType);
						$allSchedule["Success"]["holiday"][$j][]= strval($value->scheduleLevel);
						$j++;
					}
					
				}
			}
		}
		//print_r($allSchedule);
		return $allSchedule;
	}
	
	function getAutoAttendantInfoInstance($AAId){
		global $sessionid, $client;
		$aaInfoResp["Error"] = "";
		$aaInfoResp["Success"] = array();
		if ($ociVersion == "19")
		{
			$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantGetInstanceRequest19");
		}
		else
		{
			$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantGetInstanceRequest20");			
		}
		$xmlinput .= "<serviceUserId>" . $AAId. "</serviceUserId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//print_r($xml);
		if (readErrorXmlGenuine($xml) != "") {
			$allSchedule["Error"] = $xml->command->summaryEnglish;
		}else{
			$aaInfoResp["Success"]["basicInfo"]["type"] = strval($xml->command->type);
			$aaInfoResp["Success"]["basicInfo"]["aaName"] = strval($xml->command->serviceInstanceProfile->name);
			$aaInfoResp["Success"]["basicInfo"]["callingLineIdLastName"] = strval($xml->command->serviceInstanceProfile->callingLineIdLastName);
			$aaInfoResp["Success"]["basicInfo"]["callingLineIdFirstName"] = strval($xml->command->serviceInstanceProfile->callingLineIdFirstName);
			$aaInfoResp["Success"]["basicInfo"]["department"] = strval($xml->command->serviceInstanceProfile->department->name);
			$aaInfoResp["Success"]["basicInfo"]["timeZone"] = strval($xml->command->serviceInstanceProfile->timeZone);
			$aaInfoResp["Success"]["basicInfo"]["enableVideo"] = strval($xml->command->enableVideo);
			$aaInfoResp["Success"]["basicInfo"]["businessHours"] = strval($xml->command->businessHours->name);
			$aaInfoResp["Success"]["basicInfo"]["holidaySchedule"] = strval($xml->command->holidaySchedule->name);
			$aaInfoResp["Success"]["basicInfo"]["extensionDialingScope"] = strval($xml->command->extensionDialingScope);
			$aaInfoResp["Success"]["basicInfo"]["nameDialingScope"] = strval($xml->command->nameDialingScope);
			$aaInfoResp["Success"]["basicInfo"]["nameDialingEntries"] = strval($xml->command->nameDialingEntries);
			if(isset($xml->command->businessHoursMenu->keyConfiguration) && count($xml->command->businessHoursMenu->keyConfiguration)>0){
				$i=0;
				foreach ($xml->command->businessHoursMenu->keyConfiguration as $key => $value){
					$aaInfoResp["Success"]["allMenuData"]["bhData"][$i]["key"] = strval($value->key);
					$aaInfoResp["Success"]["allMenuData"]["bhData"][$i]["action"] = strval($value->entry->action);
					$i++;
				}
			}
			if(isset($xml->command->afterHoursMenu->keyConfiguration) && count($xml->command->afterHoursMenu->keyConfiguration)>0){
				$j=0;
				foreach ($xml->command->afterHoursMenu->keyConfiguration as $key => $value){
					$aaInfoResp["Success"]["allMenuData"]["ahData"][$j]["key"] = strval($value->key);
					$aaInfoResp["Success"]["allMenuData"]["ahData"][$j]["action"] = strval($value->entry->action);
					$j++;
				}
			}
			
			if(isset($xml->command->holidayMenu->keyConfiguration) && count($xml->command->holidayMenu->keyConfiguration)>0){
				$k=0;
				foreach ($xml->command->holidayMenu->keyConfiguration as $key => $value){
					$aaInfoResp["Success"]["allMenuData"]["hdData"][$k]["key"] = strval($value->key);
					$aaInfoResp["Success"]["allMenuData"]["hdData"][$k]["action"] = strval($value->entry->action);
					$k++;
				}
			}
			
			$aaInfoResp["Success"]["menuOtherDetails"]["businessHoursMenu"][0] = strval($xml->command->businessHoursMenu->announcementSelection);
			$aaInfoResp["Success"]["menuOtherDetails"]["businessHoursMenu"][1] = strval($xml->command->businessHoursMenu->enableFirstMenuLevelExtensionDialing);
			
			$aaInfoResp["Success"]["menuOtherDetails"]["afterHoursMenu"][0] = strval($xml->command->afterHoursMenu->announcementSelection);
			$aaInfoResp["Success"]["menuOtherDetails"]["afterHoursMenu"][1] = strval($xml->command->afterHoursMenu->enableFirstMenuLevelExtensionDialing);
			
			if(count($xml->command->holidayMenu) > 0){
				$aaInfoResp["Success"]["menuOtherDetails"]["holidayMenu"][0] = strval($xml->command->holidayMenu->announcementSelection);
				$aaInfoResp["Success"]["menuOtherDetails"]["holidayMenu"][1] = strval($xml->command->holidayMenu->enableFirstMenuLevelExtensionDialing);
			}
		}
		//print_r($aaInfoResp);
		return $aaInfoResp;
	}
	
	function authorizedUserServices(){
		global $sessionid, $client;
		$userAuthorizeServiceList["Error"] = "";
		$userAuthorizeServiceList["Success"] = "";
		$sp = $_SESSION['sp'];
		$groupId = $_SESSION["groupId"];
		$xmlinput = xmlHeader($sessionid, "GroupServiceGetAuthorizationListRequest");
		$xmlinput .= "<serviceProviderId>". htmlspecialchars($sp) ."</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$userAuthorizeServiceList["Error"] = $xml->command->summaryEnglish;
		}else{
			$c=0;
			foreach ($xml->command->userServicesAuthorizationTable->row as $key => $value){
				if(strval($value->col[1]) == "true"){
					$userAuthorizeServiceList["Success"][]=strval($value->col[0]);
				}
				$c++;
			}
		}
		return $userAuthorizeServiceList;
	}
	
	function serviceForAutoAttendant(){
		$serviceArray = array(
				"Alternate Numbers",
				"Anonymous Call Rejection",
				"Basic Call Logs",
				"Call Forwarding Always",
				"Call Forwarding Always Secondry",
				"Call Forwarding Busy",
				"Call Forwarding Selective",
				"Calling Line ID Delivery Blocking",
				"Call Me Now",
				"Call Notify",
				"Call Recording",
				"Communication Barring User-Control",
				"Connected Line Identification Restriction",
				"Custom Ringback User",
				"Custom Ringback User-Video",
				"Diversion Inhibitor",
				"Do Not Disturb",
				"Enhanced Call Logs",
				"External Custom Ringback",
				"Fax Messaging",
				"Group Night Forwarding",
				"Pre-alerting Announcement",
				"Fax Messaging",
				"Priority Alert",
				"Privacy",
				"Selective Call Acceptance",
				"Selective Call Rejection",
				"Third-Party Voice Mail Support",
				"Voice Messaging User",
				"Voice Messaging User - Video",
				"Zone Calling Restrictions"
		);
		return $serviceArray;
	}
	
	function availableServicesForAutoAttendantList(){
		$availArray = array();
		$serviceFroAutoAttendant = $this->serviceForAutoAttendant();
		$authorizedUserServices = $this->authorizedUserServices();
		if(count($authorizedUserServices["Success"]) > 0){
			foreach ($serviceFroAutoAttendant as $value){
				if(in_array($value, $authorizedUserServices["Success"])){
					$availArray["Success"][] = $value;
				}
			}
		}
		return $availArray;
	}
	
	//function to get call policies
	public function getCallPolicies($autoAttendantId){
		global $sessionid, $client;
		$callPlocies["Error"] = "";
		$callPlocies["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "UserCallPoliciesGetRequest17");
		$xmlinput .= "<userId>".$autoAttendantId."</userId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			$callPlocies["Error"] = $xml->command->summaryEnglish;
		}else{
			$callPlocies["Success"]['redirectedCallsCOLPPrivacy'] = strval($xml->command->redirectedCallsCOLPPrivacy);
			$callPlocies["Success"]['callBeingForwardedResponseCallType'] = strval($xml->command->callBeingForwardedResponseCallType);
		}
		return $callPlocies;		
	}
	
	function aaAssignedService($aaId){
		global $sessionid, $client;
		$aaAssignedService["Error"] = array();
		$aaAssignedService["Success"] = array();
		$xmlinput = xmlHeader($sessionid, "UserAssignedServicesGetListRequest");
		$xmlinput .= "<userId>".$aaId."</userId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if(count($xml->command->userServiceEntry) > 0){
			foreach ($xml->command->userServiceEntry as $key => $val){
				$aaAssignedService["Success"][] = strval($val->serviceName);
			}
		}
		return $aaAssignedService;
	}
	
	function availableServiceAutoAttendantForAssign($aaId){
		$availForAssign = array();
		$assigned = $this->aaAssignedService($aaId);
		$availableList = $this->availableServicesForAutoAttendantList();
		if(count($assigned["Success"]) > 0 && isset($assigned["Success"])){
			foreach ($availableList["Success"] as $key => $value){
				if (!in_array($value, $assigned["Success"])){
					$availForAssign[] = $value;
				}
			}
		}else{
			$availForAssign = $availableList["Success"];
		}
		return $availForAssign;
	}
	
	function checkAssignedServiceChange($postArray, $sessionData){
		$assignArray['unAssign'] = array();
		$assignArray['newAssign'] = array();
		//print_r($postArray);
		//print_r($sessionData);
		if(count($sessionData) > 0){
			foreach($sessionData as $key => $value){
				
				if(!in_array($value, $postArray)){
					$assignArray['unAssign'][] = $value;
				}
			}
		}
		if(count($postArray) > 0){
			foreach ($postArray as $key1 => $value1){
				if(!in_array($value1, $sessionData)){
					$assignArray['newAssign'][] = $value1;
				}
			}
		}
		return $assignArray;
	}
	
	function getAvailableNumber(){
		global $sessionid, $client;
		$xmlinput = xmlHeader($sessionid, "GroupDnGetAvailableListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		$a = 0;
		foreach ($xml->command->phoneNumber as $key => $value)
		{
			$availableNumbers[$a] = strval($value);
			$a++;
		}
		return $availableNumbers;
	}
	
	function getAutoAttendantList($ociVersion){
		global $sessionid, $client;
		
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantGetInstanceListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		$a = 0;
		if (isset($xml->command->autoAttendantTable->row))
		{
			foreach ($xml->command->autoAttendantTable->row as $key => $value)
			{
				if (($ociVersion == "17" and strval($value->col[7]) !== "Standard") or $ociVersion !== "17")
				{
					$autoAttendants[$a]["id"] = strval($value->col[0]);
					$autoAttendants[$a]["name"] = strval($value->col[1]);
					$a++;
				}
			}
		}
		
		if (isset($autoAttendants))
		{
			$autoAttendants = subval_sort($autoAttendants, "name");
		}
		return $autoAttendants;
	}
	
	function getSubMenuListRequest($aaId){
	    
	    global $sessionid, $client;
	    $submenuList["Success"] = "";
	    $submenuList["Error"] = "";
	    $xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuGetListRequest");
	    $xmlinput .= "<serviceUserId>".$aaId."</serviceUserId>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    if(count($xml->command->submenuTable->row) > 0){
	        foreach($xml->command->submenuTable->row as $key => $value){
	            $submenuList["Success"][strval($value->col[0])] = strval($value->col[1]);
	        }
	    }
	    return $submenuList;
	}
	
	function createSubmenu($aaId, $subMenuId){
	    global $sessionid, $client;
	    $returnResponse["Error"] = "";
	    $returnResponse["Success"] = "";
	    
	    $xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuAddRequest");
	    $xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
	    $xmlinput .= "<submenuId>".$subMenuId."</submenuId>";
	    $xmlinput .= "<announcementSelection>Default</announcementSelection>";
	    $xmlinput .= "<enableLevelExtensionDialing>false</enableLevelExtensionDialing>";
	    $xmlinput .= "<keyConfiguration>";
	    $xmlinput .= "<key>5</key>";
	    $xmlinput .= "<entry>";
	    $xmlinput .= "<description>previous menu</description>";
	    $xmlinput .= "<action>Return to Previous Menu</action>";
	    $xmlinput .= "</entry>";
	    $xmlinput .= "</keyConfiguration>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    return $xml;
	}
	
	function deleteSubMenuFromAutoAttendant($aaId, $subMenuId){
	    global $sessionid, $client;
	    $returnResponse["Error"] = "";
	    $returnResponse["Success"] = "";
	    
	    $xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuDeleteListRequest");
	    $xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
	    $xmlinput .= "<submenuId>".$subMenuId."</submenuId>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    return $xml;
	}
}