<?php

class AaController21{
	
	function addAutoAttendant($sp, $groupId, $postArray){
		
		global $sessionid, $client;
		$addResp["Error"]="";
		$addResp["Success"]="";
		
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantAddInstanceRequest20");
		
		$xmlinput .= "<serviceProviderId>".$sp."</serviceProviderId>";
		$xmlinput .= "<groupId>".$groupId."</groupId>";
		$xmlinput .= "<serviceUserId>".$postArray["aaId"]."</serviceUserId>";
		
		$xmlinput .= "<serviceInstanceProfile>";
			$xmlinput .= "<name>".$postArray["aaName"]."</name>";
			$xmlinput .= "<callingLineIdLastName>".$postArray["callingLineIdLastName"]."</callingLineIdLastName>";
			$xmlinput .= "<callingLineIdFirstName>".$postArray["callingLineIdFirstName"]."</callingLineIdFirstName>";
			if(!empty($postArray["phoneNumberAdd"]) && isset($postArray["phoneNumberAdd"])){
				$xmlinput .= "<phoneNumber>".$postArray["phoneNumberAdd"]."</phoneNumber>";
			}
			if(!empty($postArray["extensionAdd"]) && isset($postArray["extensionAdd"])){
				$xmlinput .= "<extension>".$postArray["extensionAdd"]."</extension>";
			}
			if(!empty($postArray["department"])){
				$xmlinput .= "<department xsi:type='GroupDepartmentKey'>";
					$xmlinput .= "<serviceProviderId>".$sp."</serviceProviderId>";
					$xmlinput .= "<groupId>".$groupId."</groupId>";
					$xmlinput .= "<name>".$postArray["department"]."</name>";
				$xmlinput .= "</department>";
			}
			$xmlinput .= "<timeZone>".$postArray["timeZone"]."</timeZone>";
		$xmlinput .= "</serviceInstanceProfile>";
		
		if(!empty($postArray["aaType"])){
			$xmlinput .= "<type>".$postArray["aaType"]."</type>";
		}
		
		if(!empty($postArray["firstDigitTimeoutSeconds"])){
			$xmlinput .= "<firstDigitTimeoutSeconds>".$postArray["firstDigitTimeoutSeconds"]."</firstDigitTimeoutSeconds>";
		}
		
		if(!empty($postArray["eVsupport"])){
			$xmlinput .= "<enableVideo>".$postArray["eVsupport"]."</enableVideo>";
		}else{
			$xmlinput .= "<enableVideo>false</enableVideo>";
		}
		
		if(!empty($postArray["businessHoursSchedule"])){
			$xmlinput .= "<businessHours>";
				$xmlinput .= "<type>Group</type>";
				$xmlinput .= "<name>".$postArray["businessHoursSchedule"]."</name>";
			$xmlinput .= "</businessHours>";
		}
		
		if(!empty($postArray["holidaySchedule"])){
			$xmlinput .= "<holidaySchedule>";
				$xmlinput .= "<type>Group</type>";
				$xmlinput .= "<name>".$postArray["holidaySchedule"]."</name>";
			$xmlinput .= "</holidaySchedule>";
		}
		if(!empty($postArray["extensionDialing"])){
			$xmlinput .= "<extensionDialingScope>".$postArray["extensionDialing"]."</extensionDialingScope>";
		}
		if(!empty($postArray["nameDialing"])){
			$xmlinput .= "<nameDialingScope>".$postArray["nameDialing"]."</nameDialingScope>";
		}
		if(!empty($postArray["nameDialingEntries"])){
			$xmlinput .= "<nameDialingEntries>".$postArray["nameDialingEntries"]."</nameDialingEntries>";
		}
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			if(!empty($xml->command->detail)){
				$detail = strval($xml->command->detail);
				$error = explode("<command", $detail);
				//$addResp["Error"]= $error[0];
				$addResp["Error"]= "Express cannot currently process AA operations. Please contact Customer Service.<br/>".$error[0];
			}else{
				$addResp["Error"]= $xml->command->summaryEnglish;
			}
		}else{
			$addResp["Success"] = "Success";
		}
		return $addResp;
	}
	
}