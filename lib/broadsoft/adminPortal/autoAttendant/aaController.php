<?php

class AaController{
	
	function addAutoAttendant($sp, $groupId, $postArray){
		
		global $sessionid, $client;
		$addResp["Error"]="";
		$addResp["Success"]="";
		
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantAddInstanceRequest19");
				
		$xmlinput .= "<serviceProviderId>".htmlspecialchars($sp)."</serviceProviderId>";
		$xmlinput .= "<groupId>".htmlspecialchars($groupId)."</groupId>";
		$xmlinput .= "<serviceUserId>".$postArray["aaId"]."</serviceUserId>";
		
		$xmlinput .= "<serviceInstanceProfile>";
			$xmlinput .= "<name>".$postArray["aaName"]."</name>";
			$xmlinput .= "<callingLineIdLastName>".$postArray["callingLineIdLastName"]."</callingLineIdLastName>";
			$xmlinput .= "<callingLineIdFirstName>".$postArray["callingLineIdFirstName"]."</callingLineIdFirstName>";
			if(!empty($postArray["phoneNumberAdd"]) && isset($postArray["phoneNumberAdd"])){
				$xmlinput .= "<phoneNumber>".$postArray["phoneNumberAdd"]."</phoneNumber>";
			}
			if(!empty($postArray["extensionAdd"]) && isset($postArray["extensionAdd"])){
				$xmlinput .= "<extension>".$postArray["extensionAdd"]."</extension>";
			}
			if(!empty($postArray["department"])){
				$xmlinput .= "<department xsi:type='GroupDepartmentKey'>";
				$xmlinput .= "<serviceProviderId>".htmlspecialchars($sp)."</serviceProviderId>";
				$xmlinput .= "<groupId>".htmlspecialchars($groupId)."</groupId>";
					$xmlinput .= "<name>".$postArray["department"]."</name>";
				$xmlinput .= "</department>";
			}
			$xmlinput .= "<timeZone>".$postArray["timeZone"]."</timeZone>";
		$xmlinput .= "</serviceInstanceProfile>";
		
		if(!empty($postArray["aaType"])){
			$xmlinput .= "<type>".$postArray["aaType"]."</type>";
		}
		
		if(!empty($postArray["eVsupport"])){
			$xmlinput .= "<enableVideo>".$postArray["eVsupport"]."</enableVideo>";
		}else{
			$xmlinput .= "<enableVideo>false</enableVideo>";
		}
		
		if(!empty($postArray["businessHoursSchedule"])){
			$xmlinput .= "<businessHours>";
				$xmlinput .= "<type>Group</type>";
				$xmlinput .= "<name>".$postArray["businessHoursSchedule"]."</name>";
			$xmlinput .= "</businessHours>";
		}
		
		if(!empty($postArray["holidaySchedule"])){
			$xmlinput .= "<holidaySchedule>";
				$xmlinput .= "<type>Group</type>";
				$xmlinput .= "<name>".$postArray["holidaySchedule"]."</name>";
			$xmlinput .= "</holidaySchedule>";
		}
		if(!empty($postArray["extensionDialing"])){
			$xmlinput .= "<extensionDialingScope>".$postArray["extensionDialing"]."</extensionDialingScope>";
		}
		if(!empty($postArray["nameDialing"])){
			$xmlinput .= "<nameDialingScope>".$postArray["nameDialing"]."</nameDialingScope>";
		}
		if(!empty($postArray["nameDialingEntries"])){
			$xmlinput .= "<nameDialingEntries>".$postArray["nameDialingEntries"]."</nameDialingEntries>";
		}
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			if(!empty($xml->command->detail)){
				$addResp["Error"]= $xml->command->detail;
			}else{
				$addResp["Error"]= $xml->command->summaryEnglish;
			}
		}else{
			$addResp["Success"] = "Success";
		}
		return $addResp;
	}
	
	//function to set call policies
	public function modifyCallPolicies($postArray){
		
		global $sessionid, $client;
		$callPlocies["Error"] = "";
		$callPlocies["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "UserCallPoliciesModifyRequest");
		$xmlinput .= "<userId>".$postArray['autoAttendantId']."</userId>";
		$xmlinput .= "<redirectedCallsCOLPPrivacy>".$postArray['redirectedCallsCOLPPrivacy']."</redirectedCallsCOLPPrivacy>";
		$xmlinput .= "<callBeingForwardedResponseCallType>".$postArray['callBeingForwardedResponseCallType']."</callBeingForwardedResponseCallType>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			$callPlocies["Error"] = $xml->command->summaryEnglish;
		}else{
			$callPlocies["Success"] = "Success";
			
		}
		return $callPlocies;
	}
	
	function addServicesToAA($aaId, $assignArray){
		global $sessionid, $client;
		$aaAssignedService["Error"] = array();
		$aaAssignedService["Success"] = array();
		$xmlinput = xmlHeader($sessionid, "UserServiceAssignListRequest");
		$xmlinput .= "<userId>".$aaId."</userId>";
		if(count($assignArray) > 0){
			foreach ($assignArray as $key => $value){
				$xmlinput .= "<serviceName>".$value."</serviceName>";
			}
		}
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	}
	
	function removeServicesFromAA($aaId, $unAssignArray){
		global $sessionid, $client;
		$aaAssignedService["Error"] = array();
		$aaAssignedService["Success"] = array();
		$xmlinput = xmlHeader($sessionid, "UserServiceUnassignListRequest");
		$xmlinput .= "<userId>".$aaId."</userId>";
		if(count($unAssignArray) > 0){
			foreach ($unAssignArray as $key => $value){
				$xmlinput .= "<serviceName>".$value."</serviceName>";
			}
		}
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	}
	
	function deleteAutoAttendant($serviceUserId){
		global $sessionid, $client;
		$delResponse["Success"] = "";
		$delResponse["Error"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantDeleteInstanceRequest");
		$xmlinput .= "<serviceUserId>".$serviceUserId."</serviceUserId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$delResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$delResponse["Success"] = "Success";
			
		}
		return $delResponse;
	}
	
	//function to generate the autoattendant name
	function constructAAName($groupId, $groupName, $phoneNummber, $ext, $aaNameCriteriaValue) {
		//$_SESSION["proxyDomain"]
		//global $aaNameCriteriaValue;
		$idValueMap = array();
		if(isset($groupName) && !empty($groupName)){
			$idValueMap[ResourceNameBuilder::GRP_NAME] = $groupName;
		}

		if(isset($phoneNummber) && !empty($phoneNummber)){
			$idValueMap[ResourceNameBuilder::DN] = $phoneNummber;
		}
		
		if(isset($ext) && !empty($ext)){
			$idValueMap[ResourceNameBuilder::EXT] = $ext;
		}
		
		if(isset($groupId) && !empty($groupId)){
			$idValueMap[ResourceNameBuilder::GRP_ID] = $groupId;
		}
		if(isset($proxyDomain) && !empty($proxyDomain)){
			$idValueMap[ResourceNameBuilder::GRP_PROXY_DOMAIN] = $proxyDomain;
		}
		
		$regexSet = array(
				ResourceNameBuilder::GRP_NAME,
				ResourceNameBuilder::DN,
				ResourceNameBuilder::EXT,
				ResourceNameBuilder::GRP_ID
		);
		
		$nameAndIdBuilder = new NameAndIDBuilder($aaNameCriteriaValue, $idValueMap, $regexSet);
		$aaName = $nameAndIdBuilder->getParsedId($aaNameCriteriaValue, $idValueMap);
		if(isset($aaName) && empty($aaName)) {
			$aaName = $nameAndIdBuilder->getParsedId($aaNameCriteriaValue, $idValueMap);
		}
		if(isset($aaName) && empty($aaName)) { //use default id
			$idDefaulRegex = $groupName."_".$groupId."-AA";
			
			$aaName = $nameAndIdBuilder->getParsedId($idDefaulRegex, $idValueMap);
		}
		
		return $aaName;
	}
	
	//function to generate the autoattendant clid lname
	function constructAAClidLName($groupId, $groupName, $phoneNummber, $ext, $aaClidLnameValue) {
		//$_SESSION["proxyDomain"]
		//global $aaClidLnameValue;
		$idValueMap = array();
		if(isset($groupName) && !empty($groupName)){
			$idValueMap[ResourceNameBuilder::GRP_NAME] = $groupName;
		}

		if(isset($phoneNummber) && !empty($phoneNummber)){
			$idValueMap[ResourceNameBuilder::DN] = $phoneNummber;
		}
		
		if(isset($ext) && !empty($ext)){
			$idValueMap[ResourceNameBuilder::EXT] = $ext;
		}
		
		if(isset($groupId) && !empty($groupId)){
			$idValueMap[ResourceNameBuilder::GRP_ID] = $groupId;
		}
		$regexSet = array(
				ResourceNameBuilder::GRP_NAME,
				ResourceNameBuilder::DN,
				ResourceNameBuilder::EXT,
				ResourceNameBuilder::GRP_ID
		);
		
		$nameAndIdBuilder = new NameAndIDBuilder($aaClidLnameValue, $idValueMap, $regexSet);
		$aaclidl = $nameAndIdBuilder->getParsedId($aaClidLnameValue, $idValueMap);
		if(isset($aaclidl) && empty($aaclidl)) {
			$aaclidl = $nameAndIdBuilder->getParsedId($aaClidLnameValue, $idValueMap);
		}
		if(isset($aaclidl) && empty($aaclidl)) { //use default id
			$idDefaulRegex = $phoneNummber."_".$ext."-".$groupId."-".$groupName;
			
			$aaclidl = $nameAndIdBuilder->getParsedId($idDefaulRegex, $idValueMap);
		}
		
		return $aaclidl;
	}
	
	//function to generate the autoattendant clid fname
	function constructAAClidFName($groupId, $groupName, $phoneNummber, $ext, $aaClidFnameValue) {
		//$_SESSION["proxyDomain"]
		//echo $aaClidFnameValue; die;
		$idValueMap = array();
		if(isset($groupName) && !empty($groupName)){
			$idValueMap[ResourceNameBuilder::GRP_NAME] = $groupName;
		}

		if(isset($phoneNummber) && !empty($phoneNummber)){
			$idValueMap[ResourceNameBuilder::DN] = $phoneNummber;
		}
		
		if(isset($ext) && !empty($ext)){
			$idValueMap[ResourceNameBuilder::EXT] = $ext;
		}
		
		if(isset($groupId) && !empty($groupId)){
			$idValueMap[ResourceNameBuilder::GRP_ID] = $groupId;
		}
		
		$regexSet = array(
				ResourceNameBuilder::GRP_NAME,
				ResourceNameBuilder::DN,
				ResourceNameBuilder::EXT,
				ResourceNameBuilder::GRP_ID
		);
		
		$nameAndIdBuilder = new NameAndIDBuilder($aaClidFnameValue, $idValueMap, $regexSet);
		$aaclidf = $nameAndIdBuilder->getParsedId($aaClidFnameValue, $idValueMap);
		if(isset($aaclidf) && empty($aaclidf)) {
			$aaclidf = $nameAndIdBuilder->getParsedId($aaClidFnameValue, $idValueMap);
		}
		if(isset($aaclidf) && empty($aaclidf)) { //use default id
			$idDefaulRegex = $ext."_".$phoneNummber."-".$groupId."-".$groupName;
			
			$aaclidf = $nameAndIdBuilder->getParsedId($idDefaulRegex, $idValueMap);
		}
		
		return $aaclidf;
	}
	
	function createSubmenu($postArray, $aaId, $audioGreeting, $playAnn){
	    global $sessionid, $client;
	    $returnResponse["Error"] = "";
	    $returnResponse["Success"] = "";
	    
	    $xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuAddRequest");
	    $xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
	    $xmlinput .= "<submenuId>".$postArray["id"]."</submenuId>";
	    if($postArray["bhGreeting"] == "Default Greeting"){
	        $xmlinput .= "<announcementSelection>Default</announcementSelection>";
	    }else{
	        $xmlinput .= "<announcementSelection>Personal</announcementSelection>";
	        $xmlinput .= $audioGreeting;
	    }
	    
	    $xmlinput .= "<enableLevelExtensionDialing>".$postArray["enableLevelExtensionDialing"]."</enableLevelExtensionDialing>";
	    foreach ($postArray["keys"] as $key => $value){
	        if($value["action"] != "" || $value["desc"] != "" || $value["phoneNumber"] != "" || $value["submenu"] != "" || $value["announcements"] != ""){
	            $xmlinput .= "<keyConfiguration>";
	            $xmlinput .= "<key>".$key."</key>";
	            $xmlinput .= "<entry>";
	            if($value["desc"] != ""){
	                $xmlinput .= "<description>".$value["desc"]."</description>";
	            }
	            if($value["action"] != ""){
	                $xmlinput .= "<action>".$value["action"]."</action>";
	            }
	            if($value["phoneNumber"] != ""){
	                $xmlinput .= "<phoneNumber>".$value["phoneNumber"]."</phoneNumber>";
	            }
	            if($value["submenu"] != ""){
	                $xmlinput .= "<submenuId>".$value["submenu"]."</submenuId>";
	            }
	            if($value["announcements"] != ""){
	                $xmlinput .= $playAnn[$key];
	            }
	            
	            
	            $xmlinput .= "</entry>";
	            $xmlinput .= "</keyConfiguration>";
	        }
	    }
	    
	    $xmlinput .= xmlFooter();
	    //print_r($xmlinput);
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    return $xml;
	}
	
	function deleteSubMenuFromAutoAttendant($aaId, $subMenuId){
	    global $sessionid, $client;
	    $returnResponse["Error"] = "";
	    $returnResponse["Success"] = "";
	    
	    $xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuDeleteListRequest");
	    $xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
	    $xmlinput .= "<submenuId>".$subMenuId."</submenuId>";
	    $xmlinput .= xmlFooter();
	    //print_r($xmlinput);
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	   // print_r($xml);
	    if (readErrorXmlGenuine($xml) != "") {
	        $returnResponse["Error"] = $xml->command->summaryEnglish;
	    }else{
	        $returnResponse["Success"] = "Success";  
	    }
	    return $returnResponse;
	}
	
	function checkIfSubmenuIsUsed($aaId, $subMenuId){
	   // print_r($subMenuId);
	    global $sessionid, $client;
	    $returnResponse["Error"] = array();
	    $returnResponse["Success"] = array();
	    
	    $xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuGetUsageListRequest");
	    $xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
	    $xmlinput .= "<submenuId>".$subMenuId."</submenuId>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    //print_r($xml);
	   if (readErrorXmlGenuine($xml) != "") {
	        $returnResponse["Error"] = $xml->command->summaryEnglish;
	    }else{
	        if(count($xml->command->submenuTable->row) > 0){
	            foreach ($xml->command->submenuTable->row as $key => $value){
	                $returnResponse["Success"][] = strval($value->col[1]);
	            }
	        }
	    }
	    return $returnResponse;
	    
	}
}