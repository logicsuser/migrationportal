<?php
	$xmlinput = xmlHeader($sessionid, "GroupGetRequest14sp7");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$offset = strval($xml->command->timeZoneDisplayName);
	$exp = explode("-", $offset);
	$offset = substr($exp[1], 0, 5);
	$offset = "-" . $offset;

	$schedName = $_POST["schedName"];
	$schedType = $_POST["schedType"];

	if (isset($_POST["startTime"]))
	{
		$exp = explode(":", $_POST["startTime"]);
		$startHour = $exp[0];
		if ($startHour == 12)
		{
			$startHour = 0;
		}
		if (substr($exp[1], -2) == "pm")
		{
			$startHour = $startHour + 12;
		}
		$startMinute = substr($exp[1], 0, 2);
	}
	if (isset($_POST["endTime"]))
	{
		$exp = explode(":", $_POST["endTime"]);
		$endHour = $exp[0];
		if ($endHour == 12)
		{
			$endHour = 0;
		}
		if (substr($exp[1], -2) == "pm")
		{
			$endHour = $endHour + 12;
		}
		$endMinute = substr($exp[1], 0, 2);
	}

	$exp = explode("/", $_POST["dateStart"]);
	$startDate = $exp[2] . "-" . $exp[0] . "-" . $exp[1];

	$exp = explode("/", $_POST["dateEnd"]);
	$endDate = $exp[2] . "-" . $exp[0] . "-" . $exp[1];

	if (isset($_POST["recurEndDate"]))
	{
		$exp = explode("/", $_POST["recurEndDate"]);
		$recurEndDate = $exp[2] . "-" . $exp[0] . "-" . $exp[1];
	}

	if (isset($_POST["delEventName"]))
	{
		$xmlinput = xmlHeader($sessionid, "GroupScheduleModifyEventRequest");
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "GroupScheduleAddEventRequest");
	}
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<scheduleKey>
				<scheduleName>" . htmlspecialchars($schedName) . "</scheduleName>
				<scheduleType>" . $schedType . "</scheduleType>
			</scheduleKey>";
	if (isset($_POST["delEventName"]))
	{
		$xmlinput .= "<eventName>" . htmlspecialchars($_POST["delEventName"]) . "</eventName>";
		$xmlinput .= "<newEventName>" . htmlspecialchars($_POST["eventName"]) . "</newEventName>";
	}
	else
	{
		$xmlinput .= "<eventName>" . htmlspecialchars($_POST["eventName"]) . "</eventName>";
	}

	$xmlinput .= "<startDate>" . $startDate . $offset . "</startDate>";

	if ($_POST["allDayEvent"] == "True")
	{
		$xmlinput .= "<allDayEvent>true</allDayEvent>";
	}
	else
	{
		$xmlinput .= "<startTime>
					<hour>" . $startHour . "</hour>
					<minute>" . $startMinute . "</minute>
				</startTime>";
		$xmlinput .= "<endTime>
					<hour>" . $endHour . "</hour>
					<minute>" . $endMinute . "</minute>
				</endTime>";
	}

	$xmlinput .= "<endDate>" . $endDate . $offset . "</endDate>";

	if ($_POST["recurrence"] !== "Never")
	{
		$xmlinput .= "<recurrence>";

		if ($_POST["recurrence"] == "Daily")
		{
			$xmlinput .= "<recurDaily>
						<recurInterval>" . $_POST["recurrenceInterval"] . "</recurInterval>
					</recurDaily>";
		}
		else if ($_POST["recurrence"] == "Weekly")
		{
			$xmlinput .= "<recurWeekly>
						<recurInterval>" . $_POST["recurrenceInterval"] . "</recurInterval>
						<sunday>" . ($_POST["weeklyIntervalSunday"] == "True" ? "true" : "false") . "</sunday>
						<monday>" . ($_POST["weeklyIntervalMonday"] == "True" ? "true" : "false") . "</monday>
						<tuesday>" . ($_POST["weeklyIntervalTuesday"] == "True" ? "true" : "false") . "</tuesday>
						<wednesday>" . ($_POST["weeklyIntervalWednesday"] == "True" ? "true" : "false") . "</wednesday>
						<thursday>" . ($_POST["weeklyIntervalThursday"] == "True" ? "true" : "false") . "</thursday>
						<friday>" . ($_POST["weeklyIntervalFriday"] == "True" ? "true" : "false") . "</friday>
						<saturday>" . ($_POST["weeklyIntervalSaturday"] == "True" ? "true" : "false") . "</saturday>
					</recurWeekly>";
		}
		else if ($_POST["recurrence"] == "Monthly")
		{
			if ($_POST["recurMonthly"] == "ByDay")
			{
				$xmlinput .= "<recurMonthlyByDay>
							<recurInterval>" . $_POST["recurrenceInterval"] . "</recurInterval>
							<dayOfMonth>" . $_POST["dayOfMonth"] . "</dayOfMonth>
						</recurMonthlyByDay>";
			}
			else if ($_POST["recurMonthly"] == "ByWeek")
			{
				$xmlinput .= "<recurMonthlyByWeek>
							<recurInterval>" . $_POST["recurrenceInterval"] . "</recurInterval>
							<dayOfWeekInMonth>" . $_POST["dayOfWeekInMonth"] . "</dayOfWeekInMonth>
							<dayOfWeek>" . $_POST["dayOfWeek"] . "</dayOfWeek>
						</recurMonthlyByWeek>";
			}
		}
		else if ($_POST["recurrence"] == "Yearly")
		{
			if ($_POST["recurYearly"] == "ByDay")
			{
				$xmlinput .= "<recurYearlyByDay>
							<recurInterval>" . $_POST["recurrenceInterval"] . "</recurInterval>
							<dayOfMonth>" . $_POST["dayOfMonthInYear"] . "</dayOfMonth>
							<month>" . $_POST["byDayMonth"] . "</month>
						</recurYearlyByDay>";
			}
			else if ($_POST["recurYearly"] == "ByWeek")
			{
				$xmlinput .= "<recurYearlyByWeek>
							<recurInterval>" . $_POST["recurrenceInterval"] . "</recurInterval>
							<dayOfWeek>" . $_POST["dayOfWeekInYear"] . "</dayOfWeek>
							<dayOfWeekInMonth>" . $_POST["dayOfWeekInMonthInYear"] . "</dayOfWeekInMonth>
							<month>" . $_POST["byWeekMonth"] . "</month>
						</recurYearlyByWeek>";
			}
		}

		if ($_POST["recurEnd"] == "Never")
		{
			$xmlinput .= "<recurForEver>true</recurForEver>";
		}
		else if ($_POST["recurEnd"] == "After")
		{
			$xmlinput .= "<recurEndOccurrence>" . $_POST["recurEndOccurrence"] . "</recurEndOccurrence>";
		}
		else if ($_POST["recurEnd"] == "Date")
		{
			$xmlinput .= "<recurEndDate>" . $recurEndDate . $offset . "</recurEndDate>";
		}

		$xmlinput .= "</recurrence>";
	}
	else if (isset($_POST["delEventName"]))
	{
		$xmlinput .= "<recurrence xsi:nil=\"true\" />";
	}

	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
//print_r($xml);
	readError($xml);
?>
