<?php 
class OCPOperations{
    function userCallingPlanGetOriginatingRequest($userId){
        global  $sessionid, $client;
        $ocpResponse["Error"] = array();
        $ocpResponse["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "UserOutgoingCallingPlanOriginatingGetRequest");
        $xmlinput .= "<userId>".$userId."</userId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $ocpResponse["Error"] = strval($xml->command->summaryEnglish);
         }else{
             $ocpResponse["Success"]["useCustomSettings"] = strval($xml->command->useCustomSettings);
             $ocpResponse["Success"]["userPermissions"] = (array) $xml->command->userPermissions;
         }
        
        return $ocpResponse;
    }
    
    function userCallingPlanModifyOriginatingRequest($userId, $arrayData){
        global  $sessionid, $client;
        $ocpModResponse["Error"] = "";
        $ocpModResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "UserOutgoingCallingPlanOriginatingModifyRequest");
        $xmlinput .= "<userId>".$userId."</userId>";
        if(isset($arrayData["useCustomSettings"]) && $arrayData["useCustomSettings"] == "false"){
            $xmlinput .= "<useCustomSettings>false</useCustomSettings>";
        }else{
            if(isset($arrayData["useCustomSettings"]) && !empty($arrayData["useCustomSettings"])){
                $xmlinput .= "<useCustomSettings>".$arrayData["useCustomSettings"]."</useCustomSettings>";
            }
            
            $xmlinput .= "<userPermissions>";
                if(isset($arrayData["userPermissionsGroup"]) && !empty($arrayData["userPermissionsGroup"])){
                    $xmlinput .= "<group>".$arrayData["userPermissionsGroup"]."</group>";
                }
                
                if(isset($arrayData["userPermissionsLocal"]) && !empty($arrayData["userPermissionsLocal"])){
                    $xmlinput .= "<local>".$arrayData["userPermissionsLocal"]."</local>";
                }
                
                if(isset($arrayData["userPermissionsTollfree"]) && !empty($arrayData["userPermissionsTollfree"])){
                    $xmlinput .= "<tollFree>".$arrayData["userPermissionsTollfree"]."</tollFree>";
                }
                
                if(isset($arrayData["userPermissionsToll"]) && !empty($arrayData["userPermissionsToll"])){
                    $xmlinput .= "<toll>".$arrayData["userPermissionsToll"]."</toll>";
                }
                
                if(isset($arrayData["userPermissionsInternational"]) && !empty($arrayData["userPermissionsInternational"])){
                    $xmlinput .= "<international>".$arrayData["userPermissionsInternational"]."</international>";
                }
                
                if(isset($arrayData["userPermissionsOperatorAssisted"]) && !empty($arrayData["userPermissionsOperatorAssisted"])){
                    $xmlinput .= "<operatorAssisted>".$arrayData["userPermissionsOperatorAssisted"]."</operatorAssisted>";
                }
                
                if(isset($arrayData["userPermissionsOperatorChargDirAssisted"]) && !empty($arrayData["userPermissionsOperatorChargDirAssisted"])){
                    $xmlinput .= "<chargeableDirectoryAssisted>".$arrayData["userPermissionsOperatorChargDirAssisted"]."</chargeableDirectoryAssisted>";
                }
                
                if(isset($arrayData["userPermissionsSpecialService1"]) && !empty($arrayData["userPermissionsSpecialService1"])){
                    $xmlinput .= "<specialServicesI>".$arrayData["userPermissionsSpecialService1"]."</specialServicesI>";
                }
                
                if(isset($arrayData["userPermissionsSpecialService2"]) && !empty($arrayData["userPermissionsSpecialService2"])){
                    $xmlinput .= "<specialServicesII>".$arrayData["userPermissionsSpecialService2"]."</specialServicesII>";
                }
                
                if(isset($arrayData["userPermissionsPremiumServices1"]) && !empty($arrayData["userPermissionsPremiumServices1"])){
                    $xmlinput .= "<premiumServicesI>".$arrayData["userPermissionsPremiumServices1"]."</premiumServicesI>";
                }
                
                if(isset($arrayData["userPermissionsPremiumServices2"]) && !empty($arrayData["userPermissionsPremiumServices2"])){
                    $xmlinput .= "<premiumServicesII>".$arrayData["userPermissionsPremiumServices2"]."</premiumServicesII>";
                }
                
                if(isset($arrayData["userPermissionsCasual"]) && !empty($arrayData["userPermissionsCasual"])){
                    $xmlinput .= "<casual>".$arrayData["userPermissionsCasual"]."</casual>";
                }
                
                if(isset($arrayData["userPermissionsUrlDialing"]) && !empty($arrayData["userPermissionsUrlDialing"])){
                    $xmlinput .= "<urlDialing>".$arrayData["userPermissionsUrlDialing"]."</urlDialing>";
                }
                
                if(isset($arrayData["userPermissionsUnknown"]) && !empty($arrayData["userPermissionsUnknown"])){
                    $xmlinput .= "<unknown>".$arrayData["userPermissionsUnknown"]."</unknown>";
                }
                
         
            $xmlinput .= "</userPermissions>";
        }
        
        
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        if (readErrorXmlGenuine($xml) != "") {
            $ocpModResponse["Error"] = strval($xml->command->summaryEnglish);
        }else{
            $ocpModResponse["Success"] = "Success";
        }
        
        return $ocpModResponse;
        
    }
    
    //Code added @ 20 Aug 2018
    function groupCallingPlanGetOriginatingRequest($serviceProiverId, $groupId){
        global  $sessionid, $client;
        $ocpResponse["Error"] = array();
        $ocpResponse["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "GroupOutgoingCallingPlanOriginatingGetListRequest");
        $xmlinput .= "<serviceProviderId>". htmlspecialchars($serviceProiverId) ."</serviceProviderId>";        
        $xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);        
        
        if (readErrorXmlGenuine($xml) != "") {
            $ocpResponse["Error"] = strval($xml->command->summaryEnglish);
         }else{
             
            $ocpResponse["Success"]["groupPermissions"] = (array) $xml->command->groupPermissions;
         }
        
        return $ocpResponse;
    }
    
    //Code added @ 20 Aug 2018
    function groupCallingPlanModifyOriginatingRequest($serviceProiverId, $groupId, $arrayData){
        global  $sessionid, $client;
        $ocpModResponse["Error"] = "";
        $ocpModResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "GroupOutgoingCallingPlanOriginatingModifyListRequest");
        $xmlinput .= "<serviceProviderId>". htmlspecialchars($serviceProiverId) ."</serviceProviderId>";        
        $xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
        
        $xmlinput .= "<groupPermissions>";
                if(isset($arrayData["groupPermissionsGroup"]) && !empty($arrayData["groupPermissionsGroup"])){
                    $xmlinput .= "<group>".$arrayData["groupPermissionsGroup"]."</group>";
                }
                
                if(isset($arrayData["groupPermissionsLocal"]) && !empty($arrayData["groupPermissionsLocal"])){
                    $xmlinput .= "<local>".$arrayData["groupPermissionsLocal"]."</local>";
                }
                
                if(isset($arrayData["groupPermissionsTollfree"]) && !empty($arrayData["groupPermissionsTollfree"])){
                    $xmlinput .= "<tollFree>".$arrayData["groupPermissionsTollfree"]."</tollFree>";
                }
                
                if(isset($arrayData["groupPermissionsToll"]) && !empty($arrayData["groupPermissionsToll"])){
                    $xmlinput .= "<toll>".$arrayData["groupPermissionsToll"]."</toll>";
                }
                
                if(isset($arrayData["groupPermissionsInternational"]) && !empty($arrayData["groupPermissionsInternational"])){
                    $xmlinput .= "<international>".$arrayData["groupPermissionsInternational"]."</international>";
                }
                
                if(isset($arrayData["groupPermissionsOperatorAssisted"]) && !empty($arrayData["groupPermissionsOperatorAssisted"])){
                    $xmlinput .= "<operatorAssisted>".$arrayData["groupPermissionsOperatorAssisted"]."</operatorAssisted>";
                }
                
                if(isset($arrayData["groupPermissionsOperatorChargDirAssisted"]) && !empty($arrayData["groupPermissionsOperatorChargDirAssisted"])){
                    $xmlinput .= "<chargeableDirectoryAssisted>".$arrayData["groupPermissionsOperatorChargDirAssisted"]."</chargeableDirectoryAssisted>";
                }
                
                if(isset($arrayData["groupPermissionsSpecialService1"]) && !empty($arrayData["groupPermissionsSpecialService1"])){
                    $xmlinput .= "<specialServicesI>".$arrayData["groupPermissionsSpecialService1"]."</specialServicesI>";
                }
                
                if(isset($arrayData["groupPermissionsSpecialService2"]) && !empty($arrayData["groupPermissionsSpecialService2"])){
                    $xmlinput .= "<specialServicesII>".$arrayData["groupPermissionsSpecialService2"]."</specialServicesII>";
                }
                
                if(isset($arrayData["groupPermissionsPremiumServices1"]) && !empty($arrayData["groupPermissionsPremiumServices1"])){
                    $xmlinput .= "<premiumServicesI>".$arrayData["groupPermissionsPremiumServices1"]."</premiumServicesI>";
                }
                
                if(isset($arrayData["groupPermissionsPremiumServices2"]) && !empty($arrayData["groupPermissionsPremiumServices2"])){
                    $xmlinput .= "<premiumServicesII>".$arrayData["groupPermissionsPremiumServices2"]."</premiumServicesII>";
                }
                
                if(isset($arrayData["groupPermissionsCasual"]) && !empty($arrayData["groupPermissionsCasual"])){
                    $xmlinput .= "<casual>".$arrayData["groupPermissionsCasual"]."</casual>";
                }
                
                if(isset($arrayData["groupPermissionsUrlDialing"]) && !empty($arrayData["groupPermissionsUrlDialing"])){
                    $xmlinput .= "<urlDialing>".$arrayData["groupPermissionsUrlDialing"]."</urlDialing>";
                }
                
                if(isset($arrayData["groupPermissionsUnknown"]) && !empty($arrayData["groupPermissionsUnknown"])){
                    $xmlinput .= "<unknown>".$arrayData["groupPermissionsUnknown"]."</unknown>";
                }                
         
            $xmlinput .= "</groupPermissions>";        
        
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            if (readErrorXmlGenuine($xml) != "") {
                $ocpModResponse["Error"] = strval($xml->command->summaryEnglish);
            }else{
                $ocpModResponse["Success"] = "Success";
            }

            return $ocpModResponse;
        
    }
    //End code
    
    function getDevicesByUsingUserId($userId){
        global  $sessionid, $client;
        $ocpResponse["Error"] = array();
        $ocpResponse["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "UserPrimaryAndSCADeviceGetListRequest");
        $xmlinput .= "<userId>".$userId."</userId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $ocpResponse["Error"] = strval($xml->command->summaryEnglish);
        }else{
            //$ocpResponse["Success"] = "Success";
            if(isset($xml->command->accessDeviceTable->row) && count($xml->command->accessDeviceTable->row) > 0){
                
                foreach ($xml->command->accessDeviceTable->row as $key => $value){
                    //print_r($value->col[0]);
                    $ocpResponse["Success"][strval($value->col[0])]["userName"] = strval($value->col[5]);
                    $ocpResponse["Success"][strval($value->col[0])]["password"] = strval($value->col[6]);
                }
            }
        }
        
        return $ocpResponse;
    }
    
    
}
?>