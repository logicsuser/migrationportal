<?php
    require_once ("/var/www/lib/broadsoft/adminPortal/services/Services.php");
    
    const ExpressCustomProfileName = "%Express_Custom_Profile_Name%";
    const ExpressTagBundleName = "%Express_Tag_Bundle%";

	unset($_SESSION["userInfo"]);

	if ($ociVersion == "17")
	{
		$xmlinput = xmlHeader($sessionid, "UserGetRequest17sp4");
	}
	else if ($ociVersion == "20")
	{
		$xmlinput = xmlHeader($sessionid, "UserGetRequest20");
	}
        else if ($ociVersion == "21")
	{
		$xmlinput = xmlHeader($sessionid, "UserGetRequest21sp1");
	}
        else if ($ociVersion == "22")
	{
		$xmlinput = xmlHeader($sessionid, "UserGetRequest21sp1");
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "UserGetRequest19");
	}
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	        
	foreach ($xml->command->attributes() as $a => $b)
	{
		if ($b == "Error")
		{
			$userInfo["error"] = "true";
		}
	}

	$userInfo["userId"] = $userId;
	$userInfo["deviceName"] = isset($xml->command->accessDeviceEndpoint->accessDevice->deviceName) ? strval($xml->command->accessDeviceEndpoint->accessDevice->deviceName) : "";
	$userInfo["firstName"] = strval($xml->command->firstName);
	$userInfo["lastName"] = strval($xml->command->lastName);
	$userInfo["addressLine1"] = strval($xml->command->address->addressLine1);
	$userInfo["addressLine2"] = strval($xml->command->address->addressLine2);
	$userInfo["city"] = strval($xml->command->address->city);
	$userInfo["stateOrProvince"] = strval($xml->command->address->stateOrProvince);
	$userInfo["zipOrPostalCode"] = strval($xml->command->address->zipOrPostalCode);
	$userInfo["emailAddress"] = strval($xml->command->emailAddress);
        $userInfo["addressLocation"] = strval($xml->command->addressLocation);
	$userInfo["phoneNumber"] = strval($xml->command->phoneNumber);
	$userInfo["callingLineIdLastName"] = strval($xml->command->callingLineIdLastName);
	$userInfo["callingLineIdFirstName"] = strval($xml->command->callingLineIdFirstName);	
	$userInfo["callingLineIdPhoneNumber"] = strval($xml->command->callingLineIdPhoneNumber);
	$userInfo["extension"] = strval($xml->command->extension);
	$userInfo["timeZone"] = strval($xml->command->timeZone);
	$userInfo["networkClassOfService"] = isset($xml->command->networkClassOfService) ? strval($xml->command->networkClassOfService) : "";
	$userInfo["department"] = strval($xml->command->department->name);
        
        if($ociVersion=="20" || $ociVersion=="21" || $ociVersion=="22"){
            $userInfo["nameDialingFirstName"] = strval($xml->command->nameDialingName->nameDialingFirstName); // code added 23-10-2018 Sollogics Developer
            $userInfo["nameDialingLastName"]  = strval($xml->command->nameDialingName->nameDialingLastName);
            $userInfo["countryCode"]          = strval($xml->command->countryCode);      //Code added @ 16 Jan 2019
            $userInfo["nationalPrefix"]       = strval($xml->command->nationalPrefix);   //Code added @ 16 Jan 2019
        }

    $userInfo["linePort"] = isset($xml->command->accessDeviceEndpoint->linePort) ? strval($xml->command->accessDeviceEndpoint->linePort) : "";
    $userInfo["linePortDomainResult"] = "";
    if ($userInfo["linePort"] != "") {
        $linePortDomain = explode("@", $userInfo["linePort"]);
        $userInfo["linePortDomainResult"] = $linePortDomain[1];
    }
	$userInfo["portNumber"] = isset($xml->command->accessDeviceEndpoint->portNumber) ? strval($xml->command->accessDeviceEndpoint->portNumber) : "";


	$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetRequest18sp1");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<deviceName>" . $userInfo["deviceName"] . "</deviceName>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$userInfo["deviceType"] = strval($xml->command->deviceType);
	$userInfo["macAddress"] = strval($xml->command->macAddress);

	$userInfo["deviceIsAnalog"] = "false";
            if (isset($userInfo["deviceType"]) && $userInfo["deviceType"] != "") {
            // shortcut custom device list for Audio Codes until they are resolved
            $deviceIsAudioCodes = substr($userInfo["deviceType"], 0, strlen("AudioCodes-")) == "AudioCodes-";
            require_once("/var/www/lib/broadsoft/adminPortal/DBLookup.php");
            $sipGatewayLookup = new DBLookup($db, "systemDevices","deviceType", "phoneType");
            if ($deviceIsAudioCodes || $sipGatewayLookup->get($value) == "Analog") {
                $userInfo["deviceIsAnalog"] = "true";
            }
        }

    //code added @ 12 Sep 2018 get the custom contact directory info      
    if(isset($userInfo["deviceName"]) && $userInfo["deviceName"]!="")
    {
        $xmlinput  = xmlHeader($sessionid, "UserPolycomPhoneServicesGetRequest");        
	$xmlinput .= "<userId>" . htmlspecialchars($userInfo["userId"]) . "</userId>";
	$xmlinput .= "<accessDevice>
                            <deviceLevel>Group</deviceLevel>
                            <deviceName>" . $userInfo["deviceName"] . "</deviceName>
                    </accessDevice>";
	$xmlinput .= xmlFooter();
	$response  = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml       = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $userInfo["ccd"] = strval($xml->command->groupCustomContactDirectory);
        $userInfo["customContactDirectory"] = strval($xml->command->groupCustomContactDirectory); 
    }
    //End code
    
    //server_fail_over_debuggin_testing(); /* for fail Over testing. */
    // get Custom Profile associated with this user
    $userInfo["customProfile"] = "None";
    $userInfo["modTagBundle"][0] = "";
    if ($userInfo["deviceName"] != "") {
        require_once("/var/www/html/Express/userMod/UserCustomTagManager.php");
        $customTags = new UserCustomTagManager($userInfo["deviceName"],$userInfo["deviceType"]);
        $userInfo["customProfile"] = $customTags->getCustomTagValue(ExpressCustomProfileName);
        if ($userInfo["customProfile"] == "") {
            $userInfo["customProfile"] = "None";
        }
        $bundleArray = $customTags->getCustomTagValue(ExpressTagBundleName);
        if($bundleArray != ""){
            $explodedVal = explode(";", $bundleArray);
            $k = 0;
            foreach ($explodedVal as $keyB => $valueB){
                $userInfo["modTagBundle"][$k] = $valueB;
                $k++;
            }
        }
        
        
    }


	//check if more than one user is associated with device
	$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetUserListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<deviceName>" . $userInfo["deviceName"] . "</deviceName>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	if (count($xml->command->deviceUserTable->row) > 1)
	{
		$userInfo["sharedCallAppearances"] = "true";
	}
	if (count($xml->command->deviceUserTable->row) > 0) {
		foreach ($xml->command->deviceUserTable->row as $key => $value) {
		    $userInfo["sharedCallAppearanceUsers"][$a]["id"] = $userInfo["devicePortAssignmentDetail"][$a]["id"] = strval($value->col[4]);
		    $userInfo["sharedCallAppearanceUsers"][$a]["name"] = $userInfo["devicePortAssignmentDetail"][$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
		    $userInfo["sharedCallAppearanceUsers"][$a]["phoneNumber"] = $userInfo["devicePortAssignmentDetail"][$a]["phoneNumber"] = strval($value->col[3]);
		    $userInfo["sharedCallAppearanceUsers"][$a]["extension"] = $userInfo["devicePortAssignmentDetail"][$a]["extension"] = strval($value->col[9]);
		    $userInfo["sharedCallAppearanceUsers"][$a]["department"] = $userInfo["devicePortAssignmentDetail"][$a]["department"] = strval($value->col[10]);
		    $userInfo["sharedCallAppearanceUsers"][$a]["linePort"] = $userInfo["devicePortAssignmentDetail"][$a]["linePort"] = strval($value->col[0]);
		    $userInfo["sharedCallAppearanceUsers"][$a]["port"] = $userInfo["devicePortAssignmentDetail"][$a]["port"] = strval($value->col[7]);
		    $userInfo["sharedCallAppearanceUsers"][$a]["endpointType"] = $userInfo["devicePortAssignmentDetail"][$a]["endpointType"] = strval($value->col[6]);
		    $userInfo["sharedCallAppearanceUsers"][$a]["primary"] = $userInfo["devicePortAssignmentDetail"][$a]["primary"] = strval($value->col[8]);
			if ($value->col[8] == "true") {
				$userInfo["primary"] = strval($value->col[0]);
			}
			$a++;
		}
	}

	$xmlinput = xmlHeader($sessionid, "UserHotelingGuestGetRequest14sp4");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$userInfo["hotelHostName"] = strval($xml->command->hostLastName) . ", " . strval($xml->command->hostFirstName);
	$userInfo["hotelHostId"] = strval($xml->command->hostUserId);
	$userInfo["hotelisActive"] = strval($xml->command->isActive);
	$userInfo["associationLimitHours"] = strval($xml->command->associationLimitHours);

	$xmlinput = xmlHeader($sessionid, "UserHotelingHostGetRequest17");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$userInfo["hotelHostisActive"] = strval($xml->command->isActive);

	$xmlinput = xmlHeader($sessionid, "UserCallForwardingAlwaysGetRequest");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= "</command>";
	$xmlinput .= "<command xsi:type=\"UserCallForwardingBusyGetRequest\" xmlns=\"\">";
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= "</command>";
	$xmlinput .= "<command xsi:type=\"UserCallForwardingNoAnswerGetRequest13mp16\" xmlns=\"\">";
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= "</command>";
	$xmlinput .= "<command xsi:type=\"UserCallForwardingNotReachableGetRequest\" xmlns=\"\">";
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	
	$userInfo["cfaActive"] = strval($xml->command[0]->isActive);
	$userInfo["cfbActive"] = strval($xml->command[1]->isActive);
	$userInfo["cfnActive"] = strval($xml->command[2]->isActive);
	$userInfo["cfrActive"] = strval($xml->command[3]->isActive);
	$userInfo["cfaForwardToPhoneNumber"] = strval($xml->command[0]->forwardToPhoneNumber);
	$userInfo["cfbForwardToPhoneNumber"] = strval($xml->command[1]->forwardToPhoneNumber);
	$userInfo["cfnForwardToPhoneNumber"] = strval($xml->command[2]->forwardToPhoneNumber);
	$userInfo["cfrForwardToPhoneNumber"] = strval($xml->command[3]->forwardToPhoneNumber);
	$userInfo["cfaIsRingSplashActive"] = strval($xml->command[0]->isRingSplashActive);
	$userInfo["cfnNumberOfRings"] = strval($xml->command[2]->numberOfRings);

	$xmlinput = xmlHeader($sessionid, "UserDoNotDisturbGetRequest");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	
	$userInfo["dnd"] = strval($xml->command->isActive);
	$userInfo["dndRingSplash"] = strval($xml->command->ringSplash);
	if ($userInfo["dnd"] !== "")
	{
		$_SESSION["userInfo"]["callControlTab"] = "true";
	}

	$xmlinput = xmlHeader($sessionid, "UserBusyLampFieldGetRequest16sp2");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	if (isset($xml->command->monitoredUserTable->row))
	{
		foreach ($xml->command->monitoredUserTable->row as $key => $value)
		{
			$userInfo["monitoredUsers"][$a]["id"] = strval($value->col[0]);
			$userInfo["monitoredUsers"][$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
			$userInfo["monitoredUsers"][$a]["phoneNumber"] = strval($value->col[5]);
			$userInfo["monitoredUsers"][$a]["extension"] =  strval($value->col[6]);
			
			$a++;
		}
	}

// Removing sorting of BLF list
/*
	if (isset($userInfo["monitoredUsers"]))
	{
		$userInfo["monitoredUsers"] = subval_sort($userInfo["monitoredUsers"], "name");
	}
 */

	$xmlinput = xmlHeader($sessionid, "UserServiceGetAssignmentListRequest");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	
	$userInfo["servicePacksAssigned"][0] = "";
	$a = 0;
	if (isset($xml->command->servicePacksAssignmentTable->row))
	{
		foreach ($xml->command->servicePacksAssignmentTable->row as $key => $value)
		{
			if ($value->col[1] == "true")
			{
				$userInfo["servicePacksAssigned"][$a] = strval($value->col[0]);
				$a++;
			}
		}
	}
	
	$userInfo["servicesAssigned"][0] = "";
	$b = 0;
	if (isset($xml->command->userServicesAssignmentTable->row))
	{
	    foreach ($xml->command->userServicesAssignmentTable->row as $key => $value)
	    {
	        if ($value->col[1] == "true")
	        {
	            $userInfo["servicesAssigned"][$b] = strval($value->col[0]);
	            $b++;
	        }
	    }
	}
	
	require_once ("/var/www/lib/broadsoft/adminPortal/users/VMUsersServices.php");
	$vmAdvancedObj = new VMUsersServices();
	$vmAdvancedGetResp = $vmAdvancedObj->modifyUsersVoiceMessagingAdvancedGetRequest($userId);
	if(isset($vmAdvancedGetResp["Error"]) && $vmAdvancedGetResp["Error"] == ""){
	    $userInfo["mailBoxLimit"] = strval($vmAdvancedGetResp["Success"][0]);
	}
	
	if(count($userInfo["servicePacksAssigned"]) > 0){
	    $servicesInServicePack = array();
	    require_once ("/var/www/lib/broadsoft/adminPortal/services/userServices.php");
	    $userObj = new userServices();
	    foreach ($userInfo["servicePacksAssigned"] as $servicepackName){
	        $srvicePackServices = $userObj->getServicePackServices($servicepackName, $_SESSION["sp"]);
	        if(count($srvicePackServices) > 0){
	            foreach ($srvicePackServices as $value){
	                $servicesInServicePack[] = $value;
	            }
	        }
	    }
	    $defaultArray = array("Call Forwarding Always", "Call Forwarding Busy", "Call Forwarding No Answer", "Call Forwarding Not Reachable", "Do Not Disturb", "Call Transfer");
	    if(count($servicesInServicePack) > 0){
	        foreach ($servicesInServicePack as $val){
	            if(in_array($val, $defaultArray)){
	                $_SESSION["userInfo"]["callControlTab"] = "true";
	            }
	        }
	    }
	}
	
	//echo "<pre>"; print_r($userInfo["servicePacksAssigned"]); die;
	foreach ($userInfo as $key => $value)
	{
		if (is_array($value))
		{
			foreach ($value as $k => $v)
			{
				$_SESSION["userInfo"][$key][$k] = $v;
			}
		}
		else
		{
			$_SESSION["userInfo"][$key] = $value;
		}
	}

	//get Speed Dial 8 info
	$xmlinput = xmlHeader($sessionid, "UserSpeedDial8GetListRequest");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	for ($i = 0; $i <= 7; $i++)
	{
		$_SESSION["userInfo"]["speedDial8"][$i + 2]["phoneNumber"] = isset($xml->command->speedDialEntry[$i]->phoneNumber) ? strval($xml->command->speedDialEntry[$i]->phoneNumber) : "";
		$_SESSION["userInfo"]["speedDial8"][$i + 2]["name"] = isset($xml->command->speedDialEntry[$i]->description) ? strval($xml->command->speedDialEntry[$i]->description) : "";
	}

	//get Speed Dial 100 info
	$xmlinput = xmlHeader($sessionid, "UserSpeedDial100GetListRequest17sp1");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->speedDialEntry as $key => $value)
	{
		$_SESSION["userInfo"]["speedDial100"][$a]["speedCode"] = strval($value->speedCode);
		$_SESSION["userInfo"]["speedDial100"][$a]["phone"] = strval($value->phoneNumber);
		$_SESSION["userInfo"]["speedDial100"][$a]["description"] = strval($value->description);
		$a++;
	}

	//get Voice Management info
	$xmlinput = xmlHeader($sessionid, "UserAssignedServicesGetListRequest");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
              
        
	for ($i = 0; $i < count($xml->command->userServiceEntry); $i++)
	{
		if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Voice Messaging User")
		{
			$_SESSION["userInfo"]["voiceMessagingUser"] = "true";
		}
                if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Busy Lamp Field")
                {
                    $_SESSION["userInfo"]["busyLampField"] = "true";
                }
		if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Call Forwarding Always")
		{
			$_SESSION["userInfo"]["cfa"] = "true";
			$_SESSION["userInfo"]["callControlTab"] = "true";
		}
		if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Call Forwarding Busy")
		{
			$_SESSION["userInfo"]["cfb"] = "true";
			$_SESSION["userInfo"]["callControlTab"] = "true";
		}
		if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Call Forwarding No Answer")
		{
			$_SESSION["userInfo"]["cfn"] = "true";
			$_SESSION["userInfo"]["callControlTab"] = "true";
		}
		if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Call Forwarding Not Reachable")
		{
			$_SESSION["userInfo"]["cfr"] = "true";
			$_SESSION["userInfo"]["callControlTab"] = "true";
		}
		if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Call Transfer")
		{
		    $_SESSION["userInfo"]["callControlTab"] = "true";
		}
		if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Do Not Disturb")
		{
		    $_SESSION["userInfo"]["callControlTab"] = "true";
		}
		if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Polycom Phone Services")
		{
			$_SESSION["userInfo"]["polycomPhoneServices"] = "true";
		}
                //Code added @ 20 Nov 2018
                if (strval($xml->command->userServiceEntry[$i]->serviceName) == "Simultaneous Ring Personal")
		{
			$_SESSION["userInfo"]["simRingService"] = "true";
		}
	}

	$srvceObj = new Services();
	$cLIDBlockRes = $srvceObj->userCallingLineIDDeliveryBlockingGetRequest($userId);
	$_SESSION["userInfo"]["userCallingLineIdBlockingOverride"] = empty($cLIDBlockRes["Error"]) ? $cLIDBlockRes["Success"]["isActive"] : "false";
	
	$xmlinput = xmlHeader($sessionid, "UserThirdPartyVoiceMailSupportGetRequest17");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$thirdParty = strval($xml->command->isActive); //must be true for Third Party users

	$xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetVoiceManagementRequest17");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$voiceManagement = strval($xml->command->isActive); //must be false for Third Party users

	if ($thirdParty == "true" and $voiceManagement == "false")
	{
		$_SESSION["userInfo"]["thirdPartyVoiceMail"] = "true";
	}

	//get Third Party or Voice Management info
	if (isset($_SESSION["userInfo"]["thirdPartyVoiceMail"]) and $_SESSION["userInfo"]["thirdPartyVoiceMail"] == "true")
	{
		$xmlinput = xmlHeader($sessionid, "UserThirdPartyVoiceMailSupportGetRequest17");
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetVoiceManagementRequest17");
	}
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$_SESSION["userInfo"]["isActive"] = strval($xml->command->isActive);
	$_SESSION["userInfo"]["alwaysRedirectToVoiceMail"] = strval($xml->command->alwaysRedirectToVoiceMail);
	$_SESSION["userInfo"]["busyRedirectToVoiceMail"] = strval($xml->command->busyRedirectToVoiceMail);
	$_SESSION["userInfo"]["noAnswerRedirectToVoiceMail"] = strval($xml->command->noAnswerRedirectToVoiceMail);
	if (isset($_SESSION["userInfo"]["thirdPartyVoiceMail"]) and $_SESSION["userInfo"]["thirdPartyVoiceMail"] == "true")
	{
		$_SESSION["userInfo"]["serverSelection"] = strval($xml->command->serverSelection);
		$_SESSION["userInfo"]["userServer"] = strval($xml->command->userServer);
		$_SESSION["userInfo"]["mailboxIdType"] = strval($xml->command->mailboxIdType);
		$_SESSION["userInfo"]["mailboxURL"] = strval($xml->command->mailboxURL);
		$_SESSION["userInfo"]["noAnswerNumberOfRings"] = strval($xml->command->noAnswerNumberOfRings);
	}
	else
	{
		$_SESSION["userInfo"]["processing"] = strval($xml->command->processing);
		$_SESSION["userInfo"]["voiceMessageDeliveryEmailAddress"] = strval($xml->command->voiceMessageDeliveryEmailAddress);
		$_SESSION["userInfo"]["usePhoneMessageWaitingIndicator"] = strval($xml->command->usePhoneMessageWaitingIndicator);
		$_SESSION["userInfo"]["sendVoiceMessageNotifyEmail"] = strval($xml->command->sendVoiceMessageNotifyEmail);
		$_SESSION["userInfo"]["voiceMessageNotifyEmailAddress"] = strval($xml->command->voiceMessageNotifyEmailAddress);
		$_SESSION["userInfo"]["sendCarbonCopyVoiceMessage"] = strval($xml->command->sendCarbonCopyVoiceMessage);
		$_SESSION["userInfo"]["voiceMessageCarbonCopyEmailAddress"] = strval($xml->command->voiceMessageCarbonCopyEmailAddress);
		$_SESSION["userInfo"]["transferOnZeroToPhoneNumber"] = strval($xml->command->transferOnZeroToPhoneNumber);
		$_SESSION["userInfo"]["transferPhoneNumber"] = strval($xml->command->transferPhoneNumber);
	}

	//get Call Transfer info
	$xmlinput = xmlHeader($sessionid, "UserCallTransferGetRequest14sp4");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$_SESSION["userInfo"]["isRecallActive"] = strval($xml->command->isRecallActive);
	$_SESSION["userInfo"]["recallNumberOfRings"] = strval($xml->command->recallNumberOfRings);
	$_SESSION["userInfo"]["useDiversionInhibitorForBlindTransfer"] = strval($xml->command->useDiversionInhibitorForBlindTransfer);
	$_SESSION["userInfo"]["useDiversionInhibitorForConsultativeCalls"] = strval($xml->command->useDiversionInhibitorForConsultativeCalls);
	$_SESSION["userInfo"]["enableBusyCampOn"] = strval($xml->command->enableBusyCampOn);
	$_SESSION["userInfo"]["busyCampOnSeconds"] = strval($xml->command->busyCampOnSeconds);

	//get Simultaneous Ring info
	$xmlinput = xmlHeader($sessionid, "UserSimultaneousRingPersonalGetRequest17");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$_SESSION["userInfo"]["simultaneousRingIsActive"] = strval($xml->command->isActive);
	$_SESSION["userInfo"]["doNotRingIfOnCall"] = strval($xml->command->doNotRingIfOnCall);
	for ($i = 0; $i < 10; $i++)
	{
		if (isset($xml->command->simultaneousRingNumber[$i])) {
			$_SESSION["userInfo"]["simultaneousRingNumber"][$i]["phoneNumber"] = strval($xml->command->simultaneousRingNumber[$i]->phoneNumber);
			$_SESSION["userInfo"]["simultaneousRingNumber"][$i]["answerConfirmationRequired"] = strval($xml->command->simultaneousRingNumber[$i]->answerConfirmationRequired);
		}
	}

	// get Call Processing policy info
	if ($ociVersion == "17") {
		$xmlinput = xmlHeader($sessionid, "UserCallProcessingGetPolicyRequest17sp4");
	}
	elseif ($ociVersion == "20") {
		$xmlinput = xmlHeader($sessionid, "UserCallProcessingGetPolicyRequest19sp1");
	}
	else {
		$xmlinput = xmlHeader($sessionid, "UserCallProcessingGetPolicyRequest18");
	}

	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	//$_SESSION["userInfo"]["useUserCLIDSetting"] = strval($xml->command->useUserCLIDSetting);
	//$_SESSION["userInfo"]["clidPolicy"] = strval($xml->command->clidPolicy);
	
	if(!empty($_SESSION["userInfo"]["phoneNumber"])){
		require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
		
		$ccDn = new Dns ();
		$numberActivateResponse = $ccDn->getUserDNActivateListRequest ( $userId );
		//echo "<pre>"; print_r($numberActivateResponse); die;
		if (empty ( $numberActivateResponse ['Error'] )) {
			if (! empty ( $numberActivateResponse ['Success'] [0] ['phoneNumber'] )) {
				$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
			}
			if (! empty ( $numberActivateResponse ['Success'] [0] ['status'] )) {
				$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
			}
		}
	}else{
		$phoneNumberStatus = "No";
	}
	$_SESSION["userInfo"]["uActivateNumber"] = $phoneNumberStatus;
	
	
	require_once ("/var/www/lib/broadsoft/adminPortal/GetUserCallProcessingInfo.php");	
	$userCall = new GetUserCallProcessingInfo(); 
	
	$userCallProcessingRequest = $userCall->getUserCallProcessingPolicy($userId);
	//echo "<pre>"; print_r($userCallProcessingRequest["Success"]);
	if(empty($userCallProcessingRequest["Error"])){
		$_SESSION["userInfo"]["useUserSetting"] = strval($userCallProcessingRequest["Success"]->useUserSetting);
		$_SESSION["userInfo"]["useMaxSimultaneousCalls"] = strval($userCallProcessingRequest["Success"]->useMaxSimultaneousCalls);
		$_SESSION["userInfo"]["maxSimultaneousCalls"] = strval($userCallProcessingRequest["Success"]->maxSimultaneousCalls);
		$_SESSION["userInfo"]["useMaxSimultaneousVideoCalls"] = strval($userCallProcessingRequest["Success"]->useMaxSimultaneousVideoCalls);
		$_SESSION["userInfo"]["maxSimultaneousVideoCalls"] = strval($userCallProcessingRequest["Success"]->maxSimultaneousVideoCalls);
		$_SESSION["userInfo"]["useMaxCallTimeForAnsweredCalls"] = strval($userCallProcessingRequest["Success"]->useMaxCallTimeForAnsweredCalls);
		$_SESSION["userInfo"]["maxCallTimeForAnsweredCallsMinutes"] = strval($userCallProcessingRequest["Success"]->maxCallTimeForAnsweredCallsMinutes);
		$_SESSION["userInfo"]["useMaxCallTimeForUnansweredCalls"] = strval($userCallProcessingRequest["Success"]->useMaxCallTimeForUnansweredCalls);
		$_SESSION["userInfo"]["maxCallTimeForUnansweredCallsMinutes"] = strval($userCallProcessingRequest["Success"]->maxCallTimeForUnansweredCallsMinutes);
		$_SESSION["userInfo"]["mediaPolicySelection"] = strval($userCallProcessingRequest["Success"]->mediaPolicySelection);
		$_SESSION["userInfo"]["useMaxConcurrentRedirectedCalls"] = strval($userCallProcessingRequest["Success"]->useMaxConcurrentRedirectedCalls);
		$_SESSION["userInfo"]["maxConcurrentRedirectedCalls"] = strval($userCallProcessingRequest["Success"]->maxConcurrentRedirectedCalls);
		$_SESSION["userInfo"]["useMaxFindMeFollowMeDepth"] = strval($userCallProcessingRequest["Success"]->useMaxFindMeFollowMeDepth);
		$_SESSION["userInfo"]["maxFindMeFollowMeDepth"] = strval($userCallProcessingRequest["Success"]->maxFindMeFollowMeDepth);
		$_SESSION["userInfo"]["maxRedirectionDepth"] = strval($userCallProcessingRequest["Success"]->maxRedirectionDepth);
		$_SESSION["userInfo"]["useMaxConcurrentFindMeFollowMeInvocations"] = strval($userCallProcessingRequest["Success"]->useMaxConcurrentFindMeFollowMeInvocations);
		$_SESSION["userInfo"]["maxConcurrentFindMeFollowMeInvocations"] = strval($userCallProcessingRequest["Success"]->maxConcurrentFindMeFollowMeInvocations);
		$_SESSION["userInfo"]["clidPolicy"] = strval($userCallProcessingRequest["Success"]->clidPolicy);
		$_SESSION["userInfo"]["emergencyClidPolicy"] = strval($userCallProcessingRequest["Success"]->emergencyClidPolicy);
		$_SESSION["userInfo"]["allowDepartmentCLIDNameOverride"] = strval($userCallProcessingRequest["Success"]->allowDepartmentCLIDNameOverride);
		$_SESSION["userInfo"]["useUserCLIDSetting"] = strval($userCallProcessingRequest["Success"]->useUserCLIDSetting);
		$_SESSION["userInfo"]["useGroupName"] = strval($userCallProcessingRequest["Success"]->useGroupName);
		$_SESSION["userInfo"]["allowAlternateNumbersForRedirectingIdentity"] = strval($userCallProcessingRequest["Success"]->allowAlternateNumbersForRedirectingIdentity);
		$_SESSION["userInfo"]["allowConfigurableCLIDForRedirect"] = strval($userCallProcessingRequest["Success"]->allowConfigurableCLIDForRedirect);
		$_SESSION["userInfo"]["blockCallingNameForExternalCalls"] = strval($userCallProcessingRequest["Success"]->blockCallingNameForExternalCalls);
		$_SESSION["userInfo"]["allowConfigurableCLIDForRedirectingIdentity"] = strval($userCallProcessingRequest["Success"]->allowConfigurableCLIDForRedirectingIdentity);
		$_SESSION["userInfo"]["enableDialableCallerID"] = strval($userCallProcessingRequest["Success"]->enableDialableCallerID);
		$_SESSION["userInfo"]["useUserCallLimitsSetting"] = strval($userCallProcessingRequest["Success"]->useUserCallLimitsSetting);
		$_SESSION["userInfo"]["useUserDCLIDSetting"] = strval($userCallProcessingRequest["Success"]->useUserDCLIDSetting);
		//echo "<pre>"; print_r($_SESSION["userInfo"]["useUserSetting"]); die;
	}
	
	$_SESSION["userInfo"]["useCustomSettings"] = "false";
	require_once ("/var/www/lib/broadsoft/adminPortal/ocpFeatures/OCPOperations.php");
	$objOCP = new OCPOperations();
	$ocpData = $objOCP->UserCallingPlanGetOriginatingRequest($userId);
	if(empty($ocpData["Error"])){
	    $_SESSION["userInfo"]["useCustomSettings"] = $ocpData["Success"]["useCustomSettings"];
	    
	    $_SESSION["userInfo"]["userPermissionsGroup"] = $ocpData["Success"]["userPermissions"]["group"];
	    $_SESSION["userInfo"]["userPermissionsLocal"] = $ocpData["Success"]["userPermissions"]["local"];
	    $_SESSION["userInfo"]["userPermissionsTollfree"] = $ocpData["Success"]["userPermissions"]["tollFree"];
	    $_SESSION["userInfo"]["userPermissionsToll"] = $ocpData["Success"]["userPermissions"]["toll"];
	    
	    $_SESSION["userInfo"]["userPermissionsInternational"] = $ocpData["Success"]["userPermissions"]["international"];
	    $_SESSION["userInfo"]["userPermissionsOperatorAssisted"] = $ocpData["Success"]["userPermissions"]["operatorAssisted"];
	    $_SESSION["userInfo"]["userPermissionsOperatorChargDirAssisted"] = $ocpData["Success"]["userPermissions"]["chargeableDirectoryAssisted"];
	    $_SESSION["userInfo"]["userPermissionsSpecialService1"] = $ocpData["Success"]["userPermissions"]["specialServicesI"];
	    
	    $_SESSION["userInfo"]["userPermissionsSpecialService2"] = $ocpData["Success"]["userPermissions"]["specialServicesII"];
	    $_SESSION["userInfo"]["userPermissionsPremiumServices1"] = $ocpData["Success"]["userPermissions"]["premiumServicesI"];
	    $_SESSION["userInfo"]["userPermissionsPremiumServices2"] = $ocpData["Success"]["userPermissions"]["premiumServicesII"];
	    $_SESSION["userInfo"]["userPermissionsCasual"] = $ocpData["Success"]["userPermissions"]["casual"];
	    
	    $_SESSION["userInfo"]["userPermissionsUrlDialing"] = $ocpData["Success"]["userPermissions"]["urlDialing"];
	    $_SESSION["userInfo"]["userPermissionsUnknown"] = $ocpData["Success"]["userPermissions"]["unknown"];
	    
	}

	$userInfo = array_merge($userInfo, $_SESSION["userInfo"]);
	
?>
