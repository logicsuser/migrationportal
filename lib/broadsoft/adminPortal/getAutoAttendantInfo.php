<?php
	unset($_SESSION["aa"]);

	if ($ociVersion == "19")
	{
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantGetInstanceRequest19");
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantGetInstanceRequest20");			
	}
	$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	//echo "<pre>"; print_r($xml);
	
	if (readErrorXmlGenuine($xml) != "") {
		if(!empty($xml->command->detail)){
			$detail = strval($xml->command->detail);
			$error = explode("<command", $detail);
			$error_detail = $error[0];
		}else{
			$error_detail = $xml->command->summaryEnglish;
		}
		//echo "<span style='text-align:center; display:block; color:red'>Error : ".$error_detail.". Please contact with customer support.</span>"; die;
		echo "<span style='text-align:center; display:block; color:red' class='broadSoftRelError'>Express cannot currently process AA operations. Please contact Customer Service. <br/>Error : ".$error_detail."</span>"; die;
	}

	$_SESSION["aa"]["aaName"] = strval($xml->command->serviceInstanceProfile->name);
	$_SESSION["aa"]["callingLineIdLastName"] = strval($xml->command->serviceInstanceProfile->callingLineIdLastName);
	$_SESSION["aa"]["callingLineIdFirstName"] = strval($xml->command->serviceInstanceProfile->callingLineIdFirstName);
	$_SESSION["aa"]["phoneNumber"] = strval($xml->command->serviceInstanceProfile->phoneNumber);
	$_SESSION["aa"]["extension"] = strval($xml->command->serviceInstanceProfile->extension);
	
	if($ociVersion == "21" || $ociVersion == "21"){
		$_SESSION["aa"]["firstDigitTimeoutSeconds"] = strval($xml->command->firstDigitTimeoutSeconds);
	}
	
	$_SESSION["aa"]["timeZone"] = strval($xml->command->serviceInstanceProfile->timeZone);
	if ($ociVersion == "17")
	{
		$_SESSION["aa"]["type"] = "Basic";
	}
	else
	{
		$_SESSION["aa"]["type"] = strval($xml->command->type);
	}
	$_SESSION["aa"]["businessHoursSchedule"] = strval($xml->command->businessHours->name);
	$_SESSION["aa"]["holidaySchedule"] = strval($xml->command->holidaySchedule->name);
	$_SESSION["aa"]["nameDialingEntries"] = strval($xml->command->nameDialingEntries);
	$_SESSION["aa"]["extensionDialingScope"] = strval($xml->command->extensionDialingScope);
	$_SESSION["aa"]["nameDialingScope"] = strval($xml->command->nameDialingScope);
	$_SESSION["aa"]["eVSupport"] = strval($xml->command->enableVideo);
	$_SESSION["aa"]["department"] = strval($xml->command->serviceInstanceProfile->department->name);

	$_SESSION["aa"]["bh"]["announcementSelection"] = strval($xml->command->businessHoursMenu->announcementSelection);
	
	//code to get the value from database for announcement of auto attendant
	$announcementAutoattendantId = $announcement->getAutoattendantAnnouncementInfo($db, $aaId, 'bhAnnouncementId');
	$_SESSION["aa"]["bh"]["announcementId"] = $announcementAutoattendantId;
	
	
//	$_SESSION["aa"]["bh"]["audioFileDescription"] = strval($xml->command->businessHoursMenu->audioFileDescription);
	$_SESSION["aa"]["bh"]["enableLevelExtensionDialing"] = strval($xml->command->businessHoursMenu->enableFirstMenuLevelExtensionDialing);
	foreach ($xml->command->businessHoursMenu->keyConfiguration as $key => $value)
	{
		$_SESSION["aa"]["bh"]["keys"][strval($value->key)]["desc"] = strval($value->entry->description);
		$_SESSION["aa"]["bh"]["keys"][strval($value->key)]["action"] = strval($value->entry->action);
		$_SESSION["aa"]["bh"]["keys"][strval($value->key)]["phoneNumber"] = strval($value->entry->phoneNumber);
		if ($ociVersion == "17")
		{
			$_SESSION["aa"]["bh"]["keys"][strval($value->key)]["submenuId"] = "";
		}
		else
		{
			$_SESSION["aa"]["bh"]["keys"][strval($value->key)]["submenuId"] = strval($value->entry->submenuId);
		}
		
		$playAnnouncement = strval($value->entry->action);
		
		if($playAnnouncement == "Play Announcement"){
			$where['audioFileDescription'] = strval($value->entry->audioFileDescription);
			$announcement = new Announcement();
			$getAnnouncementInfo = $announcement->getAnnouncementByFile($db, $where['audioFileDescription']);
			//echo "sss<pre>"; print_r($getAnnouncementInfo); die;
			if(count($getAnnouncementInfo) > 0){
				$_SESSION["aa"]["bh"]["keys"][strval($value->key)]["announcements"] = $getAnnouncementInfo[0]['announcement_id'];
			}else{
				$_SESSION["aa"]["bh"]["keys"][strval($value->key)]["announcements"] = "";
			}
		}
		
	}

	$_SESSION["aa"]["ah"]["announcementSelection"] = strval($xml->command->afterHoursMenu->announcementSelection);
	
	//code to get the value from database for announcement of auto attendant
	$announcementAutoattendantId = $announcement->getAutoattendantAnnouncementInfo($db, $aaId, 'ahAnnouncementId');
	$_SESSION["aa"]["ah"]["announcementId"] = $announcementAutoattendantId;
	
	//	$_SESSION["aa"]["ah"]["audioFileDescription"] = strval($xml->command->afterHoursMenu->audioFileDescription);
	$_SESSION["aa"]["ah"]["enableLevelExtensionDialing"] = strval($xml->command->afterHoursMenu->enableFirstMenuLevelExtensionDialing);
	foreach ($xml->command->afterHoursMenu->keyConfiguration as $key => $value)
	{
		
		$_SESSION["aa"]["ah"]["keys"][strval($value->key)]["desc"] = strval($value->entry->description);
		$_SESSION["aa"]["ah"]["keys"][strval($value->key)]["action"] = strval($value->entry->action);
		$_SESSION["aa"]["ah"]["keys"][strval($value->key)]["phoneNumber"] = strval($value->entry->phoneNumber);
		if ($ociVersion == "17")
		{
			$_SESSION["aa"]["ah"]["keys"][strval($value->key)]["submenuId"] = "";
		}
		else
		{
			$_SESSION["aa"]["ah"]["keys"][strval($value->key)]["submenuId"] = strval($value->entry->submenuId);
		}
		
		$playAnnouncement = strval($value->entry->action);
		if($playAnnouncement == "Play Announcement"){
			$where['audioFileDescription'] = strval($value->entry->audioFileDescription);
			$announcement = new Announcement();
			$getAnnouncementInfo = $announcement->getAnnouncementByFile($db, $where['audioFileDescription']);
			if(count($getAnnouncementInfo) > 0){
				$_SESSION["aa"]["ah"]["keys"][strval($value->key)]["announcements"] = $getAnnouncementInfo[0]['announcement_id'];
			}else{
				$_SESSION["aa"]["ah"]["keys"][strval($value->key)]["announcements"] = "";
			}
		}else{
			$_SESSION["aa"]["ah"]["keys"][strval($value->key)]["announcements"] = "";
		}
	}

	if ($_SESSION["aa"]["type"] == "Standard") //BroadSoft version 19 or later
	{
		$_SESSION["aa"]["h"]["announcementSelection"] = strval($xml->command->holidayMenu->announcementSelection);
		
		//code to get the value from database for announcement of auto attendant
		$announcementAutoattendantId = $announcement->getAutoattendantAnnouncementInfo($db, $aaId, 'hAnnouncementId');
		$_SESSION["aa"]["h"]["announcementId"] = $announcementAutoattendantId;
		
		
//		$_SESSION["aa"]["h"]["audioFileDescription"] = strval($xml->command->holidayMenu->audioFileDescription);
		$_SESSION["aa"]["h"]["enableLevelExtensionDialing"] = strval($xml->command->holidayMenu->enableFirstMenuLevelExtensionDialing);
		foreach ($xml->command->holidayMenu->keyConfiguration as $key => $value)
		{
			$_SESSION["aa"]["h"]["keys"][strval($value->key)]["desc"] = strval($value->entry->description);
			$_SESSION["aa"]["h"]["keys"][strval($value->key)]["action"] = strval($value->entry->action);
			$_SESSION["aa"]["h"]["keys"][strval($value->key)]["phoneNumber"] = strval($value->entry->phoneNumber);
			$_SESSION["aa"]["h"]["keys"][strval($value->key)]["submenuId"] = strval($value->entry->submenuId);
			
			$playAnnouncement = strval($value->entry->action);
			if($playAnnouncement == "Play Announcement"){ 
				$where['audioFileDescription'] = strval($value->entry->audioFileDescription);
				$announcement = new Announcement();
				$getAnnouncementInfo = $announcement->getAnnouncementByFile($db, $where['audioFileDescription']);
				$_SESSION["aa"]["h"]["keys"][strval($value->key)]["announcements"] = $getAnnouncementInfo[0]['announcement_id'];
				if(count($getAnnouncementInfo) > 0){
					$_SESSION["aa"]["h"]["keys"][strval($value->key)]["announcements"] = $getAnnouncementInfo[0]['announcement_id'];
				}else{
					$_SESSION["aa"]["h"]["keys"][strval($value->key)]["announcements"] = "";
				}
			}
			
		}

		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuGetListRequest");
		$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		$a = 0;
		foreach ($xml->command->submenuTable->row as $key => $value)
		{
			$_SESSION["aa"]["submenu" . $a]["id"] = strval($value->col[0]);
			$subId = strval($value->col[0]);

			if ($ociVersion == "20")
			{
				$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuGetRequest20");
			}
			else
			{
				$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuGetRequest");
			}
			$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
			$xmlinput .= "<submenuId>" . htmlspecialchars(strval($value->col[0])) . "</submenuId>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			
			$_SESSION["aa"]["submenu" . $a]["enableLevelExtensionDialing"] = strval($xml->command->enableLevelExtensionDialing);
			$_SESSION["aa"]["submenu" . $a]["announcementSelection"] = strval($xml->command->announcementSelection);
			$announcementAutoattendantIdSubMenu = $announcement->getAutoattendantAnnouncementInfo($db, $aaId, 'subMenuId');
			$idSubMenu = "";
			$theArray = explode(',', $announcementAutoattendantIdSubMenu);
			//print_r($theArray);
			if(count($theArray) > 0 && !empty($theArray)){
			    foreach ($theArray as $keyAnId => $valAnId){
			        $expldVal = explode('***',$valAnId);
			       // print_r($expldVal);
			        if($expldVal[0]==$subId){
			            $idSubMenu = $expldVal[1];
			        }
			    }
			}
			$_SESSION["aa"]["submenu" . $a]["announcementId"] = $idSubMenu;
			foreach ($xml->command->keyConfiguration as $k => $v)
			{
				$_SESSION["aa"]["submenu" . $a]["keys"][strval($v->key)]["desc"] = strval($v->entry->description);
				$_SESSION["aa"]["submenu" . $a]["keys"][strval($v->key)]["action"] = strval($v->entry->action);
				$_SESSION["aa"]["submenu" . $a]["keys"][strval($v->key)]["phoneNumber"] = strval($v->entry->phoneNumber);
				$_SESSION["aa"]["submenu" . $a]["keys"][strval($v->key)]["submenuId"] = strval($v->entry->submenuId);
			}
			$a++;
		}
	}
	
	//code to get the value from database for announcement of auto attendant
	$announcementAutoattendantId = $announcement->getAutoattendantAnnouncementInfo($db, $aaId, 'vpAnnouncementId');
	if(!empty($announcementAutoattendantId)){
		$_SESSION["aa"]["vp"]["announcementId"] = $announcementAutoattendantId;
	}else{
		$_SESSION["aa"]["vp"]["announcementId"] = "";		
	}
	
	$phoneNumberStatus = "No";
	$userPhoneNumber = "";
	if(isset($_SESSION["aa"]["phoneNumber"]) && !empty($_SESSION["aa"]["phoneNumber"])){
		require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
		
		$ccDn = new Dns (); 
		$numberActivateResponse = $ccDn->getUserDNActivateListRequest ( $aaId );
		//echo "<pre>"; print_r($numberActivateResponse); die;
		if (empty ( $numberActivateResponse ['Error'] )) {
			if (! empty ( $numberActivateResponse ['Success'] [0] ['phoneNumber'] )) {
				$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
			}
			if (! empty ( $numberActivateResponse ['Success'] [0] ['status'] )) {
				$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
			}
		}
	}
	$_SESSION["aa"]["aaActivateNumber"] = $phoneNumberStatus;
	
	//echo "<pre>"; print_r($_SESSION["aa"]);
	
?>
