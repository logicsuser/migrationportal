    <?php
/**
 * Created by Karl.
 * Date: 4/22/2017
 */

    $xmlinput = xmlHeader($sessionid, "SystemDomainGetListRequest");
    $xmlinput .= xmlFooter();

    $response = $client->processOCIMessage(array("in0" => $xmlinput));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

    $systemDefaultDomain = $xml->command->systemDefaultDomain;

    $a = 0;
    foreach ($xml->command->domain as $domain) {
        $systemDomains[$a++] = strval($domain);
    }

?>

