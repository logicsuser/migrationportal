<?php

/**
 * Date: 7/14/2017
 */

require_once("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig.php");

class ModifyBLFService
{
    private $error;

    public function __construct($userId, $deviceName = "")
    {
        global $sessionid, $client;

        $expl = explode("@", $userId);
        $listURI = $expl[0] . "-blf@" . $_SESSION["defaultDomain"];

        $xmlinput = xmlHeader($sessionid, "UserBusyLampFieldModifyRequest");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= "<listURI>" . $listURI . "</listURI>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $this->error = readError($xml);

        if ( ! $this->hasError() && ! empty($deviceName)) {
            rebuildAndResetDeviceConfig($deviceName, rebuildAndReset);
        }
    }

    public function getError() {
        return $this->error;
    }

    public function hasError() {
        return $this->error != "";
    }
}
