<?php

/**
 * Created by Karl.
 * Date: 9/18/2016
 */

class CSVGroupPhoneTags extends CSVManager
{
    /**
     * CSVGroupPhoneTags constructor.
     */
    public function __construct($postName)
    {
        parent::__construct($postName);

        $this->addAttribute("groupId",    "Group ID",    $this->Validate_None());
        $this->addAttribute("deviceType", "Device Type", $this->Validate_BW_Device_Type());
        $this->addAttribute("tagName",    "Tag Name",    $this->Validate_BW_Tag_Name());
        $this->addAttribute("tagValue",   "Tag Value",   $this->Validate_None());
    }
}
