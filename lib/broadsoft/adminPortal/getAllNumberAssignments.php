<?php
require_once ("/var/www/lib/broadsoft/adminPortal/autoAttendant/aaOperation.php");
	
    $xmlinput = xmlHeader($sessionid, "GroupDnGetAssignmentListRequest18");
	
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<responseSizeLimit>10000</responseSizeLimit>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
//print_r($xml);

	if (isset($xml->command->dnTable->row))
	{
		foreach ($xml->command->dnTable->row as $key => $value)
		{
			$phoneNumber = strval($value->col[0]);
//echo $phoneNumber . "<BR>";

			if ($ociVersion == "17")
			{
				if (strpos($phoneNumber, " - ") > 0)
				{
					$exp = explode(" - ", $phoneNumber);
					for ($a = $exp[0]; $a <= $exp[1]; $a++)
					{
						$numberAssignments[$a]["type"] = "Available";
					}
				}
				else
				{
					$numberAssignments[$phoneNumber]["department"] = "N/A";
					$numberAssignments[$phoneNumber]["macAddress"] = "N/A";
					$numberAssignments[$phoneNumber]["type"] = "N/A";
					$numberAssignments[$phoneNumber]["userId"] = "N/A";
					$exp = explode(",", strval($value->col[1]));
					$numberAssignments[$phoneNumber]["lastName"] = $exp[0];
					if (isset($exp[1]))
					{
						$numberAssignments[$phoneNumber]["firstName"] = $exp[1];
					}
					else
					{
						$numberAssignments[$phoneNumber]["firstName"] = "";
					}
					$numberAssignments[$phoneNumber]["extension"] = "N/A";
					$exp = explode("-", $phoneNumber);
					if (count($exp) > 1)
					{
						$ph = $exp[0] . $exp[1];
					}
					else
					{
						$ph = subtr("", 0, 1);
					}
					$userPh[$ph]["name"] = str_replace(",", ", ", strval($value->col[1]));
				}
			}
			else
			{
				if (strval($value->col[8]) == "")
				{
					$val = "Available";
				}
				else
				{
					$val = strval($value->col[8]);
				}

				if (strpos($phoneNumber, " - ") > 0)
				{
					$exp = explode(" - ", $phoneNumber);
					for ($a = $exp[0]; $a <= $exp[1]; $a++)
					{
						$numberAssignments[$a]["type"] = "Available";
					}
				}
				else
				{
					$userId = strval($value->col[3]);
					
					if ($ociVersion == "19")
					{
					    $xmlinput = xmlHeader($sessionid, "UserGetRequest19");
					}
					else
					{
					    $xmlinput = xmlHeader($sessionid, "UserGetRequest20");
					}
					$xmlinput .= "<userId>" . $userId . "</userId>";
					$xmlinput .= xmlFooter();
					$response = $client->processOCIMessage(array("in0" => $xmlinput));
					$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

					$numberAssignments[$phoneNumber]["department"] = strval($xml->command->department->name);
					if ($xml->command->accessDeviceEndpoint)
					{
						$deviceName = strval($xml->command->accessDeviceEndpoint->accessDevice->deviceName);

						$xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetRequest18sp1");
						$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
						$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
						$xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
						$xmlinput .= xmlFooter();
						$response = $client->processOCIMessage(array("in0" => $xmlinput));
						$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

						$numberAssignments[$phoneNumber]["macAddress"] = strval($xml->command->macAddress);
					}
					if($val =="Auto Attendant"){
					    $AAId = strval($value->col[3]);
					    if(isset($AAId) && !empty($AAId)){
					        $obj = new AaOperation();
					        $aaResp = $obj->getAutoAttendantInfoInstance($AAId);
					        //echo"<pre>";print_r($aaResp);
					        if(isset($aaResp["Success"]) && $aaResp["Success"] !=""){
					            $numberAssignments[$phoneNumber]["lastName"] = $aaResp["Success"]["basicInfo"]["callingLineIdLastName"];
					            $numberAssignments[$phoneNumber]["firstName"] = $aaResp["Success"]["basicInfo"]["callingLineIdFirstName"];
					        }else{
					            $numberAssignments[$phoneNumber]["lastName"] = "";
					            $numberAssignments[$phoneNumber]["firstName"] = "";
					        }
					        
					    }
					}
					else{
					    $numberAssignments[$phoneNumber]["lastName"] = strval($value->col[4]);
					    $numberAssignments[$phoneNumber]["firstName"] = strval($value->col[5]);
					}
					$numberAssignments[$phoneNumber]["type"] = $val;
					$numberAssignments[$phoneNumber]["userId"] = strval($value->col[3]);
					$numberAssignments[$phoneNumber]["extension"] = strval($value->col[6]);
					$exp = explode("-", $phoneNumber);
					if (count($exp) > 1)
					{
						$ph = $exp[0] . $exp[1];
					}
					else
					{
						$ph = subtr(strval($value->col[6]), 0, 1);
					}
					$userPh[$ph]["name"] = strval($value->col[4]) . ", " . strval($value->col[5]);
				}
			}
		}
	}
?>
