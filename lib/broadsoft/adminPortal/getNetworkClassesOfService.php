<?php
/**
 * Created by Karl.
 * Date: 4/8/2017
 */

    $xmlinput = xmlHeader($sessionid, "GroupNetworkClassOfServiceGetAssignedListRequest");
    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
    $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
    $xmlinput .= xmlFooter();

    $response = $client->processOCIMessage(array("in0" => $xmlinput));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

    if (! isset($xml->command->networkClassOfServiceTable->row)) {
        $networkClassesOfService = array();
        return;
    }

    $a = 0;
    foreach ($xml->command->networkClassOfServiceTable->row as $key => $value)
    {
        $networkClassesOfService[$a] = strval($value->col[0]);

        if (strval($value->col[2]) == "true") {
            $defaultNetworkClassOfService = strval($value->col[0]);
        }
        $a++;
    }
?>
