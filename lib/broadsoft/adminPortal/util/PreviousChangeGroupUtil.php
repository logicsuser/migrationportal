<?php 
Class PreviousChangeGroupUtil{

	public $db;
	public function __construct() {
			global $db;
			$this->db = $db;
	}

	public function getPreviousChangeGroup() {
		global $db;
        $detail = array();
		$queryCG = "SELECT * from adminChangeGroup where userId='" . $_SESSION["adminId"] . "' ORDER BY accessed_on DESC";
		$sth = $db->query($queryCG);             
        while ($row = $sth->fetch())
        {
                $detail[] = $row;
        }                
		return $detail;
	}

	public function setChangeGroupDetails($adminId, $selectedSp, $selectedSpName, $selectedGroup, $selectedGroupName, $clusterName='') {
            if($clusterName != ""){
                $uValues = array($adminId, $clusterName, $selectedSp, $selectedSpName, $selectedGroup, $selectedGroupName, date("Y-m-d H:i:s"));
            }else{
                $uValues = array($adminId, $selectedSp, $selectedSpName, $selectedGroup, $selectedGroupName, date("Y-m-d H:i:s"));
            }
	    
			$previouseInfo = $this->getPreviouseInfo($adminId, $selectedSp, $selectedGroup);
			$infoExist = false;
			$updateInfo = false;
			$rowId = "";
			foreach ($previouseInfo as $key => $value) {
				if($value['sp'] == $selectedSp && $value['groupId'] == $selectedGroup) {
					$infoExist = true;
					$updateInfo = $value['spName'] != $selectedSpName || $value['groupName'] != $selectedGroupName ? true : false;
					$rowId = $value['id'];
					break;
				}
			}
			if( ! $infoExist ) {
				if( count($previouseInfo) == 6 ) {
					$this->deleteChangeGroupDetail($adminId);
				}
                                
                                if($clusterName != ""){
                                    $query = "insert into adminChangeGroup (userId, clusterName, sp, spName, groupId, groupName, accessed_on)" . "VALUES " . "(?, ?, ?, ?, ?, ?, ?)";
                                    //print_r($uValues);
                                    $insert_stmt = $this->db->prepare($query);
                                    $insert_stmt->execute($uValues);
                                }else{
                                    $query = "insert into adminChangeGroup (userId, sp, spName, groupId, groupName, accessed_on)" . "VALUES " . "(?, ?, ?, ?, ?, ?)";
                                    //print_r($uValues);
                                    $insert_stmt = $this->db->prepare($query);
                                    $insert_stmt->execute($uValues);
                                }
				
			}
			else if($updateInfo) {
			    $this->updateChangeGroupInfo($rowId, $selectedSpName, $selectedGroupName, $clusterName);
			}
			else {
                            $this->updateChangeGroupAccessDate($rowId, $clusterName);
			}
		}
		
		public function getPreviouseInfo($adminId, $selectedSp, $selectedGroup) {
			$querySel = "SELECT * from adminChangeGroup where userId='" . $adminId . "'";
			$sthSel = $this->db->query($querySel);
			$allRowData = array();
			while( $row = $sthSel->fetch() ) {
				$allRowData[] = $row;
			}
			return $allRowData;
		}
		
		public function deleteChangeGroupDetail($adminId) {
			$queryDel = "DELETE FROM adminChangeGroup WHERE userId ='" . $adminId . "' ORDER BY id ASC LIMIT 1";
			$sthSel = $this->db->query($queryDel);
		}
		
		public function updateChangeGroupAccessDate($id, $clusterName='') {
			$date_now = date("Y-m-d H:i:s");
                        if($clusterName != ""){
                            $update_query  = 'UPDATE adminChangeGroup SET clusterName = ?, accessed_on = ?';
                            $update_query .= " where id = ?";
                            $params = array($clusterName, $date_now, $id);
                        }else{
                            $update_query = 'UPDATE adminChangeGroup SET '
                                                            . 'accessed_on = ?';
                            $update_query .= " where id = ?";
                            $params = array($date_now, $id);
                        }
			$update_stmt = $this->db->prepare($update_query);
			$update_stmt->execute($params);                
		}
		
		public function updateChangeGroupInfo($id, $selectedSpName, $selectedGroupName, $clusterName=''){
		    $date_now = date("Y-m-d H:i:s");
                    if($clusterName != ""){
                        $update_que = 'UPDATE adminChangeGroup SET '
                        . 'clusterName = ?,'
		        . 'spName = ?,'
		        . 'groupName = ?,'
		        . 'accessed_on = ?';
		        $update_que .= " where id = ?";
		        $params = array($clusterName, $selectedSpName, $selectedGroupName, $date_now, $id);
		        
                    }else{
                        $update_que = 'UPDATE adminChangeGroup SET '
		        . 'spName = ?,'
		        . 'groupName = ?,'
		        . 'accessed_on = ?';
		        $update_que .= " where id = ?";
		        $params = array($selectedSpName, $selectedGroupName, $date_now, $id);		        
                    }
                    
                    $update_stmt = $this->db->prepare($update_que);
		    $update_stmt->execute($params);
		    
		}
		
		public function deleteChangeGroupOfAdmin($adminId) {
			$queryDel = "DELETE FROM adminChangeGroup WHERE userId ='" . $adminId . "'";
			$sthSel = $this->db->query($queryDel);
		}
		
		public function deleteGroupNotExist($enterpeiseId, $groupId, $adminId) {
		    $queryDel = "DELETE FROM adminChangeGroup WHERE sp ='" . $enterpeiseId . "' AND groupId ='" . $groupId . "' AND userId ='" . $adminId . "'";
		    $sthSel = $this->db->query($queryDel);
		}
		
		public function deleteSpNotExist($enterpeiseId, $adminId) {
		    $queryDel = "DELETE FROM adminChangeGroup WHERE sp ='" . $enterpeiseId . "' AND userId ='" . $adminId . "'";
		    if($this->db->query($queryDel)) {
		        return true;
		    } else {
		        return false;
		    }
		}
		
}
?>