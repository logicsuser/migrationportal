<?php 
class ConfigUtil{
    
    private $db;
    private $type = "bwAppServer";
    
    public function __construct($db, $serverType='')
    { 
         $this->db = $db;
         if($serverType!="")
         {
            $this->type = $serverType;
         }
    }
    public function getClustersList()
    {
        require_once("/var/www/html/Express/functions.php");

        $query  = "SELECT DISTINCT(clusterName) FROM serversConfig WHERE type='" . $this->type . "' ORDER BY id asc ";
        $result = $this->db->query($query);
        $clustersList = array();
        while ($row = $result->fetch()) 
        {            
            $clustersList[] = $row['clusterName'];
        }
        return $clustersList;
    }
    
    public function createClustersDropdownList($clusterIdName, $clusterClsName, $clusterList, $currentClusterForSelect='')
    {        
        echo "<select id='$clusterIdName' class='$clusterClsName' name='$clusterIdName'>";
        foreach($clusterList as $clusterName)
        {
            $selected = "";
            if($currentClusterForSelect !="" && $clusterName == $currentClusterForSelect){
                $selected = "selected";
            }
            echo "<option value='$clusterName' $selected>$clusterName</option>";
        }        
        echo "</select>";
    }
    
    //Code added @ 18 June 2019
    public function getLoginParametersFromServersConfigForSelectedCluster($db, $serverType, $selectedCluster=''){
                
        $whereCndn = " and 1=1 ";
        if($selectedCluster != ""){
                $clusterName  = trim($selectedCluster);
                $whereCndn    = " and clusterName = '$clusterName'";
        }
        $connLoginDetails = array(); 
                
        $query            = " select clusterName, id, active, ip, userName, password, ociRelease, protocol, port, password_salt from serversConfig ";
        $query           .= " where type='" . $serverType . "' and active='true' $whereCndn ";
        $query           .= " order by id asc";
        //echo "<br />Query - ".$query;
        $result           = $db->query($query);
        $row              = $result->fetch();
        
	$connLoginDetails['bw_ews']           = $row['ip'];
	$connLoginDetails['bwuserid']         = $row['userName'];														//TODO: CHANGE NAME $bwpasswd to $bwPassword
	$connLoginDetails['ociVersion']       = $row['ociRelease'];																//TODO: CHANGE NAME $ociVersion to $bwRelease
	$connLoginDetails['bwAuthlevel']      = "system";                              
        $connLoginDetails['bwProtocol']       = $row['protocol'];
        $connLoginDetails['bwPort']           = $row['port'];
        
        if(isset($row['password_salt']) && $row['password_salt']) {
	        $connLoginDetails['bwpasswd'] = decryptString($row['password'], $row['userName'] . "_" . $row['password_salt']);
        } else {
	        $connLoginDetails['bwpasswd'] = $row['password'];
        }
        
        return $connLoginDetails;
    }
    
    //Code added @ 03 July 2019
    public function setDefaultClusterIfSeletedClusterNoExists($db, $serverType)
    {        
        $query            = "select id, clusterName from serversConfig where type='" . $serverType . "' and active='true' order by id asc ";
        $result           = $db->query($query);
        $row              = $result->fetch();
        $clusterName      = $row['clusterName'];
        return $clusterName;
    }
}

?>