<?php 
class ServerConnCheckUtil {

    
    function createSurgeMailTestUser($surgMailConnectedIp, $surgMailConnectedUsrName, $surgMailConnectedPass, $httpPrtcl, $emailAddress)
    {
        require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailOperations.php");
        $sugeObj = new SurgeMailOperations();
        $emailPassword = setEmailPassword();
        $httpProtocol = "$httpPrtcl://";
        $surgemailPost = array ("cmd" => "cmd_user_login",
            "lcmd" => "user_create",
            "show" => "simple_msg.xml",
            "username" => $surgMailConnectedUsrName,
            "password" => $surgMailConnectedPass,
            "lusername" => $emailAddress,
            "lpassword" => $emailPassword,
            "uid" => isset($userid) ? $userid : "");
        $createResponse = $sugeObj->surgeMailAccountOperations($httpProtocol . $surgMailConnectedIp . "/cgi/user.cgi", $surgemailPost);
        
        if($createResponse=="200") {
            $createSuccess = "true";
            return $createSuccess ;
         } else {
             $createSuccess = "false";
             return $createSuccess ;
         }
    }
    
    /*
     * Delete serge Mail connection 
     * 
     */
    function deleteSurgeMailTestUser($surgMailConnectedIp, $surgMailConnectedUsrName, $surgMailConnectedPass, $httpPrtcl, $emailAddress)
    {
        $userName = explode('@',$emailAddress);
        $tstUserName =  $userName['0'] ;
        
        require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailOperations.php");
        $sugeObj = new SurgeMailOperations();
        $emailPassword = setEmailPassword();
        $httpProtocol = "$httpPrtcl://";
        $deleteSurgemailPost = array ("cmd" => "cmd_user_login",
            "lcmd" => "user_delete",
            "show" => "simple_msg.xml",
            "username" => $surgMailConnectedUsrName,
            "password" => $surgMailConnectedPass,
            "lusername" => $emailAddress,
            "lpassword" => $emailPassword,
            "uid" => isset($userid) ? $userid : "");
        $deleteResponse = $sugeObj->surgeMailAccountOperations($httpProtocol . $surgMailConnectedIp . "/cgi/user.cgi", $deleteSurgemailPost);
        //print_r($deleteResponse);
        if(strpos($deleteResponse, "Account deleted") !== false)
        {
            $success ="true";
        }
        else
        {
            $success ="false";
        } 
        return $success;
    }
    
   /* use fuction for check primary or secondary server are connect
    * created date 11-06
    * use Module setting for server configuration 
    */ 
    
    function check_login_connection($bwProtocol, $bw_ews, $bwPort, $bwuserid, $bwpasswd)
    {
          try
                {    
                        $opts = array(
                                'http' => array(
                                        'user_agent'=>'PHPSoapClient'
                            ),
                                'ssl' => array(
                                        'verify_peer' => false,
                                        'verify_peer_name' => false
                                )
                        );

                        $context = stream_context_create($opts);

                        $soapClientOptions = array(
                                'stream_context' => $context,
                                'cache_wsdl' => WSDL_CACHE_NONE,
                                "exceptions" => 0,
                                "trace" => 1
                        );

                        libxml_disable_entity_loader(false);

                        $soapClientOptions["exceptions"] = 1;
                        try {                
                                $client = new SoapClient("$bwProtocol://" . $bw_ews . ":$bwPort/webservice/services/ProvisioningService?wsdl", $soapClientOptions); //Code added @ 10 May 2018
                                //$client = new SoapClient("http://" . $bw_ews . "/webservice/services/ProvisioningService?wsdl", $soapClientOptions);
                                
                                $charid = md5(uniqid(rand(), true));
                                $sessionid = substr($charid, 0, 32);
                                $nonce = "";
                                $response = $client->processOCIMessage(array("in0" => $this->buildCommandAuthenticationRequest($sessionid, $bwuserid)));
                                                                
                                if ($response and $response->processOCIMessageReturn and substr($response->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
                                {
                                        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                                        if ($xml->command["type"] != "Error")
                                        {
                                                $userName =  "ok";
                                                foreach ($xml->children() as $child)
                                                {
                                                        $temp = $child->getName();
                                                        if (!strcmp($temp, "sessionId"))
                                                        {
                                                                $sessionId = $child;
                                                        }
                                                        foreach ($child->children() as $grandson)
                                                        {
                                                                $temp = $grandson->getName();
                                                                if (!strcmp($temp, "nonce"))
                                                                {
                                                                        $nonce = $grandson;
                                                                }
                                                                foreach ($grandson->children() as $greatson)
                                                                {
                                                                        $temp = $greatson->getName();
                                                                        foreach ($greatson->children() as $ggson)
                                                                        {
                                                                                $temp = $ggson->getName();
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                }

                                //$S1 = $bwpasswd;
                                $S1 = sha1(trim($bwpasswd));
                                $S2 = $nonce . ":" . $S1;
                                $enc_pass = md5($S2);

                                $xmlinput = $this->xmlHeader($sessionid, "LoginRequest14sp4");
                                $xmlinput .= "<userId>" . $bwuserid . "</userId>";
                                $xmlinput .= "<signedPassword>" . $enc_pass . "</signedPassword>";
                                $xmlinput .= $this->xmlFooter();

                                $response = $client->processOCIMessage(array("in0" => $xmlinput));                                
                                
                                if ($response and $response->processOCIMessageReturn and substr($response->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
                                {
                                        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                                        if ($xml->command["type"] != "Error")
                                        {
                                            $password =  "ok";
                                        }
                                }
                                else
                                {
                                        $msg =  "No response at all from server. <br>";
                                }                                
                                
                                if($userName=="ok" && $password=="ok")
                                {
                                    return true;
                                    //echo "Yes - ";
                                }
                                else
                                {
                                    //echo "No - ";
                                    return false;
                                }
                        }
                        catch (SoapFault $E) {
                        }
                }
                catch (Exception $e)
                {
                        $code = $e->getCode();
                        $str = $e->getMessage();
                        exit(1);
                }
    }
    
    function xmlHeader($sessionid, $commandType)
    {        
        $header = "<BroadsoftDocument protocol=\"OCI\" xmlns=\"C\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
        $header .= "<sessionId xmlns=\"\">" . $sessionid . "</sessionId>";
        $header .= "<command xsi:type=\"" . $commandType . "\" xmlns=\"\">";
        return $header;
    }
    
    function xmlFooter()
    {
        return "</command>\n</BroadsoftDocument>\n";
    }
    
    function buildCommandAuthenticationRequest($sessionid, $bwuserid)
    {        
        $xmlstr = $this->xmlHeader($sessionid, "AuthenticationRequest");
        $xmlstr .= "<userId>" . $bwuserid . "</userId>" . "\n";
        $xmlstr .= $this->xmlFooter();        
        return $xmlstr;
    }
    
    function checkCounterPathConnection($protocol, $ipAddrr, $port, $username, $password){
        $resp = "";
        //Code added @ 22 July 2019
        if(empty($protocol) || empty($ipAddrr) || empty($port)){
            $respnse = "false";
            return $respnse;            
        }
        //End code
        $url = $protocol."://".$ipAddrr.":".$port."/stretto/prov/user";
        //echo "1- ";print_r($protocol);
        //echo "2- ";print_r($ipAddrr);
        //echo "3- ";print_r($port);
        //echo "4- ";print_r($username);
        //echo "5- ";print_r($password);
        //$username = "super";
        //$password = "Averi*";
        //$auth = base64_encode( $username . ':' . urlencode($password) );
        
        $ch = curl_init();
        //curl_setopt($ch, CURLOPT_URL, "http://56.181.14.164:9206/stretto/prov/user?method=GET&groupName=usps");
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10); //Code added @ 22 July 2019 timeout after 20 seconds 
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20); //Code added @ 22 July 2019 timeout after 20 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
        
        $result = curl_exec($ch);
        //$error = curl_error($ch);
        //print_r($error);
        //echo "123456"; print_r($result);
        $info = curl_getinfo($ch);
        //print_r($info);
        //echo "111- ";print_r($info["http_code"]);
        if ($info["http_code"] == "200") {
            $resp = "true";
            //echo 'Error:' . curl_error($ch);
        }else{
            $resp = "false";
        }
        curl_close ($ch);
        return $resp;
    }
    
	function getSoftwaReversion($bwProtocol, $bw_ews, $bwPort, $bwuserid, $bwpasswd)
    {
          try
                {    
                        $opts = array(
                                'http' => array(
                                        'user_agent'=>'PHPSoapClient'
                            ),
                                'ssl' => array(
                                        'verify_peer' => false,
                                        'verify_peer_name' => false
                                )
                        );

                        $context = stream_context_create($opts);
                        $soapClientOptions = array(
                                'stream_context' => $context,
                                'cache_wsdl' => WSDL_CACHE_NONE,
                                "exceptions" => 0,
                                "trace" => 1
                        );
                        libxml_disable_entity_loader(false);

                        $soapClientOptions["exceptions"] = 1;
                        try {         
                                $client = new SoapClient("$bwProtocol://" . $bw_ews . ":$bwPort/webservice/services/ProvisioningService?wsdl", $soapClientOptions); //Code added @ 10 May 2018
                                //$client = new SoapClient("http://" . $bw_ews . "/webservice/services/ProvisioningService?wsdl", $soapClientOptions);
                                $charid = md5(uniqid(rand(), true));
                                $sessionid = substr($charid, 0, 32);
                                $nonce = "";
                                $response = $client->processOCIMessage(array("in0" => $this->buildCommandAuthenticationRequest($sessionid, $bwuserid)));
                                                                
                                if ($response and $response->processOCIMessageReturn and substr($response->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
                                {
                                        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                                        if ($xml->command["type"] != "Error")
                                        {
                                                $userName =  "ok";
                                                foreach ($xml->children() as $child)
                                                {
                                                        $temp = $child->getName();
                                                        if (!strcmp($temp, "sessionId"))
                                                        {
                                                                $sessionId = $child;
                                                        }
                                                        foreach ($child->children() as $grandson)
                                                        {
                                                                $temp = $grandson->getName();
                                                                if (!strcmp($temp, "nonce"))
                                                                {
                                                                        $nonce = $grandson;
                                                                }
                                                                foreach ($grandson->children() as $greatson)
                                                                {
                                                                        $temp = $greatson->getName();
                                                                        foreach ($greatson->children() as $ggson)
                                                                        {
                                                                                $temp = $ggson->getName();
                                                                        }
                                                                }
                                                        }
                                                }
                                        }
                                }

                                //$S1 = $bwpasswd;
                                $S1 = sha1(trim($bwpasswd));
                                $S2 = $nonce . ":" . $S1;
                                $enc_pass = md5($S2);

                                $xmlinput = $this->xmlHeader($sessionid, "LoginRequest14sp4");
                                $xmlinput .= "<userId>" . $bwuserid . "</userId>";
                                $xmlinput .= "<signedPassword>" . $enc_pass . "</signedPassword>";
                                $xmlinput .= $this->xmlFooter();

                                $response = $client->processOCIMessage(array("in0" => $xmlinput));                                
                                
                                if ($response and $response->processOCIMessageReturn and substr($response->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
                                {
                                        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                                        if ($xml->command["type"] != "Error")
                                        {
                                            $password =  "ok";
                                        }
                                }
                                else
                                {
                                        $msg =  "No response at all from server. <br>";
                                }                                
                                
                                if($userName=="ok" && $password=="ok")
                                {
									$xmlstr = $this->xmlHeader($sessionid, "SystemSoftwareVersionGetRequest");
									$xmlstr .= $this->xmlFooter();
									$response = $client->processOCIMessage(array("in0" => $xmlstr));                                
									if ($response and $response->processOCIMessageReturn and substr($response->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
									{
											$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
											if ($xml->command["type"] != "Error")
											{												
												return $xml->command->version;
											}
									}								
								
                                }
                                else
                                {
                                    //echo "No - ";
                                    return false;
                                }
                        }
                        catch (SoapFault $E) {
                        }
                }
                catch (Exception $e)
                {
                        $code = $e->getCode();
                        $str = $e->getMessage();
                        exit(1);
                }
    }
	
    
    function getSoftwaReversionAndConnection($bwProtocol, $bw_ews, $bwPort, $bwuserid, $bwpasswd)
    {
        $returnArray = array();
        try
        {
            $opts = array(
                'http' => array(
                    'user_agent'=>'PHPSoapClient'
                ),
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false
                )
            );
            
            $context = stream_context_create($opts);
            $soapClientOptions = array(
                'stream_context' => $context,
                'cache_wsdl' => WSDL_CACHE_NONE,
                "exceptions" => 0,
                "trace" => 1
            );
            libxml_disable_entity_loader(false);
            
            $soapClientOptions["exceptions"] = 1;
            try {
                $client = new SoapClient("$bwProtocol://" . $bw_ews . ":$bwPort/webservice/services/ProvisioningService?wsdl", $soapClientOptions); //Code added @ 10 May 2018
                //$client = new SoapClient("http://" . $bw_ews . "/webservice/services/ProvisioningService?wsdl", $soapClientOptions);
                $charid = md5(uniqid(rand(), true));
                $sessionid = substr($charid, 0, 32);
                $nonce = "";
                $response = $client->processOCIMessage(array("in0" => $this->buildCommandAuthenticationRequest($sessionid, $bwuserid)));
                
                if ($response and $response->processOCIMessageReturn and substr($response->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
                {
                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                    if ($xml->command["type"] != "Error")
                    {
                        $userName =  "ok";
                        foreach ($xml->children() as $child)
                        {
                            $temp = $child->getName();
                            if (!strcmp($temp, "sessionId"))
                            {
                                $sessionId = $child;
                            }
                            foreach ($child->children() as $grandson)
                            {
                                $temp = $grandson->getName();
                                if (!strcmp($temp, "nonce"))
                                {
                                    $nonce = $grandson;
                                }
                                foreach ($grandson->children() as $greatson)
                                {
                                    $temp = $greatson->getName();
                                    foreach ($greatson->children() as $ggson)
                                    {
                                        $temp = $ggson->getName();
                                    }
                                }
                            }
                        }
                    }
                }
                
                //$S1 = $bwpasswd;
                $S1 = sha1(trim($bwpasswd));
                $S2 = $nonce . ":" . $S1;
                $enc_pass = md5($S2);
                
                $xmlinput = $this->xmlHeader($sessionid, "LoginRequest14sp4");
                $xmlinput .= "<userId>" . $bwuserid . "</userId>";
                $xmlinput .= "<signedPassword>" . $enc_pass . "</signedPassword>";
                $xmlinput .= $this->xmlFooter();
                
                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                
                if ($response and $response->processOCIMessageReturn and substr($response->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
                {
                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                    if ($xml->command["type"] != "Error")
                    {
                        $password =  "ok";
                    }
                }
                else
                {
                    $msg =  "No response at all from server. <br>";
                }
                
                if($userName=="ok" && $password=="ok")
                {
                    $xmlstr = $this->xmlHeader($sessionid, "SystemSoftwareVersionGetRequest");
                    $xmlstr .= $this->xmlFooter();
                    $response = $client->processOCIMessage(array("in0" => $xmlstr));
                    if ($response and $response->processOCIMessageReturn and substr($response->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
                    {
                        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                        if ($xml->command["type"] != "Error")
                        {
                            $returnArray["status"] = true;
                            $returnArray["version"] = $xml->command->version;
                            return $returnArray;
                        }
                    }
                    
                }
                else
                {
                    //echo "No - ";
                    $returnArray["status"] = false;
                    return $returnArray;
                }
            }
            catch (SoapFault $E) {
            }
        }
        catch (Exception $e)
        {
            $code = $e->getCode();
            $str = $e->getMessage();
            exit(1);
        }
    }
	
	
	
}


?>
