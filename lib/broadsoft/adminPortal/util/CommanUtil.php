<?php 
class CommanUtil{
    function getDefaultCountryCode() {
            global $sessionid, $client;
            $responseList["Error"]   = "";
            $responseList["Success"] = array();
            
            $xmlinput  = xmlHeader($sessionid, "SystemCountryCodeGetListRequest");
            $xmlinput .= xmlFooter();

            $response  = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml       = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            
            if (readErrorXmlGenuine($xml) != "") 
            {
                    $responseList["Error"] = $xml->command->summaryEnglish;
            }
            else
            {                    
                    $count = 0;
                    $responseList['Success']['defaultCountryCode']         = $xml->command->defaultCountryCode;
                    foreach ($xml->command->countryCodeTable->row as $key => $value)
                    {  
                        $responseList['Success']['defaultCountryCode']     = strval($xml->command->defaultCountryCode);
                        $responseList['Success']['countryCodeList'][strval($value->col[0])]['countryCode']    = strval($value->col[0]);
                        $responseList['Success']['countryCodeList'][strval($value->col[0])]['countryName']    = strval($value->col[1]);
                        $count = $count + 1;
                    }
            }                
            return $responseList;
      }
      
        function getsystemLevelEnterPriseList($currentSP ,$getSPName)
        {

                //if (isset($_SESSION["spList"]) && count($_SESSION["spList"] > 1)) {
                if (isset($_SESSION['spListNameId']) && count($_SESSION['spListNameId'] > 1)) {
                    $serviceProviders = $_SESSION['spListNameId'];
                } else {
                    $serviceProviders = $getSPName;
                }
                $str = "";
                if(count($serviceProviders) > 0){
                        //$str = "<option value=\"\"></option>";
                        $str = "";
                        foreach ($serviceProviders as $v) {
                          $providerId = explode('<span>-</span>',$v); //ex-774
                          $spIdwithName = str_replace('<span>-</span>', ' - ', $v);
                          $currentspId = $providerId[0];
                          $sel = "";
                          if($currentspId==$currentSP){
                              $sel = "selected='selected'";
                          }
                          $str .= "<option value=\"".$currentspId."\" $sel>" . $spIdwithName . "</option>";
                        }
                }
                return $str;
        }
        
        function getAllGroupListOfServiceProvider($spID){
            global $sessionid, $client;            
            $groupsList = array();
            $xmlinput = xmlHeader($sessionid, "GroupGetListInServiceProviderRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spID) . "</serviceProviderId>";
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            if(isset($response->processOCIMessageReturn) && $response->processOCIMessageReturn != "") 
            {
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                $a = 0;
                if (isset($xml->command->groupTable->row))
                {
                    foreach ($xml->command->groupTable->row as $key => $value)
                    {
                        $groupsList[$a] = strval($value->col[0]);
                        $a++;
                    }
                }
                return $groupsList;
            }
        }
}

?>