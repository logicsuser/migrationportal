<?php 
class ServerFileDownload {
    function downloadFile($serverIp, $userName, $password, $remote_file_path, $local_file_path){
    
        try {
            $connection = ssh2_connect($serverIp, '22');
            if(!$connection){
                throw new \Exception("Could not connect to $serverIp");
            }
            $auth  = ssh2_auth_password($connection, $userName, $password);
            if(!$auth){
                throw new \Exception("Could not authenticate with username $username and password ");
            }
            $sftp = ssh2_sftp($connection);
            if(!$sftp){
                throw new \Exception("Could not initialize SFTP subsystem.");
            }
            if(ssh2_scp_recv($connection, $remote_file_path, $local_file_path)) {
                return true;
            } else {
                return false;
            }
            $connection = NULL;
            
        } catch (Exception $e) {
            error_log("Error due to :".$e->getMessage());
            return false;
        }
        //echo "<pre>{$output}</pre>";
    }
}
?>