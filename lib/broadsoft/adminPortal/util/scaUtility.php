<?php 
class SCAUtility{
    function scaPortCalculation($array, $totalDevicePorts){
        
       
        $busyPorts=array();
        if(count($array) > 0){
            
            foreach ($array as $Key => $userInfo){
           
                $busyPorts[]= (int)$userInfo['order'];
                
            }
           
            if(count($busyPorts) > 0  ) {
                for($i=1; $i<=$totalDevicePorts; $i++ ) {
                    if(!in_array($i, $busyPorts)) {
                      
                        return $i;
                    }
                }               
                
                return $totalDevicePorts + 1;  
            } else {
                
                return 1;
            }
            
/*             if(isset($array["numberOfAssignedPorts"]) && $array["numberOfAssignedPorts"] != ""){
                $val = $array["numberOfAssignedPorts"];
                $val1 = (int)$val+1;
                return $val1;
            }else{
                return "";
            } */
        }
        
        return  1;
    }
    
    function scaFreePortCalculation($array, $totalDevicePorts){
        
        $busyPorts=array();
        $availPorts=array();
        if(count($array) > 0){
            
            foreach ($array as $Key => $userInfo){
                if($userInfo["endPointType"] == "Primary"){
                    $busyPorts[]= (int)$userInfo['order'];
                }
                
            }
            
            if(count($busyPorts) > 0  ) {
                for($i=1; $i<=$totalDevicePorts; $i++ ) {
                    if(!in_array($i, $busyPorts)) {
                        
                        $availPorts[] = $i;
                        
                    }
                }
               
                return $availPorts;
            } else {
                $availPorts[] = 1;
                return $availPorts;
            }
           
        }
        
        $availPorts[] = 1;
        return $availPorts;
    }
}

?>