<?php 

require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");

//call user operation class to get the list of users
require_once '/var/www/lib/broadsoft/adminPortal/userOperations/UserOperations.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserDndOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserServiceOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserCallForwardingOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserBusyLampFieldServiceOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserDetailsOperation.php';

require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserThirdPartyVoiceMailOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserVoiceMsgAdvanceVoiceMgmtServiceOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserVoiceMsgVoiceMgmtServiceOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserRemoteOfficeOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserPolycomPhoneServicesOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserBusyLampFieldServiceOperation.php';

require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/GroupDeviceCustomTagOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/GroupDeviceGetUserOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/GroupAccessDeviceGetOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserRegistrationListRequestOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserPolycomPhoneServicesOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/GroupAccessDeviceGetOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserEndpointDeviceNameOperation.php';
require_once '/var/www/lib/broadsoft/adminPortal/UserOperation/UserServiceGetAssignmentListRequestOperation.php';
require_once("/var/www/html/Express/userMod/UserCustomTagManager.php");

$providerId = $_SESSION["sp"];
$groupId = $_SESSION["groupId"];

$userOperation = new UserDetailsOperation();
$userOperation->department = $_POST["department"];
if(isset($useCurrentDetailedUsersList) && $useCurrentDetailedUsersList) {
	$registrationUsers = $users = $userOperation->getAllUserDetailsForExpress($providerId, $groupId, $useCurrentDetailedUsersList);
} else {
	$registrationUsers = $users = $userOperation->getAllUserDetailsForExpress($providerId, $groupId);
}
?>