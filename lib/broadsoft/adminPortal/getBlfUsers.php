<?php
	$xmlinput = xmlHeader($sessionid, "UserBusyLampFieldGetAvailableUserListRequest");
	$xmlinput .= "<userId>" . $userId . "</userId>";
	$xmlinput .= "<searchCriteriaExactUserGroup>";
    $xmlinput .=    "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
    $xmlinput .=    "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
    $xmlinput .= "</searchCriteriaExactUserGroup>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	if (isset($xml->command->userTable->row))
	{
		foreach ($xml->command->userTable->row as $key => $value)
		{
			if ((isset($_SESSION["userInfo"]["monitoredUsers"]) and !searchArray($_SESSION["userInfo"]["monitoredUsers"], "id", strval($value->col[0]))) or !isset($_SESSION["userInfo"]["monitoredUsers"]))
			{
				$blfUsers[$a]["id"] = strval($value->col[0]);
				$blfUsers[$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
				$a++;
			}
		}
	}

	$blfUsers = subval_sort($blfUsers, "name");
?>
