<?php

class NameAndIDBuilder
{

    // The regex containing the tokens to be replaced.
    private $regexToProcess;

    // value map with key as token in upper case and value as value with which token should be replaced.
    private $regexValueMap;

    // This is the set of tokens that are allowed in the regex.
    private $allowedTokens;

    public function __construct($idRegex, $regexValueMap1, $allowedRegex)
    {
        $this->regexToProcess = $idRegex;
        $this->regexValueMap = $regexValueMap1;
        $this->allowedTokens = $allowedRegex;
        
        //print_r( $this->allowedTokens);
    }

    // pass the regex to be processed and the map ok token and values.
    public function getParsedId($idRegex, $regexValueMap)
    {
        $id = "";
        $tokenInfo = $this->getNextTokenInfo($idRegex);        
        while (true) {
            if (isset($tokenInfo["token"]) && ! empty($tokenInfo["token"])) {
                $token = strtoupper($tokenInfo["token"]);
                if (array_search($token, $this->allowedTokens) === FALSE) {
                    return "";
                }
                
                if (isset($regexValueMap[$token]) && ! empty($regexValueMap[$token])) {
                    $id = $id . $tokenInfo["nontokenstr"] . $regexValueMap[$token];
                    $idRegex = substr($idRegex, $tokenInfo["offset"] + 1, strlen($idRegex));
                    $tokenInfo = $this->getNextTokenInfo($idRegex);
                } else {
                    return "";
                }
                  
            } else {
                if (isset($tokenInfo["nontokenstr"]) && ! empty($tokenInfo["nontokenstr"])) {
                    $id = $id . $tokenInfo["nontokenstr"];
                }
                break;
            }
        }
        return $id;
    }

    function getNextTokenInfo($idRegex)
    {
        $input = $idRegex;
        $returnTokenInfo = array();
        $returnTokenInfo["token"] = "";
        $returnTokenInfo["offset"] = "";
        $returnTokenInfo["nontokenstr"] = "";
        $nonTokenStr = "";
        $offset = 0;
        $startPosition = strpos($input, "<", $offset);
        if ($startPosition !== false) {
            if ($startPosition > 0) {
                $nonTokenStr = substr($input, 0, $startPosition);
                $input = substr($input, $startPosition, strlen($input));
            }
            $endPosition = strpos($idRegex, ">", $startPosition);
            if ($endPosition !== false) {
                $returnTokenInfo["offset"] = $endPosition;
                $value = substr($idRegex, $startPosition + 1, $endPosition - $startPosition - 1);
                $returnTokenInfo["token"] = $value;
            } else {
                $returnTokenInfo["offset"] = strlen($idRegex) - 1;
            }
        } else {
            $returnTokenInfo["offset"] = strlen($idRegex) - 1;
            $nonTokenStr = $idRegex;
        }
        $returnTokenInfo["nontokenstr"] = $nonTokenStr;
        return $returnTokenInfo;
    }
}
