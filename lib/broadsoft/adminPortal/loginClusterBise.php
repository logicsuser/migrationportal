<?php
	//error_reporting(E_ALL);
	ini_set("display_errors", "1");

	//server-specific configuration variables
	require_once("/var/www/html/Express/config.php");
        require_once("/var/www/lib/broadsoft/adminPortal/util/ConfigUtil.php");        
        $configUtilObj = new ConfigUtil($db, "bwAppServer");
        $connLoginDetails = $configUtilObj->getLoginParametersFromServersConfigForSelectedCluster($db, "bwAppServer", $selectedClusterName);
                
        $bw_ews1      = $connLoginDetails['bw_ews'];
	$bwuserid1    = $connLoginDetails['bwuserid'];																	//TODO: CHANGE NAME $bwuserid to $bwUserName
	$bwpasswd1    = $connLoginDetails['bwpasswd'];																	//TODO: CHANGE NAME $bwpasswd to $bwPassword
	$ociVersion1  = $connLoginDetails['ociVersion'];																	//TODO: CHANGE NAME $ociVersion to $bwRelease
	$bwAuthlevel1 = $connLoginDetails['bwAuthlevel'];                              
        $bwProtocol1  = $connLoginDetails['bwProtocol'];   
        $bwPort1      = $connLoginDetails['bwPort'];
        
        
	
	function openBroadsoftHeader($sessionid)
	{
		$header = "<BroadsoftDocument protocol=\"OCI\" xmlns=\"C\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
		$header .= "<sessionId xmlns=\"\">" . $sessionid . "</sessionId>";
		return $header;
	}
	
	function closeBroadSoftHeader()
	{
		return "</BroadsoftDocument>\n";
	}
	
	function openCommand($commandType)
	{
		$header .= "<command xsi:type=\"" . $commandType . "\" xmlns=\"\">";
		return $header;
	}
	
	function closeCommand()
	{
		return "</command>\n";
	}

	function xmlHeader($sessionid, $commandType)
	{
		$header = "<BroadsoftDocument protocol=\"OCI\" xmlns=\"C\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";
		$header .= "<sessionId xmlns=\"\">" . $sessionid . "</sessionId>";
//		if (strlen($commandType) > 0)
//		{
			$header .= "<command xsi:type=\"" . $commandType . "\" xmlns=\"\">";
//		}
		return $header;
	}

	
	
	function xmlFooter()
	{
		return "</command>\n</BroadsoftDocument>\n";
	}

	function buildCommandAuthenticationRequest($sessionid, $bwuserid1)
	{
		$xmlstr = xmlHeader($sessionid, "AuthenticationRequest");
		$xmlstr .= "<userId>" . $bwuserid1 . "</userId>" . "\n";
		$xmlstr .= xmlFooter();
		return $xmlstr;
	}

	try
	{                         
                $opts = array(
			'http' => array(
				'user_agent'=>'PHPSoapClient'
		    ),
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false
			)
		);

		$context = stream_context_create($opts);

		$soapClientOptions = array(
			'stream_context' => $context,
			'cache_wsdl' => WSDL_CACHE_NONE,
			"exceptions" => 0,
			"trace" => 1
		);

		libxml_disable_entity_loader(false);
		
		$soapClientOptions["exceptions"] = 1;
 		//print_r($soapClientOptions["exceptions"]); exit;
        try {                
                $clientForSelCluster = new SoapClient("$bwProtocol1://" . $bw_ews1 . ":$bwPort1/webservice/services/ProvisioningService?wsdl", $soapClientOptions); //Code added @ 10 May 2018
                $clientForSelCluster1 = new SoapClient("$bwProtocol1://" . $bw_ews1 . ":$bwPort1/webservice/services/ProvisioningService?wsdl", $soapClientOptions); //Code added @ 10 May 2018
		//$client = new SoapClient("http://" . $bw_ews . "/webservice/services/ProvisioningService?wsdl", $soapClientOptions);
                
		$charid1 = md5(uniqid(rand(), true));
		$sessionidForSelCluster = substr($charid1, 0, 32);
		$nonce = "";
		$loginResponse = $clientForSelCluster->processOCIMessage(array("in0" => buildCommandAuthenticationRequest($sessionidForSelCluster, $bwuserid1)));
                              
                //echo "<br />Login Response - ";
                //print_r($loginResponse);
                
		if ($loginResponse and $loginResponse->processOCIMessageReturn and substr($loginResponse->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
		{
			$xml = new SimpleXMLElement($loginResponse->processOCIMessageReturn, LIBXML_NOWARNING);
			//echo "<br />XML - ";
                        //print_r($xml);
                        if ($xml->command["type"] != "Error")
			{
				foreach ($xml->children() as $child)
				{
					$temp = $child->getName();
					if (!strcmp($temp, "sessionId"))
					{
						$sessionidForSelCluster = $child;
					}
					foreach ($child->children() as $grandson)
					{
						$temp = $grandson->getName();
						if (!strcmp($temp, "nonce"))
						{
							$nonce = $grandson;
						}
						foreach ($grandson->children() as $greatson)
						{
							$temp = $greatson->getName();
							foreach ($greatson->children() as $ggson)
							{
								$temp = $ggson->getName();
							}
						}
					}
				}
			}
		}
		
                //$S1 = $bwpasswd;
		$S1 = sha1($bwpasswd1);
		$S2 = $nonce . ":" . $S1;
		$enc_pass1 = md5($S2);

		$xmlinput = xmlHeader($sessionidForSelCluster, "LoginRequest14sp4");
		$xmlinput .= "<userId>" . $bwuserid1 . "</userId>";
		$xmlinput .= "<signedPassword>" . $enc_pass1 . "</signedPassword>";
		$xmlinput .= xmlFooter();

		$loginResponse = $clientForSelCluster->processOCIMessage(array("in0" => $xmlinput));

                //echo "<br />Login Response Pass - ";
                //print_r($loginResponse);
                
                
		if ($loginResponse and $loginResponse->processOCIMessageReturn and substr($loginResponse->processOCIMessageReturn, 0, 19) == "<?xml version=\"1.0\"")
		{
			$xml = new SimpleXMLElement($loginResponse->processOCIMessageReturn, LIBXML_NOWARNING);
			//echo "<br />Login Response Pass XML - ";
                        //print_r($xml);
                        if ($xml->command["type"] == "Error")
			{
			}
		}
		else
		{
			echo "No response at all from server. <br>";
		}
		foreach ($xml->children() as $child)
		{
			$temp = $child->getName();
			if (!strcmp($temp, "sessionId"))
			{
				$sessionidForSelCluster = $child;
			}
			foreach ($child->children() as $grandson)
			{
				$temp = $grandson->getName();

				foreach ($grandson->children() as $greatson)
				{
					$temp = $greatson->getName();
					foreach ($greatson->children() as $ggson)
					{
						$temp = $ggson->getName();
					}
				}
			}
		}
		
        }
        catch (SoapFault $E) {
//             echo $E->faultstring;
        }
	}
	catch (Exception $e)
	{
		$code = $e->getCode();
		$str = $e->getMessage();
		exit(1);
	}

if(!function_exists("http_post_fields")) {

	function http_post_fields($url, $data, $headers=null) {
		$data = http_build_query($data);
		$opts = array('http' => array('method' => 'POST', 'content' => $data));

		if($headers) {
			$opts['http']['header'] = $headers;
		}
		$st = stream_context_create($opts);
		$fp = fopen($url, 'rb', false, $st);

		if(!$fp) {
			return false;
		}
		return stream_get_contents($fp);
	}

}
?>
