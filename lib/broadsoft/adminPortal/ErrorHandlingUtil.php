<?php

class ErrorHandlingUtil {
    
	private static $BWErrorMsg;
	
    private static function getErrorMessageFromCode($errorCode) { 	/*Add your Error code and Custom message here.*/
        switch ($errorCode) {
            case 6004 : return "Testing Error Handling Enterprise is missing.";
            case 6005 : return "Testing Error Handling.";
			default: return self::$BWErrorMsg;
        }
    }
    
    private  static function getErrorCode($error) {
		$error = (string)$error;
        if( preg_match( '!\[([^\)]+)\]!', $error, $match ) )
        $error = $match[1];
        $errorCode = explode(" ", $error);
        $errorMessage = self::getErrorMessageFromCode($errorCode[1]);
        return $errorMessage;
    }
    
    public static function getErrorMessage($error) {
		self::$BWErrorMsg = $error['Error'];
		
        return self::getErrorCode($error['Error']);
    }
}

?>