<?php

class GroupLevelOperations
{

    function getGroupListOfProvider($provider)
    {
        global $sessionid, $client;
        $allGroups = Array();
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "GroupGetListInServiceProviderRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($provider) . "</serviceProviderId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array(
            "in0" => $xmlinput
        ));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if ($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        } else {
            $a = 0;
            if (isset($xml->command->groupTable->row)) {
                foreach ($xml->command->groupTable->row as $key => $value) {
                    $allGroups[$a]['spId'] = $provider;
                    $allGroups[$a]['grpId'] = strval($value->col[0]);
                    $a ++;
                }
            }
            
            $returnResponse["Success"] = $allGroups;
        }
        return $returnResponse;
    }
    
    function getUsersInGroup($spId, $groupId, $searchCriteria = null)
    {
        global $sessionid, $client;
        $allGroups = Array();
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "UserGetListInGroupRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId) . "</serviceProviderId>";
        $xmlinput .= "<GroupId>" . htmlspecialchars($groupId) . "</GroupId>";

        //User Filters - Start
        if ($searchCriteria) {

        	//error_log(print_r($searchCriteria, true));

	        if (isset($searchCriteria['responseSizeLimit'])) {
		        $xmlinput .= "<responseSizeLimit>{$searchCriteria['responseSizeLimit']}</responseSizeLimit>";
	        }

	        if (isset($searchCriteria['UserLastName'])) {
		        $xmlinput .= "<searchCriteriaUserLastName>";
		        $xmlinput .= "<mode>{$searchCriteria['UserLastName']['mode']}</mode>";
		        $xmlinput .= "<value>{$searchCriteria['UserLastName']['value']}</value>";
		        $xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
		        $xmlinput .= "</searchCriteriaUserLastName>";
	        }

	        if (isset($searchCriteria['UserFirstName'])) {
		        $xmlinput .= "<searchCriteriaUserFirstName>";
		        $xmlinput .= "<mode>{$searchCriteria['UserFirstName']['mode']}</mode>";
		        $xmlinput .= "<value>{$searchCriteria['UserFirstName']['value']}</value>";
		        $xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
		        $xmlinput .= "</searchCriteriaUserFirstName>";
	        }

	        if (isset($searchCriteria['Dn'])) {
		        $xmlinput .= "<searchCriteriaDn>";
		        $xmlinput .= "<mode>{$searchCriteria['Dn']['mode']}</mode>";
		        $xmlinput .= "<value>{$searchCriteria['Dn']['value']}</value>";
		        $xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
		        $xmlinput .= "</searchCriteriaDn>";
	        }

	        if (isset($searchCriteria['EmailAddress'])) {
		        $xmlinput .= "<searchCriteriaEmailAddress>";
		        $xmlinput .= "<mode>{$searchCriteria['EmailAddress']['mode']}</mode>";
		        $xmlinput .= "<value>{$searchCriteria['EmailAddress']['value']}</value>";
		        $xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
		        $xmlinput .= "</searchCriteriaEmailAddress>";
	        }

	        if (isset($searchCriteria['ExactUserDepartment'])) {
		        $xmlinput .= "<searchCriteriaExactUserDepartment>";
		        $xmlinput .= "<departmentKey xsi:type=\"GroupDepartmentKey\">";
		        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId) . "</serviceProviderId>";
		        $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
		        $xmlinput .= "<name>" . htmlspecialchars($searchCriteria['ExactUserDepartment']['value']) . "</name>";
		        $xmlinput .= "</departmentKey>";
		        $xmlinput .= "</searchCriteriaExactUserDepartment>";
	        }

	        if (isset($searchCriteria['UserId'])) {
		        $xmlinput .= "<searchCriteriaUserId>";
		        $xmlinput .= "<mode>{$searchCriteria['UserId']['mode']}</mode>";
		        $xmlinput .= "<value>{$searchCriteria['UserId']['value']}</value>";
		        $xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
		        $xmlinput .= "</searchCriteriaUserId>";
	        }

	        if (isset($searchCriteria['Extension'])) {
		        $xmlinput .= "<searchCriteriaExtension>";
		        $xmlinput .= "<mode>{$searchCriteria['Extension']['mode']}</mode>";
		        $xmlinput .= "<value>{$searchCriteria['Extension']['value']}</value>";
		        $xmlinput .= "<isCaseInsensitive>true</isCaseInsensitive>";
		        $xmlinput .= "</searchCriteriaExtension>";
	        }

        }
	    //User Filters - End

        $xmlinput .= xmlFooter();
        //error_log($xmlinput);
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    //error_log(print_r($xml, true));
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = trim($errMsg);
        }
        else
        {
            $userCount = 0;
	        $returndata = array();
            foreach ($xml->command->userTable->row as $key => $value)
            {
                $userId = strval($value->col[0]);
                $returndata[$userCount]["userId"] = strval($value->col[0]);
                $returndata[$userCount]["lastName"] = strval($value->col[1]);
                $returndata[$userCount]["firstName"] = strval($value->col[2]);
                $returndata[$userCount]["phoneNumber"] = strval($value->col[4]);
	            $returndata[$userCount]["activated"] = strval($value->col[5]);
                $returndata[$userCount]["ext"] = strval($value->col[10]);
				$returndata[$userCount]["department"] = strval($value->col[3]);
                $userCount++;
            }
            $returnResponse["Success"] = $returndata;
        }
        return $returnResponse;
    }
}

?>
