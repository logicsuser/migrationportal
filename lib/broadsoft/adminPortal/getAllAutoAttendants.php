<?php
	$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantGetInstanceListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	if (isset($xml->command->autoAttendantTable->row))
	{
		foreach ($xml->command->autoAttendantTable->row as $key => $value)
		{
			if (($ociVersion == "17" and strval($value->col[7]) !== "Standard") or $ociVersion !== "17")
			{
				$autoAttendants[$a]["id"] = strval($value->col[0]);
				$autoAttendants[$a]["name"] = strval($value->col[1]);
				$a++;
			}
		}
	}

	if (isset($autoAttendants))
	{
		$autoAttendants = subval_sort($autoAttendants, "name");
	}
?>
