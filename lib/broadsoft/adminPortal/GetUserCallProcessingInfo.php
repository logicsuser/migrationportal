<?php

/**
 * Created by Sologics.
 * Date: 12/05/2017
 */
class GetUserCallProcessingInfo{

	
	//function to get the user call processing information of the user
    public function modifyCallProcessingInfo($postUserId, $postArray){
       
		//public function modifyCallProcessingPolicy($postArray){
		global  $sessionid, $client;
		$callProcessingModifyResponse["Error"] = "";
		$callProcessingModifyResponse["Success"] = "";
		
		//call Processing
		$xmlinputCall  = xmlHeader($sessionid, "UserCallProcessingModifyPolicyRequest14sp7");
		$xmlinputCall .= "<userId>" . $postUserId. "</userId>";
		if(isset($postArray['useUserCLIDSetting'])){
		    $xmlinputCall .= "<useUserCLIDSetting>" . $postArray['useUserCLIDSetting']. "</useUserCLIDSetting>";
		}
		
		
		if(isset($postArray['useUserCallLimitsSetting'])){
		    $xmlinputCall .= "<useUserCallLimitsSetting>" . $postArray['useUserCallLimitsSetting']. "</useUserCallLimitsSetting>";
		}
		if(isset($postArray['useUserDCLIDSetting'])){
		    $xmlinputCall .= "<useUserDCLIDSetting>" . $postArray['useUserDCLIDSetting'] . "</useUserDCLIDSetting>";
		}
		if(isset($postArray['useMaxSimultaneousCalls'])){
		    $xmlinputCall .= "<useMaxSimultaneousCalls>" . $postArray['useMaxSimultaneousCalls']. "</useMaxSimultaneousCalls>";
		}
		if(isset($postArray['maxSimultaneousCalls'])){
		    $xmlinputCall .= "<maxSimultaneousCalls>" . $postArray['maxSimultaneousCalls']. "</maxSimultaneousCalls>";
		}
		if(isset($postArray['useMaxSimultaneousVideoCalls'])){
		    $xmlinputCall .= "<useMaxSimultaneousVideoCalls>" . $postArray['useMaxSimultaneousVideoCalls']. "</useMaxSimultaneousVideoCalls>";
		}
		if(isset($postArray['maxSimultaneousVideoCalls'])){
		    $xmlinputCall .= "<maxSimultaneousVideoCalls>" . $postArray['maxSimultaneousVideoCalls']. "</maxSimultaneousVideoCalls>";
		}
		if(isset($postArray['useMaxCallTimeForAnsweredCalls'])){
		    $xmlinputCall .= "<useMaxCallTimeForAnsweredCalls>" . $postArray['useMaxCallTimeForAnsweredCalls']. "</useMaxCallTimeForAnsweredCalls>";
		}
		if(isset($postArray['maxCallTimeForAnsweredCallsMinutes'])){
		    $xmlinputCall .= "<maxCallTimeForAnsweredCallsMinutes>" . $postArray['maxCallTimeForAnsweredCallsMinutes']. "</maxCallTimeForAnsweredCallsMinutes>";
		}
		if(isset($postArray['useMaxCallTimeForUnansweredCalls'])){
		    $xmlinputCall .= "<useMaxCallTimeForUnansweredCalls>" . $postArray['useMaxCallTimeForUnansweredCalls']. "</useMaxCallTimeForUnansweredCalls>";
		}
		if(isset($postArray['maxCallTimeForUnansweredCallsMinutes'])){
		    $xmlinputCall .= "<maxCallTimeForUnansweredCallsMinutes>" . $postArray['maxCallTimeForUnansweredCallsMinutes']. "</maxCallTimeForUnansweredCallsMinutes>";
		}
		
		if(isset($postArray['useMaxConcurrentRedirectedCalls'])){
		    $xmlinputCall .= "<useMaxConcurrentRedirectedCalls>" . $postArray['useMaxConcurrentRedirectedCalls'] . "</useMaxConcurrentRedirectedCalls>";
		}
		
		
		if(isset($postArray['maxConcurrentRedirectedCalls'])){
		    $xmlinputCall .= "<maxConcurrentRedirectedCalls>" . $postArray['maxConcurrentRedirectedCalls'] . "</maxConcurrentRedirectedCalls>";
		}
		
		if(isset($postArray['useMaxFindMeFollowMeDepth'])){
		  $xmlinputCall .= "<useMaxFindMeFollowMeDepth>" . $postArray['useMaxFindMeFollowMeDepth']. "</useMaxFindMeFollowMeDepth>";
		}
		if(isset($postArray['maxFindMeFollowMeDepth'])){
		  $xmlinputCall .= "<maxFindMeFollowMeDepth>" . $postArray['maxFindMeFollowMeDepth']. "</maxFindMeFollowMeDepth>";
		}
		if(isset($postArray['maxRedirectionDepth'])){
		    $xmlinputCall .= "<maxRedirectionDepth>" . $postArray['maxRedirectionDepth']. "</maxRedirectionDepth>";
		}
		
		if(isset($postArray['useMaxConcurrentFindMeFollowMeInvocations'])){
		    $xmlinputCall .= "<useMaxConcurrentFindMeFollowMeInvocations>" . $postArray['useMaxConcurrentFindMeFollowMeInvocations']. "</useMaxConcurrentFindMeFollowMeInvocations>";
		}
		
		if(isset($postArray['maxConcurrentFindMeFollowMeInvocations'])){
		    $xmlinputCall .= "<maxConcurrentFindMeFollowMeInvocations>" . $postArray['maxConcurrentFindMeFollowMeInvocations'] . "</maxConcurrentFindMeFollowMeInvocations>";
		}
		
		if(isset($postArray['clidPolicy'])){
		    $xmlinputCall .= "<clidPolicy>" . $postArray['clidPolicy']. "</clidPolicy>";
		}
		if(isset($postArray['emergencyClidPolicy'])){
		    $xmlinputCall .= "<emergencyClidPolicy>" . $postArray['emergencyClidPolicy']. "</emergencyClidPolicy>";
		}
		
		if(isset($postArray['allowAlternateNumbersForRedirectingIdentity'])){
		    $xmlinputCall .= "<allowAlternateNumbersForRedirectingIdentity>" . $postArray['allowAlternateNumbersForRedirectingIdentity']. "</allowAlternateNumbersForRedirectingIdentity>";
		}
		if(isset($postArray['useGroupName'])){
		    $xmlinputCall .= "<useGroupName>" . $postArray['useGroupName'] . "</useGroupName>";
		}
		if(isset($postArray['enableDialableCallerID'])){
		    $xmlinputCall .= "<enableDialableCallerID>" . $postArray['enableDialableCallerID']. "</enableDialableCallerID>";
		}
		if(isset($postArray['blockCallingNameForExternalCalls'])){
		    $xmlinputCall .= "<blockCallingNameForExternalCalls>" . $postArray['blockCallingNameForExternalCalls']. "</blockCallingNameForExternalCalls>";
		}
		if(isset($postArray['allowEnterpriseGroupCallTypingForPrivateDialingPlan'])){
		    $xmlinputCall .= "<allowEnterpriseGroupCallTypingForPrivateDialingPlan>" . $postArray['allowEnterpriseGroupCallTypingForPrivateDialingPlan']. "</allowEnterpriseGroupCallTypingForPrivateDialingPlan>";
		}
		if(isset($postArray['allowEnterpriseGroupCallTypingForPublicDialingPlan'])){
		    $xmlinputCall .= "<allowEnterpriseGroupCallTypingForPublicDialingPlan>" . $postArray['allowEnterpriseGroupCallTypingForPublicDialingPlan'] . "</allowEnterpriseGroupCallTypingForPublicDialingPlan>";
		}
		if(isset($postArray['overrideCLIDRestrictionForPrivateCallCategory'])){
		    $xmlinputCall .= "<overrideCLIDRestrictionForPrivateCallCategory>" . $postArray['overrideCLIDRestrictionForPrivateCallCategory']. "</overrideCLIDRestrictionForPrivateCallCategory>";
		}
		if(isset($postArray['allowConfigurableCLIDForRedirectingIdentity'])){
		    $xmlinputCall .= "<allowConfigurableCLIDForRedirectingIdentity>" . $postArray['allowConfigurableCLIDForRedirectingIdentity']. "</allowConfigurableCLIDForRedirectingIdentity>";
		}
		if(isset($postArray['allowDepartmentCLIDNameOverride'])){
		    $xmlinputCall .= "<allowDepartmentCLIDNameOverride>" . $postArray['allowDepartmentCLIDNameOverride']. "</allowDepartmentCLIDNameOverride>";
		}
		
		$xmlinputCall .= xmlFooter();
		$responseCall = $client->processOCIMessage(array("in0" => $xmlinputCall));
		$xmlCall = new SimpleXMLElement($responseCall->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xmlCall); die;
		if (readErrorXmlGenuine($xmlCall) != "") {
			if(empty($xmlCall->command->detail)){
				$callProcessingModifyResponse["Error"] = strval($xmlCall->command->summaryEnglish);
			}else{
				$callProcessingModifyResponse["Error"]['Detail'] = strval($xmlCall->command->detail);
			}
		}else{
			$callProcessingModifyResponse["Success"] = $xmlCall->command;
			
		}
		return $callProcessingModifyResponse;
	}
	
	
	public function getUserCallProcessingPolicy($userId){
		
		global  $sessionid, $client;
		
		$userCallProcessingResponse["Error"] = "";
		$userCallProcessingResponse["Success"] = "";
		
		if(!empty($userId)){
			$xmlinput = xmlHeader($sessionid, "UserCallProcessingGetPolicyRequest18");
			$xmlinput .= "<userId>" . $userId. "</userId>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			if (readErrorXmlGenuine($xml) != "") {
				$userCallProcessingResponse["Error"] = $xml->command->summaryEnglish;
			}else{
				$userCallProcessingResponse["Success"] = $xml->command;
				
			}
		}
		return $userCallProcessingResponse;
	}
	
}
	
	
?>