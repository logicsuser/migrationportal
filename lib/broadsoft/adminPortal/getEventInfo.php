<?php
	unset($_SESSION["event"]);

	if ($_POST["eventName"] == "newEvent")
	{
	}
	else
	{
		$exp = explode(":", $_POST["eventName"]);
		$schedName = $exp[0];
		$schedType = $exp[1];
		$eventName = $exp[2];

		$xmlinput = xmlHeader($sessionid, "GroupScheduleGetEventRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= "<scheduleKey>
					<scheduleName>" . htmlspecialchars($schedName) . "</scheduleName>
					<scheduleType>" . $schedType . "</scheduleType>
				</scheduleKey>";
		$xmlinput .= "<eventName>" . htmlspecialchars($eventName) . "</eventName>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		$startDate = date("m/d/Y", strtotime(strval($xml->command->startDate)));
		$startHour = strval($xml->command->startTime->hour);
		$startMinute = strval($xml->command->startTime->minute);
		if (strlen($startMinute) == 1)
		{
			$startMinute = "0" . $startMinute;
		}
		$startTime = date("g:ia", strtotime($startHour . ":" . $startMinute));
		$endDate = date("m/d/Y", strtotime(strval($xml->command->endDate)));
		$endHour = strval($xml->command->endTime->hour);
		$endMinute = strval($xml->command->endTime->minute);
		if (strlen($endMinute) == 1)
		{
			$endMinute = "0" . $endMinute;
		}
		$endTime = date("g:ia", strtotime($endHour . ":" . $endMinute));
		$allDayEvent = strval($xml->command->allDayEvent);

		if (isset($xml->command->recurrence->recurDaily->recurInterval))
		{
			//Daily recurrence
			$recurrence = "Daily";
			$_SESSION["event"]["recurInterval"] = strval($xml->command->recurrence->recurDaily->recurInterval);
		}
		else if (isset($xml->command->recurrence->recurWeekly->recurInterval))
		{
			//Weekly recurrence
			$recurrence = "Weekly";
			$_SESSION["event"]["recurInterval"] = strval($xml->command->recurrence->recurWeekly->recurInterval);
			$_SESSION["event"]["sunday"] = strval($xml->command->recurrence->recurWeekly->sunday);
			$_SESSION["event"]["monday"] = strval($xml->command->recurrence->recurWeekly->monday);
			$_SESSION["event"]["tuesday"] = strval($xml->command->recurrence->recurWeekly->tuesday);
			$_SESSION["event"]["wednesday"] = strval($xml->command->recurrence->recurWeekly->wednesday);
			$_SESSION["event"]["thursday"] = strval($xml->command->recurrence->recurWeekly->thursday);
			$_SESSION["event"]["friday"] = strval($xml->command->recurrence->recurWeekly->friday);
			$_SESSION["event"]["saturday"] = strval($xml->command->recurrence->recurWeekly->saturday);
		}
		else if (isset($xml->command->recurrence->recurMonthlyByDay->recurInterval))
		{
			//Monthly (by day) recurrence
			$recurrence = "Monthly";
			$_SESSION["event"]["recurMonthly"] = "ByDay";
			$_SESSION["event"]["recurInterval"] = strval($xml->command->recurrence->recurMonthlyByDay->recurInterval);
			$_SESSION["event"]["dayOfMonth"] = strval($xml->command->recurrence->recurMonthlyByDay->dayOfMonth);
		}
		else if (isset($xml->command->recurrence->recurMonthlyByWeek->recurInterval))
		{
			//Monthly (by week) recurrence
			$recurrence = "Monthly";
			$_SESSION["event"]["recurMonthly"] = "ByWeek";
			$_SESSION["event"]["recurInterval"] = strval($xml->command->recurrence->recurMonthlyByWeek->recurInterval);
			$_SESSION["event"]["dayOfWeekInMonth"] = strval($xml->command->recurrence->recurMonthlyByWeek->dayOfWeekInMonth);
			$_SESSION["event"]["dayOfWeek"] = strval($xml->command->recurrence->recurMonthlyByWeek->dayOfWeek);
		}
		else if (isset($xml->command->recurrence->recurYearlyByDay->recurInterval))
		{
			//Yearly (by day) recurrence
			$recurrence = "Yearly";
			$_SESSION["event"]["recurYearly"] = "ByDay";
			$_SESSION["event"]["recurInterval"] = strval($xml->command->recurrence->recurYearlyByDay->recurInterval);
			$_SESSION["event"]["dayOfMonthInYear"] = strval($xml->command->recurrence->recurYearlyByDay->dayOfMonth);
			$_SESSION["event"]["byDayMonth"] = strval($xml->command->recurrence->recurYearlyByDay->month);
		}
		else if (isset($xml->command->recurrence->recurYearlyByWeek->recurInterval))
		{
			//Yearly (by week) recurrence
			$recurrence = "Yearly";
			$_SESSION["event"]["recurYearly"] = "ByWeek";
			$_SESSION["event"]["recurInterval"] = strval($xml->command->recurrence->recurYearlyByWeek->recurInterval);
			$_SESSION["event"]["dayOfWeekInMonthInYear"] = strval($xml->command->recurrence->recurYearlyByWeek->dayOfWeekInMonth);
			$_SESSION["event"]["dayOfWeekInYear"] = strval($xml->command->recurrence->recurYearlyByWeek->dayOfWeek);
			$_SESSION["event"]["byWeekMonth"] = strval($xml->command->recurrence->recurYearlyByWeek->month);
		}
		else
		{
			$recurrence = "Never";
		}

		if (isset($xml->command->recurrence->recurForEver))
		{
			$_SESSION["event"]["recurrenceEnd"] = "Never";
		}
		else if (isset($xml->command->recurrence->recurEndOccurrence))
		{
			$_SESSION["event"]["recurrenceEnd"] = "After";
			$_SESSION["event"]["recurEndOccurrence"] = strval($xml->command->recurrence->recurEndOccurrence);
		}
		else if (isset($xml->command->recurrence->recurEndDate))
		{
			$_SESSION["event"]["recurrenceEnd"] = "Date";
			$_SESSION["event"]["recurEndDate"] = date("m/d/Y", strtotime(strval($xml->command->recurrence->recurEndDate)));
		}
	}
?>
