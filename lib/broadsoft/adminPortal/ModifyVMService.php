<?php
require_once("/var/www/lib/broadsoft/adminPortal/SurgeMailOperations.php");
/**
 * Date: 7/14/2017
 */
class ModifyVMService
{
    private $userId;
    private $emailPassword;
    private $error;

    /**
     * ModifyVMService constructor.
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
        $this->emailPassword = setEmailPassword();
    }

    /**
     * @param $passCode
     * @param $emailAddress
     */
    public function add($passCode, $emailAddress) {
        global $sessionid, $client;

        // Activate VM User Service
        $xmlinput  = xmlHeader($sessionid, "UserVoiceMessagingUserModifyVoiceManagementRequest");
        $xmlinput .= "<userId>" . $this->userId . "</userId>";
        $xmlinput .= "<isActive>true</isActive>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $this->error = readError($xml);
        if ($this->hasError()) {
            return;
        }

        // Set Mailbox passCode
        $xmlinput = xmlHeader($sessionid, "UserPortalPasscodeModifyRequest");
        $xmlinput .= "<userId>" . $this->userId . "</userId>";
        $xmlinput .= "<newPasscode>" . $passCode . "</newPasscode>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $this->error = readError($xml);
        if ($this->hasError()) {
            return;
        }

        // Configure Mail Service
        $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserModifyAdvancedVoiceManagementRequest");
        $xmlinput .= "<userId>" . $this->userId . "</userId>";
        $xmlinput .= "<groupMailServerEmailAddress>" . $emailAddress . "</groupMailServerEmailAddress>";
        $xmlinput .= "<groupMailServerUserId>" . $emailAddress . "</groupMailServerUserId>";
        $xmlinput .= "<groupMailServerPassword>" . $this->emailPassword . "</groupMailServerPassword>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));

        // Create Surge Mail
        self::createSurgeMail($emailAddress, $this->emailPassword);
    }

    /**
     * @param $surgeMailUserId
     */
    public function remove($surgeMailUserId) {
        global $policiesSupportDeleteVMOnUserDel;

        // Remove Surge Mail account
        if ($policiesSupportDeleteVMOnUserDel == "true" && $surgeMailUserId != "") {
            self::deleteSurgeMail($surgeMailUserId, $this->emailPassword);
        }
    }

    /**
     * @return mixed
     */
    public function getError() {
        return $this->error;
    }

    /**
     * @return bool
     */
    public function hasError() {
        return $this->error != "";
    }


    private static function createSurgeMail($emailAddress, $emailPassword) {
        self::httpPost("user_delete", $emailAddress, $emailPassword);
        self::httpPost("user_create", $emailAddress, $emailPassword);
    }

    private static function deleteSurgeMail($emailAddress, $emailPassword) {
        self::httpPost("user_delete", $emailAddress, $emailPassword);
    }

    private static function httpPost($action, $emailAddress, $emailPassword) {
        $sugeObj = new SurgeMailOperations();
        global $surgemailUsername, $surgemailPassword, $surgemailURL;

        $surgeMailPost = array ("cmd" => "cmd_user_login",
            "lcmd" => $action,
            "show" => "simple_msg.xml",
            "username" => $surgemailUsername,
            "password" => $surgemailPassword,
            "lusername" => $emailAddress,
            "lpassword" => $emailPassword,
            "uid" => isset($userid) ? $userid : "");
        //$res = http_post_fields($surgemailURL, $surgeMailPost);
        $srgeRsltCrteUsr = $sugeObj->surgeMailAccountOperations($surgemailURL, $surgeMailPost);
    }
}
