<?php

/**
 * Created by Dipin Krishna.
 * Date: 6/6/2018
 */
class PasswordRulesConfiguration
{
	private $db;
	private $id = "";
	private $type = "";
	private $label = "";
	private $minimumLength;
	private $numberOfDigits = "";
	private $specialCharacters = "";
	private $upperCaseLetter = "";
	private $lowerCaseLetter = "";
	private $active = "";

	private static function passwordTable() {
		return "passwordRulesConfig";
	}

	private static function colID() {
		return "id";
	}

	private static function colActive() {
		return "active";
	}

	private static function colType() {
		return "type";
	}

	private static function colLabel() {
		return "label";
	}

	private static function colMinimumLength() {
		return "minimumLength";
	}

	private static function colNumberOfDigits() {
		return "numberOfDigits";
	}

	private static function colSpecialCharacters() {
		return "specialCharacters";
	}

	private static function colUpperCaseLetter() {
		return "upperCaseLetter";
	}

	private static function colLowerCaseLetter() {
		return "lowerCaseLetter";
	}


	/**
	 * PasswordRulesConfiguration constructor.
	 * @param $passwordType
	 */
	public function __construct($passwordType = NULL) {
		global $db;

		$this->db = $db;

		if ($passwordType) {
			$this->loadRulesFor($passwordType);
		}
	}

	/**
	 * @param $passwordType
	 */
	public function loadRulesFor($passwordType) {

		$query = 'SELECT * FROM ' . self::passwordTable() . ' WHERE type = ? LIMIT 1';
		$stmt = $this->db->prepare($query);

		if ($stmt->execute(array($passwordType))) {

			if($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$this->id = $row[self::colID()];
				$this->active = $row[self::colActive()];
				$this->type = $row[self::colType()];
				$this->label = $row[self::colLabel()];
				$this->minimumLength = $row[self::colMinimumLength()];
				$this->numberOfDigits = $row[self::colNumberOfDigits()];
				$this->specialCharacters = $row[self::colSpecialCharacters()];
				$this->upperCaseLetter = $row[self::colUpperCaseLetter()];
				$this->lowerCaseLetter = $row[self::colLowerCaseLetter()];
			}

		}
	}

	/**
	 * Update Password Rules
	 * @return BOOL
	 */
	public function updateConfig() {

		$query = "UPDATE " . self::passwordTable() . " SET "
			. self::colActive() . " = ?, "
			. self::colLabel() . " = ?, "
			. self::colMinimumLength() . " = ?, "
			. self::colNumberOfDigits() . " = ?, "
			. self::colSpecialCharacters() . " = ?, "
			. self::colUpperCaseLetter() . " = ?, "
			. self::colLowerCaseLetter() . " = ? "
			. " WHERE " . self::colType() . " = ? ";

		$stmt = $this->db->prepare($query);
		if($stmt->execute(array(
			$this->active,
			$this->label,
			$this->minimumLength,
			$this->numberOfDigits,
			$this->specialCharacters,
			$this->upperCaseLetter,
			$this->lowerCaseLetter,
			$this->type
		))) {
			return true;
		}

		return false;
	}

	/**
	 * @return integer
	 */
	public function getID() {
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getLabel() {
		return $this->label;
	}

	/**
	 * @return integer
	 */
	public function getMinimumLength() {
		return $this->minimumLength;
	}


	/**
	 * @return integer
	 */
	public function getNumberOfDigits() {
		return $this->numberOfDigits;
	}

	/**
	 * @return string
	 */
	public function getSpecialCharacters() {
		return $this->specialCharacters;
	}

	/**
	 * @return string
	 */
	public function getUpperCaseLetter() {
		return $this->upperCaseLetter;
	}

	/**
	 * @return string
	 */
	public function getLowerCaseLetter() {
		return $this->lowerCaseLetter;
	}

	/**
	 * @return string
	 */
	public function getActive() {
		return $this->active;
	}

	/**
	 * @param $value
	 */
	public function setActive($value) {
		$this->active = $value;
	}

	/**
	 * @param string $label
	 */
	public function setLabel($label) {
		$this->label = $label;
	}

	/**
	 * @param mixed $minimumLength
	 */
	public function setMinimumLength($minimumLength) {
		$this->minimumLength = $minimumLength;
	}

	/**
	 * @param string $numberOfDigits
	 */
	public function setNumberOfDigits($numberOfDigits) {
		$this->numberOfDigits = $numberOfDigits;
	}

	/**
	 * @param string $upperCaseLetter
	 */
	public function setUpperCaseLetter($upperCaseLetter) {
		$this->upperCaseLetter = $upperCaseLetter;
	}

	/**
	 * @param string $lowerCaseLetter
	 */
	public function setLowerCaseLetter($lowerCaseLetter) {
		$this->lowerCaseLetter = $lowerCaseLetter;
	}

	/**
	 * @param string $specialCharacters
	 */
	public function setSpecialCharacters($specialCharacters) {
		$this->specialCharacters = $specialCharacters;
	}

	/**
	 * @return array
	 * @param $activeOnly
	 */
	public function getAllPasswordTypes($activeOnly = true) {

		$return = array();
		$query = 'SELECT type FROM ' . self::passwordTable();
		if($activeOnly) {
			$query .= " WHERE active = 'true'";
		}
		$stmt = $this->db->prepare($query);
		if($stmt->execute()) {
			while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$return[] = $row['type'];
			}
		}

		return $return;
	}
}
