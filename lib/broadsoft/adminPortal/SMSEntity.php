<?php
/**
 * Created by Dipin Krishna.
 * Date: 9/19/17
 * Time: 5:26 PM
 */

require($_SERVER['DOCUMENT_ROOT'] . "/../twilio-php/Twilio/autoload.php");

use Twilio\Rest\Client as TwilioClient;

class SMSEntity
{

	private $smsSID;
	private $smsToken;
	private $phoneNumber;

	private $fromNumber;

	// Constructor - Needs smsSID and smsToken
	public function __construct($smsSID, $smsToken, $fromNumber)
	{
		$this->smsSID = $smsSID;
		$this->smsToken = $smsToken;
		$this->fromNumber = $fromNumber;
	}

	public function sendMessage($phoneNumber, $message)
	{

		//error_log("$phoneNumber, $message");

		$this->phoneNumber = $phoneNumber;

		// Check if we have all required params
		if (isset($this->smsSID) && isset($this->smsToken) && isset($this->phoneNumber) && isset($this->fromNumber)) {

			try {
				$client = new TwilioClient($this->smsSID, $this->smsToken);

				// Send the message
				$client->messages->create(
				//To Phone Number
					$this->phoneNumber,
					array(
						// A Twilio phone number you purchased at twilio.com/console
						'from' => $this->fromNumber,
						// the body of the text message you'd like to send
						'body' => $message
					)
				);

			} catch (Exception $e) {

				error_log($e->getMessage());

				return false;
			}

			return true;

		} else {

			return false;
		}
	}

}