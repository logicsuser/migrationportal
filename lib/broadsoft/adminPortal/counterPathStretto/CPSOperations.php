<?php 
class CPSOperations{
    
    function counterPathAccountAdd($postArray, $payloadData){
        $resp = array();
        global $counterPath_ews, $counterPathUserid, $counterPathPasswd, $counterPathProtocol, $counterPathPort, $counterpathGroupName;
        $dataToSend = $this->payLoadDataCreation($postArray["cpsArray"], $payloadData);
        $username = $counterPathUserid;
        $password = $counterPathPasswd;
        $url = $counterPathProtocol."://".$counterPath_ews.":".$counterPathPort."/stretto/prov/user?method=POST&groupName=".$counterpathGroupName."&userName=".$postArray["cpsArray"]["userName"]."&profileName=".$postArray["cpsArray"]["profileName"];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataToSend);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
        
        $result = curl_exec($ch);
        if (strpos($result, "User already exists") !== false) {
            $resp["isexist"] = "true";
        }else{
            $resp["isexist"] = "false";
        }
        $info = curl_getinfo($ch);
        
        $resp["status"] = $info["http_code"];
        
        curl_close ($ch);
        return $resp;
    }
    
    function counterPathAccountModify($postArray, $payloadData){
        //print_r($postArray);
        $modResponse="";
        global $counterPath_ews, $counterPathUserid, $counterPathPasswd, $counterPathProtocol, $counterPathPort, $counterpathGroupName;
        $dataToSend = $this->payLoadDataCreation($postArray["cpsArray"], $payloadData);
        //print_r($dataToSend);
        $username = $counterPathUserid;
        $password = $counterPathPasswd;
        $url = $counterPathProtocol."://".$counterPath_ews.":".$counterPathPort."/stretto/prov/user?method=PUT&groupName=".$counterpathGroupName."&userName=".$postArray["cpsArray"]["userName"]."&profileName=".$postArray["cpsArray"]["profileName"];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataToSend);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
        
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        if ($info["http_code"] == "200") {
            $modResponse = "true";
            //echo 'Error:' . curl_error($ch);
        }else{
            $modResponse = "false";
        }
        
        curl_close ($ch);
        return $modResponse;
    }
    
    function counterPathAccountDelete($postArray){
        $delResponse = "";
        global $counterPath_ews, $counterPathUserid, $counterPathPasswd, $counterPathProtocol, $counterPathPort, $counterpathGroupName;
        
        $username = $counterPathUserid;
        $password = $counterPathPasswd;
        $url = $counterPathProtocol."://".$counterPath_ews.":".$counterPathPort."/stretto/prov/user?method=DELETE&groupName=".$counterpathGroupName."&userName=".$postArray["cpsArray"]["userName"];
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
        
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        if ($info["http_code"] == "200") {
            $delResponse = "true";
            //echo 'Error:' . curl_error($ch);
        }else{
            $delResponse = "false";
        }
        curl_close ($ch);
        return $delResponse;
    }
    
    function counterPathAccountGet($userName){
        
        $getResponse = "false";
        global $counterPath_ews, $counterPathUserid, $counterPathPasswd, $counterPathProtocol, $counterPathPort, $counterpathGroupName;
        
        $username = $counterPathUserid;
        $password = $counterPathPasswd;
        $url = $counterPathProtocol."://".$counterPath_ews.":".$counterPathPort."/stretto/prov/user?method=GET&groupName=".$counterpathGroupName."&userName=".$userName;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
        
        $result = curl_exec($ch);
        //print_r($result);
        $info = curl_getinfo($ch);
        //print_r($info);
        if ($info["http_code"] == "200") {
            if (strpos($result, "userName") !== false) {
                $getResponse = "true";
            }
        }
        curl_close ($ch);
        return $getResponse;
    }
    
    function payLoadDataCreation($cpsArray, $payloadData){
        $commandToSend = $payloadData;
        $commandToSend = str_replace("<express_email_value>",$cpsArray["userEmailAddress"], $commandToSend);
        $commandToSend = str_replace("<express_account_name>",$cpsArray["accountName"], $commandToSend);
        $commandToSend = str_replace("<express_authorizationName_value>",$cpsArray["authorizationName"], $commandToSend);
        $commandToSend = str_replace("<express_password_value>",$cpsArray["password"], $commandToSend);
        $commandToSend = str_replace("<express_username_value>",$cpsArray["crndusername"], $commandToSend);
        $commandToSend = str_replace("<express_domain_value>",$cpsArray["domain"], $commandToSend);
        $commandToSend = str_replace("<express_proxy_value>",$cpsArray["proxy"], $commandToSend);
        //print_r($commandToSend);
        return $commandToSend;
    }
    
    
    
    /*function cerateCPSUser(){
        
    }
    
    function modifyCPSUser(){
        
    }
    
    function deleteCPSUser(){
        
    }
    
    function getCPSUser(){
        
    }
    
    function sendCommandToCPSServer(){
        
    }
    
    function checkCPSConnection(){
        
    }
    
    function getParamArray(groupName,userName,profileName) {
        $paramDetails = new [];
        $paramDetails["groupName"]=$groupName;
        $paramDetails["userName"]=$userName;
        if(profileName != null){
            $paramDetails["profileName"]=$profileName;
        }
            
        return $paramDetails;
    }
    
    function getDataArray($email,accountName,authName,password,username,domain,proxy) {
        $accountDetails = new [];
        $accountDetails["email"]=$email;
        $accountDetails["authName"]=$authName;
        $accountDetails["pwd"]=$password;
        $accountDetails["username"]=$username;
        $accountDetails["domain"]=$domain;
        $accountDetails["proxy"]=$proxy;
        
        return $paramDetails;
    }*/
    
}
?>
