<?php

/**
 * Created by Karl.
 * Date: 1/23/2017
 */
class DBExpressAdmins
{
    //TODO: Implement isPasswordExpired
    //TODO: Implement updateTimestamp

    private $db;
    private $txt = "";
    private $newRecord = true;

    private $adminID;
    private $password;
    private $firstName;
    private $lastName;
    private $emailAddress;
    private $superUser;
    private $disabled = false;
    private $failedLogins;
    private $lastPasswordChange;
    private $bsAuthentication = false;
    private $updates = array();

    private static function tableName() { return "admins"; }
    private static function MaxNumFailedLogins() { return 3; }

    private static function attrAdminID()            { return "adminID"; }
    private static function attrPassword()           { return "password"; }
    private static function attrFirstName()          { return "firstName"; }
    private static function attrLastName()           { return "lastName"; }
    private static function attrEmailAddress()       { return "emailAddress"; }
    private static function attrSuperUser()          { return "superUser"; }
    private static function attrDisabled()           { return "disabled"; }
    private static function attrFailedLogins()       { return "failedLogins"; }
    private static function attrLastPasswordChange() { return "lastPasswordChange"; }
    private static function attrBsAuthentication()   { return "bsAuthentication"; }


    /**
     * DBExpressAdmins constructor.
     * @param $db
     * @param $adminID
     */
    public function __construct($db, $adminID) {
        $this->db = $db;
        $this->adminID = $adminID;

        $this->get();
    }

    public function getTxt() {
        return $this->txt;
    }

    /*************************************************
     * @param $password
     */
    public function updatePassword($password) {
        // Query
        // update admins set password='0011223344', disabled='false', failedLogins='0', lastPasswordChange='Now()'
        // where adminID='kdalka';

        $lastPasswordChange = "Now()";
        $query  = "update " . self::tableName() . " set " . self::attrPassword() . "='" . md5($password) . "', ";
        $query .= self::attrDisabled() . "='false', " . self::attrFailedLogins() . "='0', ";
        $query .= self::attrLastPasswordChange() . "='" . $lastPasswordChange . "'";
        $query .= " where " . self::attrAdminID() . "='" . $this->adminID . "'";

        $this->db->query($query);
    }

    /*************************************************
     *
     */
    public function disableAccount() {
        // Query
        // update admins set disabled='true' where adminID='kdalka';

        $query  = "update " . self::tableName() . " set " . self::attrDisabled() . "='true'";
        $query .= " where " . self::attrAdminID() . "='" . $this->adminID . "'";

        $this->db->query($query);
    }

    /*************************************************
     * @param $password
     * @return bool
     */
    public function validatePassword($password) {

        // Authenticate against password stored in DB

        if ($this->bsAuthentication) {
            return $this->authenticateBroadSoft($password);
        }

        // Authenticate against password stored in DB

        if ($this->isDisabled()) { return false; }

        if (md5($password) == $this->password) {
            if ($this->failedLogins == 0) {
                return true;
            }

            // reset failed logins counter
            $query  = "update " . self::tableName();
            $query .= " set " . self::attrFailedLogins() . "='0'";
            $query .= " where " . self::attrAdminID() . "='" . $this->adminID . "'";
            $this->db->query($query);

            return true;
        }

        // password failed update failed logins count
        $failedLogins = $this->failedLogins + 1;
        $disabled = $failedLogins >= self::MaxNumFailedLogins() ? "true" : "false";

        // build the query
        // update admins set disabled='true', failedLogins='3' where adminID='karl';

        $query  = "update " . self::tableName();
        $query .= " set " . self::attrDisabled() . "='" . $disabled . "', '" . self::attrFailedLogins() . "='" . $failedLogins . "'";
        $query .= " where " . self::attrAdminID() . "='" . $this->adminID . "'";
        $this->db->query($query);

        return false;
    }

    /*************************************************
     * @return mixed
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /*************************************************
     * @param $val
     */
    public function setFirstName($val) {
        $this->updates[self::attrFirstName()] = $val;
    }

    /*************************************************
     * @return mixed
     */
    public function getLastName() {
        return $this->lastName;
    }

    /*************************************************
     * @param $val
     */
    public function setLastName($val) {
        $this->updates[self::attrLastName()] = $val;
    }

    /*************************************************
     * @return mixed
     */
    public function getEmailAddress() {
        return $this->emailAddress;
    }

    /*************************************************
     * @param $val
     */
    public function setEmailAddress($val) {
        $this->updates[self::attrEmailAddress()] = $val;
    }

    /*************************************************
     * @return bool
     */
    public function isSuperUser() {
        return $this->superUser;
    }

    /*************************************************
     * @param $val
     */
    public function setSuperUser($val) {
        $this->updates[self::attrSuperUser()] = $val;
    }

    /*************************************************
     * @return bool
     */
    public function isDisabled() {
        return $this->disabled == "true";
    }

    /*************************************************
     * @return mixed
     */
    public function getFailedLogins() {
        return $this->failedLogins;
    }

    /*************************************************
     * @return bool
     */
    public function isPasswordExpired() {
        return false;
    }

    /*************************************************
     *
     */
    public function updateTimestamp() {

    }

    /*************************************************
     * @return bool
     */
    public function isBsAuthentication() {
        return $this->bsAuthentication == "true";
    }

    /*************************************************
     * @param $val
     */
    public function setBsAuthentication($val) {
        $this->updates[self::attrBsAuthentication()] = $val;
    }

    /*************************************************
     * @param $firstName
     * @param $lastName
     * @param $password
     * @param $emailAddress
     * @param $superUser
     * @param $bsAuthentication
     */
    public function create($firstName, $lastName, $password, $emailAddress, $superUser, $bsAuthentication) {
        // query
        // insert into admins (`adminID`,`password`,`firstName`,`lastName`,`emailAddress`,`superUser`,`lastPasswordChange`,`bsAuthentication`)
        // values ('kdalka','123456','Karl','Dalka','karl@averistar.com','true','0000-00-00 00:00:00','false');

        $lastPasswordChange = "Now()";
        $query = "insert into " . self::tableName() . " (`" . self::attrAdminID() . "`,`";
        $query .= self::attrPassword() . "`,`" . self::attrFirstName() . "`,`" . self::attrLastName() . "`,`";
        $query .= self::attrEmailAddress() . "`,`" . self::attrSuperUser() . "`,`" . self::attrLastPasswordChange() . "`,`" . self::attrBsAuthentication() . "`)";
        $query .= "values ('" . $this->getAdminID() . "','" . md5($password) . "','" . $firstName . "','" . $lastName . "','";
        $query .= $emailAddress . "','" . $superUser . "','" . $lastPasswordChange . "','" . $bsAuthentication . "')";

        $this->db->query($query);
    }

    /*************************************************
     *
     */
    public function update() {
        // Query
        // update admins set firstName='AnneMarie', superUser='false', emailAddress='kdalka@consolitusa.com' where adminID='kdalka';

        $firstParam = true;
        $query = "update " . self::tableName() . " set";

        foreach ($this->updates as $attr => $val ) {

            if ($firstParam) {
                $firstParam = false;
            } else {
                $query .= ",";
            }

            $query .= " " . $attr . "='" . $val . "'";
        }

        $query .= " where " . self::attrAdminID() . "='" . $this->adminID . "'";
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function get()
    {
//       select * from admins where adminID='test1';
        /*
			$usersQuery = "select id, userName, firstName, lastName, emailAddress, superUser, Timestampdiff(DAY, lastPasswordChange, Now()) as dayDifference, bsAuthentication";
			$usersQuery .= " from users where userName='" . $uName . "'";
         */

        $query = "select * from " . self::tableName() . " where " . self::attrAdminID() . "='" . $this->adminID . "'";

        $query  = "select " . self::attrPassword() . ", " . self::attrFirstName() . ", " . self::attrLastName() . ", ";
        $query .= self::attrEmailAddress() . ", " . self::attrSuperUser() . ", " . self::attrDisabled() . ", " . self::attrFailedLogins() . ", ";
        //TODO: CONTINUATION HERE $query ..=

        $result = $this->db->query($query);

        while ($row = $result->fetch()) {
            $this->password = $row[self::attrPassword()];
            $this->firstName = $row[self::attrFirstName()];
            $this->lastName = $row[self::attrLastName()];
            $this->emailAddress = $row[self::attrEmailAddress()];
            $this->superUser = $row[self::attrSuperUser()];
            $this->disabled = $row[self::attrDisabled()];
            $this->failedLogins = $row[self::attrFailedLogins()];
            $this->lastPasswordChange = $row[self::attrLastPasswordChange()];
            $this->bsAuthentication = $row[self::attrBsAuthentication()];

            $this->newRecord = false;
        }
    }


    //-----------------------------------------------------------------------------------------------------------------
    private function authenticateBroadSoft($password) {
        global $sessionid, $client;

        require_once("/var/www/lib/broadsoft/login.php");
        $xmlinput = xmlHeader($sessionid, "AuthenticationVerifyRequest14sp8");
        $xmlinput .= "<userId>" . $this->adminID . "</userId>";
        $xmlinput .= "<password>" . $password . "</password>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        return isset($xml->command->userId);
    }
}
