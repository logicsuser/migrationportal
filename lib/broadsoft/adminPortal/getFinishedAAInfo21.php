<?php

	if (isset($aa["aa"]))
	{
		unset($aa["aa"]);
	}

	if ($ociVersion == "19")
	{
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantGetInstanceRequest19");
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantGetInstanceRequest20");			
	}
	$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$aa["aaName"] = strval($xml->command->serviceInstanceProfile->name);
	$aa["callingLineIdLastName"] = strval($xml->command->serviceInstanceProfile->callingLineIdLastName);
	$aa["callingLineIdFirstName"] = strval($xml->command->serviceInstanceProfile->callingLineIdFirstName);
	$aa["phoneNumber"] = strval($xml->command->serviceInstanceProfile->phoneNumber);
	$aa["extension"] = strval($xml->command->serviceInstanceProfile->extension);
	$aa["timeZone"] = strval($xml->command->serviceInstanceProfile->timeZone);
	$aa["department"] = strval($xml->command->serviceInstanceProfile->department->name);
	$aa["eVSupport"] = strval($xml->command->enableVideo);
	if ($ociVersion == "17")
	{
		$aa["type"] = "Basic";
	}
	else
	{
		$aa["type"] = strval($xml->command->type);
	}
	if(!empty($aa["phoneNumber"])){
		require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
		
		$ccDn = new Dns ();
		$numberActivateResponse = $ccDn->getUserDNActivateListRequest ( $aaId );
		//echo "<pre>"; print_r($numberActivateResponse); die;
		if (empty ( $numberActivateResponse ['Error'] )) {
			if (! empty ( $numberActivateResponse ['Success'] [0] ['phoneNumber'] )) {
				$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
			}
			if (! empty ( $numberActivateResponse ['Success'] [0] ['status'] )) {
				$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
			}
		}
		$aa["aaActivateNumber"] = $phoneNumberStatus;
	}
	if ($ociVersion == "21" || $ociVersion == "22")
	{
		$aa["firstDigitTimeoutSeconds"] = strval($xml->command->firstDigitTimeoutSeconds);
	}
	$aa["businessHoursSchedule"] = strval($xml->command->businessHours->name);
	$aa["holidaySchedule"] = strval($xml->command->holidaySchedule->name);
	$aa["nameDialingEntries"] = strval($xml->command->nameDialingEntries);

	$aa["bh"]["announcementSelection"] = strval($xml->command->businessHoursMenu->announcementSelection);
	
	if(isset($xml->command->businessHoursMenu->audioFile->name)){
	    $aa["bh"]["announcementId"] = strval($xml->command->businessHoursMenu->audioFile->name).".".strval($xml->command->businessHoursMenu->audioFile->mediaFileType);
	}else{
	    $aa["bh"]["announcementId"] = "";
	}
	
	//code to get the value from database for announcement of auto attendant
	//$bhAnnouncementId = $announcement->getAutoattendantAnnouncementInfo($db, $aaId, 'bhAnnouncementId');
	//get announcement name bu using announcement id
	/*if($bhAnnouncementId <> 0){
		$where['announcement_id'] = $bhAnnouncementId;
		$getAnnouncementInfo = $announcement->getAnnouncement($db, $where);
		$aa["bh"]["announcementId"] = $getAnnouncementInfo[0]['announcement_name'];	
	}else{
		$aa["bh"]["announcementId"] = "";
	}*/
//	$aa["bh"]["audioFileDescription"] = strval($xml->command->businessHoursMenu->audioFileDescription);
	$aa["bh"]["enableLevelExtensionDialing"] = strval($xml->command->businessHoursMenu->enableFirstMenuLevelExtensionDialing);
	foreach ($xml->command->businessHoursMenu->keyConfiguration as $key => $value)
	{
		$aa["bh"]["keys"][strval($value->key)]["desc"] = strval($value->entry->description);
		$aa["bh"]["keys"][strval($value->key)]["action"] = strval($value->entry->action);
		$aa["bh"]["keys"][strval($value->key)]["phoneNumber"] = strval($value->entry->phoneNumber);
		//$aa["bh"]["keys"][strval($value->key)]["phoneNumber"] = strval($value->entry->description);
		if ($ociVersion == "17")
		{
			$aa["bh"]["keys"][strval($value->key)]["submenuId"] = "";
		}
		else
		{
			$aa["bh"]["keys"][strval($value->key)]["submenuId"] = strval($value->entry->submenuId);
		}
		
		$playAnnouncement = strval($value->entry->action);
		
		if($playAnnouncement == "Play Announcement"){
		    if(isset($value->entry->audioFile->name)){
		        $aa["bh"]["keys"][strval($value->key)]["announcements"] = $value->entry->audioFile->name.".".$value->entry->audioFile->mediaFileType;
		    }else{
		        $aa["bh"]["keys"][strval($value->key)]["announcements"] = "";
		    }
		}
		
		/*if($playAnnouncement == "Play Announcement"){
			$where['audioFileDescription'] = strval($value->entry->audioFileDescription);
			$announcement = new Announcement();
			$getAnnouncementInfo = $announcement->getAnnouncementByFile($db, $where['audioFileDescription']);
			if(count($getAnnouncementInfo) > 0){
				$aa["bh"]["keys"][strval($value->key)]["announcements"] = $getAnnouncementInfo[0]['announcement_id'];
			}else{
				$aa["bh"]["keys"][strval($value->key)]["announcements"] = "";
			}
		}else{
			$aa["bh"]["keys"][strval($value->key)]["announcements"] = "";
		}*/
		
	}

	$aa["ah"]["announcementSelection"] = strval($xml->command->afterHoursMenu->announcementSelection);
	
	if(isset($xml->command->afterHoursMenu->audioFile->name)){
	    $aa["ah"]["announcementId"] = strval($xml->command->afterHoursMenu->audioFile->name).".".strval($xml->command->afterHoursMenu->audioFile->mediaFileType);
	}else{
	    $aa["ah"]["announcementId"] = "";
	}
	
	//code to get the value from database for announcement of auto attendant
	/*$ahAnnouncementId = $announcement->getAutoattendantAnnouncementInfo($db, $aaId, 'ahAnnouncementId');
	if($ahAnnouncementId <> 0){
		$where['announcement_id'] = $ahAnnouncementId;
		$getAnnouncementInfo = $announcement->getAnnouncement($db, $where);
		$aa["ah"]["announcementId"] = $getAnnouncementInfo[0]['announcement_name'];
	}else{
		$aa["ah"]["announcementId"] = "";
	}*/
//	$aa["ah"]["audioFileDescription"] = strval($xml->command->afterHoursMenu->audioFileDescription);
	$aa["ah"]["enableLevelExtensionDialing"] = strval($xml->command->afterHoursMenu->enableFirstMenuLevelExtensionDialing);
	foreach ($xml->command->afterHoursMenu->keyConfiguration as $key => $value)
	{
		$aa["ah"]["keys"][strval($value->key)]["desc"] = strval($value->entry->description);
		$aa["ah"]["keys"][strval($value->key)]["action"] = strval($value->entry->action);
		$aa["ah"]["keys"][strval($value->key)]["phoneNumber"] = strval($value->entry->phoneNumber);
		if ($ociVersion == "17")
		{
			$aa["ah"]["keys"][strval($value->key)]["submenuId"] = "";
		}
		else
		{
			$aa["ah"]["keys"][strval($value->key)]["submenuId"] = strval($value->entry->submenuId);
		}
		
		$playAnnouncement = strval($value->entry->action);
		
		if($playAnnouncement == "Play Announcement"){
		    if(isset($value->entry->audioFile->name)){
		        $aa["ah"]["keys"][strval($value->key)]["announcements"] = $value->entry->audioFile->name.".".$value->entry->audioFile->mediaFileType;
		    }else{
		        $aa["ah"]["keys"][strval($value->key)]["announcements"] = "";
		    }
		}
		
		/*if($playAnnouncement == "Play Announcement"){
			$where['audioFileDescription'] = strval($value->entry->audioFileDescription);
			$announcement = new Announcement();
			$getAnnouncementInfo = $announcement->getAnnouncementByFile($db, $where['audioFileDescription']);
			if(count($getAnnouncementInfo) > 0){
				$aa["ah"]["keys"][strval($value->key)]["announcements"] = $getAnnouncementInfo[0]['announcement_id'];
			}else{
				$aa["ah"]["keys"][strval($value->key)]["announcements"] = "";
			}
		}else{
			$aa["ah"]["keys"][strval($value->key)]["announcements"] = "";
		}*/
	}

	if ($aa["type"] == "Standard") //BroadSoft version 19 or later
	{
		$aa["h"]["announcementSelection"] = strval($xml->command->holidayMenu->announcementSelection);
		
		if(isset($xml->command->holidayMenu->audioFile->name)){
		    $aa["h"]["announcementId"] = strval($xml->command->holidayMenu->audioFile->name).".".strval($xml->command->holidayMenu->audioFile->mediaFileType);
		}else{
		    $aa["h"]["announcementId"] = "";
		}
		
		//code to get the value from database for announcement of auto attendant
		/*$hAnnouncementId = $announcement->getAutoattendantAnnouncementInfo($db, $aaId, 'hAnnouncementId');
		if($hAnnouncementId <> 0){
			$where['announcement_id'] = $hAnnouncementId;
			$getAnnouncementInfo = $announcement->getAnnouncement($db, $where);
			$aa["h"]["announcementId"] = $getAnnouncementInfo[0]['announcement_name'];
		}else{
			$aa["h"]["announcementId"] = "";
		}*/
//		$aa["h"]["audioFileDescription"] = strval($xml->command->holidayMenu->audioFileDescription);
		$aa["h"]["enableLevelExtensionDialing"] = strval($xml->command->holidayMenu->enableFirstMenuLevelExtensionDialing);
		foreach ($xml->command->holidayMenu->keyConfiguration as $key => $value)
		{
			$aa["h"]["keys"][strval($value->key)]["desc"] = strval($value->entry->description);
			$aa["h"]["keys"][strval($value->key)]["action"] = strval($value->entry->action);
			$aa["h"]["keys"][strval($value->key)]["phoneNumber"] = strval($value->entry->phoneNumber);
			$aa["h"]["keys"][strval($value->key)]["submenuId"] = strval($value->entry->submenuId);
			
			$playAnnouncement = strval($value->entry->action);
			
			if($playAnnouncement == "Play Announcement"){
			    if(isset($value->entry->audioFile->name)){
			        $aa["h"]["keys"][strval($value->key)]["announcements"] = $value->entry->audioFile->name.".".$value->entry->audioFile->mediaFileType;
			    }else{
			        $aa["h"]["keys"][strval($value->key)]["announcements"] = "";
			    }
			}
			
			/*if($playAnnouncement == "Play Announcement"){
				$where['audioFileDescription'] = strval($value->entry->audioFileDescription);
				$announcement = new Announcement();
				$getAnnouncementInfo = $announcement->getAnnouncementByFile($db, $where['audioFileDescription']);
				if(count($getAnnouncementInfo) > 0){
					$aa["h"]["keys"][strval($value->key)]["announcements"] = $getAnnouncementInfo[0]['announcement_id'];
				}else{
					$aa["h"]["keys"][strval($value->key)]["announcements"] = "";
				}
			}else{
				$aa["h"]["keys"][strval($value->key)]["announcements"] = "";
			}*/
		}

		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuGetListRequest");
		$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		$a = 0;
		foreach ($xml->command->submenuTable->row as $key => $value)
		{
			$aa["submenu" . $a]["id"] = strval($value->col[0]);

			if ($ociVersion == "19")
			{
			    $xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuGetRequest");
				
			}
			else
			{
			    $xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuGetRequest20");
			}
			$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
			$xmlinput .= "<submenuId>" . htmlspecialchars(strval($value->col[0])) . "</submenuId>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

			$aa["submenu" . $a]["enableLevelExtensionDialing"] = strval($xml->command->enableLevelExtensionDialing);
			foreach ($xml->command->keyConfiguration as $k => $v)
			{
				$aa["submenu" . $a]["keys"][strval($v->key)]["desc"] = strval($v->entry->description);
				$aa["submenu" . $a]["keys"][strval($v->key)]["action"] = strval($v->entry->action);
				$aa["submenu" . $a]["keys"][strval($v->key)]["phoneNumber"] = strval($v->entry->phoneNumber);
				$aa["submenu" . $a]["keys"][strval($v->key)]["submenuId"] = strval($v->entry->submenuId);
				if(isset($v->entry->audioFile->name)){
				    $aa["submenu" . $a]["keys"][strval($v->key)]["announcements"] = strval($v->entry->audioFile->name).".".strval($v->entry->audioFile->mediaFileType);
				}else{
				    $aa["submenu" . $a]["keys"][strval($v->key)]["announcements"] = "";
				}
			}
			$a++;
		}
	}
?>
