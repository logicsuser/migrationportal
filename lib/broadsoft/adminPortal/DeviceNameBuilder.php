<?php

/**
 * Created by Karl.
 * Date: 10/4/2016
 */
class DeviceNameBuilder extends ResourceNameBuilder
{

    public function __construct($nameStr, $dn, $extension, $groupNumber, $forceResolve = false) {

        $registeredParams = array(
            ResourceNameBuilder::DN,
            ResourceNameBuilder::EXT,
            //ResourceNameBuilder::USR_CLID,
            ResourceNameBuilder::GRP_DN,
            //ResourceNameBuilder::USR_ID,
            ResourceNameBuilder::GRP_ID,
            //ResourceNameBuilder::SYS_DFLT_DOMAIN,
            //ResourceNameBuilder::DEFAULT_DOMAIN,
            //ResourceNameBuilder::SRG_DOMAIN,
            //ResourceNameBuilder::GRP_PROXY_DOMAIN,
            ResourceNameBuilder::FIRST_NAME,
            ResourceNameBuilder::LAST_NAME,
            //ResourceNameBuilder::DEV_NAME,
            ResourceNameBuilder::DEV_TYPE,
            ResourceNameBuilder::MAC_ADDR,
            ResourceNameBuilder::INT_1,
            ResourceNameBuilder::INT_2,
            ResourceNameBuilder::TXT
        );

        if ($dn != "")                  { $this->defaults[ResourceNameBuilder::DN]              = $dn; }
        if ($extension != "")           { $this->defaults[ResourceNameBuilder::EXT]             = $extension; }
        if ($groupNumber != "")         { $this->defaults[ResourceNameBuilder::GRP_DN]          = $groupNumber; }

        parent::__construct($nameStr, $registeredParams, $forceResolve);
    }


    public function getFallbackName()
    {
        $name = "x" . $this->getDefaultAttribute(ResourceNameBuilder::EXT);

        $attr = $this->getDefaultAttribute(ResourceNameBuilder::DN);
        if ($attr != "") { return $attr . $name; }

        $attr = $this->getDefaultAttribute(ResourceNameBuilder::GRP_DN);
        if ($attr != "") { return $attr . $name; }

        return "000" . $name;
    }
}
