<?php
require_once("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
class SimRingCriteriaManagement{
        
        function getSimRingCrietriaList($userId){            
            global $sessionid, $client , $schedules;
                    $userAssignedTags = array();
                    $xmlinput  = xmlHeader($sessionid, "UserSimultaneousRingPersonalGetRequest17");
                    $xmlinput .= "<userId>" .$userId. "</userId>";
                    $xmlinput .= xmlFooter();

                    $response = $client->processOCIMessage(array("in0" => $xmlinput));
                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                    
                    $errMsg = readErrorXmlGenuine($xml);
                    $returnResponse["Error"]     = "";
                    $returnResponse["Success"]   = array();
                    $simultaneousRingNumberArr   = array();
                    $criteriaTableArr            = array();
                    $activeCrtiaNameArr          = array();

                    if($errMsg != "") {
                            $returnResponse["Error"] = $errMsg;
                    } else {

                        $returnResponse["Success"]['activeStatus']      = strval($xml->command->isActive);
                        $returnResponse["Success"]['doNotRingIfOnCall'] = strval($xml->command->doNotRingIfOnCall);

                        if(isset($xml->command->simultaneousRingNumber)) {
                            $counter = 0;
                            foreach ($xml->command->simultaneousRingNumber as $k => $val){                             
                                $simultaneousRingNumberArr[$counter]["phoneNumber"]                = strval($val->phoneNumber);
                                $simultaneousRingNumberArr[$counter]["answerConfirmationRequired"] = strval($val->answerConfirmationRequired);
                                $counter = $counter + 1;
                            }
                            $returnResponse["Success"]['simultaneousRingNumberArr'] = $simultaneousRingNumberArr;
                        }

                        if(isset($xml->command->criteriaTable->row)) {
                                $count = 0;   
                                foreach ($xml->command->criteriaTable->row as $key => $value){
                                            if(strval($value->col[0])=="true"){
                                                $activeCrtiaNameArr[$count] = strval($value->col[1]);
                                            }
                                            $crtiaNameArr[$count]                                           = strval($value->col[1]);
                                            $criteriaTableArr[$count]["isActive"]                           = strval($value->col[0]);
                                            $criteriaTableArr[$count]["criteriaName"]                       = strval($value->col[1]);
                                            
                                            $criteriaTableArr[$count]["timeSchedule"]                       = $this->getScheduleName(strval($value->col[2]), "Time", "timeSchedule");
                                            //$criteriaTableArr[$count]["timeSchedule"]                       = strval($value->col[2]); 
                                            
                                            $criteriaTableArr[$count]["holidaySchedule"]                    = $this->getScheduleName(strval($value->col[5]), "Holiday", "holidaySchedule");
                                            //$criteriaTableArr[$count]["holidaySchedule"]                    = strval($value->col[5]);
                                            
                                            $criteriaTableArr[$count]["callsFromIncomingPhoneNumberList"]   = strval($value->col[3]);
                                            $criteriaTableArr[$count]["userSimRingPersonal"]                = strval($value->col[4]);
                                          //  $criteriaTableArr[$count]["holidaySchedule"]                    = strval($value->col[5]);
                                            $count = $count + 1;
                                    }

                                $returnResponse["Success"]['criteriaTableArr']       = $criteriaTableArr;
                                $returnResponse["Success"]['activeCriteriaNameArr']  = $activeCrtiaNameArr;
                                $returnResponse["Success"]['crtiaNameArr']  = $crtiaNameArr;
                            }			

                    }		
                    return $returnResponse;
	}
	
	function userModifyDeleteSimRing($userId, $creteriaName){
	  
                    global $sessionid, $client;
                    $xmlinput = xmlHeader($sessionid, "UserSimultaneousRingPersonalDeleteCriteriaRequest");
                    $xmlinput .= "<userId>".$userId."</userId>";
                    $xmlinput .= "<criteriaName>".$creteriaName ."</criteriaName>";
                    $xmlinput .= xmlFooter();

                    $response = $client->processOCIMessage(array("in0" => $xmlinput));
                    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                    return $xml;
	}
	
	  function addSimRingCreteria($value)
	  {             
	           global $sessionid, $client;
                   $dnList = $value["criteriaIncomingPhone"];
                   $returnResponse["Error"]   = "";
	           $returnResponse["Success"] = "";
	     
	            $xmlInput = xmlHeader($sessionid, "UserSimultaneousRingPersonalAddCriteriaRequest");
	            $xmlInput .= "<userId>" . $value["userId"] . "</userId>";
	            $xmlInput .= "<criteriaName>".$value["criteriaName"]."</criteriaName>";
	            
	            if ($value["timeScheduleName"] != "") {
	                $xmlInput .= "<timeSchedule>";
	                $xmlInput .= "<type>" . $value["timeScheduleLevel"] . "</type>";
	                $xmlInput .= "<name>" . $value["timeScheduleName"] . "</name>";
	                $xmlInput .= "</timeSchedule>";
	            }
	            
	            if ($value["holidayScheduleName"] != "") {
	                $xmlInput .= "<holidaySchedule>";
	                $xmlInput .= "<type>" . $value["holidayScheduleLevel"] . "</type>";
	                $xmlInput .= "<name>" . $value["holidayScheduleName"] . "</name>";
	                $xmlInput .= "</holidaySchedule>";
	            }
                    
	            $xmlInput .= "<blacklisted>".$value["useSimRingPersonal"]."</blacklisted>";                    
	            $xmlInput .= "<fromDnCriteria>";
                    
                    $xmlInput .= "<fromDnCriteriaSelection>".$value["simRingIncomingCallsFrom"]."</fromDnCriteriaSelection>";
                    $xmlInput .= "<includeAnonymousCallers>".$value["simRingIncomingPhoeFromPrivate"]."</includeAnonymousCallers>";
                    $xmlInput .= "<includeUnavailableCallers>".$value["simRingIncomingPhoeFromUnavailable"]."</includeUnavailableCallers>";
                   
                    if(!empty($dnList)){
                        for ($i = 0; $i < count($dnList); $i ++) {
                            if($dnList[$i]!=""){
                                $xmlInput .= "<phoneNumber>" . $dnList[$i] . "</phoneNumber>";
                            }
                        }
                    }
	            $xmlInput .= "</fromDnCriteria>";
	            $xmlInput .= xmlFooter();
	            
	            $response = $client->processOCIMessage(array(
	                "in0" => $xmlInput
	            ));
                    
	            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);                    
                    if (readErrorXmlGenuine ( $xml ) != "") {
                        if (isset ( $xml->command->detail )) {
                            $returnResponse["Error"] = strval ( $xml->command->detail );
                        } else {
                            $returnResponse["Error"] = strval ( $xml->command->summaryEnglish );
                        }
                    } else { 
                        $returnResponse["Success"] = "Success";
                    }
                    return $returnResponse;
	    }
	
        /*update device custom config tag name */
	
	function updateSimRingCriteria($value) {
            
                   global $sessionid, $client;
                   $dnList = $value["criteriaIncomingPhone"];
                   $returnResponse["Error"]   = "";
	           $returnResponse["Success"] = "";
	     
	            $xmlInput = xmlHeader($sessionid, "UserSimultaneousRingPersonalModifyCriteriaRequest");
	            $xmlInput .= "<userId>" . $value["userId"] . "</userId>";
                    
                    $xmlInput .= "<criteriaName>".$value["oldCriteriaName"]."</criteriaName>";
	            $xmlInput .= "<newCriteriaName>".$value["criteriaName"]."</newCriteriaName>";
	            
	            if ($value["timeScheduleName"] != "") {
	                $xmlInput .= "<timeSchedule>";
	                $xmlInput .= "<type>" . $value["timeScheduleLevel"] . "</type>";
	                $xmlInput .= "<name>" . $value["timeScheduleName"] . "</name>";
	                $xmlInput .= "</timeSchedule>";
                    }
                    
                    if ($value["simRingCriteriaTime"] == "") {
                        $xmlInput .= "<timeSchedule xsi:nil='true'></timeSchedule>";   
                    }
	            
                    
                    
	            if ($value["holidayScheduleName"] != "") {
	                $xmlInput .= "<holidaySchedule>";
	                $xmlInput .= "<type>" . $value["holidayScheduleLevel"] . "</type>";
	                $xmlInput .= "<name>" . $value["holidayScheduleName"] . "</name>";
	                $xmlInput .= "</holidaySchedule>";
	            }
                    
                    if ($value["simRingCriteriaHoliday"] == "") {
                        $xmlInput .= "<holidaySchedule xsi:nil='true'></holidaySchedule>";   
                    }
                    if($value["useSimRingPersonal"]!=""){
                        $xmlInput .= "<blacklisted>".$value["useSimRingPersonal"]."</blacklisted>";   
                    }
	            $xmlInput .= "<fromDnCriteria>";
                    
                    $xmlInput .= "<fromDnCriteriaSelection>".$value["simRingIncomingCallsFrom"]."</fromDnCriteriaSelection>";
                    $xmlInput .= "<includeAnonymousCallers>".$value["simRingIncomingPhoeFromPrivate"]."</includeAnonymousCallers>";
                    $xmlInput .= "<includeUnavailableCallers>".$value["simRingIncomingPhoeFromUnavailable"]."</includeUnavailableCallers>";
                                      
                    
                    if(!empty($dnList)){
                        $xmlInput .= "<phoneNumberList>";
                        for ($i = 0; $i < count($dnList); $i++) {
                            if($dnList[$i]!=""){
                                $xmlInput .= "<phoneNumber>" . $dnList[$i] . "</phoneNumber>";
                            }
                        }
                        $xmlInput .= "</phoneNumberList>";
                    }
                    
	            $xmlInput .= "</fromDnCriteria>";
	            $xmlInput .= xmlFooter();
	            
	            $response = $client->processOCIMessage(array(
	                "in0" => $xmlInput
	            ));
                    
                    $xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
                    if (readErrorXmlGenuine ( $xml ) != "") {
                        if (isset ( $xml->command->detail )) {
                            $returnResponse["Error"] = strval ( $xml->command->detail );
                        } else {
                            $returnResponse["Error"] = strval ( $xml->command->summaryEnglish );
                        }
                    } else { 
                        $returnResponse["Success"] = "Success";
                    }
                    return $returnResponse;
	}
	
	function getSimRingCreteriaDetails($userId, $criteriaName) {
	    
	    global $sessionid, $client;
	    
	    $simRingCriteriaResponse ["Error"]   = array();
	    $simRingCriteriaResponse ["Success"] = array();
	    
	    $xmlinput = xmlHeader ( $sessionid, "UserSimultaneousRingPersonalGetCriteriaRequest" );
	    $xmlinput .= "<userId>" . $userId. "</userId>";
	    $xmlinput .= "<criteriaName>" . $criteriaName . "</criteriaName>";
	    
	    $xmlinput .= xmlFooter ();
	    $response = $client->processOCIMessage ( array (
	        "in0" => $xmlinput
	    ) );
	    $xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );  
	    
	    if (readErrorXmlGenuine ( $xml ) != "") {
	        if (isset ( $xml->command->detail )) {
	            $simRingCriteriaResponse ["Error"] = strval ( $xml->command->detail );
	        } else {
	            $simRingCriteriaResponse ["Error"] = strval ( $xml->command->summaryEnglish );
	        }
	    } else {
    	        
    	        $IncomingPhoneNumberArr = array();
    	        // $deviceCustomTagModifyResponse ["Success"] = $xml->command;
    	        $simRingCriteriaResponse["Success"]['blacklisted']     = strval($xml->command->blacklisted);    	        
    	        $simRingCriteriaResponse ["Success"]["criteriaName"]   = $criteriaName;
    	                        
                if(isset($xml->command->timeSchedule)){ 
                   $simRingCriteriaResponse["Success"]["timeScheduleLevel"] =  strval($xml->command->timeSchedule->type);
                   $simRingCriteriaResponse["Success"]["timeScheduleName"]  =  strval($xml->command->timeSchedule->name);
                }
                
                if(isset($xml->command->holidaySchedule)){ 
                   $simRingCriteriaResponse["Success"]["holidayScheduleLevel"] =  strval($xml->command->holidaySchedule->type);
                   $simRingCriteriaResponse["Success"]["holidayScheduleName"]  =  strval($xml->command->holidaySchedule->name);
                }
                
    	        if(isset($xml->command->fromDnCriteria)) {
    	            $counter = 0;    	       
    	            $phoneNumArr = array();
    	            foreach ($xml->command->fromDnCriteria as $k => $val){
                        
    	                $simRingCriteriaResponse["Success"]["fromDnCriteriaSelection"]   = strval($val->fromDnCriteriaSelection);
    	                $simRingCriteriaResponse["Success"]["includeAnonymousCallers"]   = strval($val->includeAnonymousCallers);
    	                $simRingCriteriaResponse["Success"]["includeUnavailableCallers"] = strval($val->includeUnavailableCallers);    	                
    	                if(!empty($xml->command->fromDnCriteria->phoneNumber)){
    	                   $counter = 0;
    	                   foreach ($xml->command->fromDnCriteria->phoneNumber as $phoneList){    	                       
    	                       $phoneNumArr[$counter] = strval($phoneList);
    	                       $counter = $counter + 1; 
    	                   } 
    	                   $simRingCriteriaResponse["Success"]["phoneNumberList"]   =  $phoneNumArr;
    	               }
    	            }
    	        }
	    }            
	    return $simRingCriteriaResponse;
	}
	
   function getScheduleName($scheduleName, $scheduleType, $fieldName)
   {
       global $schedules;
       if($scheduleName!="Every Day All Day" || $scheduleName!="None")
       { 
            $scheduleName = trim(str_replace("(Group)", "", $scheduleName));
            foreach($schedules as $scheduleKey=>$sheduleValue)
            {            
                if($sheduleValue['name']==strval($scheduleName))
                {
                    if($sheduleValue['type']==$scheduleType){                        
                        if($sheduleValue['level'] =="Service Provider")
                        {
                            $scheduleNameFinal = strval($scheduleName)." (Enterprise)";
                        }
                        else if($sheduleValue['level'] =="Group")
                        {
                            $scheduleNameFinal = strval($scheduleName)." (Group)";
                        }
                        else if($sheduleValue['level'] =="System")
                        {
                            $scheduleNameFinal = strval($scheduleName)." (System)";
                        }
                        else
                        {
                            $scheduleNameFinal = strval($scheduleName);
                        }
                    }                    
                    return $scheduleNameFinal;
               }
            }
       }
       else 
       {
           return $scheduleName;
       }
   }
	
}
