<?php

/**
 * Created by Karl.
 * Date: 8/16/2016
 */
class GetLoginHeader
{
	public $serviceProviderList;
	public $loginHeader = array ();

	private $bwAuthlevel = "";

	public function __construct($bwAuthlevel) {
		$loginHeader = array ();
		$this->bwAuthlevel = $bwAuthlevel;

        if (isset($_SESSION["groups"])) {
            // groups are set only for regular admins
            if (count($_SESSION["groups"]) > 1 || (isset($_SESSION["spList"]) && count($_SESSION["spList"]) > 1)) {
                $this->loginHeader ['changeGroup'] = "yes";
            }
            else {
                $this->loginHeader ['changeGroup'] = "no";
            }
        }
        else {
            // admin is Super User
            $this->loginHeader ['changeGroup'] = "yes";
        }
		return $this->loginHeader;
	}
		
	public function getAllServiceProvider() {
		$sps = array();
		if($this->bwAuthlevel == "system") {
			$sps = $this->getSystemServiceProviders();
		} else if($this->bwAuthlevel == "serviceProvider") {
			$sps = $this->getSPServiceProvider();
		}
		return $sps;
	}
		

	public function groupGetListInServiceProviderRequest($serP) {
		global $sessionid, $client;
		$xmlinput = xmlHeader ( $sessionid, "GroupGetListInServiceProviderRequest" );
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars ( $serP ) . "</serviceProviderId>";
		$xmlinput .= xmlFooter ();
		$response = $client->processOCIMessage ( array (
				"in0" => $xmlinput
		) );
		$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
		$allGroups = array ();
		$a = 0;
		if (isset ( $xml->command->groupTable->row )) {
			foreach ( $xml->command->groupTable->row as $key => $value ) {
				if ($a == 1) {
					$allGroups [$a] = strval ( $value->col [0] );
					$a ++;
				}
			}
		}
		return $allGroups;
	}


	private function getSystemServiceProviders() {
		global $sessionid, $client;
		$sps = array();
		echo "Using system level sp list command";
		$xmlinput = xmlHeader ( $sessionid, "ServiceProviderGetListRequest" );
		$xmlinput .= xmlFooter ();
		$response = $client->processOCIMessage ( array (
				"in0" => $xmlinput
		) );
		$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
		$a = 0;
		foreach ( $xml->command->serviceProviderTable->row as $k => $v ) {
			$sps [$a] = strval ( $v->col [0] );
			$a ++;
		}
		
		$sps = subval_sort ( $sps );

		return $sps;
	}

	private function getSPServiceProvider() {
		global $bwuserid, $sessionid, $client;
		$xmlinput = xmlHeader ( $sessionid, "ServiceProviderAdminGetRequest14" );
		$xmlinput .= "<userId>" . $bwuserid . "</userId>"; //assuming the user id in db is of sp group admin.
		$xmlinput .= xmlFooter ();
		$response = $client->processOCIMessage ( array (
				"in0" => $xmlinput
		) );
		$xml = new SimpleXMLElement ( $response->processOCIMessageReturn, LIBXML_NOWARNING );
		$sps [0] = strval ( $xml->command->serviceProviderId );
		return $sps;
	}

		//public function

}
	