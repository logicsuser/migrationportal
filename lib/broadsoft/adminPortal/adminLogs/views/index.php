<?php
/**
 * Created by Dipin Krishna.
 * Date: 7/20/18
 * Time: 8:45 PM
 */
?>
<link rel="stylesheet" href="/Express/css/averistar_bootstrap.css?v15">
<link rel="stylesheet" href="/Express/css/jquery.dataTables.min.css">
<style>
    .trnasBack{
        background: transparent !important;
    }  
    .cdrsFormDiv{width:860px; margin:0 auto;}
       div#ui-datepicker-div {
      top: 52px !important;
       left: 15px !important;
    }
   
.alignautoCompleteList{
    position: absolute;
    max-height: 250px !important;
    height: auto !important;
    overflow-y: auto !important; 
    left: 14px !important;
    width: 97% !important;
}
#AlignUserAutoCompletelist{top:10px;}
</style>
<script>

    var availableUsers = <?= json_encode($allAdminUsers) ?>;

    $(function () {
        $('.ui-dialog-buttonpane').removeClass('.ui-button .ui-widget .ui-state-default .ui-corner-all .ui-button-icon-only .ui-dialog-titlebar-close .ui-state-focus');

        $("#module").html("> View Change Log");
        $("#endUserId").html("");

        $(".showLog").click(function () {
            var changeId = $(this).attr("id");
            $("#expand_" + changeId).dialog({
                autoOpen: false,
                width: 1000,
                modal: true,
                position: {my: "top", at: "top"},
                resizable: false,
                closeOnEscape: false,
                open: function (event) {
                	setDialogDayNightMode($(this));
                    $(this).closest(".ui-dialog").find(":button").blur();
                },

            });
            $("#expand_" + changeId).dialog("open");
            if ($("#expand_" + changeId).length == 0) {
                alert("There is no log for that event.");
            }
        });
    });

    $(document).ready(function () {

       // $('input.date_field').datepicker({dateFormat: 'mm/dd/yy', altFormat: "yy-mm-dd"});
        $('input.date_field').datepicker({dateFormat: 'mm/dd/yy', altFormat: "yy-mm-dd",beforeShow: function(input, obj) {
            $(input).after($(input).datepicker('widget'));
       }});

        $( "#searchUsers" ).autocomplete({
            source: availableUsers,
            appendTo: '#AlignUserAutoCompletelist',
            select: function( event, ui ) {
                //console.log(ui.item);
                if(ui.item) {
                    $( "#searchUsers" ).val( ui.item.label);
                    $( "#userID" ).val( ui.item.userID);
                } else {
                    $( "#userID" ).val("");
                }

                return false;
            },
            change: function( event, ui ) {
                //console.log(ui);
                if(ui.item) {
                    $( "#searchUsers" ).val( ui.item.label);
                    $( "#userID" ).val( ui.item.userID);
                } else {
                    $( "#userID" ).val("");
                }
                return false;
            }
        }).autocomplete( "widget" ).addClass("alignautoCompleteList");

        filterChangeLog("#admin_log_form");
    });

    function filterChangeLog(filterForm) {
        $("#logs").html("<div id=\"loading\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
        $("#loading").show();
        var form_data = $(filterForm).serialize();
        $.ajax({
            type: "POST",
            url: "adminLogs/search.php",
            data: form_data,
            success: function (result) {
                $("#loading").hide();
                $("#logs").html(result);
            }
        });

    }


</script>
 
 <h2 class="adminUserText">Admin Logs</h2>
<div class="">
	<div class="">
		<form class="" id="admin_log_form" name="admin_log_form">
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText">Start Date</label>
						<input value="<?= date('m/d/Y', strtotime('-90 days')) ?>"
									       autocomplete="off"
									       type="text"
									       data-date-format="yyyy-mm-dd"
									       name="start_date" id="start_date"
									       class="form-control date_field">
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText">End Date</label>
						<input value="<?= date('m/d/Y') ?>"
									       autocomplete="off"
									       type="text"
									       data-date-format="yyyy-mm-dd"
									       name="end_date" id="end_date"
									       class="form-control date_field">
					</div>
				</div>
				<!-- event row start -->
				
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText">Events</label>
						<div class="dropdown-wrap">
							<select class="form-control" id="event_type" name="event_type">
										<?php
										foreach ($eventTypes as $key => $value) {
											?>
											<option <?= $key == 'ALL' ? 'SELECTED' : '' ?> value="<?= $key ?>"><?= $value ?></option>
											<?php
										}
										?>
							</select>
						</div>
						 
					</div>
				</div>
				
				<!-- Admin-Type -->
				<div class="col-md-6">
					<div class="form-group">
						<label class="labelText">Admin Types</label>
						<div class="dropdown-wrap">
							<select class="form-control" id="admin_type" name="admin_type">
										<?php
										foreach ($adminTypes as $key => $value) {
										?>
											<option <?= $key == 'ALL' ? 'SELECTED' : '' ?> value="<?= $key ?>"><?= $value ?></option>
										<?php
										}
										?>
							</select>
						</div>
						 
					</div>
				</div>
				
				<!-- users field -->
				
				<div class="col-md-6">
                                    <div id="AlignUserAutoCompletelist">
					<div class="form-group">
						<label class="labelText">User</label>
						 <input class="form-control autoFill"
									       autocomplete="off"
									       type="text"
									       name="searchUsers"
									       id="searchUsers"
									       placeholder="Search for Username or Name">
						 <input type="hidden" name="userID" id="userID"/>
					</div>
                                    </div>
				</div>
				
			</div><!-- end main row -->
	<!-- second button row start -->
			<div class="row">
				<div class="col-md-12 alignBtn">
				 	<input type="button" onclick="filterChangeLog(this.form);" value="Search" class="subButton"/>
				</div>
			</div>	
		</form>
	</div>
	
	<!-- admin search log result data row -->
	<div class="row">
		<div id="logs">
					</div>
	</div>
</div>
	 