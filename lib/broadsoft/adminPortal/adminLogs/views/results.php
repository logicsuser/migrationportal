<?php
/**
 * Created by Dipin Krishna.
 * Date: 7/20/18
 * Time: 9:27 PM
 */
?>
<div class="selectContainer">
<div class="row">
	<div class="form-group alignBtn">
		<label class="labelText"><?= "Showing results for '$eventTypeName' Events and '$adminTypeName' Admin Types from $startDateFormatted to $endDateFormatted." ?></label>
	</div>
	 
</div>
<div class="">
  <div class="" name="downloadCSV" id="downloadCSV" value="" style="margin-bottom: -62px !important;">
		<img src="images/icons/download_csv.png"  data-alt-src="images/icons/download_csv_over.png" onclick="location.href=href='/Express/adminLogs/downloadCSV.php?params=<?= base64_encode($downloadCSVParams) ?>'">
		<br>
		<span>Download<br>CSV</span>
   </div>
</div>
	 	
		
		<div class="" id="adminLogsTableAlignId">
	   <!-- Data Append Here 
	   	<a style="margin-left: 5px;" class="customTooltip" data-toggle="tooltip" data-placement="top" title="<?= "Showing results for '$eventTypeName' Events and '$adminTypeName' Admin Types from $startDateFormatted to $endDateFormatted." ?>"><img class="infoIcon" src="images/NewIcon/info_icon.png"></a>
		-->
		
		
		
<table width="100%" class="display table table-bordered table-striped scroll" id="example" cellspacing="0">
	<thead>
	<tr>
		<th class="change thsmall">Date</th>
		<th class="change thsmall">Event</th>
		<th class="change thsmall">Username</th>
		<th class="change thsmall">Name</th>
	</tr>
	</thead>
	<tbody>
	<?php
	foreach ($searchResults as $result) {
		?>
		<tr>
			<td class="change thsmall"><span style="display: none;"><?= $result['created_on'] ?></span><?= date('M j, Y h:i a', strtotime($result['created_on'])) ?></td>
			<td class="change thsmall">
				<a class="cursorPoiter btn-link showLogDetails" onclick="showLogDetails('<?= $result['id'] ?>')" data-id="<?= $result['id'] ?>"><?= $eventTypes[$result['eventType']] ?></a>
			</td>
			<td class="change thsmall"><?= $result['adminUserName'] ?></td>
			<td class="change thsmall"><?= $result['adminName'] ?></td>
		</tr>
		<?php
	}
	?>
	</tbody>
</table>
	 </div>
</div>
	
 <hr/>
 
<div id="logDetails">
</div>
<script>

$(function () { 
	var sourceSwapImage = function() {
	    var thisId = this.id;
	    var image = $("#"+thisId).children('img');
	    var newSource = image.data('alt-src');
	    image.data('alt-src', image.attr('src'));
	    image.attr('src', newSource);
	}

/*swap hover image download css */
 $("#downloadCSV").hover(sourceSwapImage, sourceSwapImage);
 
});




    $(document).ready(function () {
        $("#example").dataTable({
            "order": [],
            "paging": true

        });
    });

    function showLogDetails(logID) {

        $("#logDetails").html("<div id=\"loadingDetails\" class=\"loading\"><img src=\"/Express/images/ajax-loader.gif\"></div>");
        $("#loadingDetails").show();
        
        //$('.ui-dialog-buttonpane').removeClass('.ui-button .ui-widget .ui-state-default .ui-corner-all .ui-button-icon-only .ui-dialog-titlebar-close .ui-state-focus');
        
        $("#logDetails").dialog({
            autoOpen: false,
            width: 1000,
            modal: true,
            position: {my: "top", at: "top"},
            resizable: false,
            closeOnEscape: false,
            open: function (event) {
            	setDialogDayNightMode($(this));
                $('.ui-dialog-buttonpane').find('button:contains("Close")').addClass('cancelButton'); //code added @11 Aug 2018
                //$(this).closest(".ui-dialog").find(":button").blur(); //Code commented @ 11 AUg 2018                  
            },
            //Code added @ 11 Aug 2018
            buttons: { 
        		"Close": function() {
        			$(this).dialog("close");
        		}
                     }
           //Code end

        });
        
        $(this).closest(".ui-dialog").find(":button").blur();
        
        $("#logDetails").dialog("open");

        $.get("adminLogs/logDetails.php?logID=" + logID, function (data) {
            $("#logDetails").html(data);
        })
        .fail(function () {
            $("#logDetails").html("Sorry, not available at this moment.");
        })
        .always(function () {
            console.log("complete");
        });

        return false;
    }

</script>
<script>
 
	$(document).ready(function(){
    //	$('#example_length').attr('style', 'width: 00px !important');
    	$(".dataTables_length select:before,.dataTables_length label").addClass("dropdown-wrap");   
	});
 
	</script>
	 
