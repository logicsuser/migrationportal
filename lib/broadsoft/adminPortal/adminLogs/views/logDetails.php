<?php
/**
 * Created by Dipin Krishna.
 * Date: 7/22/18
 * Time: 9:09 PM
 */
?>
<style>
    .averistar-bs3 hr{border-top:1px solid #ccc !important ;}
</style>
<div class="container averistar-bs3 trnasBack">
	<?php
	//print_r($details);
	if (isset($details['log'])) {
		?>
		<div class="col-xs-24">
                    <table style="width:830px; margin:0 auto;" class="scroll tablesorter dataTable no-footer tableHeadHeight" cellspacing="0" role="grid" aria-describedby="example_info" id="example">
                    
			<!--<table width="100%" class="table table-bordered table-striped scroll" id="example" cellspacing="0"> -->
				<thead>
				<tr style="font-weight: bold;">
					<th class="thsmall" style="text-align: left !important;">Event</th>
					<th class="thsmall" style="text-align: left !important;">Username</th>
					<th class="thsmall" style="text-align: left !important;">Name</th>
					<th class="thsmall" style="text-align: left !important;">Date</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td><?= $eventTypes[$details['log']['eventType']] ?></td>
					<td><?= $details['log']['adminUserName'] ?></td>
					<td><?= $details['log']['adminName'] ?></td>
					<td><?= date('M j, Y h:i a', strtotime($details['log']['created_on'])) ?></td>
				</tr>
				</tbody>
			</table>
		</div>
		<?php
		//Show Event Details
		if (isset($details['details']) && $details['details']) {
			?>
			<div class="col-xs-24">
				<div class="" style=" width: 830px;margin: 0 auto;margin-top: 23px;">
                                    <label class="labelText" style="margin-bottom: -10px;">Event Details</label>
                                    <hr/>
				</div>
				 
				<table style="width:830px; margin:0 auto;" class="scroll tablesorter dataTable no-footer tableHeadHeight" cellspacing="0" role="grid" aria-describedby="example_info" id="example">
                                
                               <!-- <table width="100%" class="table table-bordered table-striped table-condensed scroll" id="example" cellspacing="0"> -->
					<thead>
					<tr style="font-weight: bold;">
						<th class="thsmall" style="text-align: left !important;">Title</th>
						<th class="thsmall" style="text-align: left !important;"colspan="2">Detail</th>
					</tr>
					</thead>
					<tbody>
					<?php
					foreach ($details['details'] as $detail) {
						if($detail['detailType'] == 'DETAIL') {
							?>
							<tr>
								<td><?= $detail['title'] ?></td>
								<td colspan="2"><?= $detail['value1'] ?></td>
							</tr>
							<?php
						} else if($detail['detailType'] == 'CHANGE') {
							?>
							<tr>
								<td><?= $detail['title'] ?></td>
								<td class="danger">Old: <?= $detail['value2'] ?></td>
								<td class="success">New: <?= $detail['value1'] ?></td>
							</tr>
							<?php
						}
					}
					?>
					</tbody>
				</table>
			</div>
			<?php
		}
		?>
		<?php
		//Show User Details if exists
		if (isset($details['user']) && $details['user']) {
			?>
			<div class="col-xs-24">
                             
                            <div class="" style=" width: 830px;margin: 0 auto;margin-top: 23px;">
                                    <label class="labelText" style="margin-bottom: -10px;">User Details</label>
                                    <hr/>
				</div>
                             <!--<table width="100%" class="table table-bordered table-striped scroll" id="example" cellspacing="0">-->
                                <table style="width:830px; margin:0 auto;" class="scroll tablesorter dataTable no-footer tableHeadHeight" cellspacing="0" role="grid" aria-describedby="example_info" id="example">
					<thead>
					<tr style="font-weight: bold;">
						
                                                <th class="thsmall" style="text-align: left !important;white-space: nowrap;">Username</th>
						<th class="thsmall" style="text-align: left !important;white-space: nowrap;">First Name</th>
						<th class="thsmall" style="text-align: left !important;white-space: nowrap;">Last Name</th>
						<th class="thsmall" style="text-align: left !important;white-space: nowrap;">Email Address</th>
						<th class="thsmall" style="text-align: left !important;white-space: nowrap;">Super User</th>
						<th class="thsmall" style="text-align: left !important;white-space: nowrap;">Disabled</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td><?= isset($details['user']['userName']) ? $details['user']['userName'] : '-' ?></td>
						<td><?= isset($details['user']['firstName']) ? $details['user']['firstName'] : ''?></td>
						<td><?= isset($details['user']['lastName']) ? $details['user']['lastName'] : ''?></td>
						<td><?= isset($details['user']['emailAddress']) ? $details['user']['emailAddress'] : ''?></td>
						<td><?= isset($details['user']['superUser']) && $details['user']['superUser'] ? 'Yes' : 'No' ?></td>
						<td><?= isset($details['user']['disabled']) && $details['user']['disabled'] ? 'Yes' : 'No' ?></td>
					</tr>
					</tbody>
				</table>
			</div>
			<?php
		}
		?>
		<?php
	}
	?>
</div>