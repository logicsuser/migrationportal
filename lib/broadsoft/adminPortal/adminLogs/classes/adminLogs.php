<?php
/**
 * Created by Dipin Krishna.
 * Date: 7/20/18
 * Time: 8:43 PM
 */

class adminLogs
{

	public $connection;
	public $event_types;
	public $admin_types;
	public $adminLogDB;
	
	public function __construct($jon_requirement_id = null) {

		$this->event_types = $this->getEventTypes();
		$this->admin_types = $this->getAdminTypes();
		$this->adminLogDB = $this->connectionToadminLogDB();
	}

	private function connectionToadminLogDB() {
            $settings = parse_ini_file("/var/www/expressAdminLogs.ini", TRUE);
            $db = new PDO("mysql:host=" . $settings["expressAdminLogsDatabase"]["host"] . ";dbname=" . $settings["expressAdminLogsDatabase"]["database"] . ";charset=utf8",
            $settings["expressAdminLogsDatabase"]["username"],
            $settings["expressAdminLogsDatabase"]["password"]
            );
            return $db;
    }
	    
	public function getEventTypes() {

		return array(
			'ALL' => "All",
			'ADD_ADMIN' => "Add Admin",
			'DELETE_ADMIN' => 'Delete Admin',
			'MODIFY_ADMIN' => 'Modify Admin',
			'SUSPENSION' => 'Suspension',
			'FAILED_ATTEMPT' => 'Failed Attempt',
			'LOGIN' => 'Login',
			'PASSWORD_RESET' => 'Password Reset',
			'FORGOT_PASSWORD' => 'Forgot Password',
			'PASSWORD_EXPIRATION' => 'Password Expiration',
		    'ADD_ANNOUNCEMENT' => 'Add Announcement',
		    'DELETE_ANNOUNCEMENT' => 'Delete Announcement',
		    'MODIFY_ANNOUNCEMENT' => 'Modify Announcement'
		);

	}

	public function getAdminTypes() {

		global $db;

		$admin_types = array(
			'ALL' => "All",
			'SUPER_USER' => "Super Users"
		);

		$stmt = $db->prepare('SELECT adminTypeIndex, name FROM adminTypesLookup');
		if ($stmt->execute()) {
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
				$admin_types[$row['adminTypeIndex']] = $row['name'];
			}
		}

		return $admin_types;

	}

	public function searchAdminLogs($filters = null, $order_by = null, $limit = 0) {

		global $db;

		/* $query = 'SELECT adminLogs.* FROM adminLogs '
			. ' LEFT JOIN users adminUser on adminUser.id = adminLogs.adminuserID '
			. ' WHERE adminUser.superUser != 2 and 1=1 '; */
                
                
		$query = 'SELECT adminLogs.* FROM expressAdminLogs.adminLogs adminLogs'
		          . ' LEFT JOIN adminPortal.users adminUser on adminUser.id = adminLogs.adminuserID '
		          . ' WHERE 1=1';

		$query_values = array();

		if ($filters) {

			$query_result = $this->build_filter_query($filters);
			if ($query_result['query']) {
				$query .= " and " . $query_result['query'];
				$query_values = array_merge($query_values, $query_result['values']);
			}

		}

		//Order By
		if ($order_by) {
			$query .= " order by $order_by";
		}

		//Limit
		if ($limit > 0) {
			$query .= " limit $limit ";
		}

		/*
				echo "<br/>$query<br/>";
				echo "<pre>";
				print_r($query_values);
				echo "</pre>";
		*/

		$rows = array();
		$stmt = $db->prepare($query);
		if ($stmt->execute($query_values)) {
			$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		return $rows;

	}

	public function getLogDetails($logID) {

		global $db;

		$result = null;

		if ($logID > 0) {

			$query = 'SELECT adminLogs.* FROM adminLogs WHERE adminLogs.id = ? limit 1';

			$stmt = $this->adminLogDB->prepare($query);
			if ($stmt->execute(array($logID))) {

				if ($log = $stmt->fetch(PDO::FETCH_ASSOC)) {

					//Current User Details
					$user = null;
					$query = 'SELECT * FROM users WHERE id = ? limit 1';
					$stmt = $db->prepare($query);
					if ($stmt->execute(array($log['adminUserID']))) {
						$user = $stmt->fetch(PDO::FETCH_ASSOC);
					}

					//Event Details
					$details = array();
					$query = 'SELECT adminLogDetails.* FROM adminLogDetails '
						. ' WHERE adminLogID = ?';
					$stmt = $this->adminLogDB->prepare($query);
					if ($stmt->execute(array($logID))) {
						$details = $stmt->fetchAll(PDO::FETCH_ASSOC);
					}

					$result = array('log' => $log, 'user' => $user, 'details' => $details);
				}
			}
		}

		return $result;

	}

	public function addLogEntry($log) {

		global $db;

		$stmt = $this->adminLogDB->prepare('insert INTO adminLogs (adminUserID, eventType, adminUserName, adminName, updatedBy) VALUES (?, ?, ?, ?, ?)');
		if ($stmt->execute(
			array(
				$log['adminUserID'],
				$log['eventType'],
				$log['adminUserName'],
				$log['adminName'],
				isset($log['updatedBy']) ? $log['updatedBy'] : 0
			)
		)) {

		    if ($logID = $this->adminLogDB->lastInsertId()) {

				if (isset($log['details'])) {

					foreach ($log['details'] as $title => $value) {
					    $stmt = $this->adminLogDB->prepare('INSERT INTO adminLogDetails (adminLogID, title, detailType, value1, value2) VALUES (?, ?, ?, ?, ?)');
						$stmt->execute(
							array(
								$logID,
								$title,
								is_array($value) ? 'CHANGE' : 'DETAIL',
								is_array($value) ? $value[0] : $value,
								is_array($value) ? $value[1] : '',
							)
						);
					}

				}

				return $logID;
			}
		}

		return false;

	}

	public function build_filter_query($filters = null, $operator = 'and') {

		$query_items = array();
		$query_values = array();

		if ($filters) {

			foreach (array_keys($filters) as $filter_operator) {
				/*
				echo "<br/>$filter_operator<br/>";
				echo "<pre>";
				print_r($filters[$filter_operator]);
				echo "</pre>";
				*/

				if (!is_int($filter_operator)) {

					$query_result = $this->build_filter_query($filters[$filter_operator], $filter_operator);
					$query_items[] = $query_result['query'];
					$query_values = array_merge($query_values, $query_result['values']);

				} else {

					$filter = $filters[$filter_operator];

					if (count(array_filter($filter, 'is_array')) > 0) {

						$query_result = $this->build_filter_query($filters[$filter_operator], $operator);
						$query_items[] = $query_result['query'];
						$query_values = array_merge($query_values, $query_result['values']);

					} else {

						$query_items[] = "$filter[0] $filter[1] ? ";
						$query_values[] = $filter[2];

					}
				}
			}

		}

		return array('query' => '(' . implode(" $operator ", $query_items) . ')', 'values' => $query_values);
	}


}