<?php

/**
 * Created by Sollogics - Jeetesh.
 * Date: 1/23/2018
 */
class GetUserErrorExcelSheetReport
{
	public $message;
	public $mod;
	public $users = array();
	const SKY_BLUE = "dbe5f1";
	const DARK_BLUE = "254061";
	const BLUE = "0070c0";
	const Light_Grey = "D6D6D6";

	public function __construct() {
	}

	public function getColumnNames() {
		$coulmnArray = array(
			"Device Name",
			"DeviceType",
			"Registration Status",
			"IP",
			"Expiry",
			"Agent Type"
		);
		return $this->message = $coulmnArray;
	}

	public function excelCreator($objPHPExcel) {
		$creator = $objPHPExcel->getProperties()
			->setCreator("Express")
			->setLastModifiedBy("Express")
			->setTitle("An Express Document")
			->setSubject("An Express Document")
			->setDescription("An Express generated document.")
			->setKeywords("Averistar Express")
			->setCategory("Report");
		return $creator;
	}

	public function generateUserValueXLS($objPHPExcel, $columnNames, $userValArray, $userErrorArray, $headingRowForColumn) {
		$objPHPExcel->setActiveSheetIndex(0);
		$message = "";

		//print column heading for the registration
		//PhpOffice\PhpSpreadsheet\Shared\Font::setTrueTypeFontPath('');
		//PhpOffice\PhpSpreadsheet\Shared\Font::setAutoSizeMethod(PhpOffice\PhpSpreadsheet\Shared\Font::AUTOSIZE_METHOD_EXACT);
		$col = 0;
		$columnArray = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ");
		foreach ($columnNames as $key => $val) {
			//set value in column heading
			$objPHPExcel->getActiveSheet()->setCellValue($columnArray[$col] . $headingRowForColumn, $val);
			$objPHPExcel->getActiveSheet()->calculateColumnWidths();
			$objPHPExcel->getActiveSheet()->getStyle($columnArray[$col] . $headingRowForColumn)->getFont()->setBold(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnArray[$col])->setAutoSize(true);
			$col++;
		}

		//dynamic registration row for the user
		$userRowStartted = $headingRowForColumn + 1;
		$userErrorRowStartted = $userRowStartted + 1;

		$col1 = 0;
		$columnArrayCount = count($columnArray);
		$userArrayCount = count($userValArray);

		$errorString = "";
		$r1 = 0;
		$row1 = 2;
		foreach ($userValArray as $key1 => $val1) {
			$d1 = 0;
			foreach ($userValArray[$key1] as $key2 => $val2) {
				$trimVal = trim($userErrorArray[$r1][$d1]);
				if (!empty($trimVal)) {
					$errorString = "\nError: " . $userErrorArray[$r1][$d1];

					$objPHPExcel->getActiveSheet()->getStyle($columnArray[$d1] . $row1)->getFill()
						->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
						->getStartColor()->setARGB('FFFF0000');
					$objPHPExcel->getActiveSheet()->getStyle($columnArray[$d1] . $row1)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle($columnArray[$d1] . $row1)->getFont()->getColor()->setARGB(PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);


				} else {
					$errorString = "";
				}

				$objPHPExcel->getActiveSheet()->setCellValue($columnArray[$d1] . $row1, $val2 . $errorString);
				$d1++;

			}
			$row1++;
			$r1++;
		}

	}

	public function generateErrorReportXLS($postArray, $filename) {

		// making excel object
		$objPHPExcel = new PhpOffice\PhpSpreadsheet\Spreadsheet();
		// making excel write object
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header("Content-Disposition: attachment; filename=\"$filename\"");
		header("Cache-Control: max-age=0");
		$objWriter = PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, 'Xlsx');

		// create excel creator information
		$this->excelCreator($objPHPExcel);

		// code for error sheet excel
		// get column name for error sheet
		$headingRowForColumn = 1;

		$columnNames = $postArray["columnArray"];
		// get user data to print in excel

		$userArray = $postArray["valueArray"];
		$userErrorArray = $postArray["errorArray"];
		// get output in excel

		$registrationExcel = $this->generateUserValueXLS($objPHPExcel, $columnNames, $userArray, $userErrorArray, $headingRowForColumn);
		// code registration excel ends

		// footer function for excel to save the data
		$this->generateExcelFooter($objPHPExcel, $objWriter, $filename);
	}

	// code to save excel
	public function generateExcelFooter($objPHPExcel, $objWriter, $filename) {

		// Set page orientation and size
		$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
		$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);

		// Rename first worksheet
		// echo date('H:i:s') , " Rename first worksheet" , EOL;
		$objPHPExcel->getActiveSheet()->setTitle('Summary Report');

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Save Excel 2007 file
		$callStartTime = microtime(true);
		ob_clean();
		$objWriter->save("php://output");
		//$objWriter->save ( $filename );
		$callEndTime = microtime(true);
		$callTime = $callEndTime - $callStartTime;
	}
}
