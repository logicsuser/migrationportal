<?php
	if ($ociVersion == "17")
	{
		$xmlinput = xmlHeader($sessionid, "GroupDepartmentGetListRequest");
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "GroupDepartmentGetListRequest18");
	}
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<includeEnterpriseDepartments>true</includeEnterpriseDepartments>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	if ($ociVersion == "17")
	{
		foreach ($xml->command->departmentKey as $key => $value)
		{
			$departments[$a] = strval($value->name);
			$a++;
		}
	}
	else
	{
		foreach ($xml->command->departmentTable->row as $key => $value)
		{
			$departments[$a] = strval($value->col[1]);
			$a++;
		}
	}
?>
