<?php
/**
 * Created by Sollogics.
 * Date: 5/22/2017
 */
class GroupExtensionLength
{

	function getGroupExtensionLength() {

		global $sessionid, $client;
		$extensionLengthResponse = array();
		$extensionLengthResponse["Error"] = "";
		$extensionLengthResponse["Success"] = "";

		$xmlinput = xmlHeader($sessionid, "GroupExtensionLengthGetRequest17");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		if (readErrorXmlGenuine($xml) != "") {
			$extensionLengthResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$extensionLengthResponse["Success"] = $xml->command;

		}
		return $extensionLengthResponse;
	}
}
?>
