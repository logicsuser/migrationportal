<?php

/**
 * Created by Sologics.
 * Date: 12/05/2017
 */
class userPassword{

	
	//function to get the user of the user
    public function modifyPasswordInfo($userId, $newPassword){
       
		global  $sessionid, $client;
		$passwordModifyResponse["Error"] = "";
		$passwordModifyResponse["Success"] = "";
		
		//call Processing
		$xmlinputCall  = xmlHeader($sessionid, "PasswordModifyRequest");
		$xmlinputCall .= "<userId>" . $userId. "</userId>";
		$xmlinputCall .= "<newPassword>" . $newPassword. "</newPassword>";
		
		$xmlinputCall .= xmlFooter();
		
		$responseCall = $client->processOCIMessage(array("in0" => $xmlinputCall));
		$xmlCall = new SimpleXMLElement($responseCall->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xmlCall); die;
		
		if (readErrorXmlGenuine($xmlCall) != "") {
			if(empty($xmlCall->command->detail)){
				$passwordModifyResponse["Error"] = strval($xmlCall->command->summaryEnglish);
			}else{
				$detailStr = strval($xmlCall->command->detail);
				$detail = str_replace('<xml-fragment xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>', '<br/>', $detailStr);
				$passwordModifyResponse["Error"] = $detail;
			}
		}else{
			$passwordModifyResponse["Success"] = $xmlCall->command;
			
		}
		return $passwordModifyResponse;
	}
	
	public function modifyPortalPasscodeInfo($userId, $newPasscode){
		
		global  $sessionid, $client;
		$passcodeModifyResponse["Error"] = "";
		$passcodeModifyResponse["Success"] = "";
		
		//call Processing
		$xmlinputCall  = xmlHeader($sessionid, "UserPortalPasscodeModifyRequest");
		$xmlinputCall .= "<userId>" . $userId. "</userId>";
		$xmlinputCall .= "<newPasscode>" . $newPasscode. "</newPasscode>";
		
		$xmlinputCall .= xmlFooter();
		
		$responseCall = $client->processOCIMessage(array("in0" => $xmlinputCall));
		$xmlCall = new SimpleXMLElement($responseCall->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xmlCall) != "") {
			if(empty($xmlCall->command->detail)){
				$passcodeModifyResponse["Error"] = strval($xmlCall->command->summaryEnglish);
			}else{
				$detailStr = strval($xmlCall->command->detail);
				$detail = str_replace('<xml-fragment xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>', '<br/>', $detailStr);
				$passcodeModifyResponse["Error"] = $detail;
			}
		}else{
			$passcodeModifyResponse["Success"] = $xmlCall->command;
			
		}
		return $passcodeModifyResponse;
	}	
	
	public function updateResetPasscodeDefault($bwVoicePortalPasswordType, $userVoicePortalPasscodeFormula){
		
		
		if ($bwVoicePortalPasswordType == "Formula") {
			require_once("/var/www/lib/broadsoft/adminPortal/ResourceNameBuilder.php");
			require_once("/var/www/lib/broadsoft/adminPortal/VoicePasswordNameBuilder.php");
			
			$voicePasswordBuilder = new VoicePasswordNameBuilder($userVoicePortalPasscodeFormula,
					getResourceForVoicePasswordCreation(ResourceNameBuilder::DN, $value),
					getResourceForVoicePasswordCreation(ResourceNameBuilder::EXT, $value),
					getResourceForVoicePasswordCreation(ResourceNameBuilder::USR_CLID, $value),
					getResourceForVoicePasswordCreation(ResourceNameBuilder::GRP_DN, $value),
					true);
			
			if (! $voicePasswordBuilder->validate()) {
				// TODO: Implement validation resolution
			}
			
			do {
				$attr = $voicePasswordBuilder->getRequiredAttributeKey();
				$voicePasswordBuilder->setRequiredAttribute($attr, getResourceForVoicePasswordCreation($attr, $value));
			} while ($attr != "");
			
			$portalPass = $voicePasswordBuilder->getName();
		} else {
			$portalPass = setVoicePassword(); 
		}
		return $portalPass;
	
	}
	
	public function updateResetPasswordDefault($userId, $newPassword){
		
		global  $sessionid, $client;
		$passwordModifyResponse["Error"] = "";
		$passwordModifyResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "UserModifyRequest17sp4");
		$xmlinput .= "<userId>" . $userId. "</userId>";
		$xmlinput .= "<newPassword>" . $newPassword. "</newPassword>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xmlCall = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xmlCall) != "") {
			if(empty($xmlCall->command->detail)){
				$passwordModifyResponse["Error"] = strval($xmlCall->command->summaryEnglish);
			}else{
				$detailStr = strval($xmlCall->command->detail);
				$detail = str_replace('<xml-fragment xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>', '<br/>', $detailStr);
				$passwordModifyResponse["Error"] = $detail;
			}
		}else{
			$passwordModifyResponse["Success"] = $xmlCall->command;
			
		}
		return $passwordModifyResponse;
	}
	
	
}
?>