<?php

function readErrorXml($xml, $name) {
	if (readError ( $xml ) != "") {
		$errMsg = "Failed to add group call pickup name " . $name . " to server.";
		$errMsg .= "%0D%0A%0D%0A" . strval ( $xml->command->summaryEnglish );
		if ($xml->command->summary) {
			$errMsg .= "%0D%0A" . strval ( $xml->command->summary );
		}
	} else {
		$errMsg = "Success";
	}
	return $errMsg;
}

function readErrorXmlList($xml) {
	if (readError ( $xml ) != "") {
		$errMsg = "Failed to getList to server.";
		$errMsg .= "%0D%0A%0D%0A" . strval ( $xml->command->summaryEnglish );
		if ($xml->command->summary) {
			$errMsg .= "%0D%0A" . strval ( $xml->command->summary );
		}
	} else {
		$errMsg = "Success";
	}
	return $errMsg;
}

function readErrorFromCommand($command) {
	$errMsg = "";
	
	if (count ( $command->attributes () ) > 0) {
		foreach ( $command->attributes () as $a => $b ) {
			if ($b == "Error" || $b == "Warning") {
				if ($command->summaryEnglish) {
					$errMsg .= "%0D%0A" . strval ( $command->summary );
				}
			}
		}
	} else {
		$errMsg = "Success";
	}
	return $errMsg;
}

function readErrorXmlGenuine($xml) {
	global $supportEmailSupport;
	$errMsg = "";
	
	if (count ( $xml->command->attributes () ) > 0) {
		foreach ( $xml->command->attributes () as $a => $b ) {
			if ($b == "Error" || $b == "Warning") {
				if ($xml->command->summaryEnglish) {
					$errMsg .= "%0D%0A" . strval ( $xml->command->summary );
				}
			}
		}
	} else {
		$errMsg .= "Success";
	}
	return $errMsg;
}

function rebuildResetDevice($postArray, $sp, $groupId) {
	$changeString = "";
	
	require_once ("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig/rebuildAndResetDeviceConfig.php");
	$objRebuildAndReset = new RebuildAndResetDeviceConfig ();
	
	if ($postArray ['rebuildPhoneFiles'] == "true" && $postArray ['resetPhone'] == "true") {
		$respRebuild = $objRebuildAndReset->GroupCPEConfigRebuildDeviceConfig ( $sp, $groupId, $postArray ['deviceName'] );
		if (empty ( $respRebuild ['Error'] )) {
			$rebuildAndReset = $objRebuildAndReset->GroupCPEConfigResetDevice ( $sp, $groupId, $postArray ['deviceName'] );
		}
		$changeString .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">Rebuild and Reset</td><td class="errorTableRows">Successfully Updated </td></tr>';
	} else if ($postArray ['rebuildPhoneFiles'] == "true" && $postArray ['resetPhone'] == "false") {
		$respRebuild = $objRebuildAndReset->GroupCPEConfigRebuildDeviceConfig ( $sp, $groupId, $postArray ['deviceName'] );
		$changeString .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">Rebuild</td><td class="errorTableRows">Successfully Updated </td></tr>';
	} else if ($postArray ['rebuildPhoneFiles'] == "false" && $postArray ['resetPhone'] == "true") {
		$rebuildAndReset = $objRebuildAndReset->GroupCPEConfigResetDevice ( $sp, $groupId, $postArray ['deviceName'] );
		$changeString .= '<tr><td class="errorTableRows" style="background-color: #72ac5d">Reset</td><td class="errorTableRows">Successfully Updated </td></tr>';
	}
	return $changeString;
}

function rebuildResetDeviceUserModify($postArray, $sp, $groupId, $deviceName) {
	require_once ("/var/www/lib/broadsoft/adminPortal/rebuildAndResetDeviceConfig/rebuildAndResetDeviceConfig.php");
	$objRebuildAndReset = new RebuildAndResetDeviceConfig ();
	
	if ($postArray ['rebuildPhoneFiles'] == "true" && $postArray ['resetPhone'] == "false") {
		
	    $respRebuild = $objRebuildAndReset->GroupCPEConfigRebuildDeviceConfig ( $sp, $groupId, $deviceName );
	} else if ($postArray ['rebuildPhoneFiles'] == "false" && $postArray ['resetPhone'] == "true") {
		
	    $rebuildAndReset = $objRebuildAndReset->GroupCPEConfigResetDevice ( $sp, $groupId, $deviceName );
	}
}

if(!function_exists("http_post_fields")) {

	function http_post_fields($url, $data, $headers=null) {
		$data = http_build_query($data);
		$opts = array('http' => array('method' => 'POST', 'content' => $data));

		if($headers) {
			$opts['http']['header'] = $headers;
		}
		$st = stream_context_create($opts);
		$fp = fopen($url, 'rb', false, $st);

		if(!$fp) {
			return false;
		}
		return stream_get_contents($fp);
	}

}

?>
