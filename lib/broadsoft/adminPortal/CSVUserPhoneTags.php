<?php

/**
 * Created by Karl.
 * Date: 9/28/2016
 */
class CSVUserPhoneTags extends CSVManager
{
    public function __construct($postName)
    {
        parent::__construct($postName);

        $this->addAttribute("groupId",    "Group ID",   self::Validate_None());
        $this->addAttribute("userId",     "User ID",    self::Validate_UserID_Format());
        $this->addAttribute("tagName",    "Tag Name",   self::Validate_BW_Tag_Name());
        $this->addAttribute("tagValue",   "Tag Value",  self::Validate_None());
    }
}
