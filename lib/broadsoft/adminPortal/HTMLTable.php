<?php

/**
 * Created by Karl.
 * Date: 10/2/2016
 */
abstract class HTMLTable
{
    abstract protected function executeAction();

    private $keys = array();
    private $headTitles = array();
    private $numColumns = 0;

    protected $data;
    protected $numRecords = 0;
    protected $msg = "trace:";

    public function __construct()
    {
    }

    /** Creates HTML table header
     * @return string
     */
    public function createTableHeader() {
        $header = "";
        $margin = $this->numColumns == 0 ? 100 : (int)(100 / $this->numColumns);

        for ($i = 0; $i < $this->numColumns; $i++) {
            $header .= "<th style=\"width:" . $margin . "%; text-align:center\">" . $this->headTitles[$i] . "</th>";
        }

        return $header;
    }


    /** get formatted HTML table rows with data
     * @return string
     */
    public function getData() {
        $this->executeAction();

        if ($this->numRecords == 0) {
            return "";
        }

        $result = "";
        foreach ($this->data as $item => $row) {
            $result .= "<tr>";
            for ($i = 0; $i < $this->numColumns; $i++) {
                $result .= "<td>" . $row[$this->keys[$i]] . "</td>";
            }

            $result .= "</tr>";
        }

        return $result;
    }


    /**
     * @return string
     */
    public function trace() {
        return $this->msg;
    }


    //-----------------------------------------------------------------------------------------------------------------
    protected function addAttribute($key, $headTitle) {
        $this->keys[] = $key;
        $this->headTitles[] = $headTitle;

        $this->numColumns++;
    }
}
