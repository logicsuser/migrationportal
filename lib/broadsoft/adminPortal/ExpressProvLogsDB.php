<?php

Class ExpressProvLogsDB {
		
		public $expressProvLogsDb;
		
		public function __construct() {
				$this->expressProvLogsDb = $this->expressProvLogsDBConnection();
		}
		
		public function expressProvLogsDBConnection() {
			$settings = parse_ini_file("/var/www/expressProvLogs.ini", TRUE);
            $db = new PDO("mysql:host=" . $settings["expressProvLogsDatabase"]["host"] . ";dbname=" . $settings["expressProvLogsDatabase"]["database"] . ";charset=utf8",
                $settings["expressProvLogsDatabase"]["username"],
                $settings["expressProvLogsDatabase"]["password"]
                );
				return $db;
		}
		
		
}

?>