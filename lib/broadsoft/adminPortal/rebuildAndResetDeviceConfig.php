<?php
/**
 * Created by Karl.
 * Date: 11/22/2016
 */

    const rebuildOnly  = "rebuildOnly";
    const resetOnly    = "resetOnly";
    const forceRebuild = "forceRebuild";
    const rebuildAndReset = "rebuildAndReset";

    function rebuildAndResetDeviceConfig($deviceName, $rebuildOption, $forceOption = "") {
        global $sessionid, $client;

        // Rebuild Device Config
        if ($rebuildOption != resetOnly) {
            $xmlinput  = xmlHeader($sessionid, "GroupCPEConfigRebuildDeviceConfigFileRequest");
            $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
            $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
            $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
            if ($forceOption == forceRebuild) {
                $xmlinput .= "<force>true</force>";
            }
            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        }

        // Reset Device Config
        if ($rebuildOption != rebuildOnly) {
            $xmlinput  = xmlHeader($sessionid, "GroupCPEConfigResetDeviceRequest");
            $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
            $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
            $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        }
    }

?>

