<?php
    $allGroups = array();
	$xmlinput = xmlHeader($sessionid, "GroupGetListInServiceProviderRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($serP) . "</serviceProviderId>";
	$xmlinput .= xmlFooter();
	if(isset($client)) {
	    try {
        	$response = $client->processOCIMessage(array("in0" => $xmlinput));
        	if(isset($response->processOCIMessageReturn) && $response->processOCIMessageReturn != "") {
        	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);	    
        	    $a = 0;
        	    if (isset($xml->command->groupTable->row))
        	    {
        	        foreach ($xml->command->groupTable->row as $key => $value)
        	        {
        	            $allGroups[$a] = strval($value->col[0]);
        	            $a++;
        	        }
        	    }
        	}
    	
	    } catch (SoapFault $E) {
	        // echo $E->faultstring;
	    }
	}
?>
