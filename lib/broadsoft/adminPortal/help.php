<?php

/**
 * Created by Sologics.
 * Date: 07/07/2017
 */

class Help
{

	//add DN's request in service provider
	public function getHelpFile($adminType, $modId, $tabId){
		
		if($adminType != "" && $modId != "" && $tabId != ""){
			$returnUrl = $adminType."_".$modId."_".$tabId;
		}else if($adminType != "" && $modId != ""){
			$returnUrl = $adminType."_".$modId;
		}
		
		return $returnUrl;
	}

}