<?php
/**
 * Created by Karl.
 * Date: 9/13/2016
 */

    $xmlinput = xmlHeader($sessionid, "GroupDomainGetAssignedListRequest");
    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
    $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
    $xmlinput .= xmlFooter();

    $response = $client->processOCIMessage(array("in0" => $xmlinput));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

    $groupDefaultDomain = $xml->command->groupDefaultDomain;

    $a = 0;
    foreach ($xml->command->domain as $domain) {
        $groupDomains[$a++] = strval($domain);
    }
