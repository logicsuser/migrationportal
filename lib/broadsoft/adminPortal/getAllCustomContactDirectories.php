<?php
	$xmlinput = xmlHeader($sessionid, "GroupCustomContactDirectoryGetListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->name as $key => $value)
	{
		$directories[$a] = strval($value);
		$a++;
	}

	if (isset($directories))
	{
		$directories = subval_sort($directories);
	}
?>
