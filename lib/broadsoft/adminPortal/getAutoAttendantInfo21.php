<?php
	unset($_SESSION["aa"]);

	if ($ociVersion == "19")
	{
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantGetInstanceRequest19");
	}
	else
	{
		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantGetInstanceRequest20");			
	}
	$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	//echo "<pre>"; print_r($xml);
	
	if (readErrorXmlGenuine($xml) != "") {
		if(!empty($xml->command->detail)){
			$detail = strval($xml->command->detail);
			$error = explode("<command", $detail);
			$error_detail = $error[0];
		}else{
			$error_detail = $xml->command->summaryEnglish;
		}
		//echo "<span style='text-align:center; display:block; color:red'>Error : ".$error_detail.". Please contact with customer support.</span>"; die;
		echo "<span style='text-align:center; display:block; color:red' class='broadSoftRelError'>Express cannot currently process AA operations. Please contact Customer Service. <br/>Error : ".$error_detail."</span>"; die;
	}

	$_SESSION["aa"]["aaName"] = strval($xml->command->serviceInstanceProfile->name);
	$_SESSION["aa"]["callingLineIdLastName"] = strval($xml->command->serviceInstanceProfile->callingLineIdLastName);
	$_SESSION["aa"]["callingLineIdFirstName"] = strval($xml->command->serviceInstanceProfile->callingLineIdFirstName);
	$_SESSION["aa"]["phoneNumber"] = strval($xml->command->serviceInstanceProfile->phoneNumber);
	$_SESSION["aa"]["extension"] = strval($xml->command->serviceInstanceProfile->extension);
	
	if($ociVersion == "21" || $ociVersion == "22"){
		$_SESSION["aa"]["firstDigitTimeoutSeconds"] = strval($xml->command->firstDigitTimeoutSeconds);
	}
	
	$_SESSION["aa"]["timeZone"] = strval($xml->command->serviceInstanceProfile->timeZone);
	if ($ociVersion == "17")
	{
		$_SESSION["aa"]["type"] = "Basic";
	}
	else
	{
		$_SESSION["aa"]["type"] = strval($xml->command->type);
	}
	$_SESSION["aa"]["businessHoursSchedule"] = strval($xml->command->businessHours->name);
	$_SESSION["aa"]["holidaySchedule"] = strval($xml->command->holidaySchedule->name);
	$_SESSION["aa"]["nameDialingEntries"] = strval($xml->command->nameDialingEntries);
	$_SESSION["aa"]["extensionDialingScope"] = strval($xml->command->extensionDialingScope);
	$_SESSION["aa"]["nameDialingScope"] = strval($xml->command->nameDialingScope);
	$_SESSION["aa"]["eVSupport"] = strval($xml->command->enableVideo);
	$_SESSION["aa"]["department"] = strval($xml->command->serviceInstanceProfile->department->name);

	$_SESSION["aa"]["bh"]["announcementSelection"] = strval($xml->command->businessHoursMenu->announcementSelection);
	if(isset($xml->command->businessHoursMenu->audioFile->name)){
	    $_SESSION["aa"]["bh"]["announcementId"] = strval($xml->command->businessHoursMenu->audioFile->name).".".strval($xml->command->businessHoursMenu->audioFile->mediaFileType);
	}else{
	    $_SESSION["aa"]["bh"]["announcementId"] = "";
	}
	
	if(isset($xml->command->businessHoursMenu->videoFile->name)){
	    $_SESSION["aa"]["bh"]["vidAnnouncementId"] = strval($xml->command->businessHoursMenu->videoFile->name).".".strval($xml->command->businessHoursMenu->videoFile->mediaFileType);
	}else{
	    $_SESSION["aa"]["bh"]["vidAnnouncementId"] = "";
	}
	
	$_SESSION["aa"]["bh"]["enableLevelExtensionDialing"] = strval($xml->command->businessHoursMenu->enableFirstMenuLevelExtensionDialing);
	foreach ($xml->command->businessHoursMenu->keyConfiguration as $key => $value)
	{
		$_SESSION["aa"]["bh"]["keys"][strval($value->key)]["desc"] = strval($value->entry->description);
		$_SESSION["aa"]["bh"]["keys"][strval($value->key)]["action"] = strval($value->entry->action);
		$_SESSION["aa"]["bh"]["keys"][strval($value->key)]["phoneNumber"] = strval($value->entry->phoneNumber);
		if ($ociVersion == "17")
		{
			$_SESSION["aa"]["bh"]["keys"][strval($value->key)]["submenuId"] = "";
		}
		else
		{
			$_SESSION["aa"]["bh"]["keys"][strval($value->key)]["submenuId"] = strval($value->entry->submenuId);
		}
		
		$playAnnouncement = strval($value->entry->action);
		
		//if($playAnnouncement == "Play Announcement"){
		    if(isset($value->entry->audioFile->name)){
			    $_SESSION["aa"]["bh"]["keys"][strval($value->key)]["announcements"] = $value->entry->audioFile->name.".".$value->entry->audioFile->mediaFileType;
			}else{
				$_SESSION["aa"]["bh"]["keys"][strval($value->key)]["announcements"] = "";
			}
			
			if(isset($value->entry->videoFile->name)){
			    $_SESSION["aa"]["bh"]["keys"][strval($value->key)]["vidAnnouncements"] = $value->entry->videoFile->name.".".$value->entry->videoFile->mediaFileType;
			}else{
			    $_SESSION["aa"]["bh"]["keys"][strval($value->key)]["vidAnnouncements"] = "";
			}
		//}
		
	}

	$_SESSION["aa"]["ah"]["announcementSelection"] = strval($xml->command->afterHoursMenu->announcementSelection);
	if(isset($xml->command->afterHoursMenu->audioFile->name)){
	    $_SESSION["aa"]["ah"]["announcementId"] = strval($xml->command->afterHoursMenu->audioFile->name).".".strval($xml->command->afterHoursMenu->audioFile->mediaFileType);
	}else{
	    $_SESSION["aa"]["ah"]["announcementId"] = "";
	}
	
	if(isset($xml->command->afterHoursMenu->videoFile->name)){
	    $_SESSION["aa"]["ah"]["vidAnnouncementId"] = strval($xml->command->afterHoursMenu->videoFile->name).".".strval($xml->command->afterHoursMenu->videoFile->mediaFileType);
	}else{
	    $_SESSION["aa"]["ah"]["vidAnnouncementId"] = "";
	}
	
	//code to get the value from database for announcement of auto attendant
	//$announcementAutoattendantId = $announcement->getAutoattendantAnnouncementInfo($db, $aaId, 'ahAnnouncementId');
	//$_SESSION["aa"]["ah"]["announcementId"] = $announcementAutoattendantId;
	
	//	$_SESSION["aa"]["ah"]["audioFileDescription"] = strval($xml->command->afterHoursMenu->audioFileDescription);
	$_SESSION["aa"]["ah"]["enableLevelExtensionDialing"] = strval($xml->command->afterHoursMenu->enableFirstMenuLevelExtensionDialing);
	foreach ($xml->command->afterHoursMenu->keyConfiguration as $key => $value)
	{
		
		$_SESSION["aa"]["ah"]["keys"][strval($value->key)]["desc"] = strval($value->entry->description);
		$_SESSION["aa"]["ah"]["keys"][strval($value->key)]["action"] = strval($value->entry->action);
		$_SESSION["aa"]["ah"]["keys"][strval($value->key)]["phoneNumber"] = strval($value->entry->phoneNumber);
		if ($ociVersion == "17")
		{
			$_SESSION["aa"]["ah"]["keys"][strval($value->key)]["submenuId"] = "";
		}
		else
		{
			$_SESSION["aa"]["ah"]["keys"][strval($value->key)]["submenuId"] = strval($value->entry->submenuId);
		}
		
		$playAnnouncement = strval($value->entry->action);
		
		//if($playAnnouncement == "Play Announcement"){
		    if(isset($value->entry->audioFile->name)){
		        $_SESSION["aa"]["ah"]["keys"][strval($value->key)]["announcements"] = $value->entry->audioFile->name.".".$value->entry->audioFile->mediaFileType;
		    }else{
		        $_SESSION["aa"]["ah"]["keys"][strval($value->key)]["announcements"] = "";
		    }
		    
		    if(isset($value->entry->videoFile->name)){
		        $_SESSION["aa"]["ah"]["keys"][strval($value->key)]["vidAnnouncements"] = $value->entry->videoFile->name.".".$value->entry->videoFile->mediaFileType;
		    }else{
		        $_SESSION["aa"]["ah"]["keys"][strval($value->key)]["vidAnnouncements"] = "";
		    }
		//}
		
	}

	if ($_SESSION["aa"]["type"] == "Standard") //BroadSoft version 19 or later
	{
		$_SESSION["aa"]["h"]["announcementSelection"] = strval($xml->command->holidayMenu->announcementSelection);
		if(isset($xml->command->holidayMenu->audioFile->name)){
		    $_SESSION["aa"]["h"]["announcementId"] = strval($xml->command->holidayMenu->audioFile->name).".".strval($xml->command->holidayMenu->audioFile->mediaFileType);
		}else{
		    $_SESSION["aa"]["h"]["announcementId"] = "";
		}
		
		if(isset($xml->command->holidayMenu->videoFile->name)){
		    $_SESSION["aa"]["h"]["vidAnnouncementId"] = strval($xml->command->holidayMenu->videoFile->name).".".strval($xml->command->holidayMenu->videoFile->mediaFileType);
		}else{
		    $_SESSION["aa"]["h"]["vidAnnouncementId"] = "";
		}
		
		//code to get the value from database for announcement of auto attendant
		//$announcementAutoattendantId = $announcement->getAutoattendantAnnouncementInfo($db, $aaId, 'hAnnouncementId');
		//$_SESSION["aa"]["h"]["announcementId"] = $announcementAutoattendantId;
		
		
//		$_SESSION["aa"]["h"]["audioFileDescription"] = strval($xml->command->holidayMenu->audioFileDescription);
		$_SESSION["aa"]["h"]["enableLevelExtensionDialing"] = strval($xml->command->holidayMenu->enableFirstMenuLevelExtensionDialing);
		foreach ($xml->command->holidayMenu->keyConfiguration as $key => $value)
		{
			$_SESSION["aa"]["h"]["keys"][strval($value->key)]["desc"] = strval($value->entry->description);
			$_SESSION["aa"]["h"]["keys"][strval($value->key)]["action"] = strval($value->entry->action);
			$_SESSION["aa"]["h"]["keys"][strval($value->key)]["phoneNumber"] = strval($value->entry->phoneNumber);
			$_SESSION["aa"]["h"]["keys"][strval($value->key)]["submenuId"] = strval($value->entry->submenuId);
			
			$playAnnouncement = strval($value->entry->action);
			
			//if($playAnnouncement == "Play Announcement"){
			    if(isset($value->entry->audioFile->name)){
			        $_SESSION["aa"]["h"]["keys"][strval($value->key)]["announcements"] = $value->entry->audioFile->name.".".$value->entry->audioFile->mediaFileType;
			    }else{
			        $_SESSION["aa"]["h"]["keys"][strval($value->key)]["announcements"] = "";
			    }
			    
			    if(isset($value->entry->videoFile->name)){
			        $_SESSION["aa"]["h"]["keys"][strval($value->key)]["vidAnnouncements"] = $value->entry->videoFile->name.".".$value->entry->videoFile->mediaFileType;
			    }else{
			        $_SESSION["aa"]["h"]["keys"][strval($value->key)]["vidAnnouncements"] = "";
			    }
			//}
			
		}

		$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuGetListRequest");
		$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		$a = 0;
		$submenuArr = array();
		
		foreach ($xml->command->submenuTable->row as $key => $value)
		{
			$_SESSION["aa"]["submenu" . $a]["id"] = strval($value->col[0]);
			$subId = strval($value->col[0]);

			
			$xmlinput = xmlHeader($sessionid, "GroupAutoAttendantSubmenuGetRequest20");
			$xmlinput .= "<serviceUserId>" . $aaId . "</serviceUserId>";
			$xmlinput .= "<submenuId>" . htmlspecialchars(strval($value->col[0])) . "</submenuId>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			//print_r($xml);
			$_SESSION["aa"]["submenu" . $a]["enableLevelExtensionDialing"] = strval($xml->command->enableLevelExtensionDialing);
			$_SESSION["aa"]["submenu" . $a]["announcementSelection"] = strval($xml->command->announcementSelection);
			
			if(isset($xml->command->audioFile->name)){
			    $valforAudio = $subId."***".strval($xml->command->audioFile->name).".".strval($xml->command->audioFile->mediaFileType);
			}else{
			    $valforAudio = $subId."***";
			}
			
			if(isset($xml->command->videoFile->name)){
			    $valForVideo = strval($xml->command->videoFile->name).".".strval($xml->command->videoFile->mediaFileType);
			}else{
			    $valForVideo = "";
			}
			
			$submenuArr[] = $valforAudio."***".$valForVideo."***".strval($xml->command->announcementSelection);
			
			if(isset($xml->command->audioFile->name)){
			    $_SESSION["aa"]["submenu" . $a]["announcementId"] = strval($xml->command->audioFile->name).".".strval($xml->command->audioFile->mediaFileType);
			}else{
			    $_SESSION["aa"]["submenu" . $a]["announcementId"] = "";
			}
			
			if(isset($xml->command->videoFile->name)){
			    $_SESSION["aa"]["submenu" . $a]["vidAnnouncementId"] = strval($xml->command->videoFile->name).".".strval($xml->command->videoFile->mediaFileType);
			}else{
			    $_SESSION["aa"]["submenu" . $a]["vidAnnouncementId"] = "";
			}
			
			foreach ($xml->command->keyConfiguration as $k => $v)
			{
				$_SESSION["aa"]["submenu" . $a]["keys"][strval($v->key)]["desc"] = strval($v->entry->description);
				$_SESSION["aa"]["submenu" . $a]["keys"][strval($v->key)]["action"] = strval($v->entry->action);
				$_SESSION["aa"]["submenu" . $a]["keys"][strval($v->key)]["phoneNumber"] = strval($v->entry->phoneNumber);
				$_SESSION["aa"]["submenu" . $a]["keys"][strval($v->key)]["submenuId"] = strval($v->entry->submenuId);
				if(isset($v->entry->audioFile->name)){
				    $_SESSION["aa"]["submenu" . $a]["keys"][strval($v->key)]["announcements"] = strval($v->entry->audioFile->name).".".strval($v->entry->audioFile->mediaFileType);
				}else{
				    $_SESSION["aa"]["submenu" . $a]["keys"][strval($v->key)]["announcements"] = "";
				}
				
				if(isset($v->entry->videoFile->name)){
				    $_SESSION["aa"]["submenu" . $a]["keys"][strval($v->key)]["vidAnnouncements"] = strval($v->entry->videoFile->name).".".strval($v->entry->videoFile->mediaFileType);
				}else{
				    $_SESSION["aa"]["submenu" . $a]["keys"][strval($v->key)]["vidAnnouncements"] = "";
				}
				 
			}
			$a++;
		}
		
		if(count($submenuArr) > 0){
		    $_SESSION["aa"]["submenuPersonalGreetings"] = implode(",", $submenuArr);
		}else{
		    $_SESSION["aa"]["submenuPersonalGreetings"] = "";
		}
		
	}
	
	
	if ($ociVersion == "19")
	{
	    $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetVoicePortalRequest16");
	}
	else
	{
	    $xmlinput = xmlHeader($sessionid, "UserVoiceMessagingUserGetVoicePortalRequest20");
	}
	$xmlinput .= "<userId>" . $aaId . "</userId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	if (readErrorXmlGenuine($xml) == "") {
	    $anName = strval($xml->command->personalizedNameAudioFile->name);
	    $anType = strval($xml->command->personalizedNameAudioFile->mediaFileType);
	    $_SESSION["aa"]["vp"]["announcementId"] = $anName.".".$anType;
	}
	
	$phoneNumberStatus = "No";
	$userPhoneNumber = "";
	if(isset($_SESSION["aa"]["phoneNumber"]) && !empty($_SESSION["aa"]["phoneNumber"])){
		require_once ("/var/www/lib/broadsoft/adminPortal/commonUtility.php");
		require_once ("/var/www/lib/broadsoft/adminPortal/dns/dns.php");
		
		$ccDn = new Dns (); 
		$numberActivateResponse = $ccDn->getUserDNActivateListRequest ( $aaId );
		//echo "<pre>"; print_r($numberActivateResponse); die;
		if (empty ( $numberActivateResponse ['Error'] )) {
			if (! empty ( $numberActivateResponse ['Success'] [0] ['phoneNumber'] )) {
				$userPhoneNumber = $numberActivateResponse ['Success'] [0] ['phoneNumber'];
			}
			if (! empty ( $numberActivateResponse ['Success'] [0] ['status'] )) {
				$phoneNumberStatus = $numberActivateResponse ['Success'] [0] ['status'];
			}
		}
	}
	$_SESSION["aa"]["aaActivateNumber"] = $phoneNumberStatus;
	
	//echo "<pre>"; print_r($_SESSION["aa"]);
	
?>
