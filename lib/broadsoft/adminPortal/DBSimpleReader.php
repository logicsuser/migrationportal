<?php

/**
 * Created by Karl.
 * Date: 8/16/2016
 */
class DBSimpleReader
{
    private $db;
    private $values;

    public function __construct($db, $table) {
        $this->db = $db;
        $columns = $this->getColumnNames($table);
        $this->getValues($table, $columns);
    }

    public function get($column) {
        return isset($this->values[$column]) ? $this->values[$column] : "";
    }


    private function getColumnNames($table) {
        $query = "describe " . $table;
        $result = $this->db->query($query);
        $columns = array();
        while ($row = $result->fetch()) {
            $columns[] = $row["Field"];
        }

        return $columns;
    }

    private function getValues($table, $columns) {
        $query = "select * from " . $table;
        $result = $this->db->query($query);
        $row = $result->fetch();
        for ($i = 0; $i < count($columns); $i++) {
            $colName = $columns[$i];
            $this->values[$colName] = $row[$colName];
        }
    }
}
