<?php
/**
 * Created by Karl.
 * Date: 8/25/2016
 */


    unset($_SESSION["groupInfo"]);

    $xmlinput = xmlHeader($sessionid, "GroupGetRequest14sp7");
    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
    $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
    $xmlinput .= xmlFooter();
    $response = $client->processOCIMessage(array("in0" => $xmlinput));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

    $_SESSION["groupInfo"]["defaultDomain"] = strval($xml->command->defaultDomain);
    $_SESSION["groupInfo"]["groupName"] = strval($xml->command->groupName);
    $_SESSION["groupInfo"]["addressLine1"] = strval($xml->command->address->addressLine1);
    $_SESSION["groupInfo"]["addressLine2"] = strval($xml->command->address->addressLine2);
    $_SESSION["groupInfo"]["city"] = strval($xml->command->address->city);
    $_SESSION["groupInfo"]["stateOrProvince"] = strval($xml->command->address->stateOrProvince);
    $_SESSION["groupInfo"]["zipOrPostalCode"] = strval($xml->command->address->zipOrPostalCode);
    $_SESSION["groupInfo"]["country"] = strval($xml->command->address->country);

    $_SESSION["groupInfo"]["timeZone"] = strval($xml->command->timeZone);

?>
