<?php 

class ServiceProviderOperations {

		public function getSystemServerProviderList(){
			global $sessionid, $client;
			
			$sps = array();
			$getSPName = array();
			$xmlinput = xmlHeader($sessionid, "ServiceProviderGetListRequest");
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			$a = 0;
			foreach ($xml->command->serviceProviderTable->row as $k => $v)
			{
				$sps[$a] = strval($v->col[0]);
				// $getSPName[$a] = strval($v->col[0].' - '.$v->col[1]);
				if(isset($v->col[1]) && !empty($v->col[1])){
					// $getSPName[$a] = strval($v->col[0].' - '.$v->col[1]);
					$getSPName[$a] = strval($v->col[0].'<span>-</span>'.$v->col[1]); //ex-774
				}else{
					$getSPName[$a] = strval($v->col[0]);
				}
				$a++;
			}
			
			$sps = subval_sort($sps);
			return $sps;
            }
	
		public function isPreviousSessionValid($sessionValues) {
		$response = true;
		if( isset($sessionValues["sp"]) && isset($sessionValues["groupId"]) ) {
			$spProfileRes = $this->getServerProviderProfile($sessionValues["sp"]);
			if( ! empty($spProfileRes['Error']) ) {
				$response = false;
			} else {
					$groupProfileRes = $this->getGroupProfile($sessionValues["sp"], $sessionValues["groupId"]);
					if( ! empty($groupProfileRes['Error']) ) {
						$response = false;
					}
			}
			
		} else if ( isset($sessionValues["sp"]) ){
			$spProfileRes = $this->getServerProviderProfile($sessionValues["sp"]);
			if( ! empty($spProfileRes['Error']) ) {
				$response = false;
			}
		}
		return $response;
	}
	
	public function getServerProviderProfile($sp) {
		global $sessionid, $client;
			$res['Succcess'] = "";
			$res['Error'] = "";
			$xmlinput = xmlHeader($sessionid, "ServiceProviderGetRequest17sp1");
			$xmlinput .= "<serviceProviderId>" . $sp . "</serviceProviderId>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			
			 if(strval($xml->command['type']) == "Error") {
				$res['Error'] = strval($xml->command->summary);
			} else {
				$res['Succcess'] = $xml->command;
			}
			return $res;
	}
	
		public function getGroupProfile($sp, $groupId) {
		global $sessionid, $client;
			$res['Succcess'] = "";
			$res['Error'] = "";
			$xmlinput = xmlHeader($sessionid, "GroupGetRequest14sp7");
			$xmlinput .= "<serviceProviderId>" . $sp . "</serviceProviderId>";
			$xmlinput .= "<groupId>" . $groupId . "</groupId>";
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			
			 if(strval($xml->command['type']) == "Error") {
				$res['Error'] = strval($xml->command->summary);
			} else {
				$res['Succcess'] = $xml->command;
			}
			return $res;
	}
        
        public function getSystemServerProviderListForChangedCluster(){
			global $sessionid, $client;
			$spArr = array();
			$sps   = array();
			$getSPName = array();
			$xmlinput = xmlHeader($sessionid, "ServiceProviderGetListRequest");
			$xmlinput .= xmlFooter();
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			$a = 0;
			foreach ($xml->command->serviceProviderTable->row as $k => $v)
			{
				$sps[$a] = strval($v->col[0]);
				// $getSPName[$a] = strval($v->col[0].' - '.$v->col[1]);
				if(isset($v->col[1]) && !empty($v->col[1])){
					// $getSPName[$a] = strval($v->col[0].' - '.$v->col[1]);
					$getSPName[$a] = strval($v->col[0].'<span>-</span>'.$v->col[1]); //ex-774
				}else{
					$getSPName[$a] = strval($v->col[0]);
				}
				$a++;
			}
			
			$sps = subval_sort($sps);
                        $spArr['sps'] = $sps;
                        $spArr['getSPName'] = $getSPName;
			return $spArr;
            }	
}

?>