<?php

/**
 * Child class for ResourceNameBuilder
 * Note: this class must implement function: getFallBackName()
 *
 * Created by Karl.
 * Date: 8/21/2016
 */
class UserIdNameBuilder extends ResourceNameBuilder
{

    /**
     * UserIdNameBuilder constructor.
     * @param $nameStr              - formula string
     * @param $dn                   - DN
     * @param $extension            - User Extension (absolutely mandatory for name resolving)
     * @param $userClid             - User CLID
     * @param $groupNumber          - Group number
     * @param $systemDefaultDomain  - not a group default domain
     * @param bool $forceResolve    - flag indicating whether name should use fallback to be resolved
     */
    public function __construct($nameStr, $dn, $extension, $userClid, $groupNumber, $systemDefaultDomain, $ent, $forceResolve = false) {

        $registeredParams = array(
            ResourceNameBuilder::DN,
            ResourceNameBuilder::EXT,
            ResourceNameBuilder::USR_CLID,
            ResourceNameBuilder::GRP_DN,
            //ResourceNameBuilder::USR_ID,
            ResourceNameBuilder::GRP_ID,
            ResourceNameBuilder::SYS_DFLT_DOMAIN,
            ResourceNameBuilder::DEFAULT_DOMAIN,
            //ResourceNameBuilder::SRG_DOMAIN,
            //ResourceNameBuilder::GRP_PROXY_DOMAIN,
            ResourceNameBuilder::FIRST_NAME,
            ResourceNameBuilder::LAST_NAME,
            ResourceNameBuilder::DEV_NAME,
            //ResourceNameBuilder::DEV_TYPE,
            ResourceNameBuilder::MAC_ADDR,
            //ResourceNameBuilder::INT_1,
            //ResourceNameBuilder::INT_2,
            ResourceNameBuilder::TXT,
            ResourceNameBuilder::ENT
        );

        if ($dn != "")                  { $this->defaults[ResourceNameBuilder::DN]              = $dn; }
        if ($extension != "")           { $this->defaults[ResourceNameBuilder::EXT]             = $extension; }
        if ($userClid != "")            { $this->defaults[ResourceNameBuilder::USR_CLID]        = $userClid; }
        if ($groupNumber != "")         { $this->defaults[ResourceNameBuilder::GRP_DN]          = $groupNumber; }
        if ($systemDefaultDomain != "") { $this->defaults[ResourceNameBuilder::SYS_DFLT_DOMAIN] = $systemDefaultDomain; }
        if ($ent != "") { $this->defaults[ResourceNameBuilder::ENT] = $ent; }

        parent::__construct($nameStr, $registeredParams, $forceResolve);
    }


    public function getFallbackName()
    {
        $domain = $this->getDomainForFallback();
        $name = "x" . $this->getDefaultAttribute(ResourceNameBuilder::EXT) . "@" . $domain;

        $attr = $this->getDefaultAttribute(ResourceNameBuilder::DN);
        if ($attr != "") { return $attr . $name; }

        $attr = $this->getDefaultAttribute(ResourceNameBuilder::USR_CLID);
        if ($attr != "") { return $attr . $name; }

        $attr = $this->getDefaultAttribute(ResourceNameBuilder::GRP_DN);
        if ($attr != "") { return $attr . $name; }

        return "000" . $name;
    }

}//class

