<?php
	$xmlinput = xmlHeader($sessionid, "SystemLanguageGetListRequest");
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->languageTable->row as $key => $value)
	{
		$languages[$a] = strval($value->col[0]);
		$a++;
	}
?>
