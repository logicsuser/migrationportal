<?php
	$xmlinput = xmlHeader($sessionid, "GroupCallPickupGetInstanceListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$groupCallPickupList = array();
	foreach ($xml->command as $key => $value)
	{
		$groupCallPickupList[] = $value->name;
	}

	if (isset($groupCallPickupList))
	{
		$groupCallPickupList = subval_sort($groupCallPickupList);
	}


?>
