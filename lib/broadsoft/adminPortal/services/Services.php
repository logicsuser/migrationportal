<?php

class Services{
	
	public $groupId, $compareSpList, $compareSpAssignArray;
	function GroupServiceGetAuthorizationListRequest($groupId){ 
		global $sessionid, $client;
		$grpAuthorizeServiceList["Error"] = "";
		$grpAuthorizeServiceList["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupServiceGetAuthorizationListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		//$xmlinput .= "<serviceProviderId>Sollogics</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			$grpAuthorizeServiceList["Error"] = $xml->command->summaryEnglish;
		}else{
			$a=0;
			$_SESSION['groupInfoData']['servicePackAuth'] = array();
			$_SESSION['groupInfoData']['servicePackAssign'] = array();
			$_SESSION['groupInfoData']['groupServiceAuth'] = array();
			$_SESSION['groupInfoData']['groupServiceAssign'] = array();
			$_SESSION['groupInfoData']['userServiceAuth'] = array();
			$_SESSION['groupInfoData']['servicePackAuth']["auth"] = array();
			$_SESSION['groupInfoData']['servicePackAuth']["unauth"] = array();
			
			foreach ($xml->command->servicePacksAuthorizationTable->row as $key => $value){
				$grpAuthorizeServiceList["Success"]['servicePacksAuthorizationTable'][$a][0] = strval($value->col[0]);
				$grpAuthorizeServiceList["Success"]['servicePacksAuthorizationTable'][$a][1] = strval($value->col[1]);
				$grpAuthorizeServiceList["Success"]['servicePacksAuthorizationTable'][$a][2] = strval($value->col[2]);
				
				$inputAuthName =  strval($value->col[0]);
				$servicePackValueName = str_replace(array('/', ' '), array(''), $inputAuthName);
				$grpAuthorizeServiceList["Success"]['servicePacksAuthorizationTable'][$a][3] = $servicePackValueName;
				
				if(strval($value->col[1]) == "true"){
					$_SESSION['groupInfoData']['servicePackAuth']["auth"][] = strval($value->col[0]);
				} else {
					$_SESSION['groupInfoData']['servicePackAuth']["unauth"][] = strval($value->col[0]);
				}
				$a++;
			}
			$b=0;
			foreach ($xml->command->groupServicesAuthorizationTable->row as $key => $value){
				$grpAuthorizeServiceList["Success"]['groupServicesAuthorizationTable'][$b][0] = strval($value->col[0]);
				$grpAuthorizeServiceList["Success"]['groupServicesAuthorizationTable'][$b][1] = strval($value->col[1]);
				$grpAuthorizeServiceList["Success"]['groupServicesAuthorizationTable'][$b][2] = strval($value->col[2]);
				$inputAuthSName = strval($value->col[0]);
				$grpAuthorizeServiceList["Success"]['groupServicesAuthorizationTable'][$b][3] = str_replace(array('/', ' '), array(''), $inputAuthSName);
				if(strval($value->col[1]) == "true"){
					$_SESSION['groupInfoData']['groupServiceAuth'][] = strval($value->col[0]);
				} else {
					$_SESSION['groupInfoData']['groupServiceUnAuth'][] = strval($value->col[0]);
				}
				if(strval($value->col[2]) == "true"){
					$_SESSION['groupInfoData']['groupServiceAssign'][] = strval($value->col[0]);
				} else {
					$_SESSION['groupInfoData']['groupServiceUnAssign'][] = strval($value->col[0]);
				}
				$b++;
			}
			$c=0;
			foreach ($xml->command->userServicesAuthorizationTable->row as $key => $value){
				$grpAuthorizeServiceList["Success"]['userServicesAuthorizationTable'][$c][0] = strval($value->col[0]);
				$grpAuthorizeServiceList["Success"]['userServicesAuthorizationTable'][$c][1] = strval($value->col[1]);
				$grpAuthorizeServiceList["Success"]['userServicesAuthorizationTable'][$c][2] = strval($value->col[2]);
				$inputAuthUName = strval($value->col[0]);
				$grpAuthorizeServiceList["Success"]['userServicesAuthorizationTable'][$c][3] = str_replace(array('/', ' '), array(''), $inputAuthUName);
				if(strval($value->col[1]) == "true"){
					$_SESSION['groupInfoData']['userServiceAuth'][] = strval($value->col[0]);
				}
				$c++;
			}
		}
		return $grpAuthorizeServiceList;
	}
        
        
        function GroupUserServiceGetAuthorizationListRequest($groupId){                
		
                global $sessionid, $client;
		$grpAuthorizeServiceList["Error"] = "";
		$grpAuthorizeServiceList["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupServiceGetAuthorizationListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		//$xmlinput .= "<serviceProviderId>Sollogics</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		                
		if (readErrorXmlGenuine($xml) != "") {
			$grpAuthorizeServiceList["Error"] = $xml->command->summaryEnglish;
		}else{
			$a=0;			
			$_SESSION['groupInfoData']['userServiceAuth'] = array();
			$c=0;
                        $grpUserAuthorizeServiceList = array();
			foreach ($xml->command->userServicesAuthorizationTable->row as $key => $value){
				$grpAuthorizeServiceList["Success"]['userServicesAuthorizationTable'][$c][0] = strval($value->col[0]);
				$grpAuthorizeServiceList["Success"]['userServicesAuthorizationTable'][$c][1] = strval($value->col[1]);
				$grpAuthorizeServiceList["Success"]['userServicesAuthorizationTable'][$c][2] = strval($value->col[2]);
				$inputAuthUName = strval($value->col[0]);
				$grpAuthorizeServiceList["Success"]['userServicesAuthorizationTable'][$c][3] = str_replace(array('/', ' '), array(''), $inputAuthUName);
				if(strval($value->col[1]) == "true"){
					$_SESSION['groupInfoData']['userServiceAuth'][] = strval($value->col[0]);
                                        $grpUserAuthorizeServiceList[]                  = strval($value->col[0]);
				}
				$c++;
			}
		}
		return $grpUserAuthorizeServiceList;
	}
        
        
	
	function getUpdateServicePackAuth($compareSpList){ 
		$servicePackListArrayAuth['newAuth'] = array();
		$servicePackListArrayAuth['unAuth'] = array();
		
		$servicePackAuthFromSession = $_SESSION['groupInfoData']['servicePackAuth']["auth"];
		$servicePackUnAuthFromSession = $_SESSION['groupInfoData']['servicePackAuth']["unauth"];
		
		if(count($servicePackAuthFromSession) > 0){
			foreach($servicePackAuthFromSession as $key => $value){
				
				if(!in_array($value, $compareSpList)){
					$servicePackListArrayAuth['unAuth'][] = $value;
				}
			}
		}
		if(count($compareSpList) > 0){
			foreach ($compareSpList as $key1 => $value1){
				if(!in_array($value1, $servicePackAuthFromSession)){
					$servicePackListArrayAuth['newAuth'][] = $value1;
				}
			}
		}
		return $servicePackListArrayAuth;
	}
	
	function getServicePackAsgnAssign($compareSpAssignArray){
		$servicePackListArrayAssign['newSpAssign'] = array();
		$servicePackListArrayAssign['spUnAssign'] = array();
		$servicePackAssignFromSession = $_SESSION['groupInfoData']['servicePackAssign'];
		if(count($servicePackAssignFromSession) > 0){
			foreach($servicePackAssignFromSession as $key => $val){
				if(!in_array($val, $compareSpAssignArray)){
					$servicePackListArrayAssign['spUnAssign'][] = $val;
				}
			}
		}
		if(count($compareSpAssignArray) > 0){
			foreach($compareSpAssignArray as $key => $val){
				if(!in_array($val, $servicePackAssignFromSession)){
					$servicePackListArrayAssign['newSpAssign'][] = $val;
				}
			}
		}
		return $servicePackListArrayAssign;
	}
	
	function getGroupServiceAuth($compareGroupAuthArray){ 
		$groupServiceAuthArray['newGroupAuth'] = array();
		$groupServiceAuthArray['groupUnAuth'] = array();
		$groupServiceAuthFromSession = $_SESSION['groupInfoData']['groupServiceAuth'];
		if(count($groupServiceAuthFromSession) > 0){
			foreach($groupServiceAuthFromSession as $key => $val){
				if(!in_array($val, $compareGroupAuthArray)){
					$groupServiceAuthArray['groupUnAuth'][] = $val;
				}
			}
		}
		if(count($compareGroupAuthArray) > 0){
			foreach($compareGroupAuthArray as $key => $val){
				if(!in_array($val, $groupServiceAuthFromSession)){
					$groupServiceAuthArray['newGroupAuth'][] = $val;
				}
			}
		}
		return $groupServiceAuthArray;
	}
	
	function getGroupServiceAssign($compareGroupAssignArray){
		$groupServiceAsgnAssign['newGroupAssign'] = array();
		$groupServiceAsgnAssign['GroupUnAssign'] = array();
		$groupServiceAssignFromSession = $_SESSION['groupInfoData']['groupServiceAssign'];
		if(count($groupServiceAssignFromSession) > 0){
			foreach($groupServiceAssignFromSession as $key => $val){
				if(!in_array($val, $compareGroupAssignArray)){
					$groupServiceAsgnAssign['GroupUnAssign'][] = $val;
				}
			}
		}
		if(count($compareGroupAssignArray) > 0){
			foreach($compareGroupAssignArray as $key => $val){
				if(!in_array($val, $groupServiceAssignFromSession)){
					$groupServiceAsgnAssign['newGroupAssign'][] = $val;
				}
			}
		}
		return $groupServiceAsgnAssign;
	}
	
	function getUserServiceAuth($compareUserArray){
		$userServiceAuth['newUserAuth'] = array();
		$userServiceAuth['userUnAuth'] = array();
		$userServiceAuthFromSession = $_SESSION['groupInfoData']['userServiceAuth'];
		if(count($userServiceAuthFromSession) > 0){
			foreach($userServiceAuthFromSession as $key => $val){
				if(!in_array($val, $compareUserArray)){
					$userServiceAuth['userUnAuth'][] = $val;
				}
			}
		}
		if(count($compareUserArray) > 0){
			foreach($compareUserArray as $key => $val){
				if(!in_array($val, $userServiceAuthFromSession)){
					$userServiceAuth['newUserAuth'][] = $val;
				}
			}
		}
		return $userServiceAuth;
	}
	
	function updateServicePackNewAuth($changedArrayNewAuth, $groupId){
		global $sessionid, $client;
		$grpSPNewAuthUpdated["Error"] = "";
		$grpSPNewAuthUpdated["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupServiceModifyAuthorizationListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>".htmlspecialchars($groupId)."</groupId>";
		
		foreach ($changedArrayNewAuth as $Key => $value){
			$xmlinput .= "<servicePackAuthorization>";
			$xmlinput .= "<servicePackName>".$value."</servicePackName>";
			$xmlinput .= "<authorizedQuantity>";
			$xmlinput .= "<unlimited>true</unlimited>";
			$xmlinput .= "</authorizedQuantity>";
			$xmlinput .= "</servicePackAuthorization>";
		}
		
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$grpSPNewAuthUpdated["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			$grpSPNewAuthUpdated["Success"] = "Success";
		}
		return $grpSPNewAuthUpdated;
	}
	function updateServicePackUnAuth($changedArrayUnAuth, $groupId){
		global $sessionid, $client;
		$grpSPUnAuthUpdated["Error"] = "";
		$grpSPUnAuthUpdated["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupServiceModifyAuthorizationListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>".htmlspecialchars($groupId)."</groupId>";
		
		foreach ($changedArrayUnAuth as $Key => $value){
			$xmlinput .= "<servicePackAuthorization>";
			$xmlinput .= "<servicePackName>".$value."</servicePackName>";
			$xmlinput .= "<unauthorized>true</unauthorized>";
			$xmlinput .= "</servicePackAuthorization>";
		}
		
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$grpSPUnAuthUpdated["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			$grpSPUnAuthUpdated["Success"] = "Success";
		}
		return $grpSPUnAuthUpdated;
	}
	
	function updateGroupServiceNewAuth($newArray, $groupId){
		global $sessionid, $client;
		$grpNewAuthUpdated["Error"] = "";
		$grpNewAuthUpdated["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupServiceModifyAuthorizationListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>".htmlspecialchars($groupId)."</groupId>";
		
		foreach ($newArray as $Key => $value){
			$xmlinput .= "<groupServiceAuthorization>";
			$xmlinput .= "<serviceName>".$value."</serviceName>";
			$xmlinput .= "<authorizedQuantity>";
			$xmlinput .= "<unlimited>true</unlimited>";
			$xmlinput .= "</authorizedQuantity>";
			$xmlinput .= "</groupServiceAuthorization>";
		}
		
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$grpNewAuthUpdated["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			$grpNewAuthUpdated["Success"] = "Success";
		}
		return $grpNewAuthUpdated;
	}
	
	function updateGroupServiceUnAuth($unAuthArray, $groupId){
		global $sessionid, $client;
		$grpUnAuthUpdated["Error"] = "";
		$grpUnAuthUpdated["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupServiceModifyAuthorizationListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>".htmlspecialchars($groupId)."</groupId>";
		
		foreach ($unAuthArray as $Key => $value){
			$xmlinput .= "<groupServiceAuthorization>";
			$xmlinput .= "<serviceName>".$value."</serviceName>";
			$xmlinput .= "<unauthorized>true</unauthorized>";
			$xmlinput .= "</groupServiceAuthorization>";
		}
		
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$grpUnAuthUpdated["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			$grpUnAuthUpdated["Success"] = "Success";
		}
		return $grpUnAuthUpdated;
	}
	
	function updateGroupAssignService($assignArray, $groupId){
		global $sessionid, $client;
		$grpAssignUpdated["Error"] = "";
		$grpAssignUpdated["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupServiceAssignListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>".htmlspecialchars($groupId)."</groupId>";
		
		foreach ($assignArray as $Key => $value){
			$xmlinput .= "<serviceName>".$value."</serviceName>";
		}
		
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$grpAssignUpdated["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			$grpAssignUpdated["Success"] = "Success";
		}
		return $grpAssignUpdated;
	}
	
	function updateGroupUnAssignService($unAssignArray, $groupId){
		global $sessionid, $client;
		$grpUnAssignUpdated["Error"] = "";
		$grpUnAssignUpdated["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupServiceUnassignListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>".htmlspecialchars($groupId)."</groupId>";
		
		foreach ($unAssignArray as $Key => $value){
			$xmlinput .= "<serviceName>".$value."</serviceName>";
		}
		
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$grpUnAssignUpdated["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			$grpUnAssignUpdated["Success"] = "Success";
		}
		return $grpUnAssignUpdated;
	}
	
	function updateUserAuthService($userArray, $groupId){
		global $sessionid, $client;
		$userAuthArray["Error"] = "";
		$userAuthArray["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupServiceModifyAuthorizationListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		
		foreach ($userArray as $Key => $value){
			$xmlinput .= "<userServiceAuthorization>";
			$xmlinput .= "<serviceName>".$value."</serviceName>";
			$xmlinput .= "<authorizedQuantity>";
			$xmlinput .= "<unlimited>true</unlimited>";
			$xmlinput .= "</authorizedQuantity>";
			$xmlinput .= "</userServiceAuthorization>";
		}
		
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			 $userAuthArray["Error"]['Detail'] = strval($xml->command->detail);
                         $serviceName = "";
                         $tmpArr1     = explode(":", strval($xml->command->detail));                            
                         if(is_array($tmpArr1)){
                            $serviceName     = $tmpArr1[0];
                            $serviceErrorMsg = $this->retriveLicenseErrorAgainstServiceName($serviceName);
                            $userAuthArray["Error"]['Detail'] .= " (".$serviceErrorMsg.") ";
                        }
		}else{
			$userAuthArray["Success"] = "Success";
		}
		return $userAuthArray;
		
	}
	
	function updateUserUnAuthService($unAuthArray, $groupId){ 
		global $sessionid, $client;
		$userUnAuthUpdated["Error"] = "";
		$userUnAuthUpdated["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupServiceModifyAuthorizationListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		
		foreach ($unAuthArray as $Key => $value){
			$xmlinput .= "<userServiceAuthorization>";
			$xmlinput .= "<serviceName>".$value."</serviceName>";
			$xmlinput .= "<unauthorized>true</unauthorized>";
			$xmlinput .= "</userServiceAuthorization>";
		}
		
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$userUnAuthUpdated["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			$userUnAuthUpdated["Success"] = "Success";
		}
		return $userUnAuthUpdated;
	}
	
	function networkClassOfServicesAssignedListToGroupRequest($groupId){
		global $sessionid, $client;
		$ncsAssignedToGroupArr = array();
		$ncsAssignedToGroupArr["Error"] = "";
		$ncsAssignedToGroupArr["Success"] = "";
		$_SESSION['groupInfoData']['assignedNCS']=array();
		$_SESSION['groupInfoData']['assignedDefault']=array();
		$xmlinput = xmlHeader($sessionid, "GroupNetworkClassOfServiceGetAssignedListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$ncsAssignedToGroupArr["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			$i = 0;
			if(isset($xml->command->networkClassOfServiceTable->row)){
				foreach ($xml->command->networkClassOfServiceTable->row as $key => $val){
						$ncsAssignedToGroupArr["Success"]["ncsAssignedToGroupArr"][$i][0] = strval($val->col[0]);
						$_SESSION['groupInfoData']['assignedNCS'][] = strval($val->col[0]);
						$ncsAssignedToGroupArr["Success"]["ncsAssignedToGroupArr"][$i][1] = strval($val->col[2]);
						if($val->col[2] == "true"){
							$ncsAssignedToGroupArr["Success"]["ncsAssignedDefault"][] = strval($val->col[0]);
							$_SESSION['groupInfoData']['assignedDefault'][] = strval($val->col[0]);
						}
					$i++;
				}
			}
		}
		return $ncsAssignedToGroupArr;
	}
	
	function networkClassOfServicesAvailableForAssignToGroup($groupId){ 
		global $sessionid, $client;
		$ncsAvailArr = array();
		$ncsAvailArr["Error"] = array();
		$ncsAvailArr["Success"] = array();
		$xmlinput = xmlHeader($sessionid, "ServiceProviderNetworkClassOfServiceGetAssignedListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$ncsAvailArr["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			$j = 0;
			if(isset($xml->command->networkClassOfServiceTable->row)){
				foreach ($xml->command->networkClassOfServiceTable->row as $key => $val){
					$ncsAvailArr["Success"]['ncsAvailArr'][$j][0] = strval($val->col[0]);
					$ncsAvailArr["Success"]['ncsAvailArr'][$j][1] = strval($val->col[2]);
					$j++;
				}
			}
		}
		$nscAssignedArr = $this->networkClassOfServicesAssignedListToGroupRequest($groupId);
		$arrayToAssign = $this->compareAvailableAndAssignedArray($nscAssignedArr, $ncsAvailArr);
		return $arrayToAssign;
	}
	
	function compareAvailableAndAssignedArray($nscAssignedArr, $ncsAvailArr){
		
		$ncsAbailToAssign = array();
		$ncsAssigned = array();
		$availableArrayToAssign = array();
		
		if(isset($ncsAvailArr['Success']['ncsAvailArr']) && count($ncsAvailArr['Success']['ncsAvailArr']) > 0){
			foreach ($ncsAvailArr['Success']['ncsAvailArr'] as $key => $val){
				$ncsAbailToAssign[] = $val[0];
			}
		}else{
			$ncsAbailToAssign = array();
		}
		
		if(isset($nscAssignedArr['Success']['ncsAssignedToGroupArr']) && count($nscAssignedArr['Success']['ncsAssignedToGroupArr']) > 0){
			foreach ($nscAssignedArr['Success']['ncsAssignedToGroupArr'] as $key => $val){
				$ncsAssigned[] = $val[0];
			}
		}else{
			$ncsAssigned = array();
		}
		
		foreach ($ncsAbailToAssign as $key => $value){
			if(!in_array($value, $ncsAssigned)){
				$availableArrayToAssign['availableArrayToAssign'][] = $value;
			}
		}
		
		return $availableArrayToAssign;	
	}
	
	function compareAssignedNcs($assignedPostData){
		$ncsArray['newAssign'] = array();
		$ncsArray['unAssign'] = array();
		$assignedNcsFromSession = $_SESSION['groupInfoData']['assignedNCS'];

		if(count($assignedNcsFromSession) > 0){
			foreach ($assignedNcsFromSession as $key => $value){
				if(!in_array($value, $assignedPostData)){
					$ncsArray['unAssign'][] = $value;
				}
			}
		}
		if(count($assignedPostData) > 0){
			foreach ($assignedPostData as $key => $value){
				if(!in_array($value, $assignedNcsFromSession)){
					$ncsArray['newAssign'][] = $value;
				}
			}
		}
		return $ncsArray;
	}
	
	function compareAssignedDefaultNcs($assignedDefaultPostData){
		$ncsDefaultArray['newAssign'] = array();
		$ncsDefaultArray['unAssign'] = array();
		$ncsDefaultFromSession = $_SESSION['groupInfoData']['assignedDefault'];
		if(count($ncsDefaultFromSession) > 0){
			foreach ($ncsDefaultFromSession as $key => $value){
				if(!in_array($value, $assignedDefaultPostData)){
					$ncsDefaultArray['unAssign'][] = $value;
				}
			}
		}
		if(count($assignedDefaultPostData) > 0){
			foreach ($assignedDefaultPostData as $key => $value){
				if(!in_array($value, $ncsDefaultFromSession)){
					$ncsDefaultArray['newAssign'][] = $value;
				}
			}
		}
		return $ncsDefaultArray;
	}
	
	function updateNcsAssignOrDefault($ncsNewAssign, $ncsDefaultAssign, $groupId){
		global $sessionid, $client;
		$ncsAssigned["Error"] = "";
		$ncsAssigned["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupNetworkClassOfServiceAssignListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>".htmlspecialchars($groupId)."</groupId>";
		if(count($ncsNewAssign) > 0){
			foreach ($ncsNewAssign as $key => $value){
				$xmlinput .= "<networkClassOfService>".$value."</networkClassOfService>";
			}
			
		}
		if(count($ncsDefaultAssign) > 0){
			foreach ($ncsDefaultAssign as $key => $value){
				$xmlinput .= "<defaultNetworkClassOfService>".$value."</defaultNetworkClassOfService>";
			}
			
		}
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$ncsAssigned["Error"]['Detail'] = strval($xml->command->detail);
		}else{
			$ncsAssigned["Success"] = "Success";
		}
		return $ncsAssigned;
	}
	
	function updateNcsUnassign($ncsUnAssign, $groupId){ 
		global $sessionid, $client;
		$ncsUnAssigned["Error"] = "";
		$ncsUnAssigned["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupNetworkClassOfServiceUnassignListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>".htmlspecialchars($groupId)."</groupId>";
		foreach ($ncsUnAssign as $key => $value){
			$xmlinput .= "<networkClassOfService>".$value."</networkClassOfService>";
		
		}
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$ncsUnAssigned["Error"]['Detail'] = strval($xml->command->summary);
		}else{
			$ncsUnAssigned["Success"] = "Success";
		}
		return $ncsUnAssigned;
	}

	public function getServicePackServicesListServiceProvider($servicePacks) {
		
		global $sessionid, $client;
		$array = array();
		
		foreach ($servicePacks as $key=>$servicePack) {
			$xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
			$xmlinput .= "<serviceProviderId>". htmlspecialchars($_SESSION['sp']) ."</serviceProviderId>";
			//$xmlinput .= "<serviceProviderId>Sollogics</serviceProviderId>";
			$xmlinput .= "<servicePackName>" . $servicePack[0] . "</servicePackName>";
			$xmlinput .= xmlFooter();
			
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			
			$r = 0;
			foreach($xml->command->userServiceTable->row as $key1=>$val1){
				$array[$key][$r] = $val1;
				$r++;
			}
		}
		return $array;
	}
	
	public function getservicePacksHaveSCAService($servicePackList) {
		$servicePackArray = array();
		//print_r($servicePackList); die;
		foreach($servicePackList as $key=>$val){
			if($val[1] == "true"){
				$servicePackArray[] = $val;
			}
		}
		ksort($servicePackArray);
		return $servicePackArray;
	}
	
	public function checkScaServiceInServicepack($servicesArray){
		$e = 0;
		foreach($servicesArray as $sKey=>$sVal){
			foreach($sVal as $sKey1=>$sVal1){
				$pos = strpos($sVal1[0]->col[0], "Shared Call Appearance");
				if($pos === false){
					//$scaServiceArray[$sKey]->countMore = "false";
				}else{
					$scaServiceArray[$e] = $sVal1;
					//$scaServiceArray[$sKey]->count = count();
					//$scaServiceArray[$e]->countMore = "m";
				}
			}
			$e++;
			$scaServiceArray[$sKey]->count = count($sVal);
		}
		return $scaServiceArray;
	}
	
	public function checkScaUserServiceInServicepack($servicesArray){
		
		$e = 0;
		$scaServiceArray = array();
		foreach($servicesArray as $sKey=>$sVal){
			foreach($sVal as $sKey1=>$sVal1){$d = 0;
				$pos = strpos($sVal1->col[0], "Shared Call Appearance");
				if($pos === false){
					//$scaServiceArray[$sKey]->countMore = "false";
				}else{
					$scaServiceArray[$d] = $sVal1;
					
					//$scaServiceArray[$sKey]->count = count();
					//$scaServiceArray[$e]->countMore = "m";
				}$d++;
			}
			//$e++;
			//$scaServiceArray[$sKey]->count = count($sVal);
		}
		return $scaServiceArray;
	}
	

	public function assignServicesToUser($userId, $services){
	    
	    global $sessionid, $client;
	    $assignResponse["Error"] = array();
	    $assignResponse["Success"] = array();
	    
	    $xmlinput = xmlHeader($sessionid, "UserServiceAssignListRequest");
	    $xmlinput .= "<userId>" . $userId . "</userId>";
	    foreach($services as $serKey => $serValue) {
	        $xmlinput .= "<serviceName>" . htmlspecialchars($serValue) . "</serviceName>";
	    }
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    if (readErrorXmlGenuine($xml) != "") {
	        $assignResponse["Error"] = $xml->command->summaryEnglish;
	    }else{
	        $assignResponse["Success"] = "Success";
	        
	    }
	    return $assignResponse;
	}
	
	public function unAssignServicesFromUser($userId, $services){
	    
	    global $sessionid, $client;
	    $unAssignResponse["Error"] = array();
	    $unAssignResponse["Success"] = array();
	    
	    $xmlinput = xmlHeader($sessionid, "UserServiceUnassignListRequest");
	    $xmlinput .= "<userId>" . $userId . "</userId>";
	    foreach($services as $serKey => $serValue) {
	        $xmlinput .= "<serviceName>" . htmlspecialchars($serValue) . "</serviceName>";
	    }
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    if (readErrorXmlGenuine($xml) != "") {
	        $unAssignResponse["Error"] = $xml->command->summaryEnglish;
	    }else{
	        $unAssignResponse["Success"] = "Success";
	        
	    }
	    return $unAssignResponse;
	}
	
	public function assignServicePackUser($userId, $servicePack){
		
		global $sessionid, $client;
		$assignResponse["Error"] = array();
		$assignResponse["Success"] = array();
		
		$xmlinput = xmlHeader($sessionid, "UserServiceAssignListRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= "<servicePackName>" . htmlspecialchars($servicePack) . "</servicePackName>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$assignResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$assignResponse["Success"] = "Success";
		
		}
		return $assignResponse;
	}
	
	public function unAssignServicePackUser($userId, $servicePack){
		
		global $sessionid, $client;
		$unAssignResponse["Error"] = array();
		$unAssignResponse["Success"] = array();
		
		$xmlinput = xmlHeader($sessionid, "UserServiceUnassignListRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= "<servicePackName>" . htmlspecialchars($servicePack) . "</servicePackName>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$unAssignResponse["Error"] = $xml->command->summaryEnglish;
		}else{
			$unAssignResponse["Success"] = "Success";
			
		}
		return $unAssignResponse;
	}
	
	public function compareSessionScaUser(){
		$sharedCallAppearance = array();
		if (isset($_SESSION["userInfo"]["sharedCallAppearanceUsers"]))
		{
			$sc = 0;                        
			for ($a = 0; $a < count($_SESSION["userInfo"]["sharedCallAppearanceUsers"]); $a++)
			{
				if ($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["endpointType"] == "Shared Call Appearance")
				{
					if(isset($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["phoneNumber"])){
						$sharedPhone = $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["phoneNumber"];
					}else{
						$sharedPhone = "";
					}
					if(isset($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["extension"]) && $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["extension"]!=""){
						$sharedExt = "x".$_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["extension"];
					}else{
						for($x = 0; $x < count ( $users ); $x ++)
						{
							if ($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["id"]==$users[$x]["id"])
							{
								$sharedExt = $users[$x]["extension"];
							}
						}
						
					}
					if(trim($sharedPhone)!="")
					{
						
						$id = $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["id"] . ":" . $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["name"] . ":" . $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["phoneNumber"];
					}
					else
					{
						$id = $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["id"] . ":" . $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["name"] . ":" . $sharedExt;
					}
					$sharedCallAppearance["IdArray"][$sc] = $id;
					$sharedCallAppearance["NameArray"][$sc] = $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["name"] . "  (" . $sharedPhone.$sharedExt . ")";
					$sc++;
				}
			}
		}
		return $sharedCallAppearance;
	}

	public function compareSessionScaUserForModUser(){
		$sharedCallAppearance = array();
		if (isset($_SESSION["userInfo"]["sharedCallAppearanceUsers"]))
		{
			$sc = 0;                        
			for ($a = 0; $a < count($_SESSION["userInfo"]["sharedCallAppearanceUsers"]); $a++)
			{
				if ($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["endpointType"] == "Shared Call Appearance")
				{
					if(isset($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["phoneNumber"])){
						$sharedPhone = $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["phoneNumber"];
					}else{
						$sharedPhone = "";
					}
					if(isset($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["extension"]) && $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["extension"]!="")
                                            {
                                               if($sharedPhone != ""){
                                                    $sharedExt = "x".$_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["extension"];
                                               }
                                               if($sharedPhone == ""){
                                                    $sharedExt = $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["extension"];
                                               }
                                                
					}else{
						for($x = 0; $x < count ( $users ); $x ++)
						{
							if ($_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["id"]==$users[$x]["id"])
							{
								$sharedExt = $users[$x]["extension"];
							}
						}
						
					}
					if(trim($sharedPhone)!="")
					{
						
						$id = $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["id"] . ":" . $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["name"] . ":" . $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["phoneNumber"];
					}
					else
					{
						$id = $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["id"] . ":" . $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["name"] . ":" . $sharedExt;
					}
					$sharedCallAppearance["IdArray"][$sc] = $id;
					$sharedCallAppearance["NameArray"][$sc] = $_SESSION["userInfo"]["sharedCallAppearanceUsers"][$a]["name"] . "  (" . $sharedPhone.$sharedExt . ")";
					$sc++;
				}
			}
		}
		return $sharedCallAppearance;
	}

        
        //Code added @ 23 July 2018
        public function compareSessionScaUserForDeviceInvetry(){
		$sharedCallAppearance = array();
		if (isset($_SESSION["sca"]["deviceInfo"]["userList"]))
		{
			$sc = 0;
                        //echo "<br />SCA - ";
                        //print_r($_SESSION["sca"]["deviceInfo"]["userList"]);
                        
			for ($a = 0; $a < count($_SESSION["sca"]["deviceInfo"]["userList"]); $a++)
			{
				if ($_SESSION["sca"]["deviceInfo"]["userList"][$a]["endpointType"] == "Shared Call Appearance")
				{
					if(isset($_SESSION["sca"]["deviceInfo"]["userList"][$a]["phoneNumber"])){
						$sharedPhone = $_SESSION["sca"]["deviceInfo"]["userList"][$a]["phoneNumber"];
					}else{
						$sharedPhone = "";
					}
					if(isset($_SESSION["sca"]["deviceInfo"]["userList"][$a]["extn"]) && $_SESSION["sca"]["deviceInfo"]["userList"][$a]["extn"]!="")
                                        {
                                                if($sharedPhone!=""){
                                                    $sharedExt = $sharedPhone."x".$_SESSION["sca"]["deviceInfo"]["userList"][$a]["extn"];
                                                }else{
                                                    $sharedExt = $_SESSION["sca"]["deviceInfo"]["userList"][$a]["extn"];
                                                }
					}
                                        else
                                        {
						for($x = 0; $x < count ( $users ); $x ++)
						{
							if ($_SESSION["sca"]["deviceInfo"]["userList"][$a]["userId"]==$users[$x]["userId"])
							{
								$sharedExt = $users[$x]["extn"];
							}
						}
						
					}
					if(trim($sharedPhone)!=""){						
						//$id = $_SESSION["sca"]["deviceInfo"]["userList"][$a]["userId"] . ":" . $_SESSION["sca"]["deviceInfo"]["userList"][$a]["firstName"] . " ".$_SESSION["sca"]["deviceInfo"]["userList"][$a]["lastName"] .":" . $_SESSION["sca"]["deviceInfo"]["userList"][$a]["phoneNumber"]; //Code commented @ 26 Sep 2018
                                                  $id = $_SESSION["sca"]["deviceInfo"]["userList"][$a]["userId"] . ":" . $_SESSION["sca"]["deviceInfo"]["userList"][$a]["firstName"] . " ".$_SESSION["sca"]["deviceInfo"]["userList"][$a]["lastName"] .":" . $sharedExt; //Code added @ 26 Sep 2018
					}
					else{
						$id = $_SESSION["sca"]["deviceInfo"]["userList"][$a]["userId"] . ":" . $_SESSION["sca"]["deviceInfo"]["userList"][$a]["firstName"] . " ".$_SESSION["sca"]["deviceInfo"]["userList"][$a]["lastName"] .":" . $sharedExt;
					}
					$sharedCallAppearance["IdArray"][$sc] = $id;
					$sharedCallAppearance["NameArray"][$sc] = $_SESSION["sca"]["deviceInfo"]["userList"][$a]["firstName"] . " ".$_SESSION["sca"]["deviceInfo"]["userList"][$a]["lastName"] ."  (" . $sharedPhone.$sharedExt . ")";
					$sc++;
				}
			}
		}
		return $sharedCallAppearance;
	}
        
        
	
	public function assignUnassignScaUser($sharedCallAppearance, $postScaUsers){
	
		$scaUserArray["assign"] = array(); 
		$scaUserArray["unAssign"] = array(); 
		$scaUserArray["assignNameArray"] = array();  
		
		foreach($sharedCallAppearance["IdArray"] as $scaKeyParse=>$scaValueParse){

			//if(!empty($scaValueParse) && $scaValueParse != "scaUsers" && !empty($postScaUsers[0])){
            if(!empty($scaValueParse) && $scaValueParse != "scaUsers"){                                
				if(!in_array($scaValueParse, $postScaUsers)){                                        
					$scaUserArray["unAssign"][] = $scaValueParse;
				}else{                                        
					$scaUserArray["assign"][] = $scaValueParse;
					//make the string to compare the string with matching the user id coming in the tr below so that user assign cannot be again check for assign ex-615
					$usermatchString = str_replace(" ", "", $sharedCallAppearance["NameArray"][$scaKeyParse]);
					$explodeExt = explode("x", $usermatchString);                                        
					//$assignPostArrayName[] = $explodeExt[0].")";
					$scaUserArray["assignNameArray"][] = $explodeExt[0].")"; 
				}
			}
		}
		return $scaUserArray;
	}
        
        public function assignUnassignScaUserForModUser($sharedCallAppearance, $postScaUsers){
	
		$scaUserArray["assign"] = array(); 
		$scaUserArray["unAssign"] = array(); 
		$scaUserArray["assignNameArray"] = array();  
		
		foreach($sharedCallAppearance["IdArray"] as $scaKeyParse=>$scaValueParse){

			//if(!empty($scaValueParse) && $scaValueParse != "scaUsers" && !empty($postScaUsers[0])){
            if(!empty($scaValueParse) && $scaValueParse != "scaUsers"){                                
				if(!in_array($scaValueParse, $postScaUsers)){                                        
					$scaUserArray["unAssign"][] = $scaValueParse;
				}else{                                        
					$scaUserArray["assign"][] = $scaValueParse;
					//make the string to compare the string with matching the user id coming in the tr below so that user assign cannot be again check for assign ex-615
					$usermatchString = str_replace(" ", "", $sharedCallAppearance["NameArray"][$scaKeyParse]);
					$explodeExt = explode("x", $usermatchString);  
                                        if($explodeExt[1] != ""){
                                            $scaUserArray["assignNameArray"][] = $explodeExt[0]."))"; 
                                        }else{
                                            $scaUserArray["assignNameArray"][] = $explodeExt[0]."))"; 
                                        }
					//$assignPostArrayName[] = $explodeExt[0].")";
					
				}
			}
		}
		return $scaUserArray;
	}
	
	public function getServicePackServicesListUsers($servicePacks) {
		
		global $sessionid, $client;
		$array = array();
		
		foreach ($servicePacks as $key=>$servicePack) {
			$xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
			$xmlinput .= "<serviceProviderId>". htmlspecialchars($_SESSION['sp']) ."</serviceProviderId>";
			//$xmlinput .= "<serviceProviderId>Sollogics</serviceProviderId>";
			$xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
			$xmlinput .= xmlFooter();
			
			$response = $client->processOCIMessage(array("in0" => $xmlinput));
			$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
			
			$r = 0;
			foreach($xml->command->userServiceTable->row as $key1=>$val1){
				$array[$key][$r] = $val1;
				$r++;
			}
		}
		return $array;
	}
	
	public function getServicePacksHaveScaServices($servicePacks, $sessionid, $spId, $client)
        {
            $scaServicePackArray = array();
            $count = 0;
            foreach ($servicePacks as $key => $servicePack) 
            {
                $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId). "</serviceProviderId>";
                $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
                $xmlinput .= xmlFooter();

                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);                
                foreach ($xml->command->userServiceTable->row as $key => $value)
                {
                    $pos = strpos(strval($value->col[0]), "Shared Call Appearance");
                    if($pos === false)
                    {
                        //continue;
                    }
                    else
                    {                        
                        $scaServicePackArray[$count] = $servicePack;
			$count++;
                    }
                }
            }
            return array_unique($scaServicePackArray);
        }
        //End Code
        
        //Code added @ 10 July 2018 using to check service packs have SCA service or not
        function servicePacksHaveSCAService($servicePacks) {
            global $sessionid, $client;
            $flag = 0;
            foreach ($servicePacks as $servicePack) {
                $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]). "</serviceProviderId>";
                $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
                $xmlinput .= xmlFooter();

                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

                foreach ($xml->command->userServiceTable->row as $key => $value){
                    $pos = strpos(strval($value->col[0]), "Shared Call Appearance");
                    if($pos === false)
                    {
                        //continue;
                    }
                    else
                    {  
                        $flag = 1;                    
                    }   
                }
            }
            if($flag==1){
                return true;
            }else{
                return false;
            }
        }
        
        function checkIfSCAService($servicePacks, $userServices) {
            global $sessionid, $client;
            $flag = 0;
            foreach ($servicePacks as $servicePack) {
                $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]). "</serviceProviderId>";
                $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
                $xmlinput .= xmlFooter();
                
                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                
                foreach ($xml->command->userServiceTable->row as $key => $value){
                    $pos = strpos(strval($value->col[0]), "Shared Call Appearance");
                    if($pos === false)
                    {
                        //continue;
                    }
                    else
                    {
                        $flag = 1;
                    }
                }
            }
            
            if($flag==0){
                if(count($userServices) > 0){
                    foreach ($userServices as $serviceName){
                        $position = strpos($serviceName, "Shared Call Appearance");
                        if($position === false)
                        {
                            //continue;
                        }
                        else
                        {
                            $flag = 1;
                        }
                    }
                }
            }
            
            if($flag==1){
                return true;
            }else{
                return false;
            }
        }
        
        function GroupServiceGetAuthorizationListRequestForEnterprise($groupId){ 
		global $sessionid, $client;
		$grpAuthorizeServiceList["Error"] = "";
		$grpAuthorizeServiceList["Success"] = "";
                $grpAuthorizeServiceListNew = array();
		$xmlinput = xmlHeader($sessionid, "GroupServiceGetAuthorizationListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		//$xmlinput .= "<serviceProviderId>Sollogics</serviceProviderId>";
		$xmlinput .= "<groupId>".htmlspecialchars($groupId)."</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			$grpAuthorizeServiceListNew["Error"] = $xml->command->summaryEnglish;
		}else{	
			$b=0;
			foreach ($xml->command->groupServicesAuthorizationTable->row as $key => $value){				
				if(strval($value->col[2]) == "true"){
					$grpAuthorizeServiceListNew['groupServiceAssign'][] = strval($value->col[0]);
				} else {
					
				}
				$b++;
			}			
		}
		return $grpAuthorizeServiceListNew;
	}
        
        
	function getAllGroupAssignedServices($groupId){
	    global $sessionid, $client;
	    $grpAuthorizeServiceList["Error"] = "";
	    $grpAuthorizeServiceList["Success"] = "";
	    $grpAuthorizeServiceListNew = array();
	    $grpAuthorizeServicePackListNew = array();
	    $xmlinput = xmlHeader($sessionid, "GroupServiceGetAuthorizationListRequest");
	    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
	    //$xmlinput .= "<serviceProviderId>Sollogics</serviceProviderId>";
	    $xmlinput .= "<groupId>".htmlspecialchars($groupId)."</groupId>";
	    $xmlinput .= xmlFooter();
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    
	    if (readErrorXmlGenuine($xml) != "") {
	        $grpAuthorizeServiceListNew["Error"] = $xml->command->summaryEnglish;
	    }else{
	        foreach ($xml->command->userServicesAuthorizationTable->row as $key => $value){
	            if(strval($value->col[1]) == "true"){
	                $grpAuthorizeServiceListNew[] = strval($value->col[0]);
	            }
	        }
	        foreach ($xml->command->servicePacksAuthorizationTable->row as $key1 => $value1){
	            if(strval($value1->col[1]) == "true"){
	                foreach ( $this->getSericesListOfServicePack(strval($value1->col[0])) as $packKey => $packValue) {
	                    $grpAuthorizeServicePackListNew[] = $packValue;
	                }
	            }
	        }
	        
	        $grpAuthorizeServiceListNew = array_merge($grpAuthorizeServiceListNew, $grpAuthorizeServicePackListNew);
	    }
	    
	    return $grpAuthorizeServiceListNew;
	}
	
        //Code added @ 07 Sep 2018 - Create an array of Service pack that has Polycom Phone Services
        function servicePacksHavePolycomPhoneServices($servicePacks) {
            global $sessionid, $client;
            $polycomServicePacksArr = array();
            $i = 0;
            foreach ($servicePacks as $servicePack) 
            {
                $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]). "</serviceProviderId>";
                $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
                $xmlinput .= xmlFooter();

                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                foreach ($xml->command->userServiceTable->row as $key => $value)
                {
                    if (strval($value->col[0]) == "Polycom Phone Services")
                    {
                        $polycomServicePacksArr[$i++] = $servicePack;
                        break;
                    }
                }
            }
            return $polycomServicePacksArr = array_unique($polycomServicePacksArr);
        }
        
        function checkservicePacksHasPolycomPhoneService($servicePacks) {
        global $sessionid, $client;

        foreach ($servicePacks as $servicePack) {
            $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]). "</serviceProviderId>";
            $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

            foreach ($xml->command->userServiceTable->row as $key => $value){
                if (strval($value->col[0]) == "Polycom Phone Services") {
                    return true;
                }
            }
        }
        return false;
    }
    
    //Code added @ 20 Dec 2018 regarding EX-956
    function GroupUserServicesGetAuthorizationListRequestWithCompleteDetails($groupId){               
		
                global $sessionid, $client;
		$grpAuthorizeServiceList["Error"] = "";
		$grpAuthorizeServiceList["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupServiceGetAuthorizationListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($groupId) ."</groupId>";
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                
		if (readErrorXmlGenuine($xml) != "") 
                {
			$grpAuthorizeServiceList["Error"] = $xml->command->summaryEnglish;
		}
                else
                {
			$a=0;			
			//$_SESSION['groupInfoData']['userServiceAuth'] = array();
			$c=0;
                        
                        $grpAuthorizeServiceList = array();
			foreach ($xml->command->userServicesAuthorizationTable->row as $key => $value)
                        {  
                                $keyName0 = strval($xml->command->userServicesAuthorizationTable->colHeading[0]);
				$keyName1 = strval($xml->command->userServicesAuthorizationTable->colHeading[1]);
				$keyName2 = strval($xml->command->userServicesAuthorizationTable->colHeading[2]);
				$keyName3 = strval($xml->command->userServicesAuthorizationTable->colHeading[3]);
				$keyName4 = strval($xml->command->userServicesAuthorizationTable->colHeading[4]);
				$keyName5 = strval($xml->command->userServicesAuthorizationTable->colHeading[5]);
				$keyName6 = strval($xml->command->userServicesAuthorizationTable->colHeading[6]);
				$keyName7 = strval($xml->command->userServicesAuthorizationTable->colHeading[7]);
				$keyName8 = strval($xml->command->userServicesAuthorizationTable->colHeading[8]);
				$keyName9 = strval($xml->command->userServicesAuthorizationTable->colHeading[9]);
                                $serviceName = strval($value->col[0]);
				
                                $grpAuthorizeServiceList['groupUserService'][$serviceName][$keyName0] = strval($value->col[0]);
				$grpAuthorizeServiceList['groupUserService'][$serviceName][$keyName1] = strval($value->col[1]);
				$grpAuthorizeServiceList['groupUserService'][$serviceName][$keyName2] = strval($value->col[2]);
				$grpAuthorizeServiceList['groupUserService'][$serviceName][$keyName3] = strval($value->col[3]);
				$grpAuthorizeServiceList['groupUserService'][$serviceName][$keyName4] = strval($value->col[4]);
				$grpAuthorizeServiceList['groupUserService'][$serviceName][$keyName5] = strval($value->col[5]);
				$grpAuthorizeServiceList['groupUserService'][$serviceName][$keyName6] = strval($value->col[6]);
				$grpAuthorizeServiceList['groupUserService'][$serviceName][$keyName7] = strval($value->col[7]);
				$grpAuthorizeServiceList['groupUserService'][$serviceName][$keyName8] = strval($value->col[8]);
                                $grpAuthorizeServiceList['groupUserService'][$serviceName][$keyName9] = strval($value->col[9]);
				
                                $grpAuthorizeServiceList['groupUserServiceList'][$c] = strval($value->col[0]);
                                
                                $c = $c+1;
			}
		}         
		return $grpAuthorizeServiceList;
	}
        //End code
        
            //Code added @ 21 Dec 2018 regarding EX-956
            function getSericesListOfServicePack($servicePackName) {
            global $sessionid, $client;
            $serviceNameList = array();

            $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]). "</serviceProviderId>";
            $xmlinput .= "<servicePackName>" . $servicePackName . "</servicePackName>";
            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

            foreach ($xml->command->userServiceTable->row as $key => $value){
                $serviceNameList[] = strval($value->col[0]);
            }

            return $serviceNameList;
          }
        //End code
      
        //Code added @ 27 Dec 2018 regarding EX-956
        function getServiceLicenseErrorOnAssignServicePackToUser($groupId, $serviceList) {
            $serviceDetailArr      = array();
            $serviceErrorArr       = array();
            $groupServiceListomArr = $this->GroupUserServicesGetAuthorizationListRequestWithCompleteDetails($groupId);
            $groupServiceList      = $groupServiceListomArr['groupUserService'];
                     
            foreach($serviceList as $serviceName){
                    if(isset($groupServiceList[$serviceName])){
                        
                        $serviceDetailArr =  $groupServiceList[$serviceName];                        
                        if($serviceDetailArr['Limited'] && $serviceDetailArr['Quantity']==$serviceDetailArr['Usage']){
                            $serviceErrorArr[$serviceName] = "$serviceName : Service usage limit is reached";
                        }else if($serviceDetailArr['Licensed']=="false"){
                            $serviceErrorArr[$serviceName] = "Service license is violated";
                        }else if($serviceDetailArr['Licensed']=="false"){
                            $serviceErrorArr[$serviceName] = "Service license is violated";
                        }else if($serviceDetailArr['Allowed']!="Unlimited"){
                                if($serviceDetailArr['Usage']==$serviceDetailArr['Allowed']){
                                $serviceErrorArr[$serviceName] = "Limit exceeds the services that are available for allocation";
                            }
                        }else if($serviceDetailArr['User Assignable']=="false"){
                            $serviceErrorArr[$serviceName] = "Service is not assignable";
                        }else if($serviceDetailArr['User Assignable']=="false"){
                            $serviceErrorArr[$serviceName] = "Service is not assignable";
                        }else{
                            $licenseErrorMsg = $this->retriveLicenseErrorAgainstServiceName($serviceName);
                            if($licenseErrorMsg!=""){                                
                                $serviceErrorArr[$serviceName] = $this->retriveLicenseErrorAgainstServiceName($serviceName);
                            }
                        }                 
                    }
            }
            return $serviceErrorArr;          
        }
        //End code
        
        function getSystemUserServiceLicenseList(){          
		
                global $sessionid, $client;
		$sysLicenseList["Error"]   = "";
		$sysLicenseList["Success"] = array();
		$xmlinput = xmlHeader($sessionid, "SystemLicensingGetRequest14sp3");
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
                if (readErrorXmlGenuine($xml) != "") 
                {
			$sysLicenseList["Error"] = $xml->command->summaryEnglish;
		}
                else
                { 
                        $grpAuthorizeServiceList = array();
                        $count = 0;
			foreach ($xml->command->userServiceLicenseTable->row as $key => $value)
                        {  
                            
                            $licenseName = strval($value->col[0]);
                            $sysLicenseList['Success'][$licenseName]['Licensed']    = strval($value->col[1]);
                            $sysLicenseList['Success'][$licenseName]['Used']        = strval($value->col[2]);
                            $sysLicenseList['Success'][$licenseName]['Available']   = strval($value->col[3]);
                            $count = $count + 1;
                        }
                }
                
                return $sysLicenseList;
                
	}
        
        function getSystemSubscriberServiceLicenseList(){          
		
                global $sessionid, $client;
		$sysLicenseList["Error"]   = "";
		$sysLicenseList["Success"] = array();
		$xmlinput = xmlHeader($sessionid, "SystemLicensingGetRequest14sp3");
		$xmlinput .= xmlFooter();
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                //echo "<pre>XML - ";
                //print_r($xml);		
                if (readErrorXmlGenuine($xml) != "") 
                {
			$sysLicenseList["Error"] = $xml->command->summaryEnglish;
		}
                else
                { 
                        $grpAuthorizeServiceList = array();
                        $count = 0;
                        $sysLicenseList['Success']['trunckGroupUser']    = strval($xml->command->numberOfTrunkUsers);
			foreach ($xml->command->subscriberLicenseTable->row as $key => $value)
                        {
                            
                            $licenseName = strval($value->col[0]);
                            $sysLicenseList['Success'][$licenseName]['Licensed']    = strval($value->col[1]);
                            $sysLicenseList['Success'][$licenseName]['Used']        = strval($value->col[2]);
                            $sysLicenseList['Success'][$licenseName]['Available']   = strval($value->col[3]);
                            $count = $count + 1;
                        }
                }
                
                return $sysLicenseList;
                
	}
        
        
        function retriveLicenseErrorAgainstServiceName($serviceName){
            
            $userServiceLicenseArr   = $this->getSystemUserServiceLicenseList();
            $userServiceLicenseList  = $userServiceLicenseArr['Success'];            
            $allServicesLicenseArr   = array(
                                                    "Anonymous Call Rejection"                          => array("3STANDARD"),
                                                    "Authentication"                                    => array("1BASE"),
                                                    "Call Forwarding Always"                            => array("2RESSTANDARD"),
                                                    "Call Forwarding Busy"                              => array("2RESSTANDARD"),
                                                    "Call Forwarding No Answer"                         => array("2RESSTANDARD"),
                                                    "Call Notify"                                       => array("Call Notify"),
                                                    "Calling Line ID Delivery Blocking"                 => array("1BASE"),
                                                    "CommPilot Express"                                 => array("3CONSUMERMOBILITY"),
                                                    "CommPilot Call Manager"                            => array("4SOHO"),
                                                    "Do Not Disturb"                                    => array("3STANDARD"),
                                                    "Intercept User"                                    => array("1BASE"),
                                                    "Last Number Redial"                                => array("2RESSTANDARD"),
                                                    "Outlook Integration"                               => array("Outlook Integration"),
                                                    "Priority Alert"                                    => array("5RESSTANDARD"),
                                                    "Call Return"                                       => array("2RESSTANDARD"),
                                                    "Remote Office"                                     => array("4SOHO"),
                                                    "Selective Call Acceptance"                         => array("5RESSTANDARD"),
                                                    "Call Forwarding Selective"                         => array("5RESSTANDARD"),
                                                    "Selective Call Rejection"                          => array("5RESSTANDARD"),
                                                    "Service Scripts User"                              => array("1BASE"),
                                                    "Simultaneous Ring Personal"                        => array("3CONSUMERMOBILITY"),
                                                    "Alternate Numbers"                                 => array("5SOHO"),                                                    
                                                    "Speed Dial 8"                                      => array("3STANDARD"),
                                                    "Customer Originated Trace"                         => array("1BASE"),
                                                    "Attendant Console"                                 => array("AttendantConsole"),
                                                    "Third-Party MWI Control"                           => array("1BASE"),
                                                    "Client Call Control"                               => array("1BASE"),
                                                    "Shared Call Appearance"                            => array("4SOHO"),
                                                    "Shared Call Appearance 10"                         => array("5PREMIUM"),
                                                    "Shared Call Appearance 15"                         => array("5PREMIUM"),
                                                    "Shared Call Appearance 20"                         => array("5PREMIUM"),
                                                    "Shared Call Appearance 25"                         => array("5PREMIUM"),
                                                    "Shared Call Appearance 30"                         => array("5PREMIUM"),
                                                    "Shared Call Appearance 35"                         => array("5PREMIUM"),                                                    
                                                    "Voice Messaging User"                              => array("Messaging"),
                                                    "Calling Name Retrieval"                            => array("1BASE"),
                                                    "Flash Call Hold"                                   => array("1RESBASIC"),
                                                    "Speed Dial 100"                                    => array("3STANDARD"),
                                                    "Directed Call Pickup"                              => array("5PREMIUM"),
                                                    "Third-Party Voice Mail Support"                    => array("1BASE"),
                                                    "Directed Call Pickup with Barge-in"                => array("5PREMIUM"),
                                                    "Voice Portal Calling"                              => array("3CONSUMERMOBILITY"),
                                                    "External Calling Line ID Delivery"                 => array("1BASE"),
                                                    "Internal Calling Line ID Delivery"                 => array("1BASE"),
                                                    "Automatic Callback"                                => array("3STANDARD"),
                                                    "Call Waiting"                                      => array("1BASE"),
                                                    "Calling Line ID Blocking Override"                 => array("Calling Line ID Blocking Override"),
                                                    "Calling Party Category"                            => array("1BASE"),
                                                    "Barge-in Exempt"                                   => array("1BASE"),
                                                    "SMDI Message Desk"                                 => array("1BASE"),
                                                    "Video Add-On"                                      => array("Video Add-On"),
                                                    "Malicious Call Trace"                              => array("1BASE"),
                                                    "Preferred Carrier User"                            => array("1BASE"),
                                                    "Push to Talk"                                      => array("5PREMIUM"),
                                                    "Basic Call Logs"                                   => array("1BASE"),
                                                    "Enhanced Call Logs"                                => array("Enhanced Call Logs"),
                                                    "Hoteling Host"                                     => array("5PREMIUM"),
                                                    "Hoteling Guest"                                    => array("3STDENT"),
                                                    "Voice Messaging User - Video"                      => array("Messaging"),
                                                    "Diversion Inhibitor"                               => array("3STANDARD"),
                                                    "Multiple Call Arrangement"                         => array("4SOHO"),
                                                    "Custom Ringback User"                              => array("5SOHO"),
                                                    "Custom Ringback User - Video"                      => array("5SOHO"),
                                                    "Automatic Hold/Retrieve"                           => array("Automatic Hold/Retrieve"),
                                                    "Busy Lamp Field"                                   => array("5PREMIUM"),
                                                    "Three-Way Call"                                    => array("1RESBASIC"),
                                                    "Call Transfer"                                     => array("1RESBASIC"),
                                                    "Privacy"                                           => array("1BASE"),
                                                    "Fax Messaging"                                     => array("FaxMessaging"),
                                                    "Physical Location"                                 => array("1BASE"),
                                                    "Charge Number"                                     => array("1BASE"),
                                                    "BroadWorks Supervisor"                             => array("CallCenterSupervisor"),
                                                    "BroadWorks Agent"                                  => array("CallCenterAgent"),
                                                    "N-Way Call"                                        => array("5PREMIUM"),
                                                    "Directory Number Hunting"                          => array("2BUSINESSLINE"),
                                                    "Two-Stage Dialing"                                 => array("3CONSUMERMOBILITY"),
                                                    "Call Forwarding Not Reachable"                     => array("2RESSTANDARD"),
                                                    "MWI Delivery to Mobile Endpoint"                   => array("Messaging"),
                                                    "BroadWorks Receptionist - Small Business"          => array("BroadWorks Receptionist - Small Business"),
                                                    "BroadWorks Receptionist - Office"                  => array("BroadWorks Receptionist - Office"),
                                                    "External Custom Ringback"                          => array("5SOHO"),
                                                    "In-Call Service Activation"                        => array("3CONSUMERMOBILITY"),
                                                    "Connected Line Identification Presentation"        => array("1BASE"),
                                                    "Connected Line Identification Restriction"         => array("1BASE"),
                                                    "BroadWorks Anywhere"                               => array("3CONSUMERMOBILITY"),
                                                    "Zone Calling Restrictions"                         => array("1BASE"),
                                                    "Polycom Phone Services"                            => array("1BASE"),
                                                    "Custom Ringback User - Call Waiting"               => array("5SOHO"),
                                                    "Music On Hold User"                                => array("5PREMIUM"),
                                                    "Video On Hold User"                                => array("5PREMIUM"),
                                                    "Prepaid"                                           => array("Prepaid"),
                                                    "Call Center - Basic"                               => array("Call Center - Basic"),
                                                    "Call Center - Standard"                            => array("Call Center - Standard"),
                                                    "Call Center - Premium"                             => array("Call Center - Premium"),
                                                    "Communication Barring User-Control"                => array("2RESSTANDARD"),
                                                    "Classmark"                                         => array("1BASE"),
                                                    "Calling Name Delivery"                             => array("1BASE"),
                                                    "Calling Number Delivery"                           => array("1BASE"),
                                                    "Virtual On-Net Enterprise Extensions"              => array("5PREMIUM"),
                                                    "Pre-alerting Announcement"                         => array("5SOHO"),
                                                    "Call Center Monitoring"                            => array("5PREMIUM"),
                                                    "Location-Based Calling Restrictions"               => array("3CONSUMERMOBILITY"),
                                                    "BroadWorks Mobility"                               => array("BroadWorks Mobility"),
                                                    "Call Me Now"                                       => array("3STANDARD"),
                                                    "Call Recording"                                    => array("Call Recording"),
                                                    "Integrated IMP"                                    => array("Integrated IMP"),
                                                    "Group Night Forwarding"                            => array("3STDENT"),
                                                    "BroadTouch Business Communicator Desktop"          => array("BroadTouch Business Communicator Desktop"),
                                                    "BroadTouch Business Communicator Desktop - Audio"  => array("BroadTouch Business Communicator Desktop - Audio"),
                                                    "BroadTouch Business Communicator Mobile"           => array("BroadTouch Business Communicator Mobile"),
                                                    "BroadTouch Business Communicator Mobile - Audio"   => array("BroadTouch Business Communicator Mobile - Audio"),
                                                    "Client License 3"                                  => array("Client License 3"),
                                                    "Client License 4"                                  => array("BroadWorks Receptionist - Enterprise"),
                                                    "Client License 17"                                 => array("BroadTouch Business Communicator Mobile - Video"),
                                                    "Client License 18"                                 => array("BroadTouch Business Communicator Desktop - Video"),
                                                    "Client License 19"                                 => array("BroadTouch MobileLink"),
                                                    "Sequential Ring"                                   => array("3CONSUMERMOBILITY")
                                     );
              
             $serviceLicenseArr      = array();
             $serviceLicenseErrorMsg = "";
             $licenseDetailsArr      = array();
             $violatedLicenseArr     = array();
             $limitExceed            = "No";
             if(isset($allServicesLicenseArr[$serviceName])){
                  $serviceLicenseArr =  $allServicesLicenseArr[$serviceName]; 
             }
             
             foreach($serviceLicenseArr as $licenseKey => $licenseName){
                 
                    if(isset($userServiceLicenseList[$licenseName])){
                            $licenseDetailsArr =  $userServiceLicenseList[$licenseName];                            
                            if($licenseDetailsArr['Licensed']!="Unlimited" && $licenseDetailsArr['Used'] > $licenseDetailsArr['Licensed']){
                                 $limitExceed = "Yes";
                                 $violatedLicenseArr[] = $licenseName;
                            }
                            if($licenseDetailsArr['Licensed']!="Unlimited" && $licenseDetailsArr['Used'] < $licenseDetailsArr['Licensed']){
                                 $limitExceed = "No";
                                 break;
                            }
                            if($licenseDetailsArr['Licensed']=="Unlimited"){
                                 $limitExceed = "No";
                                 break;
                            }
                    }
             }
             
             if($limitExceed=="Yes"){
                 $licenseNames = implode(", ", $violatedLicenseArr);
                 if(count($violatedLicenseArr)==1){
                    $serviceLicenseErrorMsg = "Service license ($licenseNames) is violated.";
                 }
                 if(count($violatedLicenseArr)>1){
                    $serviceLicenseErrorMsg = "Service licenses ($licenseNames) are violated.";
                 }                 
             }       
             return $serviceLicenseErrorMsg;
        }
        
        
        //Code added @ 03 June 2019 regarding EX-1345
            function getTotalServicesListOfServicePacks($servicePackName, $serviceNameList) {
            global $sessionid, $client;

            $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
            $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]). "</serviceProviderId>";
            $xmlinput .= "<servicePackName>" . $servicePackName . "</servicePackName>";
            $xmlinput .= xmlFooter();

            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

            foreach ($xml->command->userServiceTable->row as $key => $value){
                $serviceNameList[] = strval($value->col[0]);
            }

            return $serviceNameList;
          }
        //End code
    
          public  function userCallingLineIDDeliveryBlockingModifyRequest($userId, $isActive) {
              global $sessionid, $client;
              $responseData["Error"] = "";
              $responseData["Success"] = "";
              
              $xmlinput = xmlHeader($sessionid, "UserCallingLineIDDeliveryBlockingModifyRequest");
              $xmlinput .= "<userId>" . htmlspecialchars($userId). "</userId>";
              $xmlinput .= "<isActive>" . $isActive . "</isActive>";
              $xmlinput .= xmlFooter();
              
              $response = $client->processOCIMessage(array("in0" => $xmlinput));
              $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
              
              if (readErrorXmlGenuine($xml) != "")
              {
                  $responseData["Error"] = strval($xml->command->summaryEnglish);
              } else {
                  $responseData["Success"] = "Success";
              }
              
              return $responseData;
          }
          
          public function userCallingLineIDDeliveryBlockingGetRequest($userId) {
              global $sessionid, $client;
              
              $responseData["Error"] = "";
              $responseData["Success"] = "";
              
              $xmlinput = xmlHeader($sessionid, "UserCallingLineIDDeliveryBlockingGetRequest");
              $xmlinput .= "<userId>" . htmlspecialchars($userId). "</userId>";
              $xmlinput .= xmlFooter();
              
              $response = $client->processOCIMessage(array("in0" => $xmlinput));
              $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
              
              if (readErrorXmlGenuine($xml) != "")
              {
                  $responseData["Error"] = strval($xml->command->summaryEnglish);
              } else {
                  $responseData["Success"]['isActive'] = strval($xml->command->isActive);
              }
              
              return $responseData;
          }
    

        //Code added @ 05 April 2019 regarding Express License Feature
        function getServicesListOfServicePackBasedOnEnterprise($servicePackName, $serviceProvider) {
        global $sessionid, $client;
        $serviceNameList = array();

        $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($serviceProvider). "</serviceProviderId>";
        $xmlinput .= "<servicePackName>" . $servicePackName . "</servicePackName>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        foreach ($xml->command->userServiceTable->row as $key => $value){
            $serviceNameList[] = strval($value->col[0]);
        }

        return $serviceNameList;
      }
        //End code

          public function hasCLIDBlocKingService($groupId) {
              $showCLIDBloc = false;
              $assignServiceList = array_unique( $this->getAllGroupAssignedServices($groupId) );
              
              if( ! empty($assignServiceList) ) {
                  if(in_array("Calling Line ID Delivery Blocking", $assignServiceList))
                  {
                      $showCLIDBloc = true;
                  }
              }
              
              return $showCLIDBloc;
          }
    
          public function getVMServicePack($servicePacks, $sp) {
              global $sessionid, $client;
              
              $vmServicePack = array();
              $i = 0;
              foreach ($servicePacks as $servicePack) {
                  $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
                  $xmlinput .= "<serviceProviderId>" . htmlspecialchars($sp). "</serviceProviderId>";
                  $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
                  $xmlinput .= xmlFooter();
                  
                  $response = $client->processOCIMessage(array("in0" => $xmlinput));
                  $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
                  
                  foreach ($xml->command->userServiceTable->row as $key => $value){
                      if (strval($value->col[0]) == "Voice Messaging User") {
                          $vmServicePack[$i++] = $servicePack;
                      }
                  }
              }
              
              return $vmServicePack;
          }
          
          public function getGroupServicePackAndUserServices($spId, $groupId) {
              global $sessionid, $client;
              
              $responseData["Error"] = "";
              $responseData["Success"] = "";
              
              $servicePacks = array();
              $userServices = array();
              $xmlinput = xmlHeader($sessionid, "GroupServiceGetAuthorizationListRequest");
              $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId) . "</serviceProviderId>";
              $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
              $xmlinput .= xmlFooter();
              $response = $client->processOCIMessage(array("in0" => $xmlinput));
              $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
              
              if (readErrorXmlGenuine($xml) != "")
              {
                      $responseData["Error"] = strval($xml->command->summaryEnglish);
              } else {
                      $a = 0;
                      foreach ($xml->command->servicePacksAuthorizationTable->row as $key => $value)
                      {
                          if ($value->col[1] == "true")
                          {
                              $servicePacks[$a] = strval($value->col[0]);
                              $a++;
                          }
                      }
                      
                      if (isset($servicePacks))
                      {
                          $servicePacks = subval_sort($servicePacks);
                      }
                      
                      $a = 0;
                      foreach ($xml->command->userServicesAuthorizationTable->row as $key => $value)
                      {
                          if ($value->col[1] == "true")
                          {
                              $userServices[$a] = strval($value->col[0]);
                              $a++;
                          }
                      }
                      
                      $responseData["Success"]["servicePacks"] = $servicePacks;
                      $responseData["Success"]["userServices"] = $userServices;
              }
              return $responseData;
          }
                    
}
