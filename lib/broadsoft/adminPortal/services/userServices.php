<?php

class userServices{
	
	function getUserServicesAll($userId, $spId){
		
		global $sessionid, $client;
		$services["Success"] = array();
		$services["Error"] = array();
		$userServices = array();
		$userServices["Services"]["assigned"] = array();
		$userServices["Services"]["unassigned"] = array();
		$userServices["ServicePack"]["assigned"] = array();
		$userServices["ServicePack"]["unassigned"] = array();
		$userServices["AssignedServicePackServices"] = array();
		$userServices["TotalServicePackServices"] = array();
		$userServices["TotalServices"] = array();
		
		$xmlinput = xmlHeader($sessionid, "UserServiceGetAssignmentListRequest");
		$xmlinput .= "<userId>" . $userId . "</userId>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

		//echo "<pre>"; print_r($xml); die;
		if(empty($xml->command->summary)){
			foreach($xml->command->userServicesAssignmentTable->row as $key => $value){
				if($value->col[1] == "true"){
					$userServices["Services"]["assigned"][] = strval($value->col[0]);
				}
				if($value->col[1] == "false"){
					$userServices["Services"]["unassigned"][] = strval($value->col[0]);
				}
			}
			
			foreach($xml->command->servicePacksAssignmentTable->row as $key1 => $value1){
				if($value1->col[1] == "true"){
					$userServices["ServicePack"]["assigned"][] = strval($value1->col[0]);
				}
				if($value1->col[1] == "false"){
					$userServices["ServicePack"]["unassigned"][] = strval($value1->col[0]);
				}
			}
			
			$servicepackServices = array();
			foreach($xml->command->servicePacksAssignmentTable->row as $key2 => $value2){
				if($value2->col[1] == "true"){
					$servicePackName = strval($value2->col[0]);
					$servicepackServices = $this->getServicePackServices($servicePackName, $spId);
					foreach ($servicepackServices as $srv) {
						if (! in_array($srv, $userServices)) {
							$userServices["AssignedServicePackServices"][$servicePackName][] = $srv;
							$userServices["TotalServicePackServices"][] = $srv;
						}
					}
				}
			}
			$userServices["TotalServices"] = array_merge($userServices["TotalServicePackServices"], $userServices["Services"]["assigned"]);
			
			$services["Success"] = $userServices;
		}else{
			$services["Error"]["summary"] = strval($xml->command->summary); 
			$services["Error"]["summaryEnglish"] = strval($xml->command->summaryEnglish); 
		}
		//echo "<pre>"; print_r($userServices); die;
		return $services;
	}
	function getServicePackServices($ServicePackName, $spId){
		global $sessionid, $client;
		$xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
		$xmlinput .= "<serviceProviderId>" .htmlspecialchars($spId). "</serviceProviderId>";
		$xmlinput .= "<servicePackName>" . $ServicePackName. "</servicePackName>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml);
		$userServices = array();
		foreach ($xml->command->userServiceTable->row as $key => $value){
			$userServices[] = strval($value->col[0]);
		}
		return $userServices;
	}
        
        //code added @ 30 Aug 2018
        public function getServicePacksListHaveScaServices($servicePacks)
        {
            global $sessionid, $client;
            $scaServicePackArray = array();
            $count = 0;
            foreach ($servicePacks as $key => $servicePack) 
            {
                $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']). "</serviceProviderId>";
                $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
                $xmlinput .= xmlFooter();

                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);                
                foreach ($xml->command->userServiceTable->row as $key => $value)
                {
                    $pos = strpos(strval($value->col[0]), "Shared Call Appearance");
                    if($pos === false)
                    {
                        //continue;
                    }
                    else
                    {                        
                        $scaServicePackArray[$count] = $servicePack;
			$count++;
                    }
                }
            }
            return array_unique($scaServicePackArray);
        }
        
        
        public function getServicePacksHasOneScaServiceOnly($servicePacks)
        {
            global $sessionid, $client;
            //$scaServicePackArray = array();
            $count = 0;
            //$oneServicePack = "false";
            foreach ($servicePacks as $key => $servicePack) 
            {
                $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
                $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']). "</serviceProviderId>";
                $xmlinput .= "<servicePackName>" . $servicePack . "</servicePackName>";
                $xmlinput .= xmlFooter();

                $response = $client->processOCIMessage(array("in0" => $xmlinput));
                $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);                
                if(count($xml->command->userServiceTable->row)==1)
                {                    
                    $scaServicePackArray[$servicePack]['hasOneSCAServiceOnly'] = "true";
                }
                if(count($xml->command->userServiceTable->row)>1)
                {                    
                    $scaServicePackArray[$servicePack]['hasMultipleSCAServiceOnly'] = "true";
                }
                $count = $count+1;
            }
            return $scaServicePackArray;
        }
        //End code

	
}