<?php
    /**
     * Get the list of all user services assigned to a user.
     * Function returns list of BroadSoft User Services or NULL if back-end error
     *
     * Created:  Karl
     * Date: 5/1/2016
     * Time: 7:54 PM
     */
    function getUserAssignedUserServices($userId)
    {
        global $sessionid, $client;
        $userAssignedUserServices = array();

        $xmlInput = xmlHeader($sessionid, "UserAssignedServicesGetListRequest");
        $xmlInput .= "<userId>" . $userId . "</userId>";
        $xmlInput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlInput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        if (readError($xml) != "") {
            return null;
        }

        foreach ($xml->command->userServiceEntry as $key => $value) {
            $userAssignedUserServices[] = strval($value->serviceName);
        }
        return $userAssignedUserServices;
    }
?>

