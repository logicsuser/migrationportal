<?php

/**
 * Created by Sologics.
 * Date: 17/07/2017
 */

class domains
{
	//get domain list for service provider
	public function getServiceProviderDomainListResponse(){
		global  $sessionid, $client;
		
		$getDomainResponse["Error"] = "";
		$getDomainResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "ServiceProviderDomainGetAssignedListRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		if (readErrorXmlGenuine($xml) != "") {
			$getDomainResponse["Error"] = strval($xml->command->summaryEnglish);
		}else{
			$getDomainResponse["Success"]['serviceProviderDefaultDomain'] = strval($xml->command->serviceProviderDefaultDomain);
			$a = 0;
			foreach ($xml->command->domain as $key => $value)
			{
				$domain[$a] = strval($value);
				$a++;
			}
			$getDomainResponse["Success"]['domain'] = $domain;			
		}
		return $getDomainResponse;
	}	
	
	//get domain list for service provider
	public function getServiceProviderDomainListRes($spId){
	    global  $sessionid, $client;
	    
	    $getDomainResponse["Error"] = "";
	    $getDomainResponse["Success"] = "";
	    
	    $xmlinput = xmlHeader($sessionid, "ServiceProviderDomainGetAssignedListRequest");
	    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId) . "</serviceProviderId>";
	    $xmlinput .= xmlFooter();
	    
	    $response = $client->processOCIMessage(array("in0" => $xmlinput));
	    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
	    if (readErrorXmlGenuine($xml) != "") {
	        $getDomainResponse["Error"] = strval($xml->command->summaryEnglish);
	    }else{
	        $getDomainResponse["Success"]['serviceProviderDefaultDomain'] = strval($xml->command->serviceProviderDefaultDomain);
	        $a = 0;
	        foreach ($xml->command->domain as $key => $value)
	        {
	            $domain[$a] = strval($value);
	            $a++;
	        }
	        $getDomainResponse["Success"]['domain'] = $domain;
	    }
	    return $getDomainResponse;
	}
	
        
	//function to get post data and filter the list for assign and unassign, replacement for modifyAssignDomain
	public function getUpdatedDomainsList(){
		
	}
}