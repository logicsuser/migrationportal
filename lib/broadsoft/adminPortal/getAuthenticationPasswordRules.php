<?php
require_once("/var/www/lib/broadsoft/login.php");
checkLogin();

$authenticationPasswordMinLength = "1";

if ($ociVersion != "17") {
    $xmlinput = xmlHeader($sessionid, "ServiceProviderSIPAuthenticationPasswordRulesGetRequest");
    $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
    $xmlinput .= xmlFooter();
    $response = $client->processOCIMessage(array("in0" => $xmlinput));
    $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

    $authenticationPasswordMinLength = strval($xml->command->minLength);
}
?>
