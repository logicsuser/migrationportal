<?php 

class UserOperations
{
    const ExpressCustomProfileName = "%Express_Custom_Profile_Name%";
    public $department;
    
    function getUserDetail($userId)
    {
        global $sessionid, $client;
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "UserGetRequest19");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        }
        else
        {
            foreach ($xml->command->attributes() as $a => $b)
            {
                if ($b == "Error")
                {
                    $userInfo["error"] = "true";
                }
            }
            
            $userInfo["serviceProviderId"] = strval($xml->command->serviceProviderId);
            $userInfo["groupId"] = strval($xml->command->groupId);
            $userInfo["deviceName"] = isset($xml->command->accessDeviceEndpoint->accessDevice->deviceName) ? strval($xml->command->accessDeviceEndpoint->accessDevice->deviceName) : "";
            $userInfo["userId"] = $userId;
            $userInfo["lastName"] = strval($xml->command->lastName);
            $userInfo["firstName"] = strval($xml->command->firstName);
            $userInfo["callingLineIdPhoneNumber"] = strval($xml->command->callingLineIdPhoneNumber);
            $userInfo["extension"] = strval($xml->command->extension);
            $userDevice = $this->getUserDeviceDetail($userInfo);
            if(count($userDevice['Success']) > 0)
            {
                $userInfo["deviceType"] =$userDevice['Success'];
            }
            $returnResponse["Success"] = $userInfo;
        }
        //print_r($returnResponse); exit;
        return $returnResponse;
    }
    
    
    
    function getUserDeviceDetail($userInfo)
    {
        global $sessionid, $client;
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceGetRequest18sp1");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($userInfo["serviceProviderId"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($userInfo["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $userInfo["deviceName"] . "</deviceName>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        }
        else
        {
            $returnResponse["Success"] = strval($xml->command->deviceType);
        }
        return $returnResponse;
    }
   
    
    function userListOfGroup($providerId, $groupId){
        global $sessionid, $client;
        $returndata = array();
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "UserGetListInGroupRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($providerId) . "</serviceProviderId>";
        $xmlinput .= "<GroupId>" . htmlspecialchars($groupId) . "</GroupId>";
        if (isset($this->department) && strlen($this->department) > 0)
        {
        	$xmlinput .= "<searchCriteriaExactUserDepartment><departmentKey xsi:type=\"GroupDepartmentKey\">";
        	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($providerId) . "</serviceProviderId>";
        	$xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
        	$xmlinput .= "<name>" . htmlspecialchars($this->department) . "</name>";
        	$xmlinput .= "</departmentKey></searchCriteriaExactUserDepartment>";
        }
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        }
        else
        {
            foreach ($xml->command->userTable->row as $key => $value)
            {
                $returndata[] = $value;
            }
            $returnResponse["Success"] = $returndata;
        }
        return $returnResponse;
    }
    
    function removeElementWithValue($listArray){
    	foreach($listArray["Success"] as $subKey => $subArray){
    		$userId = strval($subArray->col[0]);
    		$deviceName = "";
    		global $sessionid, $client;
    		$xmlinput = xmlHeader($sessionid, "UserGetRequest19");
    		$xmlinput .= "<userId>" . $userId . "</userId>";
    		$xmlinput .= xmlFooter();
    		$response = $client->processOCIMessage(array("in0" => $xmlinput));
    		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
    		
    		if(isset($xml->command->accessDeviceEndpoint->accessDevice->deviceName)){
    			$deviceName = strval($xml->command->accessDeviceEndpoint->accessDevice->deviceName);
    		}
    		//Remove user from Array if has no device
    		if($deviceName==""){
    			unset($listArray["Success"][$subKey]);
    		}
    	}
    	return $listArray;
    }
    
    function find_user_in_csv($filename, $userId) {
    	$f = fopen($filename, "r");
    	$result = false;
    	while ($row = fgetcsv($f)) {
    		if ($row[3] == $userId) {
    			$result = $row[3];
    			break;
    		}
    	}
    	fclose($f);
    	return $result;
    }
    
    
    function groupAccessDeviceDeleteCustomTag($deviceName, $tagName)
    {
        global $sessionid, $client;
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceCustomTagDeleteListRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>". $deviceName."</deviceName>";
        $xmlinput .= "<tagName>". $tagName ."</tagName>";
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        }
        else
        {
            $returnResponse["Success"] = $xml;
        }
        return $returnResponse;
    }
    
    function addDeviceCustomTag($deviceName, $tagName, $tagValue)
    {
        global $sessionid, $client;
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput  = xmlHeader($sessionid, "GroupAccessDeviceCustomTagAddRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= "<tagName>" . $tagName . "</tagName>";
        $xmlinput .= "<tagValue>" . $tagValue . "</tagValue>";
        
        $xmlinput .= xmlFooter();
        
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        }
        else
        {
            $returnResponse["Success"] = $xml;
        }
        
        return $returnResponse;
    }
    
    function getDeviceListByUserId($userId){
        global $sessionid, $client;
        $deviceList["Error"]="";
        $deviceList["Success"]="";
        $xmlinput = xmlHeader($sessionid, "UserSharedCallAppearanceGetRequest16sp2");
        $xmlinput .= "<userId>".$userId."</userId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $deviceList["Error"] = strval($xml->command->detail);
        }else{
            $deviceList["Success"]["alertAllAppearancesForClickToDialCalls"] = strval($xml->command->alertAllAppearancesForClickToDialCalls);
            $deviceList["Success"]["alertAllAppearancesForGroupPagingCalls"] = strval($xml->command->alertAllAppearancesForGroupPagingCalls);
            $deviceList["Success"]["maxAppearances"] = strval($xml->command->maxAppearances);
            $deviceList["Success"]["allowSCACallRetrieve"] = strval($xml->command->allowSCACallRetrieve);
            $deviceList["Success"]["enableMultipleCallArrangement"] = strval($xml->command->enableMultipleCallArrangement);
            $deviceList["Success"]["multipleCallArrangementIsActive"] = strval($xml->command->multipleCallArrangementIsActive);
            if(isset($xml->command->endpointTable)){
                $i = 0;
                foreach($xml->command->endpointTable->row as $key => $val){
                    $deviceList["Success"]["deviceList"][$i]["label"] = strval($val->col[0]);
                    $deviceList["Success"]["deviceList"][$i]["deviceName"] = strval($val->col[1]);
                    $deviceList["Success"]["deviceList"][$i]["deviceType"] = strval($val->col[2]);
                    $deviceList["Success"]["deviceList"][$i]["linePort"] = strval($val->col[3]);
                    $deviceList["Success"]["deviceList"][$i]["SIPContact"] = strval($val->col[4]);
                    $deviceList["Success"]["deviceList"][$i]["port"] = strval($val->col[5]);
                    $i++;
                }
            }
        }
        return $deviceList;
    }
    
    public function modifyUserDetails($userArray){
        global $sessionid, $client;
        $userResp["Error"]="";
        $userResp["Success"]="";
        $xmlinput = xmlHeader($sessionid, "UserModifyRequest17sp4");
        $xmlinput .= "<userId>".$userArray["userId"]."</userId>";
        if(isset($userArray["emailAddress"]) && $userArray["emailAddress"] !=""){
            $xmlinput .= "<emailAddress>".$userArray["emailAddress"]."</emailAddress>";
        }
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $userResp["Error"] = strval($xml->command->detail);
        }else{
            $userResp["Success"] = "Success";
        }
        return $userResp;
    }
    
    public function getGroupBlfUsersList($userId){
        global  $sessionid, $client;
        
        $resp["Error"] = "";
        $resp["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "UserBusyLampFieldGetAvailableUserListRequest");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= "<searchCriteriaExactUserGroup>";
        $xmlinput .=    "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .=    "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
        $xmlinput .= "</searchCriteriaExactUserGroup>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $resp["Error"] = $xml->command->summaryEnglish;
        }else{
            $a = 0;
            if (isset($xml->command->userTable->row))
            {
                foreach ($xml->command->userTable->row as $key => $value)
                {
                    if ((isset($_SESSION["userInfo"]["monitoredUsers"]) and !searchArray($_SESSION["userInfo"]["monitoredUsers"], "id", strval($value->col[0]))) or !isset($_SESSION["userInfo"]["monitoredUsers"]))
                    {
                        $blfUsers[$a]["id"] = strval($value->col[0]);
                        $blfUsers[$a]["name"] = strval($value->col[1]) . ", " . strval($value->col[2]);
                        if (strval($value->col[5])){
                            $blfUsers[$a]["tempId"] = substr(strval($value->col[5]), 3) . "x" . strval($value->col[6]);
                        }else{
                            $blfUsers[$a]["tempId"] = strval($value->col[0]);
                        }
                        $a++;
                    }
                }
            }
            $blfUsers = subval_sort($blfUsers, "name");
            $resp["Success"] = $blfUsers;
        }
        return $resp;
    }
    
    public function getEnterpriseBlfUsersList(){
        global  $sessionid, $client;
        
        $resp["Error"] = "";
        $resp["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "UserGetListInServiceProviderRequest");
        $xmlinput .= "<serviceProviderId>" .  htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine($xml) != "") {
            $resp["Error"] = $xml->command->summaryEnglish;
        }else{
            $a = 0;
            if (isset($xml->command->userTable->row))
            {
                foreach ($xml->command->userTable->row as $key => $value)
                {
                    if ((isset($_SESSION["userInfo"]["monitoredUsers"]) and !searchArray($_SESSION["userInfo"]["monitoredUsers"], "id", strval($value->col[0]))) or !isset($_SESSION["userInfo"]["monitoredUsers"]))
                    {
                        $blfUsers[$a]["id"] = strval($value->col[0]);
                        $blfUsers[$a]["name"] = strval($value->col[2]) . ", " . strval($value->col[3]);
                        if (strval($value->col[5])){
                            $blfUsers[$a]["tempId"] = substr(strval($value->col[5]), 3) . "x" . strval($value->col[11]);
                        }else{
                            $blfUsers[$a]["tempId"] = strval($value->col[0]);
                        }
                        $a++;
                    }
                }
            }
            $blfUsers = subval_sort($blfUsers, "name");
            $resp["Success"] = $blfUsers;
        }
        return $resp;
    }
    
    function checkUserHasDevice($userId)
    {
        global $sessionid, $client;
        $returnResponse["Error"] = "";
        $returnResponse["Success"] = "";
        
        $xmlinput = xmlHeader($sessionid, "UserGetRequest21sp1");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        $errMsg = readErrorXmlGenuine($xml);
        if($errMsg != "") {
            $returnResponse["Error"] = $errMsg;
        }
        else
        {
            $returnResponse["Success"]["deviceName"] = isset($xml->command->accessDeviceEndpoint->accessDevice->deviceName) ? strval($xml->command->accessDeviceEndpoint->accessDevice->deviceName) : "";
        }
        //print_r($returnResponse); exit;
        return $returnResponse;
    }
    
    public function groupCPEConfigReorderDeviceLinePortsRequest($linePorts, $deviceName, $spId, $groupId) {
        
        global $sessionid, $client;
        $responseData["Error"]="";
        $responseData["Success"]="";
        $xmlinput = xmlHeader($sessionid, "GroupCPEConfigReorderDeviceLinePortsRequest");
        
        $xmlinput .= "<serviceProviderId>" . $spId . "</serviceProviderId>";
        $xmlinput .= "<groupId>". $groupId ."</groupId>";
        $xmlinput .= "<deviceName>". $deviceName ."</deviceName>";
        
        foreach ($linePorts as $lineKey => $linePorts) {
            $xmlinput .= "<orderedLinePortList>" . $linePorts . "</orderedLinePortList>";
        }
        
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $responseData["Error"] = strval($xml->command->detail);
        }else{
            $responseData["Success"] = "Success";
        }
        return $responseData;
    }
    
    public function groupAccessDeviceModifyUserRequest($spId, $groupId, $deviceName, $linePort) {
        global $sessionid, $client;
        $responseData["Error"]="";
        $responseData["Success"]="";
        
        $xmlinput = xmlHeader($sessionid, "GroupAccessDeviceModifyUserRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($spId) . "</serviceProviderId>";
        $xmlinput .= "<groupId>" . htmlspecialchars($groupId) . "</groupId>";
        $xmlinput .= "<deviceName>" . $deviceName . "</deviceName>";
        $xmlinput .= "<linePort>" . $linePort . "</linePort>";
        $xmlinput .= "<isPrimaryLinePort>true</isPrimaryLinePort>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        
        if (readErrorXmlGenuine($xml) != "") {
            $responseData["Error"] = strval($xml->command->detail);
        }else{
            $responseData["Success"] = "Success";
        }
        
        return $responseData;
    }
    
}




?>
