<?php
	$xmlinput = xmlHeader($sessionid, "GroupServiceGetAuthorizationListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

	$a = 0;
	foreach ($xml->command->servicePacksAuthorizationTable->row as $key => $value)
	{
		if ($value->col[1] == "true")
		{
			$servicePacks[$a] = strval($value->col[0]);
			$a++;
		}
	}

	if (isset($servicePacks))
	{
		$servicePacks = subval_sort($servicePacks);
	}

	$a = 0;
	foreach ($xml->command->userServicesAuthorizationTable->row as $key => $value)
	{
		if ($value->col[1] == "true")
		{
			$userServices[$a] = strval($value->col[0]);
			$a++;
		}
	}

?>
