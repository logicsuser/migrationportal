<?php

/**
 * Created by Sologics.
 * Date: 11/07/2017
 */

class voicePortal
{
	
	//add DN's request in service provider
	public function getVoicePortalRequest($postArray){
		global  $sessionid, $client;
		
		$getVoicePortalResponse["Error"] = "";
		$getVoicePortalResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupVoiceMessagingGroupGetVoicePortalRequest17sp4");
		$xmlinput .= "<serviceProviderId>". htmlspecialchars($postArray['serviceProviderId']) ."</serviceProviderId>";
		$xmlinput .= "<groupId>". htmlspecialchars($postArray['groupId']) ."</groupId>";
		
		$xmlinput .= xmlFooter();
		//echo $xmlinput; die;
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		
		if (readErrorXmlGenuine($xml) != "") {
			$getVoicePortalResponse["Error"] = strval($xml->command->summaryEnglish);
		}else{
			$getVoicePortalResponse["Success"] = "Success";
			
		}
		return $getVoicePortalResponse;
	}
	
	//add DN's request in service provider
	public function setVoicePortalModifyRequest($postArray){
		global  $sessionid, $client;
		
		$setVpResponse["Error"] = "";
		$setVpResponse["Success"] = "";
		//echo "<pre>"; print_r($postArray); die;
                $voicePortalActiveStatus = htmlspecialchars($postArray['voicePortalActiveStatus']);
		$xmlinput = xmlHeader($sessionid, "GroupVoiceMessagingGroupModifyVoicePortalRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($postArray['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']) . "</groupId>";
		$xmlinput .= "<serviceInstanceProfile>" ;
		
		if(!empty($postArray['portalPhoneNo'])){
			$xmlinput .= "<phoneNumber>".$postArray['portalPhoneNo']."</phoneNumber>";
		}else{
			$xmlinput .= "<phoneNumber xsi:nil='true'></phoneNumber>";
		}
		if(!empty($postArray['portalExt'])){
			$xmlinput .= "<extension>" . $postArray['portalExt'] . "</extension>";
		}else{
		    $xmlinput .= "<extension xsi:nil='true'></extension>";
		}
		if(!empty($postArray['portalLanguage'])){
			$xmlinput .= "<language>" . $postArray['portalLanguage'] . "</language>";
		}
		if(!empty($postArray['portalTimeZone'])){
			$xmlinput .= "<timeZone>" . $postArray['portalTimeZone'] . "</timeZone>";
		}
		$xmlinput .= "</serviceInstanceProfile>";
                
                if(trim($voicePortalActiveStatus) != ""){
                        $xmlinput .= "<isActive>".$voicePortalActiveStatus."</isActive>";
                }
		$xmlinput .= xmlFooter();	
                
                
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); //die;
		if (readErrorXmlGenuine($xml) != "") {
			$setVpResponse["Error"] = strval($xml->command->summaryEnglish);
		}else{
			$setVpResponse["Success"] = "Success";
			
		}
		return $setVpResponse;
	}
	
	
	//add DN's request in service provider
	public function setVoicePortalModifyRequestWhenAssigns($postArray, $vpConfig){
		global  $sessionid, $client;
		
		$find = array("<groupid>", "<groupName>");
		//$replace = array($_SESSION['groupInfoData']['groupId'], $_SESSION['groupInfoData']['groupName']);
		$time = time();
		$replace = array($_SESSION['groupInfoData']['groupId'], $time);
		
		$configVoicePortalID = str_replace($find, $replace, $vpConfig->get("voicePortalID"));
		$configVoicePortalName = str_replace($find, $replace, $vpConfig->get("voicePortalName"));
		$configClidFirstName = str_replace($find, $replace, $vpConfig->get("clidFirstName"));
		$configClidLastName = str_replace($find, $replace, $vpConfig->get("clidLastName"));
		
		$setVpResponse["Error"] = "";
		$setVpResponse["Success"] = "";
		$xmlinput = xmlHeader($sessionid, "GroupVoiceMessagingGroupModifyVoicePortalRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']) . "</groupId>";
		$xmlinput .= "<serviceInstanceProfile>" ;
		if(!empty($configVoicePortalName)){
			$xmlinput .= "<name>" . $configVoicePortalName. "</name>";
		}
		if(!empty($configClidLastName)){
			$xmlinput .= "<callingLineIdLastName>" . $configClidLastName . "</callingLineIdLastName>";
		}
		if(!empty($configClidFirstName)){
			$xmlinput .= "<callingLineIdFirstName>" . $configClidFirstName. "</callingLineIdFirstName>";
		}		
		if(!empty($postArray['portalPhoneNo'])){
			$xmlinput .= "<phoneNumber>".$postArray['portalPhoneNo']."</phoneNumber>";
		}else{
			$xmlinput .= "<phoneNumber xsi:nil='true'></phoneNumber>";
		}
		$xmlinput .= "</serviceInstanceProfile>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			$setVpResponse["Error"] = strval($xml->command->summaryEnglish);
		}else{
			$setVpResponse["Success"] = "Success";
			
		}
		return $setVpResponse;
	}
	
	public function setVoicePortalAssignRequestWhenAssign($postArray){
		global  $sessionid, $client;
		
		$setVpResponse["Error"] = "";
		$setVpResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "GroupVoiceMessagingGroupModifyVoicePortalRequest");
		$xmlinput .= "<serviceProviderId>" . htmlspecialchars($postArray['sp']) . "</serviceProviderId>";
		$xmlinput .= "<groupId>" . htmlspecialchars($postArray['groupId']) . "</groupId>";
		$xmlinput .= "<serviceInstanceProfile>" ;
		if(!empty($postArray['voicePortalName'])){
			$xmlinput .= "<name>" . $postArray['voicePortalName']. "</name>";
		}
		if(!empty($postArray['voicePortalCLIDLName'])){
			$xmlinput .= "<callingLineIdLastName>" . $postArray['voicePortalCLIDLName']. "</callingLineIdLastName>";
		}
		if(!empty($postArray['voicePortalCLIDFName'])){
			$xmlinput .= "<callingLineIdFirstName>" . $postArray['voicePortalCLIDFName']. "</callingLineIdFirstName>";
		}
		if(!empty($postArray['portalPhoneNo'])){
			$xmlinput .= "<phoneNumber>".$postArray['portalPhoneNo']."</phoneNumber>";
		}else{
			$xmlinput .= "<phoneNumber xsi:nil='true'></phoneNumber>";
		}
		$xmlinput .= "</serviceInstanceProfile>";
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		//echo "<pre>"; print_r($xml); die;
		if (readErrorXmlGenuine($xml) != "") {
			//$setVpResponse["Error"] = strval($xml->command->summaryEnglish);
			if(empty($xml->command->detail)){
				$setVpResponse["Error"] = strval($xml->command->summaryEnglish);
			}else{
				$setVpResponse["Error"] = strval($xml->command->detail);
			}
		}else{
			$setVpResponse["Success"] = "Success";
			
		}
		return $setVpResponse;
	}
	
	public function modifyVoicePortalId($postArray){
		global  $sessionid, $client;
		
		$modifyVoicePortalIdResponse["Error"] = "";
		$modifyVoicePortalIdResponse["Success"] = "";
		
		$xmlinput = xmlHeader($sessionid, "UserModifyUserIdRequest");
		$xmlinput .= "<userId>".$postArray['userId']."</userId>";
		$xmlinput .= "<newUserId>".$postArray['newUserId']."</newUserId>";
		
		$xmlinput .= xmlFooter();
		
		$response = $client->processOCIMessage(array("in0" => $xmlinput));
		$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
		
		if (readErrorXmlGenuine($xml) != "") {
			$modifyVoicePortalIdResponse["Error"] = strval($xml->command->summaryEnglish);
		}else{
			$modifyVoicePortalIdResponse["Success"] = "Success";
			
		}
		return $modifyVoicePortalIdResponse;
	}
}