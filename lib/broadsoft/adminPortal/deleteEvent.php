<?php
	$xmlinput = xmlHeader($sessionid, "GroupScheduleDeleteEventListRequest");
	$xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]) . "</serviceProviderId>";
	$xmlinput .= "<groupId>" . htmlspecialchars($_SESSION["groupId"]) . "</groupId>";
	$xmlinput .= "<scheduleKey>
				<scheduleName>" . htmlspecialchars($_POST["schedName"]) . "</scheduleName>
				<scheduleType>" . $_POST["schedType"] . "</scheduleType>
			</scheduleKey>";
	$xmlinput .= "<eventName>" . htmlspecialchars($_POST["delEventName"]) . "</eventName>";
	$xmlinput .= xmlFooter();
	$response = $client->processOCIMessage(array("in0" => $xmlinput));
	$xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
?>
