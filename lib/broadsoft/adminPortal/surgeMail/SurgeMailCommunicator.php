<?php

class SurgeMailCommunicator {
    
    function __construct($server, $username, $password, $port) {
        $this->server = $server;
        $this->username = $username;
        $this->password = $password;
        $this->port = $port;
    }
    
    function createMailingUser($emailAddress, $emailPassword) {
    	
    	$commandToSend = "/usr/bin/sudo -S /usr/bin/tellmail add_user <userid> <password>";
        $output ="";
        $commandToSend = str_replace('<userid>', $emailAddress, $commandToSend);
        $commandToSend = str_replace('<password>', $emailPassword, $commandToSend);
        $output = $this->sendCommand($commandToSend);
        return $output;
    }
    
    function deleteMailingUser($emailAddress, $emailPassword) {
    
    	$commandToSend = "/usr/bin/sudo -S /usr/bin/tellmail delete_user <userid> <password>";
        $output ="";
        $commandToSend = str_replace('<userid>', $emailAddress, $commandToSend);
        $commandToSend = str_replace('<password>', $emailPassword, $commandToSend);
        $output = $this->sendCommand($commandToSend);
        return $output;
    }
    
    function createUserListFile($domain){
        $commandToSend = "/usr/bin/sudo -S /usr/bin/tellmail find_user <domain>";
        $output ="";
        $commandToSend = str_replace('<domain>', $domain, $commandToSend);
        $output = $this->sendCommand($commandToSend);
        return $output;
    }
    
    /**
     * @param commandToSend*/
    function sendCommand($commandToSend)  {
        $outputResp = "";
        $connection = ssh2_connect($this->server, $this->port);
        ssh2_auth_password($connection, $this->username, $this->password);
        $stream = ssh2_exec($connection, $commandToSend);
        stream_set_blocking($stream, true);
        $outputResp = stream_get_contents($stream);
        $connection = null;
        return $outputResp;
    }
    //check if SSH connected or not
    function checkConnection(){
        $returnVar = array();
        try {
            $connection = ssh2_connect($this->server, $this->port);
            if(!$connection){
                $returnVar[0] = "false";
                $returnVar[1] = "Could not connect to $this->server";
                return $returnVar;
            }
            $auth  = ssh2_auth_password($connection, $this->username, $this->password);
            if(!$auth){
                $returnVar[0] = "false";
                $returnVar[1] = "Could not authenticate with username $this->username and password";
                return $returnVar;
            }
            $sftp = ssh2_sftp($connection);
            if(!$sftp){
                $returnVar[0] = "false";
                $returnVar[1] = "Could not initialize SFTP subsystem.";
                return $returnVar;
            }
            if($connection && $auth && $sftp) {
                $returnVar[0] = "true";
                $returnVar[1] = "CONNECTED";
                return $returnVar;
            }
            $connection = NULL;
            
        } catch (Exception $e) {
            error_log("Error due to :".$e->getMessage());
            return false;
        }
    }
  
}