<?php 
class DeviceTypeManagement {
    function getAllSystemDeviceTypes(){
        global  $sessionid, $client;
        $deviceTypeList["Error"] = array();
        $deviceTypeList["Success"] = array();
        
        $xmlinput = xmlHeader($sessionid, "SystemSIPDeviceTypeGetListRequest");
        //$xmlinput .= "<deviceType>AUDIO-MP-118FXSFXO</deviceType>";
        $xmlinput .= xmlFooter();
        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
        //print_r($xml);
        if (readErrorXmlGenuine ( $xml ) != "") {
            $deviceTypeList["Error"] = strval ( $xml->command->summaryEnglish );
        }else{
            if(isset($xml->command->deviceTypeTable->row) && count($xml->command->deviceTypeTable->row) > 0){
                foreach ($xml->command->deviceTypeTable->row as $key => $value){
                    $deviceTypeList["Success"][] = strval($value->col[0]);
                }
            }
        }
        return $deviceTypeList;
    }
    
    function getDeviceTypesDetails($deviceTypeArray, $ociVersion){
        global  $sessionid, $client;
        $deviceTypeDetails["Error"] = "";
        $deviceTypeDetails["Success"] = "";
        foreach ($deviceTypeArray as $key => $value){
            if($ociVersion == "20"){
                $xmlinput = xmlHeader($sessionid, "SystemSIPDeviceTypeGetRequest20");
            }else if($ociVersion == "21"){
                $xmlinput = xmlHeader($sessionid, "SystemSIPDeviceTypeGetRequest21sp1V2");
            }else{
                $xmlinput = xmlHeader($sessionid, "SystemSIPDeviceTypeGetRequest22");
            }
            $xmlinput .= "<deviceType>".htmlspecialchars($value)."</deviceType>";
            $xmlinput .= xmlFooter();
            $response = $client->processOCIMessage(array("in0" => $xmlinput));
            $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);
            //echo $value; print_r($xml);
            if (readErrorXmlGenuine ( $xml ) != "") {
                $deviceTypeDetails["Error"][$value] = strval ( $xml->command->summaryEnglish );
            }else{
                $deviceTypeDetails["Success"][$value]["isObsolete"] = strval($xml->command->isObsolete);
                if(isset($xml->command->numberOfPorts->unlimited) && $xml->command->numberOfPorts->unlimited == "true"){
                    $deviceTypeDetails["Success"][$value]["numberOfPorts"] = 16;// if value is unlimited then it will set to 16
                }else{
                    $deviceTypeDetails["Success"][$value]["numberOfPorts"] = strval($xml->command->numberOfPorts->quantity);
                }
                $deviceTypeDetails["Success"][$value]["profile"] = strval($xml->command->profile);
                
            }
        }
        //print_r($deviceTypeDetails);
        return $deviceTypeDetails;
    }
    
    function getSystemTableDevices() {
        global $db;
        $deviceTypeList = array();
        
        //Code added @ 16 July 2019
        $whereCndtn = " WHERE 1=1 ";
        if($_SESSION['cluster_support']){
            $selectedCluster = $_SESSION['selectedCluster'];
            $whereCndtn = " WHERE clusterName ='$selectedCluster' ";
        }
        //End code
        
        $stmt = $db->prepare("SELECT * FROM systemDevices $whereCndtn ORDER BY deviceType");
        if ($stmt->execute()) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $deviceTypeList[$row['deviceType']]["enabled"] = $row['enabled'];
                $deviceTypeList[$row['deviceType']]["phoneType"] = $row['phoneType'];
                $deviceTypeList[$row['deviceType']]["softPhoneType"] = $row['softPhoneType'];
                $deviceTypeList[$row['deviceType']]["blf"] = $row['blf'];
                $deviceTypeList[$row['deviceType']]["customTagSpec"] = $row['customTagSpec'];
                $deviceTypeList[$row['deviceType']]["vdmTemplate"] = $row['vdmTemplate'];
                $deviceTypeList[$row['deviceType']]["ports"] = $row['ports'];
                $deviceTypeList[$row['deviceType']]["fxoPorts"] = $row['fxoPorts'];
                $deviceTypeList[$row['deviceType']]["sasLines"] = $row['sasLines'];
                $deviceTypeList[$row['deviceType']]["scaOnly"] = $row['scaOnly'];
                $deviceTypeList[$row['deviceType']]["sasTest"] = $row['sasTest'];
                $deviceTypeList[$row['deviceType']]["nameSubstitution"] = $row['nameSubstitution'];
                $deviceTypeList[$row['deviceType']]["description"] = $row['description'];
            }
        }
        
        return $deviceTypeList;
    }
    
    function getSystemTableDevicesForDiff() {
        global $db;
        $deviceTypeListArray = array();
        //Code added @ 16 July 2019
        $whereCndtn = " WHERE 1=1 ";
        if($_SESSION['cluster_support']){ 
            $selectedCluster = $_SESSION['selectedCluster'];
            $whereCndtn = " WHERE clusterName ='$selectedCluster' ";
        }
        //End code
        $stmt = $db->prepare("SELECT deviceType FROM systemDevices $whereCndtn ORDER BY deviceType");
        if ($stmt->execute()) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $deviceTypeListArray[] = $row["deviceType"];
            }
        }
        return $deviceTypeListArray;
    }
    
    function getEnterpriseTableDevices($sp) {
        global $db;
        $deviceTypeList = array();
        
        //Code added @ 16 July 2019
        $whereCndtn = " ";
        if($_SESSION['cluster_support']){ 
            $selectedCluster = $_SESSION['selectedCluster'];
            $whereCndtn = " AND clusterName ='$selectedCluster' ";
        }
        //End code        
        //$stmt = $db->prepare('SELECT * FROM enterpriseDevices WHERE enterpriseId="'.$sp.'"');
        $stmt = $db->prepare("SELECT * FROM enterpriseDevices WHERE enterpriseId='$sp' $whereCndtn ");
        if ($stmt->execute()) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $deviceTypeList[$row['deviceType']]["enabled"] = $row['enabled'];
                $deviceTypeList[$row['deviceType']]["phoneType"] = $row['phoneType'];
                $deviceTypeList[$row['deviceType']]["blf"] = $row['blf'];
                $deviceTypeList[$row['deviceType']]["ports"] = $row['ports'];
                $deviceTypeList[$row['deviceType']]["description"] = $row['description'];
            }
        }
        
        return $deviceTypeList;
    }
    
    function getVDMTemplate() {
        global $db;
        $deviceTypeList = array();
        $stmt = $db->prepare('SELECT * FROM vdmTemplateLookup ORDER BY name');
        if ($stmt->execute()) {
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $deviceTypeList[$row['id']] = $row['name'];
            }
        }
        return $deviceTypeList;
    }
}
?>