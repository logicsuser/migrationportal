<?php

/**
 * Date: 7/13/2017
 */
class UserServiceGetAssignmentList
{
    private $userServices = array();
	private $servicePacks = array();

    /**
     * UserServiceGetAssignmentList constructor.
     * @param $userId
     */
    public function __construct($userId)
    {
        global $sessionid, $client;
        $xmlinput = xmlHeader($sessionid, "UserServiceGetAssignmentListRequest");
        $xmlinput .= "<userId>" . $userId . "</userId>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        foreach($xml->command->userServicesAssignmentTable->row as $key => $value){
            if($value->col[1] == "true"){
                $this->userServices[] = strval($value->col[0]);
            }
        }

        foreach ($xml->command->servicePacksAssignmentTable->row as $key => $value){
            if($value->col[1] == "true"){
                $servicepackServices = self::getServicePackServices($value->col[0]);
	            $this->servicePacks[(string)$value->col[0]] = $servicepackServices;
                foreach ($servicepackServices as $srv) {
                    if (! in_array($srv, $this->userServices)) {
                        $this->userServices[] = $srv;
                    }
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getUserServices() {
        return $this->userServices;
    }

	public function getservicePacks() {
		return $this->servicePacks;
	}

    /**
     * @param $serviceName
     * @return bool
     */
    public function hasService($serviceName) {
        return in_array($serviceName, $this->userServices);
    }

    private static function getServicePackServices($servicePackName) {
        global $sessionid, $client;
        $xmlinput = xmlHeader($sessionid, "ServiceProviderServicePackGetDetailListRequest");
        $xmlinput .= "<serviceProviderId>" . htmlspecialchars($_SESSION["sp"]). "</serviceProviderId>";
        $xmlinput .= "<servicePackName>" . $servicePackName. "</servicePackName>";
        $xmlinput .= xmlFooter();

        $response = $client->processOCIMessage(array("in0" => $xmlinput));
        $xml = new SimpleXMLElement($response->processOCIMessageReturn, LIBXML_NOWARNING);

        $userServices = array();
        foreach ($xml->command->userServiceTable->row as $key => $value){
            $userServices[] = strval($value->col[0]);
        }
        return $userServices;
    }
}
