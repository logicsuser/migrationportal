<?php
require_once("/var/www/html/Express/config.php");

class ServerConnectionUtil {
	
	public function checkServerConnectionStatus() {
	    
		$alternateSerIsCononected = "false";
		$defaultSerIsCononected = "false";
		$client = "";
		$statusMessage = "";
		
		$opts = array(
			'http' => array(
				'user_agent'=>'PHPSoapClient'
			),
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false
			)
		);

		$context = stream_context_create($opts);

		$soapClientOptions = array(
			'stream_context' => $context,
			'cache_wsdl' => WSDL_CACHE_NONE,
			"exceptions" => 0,
			"trace" => 1
		);

		libxml_disable_entity_loader(false);

		$soapClientOptions["exceptions"] = 1;

		$checkToAlternateServer = isset($_SESSION['server_connection_detail']['current_server_connection']) && $_SESSION['server_connection_detail']['current_server_connection'] == "alternate" ? true : false;
		$message = array();

		try {
		
		$GLOBALS['serversDetail'] = $this->getServerDetails();
		$serverConnectionStatus = checkServerIsActive($GLOBALS['serversDetail']);
		if( !$serverConnectionStatus->defaultServerConnected || ($checkToAlternateServer && isset($serverConnectionStatus->alternateServerConnected)) ) {
			$_SESSION['server_connection_detail']['isDefaultSerConnected'] = "false";
			$message['connected_to_default'] = "false";
			if( $serverConnectionStatus->alternateServerConnected ) {
				$alternateSerIsCononected = $_SESSION['server_connection_detail']['isAlternateSerConnected'] = "true";
				$bwProtocol = $GLOBALS['serversDetail']->alternateAppServer->bwProtocol;
				$bw_ews = $GLOBALS['serversDetail']->alternateAppServer->ip;
				$bwPort = $GLOBALS['serversDetail']->alternateAppServer->port;
				$bwuserid = $GLOBALS['serversDetail']->alternateAppServer->userId;
				$bwpasswd = $GLOBALS['serversDetail']->alternateAppServer->password;
				
				$client = new SoapClient("$bwProtocol://" . $bw_ews . ":$bwPort/webservice/services/ProvisioningService?wsdl", $soapClientOptions);
				
			} else {
				$_SESSION['server_connection_detail']['isAlternateSerConnected'] = "false";
				$message['connected_to_alternate'] = "false";
			}
		} else {
			$defaultSerIsCononected = $_SESSION['server_connection_detail']['isDefaultSerConnected'] = "true";
		}

		if($_SESSION['superUser'] != "2") {
		    $statusMessage = $this->showDialogOnServerFailOver($message);
	    }
		
		}catch (SoapFault $E) {
			//             echo $E->faultstring;
		}
		
		return array("alternateSerIsCononected" => $alternateSerIsCononected, "client" => $client, "defaultSerIsCononected" => $defaultSerIsCononected, "statusMessage" => $statusMessage);
	}
	
	
	public function showDialogOnServerFailOver( $message ) {
		$response = "";
		$switchingToAlternate = false;
		$allServerFailed = false;
		
		if( $_SESSION['server_connection_detail']['current_server_connection'] != "alternate" && $message['connected_to_default'] == "false" || 
			$_SESSION['server_connection_detail']['current_server_connection'] == "alternate" && $message['connected_to_alternate'] == "false"
			) {
				if( $_SESSION['server_connection_detail']['isDefaultSerConnected'] === 'false' && $_SESSION['server_connection_detail']['isAlternateSerConnected']  === 'true') {
					$switchingToAlternate = true;
				} else if( $_SESSION['server_connection_detail']['isDefaultSerConnected'] == 'false' && $_SESSION['server_connection_detail']['isAlternateSerConnected'] == 'false') {
					$allServerFailed = true;
				}
				//print_r($switchingToAlternate); echo"LPLPLPLP"; print_r($allServerFailed);
				if( $switchingToAlternate || $allServerFailed) {
					$_SESSION["do_not_load_fail_over_action"] = "false";
					
					if( $switchingToAlternate ) {
						$response .= "<p style='display:none'> server connection error, Express cannot connect to default Feature Server; switching to alternate server </p>";
						echo '<script type="text/javascript">',
						'showServerErrorMessage = true; ',
						'serverConnectionStatusMsg = "Express cannot connect to default Feature Server; switching to alternate server."',
						'</script>'
						;
					}
					
					if( $allServerFailed ) {
						$response .= "<p style='display:none'> server connection error, Connection with Feature Server has failed. Please contact Express Administrator. </p>";
						echo '<script type="text/javascript">',
						'showServerErrorMessage = true; ',
						'serverConnectionStatusMsg = "Connection with Feature Server has failed. Please contact Express Administrator."',
						'</script>'
						;
					}
					
					echo '<script type="text/javascript">',
					'if(typeof fail_over_dialog_on_OciFailure !== "undefined") { ',
					'fail_over_dialog_on_OciFailure()',
					'}',
					'</script>'
						;
					//die;
				}
				
				
		   }
	   
		return $response;
	}

	
	public function getServerDetails() {
	    global $db;
	    
	    $serverConfiguration = new ServerConfiguration($db,appsServer,primaryServer);
	    $defaultBwAppServer = "primaryServer";
	    if ($serverConfiguration->getActive() != "true") {
	        $defaultBwAppServer = "secondaryServer";
	        $serverConfiguration = new ServerConfiguration($db,appsServer,secondaryServer);
	    }
	    
	    $serversDetail->defaultAppServer->ip = $serverConfiguration->getIP();																			//TODO: CHANGE NAME $bw_ews to $bwServerIP
	    $serversDetail->defaultAppServer->userId = $serverConfiguration->getUserName();																	//TODO: CHANGE NAME $bwuserid to $bwUserName
	    $serversDetail->defaultAppServer->password = $serverConfiguration->getPassword();																	//TODO: CHANGE NAME $bwpasswd to $bwPassword
	    $serversDetail->defaultAppServer->bwProtocol = $serverConfiguration->getProtocol(); //code added @ 10 May 2018
	    $serversDetail->defaultAppServer->port = $serverConfiguration->getPort();     //Code added @ 10 May 2018
	    $serversDetail->defaultAppServer->serverType = $defaultBwAppServer;
	    /* Start fail over server detail */
	    $alternateBwAppServer = $defaultBwAppServer == "primaryServer" ? secondaryServer : primaryServer;
	    $alternateBwAppServerDetail = new ServerConfiguration($db,appsServer,$alternateBwAppServer);
	    
	    $serversDetail->alternateAppServer->ip = $alternateBwAppServerDetail->getIP();
	    $serversDetail->alternateAppServer->userId = $alternateBwAppServerDetail->getUserName();
	    $serversDetail->alternateAppServer->password = $alternateBwAppServerDetail->getPassword();
	    $serversDetail->alternateAppServer->bwProtocol = $alternateBwAppServerDetail->getProtocol();
	    $serversDetail->alternateAppServer->port = $alternateBwAppServerDetail->getPort();
	    $serversDetail->alternateAppServer->serverType = $defaultBwAppServer == "primaryServer" ? "secondaryServer" : "primaryServer";
	    
	    return $serversDetail;
	}
	
}

?>